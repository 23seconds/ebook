USE [eBook]
GO
/****** Object:  Table [dbo].[TPBOOK_SERVICE_STATUSES]    Script Date: 5/03/2015 22:29:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TPBOOK_SERVICE_STATUSES](
	[ServiceStatuses_StatusId] [smallint] NOT NULL,
	[ServiceStatuses_Status] [varchar](250) NOT NULL,
	[ServiceStatuses_ServiceId] [uniqueidentifier] NOT NULL,
	[ServiceStatuses_Rank] [int] NOT NULL,
	[ServiceStatuses_Activation] [varchar](100) NOT NULL,
	[ServiceStatuses_FollowUpStatuses] [varchar](20) NULL,
	[ServiceStatuses_FollowUpActions] [varchar](500) NULL,
 CONSTRAINT [PK_TPBOOK_SERVICE_STATUSES] PRIMARY KEY CLUSTERED 
(
	[ServiceStatuses_StatusId] ASC,
	[ServiceStatuses_ServiceId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[TPBOOK_SERVICE_STATUSES] ([ServiceStatuses_StatusId], [ServiceStatuses_Status], [ServiceStatuses_ServiceId], [ServiceStatuses_Rank], [ServiceStatuses_Activation], [ServiceStatuses_FollowUpStatuses], [ServiceStatuses_FollowUpActions]) VALUES (1, N'BizTax automated', N'bd7c2eae-8500-4103-8984-0131e73d07fa', 1, N'User', NULL, N'[]')
INSERT [dbo].[TPBOOK_SERVICE_STATUSES] ([ServiceStatuses_StatusId], [ServiceStatuses_Status], [ServiceStatuses_ServiceId], [ServiceStatuses_Rank], [ServiceStatuses_Activation], [ServiceStatuses_FollowUpStatuses], [ServiceStatuses_FollowUpActions]) VALUES (1, N'BizTax manual', N'60ca9df9-c549-4a61-a2ad-3fdb1705cf55', 1, N'User', NULL, N'[]')
INSERT [dbo].[TPBOOK_SERVICE_STATUSES] ([ServiceStatuses_StatusId], [ServiceStatuses_Status], [ServiceStatuses_ServiceId], [ServiceStatuses_Rank], [ServiceStatuses_Activation], [ServiceStatuses_FollowUpStatuses], [ServiceStatuses_FollowUpActions]) VALUES (1, N'In progress', N'c3b8c8db-3822-4263-9098-541fae897d02', 1, N'System', N'2', N'[{type: "commit",text: "Send to Executive for Approval",followUpStatus: 2,activation:"Staff"}]')
INSERT [dbo].[TPBOOK_SERVICE_STATUSES] ([ServiceStatuses_StatusId], [ServiceStatuses_Status], [ServiceStatuses_ServiceId], [ServiceStatuses_Rank], [ServiceStatuses_Activation], [ServiceStatuses_FollowUpStatuses], [ServiceStatuses_FollowUpActions]) VALUES (2, N'Awaiting Approval by Executive', N'c3b8c8db-3822-4263-9098-541fae897d02', 2, N'Staff', N'3,4', N'[{type: "lock",followUpStatus: 2,activation:"Manager,Director,Partner"},{type: "approve",text: "Approve",followUpStatus: 3,activation:"Manager,Director,Partner"},{type: "reject",text: "Reject",followUpStatus: 4,activation:"Manager,Director,Partner"}]')
INSERT [dbo].[TPBOOK_SERVICE_STATUSES] ([ServiceStatuses_StatusId], [ServiceStatuses_Status], [ServiceStatuses_ServiceId], [ServiceStatuses_Rank], [ServiceStatuses_Activation], [ServiceStatuses_FollowUpStatuses], [ServiceStatuses_FollowUpActions]) VALUES (3, N'Approved by Executive', N'c3b8c8db-3822-4263-9098-541fae897d02', 3, N'Partner', N'5,6', N'[{type: "approve",text: "Approve",followUpStatus: 5,activation:"Partner"},{type: "reject",text: "Reject",followUpStatus:6,activation:"Partner"}]')
INSERT [dbo].[TPBOOK_SERVICE_STATUSES] ([ServiceStatuses_StatusId], [ServiceStatuses_Status], [ServiceStatuses_ServiceId], [ServiceStatuses_Rank], [ServiceStatuses_Activation], [ServiceStatuses_FollowUpStatuses], [ServiceStatuses_FollowUpActions]) VALUES (4, N'Rejected by Executive', N'c3b8c8db-3822-4263-9098-541fae897d02', 3, N'Staff', N'2', N'[{type: "commit",text: "Send to Executive for Approval",followUpStatus: 2,activation:"Staff"}]')
INSERT [dbo].[TPBOOK_SERVICE_STATUSES] ([ServiceStatuses_StatusId], [ServiceStatuses_Status], [ServiceStatuses_ServiceId], [ServiceStatuses_Rank], [ServiceStatuses_Activation], [ServiceStatuses_FollowUpStatuses], [ServiceStatuses_FollowUpActions]) VALUES (5, N'Approved by Partner', N'c3b8c8db-3822-4263-9098-541fae897d02', 4, N'OfficeValidator', N'7', N'[{type: "commit",text: "Sent to Client for Approval",followUpStatus: 7,activation:"OfficeValidator"}]')
INSERT [dbo].[TPBOOK_SERVICE_STATUSES] ([ServiceStatuses_StatusId], [ServiceStatuses_Status], [ServiceStatuses_ServiceId], [ServiceStatuses_Rank], [ServiceStatuses_Activation], [ServiceStatuses_FollowUpStatuses], [ServiceStatuses_FollowUpActions]) VALUES (6, N'Rejected by Partner', N'c3b8c8db-3822-4263-9098-541fae897d02', 4, N'Manager,Director,Partner', N'3', N'[{type: "lock",followUpStatus: 2,activation:"Manager,Director,Partner"},{type: "approve",text: "Approve",followUpStatus: 3,activation:"Manager,Director,Partner"},{type: "reject",text: "Reject",followUpStatus: 4,activation:"Manager,Director,Partner"}]')
INSERT [dbo].[TPBOOK_SERVICE_STATUSES] ([ServiceStatuses_StatusId], [ServiceStatuses_Status], [ServiceStatuses_ServiceId], [ServiceStatuses_Rank], [ServiceStatuses_Activation], [ServiceStatuses_FollowUpStatuses], [ServiceStatuses_FollowUpActions]) VALUES (7, N'Sent to Client for Approval', N'c3b8c8db-3822-4263-9098-541fae897d02', 4, N'Manager,Director,Partner,OfficeValidator', N'8,9', N'[{type: "approve",text: "Accepted by Client",followUpStatus: 8,activation:"Manager,Director,Partner,OfficeValidator"},{type: "reject",text: "Rejected by Client",followUpStatus: 9,activation:"Manager,Director,Partner,OfficeValidator"}]')
INSERT [dbo].[TPBOOK_SERVICE_STATUSES] ([ServiceStatuses_StatusId], [ServiceStatuses_Status], [ServiceStatuses_ServiceId], [ServiceStatuses_Rank], [ServiceStatuses_Activation], [ServiceStatuses_FollowUpStatuses], [ServiceStatuses_FollowUpActions]) VALUES (8, N'Accepted by client', N'c3b8c8db-3822-4263-9098-541fae897d02', 5, N'Manager,Director,Partner,OfficeValidator', N'10', N'[{type: "upload",text: "Upload Signed Docs",followUpStatus: 10,activation:"Manager,Director,Partner,OfficeValidator"}]')
INSERT [dbo].[TPBOOK_SERVICE_STATUSES] ([ServiceStatuses_StatusId], [ServiceStatuses_Status], [ServiceStatuses_ServiceId], [ServiceStatuses_Rank], [ServiceStatuses_Activation], [ServiceStatuses_FollowUpStatuses], [ServiceStatuses_FollowUpActions]) VALUES (9, N'Rejected by client', N'c3b8c8db-3822-4263-9098-541fae897d02', 5, N'Staff', N'2', N'[{type: "commit",text: "Send to Executive for Approval",followUpStatus: 2,activation:"Staff"}]')
INSERT [dbo].[TPBOOK_SERVICE_STATUSES] ([ServiceStatuses_StatusId], [ServiceStatuses_Status], [ServiceStatuses_ServiceId], [ServiceStatuses_Rank], [ServiceStatuses_Activation], [ServiceStatuses_FollowUpStatuses], [ServiceStatuses_FollowUpActions]) VALUES (10, N'Scanned and Signed by Client', N'c3b8c8db-3822-4263-9098-541fae897d02', 6, N'Manager,Director,Partner,OfficeValidator', N'11', N'[{type: "commit",text: "Filed NBB",followUpStatus: 11,activation:"Manager,Director,Partner,OfficeValidator"}]')
INSERT [dbo].[TPBOOK_SERVICE_STATUSES] ([ServiceStatuses_StatusId], [ServiceStatuses_Status], [ServiceStatuses_ServiceId], [ServiceStatuses_Rank], [ServiceStatuses_Activation], [ServiceStatuses_FollowUpStatuses], [ServiceStatuses_FollowUpActions]) VALUES (11, N'Filed NBB', N'c3b8c8db-3822-4263-9098-541fae897d02', 7, N'Manager,Director,Partner,OfficeValidator', NULL, N'[]')
