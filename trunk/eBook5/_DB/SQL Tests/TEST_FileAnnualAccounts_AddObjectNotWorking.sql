USE [eBook]
GO
/****** Object:  Table [dbo].[TPBOOK_FILE_ANNUALACCOUNTS]    Script Date: 30/10/2015 9:57:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TPBOOK_FILE_ANNUALACCOUNTS](
	[FileAnnualAccounts_Id] [uniqueidentifier] NOT NULL,
	[FileAnnualAccounts_FileId] [uniqueidentifier] NOT NULL,
	[FileAnnualAccounts_RepositoryItemId] [uniqueidentifier] NULL,
	[FileAnnualAccounts_DefinitiveGeneralAssembly] [datetime] NULL,
	[FileAnnualAccounts_CompanyType] [varchar](50) NULL,
	[FileAnnualAccounts_Model] [varchar](50) NULL,
	[FileAnnualAccounts_ChangesAfterGeneralAssembly] [bit] NULL,
	[FileAnnualAccounts_TimeLimit] [varchar](max) NULL,
	[FileAnnualAccounts_EngagementCode] [varchar](50) NULL,
	[FileAnnualAccounts_InvoiceLanguage] [varchar](10) NULL,
	[FileAnnualAccounts_InvoiceRechargeFilingExpense] [varchar](50) NULL,
	[FileAnnualAccounts_ChangesAfterGeneralAssemblyDescription] [varchar](max) NULL,
	[FileAnnualAccounts_InvoiceAmount] [decimal](13, 2) NULL,
	[FileAnnualAccounts_InvoiceStructuredMessage] [varchar](50) NULL,
	[FileAnnualAccounts_InvoiceRepositoryItemId] [uniqueidentifier] NULL,
 CONSTRAINT [PK_TPBOOK_FILE_ANNUALACCOUNTS] PRIMARY KEY CLUSTERED 
(
	[FileAnnualAccounts_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[TPBOOK_FILE_ANNUALACCOUNTS] ([FileAnnualAccounts_Id], [FileAnnualAccounts_FileId], [FileAnnualAccounts_RepositoryItemId], [FileAnnualAccounts_DefinitiveGeneralAssembly], [FileAnnualAccounts_CompanyType], [FileAnnualAccounts_Model], [FileAnnualAccounts_ChangesAfterGeneralAssembly], [FileAnnualAccounts_TimeLimit], [FileAnnualAccounts_EngagementCode], [FileAnnualAccounts_InvoiceLanguage], [FileAnnualAccounts_InvoiceRechargeFilingExpense], [FileAnnualAccounts_ChangesAfterGeneralAssemblyDescription], [FileAnnualAccounts_InvoiceAmount], [FileAnnualAccounts_InvoiceStructuredMessage], [FileAnnualAccounts_InvoiceRepositoryItemId]) VALUES (N'3785b597-b94c-440e-a55a-a225f264fae9', N'bbcad48e-0b99-4fde-bfdf-85a1f2ba53f1', N'fe556ffe-1808-4280-b791-ece390d95da4', CAST(0x0000A4E700000000 AS DateTime), N'Enterprise', N'Full presentation', 1, N'Filing before 9th month', N'99999399', N'French', N'Only filing costs', N'<p>Description test 2</p>', CAST(34.66 AS Decimal(13, 2)), N'+++090/9337/55493+++', N'e47f5074-5ee6-4fd7-a755-c04da5c1c2ca')
ALTER TABLE [dbo].[TPBOOK_FILE_ANNUALACCOUNTS]  WITH CHECK ADD  CONSTRAINT [FK_TPBOOK_FILE_ANNUALACCOUNTS_TPBOOK_FILES] FOREIGN KEY([FileAnnualAccounts_FileId])
REFERENCES [dbo].[TPBOOK_FILES] ([FILE_ID])
GO
ALTER TABLE [dbo].[TPBOOK_FILE_ANNUALACCOUNTS] CHECK CONSTRAINT [FK_TPBOOK_FILE_ANNUALACCOUNTS_TPBOOK_FILES]
GO
