
-- SCRIPT REMOVES FILE SERVICE DATA TO TEST FILE SERVICE YEAR END BUNDLE FLOW

--GET CLIENT
	/****** Script for SelectTopNRows command from SSMS  ******/
SELECT *
  FROM [eBook].[dbo].[TPBOOK_CLIENTS]
  WHERE [CLIENT_NAME] like '%TAX TEST CLIENT 07%'

--GET FILES
/****** Script for SelectTopNRows command from SSMS  ******/
SELECT *
  FROM [eBook].[dbo].[TPBOOK_FILES]
  [FILE_CLIENT_ID]
  WHERE FILE_CLIENT_ID in ('59C402DF-C178-4E95-8C9F-FF416A8B3A95','E4B799C4-BBFF-42F3-894C-F92A81A96EB8')
  aND FILE_DELETED = 0
  ORDER BY FILE_CREATION_DATE DESC

-- REMOVE LOG 
/****** Script for SelectTopNRows command from SSMS  ******/
SELECT TOP 1000 [FileServiceLog_Id]
      ,[FileServiceLog_FileId]
      ,[FileServiceLog_ServiceId]
      ,[FileServiceLog_Action]
      ,[FileServiceLog_Comment]
      ,[FileServiceLog_StatusId]
      ,[FileServiceLog_Timestamp]
      ,[FileServiceLog_RepositoryItemId]
      ,[FileServiceLog_PersonId]
      ,[FileServiceLog_PersonFirstName]
      ,[FileServiceLog_PersonLastName]
  FROM [eBook].[dbo].[TPBOOK_FILE_SERVICES_LOG]
  WHERE [FileServiceLog_FileId] = '22D55303-B9EB-4541-B100-8D56A1E1727E'
  AND [FileServiceLog_ServiceId] = 'C3B8C8DB-3822-4263-9098-541FAE897D02'


  DELETE FROM [eBook].[dbo].[TPBOOK_FILE_SERVICES_LOG] WHERE [FileServiceLog_FileId] = '983699fc-ac3e-4b90-bd34-06961430513c' AND [FileServiceLog_ServiceId] = 'C3B8C8DB-3822-4263-9098-541FAE897D02'





-- UPDATE FILE SERVICE RECORD
/****** Script for SelectTopNRows command from SSMS  ******/
SELECT TOP 1000 [FileService_FileId]
      ,[FileServices_ServiceId]
      ,[FileServices_Completed]
      ,[FileServices_CompletedBy]
      ,[FileServices_Order]
      ,[FileServices_Status]
  FROM [eBook].[dbo].[TPBOOK_FILE_SERVICES]
    WHERE  [FileService_FileId] = '983699fc-ac3e-4b90-bd34-06961430513c'
  AND [FileServices_ServiceId] = 'C3B8C8DB-3822-4263-9098-541FAE897D02'


	update  [eBook].[dbo].[TPBOOK_FILE_SERVICES]
	set fileservices_completed = null, fileServices_completedby = null, fileServices_status = 1
	WHERE  [FileService_FileId] = '27256A83-557B-4326-A07C-293C121157B3'
	AND [FileServices_ServiceId] = 'C3B8C8DB-3822-4263-9098-541FAE897D02'



	
begin tran
  update [eBook].[dbo].[TPBOOK_FILE_BUNDLES]
  set [File_Bundle_Locked] = 0, file_bundle_draft = 1
  where file_bundle_file_id = '22D55303-B9EB-4541-B100-8D56A1E1727E' AND file_bundle_name = 'jaarafsluiting' and file_bundle_deleted = 0
  rollback tran


  Select * from [eBook].[dbo].[TPBOOK_FILE_BUNDLES]
 where file_bundle_file_id = '983699FC-AC3E-4B90-BD34-06961430513C' AND file_bundle_name = 'jaarafsluiting' and file_bundle_deleted = 0


  --remove memorandum

 select * from  TPBOOK_REPOSITORY
 where repository_item_client_id = '71109471-330E-4E4D-9A48-3DD6579C27DD' and repository_item_repository_structure_id in ('B1362A5D-924A-41E1-B505-92D984EE40C9','B9069EA7-6A32-41AF-B0B2-F66DB656760B')
 
  begin tran
  update TPBOOK_REPOSITORY
  set repository_item_deleted = 1
where repository_item_client_id = '71109471-330E-4E4D-9A48-3DD6579C27DD' and repository_item_repository_structure_id in ('B1362A5D-924A-41E1-B505-92D984EE40C9','B9069EA7-6A32-41AF-B0B2-F66DB656760B')
  rollback tran



