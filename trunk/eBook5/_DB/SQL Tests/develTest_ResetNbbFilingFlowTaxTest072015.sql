

-- SCRIPT REMOVES FILE SERVICE DATA TO TEST FILE SERVICE YEAR END BUNDLE FLOW

--GET CLIENT
	/****** Script for SelectTopNRows command from SSMS  ******/
SELECT *
  FROM [eBook].[dbo].[TPBOOK_CLIENTS]
  WHERE [CLIENT_NAME] like '%TAX TEST CLIENT 07%'

--GET FILES
/****** Script for SelectTopNRows command from SSMS  ******/
SELECT *
  FROM [eBook].[dbo].[TPBOOK_FILES]
  [FILE_CLIENT_ID]
  WHERE FILE_CLIENT_ID in ('59C402DF-C178-4E95-8C9F-FF416A8B3A95','E4B799C4-BBFF-42F3-894C-F92A81A96EB8')
  aND FILE_DELETED = 0
  ORDER BY FILE_CREATION_DATE DESC

--LOG
SELECT TOP 1000 [FileServiceLog_Id]
      ,[FileServiceLog_FileId]
      ,[FileServiceLog_ServiceId]
      ,[FileServiceLog_Action]
      ,[FileServiceLog_Comment]
      ,[FileServiceLog_StatusId]
      ,[FileServiceLog_Timestamp]
      ,[FileServiceLog_RepositoryItemId]
      ,[FileServiceLog_PersonId]
      ,[FileServiceLog_PersonFirstName]
      ,[FileServiceLog_PersonLastName]
FROM [eBook].[dbo].[TPBOOK_FILE_SERVICES_LOG]
  WHERE [FileServiceLog_FileId] = 'F03CD818-8F95-46D9-9F2E-401439FE5F11'
  AND [FileServiceLog_ServiceId] = '1ABF0836-7D7B-48E7-9006-F03DB97AC28B'


  /****** Script for SelectTopNRows command from SSMS  ******/
SELECT TOP 1000 [FileService_FileId]
      ,[FileServices_ServiceId]
      ,[FileServices_Completed]
      ,[FileServices_CompletedBy]
      ,[FileServices_Order]
      ,[FileServices_Status]
  FROM [eBook].[dbo].[TPBOOK_FILE_SERVICES]

  begin tran
  update  [eBook].[dbo].[TPBOOK_FILE_SERVICES]
  set [FileServices_Status] = 1
  WHERE [FileService_FileId] = 'F03CD818-8F95-46D9-9F2E-401439FE5F11'
  AND [FileServices_ServiceId] = '1ABF0836-7D7B-48E7-9006-F03DB97AC28B'
  rollback tran
