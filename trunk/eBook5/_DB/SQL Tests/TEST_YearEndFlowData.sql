/****** Script for SelectTopNRows command from SSMS  ******/
SELECT TOP 1000 [File_Bundle_ID]
      ,[File_Bundle_File_ID]
      ,[File_Bundle_Template_ID]
      ,[File_Bundle_HeaderFooterType]
      ,[File_Bundle_Name]
      ,[File_Bundle_Culture]
      ,[File_Bundle_Draft]
      ,[File_Bundle_RectoVerso]
      ,[File_Bundle_Contents]
      ,[File_Bundle_Deleted]
      ,[File_Bundle_Locked]
  FROM [eBook].[dbo].[TPBOOK_FILE_BUNDLES]
  INNER JOIN dbo.tpbook_service_bundle_templates on servicebundles_templateID = [File_Bundle_Template_ID] AND servicebundles_serviceId = 'C3B8C8DB-3822-4263-9098-541FAE897D02'
  INNER JOIN dbo.tpbook_file_services on fileservice_fileId = [File_Bundle_File_ID]
  INNER JOIN dbo.tpbook_files on file_Id = fileservice_fileId
  where fileservices_status = 7
  --AND file_bundle_locked = 1
  AND file_client_id in (SELECT [PCR_CLIENT_ID]
  FROM [eBook].[dbo].[TPBOOK_PERSON_CLIENT_ROLE]
  where pcr_person_id = '18BC4A5D-A798-4D20-B097-E2F6E1A66E3E')   --lieve




  