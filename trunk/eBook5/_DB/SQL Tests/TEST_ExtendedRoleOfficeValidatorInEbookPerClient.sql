SELECT DISTINCT dbo.TPMT_CLIENTS.ClientId, pp.PersonGPN, dbo.TPMT_ROLES.RoleName, dbo.TPMT_CLIENTS.ClientGfisCode, et.Team_Source
FROM            dbo.TPMT_CLIENTS INNER JOIN
                         dbo.TPMT_CLIENT_DEPARTMENTS ON dbo.TPMT_CLIENT_DEPARTMENTS.ClientDepartmentClientId = dbo.TPMT_CLIENTS.ClientId INNER JOIN
                         dbo.TPMT_ENGAGEMENT_TEAM AS et ON et.Team_ClientDepartmentID = dbo.TPMT_CLIENT_DEPARTMENTS.ClientDepartmentId INNER JOIN
                         dbo.TPMT_PERSONS AS pp ON pp.PersonID = et.Team_PersonID INNER JOIN
                         dbo.TPMT_ROLES ON dbo.TPMT_ROLES.RoleId = et.Team_RoleID
WHERE        (et.Team_EndDate IS NULL OR
                         et.Team_EndDate > GETDATE()) AND (et.Team_Deleted = 0)
UNION
/*Get ACR Office Validators from extended roles table for Year End flow based on office, department and partner*/ 
SELECT DISTINCT dbo.TPMT_CLIENTS.ClientId, pp.PersonGPN, dbo.TPMT_ROLES.RoleName, dbo.TPMT_CLIENTS.ClientGfisCode, 'PMT'
FROM            dbo.TPMT_CLIENTS INNER JOIN
                         dbo.TPMT_CLIENT_DEPARTMENTS AS cd ON cd.ClientDepartmentClientId = dbo.TPMT_CLIENTS.ClientId INNER JOIN
                         [PMT].[dbo].[TPMT_EXTENDEDROLES] AS er ON er.ExtendedRolesDepartmentId = cd.[ClientDepartmentDepartmentId] AND 
                         er.ExtendedRoleOffice = cd.ClientDepartmentOfficeId INNER JOIN
                         dbo.TPMT_PERSONS AS pp ON pp.PersonID = er.ExtendedRolesPersonId INNER JOIN
                         dbo.TPMT_ROLES ON dbo.TPMT_ROLES.RoleId = er.ExtendedRolesRoleId 
						 INNER JOIN [PMT].[dbo].[TPMT_ENGAGEMENT_TEAM] AS et ON et.Team_ClientDepartmentID = cd.ClientDepartmentId AND et.Team_PersonID = er.ExtendedRolesPartnerId and et.Team_Deleted = 0 and et.Team_EndDate is null and et.Team_RoleID = '9F755631-7D15-4111-8253-733A525FB8D5' --partner
WHERE        (er.ExtendedRoleTo IS NULL OR
                         er.ExtendedRoleTo > GETDATE()) AND er.ExtendedRolesRoleId = 'F7C6F5F5-7339-43DD-9AA2-43B23E7E03F6' AND  --OV
                         er.ExtendedRolesDepartmentId = '4DEC8114-DA26-47FA-AF47-5FFA0463FC88' --ACR





---TEST CHECK CLIENTS WHICH DO NOT GET AN OFFICE VALIDATOR



select 
(select ClientName from  [PMT].[dbo].TPMT_CLIENTS where clientId = cd.ClientDepartmentClientId),
(select officeName from  [PMT].[dbo].[TPMT_OFFICES] where officeID = cd.ClientDepartmentOfficeId),					 
(select personfirstname + ' ' + personlastname from  [PMT].[dbo].[TPMT_PERSONS] where et.Team_PersonID = personId) 
from  [PMT].[dbo].[TPMT_ENGAGEMENT_TEAM] et
INNER JOIN dbo.TPMT_CLIENT_DEPARTMENTS as cd ON et.Team_ClientDepartmentID = cd.ClientDepartmentId and et.Team_Deleted = 0 and et.Team_EndDate is null and et.Team_RoleID = '9F755631-7D15-4111-8253-733A525FB8D5'
INNER JOIN eBook.dbo.TPBOOK_FILES ON cd.ClientDepartmentClientId = file_client_id AND file_deleted = 0 AND file_close_date is null
where cd.ClientDepartmentDepartmentId = '4DEC8114-DA26-47FA-AF47-5FFA0463FC88'
AND cd.ClientDepartmentClientId in
(
SELECT DISTINCT 
cd.ClientDepartmentClientId
FROM            dbo.TPMT_CLIENTS INNER JOIN
                         dbo.TPMT_CLIENT_DEPARTMENTS AS cd ON cd.ClientDepartmentClientId = dbo.TPMT_CLIENTS.ClientId INNER JOIN
                         [PMT].[dbo].[TPMT_EXTENDEDROLES] AS er ON er.ExtendedRolesDepartmentId = cd.[ClientDepartmentDepartmentId] AND 
                         er.ExtendedRoleOffice = cd.ClientDepartmentOfficeId INNER JOIN
                         dbo.TPMT_PERSONS AS pp ON pp.PersonID = er.ExtendedRolesPersonId INNER JOIN
                         dbo.TPMT_ROLES ON dbo.TPMT_ROLES.RoleId = er.ExtendedRolesRoleId 
						 --INNER JOIN [PMT].[dbo].[TPMT_ENGAGEMENT_TEAM] AS et ON et.Team_ClientDepartmentID = cd.ClientDepartmentId AND et.Team_PersonID = er.ExtendedRolesPartnerId and et.Team_Deleted = 0 and et.Team_EndDate is null and et.Team_RoleID = '9F755631-7D15-4111-8253-733A525FB8D5' --partner
WHERE        (er.ExtendedRoleTo IS NULL OR
                         er.ExtendedRoleTo > GETDATE()) AND er.ExtendedRolesRoleId = 'F7C6F5F5-7339-43DD-9AA2-43B23E7E03F6' AND  --OV
                         er.ExtendedRolesDepartmentId = '4DEC8114-DA26-47FA-AF47-5FFA0463FC88' --ACR
						 and clientid not in
						 (SELECT DISTINCT dbo.TPMT_CLIENTS.ClientId
				FROM            dbo.TPMT_CLIENTS INNER JOIN
										 dbo.TPMT_CLIENT_DEPARTMENTS AS cd ON cd.ClientDepartmentClientId = dbo.TPMT_CLIENTS.ClientId INNER JOIN
										 [PMT].[dbo].[TPMT_EXTENDEDROLES] AS er ON er.ExtendedRolesDepartmentId = cd.[ClientDepartmentDepartmentId] AND 
										 er.ExtendedRoleOffice = cd.ClientDepartmentOfficeId INNER JOIN
										 dbo.TPMT_PERSONS AS pp ON pp.PersonID = er.ExtendedRolesPersonId INNER JOIN
										 dbo.TPMT_ROLES ON dbo.TPMT_ROLES.RoleId = er.ExtendedRolesRoleId 
										 INNER JOIN [PMT].[dbo].[TPMT_ENGAGEMENT_TEAM] AS et ON et.Team_ClientDepartmentID = cd.ClientDepartmentId AND et.Team_PersonID = er.ExtendedRolesPartnerId and et.Team_Deleted = 0 and et.Team_EndDate is null and et.Team_RoleID = '9F755631-7D15-4111-8253-733A525FB8D5' --partner
				WHERE        (er.ExtendedRoleTo IS NULL OR
										 er.ExtendedRoleTo > GETDATE()) AND er.ExtendedRolesRoleId = 'F7C6F5F5-7339-43DD-9AA2-43B23E7E03F6' AND  --OV
										 er.ExtendedRolesDepartmentId = '4DEC8114-DA26-47FA-AF47-5FFA0463FC88')
										 )
										 order by cd.ClientDepartmentOfficeId
										 