/****** Script for SelectTopNRows command from SSMS  ******/
SELECT ccp.[BancAccountNr]
      ,ccp.[BIC]
      ,[ClientId]
	  , c.CLIENT_NAME
	  , bt.Nl
	  , ccps.Status
      ,[LastModified]
      ,[RepositoryItemId]
      ,[DocStoreId]
  FROM [eBook].[dbo].[TPBOOK_CLIENT_CODA_POA] ccp
  INNER JOIN [eBook].[dbo].[TPBOOK_CLIENT_CODA_POA_STATUSES] AS ccps ON ccp.StatusId = ccps.Id
  INNER JOIN [eBook].[dbo].[TPBOOK_CLIENTS] AS c ON ccp.ClientId = c.CLIENT_ID
  INNer JOIN [eBook].[dbo].[BANK_TRANSLATION] AS bt ON bt.Bic = ccp.Bic
