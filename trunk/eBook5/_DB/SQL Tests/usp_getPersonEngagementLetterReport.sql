	declare 
 @rowsTo int,
	 @personId varchar(max),
	 @statusId varchar(50),
	 @startdateTo datetime,
	 @startdateFrom datetime,
	 @enddateTo datetime,
	 @enddateFrom datetime,
	 @templateId varchar(max),
	 @validatedByGTH varchar(5),
	 @clientName varchar(100),
	--@renewal bit,
	 @expiration int,
	 @culture varchar(5),
	 @rowsFrom int,
	 @limit int,
	 @count int,-- OUTPUT;
	  @cnt int,
	  @activeClient varchar(5);

--BEGIN
		set @statusId = '6a9821f5-8724-4926-a815-6cb02c9ca83d';
		set @startdateTo = '20500101';
		set @startdateFrom = '19000101';
		set @enddateTo = '20500101';
		set @enddateFrom = '19000101';
		--set @templateId = '25dd8508-069f-4571-b719-f5391b989789';
		set @templateId = '%'
		set @validatedByGTH = '%';
		set @clientName = '%';
		set @expiration = 0;
		set @culture = 'nl-BE';
		set @limit = 25;
		set @personId = '18BC4A5D-A798-4D20-B097-E2F6E1A66E3E';
		set @rowsFrom = 0;
		set @activeClient = null;
		set @rowsTo = @rowsFrom + @limit;


--DECLARE @cnt int;
/*
--show admin all
select @cnt=count(*) 
from TPBOOK_PERSON_ROLES
where personrole_person_id=@personid
	and PersonRole_Role like 'Admin%'


if @cnt > 0
BEGIN
	set @personid = null
	print 'null person'
END;
*/

with engletters as (
	select *,ROW_NUMBER() OVER (ORDER BY clientName) AS RN
	from (
		-- Engagement letter IN repository
		select 

			manager.person_firstname + ' ' + manager.person_lastname as pmanager,
			partner1.person_firstname + ' ' + partner1.person_lastname as ppartner,
			CAST(CAST(rep.REPOSITORY_ITEM_ID AS VARCHAR(150)) + CAST(cl.client_id AS VARCHAR(150)) AS VARCHAR(150))  as pcelstatId,
			rep.REPOSITORY_ITEM_ID as ItemId,
			cl.client_name as clientName,
			cl.client_gfis_code as clientGFIS,	
			cl.client_id as clientId,
			rep.REPOSITORY_ITEM_META.value('declare namespace d="http://schemas.datacontract.org/2004/07/EY.com.eBook.API.Contracts.Data";(//d:RepositoryMetaItemDataContract[d:Id="DateELEA"]/d:Value)[1]','varchar(12)') as DateELEA,
			rep.REPOSITORY_ITEM_STARTDATE as itemStartDate,
			rep.REPOSITORY_ITEM_ENDDATE as itemEndDate,
			DATEDIFF(DAY, getdate(), rep.REPOSITORY_ITEM_ENDDATE) as expiration,
			'' as renewed,
			rep.REPOSITORY_ITEM_META.value('declare namespace d="http://schemas.datacontract.org/2004/07/EY.com.eBook.API.Contracts.Data";(//d:RepositoryMetaItemDataContract[d:Id="TemplateType"]/d:Value)[1]','varchar(max)') as templateId,
			CASE WHEN (SELECT TOP 1 [ClientDepartmentEndDate] FROM [PMT].[dbo].[TPMT_CLIENT_DEPARTMENTS] WHERE clientDepartmentClientID = (select top 1 clientId from [PMT].[dbo].[TPMT_CLIENTS] WHERE ClientGfisCode  = cl.client_GFIS_code COLLATE DATABASE_DEFAULT) AND clientdepartmentDepartmentId = '4DEC8114-DA26-47FA-AF47-5FFA0463FC88') is not NULL THEN 'Ex-client, ' + s.REPOSITORY_STATUSTYPE_ITEM_DESCRIPTION ELSE s.REPOSITORY_STATUSTYPE_ITEM_DESCRIPTION END as statusId,
			rep.REPOSITORY_ITEM_NAME as itemName,
			rep.REPOSITORY_ITEM_META.value('declare namespace d="http://schemas.datacontract.org/2004/07/EY.com.eBook.API.Contracts.Data";(//d:RepositoryMetaItemDataContract[d:Id="GTHValidated"]/d:Value)[1]','varchar(5)') as validatedByGTH,
			offi.office_name as officeName

	
		from TPBOOK_CLIENTS cl
			inner join TPBOOK_REPOSITORY rep
				on cl.CLIENT_ID = rep.REPOSITORY_ITEM_CLIENT_ID
					and rep.REPOSITORY_ITEM_REPOSITORY_STRUCTURE_ID='32F0391E-E0BD-41F9-A9C1-C04EAC3C8CB1'
					and (rep.REPOSITORY_ITEM_META.value('declare namespace d="http://schemas.datacontract.org/2004/07/EY.com.eBook.API.Contracts.Data";(//d:RepositoryMetaItemDataContract[d:Id="GTHValidated"]/d:Value)[1]','varchar(5)') like @validatedByGTH  
					OR rep.REPOSITORY_ITEM_META.value('declare namespace d="http://schemas.datacontract.org/2004/07/EY.com.eBook.API.Contracts.Data";(//d:RepositoryMetaItemDataContract[d:Id="TemplateType"]/d:Value)[1]','varchar(max)') like '%bab19d44-ba9a-4fdf-9a0a-e969d92ff0ec%'
					OR rep.REPOSITORY_ITEM_META.value('declare namespace d="http://schemas.datacontract.org/2004/07/EY.com.eBook.API.Contracts.Data";(//d:RepositoryMetaItemDataContract[d:Id="TemplateType"]/d:Value)[1]','varchar(max)') like '%69e87bb4-6377-4605-9a89-73494e42cdc0%' --true/false/%
					OR rep.REPOSITORY_ITEM_META.value('declare namespace d="http://schemas.datacontract.org/2004/07/EY.com.eBook.API.Contracts.Data";(//d:RepositoryMetaItemDataContract[d:Id="TemplateType"]/d:Value)[1]','varchar(max)') like '%28b439db-6a1d-42a8-8205-1c319d5aa8d2%')
					and rep.REPOSITORY_ITEM_STARTDATE BETWEEN @startdateFrom and @startdateTo
					and rep.REPOSITORY_ITEM_ENDDATE BETWEEN @enddateFrom and @enddateTo
					and rep.REPOSITORY_ITEM_META.value('declare namespace d="http://schemas.datacontract.org/2004/07/EY.com.eBook.API.Contracts.Data";(//d:RepositoryMetaItemDataContract[d:Id="TemplateType"]/d:Value)[1]','varchar(max)') like '%' + @templateId + '%'
					and cast([REPOSITORY_ITEM_STATUS] as varchar(max)) like @statusId
					and DATEDIFF(DAY, getdate(), rep.REPOSITORY_ITEM_ENDDATE) < CASE WHEN @expiration = 0 THEN 1000000000 ELSE @expiration 	END 
					and DATEDIFF(DAY, getdate(), rep.REPOSITORY_ITEM_ENDDATE) >= CASE WHEN @expiration = 0 THEN -1000000000	ELSE (0-@expiration) END 

			inner join TPBOOK_CLIENT_DEPARTMENTS ecd 
				on ecd.CLIENTDEPARTMENT_CLIENTID = cl.CLIENT_ID
			left outer join vwPcrGfisManager manager
				on manager.pcr_client_id = cl.client_id
					and isnull( manager.person_department,'ACR')  <> 'TAX'
			left outer join vwPcrGfisPartner partner1
				on partner1.pcr_client_id = cl.client_id
					and isnull(partner1.person_department,'ACR') <> 'TAX'
			left outer join eBook..TPBOOK_OFFICE offi
				on offi.OFFICE_ID = (select top 1 CLIENTOFFICE_OFFICE_ID from TPBOOK_CLIENT_OFFICES_NEW where clientoffice_client_id = cl.client_id and CLIENTOFFICE_DEPARTMENT_ID = 'ACR' and clientoffice_office_id <> '00176')
			inner join tpbook_repository_status_items s
				on rep.REPOSITORY_ITEM_STATUS = s.REPOSITORY_STATUSTYPE_ITEM_ID
					and s.REPOSITORY_STATUSTYPE_ITEM_CULTURE = @culture

		where 
			cl.CLIENT_NAME like @clientName
				and cl.CLIENT_ACTIVE_FLAG =  'Y'
				and (@personId is null or cl.CLIENT_ID in (select PCR_CLIENT_ID from TPBOOK_PERSON_CLIENT_ROLE where PCR_PERSON_ID=@personId))
				and ecd.CLIENTDEPARTMENT_DEPARTMENTID='ACR'
				and rep.[REPOSITORY_ITEM_DELETED] = 0
				and (@activeClient is null or @activeClient = isDate((SELECT TOP 1 [ClientDepartmentEndDate] FROM [PMT].[dbo].[TPMT_CLIENT_DEPARTMENTS] WHERE clientDepartmentClientID = (select top 1 clientId from [PMT].[dbo].[TPMT_CLIENTS] WHERE ClientGfisCode  = cl.client_GFIS_code COLLATE DATABASE_DEFAULT) AND clientdepartmentDepartmentId = '4DEC8114-DA26-47FA-AF47-5FFA0463FC88')))

	union
		--Prime Sub in repository
			select 
			manager.person_firstname + ' ' + manager.person_lastname as pmanager,
			partner1.person_firstname + ' ' + partner1.person_lastname as ppartner,
			CAST(CAST(rep.REPOSITORY_ITEM_ID AS VARCHAR(150)) + CAST(cl.client_id AS VARCHAR(150)) AS VARCHAR(150))  as pcelstatId,
			rep.REPOSITORY_ITEM_ID as ItemId,
			cl.client_name as clientName,
			cl.client_gfis_code as clientGFIS,	
			cl.client_id as clientId,
			rep.REPOSITORY_ITEM_META.value('declare namespace d="http://schemas.datacontract.org/2004/07/EY.com.eBook.API.Contracts.Data";(//d:RepositoryMetaItemDataContract[d:Id="DateELEA"]/d:Value)[1]','varchar(12)') as DateELEA,
			rep.REPOSITORY_ITEM_STARTDATE as itemStartDate,
			rep.REPOSITORY_ITEM_ENDDATE as itemEndDate,
			DATEDIFF(DAY, getdate(), rep.REPOSITORY_ITEM_ENDDATE) as expiration,
			'' as renewed,
			--rep.REPOSITORY_ITEM_META.value('declare namespace d="http://schemas.datacontract.org/2004/07/EY.com.eBook.API.Contracts.Data";(//d:RepositoryMetaItemDataContract[d:Id="TemplateType"]/d:Value)[1]','varchar(max)') as templateId,
			--Check Ex client and status = prime sub
			'{"id":"25dd8508-069f-4571-b719-f5391b989789","nl":"Prime sub","fr":"Prime sub","en":"Prime sub","value":0}' as templateId,
			CASE WHEN (SELECT TOP 1 [ClientDepartmentEndDate] FROM [PMT].[dbo].[TPMT_CLIENT_DEPARTMENTS] WHERE clientDepartmentClientID = (select top 1 clientId from [PMT].[dbo].[TPMT_CLIENTS] WHERE ClientGfisCode  = cl.client_GFIS_code COLLATE DATABASE_DEFAULT) AND clientdepartmentDepartmentId = '4DEC8114-DA26-47FA-AF47-5FFA0463FC88') is not NULL THEN 'Ex-client, Available' ELSE 'Available' END as statusId,
			rep.REPOSITORY_ITEM_NAME as itemName,
			rep.REPOSITORY_ITEM_META.value('declare namespace d="http://schemas.datacontract.org/2004/07/EY.com.eBook.API.Contracts.Data";(//d:RepositoryMetaItemDataContract[d:Id="GTHValidated"]/d:Value)[1]','varchar(5)') as validatedByGTH,
			offi.office_name as officeName
		from TPBOOK_CLIENTS cl
			inner join TPBOOK_REPOSITORY rep
				on cl.CLIENT_ID = rep.REPOSITORY_ITEM_CLIENT_ID
					and rep.REPOSITORY_ITEM_REPOSITORY_STRUCTURE_ID ='2B985402-2CA7-4D1D-AAB4-0A00B948E4DF' 
					AND (@TemplateId like '%25dd8508-069f-4571-b719-f5391b989789%' OR @TemplateId = '%')
					and rep.REPOSITORY_ITEM_STARTDATE BETWEEN @startdateFrom and @startdateTo
					and rep.REPOSITORY_ITEM_ENDDATE BETWEEN @enddateFrom and @enddateTo
					and DATEDIFF(DAY, getdate(), rep.REPOSITORY_ITEM_ENDDATE) < CASE WHEN @expiration = 0 THEN 1000000000 ELSE @expiration 	END 
					and DATEDIFF(DAY, getdate(), rep.REPOSITORY_ITEM_ENDDATE) >= CASE WHEN @expiration = 0 THEN -1000000000	ELSE (0-@expiration) END 
			inner join TPBOOK_CLIENT_DEPARTMENTS ecd 
				on ecd.CLIENTDEPARTMENT_CLIENTID = cl.CLIENT_ID
			left outer join vwPcrGfisManager manager
				on manager.pcr_client_id = cl.client_id
					and isnull( manager.person_department,'ACR')  <> 'TAX'
			left outer join vwPcrGfisPartner partner1
				on partner1.pcr_client_id = cl.client_id
					and isnull(partner1.person_department,'ACR') <> 'TAX'
			left outer join eBook..TPBOOK_OFFICE offi
				on offi.OFFICE_ID = (select top 1 CLIENTOFFICE_OFFICE_ID from TPBOOK_CLIENT_OFFICES_NEW where clientoffice_client_id = cl.client_id and CLIENTOFFICE_DEPARTMENT_ID = 'ACR' and clientoffice_office_id <> '00176')
		where 
			cl.CLIENT_NAME like @clientName
				and cl.CLIENT_ACTIVE_FLAG =  'Y'
				and (@personId is null or cl.CLIENT_ID in (select PCR_CLIENT_ID from TPBOOK_PERSON_CLIENT_ROLE where PCR_PERSON_ID=@personId))
				and ecd.CLIENTDEPARTMENT_DEPARTMENTID='ACR'
				and rep.[REPOSITORY_ITEM_DELETED] = 0
				and @statusId = '%'
				and (@activeClient is null or @activeClient = isDate((SELECT TOP 1 [ClientDepartmentEndDate] FROM [PMT].[dbo].[TPMT_CLIENT_DEPARTMENTS] WHERE clientDepartmentClientID = (select top 1 clientId from [PMT].[dbo].[TPMT_CLIENTS] WHERE ClientGfisCode  = cl.client_GFIS_code COLLATE DATABASE_DEFAULT) AND clientdepartmentDepartmentId = '4DEC8114-DA26-47FA-AF47-5FFA0463FC88')))
	union
		-- Engagement letter & Prime sub NIET IN repository
		select 
			manager.person_firstname + ' ' + manager.person_lastname as pmanager,
			partner1.person_firstname + ' ' + partner1.person_lastname as ppartner,
			CAST(CAST(cl.client_id AS VARCHAR(150)) AS VARCHAR(150))  as pcelstatId,
			rep.REPOSITORY_ITEM_ID as ItemId,
			cl.client_name as clientName,
			cl.client_gfis_code as clientGFIS,	
			cl.client_id as clientId,
			rep.REPOSITORY_ITEM_META.value('declare namespace d="http://schemas.datacontract.org/2004/07/EY.com.eBook.API.Contracts.Data";(//d:RepositoryMetaItemDataContract[d:Id="DateELEA"]/d:Value)[1]','varchar(12)') as DateELEA,
			rep.REPOSITORY_ITEM_STARTDATE as itemStartDate,
			rep.REPOSITORY_ITEM_ENDDATE as itemEndDate,
			DATEDIFF(DAY, getdate(), rep.REPOSITORY_ITEM_ENDDATE) as expiration,
			'' as 'to be renewed',
			rep.REPOSITORY_ITEM_META.value('declare namespace d="http://schemas.datacontract.org/2004/07/EY.com.eBook.API.Contracts.Data";(//d:RepositoryMetaItemDataContract[d:Id="TemplateType"]/d:Value)[1]','varchar(max)') as templateId,
			CASE WHEN (SELECT TOP 1 [ClientDepartmentEndDate] FROM [PMT].[dbo].[TPMT_CLIENT_DEPARTMENTS] WHERE clientDepartmentClientID = (select top 1 clientId from [PMT].[dbo].[TPMT_CLIENTS] WHERE ClientGfisCode  = cl.client_GFIS_code COLLATE DATABASE_DEFAULT) AND clientdepartmentDepartmentId = '4DEC8114-DA26-47FA-AF47-5FFA0463FC88') is not NULL THEN 'Ex-client, no engagement agreement' ELSE 'No engagement agreement' END as statusId,
			rep.REPOSITORY_ITEM_NAME as itemName,
			rep.REPOSITORY_ITEM_META.value('declare namespace d="http://schemas.datacontract.org/2004/07/EY.com.eBook.API.Contracts.Data";(//d:RepositoryMetaItemDataContract[d:Id="GTHValidated"]/d:Value)[1]','varchar(5)') as validatedByGTH,
			offi.office_name as officeName

		from  TPBOOK_CLIENTS cl 
			inner join TPBOOK_CLIENT_DEPARTMENTS ecd 
				on ecd.CLIENTDEPARTMENT_CLIENTID = cl.CLIENT_ID
			left outer join TPBOOK_REPOSITORY rep
				on cl.CLIENT_ID = rep.REPOSITORY_ITEM_CLIENT_ID
					and rep.REPOSITORY_ITEM_REPOSITORY_STRUCTURE_ID='32F0391E-E0BD-41F9-A9C1-C04EAC3C8CB1'
			left outer join vwPcrGfisManager manager
				on manager.pcr_client_id = cl.client_id
					and isnull( manager.person_department,'ACR')  <> 'TAX'
			left outer join vwPcrGfisPartner partner1
				on partner1.pcr_client_id = cl.client_id
					and isnull(partner1.person_department,'ACR') <> 'TAX'
			left outer join eBook..TPBOOK_OFFICE offi
				on offi.OFFICE_ID = (select top 1 CLIENTOFFICE_OFFICE_ID from TPBOOK_CLIENT_OFFICES_NEW where clientoffice_client_id = cl.client_id and CLIENTOFFICE_DEPARTMENT_ID = 'ACR' and clientoffice_office_id <> '00176')
		where 
			 ecd.CLIENTDEPARTMENT_DEPARTMENTID='ACR'
				and cl.CLIENT_NAME like @clientName
				and cl.CLIENT_ACTIVE_FLAG =  'Y'
				and (@personId is null or cl.CLIENT_ID in (select PCR_CLIENT_ID from TPBOOK_PERSON_CLIENT_ROLE where PCR_PERSON_ID=@personId))
				and cl.CLIENT_ID not in (SELECT rep.REPOSITORY_ITEM_CLIENT_ID FROM TPBOOK_REPOSITORY rep where (rep.REPOSITORY_ITEM_REPOSITORY_STRUCTURE_ID='32F0391E-E0BD-41F9-A9C1-C04EAC3C8CB1' OR rep.REPOSITORY_ITEM_REPOSITORY_STRUCTURE_ID='2B985402-2CA7-4D1D-AAB4-0A00B948E4DF') and rep.[REPOSITORY_ITEM_DELETED] = 0)
				and @templateId = '%'
				and @startdateTo = '20500101'
				and @startdateFrom = '19000101'
				and @enddateTo = '20500101'
				and @enddateFrom = '19000101'
				and @statusid = '%' OR @statusid = '64AFAD19-8407-4F9E-AF72-ED37468C4272'
				and (@activeClient is null or @activeClient = isDate((SELECT TOP 1 [ClientDepartmentEndDate] FROM [PMT].[dbo].[TPMT_CLIENT_DEPARTMENTS] WHERE clientDepartmentClientID = (select top 1 clientId from [PMT].[dbo].[TPMT_CLIENTS] WHERE ClientGfisCode  = cl.client_GFIS_code COLLATE DATABASE_DEFAULT) AND clientdepartmentDepartmentId = '4DEC8114-DA26-47FA-AF47-5FFA0463FC88')))
	) tab
)

select @count = count(*) from engletters;

with englettersrecs as (
	select *,ROW_NUMBER() OVER (ORDER BY clientName) AS RN
	from (
		-- Engagement letter IN repository
		select 

			manager.person_firstname + ' ' + manager.person_lastname as pmanager,
			partner1.person_firstname + ' ' + partner1.person_lastname as ppartner,
			CAST(CAST(rep.REPOSITORY_ITEM_ID AS VARCHAR(150)) + CAST(cl.client_id AS VARCHAR(150)) AS VARCHAR(150))  as pcelstatId,
			rep.REPOSITORY_ITEM_ID as ItemId,
			cl.client_name as clientName,
			cl.client_gfis_code as clientGFIS,	
			cl.client_id as clientId,
			rep.REPOSITORY_ITEM_META.value('declare namespace d="http://schemas.datacontract.org/2004/07/EY.com.eBook.API.Contracts.Data";(//d:RepositoryMetaItemDataContract[d:Id="DateELEA"]/d:Value)[1]','varchar(12)') as DateELEA,
			rep.REPOSITORY_ITEM_STARTDATE as itemStartDate,
			rep.REPOSITORY_ITEM_ENDDATE as itemEndDate,
			DATEDIFF(DAY, getdate(), rep.REPOSITORY_ITEM_ENDDATE) as expiration,
			'' as renewed,
			rep.REPOSITORY_ITEM_META.value('declare namespace d="http://schemas.datacontract.org/2004/07/EY.com.eBook.API.Contracts.Data";(//d:RepositoryMetaItemDataContract[d:Id="TemplateType"]/d:Value)[1]','varchar(max)') as templateId,
			CASE WHEN (SELECT TOP 1 [ClientDepartmentEndDate] FROM [PMT].[dbo].[TPMT_CLIENT_DEPARTMENTS] WHERE clientDepartmentClientID = (select top 1 clientId from [PMT].[dbo].[TPMT_CLIENTS] WHERE ClientGfisCode  = cl.client_GFIS_code COLLATE DATABASE_DEFAULT) AND clientdepartmentDepartmentId = '4DEC8114-DA26-47FA-AF47-5FFA0463FC88') is not NULL THEN 'Ex-client, ' + s.REPOSITORY_STATUSTYPE_ITEM_DESCRIPTION ELSE s.REPOSITORY_STATUSTYPE_ITEM_DESCRIPTION END as statusId,
			rep.REPOSITORY_ITEM_NAME as itemName,
			rep.REPOSITORY_ITEM_META.value('declare namespace d="http://schemas.datacontract.org/2004/07/EY.com.eBook.API.Contracts.Data";(//d:RepositoryMetaItemDataContract[d:Id="GTHValidated"]/d:Value)[1]','varchar(5)') as validatedByGTH,
			offi.office_name as officeName

	
		from TPBOOK_CLIENTS cl
			inner join TPBOOK_REPOSITORY rep
				on cl.CLIENT_ID = rep.REPOSITORY_ITEM_CLIENT_ID
					and rep.REPOSITORY_ITEM_REPOSITORY_STRUCTURE_ID='32F0391E-E0BD-41F9-A9C1-C04EAC3C8CB1'
					and (rep.REPOSITORY_ITEM_META.value('declare namespace d="http://schemas.datacontract.org/2004/07/EY.com.eBook.API.Contracts.Data";(//d:RepositoryMetaItemDataContract[d:Id="GTHValidated"]/d:Value)[1]','varchar(5)') like @validatedByGTH  
					OR rep.REPOSITORY_ITEM_META.value('declare namespace d="http://schemas.datacontract.org/2004/07/EY.com.eBook.API.Contracts.Data";(//d:RepositoryMetaItemDataContract[d:Id="TemplateType"]/d:Value)[1]','varchar(max)') like '%bab19d44-ba9a-4fdf-9a0a-e969d92ff0ec%'
					OR rep.REPOSITORY_ITEM_META.value('declare namespace d="http://schemas.datacontract.org/2004/07/EY.com.eBook.API.Contracts.Data";(//d:RepositoryMetaItemDataContract[d:Id="TemplateType"]/d:Value)[1]','varchar(max)') like '%69e87bb4-6377-4605-9a89-73494e42cdc0%' --true/false/%
					OR rep.REPOSITORY_ITEM_META.value('declare namespace d="http://schemas.datacontract.org/2004/07/EY.com.eBook.API.Contracts.Data";(//d:RepositoryMetaItemDataContract[d:Id="TemplateType"]/d:Value)[1]','varchar(max)') like '%28b439db-6a1d-42a8-8205-1c319d5aa8d2%')
					and rep.REPOSITORY_ITEM_STARTDATE BETWEEN @startdateFrom and @startdateTo
					and rep.REPOSITORY_ITEM_ENDDATE BETWEEN @enddateFrom and @enddateTo
					and rep.REPOSITORY_ITEM_META.value('declare namespace d="http://schemas.datacontract.org/2004/07/EY.com.eBook.API.Contracts.Data";(//d:RepositoryMetaItemDataContract[d:Id="TemplateType"]/d:Value)[1]','varchar(max)') like  '%' + @templateId + '%'
					and cast([REPOSITORY_ITEM_STATUS] as varchar(max)) like @statusId
					and DATEDIFF(DAY, getdate(), rep.REPOSITORY_ITEM_ENDDATE) < CASE WHEN @expiration = 0 THEN 1000000000 ELSE @expiration 	END 
					and DATEDIFF(DAY, getdate(), rep.REPOSITORY_ITEM_ENDDATE) >= CASE WHEN @expiration = 0 THEN -1000000000	ELSE (0-@expiration) END 

			inner join TPBOOK_CLIENT_DEPARTMENTS ecd 
				on ecd.CLIENTDEPARTMENT_CLIENTID = cl.CLIENT_ID
			left outer join vwPcrGfisManager manager
				on manager.pcr_client_id = cl.client_id
					and isnull( manager.person_department,'ACR')  <> 'TAX'
			left outer join vwPcrGfisPartner partner1
				on partner1.pcr_client_id = cl.client_id
					and isnull(partner1.person_department,'ACR') <> 'TAX'
			left outer join eBook..TPBOOK_OFFICE offi
				on offi.OFFICE_ID = (select top 1 CLIENTOFFICE_OFFICE_ID from TPBOOK_CLIENT_OFFICES_NEW where clientoffice_client_id = cl.client_id and CLIENTOFFICE_DEPARTMENT_ID = 'ACR' and clientoffice_office_id <> '00176')
			inner join tpbook_repository_status_items s
				on rep.REPOSITORY_ITEM_STATUS = s.REPOSITORY_STATUSTYPE_ITEM_ID
					and s.REPOSITORY_STATUSTYPE_ITEM_CULTURE = @culture

		where 
			cl.CLIENT_NAME like @clientName
				and cl.CLIENT_ACTIVE_FLAG =  'Y'
				and (@personId is null or cl.CLIENT_ID in (select PCR_CLIENT_ID from TPBOOK_PERSON_CLIENT_ROLE where PCR_PERSON_ID=@personId))
				and ecd.CLIENTDEPARTMENT_DEPARTMENTID='ACR'
				and rep.[REPOSITORY_ITEM_DELETED] = 0
				and (@activeClient is null or @activeClient = isDate((SELECT TOP 1 [ClientDepartmentEndDate] FROM [PMT].[dbo].[TPMT_CLIENT_DEPARTMENTS] WHERE clientDepartmentClientID = (select top 1 clientId from [PMT].[dbo].[TPMT_CLIENTS] WHERE ClientGfisCode  = cl.client_GFIS_code COLLATE DATABASE_DEFAULT) AND clientdepartmentDepartmentId = '4DEC8114-DA26-47FA-AF47-5FFA0463FC88')))
	union
		--Prime Sub in repository
			select 
			manager.person_firstname + ' ' + manager.person_lastname as pmanager,
			partner1.person_firstname + ' ' + partner1.person_lastname as ppartner,
			CAST(CAST(rep.REPOSITORY_ITEM_ID AS VARCHAR(150)) + CAST(cl.client_id AS VARCHAR(150)) AS VARCHAR(150))  as pcelstatId,
			rep.REPOSITORY_ITEM_ID as ItemId,
			cl.client_name as clientName,
			cl.client_gfis_code as clientGFIS,	
			cl.client_id as clientId,
			rep.REPOSITORY_ITEM_META.value('declare namespace d="http://schemas.datacontract.org/2004/07/EY.com.eBook.API.Contracts.Data";(//d:RepositoryMetaItemDataContract[d:Id="DateELEA"]/d:Value)[1]','varchar(12)') as DateELEA,
			rep.REPOSITORY_ITEM_STARTDATE as itemStartDate,
			rep.REPOSITORY_ITEM_ENDDATE as itemEndDate,
			DATEDIFF(DAY, getdate(), rep.REPOSITORY_ITEM_ENDDATE) as expiration,
			'' as renewed,
			--rep.REPOSITORY_ITEM_META.value('declare namespace d="http://schemas.datacontract.org/2004/07/EY.com.eBook.API.Contracts.Data";(//d:RepositoryMetaItemDataContract[d:Id="TemplateType"]/d:Value)[1]','varchar(max)') as templateId,
			--Check Ex client and status = prime sub
			'{"id":"25dd8508-069f-4571-b719-f5391b989789","nl":"Prime sub","fr":"Prime sub","en":"Prime sub","value":0}' as templateId,
			CASE WHEN (SELECT TOP 1 [ClientDepartmentEndDate] FROM [PMT].[dbo].[TPMT_CLIENT_DEPARTMENTS] WHERE clientDepartmentClientID = (select top 1 clientId from [PMT].[dbo].[TPMT_CLIENTS] WHERE ClientGfisCode  = cl.client_GFIS_code COLLATE DATABASE_DEFAULT) AND clientdepartmentDepartmentId = '4DEC8114-DA26-47FA-AF47-5FFA0463FC88') is not NULL THEN 'Ex-client, Available' ELSE 'Available' END as statusId,
			rep.REPOSITORY_ITEM_NAME as itemName,
			rep.REPOSITORY_ITEM_META.value('declare namespace d="http://schemas.datacontract.org/2004/07/EY.com.eBook.API.Contracts.Data";(//d:RepositoryMetaItemDataContract[d:Id="GTHValidated"]/d:Value)[1]','varchar(5)') as validatedByGTH,
			offi.office_name as officeName
		from TPBOOK_CLIENTS cl
			inner join TPBOOK_REPOSITORY rep
				on cl.CLIENT_ID = rep.REPOSITORY_ITEM_CLIENT_ID
					and rep.REPOSITORY_ITEM_REPOSITORY_STRUCTURE_ID ='2B985402-2CA7-4D1D-AAB4-0A00B948E4DF' 
					AND (@TemplateId like '%25dd8508-069f-4571-b719-f5391b989789%' OR @TemplateId = '%')
					and rep.REPOSITORY_ITEM_STARTDATE BETWEEN @startdateFrom and @startdateTo
					and rep.REPOSITORY_ITEM_ENDDATE BETWEEN @enddateFrom and @enddateTo
					and DATEDIFF(DAY, getdate(), rep.REPOSITORY_ITEM_ENDDATE) < CASE WHEN @expiration = 0 THEN 1000000000 ELSE @expiration 	END 
					and DATEDIFF(DAY, getdate(), rep.REPOSITORY_ITEM_ENDDATE) >= CASE WHEN @expiration = 0 THEN -1000000000	ELSE (0-@expiration) END 
			inner join TPBOOK_CLIENT_DEPARTMENTS ecd 
				on ecd.CLIENTDEPARTMENT_CLIENTID = cl.CLIENT_ID
			left outer join vwPcrGfisManager manager
				on manager.pcr_client_id = cl.client_id
					and isnull( manager.person_department,'ACR')  <> 'TAX'
			left outer join vwPcrGfisPartner partner1
				on partner1.pcr_client_id = cl.client_id
					and isnull(partner1.person_department,'ACR') <> 'TAX'
			left outer join eBook..TPBOOK_OFFICE offi
				on offi.OFFICE_ID = (select top 1 CLIENTOFFICE_OFFICE_ID from TPBOOK_CLIENT_OFFICES_NEW where clientoffice_client_id = cl.client_id and CLIENTOFFICE_DEPARTMENT_ID = 'ACR' and clientoffice_office_id <> '00176')
		where 
			cl.CLIENT_NAME like @clientName
				and cl.CLIENT_ACTIVE_FLAG =  'Y'
				and (@personId is null or cl.CLIENT_ID in (select PCR_CLIENT_ID from TPBOOK_PERSON_CLIENT_ROLE where PCR_PERSON_ID=@personId))
				and ecd.CLIENTDEPARTMENT_DEPARTMENTID='ACR'
				and rep.[REPOSITORY_ITEM_DELETED] = 0
				and @statusId = '%'
				and (@activeClient is null or @activeClient = isDate((SELECT TOP 1 [ClientDepartmentEndDate] FROM [PMT].[dbo].[TPMT_CLIENT_DEPARTMENTS] WHERE clientDepartmentClientID = (select top 1 clientId from [PMT].[dbo].[TPMT_CLIENTS] WHERE ClientGfisCode  = cl.client_GFIS_code COLLATE DATABASE_DEFAULT) AND clientdepartmentDepartmentId = '4DEC8114-DA26-47FA-AF47-5FFA0463FC88')))
	union
		-- Engagement letter & Prime sub NIET IN repository (no ea)
		select 
			manager.person_firstname + ' ' + manager.person_lastname as pmanager,
			partner1.person_firstname + ' ' + partner1.person_lastname as ppartner,
			CAST(CAST(cl.client_id AS VARCHAR(150)) AS VARCHAR(150))  as pcelstatId,
			rep.REPOSITORY_ITEM_ID as ItemId,
			cl.client_name as clientName,
			cl.client_gfis_code as clientGFIS,	
			cl.client_id as clientId,
			rep.REPOSITORY_ITEM_META.value('declare namespace d="http://schemas.datacontract.org/2004/07/EY.com.eBook.API.Contracts.Data";(//d:RepositoryMetaItemDataContract[d:Id="DateELEA"]/d:Value)[1]','varchar(12)') as DateELEA,
			rep.REPOSITORY_ITEM_STARTDATE as itemStartDate,
			rep.REPOSITORY_ITEM_ENDDATE as itemEndDate,
			DATEDIFF(DAY, getdate(), rep.REPOSITORY_ITEM_ENDDATE) as expiration,
			'' as 'to be renewed',
			rep.REPOSITORY_ITEM_META.value('declare namespace d="http://schemas.datacontract.org/2004/07/EY.com.eBook.API.Contracts.Data";(//d:RepositoryMetaItemDataContract[d:Id="TemplateType"]/d:Value)[1]','varchar(max)') as templateId,
			CASE WHEN (SELECT TOP 1 [ClientDepartmentEndDate] FROM [PMT].[dbo].[TPMT_CLIENT_DEPARTMENTS] WHERE clientDepartmentClientID = (select top 1 clientId from [PMT].[dbo].[TPMT_CLIENTS] WHERE ClientGfisCode  = cl.client_GFIS_code COLLATE DATABASE_DEFAULT) AND clientdepartmentDepartmentId = '4DEC8114-DA26-47FA-AF47-5FFA0463FC88') is not NULL THEN 'Ex-client, no engagement agreement' ELSE 'No engagement agreement' END as statusId,
			rep.REPOSITORY_ITEM_NAME as itemName,
			rep.REPOSITORY_ITEM_META.value('declare namespace d="http://schemas.datacontract.org/2004/07/EY.com.eBook.API.Contracts.Data";(//d:RepositoryMetaItemDataContract[d:Id="GTHValidated"]/d:Value)[1]','varchar(5)') as validatedByGTH,
			offi.office_name as officeName

		from  TPBOOK_CLIENTS cl 
			inner join TPBOOK_CLIENT_DEPARTMENTS ecd 
				on ecd.CLIENTDEPARTMENT_CLIENTID = cl.CLIENT_ID
			left outer join TPBOOK_REPOSITORY rep
				on cl.CLIENT_ID = rep.REPOSITORY_ITEM_CLIENT_ID
					and rep.REPOSITORY_ITEM_REPOSITORY_STRUCTURE_ID='32F0391E-E0BD-41F9-A9C1-C04EAC3C8CB1'
			left outer join vwPcrGfisManager manager
				on manager.pcr_client_id = cl.client_id
					and isnull( manager.person_department,'ACR')  <> 'TAX'
			left outer join vwPcrGfisPartner partner1
				on partner1.pcr_client_id = cl.client_id
					and isnull(partner1.person_department,'ACR') <> 'TAX'
			left outer join eBook..TPBOOK_OFFICE offi
				on offi.OFFICE_ID = (select top 1 CLIENTOFFICE_OFFICE_ID from TPBOOK_CLIENT_OFFICES_NEW where clientoffice_client_id = cl.client_id and CLIENTOFFICE_DEPARTMENT_ID = 'ACR' and clientoffice_office_id <> '00176')
		where 
			 ecd.CLIENTDEPARTMENT_DEPARTMENTID='ACR'
				and cl.CLIENT_NAME like @clientName
				and cl.CLIENT_ACTIVE_FLAG =  'Y'
				and (@personId is null or cl.CLIENT_ID in (select PCR_CLIENT_ID from TPBOOK_PERSON_CLIENT_ROLE where PCR_PERSON_ID=@personId))
				and cl.CLIENT_ID not in (SELECT rep.REPOSITORY_ITEM_CLIENT_ID FROM TPBOOK_REPOSITORY rep where (rep.REPOSITORY_ITEM_REPOSITORY_STRUCTURE_ID='32F0391E-E0BD-41F9-A9C1-C04EAC3C8CB1' OR rep.REPOSITORY_ITEM_REPOSITORY_STRUCTURE_ID='2B985402-2CA7-4D1D-AAB4-0A00B948E4DF') and rep.[REPOSITORY_ITEM_DELETED] = 0)
				and @templateId = '%'
				and @startdateTo = '20500101'
				and @startdateFrom = '19000101'
				and @enddateTo = '20500101'
				and @enddateFrom = '19000101'
				and @statusid = '%' OR @statusid = '64AFAD19-8407-4F9E-AF72-ED37468C4272'
				and (@activeClient is null or @activeClient = isDate((SELECT TOP 1 [ClientDepartmentEndDate] FROM [PMT].[dbo].[TPMT_CLIENT_DEPARTMENTS] WHERE clientDepartmentClientID = (select top 1 clientId from [PMT].[dbo].[TPMT_CLIENTS] WHERE ClientGfisCode  = cl.client_GFIS_code COLLATE DATABASE_DEFAULT) AND clientdepartmentDepartmentId = '4DEC8114-DA26-47FA-AF47-5FFA0463FC88')))
	) tab
)

select * from englettersrecs
WHERE ( @rowsFrom is null or (RN >= @rowsFrom AND RN <= @rowsTo));




