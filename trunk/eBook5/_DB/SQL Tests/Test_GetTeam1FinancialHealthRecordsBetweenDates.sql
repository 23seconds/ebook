--Dit script werd in 2015 gebruikt om GTH financial health Team 4 op te vullen met TODO records.

/****** Script for SelectTopNRows command from SSMS  ******/
SELECT [GTH_TEAM_STATUS_ID]
      ,[GTH_TEAM_STATUS_CLIENT_ID]
      ,[GTH_TEAM_STATUS_PERIOD_START]
      ,[GTH_TEAM_STATUS_PERIOD_END]
      ,[GTH_TEAM_STATUS_TEAM_ID]
      ,[GTH_TEAM_STATUS_STATUS]
      ,[GTH_TEAM_STATUS_SUBSTATUS]
      ,[GTH_TEAM_STATUS_LASTUPDATE]
      ,[GTH_TEAM_STATUS_PERSON_ID]
      ,[GTH_TEAM_STATUS_DUEDATE]
      ,[GTH_TEAM_STATUS_PICKEDUP]
  FROM [eBook].[dbo].[GTH_TEAM_STATUS]
  where [GTH_TEAM_STATUS_TEAM_ID] = 1







--INSERT INTO [eBook].[dbo].[GTH_TEAM_STATUS]
SELECT  
NEWID() as 'id',
Client_id as 'clientid',
null as 'period_start',
null as 'period_end',
1 as 'team_id',
'TODO' as 'status',
NULL as 'substatus',
getDate() 'lastupdate',
'C4FA0389-E998-43D1-89F1-E66ED992C3C0' as 'personId',
NULL as 'duedate',
0 as 'pickedup'
FROM [PMT].[dbo].[TPMT_CLIENTS] pmtcli
INNER JOIN [eBook].[dbo].[TPBOOK_CLIENTS] ebocli ON ebocli.CLIENT_PMT_ID = pmtcli.clientid
INNER JOIN [PMT].[dbo].[TPMT_CLIENT_DEPARTMENTS] pmtclidep ON pmtcli.clientid = pmtclidep.clientdepartmentclientid AND pmtclidep.[ClientDepartmentEndDate] is null
INNER JOIN [PMT].[dbo].[TPMT_DEPARTMENTSERVICES] pmtdepsrv ON pmtdepsrv.DepartmentService_ClientDepartmentId = pmtclidep.clientdepartmentid AND pmtdepsrv.departmentservice_name = 'Stat. Accounts Publ.' AND (pmtdepsrv.DepartmentService_ToDate > getdate() OR pmtdepsrv.DepartmentService_ToDate is null)
INNER JOIN [PMT].[dbo].[TPMT_TASKS] tsk ON tsk.taskServiceId = pmtdepsrv.DepartmentService_id AND DATEDIFF(year,tsk.taskDelayedDate,getdate()) = 0 --validated/open?
--WHERE 
--pmtcli.ClientActive = 1 AND pmtcli.ClientName = 'DESAN NV'
/*
AND 
( 
	(
		pmtcli.ClientFinancialYearSchedule.exist('/schedule/formula/dateRecurrences/dateRecurrence[@type = "3" and @offset = "12"]') = 1 --31/12
		AND pmtcli.ClientFinancialYearSchedule.exist('/schedule/formula/dateRecurrences/dateRecurrence[@type = "0" and @offset = "31"]') = 1
	)
	OR 
	pmtcli.ClientFinancialYearSchedule.exist('/schedule/formula/dateRecurrences/dateRecurrence[@type = "3" and (@offset = "1" or @offset = "2" or @offset = "3")]') = 1 --jan/feb/mar
)*/


/*CONVERT(datetime,CASE WHEN pmtcli.ClientFinancialYearSchedule.exist('/schedule/formula/dateRecurrences/dateRecurrence[@type = "3" and @offset = "12"]') = 1
THEN '2014/' ELSE '2015/0' END + pmtcli.ClientFinancialYearSchedule.value('(/schedule/formula/dateRecurrences/dateRecurrence[@type = "3"]/@offset)[1]','varchar(2)')
+ '/' + pmtcli.ClientFinancialYearSchedule.value('(/schedule/formula/dateRecurrences/dateRecurrence[@type = "0" or @type = "1"]/@offset)[1]','varchar(2)'),111) as 'period_start',
CONVERT(datetime,CASE WHEN pmtcli.ClientFinancialYearSchedule.exist('/schedule/formula/dateRecurrences/dateRecurrence[@type = "3" and @offset = "12"]') = 1
THEN '2015/' ELSE '2016/0' END + pmtcli.ClientFinancialYearSchedule.value('(/schedule/formula/dateRecurrences/dateRecurrence[@type = "3"]/@offset)[1]','varchar(2)')
+ '/' + Convert(varchar,pmtcli.ClientFinancialYearSchedule.value('(/schedule/formula/dateRecurrences/dateRecurrence[@type = "0" or @type = "1"]/@offset)[1]','int') - 1),111) as 'period_end',*/
