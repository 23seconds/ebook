
	declare 
	@rowsTo int,
	@personId varchar(MAX),
	@statusId varchar(3),
	@enddate datetime,
	@dateModified datetime,
	@clientName varchar(100),
	@officeName varchar(50),
	@rowsFrom int,
	@limit int,
	@count int,-- OUTPUT;
	@cnt int,
	@service varchar(50),
	@department varchar(3),
	@newestFile bit;

	set @clientName = '%';
	set @personId = '6bf61892-d9a0-495e-a07b-185dc7b03f54'--'6bf61892-d9a0-495e-a07b-185dc7b03f54';--'18BC4A5D-A798-4D20-B097-E2F6E1A66E3E';--'18BC4A5D-A798-4D20-B097-E2F6E1A66E3E';
	set @statusId = '%';
	set @enddate = null; --'20500101';
	set @dateModified = null;--'19000101';
	set @department = 'ACR';
	set @service = '1ABF0836-7D7B-48E7-9006-F03DB97AC28B'; --	'C3B8C8DB-3822-4263-9098-541FAE897D02'
	set @limit = 25;
	set @rowsFrom = 0;
	set @rowsTo = @rowsFrom + @limit;
	set @officeName = null;
	set @newestfile = 0;



if @rowsFrom is not null
BEGIN
		set @rowsTo = @rowsFrom + @limit;
END;


select @cnt=count(*) 
from TPBOOK_PERSON_ROLES
where personrole_person_id=@personid
	and PersonRole_Role like 'Admin%'


if @cnt > 0
BEGIN
	set @personid = null
	print 'null person'
END;

with fileServiceReport as (
	select *,ROW_NUMBER() OVER (ORDER BY clientName) AS RN
		from (
			--File procedure started (status != 1 and log records)
			SELECT
				FI.FILE_ID AS 'fileId',
				O.OFFICE_NAME AS 'officeName',
				CL.CLIENT_NAME AS 'clientName',
				CL.CLIENT_ID AS 'clientId',
				CL.CLIENT_GFIS_CODE AS 'clientGfis',
				SUBSTRING(
				(SELECT ',' + MG.PERSON_FIRSTNAME + ' ' + MG.PERSON_LASTNAME 
				FROM VWPCRGFISMANAGER MG
				WHERE MG.PCR_CLIENT_ID = CL.CLIENT_ID
				AND isnull(PERSON_DEPARTMENT,'ACR') = @department FOR XML PATH (''))
				,2,1000) AS 'pmanager', --Concatenate managers
				SUBSTRING(
				(SELECT ',' + PT.PERSON_FIRSTNAME + ' ' + PT.PERSON_LASTNAME 
				FROM VWPCRGFISPARTNER PT
				WHERE PT.PCR_CLIENT_ID = CL.CLIENT_ID
				AND isnull(PERSON_DEPARTMENT,'ACR') = @department FOR XML PATH (''))
				,2,1000) AS 'ppartner', --Concatenate partners
				FI.FILE_END_DATE AS 'endDate',
				PMTCL.[ClientAnualMeetingNext] AS 'nextAnnualMeeting',
				SS.ServiceStatuses_Status AS 'status',
				FSL.FILESERVICELOG_PERSONFIRSTNAME + ' ' + FSL.FILESERVICELOG_PERSONLASTNAME AS 'person',
				FSL.FILESERVICELOG_TIMESTAMP as 'dateModified',
				DATEDIFF(day,FSL.FILESERVICELOG_TIMESTAMP,getdate()) as 'daysOnStatus'

			FROM TPBOOK_CLIENTS AS CL --All clients
				INNER JOIN TPBOOK_FILES AS FI ON FI.FILE_CLIENT_ID= CL.CLIENT_ID AND FI.FILE_CLOSE_DATE is NULL AND FI.FILE_DELETED = 0 AND FILE_MARK_DELETE = 0 --Open files
				INNER JOIN [PMT].[dbo].[TPMT_CLIENTS] AS PMTCL ON [ClientId] = CL.CLIENT_PMT_ID
				INNER JOIN TPBOOK_FILE_SERVICES AS FS ON FS.FILESERVICE_FILEID = FI.FILE_ID --Files linked to year end service
				INNER JOIN TPBOOK_SERVICE_STATUSES AS SS ON FS.FILESERVICES_STATUS = SS.SERVICESTATUSES_STATUSID AND FS.FILESERVICES_SERVICEID = SS.SERVICESTATUSES_SERVICEID --Status name
				INNER JOIN  TPBOOK_CLIENT_DEPARTMENTS AS CD ON CD.CLIENTDEPARTMENT_CLIENTID = CL.CLIENT_ID --department
				LEFT OUTER JOIN eBook..TPBOOK_OFFICE O ON O.OFFICE_ID = (
																			SELECT TOP 1 CLIENTOFFICE_OFFICE_ID 
																			FROM TPBOOK_CLIENT_OFFICES_NEW 
																			WHERE CLIENTOFFICE_CLIENT_ID = CL.CLIENT_ID 
																			AND CLIENTOFFICE_DEPARTMENT_ID = @department 
																			AND CLIENTOFFICE_OFFICE_ID <> '00176'
																		) --office
				CROSS APPLY
				(
					SELECT TOP 1 FILESERVICELOG_PERSONFIRSTNAME,FILESERVICELOG_PERSONLASTNAME,FILESERVICELOG_TIMESTAMP
					FROM TPBOOK_FILE_SERVICES_LOG
					WHERE FILESERVICELOG_FILEID = FI.FILE_ID 
					AND FILESERVICELOG_SERVICEID = FS.FILESERVICES_SERVICEID
					AND FILESERVICELOG_STATUSID = FS.FILESERVICES_STATUS
					ORDER BY FILESERVICELOG_TIMESTAMP DESC
				)  AS FSL --Person and timestamp

			WHERE CL.CLIENT_NAME like '%' + @clientName + '%'
				AND CL.CLIENT_ACTIVE_FLAG =  'Y'
				AND FI.FILE_ID in 
				(SELECT FI2.FILE_ID 
				FROM 
					(SELECT FI4.FILE_CLIENT_ID,FI4.FILE_CREATION_DATE, row_number() 
					over (partition by FI4.FILE_CLIENT_ID order by FI4.FILE_CREATION_DATE DESC) as ordernmbr 
					FROM TPBOOK_FILES FI4 WHERE FI4.FILE_CLIENT_ID = CL.CLIENT_ID) FI3 
					INNER JOIN TPBOOK_FILES FI2 
					ON FI3.FILE_CLIENT_ID = FI2.FILE_CLIENT_ID
					AND FI3.FILE_CREATION_DATE = FI2.FILE_CREATION_DATE
					WHERE FI3.ordernmbr = 1 OR @newestFile = 0)  --GET MOST RECENT CREATED FILE ONLY
				AND (@personId is null or CL.CLIENT_ID in (select PCR_CLIENT_ID from TPBOOK_PERSON_CLIENT_ROLE where PCR_PERSON_ID = @personId))
				AND CAST(FS.FILESERVICES_STATUS AS VARCHAR(MAX)) like @statusId
				AND FS.FILESERVICES_STATUS != 1
				AND (DATEDIFF(DAY,FI.FILE_END_DATE,@enddate) = 0 OR @enddate is null)
				AND (DATEDIFF(DAY,FSL.FILESERVICELOG_TIMESTAMP,@dateModified) = 0 OR @dateModified is null)
				AND CD.CLIENTDEPARTMENT_DEPARTMENTID like @department
				AND CAST(FS.FILESERVICES_SERVICEID AS VARCHAR(50)) like @service
				AND (@officeName is null or O.OFFICE_NAME like '%' + @officeName + '%')
			Union
			--File service procedure NOT started (status 1 and not logged)
			SELECT
				FI.FILE_ID AS 'fileId',
				O.OFFICE_NAME AS 'officeName',
				CL.CLIENT_NAME AS 'clientName',
				CL.CLIENT_ID AS 'clientId',
				CL.CLIENT_GFIS_CODE AS 'clientGfis',
				SUBSTRING(
				(SELECT ',' + MG.PERSON_FIRSTNAME + ' ' + MG.PERSON_LASTNAME 
				FROM VWPCRGFISMANAGER MG
				WHERE MG.PCR_CLIENT_ID = CL.CLIENT_ID
				AND isnull(PERSON_DEPARTMENT,'ACR') = @department FOR XML PATH (''))
				,2,1000) AS 'pmanager', --Concatenate managers
				SUBSTRING(
				(SELECT ',' + PT.PERSON_FIRSTNAME + ' ' + PT.PERSON_LASTNAME 
				FROM VWPCRGFISPARTNER PT
				WHERE PT.PCR_CLIENT_ID = CL.CLIENT_ID
				AND isnull(PERSON_DEPARTMENT,'ACR') = @department FOR XML PATH (''))
				,2,1000) AS 'ppartner', --Concatenate partners
				FI.FILE_END_DATE AS 'endDate',
				PMTCL.[ClientAnualMeetingNext] AS 'nextAnnualMeeting',
				SS.ServiceStatuses_Status AS 'status',
				'' AS 'person',
				''as 'dateModified',
				'' as 'daysOnStatus'
			FROM TPBOOK_CLIENTS AS CL --All clients
				INNER JOIN [PMT].[dbo].[TPMT_CLIENTS] AS PMTCL ON [ClientId] = CL.CLIENT_PMT_ID
				INNER JOIN TPBOOK_FILES AS FI ON FI.FILE_CLIENT_ID = CL.CLIENT_ID AND FI.FILE_CLOSE_DATE is NULL AND FI.FILE_DELETED = 0 AND FILE_MARK_DELETE = 0 --Open files
				INNER JOIN TPBOOK_FILE_SERVICES AS FS ON FS.FILESERVICE_FILEID = FI.FILE_ID --Files linked to year end service
				INNER JOIN TPBOOK_SERVICE_STATUSES AS SS ON FS.FILESERVICES_STATUS = SS.SERVICESTATUSES_STATUSID AND FS.FILESERVICES_SERVICEID = SS.SERVICESTATUSES_SERVICEID --Status name
				INNER JOIN  TPBOOK_CLIENT_DEPARTMENTS AS CD ON CD.CLIENTDEPARTMENT_CLIENTID = CL.CLIENT_ID --department
				LEFT OUTER JOIN eBook..TPBOOK_OFFICE O ON O.OFFICE_ID = (
																			SELECT TOP 1 CLIENTOFFICE_OFFICE_ID 
																			FROM TPBOOK_CLIENT_OFFICES_NEW 
																			WHERE CLIENTOFFICE_CLIENT_ID = CL.CLIENT_ID 
																			AND CLIENTOFFICE_DEPARTMENT_ID = @department 
																			AND CLIENTOFFICE_OFFICE_ID <> '00176'
																		) --office
			WHERE cl.CLIENT_NAME like '%' + @clientName + '%'
				AND cl.CLIENT_ACTIVE_FLAG =  'Y'
				AND FI.FILE_ID in 
				(SELECT FI2.FILE_ID 
				FROM 
					(SELECT FI4.FILE_CLIENT_ID,FI4.FILE_CREATION_DATE, row_number() 
					over (partition by FI4.FILE_CLIENT_ID order by FI4.FILE_CREATION_DATE DESC) as ordernmbr 
					FROM TPBOOK_FILES FI4 WHERE FI4.FILE_CLIENT_ID = CL.CLIENT_ID) FI3 
					INNER JOIN TPBOOK_FILES FI2 
					ON FI3.FILE_CLIENT_ID = FI2.FILE_CLIENT_ID
					AND FI3.FILE_CREATION_DATE = FI2.FILE_CREATION_DATE
					WHERE FI3.ordernmbr = 1 OR @newestFile = 0)  --GET MOST RECENT CREATED FILE ONLY
				AND (@personId is null or CL.CLIENT_ID in (select PCR_CLIENT_ID from TPBOOK_PERSON_CLIENT_ROLE where PCR_PERSON_ID = @personId))
				AND CAST(@statusId AS VARCHAR(MAX)) in ('1','%')
				AND FS.FILESERVICES_STATUS = 1
				AND (DATEDIFF(DAY,FI.FILE_END_DATE,@enddate) = 0 OR @enddate is null)
				AND @dateModified is null
				AND CD.CLIENTDEPARTMENT_DEPARTMENTID like @department
				AND CAST(FS.FILESERVICES_SERVICEID AS VARCHAR(50)) like @service
				AND (@officeName is null or O.OFFICE_NAME like '%' + @officeName + '%')
	) tab
)
		
select @count = count(*) from fileServiceReport;

with fileServiceReportRecords as (
	select *,ROW_NUMBER() OVER (ORDER BY clientName) AS RN
		from (
		SELECT
			FI.FILE_ID AS 'fileId',
			O.OFFICE_NAME AS 'officeName',
			CL.CLIENT_NAME AS 'clientName',
			CL.CLIENT_ID AS 'clientId',
			CL.CLIENT_GFIS_CODE AS 'clientGfis',
			SUBSTRING(
			(SELECT ',' + MG.PERSON_FIRSTNAME + ' ' + MG.PERSON_LASTNAME 
			FROM VWPCRGFISMANAGER MG
			WHERE MG.PCR_CLIENT_ID = CL.CLIENT_ID
			AND isnull(PERSON_DEPARTMENT,'ACR') = @department FOR XML PATH (''))
			,2,1000) AS 'pmanager', --Concatenate managers
			SUBSTRING(
			(SELECT ',' + PT.PERSON_FIRSTNAME + ' ' + PT.PERSON_LASTNAME 
			FROM VWPCRGFISPARTNER PT
			WHERE PT.PCR_CLIENT_ID = CL.CLIENT_ID
			AND isnull(PERSON_DEPARTMENT,'ACR') = @department FOR XML PATH (''))
			,2,1000) AS 'ppartner', --Concatenate partners
			FI.FILE_END_DATE AS 'endDate',
			PMTCL.[ClientAnualMeetingNext] AS 'nextAnnualMeeting',
			SS.ServiceStatuses_Status AS 'status',
			FSL.FILESERVICELOG_PERSONFIRSTNAME + ' ' + FSL.FILESERVICELOG_PERSONLASTNAME AS 'person',
			FSL.FILESERVICELOG_TIMESTAMP as 'dateModified',
			DATEDIFF(day,FSL.FILESERVICELOG_TIMESTAMP,getdate()) as 'daysOnStatus'

		FROM TPBOOK_CLIENTS AS CL --All clients
			INNER JOIN TPBOOK_FILES AS FI ON FI.FILE_CLIENT_ID= CL.CLIENT_ID AND FI.FILE_CLOSE_DATE is NULL AND FI.FILE_DELETED = 0  AND FILE_MARK_DELETE = 0 --Open files
			INNER JOIN [PMT].[dbo].[TPMT_CLIENTS] AS PMTCL ON [ClientId] = CL.CLIENT_PMT_ID
			INNER JOIN TPBOOK_FILE_SERVICES AS FS ON FS.FILESERVICE_FILEID = FI.FILE_ID --Files linked to year end service
			INNER JOIN TPBOOK_SERVICE_STATUSES AS SS ON FS.FILESERVICES_STATUS = SS.SERVICESTATUSES_STATUSID AND FS.FILESERVICES_SERVICEID = SS.SERVICESTATUSES_SERVICEID --Status name
			INNER JOIN  TPBOOK_CLIENT_DEPARTMENTS AS CD ON CD.CLIENTDEPARTMENT_CLIENTID = CL.CLIENT_ID --department
			LEFT OUTER JOIN eBook..TPBOOK_OFFICE O ON O.OFFICE_ID = (
																		SELECT TOP 1 CLIENTOFFICE_OFFICE_ID 
																		FROM TPBOOK_CLIENT_OFFICES_NEW 
																		WHERE CLIENTOFFICE_CLIENT_ID = CL.CLIENT_ID 
																		AND CLIENTOFFICE_DEPARTMENT_ID = @department 
																		AND CLIENTOFFICE_OFFICE_ID <> '00176'
																	) --office
			CROSS APPLY
			(
				SELECT TOP 1 FILESERVICELOG_PERSONFIRSTNAME,FILESERVICELOG_PERSONLASTNAME,FILESERVICELOG_TIMESTAMP
				FROM TPBOOK_FILE_SERVICES_LOG
				WHERE FILESERVICELOG_FILEID = FI.FILE_ID 
				AND FILESERVICELOG_SERVICEID = FS.FILESERVICES_SERVICEID
				AND FILESERVICELOG_STATUSID = FS.FILESERVICES_STATUS
				ORDER BY FILESERVICELOG_TIMESTAMP DESC
			)  AS FSL --Person and timestamp

		WHERE cl.CLIENT_NAME like '%' + @clientName + '%'
			AND cl.CLIENT_ACTIVE_FLAG =  'Y'
			AND FI.FILE_ID in 
				(SELECT FI2.FILE_ID 
				FROM 
					(SELECT FI4.FILE_CLIENT_ID,FI4.FILE_CREATION_DATE, row_number() 
					over (partition by FI4.FILE_CLIENT_ID order by FI4.FILE_CREATION_DATE DESC) as ordernmbr 
					FROM TPBOOK_FILES FI4 WHERE FI4.FILE_CLIENT_ID = CL.CLIENT_ID) FI3 
					INNER JOIN TPBOOK_FILES FI2 
					ON FI3.FILE_CLIENT_ID = FI2.FILE_CLIENT_ID
					AND FI3.FILE_CREATION_DATE = FI2.FILE_CREATION_DATE
					WHERE FI3.ordernmbr = 1 OR @newestFile = 0)  --GET MOST RECENT CREATED FILE ONLY
			AND (@personId is null or CL.CLIENT_ID in (select PCR_CLIENT_ID from TPBOOK_PERSON_CLIENT_ROLE where PCR_PERSON_ID = @personId))
			AND CAST(FS.FILESERVICES_STATUS AS VARCHAR(MAX)) like @statusId
			AND FS.FILESERVICES_STATUS != 1
			AND (DATEDIFF(DAY,FI.FILE_END_DATE,@enddate) = 0 OR @enddate is null)
			AND (DATEDIFF(DAY,FSL.FILESERVICELOG_TIMESTAMP,@dateModified) = 0 OR @dateModified is null)
			AND CD.CLIENTDEPARTMENT_DEPARTMENTID like @department
			AND CAST(FS.FILESERVICES_SERVICEID AS VARCHAR(50)) like @service
			AND (@officeName is null or O.OFFICE_NAME like '%' + @officeName + '%')
			
		Union
		--File service procedure NOT started (status 1 and not logged)
		SELECT
			FI.FILE_ID AS 'fileId',
			O.OFFICE_NAME AS 'officeName',
			CL.CLIENT_NAME AS 'clientName',
			CL.CLIENT_ID AS 'clientId',
			CL.CLIENT_GFIS_CODE AS 'clientGfis',
			SUBSTRING(
			(SELECT ',' + MG.PERSON_FIRSTNAME + ' ' + MG.PERSON_LASTNAME 
			FROM VWPCRGFISMANAGER MG
			WHERE MG.PCR_CLIENT_ID = CL.CLIENT_ID
			AND isnull(PERSON_DEPARTMENT,'ACR') = @department FOR XML PATH (''))
			,2,1000) AS 'pmanager', --Concatenate managers
			SUBSTRING(
			(SELECT ',' + PT.PERSON_FIRSTNAME + ' ' + PT.PERSON_LASTNAME 
			FROM VWPCRGFISPARTNER PT
			WHERE PT.PCR_CLIENT_ID = CL.CLIENT_ID
			AND isnull(PERSON_DEPARTMENT,'ACR') = @department FOR XML PATH (''))
			,2,1000) AS 'ppartner', --Concatenate partners
			--MG.PERSON_FIRSTNAME + ' ' + MG.PERSON_LASTNAME AS 'pmanager',
			--PT.PERSON_FIRSTNAME + ' ' + PT.PERSON_LASTNAME AS 'ppartner',
			FI.FILE_END_DATE AS 'endDate',
			PMTCL.[ClientAnualMeetingNext] AS 'nextAnnualMeeting',
			SS.ServiceStatuses_Status AS 'status',
			'' AS 'person',
			null as 'dateModified',
			null as 'daysOnStatus'
		FROM TPBOOK_CLIENTS AS CL --All clients
			INNER JOIN [PMT].[dbo].[TPMT_CLIENTS] AS PMTCL ON [ClientId] = CL.CLIENT_PMT_ID
			INNER JOIN TPBOOK_FILES AS FI ON FI.FILE_CLIENT_ID = CL.CLIENT_ID AND FI.FILE_CLOSE_DATE is NULL AND FI.FILE_DELETED = 0 AND FILE_MARK_DELETE = 0 --Open files
			INNER JOIN TPBOOK_FILE_SERVICES AS FS ON FS.FILESERVICE_FILEID = FI.FILE_ID --Files linked to year end service
			INNER JOIN TPBOOK_SERVICE_STATUSES AS SS ON FS.FILESERVICES_STATUS = SS.SERVICESTATUSES_STATUSID AND FS.FILESERVICES_SERVICEID = SS.SERVICESTATUSES_SERVICEID --Status name
			INNER JOIN  TPBOOK_CLIENT_DEPARTMENTS AS CD ON CD.CLIENTDEPARTMENT_CLIENTID = CL.CLIENT_ID --department
			LEFT OUTER JOIN eBook..TPBOOK_OFFICE O ON O.OFFICE_ID = (
																		SELECT TOP 1 CLIENTOFFICE_OFFICE_ID 
																		FROM TPBOOK_CLIENT_OFFICES_NEW 
																		WHERE CLIENTOFFICE_CLIENT_ID = CL.CLIENT_ID 
																		AND CLIENTOFFICE_DEPARTMENT_ID = @department 
																		AND CLIENTOFFICE_OFFICE_ID <> '00176'
																	) --office
		WHERE cl.CLIENT_NAME like '%' + @clientName + '%'
			AND cl.CLIENT_ACTIVE_FLAG =  'Y'
			AND FI.FILE_ID in 
				(SELECT FI2.FILE_ID 
				FROM 
					(SELECT FI4.FILE_CLIENT_ID,FI4.FILE_CREATION_DATE, row_number() 
					over (partition by FI4.FILE_CLIENT_ID order by FI4.FILE_CREATION_DATE DESC) as ordernmbr 
					FROM TPBOOK_FILES FI4 WHERE FI4.FILE_CLIENT_ID = CL.CLIENT_ID) FI3 
					INNER JOIN TPBOOK_FILES FI2 
					ON FI3.FILE_CLIENT_ID = FI2.FILE_CLIENT_ID
					AND FI3.FILE_CREATION_DATE = FI2.FILE_CREATION_DATE
					WHERE FI3.ordernmbr = 1 OR @newestFile = 0)  --GET MOST RECENT CREATED FILE ONLY
			AND (@personId is null or CL.CLIENT_ID in (select PCR_CLIENT_ID from TPBOOK_PERSON_CLIENT_ROLE where PCR_PERSON_ID = @personId))
			AND CAST(@statusId AS VARCHAR(MAX)) in ('1','%')
			AND FS.FILESERVICES_STATUS = 1
			AND (DATEDIFF(DAY,FI.FILE_END_DATE,@enddate) = 0 OR @enddate is null)
			AND @dateModified is null
			AND CD.CLIENTDEPARTMENT_DEPARTMENTID like @department
			AND CAST(FS.FILESERVICES_SERVICEID AS VARCHAR(50)) like @service
			AND (@officeName is null or O.OFFICE_NAME like '%' + @officeName + '%')
	) tab
)



select * from fileServiceReportRecords
WHERE ( @rowsFrom is null or (RN >= @rowsFrom AND RN <= @rowsTo));

