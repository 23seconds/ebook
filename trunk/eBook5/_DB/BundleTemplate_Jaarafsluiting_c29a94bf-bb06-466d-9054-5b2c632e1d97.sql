<ArrayOfIndexItemBaseDataContract xmlns:i="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://schemas.datacontract.org/2004/07/EY.com.eBook.API.Contracts.Data">
  <IndexItemBaseDataContract i:type="CoverpageDataContract">
    <EndsAt>0</EndsAt>
    <FooterConfig>
      <Enabled>false</Enabled>
      <FootNote i:nil="true" />
      <ShowFooterNote>false</ShowFooterNote>
      <ShowPageNr>false</ShowPageNr>
      <Style i:nil="true" />
    </FooterConfig>
    <HeaderConfig>
      <Enabled>false</Enabled>
      <ShowTitle>false</ShowTitle>
      <Style i:nil="true" />
    </HeaderConfig>
    <NoDraft>false</NoDraft>
    <StartsAt>0</StartsAt>
    <iTextTemplate i:nil="true" />
    <iconCls i:nil="true" />
    <id>00000000-0000-0000-0000-000000000000</id>
    <indexed>false</indexed>
    <locked>false</locked>
    <title>Titelpagina</title>
    <hidden>false</hidden>
    <type>COVERPAGE</type>
  </IndexItemBaseDataContract>
  <IndexItemBaseDataContract i:type="DocumentItemDataContract">
    <EndsAt>0</EndsAt>
    <FooterConfig>
      <Enabled>false</Enabled>
      <FootNote i:nil="true" />
      <ShowFooterNote>false</ShowFooterNote>
      <ShowPageNr>false</ShowPageNr>
      <Style i:nil="true" />
    </FooterConfig>
    <HeaderConfig>
      <Enabled>false</Enabled>
      <ShowTitle>false</ShowTitle>
      <Style i:nil="true" />
    </HeaderConfig>
    <NoDraft>false</NoDraft>
    <StartsAt>0</StartsAt>
    <iTextTemplate i:nil="true" />
    <iconCls i:nil="true" />
    <id>00000000-0000-0000-0000-000000000000</id>
    <indexed>true</indexed>
    <locked>false</locked>
    <title>Accounting memorandum</title>
    <Culture>nl-BE</Culture>
    <DocumentId>00000000-0000-0000-0000-000000000000</DocumentId>
    <DocumentTypeId>012C591F-A798-4BA2-A51F-6AB63F23C4A0</DocumentTypeId>
    <FileId>00000000-0000-0000-0000-000000000000</FileId>
    <type>DOCUMENT</type>
  </IndexItemBaseDataContract>
  <IndexItemBaseDataContract i:type="DocumentItemDataContract">
    <EndsAt>0</EndsAt>
    <FooterConfig>
      <Enabled>false</Enabled>
      <FootNote i:nil="true" />
      <ShowFooterNote>false</ShowFooterNote>
      <ShowPageNr>false</ShowPageNr>
      <Style i:nil="true" />
    </FooterConfig>
    <HeaderConfig>
      <Enabled>false</Enabled>
      <ShowTitle>false</ShowTitle>
      <Style i:nil="true" />
    </HeaderConfig>
    <NoDraft>false</NoDraft>
    <StartsAt>0</StartsAt>
    <iTextTemplate i:nil="true" />
    <iconCls i:nil="true" />
    <id>00000000-0000-0000-0000-000000000000</id>
    <indexed>true</indexed>
    <locked>false</locked>
    <title>Representation letter</title>
    <Culture>nl-BE</Culture>
    <DocumentId>00000000-0000-0000-0000-000000000000</DocumentId>
    <DocumentTypeId>2C6CDDD5-B8F0-41A3-B2B8-73112C334B26</DocumentTypeId>
    <FileId>00000000-0000-0000-0000-000000000000</FileId>
    <type>DOCUMENT</type>
  </IndexItemBaseDataContract>
  <IndexItemBaseDataContract i:type="ChapterDataContract">
    <EndsAt>0</EndsAt>
    <FooterConfig>
      <Enabled>false</Enabled>
      <FootNote i:nil="true" />
      <ShowFooterNote>false</ShowFooterNote>
      <ShowPageNr>false</ShowPageNr>
      <Style i:nil="true" />
    </FooterConfig>
    <HeaderConfig>
      <Enabled>false</Enabled>
      <ShowTitle>false</ShowTitle>
      <Style i:nil="true" />
    </HeaderConfig>
    <NoDraft>false</NoDraft>
    <StartsAt>0</StartsAt>
    <iTextTemplate i:nil="true" />
    <iconCls i:nil="true" />
    <id>00000000-0000-0000-0000-000000000000</id>
    <indexed>true</indexed>
    <locked>false</locked>
    <title>Jaarrekening</title>
    <items />
    <nocoverpage>false</nocoverpage>
    <signedrepo>true</signedrepo>
    <type>CHAPTER</type>
  </IndexItemBaseDataContract>
  <IndexItemBaseDataContract i:type="ChapterDataContract">
    <EndsAt>0</EndsAt>
    <FooterConfig>
      <Enabled>false</Enabled>
      <FootNote i:nil="true" />
      <ShowFooterNote>false</ShowFooterNote>
      <ShowPageNr>false</ShowPageNr>
      <Style i:nil="true" />
    </FooterConfig>
    <HeaderConfig>
      <Enabled>false</Enabled>
      <ShowTitle>false</ShowTitle>
      <Style i:nil="true" />
    </HeaderConfig>
    <NoDraft>false</NoDraft>
    <StartsAt>0</StartsAt>
    <iTextTemplate i:nil="true" />
    <iconCls i:nil="true" />
    <id>00000000-0000-0000-0000-000000000000</id>
    <indexed>true</indexed>
    <locked>false</locked>
    <title>Vergaderstukken</title>
    <items />
    <nocoverpage>false</nocoverpage>
    <signedrepo>true</signedrepo>
    <type>CHAPTER</type>
  </IndexItemBaseDataContract>
  <IndexItemBaseDataContract i:type="ChapterDataContract">
    <EndsAt>0</EndsAt>
    <FooterConfig>
      <Enabled>false</Enabled>
      <FootNote i:nil="true" />
      <ShowFooterNote>false</ShowFooterNote>
      <ShowPageNr>false</ShowPageNr>
      <Style i:nil="true" />
    </FooterConfig>
    <HeaderConfig>
      <Enabled>false</Enabled>
      <ShowTitle>false</ShowTitle>
      <Style i:nil="true" />
    </HeaderConfig>
    <NoDraft>false</NoDraft>
    <StartsAt>0</StartsAt>
    <iTextTemplate i:nil="true" />
    <iconCls i:nil="true" />
    <id>00000000-0000-0000-0000-000000000000</id>
    <indexed>true</indexed>
    <locked>false</locked>
    <title>Proef- en saldibalans</title>
    <items>
      <IndexItemBaseDataContract i:type="StatementsDataContract">
        <EndsAt>0</EndsAt>
        <FooterConfig>
          <Enabled>true</Enabled>
          <FootNote i:nil="true" />
          <ShowFooterNote>true</ShowFooterNote>
          <ShowPageNr>true</ShowPageNr>
          <Style i:nil="true" />
        </FooterConfig>
        <HeaderConfig>
          <Enabled>true</Enabled>
          <ShowTitle>true</ShowTitle>
          <Style i:nil="true" />
        </HeaderConfig>
        <NoDraft>false</NoDraft>
        <StartsAt>0</StartsAt>
        <iTextTemplate i:nil="true" />
        <iconCls i:nil="true" />
        <id>00000000-0000-0000-0000-000000000000</id>
        <indexed>true</indexed>
        <locked>false</locked>
        <title>Activa</title>
        <Comparable>false</Comparable>
        <Culture>nl-BE</Culture>
        <Detailed>true</Detailed>
        <FileId>00000000-0000-0000-0000-000000000000</FileId>
        <layout>ACTIVA</layout>
        <type>STATEMENT</type>
      </IndexItemBaseDataContract>
      <IndexItemBaseDataContract i:type="StatementsDataContract">
        <EndsAt>0</EndsAt>
        <FooterConfig>
          <Enabled>true</Enabled>
          <FootNote i:nil="true" />
          <ShowFooterNote>true</ShowFooterNote>
          <ShowPageNr>true</ShowPageNr>
          <Style i:nil="true" />
        </FooterConfig>
        <HeaderConfig>
          <Enabled>true</Enabled>
          <ShowTitle>true</ShowTitle>
          <Style i:nil="true" />
        </HeaderConfig>
        <NoDraft>false</NoDraft>
        <StartsAt>0</StartsAt>
        <iTextTemplate i:nil="true" />
        <iconCls i:nil="true" />
        <id>00000000-0000-0000-0000-000000000000</id>
        <indexed>true</indexed>
        <locked>false</locked>
        <title>Passiva</title>
        <Comparable>false</Comparable>
        <Culture>nl-BE</Culture>
        <Detailed>true</Detailed>
        <FileId>00000000-0000-0000-0000-000000000000</FileId>
        <layout>PASSIVA</layout>
        <type>STATEMENT</type>
      </IndexItemBaseDataContract>
      <IndexItemBaseDataContract i:type="StatementsDataContract">
        <EndsAt>0</EndsAt>
        <FooterConfig>
          <Enabled>true</Enabled>
          <FootNote i:nil="true" />
          <ShowFooterNote>true</ShowFooterNote>
          <ShowPageNr>true</ShowPageNr>
          <Style i:nil="true" />
        </FooterConfig>
        <HeaderConfig>
          <Enabled>true</Enabled>
          <ShowTitle>true</ShowTitle>
          <Style i:nil="true" />
        </HeaderConfig>
        <NoDraft>false</NoDraft>
        <StartsAt>0</StartsAt>
        <iTextTemplate i:nil="true" />
        <iconCls i:nil="true" />
        <id>00000000-0000-0000-0000-000000000000</id>
        <indexed>true</indexed>
        <locked>false</locked>
        <title>Resultatenrekening</title>
        <Comparable>false</Comparable>
        <Culture>nl-BE</Culture>
        <Detailed>true</Detailed>
        <FileId>00000000-0000-0000-0000-000000000000</FileId>
        <layout>RESULTATENREKENING</layout>
        <type>STATEMENT</type>
      </IndexItemBaseDataContract>
      <IndexItemBaseDataContract i:type="StatementsDataContract">
        <EndsAt>0</EndsAt>
        <FooterConfig>
          <Enabled>true</Enabled>
          <FootNote i:nil="true" />
          <ShowFooterNote>true</ShowFooterNote>
          <ShowPageNr>true</ShowPageNr>
          <Style i:nil="true" />
        </FooterConfig>
        <HeaderConfig>
          <Enabled>true</Enabled>
          <ShowTitle>true</ShowTitle>
          <Style i:nil="true" />
        </HeaderConfig>
        <NoDraft>false</NoDraft>
        <StartsAt>0</StartsAt>
        <iTextTemplate i:nil="true" />
        <iconCls i:nil="true" />
        <id>00000000-0000-0000-0000-000000000000</id>
        <indexed>true</indexed>
        <locked>false</locked>
        <title>Resultaatverwerking</title>
        <Comparable>false</Comparable>
        <Culture>nl-BE</Culture>
        <Detailed>false</Detailed>
        <FileId>00000000-0000-0000-0000-000000000000</FileId>
        <layout>RESULTAATVERWERKING</layout>
        <type>STATEMENT</type>
      </IndexItemBaseDataContract>
    </items>
    <nocoverpage>false</nocoverpage>
    <type>CHAPTER</type>
  </IndexItemBaseDataContract>
  <IndexItemBaseDataContract i:type="ChapterDataContract">
    <EndsAt>0</EndsAt>
    <FooterConfig>
      <Enabled>false</Enabled>
      <FootNote i:nil="true" />
      <ShowFooterNote>false</ShowFooterNote>
      <ShowPageNr>false</ShowPageNr>
      <Style i:nil="true" />
    </FooterConfig>
    <HeaderConfig>
      <Enabled>false</Enabled>
      <ShowTitle>false</ShowTitle>
      <Style i:nil="true" />
    </HeaderConfig>
    <NoDraft>false</NoDraft>
    <StartsAt>0</StartsAt>
    <iTextTemplate i:nil="true" />
    <iconCls i:nil="true" />
    <id>00000000-0000-0000-0000-000000000000</id>
    <indexed>true</indexed>
    <locked>false</locked>
    <title>Andere bijlagen</title>
    <items />
    <nocoverpage>false</nocoverpage>
    <type>CHAPTER</type>
  </IndexItemBaseDataContract>
</ArrayOfIndexItemBaseDataContract>