declare @cntKey int
declare @keyId uniqueidentifier

set @keyId='76CAF6DF-7529-4AB5-A22C-BD78ABA1AF72'
select @cntKey = COUNT(*) from [TPBOOK_COEFFICIENTKEYS] where CoeffKey_ID=@keyId

IF @cntKey=0
BEGIN
	print 'insert coeff key'
	insert into [TPBOOK_COEFFICIENTKEYS]
	values(@keyId,'VENB_FAIRNESSTAX_PERCENT','<CoefficientKeySettingsDataContract xmlns:i="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://schemas.datacontract.org/2004/07/EY.com.eBook.API.Contracts.Data">
	  <Period>AY</Period>
	</CoefficientKeySettingsDataContract>')

	print 'insert coeff key translation nl only'
	insert into TPBOOK_NAMES
	values(@keyId,'nl-BE','VenB: Fairness tax tarief')

	print 'insert coeff '
	insert into TPBOOK_COEFFICIENTS
	values(NEWID(),'C3D2E7D0-5479-4294-9E29-759757DBC737','VENB_FAIRNESSTAX_PERCENT',5.15,2014,null,null,'Tarief fairness tax')

END

