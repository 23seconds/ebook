

/****** Script for SelectTopNRows command from SSMS  ******/
SELECT *
  FROM [eBook].[dbo].[TPBOOK_PERSON_ROLES]


 ALTER TABLE [eBook].[dbo].[TPBOOK_PERSON_ROLES]
ADD PersonRole_LastUsed bit null

UPDATE  [eBook].[dbo].[TPBOOK_PERSON_ROLES]
SET personRole_lastUsed = 0

 ALTER TABLE [eBook].[dbo].[TPBOOK_PERSON_ROLES]
ALTER COLUMN PersonRole_LastUsed bit not null