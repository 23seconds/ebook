/****** Script for SelectTopNRows command from SSMS  ******/
SELECT TOP 1000 [ServiceStatuses_StatusId]
      ,[ServiceStatuses_Status]
      ,[ServiceStatuses_ServiceId]
      ,[ServiceStatuses_Rank]
      ,[ServiceStatuses_Activation]
      ,[ServiceStatuses_FollowUpStatuses]
      ,[ServiceStatuses_FollowUpActions]
  FROM [eBook].[dbo].[TPBOOK_SERVICE_STATUSES]


--remove partner from manager step
  UPDATE [eBook].[dbo].[TPBOOK_SERVICE_STATUSES]
    SET servicestatuses_followupactions = '[{type: "lock",followUpStatus: 2,activation:"Manager,Director"},{type: "approve",text: "Approve",followUpStatus: 3,activation:"Manager,Director"},{type: "reject",text: "Reject",followUpStatus: 4,activation:"Manager,Director"}]'
  WHERE servicestatuses_followupactions = '[{type: "lock",followUpStatus: 2,activation:"Manager,Director,Partner"},{type: "approve",text: "Approve",followUpStatus: 3,activation:"Manager,Director,Partner"},{type: "reject",text: "Reject",followUpStatus: 4,activation:"Manager,Director,Partner"}]'

--add office validator to partner step
UPDATE [eBook].[dbo].[TPBOOK_SERVICE_STATUSES]
SET servicestatuses_followupactions = '[{type: "approve",text: "Approve",followUpStatus: 5,activation:"Partner,OfficeValidator"},{type: "reject",text: "Reject",followUpStatus:6,activation:"Partner,OfficeValidator"}]'
WHERE servicestatuses_followupactions = '[{type: "approve",text: "Approve",followUpStatus: 5,activation:"Partner"},{type: "reject",text: "Reject",followUpStatus:6,activation:"Partner"}]'
