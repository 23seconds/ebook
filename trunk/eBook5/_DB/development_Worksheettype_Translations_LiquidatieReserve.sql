/****** Script for SelectTopNRows command from SSMS  ******/
SELECT [WORKSHEETTYPE_ID]
      ,[WORKSHEETTYPE_ENTITYORCOLLECTION]
      ,[WORKSHEETTYPE_FIELDNAME]
      ,[WORKSHEETTYPE_CULTURE]
      ,[WORKSHEETTYPE_OUTPUTTYPE]
      ,[WORKSHEETTYPE_TRANSLATION]
  FROM [eBook].[dbo].[TPBOOK_WORKSHEETTYPE_TRANSLATIONS]
  where worksheettype_id = '803D6A06-352E-4E28-ADE5-214DEAAC76CA'

INSERT INTO [eBook].[dbo].[TPBOOK_WORKSHEETTYPE_TRANSLATIONS]
VALUES('803D6A06-352E-4E28-ADE5-214DEAAC76CA','AanslagItem','LiquidatieReserve','nl-BE','print','Afzonderlijke aanslag van het gedeelte van de boekhoudkundige winst na belasting dat is overgeboekt naar de liquidatiereserve')

INSERT INTO [eBook].[dbo].[TPBOOK_WORKSHEETTYPE_TRANSLATIONS]
VALUES('803D6A06-352E-4E28-ADE5-214DEAAC76CA','AanslagItem','LiquidatieReserve','fr-FR','print','Cotisation distincte sur la partie du bénéfice comptable après impôt affectée à la réserve de liquidation')

INSERT INTO [eBook].[dbo].[TPBOOK_WORKSHEETTYPE_TRANSLATIONS]
VALUES('803D6A06-352E-4E28-ADE5-214DEAAC76CA','AanslagItem','LiquidatieReserve','en-US','print','Individual assessment of the part of the accounting profit after tax which is transferred to the liquidation reserve')

