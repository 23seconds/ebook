	

	
	--
	;with inactiveDepartments
	AS
	(
	Select pmtcli.ClientId, eboclidep.CLIENTDEPARTMENT_CLIENTID as 'EBOOKCLIENTID',eboclidep.CLIENTDEPARTMENT_DEPARTMENTID,max(ClientDepartmentEndDate) as 'MAXENDDATE'
	FROM eBook..TPBOOK_CLIENT_DEPARTMENTS eboclidep --eboclidep
	INNER JOIN eBook..TPBOOK_CLIENTS ebocli ON ebocli.CLIENT_ID = eboclidep.CLIENTDEPARTMENT_CLIENTID --ebocli to eboclidep
	INNER JOIN pmt.dbo.TPMT_CLIENTS pmtcli  ON pmtcli.ClientId = ebocli.Client_pmt_id --pmtcli to ebocli
	INNER JOIN pmt.dbo.[TPMT_CLIENT_DEPARTMENTS] pmtclidep ON pmtclidep.ClientDepartmentClientId = pmtcli.clientid --pmtclidep to pmtcli
	INNER JOIN pmt.dbo.[TPMT_DEPARTMENTS] pmtdep ON pmtdep.DepartmentId = pmtclidep.ClientDepartmentDepartmentId AND pmtdep.DepartmentName = eboclidep.CLIENTDEPARTMENT_DEPARTMENTID  COLLATE DATABASE_DEFAULT -- pmtdep to pmtclidep and pmtdep to eboclidep
	WHERE 0 = (--none active for client and department
		select Count(ClientDepartmentClientId) 
		from pmt.dbo.[TPMT_CLIENT_DEPARTMENTS] pmtclidep2
		where ClientDepartmentEndDate is null
		AND pmtclidep2.ClientDepartmentClientId = pmtclidep.ClientDepartmentClientId
		AND pmtclidep2.ClientDepartmentDepartmentId = pmtclidep.ClientDepartmentDepartmentId
	)
	Group by eboclidep.CLIENTDEPARTMENT_CLIENTID,pmtcli.ClientId,eboclidep.CLIENTDEPARTMENT_DEPARTMENTID 
	)
	UPDATE D
	SET CLIENTDEPARTMENT_ENDDATE = inactiveDepartments.MAXENDDATE
	from eBook..TPBOOK_CLIENT_DEPARTMENTS D
	INNER JOIN inactiveDepartments on D.CLIENTDEPARTMENT_CLIENTID = inactiveDepartments.EBOOKCLIENTID AND D.CLIENTDEPARTMENT_DEPARTMENTID = inactiveDepartments.CLIENTDEPARTMENT_DEPARTMENTID


