USE [eBook]
GO

/****** Object:  Table [dbo].[TPBOOK_FILE_SERVICES_LOG]    Script Date: 1/03/2015 18:31:09 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[TPBOOK_FILE_SERVICES_LOG](
	[FileServiceLog_Id] [uniqueidentifier] NOT NULL,
	[FileServiceLog_FileId] [uniqueidentifier] NOT NULL,
	[FileServiceLog_ServiceId] [uniqueidentifier] NOT NULL,
	[FileServiceLog_Action] [varchar](50) NOT NULL,
	[FileServiceLog_Comment] [varchar](max) NULL,
	[FileServiceLog_StatusId] [smallint] NULL,
	[FileServiceLog_Timestamp] [datetime] NOT NULL,
	[FileServiceLog_RepositoryItemId] [uniqueidentifier] NULL,
	[FileServiceLog_PersonId] [uniqueidentifier] NOT NULL,
	[FileServiceLog_PersonFirstName] [varchar](50) NOT NULL,
	[FileServiceLog_PersonLastName] [varchar](50) NOT NULL,
 CONSTRAINT [PK_TPBOOK_FILE_SERVICES_LOG] PRIMARY KEY CLUSTERED 
(
	[FileServiceLog_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


