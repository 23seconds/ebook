/****** Script for SelectTopNRows command from SSMS  ******/
SELECT TOP 1000 [BancAccountNr]
      ,[BIC]
      ,[ClientId]
      ,[StatusId]
      ,[LastModified]
      ,[RepositoryItemId]
      ,[DocStoreId]
      ,[PmtAllowCoda]
      ,[StatusModified]
  FROM [eBook].[dbo].[TPBOOK_CLIENT_CODA_POA]
   
UPDATE t
set t.StatusModified = l.timestamp 
from [eBook].[dbo].[TPBOOK_CLIENT_CODA_POA] t
inner join 
(SELECT BankAccountNr,max(timestamp) as 'timestamp'
  FROM [eBook].[dbo].[TPBOOK_CLIENT_CODA_POA_LOG]
  where action like 'status'
  group by [BankAccountNr]) l on t.BancAccountNr = l.BankAccountNr


