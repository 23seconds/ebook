SELECT DISTINCT 
dbo.TPMT_CLIENTS.ClientId, 
pp.PersonGPN, 
dbo.TPMT_ROLES.RoleName, 
dbo.TPMT_CLIENTS.ClientGfisCode, 
et.Team_Source
FROM            
dbo.TPMT_CLIENTS INNER JOIN
dbo.TPMT_CLIENT_DEPARTMENTS ON dbo.TPMT_CLIENT_DEPARTMENTS.ClientDepartmentClientId = dbo.TPMT_CLIENTS.ClientId INNER JOIN
dbo.TPMT_ENGAGEMENT_TEAM AS et ON et.Team_ClientDepartmentID = dbo.TPMT_CLIENT_DEPARTMENTS.ClientDepartmentId INNER JOIN
dbo.TPMT_PERSONS AS pp ON pp.PersonID = et.Team_PersonID INNER JOIN
dbo.TPMT_ROLES ON dbo.TPMT_ROLES.RoleId = et.Team_RoleID
WHERE        (et.Team_EndDate IS NULL OR
                         et.Team_EndDate > GETDATE()) AND (et.Team_Deleted = 0)

SELECT DISTINCT 
	(select personfirstname + ' ' + personlastname from  [PMT].[dbo].[TPMT_PERSONS] where personid = [ExtendedRolesPersonId])
	, (select rolename from  [PMT].[dbo].[TPMT_ROLES] where roleid = [ExtendedRolesRoleId])
		, (select officeName from  [PMT].[dbo].[TPMT_OFFICES] where officeID = [ExtendedRoleOffice])
,clientname,
dbo.TPMT_CLIENTS.ClientId, 
pp.PersonGPN, 
dbo.TPMT_ROLES.RoleName, 
dbo.TPMT_CLIENTS.ClientGfisCode, 
'PMT'
FROM            
dbo.TPMT_CLIENTS INNER JOIN
dbo.TPMT_CLIENT_DEPARTMENTS as cd ON cd.ClientDepartmentClientId = dbo.TPMT_CLIENTS.ClientId INNER JOIN
[PMT].[dbo].[TPMT_EXTENDEDROLES] AS er ON er.ExtendedRolesDepartmentId = cd.[ClientDepartmentDepartmentId] AND er.ExtendedRoleOffice = cd.ClientDepartmentOfficeId INNER JOIN
dbo.TPMT_PERSONS AS pp ON pp.PersonID = er.ExtendedRolesPersonId INNER JOIN
dbo.TPMT_ROLES ON dbo.TPMT_ROLES.RoleId = er.ExtendedRolesRoleId
WHERE        (er.ExtendedRoleTo IS NULL OR er.ExtendedRoleTo > GETDATE())
AND er.ExtendedRolesRoleId = 'F7C6F5F5-7339-43DD-9AA2-43B23E7E03F6'
AND er.ExtendedRolesDepartmentId = '4DEC8114-DA26-47FA-AF47-5FFA0463FC88'
order by dbo.TPMT_CLIENTS.ClientId


/****** Script for SelectTopNRows command from SSMS  ******/
SELECT
	(select personfirstname + ' ' + personlastname from  [PMT].[dbo].[TPMT_PERSONS] where personid = [ExtendedRolesPersonId])
	, (select rolename from  [PMT].[dbo].[TPMT_ROLES] where roleid = [ExtendedRolesRoleId])
		, (select officeName from  [PMT].[dbo].[TPMT_OFFICES] where officeID = [ExtendedRoleOffice])
 [ExtendedRolesId]
      ,[ExtendedRolesPersonId]
      ,[ExtendedRolesRoleId]
      ,[ExtendedRoleOffice]
      ,[ExtendedRoleFrom]
      ,[ExtendedRoleTo]
      ,[ExtendedRolesDepartmentId]
      ,[ExtendedRolesPartnerId]
  FROM [PMT].[dbo].[TPMT_EXTENDEDROLES] as er
   INNER JOIN dbo.TPMT_CLIENT_DEPARTMENTS as cd ON er.ExtendedRolesDepartmentId = cd.[ClientDepartmentDepartmentId]  and er.ExtendedRoleOffice = cd.ClientDepartmentOfficeId
  WHERE        (er.ExtendedRoleTo IS NULL OR er.ExtendedRoleTo > GETDATE())
AND er.ExtendedRolesRoleId = 'F7C6F5F5-7339-43DD-9AA2-43B23E7E03F6'
AND er.ExtendedRolesDepartmentId = '4DEC8114-DA26-47FA-AF47-5FFA0463FC88'

