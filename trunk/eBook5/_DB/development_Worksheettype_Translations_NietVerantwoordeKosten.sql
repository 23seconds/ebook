/****** Script for SelectTopNRows command from SSMS  ******/
SELECT [WORKSHEETTYPE_ID]
      ,[WORKSHEETTYPE_ENTITYORCOLLECTION]
      ,[WORKSHEETTYPE_FIELDNAME]
      ,[WORKSHEETTYPE_CULTURE]
      ,[WORKSHEETTYPE_OUTPUTTYPE]
      ,[WORKSHEETTYPE_TRANSLATION]
  FROM [eBook].[dbo].[TPBOOK_WORKSHEETTYPE_TRANSLATIONS]
  where worksheettype_id = '803D6A06-352E-4E28-ADE5-214DEAAC76CA'
  and [WORKSHEETTYPE_FIELDNAME] like '%verantwoord%'

 
INSERT INTO [eBook].[dbo].[TPBOOK_WORKSHEETTYPE_TRANSLATIONS]
VALUES('803D6A06-352E-4E28-ADE5-214DEAAC76CA','AanslagItem','NietVerantwoordeKosten50','nl-BE','print','Afzonderlijke aanslag van de niet-verantwoorde kosten of voordelen van alle aard, de verdoken meerwinsten en de financiële voordelen of voordelen van alle aard, tegen 50%')

INSERT INTO [eBook].[dbo].[TPBOOK_WORKSHEETTYPE_TRANSLATIONS]
VALUES('803D6A06-352E-4E28-ADE5-214DEAAC76CA','AanslagItem','NietVerantwoordeKosten50','fr-FR','print','Cotisation distincte sur les dépenses ou avantages de toute nature non justifiés, bénéfices dissimulés et avantages financiers ou de toute nature à 50%')

INSERT INTO [eBook].[dbo].[TPBOOK_WORKSHEETTYPE_TRANSLATIONS]
VALUES('803D6A06-352E-4E28-ADE5-214DEAAC76CA','AanslagItem','NietVerantwoordeKosten50','en-US','print','Separate assessment of non accepted charges or benefits in kind, hidden profits and financial advantages or benefits in kind, at 50%')

 
INSERT INTO [eBook].[dbo].[TPBOOK_WORKSHEETTYPE_TRANSLATIONS]
VALUES('803D6A06-352E-4E28-ADE5-214DEAAC76CA','AanslagItem','NietVerantwoordeKosten100','nl-BE','print','Afzonderlijke aanslag van de niet-verantwoorde kosten of voordelen van alle aard, de verdoken meerwinsten en de financiële voordelen of voordelen van alle aard, tegen 100%')

INSERT INTO [eBook].[dbo].[TPBOOK_WORKSHEETTYPE_TRANSLATIONS]
VALUES('803D6A06-352E-4E28-ADE5-214DEAAC76CA','AanslagItem','NietVerantwoordeKosten100','fr-FR','print','Cotisation distincte sur les dépenses ou avantages de toute nature non justifiés, bénéfices dissimulés et avantages financiers ou de toute nature à 100%')

INSERT INTO [eBook].[dbo].[TPBOOK_WORKSHEETTYPE_TRANSLATIONS]
VALUES('803D6A06-352E-4E28-ADE5-214DEAAC76CA','AanslagItem','NietVerantwoordeKosten100','en-US','print','Separate assessment of non accepted charges or benefits in kind, hidden profits and financial advantages or benefits in kind, at 100%')
