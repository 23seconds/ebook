USE [eBook]
GO

/****** Object:  Table [dbo].[TPBOOK_CLIENT_CODA_POA_LOG]    Script Date: 22/01/2015 8:54:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[TPBOOK_CLIENT_CODA_POA_LOG](
	[Id] [uniqueidentifier] NOT NULL,
	[BancAccountNr] [varchar](50) NOT NULL,
	[Action] [varchar](50) NOT NULL,
	[Comment] [varchar](100) NULL,
	[StatusId] [int] NOT NULL,
	[Timestamp] [datetime] NOT NULL,
	[RepositoryItemId] [uniqueidentifier] NULL,
	[PersonId] [uniqueidentifier] NOT NULL,
	[PersonFirstName] [varchar](50) NOT NULL,
	[PersonLastName] [varchar](50) NOT NULL,
 CONSTRAINT [PK_TPBOOK_CLIENT_CODA_POA_LOG] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

