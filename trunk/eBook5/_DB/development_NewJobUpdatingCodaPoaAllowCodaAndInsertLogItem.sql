  SELECT NEWID(),ccp.BancAccountNr,CASE WHEN pmtcli.ClientCodaFlow = 0 THEN 'disallowed' ELSE 'allowed' END,'',null,getdate(),null,'00000000-0000-0000-0000-000000000000','System','PmtApi'
  FROM [eBook].[dbo].[TPBOOK_CLIENT_CODA_POA] ccp
  INNER JOIN [eBook].[dbo].[TPBOOK_CLIENTS] cli ON cli.CLIENT_ID = ccp.clientId
  INNER JOIN [PMT].[dbo].[TPMT_CLIENTS] pmtcli ON cli.CLIENT_PMT_ID = pmtcli.ClientId AND ccp.PmtAllowCoda != pmtcli.ClientCodaFlow

SELECT TOP 1000 [Id]
      ,[BankAccountNr]
      ,[Action]
      ,[Comment]
      ,[StatusId]
      ,[Timestamp]
      ,[RepositoryItemId]
      ,[PersonId]
      ,[PersonFirstName]
      ,[PersonLastName]
  FROM [eBook].[dbo].[TPBOOK_CLIENT_CODA_POA_LOG]

--ADD ALLOW CODA LOG ITEM
INSERT INTO [dbo].[TPBOOK_CLIENT_CODA_POA_LOG]
           ([Id]
           ,[BankAccountNr]
           ,[Action]
           ,[Comment]
           ,[StatusId]
           ,[Timestamp]
           ,[RepositoryItemId]
           ,[PersonId]
           ,[PersonFirstName]
           ,[PersonLastName])
SELECT NEWID(),ccp.BancAccountNr,CASE WHEN pmtcli.ClientCodaFlow = 0 THEN 'disallowed' ELSE 'allowed' END,'',null,getdate(),null,'00000000-0000-0000-0000-000000000000','System','PmtApi'
  FROM [eBook].[dbo].[TPBOOK_CLIENT_CODA_POA] ccp
  INNER JOIN [eBook].[dbo].[TPBOOK_CLIENTS] cli ON cli.CLIENT_ID = ccp.clientId
  INNER JOIN [PMT].[dbo].[TPMT_CLIENTS] pmtcli ON cli.CLIENT_PMT_ID = pmtcli.ClientId AND ccp.PmtAllowCoda != pmtcli.ClientCodaFlow


--UPDATE ALLOW CODA COLUMN
  BEGIN TRAN
  UPDATE ccp
  set ccp.[PmtAllowCoda] = pmtcli.ClientCodaFlow
  FROM [eBook].[dbo].[TPBOOK_CLIENT_CODA_POA] ccp
  INNER JOIN [eBook].[dbo].[TPBOOK_CLIENTS] cli ON cli.CLIENT_ID = ccp.clientId
  INNER JOIN [PMT].[dbo].[TPMT_CLIENTS] pmtcli ON cli.CLIENT_PMT_ID = pmtcli.ClientId AND ccp.PmtAllowCoda != pmtcli.ClientCodaFlow
  ROLLBACK TRAN


