USE [eBook]
GO
/****** Object:  Table [dbo].[TPBOOK_CLIENT_CODA_POA_STATUSES]    Script Date: 12/02/2015 11:21:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TPBOOK_CLIENT_CODA_POA_STATUSES](
	[Id] [int] NOT NULL,
	[Status] [varchar](250) NOT NULL,
	[Rank] [int] NOT NULL,
	[activation] [varchar](10) NOT NULL,
 CONSTRAINT [PK_TPBOOK_CLIENT_CODA_POA_STATUSES] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[TPBOOK_CLIENT_CODA_POA_STATUSES] ([Id], [Status], [Rank], [activation]) VALUES (10, N'Proxy request sent to client', 1, N'User')
INSERT [dbo].[TPBOOK_CLIENT_CODA_POA_STATUSES] ([Id], [Status], [Rank], [activation]) VALUES (20, N'Signed proxy received from client ', 2, N'User')
INSERT [dbo].[TPBOOK_CLIENT_CODA_POA_STATUSES] ([Id], [Status], [Rank], [activation]) VALUES (21, N'Signed proxy received from client - incomplete', 3, N'User')
INSERT [dbo].[TPBOOK_CLIENT_CODA_POA_STATUSES] ([Id], [Status], [Rank], [activation]) VALUES (-99, N'No CODA files requested by client ', 4, N'User')
INSERT [dbo].[TPBOOK_CLIENT_CODA_POA_STATUSES] ([Id], [Status], [Rank], [activation]) VALUES (40, N'Signed proxy sent to bank', 5, N'User')
INSERT [dbo].[TPBOOK_CLIENT_CODA_POA_STATUSES] ([Id], [Status], [Rank], [activation]) VALUES (99, N'CODA files received', 6, N'System')
INSERT [dbo].[TPBOOK_CLIENT_CODA_POA_STATUSES] ([Id], [Status], [Rank], [activation]) VALUES (-98, N'CODA files cancelled by client', 7, N'User')
ALTER TABLE [dbo].[TPBOOK_CLIENT_CODA_POA_STATUSES] ADD  CONSTRAINT [DF_TPBOOK_CLIENT_CODA_POA_STATUSES_Rank]  DEFAULT ((0)) FOR [Rank]
GO
ALTER TABLE [dbo].[TPBOOK_CLIENT_CODA_POA_STATUSES] ADD  DEFAULT ('User') FOR [activation]
GO