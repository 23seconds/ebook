/****** Script for SelectTopNRows command from SSMS  ******/
SELECT TOP 1000 [PersonID]
      ,[PersonGPN]
      ,[PersonLastName]
      ,[PersonFirstName]
      ,[PersonEmail]
      ,[PersonDefaultOffice]
      ,[PersonWindowsAccount]
      ,[PersonLastLogon]
      ,[PersonDataUpdated]
      ,[PersonEndedAt]
      ,[PersonDepartmentId]
      ,[PersonRole]
      ,[PersonRoleId]
      ,[PersonDefaultRoleId]
      ,[PersonCounsellorId]
  FROM [PMT].[dbo].[TPMT_PERSONS]
  where [PersonLastName] in ('Neukermans','VAN CALSTER')

  /****** Script for SelectTopNRows command from SSMS  ******/
SELECT 
	(select personlastname from  [PMT].[dbo].[TPMT_PERSONS] where personid = [ExtendedRolesPersonId])
	, (select rolename from  [PMT].[dbo].[TPMT_ROLES] where roleid = [ExtendedRolesRoleId])
		, (select officeName from  [PMT].[dbo].[TPMT_OFFICES] where officeID = [ExtendedRoleOffice])
	, [ExtendedRolesId]
      ,[ExtendedRolesPersonId]
      ,[ExtendedRolesRoleId]
      ,[ExtendedRoleOffice]
      ,[ExtendedRoleFrom]
      ,[ExtendedRoleTo]
      ,[ExtendedRolesDepartmentId]
      ,[ExtendedRolesPartnerId]
  FROM [PMT].[dbo].[TPMT_EXTENDEDROLES]
  WHERE [ExtendedRolesPersonId] in
  ('D901ABA5-410D-4B4D-9626-589CE1D112FC',
'B0E64327-840F-452E-8EDE-DABA861CED58')


--INSERT EXTENDED ROLE
INSERT INTO [PMT].[dbo].[TPMT_EXTENDEDROLES]
VALUES (NEWID(),'B0E64327-840F-452E-8EDE-DABA861CED58','F7C6F5F5-7339-43DD-9AA2-43B23E7E03F6','00473',getDate(),null,'4DEC8114-DA26-47FA-AF47-5FFA0463FC88',null)


/****** Script for SelectTopNRows command from SSMS  ******/
SELECT TOP 1000 [ClientDepartmentId]
      ,[ClientDepartmentClientId]
      ,[ClientDepartmentDepartmentId]
      ,[ClientDepartmentOfficeId]
      ,[ClientDepartmentStartDate]
      ,[ClientDepartmentEndDate]
  FROM [PMT].[dbo].[TPMT_CLIENT_DEPARTMENTS]


--Insert Office Validators based on department, office (and partner?)
SELECT DISTINCT 
dbo.TPMT_CLIENTS.ClientId, 
pp.PersonGPN, 
dbo.TPMT_ROLES.RoleName, 
dbo.TPMT_CLIENTS.ClientGfisCode, 
et.Team_Source
FROM            
dbo.TPMT_CLIENTS INNER JOIN
dbo.TPMT_CLIENT_DEPARTMENTS as cd ON cd.ClientDepartmentClientId = dbo.TPMT_CLIENTS.ClientId INNER JOIN
[PMT].[dbo].[TPMT_EXTENDEDROLES] AS er ON er.ExtendedRolesDepartmentId = cd.departmentId AND er.ExtendedRoleOffice = cd.ClientDepartmentOfficeId INNER JOIN
--dbo.TPMT_ENGAGEMENT_TEAM AS et ON et.Team_ClientDepartmentID = dbo.TPMT_CLIENT_DEPARTMENTS.ClientDepartmentId INNER JOIN
dbo.TPMT_PERSONS AS pp ON pp.PersonID = er.ExtendedRolesPersonId INNER JOIN
dbo.TPMT_ROLES ON dbo.TPMT_ROLES.RoleId = er.ExtendedRolesRoleId
WHERE        (er.ExtendedRoleTo IS NULL OR er.ExtendedRoleTo > GETDATE())
AND er.ExtendedRolesRoleId = 'F7C6F5F5-7339-43DD-9AA2-43B23E7E03F6'


/*
SELECT DISTINCT 
dbo.TPMT_CLIENTS.ClientId, 
pp.PersonGPN, 
dbo.TPMT_ROLES.RoleName, 
dbo.TPMT_CLIENTS.ClientGfisCode, 
et.Team_Source
FROM            
dbo.TPMT_CLIENTS INNER JOIN
dbo.TPMT_CLIENT_DEPARTMENTS ON dbo.TPMT_CLIENT_DEPARTMENTS.ClientDepartmentClientId = dbo.TPMT_CLIENTS.ClientId INNER JOIN
dbo.TPMT_ENGAGEMENT_TEAM AS et ON et.Team_ClientDepartmentID = dbo.TPMT_CLIENT_DEPARTMENTS.ClientDepartmentId INNER JOIN
dbo.TPMT_PERSONS AS pp ON pp.PersonID = et.Team_PersonID INNER JOIN
dbo.TPMT_ROLES ON dbo.TPMT_ROLES.RoleId = et.Team_RoleID
WHERE        (et.Team_EndDate IS NULL OR
                         et.Team_EndDate > GETDATE()) AND (et.Team_Deleted = 0)
*/