--Script can be utilized to change the person roles for certain clients
--EXECUTE DELETE BElOW FIRST

-- get  persons
SELECT *
  FROM [eBook].[dbo].[TPBOOK_PERSONS]
  WHERE Person_firstname + Person_lastname in ('anmaes','timDE PLECKER','theoHeselmans','lievepoellaer','c�dricvanopdenbosch','sarahVANDENHENDE')
 

  --get clietns
SELECT * FROM [eBook].[dbo].[TPBOOK_CLIENTS] WHERE [CLIENT_NAME] in ('TAX TEST CLIENT 07','TAX TEST CLIENT 16','TAX TEST CLIENT 15','TAX TEST CLIENT 17','TAX TEST CLIENT 18')

--get all possible roles
SELECT distinct [PCR_ROLE]
  FROM [eBook].[dbo].[TPBOOK_PERSON_CLIENT_ROLE]

--DELETE FIRST
begin tran
DELETE FROM [eBook].[dbo].[TPBOOK_PERSON_CLIENT_ROLE]
  where PCR_CLIENT_ID IN
(
  	SELECT CLIENT_ID
	FROM [eBook].[dbo].[TPBOOK_CLIENTS]
	WHERE [CLIENT_NAME] in ('TAX TEST CLIENT 07','TAX TEST CLIENT 16','TAX TEST CLIENT 15','TAX TEST CLIENT 17','TAX TEST CLIENT 18')
)
rollback tran


--Add roles of to client looping to both persons and clients
USE ebook;
GO
Declare @countPersons int
Declare @countClients int
Declare @flagPersons int
Declare @flagClients int
Declare @personId uniqueidentifier
Declare @clientId uniqueidentifier
SET @countPersons = (SELECT count(person_id) FROM [eBook].[dbo].[TPBOOK_PERSONS] WHERE Person_firstname + Person_lastname in ('anmaes','timDE PLECKER','theoHeselmans','lievepoellaer','c�dricvanopdenbosch','sarahVANDENHENDE')
  )
SET @countClients= (SELECT count(CLIENT_ID) FROM [eBook].[dbo].[TPBOOK_CLIENTS] WHERE [CLIENT_NAME] in ('TAX TEST CLIENT 07','TAX TEST CLIENT 16','TAX TEST CLIENT 15','TAX TEST CLIENT 17','TAX TEST CLIENT 18'))
SET @flagPersons = 1
SET @flagClients = 1


WHILE @flagPersons - 1 < @countPersons
BEGIN
	--PRINT 'person: ' + CAST(@flagPersons as varchar(10))
	SET @flagClients = 1
	
	SET @personId = (SELECT person_id FROM (
	  SELECT
		ROW_NUMBER() OVER (ORDER BY person_id ASC) AS rownumber,
		person_id
	  FROM [eBook].[dbo].[TPBOOK_PERSONS] WHERE Person_firstname + Person_lastname in ('anmaes','timDE PLECKER','theoHeselmans','lievepoellaer','c�dricvanopdenbosch','sarahVANDENHENDE')
  
	) AS foo
	WHERE rownumber = @flagPersons)

	--this also makes the user admin
	insert into [eBook].[dbo].[TPBOOK_PERSON_ROLES]
	VALUES (@personId,'Admin',1)
	
	WHILE @flagClients - 1 < @countClients
	BEGIN
		--PRINT 'client: ' + CAST(@flagClients as varchar(10))
		
		SET @clientId = (SELECT CLIENT_ID FROM (
		SELECT
		ROW_NUMBER() OVER (ORDER BY CLIENT_ID ASC) AS rownumber,
		CLIENT_ID
		FROM [eBook].[dbo].[TPBOOK_CLIENTS] WHERE [CLIENT_NAME] in ('TAX TEST CLIENT 07','TAX TEST CLIENT 16','TAX TEST CLIENT 15','TAX TEST CLIENT 17','TAX TEST CLIENT 18')
		) AS foo
		WHERE rownumber = @flagClients)
			
		insert INTO [TPBOOK_PERSON_CLIENT_ROLE]
		VALUES (@clientId,@personId,'OfficeValidator',0,0)
		insert INTO [TPBOOK_PERSON_CLIENT_ROLE]
		VALUES (@clientId,@personId,'Manager',0,0)
		insert INTO [TPBOOK_PERSON_CLIENT_ROLE]
		VALUES (@clientId,@personId,'Staff',0,0)
		insert INTO [TPBOOK_PERSON_CLIENT_ROLE]
		VALUES (@clientId,@personId,'Admin',0,0)
		
		PRINT 'Client ID:'  + CAST(@clientId as varchar(90))
		PRINT 'Person ID:'  + CAST(@personid as varchar(90))
		SET @flagClients = @flagClients + 1
	END
	SET @flagPersons = @flagPersons + 1 
END



SELECT * ,(SELECT Person_firstname + Person_lastname
  FROM [eBook].[dbo].[TPBOOK_PERSONS]
  WHERE person_id = pcr_person_id)
  FROM [eBook].[dbo].[TPBOOK_PERSON_CLIENT_ROLE]
  where PCR_CLIENT_ID IN
(
  	SELECT CLIENT_ID
	FROM [eBook].[dbo].[TPBOOK_CLIENTS]
	WHERE [CLIENT_NAME] in ('TAX TEST CLIENT 07','TAX TEST CLIENT 16','TAX TEST CLIENT 15','TAX TEST CLIENT 17','TAX TEST CLIENT 18')
)

/****** Script for SelectTopNRows command from SSMS  ******/
SELECT TOP 1000 [PersonRole_Person_ID]
      ,[PersonRole_Role]
      ,[PersonRole_manual]
  FROM [eBook].[dbo].[TPBOOK_PERSON_ROLES]
  where personrole_role = 'admin'
  AND [PersonRole_Person_ID] IN
(
 SELECT person_id
	  FROM [eBook].[dbo].[TPBOOK_PERSONS] WHERE Person_firstname + Person_lastname in ('anmaes','timDE PLECKER','theoHeselmans','lievepoellaer','c�dricvanopdenbosch','sarahVANDENHENDE')
  
)

/*
OfficeValidator
Manager
Staff
Director
Admin
Partner
*/

/* TEST PCR BEFORE CHANGE
PCR_CLIENT_ID	PCR_PERSON_ID	PCR_ROLE	PCR_MARKDELETE	PCR_GFIS
E22A96A6-3F59-42AD-9675-850354549D1D	D575457D-FF33-42DE-BA9B-22AF8E6C2100	Manager	0	0
E22A96A6-3F59-42AD-9675-850354549D1D	27920935-6DDD-40F1-9C16-536A1D696B58	Staff	0	0
E22A96A6-3F59-42AD-9675-850354549D1D	BF0FA3C4-86DF-4947-A73D-79AB5F546A90	Staff	0	0
E22A96A6-3F59-42AD-9675-850354549D1D	C97899A9-94AE-4219-B26F-9BD106B30AFD	Staff	0	0
E22A96A6-3F59-42AD-9675-850354549D1D	E66A56EF-F44F-4A6A-9977-C7F54A5D25BA	Staff	0	0
EEF7E641-9157-4997-B4E5-F19D4295C27F	6BF61892-D9A0-495E-A07B-185DC7B03F54	Staff	0	0
EEF7E641-9157-4997-B4E5-F19D4295C27F	D575457D-FF33-42DE-BA9B-22AF8E6C2100	Manager	0	0
EEF7E641-9157-4997-B4E5-F19D4295C27F	27920935-6DDD-40F1-9C16-536A1D696B58	Staff	0	0
EEF7E641-9157-4997-B4E5-F19D4295C27F	BF0FA3C4-86DF-4947-A73D-79AB5F546A90	Staff	0	0
EEF7E641-9157-4997-B4E5-F19D4295C27F	C97899A9-94AE-4219-B26F-9BD106B30AFD	Staff	0	0
EEF7E641-9157-4997-B4E5-F19D4295C27F	E66A56EF-F44F-4A6A-9977-C7F54A5D25BA	Staff	0	0
59C402DF-C178-4E95-8C9F-FF416A8B3A95	D575457D-FF33-42DE-BA9B-22AF8E6C2100	Manager	0	0
59C402DF-C178-4E95-8C9F-FF416A8B3A95	27920935-6DDD-40F1-9C16-536A1D696B58	Staff	0	0
59C402DF-C178-4E95-8C9F-FF416A8B3A95	BF0FA3C4-86DF-4947-A73D-79AB5F546A90	Staff	0	0
59C402DF-C178-4E95-8C9F-FF416A8B3A95	C97899A9-94AE-4219-B26F-9BD106B30AFD	Staff	0	0
59C402DF-C178-4E95-8C9F-FF416A8B3A95	E66A56EF-F44F-4A6A-9977-C7F54A5D25BA	Staff	0	0
*/