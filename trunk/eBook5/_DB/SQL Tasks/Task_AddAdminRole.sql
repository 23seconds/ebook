/****** Script for SelectTopNRows command from SSMS  ******/
SELECT TOP 1000 [PersonRole_Person_ID]
      ,[PersonRole_Role]
      ,[PersonRole_manual]
  FROM [eBook].[dbo].[TPBOOK_PERSON_ROLES]

  --Look for person ID
  SELECT person_id FROM [eBook].[dbo].[TPBOOK_PERSONS] WHERE person_firstName + ' ' + person_lastname = 'Virginie Vanoudewater'

  --Insert admin role using full name
  begin tran
  INSERT INTO [eBook].[dbo].[TPBOOK_PERSON_ROLES]
  VALUES(
  (SELECT person_id FROM [eBook].[dbo].[TPBOOK_PERSONS] WHERE person_firstName + ' ' + person_lastname = 'frédérique d''haeye'),
  'Admin',
  1)
  rollback tran


  begin tran

  rollback tran