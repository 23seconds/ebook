

INSERT INTO [eBook].[dbo].[TPBOOK_PERSONS] VALUES (newid(),'BE000000001','Michielsens','Lisa','lisa.michielsens@be.ey.com','00473','EURW\bebxaudlmic',null,getdate(),null,1,'ACR')
INSERT INTO [eBook].[dbo].[TPBOOK_PERSON_ROLES] VALUES('B4AD77CE-447A-42F1-BB8E-357885B1985C','Admin',1,0) 

/****** Script for SelectTopNRows command from SSMS  ******/
SELECT TOP 1000 [Person_ID]
      ,[Person_GPN]
      ,[Person_LastName]
      ,[Person_FirstName]
      ,[Person_Email]
      ,[Person_DefaultOffice]
      ,[Person_WindowsAccount]
      ,[Person_LastLogon]
      ,[Person_DataUpdated]
      ,[Person_EndedAt]
      ,[Person_Manual]
      ,[Person_Department]
  FROM [eBook].[dbo].[TPBOOK_PERSONS]
    INNER JOIN eBook.dbo.TPBOOK_PERSON_ROLES ON person_id = PersonRole_Person_ID
  INNER JOIN eBook.dbo.TPBOOK_OFFICE ON person_defaultOffice = office_id
  where [Person_GPN] = 'BE000000001'