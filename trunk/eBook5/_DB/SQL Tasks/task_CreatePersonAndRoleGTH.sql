
--GTH Team 1
select newid()

INSERT INTO [eBook].[dbo].[TPBOOK_PERSONS] VALUES ('AF567128-8B45-49B6-AFC1-D24CD16FD7C7','IN010M30253','Sharma','Rubal ','rubal.sharma@in.ey.com','00473','MEA\Rubal.Sharma',null,getdate(),null,1,'ACR')
INSERT INTO [eBook].[dbo].[TPBOOK_PERSON_ROLES] VALUES('AF567128-8B45-49B6-AFC1-D24CD16FD7C7','GTH Team 1',1,0) 

INSERT INTO [eBook].[dbo].[TPBOOK_PERSONS] VALUES ('5519B405-76F0-47B6-A9D9-D8E7C069BC58','IN010M29881','Channa','Chandni','chandni.channa@in.ey.com','00473','MEA\Chandni.Channa',null,getdate(),null,1,'ACR')
INSERT INTO [eBook].[dbo].[TPBOOK_PERSON_ROLES] VALUES('5519B405-76F0-47B6-A9D9-D8E7C069BC58','GTH Team 1',1,0) 

INSERT INTO [eBook].[dbo].[TPBOOK_PERSONS] VALUES ('5A6E3006-9ECF-402B-B2AD-70A5D31AE43C','IN010M29754','Aggarwal','Komal','komal.aggarwal@in.ey.com','00473','MEA\Komal.Aggarwal',null,getdate(),null,1,'ACR')
INSERT INTO [eBook].[dbo].[TPBOOK_PERSON_ROLES] VALUES('5A6E3006-9ECF-402B-B2AD-70A5D31AE43C','GTH Team 1',1,0) 

INSERT INTO [eBook].[dbo].[TPBOOK_PERSONS] VALUES ('6AC454ED-5019-484E-8E93-112B4566D613','IN01M30100','Chauhan','Priyanka','priyanka.chauhan@in.ey.com','00473','MEA\Priyanka.Chauhan',null,getdate(),null,1,'ACR')
INSERT INTO [eBook].[dbo].[TPBOOK_PERSON_ROLES] VALUES('6AC454ED-5019-484E-8E93-112B4566D613','GTH Team 1',1,0) 

--GTH Team 4
select newid()

INSERT INTO [eBook].[dbo].[TPBOOK_PERSONS] VALUES ('C4FA0389-E998-43D1-89F1-E66ED992C3C0','IN010M28446','Kaur','Harleen','harleen.kaur1@in.ey.com','00473','MEA\Harleen.Kaur1',null,getdate(),null,1,'ACR')
INSERT INTO [eBook].[dbo].[TPBOOK_PERSON_ROLES] VALUES('C4FA0389-E998-43D1-89F1-E66ED992C3C0','GTH Team 4',1,0) 

INSERT INTO [eBook].[dbo].[TPBOOK_PERSONS] VALUES ('4E0BEDE7-9F7E-4741-AB65-BF540B083105','IN010M28941','Sharma','Khushbu','khushbu.sharma@in.ey.com','00473','MEA\Khushbu.Sharma',null,getdate(),null,1,'ACR')
INSERT INTO [eBook].[dbo].[TPBOOK_PERSON_ROLES] VALUES('4E0BEDE7-9F7E-4741-AB65-BF540B083105','GTH Team 4',1,0) 

INSERT INTO [eBook].[dbo].[TPBOOK_PERSONS] VALUES ('A8E0D7BF-E660-41AC-B668-EDCA3A7C4464','IN010M11177','Saxena','Karishma','karishma.saxena@in.ey.com','00473','MEA\Karishma.Saxena',null,getdate(),null,1,'ACR')
INSERT INTO [eBook].[dbo].[TPBOOK_PERSON_ROLES] VALUES('A8E0D7BF-E660-41AC-B668-EDCA3A7C4464','GTH Team 4',1,0) 

INSERT INTO [eBook].[dbo].[TPBOOK_PERSONS] VALUES ('405FE843-FE51-4AEB-80B8-BC8A83D4A521','IN010M11191','Verma','Chandni','chandni.verma@in.ey.com','00473','MEA\Chandni.Verma',null,getdate(),null,1,'ACR')
INSERT INTO [eBook].[dbo].[TPBOOK_PERSON_ROLES] VALUES('405FE843-FE51-4AEB-80B8-BC8A83D4A521','GTH Team 4',1,0) 

INSERT INTO [eBook].[dbo].[TPBOOK_PERSONS] VALUES ('86218B90-1828-4BCE-88D1-5935AA226529','IN010M09018','Aamir','Mohd F','mohd.aamir@in.ey.com','00473','MEA\Mohd.Aamir',null,getdate(),null,1,'ACR')
INSERT INTO [eBook].[dbo].[TPBOOK_PERSON_ROLES] VALUES('86218B90-1828-4BCE-88D1-5935AA226529','GTH Team 4',1,0) 

INSERT INTO [eBook].[dbo].[TPBOOK_PERSONS] VALUES ('A901F77C-5057-437E-AEB6-9CFF19DDB409','IN010M08243','Sharma','Bhumika','bhumika.sharma@in.ey.com','00473','MEA\Bhumika.Sharma',null,getdate(),null,1,'ACR')
INSERT INTO [eBook].[dbo].[TPBOOK_PERSON_ROLES] VALUES('A901F77C-5057-437E-AEB6-9CFF19DDB409','GTH Team 4',1,0) 




/****** Script for SelectTopNRows command from SSMS  ******/
SELECT TOP 1000 [Person_ID]
      ,[Person_GPN]
      ,[Person_LastName]
      ,[Person_FirstName]
      ,[Person_Email]
      ,[Person_DefaultOffice]
      ,[Person_WindowsAccount]
      ,[Person_LastLogon]
      ,[Person_DataUpdated]
      ,[Person_EndedAt]
      ,[Person_Manual]
      ,[Person_Department]
  FROM [eBook].[dbo].[TPBOOK_PERSONS]
    INNER JOIN eBook.dbo.TPBOOK_PERSON_ROLES ON person_id = PersonRole_Person_ID
  INNER JOIN eBook.dbo.TPBOOK_OFFICE ON person_defaultOffice = office_id
  where [Person_GPN] = 'BE000000001'