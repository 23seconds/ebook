

--FIND CLIENT BY NAME
SELECT TOP 1000 *
  FROM [eBook].[dbo].[TPBOOK_CLIENTS]
  WHERE CLIENT_NAME like '%XCEED%'

--GET NON-DELETED REPOSITORY FILES FROM CLIENT
SELECT TOP 1000 * 
FROM TPBOOK_REPOSITORY
WHERE REPOSITORY_ITEM_CLIENT_ID = (SELECT CLIENT_ID
  FROM [eBook].[dbo].[TPBOOK_CLIENTS]
  WHERE CLIENT_NAME like '%XCEED%')
  --STRUCTURE FILTER
  AND REPOSITORY_ITEM_REPOSITORY_STRUCTURE_ID = '32F0391E-E0BD-41F9-A9C1-C04EAC3C8CB1'
  --NOT DELETED
  AND REPOSITORY_ITEM_DELETED != 1


  -- SOFT DELETE FILE
  begin tran
  update TPBOOK_REPOSITORY
  set repository_item_deleted = 1
  WHERE repository_item_id = '43E03BE0-2630-4D9A-A20C-C126771AB82C'
  rollback tran
