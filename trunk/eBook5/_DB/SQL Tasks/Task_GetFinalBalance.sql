/*GET FINAL BALANCE*/
	SELECT Account_VisualNR as 'Compte', 
	isnull([Account_Description],'')  as 'Description',
	[Account_PreviousSaldo] as 'Bilan avant ajustements', 
	CASE WHEN [Account_CurrentSaldo] - Account_PreviousSaldo > 0 THEN [Account_CurrentSaldo] - Account_PreviousSaldo  ELSE 0 END   as 'Adjustments débet',
	CASE WHEN [Account_CurrentSaldo] - Account_PreviousSaldo < 0 THEN [Account_CurrentSaldo] - Account_PreviousSaldo  ELSE 0 END   as 'Adjustments crédit',
	[Account_CurrentSaldo] AS 'Bilan après adjustements'
	FROM [eBook].[dbo].[TPBOOK_FILE_ACCOUNTS] fa
	LEFT JOIN [eBook].[dbo].[TPBOOK_FILE_ACCOUNT_DESCRIPTIONS] fad ON  fad.[Account_File_ID] = fa.account_file_id AND [Account_Description_Culture] = 'fr-FR' AND fad.[Account_InternalNR] = fa.[Account_InternalNR]
	WHERE fa.account_file_id = '49daec05-7e5b-477b-81a8-fa04d38cc813'
	AND ([Account_PreviousSaldo] != 0
	OR [Account_CurrentSaldo] != 0)
	AND [Account_IsTaxAdjustment] = 0
	AND fa.[Account_InternalNR] not like '%.%'


