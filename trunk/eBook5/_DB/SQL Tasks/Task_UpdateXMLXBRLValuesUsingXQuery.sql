--GET XBRL DATA OF FILE
SELECT  [FILE_XBRL_FILE_ID]
      ,[FILE_XBRL_TYPE]
      ,[FILE_XBRL_DATA]
      ,[FILE_XBRL_CALCULATION]
      ,[FILE_XBRL_VALIDATED]
      ,[FILE_XBRL_STATUS_ID]
      ,[FILE_XBRL_LOCKED]
      ,[FILE_XBRL_DECLARING_DEPARTMENT]
      ,[FILE_XBRL_DECLARING_PARTNER_ID]
      ,[FILE_XBRL_DECLARING_PARTNER_NAME]
  FROM [eBook].[dbo].[TPBOOK_FILE_XBRLDATA]
  where file_xbrl_file_id = '69d9a16e-2ae1-41e3-bb89-5155a09bc571'



  --FILTERING OUT XML VALUES USING XQUERY
  --SELECT AnnualWorkForceAverageCorporationCodeCurrentTaxPeriod
  ---------
WITH XMLNAMESPACES 
('http://schemas.datacontract.org/2004/07/EY.com.eBook.BizTax.Contracts' AS ns,
'http://www.w3.org/2001/XMLSchema-instance' as ns2,
'http://schemas.datacontract.org/2004/07/EY.com.eBook.BizTax' as d2p1,
DEFAULT 'http://schemas.datacontract.org/2004/07/EY.com.eBook.BizTax.Contracts' )
SELECT file_xbrl_file_id,
[FILE_XBRL_DATA].value('(/BizTaxDataContract/Elements/d2p1:XbrlElementDataContract[d2p1:Name/text()="AnnualWorkForceAverageCorporationCodeCurrentTaxPeriod"]/d2p1:UnitRef)[1]','varchar(50)') as 'unitref',
[FILE_XBRL_DATA].value('(/BizTaxDataContract/Elements/d2p1:XbrlElementDataContract[d2p1:Name/text()="AnnualWorkForceAverageCorporationCodeCurrentTaxPeriod"]/d2p1:Namespace)[1]','varchar(50)') as 'Namespace',
[FILE_XBRL_DATA].value('(/BizTaxDataContract/Elements/d2p1:XbrlElementDataContract[d2p1:Name/text()="AnnualWorkForceAverageCorporationCodeCurrentTaxPeriod"]/d2p1:Value)[1]','varchar(50)') as 'Value',
[FILE_XBRL_DATA].value('(/BizTaxDataContract/Elements/d2p1:XbrlElementDataContract[d2p1:Name/text()="AnnualWorkForceAverageCorporationCodeCurrentTaxPeriod"]/d2p1:Calculated)[1]','varchar(50)') as 'Calculated'
FROM [eBook].[dbo].[TPBOOK_FILE_XBRLDATA]
WHERE [FILE_XBRL_DATA].exist('(/BizTaxDataContract/Elements/d2p1:XbrlElementDataContract[d2p1:Name/text()="AnnualWorkForceAverageCorporationCodeCurrentTaxPeriod"]/d2p1:UnitRef[.="EUR"])')=1
AND file_xbrl_file_id = '69d9a16e-2ae1-41e3-bb89-5155a09bc571'
GO
---------
--PRevious value 69D9A16E-2AE1-41E3-BB89-5155A09BC571	EUR	NULL		true

--UPDATE AnnualWorkForceAverageCorporationCodeCurrentTaxPeriod Calculated value
WITH XMLNAMESPACES 
('http://schemas.datacontract.org/2004/07/EY.com.eBook.BizTax.Contracts' AS ns,
'http://www.w3.org/2001/XMLSchema-instance' as ns2,
'http://schemas.datacontract.org/2004/07/EY.com.eBook.BizTax' as d2p1,
DEFAULT 'http://schemas.datacontract.org/2004/07/EY.com.eBook.BizTax.Contracts' )
UPDATE [eBook].[dbo].[TPBOOK_FILE_XBRLDATA]
SET [FILE_XBRL_DATA].modify('replace value of (/BizTaxDataContract/Elements/d2p1:XbrlElementDataContract[d2p1:Name/text()="AnnualWorkForceAverageCorporationCodeCurrentTaxPeriod"]/d2p1:Calculated/text())[1] with ("false")')
WHERE [FILE_XBRL_DATA].exist('(/BizTaxDataContract/Elements/d2p1:XbrlElementDataContract[d2p1:Name/text()="AnnualWorkForceAverageCorporationCodeCurrentTaxPeriod"]/d2p1:UnitRef[.="EUR"])')=1
AND file_xbrl_file_id = '69d9a16e-2ae1-41e3-bb89-5155a09bc571';

--UPDATE AnnualWorkForceAverageCorporationCodeCurrentTaxPeriod UnitRef value
WITH XMLNAMESPACES 
('http://schemas.datacontract.org/2004/07/EY.com.eBook.BizTax.Contracts' AS ns,
'http://www.w3.org/2001/XMLSchema-instance' as ns2,
'http://schemas.datacontract.org/2004/07/EY.com.eBook.BizTax' as d2p1,
DEFAULT 'http://schemas.datacontract.org/2004/07/EY.com.eBook.BizTax.Contracts' )
UPDATE [eBook].[dbo].[TPBOOK_FILE_XBRLDATA]
SET [FILE_XBRL_DATA].modify('replace value of (/BizTaxDataContract/Elements/d2p1:XbrlElementDataContract[d2p1:Name/text()="AnnualWorkForceAverageCorporationCodeCurrentTaxPeriod"]/d2p1:UnitRef/text())[1] with ("U-Pure")')
WHERE [FILE_XBRL_DATA].exist('(/BizTaxDataContract/Elements/d2p1:XbrlElementDataContract[d2p1:Name/text()="AnnualWorkForceAverageCorporationCodeCurrentTaxPeriod"]/d2p1:UnitRef[.="EUR"])')=1
AND file_xbrl_file_id = '69d9a16e-2ae1-41e3-bb89-5155a09bc571'





  --FILTERING OUT XML VALUES USING XQUERY
  --SELECT AnnualTurnoverExcludingVATCorporationCodeCurrentTaxPeriod
  ---------
WITH XMLNAMESPACES 
('http://schemas.datacontract.org/2004/07/EY.com.eBook.BizTax.Contracts' AS ns,
'http://www.w3.org/2001/XMLSchema-instance' as ns2,
'http://schemas.datacontract.org/2004/07/EY.com.eBook.BizTax' as d2p1,
DEFAULT 'http://schemas.datacontract.org/2004/07/EY.com.eBook.BizTax.Contracts' )
SELECT file_xbrl_file_id,
[FILE_XBRL_DATA].value('(/BizTaxDataContract/Elements/d2p1:XbrlElementDataContract[d2p1:Name/text()="AnnualTurnoverExcludingVATCorporationCodeCurrentTaxPeriod"]/d2p1:UnitRef)[1]','varchar(50)') as 'unitref',
[FILE_XBRL_DATA].value('(/BizTaxDataContract/Elements/d2p1:XbrlElementDataContract[d2p1:Name/text()="AnnualTurnoverExcludingVATCorporationCodeCurrentTaxPeriod"]/d2p1:Namespace)[1]','varchar(50)') as 'Namespace',
[FILE_XBRL_DATA].value('(/BizTaxDataContract/Elements/d2p1:XbrlElementDataContract[d2p1:Name/text()="AnnualTurnoverExcludingVATCorporationCodeCurrentTaxPeriod"]/d2p1:Value)[1]','varchar(50)') as 'Value',
[FILE_XBRL_DATA].value('(/BizTaxDataContract/Elements/d2p1:XbrlElementDataContract[d2p1:Name/text()="AnnualTurnoverExcludingVATCorporationCodeCurrentTaxPeriod"]/d2p1:Calculated)[1]','varchar(50)') as 'Calculated'
FROM [eBook].[dbo].[TPBOOK_FILE_XBRLDATA]
WHERE [FILE_XBRL_DATA].exist('(/BizTaxDataContract/Elements/d2p1:XbrlElementDataContract[d2p1:Name/text()="AnnualTurnoverExcludingVATCorporationCodeCurrentTaxPeriod"]/d2p1:UnitRef[.="EUR"])')=1
AND file_xbrl_file_id = '69d9a16e-2ae1-41e3-bb89-5155a09bc571'
GO
---------

--UPDATE AnnualTurnoverExcludingVATCorporationCodeCurrentTaxPeriod Calculated value
WITH XMLNAMESPACES 
('http://schemas.datacontract.org/2004/07/EY.com.eBook.BizTax.Contracts' AS ns,
'http://www.w3.org/2001/XMLSchema-instance' as ns2,
'http://schemas.datacontract.org/2004/07/EY.com.eBook.BizTax' as d2p1,
DEFAULT 'http://schemas.datacontract.org/2004/07/EY.com.eBook.BizTax.Contracts' )
UPDATE [eBook].[dbo].[TPBOOK_FILE_XBRLDATA]
SET [FILE_XBRL_DATA].modify('replace value of (/BizTaxDataContract/Elements/d2p1:XbrlElementDataContract[d2p1:Name/text()="AnnualTurnoverExcludingVATCorporationCodeCurrentTaxPeriod"]/d2p1:Calculated/text())[1] with ("false")')
WHERE [FILE_XBRL_DATA].exist('(/BizTaxDataContract/Elements/d2p1:XbrlElementDataContract[d2p1:Name/text()="AnnualTurnoverExcludingVATCorporationCodeCurrentTaxPeriod"]/d2p1:UnitRef[.="EUR"])')=1 
AND file_xbrl_file_id = '69d9a16e-2ae1-41e3-bb89-5155a09bc571';





  --FILTERING OUT XML VALUES USING XQUERY
  --SELECT BalanceSheetTotalCorporationCodeCurrentTaxPeriod
  ---------
WITH XMLNAMESPACES 
('http://schemas.datacontract.org/2004/07/EY.com.eBook.BizTax.Contracts' AS ns,
'http://www.w3.org/2001/XMLSchema-instance' as ns2,
'http://schemas.datacontract.org/2004/07/EY.com.eBook.BizTax' as d2p1,
DEFAULT 'http://schemas.datacontract.org/2004/07/EY.com.eBook.BizTax.Contracts' )
SELECT file_xbrl_file_id,
[FILE_XBRL_DATA].value('(/BizTaxDataContract/Elements/d2p1:XbrlElementDataContract[d2p1:Name/text()="BalanceSheetTotalCorporationCodeCurrentTaxPeriod"]/d2p1:UnitRef)[1]','varchar(50)') as 'unitref',
[FILE_XBRL_DATA].value('(/BizTaxDataContract/Elements/d2p1:XbrlElementDataContract[d2p1:Name/text()="BalanceSheetTotalCorporationCodeCurrentTaxPeriod"]/d2p1:Namespace)[1]','varchar(50)') as 'Namespace',
[FILE_XBRL_DATA].value('(/BizTaxDataContract/Elements/d2p1:XbrlElementDataContract[d2p1:Name/text()="BalanceSheetTotalCorporationCodeCurrentTaxPeriod"]/d2p1:Value)[1]','varchar(50)') as 'Value',
[FILE_XBRL_DATA].value('(/BizTaxDataContract/Elements/d2p1:XbrlElementDataContract[d2p1:Name/text()="BalanceSheetTotalCorporationCodeCurrentTaxPeriod"]/d2p1:Calculated)[1]','varchar(50)') as 'Calculated'
FROM [eBook].[dbo].[TPBOOK_FILE_XBRLDATA]
WHERE [FILE_XBRL_DATA].exist('(/BizTaxDataContract/Elements/d2p1:XbrlElementDataContract[d2p1:Name/text()="BalanceSheetTotalCorporationCodeCurrentTaxPeriod"]/d2p1:UnitRef[.="EUR"])')=1
AND file_xbrl_file_id = '69d9a16e-2ae1-41e3-bb89-5155a09bc571'
GO
---------

--UPDATE BalanceSheetTotalCorporationCodeCurrentTaxPeriod Calculated value
WITH XMLNAMESPACES 
('http://schemas.datacontract.org/2004/07/EY.com.eBook.BizTax.Contracts' AS ns,
'http://www.w3.org/2001/XMLSchema-instance' as ns2,
'http://schemas.datacontract.org/2004/07/EY.com.eBook.BizTax' as d2p1,
DEFAULT 'http://schemas.datacontract.org/2004/07/EY.com.eBook.BizTax.Contracts' )
UPDATE [eBook].[dbo].[TPBOOK_FILE_XBRLDATA]
SET [FILE_XBRL_DATA].modify('replace value of (/BizTaxDataContract/Elements/d2p1:XbrlElementDataContract[d2p1:Name/text()="BalanceSheetTotalCorporationCodeCurrentTaxPeriod"]/d2p1:Calculated/text())[1] with ("false")')
WHERE [FILE_XBRL_DATA].exist('(/BizTaxDataContract/Elements/d2p1:XbrlElementDataContract[d2p1:Name/text()="BalanceSheetTotalCorporationCodeCurrentTaxPeriod"]/d2p1:UnitRef[.="EUR"])')=1 
AND file_xbrl_file_id = '69d9a16e-2ae1-41e3-bb89-5155a09bc571';





--FILES TO FIX

/*
9F5588E0-BB88-4A26-B757-1B7E40245787
79EC88B6-CCC9-49CF-ACB0-1B87D6DAF9DC
A19B6AA0-0CEC-4E18-AA8B-1E396B22BB82
189879EA-E56B-4A62-B843-22E6EB7F96EA
6035E9CC-FFA7-4209-9B2E-3B79517CC892
8A92FFE7-8B33-43F0-AD0B-43BED05318FF
BF7C3FBC-C757-4B5A-9908-4867DD30A333
226A205B-1067-4B41-AA73-643ABF7FBA54
4A5FF1B3-A467-412C-BAB0-654105C31B90
68A0AD4A-B308-4C21-ADFA-6ABA63129838
7D223612-AA55-4BDB-92CD-6F918F3881D0
696AB735-51BC-49CD-A29E-AC4FE70FB280
0B2ABE29-41FF-43DB-AB14-BB38A96F932B
1738B636-CAC9-4C1B-9E2C-C8381CDF86A6
2237DEF6-26F1-40E1-A7FA-C8A733E16777
B82DCDF3-DD67-41C7-9213-C99B848DBB18
C204F92E-FFC3-45FC-97E7-D4E88025D22A
62182D91-959E-4DD9-8C28-D787CB64BBC3
7DF68A50-D107-4B29-A8EC-DBFFF1C3BAD9
3AC88016-6060-4610-8ADB-DC1028ADFFD0
F0605A48-C4B3-4C7B-96E5-E1AE033DB4A9
201E18B3-C144-4DB7-BED8-ED0C7C672324
1FCCE8EB-523B-484F-B034-FB0BC9ED226B
*/