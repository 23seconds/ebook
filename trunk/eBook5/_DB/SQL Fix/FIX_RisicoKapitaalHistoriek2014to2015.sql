

  /****** Script for SelectTopNRows command from SSMS  ******/
SELECT TOP 1000 [FILE_ID]
      ,[FILE_CLIENT_ID]
      ,[FILE_NAME]
      ,[FILE_CULTURE]
      ,[FILE_START_DATE]
      ,[FILE_END_DATE]
      ,[FILE_PREV_START_DATE]
      ,[FILE_PREV_END_DATE]
      ,[FILE_CREATION_DATE]
      ,[FILE_CREATED_BY]
      ,[FILE_LAST_UPDATED_DATE]
      ,[FILE_LAST_UPDATED_BY]
      ,[FILE_CLOSE_DATE]
      ,[FILE_CLOSED_BY]
      ,[FILE_DELETED]
      ,[FILE_PREVIOUSFILE_ID]
      ,[FILE_ACCOUNT_LENGTH]
      ,[FILE_IMPORT_DATE]
      ,[FILE_IMPORT_TYPE]
      ,[FILE_MARK_DELETE]
      ,[FILE_NUMBERS_LOCKED]
      ,[FILE_SETTINGS]
      ,[FILE_TOMIGRATE]
  FROM [eBook].[dbo].[TPBOOK_FILES]
  where [FILE_PREVIOUSFILE_ID] = '7226fd60-7707-4df8-8415-6c5edebbf437'

--TESTFILE--429E05AD-CA32-4AE3-89C0-A7E66A1D57EA

--GET WORKSHEET OF PREVIOUS FILE
SELECT TOP 1000 [WORKSHEET_FILE_ID]
      ,[WORKSHEET_TYPE_ID]
      ,[WORKSHEET_DATA_LU]
      ,[WORKSHEET_DATA_LUB]
      ,[WORKSHEET_DATA]
      ,[WORKSHEET_CLOSED]
      ,[WORKSHEET_CLOSEDDATE]
      ,[WORKSHEET_CLOSEDBY]
  FROM [eBook].[dbo].[TPBOOK_FILE_WORKSHEETS]
  where worksheet_type_id = 'E4C15BB2-CDD4-4D9D-A7C9-7D1FD4080776'
  AND [WORKSHEET_FILE_ID] =
	( SELECT [FILE_PREVIOUSFILE_ID] FROM [eBook].[dbo].[TPBOOK_FILES]
  where [FILE_ID] = '429E05AD-CA32-4AE3-89C0-A7E66A1D57EA')

--GET WORKSHEET OF CURRENT FILE
SELECT TOP 1000 [WORKSHEET_FILE_ID]
      ,[WORKSHEET_TYPE_ID]
      ,[WORKSHEET_DATA_LU]
      ,[WORKSHEET_DATA_LUB]
      ,[WORKSHEET_DATA]
      ,[WORKSHEET_CLOSED]
      ,[WORKSHEET_CLOSEDDATE]
      ,[WORKSHEET_CLOSEDBY]
  FROM [eBook].[dbo].[TPBOOK_FILE_WORKSHEETS]
  where worksheet_type_id = 'E4C15BB2-CDD4-4D9D-A7C9-7D1FD4080776'
  AND [WORKSHEET_FILE_ID] = '429E05AD-CA32-4AE3-89C0-A7E66A1D57EA'

  --GET WORKSHEET OF CURRENT UNCHANGED FILE
SELECT TOP 1000 [WORKSHEET_FILE_ID]
      ,[WORKSHEET_TYPE_ID]
      ,[WORKSHEET_DATA_LU]
      ,[WORKSHEET_DATA_LUB]
      ,[WORKSHEET_DATA]
      ,[WORKSHEET_CLOSED]
      ,[WORKSHEET_CLOSEDDATE]
      ,[WORKSHEET_CLOSEDBY]
  FROM [eBook].[dbo].[TPBOOK_FILE_WORKSHEETS]
  where worksheet_type_id = 'E4C15BB2-CDD4-4D9D-A7C9-7D1FD4080776'
  AND [WORKSHEET_FILE_ID] = 'DEC32853-F8AE-4291-8B5C-5B0E7304EBF4'


drop table #temp

--declare counter vars
declare @recCount int,
@i int;
--initialize iterator
SET @i = 1;

--Retrieve all 2014 risicokapitaalhistoriek worksheets
--WHERE a 2007 historyItem exists
--AND beperktOverdraagbaarVolgendAJ - gebruiktHuidigAJ > 0
--AND has a link with a 2015 campaign
--WHERE risicokapitaalhistoriek does not contain a 2007 historyItem
WITH XMLNAMESPACES 
('http://www.w3.org/2001/XMLSchema-instance' as ns2,
DEFAULT 'AY2014.RisicoKapitaalHistoriekApp' ), CTEQuery as
(
	SELECT
		CLI.CLIENT_NAME as 'clientName',
		FI.FILE_PREVIOUSFILE_ID as '2014file_id',
		FI.FILE_ID as '2015file_id', 
		FI.FILE_START_DATE '2015startdate',
		FI.FILE_END_DATE as '2015enddate',
		FIWO.[WORKSHEET_DATA].query('declare default element namespace"AY2014.RisicoKapitaalHistoriekApp";(/WPRisicokapitaalHistoriek/Historiek/HistoryItem[BerekendInAJString/text()="2007"])[1]') as 'historyItem',
		FIWO.[WORKSHEET_DATA].value('(/WPRisicokapitaalHistoriek/Historiek/HistoryItem[BerekendInAJString/text()="2007"]/BeperktOverdraagbaarVolgendAJ)[1]','decimal(20,2)') as 'BeperktOverdraagbaarSaldo',
		FIWO.[WORKSHEET_DATA].value('(/WPRisicokapitaalHistoriek/Historiek/HistoryItem[BerekendInAJString/text()="2007"]/GebruiktHuidigAJ)[1]','decimal(20,2)') as 'GebruiktHuidigAJ'
	FROM 
	--Get files that
	[eBook].[dbo].[TPBOOK_FILES] FI
	--have a risicokapitaalhistoriek worksheet in previous linked year
	INNER JOIN  [eBook].[dbo].[TPBOOK_FILE_WORKSHEETS] FIWO ON FIWO.[WORKSHEET_FILE_ID] = FI.[FILE_PREVIOUSFILE_ID] AND FIWO.worksheet_type_id = 'E4C15BB2-CDD4-4D9D-A7C9-7D1FD4080776'
	--have client info
	INNER JOIN [eBook].[dbo].[TPBOOK_CLIENTS] CLI ON FI.FILE_CLIENT_ID = CLI.CLIENT_ID
	--are 2015
	where datediff(day,FI.file_end_date,'2014-12-31 00:00:00.000') <= 0
	AND datediff(day,FI.file_end_date,'2015-12-31 00:00:00.000') > 0
	--are not deleted
	AND FI.FILE_DELETED = 0
	--have a worksheet historyItem of 2007
	AND [WORKSHEET_DATA].exist('(/WPRisicokapitaalHistoriek/Historiek/HistoryItem[BerekendInAJString/text()="2007"])')=1
	--have historyItem values BeperktOverdraagbaarVolgendAJ - GebruiktHuidigAJ > 0
	AND FIWO.[WORKSHEET_DATA].value('(/WPRisicokapitaalHistoriek/Historiek/HistoryItem[BerekendInAJString/text()="2007"]/BeperktOverdraagbaarVolgendAJ)[1]','money') -
	FIWO.[WORKSHEET_DATA].value('(/WPRisicokapitaalHistoriek/Historiek/HistoryItem[BerekendInAJString/text()="2007"]/GebruiktHuidigAJ)[1]','money') > 0
	--don't have a worksheet historyItem of 2007 in 2015
	AND FI.FILE_ID NOT IN (SELECT [WORKSHEET_FILE_ID]
							FROM [eBook].[dbo].[TPBOOK_FILES] FI2
							INNER JOIN  [eBook].[dbo].[TPBOOK_FILE_WORKSHEETS] FIWO2 ON FIWO2.[WORKSHEET_FILE_ID] = FI2.[FILE_PREVIOUSFILE_ID] AND FIWO2.worksheet_type_id = 'E4C15BB2-CDD4-4D9D-A7C9-7D1FD4080776'
							AND datediff(day,FI2.file_end_date,'2014-12-31 00:00:00.000') <= 0
							AND datediff(day,FI2.file_end_date,'2015-12-31 00:00:00.000') > 0
							AND [WORKSHEET_DATA].exist('(/WPRisicokapitaalHistoriek/Historiek/HistoryItem[BerekendInAJString/text()="2007"])')=0
							AND FI2.FILE_ID = FI.FILE_ID)
)

--create virtual temp table
select IDENTITY(int) as recordId,* into #temp FROM CTEQuery

--Get Count
SELECT @recCount = count([2015file_id]) FROM #temp


--Loop based on count
WHILE(@i <= @recCount)
BEGIN
	--Declare data holders for each record
	declare @fileId2014 uniqueidentifier,
		@fileId2015 uniqueidentifier,
		@historyItem xml,
		@BeperktOverdraagbaarSaldo decimal(20,2),
		@GebruiktHuidigAJ decimal(20,2),
		@difference decimal(20,2),
		@result XML;

	--Get data
	/*WITH XMLNAMESPACES 
	(DEFAULT 'AY2014.RisicoKapitaalHistoriekApp' )*/
	SELECT 
		@fileId2014 = [2014file_id], 
		@fileId2015 = [2015file_id],
		@historyItem = [historyItem],
		@BeperktOverdraagbaarSaldo = [BeperktOverdraagbaarSaldo],
		@GebruiktHuidigAJ = [GebruiktHuidigAJ]  
	FROM #temp WHERE recordId = @i;
	--Print data
	--Print cast(@i as varchar(5)) + cast(@fileId2014 as varchar(50)) + ' - ' + cast(@fileId2015 as varchar(50)) + ' - ' + cast(@historyItem as varchar(max)) + ' - ' + cast(@BeperktOverdraagbaarSaldo as varchar(50)) + ' - ' + cast(@GebruiktHuidigAJ as varchar(50))

	--Add a "vervallenHuidigAJ" to each 2015 historyItem (not needed? TBD)

	--Calculation
	SET @difference =  @BeperktOverdraagbaarSaldo - @GebruiktHuidigAJ

	--Add the "vervallenHuidigAJ" = @BeperktOverdraagbaarSaldo - @GebruiktHuidigAJ
	SET @historyItem.modify('declare default element namespace"AY2014.RisicoKapitaalHistoriekApp";insert <VervallenHuidigAJ>{ xs:string(sql:variable("@difference")) }</VervallenHuidigAJ> into (/HistoryItem)[1]')
	--Set BeperktOverdraagbaarSaldo to 0
	--SET @historyItem.modify('declare default element namespace"AY2014.RisicoKapitaalHistoriekApp";replace value of (/HistoryItem/BeperktOverdraagbaarSaldo/text())[1] with ("0")')
	--Set GebruiktHuidigAJ to 0
	SET @historyItem.modify('declare default element namespace"AY2014.RisicoKapitaalHistoriekApp";replace value of (/HistoryItem/BeperktOverdraagbaarVolgendAJ/text())[1] with ("0")')
	--Set BeperktOverdraagbaarSaldo to 0
	--SET @historyItem.modify('declare default element namespace"AY2014.RisicoKapitaalHistoriekApp";replace value of (/HistoryItem/OnbeperktOverdraagbaarSaldo/text())[1] with ("0")')
	--Set GebruiktHuidigAJ to 0
	SET @historyItem.modify('declare default element namespace"AY2014.RisicoKapitaalHistoriekApp";replace value of (/HistoryItem/BeperktOverdraagbaarVolgendAJ/text())[1] with ("0")')
	--Set GebruiktHuidigAJ to 0
	--SET @historyItem.modify('declare default element namespace"AY2014.RisicoKapitaalHistoriekApp";replace value of (/HistoryItem/OnbeperktOverdraagbaarVolgendAJ/text())[1] with ("0")')
	-- Replace namespace
	set @historyItem = cast(replace(cast(@historyItem as varchar(max)),'AY2014.RisicoKapitaalHistoriekApp','AY2015.RisicoKapitaalHistoriekApp') as xml)

	--END

	--Insert the altered 2007 historyItem into the 2015 worksheet (notice the 2015 namespace)
	PRINT CAST(@historyItem as varchar(max))
	UPDATE [eBook].[dbo].[TPBOOK_FILE_WORKSHEETS]
	SET WORKSHEET_DATA.modify('declare default element namespace"AY2015.RisicoKapitaalHistoriekApp"; insert sql:variable("@historyItem") as first into (/WPRisicokapitaalHistoriek/Historiek)[1]')
	WHERE [WORKSHEET_TYPE_ID] = 'E4C15BB2-CDD4-4D9D-A7C9-7D1FD4080776'
	AND [WORKSHEET_FILE_ID] = @fileId2015


	--<SaldoVervallendHuidigAJ>13765.54</SaldoVervallendHuidigAJ>
	--	SET @historyItem.modify('declare default element namespace"AY2014.RisicoKapitaalHistoriekApp";insert <VervallenHuidigAJ>{ xs:string(sql:variable("@difference")) }</VervallenHuidigAJ> into (/HistoryItem)[1]')
	-- * double trouble *
	--PRINT CAST(@historyItem as varchar(max))
	--UPDATE [eBook].[dbo].[TPBOOK_FILE_WORKSHEETS]
	--SET WORKSHEET_DATA.modify('declare default element namespace"AY2015.RisicoKapitaalHistoriekApp"; insert sql:variable("@historyItem") as first into (/WPRisicokapitaalHistoriek/Historiek)[1]')
	--WHERE [WORKSHEET_TYPE_ID] = 'E4C15BB2-CDD4-4D9D-A7C9-7D1FD4080776'
	--AND [WORKSHEET_FILE_ID] = @fileId2015

	--Print 2015 WS
	SELECT @result = WORKSHEET_DATA FROM [eBook].[dbo].[TPBOOK_FILE_WORKSHEETS]
	WHERE [WORKSHEET_TYPE_ID] = 'E4C15BB2-CDD4-4D9D-A7C9-7D1FD4080776'
	AND [WORKSHEET_FILE_ID] = @fileId2015
	PRINT CAST(@result as varchar(max))
	--+1
	SET @i = @i + 1
END
--drop temp
drop table #temp


