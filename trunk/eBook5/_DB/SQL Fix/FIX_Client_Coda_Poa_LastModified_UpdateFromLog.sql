/****** Script for SelectTopNRows command from SSMS  ******/
SELECT 
		(select client_name from dbo.tpbook_clients cli where cli.client_id = ccp.clientId),
		[BancAccountNr]
      ,[BIC]
      ,[ClientId]
      ,[StatusId]
      ,[LastModified]
	  ,(select top 1 ccpl.timestamp from [eBook].[dbo].[TPBOOK_CLIENT_CODA_POA_LOG] ccpl where ccpl.BankAccountNr = ccp.[BancAccountNr] order by timestamp desc)
      ,[RepositoryItemId]
      ,[DocStoreId]
  FROM [eBook].[dbo].[TPBOOK_CLIENT_CODA_POA] ccp
  WHERE [LastModified] != (select top 1 timestamp from [eBook].[dbo].[TPBOOK_CLIENT_CODA_POA_LOG] where BankAccountNr = ccp.[BancAccountNr] order by timestamp desc)

  --3571 of 3952

  begin tran
  UPDATE ccp
  set [LastModified] = (select top 1 ccpl.timestamp from [eBook].[dbo].[TPBOOK_CLIENT_CODA_POA_LOG] ccpl where ccpl.BankAccountNr = ccp.[BancAccountNr] order by timestamp desc)
  from [eBook].[dbo].[TPBOOK_CLIENT_CODA_POA] as ccp
   WHERE [LastModified] != (select top 1 timestamp from [eBook].[dbo].[TPBOOK_CLIENT_CODA_POA_LOG] where BankAccountNr = ccp.[BancAccountNr] order by timestamp desc)
  rollback tran


  /****** Script for SelectTopNRows command from SSMS  ******/
/****** Script for SelectTopNRows command from SSMS  ******/
SELECT 
		(select client_name from dbo.tpbook_clients cli where cli.client_id = ccp.clientId),
		[BancAccountNr]
      ,[BIC]
      ,[ClientId]
      ,[StatusId]
      ,[LastModified]
	  ,(select top 1 ccpl.timestamp from [eBook].[dbo].[TPBOOK_CLIENT_CODA_POA_LOG] ccpl where ccpl.BankAccountNr = ccp.[BancAccountNr] order by timestamp desc)
      ,[RepositoryItemId]
      ,[DocStoreId]
  FROM [eBook].[dbo].[TPBOOK_CLIENT_CODA_POA] ccp
  WHERE [BancAccountNr] not in (
  SELECT 
		[BancAccountNr]
  FROM [eBook].[dbo].[TPBOOK_CLIENT_CODA_POA] ccp
  WHERE [LastModified] != (select top 1 timestamp from [eBook].[dbo].[TPBOOK_CLIENT_CODA_POA_LOG] where BankAccountNr = ccp.[BancAccountNr] order by timestamp desc)

  )


  select * from [eBook].[dbo].[TPBOOK_CLIENT_CODA_POA_LOG] where bankaccountnr = 'BE02375057759640'

  select count(*) from   [eBook].[dbo].[TPBOOK_CLIENT_CODA_POA]