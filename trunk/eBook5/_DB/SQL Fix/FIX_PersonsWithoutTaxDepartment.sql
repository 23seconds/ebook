/****** Script for SelectTopNRows command from SSMS  ******/
SELECT TOP 1000 [Person_ID]
      ,[Person_GPN]
      ,[Person_LastName]
      ,[Person_FirstName]
      ,[Person_Email]
      ,[Person_DefaultOffice]
      ,[Person_WindowsAccount]
      ,[Person_LastLogon]
      ,[Person_DataUpdated]
      ,[Person_EndedAt]
      ,[Person_Manual]
      ,[Person_Department]
	   , [PMT].[dbo].[TPMT_PERSONS].*
  FROM [eBook].[dbo].[TPBOOK_PERSONS]
  INNER JOIN [PMT].[dbo].[TPMT_PERSONS] ON [PersonGPN] = PERSON_GPN
  WHERE person_lastname = 'joosten'
   and person_department is null
  and personwindowsaccount is not null

  begin tran
  UPDATE t
  SET t.[Person_Department] = 'TAX'
  FROM [eBook].[dbo].[TPBOOK_PERSONS] as t
  INNER JOIN [PMT].[dbo].[TPMT_PERSONS] ON [PersonGPN] = PERSON_GPN
  WHERE  person_department is null
  and personwindowsaccount is not null
  rollback tran