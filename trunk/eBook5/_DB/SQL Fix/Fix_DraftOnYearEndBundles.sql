/****** Script for SelectTopNRows command from SSMS  ******/
SELECT [File_Bundle_ID]
      ,[File_Bundle_File_ID]
      ,[File_Bundle_Template_ID]
	  ,[FileServices_Status]
	  ,[File_Bundle_Draft]
      ,[File_Bundle_HeaderFooterType]
      ,[File_Bundle_Name]
      ,[File_Bundle_Culture]
      ,[File_Bundle_Draft]
      ,[File_Bundle_RectoVerso]
      ,[File_Bundle_Contents]
      ,[File_Bundle_Deleted]
      ,[File_Bundle_Locked]
  FROM [eBook].[dbo].[TPBOOK_FILE_BUNDLES]
  INNER JOIN [eBook].[dbo].[TPBOOK_FILE_SERVICES] ON [File_Bundle_File_ID] = [FileService_FileId] AND [FileServices_ServiceId] = 'C3B8C8DB-3822-4263-9098-541FAE897D02'
  WHERE FILE_BUNDLE_DRAFT = 1
  AND fileservices_status in (3,5,7,8,10,11)

  begin tran
  UPDATE bundles
  SET [File_Bundle_Draft] = 0
  FROM [eBook].[dbo].[TPBOOK_FILE_BUNDLES] AS bundles
  INNER JOIN [eBook].[dbo].[TPBOOK_FILE_SERVICES] ON bundles.[File_Bundle_File_ID] = [FileService_FileId] AND [FileServices_ServiceId] = 'C3B8C8DB-3822-4263-9098-541FAE897D02'
  WHERE bundles.FILE_BUNDLE_DRAFT = 1
  AND fileservices_status in (3,5,7,8,10,11)
  rollback tran