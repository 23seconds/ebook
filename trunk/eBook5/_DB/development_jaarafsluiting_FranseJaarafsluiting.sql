/****** Script for SelectTopNRows command from SSMS  ******/
SELECT TOP 1000 [Bundle_Template_ID]
      ,[Bundle_Template_Culture]
      ,[Bundle_Template_Name]
      ,[Bundle_Template_Content]
      ,[Bundle_Template_Type]
  FROM [eBook].[dbo].[TPBOOK_BUNDLE_TEMPLATES]
  -- WHERE [Bundle_Template_Content].value('(/ArrayOfIndexItemBaseDataContract)[1]','varchar(max)') like '%vergaderstukken%'µ


   begin tran
   update [eBook].[dbo].[TPBOOK_BUNDLE_TEMPLATES]
   set [Bundle_Template_Name]= 'Clôture annuelle' , bundle_template_content = '<ArrayOfIndexItemBaseDataContract xmlns:i="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://schemas.datacontract.org/2004/07/EY.com.eBook.API.Contracts.Data">
  <IndexItemBaseDataContract i:type="CoverpageDataContract">
    <EndsAt>0</EndsAt>
    <FooterConfig>
      <Enabled>false</Enabled>
      <FootNote i:nil="true" />
      <ShowFooterNote>false</ShowFooterNote>
      <ShowPageNr>false</ShowPageNr>
      <Style i:nil="true" />
    </FooterConfig>
    <HeaderConfig>
      <Enabled>false</Enabled>
      <ShowTitle>false</ShowTitle>
      <Style i:nil="true" />
    </HeaderConfig>
    <NoDraft>false</NoDraft>
    <StartsAt>0</StartsAt>
    <iTextTemplate i:nil="true" />
    <iconCls i:nil="true" />
    <id>00000000-0000-0000-0000-000000000000</id>
    <indexed>false</indexed>
    <locked>false</locked>
    <title>page de garde</title>
    <hidden>false</hidden>
    <type>COVERPAGE</type>
  </IndexItemBaseDataContract>
  <IndexItemBaseDataContract i:type="ChapterDataContract">
    <EndsAt>0</EndsAt>
    <FooterConfig>
      <Enabled>false</Enabled>
      <FootNote i:nil="true" />
      <ShowFooterNote>false</ShowFooterNote>
      <ShowPageNr>false</ShowPageNr>
      <Style i:nil="true" />
    </FooterConfig>
    <HeaderConfig>
      <Enabled>false</Enabled>
      <ShowTitle>false</ShowTitle>
      <Style i:nil="true" />
    </HeaderConfig>
    <NoDraft>false</NoDraft>
    <StartsAt>0</StartsAt>
    <iTextTemplate i:nil="true" />
    <iconCls i:nil="true" />
    <id>00000000-0000-0000-0000-000000000000</id>
    <indexed>false</indexed>
    <locked>false</locked>
    <title>Comptes annuels</title>
    <items />
    <nocoverpage>false</nocoverpage>
    <signedrepo>true</signedrepo>
    <type>CHAPTER</type>
  </IndexItemBaseDataContract>
  <IndexItemBaseDataContract i:type="ChapterDataContract">
    <EndsAt>0</EndsAt>
    <FooterConfig>
      <Enabled>false</Enabled>
      <FootNote i:nil="true" />
      <ShowFooterNote>false</ShowFooterNote>
      <ShowPageNr>false</ShowPageNr>
      <Style i:nil="true" />
    </FooterConfig>
    <HeaderConfig>
      <Enabled>false</Enabled>
      <ShowTitle>false</ShowTitle>
      <Style i:nil="true" />
    </HeaderConfig>
    <NoDraft>false</NoDraft>
    <StartsAt>0</StartsAt>
    <iTextTemplate i:nil="true" />
    <iconCls i:nil="true" />
    <id>00000000-0000-0000-0000-000000000000</id>
    <indexed>false</indexed>
    <locked>false</locked>
    <title>Procès-verbaux</title>
    <items />
    <nocoverpage>false</nocoverpage>
    <signedrepo>true</signedrepo>
    <type>CHAPTER</type>
  </IndexItemBaseDataContract>
  <IndexItemBaseDataContract i:type="ChapterDataContract">
    <EndsAt>0</EndsAt>
    <FooterConfig>
      <Enabled>false</Enabled>
      <FootNote i:nil="true" />
      <ShowFooterNote>false</ShowFooterNote>
      <ShowPageNr>false</ShowPageNr>
      <Style i:nil="true" />
    </FooterConfig>
    <HeaderConfig>
      <Enabled>false</Enabled>
      <ShowTitle>false</ShowTitle>
      <Style i:nil="true" />
    </HeaderConfig>
    <NoDraft>false</NoDraft>
    <StartsAt>0</StartsAt>
    <iTextTemplate i:nil="true" />
    <iconCls i:nil="true" />
    <id>00000000-0000-0000-0000-000000000000</id>
    <indexed>false</indexed>
    <locked>false</locked>
    <title>Balance des comptes généraux</title>
    <items>
      <IndexItemBaseDataContract i:type="StatementsDataContract">
        <EndsAt>0</EndsAt>
        <FooterConfig>
          <Enabled>false</Enabled>
          <FootNote i:nil="true" />
          <ShowFooterNote>true</ShowFooterNote>
          <ShowPageNr>false</ShowPageNr>
          <Style i:nil="true" />
        </FooterConfig>
        <HeaderConfig>
          <Enabled>true</Enabled>
          <ShowTitle>true</ShowTitle>
          <Style i:nil="true" />
        </HeaderConfig>
        <NoDraft>false</NoDraft>
        <StartsAt>0</StartsAt>
        <iTextTemplate i:nil="true" />
        <iconCls i:nil="true" />
        <id>00000000-0000-0000-0000-000000000000</id>
        <indexed>false</indexed>
        <locked>false</locked>
        <title>Actif</title>
		<Comparable>false</Comparable>
        <Culture>fr-FR</Culture>
        <Detailed>true</Detailed>
        <FileId>00000000-0000-0000-0000-000000000000</FileId>
        <layout>ACTIVA</layout>
        <type>STATEMENT</type>
      </IndexItemBaseDataContract>
      <IndexItemBaseDataContract i:type="StatementsDataContract">
        <EndsAt>0</EndsAt>
        <FooterConfig>
          <Enabled>false</Enabled>
          <FootNote i:nil="true" />
          <ShowFooterNote>false</ShowFooterNote>
          <ShowPageNr>false</ShowPageNr>
          <Style i:nil="true" />
        </FooterConfig>
        <HeaderConfig>
          <Enabled>true</Enabled>
          <ShowTitle>true</ShowTitle>
          <Style i:nil="true" />
        </HeaderConfig>
        <NoDraft>false</NoDraft>
        <StartsAt>0</StartsAt>
        <iTextTemplate i:nil="true" />
        <iconCls i:nil="true" />
        <id>00000000-0000-0000-0000-000000000000</id>
        <indexed>false</indexed>
        <locked>false</locked>
        <title>Passif</title>
		<Comparable>false</Comparable>
        <Culture>fr-FR</Culture>
        <Detailed>true</Detailed>
        <FileId>00000000-0000-0000-0000-000000000000</FileId>
        <layout>PASSIVA</layout>
        <type>STATEMENT</type>
      </IndexItemBaseDataContract>
      <IndexItemBaseDataContract i:type="StatementsDataContract">
        <EndsAt>0</EndsAt>
        <FooterConfig>
          <Enabled>false</Enabled>
          <FootNote i:nil="true" />
          <ShowFooterNote>false</ShowFooterNote>
          <ShowPageNr>false</ShowPageNr>
          <Style i:nil="true" />
        </FooterConfig>
        <HeaderConfig>
          <Enabled>true</Enabled>
          <ShowTitle>true</ShowTitle>
          <Style i:nil="true" />
        </HeaderConfig>
        <NoDraft>false</NoDraft>
        <StartsAt>0</StartsAt>
        <iTextTemplate i:nil="true" />
        <iconCls i:nil="true" />
        <id>00000000-0000-0000-0000-000000000000</id>
        <indexed>false</indexed>
        <locked>false</locked>
        <title>Compte de résultats</title>
        <Comparable>false</Comparable>
        <Culture>fr-FR</Culture>
        <Detailed>true</Detailed>
        <FileId>00000000-0000-0000-0000-000000000000</FileId>
        <layout>RESULTATENREKENING</layout>
        <type>STATEMENT</type>
      </IndexItemBaseDataContract>
      <IndexItemBaseDataContract i:type="StatementsDataContract">
        <EndsAt>0</EndsAt>
        <FooterConfig>
          <Enabled>false</Enabled>
          <FootNote i:nil="true" />
          <ShowFooterNote>false</ShowFooterNote>
          <ShowPageNr>false</ShowPageNr>
          <Style i:nil="true" />
        </FooterConfig>
        <HeaderConfig>
          <Enabled>true</Enabled>
          <ShowTitle>true</ShowTitle>
          <Style i:nil="true" />
        </HeaderConfig>
        <NoDraft>false</NoDraft>
        <StartsAt>0</StartsAt>
        <iTextTemplate i:nil="true" />
        <iconCls i:nil="true" />
        <id>00000000-0000-0000-0000-000000000000</id>
        <indexed>false</indexed>
        <locked>false</locked>
        <title>Affectations et prélévements</title>
		<Comparable>false</Comparable>
        <Culture>fr-FR</Culture>
        <Detailed>false</Detailed>
        <FileId>00000000-0000-0000-0000-000000000000</FileId>
        <layout>RESULTAATVERWERKING</layout>
        <type>STATEMENT</type>
      </IndexItemBaseDataContract>
    </items>
    <nocoverpage>false</nocoverpage>
    <type>CHAPTER</type>
  </IndexItemBaseDataContract>
  <IndexItemBaseDataContract i:type="ChapterDataContract">
    <EndsAt>0</EndsAt>
    <FooterConfig>
      <Enabled>false</Enabled>
      <FootNote i:nil="true" />
      <ShowFooterNote>false</ShowFooterNote>
      <ShowPageNr>false</ShowPageNr>
      <Style i:nil="true" />
    </FooterConfig>
    <HeaderConfig>
      <Enabled>false</Enabled>
      <ShowTitle>false</ShowTitle>
      <Style i:nil="true" />
    </HeaderConfig>
    <NoDraft>false</NoDraft>
    <StartsAt>0</StartsAt>
    <iTextTemplate i:nil="true" />
    <iconCls i:nil="true" />
    <id>00000000-0000-0000-0000-000000000000</id>
    <indexed>false</indexed>
    <locked>false</locked>
    <title>Autres annexes</title>
    <items />
    <nocoverpage>false</nocoverpage>
    <type>CHAPTER</type>
  </IndexItemBaseDataContract>
</ArrayOfIndexItemBaseDataContract>'
where bundle_template_id ='43C27DBE-AE64-4208-BD57-060553D41EC8'
rollback tran