USE [eBook]
GO
/****** Object:  StoredProcedure [dbo].[usp_getClientPersonFileServiceReport]    Script Date: 12/06/2015 13:09:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[usp_getClientPersonFileServiceReport]
	@personId varchar(MAX),
	@statusId varchar(3),
	@enddate datetime,
	@dateModified datetime,
	@clientName varchar(100),
	@officeName varchar(50),
	@rowsFrom int,
	@limit int,
	@service varchar(50),
	@department varchar(3),
	@count int OUTPUT
	AS
BEGIN

declare  @rowsTo int;
	 

if @rowsFrom is not null
BEGIN
		set @rowsTo = @rowsFrom + @limit;
END;

DECLARE @cnt int;

with fileServiceReport as (
	select *,ROW_NUMBER() OVER (ORDER BY clientName) AS RN
	from (
		SELECT
		FI.FILE_ID AS 'fileId',
		O.OFFICE_NAME AS 'officeName',
		CL.CLIENT_NAME AS 'clientName',
		CL.CLIENT_ID AS 'clientId',
		CL.CLIENT_GFIS_CODE AS 'clientGfis',
		MG.PERSON_FIRSTNAME + ' ' + MG.PERSON_LASTNAME AS 'pmanager',
		PT.PERSON_FIRSTNAME + ' ' + PT.PERSON_LASTNAME AS 'ppartner',
		FI.FILE_END_DATE AS 'endDate',
		(SELECT [ClientAnualMeetingNext]FROM [PMT].[dbo].[TPMT_CLIENTS] WHERE [ClientId] = CL.CLIENT_ID) as 'nextAnnualMeeting',
		SS.ServiceStatuses_Status AS 'status',
		FSL.FILESERVICELOG_PERSONFIRSTNAME + ' ' + FSL.FILESERVICELOG_PERSONLASTNAME AS 'person',
		FSL.FILESERVICELOG_TIMESTAMP as 'dateModified',
		DATEDIFF(day,FSL.FILESERVICELOG_TIMESTAMP,getdate()) as 'daysOnStatus'

		FROM TPBOOK_CLIENTS AS CL --All clients
		INNER JOIN TPBOOK_FILES AS FI ON FI.FILE_CLIENT_ID= CL.CLIENT_ID AND FI.FILE_CLOSE_DATE is NULL AND FI.FILE_DELETED = 0 --Open files
		--INNER JOIN algemene vergadering
		INNER JOIN TPBOOK_FILE_SERVICES AS FS ON FS.FILESERVICE_FILEID = FI.FILE_ID --Files linked to year end service
		INNER JOIN TPBOOK_SERVICE_STATUSES AS SS ON FS.FILESERVICES_STATUS = SS.SERVICESTATUSES_STATUSID AND FS.FILESERVICES_SERVICEID = SS.SERVICESTATUSES_SERVICEID --Status name
		INNER JOIN  TPBOOK_CLIENT_DEPARTMENTS AS CD ON CD.CLIENTDEPARTMENT_CLIENTID = CL.CLIENT_ID --department
		LEFT OUTER JOIN VWPCRGFISMANAGER MG ON MG.PCR_CLIENT_ID = CL.CLIENT_ID  --manager
		LEFT OUTER JOIN VWPCRGFISPARTNER PT ON PT.PCR_CLIENT_ID = cl.client_id --partner
		LEFT OUTER JOIN eBook..TPBOOK_OFFICE O ON O.OFFICE_ID = (
																	SELECT TOP 1 CLIENTOFFICE_OFFICE_ID 
																	FROM TPBOOK_CLIENT_OFFICES_NEW 
																	WHERE CLIENTOFFICE_CLIENT_ID = CL.CLIENT_ID 
																	AND CLIENTOFFICE_DEPARTMENT_ID = @department 
																	AND CLIENTOFFICE_OFFICE_ID <> '00176'
																) --office
		CROSS APPLY
		(
			SELECT TOP 1 FILESERVICELOG_PERSONFIRSTNAME,FILESERVICELOG_PERSONLASTNAME,FILESERVICELOG_TIMESTAMP
			FROM TPBOOK_FILE_SERVICES_LOG
			WHERE FILESERVICELOG_FILEID = FI.FILE_ID 
			AND FILESERVICELOG_SERVICEID = FS.FILESERVICES_SERVICEID
			AND FILESERVICELOG_STATUSID = FS.FILESERVICES_STATUS
			ORDER BY FILESERVICELOG_TIMESTAMP DESC
		)  AS FSL --Person and timestamp

		WHERE cl.CLIENT_NAME like '%' + @clientName + '%'
		AND cl.CLIENT_ACTIVE_FLAG =  'Y'
		AND (@personId is null or CL.CLIENT_ID in (select PCR_CLIENT_ID from TPBOOK_PERSON_CLIENT_ROLE where PCR_PERSON_ID = @personId))
		AND CAST(FS.FILESERVICES_STATUS AS VARCHAR(MAX)) like @statusId
		AND (DATEDIFF(DAY,FI.FILE_END_DATE,@enddate) = 0 OR @enddate is null)
		AND (DATEDIFF(DAY,FSL.FILESERVICELOG_TIMESTAMP,@dateModified) = 0 OR @dateModified is null)
		AND CD.CLIENTDEPARTMENT_DEPARTMENTID like @department
		AND isnull(MG.PERSON_DEPARTMENT,'ACR') = @department
		AND isnull(PT.PERSON_DEPARTMENT,'ACR') = @department
		AND CAST(FS.FILESERVICES_SERVICEID AS VARCHAR(50)) like @service
		AND O.OFFICE_NAME like '%' + @officeName + '%'
	) tab
)
		
select @count = count(*) from fileServiceReport;

with fileServiceReportRecords as (
		select *,ROW_NUMBER() OVER (ORDER BY clientName) AS RN
		from (
		SELECT
		FI.FILE_ID AS 'fileId',
		O.OFFICE_NAME AS 'officeName',
		CL.CLIENT_NAME AS 'clientName',
		CL.CLIENT_ID AS 'clientId',
		CL.CLIENT_GFIS_CODE AS 'clientGfis',
		MG.PERSON_FIRSTNAME + ' ' + MG.PERSON_LASTNAME AS 'pmanager',
		PT.PERSON_FIRSTNAME + ' ' + PT.PERSON_LASTNAME AS 'ppartner',
		FI.FILE_END_DATE AS 'endDate',
		(SELECT [ClientAnualMeetingNext]FROM [PMT].[dbo].[TPMT_CLIENTS] WHERE [ClientId] = CL.CLIENT_ID) as 'nextAnnualMeeting',
		SS.ServiceStatuses_Status AS 'status',
		FSL.FILESERVICELOG_PERSONFIRSTNAME + ' ' + FSL.FILESERVICELOG_PERSONLASTNAME AS 'person',
		FSL.FILESERVICELOG_TIMESTAMP as 'dateModified',
		DATEDIFF(day,FSL.FILESERVICELOG_TIMESTAMP,getdate()) as 'daysOnStatus'

		FROM TPBOOK_CLIENTS AS CL --All clients
		INNER JOIN TPBOOK_FILES AS FI ON FI.FILE_CLIENT_ID= CL.CLIENT_ID AND FI.FILE_CLOSE_DATE is NULL AND FI.FILE_DELETED = 0 --Open files
		--INNER JOIN algemene vergadering
		INNER JOIN TPBOOK_FILE_SERVICES AS FS ON FS.FILESERVICE_FILEID = FI.FILE_ID --Files linked to year end service
		INNER JOIN TPBOOK_SERVICE_STATUSES AS SS ON FS.FILESERVICES_STATUS = SS.SERVICESTATUSES_STATUSID AND FS.FILESERVICES_SERVICEID = SS.SERVICESTATUSES_SERVICEID --Status name
		INNER JOIN  TPBOOK_CLIENT_DEPARTMENTS AS CD ON CD.CLIENTDEPARTMENT_CLIENTID = CL.CLIENT_ID --department
		LEFT OUTER JOIN VWPCRGFISMANAGER MG ON MG.PCR_CLIENT_ID = CL.CLIENT_ID  --manager
		LEFT OUTER JOIN VWPCRGFISPARTNER PT ON PT.PCR_CLIENT_ID = cl.client_id --partner
		LEFT OUTER JOIN eBook..TPBOOK_OFFICE O ON O.OFFICE_ID = (
																	SELECT TOP 1 CLIENTOFFICE_OFFICE_ID 
																	FROM TPBOOK_CLIENT_OFFICES_NEW 
																	WHERE CLIENTOFFICE_CLIENT_ID = CL.CLIENT_ID 
																	AND CLIENTOFFICE_DEPARTMENT_ID = @department 
																	AND CLIENTOFFICE_OFFICE_ID <> '00176'
																) --office
		CROSS APPLY
		(
			SELECT TOP 1 FILESERVICELOG_PERSONFIRSTNAME,FILESERVICELOG_PERSONLASTNAME,FILESERVICELOG_TIMESTAMP
			FROM TPBOOK_FILE_SERVICES_LOG
			WHERE FILESERVICELOG_FILEID = FI.FILE_ID 
			AND FILESERVICELOG_SERVICEID = FS.FILESERVICES_SERVICEID
			AND FILESERVICELOG_STATUSID = FS.FILESERVICES_STATUS
			ORDER BY FILESERVICELOG_TIMESTAMP DESC
		)  AS FSL --Person and timestamp

		WHERE cl.CLIENT_NAME like '%' + @clientName + '%'
		AND cl.CLIENT_ACTIVE_FLAG =  'Y'
		AND (@personId is null or CL.CLIENT_ID in (select PCR_CLIENT_ID from TPBOOK_PERSON_CLIENT_ROLE where PCR_PERSON_ID = @personId))
		AND CAST(FS.FILESERVICES_STATUS AS VARCHAR(MAX)) like @statusId
		AND (DATEDIFF(DAY,FI.FILE_END_DATE,@enddate) = 0 OR @enddate is null)
		AND (DATEDIFF(DAY,FSL.FILESERVICELOG_TIMESTAMP,@dateModified) = 0 OR @dateModified is null)
		AND CD.CLIENTDEPARTMENT_DEPARTMENTID like @department
		AND isnull(MG.PERSON_DEPARTMENT,'ACR') = @department
		AND isnull(PT.PERSON_DEPARTMENT,'ACR') = @department
		AND CAST(FS.FILESERVICES_SERVICEID AS VARCHAR(50)) like @service
		AND O.OFFICE_NAME like '%' + @officeName + '%'
	) tab
)



select * from fileServiceReportRecords
WHERE ( @rowsFrom is null or (RN >= @rowsFrom AND RN <= @rowsTo));

END

