USE [eBook]
GO

/****** Object:  Table [dbo].[TPBOOK_FILE_ANNUALACCOUNTS]    Script Date: 1/03/2015 18:31:09 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[TPBOOK_FILE_ANNUALACCOUNTS](
	[FileAnnualAccounts_Id] [uniqueidentifier] NOT NULL,
	[FileAnnualAccounts_FileId] [uniqueidentifier] NOT NULL,
	[FileAnnualAccounts_RepositoryItemId] [uniqueidentifier] NOT NULL,
	[FileAnnualAccounts_DefinitiveGeneralAssembly] [datetime] NULL,
	[FileAnnualAccounts_CompanyType] [varchar](50) NULL,
	[FileAnnualAccounts_Model] [varchar](50) NULL,
	[FileAnnualAccounts_ChangesAfterGeneralAssembly] [bit] NULL,
	[FileAnnualAccounts_TimeLimit] [varchar](max) NULL,
	[FileAnnualAccounts_EngagementCode] [varchar](50) NULL,
	[FileAnnualAccounts_InvoiceLanguage] [varchar](10) NULL,
	[FileAnnualAccounts_InvoiceRechargeFilingExpense] [varchar](50) NULL
 CONSTRAINT [PK_TPBOOK_FILE_ANNUALACCOUNTS] PRIMARY KEY CLUSTERED 
(
	[FileAnnualAccounts_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

SET ANSI_PADDING OFF
GO



ALTER TABLE [dbo].[TPBOOK_FILE_ANNUALACCOUNTS]
ADD FOREIGN KEY (FileAnnualAccounts_FileId)
REFERENCES [PK_TPBOOK_FILES](FILE_ID)
