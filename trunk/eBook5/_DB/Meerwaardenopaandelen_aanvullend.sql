declare @cntKey int
declare @keyId uniqueidentifier

set @keyId='CE24C201-3F95-4621-BF40-A4C8130D3763'
select @cntKey = COUNT(*) from [TPBOOK_COEFFICIENTKEYS] where CoeffKey_ID=@keyId

IF @cntKey=0
BEGIN
	print 'insert coeff key'
	insert into [TPBOOK_COEFFICIENTKEYS]
	values(@keyId,'VENB_MEERWAARDENAANDELEN_AANVULLEND_PERCENT','<CoefficientKeySettingsDataContract xmlns:i="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://schemas.datacontract.org/2004/07/EY.com.eBook.API.Contracts.Data">
	  <Period>AY</Period>
	</CoefficientKeySettingsDataContract>')

	print 'insert coeff key translation nl only'
	insert into TPBOOK_NAMES
	values(@keyId,'nl-BE','VenB: Meerwaarden op aandelen, aanvullend tarief')

	print 'insert coeff '
	insert into TPBOOK_COEFFICIENTS
	values(NEWID(),'C3D2E7D0-5479-4294-9E29-759757DBC737','VENB_MEERWAARDENAANDELEN_AANVULLEND_PERCENT',0.412,2014,null,null,'Meerwaarden op aandelen aanvullend tarief grote ondernemingen 0.472%')

END

declare @id uniqueidentifier
declare @cnt int

set @id = '3410F9C9-ED85-4BB0-87E6-8D91962210DF'
select @cnt = COUNT(*) from [TPBOOK_LISTITEMS] where ListItem_ID = @id
print @cnt
if @cnt = 0
Begin
	print 'insert list item'
	insert into [TPBOOK_LISTITEMS]
	values ('A86784B2-87B9-4B19-9088-9254BD90F5F8',@id,null,null,null)

	print 'insert list item tranlsations en,fr,nl'
	insert into TPBOOK_NAMES
	values(@id,'en-US','Taxable at the rate of 0.412%')
	insert into TPBOOK_NAMES
	values(@id,'fr-FR','Imposable au taux de 0.412%')
	insert into TPBOOK_NAMES
	values(@id,'nl-BE','Belastbaar aan het tarief van 0.412%')
END

insert into [TPBOOK_WORKSHEETTYPE_TRANSLATIONS]
SELECT [WORKSHEETTYPE_ID]
      ,[WORKSHEETTYPE_ENTITYORCOLLECTION]
      ,'CapitalGainsShares0412'
      ,[WORKSHEETTYPE_CULTURE]
      ,[WORKSHEETTYPE_OUTPUTTYPE]
      ,'Meerwaarden op aandelen belastbaar tegen 0,412%'
  FROM [TPBOOK_WORKSHEETTYPE_TRANSLATIONS]
where WORKSHEETTYPE_ID='803D6A06-352E-4E28-ADE5-214DEAAC76CA'
and WORKSHEETTYPE_FIELDNAME='CapitalGainsShares25'
and WORKSHEETTYPE_CULTURE = 'nl-be'

insert into [TPBOOK_WORKSHEETTYPE_TRANSLATIONS]
SELECT [WORKSHEETTYPE_ID]
      ,[WORKSHEETTYPE_ENTITYORCOLLECTION]
      ,'CapitalGainsShares0412'
      ,[WORKSHEETTYPE_CULTURE]
      ,[WORKSHEETTYPE_OUTPUTTYPE]
      ,'Plus-values sur actions ou parts imposables � 0,412%'
  FROM [TPBOOK_WORKSHEETTYPE_TRANSLATIONS]
where WORKSHEETTYPE_ID='803D6A06-352E-4E28-ADE5-214DEAAC76CA'
and WORKSHEETTYPE_FIELDNAME='CapitalGainsShares25'
and WORKSHEETTYPE_CULTURE = 'fr-FR'

insert into [TPBOOK_WORKSHEETTYPE_TRANSLATIONS]
SELECT [WORKSHEETTYPE_ID]
      ,[WORKSHEETTYPE_ENTITYORCOLLECTION]
      ,'CapitalGainsShares0412'
      ,[WORKSHEETTYPE_CULTURE]
      ,[WORKSHEETTYPE_OUTPUTTYPE]
      ,'Capital gains shares taxable at 0,412%'
  FROM [TPBOOK_WORKSHEETTYPE_TRANSLATIONS]
where WORKSHEETTYPE_ID='803D6A06-352E-4E28-ADE5-214DEAAC76CA'
and WORKSHEETTYPE_FIELDNAME='CapitalGainsShares25'
and WORKSHEETTYPE_CULTURE = 'en-US'

insert into [TPBOOK_WORKSHEETTYPE_TRANSLATIONS]
SELECT [WORKSHEETTYPE_ID]
      ,[WORKSHEETTYPE_ENTITYORCOLLECTION]
      ,'CapitalGainsShares0412Perc'
      ,[WORKSHEETTYPE_CULTURE]
      ,[WORKSHEETTYPE_OUTPUTTYPE]
      ,'CapitalGainsShares0412Perc'
  FROM [TPBOOK_WORKSHEETTYPE_TRANSLATIONS]
where WORKSHEETTYPE_ID='803D6A06-352E-4E28-ADE5-214DEAAC76CA'
and WORKSHEETTYPE_FIELDNAME='CapitalGainsShares25Perc'

insert into [TPBOOK_WORKSHEETTYPE_TRANSLATIONS]
SELECT [WORKSHEETTYPE_ID]
      ,[WORKSHEETTYPE_ENTITYORCOLLECTION]
      ,'CapitalGainsShares0412Result'
      ,[WORKSHEETTYPE_CULTURE]
      ,[WORKSHEETTYPE_OUTPUTTYPE]
      ,WORKSHEETTYPE_TRANSLATION
  FROM [TPBOOK_WORKSHEETTYPE_TRANSLATIONS]
where WORKSHEETTYPE_ID='803D6A06-352E-4E28-ADE5-214DEAAC76CA'
and WORKSHEETTYPE_FIELDNAME='CapitalGainsShares0412'
