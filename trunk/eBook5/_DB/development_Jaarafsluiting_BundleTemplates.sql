/****** Script for SelectTopNRows command from SSMS  ******/
SELECT TOP 1000 [Bundle_Template_ID]
      ,[Bundle_Template_Culture]
      ,[Bundle_Template_Name]
      ,[Bundle_Template_Content]
      ,[Bundle_Template_Type]
  FROM [eBook].[dbo].[TPBOOK_BUNDLE_TEMPLATES]

  --UPDATE JAARASLUITING NAME
  /*
  begin tran
  update [eBook].[dbo].[TPBOOK_BUNDLE_TEMPLATES]
  SET bundle_template_name = 'Jaarafsluiting - proef- en saldibalans'
  WHERE bundle_template_name = 'jaarafsluiting - proef- en saldibalans'  and [Bundle_Template_Culture] = 'nl-BE'
  rollback tran
  */
  /*
    begin tran
  update [eBook].[dbo].[TPBOOK_BUNDLE_TEMPLATES]
  SET bundle_template_name = 'Jaarafsluiting - bundel ter ondertekening'
  WHERE bundle_template_name = 'Jaarafsluiting - bundel'  and [Bundle_Template_Culture] = 'nl-BE'
  rollback tran
  */

SELECT * FROM [eBook].[dbo].[TPBOOK_BUNDLE_TEMPLATES]
WHERE bundle_template_name = 'Jaarafsluiting'  and [Bundle_Template_Culture] = 'nl-BE'

/*
begin tran
DELETE FROM [eBook].[dbo].[TPBOOK_BUNDLE_TEMPLATES] WHERE Bundle_Template_ID = 'A777ED40-3E39-4B96-A828-028D86E6CCEF'
rollback tran
*/

--INSERT PROEF EN SALIDBALANS BUNDLE
begin tran
insert into [eBook].[dbo].[TPBOOK_BUNDLE_TEMPLATES]
values('A777ED40-3E39-4B96-A828-028D86E6CCEF','nl-BE','jaarafsluiting - proef- en saldibalans','<ArrayOfIndexItemBaseDataContract xmlns:i="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://schemas.datacontract.org/2004/07/EY.com.eBook.API.Contracts.Data">
	<IndexItemBaseDataContract i:type="CoverpageDataContract">
		<EndsAt>0</EndsAt>
		<FooterConfig>
			<Enabled>false</Enabled>
			<FootNote i:nil="true" />
			<ShowFooterNote>false</ShowFooterNote>
			<ShowPageNr>false</ShowPageNr>
			<Style i:nil="true" />
		</FooterConfig>
		<HeaderConfig>
			<Enabled>false</Enabled>
			<ShowTitle>false</ShowTitle>
			<Style i:nil="true" />
		</HeaderConfig>
		<NoDraft>false</NoDraft>
		<StartsAt>0</StartsAt>
		<iTextTemplate i:nil="true" />
		<iconCls i:nil="true" />
		<id>00000000-0000-0000-0000-000000000000</id>
		<indexed>false</indexed>
		<locked>false</locked>
		<title>Titelpagina</title>
		<hidden>false</hidden>
		<type>COVERPAGE</type>
	</IndexItemBaseDataContract>
	<IndexItemBaseDataContract i:type="ChapterDataContract">
		<EndsAt>0</EndsAt>
		<FooterConfig>
			<Enabled>false</Enabled>
			<FootNote i:nil="true" />
			<ShowFooterNote>false</ShowFooterNote>
			<ShowPageNr>false</ShowPageNr>
			<Style i:nil="true" />
		</FooterConfig>
		<HeaderConfig>
			<Enabled>false</Enabled>
			<ShowTitle>false</ShowTitle>
			<Style i:nil="true" />
		</HeaderConfig>
		<NoDraft>false</NoDraft>
		<StartsAt>0</StartsAt>
		<iTextTemplate i:nil="true" />
		<iconCls i:nil="true" />
		<id>00000000-0000-0000-0000-000000000000</id>
		<indexed>false</indexed>
		<locked>false</locked>
		<title>Proef- en saldibalans</title>
		<items>
			<IndexItemBaseDataContract i:type="StatementsDataContract">
				<EndsAt>0</EndsAt>
				<FooterConfig>
					<Enabled>false</Enabled>
					<FootNote i:nil="true" />
					<ShowFooterNote>true</ShowFooterNote>
					<ShowPageNr>false</ShowPageNr>
					<Style i:nil="true" />
				</FooterConfig>
				<HeaderConfig>
					<Enabled>false</Enabled>
					<ShowTitle>true</ShowTitle>
					<Style i:nil="true" />
				</HeaderConfig>
				<NoDraft>false</NoDraft>
				<StartsAt>0</StartsAt>
				<iTextTemplate i:nil="true" />
				<iconCls i:nil="true" />
				<id>00000000-0000-0000-0000-000000000000</id>
				<indexed>false</indexed>
				<locked>false</locked>
				<title>Activa</title>
				<Comparable>false</Comparable>
				<Culture>nl-BE</Culture>
				<Detailed>true</Detailed>
				<FileId>00000000-0000-0000-0000-000000000000</FileId>
				<layout>ACTIVA</layout>
				<type>STATEMENT</type>
			</IndexItemBaseDataContract>
			<IndexItemBaseDataContract i:type="StatementsDataContract">
				<EndsAt>0</EndsAt>
				<FooterConfig>
					<Enabled>false</Enabled>
					<FootNote i:nil="true" />
					<ShowFooterNote>true</ShowFooterNote>
					<ShowPageNr>false</ShowPageNr>
					<Style i:nil="true" />
				</FooterConfig>
				<HeaderConfig>
					<Enabled>true</Enabled>
					<ShowTitle>true</ShowTitle>
					<Style i:nil="true" />
				</HeaderConfig>
				<NoDraft>false</NoDraft>
				<StartsAt>0</StartsAt>
				<iTextTemplate i:nil="true" />
				<iconCls i:nil="true" />
				<id>00000000-0000-0000-0000-000000000000</id>
				<indexed>false</indexed>
				<locked>false</locked>
				<title>Passiva</title>
				<Comparable>false</Comparable>
				<Culture>nl-BE</Culture>
				<Detailed>true</Detailed>
				<FileId>00000000-0000-0000-0000-000000000000</FileId>
				<layout>PASSIVA</layout>
				<type>STATEMENT</type>
			</IndexItemBaseDataContract>
			<IndexItemBaseDataContract i:type="StatementsDataContract">
				<EndsAt>0</EndsAt>
				<FooterConfig>
					<Enabled>false</Enabled>
					<FootNote i:nil="true" />
					<ShowFooterNote>true</ShowFooterNote>
					<ShowPageNr>false</ShowPageNr>
					<Style i:nil="true" />
				</FooterConfig>
				<HeaderConfig>
					<Enabled>true</Enabled>
					<ShowTitle>true</ShowTitle>
					<Style i:nil="true" />
				</HeaderConfig>
				<NoDraft>false</NoDraft>
				<StartsAt>0</StartsAt>
				<iTextTemplate i:nil="true" />
				<iconCls i:nil="true" />
				<id>00000000-0000-0000-0000-000000000000</id>
				<indexed>false</indexed>
				<locked>false</locked>
				<title>Resultatenrekening</title>
				<Comparable>false</Comparable>
				<Culture>nl-BE</Culture>
				<Detailed>true</Detailed>
				<FileId>00000000-0000-0000-0000-000000000000</FileId>
				<layout>RESULTATENREKENING</layout>
				<type>STATEMENT</type>
			</IndexItemBaseDataContract>
			<IndexItemBaseDataContract i:type="StatementsDataContract">
				<EndsAt>0</EndsAt>
				<FooterConfig>
					<Enabled>false</Enabled>
					<FootNote i:nil="true" />
					<ShowFooterNote>true</ShowFooterNote>
					<ShowPageNr>false</ShowPageNr>
					<Style i:nil="true" />
				</FooterConfig>
				<HeaderConfig>
					<Enabled>true</Enabled>
					<ShowTitle>true</ShowTitle>
					<Style i:nil="true" />
				</HeaderConfig>
				<NoDraft>false</NoDraft>
				<StartsAt>0</StartsAt>
				<iTextTemplate i:nil="true" />
				<iconCls i:nil="true" />
				<id>00000000-0000-0000-0000-000000000000</id>
				<indexed>false</indexed>
				<locked>false</locked>
				<title>Resultaatverwerking</title>
				<Comparable>false</Comparable>
				<Culture>nl-BE</Culture>
				<Detailed>false</Detailed>
				<FileId>00000000-0000-0000-0000-000000000000</FileId>
				<layout>RESULTAATVERWERKING</layout>
				<type>STATEMENT</type>
			</IndexItemBaseDataContract>
		</items>
		<nocoverpage>false</nocoverpage>
		<type>CHAPTER</type>
	</IndexItemBaseDataContract>
</ArrayOfIndexItemBaseDataContract>','ACR')
rollback tran


--C29A94BF-BB06-466D-9054-5B2C632E1D97

/****** Script for SelectTopNRows command from SSMS  ******/
SELECT TOP 1000 [ServiceBundles_ServiceID]
      ,[ServiceBundles_TemplateID]
  FROM [eBook].[dbo].[TPBOOK_SERVICE_BUNDLE_TEMPLATES]
  -- jaarafsluiting


  --UPDATE JAARAFSLUITING NL (REMOVE FOOTER)
  begin tran
  UPDATE [eBook].[dbo].[TPBOOK_BUNDLE_TEMPLATES]
  SET bundle_template_content = '<ArrayOfIndexItemBaseDataContract xmlns:i="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://schemas.datacontract.org/2004/07/EY.com.eBook.API.Contracts.Data">
  <IndexItemBaseDataContract i:type="CoverpageDataContract">
    <EndsAt>0</EndsAt>
    <FooterConfig>
      <Enabled>false</Enabled>
      <FootNote i:nil="true" />
      <ShowFooterNote>false</ShowFooterNote>
      <ShowPageNr>false</ShowPageNr>
      <Style i:nil="true" />
    </FooterConfig>
    <HeaderConfig>
      <Enabled>false</Enabled>
      <ShowTitle>false</ShowTitle>
      <Style i:nil="true" />
    </HeaderConfig>
    <NoDraft>false</NoDraft>
    <StartsAt>0</StartsAt>
    <iTextTemplate i:nil="true" />
    <iconCls i:nil="true" />
    <id>00000000-0000-0000-0000-000000000000</id>
    <indexed>false</indexed>
    <locked>false</locked>
    <title>Titelpagina</title>
    <hidden>false</hidden>
    <type>COVERPAGE</type>
  </IndexItemBaseDataContract>
  <IndexItemBaseDataContract i:type="ChapterDataContract">
    <EndsAt>0</EndsAt>
    <FooterConfig>
      <Enabled>false</Enabled>
      <FootNote i:nil="true" />
      <ShowFooterNote>false</ShowFooterNote>
      <ShowPageNr>false</ShowPageNr>
      <Style i:nil="true" />
    </FooterConfig>
    <HeaderConfig>
      <Enabled>false</Enabled>
      <ShowTitle>false</ShowTitle>
      <Style i:nil="true" />
    </HeaderConfig>
    <NoDraft>false</NoDraft>
    <StartsAt>0</StartsAt>
    <iTextTemplate i:nil="true" />
    <iconCls i:nil="true" />
    <id>00000000-0000-0000-0000-000000000000</id>
    <indexed>false</indexed>
    <locked>false</locked>
    <title>Jaarrekening</title>
    <items />
    <nocoverpage>false</nocoverpage>
    <signedrepo>true</signedrepo>
    <type>CHAPTER</type>
  </IndexItemBaseDataContract>
  <IndexItemBaseDataContract i:type="ChapterDataContract">
    <EndsAt>0</EndsAt>
    <FooterConfig>
      <Enabled>false</Enabled>
      <FootNote i:nil="true" />
      <ShowFooterNote>false</ShowFooterNote>
      <ShowPageNr>false</ShowPageNr>
      <Style i:nil="true" />
    </FooterConfig>
    <HeaderConfig>
      <Enabled>false</Enabled>
      <ShowTitle>false</ShowTitle>
      <Style i:nil="true" />
    </HeaderConfig>
    <NoDraft>false</NoDraft>
    <StartsAt>0</StartsAt>
    <iTextTemplate i:nil="true" />
    <iconCls i:nil="true" />
    <id>00000000-0000-0000-0000-000000000000</id>
    <indexed>false</indexed>
    <locked>false</locked>
    <title>Vergaderstukken</title>
    <items />
    <nocoverpage>false</nocoverpage>
    <signedrepo>true</signedrepo>
    <type>CHAPTER</type>
  </IndexItemBaseDataContract>
  <IndexItemBaseDataContract i:type="ChapterDataContract">
    <EndsAt>0</EndsAt>
    <FooterConfig>
      <Enabled>false</Enabled>
      <FootNote i:nil="true" />
      <ShowFooterNote>false</ShowFooterNote>
      <ShowPageNr>false</ShowPageNr>
      <Style i:nil="true" />
    </FooterConfig>
    <HeaderConfig>
      <Enabled>false</Enabled>
      <ShowTitle>false</ShowTitle>
      <Style i:nil="true" />
    </HeaderConfig>
    <NoDraft>false</NoDraft>
    <StartsAt>0</StartsAt>
    <iTextTemplate i:nil="true" />
    <iconCls i:nil="true" />
    <id>00000000-0000-0000-0000-000000000000</id>
    <indexed>false</indexed>
    <locked>false</locked>
    <title>Proef- en saldibalans</title>
    <items>
      <IndexItemBaseDataContract i:type="StatementsDataContract">
        <EndsAt>0</EndsAt>
        <FooterConfig>
          <Enabled>false</Enabled>
          <FootNote i:nil="true" />
          <ShowFooterNote>true</ShowFooterNote>
          <ShowPageNr>false</ShowPageNr>
          <Style i:nil="true" />
        </FooterConfig>
        <HeaderConfig>
          <Enabled>true</Enabled>
          <ShowTitle>true</ShowTitle>
          <Style i:nil="true" />
        </HeaderConfig>
        <NoDraft>false</NoDraft>
        <StartsAt>0</StartsAt>
        <iTextTemplate i:nil="true" />
        <iconCls i:nil="true" />
        <id>00000000-0000-0000-0000-000000000000</id>
        <indexed>false</indexed>
        <locked>false</locked>
        <title>Activa</title>
        <Comparable>false</Comparable>
        <Culture>nl-BE</Culture>
        <Detailed>true</Detailed>
        <FileId>00000000-0000-0000-0000-000000000000</FileId>
        <layout>ACTIVA</layout>
        <type>STATEMENT</type>
      </IndexItemBaseDataContract>
      <IndexItemBaseDataContract i:type="StatementsDataContract">
        <EndsAt>0</EndsAt>
        <FooterConfig>
          <Enabled>false</Enabled>
          <FootNote i:nil="true" />
          <ShowFooterNote>false</ShowFooterNote>
          <ShowPageNr>false</ShowPageNr>
          <Style i:nil="true" />
        </FooterConfig>
        <HeaderConfig>
          <Enabled>true</Enabled>
          <ShowTitle>true</ShowTitle>
          <Style i:nil="true" />
        </HeaderConfig>
        <NoDraft>false</NoDraft>
        <StartsAt>0</StartsAt>
        <iTextTemplate i:nil="true" />
        <iconCls i:nil="true" />
        <id>00000000-0000-0000-0000-000000000000</id>
        <indexed>false</indexed>
        <locked>false</locked>
        <title>Passiva</title>
        <Comparable>false</Comparable>
        <Culture>nl-BE</Culture>
        <Detailed>true</Detailed>
        <FileId>00000000-0000-0000-0000-000000000000</FileId>
        <layout>PASSIVA</layout>
        <type>STATEMENT</type>
      </IndexItemBaseDataContract>
      <IndexItemBaseDataContract i:type="StatementsDataContract">
        <EndsAt>0</EndsAt>
        <FooterConfig>
          <Enabled>false</Enabled>
          <FootNote i:nil="true" />
          <ShowFooterNote>false</ShowFooterNote>
          <ShowPageNr>false</ShowPageNr>
          <Style i:nil="true" />
        </FooterConfig>
        <HeaderConfig>
          <Enabled>true</Enabled>
          <ShowTitle>true</ShowTitle>
          <Style i:nil="true" />
        </HeaderConfig>
        <NoDraft>false</NoDraft>
        <StartsAt>0</StartsAt>
        <iTextTemplate i:nil="true" />
        <iconCls i:nil="true" />
        <id>00000000-0000-0000-0000-000000000000</id>
        <indexed>false</indexed>
        <locked>false</locked>
        <title>Resultatenrekening</title>
        <Comparable>false</Comparable>
        <Culture>nl-BE</Culture>
        <Detailed>true</Detailed>
        <FileId>00000000-0000-0000-0000-000000000000</FileId>
        <layout>RESULTATENREKENING</layout>
        <type>STATEMENT</type>
      </IndexItemBaseDataContract>
      <IndexItemBaseDataContract i:type="StatementsDataContract">
        <EndsAt>0</EndsAt>
        <FooterConfig>
          <Enabled>false</Enabled>
          <FootNote i:nil="true" />
          <ShowFooterNote>false</ShowFooterNote>
          <ShowPageNr>false</ShowPageNr>
          <Style i:nil="true" />
        </FooterConfig>
        <HeaderConfig>
          <Enabled>true</Enabled>
          <ShowTitle>true</ShowTitle>
          <Style i:nil="true" />
        </HeaderConfig>
        <NoDraft>false</NoDraft>
        <StartsAt>0</StartsAt>
        <iTextTemplate i:nil="true" />
        <iconCls i:nil="true" />
        <id>00000000-0000-0000-0000-000000000000</id>
        <indexed>false</indexed>
        <locked>false</locked>
        <title>Resultaatverwerking</title>
        <Comparable>false</Comparable>
        <Culture>nl-BE</Culture>
        <Detailed>false</Detailed>
        <FileId>00000000-0000-0000-0000-000000000000</FileId>
        <layout>RESULTAATVERWERKING</layout>
        <type>STATEMENT</type>
      </IndexItemBaseDataContract>
    </items>
    <nocoverpage>false</nocoverpage>
    <type>CHAPTER</type>
  </IndexItemBaseDataContract>
  <IndexItemBaseDataContract i:type="ChapterDataContract">
    <EndsAt>0</EndsAt>
    <FooterConfig>
      <Enabled>false</Enabled>
      <FootNote i:nil="true" />
      <ShowFooterNote>false</ShowFooterNote>
      <ShowPageNr>false</ShowPageNr>
      <Style i:nil="true" />
    </FooterConfig>
    <HeaderConfig>
      <Enabled>false</Enabled>
      <ShowTitle>false</ShowTitle>
      <Style i:nil="true" />
    </HeaderConfig>
    <NoDraft>false</NoDraft>
    <StartsAt>0</StartsAt>
    <iTextTemplate i:nil="true" />
    <iconCls i:nil="true" />
    <id>00000000-0000-0000-0000-000000000000</id>
    <indexed>false</indexed>
    <locked>false</locked>
    <title>Andere bijlagen</title>
    <items />
    <nocoverpage>false</nocoverpage>
    <type>CHAPTER</type>
  </IndexItemBaseDataContract>
</ArrayOfIndexItemBaseDataContract>'
  WHERE bundle_template_ID = 'C29A94BF-BB06-466D-9054-5B2C632E1D97'
  rollback tran


  SELECT * FROM [eBook].[dbo].[TPBOOK_BUNDLE_TEMPLATES]
WHERE bundle_template_name = 'Jaarafsluiting'  and [Bundle_Template_Culture] = 'fr-BE'

--UPDATE JAARAFSLUITING FR (REMOVE FOOTER)
  begin tran
  UPDATE [eBook].[dbo].[TPBOOK_BUNDLE_TEMPLATES]
  SET bundle_template_content = '<ArrayOfIndexItemBaseDataContract xmlns:i="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://schemas.datacontract.org/2004/07/EY.com.eBook.API.Contracts.Data">
  <IndexItemBaseDataContract i:type="CoverpageDataContract">
    <EndsAt>0</EndsAt>
    <FooterConfig>
      <Enabled>false</Enabled>
      <FootNote i:nil="true" />
      <ShowFooterNote>false</ShowFooterNote>
      <ShowPageNr>false</ShowPageNr>
      <Style i:nil="true" />
    </FooterConfig>
    <HeaderConfig>
      <Enabled>false</Enabled>
      <ShowTitle>false</ShowTitle>
      <Style i:nil="true" />
    </HeaderConfig>
    <NoDraft>false</NoDraft>
    <StartsAt>0</StartsAt>
    <iTextTemplate i:nil="true" />
    <iconCls i:nil="true" />
    <id>00000000-0000-0000-0000-000000000000</id>
    <indexed>false</indexed>
    <locked>false</locked>
    <title>Titelpagina</title>
    <hidden>false</hidden>
    <type>COVERPAGE</type>
  </IndexItemBaseDataContract>
  <IndexItemBaseDataContract i:type="ChapterDataContract">
    <EndsAt>0</EndsAt>
    <FooterConfig>
      <Enabled>false</Enabled>
      <FootNote i:nil="true" />
      <ShowFooterNote>false</ShowFooterNote>
      <ShowPageNr>false</ShowPageNr>
      <Style i:nil="true" />
    </FooterConfig>
    <HeaderConfig>
      <Enabled>false</Enabled>
      <ShowTitle>false</ShowTitle>
      <Style i:nil="true" />
    </HeaderConfig>
    <NoDraft>false</NoDraft>
    <StartsAt>0</StartsAt>
    <iTextTemplate i:nil="true" />
    <iconCls i:nil="true" />
    <id>00000000-0000-0000-0000-000000000000</id>
    <indexed>false</indexed>
    <locked>false</locked>
    <title>Jaarrekening</title>
    <items />
    <nocoverpage>false</nocoverpage>
    <signedrepo>true</signedrepo>
    <type>CHAPTER</type>
  </IndexItemBaseDataContract>
  <IndexItemBaseDataContract i:type="ChapterDataContract">
    <EndsAt>0</EndsAt>
    <FooterConfig>
      <Enabled>false</Enabled>
      <FootNote i:nil="true" />
      <ShowFooterNote>false</ShowFooterNote>
      <ShowPageNr>false</ShowPageNr>
      <Style i:nil="true" />
    </FooterConfig>
    <HeaderConfig>
      <Enabled>false</Enabled>
      <ShowTitle>false</ShowTitle>
      <Style i:nil="true" />
    </HeaderConfig>
    <NoDraft>false</NoDraft>
    <StartsAt>0</StartsAt>
    <iTextTemplate i:nil="true" />
    <iconCls i:nil="true" />
    <id>00000000-0000-0000-0000-000000000000</id>
    <indexed>false</indexed>
    <locked>false</locked>
    <title>Vergaderstukken</title>
    <items />
    <nocoverpage>false</nocoverpage>
    <signedrepo>true</signedrepo>
    <type>CHAPTER</type>
  </IndexItemBaseDataContract>
  <IndexItemBaseDataContract i:type="ChapterDataContract">
    <EndsAt>0</EndsAt>
    <FooterConfig>
      <Enabled>false</Enabled>
      <FootNote i:nil="true" />
      <ShowFooterNote>false</ShowFooterNote>
      <ShowPageNr>false</ShowPageNr>
      <Style i:nil="true" />
    </FooterConfig>
    <HeaderConfig>
      <Enabled>false</Enabled>
      <ShowTitle>false</ShowTitle>
      <Style i:nil="true" />
    </HeaderConfig>
    <NoDraft>false</NoDraft>
    <StartsAt>0</StartsAt>
    <iTextTemplate i:nil="true" />
    <iconCls i:nil="true" />
    <id>00000000-0000-0000-0000-000000000000</id>
    <indexed>false</indexed>
    <locked>false</locked>
    <title>Proef- en saldibalans</title>
    <items>
      <IndexItemBaseDataContract i:type="StatementsDataContract">
        <EndsAt>0</EndsAt>
        <FooterConfig>
          <Enabled>false</Enabled>
          <FootNote i:nil="true" />
          <ShowFooterNote>true</ShowFooterNote>
          <ShowPageNr>false</ShowPageNr>
          <Style i:nil="true" />
        </FooterConfig>
        <HeaderConfig>
          <Enabled>true</Enabled>
          <ShowTitle>true</ShowTitle>
          <Style i:nil="true" />
        </HeaderConfig>
        <NoDraft>false</NoDraft>
        <StartsAt>0</StartsAt>
        <iTextTemplate i:nil="true" />
        <iconCls i:nil="true" />
        <id>00000000-0000-0000-0000-000000000000</id>
        <indexed>false</indexed>
        <locked>false</locked>
        <title>Activa</title>
        <Comparable>false</Comparable>
        <Culture>nl-BE</Culture>
        <Detailed>true</Detailed>
        <FileId>00000000-0000-0000-0000-000000000000</FileId>
        <layout>ACTIVA</layout>
        <type>STATEMENT</type>
      </IndexItemBaseDataContract>
      <IndexItemBaseDataContract i:type="StatementsDataContract">
        <EndsAt>0</EndsAt>
        <FooterConfig>
          <Enabled>false</Enabled>
          <FootNote i:nil="true" />
          <ShowFooterNote>false</ShowFooterNote>
          <ShowPageNr>false</ShowPageNr>
          <Style i:nil="true" />
        </FooterConfig>
        <HeaderConfig>
          <Enabled>true</Enabled>
          <ShowTitle>true</ShowTitle>
          <Style i:nil="true" />
        </HeaderConfig>
        <NoDraft>false</NoDraft>
        <StartsAt>0</StartsAt>
        <iTextTemplate i:nil="true" />
        <iconCls i:nil="true" />
        <id>00000000-0000-0000-0000-000000000000</id>
        <indexed>false</indexed>
        <locked>false</locked>
        <title>Passiva</title>
        <Comparable>false</Comparable>
        <Culture>nl-BE</Culture>
        <Detailed>true</Detailed>
        <FileId>00000000-0000-0000-0000-000000000000</FileId>
        <layout>PASSIVA</layout>
        <type>STATEMENT</type>
      </IndexItemBaseDataContract>
      <IndexItemBaseDataContract i:type="StatementsDataContract">
        <EndsAt>0</EndsAt>
        <FooterConfig>
          <Enabled>false</Enabled>
          <FootNote i:nil="true" />
          <ShowFooterNote>false</ShowFooterNote>
          <ShowPageNr>false</ShowPageNr>
          <Style i:nil="true" />
        </FooterConfig>
        <HeaderConfig>
          <Enabled>true</Enabled>
          <ShowTitle>true</ShowTitle>
          <Style i:nil="true" />
        </HeaderConfig>
        <NoDraft>false</NoDraft>
        <StartsAt>0</StartsAt>
        <iTextTemplate i:nil="true" />
        <iconCls i:nil="true" />
        <id>00000000-0000-0000-0000-000000000000</id>
        <indexed>false</indexed>
        <locked>false</locked>
        <title>Resultatenrekening</title>
        <Comparable>false</Comparable>
        <Culture>nl-BE</Culture>
        <Detailed>true</Detailed>
        <FileId>00000000-0000-0000-0000-000000000000</FileId>
        <layout>RESULTATENREKENING</layout>
        <type>STATEMENT</type>
      </IndexItemBaseDataContract>
      <IndexItemBaseDataContract i:type="StatementsDataContract">
        <EndsAt>0</EndsAt>
        <FooterConfig>
          <Enabled>false</Enabled>
          <FootNote i:nil="true" />
          <ShowFooterNote>false</ShowFooterNote>
          <ShowPageNr>false</ShowPageNr>
          <Style i:nil="true" />
        </FooterConfig>
        <HeaderConfig>
          <Enabled>true</Enabled>
          <ShowTitle>true</ShowTitle>
          <Style i:nil="true" />
        </HeaderConfig>
        <NoDraft>false</NoDraft>
        <StartsAt>0</StartsAt>
        <iTextTemplate i:nil="true" />
        <iconCls i:nil="true" />
        <id>00000000-0000-0000-0000-000000000000</id>
        <indexed>false</indexed>
        <locked>false</locked>
        <title>Resultaatverwerking</title>
        <Comparable>false</Comparable>
        <Culture>nl-BE</Culture>
        <Detailed>false</Detailed>
        <FileId>00000000-0000-0000-0000-000000000000</FileId>
        <layout>RESULTAATVERWERKING</layout>
        <type>STATEMENT</type>
      </IndexItemBaseDataContract>
    </items>
    <nocoverpage>false</nocoverpage>
    <type>CHAPTER</type>
  </IndexItemBaseDataContract>
  <IndexItemBaseDataContract i:type="ChapterDataContract">
    <EndsAt>0</EndsAt>
    <FooterConfig>
      <Enabled>false</Enabled>
      <FootNote i:nil="true" />
      <ShowFooterNote>false</ShowFooterNote>
      <ShowPageNr>false</ShowPageNr>
      <Style i:nil="true" />
    </FooterConfig>
    <HeaderConfig>
      <Enabled>false</Enabled>
      <ShowTitle>false</ShowTitle>
      <Style i:nil="true" />
    </HeaderConfig>
    <NoDraft>false</NoDraft>
    <StartsAt>0</StartsAt>
    <iTextTemplate i:nil="true" />
    <iconCls i:nil="true" />
    <id>00000000-0000-0000-0000-000000000000</id>
    <indexed>false</indexed>
    <locked>false</locked>
    <title>Andere bijlagen</title>
    <items />
    <nocoverpage>false</nocoverpage>
    <type>CHAPTER</type>
  </IndexItemBaseDataContract>
</ArrayOfIndexItemBaseDataContract>'
  WHERE bundle_template_ID = '43C27DBE-AE64-4208-BD57-060553D41EC8'
  rollback tran

  --CHANGE STATUS
 begin tran
  UPDATE [eBook].[dbo].[TPBOOK_REPOSITORY_STRUCTURE]
  set repository_structure_statustype = 'DRAFTSIGNEDANY'
  where repository_structure_id = 'B9069EA7-6A32-41AF-B0B2-F66DB656760B'
  rollback tran