USE [eBook]
GO

/****** Object:  Table [dbo].[TPBOOK_CLIENT_CODA_POA]    Script Date: 3/02/2015 10:01:27 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[TPBOOK_CLIENT_CODA_POA](
	[BancAccountNr] [varchar](50) NOT NULL,
	[BIC] [varchar](50) NOT NULL,
	[ClientId] [uniqueidentifier] NOT NULL,
	[StatusId] [int] NOT NULL,
	[LastModified] [datetime] NOT NULL,
	[RepositoryItemId] [uniqueidentifier] NULL,
	[DocStoreId] [uniqueidentifier] NULL,
 CONSTRAINT [PK_TPBOOK_CLIENT_CODA_POA] PRIMARY KEY CLUSTERED 
(
	[BancAccountNr] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


