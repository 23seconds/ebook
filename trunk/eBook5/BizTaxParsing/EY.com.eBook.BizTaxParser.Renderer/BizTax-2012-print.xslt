﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:decimal-format name="NonNumber" NaN="0" zero-digit="0" />
  <xsl:variable name="WorksheetLabelFontsize" select="string('10')" />
  <xsl:variable name="WorksheetFontsize" select="string('10')" />
  <xsl:variable name="WorksheetNumbersize" select="string('8')" />
  <xsl:variable name="WorksheetSmallFontsize" select="string('8')" />
  <xsl:variable name="WorksheetTotalsFontsize" select="string('10')" />
  <xsl:variable name="WorksheetSpacing" select="1.1" />
  <xsl:decimal-format name="euro" decimal-separator="," grouping-separator="." />
  <xsl:variable name="NumberFormatting" select="string('#.##0,00')" />
  <xsl:variable name="NumberFormattingNoDigit" select="string('#.##0')" />
  <xsl:variable name="FONT" select="string('C:\WINDOWS\Fonts\EYInterstate-Light.ttf')" />
  <xsl:param name="Culture" />
  <xsl:variable name="lang" select="substring-before($Culture,'-')" />
  <xsl:template match="BizTaxDataContract">
    <xsl:apply-templates select="." mode="TAB_Id" />
    <newpage/>
    <xsl:apply-templates select="." mode="TAB_275.1.A" />
    <newpage/>
    <xsl:apply-templates select="." mode="TAB_275.1.B" />
    <newpage/>
    <xsl:apply-templates select="." mode="TAB_204.3" />
    <newpage/>
    <xsl:apply-templates select="." mode="TAB_275C" />
  </xsl:template>
  <xsl:template match="BizTaxDataContract" mode="TAB_Id">
    <phrase>Identificatiegegevens van de onderneming</phrase>
    <phrase>Ondernemingsnummer</phrase>
    <table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" columns="4" width="100%" align="Center" widths="75;5;5;15" cellspacing="0" cellpadding="1">
      <row>
        <cell>
          <phrase>&amp;#160;HEADING</phrase>
        </cell>
        <cell>
          <phrase>oldcode</phrase>
        </cell>
        <cell>
          <phrase>code</phrase>
        </cell>
        <cell>
          <phrase>DURATION</phrase>
        </cell>
      </row>
      <row>
        <cell>
          <phrase>Type nummer</phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>
          <phrase />
        </cell>
        <cell>VALUE pfs-gcd_IdentifierName - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Opgave nummer</phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>
          <phrase />
        </cell>
        <cell>VALUE pfs-gcd_IdentifierValue - D </cell>
      </row>
    </table>
    <phrase>Naam</phrase>
    <table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" columns="4" width="100%" align="Center" widths="75;5;5;15" cellspacing="0" cellpadding="1">
      <row>
        <cell>
          <phrase>&amp;#160;HEADING</phrase>
        </cell>
        <cell>
          <phrase>oldcode</phrase>
        </cell>
        <cell>
          <phrase>code</phrase>
        </cell>
        <cell>
          <phrase>DURATION</phrase>
        </cell>
      </row>
      <row>
        <cell>
          <phrase>Wettelijke benaming</phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>
          <phrase />
        </cell>
        <cell>VALUE pfs-gcd_EntityCurrentLegalName - D </cell>
      </row>
    </table>
    <phrase>Rechtsvorm</phrase>
    <table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" columns="4" width="100%" align="Center" widths="75;5;5;15" cellspacing="0" cellpadding="1">
      <row>
        <cell>
          <phrase>&amp;#160;HEADING</phrase>
        </cell>
        <cell>
          <phrase>oldcode</phrase>
        </cell>
        <cell>
          <phrase>code</phrase>
        </cell>
        <cell>
          <phrase>DURATION</phrase>
        </cell>
      </row>
      <xsl:if test="ValueChosen">
        <row>
          <cell>
            <phrase>Lijst</phrase>
          </cell>
          <cell>
            <phrase />
          </cell>
          <cell>
            <phrase></phrase>
          </cell>
          <cell>
            <phrase>DURATION VALUELIST</phrase>
          </cell>
        </row>
      </xsl:if>
      <xsl:if test="OTHERS">
        <row>
          <cell>
            <phrase>Andere</phrase>
          </cell>
          <cell>
            <phrase />
          </cell>
          <cell>
            <phrase></phrase>
          </cell>
          <cell>
            <phrase>DURATION OTHER ITEM</phrase>
          </cell>
        </row>
      </xsl:if>
    </table>
    <phrase>Adres</phrase>
    <table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" columns="4" width="100%" align="Center" widths="75;5;5;15" cellspacing="0" cellpadding="1">
      <row>
        <cell>
          <phrase>&amp;#160;HEADING</phrase>
        </cell>
        <cell>
          <phrase>oldcode</phrase>
        </cell>
        <cell>
          <phrase>code</phrase>
        </cell>
        <cell>
          <phrase>DURATION</phrase>
        </cell>
      </row>
      <row>
        <cell colspan="4">
          <phrase>Aard adres</phrase>
        </cell>
      </row>
      <xsl:if test="ValueChosen">
        <row>
          <cell>
            <phrase>Lijst</phrase>
          </cell>
          <cell>
            <phrase />
          </cell>
          <cell>
            <phrase></phrase>
          </cell>
          <cell>
            <phrase>DURATION VALUELIST</phrase>
          </cell>
        </row>
      </xsl:if>
      <xsl:if test="OTHERS">
        <row>
          <cell>
            <phrase>Andere</phrase>
          </cell>
          <cell>
            <phrase />
          </cell>
          <cell>
            <phrase></phrase>
          </cell>
          <cell>
            <phrase>DURATION OTHER ITEM</phrase>
          </cell>
        </row>
      </xsl:if>
      <row>
        <cell>
          <phrase>Straat</phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>
          <phrase />
        </cell>
        <cell>VALUE pfs-gcd_Street - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Nr</phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>
          <phrase />
        </cell>
        <cell>VALUE pfs-gcd_Number - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Bus</phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>
          <phrase />
        </cell>
        <cell>VALUE pfs-gcd_Box - D </cell>
      </row>
      <row>
        <cell colspan="4">
          <phrase>Postcode en gemeente</phrase>
        </cell>
      </row>
      <xsl:if test="ValueChosen">
        <row>
          <cell>
            <phrase>Lijst</phrase>
          </cell>
          <cell>
            <phrase />
          </cell>
          <cell>
            <phrase></phrase>
          </cell>
          <cell>
            <phrase>DURATION VALUELIST</phrase>
          </cell>
        </row>
      </xsl:if>
      <xsl:if test="OTHERS">
        <row>
          <cell>
            <phrase>Andere</phrase>
          </cell>
          <cell>
            <phrase />
          </cell>
          <cell>
            <phrase></phrase>
          </cell>
          <cell>
            <phrase>DURATION OTHER ITEM</phrase>
          </cell>
        </row>
        <row>
          <cell>
            <phrase>Gemeente</phrase>
          </cell>
          <cell>
            <phrase />
          </cell>
          <cell>
            <phrase></phrase>
          </cell>
          <cell>
            <phrase>DURATION OTHER ITEM</phrase>
          </cell>
        </row>
      </xsl:if>
      <row>
        <cell colspan="4">
          <phrase>Land</phrase>
        </cell>
      </row>
      <xsl:if test="ValueChosen">
        <row>
          <cell>
            <phrase>Lijst</phrase>
          </cell>
          <cell>
            <phrase />
          </cell>
          <cell>
            <phrase></phrase>
          </cell>
          <cell>
            <phrase>DURATION VALUELIST</phrase>
          </cell>
        </row>
      </xsl:if>
      <xsl:if test="OTHERS">
        <row>
          <cell>
            <phrase>Andere</phrase>
          </cell>
          <cell>
            <phrase />
          </cell>
          <cell>
            <phrase></phrase>
          </cell>
          <cell>
            <phrase>DURATION OTHER ITEM</phrase>
          </cell>
        </row>
      </xsl:if>
    </table>
    <phrase>Bankinformatie</phrase>
    <table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" columns="4" width="100%" align="Center" widths="75;5;5;15" cellspacing="0" cellpadding="1">
      <row>
        <cell>
          <phrase>&amp;#160;HEADING</phrase>
        </cell>
        <cell>
          <phrase>oldcode</phrase>
        </cell>
        <cell>
          <phrase>code</phrase>
        </cell>
        <cell>
          <phrase>DURATION</phrase>
        </cell>
      </row>
      <row>
        <cell>
          <phrase>BIC</phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>
          <phrase />
        </cell>
        <cell>VALUE pfs-gcd_BankIdentifierCode - D </cell>
      </row>
      <row>
        <cell>
          <phrase>IBAN</phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>
          <phrase />
        </cell>
        <cell>VALUE pfs-gcd_InternationalBankAccountNumber - D </cell>
      </row>
    </table>
    <phrase>Identificerend nummer van de zonder vereffening ontbonden vennootschap</phrase>
    <table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" columns="4" width="100%" align="Center" widths="75;5;5;15" cellspacing="0" cellpadding="1">
      <row>
        <cell>
          <phrase>&amp;#160;HEADING</phrase>
        </cell>
        <cell>
          <phrase>oldcode</phrase>
        </cell>
        <cell>
          <phrase>code</phrase>
        </cell>
        <cell>
          <phrase>DURATION</phrase>
        </cell>
      </row>
      <row>
        <cell>
          <phrase>Type nummer</phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>
          <phrase />
        </cell>
        <cell>VALUE pfs-gcd_IdentifierName - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Opgave nummer</phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>
          <phrase />
        </cell>
        <cell>VALUE pfs-gcd_IdentifierValue - D </cell>
      </row>
    </table>
    <phrase>Inlichtingen omtrent het betreffende boekjaar</phrase>
    <table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" columns="5" width="100%" align="Center" widths="60;5;5;15;15" cellspacing="0" cellpadding="1">
      <row>
        <cell>
          <phrase>&amp;#160;HEADING</phrase>
        </cell>
        <cell>
          <phrase>oldcode</phrase>
        </cell>
        <cell>
          <phrase>code</phrase>
        </cell>
        <cell>
          <phrase>I-START</phrase>
        </cell>
        <cell>
          <phrase>I-END</phrase>
        </cell>
      </row>
      <row>
        <cell>
          <phrase>Begindatum van het boekjaar</phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>VALUE tax-inc_PeriodStartDate - I-START</cell>
        <cell>VALUE tax-inc_PeriodStartDate - I-END</cell>
      </row>
      <row>
        <cell>
          <phrase>Einddatum van het boekjaar</phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>VALUE tax-inc_PeriodEndDate - I-START</cell>
        <cell>VALUE tax-inc_PeriodEndDate - I-END</cell>
      </row>
      <row>
        <cell>
          <phrase>Aanslagjaar</phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>VALUE tax-inc_AssessmentYear - I-START</cell>
        <cell>VALUE tax-inc_AssessmentYear - I-END</cell>
      </row>
      <row>
        <cell>
          <phrase>Aangifte</phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>VALUE tax-inc_TaxReturnType - I-START</cell>
        <cell>VALUE tax-inc_TaxReturnType - I-END</cell>
      </row>
    </table>
    <phrase>Inlichtingen omtrent het document</phrase>
    <phrase>Contactpersoon</phrase>
    <table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" columns="4" width="100%" align="Center" widths="75;5;5;15" cellspacing="0" cellpadding="1">
      <row>
        <cell>
          <phrase>&amp;#160;HEADING</phrase>
        </cell>
        <cell>
          <phrase>oldcode</phrase>
        </cell>
        <cell>
          <phrase>code</phrase>
        </cell>
        <cell>
          <phrase>DURATION</phrase>
        </cell>
      </row>
      <phrase>Hoedanigheid van de contactpersoon</phrase>
      <row>
        <cell>
          <phrase>Naam</phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>
          <phrase />
        </cell>
        <cell>VALUE pfs-gcd_ContactName - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Voornaam</phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>
          <phrase />
        </cell>
        <cell>VALUE pfs-gcd_ContactFirstName - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Functie</phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>
          <phrase />
        </cell>
        <cell>VALUE pfs-gcd_ContactTitlePosition - D </cell>
      </row>
      <row>
        <cell colspan="4">
          <phrase>Adres</phrase>
        </cell>
      </row>
      <row>
        <cell colspan="4">
          <phrase>Aard adres</phrase>
        </cell>
      </row>
      <xsl:if test="ValueChosen">
        <row>
          <cell>
            <phrase>Lijst</phrase>
          </cell>
          <cell>
            <phrase />
          </cell>
          <cell>
            <phrase></phrase>
          </cell>
          <cell>
            <phrase>DURATION VALUELIST</phrase>
          </cell>
        </row>
      </xsl:if>
      <xsl:if test="OTHERS">
        <row>
          <cell>
            <phrase>Andere</phrase>
          </cell>
          <cell>
            <phrase />
          </cell>
          <cell>
            <phrase></phrase>
          </cell>
          <cell>
            <phrase>DURATION OTHER ITEM</phrase>
          </cell>
        </row>
      </xsl:if>
      <row>
        <cell>
          <phrase>Straat</phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>
          <phrase />
        </cell>
        <cell>VALUE pfs-gcd_Street - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Nr</phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>
          <phrase />
        </cell>
        <cell>VALUE pfs-gcd_Number - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Bus</phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>
          <phrase />
        </cell>
        <cell>VALUE pfs-gcd_Box - D </cell>
      </row>
      <row>
        <cell colspan="4">
          <phrase>Postcode en gemeente</phrase>
        </cell>
      </row>
      <xsl:if test="ValueChosen">
        <row>
          <cell>
            <phrase>Lijst</phrase>
          </cell>
          <cell>
            <phrase />
          </cell>
          <cell>
            <phrase></phrase>
          </cell>
          <cell>
            <phrase>DURATION VALUELIST</phrase>
          </cell>
        </row>
      </xsl:if>
      <xsl:if test="OTHERS">
        <row>
          <cell>
            <phrase>Andere</phrase>
          </cell>
          <cell>
            <phrase />
          </cell>
          <cell>
            <phrase></phrase>
          </cell>
          <cell>
            <phrase>DURATION OTHER ITEM</phrase>
          </cell>
        </row>
        <row>
          <cell>
            <phrase>Gemeente</phrase>
          </cell>
          <cell>
            <phrase />
          </cell>
          <cell>
            <phrase></phrase>
          </cell>
          <cell>
            <phrase>DURATION OTHER ITEM</phrase>
          </cell>
        </row>
      </xsl:if>
      <row>
        <cell colspan="4">
          <phrase>Land</phrase>
        </cell>
      </row>
      <xsl:if test="ValueChosen">
        <row>
          <cell>
            <phrase>Lijst</phrase>
          </cell>
          <cell>
            <phrase />
          </cell>
          <cell>
            <phrase></phrase>
          </cell>
          <cell>
            <phrase>DURATION VALUELIST</phrase>
          </cell>
        </row>
      </xsl:if>
      <xsl:if test="OTHERS">
        <row>
          <cell>
            <phrase>Andere</phrase>
          </cell>
          <cell>
            <phrase />
          </cell>
          <cell>
            <phrase></phrase>
          </cell>
          <cell>
            <phrase>DURATION OTHER ITEM</phrase>
          </cell>
        </row>
      </xsl:if>
      <row>
        <cell colspan="4">
          <phrase>Telefoonnummer of faxnummer</phrase>
        </cell>
      </row>
      <row>
        <cell>
          <phrase>Aanduiding of het een telefoon- dan wel een faxnummer betreft</phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>
          <phrase />
        </cell>
        <cell>VALUE pfs-gcd_PhoneFaxNumber - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Lokaal telefoonnummer</phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>
          <phrase />
        </cell>
        <cell>VALUE pfs-gcd_LocalPhoneNumber - D </cell>
      </row>
      <row>
        <cell colspan="4">
          <phrase>E-mail</phrase>
        </cell>
      </row>
      <row>
        <cell>
          <phrase>E-mail adres</phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>
          <phrase />
        </cell>
        <cell>VALUE pfs-gcd_EmailAddress - D </cell>
      </row>
    </table>
  </xsl:template>
  <xsl:template match="BizTaxDataContract" mode="TAB_275.1.A">
    <phrase>Reserves</phrase>
    <phrase>Belastbare gereserveerde winst</phrase>
    <table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" columns="5" width="100%" align="Center" widths="60;5;5;15;15" cellspacing="0" cellpadding="1">
      <row>
        <cell>
          <phrase>&amp;#160;HEADING</phrase>
        </cell>
        <cell>
          <phrase>oldcode</phrase>
        </cell>
        <cell>
          <phrase>code</phrase>
        </cell>
        <cell>
          <phrase>I-START</phrase>
        </cell>
        <cell>
          <phrase>I-END</phrase>
        </cell>
      </row>
      <row>
        <cell>
          <phrase>Belastbare reserves in het kapitaal en belastbare uitgiftepremies</phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>
          <phrase>1001 PN</phrase>
        </cell>
        <cell>VALUE tax-inc_TaxableReservesCapitalSharePremiums - I-START</cell>
        <cell>VALUE tax-inc_TaxableReservesCapitalSharePremiums - I-END</cell>
      </row>
      <row>
        <cell>
          <phrase>Belastbaar gedeelte van de herwaarderingsmeerwaarden</phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>
          <phrase>1004</phrase>
        </cell>
        <cell>VALUE tax-inc_TaxablePortionRevaluationSurpluses - I-START</cell>
        <cell>VALUE tax-inc_TaxablePortionRevaluationSurpluses - I-END</cell>
      </row>
      <row>
        <cell>
          <phrase>Wettelijke reserve</phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>
          <phrase>1005</phrase>
        </cell>
        <cell>VALUE tax-inc_LegalReserve - I-START</cell>
        <cell>VALUE tax-inc_LegalReserve - I-END</cell>
      </row>
      <row>
        <cell>
          <phrase>Onbeschikbare reserves</phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>
          <phrase>1006</phrase>
        </cell>
        <cell>VALUE tax-inc_UnavailableReserves - I-START</cell>
        <cell>VALUE tax-inc_UnavailableReserves - I-END</cell>
      </row>
      <row>
        <cell>
          <phrase>Beschikbare reserves</phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>
          <phrase>1007</phrase>
        </cell>
        <cell>VALUE tax-inc_AvailableReserves - I-START</cell>
        <cell>VALUE tax-inc_AvailableReserves - I-END</cell>
      </row>
      <row>
        <cell>
          <phrase>Overgedragen winst (verlies)</phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>
          <phrase>1008 PN</phrase>
        </cell>
        <cell>VALUE tax-inc_AccumulatedProfitsLosses - I-START</cell>
        <cell>VALUE tax-inc_AccumulatedProfitsLosses - I-END</cell>
      </row>
      <row>
        <cell>
          <phrase>Belastbare voorzieningen</phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>
          <phrase>1009</phrase>
        </cell>
        <cell>VALUE tax-inc_TaxableProvisions - I-START</cell>
        <cell>VALUE tax-inc_TaxableProvisions - I-END</cell>
      </row>
      <row>
        <cell colspan="5">
          <phrase>Onzichtbare reserves</phrase>
        </cell>
      </row>
      <row>
        <cell>
          <phrase>Belastbare waardeverminderingen</phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>
          <phrase>1020</phrase>
        </cell>
        <cell>VALUE tax-inc_TaxableWriteDownsUndisclosedReserve - I-START</cell>
        <cell>VALUE tax-inc_TaxableWriteDownsUndisclosedReserve - I-END</cell>
      </row>
      <row>
        <cell>
          <phrase>Overdreven afschrijvingen</phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>
          <phrase>1021</phrase>
        </cell>
        <cell>VALUE tax-inc_ExaggeratedDepreciationsUndisclosedReserve - I-START</cell>
        <cell>VALUE tax-inc_ExaggeratedDepreciationsUndisclosedReserve - I-END</cell>
      </row>
      <row>
        <cell>
          <phrase>Andere onderschattingen van activa</phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>
          <phrase>1022</phrase>
        </cell>
        <cell>VALUE tax-inc_OtherUnderestimationsAssetsUndisclosedReserve - I-START</cell>
        <cell>VALUE tax-inc_OtherUnderestimationsAssetsUndisclosedReserve - I-END</cell>
      </row>
      <row>
        <cell>
          <phrase>Overschattingen van passiva</phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>
          <phrase>1023</phrase>
        </cell>
        <cell>VALUE tax-inc_OtherOverestimationsLiabilitiesUndisclosedReserve - I-START</cell>
        <cell>VALUE tax-inc_OtherOverestimationsLiabilitiesUndisclosedReserve - I-END</cell>
      </row>
      <row>
        <cell>
          <phrase>Belastbare reserves</phrase>
        </cell>
        <cell>
          <phrase>004 005 012 013</phrase>
        </cell>
        <cell>
          <phrase>1040 PN</phrase>
        </cell>
        <cell>VALUE tax-inc_TaxableReserves - I-START</cell>
        <cell>VALUE tax-inc_TaxableReserves - I-END</cell>
      </row>
      <row>
        <cell colspan="5">
          <phrase>Aanpassingen in meer van de begintoestand der reserves</phrase>
        </cell>
      </row>
      <row>
        <cell>
          <phrase>Meerwaarden op aandelen</phrase>
        </cell>
        <cell>
          <phrase>006 (1)</phrase>
        </cell>
        <cell>
          <phrase>1051</phrase>
        </cell>
        <cell>VALUE tax-inc_CapitalGainsShares - I-START</cell>
        <cell>VALUE tax-inc_CapitalGainsShares - I-END</cell>
      </row>
      <row>
        <cell>
          <phrase>Terugnemingen van vroegere in verworpen uitgaven opgenomen waardeverminderingen op aandelen</phrase>
        </cell>
        <cell>
          <phrase>006 (2)</phrase>
        </cell>
        <cell>
          <phrase>1052</phrase>
        </cell>
        <cell>VALUE tax-inc_CapitalGainsSharesReversalPreviousExemptionsAdjustmentReservesPlus - I-START</cell>
        <cell>VALUE tax-inc_CapitalGainsSharesReversalPreviousExemptionsAdjustmentReservesPlus - I-END</cell>
      </row>
      <row>
        <cell>
          <phrase>Definitieve vrijstelling tax shelter erkende audiovisuele werken</phrase>
        </cell>
        <cell>
          <phrase>008</phrase>
        </cell>
        <cell>
          <phrase>1053</phrase>
        </cell>
        <cell>VALUE tax-inc_DefinitiveExemptionTaxShelterAuthorisedAudiovisualWorksAdjustmentReservesPlus - I-START</cell>
        <cell>VALUE tax-inc_DefinitiveExemptionTaxShelterAuthorisedAudiovisualWorksAdjustmentReservesPlus - I-END</cell>
      </row>
      <row>
        <cell>
          <phrase>Vrijstelling gewestelijke premies en kapitaal- en interestsubsidies</phrase>
        </cell>
        <cell>
          <phrase>014</phrase>
        </cell>
        <cell>
          <phrase>1054</phrase>
        </cell>
        <cell>VALUE tax-inc_ExemptionRegionalGrantsCapitalSubsidiesInterestSubsidiesAdjustmentReservesPlus - I-START</cell>
        <cell>VALUE tax-inc_ExemptionRegionalGrantsCapitalSubsidiesInterestSubsidiesAdjustmentReservesPlus - I-END</cell>
      </row>
      <row>
        <cell>
          <phrase>Definitieve vrijstelling winst voortvloeiend uit de homologatie van een reorganisatieplan en uit de vaststelling van een minnelijk akkoord</phrase>
        </cell>
        <cell>
          <phrase>019</phrase>
        </cell>
        <cell>
          <phrase>1055</phrase>
        </cell>
        <cell>VALUE tax-inc_FinalExemptionProfitHomologationReorganizationPlanAmicableSettlement - I-START</cell>
        <cell>VALUE tax-inc_FinalExemptionProfitHomologationReorganizationPlanAmicableSettlement - I-END</cell>
      </row>
      <row>
        <cell>
          <phrase>Andere</phrase>
        </cell>
        <cell>
          <phrase>007</phrase>
        </cell>
        <cell>
          <phrase>1056</phrase>
        </cell>
        <cell>VALUE tax-inc_OtherAdjustmentsReservesPlus - I-START</cell>
        <cell>VALUE tax-inc_OtherAdjustmentsReservesPlus - I-END</cell>
      </row>
      <row>
        <cell>
          <phrase>Aanpassingen in min van de begintoestand der reserves</phrase>
        </cell>
        <cell>
          <phrase>009</phrase>
        </cell>
        <cell>
          <phrase>1061</phrase>
        </cell>
        <cell>VALUE tax-inc_AdjustmentsReservesMinus - I-START</cell>
        <cell>VALUE tax-inc_AdjustmentsReservesMinus - I-END</cell>
      </row>
      <row>
        <cell>
          <phrase>Belastbare reserves na aanpassing van de begintoestand der reserves</phrase>
        </cell>
        <cell>
          <phrase>010 011</phrase>
        </cell>
        <cell>
          <phrase>1070 PN</phrase>
        </cell>
        <cell>VALUE tax-inc_TaxableReservesAfterAdjustments - I-START</cell>
        <cell>VALUE tax-inc_TaxableReservesAfterAdjustments - I-END</cell>
      </row>
      <row>
        <cell>
          <phrase>Belastbare gereserveerde winst</phrase>
        </cell>
        <cell>
          <phrase>020 021</phrase>
        </cell>
        <cell>
          <phrase>1080 PN</phrase>
        </cell>
        <cell>VALUE tax-inc_TaxableReservedProfit - I-START</cell>
        <cell>VALUE tax-inc_TaxableReservedProfit - I-END</cell>
      </row>
    </table>
    <phrase>Vrijgestelde gereserveerde winst</phrase>
    <table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" columns="5" width="100%" align="Center" widths="60;5;5;15;15" cellspacing="0" cellpadding="1">
      <row>
        <cell>
          <phrase>&amp;#160;HEADING</phrase>
        </cell>
        <cell>
          <phrase>oldcode</phrase>
        </cell>
        <cell>
          <phrase>code</phrase>
        </cell>
        <cell>
          <phrase>I-START</phrase>
        </cell>
        <cell>
          <phrase>I-END</phrase>
        </cell>
      </row>
      <row>
        <cell>
          <phrase>Vrijgestelde waardevermindering</phrase>
        </cell>
        <cell>
          <phrase>301 316</phrase>
        </cell>
        <cell>
          <phrase>1101</phrase>
        </cell>
        <cell>VALUE tax-inc_ExemptWriteDownDebtClaim - I-START</cell>
        <cell>VALUE tax-inc_ExemptWriteDownDebtClaim - I-END</cell>
      </row>
      <row>
        <cell>
          <phrase>Vrijgestelde voorziening</phrase>
        </cell>
        <cell>
          <phrase>302 317</phrase>
        </cell>
        <cell>
          <phrase>1102</phrase>
        </cell>
        <cell>VALUE tax-inc_ExemptProvisionRisksExpenses - I-START</cell>
        <cell>VALUE tax-inc_ExemptProvisionRisksExpenses - I-END</cell>
      </row>
      <row>
        <cell>
          <phrase>Uitgedrukte maar niet-verwezenlijkte meerwaarden</phrase>
        </cell>
        <cell>
          <phrase>303 318</phrase>
        </cell>
        <cell>
          <phrase>1103</phrase>
        </cell>
        <cell>VALUE tax-inc_UnrealisedExpressedCapitalGainsExemptReserve - I-START</cell>
        <cell>VALUE tax-inc_UnrealisedExpressedCapitalGainsExemptReserve - I-END</cell>
      </row>
      <row>
        <cell colspan="5">
          <phrase>Verwezenlijkte meerwaarden</phrase>
        </cell>
      </row>
      <row>
        <cell>
          <phrase>Gespreid te belasten meerwaarden op bepaalde effecten</phrase>
        </cell>
        <cell>
          <phrase>305 320</phrase>
        </cell>
        <cell>
          <phrase>1111</phrase>
        </cell>
        <cell>VALUE tax-inc_CapitalGainsSpecificSecuritiesExemptReserve - I-START</cell>
        <cell>VALUE tax-inc_CapitalGainsSpecificSecuritiesExemptReserve - I-END</cell>
      </row>
      <row>
        <cell>
          <phrase>Gespreid te belasten meerwaarden op materiële en immateriële vaste activa</phrase>
        </cell>
        <cell>
          <phrase>305 320</phrase>
        </cell>
        <cell>
          <phrase>1112</phrase>
        </cell>
        <cell>VALUE tax-inc_CapitalGainsTangibleIntangibleFixedAssetsExemptReserve - I-START</cell>
        <cell>VALUE tax-inc_CapitalGainsTangibleIntangibleFixedAssetsExemptReserve - I-END</cell>
      </row>
      <row>
        <cell>
          <phrase>Andere verwezenlijkte meerwaarden</phrase>
        </cell>
        <cell>
          <phrase>304 319</phrase>
        </cell>
        <cell>
          <phrase>1113</phrase>
        </cell>
        <cell>VALUE tax-inc_OtherRealisedCapitalGainsExemptReserve - I-START</cell>
        <cell>VALUE tax-inc_OtherRealisedCapitalGainsExemptReserve - I-END</cell>
      </row>
      <row>
        <cell>
          <phrase>Meerwaarden op bedrijfsvoertuigen</phrase>
        </cell>
        <cell>
          <phrase>306 321</phrase>
        </cell>
        <cell>
          <phrase>1114</phrase>
        </cell>
        <cell>VALUE tax-inc_CapitalGainsCorporateVehiclesExemptReserve - I-START</cell>
        <cell>VALUE tax-inc_CapitalGainsCorporateVehiclesExemptReserve - I-END</cell>
      </row>
      <row>
        <cell>
          <phrase>Meerwaarden op binnenschepen</phrase>
        </cell>
        <cell>
          <phrase>311 327</phrase>
        </cell>
        <cell>
          <phrase>1115</phrase>
        </cell>
        <cell>VALUE tax-inc_CapitalGainsRiverVesselExemptReserve - I-START</cell>
        <cell>VALUE tax-inc_CapitalGainsRiverVesselExemptReserve - I-END</cell>
      </row>
      <row>
        <cell>
          <phrase>Meerwaarden op zeeschepen</phrase>
        </cell>
        <cell>
          <phrase>307 322</phrase>
        </cell>
        <cell>
          <phrase>1116</phrase>
        </cell>
        <cell>VALUE tax-inc_CapitalGainsSeaVesselExemptReserve - I-START</cell>
        <cell>VALUE tax-inc_CapitalGainsSeaVesselExemptReserve - I-END</cell>
      </row>
      <row>
        <cell>
          <phrase>Investeringsreserve</phrase>
        </cell>
        <cell>
          <phrase>308 323</phrase>
        </cell>
        <cell>
          <phrase>1121</phrase>
        </cell>
        <cell>VALUE tax-inc_ExemptInvestmentReserve - I-START</cell>
        <cell>VALUE tax-inc_ExemptInvestmentReserve - I-END</cell>
      </row>
      <row>
        <cell>
          <phrase>Tax shelter erkende audiovisuele werken</phrase>
        </cell>
        <cell>
          <phrase>309 324</phrase>
        </cell>
        <cell>
          <phrase>1122</phrase>
        </cell>
        <cell>VALUE tax-inc_TaxShelterAuthorisedAudiovisualWorkExemptReserve - I-START</cell>
        <cell>VALUE tax-inc_TaxShelterAuthorisedAudiovisualWorkExemptReserve - I-END</cell>
      </row>
      <row>
        <cell>
          <phrase>Winst voortvloeiend uit de homologatie van een reorganisatieplan en uit de vaststelling van een minnelijk akkoord</phrase>
        </cell>
        <cell>
          <phrase>312 328</phrase>
        </cell>
        <cell>
          <phrase>1123</phrase>
        </cell>
        <cell>VALUE tax-inc_ProfitHomologationReorganizationPlanAmicableSettlementExemptReserve - I-START</cell>
        <cell>VALUE tax-inc_ProfitHomologationReorganizationPlanAmicableSettlementExemptReserve - I-END</cell>
      </row>
      <row>
        <cell>
          <phrase>Andere vrijgestelde bestanddelen</phrase>
        </cell>
        <cell>
          <phrase>310 325</phrase>
        </cell>
        <cell>
          <phrase>1124</phrase>
        </cell>
        <cell>VALUE tax-inc_OtherExemptReserves - I-START</cell>
        <cell>VALUE tax-inc_OtherExemptReserves - I-END</cell>
      </row>
      <row>
        <cell>
          <phrase>Vrijgestelde gereserveerde winst</phrase>
        </cell>
        <cell>
          <phrase>315 326</phrase>
        </cell>
        <cell>
          <phrase>1140</phrase>
        </cell>
        <cell>VALUE tax-inc_ExemptReservedProfit - I-START</cell>
        <cell>VALUE tax-inc_ExemptReservedProfit - I-END</cell>
      </row>
    </table>
    <phrase>Verworpen uitgaven</phrase>
    <table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" columns="4" width="100%" align="Center" widths="75;5;5;15" cellspacing="0" cellpadding="1">
      <row>
        <cell>
          <phrase>&amp;#160;HEADING</phrase>
        </cell>
        <cell>
          <phrase>oldcode</phrase>
        </cell>
        <cell>
          <phrase>code</phrase>
        </cell>
        <cell>
          <phrase>DURATION</phrase>
        </cell>
      </row>
      <row>
        <cell>
          <phrase>Niet-aftrekbare belastingen</phrase>
        </cell>
        <cell>
          <phrase>029</phrase>
        </cell>
        <cell>
          <phrase>1201</phrase>
        </cell>
        <cell>VALUE tax-inc_NonDeductibleTaxes - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Gewestelijke belastingen, heffingen en retributies</phrase>
        </cell>
        <cell>
          <phrase>028</phrase>
        </cell>
        <cell>
          <phrase>1202</phrase>
        </cell>
        <cell>VALUE tax-inc_NonDeductibleRegionalTaxesDutiesRetributions - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Geldboeten, verbeurdverklaringen en straffen van alle aard</phrase>
        </cell>
        <cell>
          <phrase>030</phrase>
        </cell>
        <cell>
          <phrase>1203</phrase>
        </cell>
        <cell>VALUE tax-inc_NonDeductibleFinesConfiscationsPenaltiesAllKind - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Niet-aftrekbare pensioenen, kapitalen, werkgeversbijdragen en -premies</phrase>
        </cell>
        <cell>
          <phrase>031</phrase>
        </cell>
        <cell>
          <phrase>1204</phrase>
        </cell>
        <cell>VALUE tax-inc_NonDeductiblePensionsCapitalEmployerContributionsEmployerPremiums - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Niet-aftrekbare autokosten en minderwaarden op autovoertuigen</phrase>
        </cell>
        <cell>
          <phrase>032</phrase>
        </cell>
        <cell>
          <phrase>1205</phrase>
        </cell>
        <cell>VALUE tax-inc_NonDeductibleCarExpensesLossValuesCars - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Autokosten ten belope van een gedeelte van het voordeel van alle aard</phrase>
        </cell>
        <cell>
          <phrase>022 074</phrase>
        </cell>
        <cell>
          <phrase>1206</phrase>
        </cell>
        <cell>VALUE tax-inc_NonDeductibleCarExpensesPartBenefitsAllKind - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Niet-aftrekbare receptiekosten en kosten voor relatiegeschenken</phrase>
        </cell>
        <cell>
          <phrase>033</phrase>
        </cell>
        <cell>
          <phrase>1207</phrase>
        </cell>
        <cell>VALUE tax-inc_NonDeductibleReceptionBusinessGiftsExpenses - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Niet-aftrekbare restaurantkosten</phrase>
        </cell>
        <cell>
          <phrase>025</phrase>
        </cell>
        <cell>
          <phrase>1208</phrase>
        </cell>
        <cell>VALUE tax-inc_NonDeductibleRestaurantExpenses - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Kosten voor niet-specifieke beroepskledij</phrase>
        </cell>
        <cell>
          <phrase>034</phrase>
        </cell>
        <cell>
          <phrase>1209</phrase>
        </cell>
        <cell>VALUE tax-inc_NonDeductibleNonSpecificProfessionalClothsExpenses - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Overdreven interesten</phrase>
        </cell>
        <cell>
          <phrase>035</phrase>
        </cell>
        <cell>
          <phrase>1210</phrase>
        </cell>
        <cell>VALUE tax-inc_ExaggeratedInterests - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Interesten met betrekking tot een gedeelte van bepaalde leningen</phrase>
        </cell>
        <cell>
          <phrase>036</phrase>
        </cell>
        <cell>
          <phrase>1211</phrase>
        </cell>
        <cell>VALUE tax-inc_NonDeductibleParticularPortionInterestsLoans - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Abnormale of goedgunstige voordelen</phrase>
        </cell>
        <cell>
          <phrase>037</phrase>
        </cell>
        <cell>
          <phrase>1212</phrase>
        </cell>
        <cell>VALUE tax-inc_AbnormalBenevolentAdvantages - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Sociale voordelen</phrase>
        </cell>
        <cell>
          <phrase>038</phrase>
        </cell>
        <cell>
          <phrase>1214</phrase>
        </cell>
        <cell>VALUE tax-inc_NonDeductibleSocialAdvantages - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Voordelen uit maaltijd-, sport-, cultuur- of ecocheques</phrase>
        </cell>
        <cell>
          <phrase>023</phrase>
        </cell>
        <cell>
          <phrase>1215</phrase>
        </cell>
        <cell>VALUE tax-inc_NonDeductibleAdvantagesLuncheonSportsCultureEcoVouchers - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Liberaliteiten</phrase>
        </cell>
        <cell>
          <phrase>039</phrase>
        </cell>
        <cell>
          <phrase>1216</phrase>
        </cell>
        <cell>VALUE tax-inc_Liberalities - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Waardeverminderingen en minderwaarden op aandelen</phrase>
        </cell>
        <cell>
          <phrase>040</phrase>
        </cell>
        <cell>
          <phrase>1217</phrase>
        </cell>
        <cell>VALUE tax-inc_WriteDownsLossValuesShares - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Terugnemingen van vroegere vrijstellingen</phrase>
        </cell>
        <cell>
          <phrase>041</phrase>
        </cell>
        <cell>
          <phrase>1218</phrase>
        </cell>
        <cell>VALUE tax-inc_ReversalPreviousExemptions - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Werknemersparticipatie</phrase>
        </cell>
        <cell>
          <phrase>043 072</phrase>
        </cell>
        <cell>
          <phrase>1219</phrase>
        </cell>
        <cell>VALUE tax-inc_EmployeeParticipation - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Vergoedingen ontbrekende coupon</phrase>
        </cell>
        <cell>
          <phrase>026</phrase>
        </cell>
        <cell>
          <phrase>1220</phrase>
        </cell>
        <cell>VALUE tax-inc_IndemnityMissingCoupon - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Kosten tax shelter erkende audiovisuele werken</phrase>
        </cell>
        <cell>
          <phrase>027</phrase>
        </cell>
        <cell>
          <phrase>1221</phrase>
        </cell>
        <cell>VALUE tax-inc_ExpensesTaxShelterAuthorisedAudiovisualWork - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Gewestelijke premies en kapitaal- en interestsubsidies</phrase>
        </cell>
        <cell>
          <phrase>024</phrase>
        </cell>
        <cell>
          <phrase>1222</phrase>
        </cell>
        <cell>VALUE tax-inc_RegionalPremiumCapitalSubsidiesInterestSubsidies - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Niet-aftrekbare betalingen naar bepaalde Staten</phrase>
        </cell>
        <cell>
          <phrase>054</phrase>
        </cell>
        <cell>
          <phrase>1223</phrase>
        </cell>
        <cell>VALUE tax-inc_NonDeductiblePaymentsCertainStates - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Andere verworpen uitgaven</phrase>
        </cell>
        <cell>
          <phrase>042</phrase>
        </cell>
        <cell>
          <phrase>1239</phrase>
        </cell>
        <cell>VALUE tax-inc_OtherDisallowedExpenses - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Verworpen uitgaven</phrase>
        </cell>
        <cell>
          <phrase>044</phrase>
        </cell>
        <cell>
          <phrase>1240</phrase>
        </cell>
        <cell>VALUE tax-inc_DisallowedExpenses - D </cell>
      </row>
    </table>
    <phrase>Uitgekeerde dividenden</phrase>
    <table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" columns="4" width="100%" align="Center" widths="75;5;5;15" cellspacing="0" cellpadding="1">
      <row>
        <cell>
          <phrase>&amp;#160;HEADING</phrase>
        </cell>
        <cell>
          <phrase>oldcode</phrase>
        </cell>
        <cell>
          <phrase>code</phrase>
        </cell>
        <cell>
          <phrase>DURATION</phrase>
        </cell>
      </row>
      <row>
        <cell>
          <phrase>Gewone dividenden</phrase>
        </cell>
        <cell>
          <phrase>050</phrase>
        </cell>
        <cell>
          <phrase>1301</phrase>
        </cell>
        <cell>VALUE tax-inc_OrdinaryDividends - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Verkrijging van eigen aandelen</phrase>
        </cell>
        <cell>
          <phrase>051</phrase>
        </cell>
        <cell>
          <phrase>1302</phrase>
        </cell>
        <cell>VALUE tax-inc_AcquisitionOwnShares - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Overlijden, uittreding of uitsluiting van een vennoot</phrase>
        </cell>
        <cell>
          <phrase>052</phrase>
        </cell>
        <cell>
          <phrase>1303</phrase>
        </cell>
        <cell>VALUE tax-inc_DeceaseDepartureExclusionPartner - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Verdeling van maatschappelijk vermogen</phrase>
        </cell>
        <cell>
          <phrase>053</phrase>
        </cell>
        <cell>
          <phrase>1304</phrase>
        </cell>
        <cell>VALUE tax-inc_DistributionCompanyAssets - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Uitgekeerde dividenden</phrase>
        </cell>
        <cell>
          <phrase>059</phrase>
        </cell>
        <cell>
          <phrase>1320</phrase>
        </cell>
        <cell>VALUE tax-inc_TaxableDividendsPaid - D </cell>
      </row>
    </table>
    <phrase>Uiteenzetting van de winst</phrase>
    <table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" columns="4" width="100%" align="Center" widths="75;5;5;15" cellspacing="0" cellpadding="1">
      <row>
        <cell>
          <phrase>&amp;#160;HEADING</phrase>
        </cell>
        <cell>
          <phrase>oldcode</phrase>
        </cell>
        <cell>
          <phrase>code</phrase>
        </cell>
        <cell>
          <phrase>DURATION</phrase>
        </cell>
      </row>
      <row>
        <cell>
          <phrase>Belastbare gereserveerde winst</phrase>
        </cell>
        <cell>
          <phrase>020 021</phrase>
        </cell>
        <cell>
          <phrase>1080 PN</phrase>
        </cell>
        <cell>VALUE tax-inc_TaxableReservedProfit - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Verworpen uitgaven</phrase>
        </cell>
        <cell>
          <phrase>044</phrase>
        </cell>
        <cell>
          <phrase>1240</phrase>
        </cell>
        <cell>VALUE tax-inc_DisallowedExpenses - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Uitgekeerde dividenden</phrase>
        </cell>
        <cell>
          <phrase>059</phrase>
        </cell>
        <cell>
          <phrase>1320</phrase>
        </cell>
        <cell>VALUE tax-inc_TaxableDividendsPaid - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Resultaat van het belastbare tijdperk</phrase>
        </cell>
        <cell>
          <phrase>060 061</phrase>
        </cell>
        <cell>
          <phrase>1410 PN</phrase>
        </cell>
        <cell>VALUE tax-inc_FiscalResult - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Werkelijk resultaat uit de zeescheepvaart waarvoor de winst wordt vastgesteld op basis van de tonnage</phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>
          <phrase>1411 PN</phrase>
        </cell>
        <cell>VALUE tax-inc_ShippingResultTonnageBased - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Werkelijk resultaat uit activiteiten waarvoor de winst niet wordt vastgesteld op basis van de tonnage</phrase>
        </cell>
        <cell>
          <phrase>062 063</phrase>
        </cell>
        <cell>
          <phrase>1412 PN</phrase>
        </cell>
        <cell>VALUE tax-inc_ShippingResultNotTonnageBased - D </cell>
      </row>
      <row>
        <cell colspan="4">
          <phrase>Bestanddelen van het resultaat waarop de aftrekbeperking van toepassing is</phrase>
        </cell>
      </row>
      <row>
        <cell>
          <phrase>Verkregen abnormale of goedgunstige voordelen en verkregen financiële voordelen of voordelen van alle aard</phrase>
        </cell>
        <cell>
          <phrase>070</phrase>
        </cell>
        <cell>
          <phrase>1421</phrase>
        </cell>
        <cell>VALUE tax-inc_BenevolentAbnormalFinancialAdvantagesBenefitsAllKind - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Niet-naleving investeringsverplichting of onaantastbaarheidsvoorwaarde voor de investeringsreserve</phrase>
        </cell>
        <cell>
          <phrase>071</phrase>
        </cell>
        <cell>
          <phrase>1422</phrase>
        </cell>
        <cell>VALUE tax-inc_ViolationInvestmentCommitmentIntangibilityConditionInvestmentReserve - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Autokosten ten belope van een gedeelte van het voordeel van alle aard</phrase>
        </cell>
        <cell>
          <phrase>022 074</phrase>
        </cell>
        <cell>
          <phrase>1206</phrase>
        </cell>
        <cell>VALUE tax-inc_NonDeductibleCarExpensesPartBenefitsAllKind - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Werknemersparticipatie</phrase>
        </cell>
        <cell>
          <phrase>043 072</phrase>
        </cell>
        <cell>
          <phrase>1219</phrase>
        </cell>
        <cell>VALUE tax-inc_EmployeeParticipation - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Kapitaal- en interestsubsidies in het kader van de steun aan de landbouw</phrase>
        </cell>
        <cell>
          <phrase>076</phrase>
        </cell>
        <cell>
          <phrase>1423</phrase>
        </cell>
        <cell>VALUE tax-inc_CapitalSubsidiesInterestSubsidiesAgriculturalSupport - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Resterend resultaat</phrase>
        </cell>
        <cell>
          <phrase>077 078</phrase>
        </cell>
        <cell>
          <phrase>1430 PN</phrase>
        </cell>
        <cell>VALUE tax-inc_RemainingFiscalResultBeforeOriginDistribution - D </cell>
      </row>
    </table>
    <table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" columns="6" width="100%" align="Center" widths="45;5;5;15;15;15" cellspacing="0" cellpadding="1">
      <row>
        <cell>
          <phrase>&amp;#160;HEADING</phrase>
        </cell>
        <cell>
          <phrase>OLDCODE</phrase>
        </cell>
        <cell>
          <phrase>CODE</phrase>
        </cell>
        <cell>
          <phrase>Bij verdrag vrijgesteld</phrase>
        </cell>
        <cell>
          <phrase>Niet bij verdrag vrijgesteld</phrase>
        </cell>
        <cell>
          <phrase>Belgisch</phrase>
        </cell>
      </row>
      <row>
        <cell>
          <phrase>Resterend resultaat volgens oorsprong</phrase>
        </cell>
        <cell>
          <phrase>1431 PN</phrase>
        </cell>
        <cell>
          <phrase>084 085 082 083 080 081</phrase>
        </cell>
        <cell>
          <phrase>tax-inc_RemainingFiscalResult - be-tax-d-origin-2012-04-30.xsd#d-origin_TaxTreatyMember</phrase>
        </cell>
        <cell>
          <phrase>tax-inc_RemainingFiscalResult - be-tax-d-origin-2012-04-30.xsd#d-origin_NoTaxTreatyMember</phrase>
        </cell>
        <cell>
          <phrase>tax-inc_RemainingFiscalResult - be-tax-d-origin-2012-04-30.xsd#d-origin_BelgiumMember</phrase>
        </cell>
      </row>
      <row>
        <cell colspan="6">
          <phrase>Aftrekken van de resterende winst</phrase>
        </cell>
      </row>
      <row>
        <cell>
          <phrase>Niet-belastbare bestanddelen</phrase>
        </cell>
        <cell>
          <phrase>1432</phrase>
        </cell>
        <cell>
          <phrase>097 096</phrase>
        </cell>
        <cell>
          <phrase>tax-inc_MiscellaneousExemptions - be-tax-d-origin-2012-04-30.xsd#d-origin_TaxTreatyMember</phrase>
        </cell>
        <cell>
          <phrase>tax-inc_MiscellaneousExemptions - be-tax-d-origin-2012-04-30.xsd#d-origin_NoTaxTreatyMember</phrase>
        </cell>
        <cell>
          <phrase>tax-inc_MiscellaneousExemptions - be-tax-d-origin-2012-04-30.xsd#d-origin_BelgiumMember</phrase>
        </cell>
      </row>
      <row>
        <cell>
          <phrase>Definitief belaste inkomsten en vrijgestelde roerende inkomsten</phrase>
        </cell>
        <cell>
          <phrase>1433</phrase>
        </cell>
        <cell>
          <phrase>099 098</phrase>
        </cell>
        <cell>
          <phrase>tax-inc_PEExemptIncomeMovableAssets - be-tax-d-origin-2012-04-30.xsd#d-origin_TaxTreatyMember</phrase>
        </cell>
        <cell>
          <phrase>tax-inc_PEExemptIncomeMovableAssets - be-tax-d-origin-2012-04-30.xsd#d-origin_NoTaxTreatyMember</phrase>
        </cell>
        <cell>
          <phrase>tax-inc_PEExemptIncomeMovableAssets - be-tax-d-origin-2012-04-30.xsd#d-origin_BelgiumMember</phrase>
        </cell>
      </row>
      <row>
        <cell>
          <phrase>Aftrek voor octrooi-inkomsten</phrase>
        </cell>
        <cell>
          <phrase>1434</phrase>
        </cell>
        <cell>
          <phrase>102 101</phrase>
        </cell>
        <cell>
          <phrase>tax-inc_DeductionPatentsIncome - be-tax-d-origin-2012-04-30.xsd#d-origin_TaxTreatyMember</phrase>
        </cell>
        <cell>
          <phrase>tax-inc_DeductionPatentsIncome - be-tax-d-origin-2012-04-30.xsd#d-origin_NoTaxTreatyMember</phrase>
        </cell>
        <cell>
          <phrase>tax-inc_DeductionPatentsIncome - be-tax-d-origin-2012-04-30.xsd#d-origin_BelgiumMember</phrase>
        </cell>
      </row>
      <row>
        <cell>
          <phrase>Aftrek voor risicokapitaal dat voor het huidig aanslagjaar wordt afgetrokken</phrase>
        </cell>
        <cell>
          <phrase>1435</phrase>
        </cell>
        <cell>
          <phrase>104 103</phrase>
        </cell>
        <cell>
          <phrase>tax-inc_AllowanceCorporateEquity - be-tax-d-origin-2012-04-30.xsd#d-origin_TaxTreatyMember</phrase>
        </cell>
        <cell>
          <phrase>tax-inc_AllowanceCorporateEquity - be-tax-d-origin-2012-04-30.xsd#d-origin_NoTaxTreatyMember</phrase>
        </cell>
        <cell>
          <phrase>tax-inc_AllowanceCorporateEquity - be-tax-d-origin-2012-04-30.xsd#d-origin_BelgiumMember</phrase>
        </cell>
      </row>
      <row>
        <cell>
          <phrase>Gecompenseerde verliezen</phrase>
        </cell>
        <cell>
          <phrase>1436</phrase>
        </cell>
        <cell>
          <phrase>106 105 236</phrase>
        </cell>
        <cell>
          <phrase>tax-inc_CompensatedTaxLosses - be-tax-d-origin-2012-04-30.xsd#d-origin_TaxTreatyMember</phrase>
        </cell>
        <cell>
          <phrase>tax-inc_CompensatedTaxLosses - be-tax-d-origin-2012-04-30.xsd#d-origin_NoTaxTreatyMember</phrase>
        </cell>
        <cell>
          <phrase>tax-inc_CompensatedTaxLosses - be-tax-d-origin-2012-04-30.xsd#d-origin_BelgiumMember</phrase>
        </cell>
      </row>
      <row>
        <cell>
          <phrase>Investeringsaftrek</phrase>
        </cell>
        <cell>
          <phrase>1437</phrase>
        </cell>
        <cell>
          <phrase>107</phrase>
        </cell>
        <cell>
          <phrase>tax-inc_AllowanceInvestmentDeduction - be-tax-d-origin-2012-04-30.xsd#d-origin_TaxTreatyMember</phrase>
        </cell>
        <cell>
          <phrase>tax-inc_AllowanceInvestmentDeduction - be-tax-d-origin-2012-04-30.xsd#d-origin_NoTaxTreatyMember</phrase>
        </cell>
        <cell>
          <phrase>tax-inc_AllowanceInvestmentDeduction - be-tax-d-origin-2012-04-30.xsd#d-origin_BelgiumMember</phrase>
        </cell>
      </row>
      <row>
        <cell>
          <phrase>Resterende winst volgens oorsprong</phrase>
        </cell>
        <cell>
          <phrase>1450</phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>
          <phrase>tax-inc_RemainingFiscalProfitCommonRate - be-tax-d-origin-2012-04-30.xsd#d-origin_TaxTreatyMember</phrase>
        </cell>
        <cell>
          <phrase>tax-inc_RemainingFiscalProfitCommonRate - be-tax-d-origin-2012-04-30.xsd#d-origin_NoTaxTreatyMember</phrase>
        </cell>
        <cell>
          <phrase>tax-inc_RemainingFiscalProfitCommonRate - be-tax-d-origin-2012-04-30.xsd#d-origin_BelgiumMember</phrase>
        </cell>
      </row>
    </table>
    <table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" columns="6" width="100%" align="Center" widths="60;5;5;15;15" cellspacing="0" cellpadding="1">
      <row>
        <cell>
          <phrase>&amp;#160;HEADING</phrase>
        </cell>
        <cell>
          <phrase>oldcode</phrase>
        </cell>
        <cell>
          <phrase>code</phrase>
        </cell>
        <cell>
          <phrase>I-START</phrase>
        </cell>
        <cell>
          <phrase>I-END</phrase>
        </cell>
      </row>
      <row>
        <cell colspan="4">
          <phrase>Belastbare grondslag</phrase>
        </cell>
      </row>
      <row>
        <cell colspan="4">
          <phrase>Belastbaar tegen gewoon tarief</phrase>
        </cell>
      </row>
      <row>
        <cell>
          <phrase>Resterende winst volgens oorsprong</phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>
          <phrase>1450</phrase>
        </cell>
        <cell>VALUE tax-inc_RemainingFiscalProfitCommonRate - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Winst uit zeescheepvaart, vastgesteld op basis van de tonnage</phrase>
        </cell>
        <cell>
          <phrase>108</phrase>
        </cell>
        <cell>
          <phrase>1461</phrase>
        </cell>
        <cell>VALUE tax-inc_ShippingProfitTonnageBased - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Verkregen abnormale of goedgunstige voordelen en verkregen financiële voordelen of voordelen van alle aard</phrase>
        </cell>
        <cell>
          <phrase>070</phrase>
        </cell>
        <cell>
          <phrase>1421</phrase>
        </cell>
        <cell>VALUE tax-inc_BenevolentAbnormalFinancialAdvantagesBenefitsAllKind - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Niet-naleving investeringsverplichting of onaantastbaarheidsvoorwaarde voor de investeringsreserve</phrase>
        </cell>
        <cell>
          <phrase>071</phrase>
        </cell>
        <cell>
          <phrase>1422</phrase>
        </cell>
        <cell>VALUE tax-inc_ViolationInvestmentCommitmentIntangibilityConditionInvestmentReserve - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Autokosten ten belope van een gedeelte van het voordeel van alle aard</phrase>
        </cell>
        <cell>
          <phrase>022 074</phrase>
        </cell>
        <cell>
          <phrase>1206</phrase>
        </cell>
        <cell>VALUE tax-inc_NonDeductibleCarExpensesPartBenefitsAllKind - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Werknemersparticipatie</phrase>
        </cell>
        <cell>
          <phrase>043 072</phrase>
        </cell>
        <cell>
          <phrase>1219</phrase>
        </cell>
        <cell>VALUE tax-inc_EmployeeParticipation - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Meerwaarden op aandelen belastbaar tegen 25%</phrase>
        </cell>
        <cell>
          <phrase>118</phrase>
        </cell>
        <cell>
          <phrase>1465</phrase>
        </cell>
        <cell>VALUE tax-inc_CapitalGainsSharesRate2500 - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Belastbaar tegen exit tax tarief (16,5%)</phrase>
        </cell>
        <cell>
          <phrase>115</phrase>
        </cell>
        <cell>
          <phrase>1470</phrase>
        </cell>
        <cell>VALUE tax-inc_BasicTaxableAmountExitTaxRate - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Kapitaal- en interestsubsidies in het kader van de steun aan de landbouw, belastbaar tegen 5 %</phrase>
        </cell>
        <cell>
          <phrase>119</phrase>
        </cell>
        <cell>
          <phrase>1481</phrase>
        </cell>
        <cell>VALUE tax-inc_CapitalSubsidiesInterestSubsidiesAgriculturalSupportRate500 - D </cell>
      </row>
    </table>
    <phrase>Afzonderlijke aanslagen</phrase>
    <table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" columns="4" width="100%" align="Center" widths="75;5;5;15" cellspacing="0" cellpadding="1">
      <row>
        <cell>
          <phrase>&amp;#160;HEADING</phrase>
        </cell>
        <cell>
          <phrase>oldcode</phrase>
        </cell>
        <cell>
          <phrase>code</phrase>
        </cell>
        <cell>
          <phrase>DURATION</phrase>
        </cell>
      </row>
      <row>
        <cell>
          <phrase>Niet-verantwoorde kosten of voordelen van alle aard, verdoken meerwinsten en financiële voordelen of voordelen van alle aard</phrase>
        </cell>
        <cell>
          <phrase>120</phrase>
        </cell>
        <cell>
          <phrase>1501</phrase>
        </cell>
        <cell>VALUE tax-inc_UnjustifiedExpensesHiddenSurplusProfitsFinancialAdvantagesBenefitsAllKind - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Afzonderlijke aanslag van de belaste reserves ten name van erkende kredietinstellingen</phrase>
        </cell>
        <cell>
          <phrase>123</phrase>
        </cell>
        <cell>
          <phrase>1502</phrase>
        </cell>
        <cell>VALUE tax-inc_SeparateAssessmentTaxedReservesCreditAssociationsInstitutionsOtherAuthorisedInstitutions - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Afzonderlijke aanslag van de uitgekeerde dividenden ten name van vennootschappen die krediet voor ambachtsoutillage mogen verstrekken en vennootschappen voor huisvesting</phrase>
        </cell>
        <cell>
          <phrase>124</phrase>
        </cell>
        <cell>
          <phrase>1503</phrase>
        </cell>
        <cell>VALUE tax-inc_SeparateAssessmentDividendsPaidCreditCorporationTradeEquipmentHousingCorporation - D </cell>
      </row>
    </table>
    <phrase>Bijzondere aanslagen met betrekking tot verrichtingen die vóór 1 januari 1990 hebben plaatsgevonden</phrase>
    <table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" columns="4" width="100%" align="Center" widths="75;5;5;15" cellspacing="0" cellpadding="1">
      <row>
        <cell>
          <phrase>&amp;#160;HEADING</phrase>
        </cell>
        <cell>
          <phrase>oldcode</phrase>
        </cell>
        <cell>
          <phrase>code</phrase>
        </cell>
        <cell>
          <phrase>DURATION</phrase>
        </cell>
      </row>
      <row>
        <cell>
          <phrase>Gehele of gedeeltelijke verdeling van maatschappelijk vermogen, belastbaar tegen 33%</phrase>
        </cell>
        <cell>
          <phrase>125</phrase>
        </cell>
        <cell>
          <phrase>1511</phrase>
        </cell>
        <cell>VALUE tax-inc_SpecialAssessmentPre1990FullPartialDistributionCompanyAssetsRate3300 - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Gehele of gedeeltelijke verdeling van maatschappelijk vermogen, belastbaar tegen 16,5%</phrase>
        </cell>
        <cell>
          <phrase>126</phrase>
        </cell>
        <cell>
          <phrase>1512</phrase>
        </cell>
        <cell>VALUE tax-inc_SpecialAssessmentPre1990FullPartialDistributionCompanyAssetsRate1650 - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Voordelen van alle aard verleend door vennootschappen in vereffening</phrase>
        </cell>
        <cell>
          <phrase>128</phrase>
        </cell>
        <cell>
          <phrase>1513</phrase>
        </cell>
        <cell>VALUE tax-inc_SpecialAssessmentPre1990BenefitsAllKindCompanyLiquidation - D </cell>
      </row>
    </table>
    <phrase>Aanvullende heffing erkende diamanthandelaars en terugbetaling van voorheen verleend belastingkrediet voor onderzoek en ontwikkeling</phrase>
    <table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" columns="4" width="100%" align="Center" widths="75;5;5;15" cellspacing="0" cellpadding="1">
      <row>
        <cell>
          <phrase>&amp;#160;HEADING</phrase>
        </cell>
        <cell>
          <phrase>oldcode</phrase>
        </cell>
        <cell>
          <phrase>code</phrase>
        </cell>
        <cell>
          <phrase>DURATION</phrase>
        </cell>
      </row>
      <row>
        <cell>
          <phrase>Aanvullende heffing erkende diamanthandelaars</phrase>
        </cell>
        <cell>
          <phrase>109</phrase>
        </cell>
        <cell>
          <phrase>1531</phrase>
        </cell>
        <cell>VALUE tax-inc_AdditionalDutiesDiamondTraders - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Terugbetaling van een gedeelte van het voorheen verleende belastingkrediet voor onderzoek en ontwikkeling</phrase>
        </cell>
        <cell>
          <phrase>223</phrase>
        </cell>
        <cell>
          <phrase>1532</phrase>
        </cell>
        <cell>VALUE tax-inc_RetributionTaxCreditResearchDevelopment - D </cell>
      </row>
    </table>
    <phrase>Niet-belastbare bestanddelen</phrase>
    <table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" columns="4" width="100%" align="Center" widths="75;5;5;15" cellspacing="0" cellpadding="1">
      <row>
        <cell>
          <phrase>&amp;#160;HEADING</phrase>
        </cell>
        <cell>
          <phrase>oldcode</phrase>
        </cell>
        <cell>
          <phrase>code</phrase>
        </cell>
        <cell>
          <phrase>DURATION</phrase>
        </cell>
      </row>
      <row>
        <cell>
          <phrase>Vrijgestelde giften</phrase>
        </cell>
        <cell>
          <phrase>090</phrase>
        </cell>
        <cell>
          <phrase>1601</phrase>
        </cell>
        <cell>VALUE tax-inc_ExemptGifts - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Vrijstelling aanvullend personeel</phrase>
        </cell>
        <cell>
          <phrase>091</phrase>
        </cell>
        <cell>
          <phrase>1602</phrase>
        </cell>
        <cell>VALUE tax-inc_ExemptionAdditionalPersonnelMiscellaneousExemptions - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Vrijstelling bijkomend personeel KMO</phrase>
        </cell>
        <cell>
          <phrase>092</phrase>
        </cell>
        <cell>
          <phrase>1603</phrase>
        </cell>
        <cell>VALUE tax-inc_ExemptionAdditionalPersonnelSMEs - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Vrijstelling stagebonus</phrase>
        </cell>
        <cell>
          <phrase>094</phrase>
        </cell>
        <cell>
          <phrase>1604</phrase>
        </cell>
        <cell>VALUE tax-inc_ExemptionTrainingPeriodBonus - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Andere niet-belastbare bestanddelen</phrase>
        </cell>
        <cell>
          <phrase>095</phrase>
        </cell>
        <cell>
          <phrase>1605</phrase>
        </cell>
        <cell>VALUE tax-inc_OtherMiscellaneousExemptions - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Niet-belastbare bestanddelen</phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>
          <phrase>1610</phrase>
        </cell>
        <cell>VALUE tax-inc_DeductibleMiscellaneousExemptions - D </cell>
      </row>
    </table>
    <table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" columns="5" width="100%" align="Center" widths="60;5;5;15;15" cellspacing="0" cellpadding="1">
      <row>
        <cell>
          <phrase>&amp;#160;HEADING</phrase>
        </cell>
        <cell>
          <phrase>OLDCODE</phrase>
        </cell>
        <cell>
          <phrase>CODE</phrase>
        </cell>
        <cell>
          <phrase>Belgische inrichtingen</phrase>
        </cell>
        <cell>
          <phrase>Buitenlandse inrichtingen</phrase>
        </cell>
      </row>
      <row>
        <cell colspan="5">
          <phrase>Definitief belaste inkomsten en vrijgestelde roerende inkomsten uit aandelen</phrase>
        </cell>
      </row>
      <row>
        <cell colspan="5">
          <phrase>Inkomsten toegekend door een vennootschap gevestigd in een lidstaat van de EER</phrase>
        </cell>
      </row>
      <row>
        <cell>
          <phrase>Nettobedrag, Belgische inkomsten</phrase>
        </cell>
        <cell>
          <phrase>1631</phrase>
        </cell>
        <cell>
          <phrase>216</phrase>
        </cell>
        <cell>
          <phrase>tax-inc_NetBelgianIncomeSharesPEExemptIncomeMovableAssets - be-tax-d-br-2012-04-30.xsd#d-br_BelgianBranchMember</phrase>
        </cell>
        <cell>
          <phrase>tax-inc_NetBelgianIncomeSharesPEExemptIncomeMovableAssets - be-tax-d-br-2012-04-30.xsd#d-br_ForeignBranchMember</phrase>
        </cell>
      </row>
      <row>
        <cell>
          <phrase>Roerende voorheffing, Belgische inkomsten</phrase>
        </cell>
        <cell>
          <phrase>1632</phrase>
        </cell>
        <cell>
          <phrase>217</phrase>
        </cell>
        <cell>
          <phrase>tax-inc_WithholdingTaxBelgianIncomeSharesPEExemptIncomeMovableAssets - be-tax-d-br-2012-04-30.xsd#d-br_BelgianBranchMember</phrase>
        </cell>
        <cell>
          <phrase>tax-inc_WithholdingTaxBelgianIncomeSharesPEExemptIncomeMovableAssets - be-tax-d-br-2012-04-30.xsd#d-br_ForeignBranchMember</phrase>
        </cell>
      </row>
      <row>
        <cell>
          <phrase>Nettobedrag, buitenlandse inkomsten</phrase>
        </cell>
        <cell>
          <phrase>1633</phrase>
        </cell>
        <cell>
          <phrase>218</phrase>
        </cell>
        <cell>
          <phrase>tax-inc_NetForeignIncomeSharesPEExemptIncomeMovableAssets - be-tax-d-br-2012-04-30.xsd#d-br_BelgianBranchMember</phrase>
        </cell>
        <cell>
          <phrase>tax-inc_NetForeignIncomeSharesPEExemptIncomeMovableAssets - be-tax-d-br-2012-04-30.xsd#d-br_ForeignBranchMember</phrase>
        </cell>
      </row>
      <row>
        <cell>
          <phrase>Roerende voorheffing, buitenlandse inkomsten</phrase>
        </cell>
        <cell>
          <phrase>1634</phrase>
        </cell>
        <cell>
          <phrase>219</phrase>
        </cell>
        <cell>
          <phrase>tax-inc_WithholdingTaxForeignIncomeSharesPEExemptIncomeMovableAssets - be-tax-d-br-2012-04-30.xsd#d-br_BelgianBranchMember</phrase>
        </cell>
        <cell>
          <phrase>tax-inc_WithholdingTaxForeignIncomeSharesPEExemptIncomeMovableAssets - be-tax-d-br-2012-04-30.xsd#d-br_ForeignBranchMember</phrase>
        </cell>
      </row>
      <row>
        <cell colspan="5">
          <phrase>Andere inkomsten</phrase>
        </cell>
      </row>
      <row>
        <cell>
          <phrase>Nettobedrag, Belgische inkomsten</phrase>
        </cell>
        <cell>
          <phrase>1635</phrase>
        </cell>
        <cell>
          <phrase>220</phrase>
        </cell>
        <cell>
          <phrase>tax-inc_NetOtherBelgianIncomeSharesPEExemptIncomeMovableAssets - be-tax-d-br-2012-04-30.xsd#d-br_BelgianBranchMember</phrase>
        </cell>
        <cell>
          <phrase>tax-inc_NetOtherBelgianIncomeSharesPEExemptIncomeMovableAssets - be-tax-d-br-2012-04-30.xsd#d-br_ForeignBranchMember</phrase>
        </cell>
      </row>
      <row>
        <cell>
          <phrase>Roerende voorheffing, Belgische inkomsten</phrase>
        </cell>
        <cell>
          <phrase>1636</phrase>
        </cell>
        <cell>
          <phrase>221</phrase>
        </cell>
        <cell>
          <phrase>tax-inc_WithholdingTaxOtherBelgianIncomeSharesPEExemptIncomeMovableAssets - be-tax-d-br-2012-04-30.xsd#d-br_BelgianBranchMember</phrase>
        </cell>
        <cell>
          <phrase>tax-inc_WithholdingTaxOtherBelgianIncomeSharesPEExemptIncomeMovableAssets - be-tax-d-br-2012-04-30.xsd#d-br_ForeignBranchMember</phrase>
        </cell>
      </row>
      <row>
        <cell>
          <phrase>Nettobedrag, buitenlandse inkomsten</phrase>
        </cell>
        <cell>
          <phrase>1637</phrase>
        </cell>
        <cell>
          <phrase>225</phrase>
        </cell>
        <cell>
          <phrase>tax-inc_NetOtherForeignIncomeSharesPEExemptIncomeMovableAssets - be-tax-d-br-2012-04-30.xsd#d-br_BelgianBranchMember</phrase>
        </cell>
        <cell>
          <phrase>tax-inc_NetOtherForeignIncomeSharesPEExemptIncomeMovableAssets - be-tax-d-br-2012-04-30.xsd#d-br_ForeignBranchMember</phrase>
        </cell>
      </row>
      <row>
        <cell>
          <phrase>Roerende voorheffing, buitenlandse inkomsten</phrase>
        </cell>
        <cell>
          <phrase>1638</phrase>
        </cell>
        <cell>
          <phrase>226</phrase>
        </cell>
        <cell>
          <phrase>tax-inc_WithholdingTaxOtherForeignIncomeSharesPEExemptIncomeMovableAssets - be-tax-d-br-2012-04-30.xsd#d-br_BelgianBranchMember</phrase>
        </cell>
        <cell>
          <phrase>tax-inc_WithholdingTaxOtherForeignIncomeSharesPEExemptIncomeMovableAssets - be-tax-d-br-2012-04-30.xsd#d-br_ForeignBranchMember</phrase>
        </cell>
      </row>
      <row>
        <cell>
          <phrase>Andere vrijgestelde roerende inkomsten</phrase>
        </cell>
        <cell>
          <phrase>1639</phrase>
        </cell>
        <cell>
          <phrase>228</phrase>
        </cell>
        <cell>
          <phrase>tax-inc_OtherExemptIncomeMovableAssets - be-tax-d-br-2012-04-30.xsd#d-br_BelgianBranchMember</phrase>
        </cell>
        <cell>
          <phrase>tax-inc_OtherExemptIncomeMovableAssets - be-tax-d-br-2012-04-30.xsd#d-br_ForeignBranchMember</phrase>
        </cell>
      </row>
      <row>
        <cell>
          <phrase>Definitief belaste inkomsten en vrijgestelde roerende inkomsten voor aftrek kosten</phrase>
        </cell>
        <cell>
          <phrase>1640</phrase>
        </cell>
        <cell>
          <phrase>229</phrase>
        </cell>
        <cell>
          <phrase>tax-inc_GrossPEExemptIncomeMovableAssets - be-tax-d-br-2012-04-30.xsd#d-br_BelgianBranchMember</phrase>
        </cell>
        <cell>
          <phrase>tax-inc_GrossPEExemptIncomeMovableAssets - be-tax-d-br-2012-04-30.xsd#d-br_ForeignBranchMember</phrase>
        </cell>
      </row>
      <row>
        <cell>
          <phrase>Kosten</phrase>
        </cell>
        <cell>
          <phrase>1641</phrase>
        </cell>
        <cell>
          <phrase>230</phrase>
        </cell>
        <cell>
          <phrase>tax-inc_ExpensesSharesPEExemptIncomeMovableAssets - be-tax-d-br-2012-04-30.xsd#d-br_BelgianBranchMember</phrase>
        </cell>
        <cell>
          <phrase>tax-inc_ExpensesSharesPEExemptIncomeMovableAssets - be-tax-d-br-2012-04-30.xsd#d-br_ForeignBranchMember</phrase>
        </cell>
      </row>
      <row>
        <cell>
          <phrase>Definitief belaste inkomsten en vrijgestelde roerende inkomsten na aftrek kosten</phrase>
        </cell>
        <cell>
          <phrase>1642</phrase>
        </cell>
        <cell>
          <phrase>231</phrase>
        </cell>
        <cell>
          <phrase>tax-inc_NetPEExemptIncomeMovableAssets - be-tax-d-br-2012-04-30.xsd#d-br_BelgianBranchMember</phrase>
        </cell>
        <cell>
          <phrase>tax-inc_NetPEExemptIncomeMovableAssets - be-tax-d-br-2012-04-30.xsd#d-br_ForeignBranchMember</phrase>
        </cell>
      </row>
      <row>
        <cell>
          <phrase>Inkomsten uit niet-vergoede inbrengen in geval van fusie, splitsing of hiermee gelijkgestelde verrichtingen omdat de overnemende of verkrijgende vennootschap in het bezit is van aandelen van de overgenomen of gesplitste vennootschap of gelijkaardige verrichtingen in een andere EU-lidstaat</phrase>
        </cell>
        <cell>
          <phrase>1643</phrase>
        </cell>
        <cell>
          <phrase>232</phrase>
        </cell>
        <cell>
          <phrase>tax-inc_IncomeMergerAcquisitionRetainedSharesSimilarRegulationOtherEUMemberState - be-tax-d-br-2012-04-30.xsd#d-br_BelgianBranchMember</phrase>
        </cell>
        <cell>
          <phrase>tax-inc_IncomeMergerAcquisitionRetainedSharesSimilarRegulationOtherEUMemberState - be-tax-d-br-2012-04-30.xsd#d-br_ForeignBranchMember</phrase>
        </cell>
      </row>
      <row>
        <cell>
          <phrase>Vrijgestelde roerende inkomsten uit effecten van bepaalde herfinancieringsleningen</phrase>
        </cell>
        <cell>
          <phrase>1644</phrase>
        </cell>
        <cell>
          <phrase>233</phrase>
        </cell>
        <cell>
          <phrase>tax-inc_ExemptIncomeMovableAssetsRefinancingLoans - be-tax-d-br-2012-04-30.xsd#d-br_BelgianBranchMember</phrase>
        </cell>
        <cell>
          <phrase>tax-inc_ExemptIncomeMovableAssetsRefinancingLoans - be-tax-d-br-2012-04-30.xsd#d-br_ForeignBranchMember</phrase>
        </cell>
      </row>
      <row>
        <cell>
          <phrase>Definitief belaste inkomsten en vrijgestelde roerende inkomsten</phrase>
        </cell>
        <cell>
          <phrase>1650</phrase>
        </cell>
        <cell>
          <phrase>234</phrase>
        </cell>
        <cell>
          <phrase>tax-inc_DeductiblePEExemptIncomeMovableAssets - be-tax-d-br-2012-04-30.xsd#d-br_BelgianBranchMember</phrase>
        </cell>
        <cell>
          <phrase>tax-inc_DeductiblePEExemptIncomeMovableAssets - be-tax-d-br-2012-04-30.xsd#d-br_ForeignBranchMember</phrase>
        </cell>
      </row>
    </table>
    <phrase>Overdracht aftrek definitief belaste inkomsten</phrase>
    <table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" columns="4" width="100%" align="Center" widths="75;5;5;15" cellspacing="0" cellpadding="1">
      <row>
        <cell>
          <phrase>&amp;#160;HEADING</phrase>
        </cell>
        <cell>
          <phrase>oldcode</phrase>
        </cell>
        <cell>
          <phrase>code</phrase>
        </cell>
        <cell>
          <phrase>DURATION</phrase>
        </cell>
      </row>
      <row>
        <cell>
          <phrase>Saldo van de overgedragen aftrek definitief belaste inkomsten</phrase>
        </cell>
        <cell>
          <phrase>262</phrase>
        </cell>
        <cell>
          <phrase>1701</phrase>
        </cell>
        <cell>VALUE tax-inc_AccumulatedPEExemptIncomeMovableAssets - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Aftrek definitief belaste inkomsten van het belastbare tijdperk dat overdraagbaar is naar het volgende belastbare tijdperk</phrase>
        </cell>
        <cell>
          <phrase>263</phrase>
        </cell>
        <cell>
          <phrase>1702</phrase>
        </cell>
        <cell>VALUE tax-inc_CarryOverNextTaxPeriodInsufficientProfitsPEExemptIncomeMovableAssetsCurrentTaxPeriod - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Overgedragen aftrek definitief belaste inkomsten die werkelijk wordt afgetrokken van het belastbare tijdperk</phrase>
        </cell>
        <cell>
          <phrase>264</phrase>
        </cell>
        <cell>
          <phrase>1703</phrase>
        </cell>
        <cell>VALUE tax-inc_DeductionPEExemptIncomeMovableAssetsPreviousTaxPeriod - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Saldo van de aftrek definitief belaste inkomsten dat overdraagbaar is naar het volgende belastbare tijdperk</phrase>
        </cell>
        <cell>
          <phrase>265</phrase>
        </cell>
        <cell>
          <phrase>1704</phrase>
        </cell>
        <cell>VALUE tax-inc_CarryOverNextTaxPeriodPEExemptIncomeMovableAssets - D </cell>
      </row>
    </table>
    <phrase>Overdracht aftrek voor risicokapitaal</phrase>
    <table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" columns="4" width="100%" align="Center" widths="75;5;5;15" cellspacing="0" cellpadding="1">
      <row>
        <cell>
          <phrase>&amp;#160;HEADING</phrase>
        </cell>
        <cell>
          <phrase>oldcode</phrase>
        </cell>
        <cell>
          <phrase>code</phrase>
        </cell>
        <cell>
          <phrase>DURATION</phrase>
        </cell>
      </row>
      <row>
        <cell>
          <phrase>Saldo van de overgedragen aftrek voor risicokapitaal</phrase>
        </cell>
        <cell>
          <phrase>330</phrase>
        </cell>
        <cell>
          <phrase>1711</phrase>
        </cell>
        <cell>VALUE tax-inc_AccumulatedAllowanceCorporateEquity - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Saldo van de aftrek voor risicokapitaal dat overdraagbaar is naar het volgende belastbare tijdperk</phrase>
        </cell>
        <cell>
          <phrase>332</phrase>
        </cell>
        <cell>
          <phrase>1712</phrase>
        </cell>
        <cell>VALUE tax-inc_CarryOverNextTaxPeriodInsufficientProfitsAllowanceCorporateEquity - D </cell>
      </row>
    </table>
    <phrase>Compenseerbare verliezen</phrase>
    <table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" columns="4" width="100%" align="Center" widths="75;5;5;15" cellspacing="0" cellpadding="1">
      <row>
        <cell>
          <phrase>&amp;#160;HEADING</phrase>
        </cell>
        <cell>
          <phrase>oldcode</phrase>
        </cell>
        <cell>
          <phrase>code</phrase>
        </cell>
        <cell>
          <phrase>DURATION</phrase>
        </cell>
      </row>
      <row>
        <cell>
          <phrase>Saldo van de compenseerbare vorige verliezen</phrase>
        </cell>
        <cell>
          <phrase>235</phrase>
        </cell>
        <cell>
          <phrase>1721 N</phrase>
        </cell>
        <cell>VALUE tax-inc_CompensableTaxLosses - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Gecompenseerde verliezen</phrase>
        </cell>
        <cell>
          <phrase>106 105 236</phrase>
        </cell>
        <cell>
          <phrase>1436</phrase>
        </cell>
        <cell>VALUE tax-inc_CompensatedTaxLosses - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Verlies van het belastbare tijdperk</phrase>
        </cell>
        <cell>
          <phrase>237</phrase>
        </cell>
        <cell>
          <phrase>1722 N</phrase>
        </cell>
        <cell>VALUE tax-inc_LossCurrentTaxPeriod - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Verlies over te brengen naar het volgende belastbare tijdperk</phrase>
        </cell>
        <cell>
          <phrase>238</phrase>
        </cell>
        <cell>
          <phrase>1730 N</phrase>
        </cell>
        <cell>VALUE tax-inc_CarryOverTaxLosses - D </cell>
      </row>
    </table>
    <phrase>Tarief van de belasting</phrase>
    <table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" columns="4" width="100%" align="Center" widths="75;5;5;15" cellspacing="0" cellpadding="1">
      <row>
        <cell>
          <phrase>&amp;#160;HEADING</phrase>
        </cell>
        <cell>
          <phrase>oldcode</phrase>
        </cell>
        <cell>
          <phrase>code</phrase>
        </cell>
        <cell>
          <phrase>DURATION</phrase>
        </cell>
      </row>
      <row>
        <cell>
          <phrase>De vennootschap kan bij uw weten aanspraak maken op het verminderd tarief</phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>
          <phrase>1751</phrase>
        </cell>
        <cell>VALUE tax-inc_ExclusionReducedRate - D </cell>
      </row>
      <row>
        <cell>
          <phrase>De vennootschap is ofwel een vennootschap die krediet voor ambachtsoutillage mag verstrekken, ofwel een vennootschap voor huisvesting, belastbaar tegen het tarief van 5%</phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>
          <phrase>1752</phrase>
        </cell>
        <cell>VALUE tax-inc_CreditCorporationTradeEquipmentHousingCorporationTaxRate - D </cell>
      </row>
    </table>
    <phrase>Voorafbetalingen</phrase>
    <table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" columns="4" width="100%" align="Center" widths="75;5;5;15" cellspacing="0" cellpadding="1">
      <row>
        <cell>
          <phrase>&amp;#160;HEADING</phrase>
        </cell>
        <cell>
          <phrase>oldcode</phrase>
        </cell>
        <cell>
          <phrase>code</phrase>
        </cell>
        <cell>
          <phrase>DURATION</phrase>
        </cell>
      </row>
      <row>
        <cell>
          <phrase>Deze aangifte heeft betrekking op één van de eerste drie boekjaren vanaf de oprichting van de vennootschap die een kleine vennootschap is in de zin van het Wetboek van vennootschappen</phrase>
        </cell>
        <cell>
          <phrase>169</phrase>
        </cell>
        <cell>
          <phrase>1801</phrase>
        </cell>
        <cell>VALUE tax-inc_FirstThreeAccountingYearsSmallCompanyCorporationCode - D </cell>
      </row>
      <row>
        <cell colspan="4">
          <phrase>In aanmerking te nemen voorafbetalingen</phrase>
        </cell>
      </row>
      <row>
        <cell>
          <phrase>Voorafbetaling, eerste kwartaal</phrase>
        </cell>
        <cell>
          <phrase>170</phrase>
        </cell>
        <cell>
          <phrase>1811</phrase>
        </cell>
        <cell>VALUE tax-inc_PrepaymentFirstQuarter - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Voorafbetaling, tweede kwartaal</phrase>
        </cell>
        <cell>
          <phrase>171</phrase>
        </cell>
        <cell>
          <phrase>1812</phrase>
        </cell>
        <cell>VALUE tax-inc_PrepaymentSecondQuarter - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Voorafbetaling, derde kwartaal</phrase>
        </cell>
        <cell>
          <phrase>172</phrase>
        </cell>
        <cell>
          <phrase>1813</phrase>
        </cell>
        <cell>VALUE tax-inc_PrepaymentThirdQuarter - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Voorafbetaling, vierde kwartaal</phrase>
        </cell>
        <cell>
          <phrase>173</phrase>
        </cell>
        <cell>
          <phrase>1814</phrase>
        </cell>
        <cell>VALUE tax-inc_PrepaymentFourthQuarter - D </cell>
      </row>
      <row>
        <cell colspan="4">
          <phrase>Referentienummer verschillend van het ondernemingsnummer en toegekend door de Dienst Voorafbetalingen</phrase>
        </cell>
      </row>
      <row>
        <cell>
          <phrase>Eerste ander referentienummer</phrase>
        </cell>
        <cell>
          <phrase>176</phrase>
        </cell>
        <cell>
          <phrase>1821</phrase>
        </cell>
        <cell>VALUE tax-inc_PrepaymentReferenceNumberNotEntityIdentifierFirstOccurrence - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Tweede ander referentienummer</phrase>
        </cell>
        <cell>
          <phrase>177</phrase>
        </cell>
        <cell>
          <phrase>1822</phrase>
        </cell>
        <cell>VALUE tax-inc_PrepaymentReferenceNumberNotEntityIdentifierSecondOccurrence - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Derde ander referentienummer</phrase>
        </cell>
        <cell>
          <phrase>178</phrase>
        </cell>
        <cell>
          <phrase>1823</phrase>
        </cell>
        <cell>VALUE tax-inc_PrepaymentReferenceNumberNotEntityIdentifierThirdOccurrence - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Vierde ander referentienummer</phrase>
        </cell>
        <cell>
          <phrase>179</phrase>
        </cell>
        <cell>
          <phrase>1824</phrase>
        </cell>
        <cell>VALUE tax-inc_PrepaymentReferenceNumberNotEntityIdentifierFourthOccurrence - D </cell>
      </row>
    </table>
    <phrase>Verrekenbare voorheffingen</phrase>
    <table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" columns="4" width="100%" align="Center" widths="75;5;5;15" cellspacing="0" cellpadding="1">
      <row>
        <cell>
          <phrase>&amp;#160;HEADING</phrase>
        </cell>
        <cell>
          <phrase>oldcode</phrase>
        </cell>
        <cell>
          <phrase>code</phrase>
        </cell>
        <cell>
          <phrase>DURATION</phrase>
        </cell>
      </row>
      <row>
        <cell colspan="4">
          <phrase>Niet-terugbetaalbare voorheffingen</phrase>
        </cell>
      </row>
      <row>
        <cell>
          <phrase>Fictieve roerende voorheffing</phrase>
        </cell>
        <cell>
          <phrase>182</phrase>
        </cell>
        <cell>
          <phrase>1831</phrase>
        </cell>
        <cell>VALUE tax-inc_NonRepayableFictiousWitholdingTax - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Forfaitair gedeelte van buitenlandse belasting</phrase>
        </cell>
        <cell>
          <phrase>183</phrase>
        </cell>
        <cell>
          <phrase>1832</phrase>
        </cell>
        <cell>VALUE tax-inc_NonRepayableLumpSumForeignTaxes - D </cell>
      </row>
      <row>
        <cell>
          <phrase>In beginsel verrekenbaar belastingkrediet voor onderzoek en ontwikkeling</phrase>
        </cell>
        <cell>
          <phrase>184</phrase>
        </cell>
        <cell>
          <phrase>1833</phrase>
        </cell>
        <cell>VALUE tax-inc_TaxCreditResearchDevelopment - D </cell>
      </row>
      <row>
        <cell colspan="4">
          <phrase>Terugbetaalbare voorheffingen</phrase>
        </cell>
      </row>
      <row>
        <cell>
          <phrase>Werkelijke of fictieve roerende voorheffing op Belgische definitief belaste en vrijgestelde roerende inkomsten uit aandelen, andere dan definitief belaste liquidatieboni of boni bij verkrijging van eigen aandelen</phrase>
        </cell>
        <cell>
          <phrase>187</phrase>
        </cell>
        <cell>
          <phrase>1841</phrase>
        </cell>
        <cell>VALUE tax-inc_RepayableRealFictitiousWithholdingTaxOtherSharesPEExemptIncomeMovableAssetsBelgium - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Roerende voorheffing op definitief belaste liquidatieboni of boni bij verkrijging van eigen aandelen</phrase>
        </cell>
        <cell>
          <phrase>188</phrase>
        </cell>
        <cell>
          <phrase>1842</phrase>
        </cell>
        <cell>VALUE tax-inc_RepayableWithholdingTaxPEExemptionLiquidationBonusesBonusesAcquisitionOwnSharesBelgiumForeign - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Roerende voorheffing op buitenlandse definitief belaste inkomsten, andere dan definitief belaste liquidatieboni of boni bij verkrijging van eigen aandelen</phrase>
        </cell>
        <cell>
          <phrase>190</phrase>
        </cell>
        <cell>
          <phrase>1843</phrase>
        </cell>
        <cell>VALUE tax-inc_RepayableWithholdingTaxOtherPEForeign - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Roerende voorheffing op andere liquidatieboni of boni bij verkrijging van eigen aandelen</phrase>
        </cell>
        <cell>
          <phrase>192</phrase>
        </cell>
        <cell>
          <phrase>1844</phrase>
        </cell>
        <cell>VALUE tax-inc_RepayableWithholdingTaxOtherLiquidationBonusesBonusesAcquisitionOwnShares - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Roerende voorheffing op andere dividenden</phrase>
        </cell>
        <cell>
          <phrase>194</phrase>
        </cell>
        <cell>
          <phrase>1845</phrase>
        </cell>
        <cell>VALUE tax-inc_RepayableWithholdingTaxOtherDividends - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Andere terugbetaalbare roerende voorheffing</phrase>
        </cell>
        <cell>
          <phrase>195</phrase>
        </cell>
        <cell>
          <phrase>1846</phrase>
        </cell>
        <cell>VALUE tax-inc_OtherRepayableWithholdingTaxes - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Belastingkrediet voor onderzoek en ontwikkeling dat voor het huidig belastbare tijdperk terugbetaalbaar is</phrase>
        </cell>
        <cell>
          <phrase>198</phrase>
        </cell>
        <cell>
          <phrase>1850</phrase>
        </cell>
        <cell>VALUE tax-inc_TaxCreditResearchDevelopmentClearablePreviousTaxPeriodsRepayableCurrentAssessmentYear - D </cell>
      </row>
    </table>
    <phrase>Tax shelter</phrase>
    <table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" columns="4" width="100%" align="Center" widths="75;5;5;15" cellspacing="0" cellpadding="1">
      <row>
        <cell>
          <phrase>&amp;#160;HEADING</phrase>
        </cell>
        <cell>
          <phrase>oldcode</phrase>
        </cell>
        <cell>
          <phrase>code</phrase>
        </cell>
        <cell>
          <phrase>DURATION</phrase>
        </cell>
      </row>
      <row>
        <cell>
          <phrase>De vennootschap is een binnenlandse vennootschap voor de productie van audiovisuele werken, die als voornaamste doel de ontwikkeling en de productie van audiovisuele werken heeft en niet zijnde een televisieomroep of een onderneming die verbonden is met Belgische of buitenlandse televisieomroepen, die een raamovereenkomst voor de productie van een erkend Belgisch audiovisueel werk heeft gesloten</phrase>
        </cell>
        <cell>
          <phrase>363</phrase>
        </cell>
        <cell>
          <phrase>1861</phrase>
        </cell>
        <cell>VALUE tax-inc_BelgianCorporationAudiovisualWorksTaxShelterAgreement - D </cell>
      </row>
    </table>
    <phrase>Grootte van de vennootschap in de zin van het Wetboek van Vennootschappen</phrase>
    <phrase>Inlichtingen ter beoordeling kleine vennootschap</phrase>
    <phrase>Gegevens met betrekking tot het belastbaar tijdperk</phrase>
    <table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" columns="4" width="100%" align="Center" widths="75;5;5;15" cellspacing="0" cellpadding="1">
      <row>
        <cell>
          <phrase>&amp;#160;HEADING</phrase>
        </cell>
        <cell>
          <phrase>oldcode</phrase>
        </cell>
        <cell>
          <phrase>code</phrase>
        </cell>
        <cell>
          <phrase>DURATION</phrase>
        </cell>
      </row>
      <row>
        <cell>
          <phrase>De vennootschap is verbonden met één of meerdere andere vennootschappen in de zin van het Wetboek van Vennootschappen</phrase>
        </cell>
        <cell>
          <phrase>268</phrase>
        </cell>
        <cell>
          <phrase>1871</phrase>
        </cell>
        <cell>VALUE tax-inc_AssociatedCompanyCorporationCodeCurrentTaxPeriod - D </cell>
      </row>
      <row>
        <cell>
          <phrase>De gegevens op niet-geconsolideerde basis vermelden, tenzij de vennootschap verbonden is met één of meerdere andere vennootschappen in de zin van het Wetboek van Vennootschappen, dan die gegevens op geconsolideerde basis vermelden</phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>VALUE tax-inc_ConsolidatedDataIfApplicableTitle - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Jaargemiddelde van het personeelsbestand</phrase>
        </cell>
        <cell>
          <phrase>267</phrase>
        </cell>
        <cell>
          <phrase>1872</phrase>
        </cell>
        <cell>VALUE tax-inc_AnnualWorkForceAverageCorporationCodeCurrentTaxPeriod - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Jaaromzet, exclusief btw</phrase>
        </cell>
        <cell>
          <phrase>261</phrase>
        </cell>
        <cell>
          <phrase>1873</phrase>
        </cell>
        <cell>VALUE tax-inc_AnnualTurnoverExcludingVATCorporationCodeCurrentTaxPeriod - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Balanstotaal</phrase>
        </cell>
        <cell>
          <phrase>251</phrase>
        </cell>
        <cell>
          <phrase>1874</phrase>
        </cell>
        <cell>VALUE tax-inc_BalanceSheetTotalCorporationCodeCurrentTaxPeriod - D </cell>
      </row>
    </table>
    <phrase>Diverse bescheiden en opgaven</phrase>
  </xsl:template>
  <xsl:template match="BizTaxDataContract" mode="TAB_275.1.B">
    <phrase>Jaarrekening (balans, resultatenrekening en eventuele toelichting)</phrase>
    <table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" columns="4" width="100%" align="Center" widths="75;5;5;15" cellspacing="0" cellpadding="1">
      <row>
        <cell>
          <phrase>&amp;#160;HEADING</phrase>
        </cell>
        <cell>
          <phrase>oldcode</phrase>
        </cell>
        <cell>
          <phrase>code</phrase>
        </cell>
        <cell>
          <phrase>DURATION</phrase>
        </cell>
      </row>
      <row>
        <cell>
          <phrase>Bij te voegen, indien de belastingplichtige niet verplicht is deze neer te leggen bij de Balanscentrale van de Nationale Bank</phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>VALUE tax-inc_AnnualAccountsMandatoryAnnexeFilingCentralBalanceSheetOfficeNotRequired - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Jaarrekening (balans, resultatenrekening en eventuele toelichting)</phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>VALUE tax-inc_StatutoryAccounts - D </cell>
      </row>
    </table>
    <phrase>Verslagen aan en besluiten van de algemene vergadering</phrase>
    <table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" columns="4" width="100%" align="Center" widths="75;5;5;15" cellspacing="0" cellpadding="1">
      <row>
        <cell>
          <phrase>&amp;#160;HEADING</phrase>
        </cell>
        <cell>
          <phrase>oldcode</phrase>
        </cell>
        <cell>
          <phrase>code</phrase>
        </cell>
        <cell>
          <phrase>DURATION</phrase>
        </cell>
      </row>
      <row>
        <cell>
          <phrase>Verplicht bij te voegen</phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>VALUE tax-inc_GeneralMeetingMinutesDecisionsMandatoryAnnexe - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Verslagen aan en besluiten van de algemene vergadering</phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>VALUE tax-inc_GeneralMeetingMinutesDecisions - D </cell>
      </row>
    </table>
    <phrase>De documenten in geval van toepassing van de vrijstelling van de winst voortvloeiend uit de homologatie van een reorganisatieplan en uit de vaststelling van een minnelijk akkoord</phrase>
    <table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" columns="4" width="100%" align="Center" widths="75;5;5;15" cellspacing="0" cellpadding="1">
      <row>
        <cell>
          <phrase>&amp;#160;HEADING</phrase>
        </cell>
        <cell>
          <phrase>oldcode</phrase>
        </cell>
        <cell>
          <phrase>code</phrase>
        </cell>
        <cell>
          <phrase>DURATION</phrase>
        </cell>
      </row>
      <row>
        <cell>
          <phrase>Verplicht bij te voegen</phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>VALUE tax-inc_ExemptionProfitHomologationReorganizationPlanAmicableSettlementDocumentsMandatoryAnnexe - D </cell>
      </row>
      <row>
        <cell>
          <phrase>De documenten in geval van toepassing van de vrijstelling van de winst voortvloeiend uit de homologatie van een reorganisatieplan en uit de vaststelling van een minnelijk akkoord</phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>VALUE tax-inc_ExemptionProfitHomologationReorganizationPlanAmicableSettlementDocuments - D </cell>
      </row>
    </table>
    <phrase>Interne jaarrekening</phrase>
    <table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" columns="4" width="100%" align="Center" widths="75;5;5;15" cellspacing="0" cellpadding="1">
      <row>
        <cell>
          <phrase>&amp;#160;HEADING</phrase>
        </cell>
        <cell>
          <phrase>oldcode</phrase>
        </cell>
        <cell>
          <phrase>code</phrase>
        </cell>
        <cell>
          <phrase>DURATION</phrase>
        </cell>
      </row>
      <row>
        <cell>
          <phrase>Optioneel bij te voegen</phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>VALUE tax-inc_InternalStatutoryAccountsNonMandatoryAnnexe - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Interne jaarrekening</phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>VALUE tax-inc_InternalStatutoryAccounts - D </cell>
      </row>
    </table>
    <phrase>Afschrijvingstabellen</phrase>
    <phrase>Afschrijvingstabellen</phrase>
    <table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" columns="4" width="100%" align="Center" widths="75;5;5;15" cellspacing="0" cellpadding="1">
      <row>
        <cell>
          <phrase>&amp;#160;HEADING</phrase>
        </cell>
        <cell>
          <phrase>oldcode</phrase>
        </cell>
        <cell>
          <phrase>code</phrase>
        </cell>
        <cell>
          <phrase>DURATION</phrase>
        </cell>
      </row>
      <row>
        <cell>
          <phrase>Afschrijvingstabel niet-gestandaardiseerd</phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>VALUE tax-inc_DepreciationTableNonStructured - D </cell>
      </row>
    </table>
    <phrase>Detail verworpen uitgaven</phrase>
    <phrase>Niet-aftrekbare belastingen</phrase>
    <table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" columns="4" width="100%" align="Center" widths="75;5;5;15" cellspacing="0" cellpadding="1">
      <row>
        <cell>
          <phrase>&amp;#160;HEADING</phrase>
        </cell>
        <cell>
          <phrase>oldcode</phrase>
        </cell>
        <cell>
          <phrase>code</phrase>
        </cell>
        <cell>
          <phrase>DURATION</phrase>
        </cell>
      </row>
      <row>
        <cell colspan="4">
          <phrase>Niet-terugbetaalbare voorheffingen</phrase>
        </cell>
      </row>
      <row>
        <cell>
          <phrase>Fictieve roerende voorheffing</phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>VALUE tax-inc_NonRepayableFictiousWitholdingTaxNonDeductibleTaxes - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Forfaitair gedeelte van buitenlandse belasting</phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>VALUE tax-inc_NonRepayableLumpSumForeignTaxesNonDeductibleTaxes - D </cell>
      </row>
      <row>
        <cell colspan="4">
          <phrase>Terugbetaalbare voorheffingen</phrase>
        </cell>
      </row>
      <row>
        <cell>
          <phrase>Werkelijke of fictieve roerende voorheffing op Belgische definitief belaste en vrijgestelde roerende inkomsten uit aandelen, andere dan definitief belaste liquidatieboni of boni bij verkrijging van eigen aandelen</phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>VALUE tax-inc_RepayableRealFictitiousWithholdingTaxOtherSharesPEExemptIncomeMovableAssetsBelgiumNonDeductibleTaxes - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Roerende voorheffing op definitief belaste liquidatieboni of boni bij verkrijging van eigen aandelen</phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>VALUE tax-inc_RepayableWithholdingTaxPEExemptionLiquidationBonusesBonusesAcquisitionOwnSharesBelgiumForeignNonDeductibleTaxes - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Roerende voorheffing op buitenlandse definitief belaste inkomsten, andere dan definitief belaste liquidatieboni of boni bij verkrijging van eigen aandelen</phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>VALUE tax-inc_RepayableWithholdingTaxOtherPEForeignNonDeductibleTaxes - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Roerende voorheffing op andere liquidatieboni of boni bij verkrijging van eigen aandelen</phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>VALUE tax-inc_RepayableWithholdingTaxOtherLiquidationBonusesBonusesAcquisitionOwnSharesNonDeductibleTaxes - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Roerende voorheffing op andere dividenden</phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>VALUE tax-inc_RepayableWithholdingTaxOtherDividendsNonDeductibleTaxes - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Andere terugbetaalbare roerende voorheffing</phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>VALUE tax-inc_OtherRepayableWithholdingTaxesNonDeductibleTaxes - D </cell>
      </row>
      <row>
        <cell colspan="4">
          <phrase>In aanmerking te nemen voorafbetalingen</phrase>
        </cell>
      </row>
      <row>
        <cell>
          <phrase>Voorafbetaling, eerste kwartaal</phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>VALUE tax-inc_PrepaymentFirstQuarterNonDeductibleTaxes - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Voorafbetaling, tweede kwartaal</phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>VALUE tax-inc_PrepaymentSecondQuarterNonDeductibleTaxes - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Voorafbetaling, derde kwartaal</phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>VALUE tax-inc_PrepaymentThirdQuarterNonDeductibleTaxes - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Voorafbetaling, vierde kwartaal</phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>VALUE tax-inc_PrepaymentFourthQuarterNonDeductibleTaxes - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Vennnootschapsbelasting inclusief vermeerdering, verhoging en nalatigheidsinteresten</phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>VALUE tax-inc_CorporateIncomeTaxIncludingPenaltySurchargeDefaultInterests - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Geraamd bedrag der belastingschulden</phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>VALUE tax-inc_EstimatedCorporateIncomeTaxDebt - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Door de vennootschap gedragen roerende voorheffing op uitgekeerde dividenden</phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>VALUE tax-inc_WithholdingTaxDividendsPaid - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Door de vennootschap gedragen roerende voorheffing op andere uitbetaalde inkomsten</phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>VALUE tax-inc_WithholdingTaxOtherIncomePaid - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Andere niet-aftrekbare belastingen</phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>VALUE tax-inc_OtherNonDeductibleTaxes - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Niet-aftrekbare belastingen voor aftrek van terugbetalingen en regulariseringen</phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>VALUE tax-inc_NonDeductibleTaxesNoReimbursementsRegularizations - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Terugbetalingen en regulariseringen</phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>VALUE tax-inc_ReimbursementsRegularizationsNonDeductibleTaxes - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Niet-aftrekbare belastingen</phrase>
        </cell>
        <cell>
          <phrase>029</phrase>
        </cell>
        <cell>
          <phrase>1201</phrase>
        </cell>
        <cell>VALUE tax-inc_NonDeductibleTaxes - D </cell>
      </row>
    </table>
    <phrase>Gewestelijke belastingen, heffingen en retributies</phrase>
    <table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" columns="4" width="100%" align="Center" widths="75;5;5;15" cellspacing="0" cellpadding="1">
      <row>
        <cell>
          <phrase>&amp;#160;HEADING</phrase>
        </cell>
        <cell>
          <phrase>oldcode</phrase>
        </cell>
        <cell>
          <phrase>code</phrase>
        </cell>
        <cell>
          <phrase>DURATION</phrase>
        </cell>
      </row>
      <row>
        <cell>
          <phrase>Gewestelijke belastingen, heffingen en retributies</phrase>
        </cell>
        <cell>
          <phrase>028</phrase>
        </cell>
        <cell>
          <phrase>1202</phrase>
        </cell>
        <cell>VALUE tax-inc_NonDeductibleRegionalTaxesDutiesRetributions - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Uitleg</phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>VALUE tax-inc_DetailNonDeductibleRegionalTaxesDutiesRetributions - D </cell>
      </row>
    </table>
    <phrase>Geldboeten, verbeurdverklaringen en straffen van alle aard</phrase>
    <table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" columns="4" width="100%" align="Center" widths="75;5;5;15" cellspacing="0" cellpadding="1">
      <row>
        <cell>
          <phrase>&amp;#160;HEADING</phrase>
        </cell>
        <cell>
          <phrase>oldcode</phrase>
        </cell>
        <cell>
          <phrase>code</phrase>
        </cell>
        <cell>
          <phrase>DURATION</phrase>
        </cell>
      </row>
      <row>
        <cell>
          <phrase>Geldboeten, verbeurdverklaringen en straffen van alle aard</phrase>
        </cell>
        <cell>
          <phrase>030</phrase>
        </cell>
        <cell>
          <phrase>1203</phrase>
        </cell>
        <cell>VALUE tax-inc_NonDeductibleFinesConfiscationsPenaltiesAllKind - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Uitleg</phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>VALUE tax-inc_DetailNonDeductibleFinesConfiscationsPenaltiesAllKind - D </cell>
      </row>
    </table>
    <phrase>Niet-aftrekbare pensioenen, kapitalen, werkgeversbijdragen en -premies</phrase>
    <table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" columns="4" width="100%" align="Center" widths="75;5;5;15" cellspacing="0" cellpadding="1">
      <row>
        <cell>
          <phrase>&amp;#160;HEADING</phrase>
        </cell>
        <cell>
          <phrase>oldcode</phrase>
        </cell>
        <cell>
          <phrase>code</phrase>
        </cell>
        <cell>
          <phrase>DURATION</phrase>
        </cell>
      </row>
      <row>
        <cell>
          <phrase>Niet-aftrekbare pensioenen, kapitalen, werkgeversbijdragen en -premies</phrase>
        </cell>
        <cell>
          <phrase>031</phrase>
        </cell>
        <cell>
          <phrase>1204</phrase>
        </cell>
        <cell>VALUE tax-inc_NonDeductiblePensionsCapitalEmployerContributionsEmployerPremiums - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Uitleg</phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>VALUE tax-inc_DetailNonDeductiblePensionsCapitalEmployerContributionsEmployerPremiums - D </cell>
      </row>
    </table>
    <phrase>Autokosten ten belope van een gedeelte van het voordeel van alle aard</phrase>
    <table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" columns="4" width="100%" align="Center" widths="75;5;5;15" cellspacing="0" cellpadding="1">
      <row>
        <cell>
          <phrase>&amp;#160;HEADING</phrase>
        </cell>
        <cell>
          <phrase>oldcode</phrase>
        </cell>
        <cell>
          <phrase>code</phrase>
        </cell>
        <cell>
          <phrase>DURATION</phrase>
        </cell>
      </row>
      <row>
        <cell>
          <phrase>Autokosten ten belope van een gedeelte van het voordeel van alle aard</phrase>
        </cell>
        <cell>
          <phrase>022 074</phrase>
        </cell>
        <cell>
          <phrase>1206</phrase>
        </cell>
        <cell>VALUE tax-inc_NonDeductibleCarExpensesPartBenefitsAllKind - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Uitleg</phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>VALUE tax-inc_DetailNonDeductibleCarExpensesPartBenefitsAllKind - D </cell>
      </row>
    </table>
    <phrase>Kosten voor niet-specifieke beroepskledij</phrase>
    <table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" columns="4" width="100%" align="Center" widths="75;5;5;15" cellspacing="0" cellpadding="1">
      <row>
        <cell>
          <phrase>&amp;#160;HEADING</phrase>
        </cell>
        <cell>
          <phrase>oldcode</phrase>
        </cell>
        <cell>
          <phrase>code</phrase>
        </cell>
        <cell>
          <phrase>DURATION</phrase>
        </cell>
      </row>
      <row>
        <cell>
          <phrase>Kosten voor niet-specifieke beroepskledij</phrase>
        </cell>
        <cell>
          <phrase>034</phrase>
        </cell>
        <cell>
          <phrase>1209</phrase>
        </cell>
        <cell>VALUE tax-inc_NonDeductibleNonSpecificProfessionalClothsExpenses - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Uitleg</phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>VALUE tax-inc_DetailNonDeductibleNonSpecificProfessionalClothsExpenses - D </cell>
      </row>
    </table>
    <phrase>Overdreven interesten</phrase>
    <table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" columns="4" width="100%" align="Center" widths="75;5;5;15" cellspacing="0" cellpadding="1">
      <row>
        <cell>
          <phrase>&amp;#160;HEADING</phrase>
        </cell>
        <cell>
          <phrase>oldcode</phrase>
        </cell>
        <cell>
          <phrase>code</phrase>
        </cell>
        <cell>
          <phrase>DURATION</phrase>
        </cell>
      </row>
      <row>
        <cell>
          <phrase>Overdreven interesten</phrase>
        </cell>
        <cell>
          <phrase>035</phrase>
        </cell>
        <cell>
          <phrase>1210</phrase>
        </cell>
        <cell>VALUE tax-inc_ExaggeratedInterests - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Uitleg</phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>VALUE tax-inc_DetailExaggeratedInterests - D </cell>
      </row>
    </table>
    <phrase>Interesten met betrekking tot een gedeelte van bepaalde leningen</phrase>
    <table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" columns="4" width="100%" align="Center" widths="75;5;5;15" cellspacing="0" cellpadding="1">
      <row>
        <cell>
          <phrase>&amp;#160;HEADING</phrase>
        </cell>
        <cell>
          <phrase>oldcode</phrase>
        </cell>
        <cell>
          <phrase>code</phrase>
        </cell>
        <cell>
          <phrase>DURATION</phrase>
        </cell>
      </row>
      <row>
        <cell>
          <phrase>Interesten met betrekking tot een gedeelte van bepaalde leningen</phrase>
        </cell>
        <cell>
          <phrase>036</phrase>
        </cell>
        <cell>
          <phrase>1211</phrase>
        </cell>
        <cell>VALUE tax-inc_NonDeductibleParticularPortionInterestsLoans - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Uitleg</phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>VALUE tax-inc_DetailNonDeductibleParticularPortionInterestsLoans - D </cell>
      </row>
    </table>
    <phrase>Abnormale of goedgunstige voordelen</phrase>
    <table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" columns="4" width="100%" align="Center" widths="75;5;5;15" cellspacing="0" cellpadding="1">
      <row>
        <cell>
          <phrase>&amp;#160;HEADING</phrase>
        </cell>
        <cell>
          <phrase>oldcode</phrase>
        </cell>
        <cell>
          <phrase>code</phrase>
        </cell>
        <cell>
          <phrase>DURATION</phrase>
        </cell>
      </row>
      <row>
        <cell>
          <phrase>Abnormale of goedgunstige voordelen</phrase>
        </cell>
        <cell>
          <phrase>037</phrase>
        </cell>
        <cell>
          <phrase>1212</phrase>
        </cell>
        <cell>VALUE tax-inc_AbnormalBenevolentAdvantages - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Uitleg</phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>VALUE tax-inc_DetailAbnormalBenevolentAdvantages - D </cell>
      </row>
    </table>
    <phrase>Sociale voordelen</phrase>
    <table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" columns="4" width="100%" align="Center" widths="75;5;5;15" cellspacing="0" cellpadding="1">
      <row>
        <cell>
          <phrase>&amp;#160;HEADING</phrase>
        </cell>
        <cell>
          <phrase>oldcode</phrase>
        </cell>
        <cell>
          <phrase>code</phrase>
        </cell>
        <cell>
          <phrase>DURATION</phrase>
        </cell>
      </row>
      <row>
        <cell>
          <phrase>Sociale voordelen</phrase>
        </cell>
        <cell>
          <phrase>038</phrase>
        </cell>
        <cell>
          <phrase>1214</phrase>
        </cell>
        <cell>VALUE tax-inc_NonDeductibleSocialAdvantages - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Uitleg</phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>VALUE tax-inc_DetailNonDeductibleSocialAdvantages - D </cell>
      </row>
    </table>
    <phrase>Voordelen uit maaltijd-, sport-, cultuur- of ecocheques</phrase>
    <table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" columns="4" width="100%" align="Center" widths="75;5;5;15" cellspacing="0" cellpadding="1">
      <row>
        <cell>
          <phrase>&amp;#160;HEADING</phrase>
        </cell>
        <cell>
          <phrase>oldcode</phrase>
        </cell>
        <cell>
          <phrase>code</phrase>
        </cell>
        <cell>
          <phrase>DURATION</phrase>
        </cell>
      </row>
      <row>
        <cell>
          <phrase>Voordelen uit maaltijd-, sport-, cultuur- of ecocheques</phrase>
        </cell>
        <cell>
          <phrase>023</phrase>
        </cell>
        <cell>
          <phrase>1215</phrase>
        </cell>
        <cell>VALUE tax-inc_NonDeductibleAdvantagesLuncheonSportsCultureEcoVouchers - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Uitleg</phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>VALUE tax-inc_DetailNonDeductibleAdvantagesLuncheonSportsCultureEcoVouchers - D </cell>
      </row>
    </table>
    <phrase>Liberaliteiten</phrase>
    <table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" columns="4" width="100%" align="Center" widths="75;5;5;15" cellspacing="0" cellpadding="1">
      <row>
        <cell>
          <phrase>&amp;#160;HEADING</phrase>
        </cell>
        <cell>
          <phrase>oldcode</phrase>
        </cell>
        <cell>
          <phrase>code</phrase>
        </cell>
        <cell>
          <phrase>DURATION</phrase>
        </cell>
      </row>
      <row>
        <cell>
          <phrase>Liberaliteiten</phrase>
        </cell>
        <cell>
          <phrase>039</phrase>
        </cell>
        <cell>
          <phrase>1216</phrase>
        </cell>
        <cell>VALUE tax-inc_Liberalities - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Uitleg</phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>VALUE tax-inc_DetailLiberalities - D </cell>
      </row>
    </table>
    <phrase>Waardeverminderingen en minderwaarden op aandelen</phrase>
    <table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" columns="4" width="100%" align="Center" widths="75;5;5;15" cellspacing="0" cellpadding="1">
      <row>
        <cell>
          <phrase>&amp;#160;HEADING</phrase>
        </cell>
        <cell>
          <phrase>oldcode</phrase>
        </cell>
        <cell>
          <phrase>code</phrase>
        </cell>
        <cell>
          <phrase>DURATION</phrase>
        </cell>
      </row>
      <row>
        <cell>
          <phrase>Waardeverminderingen en minderwaarden op aandelen</phrase>
        </cell>
        <cell>
          <phrase>040</phrase>
        </cell>
        <cell>
          <phrase>1217</phrase>
        </cell>
        <cell>VALUE tax-inc_WriteDownsLossValuesShares - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Uitleg</phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>VALUE tax-inc_DetailWriteDownsLossValuesShares - D </cell>
      </row>
    </table>
    <phrase>Terugnemingen van vroegere vrijstellingen</phrase>
    <table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" columns="4" width="100%" align="Center" widths="75;5;5;15" cellspacing="0" cellpadding="1">
      <row>
        <cell>
          <phrase>&amp;#160;HEADING</phrase>
        </cell>
        <cell>
          <phrase>oldcode</phrase>
        </cell>
        <cell>
          <phrase>code</phrase>
        </cell>
        <cell>
          <phrase>DURATION</phrase>
        </cell>
      </row>
      <row>
        <cell>
          <phrase>Terugnemingen van vroegere vrijstellingen</phrase>
        </cell>
        <cell>
          <phrase>041</phrase>
        </cell>
        <cell>
          <phrase>1218</phrase>
        </cell>
        <cell>VALUE tax-inc_ReversalPreviousExemptions - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Uitleg</phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>VALUE tax-inc_DetailReversalPreviousExemptions - D </cell>
      </row>
    </table>
    <phrase>Werknemersparticipatie</phrase>
    <table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" columns="4" width="100%" align="Center" widths="75;5;5;15" cellspacing="0" cellpadding="1">
      <row>
        <cell>
          <phrase>&amp;#160;HEADING</phrase>
        </cell>
        <cell>
          <phrase>oldcode</phrase>
        </cell>
        <cell>
          <phrase>code</phrase>
        </cell>
        <cell>
          <phrase>DURATION</phrase>
        </cell>
      </row>
      <row>
        <cell>
          <phrase>Werknemersparticipatie</phrase>
        </cell>
        <cell>
          <phrase>043 072</phrase>
        </cell>
        <cell>
          <phrase>1219</phrase>
        </cell>
        <cell>VALUE tax-inc_EmployeeParticipation - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Uitleg</phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>VALUE tax-inc_DetailEmployeeParticipation - D </cell>
      </row>
    </table>
    <phrase>Vergoedingen ontbrekende coupon</phrase>
    <table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" columns="4" width="100%" align="Center" widths="75;5;5;15" cellspacing="0" cellpadding="1">
      <row>
        <cell>
          <phrase>&amp;#160;HEADING</phrase>
        </cell>
        <cell>
          <phrase>oldcode</phrase>
        </cell>
        <cell>
          <phrase>code</phrase>
        </cell>
        <cell>
          <phrase>DURATION</phrase>
        </cell>
      </row>
      <row>
        <cell>
          <phrase>Vergoedingen ontbrekende coupon</phrase>
        </cell>
        <cell>
          <phrase>026</phrase>
        </cell>
        <cell>
          <phrase>1220</phrase>
        </cell>
        <cell>VALUE tax-inc_IndemnityMissingCoupon - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Uitleg</phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>VALUE tax-inc_DetailIndemnityMissingCoupon - D </cell>
      </row>
    </table>
    <phrase>Kosten tax shelter erkende audiovisuele werken</phrase>
    <table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" columns="4" width="100%" align="Center" widths="75;5;5;15" cellspacing="0" cellpadding="1">
      <row>
        <cell>
          <phrase>&amp;#160;HEADING</phrase>
        </cell>
        <cell>
          <phrase>oldcode</phrase>
        </cell>
        <cell>
          <phrase>code</phrase>
        </cell>
        <cell>
          <phrase>DURATION</phrase>
        </cell>
      </row>
      <row>
        <cell>
          <phrase>Kosten tax shelter erkende audiovisuele werken</phrase>
        </cell>
        <cell>
          <phrase>027</phrase>
        </cell>
        <cell>
          <phrase>1221</phrase>
        </cell>
        <cell>VALUE tax-inc_ExpensesTaxShelterAuthorisedAudiovisualWork - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Uitleg</phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>VALUE tax-inc_DetailExpensesTaxShelterAuthorisedAudiovisualWork - D </cell>
      </row>
    </table>
    <phrase>Gewestelijke premies en kapitaal- en interestsubsidies</phrase>
    <table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" columns="4" width="100%" align="Center" widths="75;5;5;15" cellspacing="0" cellpadding="1">
      <row>
        <cell>
          <phrase>&amp;#160;HEADING</phrase>
        </cell>
        <cell>
          <phrase>oldcode</phrase>
        </cell>
        <cell>
          <phrase>code</phrase>
        </cell>
        <cell>
          <phrase>DURATION</phrase>
        </cell>
      </row>
      <row>
        <cell>
          <phrase>Gewestelijke premies en kapitaal- en interestsubsidies</phrase>
        </cell>
        <cell>
          <phrase>024</phrase>
        </cell>
        <cell>
          <phrase>1222</phrase>
        </cell>
        <cell>VALUE tax-inc_RegionalPremiumCapitalSubsidiesInterestSubsidies - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Uitleg</phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>VALUE tax-inc_DetailRegionalPremiumCapitalSubsidiesInterestSubsidies - D </cell>
      </row>
    </table>
    <phrase>Niet-aftrekbare betalingen naar bepaalde Staten</phrase>
    <table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" columns="4" width="100%" align="Center" widths="75;5;5;15" cellspacing="0" cellpadding="1">
      <row>
        <cell>
          <phrase>&amp;#160;HEADING</phrase>
        </cell>
        <cell>
          <phrase>oldcode</phrase>
        </cell>
        <cell>
          <phrase>code</phrase>
        </cell>
        <cell>
          <phrase>DURATION</phrase>
        </cell>
      </row>
      <row>
        <cell>
          <phrase>Niet-aftrekbare betalingen naar bepaalde Staten</phrase>
        </cell>
        <cell>
          <phrase>054</phrase>
        </cell>
        <cell>
          <phrase>1223</phrase>
        </cell>
        <cell>VALUE tax-inc_NonDeductiblePaymentsCertainStates - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Uitleg</phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>VALUE tax-inc_DetailNonDeductiblePaymentsCertainStates - D </cell>
      </row>
    </table>
    <phrase>Andere verworpen uitgaven</phrase>
    <table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" columns="4" width="100%" align="Center" widths="75;5;5;15" cellspacing="0" cellpadding="1">
      <row>
        <cell>
          <phrase>&amp;#160;HEADING</phrase>
        </cell>
        <cell>
          <phrase>oldcode</phrase>
        </cell>
        <cell>
          <phrase>code</phrase>
        </cell>
        <cell>
          <phrase>DURATION</phrase>
        </cell>
      </row>
      <row>
        <cell>
          <phrase>Andere verworpen uitgaven</phrase>
        </cell>
        <cell>
          <phrase>042</phrase>
        </cell>
        <cell>
          <phrase>1239</phrase>
        </cell>
        <cell>VALUE tax-inc_OtherDisallowedExpenses - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Uitleg</phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>VALUE tax-inc_DetailOtherDisallowedExpenses - D </cell>
      </row>
    </table>
    <phrase>Andere</phrase>
    <table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" columns="4" width="100%" align="Center" widths="75;5;5;15" cellspacing="0" cellpadding="1">
      <row>
        <cell>
          <phrase>&amp;#160;HEADING</phrase>
        </cell>
        <cell>
          <phrase>oldcode</phrase>
        </cell>
        <cell>
          <phrase>code</phrase>
        </cell>
        <cell>
          <phrase>DURATION</phrase>
        </cell>
      </row>
      <row>
        <cell>
          <phrase>Optioneel bij te voegen</phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>VALUE tax-inc_OtherDocumentsNonMandatoryAnnexe - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Andere</phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>VALUE tax-inc_OtherDocuments - D </cell>
      </row>
    </table>
  </xsl:template>
  <xsl:template match="BizTaxDataContract" mode="TAB_204.3" />
  <xsl:template match="BizTaxDataContract" mode="TAB_275C">
    <phrase>Berekening aftrek voor risicokapitaal</phrase>
    <table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" columns="5" width="100%" align="Center" widths="60;5;5;15;15" cellspacing="0" cellpadding="1">
      <row>
        <cell>
          <phrase>&amp;#160;HEADING</phrase>
        </cell>
        <cell>
          <phrase>oldcode</phrase>
        </cell>
        <cell>
          <phrase>code</phrase>
        </cell>
        <cell>
          <phrase>I-START</phrase>
        </cell>
        <cell>
          <phrase>I-END</phrase>
        </cell>
      </row>
      <row>
        <cell>
          <phrase>Eigen vermogen</phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>
          <phrase>8001</phrase>
        </cell>
        <cell>VALUE tax-inc_Equity - I-START</cell>
        <cell>VALUE tax-inc_Equity - I-END</cell>
      </row>
      <row>
        <cell colspan="5">
          <phrase>Bestanddelen af te trekken van het eigen vermogen</phrase>
        </cell>
      </row>
      <row>
        <cell>
          <phrase>Eigen aandelen</phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>
          <phrase>8011</phrase>
        </cell>
        <cell>VALUE tax-inc_OwnSharesFiscalValue - I-START</cell>
        <cell>VALUE tax-inc_OwnSharesFiscalValue - I-END</cell>
      </row>
      <row>
        <cell>
          <phrase>Financiële vaste activa die uit deelnemingen en andere aandelen bestaan</phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>
          <phrase>8012</phrase>
        </cell>
        <cell>VALUE tax-inc_FinancialFixedAssetsParticipationsOtherShares - I-START</cell>
        <cell>VALUE tax-inc_FinancialFixedAssetsParticipationsOtherShares - I-END</cell>
      </row>
      <row>
        <cell>
          <phrase>Aandelen van beleggingsvennootschappen</phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>
          <phrase>8013</phrase>
        </cell>
        <cell>VALUE tax-inc_SharesInvestmentCorporations - I-START</cell>
        <cell>VALUE tax-inc_SharesInvestmentCorporations - I-END</cell>
      </row>
      <row>
        <cell>
          <phrase>Inrichtingen gelegen in een land met verdrag</phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>
          <phrase>8014</phrase>
        </cell>
        <cell>VALUE tax-inc_BranchesCountryTaxTreaty - I-START</cell>
        <cell>VALUE tax-inc_BranchesCountryTaxTreaty - I-END</cell>
      </row>
      <row>
        <cell>
          <phrase>Onroerende goederen gelegen in een land met verdrag</phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>
          <phrase>8015</phrase>
        </cell>
        <cell>VALUE tax-inc_ImmovablePropertyCountryTaxTreaty - I-START</cell>
        <cell>VALUE tax-inc_ImmovablePropertyCountryTaxTreaty - I-END</cell>
      </row>
      <row>
        <cell>
          <phrase>Materiële vaste activa in zover de erop betrekking hebbende kosten onredelijk zijn</phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>
          <phrase>8016</phrase>
        </cell>
        <cell>VALUE tax-inc_TangibleFixedAssetsUnreasonableRelatedCosts - I-START</cell>
        <cell>VALUE tax-inc_TangibleFixedAssetsUnreasonableRelatedCosts - I-END</cell>
      </row>
      <row>
        <cell>
          <phrase>Bestanddelen die als belegging worden gehouden en geen belastbaar periodiek inkomen voortbrengen</phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>
          <phrase>8017</phrase>
        </cell>
        <cell>VALUE tax-inc_InvestmentsNoPeriodicalIncome - I-START</cell>
        <cell>VALUE tax-inc_InvestmentsNoPeriodicalIncome - I-END</cell>
      </row>
      <row>
        <cell>
          <phrase>Onroerende goederen waarvan bedrijfsleiders het gebruik hebben</phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>
          <phrase>8018</phrase>
        </cell>
        <cell>VALUE tax-inc_ImmovablePropertyUseManager - I-START</cell>
        <cell>VALUE tax-inc_ImmovablePropertyUseManager - I-END</cell>
      </row>
      <row>
        <cell>
          <phrase>Uitgedrukte maar niet-verwezenlijkte meerwaarden</phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>
          <phrase>8019</phrase>
        </cell>
        <cell>VALUE tax-inc_UnrealisedExpressedCapitalGains - I-START</cell>
        <cell>VALUE tax-inc_UnrealisedExpressedCapitalGains - I-END</cell>
      </row>
      <row>
        <cell>
          <phrase>Belastingkrediet voor onderzoek en ontwikkeling</phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>
          <phrase>8020</phrase>
        </cell>
        <cell>VALUE tax-inc_TaxCreditResearchDevelopmentDeductionEquityAllowanceCorporateEquity - I-START</cell>
        <cell>VALUE tax-inc_TaxCreditResearchDevelopmentDeductionEquityAllowanceCorporateEquity - I-END</cell>
      </row>
      <row>
        <cell>
          <phrase>Kapitaalsubsidies</phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>
          <phrase>8021</phrase>
        </cell>
        <cell>VALUE tax-inc_InvestmentGrants - I-START</cell>
        <cell>VALUE tax-inc_InvestmentGrants - I-END</cell>
      </row>
      <row>
        <cell>
          <phrase>Voorraadactualisering erkende diamanthandelaars</phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>
          <phrase>8022</phrase>
        </cell>
        <cell>VALUE tax-inc_ActualisationStockRecognisedDiamondTraders - I-START</cell>
        <cell>VALUE tax-inc_ActualisationStockRecognisedDiamondTraders - I-END</cell>
      </row>
      <row>
        <cell>
          <phrase>Ten name van de hoofdzetel ontleende middelen met betrekking tot dewelke de interesten ten laste van het belastbaar resultaat van de Belgische inrichting wordt gelegd</phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>
          <phrase>8023</phrase>
        </cell>
        <cell>VALUE tax-inc_BorrowedFundsMainEstablishmentInterestChargedTaxableResultBelgianBranch - I-START</cell>
        <cell>VALUE tax-inc_BorrowedFundsMainEstablishmentInterestChargedTaxableResultBelgianBranch - I-END</cell>
      </row>
      <row>
        <cell>
          <phrase>Wijzigingen tijdens het belastbare tijdperk van het eigen vermogen en van de bestanddelen die hiervan afgetrokken mogen worden</phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>
          <phrase>8040</phrase>
        </cell>
        <cell>VALUE tax-inc_MovementEquityAfterDeductionsAllowanceCorporateEquity - I-START</cell>
        <cell>VALUE tax-inc_MovementEquityAfterDeductionsAllowanceCorporateEquity - I-END</cell>
      </row>
      <row>
        <cell>
          <phrase>Risicokapitaal van het belastbaar tijdperk</phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>
          <phrase>8050</phrase>
        </cell>
        <cell>VALUE tax-inc_AllowanceCorporateEquityCurrentTaxPeriod - I-START</cell>
        <cell>VALUE tax-inc_AllowanceCorporateEquityCurrentTaxPeriod - I-END</cell>
      </row>
      <row>
        <cell colspan="5">
          <phrase>Aftrek risicokapitaal die voor het aanslagjaar in principe aftrekbaar is</phrase>
        </cell>
      </row>
      <row>
        <cell>
          <phrase>Indien het belastbare tijdperk niet gelijk is aan 12 maanden of voor het eerste belastbare tijdperk moet het tarief worden vermenigvuldigd met een breuk waarvan de teller gelijk is aan het totaal aantal dagen van het belastbare tijdperk en de noemer gelijk is aan 365.</phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>VALUE tax-inc_ApplicableRateAllowanceCorporateEquity - D </cell>
      </row>
      <row>
        <cell>
          <phrase>In principe aftrekbaar</phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>
          <phrase>8051</phrase>
        </cell>
        <cell>VALUE tax-inc_DeductibleAllowanceCorporateEquityCurrentAssessmentYear - D </cell>
      </row>
      <row>
        <cell>
          <phrase>Aftrek voor risicokapitaal van het huidig aanslagjaar die werkelijk wordt afgetrokken</phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>
          <phrase>8052</phrase>
        </cell>
        <cell>VALUE tax-inc_DeductionAllowanceCorporateEquityCurrentAssessmentYear - I-START</cell>
        <cell>VALUE tax-inc_DeductionAllowanceCorporateEquityCurrentAssessmentYear - I-END</cell>
      </row>
      <row>
        <cell>
          <phrase>Saldo van de aftrek voor risicokapitaal van het huidig aanslagjaar dat overdraagbaar is naar latere aanslagjaren</phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>
          <phrase>8053</phrase>
        </cell>
        <cell>VALUE tax-inc_CarryOverCurrentAssessmentYearInsufficientProfitsAllowanceCorporateEquity - I-START</cell>
        <cell>VALUE tax-inc_CarryOverCurrentAssessmentYearInsufficientProfitsAllowanceCorporateEquity - I-END</cell>
      </row>
      <row>
        <cell>
          <phrase>Saldo van de aftrek voor risicokapitaal dat werd gevormd tijdens vorige aanslagjaren</phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>
          <phrase>8060</phrase>
        </cell>
        <cell>VALUE tax-inc_AllowanceCorporateEquityPreviousAssessmentYears - I-START</cell>
        <cell>VALUE tax-inc_AllowanceCorporateEquityPreviousAssessmentYears - I-END</cell>
      </row>
      <row>
        <cell>
          <phrase>Aftrek voor risicokapitaal van vorige aanslagjaren dat werkelijk wordt afgetrokken</phrase>
        </cell>
        <cell>
          <phrase></phrase>
        </cell>
        <cell>
          <phrase>8061</phrase>
        </cell>
        <cell>VALUE tax-inc_DeductionAllowanceCorporateEquityPreviousAssessmentYears - I-START</cell>
        <cell>VALUE tax-inc_DeductionAllowanceCorporateEquityPreviousAssessmentYears - I-END</cell>
      </row>
      <row>
        <cell>
          <phrase>Aftrek voor risicokapitaal dat voor het huidig aanslagjaar wordt afgetrokken</phrase>
        </cell>
        <cell>
          <phrase>104 103</phrase>
        </cell>
        <cell>
          <phrase>1435</phrase>
        </cell>
        <cell>VALUE tax-inc_AllowanceCorporateEquity - I-START</cell>
        <cell>VALUE tax-inc_AllowanceCorporateEquity - I-END</cell>
      </row>
      <row>
        <cell>
          <phrase>Saldo van de aftrek voor risicokapitaal dat overdraagbaar is naar het volgende belastbare tijdperk</phrase>
        </cell>
        <cell>
          <phrase>332</phrase>
        </cell>
        <cell>
          <phrase>1712</phrase>
        </cell>
        <cell>VALUE tax-inc_CarryOverNextTaxPeriodInsufficientProfitsAllowanceCorporateEquity - I-START</cell>
        <cell>VALUE tax-inc_CarryOverNextTaxPeriodInsufficientProfitsAllowanceCorporateEquity - I-END</cell>
      </row>
    </table>
    <phrase>Uitleg wijzigingen tijdens het belastbare tijdperk van het eigen vermogen</phrase>
  </xsl:template>
</xsl:stylesheet>