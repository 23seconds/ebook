﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl"
>
    <xsl:output method="text" indent="yes"/>

    <xsl:template match="root">

      #region Rendered Methods
      #region Schema Info


      private string SchemaRef = "<xsl:value-of select="@schemaRef"/>";
      
      private List&lt;XmlQualifiedName&gt; NameSpaces = new List&lt;XmlQualifiedName&gt; {
          <xsl:for-each select="namespaces/namespace">
            <xsl:if test="position()>1">,</xsl:if>
            new XmlQualifiedName("<xsl:value-of select="@name"/>","<xsl:value-of select="@namespace"/>")
          </xsl:for-each>
      };

      #endregion

      #region BizTaxDataContract

      public BizTaxDataContract SetGeneralData(BizTaxDataContract btdc, WorksheetSet set)
      {
          XbrlElementDataContract xedc = null;
          XbrlElementDataContract pxedc = null;
          string xvalue=null;
          <xsl:apply-templates select="mappings/global"/>
          return btdc;
      }

      public BizTaxDataContract UpdateNoWorksheets(BizTaxDataContract btdc, WorksheetSet set)
      {
          if (btdc == null)
          {
            btdc = CreateDefault(set.DataContainer.Client,set.DataContainer.DataContract);
          }
          // clear all automated rendered items
          btdc.Elements.RemoveAll(e => e.AutoRendered);
          btdc.Contexts.RemoveAll(c => c.AutoRendered);
          XbrlElementDataContract xedc = null;
          XbrlElementDataContract pxedc = null;
          string xvalue=null;
          SetGeneralData(btdc,set);
          SaveBizTax(new CriteriaFileBizTaxDataContract { FileId = set.DataContainer.DataContract.Id, Culture = set.DataContainer.DataContract.Culture, BizTax = btdc });
          return btdc;
      }

      public BizTaxDataContract UpdateFromWorksheets(BizTaxDataContract btdc, WorksheetSet set)
      {

          if (btdc == null)
          {
            btdc = CreateDefault(set.DataContainer.Client,set.DataContainer.DataContract);
          }
          // clear all automated rendered items
          btdc.Elements.RemoveAll(e => e.AutoRendered);
          btdc.Contexts.RemoveAll(c => c.AutoRendered);
          XbrlElementDataContract xedc = null;
          XbrlElementDataContract pxedc = null;
          string xvalue=null;
          SetGeneralData(btdc,set);
          <xsl:apply-templates select="mappings/worksheet"/>
          
          /*
          XElement xe = CoreHelper.SerializeToXElement(btdc, true);
            xe.Save(@"C:\Projects_TFS\EY\Ebook\Main-v5.0\DATA\" + Guid.NewGuid().ToString() + ".xml");
            RenderBizTaxFile(btdc);
            */
          SaveBizTax(new CriteriaFileBizTaxDataContract { FileId = set.DataContainer.DataContract.Id, Culture = set.DataContainer.DataContract.Culture, BizTax = btdc });
          
          return btdc;
    
      }
      #endregion
      
      #region Worksheet specific BizTaxDataContract
      <xsl:for-each select="mappings/worksheet[@isList='true']">
        <xsl:apply-templates select="." mode="RENDERUPDATEPROC"/>
      </xsl:for-each>
      #endregion

      #region Worksheet specific Calculations
      public BizTaxDataContract Calculate(BizTaxDataContract btdc)
      {
          <xsl:apply-templates select="calculations"/>
      }
      #endregion

      #endregion
    </xsl:template>

  <!--xsl:template match="mappings">
    <xsl:apply-templates select="global"/>
    <xsl:apply-templates select="worksheet"/>
  </xsl:template-->

  <xsl:template match="worksheet" mode="RENDERUPDATEPROC">
  <xsl:variable name="dataType"><xsl:apply-templates select="." mode="DATATYPE"/></xsl:variable>
    private void UpdateGrid<xsl:value-of select="@id"/><xsl:value-of select="@rootProp"/>(ref BizTaxDataContract btdc, WorksheetSet set, <xsl:value-of select="$dataType"/> entity) {
    
        XbrlElementDataContract xedc = null;
        XbrlElementDataContract pxedc = null;
        string xvalue=null;
        <xsl:apply-templates select="contextScenarios"/>
        <xsl:apply-templates select="element" mode="CONTEXTED"/>
    }
  </xsl:template>

  <xsl:template match="contextScenarios">
    List&lt;ContextScenarioDataContract&gt; scenarios = new List&lt;ContextScenarioDataContract&gt; {
        <xsl:for-each select="contextScenario">
          <xsl:if test="position()>1">,</xsl:if>
          CreateScenario("<xsl:value-of select="@type"/>", "<xsl:value-of select="@dimension"/>", <xsl:apply-templates select="*[1]" mode="RenderElementCONTEXTED"/> , "<xsl:value-of select="@typeId"/>")
      </xsl:for-each>
    };
    
   
    List&lt;ContextElementDataContract&gt; contexts = CreateContextAllPeriods(set.DataContainer.Client,set.DataContainer.DataContract, scenarios,true);
    
    btdc.Contexts.AddRange(contexts);
    
    
  </xsl:template>
  
  <xsl:template match="worksheet">
    
    #region <xsl:value-of select="@id"/> - <xsl:value-of select="@rootProp"/>
    <xsl:choose>
      <xsl:when test="@isList='true'">
        <xsl:variable name="dataType"><xsl:apply-templates select="." mode="DATATYPE"/></xsl:variable>
        
        foreach(<xsl:value-of select="$dataType"/> entity in set.DataContainer.<xsl:value-of select="@id"/>.Data.<xsl:value-of select="@rootProp"/>) {
        
            UpdateGrid<xsl:value-of select="@id"/><xsl:value-of select="@rootProp"/>(ref btdc, set, entity);
        }
        
      </xsl:when>
      <xsl:otherwise>
        <xsl:apply-templates select="element"/>
      </xsl:otherwise>
    </xsl:choose>
    
    #endregion
  </xsl:template>
  
  <xsl:template match="global">
    #region SET GLOBAL INFO
    <xsl:apply-templates select="element"/>
    #endregion
  </xsl:template>

  <xsl:template match="element" mode="CONTEXTED">

    xedc = <xsl:apply-templates select="." mode="RenderElementCONTEXTED"/>;
    <!--
    if(xedc!=null &amp;&amp; (xedc.Value!=null || xedc.Children!=null) &amp;&amp; !btdc.Elements.ContainsKey(xedc.Id)) {
      btdc.Elements.Add(xedc.id,xedc);
    } //-->
    if(xedc!=null &amp;&amp; (!string.IsNullOrEmpty(xedc.Value) || xedc.Children!=null)) {
      btdc.Elements.Add(xedc);
    }
  </xsl:template>

  <xsl:template match="element">

    xedc = <xsl:apply-templates select="." mode="RenderElement"/>;
<!--
    if(xedc!=null &amp;&amp; (xedc.Value!=null || xedc.Children!=null) &amp;&amp; !btdc.Elements.ContainsKey(xedc.Id)) {
      btdc.Elements.Add(xedc.id,xedc);
    } //-->
    <xsl:choose>
    <xsl:when test="element">
      
      if(xedc!=null) {
        if(btdc.Elements.Count(e=>e.Name==xedc.Name &amp;&amp; e.Period==xedc.Period &amp;&amp; e.Prefix==xedc.Prefix &amp;&amp; e.Context==xedc.Context)>0) {
          pxedc = btdc.Elements.First(e=>e.Name==xedc.Name &amp;&amp; e.Period==xedc.Period &amp;&amp; e.Prefix==xedc.Prefix &amp;&amp; e.Context==xedc.Context);
          CheckChildren(ref pxedc, xedc.Children);
        } else {
          btdc.Elements.Add(xedc);
        }
      }
    </xsl:when>
      <xsl:otherwise>
        if(xedc!=null &amp;&amp; (!string.IsNullOrEmpty(xedc.Value) || xedc.Children!=null)) {
      if(btdc.Elements.Count(e=>e.Name==xedc.Name &amp;&amp; e.Period==xedc.Period &amp;&amp; e.Prefix==xedc.Prefix &amp;&amp; e.Context==xedc.Context)>0) {
        pxedc = btdc.Elements.First(e=>e.Name==xedc.Name &amp;&amp; e.Period==xedc.Period &amp;&amp; e.Prefix==xedc.Prefix &amp;&amp; e.Context==xedc.Context);
        if(pxedc.AutoRendered || string.IsNullOrEmpty(pxedc.Value)) {
          pxedc.Value = xedc.Value;
        }
      } else {
        pxedc=xedc;
        btdc.Elements.Add(pxedc);
      }
    }
      </xsl:otherwise>
    </xsl:choose>

  </xsl:template>

  <xsl:template match="element" mode="RenderElementCONTEXTED">
    <xsl:variable name="selectData"><xsl:apply-templates select="*[not(local-name()='element')]" mode="RenderElementCONTEXTED"/></xsl:variable>
    <xsl:variable name="xvalue"><xsl:choose><xsl:when test="$selectData='' or not($selectData)">null</xsl:when><xsl:otherwise><xsl:value-of select="$selectData"/></xsl:otherwise></xsl:choose></xsl:variable>new XbrlElementDataContract
      {
        AutoRendered = <xsl:choose><xsl:when test="element">false</xsl:when><xsl:otherwise>true</xsl:otherwise></xsl:choose>
        ,
        Children = <xsl:choose>
    <xsl:when test="element">
      new List&lt;XbrlElementDataContract&gt; {
        <xsl:for-each select="element">
          <xsl:if test="position()>1">,</xsl:if><xsl:apply-templates select="." mode="RenderElementCONTEXTED"/>
          </xsl:for-each>
      } 
    </xsl:when>
    <xsl:otherwise>null</xsl:otherwise>
  </xsl:choose>
        ,
        Context = contexts.First(c=>c.Id.StartsWith("<xsl:value-of select="@period"/>__")).Id.Replace("<xsl:value-of select="@period"/>__","")
        ,
        Period = "<xsl:value-of select="@period"/>"
        ,
        Decimals = "<xsl:value-of select="@decimals"/>"
        ,
        Name = "<xsl:value-of select="@id"/>"
        ,
        NameSpace = ""
        ,
        Prefix = "<xsl:value-of select="@prefix"/>"
        ,
        UnitRef = "<xsl:value-of select="@unitRef"/>"
        ,
        Value = <xsl:value-of select="$xvalue"/>
      }</xsl:template>
  
  
  <xsl:template match="element" mode="RenderElement">
    <xsl:variable name="selectData"><xsl:apply-templates select="*[not(local-name()='element')]"/></xsl:variable>
    <xsl:variable name="xvalue"><xsl:choose><xsl:when test="$selectData='' or not($selectData)">null</xsl:when><xsl:otherwise><xsl:value-of select="$selectData"/></xsl:otherwise></xsl:choose></xsl:variable>new XbrlElementDataContract
      {
        AutoRendered = <xsl:choose><xsl:when test="element">false</xsl:when><xsl:otherwise>true</xsl:otherwise></xsl:choose>
        ,
        Children = <xsl:choose>
    <xsl:when test="element">
      new List&lt;XbrlElementDataContract&gt; {
        <xsl:for-each select="element">
          <xsl:if test="position()>1">,</xsl:if><xsl:apply-templates select="." mode="RenderElement"/>
          </xsl:for-each>
      } 
    </xsl:when>
          <xsl:when test="method"><xsl:apply-templates select="method"/></xsl:when>
    <xsl:otherwise>null</xsl:otherwise>
  </xsl:choose>
        ,
        Context = "<xsl:value-of select="@context"/>"
        ,
        Period = "<xsl:value-of select="@period"/>"
        ,
        Decimals = "<xsl:value-of select="@decimals"/>"
        ,
        Name = "<xsl:value-of select="@id"/>"
        ,
        NameSpace = ""
        ,
        Prefix = "<xsl:value-of select="@prefix"/>"
        ,
        UnitRef = "<xsl:value-of select="@unitRef"/>"
        ,
        Value = <xsl:choose><xsl:when test="not(method)"><xsl:value-of select="$xvalue"/></xsl:when><xsl:otherwise>null</xsl:otherwise></xsl:choose>
      }</xsl:template>

  <xsl:template match="file">set.DataContainer.DataContract.<xsl:value-of select="."/>.ToXbrlValue()</xsl:template>
  <xsl:template match="client">set.DataContainer.Client.<xsl:value-of select="."/></xsl:template>
  <xsl:template match="method"><xsl:value-of select="."/></xsl:template>
  
  <xsl:template match="string">"<xsl:value-of select="."/>"</xsl:template>
  <xsl:template match="property"><xsl:variable name="wel" select="ancestor::worksheet"/>set.DataContainer.<xsl:value-of select="$wel/@id"/>.Data.<xsl:value-of select="$wel/@rootProp"/>.<xsl:value-of select="."/>.ToXbrlValue()</xsl:template>
  
  <xsl:template match="property" mode="RenderElementCONTEXTED">entity.<xsl:value-of select="."/>.ToXbrlValue()</xsl:template>
  


  <xsl:template match="worksheet" mode="DATATYPE"><xsl:variable name="worksheetId" select="@id"/><xsl:variable name="worksheetProp" select="@rootProp"/><xsl:value-of select="@id" />Types.<xsl:value-of select="//worksheetInfo/Worksheettype[@name=$worksheetId]/Types/Root/*/*[local-name()=$worksheetProp]/@entity"/></xsl:template>

  
    <xsl:template match="calculations">
      XbrlElementDataContract xed = null;
      decimal? value = null;
      <xsl:apply-templates select="*" mode="CALC"/>
    
    </xsl:template>

  <xsl:template match="field" mode="CALC">
    <xsl:choose>
      <xsl:when test="@accuracy">
        value = Math.Round(<xsl:apply-templates select="*" mode="GenerateCalc"/>,<xsl:value-of select="@accuracy"/>);
      </xsl:when>
      <xsl:otherwise>
        value = <xsl:apply-templates select="*" mode="GenerateCalc"/>;
      </xsl:otherwise>
    </xsl:choose>
    
    xedc = new XbrlElementDataContract
      {
        AutoRendered = true
        ,
        Children = null
        ,
        Context = "<xsl:value-of select="@context"/>"
        ,
        Period = "<xsl:value-of select="@period"/>"
        ,
        Decimals = "INF"
        ,
        Name = "<xsl:value-of select="@name"/>"
        ,
        NameSpace = ""
        ,
        Prefix = "<xsl:value-of select="@prefix"/>"
        ,
        UnitRef = "EUR"
        ,
        Value = value.ToXbrlValue()
      }; 
      if(xedc!=null &amp;&amp; (!string.IsNullOrEmpty(xedc.Value) || xedc.Children!=null)) {
        if(btdc.Elements.Count(e=>e.Name==xedc.Name &amp;&amp; e.Period==xedc.Period &amp;&amp; e.Prefix==xedc.Prefix &amp;&amp; e.Context==xedc.Context)>0) {
          pxedc = btdc.Elements.First(e=>e.Name==xedc.Name &amp;&amp; e.Period==xedc.Period &amp;&amp; e.Prefix==xedc.Prefix &amp;&amp; e.Context==xedc.Context);
          if(pxedc.AutoRendered || pxedc.Value!=xedc.Value) {
            pxedc.Value = xedc.Value;
          }
        } else {
          pxedc=xedc;
          btdc.Elements.Add(pxedc);
        }
      }
    
  </xsl:template>

  <xsl:template match="field" mode="ConstructId"><xsl:value-of select="@prefix"/>_<xsl:value-of select="@name"/>_<xsl:value-of select="@period"/><xsl:if test="@context and not(@context='')">__<xsl:value-of select="@context"/></xsl:if></xsl:template>
  
  <xsl:template match="setVariable" mode="CALC">
    decimal? <xsl:value-of select="@name"/> = <xsl:apply-templates select="*" mode="GenerateCalc"/>;
  </xsl:template>
  
  <xsl:template match="sum" mode="GenerateCalc"><xsl:for-each select="*"><xsl:if test="position()>1"> + </xsl:if><xsl:apply-templates select="." mode="GetDataValue"/></xsl:for-each></xsl:template>
  <xsl:template match="max" mode="GenerateCalc">Math.Max(<xsl:for-each select="*"><xsl:if test="position()>1">,</xsl:if>(<xsl:apply-templates select="." mode="GenerateCalc"/>)</xsl:for-each>)</xsl:template>
  <xsl:template match="if" mode="GenerateCalc"><xsl:apply-templates select="test" mode="GenerateCalc"/> ? <xsl:apply-templates select="then" mode="GenerateCalc"/> : <xsl:apply-templates select="else" mode="GenerateCalc"/></xsl:template>

  <xsl:template match="test" mode="GenerateCalc"><xsl:apply-templates select="*" mode="GenerateCalc"/></xsl:template>
  <xsl:template match="then" mode="GenerateCalc"><xsl:apply-templates select="*" mode="GenerateCalc"/></xsl:template>
  <xsl:template match="else" mode="GenerateCalc"><xsl:apply-templates select="*" mode="GenerateCalc"/></xsl:template>
  
  
  <xsl:template match="variable" mode="GenerateCalc"><xsl:apply-templates select="." mode="GetDataValue"/></xsl:template>
  <xsl:template match="variable" mode="GetDataValue"><xsl:if test="@negative='true'">-</xsl:if><xsl:value-of select="@name"/></xsl:template>

  <xsl:template match="equals" mode="GenerateCalc"> == </xsl:template>
  <xsl:template match="greaterThen" mode="GenerateCalc"> &gt; </xsl:template>
  <xsl:template match="lowerThen" mode="GenerateCalc"> &lt; </xsl:template>
  <xsl:template match="number" mode="GenerateCalc"> <xsl:value-of select="."/> </xsl:template>

  <xsl:template match="multiply" mode="GenerateCalc"> * </xsl:template>
  <xsl:template match="divide" mode="GenerateCalc"> / </xsl:template>
  <xsl:template match="minus" mode="GenerateCalc"> - </xsl:template>

  <xsl:template match="field" mode="GenerateCalc"><xsl:apply-templates select="." mode="GetDataValue"/></xsl:template>
  <xsl:template match="field" mode="GetDataValue"><xsl:if test="@negative='true'">-</xsl:if><xsl:choose><xsl:when test="contains(@contextRef,'*')">CalculationGetSumOf("<xsl:value-of select="@prefix"/>","<xsl:value-of select="@name"/>","<xsl:value-of select="translate(@contextRef,'*','')"/>", btdc)</xsl:when><xsl:otherwise>CalculationGetElValue("<xsl:value-of select="@prefix"/>_<xsl:value-of select="@name"/>_<xsl:value-of select="@contextRef"/>", btdc)</xsl:otherwise></xsl:choose></xsl:template>
  
  
</xsl:stylesheet>
