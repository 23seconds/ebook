﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Xml;
using System.Xml.XPath;
using System.Text.RegularExpressions;
using EY.com.eBook.BizTaxParser.Test2;

namespace EY.com.eBook.BizTaxParser.Renderer
{
    public class AssertionReader
    {
        public XElement parameters;
        private List<XbrlElementDefinition> _definitions;
        public bool include = true;
        public XElement Read(string path, List<XbrlElementDefinition> definitions, XElement results)
        {
            parameters = new XElement("parameters");
            FormulaEvaluator fe = new FormulaEvaluator();
            _definitions = definitions;
            Regex reNumeric = new Regex(@"[0-9]|\.");
            Regex reDate = new Regex(@"^'([0-9]{4})-([0-9]{2})-([0-9]{2})");
            XmlReader reader = new XmlTextReader(path);
            XmlNamespaceManager xnsm = new XmlNamespaceManager(new NameTable());
            XDocument xdoc = XDocument.Load(path);
            Dictionary<string, XNamespace> namespaces = xdoc.Root.Attributes().
                        Where(a => a.IsNamespaceDeclaration).
                        GroupBy(a => a.Name.Namespace == XNamespace.None ? String.Empty : a.Name.LocalName,
                                a => XNamespace.Get(a.Value)).
                        ToDictionary(g => g.Key,
                                     g => g.First());

            foreach (string key in namespaces.Keys)
            {
                xnsm.AddNamespace(key, namespaces[key].NamespaceName);
            }


            // Loop through generic links
            
            XElement assGens = new XElement("containers");
            
            assGens.Add(new XAttribute("id", System.IO.Path.GetFileNameWithoutExtension(path)));
            assGens.Add(new XAttribute("path", path));
            foreach (XElement genlink in xdoc.Root.Elements(namespaces["generic"] + "link"))
            {
                Console.WriteLine("GENLINK");
                XElement assGen = new XElement("container");
                XElement labels = new XElement("labels");

                foreach (XElement varAss in genlink.Elements())
                {
                    if (varAss.Name == namespaces["va"] + "valueAssertion")
                    {
                        string assId = varAss.Attribute("id").Value;
                        XElement xAss = new XElement("valueAssertion", varAss.Attribute("id"));
                        XElement xVariables = new XElement("variables");
                        foreach (XElement varArc in genlink.XPathSelectElements(string.Format("variable:variableArc[@xlink:from='{0}']", assId), xnsm))
                        {
                            string arcTo = varArc.Attribute(namespaces["xlink"] + "to").Value;

                            XElement variable = new XElement("variable", varArc.Attribute("name"));

                            // factVariable
                            XElement factVar = genlink.XPathSelectElement(string.Format("variable:factVariable[@xlink:label='{0}']", arcTo), xnsm);
                            if (factVar != null && factVar.Attribute("fallbackValue") != null)
                            {
                                string fallback = factVar.Attribute("fallbackValue").Value;
                                variable.Add(new XAttribute("fallBackValue", fallback));
                                if (factVar.Attribute("bindAsSequence") != null && factVar.Attribute("bindAsSequence").Value == "true")
                                {
                                    variable.Add(new XAttribute("sumOfAllFound", true));
                                }
                            }

                            XElement generalVar = genlink.XPathSelectElement(string.Format("variable:generalVariable[@xlink:label='{0}']", arcTo), xnsm);
                            if (generalVar != null)
                            {
                                if (generalVar.Attribute("select") != null)
                                {
                                    variable.Add(generalVar.Attribute("select"));
                                    variable.Add(fe.TransformFormula(generalVar.Attribute("select").Value));
                                }
                            }
                            XElement eldef =null;
                            foreach (XElement varfilters in genlink.XPathSelectElements(string.Format("variable:variableFilterArc[@xlink:from='{0}']", arcTo), xnsm))
                            {
                                string filterTo = varfilters.Attribute(namespaces["xlink"] + "to").Value;
                                List<XElement> filters = genlink.XPathSelectElements(string.Format("*[@xlink:label='{0}']", filterTo), xnsm).ToList();
                                if (filters != null && filters.Count>0)
                                {
                                    foreach (XElement filter in filters)
                                    {
                                        switch (filter.Name.LocalName)
                                        {
                                            case "explicitDimension":
                                                XElement elExDim = new XElement("explicitDimension");
                                                foreach (XElement elExDimCild in filter.Elements())
                                                {
                                                    string[] elExValue = elExDimCild.Elements().First().Value.Split(new char[] {':'});

                                                    elExDim.Add(new XElement(elExDimCild.Name.LocalName, new XAttribute("prefix", elExValue[0]), elExValue[1]));
                                                }
                                                variable.Add(elExDim);
                                                break;
                                            case "conceptName":
                                                string cfName;
                                                string[] parts;
                                                XElement elConceptLink;
                                                XElement elConcept = new XElement("Concept");
                                                if (filter.Elements(namespaces["cf"] + "concept").Count() > 1)
                                                {
                                                    XElement elSum = new XElement("Sum");
                                                    foreach (XElement xcon in filter.Elements(namespaces["cf"] + "concept"))
                                                    {
                                                        cfName = xcon.Element(namespaces["cf"] + "qname").Value;
                                                        parts = cfName.Split(new char[] { ':' });
                                                        eldef = results.XPathSelectElements(string.Format("//element[@xbrlId='{0}_{1}']", parts[0], parts[1])).FirstOrDefault();

                                                        elConceptLink = new XElement("ConceptLink",
                                                                new XAttribute("xbrlName", parts[1])
                                                                , new XAttribute("xbrlPrefix", parts[0]));
                                                        if(eldef!=null && eldef.Element("period")!=null) 
                                                        {
                                                            elConceptLink.Add(GetPeriods(eldef.Element("period")));
                                                        }
                                                        if (eldef != null && eldef.Attribute("type") != null)
                                                        {
                                                            elConceptLink.Add(eldef.Attribute("type"));
                                                        }
                                                        if (eldef != null && eldef.Element("dimensions") != null)
                                                        {
                                                            elConceptLink.Add(eldef.Element("dimensions"));
                                                        }
                                                        elSum.Add(elConceptLink);
                                                        if (include) include = eldef != null;
                                                    }
                                                    elConcept.Add(elSum);
                                                    
                                                }
                                                else
                                                {

                                                    cfName = filter.Element(namespaces["cf"] + "concept").Element(namespaces["cf"] + "qname").Value;
                                                    parts = cfName.Split(new char[] { ':' });
                                                    eldef = results.XPathSelectElements(string.Format("//element[@xbrlId='{0}_{1}']", parts[0], parts[1])).FirstOrDefault();

                                                    elConceptLink = new XElement("ConceptLink",
                                                            new XAttribute("xbrlName", parts[1])
                                                            , new XAttribute("xbrlPrefix", parts[0]));
                                                    if (eldef != null && eldef.Element("period") != null)
                                                    {
                                                        elConceptLink.Add(GetPeriods(eldef.Element("period")));
                                                    }
                                                    if (eldef != null && eldef.Attribute("type") != null)
                                                    {
                                                        elConceptLink.Add(eldef.Attribute("type"));
                                                    }
                                                    if (eldef != null && eldef.Element("dimensions") != null)
                                                    {
                                                        elConceptLink.Add(eldef.Element("dimensions"));
                                                    }
                                                    elConcept.Add(elConceptLink);
                                                    if(include) include = eldef != null;
                                                }
                                                variable.Add(elConcept);
                                                break;
                                            case "period":
                                                string period = "D";
                                                string pvalue = filter.Attribute("test").Value;
                                                if (pvalue.Contains("xfi:is-instant-period"))
                                                {
                                                    if (pvalue.Contains("eq xs:date($PeriodEndDate)"))
                                                    {
                                                        period = "I-End";
                                                    }
                                                    else if (pvalue.Contains("eq xs:date($PeriodStartDate)"))
                                                    {
                                                        period = "I-Start";
                                                    }
                                                }
                                                variable.Add(new XAttribute("xbrlPeriod", period));
                                                break;
                                            
                                        }
                                    }
                                }
                            }
                            xVariables.Add(variable);

                        }


                        xAss.Add(xVariables);
                        assGen.Add(varAss.Attribute("test"));
                        assGen.Add(fe.TransformFormula(varAss.Attribute("test").Value));

                        if (include)
                        {
                            List<XElement> periods = xVariables.Elements("variable").Where(e => e.Attribute("xbrlPeriod") == null && e.Element("Concept") != null)
                                .SelectMany(e => e.XPathSelectElements("//period").ToList()).ToList();
                            List<string> pids = periods.Select(e => e.Attribute("id").Value).ToList().Distinct().ToList();
                            if (pids.Count > 1)
                            {
                                assGen.Add(new XAttribute("multiperiod", true));
                                XElement genPeriods = new XElement("periods");
                                foreach (string pid in pids)
                                {
                                    genPeriods.Add(new XElement("period", new XAttribute("id", pid)));
                                }
                                assGen.Add(genPeriods);
                            }


                            List<XElement> dims = xVariables.Elements("variable").Where(e => e.Element("explicitDimension") == null && e.Element("Concept") != null)
                                .SelectMany(e => e.XPathSelectElements(".//ConceptLink//dimension").ToList()).ToList();
                            List<string> dids = dims.Select(e => string.Format("{0}#{1}",e.Attribute("id").Value,e.Value)).ToList().Distinct().ToList();
                            if (dids.Count() > 1)
                            {
                                assGen.Add(new XAttribute("multidimensional", true));
                            }
                            XElement gendims = new XElement("dimensions");
                            foreach (string did in dids)
                            {
                                string[] didsplit = did.Split(new char[] { '#' });
                                gendims.Add(new XElement("dimension", new XAttribute("id", didsplit[0]), didsplit[1]));
                            }
                            assGen.Add(gendims);
                            
                        }


                        assGen.Add(xAss);
                    }
                    else if (varAss.Name == namespaces["variable"] + "precondition")
                    {
                        XElement precondition = new XElement("Precondition",varAss.Attribute("test"));
                        precondition.Add(fe.TransformFormula(varAss.Attribute("test").Value));
                        assGen.Add(precondition);
                    }
                    else if (varAss.Name == namespaces["variable"] + "parameter")
                    {
                        XElement xparam = new XElement("Parameter", varAss.Attribute("name"));
                        string paramsel = varAss.Attribute("select").Value;
                        if (paramsel.StartsWith("'"))
                        {
                            if (reDate.IsMatch(paramsel))
                            {
                                xparam.Add(new XElement("Date", paramsel.Replace("'", "")));
                            }
                            else
                            {
                                xparam.Add(new XElement("String", paramsel.Replace("'", "")));
                            }
                        }
                        else if (reNumeric.IsMatch(paramsel))
                        {
                            xparam.Add(new XElement("Numeric", paramsel));
                        }
                        parameters.Add(xparam);

                    }
                    else if (varAss.Name == namespaces["label"] + "label")
                    {
                        labels.Add(GetLabel(varAss, namespaces));
                    }
                    else
                    {
                        //assGen.Add(new XElement("UNKNOWN", new XAttribute("NodeName", varAss.Name.LocalName), new XAttribute("NodeXmlns", varAss.Name.NamespaceName)));
                    }
                }
                assGen.Add(labels);
                assGens.Add(assGen);
                
            }
            //assGens.Add(parameters);
            return assGens;   
        }

        private XElement GetLabel(XElement def,Dictionary<string,XNamespace> namespaces )
        {
            
            string language = def.Attribute(XNamespace.Xml + "lang").Value;
            string rol = def.Attribute(namespaces["xlink"] + "role").Value;
            string[] rolParts = rol.Split(new char[] { '/' });
            rol = rolParts[rolParts.Length - 1];
            string desc = def.Value;
            XElement xlabel = new XElement("label", new XAttribute("lang", language), new XAttribute("role", rol));
            Regex reValueGroups = new Regex(@"\{[^\{\}]+\}");
            MatchCollection matches = reValueGroups.Matches(desc);
            if (matches.Count == 0)
            {
                xlabel.Add(desc);
            }
            else
            {
                foreach (Match match in matches)
                {
                    if (match.Value.StartsWith("{$"))
                    {
                        int idx = desc.IndexOf(match.Value);
                        if (idx > -1)
                        {
                            xlabel.Add(desc.Substring(0, idx));
                            xlabel.Add(match.Value.Replace("{", "").Replace("}", ""));
                            //xlabel.Add(new XElement("variable", new XAttribute("name", match.Value.Replace("{$", "").Replace("}", ""))));
                            desc = desc.Substring(idx + match.Value.Length);
                        }
                    }
                    else if(match.Value.StartsWith("{xfi:concept-label"))
                    {
                        string label = "";
                        //{xfi:concept-label( QName('http://www.minfin.fgov.be/be/tax/inc/2012-04-30', 'RemainingFiscalProfitCommonRate'), '', '', 'nl')}
                        int idx = desc.IndexOf(match.Value);
                        if (idx > -1)
                        {
                            //xlabel.Add(desc.Substring(0, idx));

                            Regex reQname = new Regex(@"QName\([^\(\)]+\)");
                            string labelLookup = match.Value.Replace("{xfi:concept-label(", "").Replace(")}", "");
                            Match qmatch = reQname.Match(labelLookup);
                            if (qmatch != null)
                            {
                                string[] qparts = qmatch.Value.Replace("QName(","").Replace(")","").Split(new char[] { ',' });
                                string prefix = "";
                                if (qparts[0].Contains("http://www.minfin.fgov.be/be/tax/inc"))
                                {
                                    prefix = "tax-inc";
                                }
                                else if (qparts[0].Contains("http://www.nbb.be/be/fr/pfs/ci/gcd"))
                                {
                                    prefix = "pfs-gcd";
                                }
                                else if (qparts[0].Contains("http://www.nbb.be/be/fr/pfs/ci/vl"))
                                {
                                    prefix = "pfs-vl";
                                }
                                else if (qparts[0].Contains("http://www.nbb.be/be/fr/pfs/ci/dt"))
                                {
                                    prefix = "pfs-dt";
                                }
                                labelLookup = labelLookup.Replace(qmatch.Value, "processed");

                                string[] clparts = labelLookup.Replace("'", "").Split(new char[] { ',' });
                                string cllang = clparts[clparts.Length - 1].Trim();
                                XbrlElementDefinition xed = _definitions.FirstOrDefault(x => x.Name == qparts[1].Replace("'","").Trim() && x.Id.StartsWith(prefix));
                                if (xed != null)
                                {
                                    Translation tr = xed.Labels.First(l => l.Role == "label").Descriptions.FirstOrDefault(d => d.Language == cllang);
                                    if (tr != null)
                                    {
                                        desc = desc.Replace(match.Value, tr.Description);
                                    }
                                }


                            }
                            else
                            {
                                xlabel.Add(new XElement("labellookup", match.Value));
                                desc = desc.Substring(idx + match.Value.Length);
                            }
                            
                        }
                    }
                    else {
                         int idx = desc.IndexOf(match.Value);
                        if (idx > -1)
                        {
                            xlabel.Add(desc.Substring(0, idx));
                            // xlabel.Add(new XElement("UNKNOWN", match.Value));
                            desc = desc.Substring(idx + match.Value.Length);
                        }
                    }

                }
               if(!string.IsNullOrEmpty(desc)) {
                   xlabel.Add(desc);
               }
            }
            return xlabel;
        }

        private XElement GetPeriods(XElement period)
        {
            XElement periods = new XElement("periods");
            if (period.Attribute("type").Value == "duration")
            {
                bool isDur = true;
                if (period.Attribute("start") != null && period.Attribute("start").Value == "true")
                {
                    periods.Add(new XElement("period", new XAttribute("id", "I-Start")));
                    isDur = false;
                }
               
               if(isDur) periods.Add(new XElement("period", new XAttribute("id", "D")));
            }
            else
            {
                if (period.Attribute("start") !=null && period.Attribute("start").Value == "true")
                {
                    periods.Add(new XElement("period", new XAttribute("id", "I-Start")));
                }
                if (period.Attribute("end") != null && period.Attribute("end").Value == "true")
                {
                    periods.Add(new XElement("period", new XAttribute("id", "I-End")));
                }
            }
            return periods;
        }
    }

  

    public class FormulaEvaluator
    {

        private readonly Regex itemsRegEx = new Regex(@"(\-{0,1}\$[\w]*)|([0-9]+\.{0,1}[0-9]+){1}|(,){1}|(\({1})|(\){1})|(\w+)|\*{1}", RegexOptions.Compiled);
        private readonly Regex reNumeric = new Regex(@"[0-9]|\.");

        public XElement TransformFormula(string formula)
        {
            formula = formula.Replace("xs:", "To").Replace("string-length", "StringLength");
            XElement xformula = new XElement("formula");
          //  Console.WriteLine(formula);
            StringBuilder sb = new StringBuilder("");
            MatchCollection coll = itemsRegEx.Matches(formula);
            List<string> items = new List<string>();
            foreach (Match match in coll)
            {
                if(match.Value.Trim().Length>0) {
                    items.Add(match.Value.Trim());
               }
            }
            RenderXml(0, items, ref xformula);
            return xformula;
        }


        private int RenderXml(int startfrom, List<string> items, ref XElement parent) //, string stop
        {
            for (int i = startfrom; i < items.Count; i++)
            {
                string item = items[i];
                if (string.IsNullOrEmpty(item)) //!string.IsNullOrEmpty(stop) && item == stop)
                {
                    return i;
                }
                else
                {
                    switch (item.ToLower())
                    {
                        case "*":
                            parent.Add(new XElement("multiply"));
                            break;
                        case "(":
                            XElement xgroup = new XElement("group");
                            i = RenderXml(i + 1, items, ref xgroup);
                            parent.Add(xgroup);
                            break;
                        case ")":
                            return i;
                            break;
                        case "stringlength":
                            XElement xstrl = new XElement("StringLength"); //, new XAttribute("isGrouped",true)
                            int xmstrlStart = items.IndexOf("(", i);
                            i = RenderXml(xmstrlStart + 1, items, ref xstrl);
                            parent.Add(xstrl);
                            break;
                        case "tostring":
                            XElement xstr = new XElement("ToString"); //, new XAttribute("isGrouped",true)
                            int xmstrStart = items.IndexOf("(", i);
                            i = RenderXml(xmstrStart + 1, items, ref xstr);
                            parent.Add(xstr);
                            break;
                        case "todate":
                            XElement xdte = new XElement("ToDate"); //, new XAttribute("isGrouped",true)
                            int xmdteStart = items.IndexOf("(", i);
                            i = RenderXml(xmdteStart + 1, items, ref xdte);
                            parent.Add(xdte);
                            break;
                        case "max":
                            XElement xmax = new XElement("max"); //, new XAttribute("isGrouped",true)
                            int xmaStart = items.IndexOf("(", i);
                            i = RenderXml(xmaStart + 1, items, ref xmax);
                            parent.Add(xmax);
                            break;
                        case "min":
                            XElement xmin = new XElement("max"); //, new XAttribute("isGrouped",true)
                            int xmiStart = items.IndexOf("(", i);
                            i = RenderXml(xmiStart + 1, items, ref xmin);
                            parent.Add(xmin);
                            break;
                        case "round":
                            XElement xround = new XElement("round"); //, new XAttribute("isGrouped",true)
                            int xrStart = items.IndexOf("(", i);
                            i = RenderXml(xrStart + 1, items, ref xround);
                            parent.Add(xround);
                            break;
                        case "exists":
                            XElement xexists = new XElement("exists"); //, new XAttribute("isGrouped",true)
                            int xeStart = items.IndexOf("(", i);
                            i = RenderXml(xeStart + 1, items, ref xexists);
                            parent.Add(xexists);
                            break;
                        case "count":
                            XElement xcount = new XElement("count"); //, new XAttribute("isGrouped",true)
                            int xcStart = items.IndexOf("(", i);
                            i = RenderXml(xcStart + 1, items, ref xcount);
                            parent.Add(xcount);
                            break;
                        case "sum":
                            XElement xsum = new XElement("sum"); //, new XAttribute("isGrouped",true)
                            int xgStart = items.IndexOf("(", i);
                            i = RenderXml(xgStart + 1, items, ref xsum);
                            parent.Add(xsum);
                            break;
                        default:
                            if (item.StartsWith("$") || item.StartsWith("-$"))
                            {
                                bool negative = false;
                                if (item.StartsWith("-"))
                                {
                                    negative = true;
                                    item = item.Substring(1);
                                }
                                parent.Add(new XElement("variable", new XAttribute("name", item.Substring(1)),new XAttribute("negative",negative)));
                            }
                            else if (item == ",")
                            {
                            }
                            else if (reNumeric.IsMatch(item))
                            {
                                parent.Add(new XElement("number", item));
                            }
                            else
                            {
                                parent.Add(new XElement(item));
                            }
                            break;
                    }
                }
            }
            return items.Count+1;
        }
    }
}

/*
 *  private readonly Regex countRegex = new Regex(@"count(-?\d+.?\d*)", RegexOptions.Compiled);
        private readonly Regex bracketsRegex = new Regex(@"([a-z]*)\(([^\(\)]+)\)(\^|!?)", RegexOptions.Compiled);
        private readonly Regex cosRegex = new Regex(@"cos(-?\d+.?\d*)", RegexOptions.Compiled);
        private readonly Regex sinRegex = new Regex(@"sin(-?\d+.?\d*)", RegexOptions.Compiled);
        private readonly Regex powerRegex = new Regex(@"(-?\d+\.?\d*)\^(-?\d+\.?\d*)", RegexOptions.Compiled);
        private readonly Regex multiplyRegex = new Regex(@"(-?\d+\.?\d*)\*(-?\d+\.?\d*)", RegexOptions.Compiled);
        private readonly Regex divideRegex = new Regex(@"(-?\d+\.?\d*)/(-?\d+\.?\d*)", RegexOptions.Compiled);
        private readonly Regex addRegex = new Regex(@"(-?\d+\.?\d*)\+(-?\d+\.?\d*)", RegexOptions.Compiled);
        private readonly Regex subtractRegex = new Regex(@"(-?\d+\.?\d*)-(-?\d+\.?\d*)", RegexOptions.Compiled);
 * 
 * 
foreach (XElement el in genlink.Elements())
                {
                    
                    if (el.Name.Namespace == namespaces["va"])
                    {
                    }
                    else if (el.Name.Namespace == namespaces["generic"])
                    {
                    }
                    else if (el.Name.Namespace == namespaces["validation"])
                    {
                    }
                    else if (el.Name.Namespace == namespaces["variable"])
                    {
                    }
                    else if (el.Name.Namespace == namespaces["cf"])
                    {
                    }
                    else if (el.Name.Namespace == namespaces["formula"])
                    {
                    }
                    else if (el.Name.Namespace == namespaces["ca"])
                    {
                    }
                    else if (el.Name.Namespace == namespaces["ea"])
                    {
                    }
                    else if (el.Name.Namespace == namespaces["pf"])
                    {
                    }
                    else if (el.Name.Namespace == namespaces["gf"])
                    {
                    }
                    else if (el.Name.Namespace == namespaces["uf"])
                    {
                    }
                    else if (el.Name.Namespace == namespaces["label"])
                    {
                    }
                    else if (el.Name.Namespace == namespaces["xfi"])
                    {
                    }
                    else if (el.Name.Namespace == namespaces["xsi"])
                    {
                    }
                    else if (el.Name.Namespace == namespaces["xs"])
                    {
                    }
                    else if (el.Name.Namespace == namespaces["v"])
                    {
                    }
                    else if (el.Name.Namespace == namespaces["reference"])
                    {
                    }
                    else if (el.Name.Namespace == namespaces["ref"])
                    {
                    }
                    else if (el.Name.Namespace == namespaces["mf"])
                    {
                    }
                    else if (el.Name.Namespace == namespaces["tf"])
                    {
                    }
                    else if (el.Name.Namespace == namespaces["xbrli"])
                    {
                    }
                    else if (el.Name.Namespace == namespaces["fn"])
                    {
                    }
                    else if (el.Name.Namespace == namespaces["tax-inc"])
                    {
                    }
                    else
                    {
                        Console.WriteLine("UNKNOWN NAMESPACE {0}", el.Name.Namespace);
                    }
                    //
                    
                }
*/