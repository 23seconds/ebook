﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl"
>
  <xsl:output method="xml" indent="yes"/>

  <xsl:variable name="assess">2012</xsl:variable>
  <xsl:variable name="culture">nl</xsl:variable> 
  <xsl:variable name="target">nrcorp</xsl:variable>
  <xsl:variable name="smallcase" select="'abcdefghijklmnopqrstuvwxyz'" />
  <xsl:variable name="uppercase" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZ'" />
  <xsl:variable name="_crlf"><xsl:text>
</xsl:text></xsl:variable>
  <xsl:variable name="crlf" select="string($_crlf)"/>
  <xsl:variable name="jscontainer">eBook.BizTax.AY<xsl:value-of select="$assess"/>_<xsl:value-of select="$target"/></xsl:variable>
  <xsl:template match="root">
    <xsl:apply-templates select="Presentation"/>
  </xsl:template>
  
    <xsl:template match="Presentation">
      <basicvalidations>
        <xsl:for-each select="Tab[@hidden='false']//Pane[@hidden='false']//Field[@hidden='false' and not(@isAbstract='true')]">
          <field>
            <xsl:variable name="fieldPrefix" select="substring-before(@xbrlId,'_')"/>
            <xsl:attribute name="oldCode"><xsl:apply-templates mode="GetOldCode" select="."/></xsl:attribute>
            <xsl:attribute name="code"><xsl:apply-templates mode="GetCode" select="."/></xsl:attribute>
            <xsl:attribute name="prefix"><xsl:value-of select="$fieldPrefix"/></xsl:attribute>
            <xsl:attribute name="name"><xsl:value-of select="@xbrlName"/></xsl:attribute>
            <xsl:apply-templates select="." mode="GetFieldType"/>
            <periods>
              <xsl:if test="Context/period">
                <xsl:choose>
                  <xsl:when test="Context/period/@start='true' and Context/period/@end='true'">
                    <period id="I-Start"/>
                    <period id="I-End"/>
                  </xsl:when>
                  <xsl:when test="(Context/period/@start='false' or not(Context/period/@start)) and Context/period/@end='true'">
                    <period id="I-End"/>
                  </xsl:when>
                  <xsl:when test="Context/period/@start='true' and (Context/period/@end='false'  or not(Context/period/@end))">
                    <period id="I-Start"/>
                  </xsl:when>
                  <xsl:when test="not(Context/period/@start) and not(Context/period/@end)">
                    <period id="D"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <period id="D"/>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:if>
            </periods>
            <labels>
              <label lang="nl">
              <xsl:apply-templates select="." mode="GetLabel">
                <xsl:with-param name="type" select="'label'"/>
                <xsl:with-param name="myculture" select="'nl'"/>
              </xsl:apply-templates>
              </label>
              <label lang="fr">
                <xsl:apply-templates select="." mode="GetLabel">
                  <xsl:with-param name="type" select="'label'"/>
                  <xsl:with-param name="myculture" select="'fr'"/>
                </xsl:apply-templates>
              </label>
              <label lang="de">
                <xsl:apply-templates select="." mode="GetLabel">
                  <xsl:with-param name="type" select="'label'"/>
                  <xsl:with-param name="myculture" select="'de'"/>
                </xsl:apply-templates>
              </label>
              <label lang="en">
                <xsl:value-of select="@xbrlName"/>
              </label>
            </labels>
          </field>
        </xsl:for-each>
      </basicvalidations>
    </xsl:template>


  <xsl:template match="*" mode="GetCode">
    <xsl:if test="not(starts-with(@xbrlId,'pfs-'))">
      <xsl:apply-templates select="." mode="GetLabel">
        <xsl:with-param name="type">documentation</xsl:with-param>
        <xsl:with-param name="myculture">en</xsl:with-param>
      </xsl:apply-templates>
    </xsl:if>
  </xsl:template>

  <xsl:template match="*" mode="GetOldCode">
    <xsl:apply-templates select="." mode="GetLabel">
      <xsl:with-param name="type">documentation</xsl:with-param>
      <xsl:with-param name="myculture">en-US</xsl:with-param>
    </xsl:apply-templates>
  </xsl:template>

  <xsl:template name="ClearOutApos">
    <xsl:param name="txt"/>
    <xsl:variable name="apos">'</xsl:variable>
    <xsl:variable name="aposEsc">\'</xsl:variable>
    <xsl:call-template name="string-replace-all">
      <xsl:with-param name="text" select="string($txt)" />
      <xsl:with-param name="replace" select="$apos" />
      <xsl:with-param name="by" select="$aposEsc" />
    </xsl:call-template>
  </xsl:template>
  
  <xsl:template match="*" mode="GetLabel">
    <xsl:param name="type"/>
    <xsl:param name="myculture"/>
    <xsl:variable name="selCulture"><xsl:choose>
      <xsl:when test="$myculture and not($myculture='')"><xsl:value-of select="$myculture"/></xsl:when>
      <xsl:otherwise><xsl:value-of select="$culture"/></xsl:otherwise></xsl:choose></xsl:variable>
    <xsl:variable name="apos">'</xsl:variable>
    <xsl:variable name="aposEsc">\'</xsl:variable>
    <xsl:variable name="txt">
      <xsl:choose>
        <xsl:when test="labels/label[@type=$type]">
          <xsl:value-of select="labels/label[@type=$type]/translation[@lang=$selCulture]"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:if test="labels/label">
            <xsl:value-of select="labels/label[1]/translation[@lang=$selCulture]"/>
          </xsl:if>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:call-template name="string-replace-all">
      <xsl:with-param name="text" select="string($txt)" />
      <xsl:with-param name="replace" select="$apos" />
      <xsl:with-param name="by" select="$aposEsc" />
    </xsl:call-template>
  </xsl:template>

  <xsl:template match="Field|Pane|ValueList" mode="FieldTypeAttributes">
  
  </xsl:template>

 
  <xsl:template match="Field" mode="GetFieldType">
    <xsl:choose>
      <xsl:when test="not(@itemType) or @itemType=''"></xsl:when>
      <xsl:otherwise>
        <xsl:variable name="itemType" select="@itemType"/>
        <xsl:variable name="typeEl" select="//XbrlDataType[Name=$itemType]"/>
        <xsl:choose>
          <xsl:when test="$typeEl">
            <xsl:apply-templates select="$typeEl" mode="GetFieldType"/>
          </xsl:when>
          <xsl:otherwise>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

   <xsl:template match="TypeElement" mode="GetFieldType">
     
  </xsl:template>
  
  <xsl:template match="XbrlDataType|DataType" mode="GetFieldType">
    <xsl:variable name="itemType" select="Name/text()"/>
    <xsl:choose>
        <xsl:when test="./ValueType='System.Decimal'"><xsl:apply-templates select="." mode="DetermineNumberFieldDef"><xsl:with-param name="itemType" select="$itemType"/></xsl:apply-templates></xsl:when>
        <xsl:when test="./ValueType='System.Byte[]'"><xsl:apply-templates select="." mode="BundleBuilderFieldDef"/></xsl:when>
        <xsl:when test="./ValueType='System.DateTime'"><xsl:apply-templates select="." mode="DateFieldDef"/></xsl:when>
        <xsl:when test="./ValueType='System.String'"><xsl:apply-templates select="." mode="TextFieldDef"/></xsl:when>
        <xsl:when test="./ValueType='System.Single'"><xsl:apply-templates select="." mode="DetermineNumberFieldDef"><xsl:with-param name="itemType" select="$itemType"/></xsl:apply-templates></xsl:when>
        <xsl:when test="./ValueType='System.Double'"><xsl:apply-templates select="." mode="DetermineNumberFieldDef"><xsl:with-param name="itemType" select="$itemType"/></xsl:apply-templates></xsl:when>
        <xsl:when test="./ValueType='System.Single'"><xsl:apply-templates select="." mode="DetermineNumberFieldDef"><xsl:with-param name="itemType" select="$itemType"/></xsl:apply-templates></xsl:when>
        <xsl:when test="./ValueType='System.Int64'"><xsl:apply-templates select="." mode="DetermineNumberFieldDef"><xsl:with-param name="itemType" select="$itemType"/></xsl:apply-templates></xsl:when>
        <xsl:when test="./ValueType='System.Int32'"><xsl:apply-templates select="." mode="DetermineNumberFieldDef"><xsl:with-param name="itemType" select="$itemType"/></xsl:apply-templates></xsl:when>
        <xsl:when test="./ValueType='System.Int16'"><xsl:apply-templates select="." mode="DetermineNumberFieldDef"><xsl:with-param name="itemType" select="$itemType"/></xsl:apply-templates></xsl:when>
        <xsl:when test="./ValueType='System.UInt64'"><xsl:apply-templates select="." mode="DetermineNumberFieldDef"><xsl:with-param name="itemType" select="$itemType"/></xsl:apply-templates></xsl:when>
        <xsl:when test="./ValueType='System.UInt32'"><xsl:apply-templates select="." mode="DetermineNumberFieldDef"><xsl:with-param name="itemType" select="$itemType"/></xsl:apply-templates></xsl:when>
        <xsl:when test="./ValueType='System.UInt16'"><xsl:apply-templates select="." mode="DetermineNumberFieldDef"><xsl:with-param name="itemType" select="$itemType"/></xsl:apply-templates></xsl:when>
        <xsl:when test="./ValueType='System.SByte'"><xsl:apply-templates select="." mode="BundleBuilderFieldDef"/></xsl:when>
        <xsl:when test="./ValueType='System.Byte'"><xsl:apply-templates select="." mode="BundleBuilderFieldDef"/></xsl:when>
        <xsl:when test="./ValueType='System.Boolean'"><xsl:apply-templates select="." mode="CheckFieldDef"/></xsl:when>
        <xsl:when test="./ValueType='System.Uri'"><xsl:apply-templates select="." mode="TextFieldDef"/></xsl:when>
        <xsl:when test="./ValueType='System.TimeSpan'"><xsl:apply-templates select="." mode="DetermineNumberFieldDef"><xsl:with-param name="itemType" select="$itemType"/></xsl:apply-templates></xsl:when>
        <xsl:when test="./ValueType='System.Object'"></xsl:when>
      </xsl:choose>
  </xsl:template>
  

  <xsl:template match="XbrlDataType|DataType" mode="DetermineNumberFieldDef">
    <xsl:param name="itemType"/>
    <xsl:choose><xsl:when test="contains($itemType,'monetary') or contains($itemType,'Monetary')"><xsl:apply-templates select="." mode="CurrencyFieldDef"/></xsl:when><xsl:when test="contains($itemType,'percent')"><xsl:apply-templates select="." mode="PercentageFieldDef"/></xsl:when><xsl:otherwise><xsl:apply-templates select="." mode="NumberFieldDef"/></xsl:otherwise></xsl:choose>
  </xsl:template>
  
  <xsl:template match="XbrlDataType|DataType" mode="TextFieldDef">
    <xsl:attribute name="type">text</xsl:attribute>
    <restrictions>
    <xsl:for-each select="Restrictions/Restriction">
      <xsl:choose>
        <xsl:when test="Name='System.Xml.Schema.XmlSchemaPatternFacet'">
          <regex><xsl:value-of select="Value"/></regex>
        </xsl:when>
        <xsl:when test="Name='System.Xml.Schema.XmlSchemaMinLengthFacet'">
          <minlength><xsl:value-of select="Value"/></minlength>
        </xsl:when>
        <xsl:when test="Name='System.Xml.Schema.XmlSchemaMaxLengthFacet'">
          <maxlength><xsl:value-of select="Value"/></maxlength>
       </xsl:when>        
      </xsl:choose>
    </xsl:for-each>
    </restrictions>
  </xsl:template>

  <xsl:template match="XbrlDataType|DataType" mode="CheckFieldDef">
    <xsl:attribute name="type">bool</xsl:attribute>
  </xsl:template>

  <xsl:template match="XbrlDataType|DataType" mode="NumberFieldDef">
    <xsl:attribute name="type">numeric</xsl:attribute>
    <xsl:apply-templates select="." mode="SetNumberFieldProps"/>
  </xsl:template>

  <xsl:template match="XbrlDataType|DataType" mode="CurrencyFieldDef">
    <xsl:attribute name="type">numeric</xsl:attribute>
    <xsl:apply-templates select="." mode="SetNumberFieldProps"/>
  </xsl:template>

  <xsl:template match="XbrlDataType|DataType" mode="PercentageFieldDef">
    <xsl:attribute name="type">numeric</xsl:attribute>
    <xsl:apply-templates select="." mode="SetNumberFieldProps"/>
  </xsl:template>

  <xsl:template match="XbrlDataType|DataType" mode="SetNumberFieldProps">
    <restrictions>
    <xsl:for-each select="Restrictions/Restriction">
      <xsl:choose>
        <xsl:when test="Name='System.Xml.Schema.XmlSchemaTotalDigitsFacet'">
          <maxlength><xsl:value-of select="Value"/></maxlength></xsl:when>
        <xsl:when test="Name='System.Xml.Schema.XmlSchemaFractionDigitsFacet'">
          <precision><xsl:value-of select="Value"/></precision></xsl:when>
        <xsl:when test="Name='System.Xml.Schema.XmlSchemaMinInclusiveFacet'">
          <minValue><xsl:value-of select="Value"/></minValue></xsl:when>
        <xsl:when test="Name='System.Xml.Schema.XmlSchemaMaxInclusiveFacet'">
          <maxValue><xsl:value-of select="Value"/></maxValue></xsl:when>
      </xsl:choose>
    </xsl:for-each>
    <xsl:if test="TypeCode='Integer' and not(Restrictions/Restriction[Name='System.Xml.Schema.XmlSchemaFractionDigitsFacet'])">
      <nodecimals/>
    </xsl:if>
      </restrictions>
  </xsl:template>

  <xsl:template match="XbrlDataType|DataType" mode="DateFieldDef">
    <xsl:attribute name="type">date</xsl:attribute>
    <restrictions>
    <xsl:for-each select="Restrictions/Restriction">
      <xsl:choose>
        <xsl:when test="Name='System.Xml.Schema.XmlSchemaMinInclusiveFacet'">
          <minValue><xsl:value-of select="Value"/></minValue></xsl:when>
        <xsl:when test="Name='System.Xml.Schema.XmlSchemaMaxInclusiveFacet'">
          <maxValue><xsl:value-of select="Value"/></maxValue></xsl:when>
      </xsl:choose>
    </xsl:for-each>
    </restrictions>
  </xsl:template>

  <xsl:template match="XbrlDataType|DataType" mode="BundleBuilderFieldDef">
    <xsl:attribute name="type">binary</xsl:attribute>
    <restrictions>
      <xsl:if test="Name='nonEmptyBase64BinaryItemType'">
        <required/>
      </xsl:if>
    </restrictions>
  </xsl:template>

  <xsl:template name="string-replace-all">
    <xsl:param name="text" />
    <xsl:param name="replace" />
    <xsl:param name="by" />
    <xsl:choose>
      <xsl:when test="contains($text, $replace)">
        <xsl:value-of select="substring-before($text,$replace)" />
        <xsl:value-of select="$by" />
        <xsl:call-template name="string-replace-all">
          <xsl:with-param name="text"
          select="substring-after($text,$replace)" />
          <xsl:with-param name="replace" select="$replace" />
          <xsl:with-param name="by" select="$by" />
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:call-template name="crlf-replace"><xsl:with-param name="subject" select="$text"/></xsl:call-template>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  
  <xsl:template name="crlf-replace">
    <xsl:param name="subject"/>

    <xsl:choose>
      <xsl:when test="contains($subject, $crlf)">
        <xsl:value-of select="substring-before($subject, $crlf)"/><xsl:call-template name="crlf-replace"><xsl:with-param name="subject" select="substring-after($subject, $crlf)"/></xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$subject"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
</xsl:stylesheet>
