﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl">
<!--
TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO 

assertions:
declare parameters.

declare variables
load variables

precondition if true
validation formula if false
label

-->
  <!--
  res = self.getElValue('tax-inc_TaxableReservesCapitalSharePremiums_I-Start', store) + self.getElValue('tax-inc_TaxablePortionRevaluationSurpluses_I-Start', store) + self.getElValue('tax-inc_LegalReserve_I-Start', store) + self.getElValue('tax-inc_UnavailableReserves_I-Start', store) + self.getElValue('tax-inc_AvailableReserves_I-Start', store) + self.getElValue('tax-inc_AccumulatedProfitsLosses_I-Start', store) + self.getElValue('tax-inc_TaxableProvisions_I-Start', store) +
  self.getSumOf('tax-inc_OtherReserves', 'I-Start', store) + self.getSumOf('tax-inc_OtherTaxableReserves', 'I-Start', store) + self.getElValue('tax-inc_TaxableWriteDownsUndisclosedReserve_I-Start', store) + self.getElValue('tax-inc_ExaggeratedDepreciationsUndisclosedReserve_I-Start', store) + self.getElValue('tax-inc_OtherUnderestimationsAssetsUndisclosedReserve_I-Start', store) + self.getElValue('tax-inc_OtherOverestimationsLiabilitiesUndisclosedReserve_I-Start', store) };
  store = self.checkStore(res,store);
  results.push(res);-->
  <xsl:output method="text" indent="no"/>
  <xsl:variable name="culture">nl</xsl:variable>

  <xsl:template name="EXTJS">
    Ext = {

    /**
    * The version of the framework
    * @type String
    */
    version: '3.2.1',
    versionDetail: {
    major: 3,
    minor: 2,
    patch: 1
    }
    };

    Ext.apply = function(o, c, defaults) {
    // no "this" reference for friendly out of scope calls
    if (defaults) {
    Ext.apply(o, defaults);
    }
    if (o &amp;&amp; c &amp;&amp; typeof c == 'object') {
    for (var p in c) {
    o[p] = c[p];
    }
    }
    return o;
    };


    Ext.apply(Ext, {

    USE_NATIVE_JSON: false,
    applyIf: function(o, c) {
    if (o) {
    for (var p in c) {
    if (!Ext.isDefined(o[p])) {
    o[p] = c[p];
    }
    }
    }
    return o;
    },
    extend: function() {
    // inline overrides
    var io = function(o) {
    for (var m in o) {
    this[m] = o[m];
    }
    };
    var oc = Object.prototype.constructor;

    return function(sb, sp, overrides) {
    if (typeof sp == 'object') {
    overrides = sp;
    sp = sb;
    sb = overrides.constructor != oc ? overrides.constructor : function() { sp.apply(this, arguments); };
    }
    var F = function() { },
    sbp,
    spp = sp.prototype;

    F.prototype = spp;
    sbp = sb.prototype = new F();
    sbp.constructor = sb;
    sb.superclass = spp;
    if (spp.constructor == oc) {
    spp.constructor = sp;
    }
    sb.override = function(o) {
    Ext.override(sb, o);
    };
    sbp.superclass = sbp.supr = (function() {
    return spp;
    });
    sbp.override = io;
    Ext.override(sb, overrides);
    sb.extend = function(o) { return Ext.extend(sb, o); };
    return sb;
    };
    } (),

    override: function(origclass, overrides) {
    if (overrides) {
    var p = origclass.prototype;
    Ext.apply(p, overrides);
    if (false &amp;&amp; overrides.hasOwnProperty('toString')) {
    p.toString = overrides.toString;
    }
    }
    },
    toArray: function() {
    return false ?
    function(a, i, j, res) {
    res = [];
    for (var x = 0, len = a.length; x &lt; len; x++) {
    res.push(a[x]);
    }
    return res.slice(i || 0, j || res.length);
    } :
    function(a, i, j) {
    return Array.prototype.slice.call(a, i || 0, j || a.length);
    }
    } (),


    isEmpty: function(v, allowBlank) {
    return v === null || v === undefined || ((Ext.isArray(v) &amp;&amp; !v.length)) || (!allowBlank ? v === '' : false);
    },
    isArray: function(v) {
    return toString.apply(v) === '[object Array]';
    },
    isDate: function(v) {
    return toString.apply(v) === '[object Date]';
    },
    isObject: function(v) {
    return !!v &amp;&amp; Object.prototype.toString.call(v) === '[object Object]';
    },
    isPrimitive: function(v) {
    return Ext.isString(v) || Ext.isNumber(v) || Ext.isBoolean(v);
    },
    isFunction: function(v) {
    return toString.apply(v) === '[object Function]';
    },
    isNumber: function(v) {
    return typeof v === 'number' &amp;&amp; isFinite(v);
    },
    isString: function(v) {
    return typeof v === 'string';
    },
    isBoolean: function(v) {
    return typeof v === 'boolean';
    },
    isElement: function(v) {
    return v ? !!v.tagName : false;
    },
    isDefined: function(v) {
    return typeof v !== 'undefined';
    },


    /**
    * True if the detected browser is Opera.
    * @type Boolean
    */
    isOpera: false,

    /**
    * True if the detected browser uses WebKit.
    * @type Boolean
    */
    isWebKit: true,

    /**
    * True if the detected browser is Chrome.
    * @type Boolean
    */
    isChrome: true,

    /**
    * True if the detected browser is Safari.
    * @type Boolean
    */
    isSafari: false,

    /**
    * True if the detected browser is Safari 3.x.
    * @type Boolean
    */
    isSafari3: false,

    /**
    * True if the detected browser is Safari 4.x.
    * @type Boolean
    */
    isSafari4: false,

    /**
    * True if the detected browser is Safari 2.x.
    * @type Boolean
    */
    isSafari2: false,

    /**
    * True if the detected browser is Internet Explorer.
    * @type Boolean
    */
    isIE: false,

    /**
    * True if the detected browser is Internet Explorer 6.x.
    * @type Boolean
    */
    isIE6: false,

    /**
    * True if the detected browser is Internet Explorer 7.x.
    * @type Boolean
    */
    isIE7: false,

    /**
    * True if the detected browser is Internet Explorer 8.x.
    * @type Boolean
    */
    isIE8: false,

    /**
    * True if the detected browser uses the Gecko layout engine (e.g. Mozilla, Firefox).
    * @type Boolean
    */
    isGecko: false,

    /**
    * True if the detected browser uses a pre-Gecko 1.9 layout engine (e.g. Firefox 2.x).
    * @type Boolean
    */
    isGecko2: false,

    /**
    * True if the detected browser uses a Gecko 1.9+ layout engine (e.g. Firefox 3.x).
    * @type Boolean
    */
    isGecko3: false,

    /**
    * True if the detected browser is Internet Explorer running in non-strict mode.
    * @type Boolean
    */
    isBorderBox: false,

    /**
    * True if the detected platform is Linux.
    * @type Boolean
    */
    isLinux: false,

    /**
    * True if the detected platform is Windows.
    * @type Boolean
    */
    isWindows: true,

    /**
    * True if the detected platform is Mac OS.
    * @type Boolean
    */
    isMac: false,

    /**
    * True if the detected platform is Adobe Air.
    * @type Boolean
    */
    isAir: false
    });

  </xsl:template>

  <xsl:template name="STORE_FUNCTIONS">
    self.storeFunctions = {
    registerField: function(fieldId, elementId, path, xtype) {
    var eid = path ? xtype != 'combo' ? path + '/' + elementId : path : elementId;
    if (!this.elementsByFields[fieldId]) this.elementsByFields[fieldId] = eid;
    if (!this.fieldsByElements[elementId]) this.fieldsByElements[elementId] = [];
    this.fieldsByElements[elementId].push(fieldId);
    return this.getElementValueById(elementId, xtype);
    }
    , setValueByField: function(fld) {
    var id = this.elementsByFields[fld.id];
    if (id.indexOf('/') &gt; -1) {
    // structured node
    var xtype = fld.getXType();
    var lst = id.split('/');
    var mi = this.elementsByNamePeriodContext[lst[0]];
    if (!mi) {
    var nmsplit = lst[0].split('_');
    this.addElement(this.createStructuredElement(nmsplit[0], nmsplit[1]));
    mi = this.elementsByNamePeriodContext[lst[0]];
    }
    var midx = mi[0];
    var el = this.elements[midx];
    for (var i = 1; i &lt; lst.length; i++) {
    if (el.Children == null) el.Children = [];
    var nel = this.getElementChildById(el, lst[i]);
    if (nel == null) {
    if (i &lt; lst.length - 1) {
    var its = lst[i].split('_');
    nel = this.createStructuredElement(its[0], its[1]);
    el.Children.push(nel);

    } else {
    if (xtype == 'combo') {
    var its = lst[i].split('_');
    nel = this.createStructuredElement(its[0], its[1]);
    el.Children.push(nel);
    } else {
    nel = this.createElementFromField(fld);
    el.Children.push(nel);
    }
    }
    }
    el = nel;
    }
    if (el == null) return null;
    this.updateElement(el, fld.getValue(), xtype, fld);
    return this.retrieveElementValue(el);
    }
    else {

    if (id == null) {
    // NEW CREATION
    this.addElementByField(fld);
    return;
    }
    var idx = this.elementsByNamePeriodContext[id];
    if (idx) {
    this.updateElement(idx[0], fld.getValue(), fld.getXType(), fld);
    } else {
    this.addElementByField(fld);
    return;
    }
    }
    }
    , createElementByCalcCfg: function(cfg) {
    var cref = cfg.period;
    if (cfg.context != null &amp;&amp; cfg.context != '') cref += ('__' + cref.context);
    var el = {
    AutoRendered: false
    , Children: null
    , Context: cfg.context
    , ContextRef: cref
    , Decimals: "INF" // to determine?
    , Id: cfg.id
    , Name: cfg.name
    , NameSpace: ""
    , Period: cfg.period
    , Prefix: cfg.prefix
    , UnitRef: "EUR" // to determine?
    , Value: "" + (cfg.value == null ? "" : cfg.value)
    };
    return el;
    }
    , createComboValueElement: function(fld) {

    var val = fld.getValue(); // is id like pfs-vl_XCode_LegalFormCode_001
    if (Ext.isEmpty(val)) return null;
    var splitted = val.split('_');
    var prefix = splitted[0];
    var value = splitted[splitted.length - 1];
    //splitted.splice(splitted.length - 1,1);
    splitted.splice(0, 1);
    var name = splitted.join('_');
    return {
    AutoRendered: false
    , Children: null
    , BinaryValue: null
    , Context: fld.XbrlDef.Context
    , ContextRef: fld.XbrlDef.ContextRef
    , Decimals: fld.XbrlDef.Decimals
    , Id: ''
    , Name: name
    , NameSpace: ""
    , Period: fld.XbrlDef.Period
    , Prefix: prefix
    , UnitRef: fld.XbrlDef.UnitRef
    , Value: value
    };
    }
    , createElementFromField: function(fld) {
    var value = fld.getValue();
    var xtype = fld.getXType();
    var el;
    if (xtype == 'combo') {
    var child = this.createComboValueElement(fld);
    el = {
    AutoRendered: false
    , Children: child ? [child] : []
    , BinaryValue: null
    , Context: null
    , ContextRef: null
    , Decimals: null
    , Id: fld.XbrlDef.Prefix + '_' + fld.XbrlDef.Name
    , Name: fld.XbrlDef.Name
    , NameSpace: ""
    , Period: null
    , Prefix: fld.XbrlDef.Prefix
    , UnitRef: fld.XbrlDef.UnitRef
    , Value: null
    }
    } else {
    el = {
    AutoRendered: false
    , Children: null
    , BinaryValue: xtype == 'biztax-uploadfield' ? Ext.decode(Ext.encode(value)) : null
    , Context: fld.XbrlDef.Context
    , ContextRef: fld.XbrlDef.ContextRef
    , Decimals: fld.XbrlDef.Decimals
    , Id: fld.XbrlDef.Id
    , Name: fld.XbrlDef.Name
    , NameSpace: ""
    , Period: fld.XbrlDef.Period
    , Prefix: fld.XbrlDef.Prefix
    , UnitRef: fld.XbrlDef.UnitRef
    , Value: xtype != 'biztax-uploadfield' ? "" + (value == null ? "" : value) : null
    };

    }
    return el;
    }
    , addElementByField: function(fld) {

    this.addElement(this.createElementFromField(fld));
    }
    , createStructuredElement: function(prefix, name) {
    return {
    AutoRendered: false
    , Children: []
    , Context: ''
    , ContextRef: ''
    , Decimals: null
    , Id: prefix + '_' + name
    , Name: name
    , NameSpace: ""
    , Period: ''
    , Prefix: prefix
    , UnitRef: ''
    , Value: null
    };
    }
    , getValueForField: function(fieldId, xtype) {
    var id = this.elementsByFields[fieldId];
    if (id == null) return null;
    if (id.indexOf('/') &gt; -1) {
    // structured node
    var lst = id.split('/');
    var el = this.getElementById(lst[0]);
    if (el == null) return null;
    for (var i = 1; i &lt; lst.length; i++) {
    el = this.getElementChildById(el, lst[i]);
    if (el == null) return null;
    }
    if (el == null) return null;
    return this.retrieveElementValue(el, xtype);
    }
    else {
    return this.getElementValueById(id, xtype);
    }
    }
    , getElementChildById: function(mainEl, id) {
    if (mainEl.Children == null) return null;
    if (!Ext.isArray(mainEl.Children)) return null;
    for (var j = 0; j &lt; mainEl.Children.length; j++) {
    if (mainEl.Children[j].Id == id) return mainEl.Children[j];
    }
    return null;
    }
    , addContexts: function(contexts) {
    for (var i = 0; i &lt; contexts.length; i++) {
    this.contexts[contexts[i].Id] = contexts[i];
    }
    }
    , createContext: function() { }
    , removeContexts: function(refs) {
    if (!Ext.isDefined(refs)) return;
    if (!Ext.isArray(refs)) refs = [refs];
    if (refs.length == 0) return;
    var args = [this.contexts];
    args = args.concat(refs);
    Ext.destroyMembers(args);
    for (var i = 0; i &lt; refs.length; i++) {
    refs[i] = refs[i].replace('D__', '').replace('I-Start__', '').replace('I-End__', '');
    }
    refs = Ext.unique(refs);
    var els = [];
    for (var i = 0; i &lt; refs.length; i++) {
    var arrs = this.elementsByContext[refs[i]];
    if (arrs != null) els = els.concat(arrs);
    }
    if (els.length == 0) return;

    els = Ext.unique(els).sort(function(a, b) { return b - a; }); // reversed to not affect indexes while removing

    for (var i = 0; i &lt; els.length; i++) {
    this.elements.splice(els[i], 1);
    }
    //alert("reindex");
    this.indexElements();
    }
    , createElement: function() { }
    , getDataContract: function() {
    var dc = {};
    Ext.apply(dc, this.biztaxGeneral);
    dc.Elements = this.elements;
    dc.Contexts = [];
    for (var att in this.contexts) {
    if (!Ext.isFunction(this.contexts[att]) &amp;&amp; this.contexts[att].Period) dc.Contexts.push(this.contexts[att]);
    }
    return dc;
    }
    , loadData: function(callback, scope) {
    eBook.CachedAjax.request({
    url: eBook.Service.rule2012 + 'GetBizTax'
    , method: 'POST'
    , params: Ext.encode({ cfdc: {
    FileId: eBook.Interface.currentFile.get('Id')
    , Culture: eBook.Interface.Culture
    }
    })
    , callback: this.onDataRetreived
    , scope: this
    , callerCallback: { fn: callback, scope: scope }
    });
    }
    , onDataRetreived: function(opts, success, resp) {
    if (!success) {
    alert("failed!");
    opts.callerCallback.fn.call(opts.callerCallback.scope || this);
    return null;
    }
    var robj = Ext.decode(resp.responseText);
    var dta = robj.GetBizTaxResult;
    this.clearData(); // clear existing data
    // copy global info
    Ext.copyTo(this.biztaxGeneral, dta, ["AssessmentYear", "EntityIdentifier", "Units"]);

    // load in contexts by Id
    this.contexts = {};
    for (var i = 0; i &lt; dta.Contexts.length; i++) {
    this.contexts[dta.Contexts[i].Id] = dta.Contexts[i];
    }

    // load in elements
    this.elements = dta.Elements;

    // index elements
    this.indexElements();

    opts.callerCallback.fn.call(opts.callerCallback.scope || this);

    }
    , addIdx: function(idxName, id, idx) {
    if (!this[idxName][id]) this[idxName][id] = [];
    this[idxName][id].push(idx);
    }
    , indexElements: function() {
    this.clearElementIndexes();
    for (var i = 0; i &lt; this.elements.length; i++) {
    var el = this.elements[i];
    this.addElementToIndex(el, i);
    }
    }
    , addElement: function(el) {
    if (!this.elementsByNamePeriodContext[el.id]) {
    this.elements.push(el);
    this.addElementToIndex(el, this.elements.length - 1);
    return this.elements.length - 1;
    } else {
    var idx = this.elementsByNamePeriodContext[el.id];
    this.elements[idx] = el;
    return idx;
    }
    }
    , addElementToIndex: function(el, i) {
    var id = el.Id,
    fullName = el.Prefix + '_' + el.Name;

    // unique idx
    // id = fullName + '_' + el.Period + '_' + el.Context;
    this.addIdx('elementsByNamePeriodContext', id, i);

    // non-unique idx's
    this.addIdx('elementsByName', fullName, i);

    id = el.Period;
    this.addIdx('elementsByPeriod', id, i);

    id = fullName + '_' + el.Period;
    this.addIdx('elementsByNamePeriod', id, i);

    id = el.Context;
    this.addIdx('elementsByContext', id, i);

    id = fullName + '_' + el.Context;
    this.addIdx('elementsByNameContext', id, i);
    }
    , clearElementIndexes: function() {
    this.elementsByPeriod = {};
    this.elementsByNamePeriod = {};
    this.elementsByContext = {};
    this.elementsByNameContext = {};
    this.elementsByNamePeriodContext = {}; //unique
    this.elementsByName = {};
    }
    , clearData: function() {
    this.biztaxGeneral = {};
    this.contexts = {};
    this.elements = [];
    this.clearElementIndexes();
    //        this.elementsByFields = {};
    //        this.fieldsByElements = {};
    }
    , getElementsByIndexes: function(idxs) {
    var res = [];
    if (!Ext.isArray(idxs)) idxs = [idxs];
    for (var i = 0; i &lt; idxs.length; i++) {
    res.push(this.elements[idxs[i]]);
    }
    return res;
    }
    , getElementValueById: function(id, xtype) {
    var el = this.getElementById(id);
    return this.retrieveElementValue(el, xtype);
    }
    , retrieveElementValue: function(el, xtype) {
    if (!el) return null;
    if (!Ext.isEmpty(el.Decimals)) {
    return Ext.isEmpty(el.Value) ? 0 : parseFloat(el.Value);
    }
    if (xtype == "biztax-uploadfield") {
    return el.BinaryValue;
    }
    if (xtype == "combo") {
    if (el.Children != null &amp;&amp; el.Children.length &gt; 0) {
    return el.Children[0].Prefix + '_' + el.Children[0].Name;
    }
    }
    //  if (el.Value == "true" || el.Value == "false") return eval(el.Value);
    return el.Value;
    }
    , getNumericElementValueById: function(id) {
    var val = this.getElementValueById(id);
    if (val == null) return 0;
    return val;
    }
    , addOrUpdateElement: function(cfg) {
    if (!this.elementsByNamePeriodContext[cfg.id]) {
    this.addElement(this.createElementByCalcCfg(cfg));
    } else {
    this.updateElement(cfg.id, cfg.value, '', null, true);
    }
    }
    , updateElement: function(idxorEl, value, xtype, fld, donotoverwrite) {
    if (Ext.isString(idxorEl)) {
    idxorEl = this.elementsByNamePeriodContext[idxorEl];
    if (idxorEl == null || !Ext.isDefined(idxorEl)) return;
    }
    if (Ext.isObject(idxorEl)) {
    // for child elements.
    if (idxorEl.AutoRendered &amp;&amp; donotoverwrite) return;
    if (xtype == "biztax-uploadfield") {
    idxorEl.BinaryValue = Ext.decode(Ext.encode(value));
    idxorEl.Value = null;
    } if (xtype == "combo") {
    var child = this.createComboValueElement(fld);
    idxorEl.Children = child ? [child] : [];
    } else {
    idxorEl.Value = "" + (value == null ? "" : value);
    }
    idxorEl.AutoRendered = false;
    return idxorEl;
    } else {
    if (this.elements[idxorEl].AutoRendered &amp;&amp; donotoverwrite) return;
    if (xtype == "biztax-uploadfield") {
    this.elements[idxorEl].BinaryValue = Ext.decode(Ext.encode(value));
    this.elements[idxorEl].Value = null;
    } if (xtype == "combo") {
    var child = this.createComboValueElement(value);
    this.elements[idxorEl].Children = child ? [child] : [];
    } else {
    this.elements[idxorEl].Value = "" + (value == null ? "" : value);
    }
    this.elements[idxorEl].AutoRendered = false;
    }
    }
    , findElementsByIndex: function(idxName, id) {
    if (!this[idxName]) return [];
    if (!this[idxName][id]) return [];
    return this.getElementsByIndexes(this[idxName][id]);
    }
    , getElementById: function(id) {
    var res = this.findElementsForId(id);
    if (res.length == 0) return null;
    return res[0];
    }
    , findElementsForId: function(id) {
    return this.findElementsByIndex('elementsByNamePeriodContext', id);
    }
    , findElementsForName: function(fullname) {
    return this.findElementsByIndex('elementsByName', fullname);
    }
    , findElementsForPeriod: function(period) {
    return this.findElementsByIndex('elementsByPeriod', period);
    }
    , findElementsForNamePeriod: function(fullname, period) {
    return this.findElementsByIndex('elementsByNamePeriod', fullname + '_' + period);
    }
    , findElementsForContext: function(context) {
    return this.findElementsByIndex('elementsByContext', context);
    }
    , findElementsForNameContext: function(fullname, context) {
    return this.findElementsByIndex('elementsByNameContext', fullname + '_' + context);
    }
    , getContextGridData: function(targetField) {
    var target = this.findElementsForName(targetField);
    if (target != null &amp;&amp; target.length &gt; 0) {
    var result = [];
    for (var i = 0; i &lt; target.length; i++) {
    var ctx = target[i].Context;
    var context = this.contexts['D__' + ctx];
    result.push({
    elements: this.findElementsForContext(ctx)
    , scenarios: context.Scenario
    , context: ctx
    });
    }

    return result;
    }
    return null;
    }
    , destroy: function() {
    delete this.biztaxGeneral;
    delete this.contexts;
    delete this.elements;
    delete this.elementsByPeriod;
    delete this.elementsByNamePeriod;
    delete this.elementsByContext;
    delete this.elementsByNameContext;
    delete this.elementsByNamePeriodContext;
    delete this.elementsByName;
    delete this.elementsByFields;
    delete this.fieldsByElements;
    }

    ,getElementValue:function(prefix,name,period,dimension,alldimensions,tpe,fallback) {
      return 0;
    }

    };
  </xsl:template>

  <xsl:template name="SELF_FUNCTIONS">

    self.getElIdx = function(elId, data) {
    if (data.elementsByNamePeriodContext[elId]) {
    return data.elementsByNamePeriodContext[elId];
    }
    return -1;
    }

    self.getEl = function(elId, data) {
    var idx = self.getElIdx(elId, data);
    if (idx&gt;-1) {
    var idx = data.elementsByNamePeriodContext[elId];
    return data.elements[idx];
    }
    return null;
    };

    self.getElValue = function(elId, data, def) {
    var el = self.getEl(elId, data);
    return self.getValueOfEl(el,def);
    };

    self.getValueOfEl = function(el, def) {
    if (el) {
    var res = 0;
    try {
    res = parseFloat(el.Value);
    } catch (e) {
    }
    if (isNaN(res)) return def | 0;
    return res;
    } else {
    return def | 0;
    }
    };

    self.getSumOf = function(elPrefix, elName, testCtx, data) {
    var elId = elPrefix + '_' + elName;
    var idxs = data.elementsByName[elId];
    //return idxs;
    if (idxs != null) {
    var res = 0;
    for (var i = 0; i &lt; idxs.length; i++) {
    if (testCtx) {
    if (data.elements[idxs[i]].ContextRef.indexOf(testCtx) &gt; -1) {
    res += self.getValueOfEl(data.elements[idxs[i]]);
    }
    } else {
    res += self.getValueOfEl(data.elements[idxs[i]]);
    }
    }
    return res;
    }
    return 0;
    }

    self.checkStore = function(result, store) {
    // context should always exist. (are fixed contexts only?)
    //    var contextId = results[i].period;
    //    if (results[i].context != '' &amp;&amp; results[i].context != null) contextId += ('__' + results[i].context);
    //    if (!store.contexts[contextId]) {
    //        store.createContextByCfg(result);
    //    }
    if (store.elementsByNamePeriodContext[result.id] == undefined) {
    store.createElementByCalcCfg(result);
    }
    return store;
    };
  </xsl:template>

  <xsl:template name="ASSERTION_FUNCTIONS">
    
  </xsl:template>
  
  <xsl:template match="validations">
    <!--xsl:for-each select="//restrictions//*">
      <xsl:sort select="local-name(.)"/>
      
      <xsl:value-of select="local-name(.)"/>
      ,
    
    </xsl:for-each-->
    <xsl:call-template name="EXTJS"/>

    <xsl:call-template name="STORE_FUNCTIONS"/>

    <xsl:call-template name="SELF_FUNCTIONS"/>

    self.getElementValidationValue = function(el,tpe) {
      var res;
      if(tpe!='binary' &amp;&amp; Ext.isEmpty(el.Value)) return null;
      if(tpe=='binary' &amp;&amp; (Ext.isEmpty(el.BinaryValue) || !Ext.isArray(el.BinaryValue) || (Ext.isArray(el.BinaryValue) &amp;&amp; el.BinaryValue.length==0))) return null;
      switch(tpe) {
        case 'binary':
          res= el.BinaryValue;
        case 'numeric':
          try {
            res = parseFloat(el.Value);
          } catch (e) {
            res = null;
          }
          break;
        case 'text':
          res = el.Value;
          break;
        case 'bool':
          res = el.Value=='true';
          break;
        case 'date':
          try {
            res = new Date(Date.parse(el.Value));
          } catch(e) {
            return null;
          }
          break;
      }
      return res;
    };

    <!---->xsl:apply-templates select="assertions" mode="Assertions"/-->

    self.addEventListener('message', function(e) {
    var store = e.data.store, idx = -1, i=0, value=null, valid=true;
    var results = [], fields=[];
    self.parameters['periodstart'] = e.data.startdate;
    self.parameters['periodend'] = e.data.enddate;
    var re = /(I-Start)/;
    Ext.apply(store,self.storeFunctions);
    var msgs= [],fieldMsgs={};

    <xsl:apply-templates select="basicvalidations" mode="BasicValid"/>
    /*
    for(var i = 0; i&lt;self.assertions.length;i++) {
      var msg = self.assertions[i].fn(store);
    }*/

    var res = { result: results, scopeId: e.data.scopeId,msgs:msgs,fieldMsgs:fieldMsgs };
    self.postMessage(res);
    }, false);
  </xsl:template>

  <xsl:template match="basicvalidations" mode="BasicValid">
    <xsl:apply-templates select="field[count(restrictions/*)>0]" mode="BasicValid"/>
  </xsl:template>
  
  <xsl:template match="field" mode="BasicValid">
    fields = store.findElementsForName('<xsl:value-of select="@prefix"/>_<xsl:value-of select="@name"/>');
    
    for(i=0;i&lt;fields.length;i++) 
    {
        value = self.getElementValidationValue(fields[i],'<xsl:value-of select="@type"/>');
        <xsl:apply-templates select="restrictions/*" mode="BasicValid">
          <xsl:with-param name="type" select="@type"/></xsl:apply-templates>
        
    }
    <xsl:if test="restrictions/required">
      if(fields.length==0) {
        msgs.push({prefix:'<xsl:value-of select="@prefix"/>',name:'<xsl:value-of select="@name"/>', fieldName:'<xsl:value-of select="labels/label[@lang=$culture]"/>', msg:'This field is required!'});
        if(!fieldMsgs['<xsl:value-of select="@prefix"/>_<xsl:value-of select="@name"/>'])  {
            fieldMsgs['<xsl:value-of select="@prefix"/>_<xsl:value-of select="@name"/>'] = '';
        }
        fieldMsgs['<xsl:value-of select="@prefix"/>_<xsl:value-of select="@name"/>'] =msgs[msgs.length-1].msg;
      }
    </xsl:if>
    
  </xsl:template>

  <xsl:template match="maxlength" mode="BasicValid">
    <xsl:param name="type"/>
    if(value!=null &amp;&amp; value.toString().length &gt; <xsl:value-of select="."/>) {
      msgs.push({prefix:'<xsl:value-of select="../../@prefix"/>',name:'<xsl:value-of select="../../@name"/>', fieldName:'<xsl:value-of select="../../labels/label[@lang=$culture]"/>', msg:'Fieldvalue is too long, maximum length is <xsl:value-of select="." />'});
       if(!fieldMsgs['<xsl:value-of select="../../@prefix"/>_<xsl:value-of select="../../@name"/>'])  {
            fieldMsgs['<xsl:value-of select="../../@prefix"/>_<xsl:value-of select="../../@name"/>'] = '';
        }
        fieldMsgs['<xsl:value-of select="../../@prefix"/>_<xsl:value-of select="../../@name"/>'] =msgs[msgs.length-1].msg;
    }
  </xsl:template>

  <xsl:template match="minValue" mode="BasicValid">
    <xsl:param name="type"/>
    <xsl:if test="$type='numeric'">
    if(value!=null &amp;&amp; value &lt; <xsl:value-of select="."/>) {
      msgs.push({prefix:'<xsl:value-of select="../../@prefix"/>',name:'<xsl:value-of select="../../@name"/>', fieldName:'<xsl:value-of select="../../labels/label[@lang=$culture]"/>', msg:'Fieldvalue is too small, minimum allowed value is <xsl:value-of select="." />'});
       if(!fieldMsgs['<xsl:value-of select="../../@prefix"/>_<xsl:value-of select="../../@name"/>'])  {
            fieldMsgs['<xsl:value-of select="../../@prefix"/>_<xsl:value-of select="../../@name"/>'] = '';
        }
        fieldMsgs['<xsl:value-of select="../../@prefix"/>_<xsl:value-of select="../../@name"/>'] =msgs[msgs.length-1].msg;
    }
    </xsl:if>
  </xsl:template>

  <xsl:template match="maxValue" mode="BasicValid">
    <xsl:param name="type"/>
    <xsl:if test="$type='numeric'">
      if(value!=null &amp;&amp; value &gt; <xsl:value-of select="."/>) {
      msgs.push({prefix:'<xsl:value-of select="../../@prefix"/>',name:'<xsl:value-of select="../../@name"/>', fieldName:'<xsl:value-of select="../../labels/label[@lang=$culture]"/>', msg:'Fieldvalue is too great, maximum allowed value is <xsl:value-of select="." />'});
       if(!fieldMsgs['<xsl:value-of select="../../@prefix"/>_<xsl:value-of select="../../@name"/>'])  {
            fieldMsgs['<xsl:value-of select="../../@prefix"/>_<xsl:value-of select="../../@name"/>'] = '';
        }
        fieldMsgs['<xsl:value-of select="../../@prefix"/>_<xsl:value-of select="../../@name"/>'] =msgs[msgs.length-1].msg;
      }
    </xsl:if>
  </xsl:template>

  <xsl:template match="required" mode="BasicValid">
    <xsl:param name="type"/>

      if(value==null) {
      msgs.push({prefix:'<xsl:value-of select="../../@prefix"/>',name:'<xsl:value-of select="../../@name"/>', fieldName:'<xsl:value-of select="../../labels/label[@lang=$culture]"/>', msg:'This field is required!'});
       if(!fieldMsgs['<xsl:value-of select="../../@prefix"/>_<xsl:value-of select="../../@name"/>'])  {
            fieldMsgs['<xsl:value-of select="../../@prefix"/>_<xsl:value-of select="../../@name"/>'] = '';
        }
        fieldMsgs['<xsl:value-of select="../../@prefix"/>_<xsl:value-of select="../../@name"/>'] =msgs[msgs.length-1].msg;
      }

  </xsl:template>
  
  <xsl:template match="entiyFormRequired" mode="BasicValid">
    <xsl:param name="type"/>

      if(fields[i].Children==null ||fields[i].Children.length==0) {
        msgs.push({prefix:'<xsl:value-of select="@prefix"/>',name:'<xsl:value-of select="@name"/>', fieldName:'<xsl:value-of select="../../labels/label[@lang=$culture]"/>', msg:'Entity form missing or unable to match to Biztax type, check client gfis data!'});
       if(!fieldMsgs['<xsl:value-of select="@prefix"/>_<xsl:value-of select="@name"/>'])  {
            fieldMsgs['<xsl:value-of select="@prefix"/>_<xsl:value-of select="@name"/>'] = '';
        }
        fieldMsgs['<xsl:value-of select="@prefix"/>_<xsl:value-of select="@name"/>'] =msgs[msgs.length-1].msg;
      }

  </xsl:template>

  <xsl:template match="*" mode="BasicValid"></xsl:template>


  <xsl:template match="assertions" mode="Assertions">
    self.parameters = {
    <xsl:for-each select="parameters/Parameter">
      <xsl:if test="position()>1">,</xsl:if>'<xsl:value-of select="@name"/>' : <xsl:apply-templates select="*"/>
    </xsl:for-each>
    };


    self.assertions = [];
    <xsl:for-each select="assertionSet[validation[not(@foreachPeriod) and not(@foreachDimension)]]">
      self.assertions.push({
        id:'<xsl:value-of select="@id"/>'
        ,fn:function(store) {
          <xsl:apply-templates select="validation" mode="Assertions"/>
        }
      });
    </xsl:for-each>
    
  </xsl:template>

  <xsl:template match="validation" mode="Assertions">
    <xsl:choose>
      <xsl:when test="@foreachPeriod='true'">
      </xsl:when>
      <xsl:when test="@foreachDimension='true'"></xsl:when>
      <xsl:otherwise><xsl:apply-templates select="." mode="CompileAssertion"/></xsl:otherwise>
    </xsl:choose>
    
  </xsl:template>

  <xsl:template match="validation" mode="CompileAssertion">
    var goahead = true, valid=true;
    <xsl:apply-templates select="variables/*[not(formula)]" mode="AssertionVariableDeclaration"/>
    <xsl:apply-templates select="variables/*[formula]" mode="AssertionVariableDeclaration"/>
    <xsl:if test="Precondition">
      goahead = <xsl:apply-templates select="Precondition/formula" mode="FORMULA"/>;
    </xsl:if>
    if(goahead) {
      valid = <xsl:apply-templates select="formula" mode="FORMULA"/>;
    }
    if(!valid) {
      return "invalid";
    }
    return null;
  </xsl:template>

  <xsl:template match="periodStartDate" mode="AssertionVariableDeclaration">var <xsl:value-of select="@name"/> = self.parameters['periodstart'];</xsl:template>
  <xsl:template match="periodEndDate" mode="AssertionVariableDeclaration">var <xsl:value-of select="@name"/> = self.parameters['periodend'];</xsl:template>
  
  <xsl:template match="variable" mode="AssertionVariableDeclaration">
    var <xsl:value-of select="@name"/> = <xsl:choose>
      <xsl:when test="count(conceptLink)=1"><xsl:apply-templates select="conceptLink" mode="GetConceptValue"/></xsl:when>
      <xsl:when test="count(conceptLink)>1">
        <xsl:choose>
          <xsl:when test="@fallBackValue='()'">
            <xsl:for-each select="conceptLink">
              <xsl:if test="position()>1"> &amp;&amp; </xsl:if><xsl:apply-templates select="." mode="GetConceptExists"/>
            </xsl:for-each>
          </xsl:when>
          <xsl:otherwise>
             <xsl:for-each select="conceptLink">
              <xsl:if test="position()>1"> + </xsl:if><xsl:apply-templates select="." mode="GetConceptValue"/>
            </xsl:for-each>
          </xsl:otherwise>
        </xsl:choose></xsl:when>
    <xsl:when test="formula"><xsl:apply-templates select="*" mode="FORMULA"/></xsl:when></xsl:choose>;
  </xsl:template>

  <xsl:template match="conceptLink" mode="GetConceptValue">store.getElementValue('<xsl:value-of select="@prefix"/>','<xsl:value-of select="@name"/>',<xsl:choose><xsl:when test="@period and not(@period='')">'<xsl:value-of select="@period"/>'</xsl:when><xsl:otherwise>null</xsl:otherwise></xsl:choose>,<xsl:choose><xsl:when test="../explicitDimension and ../explicitDimension/member">'<xsl:value-of select="../explicitDimension/member"/>'</xsl:when><xsl:otherwise>null</xsl:otherwise></xsl:choose>, <xsl:choose><xsl:when test="../explicitDimension">true</xsl:when><xsl:otherwise>false</xsl:otherwise></xsl:choose>,'<xsl:value-of select="@type"/>',<xsl:choose><xsl:when test="../@fallBackValue and not(../@fallBackValue='') and not(../@fallBackValue='()')"><xsl:value-of select="../@fallBackValue"/></xsl:when><xsl:otherwise>null</xsl:otherwise></xsl:choose>)</xsl:template>
  <xsl:template match="conceptLink" mode="GetConceptExists">store.getElementValue('<xsl:value-of select="@prefix"/>','<xsl:value-of select="@name"/>',<xsl:choose><xsl:when test="@period and not(@period='')">'<xsl:value-of select="@period"/>'</xsl:when><xsl:otherwise>null</xsl:otherwise></xsl:choose>,<xsl:choose><xsl:when test="../explicitDimension and ../explicitDimension/member">'<xsl:value-of select="../explicitDimension/member"/>'</xsl:when><xsl:otherwise>null</xsl:otherwise></xsl:choose>, <xsl:choose><xsl:when test="../explicitDimension">true</xsl:when><xsl:otherwise>false</xsl:otherwise></xsl:choose>,'<xsl:value-of select="@type"/>',null)!=null</xsl:template>

  <xsl:template match="Numeric"><xsl:value-of select="."/></xsl:template>
  <xsl:template match="String">'<xsl:value-of select="."/>'</xsl:template>
  <xsl:template match="Date">new Date(Date.parse('<xsl:value-of select="."/>'))</xsl:template>

  <xsl:template match="true" mode="FORMULA"> true </xsl:template>
  <xsl:template match="false" mode="FORMULA"> false </xsl:template>
  <xsl:template match="eq" mode="FORMULA"> == </xsl:template>
  <xsl:template match="ne" mode="FORMULA"> != </xsl:template>
  <xsl:template match="lt" mode="FORMULA"> &lt; </xsl:template>
  <xsl:template match="le" mode="FORMULA"> &lt;= </xsl:template>
  <xsl:template match="ge" mode="FORMULA"> &gt;= </xsl:template>
  <xsl:template match="gt" mode="FORMULA"> &gt; </xsl:template>
  <xsl:template match="multiply" mode="FORMULA"> * </xsl:template>
  <xsl:template match="div" mode="FORMULA"> / </xsl:template>
  <xsl:template match="exists" mode="FORMULA"> !Ext.isEmpty(<xsl:apply-templates select="*" mode="FORMULA"/>) </xsl:template>
  
  <xsl:template match="and" mode="FORMULA"> &amp;&amp; </xsl:template>
  <xsl:template match="or" mode="FORMULA"> || </xsl:template>
  <xsl:template match="group" mode="FORMULA"><xsl:if test="not(preceding-sibling::*[1][local-name()='true' or local-name()='false'])">(<xsl:apply-templates select="*" mode="FORMULA"/>)</xsl:if></xsl:template>

  <xsl:template match="number" mode="FORMULA"> <xsl:value-of select="."/> </xsl:template>
  <xsl:template match="variable" mode="FORMULA"> <xsl:if test="@negative='true'">-</xsl:if><xsl:value-of select="@name"/></xsl:template>

  <xsl:template match="useParameter" mode="FORMULA"> self.parameters['<xsl:value-of select="@name"/>'] </xsl:template>
  
  <xsl:template match="sum" mode="FORMULA">
    <xsl:call-template name="ApplySum">
      <xsl:with-param name="children" select="*"/>
      </xsl:call-template>
  </xsl:template>

  <xsl:template match="ToString" mode="FORMULA"> self.toString(<xsl:apply-templates select="*[1]" mode="FORMULA"/>) </xsl:template>

  <xsl:template match="ToDate" mode="FORMULA"> self.toDate(<xsl:apply-templates select="*[1]" mode="FORMULA"/>) </xsl:template>

  <xsl:template match="max" mode="FORMULA"><xsl:choose><xsl:when test="count(*)=0"></xsl:when><xsl:when test="count(*)>1"><xsl:call-template name="DoMax"><xsl:with-param name="source" select="."/></xsl:call-template></xsl:when><xsl:otherwise><xsl:call-template name="DoMax"><xsl:with-param name="source" select="./*[1]"/></xsl:call-template></xsl:otherwise></xsl:choose></xsl:template>
  <xsl:template match="min" mode="FORMULA"><xsl:choose><xsl:when test="count(*)=0"></xsl:when><xsl:when test="count(*)>1"><xsl:call-template name="DoMin"><xsl:with-param name="source" select="."/></xsl:call-template></xsl:when><xsl:otherwise><xsl:call-template name="DoMin"><xsl:with-param name="source" select="./*[1]"/></xsl:call-template></xsl:otherwise></xsl:choose></xsl:template>
  <xsl:template match="round" mode="FORMULA"> Math.round(<xsl:apply-templates select="*" mode="FORMULA"/>) </xsl:template>

  <xsl:template name="DoMax"><xsl:param name="source"/>Math.max(<xsl:apply-templates select="$source/*[1]" mode="FORMULA"/>,<xsl:apply-templates select="$source/*[2]" mode="FORMULA"/>) </xsl:template>
  
  <xsl:template name="DoMin"><xsl:param name="source"/>Math.min(<xsl:apply-templates select="$source/*[1]" mode="FORMULA"/>,<xsl:apply-templates select="$source/*[2]" mode="FORMULA"/>) </xsl:template>
  
  <xsl:template match="StringLength" mode="FORMULA">self.toString(<xsl:apply-templates select="*[1]" mode="FORMULA"/>).length</xsl:template>
  
<xsl:template match="count" mode="FORMULA">
  (<xsl:call-template name="ApplyCount">
    <xsl:with-param name="children" select="*"/>
  </xsl:call-template>)
</xsl:template>

  <xsl:template name="ApplySum">
    <xsl:param name="children"/>
    <xsl:for-each select="$children">
      <xsl:if test="position()>1"> + </xsl:if>
      <xsl:choose>
        <xsl:when test="local-name(.)='group'"> (<xsl:call-template name="ApplySum"><xsl:with-param name="children" select="*"/></xsl:call-template>) </xsl:when>
        <xsl:otherwise> <xsl:apply-templates select="." mode="FORMULA"/> </xsl:otherwise>
      </xsl:choose>
    </xsl:for-each>
  </xsl:template>
  
  <xsl:template name="ApplyCount">
    <xsl:param name="children"/>
    <xsl:for-each select="$children">
      <xsl:if test="position()>1"> + </xsl:if>
      <xsl:choose>
        <xsl:when test="local-name(.)='group'"><xsl:call-template name="ApplyCount"><xsl:with-param name="children" select="*"/></xsl:call-template></xsl:when>
        <xsl:otherwise> (<xsl:apply-templates select="." mode="FORMULA"/>!=null ? 1 : 0) </xsl:otherwise>
      </xsl:choose>
    </xsl:for-each>
  </xsl:template>
  
</xsl:stylesheet>
