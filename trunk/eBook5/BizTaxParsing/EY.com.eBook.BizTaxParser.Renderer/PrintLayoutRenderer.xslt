﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes=""
    xmlns:xso="dummy"
>
  
  <xsl:output method="xml" indent="no"/>
 
  <xsl:variable name="Levels">
    <levels>
      <level nr="0" spacingbefore="2" spacingafter="4" fontsize="11" style="bold underline" lineHeight="1.3"/>
      <level nr="1" spacingbefore="2" spacingafter="4"  fontsize="10" style="bold" lineHeight="1.2"/>
      <level nr="2" spacingbefore="2" spacingafter="4"  fontsize="8" style="bold" lineHeight="1.1"/>
      <level nr="3" spacingbefore="2" spacingafter="4" fontsize="8" style="underline" lineHeight="1.1"/>
      <level nr="4" spacingbefore="2" spacingafter="4" fontsize="8" style="italic" lineHeight="1.1"/>
      <level nr="5" spacingbefore="2" spacingafter="4" fontsize="8" style="italic" lineHeight="1.1"/>
      <level nr="6" spacingbefore="2" spacingafter="4" fontsize="8" style="italic" lineHeight="1.1"/>
      <level nr="7" spacingbefore="2" spacingafter="4" fontsize="8" style="italic" lineHeight="1.1"/>
    </levels>
  </xsl:variable>
  <xsl:variable name="xlevels" select="msxsl:node-set($Levels)"/>
  

  <xsl:variable name="ctxfontsize" select="8" />
  
  <xsl:variable name="WorksheetLabelFontsize" select="string('10')" />
  <xsl:variable name="WorksheetFontsize" select="string('10')" />
  <xsl:variable name="WorksheetNumbersize" select="string('8')" />
  <xsl:variable name="WorksheetSmallFontsize" select="string('8')" />
  <xsl:variable name="WorksheetTotalsFontsize" select="string('10')" />
  <xsl:variable name="WorksheetSpacing" select="1.1" />
  <xsl:variable name="assess">2013</xsl:variable>
  <xsl:variable name="culture">fr</xsl:variable> 
  <xsl:variable name="target">rcorp</xsl:variable>
  <xsl:variable name="smallcase" select="'abcdefghijklmnopqrstuvwxyz'" />
  <xsl:variable name="uppercase" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZ'" />
  <xsl:variable name="CODES">
    <xsl:choose>
      <xsl:when test="$culture='nl'">Codes</xsl:when>
      <xsl:when test="$culture='fr'">Codes</xsl:when>
      <xsl:otherwise>Codes</xsl:otherwise>
    </xsl:choose>
  </xsl:variable>
  <xsl:variable name="DURATION">
    <xsl:choose>
      <xsl:when test="$culture='nl'">Belastbaar tijdperk</xsl:when>
      <xsl:when test="$culture='fr'">Période imposable</xsl:when>
      <xsl:otherwise>Belastbaar tijdperk</xsl:otherwise>
    </xsl:choose>
  </xsl:variable>
  <xsl:variable name="ISTART">
    <xsl:choose>
      <xsl:when test="$culture='nl'">Bij het begin van het belastbare tijdperk</xsl:when>
      <xsl:when test="$culture='fr'">Au début de la période imposable</xsl:when>
      <xsl:otherwise>Bij het begin van het belastbare tijdperk</xsl:otherwise>
    </xsl:choose>
  </xsl:variable>
  <xsl:variable name="IEND">
    <xsl:choose>
      <xsl:when test="$culture='nl'">Op het einde van het belastbare tijdperk</xsl:when>
      <xsl:when test="$culture='fr'">A la fin de la période imposable</xsl:when>
      <xsl:otherwise>Op het einde van het belastbare tijdperk</xsl:otherwise>
    </xsl:choose>
  </xsl:variable>
  <xsl:variable name="_crlf"><xsl:text>
</xsl:text></xsl:variable>
  <xsl:variable name="crlf" select="string($_crlf)"/>
  <xsl:variable name="jscontainer">eBook.BizTax.AY<xsl:value-of select="$assess"/>_<xsl:value-of select="$target"/></xsl:variable>

  <xsl:variable name="empty">&amp;#160;</xsl:variable>
  <xsl:namespace-alias stylesheet-prefix="xso" result-prefix="xsl"/>
  
  <xsl:template match="root">
    <xso:stylesheet version="1.0" exclude-result-prefixes="msxsl Helper" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"  xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:Helper="urn:Helper">
      <xso:decimal-format name="NonNumber" NaN="0" zero-digit="0"/>
      <xso:variable name="WorksheetLabelFontsize" select="string('10')"/>
      <xso:variable name="WorksheetFontsize" select="string('10')"/>
      <xso:variable name="WorksheetNumbersize" select="string('8')"/>
      <xso:variable name="WorksheetSmallFontsize" select="string('8')"/>
      <xso:variable name="WorksheetTotalsFontsize" select="string('10')"/>
      <xso:variable name="WorksheetSpacing" select="1.1"/>
      <xso:decimal-format name="euro" decimal-separator="," grouping-separator="."/>
      <xso:variable name="NumberFormatting" select="string('#.##0,00')"/>
      <xso:variable name="NumberFormattingNoDigit" select="string('#.##0')"/>
      <xso:variable name="FONT" select="string('C:\WINDOWS\Fonts\EYInterstate-Light.ttf')"/>
      <xsl:variable name="cmembers">
        <xsl:for-each select="//Pane[@paneType='contextGrid']/Context/ContextDefinition//FieldListItem/ElementDef">
          <xsl:sort select="Id"/>
          <xsl:copy-of select="."/>
        </xsl:for-each>
      </xsl:variable>
      <xsl:variable name="xcmembers" select="msxsl:node-set($cmembers)"/>
      <xso:variable name="contextMembers">
        <lists>
          <xsl:for-each select="$xcmembers/*">
            <xsl:sort select="Id"/>
            <xsl:if test="not(Id=preceding-sibling::ElementDef/Id)">
            <item>
              <xsl:attribute name="id"><xsl:value-of select="translate(Id,'_',':')"/></xsl:attribute>
              <xsl:value-of select="Labels/ElementLabel[Role='label']/Descriptions/Translation[Language=$culture]/Description"/>
            </item>
            </xsl:if>
          </xsl:for-each>
        </lists>
      </xso:variable>
      <xso:variable name="xcontextMembers" select="msxsl:node-set($contextMembers)"/>
      <xso:variable name="XbrlListsPlane">
        <lists>
          <xsl:for-each select="ValueLists/List">
            <list id="{@id}">
              <xsl:for-each select="ListItem">
                <item id="{@xbrlName}"><xsl:value-of select="labels/label[@type='label']/translation[@lang=$culture]"/> (<xsl:value-of select="@Fixed"/>)</item>
              </xsl:for-each>
            </list>
          </xsl:for-each>
        </lists>
      </xso:variable>
      <xso:variable name="XbrlLists" select="msxsl:node-set($XbrlListsPlane)"/>
      
      
      <xso:param name="Culture"/>
      <xso:param name="Binaries"/>

      <xso:variable name="lang" select="substring-before($Culture,'-')"/>

      <xso:template name="contextLookup">
        <xso:param name="value"/>
        <xso:choose><xso:when test="not($value) or $value=''"><xsl:text disable-output-escaping="yes">&amp;#160;</xsl:text></xso:when><xso:otherwise><xso:variable name="listItem" select="$xcontextMembers//item[@id=$value]"/><xso:choose><xso:when test="not($listItem)"><xsl:text disable-output-escaping="yes">&amp;#160;</xsl:text></xso:when><xso:otherwise><xso:value-of select="$listItem"/></xso:otherwise></xso:choose></xso:otherwise></xso:choose>
      </xso:template>
      
      <xso:template match="BizTaxDataContract">
        <!-- MINIMUM ID, 275.1a 275.1B-->
        <Root>
          <xsl:for-each select="Presentation/Tab[@hidden='false']">
            <xsl:variable name="requiredElement"></xsl:variable>
            <xsl:variable name="mode" select="labels/label/translation[../@type='documentation' and @lang='en']"/>
            <xsl:variable name="orientation">
              <xsl:if test="position()>1">
                <xsl:choose>
                  <xsl:when test=".//Pane[@hidden='false' and @paneType='contextGrid' and count(.//Fields) > 6]">landscape</xsl:when>
                  <xsl:otherwise>portrait</xsl:otherwise>
                </xsl:choose>
              </xsl:if>
            </xsl:variable>
            <xso:apply-templates select="." mode="TAB_{$mode}">
              <xso:with-param name="orientation">
                <xsl:attribute name="select">'<xsl:value-of select="$orientation"/>'</xsl:attribute>
              </xso:with-param>
            </xso:apply-templates>
          </xsl:for-each>
        </Root>
      </xso:template>

      <xsl:for-each select="Presentation/Tab[@hidden='false']">
        <xsl:variable name="requiredElement"></xsl:variable>
        <xsl:variable name="mode" select="labels/label/translation[../@type='documentation' and @lang='en']"/>
      <xso:template match="BizTaxDataContract" mode="TAB_{$mode}">
        <xso:param name="orientation"/>
        <xsl:variable name="title" select="labels/label[@type='label']/translation[@lang=$culture]"/>
        <xsl:variable name="codedTitle"><xsl:choose><xsl:when test="string(number(substring-before($mode,1)))='NaN'"><xsl:value-of select="$title"/></xsl:when><xsl:otherwise><xsl:value-of select="$mode"/> - <xsl:value-of select="$title"/></xsl:otherwise></xsl:choose></xsl:variable>
        <chapter title="{$codedTitle}">
          <xso:if test="$orientation"><xso:attribute name="orientation"><xso:value-of select="$orientation"/></xso:attribute></xso:if>
        <xsl:call-template name="SetParagraph">
            <xsl:with-param name="title" select="$codedTitle"/>
            <xsl:with-param name="levelNr" select="0"/>
            <xsl:with-param name="indent" select="0"/>
          </xsl:call-template>
          <xsl:apply-templates select="Pane[@hidden='false']">
            <xsl:with-param name="parentTableCols" select="false"/>
            <xsl:with-param name="level" select="1"/>
            <xsl:with-param name="indent" select="0"/>
          </xsl:apply-templates>
          <xsl:if test=".//Field[contains(@itemType,'base64') or contains(@itemType,'Base64')]">
           
            <xsl:for-each select=".//Field[contains(@itemType,'base64') or contains(@itemType,'Base64')]">
              <xsl:variable name="itemPath">
                <xsl:apply-templates select="." mode="GetFieldDataSelect">
                  <xsl:with-param name="period" select="'D'"/>
                  <xsl:with-param name="contextPart"/>
                  <xsl:with-param name="contextExact"/>
                  <xsl:with-param name="ftype" select="'BinaryValue'"/>
                </xsl:apply-templates>
              </xsl:variable>
              
              <xso:if>
                <xsl:attribute name="test">count(<xsl:value-of select="$itemPath"/>/BinaryValue/*) &gt; 0</xsl:attribute>
                <xso:variable name="itemId">
                  <xsl:attribute name="select"><xsl:value-of select="$itemPath"/>/Id</xsl:attribute>
                </xso:variable>
                <xso:variable name="contents" select="$Binaries//IndexItemBaseDataContract[title=$itemId/text()]"/>
                
                <chapter>
                  <xso:attribute name="title"><xsl:apply-templates select="." mode="GetLabel"><xsl:with-param name="type" select="'label'"/></xsl:apply-templates></xso:attribute>
                  <xso:for-each select="$contents/items/*">
                    <pdf>
                      <xso:attribute name="path"><xso:value-of select="pdfLocation"/></xso:attribute>
                    </pdf>
                  </xso:for-each>
                </chapter>
              </xso:if>
            </xsl:for-each>

          </xsl:if>
          
        </chapter>
      </xso:template>
      </xsl:for-each>


      <xso:template name="GetListValue">
        <xso:param name="list"/>
        <xso:param name="value"/>
        <xso:variable name="xlist" select="$XbrlLists//list[@id=$list]"/>
        <xso:choose>
          <xso:when test="not($xlist)"><xso:value-of select="$value"/></xso:when>
          <xso:otherwise>
            <xso:choose>
              <xso:when test="not($value) or $value=''"><xsl:text disable-output-escaping="yes">&amp;#160;</xsl:text></xso:when>
              <xso:otherwise>
                <xso:variable name="listItem" select="$xlist/item[@id=$value]"/>
                <xso:choose>
                  <xso:when test="not($listItem)"><xsl:text disable-output-escaping="yes">&amp;#160;</xsl:text></xso:when>
                  <xso:otherwise><xso:value-of select="$listItem"/></xso:otherwise>
                </xso:choose>
              </xso:otherwise>
            </xso:choose>
          </xso:otherwise>
        </xso:choose>
        
      </xso:template>
      
      <xso:template name="BooleanValue">
        <xso:param name="value"/>
        <xso:choose>
          <xso:when test="$value='true'">X</xso:when>
          <xso:when test="$value='false'">/</xso:when>
          <xso:otherwise>
            <xsl:text disable-output-escaping="yes">&amp;#160;</xsl:text>
          </xso:otherwise>
        </xso:choose>
      </xso:template>

      <xso:template name="BinaryValue">
        <xso:param name="value"/>
        <xso:choose>
          <xso:when test="not($value) or $value='' or $value='0'">
            <xsl:text disable-output-escaping="yes">&amp;#160;</xsl:text>
          </xso:when>
          <xso:otherwise>
            <list ordered="false" numbered="false" lettered="false">
              <xso:variable name="id" select="$value/Id"/>
              <xso:variable name="contents" select="$Binaries//IndexItemBaseDataContract[title=$id/text()]"/>
            <xso:for-each select="$contents/items/*">
              <listitem>
                <xso:value-of select="title"/>
              </listitem>
            </xso:for-each>
            </list>
          </xso:otherwise>
        </xso:choose>
      </xso:template>
      
      <xso:template name="MonetaryValue">
        <xso:param name="value"/>
        <xso:choose>
          <xso:when test="not($value) or $value='' or $value='0'">
            <xsl:text disable-output-escaping="yes">&amp;#160;</xsl:text>
          </xso:when>
          <xso:otherwise>
            <xso:value-of select="format-number($value,$NumberFormatting,'euro')"/>€
          </xso:otherwise>
        </xso:choose>
      </xso:template>

      <xso:template name="PercentValue">
        <xso:param name="value"/>
        <xso:choose>
          <xso:when test="not($value) or $value='' or $value='0'"><xsl:text disable-output-escaping="yes">&amp;#160;</xsl:text></xso:when>
          <xso:otherwise><xso:value-of select="format-number(number($value) * 100,$NumberFormatting,'euro')"/>%</xso:otherwise>
        </xso:choose>
      </xso:template>

      <xso:template name="StandardValue">
        <xso:param name="value"/>
        <xso:choose>
          <xso:when test="not($value) or $value='' or $value='0'"><xsl:text disable-output-escaping="yes">&amp;#160;</xsl:text></xso:when>
          <xso:otherwise><xso:value-of select="$value"/></xso:otherwise>
        </xso:choose>
      </xso:template>
      
      
    </xso:stylesheet>
  </xsl:template>

  <xsl:template name="SetPhrase">
    <xsl:param name="title"/>
    <xsl:param name="levelNr"/>
    <xsl:param name="indent"/>
    <xsl:variable name="realNr"><xsl:choose><xsl:when test="$levelNr>3">3</xsl:when><xsl:otherwise><xsl:value-of select="$levelNr"/></xsl:otherwise></xsl:choose></xsl:variable>
    <xsl:variable name="level" select="$xlevels//level[@nr=$levelNr]"/>
    <paragraph lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}" fontstyle="bold underline">
      <xsl:attribute name="lineheight">
        <xsl:value-of select="$level/@lineHeight"/>
      </xsl:attribute>
      <xsl:attribute name="size">
        <xsl:value-of select="$level/@fontsize"/>
      </xsl:attribute>
      <xsl:attribute name="fontstyle">
        <xsl:value-of select="$level/@style"/>
      </xsl:attribute>
      <xsl:attribute name="indentationleft">
        <xsl:value-of select="number($indent) * 10"/>
      </xsl:attribute>
      <xsl:attribute name="spacingbefore">
        <xsl:value-of select="$level/@spacingbefore"/>
      </xsl:attribute>
      <xsl:attribute name="spacingafter">
        <xsl:value-of select="$level/@spacingafter"/>
      </xsl:attribute>
      <xsl:value-of select="$title"/>
    </paragraph>
  </xsl:template>

  <xsl:template name="SetParagraph">
    <xsl:param name="title"/>
    <xsl:param name="levelNr"/>
    <xsl:param name="indent"/>
    
    <xsl:variable name="realNr">
      <xsl:choose>
        <xsl:when test="$levelNr>3">3</xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="$levelNr"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    
    <xsl:variable name="level" select="$xlevels//level[@nr=$levelNr]"/>
    <paragraph lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}" fontstyle="bold underline">
      <xsl:attribute name="lineheight">
        <xsl:value-of select="$level/@lineHeight"/>
      </xsl:attribute>
      <xsl:attribute name="size">
        <xsl:value-of select="$level/@fontsize"/>
      </xsl:attribute>
      <xsl:attribute name="fontstyle">
        <xsl:value-of select="$level/@style"/>
      </xsl:attribute>
      <xsl:call-template name="indentloop">
        <xsl:with-param name="cycles" select="number($levelNr) * 3"/>
      </xsl:call-template>
      <xsl:attribute name="indentationleft">
        <xsl:value-of select="number($indent) * 10"/>
      </xsl:attribute>
      <xsl:attribute name="spacingbefore">
        <xsl:value-of select="$level/@spacingbefore"/>
      </xsl:attribute>
      <xsl:attribute name="spacingafter">
        <xsl:value-of select="$level/@spacingafter"/>
      </xsl:attribute>
      <xsl:value-of select="$title"/>
    </paragraph>
  </xsl:template>

  <xsl:template name="indentloop"><xsl:param name="cycles" />
    <!--xsl:choose><xsl:when test="number($cycles) &gt; 0"><xsl:text disable-output-escaping="yes">&amp;#160;</xsl:text><xsl:call-template name="indentloop"><xsl:with-param name="cycles" select="number($cycles)-1" /> </xsl:call-template></xsl:when></xsl:choose-->
  </xsl:template>
  

  <xsl:template match="Pane">
    <xsl:param name="parentTableCols"/>
    <xsl:param name="level"/>
    <xsl:param name="indent"/>
    <xsl:if test="not($parentTableCols) or $parentTableCols=0">
      <newline/>
    </xsl:if>
    <xsl:choose>
      <xsl:when test="@paneType='contextGrid'">
        <!--xsl:apply-templates select="." mode="ContextGrid"/-->
        <xsl:apply-templates select="." mode="ContextGrid">
          <xsl:with-param name="parentTableCols" select="$parentTableCols"/>
          <xsl:with-param name="level" select="$level"/>
          <xsl:with-param name="indent" select="$indent"/>
        </xsl:apply-templates>
      </xsl:when>
      <xsl:when test="@paneType='pivot'">
        <!--xsl:apply-templates select="." mode="Pivot"/-->
        <xsl:apply-templates select="." mode="Pivot">
          <xsl:with-param name="parentTableCols" select="$parentTableCols"/>
          <xsl:with-param name="level" select="$level"/>
          <xsl:with-param name="indent" select="$indent"/>
        </xsl:apply-templates>
      </xsl:when>
      <xsl:when test="@paneType='ListOrOther'">
        <xsl:apply-templates select="." mode="ListOrOther">
          <xsl:with-param name="parentTableCols" select="$parentTableCols"/>
          <xsl:with-param name="level" select="$level"/>
          <xsl:with-param name="indent" select="$indent"/>
        </xsl:apply-templates>
      </xsl:when>
      <xsl:otherwise>
        <xsl:apply-templates select="." mode="Normal">
          <xsl:with-param name="parentTableCols" select="$parentTableCols"/>
          <xsl:with-param name="level" select="$level"/>
          <xsl:with-param name="indent" select="$indent"/>
        </xsl:apply-templates>
      </xsl:otherwise>
     
    </xsl:choose>
  </xsl:template>

  <xsl:template match="Pane" mode="ListOrOther">
    <xsl:param name="parentTableCols"/>
    <xsl:param name="level"/>
    <xsl:param name="indent"/>


    <xsl:variable name="fieldIndent" select="number($indent)+1"/>
    
    
    <xsl:variable name="myCols">
      <xsl:choose>
        <xsl:when test="not($parentTableCols) or $parentTableCols=0">
          <xsl:choose>
            <xsl:when test="./Field/Context/period[@start='true' or @end='true']">5</xsl:when>
            <xsl:when test="ancestor::Tab[@xbrlName='CompanyInformationForm']">2</xsl:when>
            <xsl:otherwise>4</xsl:otherwise>
          </xsl:choose>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="$parentTableCols"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    
    <xsl:choose>
      <xsl:when test="not($parentTableCols) or $parentTableCols=0">
        <xsl:call-template name="StartTable">
          <xsl:with-param name="cols" select="$myCols"/>
        </xsl:call-template>
        <xsl:call-template name="TitleRow">
          <xsl:with-param name="cols" select="$myCols"/>
          <xsl:with-param name="level" select="$level"/>
          <xsl:with-param name="indent" select="0"/>
          <xsl:with-param name="title">
            <xsl:apply-templates select="." mode="GetLabel">
              <xsl:with-param name="type" select="'label'"/>
            </xsl:apply-templates>
          </xsl:with-param>
          <xsl:with-param name="border" select="false"/>
        </xsl:call-template>
        <xsl:call-template name="SetHeading">
          <xsl:with-param name="cols" select="$myCols"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:call-template name="TitleRow">
          <xsl:with-param name="cols" select="$myCols"/>
          <xsl:with-param name="level" select="$level"/>
          <xsl:with-param name="indent" select="$indent"/>
          <xsl:with-param name="title">
            <xsl:apply-templates select="." mode="GetLabel">
              <xsl:with-param name="type" select="'label'"/>
            </xsl:apply-templates>
          </xsl:with-param>
          <xsl:with-param name="border" select="boolean('true')"/>
        </xsl:call-template>
      </xsl:otherwise>
    </xsl:choose>
   
    <row>
      <cell left="true" bottom="true" verticalalign="middle">
        <paragraph lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
          <xsl:attribute name="indentationleft">
            <xsl:value-of select="number($fieldIndent) * 10"/>
          </xsl:attribute>
          <xsl:apply-templates select="ValueList" mode="GetLabel">
            <xsl:with-param name="type" select="'label'"/>
          </xsl:apply-templates>
        </paragraph>
      </cell>
      <xsl:if test="not(number($myCols)=2)">
        <cell left="true" bottom="true" verticalalign="middle">
          <paragraph lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
            <xsl:apply-templates select="ValueList" mode="GetOldCode"/>
          </paragraph>
        </cell>
        <cell left="true" bottom="true" verticalalign="middle">
          <paragraph lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
            <xsl:apply-templates select="ValueList" mode="GetCode"/>
          </paragraph>
        </cell>
      </xsl:if>
      <xsl:choose>
        <xsl:when test="number($myCols)=4 or number($myCols)=2">
          <cell left="true" bottom="true" right="true" verticalalign="middle">
            <paragraph lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
              <xso:call-template name="GetListValue">
                <xso:with-param name="list">
                  <xsl:attribute name="select">'<xsl:value-of select="ValueList/@xbrlId"/>'</xsl:attribute>
                </xso:with-param>
                <xso:with-param name="value"><xsl:attribute name="select"><xsl:apply-templates select="ValueList" mode="GetFieldData"><xsl:with-param name="period" select="'D'"/></xsl:apply-templates></xsl:attribute></xso:with-param>
              </xso:call-template>
             </paragraph>
          </cell>
        </xsl:when>
        <xsl:otherwise>
          <cell left="true" bottom="true" verticalalign="middle">
            <paragraph lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}"> <xso:call-template name="GetListValue">
                <xso:with-param name="list">
                  <xsl:attribute name="select">'<xsl:value-of select="ValueList/@xbrlId"/>'</xsl:attribute>
                </xso:with-param>
                <xso:with-param name="value"><xsl:attribute name="select"><xsl:apply-templates select="ValueList" mode="GetFieldData"><xsl:with-param name="period" select="'I-Start'"/></xsl:apply-templates></xsl:attribute></xso:with-param>
              </xso:call-template></paragraph>
          </cell>
          <cell left="true" bottom="true" right="true" verticalalign="middle">
            <paragraph lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}"> <xso:call-template name="GetListValue">
                <xso:with-param name="list">
                  <xsl:attribute name="select">'<xsl:value-of select="ValueList/@xbrlId"/>'</xsl:attribute>
                </xso:with-param>
                <xso:with-param name="value"><xsl:attribute name="select"><xsl:apply-templates select="ValueList" mode="GetFieldData"><xsl:with-param name="period" select="'I-End'"/></xsl:apply-templates></xsl:attribute></xso:with-param>
              </xso:call-template></paragraph>
          </cell>
        </xsl:otherwise>
      </xsl:choose>
    </row>
    <xsl:for-each select=".//Field">
      <row>
        <cell left="true" bottom="true" verticalalign="middle">
          <paragraph lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
            <xsl:attribute name="indentationleft">
              <xsl:value-of select="number($fieldIndent) * 10"/>
            </xsl:attribute>
            <xsl:apply-templates select="." mode="GetLabel">
              <xsl:with-param name="type" select="'label'"/>
            </xsl:apply-templates>
          </paragraph>
        </cell>
        <xsl:if test="not(number($myCols)=2)">
          <cell left="true" bottom="true" verticalalign="middle">
            <paragraph lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
              <xsl:apply-templates select="." mode="GetOldCode"/>
            </paragraph>
          </cell>
          <cell left="true" bottom="true" verticalalign="middle">
            <paragraph lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
              <xsl:apply-templates select="." mode="GetCode"/>
            </paragraph>
          </cell>
        </xsl:if>
        <xsl:choose>
          <xsl:when test="number($myCols)=4 or number($myCols)=2">
            <cell left="true" bottom="true" right="true" verticalalign="middle">
              <paragraph lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}"><xsl:apply-templates select="." mode="GetFieldData"><xsl:with-param name="period" select="'D'"/></xsl:apply-templates></paragraph>
            </cell>
          </xsl:when>
          <xsl:otherwise>
            <cell left="true" bottom="true" verticalalign="middle">
              <paragraph lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}"><xsl:apply-templates select="." mode="GetFieldData"><xsl:with-param name="period" select="'I-Start'"/></xsl:apply-templates></paragraph>
            </cell>
            <cell left="true" bottom="true" right="true" verticalalign="middle">
              <paragraph lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}"><xsl:apply-templates select="." mode="GetFieldData"><xsl:with-param name="period" select="'I-End'"/></xsl:apply-templates></paragraph>
            </cell>
          </xsl:otherwise>
        </xsl:choose>
      </row>
    </xsl:for-each>

    <xsl:if test="not($parentTableCols) or $parentTableCols=0">
      <xsl:call-template name="EndTable"/>
    </xsl:if>
  </xsl:template>

 

  <xsl:template name="EndTable">
    <xsl:text disable-output-escaping="yes">&lt;/table&gt;</xsl:text>
  </xsl:template>
  <xsl:template name="StartTableInfo">
    <xsl:text disable-output-escaping="yes">&lt;table tablefitspage="true" convert2pdfp="true" cellsfitpage="true" borderwidth="0.02" columns="2" width="100%" align="right" widths="50;50" cellspacing="0" cellpadding="3"&gt;</xsl:text>
  </xsl:template>
  <xsl:template name="StartTableDuration">
    <xsl:text disable-output-escaping="yes">&lt;table tablefitspage="true" convert2pdfp="true" cellsfitpage="true" borderwidth="0.02" columns="4" width="100%" align="right" widths="64;10;10;20" cellspacing="0" cellpadding="3"&gt;</xsl:text>
  </xsl:template>
  <xsl:template name="StartTableInstant">
    <xsl:text disable-output-escaping="yes">&lt;table tablefitspage="true" convert2pdfp="true" cellsfitpage="true" borderwidth="0.02" columns="5" width="100%" align="right" widths="44;10;10;20;20" cellspacing="0" cellpadding="3"&gt;</xsl:text>
  </xsl:template>
  <xsl:template name="StartTable">
    <xsl:param name="cols"/>
    <xsl:choose><xsl:when test="number($cols)=2"><xsl:call-template name="StartTableInfo"/></xsl:when><xsl:when test="number($cols)=4"><xsl:call-template name="StartTableDuration"/></xsl:when><xsl:when test="number($cols)=5"><xsl:call-template name="StartTableInstant"/></xsl:when></xsl:choose>
  </xsl:template>

  <xsl:template name="TitleValueRow">
    <xsl:param name="cols"/>
    <xsl:param name="level"/>
    <xsl:param name="indent"/>
    <xsl:param name="title"/>
    <xsl:param name="border"/>
    <xsl:param name="item"/>

    <!--xsl:variable name="mycols">
      <xsl:choose>
        <xsl:when test="Context/period[@start='true' or @end='true']">5</xsl:when>
        <xsl:otherwise>4</xsl:otherwise>
      </xsl:choose>
    </xsl:variable-->
 
        <row>
      <cell left="true" bottom="true" verticalalign="middle"><xsl:if test="$border"><xsl:attribute name="left">true</xsl:attribute><xsl:attribute name="right">true</xsl:attribute><xsl:attribute name="bottom">true</xsl:attribute></xsl:if><xsl:call-template name="SetPhrase"><xsl:with-param name="title" select="$title"/><xsl:with-param name="levelNr" select="$level"/><xsl:with-param name="indent" select="$indent"/></xsl:call-template></cell>
          <xsl:if test="not(number($cols)=2)">
      <cell left="true" bottom="true" verticalalign="middle"><paragraph lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}"><xsl:apply-templates select="$item" mode="GetOldCode"/></paragraph></cell>
      <cell left="true" bottom="true" verticalalign="middle"><paragraph lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}"><xsl:apply-templates select="$item" mode="GetCode"/></paragraph></cell>
          </xsl:if>
    <xsl:choose>
      <xsl:when test="number($cols)=4 or number($cols)=2">
        <cell left="true" bottom="true" right="true" verticalalign="middle" horizontalalign="right">
          <paragraph lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}"><xsl:apply-templates select="$item" mode="GetFieldData"><xsl:with-param name="period" select="'D'"/></xsl:apply-templates></paragraph>
          <!--xsl:value-of select="@xbrlId"/--></cell>
      </xsl:when>
      <xsl:when test="number($cols)=5">
        <cell left="true" bottom="true" verticalalign="middle" horizontalalign="right"><paragraph lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}"><xsl:choose><xsl:when test="$item/Context/period[@start='true']"><xsl:apply-templates select="$item" mode="GetFieldData"><xsl:with-param name="period" select="'I-Start'"/></xsl:apply-templates></xsl:when><xsl:otherwise><xsl:text disable-output-escaping="yes">&amp;#160;</xsl:text></xsl:otherwise></xsl:choose><!--xsl:value-of select="@xbrlId"/--></paragraph></cell>
        <cell left="true" bottom="true" right="true" verticalalign="middle" horizontalalign="right"><paragraph lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}"><xsl:choose><xsl:when test="$item/Context/period[@end='true']"><xsl:apply-templates select="$item" mode="GetFieldData"><xsl:with-param name="period" select="'I-End'"/></xsl:apply-templates></xsl:when><xsl:when test="Context/period[(@type='duration' and not(@start='true'))]"><xsl:apply-templates select="$item" mode="GetFieldData"><xsl:with-param name="period" select="'D'"/></xsl:apply-templates></xsl:when><xsl:otherwise><xsl:text disable-output-escaping="yes">&amp;#160;</xsl:text></xsl:otherwise></xsl:choose><!--xsl:value-of select="@xbrlId"/--></paragraph></cell>
      </xsl:when>
    </xsl:choose>
    </row>
  </xsl:template>
  
  <xsl:template name="TitleRow">
    <xsl:param name="cols"/>
    <xsl:param name="level"/>
    <xsl:param name="indent"/>
    <xsl:param name="title"/>
    <xsl:param name="border"/>
    
    <row>
      <cell colspan="{$cols}">
        <xsl:if test="$border">
          <xsl:attribute name="left">true</xsl:attribute>
          <xsl:attribute name="right">true</xsl:attribute>
          <xsl:attribute name="bottom">true</xsl:attribute>
        </xsl:if>
        <xsl:call-template name="SetPhrase">
          <xsl:with-param name="title" select="$title"/>
          <xsl:with-param name="levelNr" select="$level"/>
          <xsl:with-param name="indent" select="$indent"/>
        </xsl:call-template>
      </cell>
    </row>
  </xsl:template>

  <xsl:template name="SetHeading">
    <xsl:param name="cols"/>
    <xsl:choose>
      <xsl:when test="number($cols)=2">
        <xsl:call-template name="GETINFOHEADERS"/>
      </xsl:when>
      <xsl:when test="number($cols)=4">
        <xsl:call-template name="GETDURATIONHEADERS"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:call-template name="GETINSTANTHEADERS"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  
  
  <xsl:template match="Pane" mode="Normal">
    <xsl:param name="parentTableCols"/>
    <xsl:param name="level"/>
    <xsl:param name="indent"/>

    <xsl:variable name="preceding"><xsl:choose><xsl:when test="preceding-sibling::Pane"><xsl:choose><xsl:when test="preceding-sibling::Pane//Field/Context/period[@start='true' or @end='true']">INSTANT</xsl:when><xsl:otherwise>DURATION</xsl:otherwise></xsl:choose></xsl:when><xsl:otherwise>NULL</xsl:otherwise></xsl:choose></xsl:variable>

    <xsl:variable name="myCols">
      <xsl:choose>
        <xsl:when test="./Field/Context/period[@start='true' or @end='true']">5</xsl:when>
        <xsl:when test="ancestor::Tab[@xbrlName='CompanyInformationForm']">2</xsl:when>
        <xsl:when test="count(Field[not(contains(@itemType,'ase64')) and not(@isAbstract='true')])=0 and Field[contains(@itemType,'ase64')]">2</xsl:when>
        <xsl:when test="not(Field[not(@isAbstract='true')])">0</xsl:when>
        <xsl:otherwise>4</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:choose>
      <xsl:when test="$parentTableCols and not($parentTableCols=0)">
        <!--xsl:variable name="myCols"><xsl:choose><xsl:when test=".//Field/Context/period[@start='true' or @end='true']">5</xsl:when><xsl:otherwise>4</xsl:otherwise></xsl:choose></xsl:variable-->
        <xsl:choose>
          <xsl:when test="$parentTableCols=$myCols">
            <xsl:choose>
              <xsl:when test="@isAbstract='false' and @itemType">
                <xsl:call-template name="TitleValueRow">
                  <xsl:with-param name="cols" select="$myCols"/>
                  <xsl:with-param name="level" select="$level"/>
                  <xsl:with-param name="indent" select="$indent"/>
                  <xsl:with-param name="title">
                    <xsl:apply-templates select="." mode="GetLabel">
                      <xsl:with-param name="type" select="'label'"/>
                    </xsl:apply-templates>
                  </xsl:with-param>
                  <xsl:with-param name="border" select="boolean('true')"/>
                  <xsl:with-param name="item" select="."/>
                </xsl:call-template>
              </xsl:when>
              <xsl:otherwise>
                <xsl:call-template name="TitleRow">
                  <xsl:with-param name="cols" select="$myCols"/>
                  <xsl:with-param name="level" select="$level"/>
                  <xsl:with-param name="indent" select="$indent"/>
                  <xsl:with-param name="title">
                    <xsl:apply-templates select="." mode="GetLabel">
                      <xsl:with-param name="type" select="'label'"/>
                    </xsl:apply-templates>
                  </xsl:with-param>
                  <xsl:with-param name="border" select="boolean('true')"/>
                </xsl:call-template>
              </xsl:otherwise>
            </xsl:choose>
            <!--xsl:call-template name="StartTable">
              <xsl:with-param name="cols" select="$myCols"/>
            </xsl:call-template-->
            <xsl:apply-templates select="Pane[@hidden='false']|Field[@hidden='false']">
              <xsl:with-param name="parentTableCols" select="$myCols"/>
              <xsl:with-param name="level" select="$level+1"/>
              <xsl:with-param name="indent" select="$indent+1"/>
            </xsl:apply-templates>
            <!--xsl:call-template name="EndTable"/-->
          </xsl:when>
          <xsl:otherwise>
            <xsl:choose>
              <xsl:when test="$parentTableCols=5 and $myCols=4">
                <xsl:call-template name="TitleRow">
                  <xsl:with-param name="cols" select="$parentTableCols"/>
                  <xsl:with-param name="level" select="$level"/>
                  <xsl:with-param name="indent" select="0"/>
                  <xsl:with-param name="title">
                    <xsl:apply-templates select="." mode="GetLabel">
                      <xsl:with-param name="type" select="'label'"/>
                    </xsl:apply-templates>
                  </xsl:with-param>
                  <xsl:with-param name="border" select="boolean('true')"/>
                </xsl:call-template>
                <xsl:apply-templates select="Pane[@hidden='false']|Field[@hidden='false']">
                  <xsl:with-param name="parentTableCols" select="$parentTableCols"/>
                  <xsl:with-param name="level" select="$level+1"/>
                  <xsl:with-param name="indent" select="$indent+1"/>
                </xsl:apply-templates>
              </xsl:when>
              <xsl:otherwise>
                <xsl:call-template name="EndTable"/>
                <!--xsl:call-template name="SetPhrase">
              <xsl:with-param name="title">
                <xsl:apply-templates select="." mode="GetLabel">
                  <xsl:with-param name="type" select="'label'"/>
                </xsl:apply-templates>
              </xsl:with-param>
              <xsl:with-param name="levelNr" select="$indent"/>
            </xsl:call-template-->
                <xsl:call-template name="StartTable">
                  <xsl:with-param name="cols" select="$myCols"/>
                </xsl:call-template>
                <xsl:call-template name="TitleRow">
                  <xsl:with-param name="cols" select="$myCols"/>
                  <xsl:with-param name="level" select="$level"/>
                  <xsl:with-param name="indent" select="0"/>
                  <xsl:with-param name="title">
                    <xsl:apply-templates select="." mode="GetLabel">
                      <xsl:with-param name="type" select="'label'"/>
                    </xsl:apply-templates>
                  </xsl:with-param>
                  <xsl:with-param name="border" select="false"/>
                </xsl:call-template>
                <xsl:call-template name="SetHeading">
                  <xsl:with-param name="cols" select="$myCols"/>
                </xsl:call-template>
                <xsl:apply-templates select="Pane[@hidden='false']|Field[@hidden='false']">
                  <xsl:with-param name="parentTableCols" select="$myCols"/>
                  <xsl:with-param name="level" select="$level+1"/>
                  <xsl:with-param name="indent" select="0"/>
                </xsl:apply-templates>
                <xsl:call-template name="EndTable"/>
                <xsl:choose>
                  <xsl:when test="local-name(following-sibling::*[1])='Field'">
                    <xsl:call-template name="StartTable">
                      <xsl:with-param name="cols" select="$parentTableCols"/>
                    </xsl:call-template>
                  </xsl:when>

                </xsl:choose>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:when>
      <xsl:otherwise>
        
        <xsl:choose>
          <xsl:when test="number($myCols)=0">
            <xsl:if test="Pane">
            <xsl:call-template name="SetParagraph">
              <xsl:with-param name="title">
                <xsl:apply-templates select="." mode="GetLabel">
                  <xsl:with-param name="type" select="'label'"/>
                </xsl:apply-templates>
              </xsl:with-param>
              <xsl:with-param name="levelNr" select="$level"/>
              <xsl:with-param name="indent" select="0"/>
            </xsl:call-template>
            <xsl:apply-templates select="Pane[@hidden='false']">
              <xsl:with-param name="parentTableCols" select="$myCols"/>
              <xsl:with-param name="level" select="$level+1"/>
              <xsl:with-param name="indent" select="$indent+1"/>
            </xsl:apply-templates>
            </xsl:if>
          </xsl:when>
          <xsl:otherwise>
            <xsl:call-template name="StartTable">
              <xsl:with-param name="cols" select="$myCols"/>
            </xsl:call-template>
            <xsl:call-template name="TitleRow">
              <xsl:with-param name="cols" select="$myCols"/>
              <xsl:with-param name="level" select="$level"/>
              <xsl:with-param name="indent" select="0"/>
              <xsl:with-param name="title">
                <xsl:apply-templates select="." mode="GetLabel">
                  <xsl:with-param name="type" select="'label'"/>
                </xsl:apply-templates>
              </xsl:with-param>
              <xsl:with-param name="border" select="false"/>
            </xsl:call-template>
            <xsl:call-template name="SetHeading">
              <xsl:with-param name="cols" select="$myCols"/>
            </xsl:call-template>
            <xsl:apply-templates select="Pane[@hidden='false']|Field[@hidden='false']">
              <xsl:with-param name="parentTableCols" select="$myCols"/>
              <xsl:with-param name="level" select="$level+1"/>
              <xsl:with-param name="indent" select="0"/>
            </xsl:apply-templates>
            <xsl:call-template name="EndTable"/>
          </xsl:otherwise>
        </xsl:choose>
       
      </xsl:otherwise>
    </xsl:choose>
    
  
  </xsl:template>


  <xsl:template name="GETINFOHEADERS">
    <row>
      <cell left="true" bottom="true" top="true" grayfill="0.9" verticalalign="Top">
        <paragraph lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
          <xsl:text disable-output-escaping="yes">&amp;</xsl:text>#160;</paragraph>
      </cell>
      <cell left="true" right="true" bottom="true" top="true" grayfill="0.9" horizontalalign="center" verticalalign="Top">
        <paragraph lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}"><xsl:value-of select="$DURATION"/></paragraph>
      </cell>

    </row>
  </xsl:template>
  
  <xsl:template name="GETDURATIONHEADERS">
    <row>
      <cell left="true" bottom="true" top="true" grayfill="0.9" verticalalign="Top">
        <paragraph lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>#160;</paragraph>
      </cell>
      <cell colspan="2" left="true" bottom="true" top="true" grayfill="0.9" horizontalalign="center" verticalalign="Top">
        <paragraph lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}"><xsl:value-of select="$CODES"/></paragraph>
      </cell>
      <cell left="true" right="true" bottom="true" top="true" grayfill="0.9" horizontalalign="center" verticalalign="Top">
        <paragraph lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}"><xsl:value-of select="$DURATION"/></paragraph>
      </cell>

    </row>
  </xsl:template>
  
    <xsl:template name="GETINSTANTHEADERS">
    <row>
      <cell left="true" bottom="true" top="true" grayfill="0.9" verticalalign="Top">
        <paragraph lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>#160;</paragraph>
      </cell>
      <cell colspan="2" left="true" bottom="true" top="true" grayfill="0.9" horizontalalign="center" verticalalign="Top">
        <paragraph lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}"><xsl:value-of select="$CODES"/></paragraph>
      </cell>
      <cell left="true" bottom="true" top="true" grayfill="0.9" verticalalign="Top" horizontalalign="center">
        <paragraph lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}"><xsl:value-of select="$ISTART"/></paragraph>
      </cell>
      <cell left="true" right="true" bottom="true" top="true" grayfill="0.9" verticalalign="Top" horizontalalign="center">
        <paragraph lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}"><xsl:value-of select="$IEND"/></paragraph>
      </cell>

    </row>
  </xsl:template>
  
  <xsl:template match="Pane" mode="GETTITLE">
    <xsl:param name="parentTableCols"/>
    <xsl:param name="cols"/>
    <xsl:choose>
      <xsl:when test="string(number($parentTableCols))='NaN'">
        <paragraph lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" spacingafter="3"><xsl:apply-templates select="." mode="GetLabel"><xsl:with-param name="type" select="'label'"/></xsl:apply-templates></paragraph>
      </xsl:when>
      <xsl:otherwise>
        <row>
          <cell colspan="{$parentTableCols}"><paragraph lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}"><xsl:apply-templates select="." mode="GetLabel"><xsl:with-param name="type" select="'label'"/></xsl:apply-templates></paragraph></cell>
        </row>
    </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <!--xsl:template match="Pane" mode="GETBODY">
    <xsl:param name="parentTableCols"/>
    <xsl:param name="cols"/>
    <xsl:choose>
      <xsl:when test="string(number($parentTableCols))='NaN'">
        <xsl:apply-templates select="Pane"/>
      </xsl:when>
      <xsl:when test="string(number($parentTableCols))='NaN'">
        
          <row>
            <cell left="true" bottom="true" top="true" grayfill="0.9" verticalalign="Top">
              <paragraph lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>#160;</paragraph>
            </cell>
            <cell left="true" bottom="true" top="true" grayfill="0.9" verticalalign="Top">
              <paragraph lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">oldcode</paragraph>
            </cell>
            <cell left="true" bottom="true" top="true" grayfill="0.9" verticalalign="Top">
              <paragraph lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">code</paragraph>
            </cell>
            <xsl:choose>
              <xsl:when test="number($cols)=4">
                <cell left="true" right="true" bottom="true" top="true" grayfill="0.9" verticalalign="Top">
                  <paragraph lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}"><xsl:value-of select="$DURATION"/></paragraph>
                </cell>
              </xsl:when>
              <xsl:otherwise>
                <cell left="true" bottom="true" top="true" grayfill="0.9" verticalalign="Top">
                  <paragraph lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}"><xsl:value-of select="$ISTART"/></paragraph>
                </cell>
                <cell left="true" right="true" bottom="true" top="true" grayfill="0.9" verticalalign="Top">
                  <paragraph lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">I-END</paragraph>
                </cell>
              </xsl:otherwise>
            </xsl:choose>
          </row>
         
        </table>
      </xsl:when>
      <xsl:otherwise>
        <xsl:apply-templates select="Pane[@hidden='false']|Field[@hidden='false']">
          <xsl:with-param name="parentTableCols" select="$cols"/>
        </xsl:apply-templates>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template-->
  
  <xsl:template match="Field">
    <xsl:param name="parentTableCols"/>
    <xsl:param name="level"/>
    <xsl:param name="indent"/>

    <!--xsl:variable name="mycols">
      <xsl:choose>
        <xsl:when test="Context/period[@start='true' or @end='true']">5</xsl:when>
        <xsl:otherwise>4</xsl:otherwise>
      </xsl:choose>
    </xsl:variable-->
    
    <xsl:choose>
      <xsl:when test="@isAbstract='true'">
        <row>
          <cell colspan="{$parentTableCols}" left="true" bottom="true" right="true" verticalalign="middle">
            <paragraph lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" style="italic">
              <xsl:attribute name="indentationleft">
                <xsl:value-of select="number($indent) * 10"/>
              </xsl:attribute>
              <xsl:apply-templates select="." mode="GetLabel">
                <xsl:with-param name="type" select="'label'"/>
              </xsl:apply-templates>
            </paragraph>
          </cell>
        </row>
      </xsl:when>
      <xsl:otherwise>
        <row>
      <cell left="true" bottom="true" verticalalign="middle"><paragraph lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}"><xsl:attribute name="indentationleft"><xsl:value-of select="number($indent) * 10"/></xsl:attribute><xsl:apply-templates select="." mode="GetLabel"><xsl:with-param name="type" select="'label'"/></xsl:apply-templates></paragraph></cell>
          <xsl:if test="not(number($parentTableCols)=2)">
      <cell left="true" bottom="true" verticalalign="middle"><paragraph lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}"><xsl:apply-templates select="." mode="GetOldCode"/></paragraph></cell>
      <cell left="true" bottom="true" verticalalign="middle"><paragraph lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}"><xsl:apply-templates select="." mode="GetCode"/></paragraph></cell>
          </xsl:if>
    <xsl:choose>
      <xsl:when test="number($parentTableCols)=4 or number($parentTableCols)=2">
        <cell left="true" bottom="true" right="true" verticalalign="middle" horizontalalign="right">
          <xsl:choose>
            <xsl:when test="contains(@itemType,'base64') or contains(@itemType,'Base64')"><xsl:apply-templates select="." mode="GetFieldData"><xsl:with-param name="period" select="'D'"/></xsl:apply-templates></xsl:when>
            <xsl:otherwise><paragraph lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}"><xsl:apply-templates select="." mode="GetFieldData"><xsl:with-param name="period" select="'D'"/></xsl:apply-templates></paragraph></xsl:otherwise>
          </xsl:choose>
          
          <!--xsl:value-of select="@xbrlId"/--></cell>
      </xsl:when>
      <xsl:when test="number($parentTableCols)=5">
        <cell left="true" bottom="true" verticalalign="middle" horizontalalign="right"><paragraph lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}"><xsl:choose><xsl:when test="Context/period[@start='true']"><xsl:apply-templates select="." mode="GetFieldData"><xsl:with-param name="period" select="'I-Start'"/></xsl:apply-templates></xsl:when><xsl:otherwise><xsl:text disable-output-escaping="yes">&amp;#160;</xsl:text></xsl:otherwise></xsl:choose><!--xsl:value-of select="@xbrlId"/--></paragraph></cell>
        <cell left="true" bottom="true" right="true" verticalalign="middle" horizontalalign="right"><paragraph lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}"><xsl:choose><xsl:when test="Context/period[@end='true']"><xsl:apply-templates select="." mode="GetFieldData"><xsl:with-param name="period" select="'I-End'"/></xsl:apply-templates></xsl:when><xsl:when test="Context/period[(@type='duration' and not(@start='true'))]"><xsl:apply-templates select="." mode="GetFieldData"><xsl:with-param name="period" select="'D'"/></xsl:apply-templates></xsl:when><xsl:otherwise><xsl:text disable-output-escaping="yes">&amp;#160;</xsl:text></xsl:otherwise></xsl:choose><!--xsl:value-of select="@xbrlId"/--></paragraph></cell>
      </xsl:when>
    </xsl:choose>
    </row>
      </xsl:otherwise>
    </xsl:choose>

    
    
    
  </xsl:template>

  <xsl:template match="ValueList" mode="GetFieldData">
    <xsl:param name="period"/><xsl:variable name="fieldName" select="@xbrlName"/><xsl:variable name="fieldPrefix" select="substring-before(@xbrlId,'_')"/><xsl:choose><xsl:when test="parent::*[@isAbstract='false' and starts-with(@xbrlId,'pfs')]">/<xsl:for-each select="ancestor::Pane[@isAbstract='false']"><xsl:variable name="parName" select="@xbrlName"/><xsl:variable name="parPrefix" select="substring-before(@xbrlId,'_')"/>/XbrlElementDataContract[Prefix='<xsl:value-of select="$parPrefix"/>' and Name='<xsl:value-of select="$parName"/>']/Children</xsl:for-each>/XbrlElementDataContract[Period='<xsl:value-of select="$period"/>'][1]/Name</xsl:when><xsl:otherwise>//XbrlElementDataContract[Prefix='<xsl:value-of select="$fieldPrefix"/>' and Name='<xsl:value-of select="$fieldName"/>']//Children[Period='<xsl:value-of select="$period"/>'][1]/Name</xsl:otherwise></xsl:choose>
  </xsl:template>
  
  <xsl:template match="Field|Pane" mode="GetFieldData">
    <xsl:param name="period"/>
    <xsl:param name="contextPart"/>
    <xsl:param name="contextExact"/>
    <xsl:variable name="ftype">
      <xsl:choose>
        <xsl:when test="contains(@itemType,'monetary') or contains(@itemType,'Monetary')">MonetaryValue</xsl:when>
        <xsl:when test="@itemType='percentageItemType'">PercentValue</xsl:when>
        <xsl:when test="@itemType='booleanItemType'">BooleanValue</xsl:when>
        <xsl:when test="contains(@itemType,'base64') or contains(@itemType,'Base64')">BinaryValue</xsl:when>
        <xsl:otherwise>StandardValue</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xso:call-template>
      <xsl:attribute name="name"><xsl:value-of select="$ftype"/></xsl:attribute>
      <xso:with-param name="value">
        <xsl:attribute name="select"><xsl:apply-templates select="." mode="GetFieldDataSelect"><xsl:with-param name="period" select="$period"/><xsl:with-param name="contextPart" select="$contextPart"/><xsl:with-param name="contextExact" select="$contextExact"/><xsl:with-param name="ftype" select="$ftype"/></xsl:apply-templates></xsl:attribute>
      </xso:with-param>
    </xso:call-template>
  </xsl:template>

  <xsl:template match="Field|Pane" mode="GetFieldDataSelect"><xsl:param name="period"/><xsl:param name="contextPart"/><xsl:param name="contextExact"/><xsl:param name="ftype"/><xsl:variable name="fieldName" select="@xbrlName"/><xsl:variable name="fieldPrefix" select="substring-before(@xbrlId,'_')"/><xsl:choose><xsl:when test="$contextPart">//XbrlElementDataContract[Prefix='<xsl:value-of select="$fieldPrefix"/>' and Name='<xsl:value-of select="$fieldName"/>' and Period='<xsl:value-of select="$period"/>' and Context=<xsl:value-of select="$contextPart"/>]<xsl:choose><xsl:when test="$ftype='BinaryValue'"></xsl:when><xsl:otherwise>/Value</xsl:otherwise></xsl:choose></xsl:when><xsl:when test="$contextExact">//XbrlElementDataContract[Prefix='<xsl:value-of select="$fieldPrefix"/>' and Name='<xsl:value-of select="$fieldName"/>' and Period='<xsl:value-of select="$period"/>' and ContextRef='<xsl:value-of select="$contextExact"/>']<xsl:choose><xsl:when test="$ftype='BinaryValue'"></xsl:when><xsl:otherwise>/Value</xsl:otherwise></xsl:choose></xsl:when><xsl:otherwise><xsl:choose><xsl:when test="parent::*[@isAbstract='false' and starts-with(@xbrlId,'pfs')]">/<xsl:for-each select="ancestor::Pane[@isAbstract='false']"><xsl:variable name="parName" select="@xbrlName"/><xsl:variable name="parPrefix" select="substring-before(@xbrlId,'_')"/>/XbrlElementDataContract[Prefix='<xsl:value-of select="$parPrefix"/>' and Name='<xsl:value-of select="$parName"/>']/Children</xsl:for-each>/XbrlElementDataContract[Prefix='<xsl:value-of select="$fieldPrefix"/>' and Name='<xsl:value-of select="$fieldName"/>' and Period='<xsl:value-of select="$period"/>' and ContextRef='<xsl:value-of select="$period"/>']<xsl:choose><xsl:when test="$ftype='BinaryValue'"></xsl:when><xsl:otherwise>/Value</xsl:otherwise></xsl:choose></xsl:when><xsl:otherwise>//XbrlElementDataContract[Prefix='<xsl:value-of select="$fieldPrefix"/>' and Name='<xsl:value-of select="$fieldName"/>' and Period='<xsl:value-of select="$period"/>' and ContextRef='<xsl:value-of select="$period"/>']<xsl:choose><xsl:when test="$ftype='BinaryValue'"></xsl:when><xsl:otherwise>/Value</xsl:otherwise></xsl:choose></xsl:otherwise></xsl:choose></xsl:otherwise></xsl:choose></xsl:template>
  

  <xsl:template name="columnloop"><xsl:param name="cycles" /><xsl:param name="columnwidth" /><xsl:choose><xsl:when test="number($cycles) &gt; 0"><xsl:value-of select="$columnwidth"/><xsl:if test="number($cycles) &gt; 1">;</xsl:if><xsl:call-template name="columnloop"><xsl:with-param name="cycles" select="number($cycles)-1" /><xsl:with-param name="columnwidth" select="$columnwidth"/></xsl:call-template></xsl:when></xsl:choose></xsl:template>
  
  <xsl:template match="Pane" mode="ContextGrid">
    <xsl:param name="parentTableCols"/>
    <xsl:param name="level"/>
    <xsl:param name="indent"/>

    <xsl:if test="$parentTableCols and not($parentTableCols=0)">
      <xsl:call-template name="EndTable"/>
    </xsl:if>
    <xsl:variable name="doubleheaders" select="count(.//Field[Context/period[@start='true' and @end='true']])" />
    <xsl:variable name="headers" select="count(ElementContextDef/FieldDefinition) + count(.//Field) + $doubleheaders"/>
    <xsl:variable name="columnWidths" select="100 div $headers"/>

    <xsl:variable name="fontsize">
      <xsl:choose>
        <xsl:when test="$headers>5">4<!--xsl:value-of select="round((number($ctxfontsize) div 5))"/--></xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="$ctxfontsize"/></xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    
    <xsl:variable name="columns"><xsl:call-template name="columnloop"><xsl:with-param name="cycles" select="$headers"/><xsl:with-param name="columnwidth" select="$columnWidths"/></xsl:call-template></xsl:variable>

    <table tablefitspage="true" convert2pdfp="true" cellsfitpage="true" borderwidth="0.02" columns="{$headers}" width="100%" align="right" widths="{$columns}" cellspacing="0" cellpadding="3">
      <xsl:call-template name="TitleRow">
        <xsl:with-param name="cols" select="$headers"/>
        <xsl:with-param name="level" select="$level"/>
        <xsl:with-param name="indent" select="0"/>
        <xsl:with-param name="title">
          <xsl:apply-templates select="." mode="GetLabel">
            <xsl:with-param name="type" select="'label'"/>
          </xsl:apply-templates>
        </xsl:with-param>
        <xsl:with-param name="border" select="false"/>
      </xsl:call-template>
      <row>
        <xsl:for-each select="ElementContextDef/FieldDefinition">
          <cell left="true" bottom="true" top="true" grayfill="0.9" horizontalalign="center" verticalalign="Top">
            <xsl:if test="$doubleheaders"><xsl:attribute name="rowspan">2</xsl:attribute></xsl:if>
            <paragraph lineheight="{$WorksheetSpacing}" size="{$fontsize}"><xsl:value-of select="./ArrayOfElementLabel/ElementLabel/Descriptions/Translation[Language=$culture]/Description"/></paragraph>
          </cell>
        </xsl:for-each>
        <xsl:for-each select=".//Field">
          <cell left="true" bottom="true" top="true" grayfill="0.9" horizontalalign="center" verticalalign="Top">
            <xsl:if test="position()=last()">
              <xsl:attribute name="right">true</xsl:attribute>
            </xsl:if>
            <xsl:if test="$doubleheaders and Context/period[@start='true' and @end='true']">
              <xsl:attribute name="colspan">2</xsl:attribute>
            </xsl:if>
            <xsl:if test="$doubleheaders and not(Context/period[@start='true' and @end='true'])">
              <xsl:attribute name="rowspan">2</xsl:attribute>
            </xsl:if>
            <paragraph lineheight="{$WorksheetSpacing}" size="{$fontsize}"><xsl:apply-templates select="." mode="GetLabel"><xsl:with-param name="type" select="'label'"/></xsl:apply-templates></paragraph>
          </cell>
        </xsl:for-each>
      </row>
      <xsl:if test="$doubleheaders">
        <row>
          <xsl:for-each select=".//Field[Context/period[@start='true' and @end='true']]">
            <cell left="true" bottom="true" grayfill="0.9" horizontalalign="center" verticalalign="Top">
              <xsl:if test="position()=last()">
                <xsl:attribute name="right">true</xsl:attribute>
              </xsl:if>
              <paragraph lineheight="{$WorksheetSpacing}" size="{$fontsize}"><xsl:value-of select="$ISTART"/></paragraph>
            </cell>
             <cell left="true" bottom="true" grayfill="0.9" horizontalalign="center" verticalalign="Top">
              <xsl:if test="position()=last()">
                <xsl:attribute name="right">true</xsl:attribute>
              </xsl:if>
              <paragraph lineheight="{$WorksheetSpacing}" size="{$fontsize}"><xsl:value-of select="$IEND"/></paragraph>
            </cell>
          </xsl:for-each>
        </row>
      </xsl:if>
      <xsl:variable name="firstFieldId" select=".//Field[1]/@xbrlId"/>
      <xsl:variable name="lastFieldId" select=".//Field[last()]/@xbrlId"/>

      <xsl:variable name="prefix" select="substring-before($firstFieldId,'_')"/>
      <xsl:variable name="firstName" select=".//Field[1]/@xbrlName"/>
      <xsl:variable name="lastName" select=".//Field[last()]/@xbrlName"/>


      <xso:variable name="contextIds">
        <xsl:attribute name="select">Helper:Distinct(//XbrlElementDataContract[Prefix='<xsl:value-of select="$prefix"/>' and (Name='<xsl:value-of select="$firstName"/>' or Name='<xsl:value-of select="$lastName"/>')]/Context,'./text()')</xsl:attribute>
      </xso:variable>
      <xso:for-each select="$contextIds">
        <xso:variable name="contextId" select="string(.)"/>
        <xso:variable name="context">
          <xsl:attribute name="select">//ContextElementDataContract[contains(Id,$contextId)][1]</xsl:attribute>
        </xso:variable>
        <row>
        <xsl:for-each select="ElementContextDef/FieldDefinition">
          <xsl:variable name="dimensionPart" select="substring-after(Dimension,'#')"/>
          <xsl:variable name="dimension" select="concat(substring-before($dimensionPart,'_'),':',substring-after($dimensionPart,'_'))"/>
          <cell left="true" bottom="true" horizontalalign="center" verticalalign="Top">
            <paragraph lineheight="{$WorksheetSpacing}" size="{$fontsize}">
              <xsl:choose>
                <xsl:when test="ListData/FieldListItem"><xso:call-template name="contextLookup"><xso:with-param name="value"><xsl:attribute name="select">$context/Scenario/ContextScenarioDataContract[Dimension='<xsl:value-of select="$dimension"/>']/Value</xsl:attribute></xso:with-param></xso:call-template></xsl:when>
                <xsl:otherwise><xso:value-of><xsl:attribute name="select">$context/Scenario/ContextScenarioDataContract[Dimension='<xsl:value-of select="$dimension"/>']/Value</xsl:attribute></xso:value-of></xsl:otherwise>
              </xsl:choose>
              
            </paragraph>
          </cell>
        </xsl:for-each>
          <xsl:for-each select=".//Field">
            <xsl:choose>
              <xsl:when test="Context/period[@start='true' and @end='true']">
                <cell left="true" bottom="true" horizontalalign="right" verticalalign="Top">
                  <paragraph lineheight="{$WorksheetSpacing}" size="{$fontsize}">
                    <xsl:apply-templates select="." mode="GetFieldData">
                      <xsl:with-param name="period" select="'I-Start'"/>
                      <xsl:with-param name="contextPart" select="'$contextId'"/>
                    </xsl:apply-templates>
                  </paragraph>
                </cell>
                <cell left="true" bottom="true" horizontalalign="right" verticalalign="Top">
                  <xsl:if test="position()=last()">
                    <xsl:attribute name="right">true</xsl:attribute>
                  </xsl:if>
                  <paragraph lineheight="{$WorksheetSpacing}" size="{$fontsize}">
                    <xsl:apply-templates select="." mode="GetFieldData">
                      <xsl:with-param name="period" select="'I-End'"/>
                      <xsl:with-param name="contextPart" select="'$contextId'"/>
                    </xsl:apply-templates>
                  </paragraph>
                </cell>
              </xsl:when>
              <xsl:when test="Context/period[@start='true']">
                <cell left="true" bottom="true" horizontalalign="right" verticalalign="Top">
                  <xsl:if test="position()=last()">
                    <xsl:attribute name="right">true</xsl:attribute>
                  </xsl:if>
                  <paragraph lineheight="{$WorksheetSpacing}" size="{$fontsize}">
                    <xsl:apply-templates select="." mode="GetFieldData">
                      <xsl:with-param name="period" select="'I-Start'"/>
                      <xsl:with-param name="contextPart" select="'$contextId'"/>
                    </xsl:apply-templates>
                  </paragraph>
                </cell>
              </xsl:when>
              <xsl:when test="Context/period[@end='true']">
                <cell left="true" bottom="true" horizontalalign="right" verticalalign="Top">
                  <xsl:if test="position()=last()">
                    <xsl:attribute name="right">true</xsl:attribute>
                  </xsl:if>
                  <paragraph lineheight="{$WorksheetSpacing}" size="{$fontsize}">
                    <xsl:apply-templates select="." mode="GetFieldData">
                      <xsl:with-param name="period" select="'I-End'"/>
                      <xsl:with-param name="contextPart" select="'$contextId'"/>
                    </xsl:apply-templates>
                  </paragraph>
                </cell>
              </xsl:when>
              <xsl:when test="Context/period[@type='duration']">
                <cell left="true" bottom="true" horizontalalign="right" verticalalign="Top">
                  <xsl:if test="position()=last()">
                    <xsl:attribute name="right">true</xsl:attribute>
                  </xsl:if>
                  <paragraph lineheight="{$WorksheetSpacing}" size="{$fontsize}">
                    <xsl:apply-templates select="." mode="GetFieldData">
                      <xsl:with-param name="period" select="'D'"/>
                      <xsl:with-param name="contextPart" select="'$contextId'"/>
                    </xsl:apply-templates>
                  </paragraph>
                </cell>
              </xsl:when>
            </xsl:choose>
            
          </xsl:for-each>
        </row>
      </xso:for-each>
     
      
      
    </table>


    <xsl:if test="$parentTableCols and not($parentTableCols=0)">
       <xsl:call-template name="StartTable">
              <xsl:with-param name="cols" select="$parentTableCols"/>
            </xsl:call-template>
      <xsl:call-template name="SetHeading">
        <xsl:with-param name="cols" select="$parentTableCols"/>
      </xsl:call-template>
    </xsl:if>
    

    
    
  </xsl:template>

  <xsl:template match="Pane" mode="Pivot">
    <xsl:param name="parentTableCols"/>
    <xsl:param name="level"/>
    <xsl:param name="indent"/>
    <xsl:variable name="countHeaders" select="count(PivotHeaders/FieldDefinition)"/>
    <xsl:variable name="cols" select="$countHeaders+3"/>
    <xsl:variable name="colwidths">
      <xsl:value-of select="100-($countHeaders * 20)-16"/>;8;8<xsl:for-each select="PivotHeaders/FieldDefinition">;20</xsl:for-each>
    </xsl:variable>
    <xsl:if test="not(string(number($parentTableCols))='NaN')">
      <xsl:text disable-output-escaping="yes">&lt;/table&gt;</xsl:text>
    </xsl:if>
    <xsl:variable name="headers" select="PivotHeaders/FieldDefinition"/>
    <table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" columns="{$cols}" width="100%" align="Center" widths="{$colwidths}" cellspacing="0" cellpadding="3">
      <row>
        <cell colspan="{$cols}">
          <xsl:call-template name="SetPhrase">
            <xsl:with-param name="title">
              <xsl:apply-templates select="." mode="GetLabel">
                <xsl:with-param name="type" select="'label'"/>
              </xsl:apply-templates>
            </xsl:with-param>
            <xsl:with-param name="levelNr" select="$level"/>
            <xsl:with-param name="indent" select="$indent"/>
          </xsl:call-template>
        </cell>
      </row>
      <row>
        <cell left="true" bottom="true" top="true" grayfill="0.9" verticalalign="Top">
          <paragraph lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}"><xsl:text disable-output-escaping="yes">&amp;</xsl:text>#160;</paragraph>
        </cell>
        <cell colspan="2" left="true" bottom="true" top="true" grayfill="0.9" horizontalalign="center" verticalalign="Top">
          <paragraph lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}"><xsl:value-of select="$CODES"/></paragraph>
        </cell>
        <xsl:for-each select="$headers">
          <cell left="true" bottom="true" top="true" grayfill="0.9" verticalalign="Top">
            <xsl:if test="position()=$countHeaders">
              <xsl:attribute name="right">true</xsl:attribute>
            </xsl:if>
            <paragraph lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" horizontalalign="center">
              <xsl:value-of select="TypeElement/Labels/ElementLabel/Descriptions/Translation[Language=$culture]/Description"/>
            </paragraph>
          </cell>
        </xsl:for-each>
      </row>
      <xsl:apply-templates select="Field|Pane" mode="Pivotchild">
        <xsl:with-param name="headers" select="$headers"/>
        <xsl:with-param name="level" select="$level"/>
        <xsl:with-param name="indent" select="0"/>
      </xsl:apply-templates>
     
    </table>
    <xsl:if test="$parentTableCols and not($parentTableCols=0) and local-name(following-sibling::*[1])='Pane'">
        <xsl:variable name="curCols">
          <xsl:choose>
            <xsl:when test="following-sibling::*[1]/Field/Context/period[@start='true' or @end='true']">5</xsl:when>
            <xsl:when test="ancestor::Tab[@xbrlName='CompanyInformationForm']">2</xsl:when>
            <xsl:when test="not(following-sibling::*[1]/Field[not(@isAbstract='true')])">0</xsl:when>
            <xsl:otherwise>4</xsl:otherwise>
          </xsl:choose>
        </xsl:variable>
      <start/>
      <xsl:call-template name="StartTable">
        <xsl:with-param name="cols" select="$curCols"/>
      </xsl:call-template>
      <xsl:call-template name="SetHeading">
        <xsl:with-param name="cols" select="$curCols"/>
      </xsl:call-template>
    </xsl:if>
  </xsl:template>

  <xsl:template match="Field" mode="Pivotchild">
    <xsl:param name="headers"/>
    <xsl:param name="level"/>
    <xsl:param name="indent"/>
    <row>
      <cell bottom="true" left="true">
        <paragraph lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
          <xsl:attribute name="indentationleft"><xsl:value-of select="$indent * 10"/></xsl:attribute>
          <xsl:apply-templates select="." mode="GetLabel">
            <xsl:with-param name="type" select="'label'"/>
          </xsl:apply-templates>
        </paragraph>
      </cell>
      <cell bottom="true" left="true">
        <paragraph lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
          <xsl:apply-templates select="." mode="GetOldCode"/>
        </paragraph>
      </cell>
      <cell bottom="true" left="true">
        <paragraph lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
          <xsl:apply-templates select="." mode="GetCode"/>
        </paragraph>
      </cell>
      <xsl:variable name="fld" select="."/>
      <xsl:variable name="period"><xsl:choose><xsl:when test="Context/period[@start='true']">I-Start</xsl:when><xsl:when test="Context/period[@end='true']">I-End</xsl:when><xsl:when test="Context/period[@type='duration']">D</xsl:when></xsl:choose></xsl:variable>
      <xsl:for-each select="$headers">
        <cell bottom="true" left="true">
          <xsl:if test="position()=last()">
            <xsl:attribute name="right">true</xsl:attribute>
          </xsl:if>
          <paragraph lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
           <xsl:apply-templates select="$fld" mode="GetFieldData"><xsl:with-param name="period" select="$period"/><xsl:with-param name="contextExact"><xsl:value-of select="$period"/>__<xsl:value-of select="ListData/FieldListItem/ElementDef/Name"/></xsl:with-param></xsl:apply-templates>
          </paragraph>
        </cell>
      </xsl:for-each>
    </row>
  </xsl:template>

  <xsl:template match="Pane" mode="Pivotchild">
    <xsl:param name="headers"/>
    <xsl:param name="level"/>
    <xsl:param name="indent"/>
    <xsl:variable name="cols" select="count($headers)+3"/>
    <row>
      <cell left="true" right="true" bottom="true" colspan="{$cols}">
        <paragraph lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
          <xsl:apply-templates select="." mode="GetLabel">
            <xsl:with-param name="type" select="'label'"/>
          </xsl:apply-templates>
        </paragraph>
      </cell>
    </row>
    <xsl:apply-templates select="Field|Pane" mode="Pivotchild">
      <xsl:with-param name="headers" select="$headers"/>
      <xsl:with-param name="level" select="$level+1"/>
      <xsl:with-param name="indent" select="$indent+1"/>
    </xsl:apply-templates>
  </xsl:template>
  
 <xsl:template match="Pane" mode="PivotContextHeaders">
  <xsl:for-each select="PivotHeaders/FieldDefinition">
    <xsl:if test="position()>1">,</xsl:if>{id:'<xsl:value-of select="TypeElement/Id"/>'
    ,name:'<xsl:value-of select="TypeElement/Labels/ElementLabel/Descriptions/Translation[Language=$culture]/Description"/>'
    ,dimension:'<xsl:value-of select="Dimension"/>'
    ,domain:'<xsl:value-of select="ListData/FieldListItem[1]/Domain"/>'
    ,domainMember:'<xsl:value-of select="ListData/FieldListItem[1]/DomainMember"/>'
    ,dimensionType:'<xsl:value-of select="DimensionType"/>'
    }
  </xsl:for-each>
  </xsl:template>
  
  <xsl:template match="*" mode="GetContexts">
    <xsl:for-each select="Context">
      
    </xsl:for-each>
  </xsl:template>

  <xsl:template match="*" mode="GetPeriods">
  <xsl:if test="Context/period"><xsl:choose><xsl:when test="Context/period/@start='true' and Context/period/@end='true'">'I-Start','I-End'</xsl:when><xsl:when test="(Context/period/@start='false' or not(Context/period/@start)) and Context/period/@end='true'">'I-End'</xsl:when><xsl:when test="Context/period/@start='true' and (Context/period/@end='false'  or not(Context/period/@end))">'I-Start'</xsl:when><xsl:when test="not(Context/period/@start) and not(Context/period/@end)">'D'</xsl:when><xsl:otherwise>'D'</xsl:otherwise></xsl:choose></xsl:if>
  </xsl:template>
  
  <xsl:template match="Pane" mode="CommonPaneAttribs">
    ,title:'<xsl:apply-templates select="." mode="GetLabel"><xsl:with-param name="type" select="@preferredLabel"/></xsl:apply-templates>'
    ,code:'<xsl:apply-templates select="." mode="GetCode"/>'
    ,documentation:'<xsl:apply-templates select="." mode="GetLabel"><xsl:with-param name="type">documentation</xsl:with-param></xsl:apply-templates>'
    ,multipleContexts:<xsl:value-of select="@multipleContexts='true'"/>
    ,viewTplType:'<xsl:choose><xsl:when test="not(Field[not(@isAbstract='true')])">panesOnly</xsl:when><xsl:when test=".//Field/Context/period[@start='true' or @end='true']">instant</xsl:when><xsl:otherwise >duration</xsl:otherwise></xsl:choose>'
    <xsl:if test="not(@isAbstract='true') and @itemType">
    ,fieldInfo:<xsl:apply-templates select="." mode="PaneField"/>
    </xsl:if>
    <xsl:apply-templates select="." mode="CommonAttribs"/>
  </xsl:template>

  <xsl:template match="*" mode="CommonAttribs">
    , xbrlDef: {
      id:'<xsl:value-of select="@xbrlId"/>'
      , name:'<xsl:value-of select="@xbrlName"/>'
      , itemType:'<xsl:value-of select="@itemType"/>'
    }
    ,isAbstract:<xsl:value-of select="@isAbstract='true'"/>
    <xsl:variable name="id" select="@xbrlId"/>
    <xsl:variable name="lockitem" select="//MetaData/itemlocks/itemlock[@id=$id]"/>
    <xsl:if test="$lockitem">
      <xsl:choose>
        <xsl:when test="$lockitem/@locktype='always'">, elementsDisabled:true</xsl:when>
        <xsl:otherwise>, elementsDisabledFn:function() { return eBook.Interface.hasService('<xsl:value-of select="$lockitem/service/@id"/>'); } </xsl:otherwise>
      </xsl:choose>
    </xsl:if>
    

  </xsl:template>
  
  <xsl:template match="*" mode="GetCode">
    <xsl:if test="not(starts-with(@xbrlId,'pfs-'))">
    <xsl:apply-templates select="." mode="GetLabel">
      <xsl:with-param name="type">documentation</xsl:with-param>
      <xsl:with-param name="myculture">en</xsl:with-param>
    </xsl:apply-templates>
    </xsl:if>
  </xsl:template>

  <xsl:template match="*" mode="GetOldCode">
    <xsl:apply-templates select="." mode="GetLabel">
      <xsl:with-param name="type">documentation</xsl:with-param>
      <xsl:with-param name="myculture">en-US</xsl:with-param>
    </xsl:apply-templates>
  </xsl:template>

  <xsl:template name="ClearOutApos">
    <xsl:param name="txt"/>
    <xsl:variable name="apos">'</xsl:variable>
    <xsl:variable name="aposEsc">'</xsl:variable>
    <xsl:call-template name="string-replace-all">
      <xsl:with-param name="text" select="string($txt)" />
      <xsl:with-param name="replace" select="$apos" />
      <xsl:with-param name="by" select="$aposEsc" />
    </xsl:call-template>
  </xsl:template>
  
  <xsl:template match="*" mode="GetLabel">
    <xsl:param name="type"/>
    <xsl:param name="myculture"/>
    <xsl:variable name="selCulture"><xsl:choose>
      <xsl:when test="$myculture and not($myculture='')"><xsl:value-of select="$myculture"/></xsl:when>
      <xsl:otherwise><xsl:value-of select="$culture"/></xsl:otherwise></xsl:choose></xsl:variable>
    <xsl:variable name="apos">'</xsl:variable>
    <xsl:variable name="aposEsc">'</xsl:variable>
    <xsl:variable name="txt">
      <xsl:choose>
        <xsl:when test="labels/label[@type=$type]">
          <xsl:value-of select="labels/label[@type=$type]/translation[@lang=$selCulture]"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:if test="labels/label">
            <xsl:value-of select="labels/label[1]/translation[@lang=$selCulture]"/>
          </xsl:if>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:call-template name="string-replace-all">
      <xsl:with-param name="text" select="string($txt)" />
      <xsl:with-param name="replace" select="$apos" />
      <xsl:with-param name="by" select="$aposEsc" />
    </xsl:call-template>
  </xsl:template>

  <xsl:template match="Field|Pane|ValueList" mode="FieldTypeAttributes">
    <xsl:variable name="valueType"><xsl:choose><xsl:when test="not(@itemType) or @itemType=''"></xsl:when><xsl:otherwise><xsl:variable name="itemType" select="@itemType"/><xsl:variable name="typeEl" select="//XbrlDataType[Name=$itemType]"/><xsl:choose><xsl:when test="$typeEl"><xsl:value-of select="$typeEl/ValueType"/></xsl:when><xsl:otherwise></xsl:otherwise></xsl:choose></xsl:otherwise></xsl:choose></xsl:variable>
    
    <xsl:variable name="setType">
      <xsl:choose>
        <xsl:when test="contains(@itemType,'monetary') or contains(@itemType,'Monetary')">EUR</xsl:when>
        <xsl:otherwise>
        <xsl:choose>
          <xsl:when test="string($valueType)='System.Decimal'">PURE</xsl:when>
          <xsl:when test="string($valueType)='System.Byte[]'">BTE</xsl:when>
          <xsl:when test="string($valueType)='System.DateTime'">DTE</xsl:when>
          <xsl:when test="string($valueType)='System.String'">STR</xsl:when>
          <xsl:when test="string($valueType)='System.Single'">PURE</xsl:when>
          <xsl:when test="string($valueType)='System.Double'">PURE</xsl:when>
          <xsl:when test="string($valueType)='System.Single'">PURE</xsl:when>
          <xsl:when test="string($valueType)='System.Int64'">PURE</xsl:when>
          <xsl:when test="string($valueType)='System.Int32'">PURE</xsl:when>
          <xsl:when test="string($valueType)='System.Int16'">PURE</xsl:when>
          <xsl:when test="string($valueType)='System.UInt64'">PURE</xsl:when>
          <xsl:when test="string($valueType)='System.UInt32'">PURE</xsl:when>
          <xsl:when test="string($valueType)='System.UInt16'">PURE</xsl:when>
          <xsl:when test="string($valueType)='System.SByte'">BTE</xsl:when>
          <xsl:when test="string($valueType)='System.Byte'">BTE</xsl:when>
          <xsl:when test="string($valueType)='System.Boolean'">BOOL</xsl:when>
          <xsl:when test="string($valueType)='System.Uri'">URI</xsl:when>
          <xsl:when test="string($valueType)='System.TimeSpan'">TS</xsl:when>
          <xsl:when test="string($valueType)='System.Object'">OBJ</xsl:when>
          <xsl:otherwise>NULL</xsl:otherwise>
        </xsl:choose>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:choose>
      <xsl:when test="$setType='EUR'">
        ,decimalAttribute:'INF'
        ,unitAttribute:'EUR'
      </xsl:when>
      <xsl:when test="$setType='PURE'">
        ,decimalAttribute:'INF'
        ,unitAttribute:'U-Pure'
      </xsl:when>
      <xsl:otherwise>
        ,decimalAttribute:null
        ,unitAttribute:null
      </xsl:otherwise>
    </xsl:choose>
    
  </xsl:template>

  
 
  
  <xsl:template match="Field|Pane" mode="GetFieldType">
    <xsl:choose>
      <xsl:when test="not(@itemType) or @itemType=''">unknown</xsl:when>
      <xsl:otherwise>
        <xsl:variable name="itemType" select="@itemType"/>
        <xsl:variable name="typeEl" select="//XbrlDataType[Name=$itemType]"/>
        <xsl:choose>
          <xsl:when test="$typeEl">
            <xsl:apply-templates select="$typeEl" mode="GetFieldType"/>
          </xsl:when>
          <xsl:otherwise>
            Type <xsl:value-of select="$itemType"/> NOT FOUND
          </xsl:otherwise>
        </xsl:choose>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

   <xsl:template match="TypeElement" mode="GetFieldType">
      <xsl:choose>
        <xsl:when test="./Type='http://www.w3.org/2001/XMLSchema:date'">{xtype:'datefield'}</xsl:when>
        <xsl:otherwise>,:</xsl:otherwise> <!-- FORCE JS ERROR -->
      </xsl:choose>
  </xsl:template>
  
  <xsl:template match="XbrlDataType|DataType" mode="GetFieldType">
    <xsl:variable name="itemType" select="Name/text()"/>
    <xsl:choose>
        <xsl:when test="./ValueType='System.Decimal'"><xsl:apply-templates select="." mode="DetermineNumberFieldDef"><xsl:with-param name="itemType" select="$itemType"/></xsl:apply-templates></xsl:when>
        <xsl:when test="./ValueType='System.Byte[]'"><xsl:apply-templates select="." mode="BundleBuilderFieldDef"/></xsl:when>
        <xsl:when test="./ValueType='System.DateTime'"><xsl:apply-templates select="." mode="DateFieldDef"/></xsl:when>
        <xsl:when test="./ValueType='System.String'"><xsl:apply-templates select="." mode="TextFieldDef"/></xsl:when>
        <xsl:when test="./ValueType='System.Single'"><xsl:apply-templates select="." mode="DetermineNumberFieldDef"><xsl:with-param name="itemType" select="$itemType"/></xsl:apply-templates></xsl:when>
        <xsl:when test="./ValueType='System.Double'"><xsl:apply-templates select="." mode="DetermineNumberFieldDef"><xsl:with-param name="itemType" select="$itemType"/></xsl:apply-templates></xsl:when>
        <xsl:when test="./ValueType='System.Single'"><xsl:apply-templates select="." mode="DetermineNumberFieldDef"><xsl:with-param name="itemType" select="$itemType"/></xsl:apply-templates></xsl:when>
        <xsl:when test="./ValueType='System.Int64'"><xsl:apply-templates select="." mode="DetermineNumberFieldDef"><xsl:with-param name="itemType" select="$itemType"/></xsl:apply-templates></xsl:when>
        <xsl:when test="./ValueType='System.Int32'"><xsl:apply-templates select="." mode="DetermineNumberFieldDef"><xsl:with-param name="itemType" select="$itemType"/></xsl:apply-templates></xsl:when>
        <xsl:when test="./ValueType='System.Int16'"><xsl:apply-templates select="." mode="DetermineNumberFieldDef"><xsl:with-param name="itemType" select="$itemType"/></xsl:apply-templates></xsl:when>
        <xsl:when test="./ValueType='System.UInt64'"><xsl:apply-templates select="." mode="DetermineNumberFieldDef"><xsl:with-param name="itemType" select="$itemType"/></xsl:apply-templates></xsl:when>
        <xsl:when test="./ValueType='System.UInt32'"><xsl:apply-templates select="." mode="DetermineNumberFieldDef"><xsl:with-param name="itemType" select="$itemType"/></xsl:apply-templates></xsl:when>
        <xsl:when test="./ValueType='System.UInt16'"><xsl:apply-templates select="." mode="DetermineNumberFieldDef"><xsl:with-param name="itemType" select="$itemType"/></xsl:apply-templates></xsl:when>
        <xsl:when test="./ValueType='System.SByte'"><xsl:apply-templates select="." mode="BundleBuilderFieldDef"/></xsl:when>
        <xsl:when test="./ValueType='System.Byte'"><xsl:apply-templates select="." mode="BundleBuilderFieldDef"/></xsl:when>
        <xsl:when test="./ValueType='System.Boolean'"><xsl:apply-templates select="." mode="CheckFieldDef"/></xsl:when>
        <xsl:when test="./ValueType='System.Uri'"><xsl:apply-templates select="." mode="TextFieldDef"/></xsl:when>
        <xsl:when test="./ValueType='System.TimeSpan'"><xsl:apply-templates select="." mode="DetermineNumberFieldDef"><xsl:with-param name="itemType" select="$itemType"/></xsl:apply-templates></xsl:when>
        <xsl:when test="./ValueType='System.Object'">{xtype:'textfield',value:'unknown'}</xsl:when>
      </xsl:choose>
  </xsl:template>
  

  <xsl:template match="XbrlDataType|DataType" mode="DetermineNumberFieldDef">
    <xsl:param name="itemType"/>
    <xsl:choose><xsl:when test="contains($itemType,'monetary') or contains($itemType,'Monetary')"><xsl:apply-templates select="." mode="CurrencyFieldDef"/></xsl:when><xsl:when test="contains($itemType,'percent')"><xsl:apply-templates select="." mode="PercentageFieldDef"/></xsl:when><xsl:otherwise><xsl:apply-templates select="." mode="NumberFieldDef"/></xsl:otherwise></xsl:choose>
  </xsl:template>
  
  <xsl:template match="XbrlDataType|DataType" mode="TextFieldDef">
    {xtype:'textfield'
    <xsl:for-each select="Restrictions/Restriction">
      <xsl:choose>
        <xsl:when test="Name='System.Xml.Schema.XmlSchemaPatternFacet'">,regex:new RegExp('<xsl:value-of select="Value"/>')</xsl:when>
        <xsl:when test="Name='System.Xml.Schema.XmlSchemaMinLengthFacet'">,minLength: <xsl:value-of select="Value"/></xsl:when>
        <xsl:when test="Name='System.Xml.Schema.XmlSchemaMaxLengthFacet'">,maxLength: <xsl:value-of select="Value"/></xsl:when>        
      </xsl:choose>
    </xsl:for-each>
    }
  </xsl:template>

  <xsl:template match="XbrlDataType|DataType" mode="CheckFieldDef">
    {xtype:'checkbox'}
  </xsl:template>

  <xsl:template match="XbrlDataType|DataType" mode="NumberFieldDef">
    {xtype:'numberfield'
    <xsl:apply-templates select="." mode="SetNumberFieldProps"/>
    }
  </xsl:template>

  <xsl:template match="XbrlDataType|DataType" mode="CurrencyFieldDef">
    {xtype:'currencyfield'
      ,currencySymbol: '€'
      ,useThousandSeparator: true
      ,thousandSeparator: '.'
      ,alwaysDisplayDecimals: true
      ,decimalSeparator:','
      <xsl:apply-templates select="." mode="SetNumberFieldProps"/>
    }
  </xsl:template>

  <xsl:template match="XbrlDataType|DataType" mode="PercentageFieldDef">
    {xtype:'currencyfield'
    ,currencySymbol: '%'
    ,useThousandSeparator: false
    ,thousandSeparator: '.'
    ,alwaysDisplayDecimals: true
    ,decimalSeparator:','
    <xsl:apply-templates select="." mode="SetNumberFieldProps"/>
    }
  </xsl:template>

  <xsl:template match="XbrlDataType|DataType" mode="SetNumberFieldProps">
    <xsl:for-each select="Restrictions/Restriction">
      <xsl:choose>
        <xsl:when test="Name='System.Xml.Schema.XmlSchemaTotalDigitsFacet'">,maxLength:<xsl:value-of select="Value"/></xsl:when>
        <xsl:when test="Name='System.Xml.Schema.XmlSchemaFractionDigitsFacet'">,decimalPrecision:<xsl:value-of select="Value"/></xsl:when>
        <xsl:when test="Name='System.Xml.Schema.XmlSchemaMinInclusiveFacet'">,minValue:<xsl:value-of select="Value"/></xsl:when>
        <xsl:when test="Name='System.Xml.Schema.XmlSchemaMaxInclusiveFacet'">,maxValue:<xsl:value-of select="Value"/></xsl:when>
        
      </xsl:choose>
    </xsl:for-each>
    <xsl:if test="TypeCode='Integer' and not(Restrictions/Restriction[Name='System.Xml.Schema.XmlSchemaFractionDigitsFacet'])">
      ,decimalPrecision:0
      ,allowDecimals:false
    </xsl:if>
  </xsl:template>

  <xsl:template match="XbrlDataType|DataType" mode="DateFieldDef">
    {xtype:'datefield',format: 'Y-m-d'
    <xsl:for-each select="Restrictions/Restriction">
      <xsl:choose>
        <xsl:when test="Name='System.Xml.Schema.XmlSchemaMinInclusiveFacet'">,minValue:'<xsl:value-of select="Value"/>'</xsl:when>
        <xsl:when test="Name='System.Xml.Schema.XmlSchemaMaxInclusiveFacet'">,maxValue:'<xsl:value-of select="Value"/>'</xsl:when>
      </xsl:choose>
    </xsl:for-each>
    }
  </xsl:template>

  <xsl:template match="XbrlDataType|DataType" mode="BundleBuilderFieldDef">
    {xtype:'biztax-uploadfield'
    , isRequired:<xsl:value-of select="Name='nonEmptyBase64BinaryItemType'"/>
    }
  </xsl:template>

  <xsl:template name="string-replace-all">
    <xsl:param name="text" />
    <xsl:param name="replace" />
    <xsl:param name="by" />
    <xsl:choose>
      <xsl:when test="contains($text, $replace)">
        <xsl:value-of select="substring-before($text,$replace)" />
        <xsl:value-of select="$by" />
        <xsl:call-template name="string-replace-all">
          <xsl:with-param name="text"
          select="substring-after($text,$replace)" />
          <xsl:with-param name="replace" select="$replace" />
          <xsl:with-param name="by" select="$by" />
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:call-template name="crlf-replace"><xsl:with-param name="subject" select="$text"/></xsl:call-template>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  
  <xsl:template name="crlf-replace">
    <xsl:param name="subject"/>

    <xsl:choose>
      <xsl:when test="contains($subject, $crlf)">
        <xsl:value-of select="substring-before($subject, $crlf)"/><xsl:call-template name="crlf-replace"><xsl:with-param name="subject" select="substring-after($subject, $crlf)"/></xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$subject"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
</xsl:stylesheet>
