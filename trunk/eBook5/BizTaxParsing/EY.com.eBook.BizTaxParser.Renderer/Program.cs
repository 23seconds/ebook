﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using EY.com.eBook.BizTaxParser.Test2;
using System.Xml.Linq;
using System.Runtime.Serialization;
using System.Xml;
using System.Xml.XPath;
using System.Text.RegularExpressions;


namespace EY.com.eBook.BizTaxParser.Renderer
{
    class Program
    {
        private static List<XbrlElementDefinition> definitions;
        private static List<XbrlDataType> datatypes;
        private static Dictionary<string, XElement> valueLists;
        private static XDocument metadata;
        private static Regex reSquares = new Regex("\\[.*\\]", RegexOptions.IgnoreCase);

        static void MainAfterRender(string[] args)
        {
            /*
            FormulaEvaluator fe = new FormulaEvaluator();
            fe.TransformFormula("count(($RemainingFiscalResultBelgium, $RemainingFiscalResultTaxTreaty, $RemainingFiscalResultNoTaxTreaty)) ge 1 and  (($RemainingFiscalResultBelgium ge 0) and ($RemainingFiscalResultTaxTreaty ge 0) and ($RemainingFiscalResultNoTaxTreaty ge 0)) and  not(($RemainingFiscalResultBelgium eq 0) and ($RemainingFiscalResultTaxTreaty eq 0) and ($RemainingFiscalResultNoTaxTreaty eq 0))");
            Console.ReadLine();
             * */
            XElement results = XElement.Load("Result.xml");
            Console.WriteLine("Getting active fields");
            XElement activeFields = new XElement("activeFields");
            foreach (XElement el in results.XPathSelectElements("//Presentation//*[(local-name(.)='Pane' or local-name(.)='Field') and  not(ancestor-or-self::*[@hidden='true'])]"))
            {
                XElement xe = new XElement("element", el.Attribute("xbrlId"), new XAttribute("type",  el.Attribute("itemType") !=null ? GetTpe(el.Attribute("itemType").Value) : ""));
                if (el.Element("Context") != null && el.Element("Context").Element("period") != null)
                {
                    xe.Add(el.Element("Context").Element("period"));
                }
                if (el.Elements("Context").Count() > 1)
                {
                    XElement eldims = new XElement("dimensions");

                    foreach (XElement ctx in el.XPathSelectElements("Context/ContextDefinition/Fields/FieldDefinition/ListData/FieldListItem/ElementDef/Name"))
                    {
                        eldims.Add(new XElement("dimension", new XAttribute("id", ctx.Parent.Parent.Element("Domain").Value.Split(new char[] { '#' })[1]), ctx.Value));
                    }
                    xe.Add(eldims);
                }
                activeFields.Add(xe);
            }
            List<string> itemtypes = activeFields.Elements().Select(e => e.Attribute("itemType") != null ? e.Attribute("itemType").Value : "null").ToList().Distinct().ToList();
            
            XElement assertionexcludes = XElement.Load("AssertionExcludes.xml");
            metadata = XDocument.Load("metaInfo.xml");
            XElement calc = XElement.Load("Calculations.xml");
            valueLists = new Dictionary<string, XElement>();
            Console.WriteLine("Getting element definitions");
            definitions = DeSerializeObject<List<XbrlElementDefinition>>(@"C:\Projects_TFS\EY\Ebook\Main-v5.0\EY.com.eBook.BizTaxParser.Test2\bin\Debug\be-tax-inc-rcorp-2013-04-30-Element_Definitions.bin");

            datatypes = DeSerializeObject<List<XbrlDataType>>(@"C:\Projects_TFS\EY\Ebook\Main-v5.0\EY.com.eBook.BizTaxParser.Test2\bin\Debug\be-tax-inc-rcorp-2013-04-30-Data_Types.bin");

            
            XDocument xdoc = XDocument.Load(@"C:\Projects_TFS\EY\Ebook\Main-v5.0\EY.com.eBook.BizTaxParser.Test2\bin\Debug\be-tax-inc-rcorp-2013-04-30.xsd-assertions.xml");
            XElement elem = new XElement("AllAssertions");
            XElement elparams = new XElement("parameters");
            foreach (XElement el in xdoc.Root.Elements())
            {
                string path = el.Attribute("href").Value;
                string fid = System.IO.Path.GetFileNameWithoutExtension(path);
                XElement excl = assertionexcludes.XPathSelectElement(string.Format("//exclude[@id='{0}']",fid));
                if (excl == null)
                {
                    AssertionReader ar = new AssertionReader();
                    XElement test = ar.Read(path, definitions, activeFields);
                    if (ar.parameters.HasElements)
                    {
                        elparams.Add(ar.parameters.Elements());
                    }
                    if (ar.include)
                    {
                        elem.Add(test);
                    }
                }
            }
            elem.Add(elparams);
            elem.Save("test-assertions.xml");
          
        }

        static string GetTpe(string it)
        {
            if(string.IsNullOrEmpty(it)) return "";
            switch (it)
            {
                case "stringItemType":
                    return "string";
                case "dateItemType":
                    return "date";
                case "monetary14D2ItemType":
                case "nonNegativeMonetary14D2ItemType":
                case "nonPositiveMonetary14D2ItemType":
                case "nonNegativeDecimal6D2ItemType":
                    return "number";
                case "integerItemType":
                    return "int";
                case "booleanItemType":
                    return "bool";
                case "base64BinaryItemType":
                case "nonEmptyBase64BinaryItemType":
                    return "binary";
            }
            return "string";
        }

        static void Assertions()
        {

        }

        static void Main(string[] args)
        {
            MainRender(args);
        }

        static void ReadAssertionFile(string path)
        {

        }

        static void MainRender(string[] args)
        {
            

            metadata = XDocument.Load("metaInfo.xml");
            XElement calc = XElement.Load("Calculations.xml");
            valueLists = new Dictionary<string, XElement>();
            Console.WriteLine("Getting element definitions");
            definitions = DeSerializeObject<List<XbrlElementDefinition>>(@"C:\Projects_TFS\EY\Ebook\Main-v5.0\EY.com.eBook.BizTaxParser.Test2\bin\Debug\be-tax-inc-rcorp-2013-04-30-Element_Definitions.bin");

            datatypes = DeSerializeObject<List<XbrlDataType>>(@"C:\Projects_TFS\EY\Ebook\Main-v5.0\EY.com.eBook.BizTaxParser.Test2\bin\Debug\be-tax-inc-rcorp-2013-04-30-Data_Types.bin");

            Console.WriteLine("Create presentation file");

            
            // Load presentation file
            XDocument presResult = new XDocument();
            XElement presRoot = new XElement("root");
            XElement presResRoot = new XElement("Presentation");
            XDocument presDoc = XDocument.Load(@"C:\Projects_TFS\EY\Ebook\Main-v5.0\EY.com.eBook.BizTaxParser.Test2\bin\Debug\be-tax-inc-rcorp-2013-04-30.xsd-presentation.xml");
            XElement prevTab = null;
            foreach (XElement tabEl in presDoc.Root.Element("Tabs").Elements("Element"))
            {
                XElement tab = new XElement("Tab");
                
                string contextRole = string.Empty;
                if (tabEl.Element("labels").HasElements)
                {
                    SetDefaultAttribs(ref tab,tabEl);
                    tab.Add(tabEl.Element("labels"));
                    contextRole = AddContext(ref tab, tabEl);
                    ProcessChildren(ref tab, tabEl, contextRole);
                }
                else
                {
                    SetDefaultAttribs(ref tab, tabEl.Element("Element"));
                    tab.Add(tabEl.Element("Element").Element("labels"));
                    contextRole = AddContext(ref tab, tabEl.Element("Element"));
                    ProcessChildren(ref tab, tabEl.Element("Element"), contextRole);
                }

                if (prevTab != null && prevTab.Attribute("xbrlId").Value == tab.Attribute("xbrlId").Value)
                {
                    prevTab.Add(tab.Elements("Pane"));
                }
                else
                {
                    presResRoot.Add(tab);
                }
                prevTab = tab;
            }

            presRoot.Add(presResRoot);
            Console.WriteLine("Add value lists");
            XElement vlEls = new XElement("ValueLists");
            foreach (XElement el in valueLists.Values)
            {
                vlEls.Add(el);
            }
            presRoot.Add(vlEls);

            Console.WriteLine("Add datatypes");
            presRoot.Add(ToXml(datatypes, true));

            Console.WriteLine("Add Meta");
            presRoot.Add(new XElement("MetaData", metadata.Root.Elements()));

            Console.WriteLine("Add Calculations");
            presRoot.Add(calc);

            presResult.Add(presRoot);

            presResult.Save("Result.xml");

            
            /*
            foreach (XbrlElementDefinition xed in definitions)
            {
                Console.WriteLine(xed.Href);
            }
             * */
        }

        private static void ProcessChildren(ref XElement tab, XElement sourceEl, string parentContextRole)
        {
            if (tab.Name.LocalName == "ValueList")
            {
                if (!valueLists.ContainsKey(tab.Attribute("xbrlId").Value))
                {
                    XElement lst = new XElement("List",new XAttribute("id",tab.Attribute("xbrlId").Value));
                    foreach (XElement listChildEl in sourceEl.Elements("Element"))
                    {
                        XElement listItem = new XElement("ListItem");
                        SetDefaultAttribs(ref listItem, listChildEl);
                        string href = listChildEl.Attribute("href").Value;
                        XbrlElementDefinition xed = definitions.FirstOrDefault(x => x.Href == href);
                        listItem.Add(new XAttribute("Fixed", xed.Fixed));
                        listItem.Add(listChildEl.Element("labels"));
                        lst.Add(listItem);
                    }
                    valueLists.Add(tab.Attribute("xbrlId").Value, lst);
                }
            }
            else
            {

                foreach (XElement childEl in sourceEl.Elements("Element"))
                {
                    string type = childEl.Elements("Element").Count() > 0 ? "Pane" : "Field";
                    if (childEl.Attribute("id").Value.EndsWith("CodeHead")) type = "ValueList";
                    if (tab.Name.LocalName == "ValueList") type = "ListItem";
                    XElement nwEl = new XElement(type);
                    string contextRole = string.Empty;

                    SetDefaultAttribs(ref nwEl, childEl);
                    nwEl.Add(childEl.Element("labels"));
                    tab.Add(nwEl);
                    contextRole = AddContext(ref nwEl, childEl, parentContextRole);
                    nwEl.Add(new XAttribute("multipleContexts", (nwEl.Elements("Context").Count() > 1)));

                    ProcessChildren(ref nwEl, childEl, contextRole);


                }
                if (tab.Name.LocalName == "Pane")
                {
                    string paneType = "normal";
                    if (tab.Elements("Field").Where(f => f.Element("Context") != null).Count() > 0)
                    {
                        int distinctRoles = tab.Elements("Field").Where(f => f.Element("Context") != null).Select(f => f.Element("Context").Attribute("role").Value).Distinct().Count();
                        if (distinctRoles == 1)
                        {
                            string parentPaneType = string.Empty;
                            if (tab.Parent.Attribute("paneType") != null)
                            {
                                parentPaneType = tab.Parent.Attribute("paneType").Value;
                            }
                            if (tab.Elements("Field").Count(f => f.Attribute("multipleContexts").Value == "true") > 0 && parentPaneType!="pivot")
                            {
                                
                                paneType = "pivot";

                                List<string> fieldDefs = tab.XPathSelectElements(".//Context/ContextDefinition/Fields/*").Select(f => f.ToString()).Distinct().ToList();
                                XElement pivotHeaders = new XElement("PivotHeaders");
                                foreach (string fdef in fieldDefs)
                                {
                                    pivotHeaders.Add(XElement.Parse(fdef));
                                }
                                tab.Add(pivotHeaders);

                            }
                            else
                            {
                                /*
                                 List<XElement> fieldEls = tab.Elements("Field").Where(f => f.Element("Context") != null
                                     && f.Element("Context").Element("ContextDefinition") != null
                                     && f.Element("Context").Element("ContextDefinition").Element("Fields") != null
                                     && f.Element("Context").Element("ContextDefinition").Element("Fields").HasElements).ToList();
                                 if (fieldEls.Count > 0)*/
                                if (tab.XPathSelectElements("Field/Context/ContextDefinition/Fields[count(*)>0]").Count() > 0)
                                {
                                    //List<XElement> fieldDefs = tab.XPathSelectElements("Field/Context/ContextDefinition/Fields/*").Distinct().ToList();
                                    List<XElement> fieldDefs = tab.XPathSelectElements("Field/Context/ContextDefinition/Fields/*").Select(x => x.ToString()).Distinct().Select(x => XElement.Parse(x)).ToList();

                                    //XElement ctxDef = new XElement("ElementContextDef", new XAttribute("role", fieldDefs.First().Parent.Parent.Element("Role").Value));
                                    XElement ctxDef = new XElement("ElementContextDef", new XAttribute("role", tab.XPathSelectElement("Field/Context[1]/ContextDefinition/Role").Value));
                                    foreach (XElement fieldEl in fieldDefs)
                                    {
                                        string labelHref = fieldEl.Element("Dimension").Value;
                                        XbrlElementDefinition xed = definitions.FirstOrDefault(f=>f.Href==labelHref);
                                        XElement fex = XElement.Parse(fieldEl.ToString());
                                        foreach (ElementLabel lbl in xed.Labels)
                                        {
                                            foreach (Translation tr in lbl.Descriptions)
                                            {
                                                tr.Description = reSquares.Replace(tr.Description, "").Trim();
                                            }
                                        }
                                        fex.Add(ToXml(xed.Labels,true));
                                        
                                        ctxDef.Add(fex);
                                    }
                                    tab.Add(ctxDef);
                                    paneType = "contextGrid";
                                }
                            }
                        }
                    }
                    if (tab.Elements("ValueList").Count() == 1 && (tab.Elements("Field").Count() == 1 || tab.Elements("Pane").Count() == 1))
                    {
                        paneType = "ListOrOther";
                    }
                    tab.Add(new XAttribute("paneType", paneType));
                }
            }
        }

        private static string AddContext(ref XElement tab, XElement source)
        {
            return AddContext(ref tab, source, null);
        }

        private static string AddContext(ref XElement tab, XElement source, string parentCtx)
        {
            string ctx = string.Empty;
            string href = source.Attribute("href").Value;
            XbrlElementDefinition xed = definitions.FirstOrDefault(x => x.Href == href);
            if (xed == null || xed.PeriodType==null)
            {
                tab.Add(new XElement("Context", new XAttribute("role", string.Empty)));
                return ctx;

            }
            
            XElement period = new XElement("period", new XAttribute("type", xed.PeriodType));
            //if (xed.PeriodType.ToLower() == "instant")
            //{
                if (xed.Labels.Count(l => l.Role == "periodStartLabel") > 0) period.Add(new XAttribute("start", "true"));
                if (xed.Labels.Count(l => l.Role == "periodEndLabel") > 0) period.Add(new XAttribute("end", "true"));
            //}

            if (xed.Abstract || xed.ContextDefinitions == null || xed.ContextDefinitions.Count == 0)
            {
                if (xed.Abstract && tab.PreviousNode!=null)
                {
                    XElement prevElement = ((XElement)tab.PreviousNode);
                    foreach (XElement parCtx in prevElement.Elements("Context"))
                    {
                        tab.Add(parCtx);
                        ctx = parCtx.Attribute("role").Value;
                    }
                    return ctx;
                }
                else
                {
                    tab.Add(new XElement("Context", new XAttribute("role", string.Empty), period));
                    return ctx;
                }
            }

            ContextDefinition cd=null;
            if (parentCtx == null)
            {
                cd = xed.ContextDefinitions.First();
            }
            else
            {
                if (tab.PreviousNode != null)
                {
                    XElement prevEl = (XElement)tab.PreviousNode;
                    if (prevEl.Element("Context") != null)
                    {
                        string rle = prevEl.Element("Context").Attribute("role").Value;
                        cd = xed.ContextDefinitions.FirstOrDefault(c => c.Role == rle);
                    }
                }
                if (cd == null)
                {
                    cd = xed.ContextDefinitions.FirstOrDefault(c => c.Role == parentCtx);
                    if (cd == null)
                    {
                        cd = xed.ContextDefinitions.First();
                    }
                }
            }
            ctx = cd.Role;

            if (xed.ContextDefinitions.Count(c => c.Role == cd.Role && cd.Fields!=null && cd.Fields.Count>0 && cd.Fields.First().Fixed) > 1)
            {
                foreach (ContextDefinition cdef in xed.ContextDefinitions.Where(c => c.Role == cd.Role))
                {
                    // add each
                    XElement ctxEl = new XElement("Context", new XAttribute("role", ctx), period);
                    ctxEl.Add(ToXml(cdef, true));
                    tab.Add(ctxEl);
                }
            }
            else
            {
                XElement ctxEl = new XElement("Context", new XAttribute("role", ctx), period);
                ctxEl.Add(ToXml(cd, true));
                tab.Add(ctxEl);
            }
            
            
            //if(
            //if (xed.ContextDefinitions == null || xed.ContextDefinitions.Count == 0)
            //{
                
            //}
            //if (parentCtx == null)
            //{

            //}

            
            return ctx;
        }

        public static XElement ToXml(object instance, bool removeNs)
        {
            string result = null;
            DataContractSerializer dcs = new DataContractSerializer(instance.GetType());
            var sw = new System.IO.StringWriter();
            var xw = new XmlTextWriter(sw);
            dcs.WriteObject(xw, instance);
            xw.Close();
            result = sw.ToString();
            sw.Close();
            XElement xe = XElement.Parse(result);
            if (removeNs) xe = xe.StripNamespaces();
            return xe;
        }

        private static void SetDefaultAttribs(ref XElement tab, XElement source)
        {
            bool hidden = metadata.Root.XPathSelectElement(string.Format("//hide/item[@id='{0}']", source.Attribute("id").Value)) != null;
            tab.Add(new XAttribute("hidden", hidden));

            tab.Add(new XAttribute("xbrlId", source.Attribute("id").Value));
            tab.Add(new XAttribute("xbrlName", source.Attribute("name").Value));
            string href = source.Attribute("href").Value;
            XbrlElementDefinition xed = definitions.FirstOrDefault(x => x.Href == href);

            if (source.Attribute("type") != null)
            {
                if (!string.IsNullOrEmpty(source.Attribute("type").Value))
                {
                    string[] tpe = source.Attribute("type").Value.Split(new char[] { ':' });
                    tab.Add(new XAttribute("itemType", tpe[tpe.Length-1]));
                }
            }
            //tab.Add(new XAttribute("itemType", source.Attribute("type") != null ? source.Attribute("type").Value : string.Empty));
            tab.Add(new XAttribute("allowNull", source.Attribute("nillable") != null ? source.Attribute("nillable").Value.ToLower() != "false" : true));
            tab.Add(new XAttribute("isAbstract", source.Attribute("abstract") != null ? source.Attribute("abstract").Value.ToLower() == "true" : false));
            tab.Add(new XAttribute("order", source.Attribute("order") != null ? source.Attribute("order").Value : string.Empty));
            tab.Add(new XAttribute("preferredLabel", source.Attribute("preferredLabel") != null ? source.Attribute("preferredLabel").Value : "label"));
          //  tab.Add(new XAttribute("decimals",xed.de

        }

        public static void SerializeObject<T>(string filename, T objectToSerialize)
        {
            Stream stream = File.Open(filename, FileMode.Create);
            BinaryFormatter bFormatter = new BinaryFormatter();
            bFormatter.Serialize(stream, objectToSerialize);
            stream.Close();
        }

        public static T DeSerializeObject<T>(string filename)
        {
            T objectToSerialize;
            Stream stream = File.Open(filename, FileMode.Open);
            BinaryFormatter bFormatter = new BinaryFormatter();
            objectToSerialize = (T)bFormatter.Deserialize(stream);
            stream.Close();
            return objectToSerialize;
        }

        /*
        public static void CreateDefaultBizTaxDataContract()
        {
            BizTaxDataContract btdc = new BizTaxDataContract
            {
                AssessmentYear = ""
                ,
                Contexts = new List<ContextElementDataContract>()
                ,
                Elements = new List<XbrlElementDataContract>()
                ,
                IdentifierValue = string.Empty
                ,
                Units = new List<UnitElementDataContract>
                {
                    new UnitElementDataContract { Measure="pure",Id="U-Pure"}
                    ,new UnitElementDataContract { Measure="iso4217:EUR",Id="EUR"}
                }
            };
        }
         * */
    }
}
