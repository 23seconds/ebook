﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl"
>
    <xsl:output method="xml" indent="no"/>

    <xsl:template match="AllAssertions">
      <assertions>
      <xsl:copy-of select="parameters"/>
      <xsl:apply-templates select="containers[container[@test]]"/>
      </assertions>
    </xsl:template>

  <xsl:template match="containers">
    <assertionSet id="{@id}">
      <xsl:for-each select="container">
        <validation test="{@test}">
          <xsl:if test="@multiperiod='true'">
            <xsl:attribute name="foreachPeriod">true</xsl:attribute>
          </xsl:if>
          <xsl:if test="@multidimensional='true'">
            <xsl:attribute name="foreachDimension">true</xsl:attribute>
          </xsl:if>
          <xsl:copy-of select="periods"/>
          <xsl:copy-of select="dimensions"/>
          <variables>
            <xsl:apply-templates select="valueAssertion">

            </xsl:apply-templates>
          </variables>
          <xsl:if test="Precondition">
            <xsl:copy-of select="Precondition"/>
          </xsl:if>
          <xsl:copy-of select="formula"/>
          <xsl:copy-of select="labels"/>
        </validation>
        
      </xsl:for-each>

     
    </assertionSet>
    
  </xsl:template>

  <xsl:template match="valueAssertion">
    <xsl:apply-templates select="variables/variable"></xsl:apply-templates>
    
  </xsl:template>

  <xsl:template match="variable">
    <xsl:choose>
      <xsl:when test="@name='PeriodStartDate'">
        <periodStartDate name="{@name}"/>
      </xsl:when>
      <xsl:when test="@name='PeriodEndDate'">
        <periodEndDate name="{@name}"/>
      </xsl:when>
      <xsl:when test="Concept">
        <variable name="{@name}" fallBackValue="{@fallBackValue}" sumOfAllFound="{@sumOfAllFound}">
          <xsl:for-each select="*//ConceptLink">
            <conceptLink>
          <xsl:attribute name="period">
            <xsl:value-of select="../../@xbrlPeriod"/>
          </xsl:attribute>
          <xsl:attribute name="prefix">
            <xsl:value-of select="@xbrlPrefix"/>
          </xsl:attribute>
          <xsl:attribute name="name">
            <xsl:value-of select="@xbrlName"/>
          </xsl:attribute>   
          <xsl:attribute name="type">
            <xsl:value-of select="@type"/>
          </xsl:attribute>
              <xsl:if test="../../explicitDimension">
                <xsl:copy-of select="../../explicitDimension"/>
              </xsl:if>
            </conceptLink>
          </xsl:for-each>
        </variable>
      </xsl:when>
      <xsl:when test="formula">
        <variable name="{@name}" fallBackValue="{@fallBackValue}">
          <xsl:if test="formula">
            <xsl:copy-of select="formula"/>
          </xsl:if>
        </variable>
      </xsl:when>
      <xsl:otherwise>
        <useParameter name="{@name}" fallBackValue="{@fallBackValue}"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  
</xsl:stylesheet>
