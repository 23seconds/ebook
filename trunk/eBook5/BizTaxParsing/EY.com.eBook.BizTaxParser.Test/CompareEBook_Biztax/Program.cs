﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.IO;
using System.Xml.XPath;
using System.Xml;

namespace CompareEBook_Biztax
{
    class Program
    {



        private static char[] alfabet = new char[] { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z' };
        private static int alfIdx = 0;

        static void Main(string[] args)
        {
            string folder = @"Z:\TAX CENTRAL PROCESSING\BizTax\AANSLAGJAAR_2013\3_BizTax Out\Success\HERWIG.JOOSTEN\ABBOTTVASCULARINTERNATIONAL\31-12-2012";
            string pathXbrlAangifte = Path.Combine(folder,"XBRL_van_de_aangifte_03_09_2013.xbrl");
            string pathXbrleBook = Path.Combine(folder, "EqualityCheckbiztax.biztax");

            

            XDocument xbrlAangifte = XDocument.Load(pathXbrlAangifte);
            XDocument xbrleBookBT = XDocument.Load(pathXbrleBook);
            XDocument xbrleBook = XDocument.Parse(xbrleBookBT.Root.Elements().First().ToString());

            XmlNamespaceManager namespaces = new XmlNamespaceManager(new NameTable());
            XNamespace ns = xbrlAangifte.Root.GetDefaultNamespace();
           // namespaces.AddNamespace( ns.NamespaceName);

            foreach (XElement actx in xbrlAangifte.Root.Elements())
            {
                CompareElements(actx, xbrleBook.Root, "", ref namespaces);
            }


            

            

            Console.WriteLine("done.");
            Console.ReadLine();

        }

        private static void CompareElements(XElement actx, XElement xeBookParent, string path, ref XmlNamespaceManager namespaces)
        {
            string prefix = namespaces.LookupPrefix(actx.Name.NamespaceName);
            if (string.IsNullOrEmpty(prefix))
            {
                prefix = alfabet[alfIdx].ToString();
                alfIdx++;
                namespaces.AddNamespace(prefix, actx.Name.NamespaceName);
            }

            string xpathSql = prefix + ":" + actx.Name.LocalName;
            XElement ectx = null;
            if (actx.Attribute("id") != null)
            {
                xpathSql += "[@id='" + actx.Attribute("id").Value + "']";
            }
            else if (actx.Attribute("contextRef")!=null)
            {
                xpathSql += "[@contextRef='" + actx.Attribute("contextRef").Value + "']";
            }
            else if (actx.Attribute("dimension") != null)
            {
                xpathSql += "[@dimension='" + actx.Attribute("dimension").Value + "']";
            }
            else
            {
            }
            ectx = xeBookParent.XPathSelectElement(xpathSql, namespaces);

            if (ectx == null)
            {
                Console.WriteLine("Element NOT found: '{0}'", xpathSql);
            }
            else
            {
                if (ectx.Value != actx.Value && !actx.HasElements && actx.Value.Length < 1000) 
                {
                    Console.WriteLine("Element value NOT equal'{0}' :: {1} vs {2}", xpathSql,actx.Value,ectx.Value);
                }
                foreach (XAttribute att in actx.Attributes())
                {
                   /* string attprefix = namespaces.LookupPrefix(att.Name.NamespaceName);
                    if (string.IsNullOrEmpty(attprefix))
                    {
                        attprefix = alfabet[alfIdx].ToString();
                        alfIdx++;
                        namespaces.AddNamespace(attprefix, att.Name.NamespaceName);
                    }*/

                    XAttribute ett = ectx.Attribute(att.Name);
                    if (ett == null)
                    {
                        Console.WriteLine("Attribute {0} not found in element {1}", att.Name.LocalName, xpathSql);
                    }
                    else
                    {
                        if (ett.Value != att.Value)
                        {
                            Console.WriteLine("Element attribute {3} value NOT equal'{0}' :: {1} vs {2}", xpathSql, ett.Value, att.Value,att.Name.LocalName);
                        }
                    }
                }

                if (actx.HasElements)
                {
                    foreach (XElement actxChild in actx.Elements())
                    {
                        CompareElements(actxChild, ectx, "", ref namespaces);
                    }
                }
            }
            

           // = xeBookParent.XPathSelectElement("//" + prefix + ":context[@id='" + actx.Attribute("id").Value + "']", namespaces);

        }


    }
}
