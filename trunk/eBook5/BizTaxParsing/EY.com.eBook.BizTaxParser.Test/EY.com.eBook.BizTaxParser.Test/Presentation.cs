﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace EY.com.eBook.BizTaxParser.Test
{
    public class Presentation
    {
        
        private XDocument _doc;
        private Dictionary<string, XNamespace> namespaces;

        public List<Reference> References { get; set; }
        public List<PresentationLink> PresentationLinks { get; set; }
        //public List<PresentationArc> PresentationArcs { get; set; }
        //public List<Locator> Locators { get; set; }

        public Presentation(string fname)
        {
            References = new List<Reference>();
            PresentationLinks = new List<PresentationLink>();
           // Locators = new List<Locator>();
            _doc = XDocument.Load(fname);

            namespaces = _doc.Root.Attributes().
                            Where(a => a.IsNamespaceDeclaration).
                            GroupBy(a => a.Name.Namespace == XNamespace.None ? String.Empty : a.Name.LocalName,
                                    a => XNamespace.Get(a.Value)).
                            ToDictionary(g => g.Key,
                                         g => g.First());

            Travers(_doc.Root.Descendants());

        }

        private void Travers(IEnumerable<XElement> elements)
        {
            foreach (XElement element in elements)
            {
                switch (element.Name.LocalName)
                {
                    case "roleRef":
                        References.Add(Reference.CreateFromElement(element, namespaces));
                        break;
                    case "presentationLink":
                        PresentationLinks.Add(PresentationLink.CreateFromElement(element,namespaces));
                        // children
                        break;
                    //case "presentationArc":
                    //    PresentationLinks.Add(PresentationLink.CreateFromElement(element, namespaces));
                    //    break;
                    //case "loc":
                    //    Locators.Add(Locator.CreateFromElement(element, namespaces));
                    //    break;
                }
                //if (element.HasElements) Travers(element.Descendants());
            }
        }

        public XElement GenerateXml(LabelList labels,List<XsdElement> elements)
        {
            XElement root = new XElement("root");
            foreach (PresentationLink pl in PresentationLinks)
            {
                root.Add(pl.GenerateXml(labels, elements));
            }
           // root.Save(string.Format("{0}.xml", Guid.NewGuid()));
            return root;
        }

    }
}
