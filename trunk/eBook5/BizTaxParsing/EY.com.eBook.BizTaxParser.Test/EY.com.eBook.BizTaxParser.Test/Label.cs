﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Xml;

namespace EY.com.eBook.BizTaxParser.Test
{
    public class Label
    {

        /*
         * <label xlink:type="resource"
      xlink:label="tax-inc_DetailDisallowedExpensesNonMandatoryAnnexe_lbl"
      xml:lang="de"
      xlink:role="http://www.xbrl.org/2003/role/label">Fakultative Anlage</label>
         */

        public static Label CreateFromElement(XElement element, Dictionary<string, XNamespace> namespaces)
        {
            Label lbl = new Label();
            lbl.Type = element.Attribute(namespaces["xlink"] + "type") != null ? element.Attribute(namespaces["xlink"] + "type").Value : null;
            lbl.Id = element.Attribute(namespaces["xlink"] + "label") != null ? element.Attribute(namespaces["xlink"] + "label").Value : null;
            lbl.Language = element.Attribute(XNamespace.Xml+"lang") != null ? element.Attribute(XNamespace.Xml+"lang").Value : null;
            lbl.Role = element.Attribute(namespaces["xlink"] + "role") != null ? element.Attribute(namespaces["xlink"] + "role").Value : null;
            lbl.RoleId = lbl.Role.Replace("http://www.xbrl.org/2003/role/", "");
            lbl.Description = element.Value;
            
            return lbl;
        }

        public string Key { get { return string.Format("{0}/{1}", Role,Id); } }
        public string Type { get; set; }
        public string Id { get; set; }
        public string Language { get; set; }
        public string Role { get; set; }
        public string RoleId { get; set; }
        public string Description { get; set; }
    }

}
