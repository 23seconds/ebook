﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;


namespace EY.com.eBook.BizTaxParser.Test
{
    public class Definitions
    {
        public List<DefinitionLink> Links { get; set; }

        public Definitions(string directory)
        {
           // FileName = System.IO.Path.GetFileNameWithoutExtension(fle);
            ReadFiles(directory);
        }

        internal void ReadFiles(string directory)
        {
            //Definitions = new Dictionary<string, Dictionary<string, List<Translation>>>();
            Links = new List<DefinitionLink>();
           

            string[] files = System.IO.Directory.GetFiles(directory, "*definition.xml");
            foreach (string lblFile in files)
            {
                Console.WriteLine("Definition: " + Path.GetFileNameWithoutExtension(lblFile));
                XmlReader reader = new XmlTextReader(lblFile);
                XDocument ldoc = XDocument.Load(lblFile);
                Dictionary<string, XNamespace> mnamespaces = ldoc.Root.Attributes().
                            Where(a => a.IsNamespaceDeclaration).
                            GroupBy(a => a.Name.Namespace == XNamespace.None ? String.Empty : a.Name.LocalName,
                                    a => XNamespace.Get(a.Value)).
                            ToDictionary(g => g.Key,
                                         g => g.First());

                foreach (XElement element in ldoc.XPathSelectElements("//node()[local-name()='definitionLink']", new XmlNamespaceManager(reader.NameTable)))
                {

                    Links.Add(DefinitionLink.CreateFromElement(element, mnamespaces));
                }

            }
        }

       

    }
}
