﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace EY.com.eBook.BizTaxParser.Test
{
    public class XsdElement
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public string SubstitutionGroup { get; set; }
        public string PeriodType { get; set; }
        public bool Abstract { get; set; }
        public bool Nillable { get; set; }

        public string TypedDomainRef { get; set; }
        public string Balance { get; set; }
        public string Ref { get; set; }
        public string MinOccurs { get; set; }
        public string MaxOccurs { get; set; }
        public string Fixed { get; set; }



        public XsdElement(XElement element, Dictionary<string, XNamespace> namespaces)
        {
            Id = element.Attribute("id") != null ? element.Attribute("id").Value : null;
            Name = element.Attribute("name") != null ? element.Attribute("name").Value : null;
            Type = element.Attribute("type") != null ? element.Attribute("type").Value : null;
            SubstitutionGroup= element.Attribute("substitutionGroup") != null ? element.Attribute("substitutionGroup").Value : null;
            PeriodType = element.Attribute(namespaces["xbrli"] + "periodType") != null ? element.Attribute(namespaces["xbrli"] + "periodType").Value : null;
            Abstract = element.Attribute("abstract") != null ? element.Attribute("abstract").Value.ToLower() == "true" : false;
            Nillable = element.Attribute("nillable") != null ? element.Attribute("nillable").Value.ToLower() == "true" : false;

            if(namespaces.ContainsKey("xbrldt")) TypedDomainRef = element.Attribute(namespaces["xbrldt"] + "typedDomainRef") != null ? element.Attribute(namespaces["xbrldt"] + "typedDomainRef").Value : null;
            Balance = element.Attribute(namespaces["xbrli"] + "balance") != null ? element.Attribute(namespaces["xbrli"] + "balance").Value : null;
         
            Ref = element.Attribute("ref") != null ? element.Attribute("ref").Value : null;
            MinOccurs = element.Attribute("minOccurs") != null ? element.Attribute("minOccurs").Value : null;
            MaxOccurs = element.Attribute("maxOccurs") != null ? element.Attribute("maxOccurs").Value : null;
            Fixed = element.Attribute("fixed") != null ? element.Attribute("fixed").Value : null;
        }
    }
}
