﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Xml.XPath;

namespace EY.com.eBook.BizTaxParser.Test
{
    public class PresentationLink
    {
        /*
         * 
         * <presentationLink xlink:type="extended"
    xlink:role="http://www.minfin.fgov.be/be/tax/inc/rcorp/2011-04-30/role/IdentifyingDataTaxReturn">
         */

        public static new PresentationLink CreateFromElement(XElement element, Dictionary<string, XNamespace> namespaces) 
        {
            return CreateFromElement<PresentationLink>(element,namespaces);
        }

        public static new T CreateFromElement<T>(XElement element, Dictionary<string, XNamespace> namespaces) where T:PresentationLink, new()
        {

            T pl = new T();
            pl.Type = element.Attribute(namespaces["xlink"] + "type") != null ? element.Attribute(namespaces["xlink"] + "type").Value : null;
            pl.Role = element.Attribute(namespaces["xlink"] + "role") != null ? element.Attribute(namespaces["xlink"] + "role").Value : null;
            pl.Locators = new List<Locator>();
            pl.PresentationArcs = new List<PresentationArc>();
            string[] roleSplit = pl.Role.Split(new char[] { '/' });
            pl.Id = roleSplit[roleSplit.Length - 1];
            if(element.HasElements)  pl.Travers(element.Elements(),namespaces);

            return pl;
        }

        private void Travers(IEnumerable<XElement> elements, Dictionary<string, XNamespace> namespaces)
        {
            foreach (XElement element in elements)
            {
                switch (element.Name.LocalName)
                {
                    case "presentationArc":
                        PresentationArcs.Add(PresentationArc.CreateFromElement(element, namespaces));
                        break;
                    case "loc":
                        Locators.Add(Locator.CreateFromElement(element, namespaces));
                        break;
                    default:
                        Console.WriteLine(element.Name.LocalName + " " + element.Name.NamespaceName);
                        break;
                }
                if (element.HasElements) Travers(element.Descendants(),namespaces);
            }
        }

        public List<string> GetLinkRoots()
        {
            List<string> tos = PresentationArcs.Select(p=>p.To).Distinct().ToList();
            return PresentationArcs.Where(p => !tos.Contains(p.From)).Select(p=>p.From).ToList();
        }

        public XElement GenerateXml(LabelList labels,List<XsdElement> elements)
        {
            XElement root = new XElement(Id);
            //foreach (Locator loc in Locators)
            //{
            //    XElement el = new XElement("Element",new XAttribute("id", loc.ItemId));
            //    Dictionary<string, List<Translation>> translations = labels.GetTranslations(loc.ItemId);
            //    if (translations != null)
            //    {
            //        XElement transel = new XElement("Translations");
            //        foreach (string role in translations.Keys)
            //        {
            //            XElement transrole = new XElement("Role", new XAttribute("id", role));
            //            foreach (Translation t in translations[role])
            //            {
            //                transrole.Add(new XElement(t.Culture, t.Description));
            //            }
            //            transel.Add(transrole);
            //        }
            //        el.Add(transel);
            //    }
            //    XsdElement xel = elements.FirstOrDefault(x => x.Id == loc.ItemId);
            //    if (xel != null)
            //    {
            //        el.Add(new XAttribute("abstract", xel.Abstract));
            //        el.Add(new XAttribute("nillable", xel.Nillable));
            //        el.Add(new XAttribute("name", xel.Name));
            //        el.Add(new XAttribute("periodType", xel.PeriodType == null ? "" : xel.PeriodType));
            //        el.Add(new XAttribute("substitutionGroup", xel.SubstitutionGroup == null ? "" : xel.SubstitutionGroup));
            //        el.Add(new XAttribute("type", xel.Type == null ? "" : xel.Type));

            //        el.Add(new XAttribute("typedDomainRef", xel.TypedDomainRef == null ? "" : xel.TypedDomainRef));
            //        el.Add(new XAttribute("balance", xel.Balance == null ? "" : xel.Balance));
            //        el.Add(new XAttribute("ref", xel.Ref == null ? "" : xel.Ref));
            //        el.Add(new XAttribute("minOccurs", xel.MinOccurs == null ? "" : xel.MinOccurs));
            //        el.Add(new XAttribute("maxOccurs", xel.MaxOccurs == null ? "" : xel.MaxOccurs));
            //        el.Add(new XAttribute("fixed", xel.Fixed == null ? "" : xel.Fixed));


            //    }
            //    root.Add(el);
            //}
            foreach (PresentationArc arc in PresentationArcs)
            {
                XElement parent = root.XPathSelectElement(string.Format("//Element[@id='{0}']" ,arc.From));
                XElement child = root.XPathSelectElement(string.Format("//Element[@id='{0}']" ,arc.To));
                if (parent == null)
                {
                    parent = CreateElement(labels, elements, arc.From);
                    root.Add(parent);
                }
                if (child == null)
                {
                    child = CreateElement(labels, elements, arc.To);
                }
                if (child != null && parent != null)
                {
                   // if(child.Parent!=null && child.Parent.Name.LocalName=="root") child.Remove();
                    parent.Add(child);
                }
            }
           // root.Save(string.Format("{0}.xml",  Id));
            if (root.Elements().Count() == 1) return root.Elements().First();
            return root;
            
        }

        private XElement CreateElement(LabelList labels, List<XsdElement> elements,string id)
        {
            XsdElement xel = elements.FirstOrDefault(e => e.Id == id);
            if (xel != null)
            {
                XElement el = new XElement("Element", new XAttribute("id", xel.Id));
                Dictionary<string, List<Translation>> translations = labels.GetTranslations(xel.Id);
                if (translations != null)
                {
                    XElement transel = new XElement("Translations");
                    foreach (string role in translations.Keys)
                    {
                        XElement transrole = new XElement("Role", new XAttribute("id", role));
                        foreach (Translation t in translations[role])
                        {
                            transrole.Add(new XElement(t.Culture, t.Description));
                        }
                        transel.Add(transrole);
                    }
                    el.Add(transel);
                }

                el.Add(new XAttribute("abstract", xel.Abstract));
                el.Add(new XAttribute("nillable", xel.Nillable));
                el.Add(new XAttribute("name", xel.Name));
                el.Add(new XAttribute("periodType", xel.PeriodType == null ? "" : xel.PeriodType));
                el.Add(new XAttribute("substitutionGroup", xel.SubstitutionGroup == null ? "" : xel.SubstitutionGroup));
                el.Add(new XAttribute("type", xel.Type == null ? "" : xel.Type));

                el.Add(new XAttribute("typedDomainRef", xel.TypedDomainRef == null ? "" : xel.TypedDomainRef));
                el.Add(new XAttribute("balance", xel.Balance == null ? "" : xel.Balance));
                el.Add(new XAttribute("ref", xel.Ref == null ? "" : xel.Ref));
                el.Add(new XAttribute("minOccurs", xel.MinOccurs == null ? "" : xel.MinOccurs));
                el.Add(new XAttribute("maxOccurs", xel.MaxOccurs == null ? "" : xel.MaxOccurs));
                el.Add(new XAttribute("fixed", xel.Fixed == null ? "" : xel.Fixed));

                return el;
            }
            return null;
        }

        public List<Locator> Locators { get; set; }

        public List<PresentationArc> PresentationArcs { get; set; }


        public string Type { get; set; }

        public string Role { get; set; }

        public string Id { get; set; }
    }
}
