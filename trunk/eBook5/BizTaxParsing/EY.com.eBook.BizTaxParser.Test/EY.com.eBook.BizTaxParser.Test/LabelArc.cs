﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace EY.com.eBook.BizTaxParser.Test
{
    public class LabelArc : Arc
    {
        public static LabelArc CreateFromElement(XElement element, Dictionary<string, XNamespace> namespaces)
        {
            return CreateFromElement<LabelArc>(element, namespaces);
        }

        public static T CreateFromElement<T>(XElement element, Dictionary<string, XNamespace> namespaces) where T : LabelArc, new()
        {
            return Arc.CreateFromElement<T>(element, namespaces);
        }
    }
}
