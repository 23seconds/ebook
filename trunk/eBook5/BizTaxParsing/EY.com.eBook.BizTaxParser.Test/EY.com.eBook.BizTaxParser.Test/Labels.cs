﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Xml;
using System.IO;

namespace EY.com.eBook.BizTaxParser.Test
{
    public class LabelList
    {

        public Dictionary<string, Dictionary<string, List<Translation>>> Labels { get; set; }
        public List<LabelArc> Arcs {get;set;}
        public List<Locator> Locators { get; set; }

        public LabelList(string directory)
        {
           // FileName = System.IO.Path.GetFileNameWithoutExtension(fle);
            ReadFiles(directory);
        }

        internal void ReadFiles(string directory)
        {
            Labels = new Dictionary<string, Dictionary<string, List<Translation>>>();
            Arcs = new List<LabelArc>();
            Locators = new List<Locator>();

            string[] files = System.IO.Directory.GetFiles(directory, "*label.xml");
            foreach (string lblFile in files)
            {
                Console.WriteLine("Label: " + Path.GetFileNameWithoutExtension(lblFile));
                XmlReader reader = new XmlTextReader(lblFile);
                XDocument ldoc = XDocument.Load(lblFile);
                Dictionary<string, XNamespace> mnamespaces = ldoc.Root.Attributes().
                            Where(a => a.IsNamespaceDeclaration).
                            GroupBy(a => a.Name.Namespace == XNamespace.None ? String.Empty : a.Name.LocalName,
                                    a => XNamespace.Get(a.Value)).
                            ToDictionary(g => g.Key,
                                         g => g.First());

                foreach (XElement element in ldoc.XPathSelectElements("//node()[local-name()='labelArc']", new XmlNamespaceManager(reader.NameTable)))
                {
                    Arcs.Add(LabelArc.CreateFromElement(element, mnamespaces));
                }

                foreach (XElement element in ldoc.XPathSelectElements("//node()[local-name()='loc']", new XmlNamespaceManager(reader.NameTable)))
                {

                    Locators.Add(Locator.CreateFromElement(element, mnamespaces));
                }

                foreach (XElement element in ldoc.XPathSelectElements("//node()[local-name()='label']", new XmlNamespaceManager(reader.NameTable)))
                {

                    Label lbl = Label.CreateFromElement(element,mnamespaces);
                    if (!Labels.ContainsKey(lbl.Id)) Labels.Add(lbl.Id, new Dictionary<string, List<Translation>>());
                    if (!Labels[lbl.Id].ContainsKey(lbl.RoleId)) Labels[lbl.Id].Add(lbl.RoleId,new List<Translation>());


                    if (Labels[lbl.Id][lbl.RoleId].Count(t => t.Culture == lbl.Language) == 0)
                    {
                        Labels[lbl.Id][lbl.RoleId].Add(new Translation { Culture = lbl.Language, Description = lbl.Description });
                    }
                }

            }
        }


        public string GetTranslation(string id, string language)
        {

            List<Translation> translations = GetRoleTranslations(id,"label");
            if (translations == null) return id;
            Translation tr = translations.FirstOrDefault(t => t.Culture == language);
            if (tr == null) return id;
            return tr.Description;
        }

        public Dictionary<string, List<Translation>> GetTranslations(string id)
        {
            LabelArc la = Arcs.FirstOrDefault(a => a.From == id);
            if (la == null)
            {
                Locator loc = Locators.FirstOrDefault(l => l.ItemId == id);
                if (loc != null)
                {
                    la = Arcs.FirstOrDefault(a => a.From == loc.Label);
                }
            }
            if (la == null) return null;
            if (!Labels.ContainsKey(la.To)) return null;
            return Labels[la.To];
        }

        public List<Translation> GetRoleTranslations(string id,string role)
        {
            Dictionary<string, List<Translation>> trans = GetTranslations(id);
            if (trans == null) return null;
            if (!trans.ContainsKey(role)) return null;
            return trans[role];
            
        }
    }

}
