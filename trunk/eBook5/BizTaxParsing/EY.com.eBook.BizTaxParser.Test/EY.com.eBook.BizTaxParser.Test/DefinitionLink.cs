﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Xml.XPath;

namespace EY.com.eBook.BizTaxParser.Test
{
    public class DefinitionLink
    {
        public List<DefinitionArc> DefinitionArcs { get; set; }
        public List<Locator> Locators { get; set; }

        public string Type { get; set; }
        public string Role { get; set; }

        public string Id { get; set; }

        public static new DefinitionLink CreateFromElement(XElement element, Dictionary<string, XNamespace> namespaces)
        {
            return CreateFromElement<DefinitionLink>(element, namespaces);
        }

        public static new T CreateFromElement<T>(XElement element, Dictionary<string, XNamespace> namespaces) where T : DefinitionLink, new()
        {

            T pl = new T();
            pl.Type = element.Attribute(namespaces["xlink"] + "type") != null ? element.Attribute(namespaces["xlink"] + "type").Value : null;
            pl.Role = element.Attribute(namespaces["xlink"] + "role") != null ? element.Attribute(namespaces["xlink"] + "role").Value : null;
            pl.Locators = new List<Locator>();
            pl.DefinitionArcs = new List<DefinitionArc>();
            string[] roleSplit = pl.Role.Split(new char[] { '/' });
            pl.Id = roleSplit[roleSplit.Length - 1];
            if (element.HasElements) pl.Travers(element.Elements(), namespaces);

            return pl;
        }

        private void Travers(IEnumerable<XElement> elements, Dictionary<string, XNamespace> namespaces)
        {
            foreach (XElement element in elements)
            {
                switch (element.Name.LocalName)
                {
                    case "definitionArc":
                        DefinitionArcs.Add(DefinitionArc.CreateFromElement(element, namespaces));
                        break;
                    case "loc":
                        Locators.Add(Locator.CreateFromElement(element, namespaces));
                        break;
                    default:
                        Console.WriteLine(element.Name.LocalName + " " + element.Name.NamespaceName);
                        break;
                }
                if (element.HasElements) Travers(element.Descendants(), namespaces);
            }
        }
    }
}
