﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace EY.com.eBook.BizTaxParser.Test
{
    public class PresentationArc : Arc
    {
        /*
         * 
         * xlink:type="arc"
      xlink:arcrole="http://www.xbrl.org/2003/arcrole/parent-child"
      xlink:from="tax-inc_CompanyInformationForm"
      xlink:to="pfs-gcd_EntityInformation"
      order="1"
      use="optional" 
         */

        public static new PresentationArc CreateFromElement(XElement element, Dictionary<string, XNamespace> namespaces) 
        {
            return CreateFromElement<PresentationArc>(element, namespaces);
        }

        public static new T CreateFromElement<T>(XElement element, Dictionary<string, XNamespace> namespaces) where T : PresentationArc, new()
        {
            T pl = Arc.CreateFromElement<T>(element, namespaces);
            int order = 0;
            if (element.Attribute(namespaces[""] + "order") != null)
            {
                int.TryParse(element.Attribute(namespaces[""] + "order").Value, out order);
            }
            pl.Order = order;
            pl.Usage = element.Attribute(namespaces[""] + "use") != null ? element.Attribute(namespaces[""] + "use").Value : null;
            return pl;
        }

        public int Order { get; set; }

        public string Usage { get; set; }
    }
}
