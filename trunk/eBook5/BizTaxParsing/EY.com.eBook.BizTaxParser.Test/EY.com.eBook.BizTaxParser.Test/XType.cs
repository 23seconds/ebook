﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace EY.com.eBook.BizTaxParser.Test
{
    public class XType
    {
        public static XType CreateFromElement(XElement element, Dictionary<string, XNamespace> namespaces)
        {
            return CreateFromElement<XType>(element, namespaces);
        }


        public static T CreateFromElement<T>(XElement element, Dictionary<string, XNamespace> namespaces) where T:XType,new()
        {
            return new T
            {
                Type = element.Attribute(namespaces["xlink"] + "type") != null ? element.Attribute(namespaces["xlink"] + "type").Value : null
            };
        }

        public string Type { get; set; }

    }
}
