﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace EY.com.eBook.BizTaxParser.Test
{
    public class LinkDefinitionRoleType
    {
        public string RoleUri { get; set; }
        public string Id {get;set;}
        public string Definition { get; set; }
        public string UsedOn { get; set; }

        public static LinkDefinitionRoleType CreateFromElement(XElement element, Dictionary<string, XNamespace> namespaces)
        {
            return LinkDefinitionRoleType.CreateFromElement<LinkDefinitionRoleType>(element, namespaces);
        }

        public static T CreateFromElement<T>(XElement element, Dictionary<string, XNamespace> namespaces) where T : LinkDefinitionRoleType, new()
        {
            T pl = new T();
            pl.RoleUri = element.Attribute("roleURI") != null ? element.Attribute("roleURI").Value : null;
            pl.Id = element.Attribute("id") != null ? element.Attribute("id").Value : null;

            XElement usedOn = element.Element(namespaces["link"] + "usedOn");
            XElement def = element.Element(namespaces["link"] + "definition");

            pl.UsedOn = usedOn != null ? usedOn.Value : null;
            pl.Definition = def != null ? def.Value : null;
            return pl;
        }

    }
}
