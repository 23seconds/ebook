﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace EY.com.eBook.BizTaxParser.Test
{
    public class Locator : Link
    {
        public static Locator CreateFromElement(XElement element, Dictionary<string, XNamespace> namespaces)
        {
            return CreateFromElement<Locator>(element, namespaces);
        }

        public static T CreateFromElement<T>(XElement element, Dictionary<string, XNamespace> namespaces) where T : Locator, new()
        {
            T r = Link.CreateFromElement<T>(element, namespaces);
            if (element.Attribute("label") != null) { r.Label = element.Attribute("label").Value; }
            else if (element.Attribute(namespaces["xlink"] + "label") != null) { r.Label = element.Attribute(namespaces["xlink"] + "label").Value; } 
            if (element.Parent.Name.LocalName == "presentationLink")
            {
                if (element.Parent.Attribute(namespaces["xlink"] + "role") != null)
                {
                    r.Role = element.Parent.Attribute(namespaces["xlink"] + "role").Value;
                }
            }

            return r;
        }

        public string Label { get; set; }

    }
}
