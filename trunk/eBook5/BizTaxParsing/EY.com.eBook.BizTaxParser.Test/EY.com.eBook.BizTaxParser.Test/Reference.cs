﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace EY.com.eBook.BizTaxParser.Test
{
    public class Reference : Link
    {
        public static Reference CreateFromElement(XElement element, Dictionary<string, XNamespace> namespaces)
        {
            return CreateFromElement<Reference>(element, namespaces);
        }

        public static T CreateFromElement<T>(XElement element, Dictionary<string, XNamespace> namespaces) where T:Reference, new()
        {
            T r = Link.CreateFromElement<T>(element, namespaces);
            r.NodeName = element.Name.LocalName;
            r.Role = element.Attribute("roleURI").Value;
            return r;
        }

       

        public string NodeName { get; set; }


    }

}
