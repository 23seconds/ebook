﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Xml.Schema;

namespace EY.com.eBook.BizTaxParser.Test
{
    public class Xsd
    {
        private XmlNameTable table;
        private Dictionary<string, XNamespace> namespaces;

        public List<XsdElement> Elements { get; set; }

        public List<LinkDefinitionRoleType> LinkDefinitionRoleTypes { get; set; }

        public List<string> Attributes { get; set; }

        public Presentation Presentation { get; set; }

        public List<Xsd> ImportedXsds { get; set; }

        public string Name { get; set; }

        public string TargetFolder { get; set; }

        public Xsd(string xsdFile, string targetFolder)
        {
            Name = System.IO.Path.GetFileNameWithoutExtension(xsdFile);
            TargetFolder = targetFolder;
            ReadFromFile(xsdFile, targetFolder);
        }

        internal void ReadFromFile(string xsdFile, string targetFolder)
        {
           
            

            Console.WriteLine("reading " + xsdFile);
            Elements = new List<XsdElement>();
            Attributes = new List<string>();
            string rootDir = System.IO.Path.GetDirectoryName(xsdFile);
            if (!System.IO.File.Exists(xsdFile)) return;
            XmlReader reader = new XmlTextReader(xsdFile);
            XDocument xdoc = XDocument.Load(reader);
            table = reader.NameTable;
            namespaces = xdoc.Root.Attributes().
                            Where(a => a.IsNamespaceDeclaration).
                            GroupBy(a => a.Name.Namespace == XNamespace.None ? String.Empty : a.Name.LocalName,
                                    a => XNamespace.Get(a.Value)).
                            ToDictionary(g => g.Key,
                                         g => g.First());

            //  XmlNamespaceManager xnsmgr = new XmlNamespaceManager(table);
            XmlNamespaceManager xnsmgr = new XmlNamespaceManager(new NameTable());
            foreach (KeyValuePair<string, XNamespace> keypair in namespaces)
            {
                xnsmgr.AddNamespace(keypair.Key, keypair.Value.NamespaceName);
                Console.WriteLine("{0} - {1}", keypair.Key, keypair.Value);
            }

            Console.WriteLine("reading " + xsdFile + " - presentation");
            string presFile = null;
            foreach (XElement element in xdoc.Root.XPathSelectElements("//link:linkbaseRef", xnsmgr))
            {
                XAttribute role = element.Attribute(namespaces["xlink"] + "role");
                if (role != null && role.Value.Contains("role/presentationLinkbaseRef"))
                {
                    presFile = element.Attribute(namespaces["xlink"] + "href").Value;
                    Console.WriteLine(presFile);
                    Presentation= new Presentation(System.IO.Path.Combine(rootDir, presFile));
                }
            }

            Console.WriteLine("reading " + xsdFile + " - LinkDefinitionRoleType");
            LinkDefinitionRoleTypes = new List<LinkDefinitionRoleType>();
            foreach (XElement element in xdoc.Root.XPathSelectElements("//link:roleType", xnsmgr))
            {
                XElement usedOn = element.Element(namespaces["link"] + "usedOn");
                if (usedOn != null && usedOn.Value=="link:definitionLink")
                {
                    LinkDefinitionRoleType ldrt = LinkDefinitionRoleType.CreateFromElement(element, namespaces);
                    LinkDefinitionRoleTypes.Add(ldrt);

                    // get definition=> definitionLink xlink:role
                    // get arcs -> to presentation (/or light up parent as "list", with config=> hypercube
                }
            }

            Console.WriteLine("reading " + xsdFile + " - attributes");
            foreach (XElement element in xdoc.Root.XPathSelectElements("//node()[local-name()='element']", xnsmgr))
            {
                XsdElement xel = new XsdElement(element, namespaces);
                foreach (XAttribute attrib in element.Attributes())
                {
                    Attributes.Add(attrib.Name.ToString());
                }
                Elements.Add(xel);
            }

            Console.WriteLine("reading " + xsdFile + " - Imported");
            ImportedXsds = new List<Xsd>();
            foreach (XElement element in xdoc.Root.XPathSelectElements("//node()[local-name()='import']", xnsmgr))
            {
                string location = element.Attribute("schemaLocation").Value;
                if (!location.StartsWith("http://"))
                {
                    Xsd subXsd = new Xsd(System.IO.Path.Combine(rootDir, location), targetFolder);
                    ImportedXsds.Add(subXsd);
                    if (subXsd.Elements != null) Elements.AddRange(subXsd.Elements);
                    if (subXsd.Attributes != null) Attributes.AddRange(subXsd.Attributes);
                    if(subXsd.LinkDefinitionRoleTypes!=null) LinkDefinitionRoleTypes.AddRange(subXsd.LinkDefinitionRoleTypes);
                }
            }
            LinkDefinitionRoleTypes = LinkDefinitionRoleTypes.Distinct().ToList();
            Attributes = Attributes.Distinct().ToList();
        }

        public XElement GenerateXml(LabelList labels, Definitions definitions, List<XsdElement> elements)
        {
            XElement root = new XElement("root");
            XElement presentation = new XElement("Presentation");
            XElement pres = Presentation.GenerateXml(labels, elements);
            presentation.Add(pres.Elements());
            foreach (Xsd xsd in ImportedXsds)
            {
                XElement sub = null;
                if (xsd.Presentation != null) sub = xsd.Presentation.GenerateXml(labels, xsd.Elements);
                if (sub != null)
                {
                    presentation.Add(sub.Elements());
                }
            }
            root.Add(presentation);

            // get definition=> definitionLink xlink:role
            // get arcs -> to presentation (/or light up parent as "list", with config=> hypercube

            Console.WriteLine("Getting definition arcs");
            List<DefinitionArc> darcs = definitions.Links.SelectMany(l => l.DefinitionArcs).ToList();
            darcs = darcs.Distinct().ToList();

            Console.WriteLine("Setting primary concepts");
            foreach (DefinitionArc da in darcs.Where(d => d.From.Contains("PrimaryConcepts") && !d.To.EndsWith("Hypercube")))
            {
                IEnumerable<XElement> childs = root.XPathSelectElements(string.Format("//Element[@id='{0}']", da.To));
                foreach (XElement child in childs)
                {
                    if (child.Attribute("primaryConcept") == null) child.Add(new XAttribute("primaryConcept", da.From));
                }
            }

            Console.WriteLine("Setting hypercubes");
            List<string> hypercubes = new List<string>();
            foreach (DefinitionArc da in darcs.Where(d => d.From.Contains("PrimaryConcepts") && d.To.EndsWith("Hypercube")))
            {
                IEnumerable<XElement> childs = root.XPathSelectElements(string.Format("//Element[@primaryConcept='{0}']", da.From));
                foreach (XElement child in childs)
                {
                    if (child.Attribute("hyperCube") == null) child.Add(new XAttribute("hyperCube", da.To));
                    if (!hypercubes.Contains(da.To)) hypercubes.Add(da.To);
                }
            }

            XElement xhypers = new XElement("Hypercubes");
            foreach (string hypercube in hypercubes)
            {
                XElement xhyper = new XElement("hypercube", new XAttribute("id", hypercube));
                
                foreach (string da in darcs.Where(d => d.From == hypercube).Select(d=>d.To))
                {
                    xhyper.Add(new XElement("dimension", new XAttribute("id", da)));
                }
                xhypers.Add(xhyper);
            }
            root.Add(xhypers);

            /*
            foreach (LinkDefinitionRoleType ldrt in LinkDefinitionRoleTypes)
            {
                // get defintion
                DefinitionLink dl = definitions.Links.FirstOrDefault(d => d.Role == ldrt.RoleUri);
                if (dl != null)
                {
                    foreach (DefinitionArc da in dl.DefinitionArcs)
                    {
                        IEnumerable<XElement> childs = root.XPathSelectElements(string.Format("//Element[@id='{0}']", da.To));
                        foreach (XElement child in childs)
                        {
                            if (da.From.Contains("PrimaryConcepts"))
                            {
                                if (child.Attribute("primaryConcept") == null) child.Add(new XAttribute("primaryConcept", da.From));
                            }
                            else
                            {

                                XElement par = child.Parent;
                                XAttribute pxid = par.Attribute("id");
                                if (pxid == null || pxid.Value != da.From)
                                {
                                    XElement newpar = par.XPathSelectElement(string.Format("//Element[@id='{0}']", da.From));
                                    if (newpar == null)
                                    {
                                        newpar = new XElement("Element", new XAttribute("id", da.From), new XAttribute("toattrib", true));
                                        par.Add(newpar);
                                    }
                                    child.Remove();
                                    newpar.Add(child);

                                }
                            }
                        }
                        if (childs == null || childs.Count() == 0 && !string.IsNullOrEmpty(da.TargetRole))
                        {
                            //HYPERCUBE
                            XElement newadd = root.XPathSelectElement(string.Format("//Element[@id='{0}']", da.From));
                            if (newadd != null)
                            {
                                XElement hyper = new XElement("Hypercube", new XAttribute("id", da.To));
                                newadd.Add(hyper);
                            }
                        }
                    }
                }
            }
            */
            

            root.Save(System.IO.Path.Combine(TargetFolder, string.Format("{0}.xml", Name)));
            return root;
        }

        //public XElement GenerateXml(LabelList labels,Definitions definitions, List<XsdElement> elements)
        //{
        //    XElement root = new XElement("root");
            
        //    // write ALL elements
        //    List<string> ids = Elements.Where(r=>!string.IsNullOrEmpty(r.Id)).Select(r=>r.Id).Distinct().ToList();
        //    foreach (string xid in ids)
        //    {
        //        XsdElement xel = Elements.First(r => !string.IsNullOrEmpty(r.Id) && r.Id == xid);
        //        XElement el = new XElement("Element", new XAttribute("id", xel.Id));
        //        el.Add(new XAttribute("abstract", xel.Abstract));
        //        el.Add(new XAttribute("nillable", xel.Nillable));
        //        el.Add(new XAttribute("name", xel.Name));
        //        el.Add(new XAttribute("periodType", xel.PeriodType == null ? "" : xel.PeriodType));
        //        el.Add(new XAttribute("substitutionGroup", xel.SubstitutionGroup == null ? "" : xel.SubstitutionGroup));
        //        el.Add(new XAttribute("type", xel.Type == null ? "" : xel.Type));
        //        el.Add(new XAttribute("typedDomainRef", xel.TypedDomainRef == null ? "" : xel.TypedDomainRef));
        //        el.Add(new XAttribute("balance", xel.Balance == null ? "" : xel.Balance));
        //        el.Add(new XAttribute("ref", xel.Ref == null ? "" : xel.Ref));
        //        el.Add(new XAttribute("minOccurs", xel.MinOccurs == null ? "" : xel.MinOccurs));
        //        el.Add(new XAttribute("maxOccurs", xel.MaxOccurs == null ? "" : xel.MaxOccurs));
        //        el.Add(new XAttribute("fixed", xel.Fixed == null ? "" : xel.Fixed));

        //        // ADD LABELS
        //        Dictionary<string, List<Translation>> translations = labels.GetTranslations(xel.Id);
        //        if (translations != null)
        //        {
        //            XElement transel = new XElement("Translations");
        //            foreach (string role in translations.Keys)
        //            {
        //                XElement transrole = new XElement("Role", new XAttribute("id", role));
        //                foreach (Translation t in translations[role])
        //                {
        //                    transrole.Add(new XElement(t.Culture, t.Description));
        //                }
        //                transel.Add(transrole);
        //            }
        //            el.Add(transel);
        //        }
        //        root.Add(el);
        //    }

        //    // CREATE STRUCTURE FROM PRESENTATIONS

        //    //root = PresentationStructure(root, Presentation);

        //    List<PresentationArc> presentationArcs = new List<PresentationArc>();

        //    List<PresentationLink> PresentationLinks = new List<PresentationLink>();
        //    foreach (PresentationLink pl in Presentation.PresentationLinks)
        //    {
        //        presentationArcs.AddRange(pl.PresentationArcs);
        //        PresentationLinks.Add(pl);
        //    }

            
        //    foreach (Xsd xsd in ImportedXsds)
        //    {
        //        if (xsd.Presentation != null)
        //        {
        //            foreach (PresentationLink pl in Presentation.PresentationLinks)
        //            {
        //                presentationArcs.AddRange(pl.PresentationArcs);
        //                PresentationLinks.Add(pl);
        //            }
        //        }
                    
        //    }
        //    presentationArcs = presentationArcs.Distinct().ToList();
        //    root = PresentationStructure(root, presentationArcs);

        //    foreach (PresentationLink pl in PresentationLinks)
        //    {
        //        XElement el = root.XPathSelectElement(string.Format("//Element[@id='{0}']", pl.Id));
        //            if (el == null)
        //            {
        //                el = new XElement("Element", new XAttribute("id", pl.Id));
        //            }
        //        foreach (string xid in pl.GetLinkRoots())
        //        {
        //            XElement child = root.XPathSelectElement(string.Format("//Element[@id='{0}']", xid));
        //            if (child != null && child.Parent!= el)
        //            {
        //                XElement par = child.Parent;
        //                child.Remove();
        //                el.Add(child);
        //                par.Add(el); 
        //            }
        //        }
        //    }

        //    // loop definitions
        //    foreach (DefinitionLink dl in definitions.Links)
        //    {
        //        foreach (DefinitionArc arc in dl.DefinitionArcs)
        //        {
        //            XElement parent = root.XPathSelectElement(string.Format("//Element[@id='{0}']", arc.From));
        //            XElement child = root.XPathSelectElement(string.Format("//Element[@id='{0}']", arc.To));
        //            if (child != null && parent != null && child.Parent != parent)
        //            {
                        
        //                    XElement rootPar = child.Parent;

        //                    XElement par = CopyXElementUndeep(parent);
        //                    child.Remove();
        //                    par.Add(child);
        //                    rootPar.Add(par);
        //                    if (parent.Parent.Name.LocalName == "root") parent.Remove();

        //            }
        //            else if (child != null && parent != null && child.Parent == parent)
        //            {
        //                System.Diagnostics.Debug.WriteLine("no");
        //            }
        //        }
        //    }

        //    root.Save(System.IO.Path.Combine(TargetFolder, string.Format("{0}.xml", Name)));
        //    return root;
        //}

        private XElement CopyXElementUndeep(XElement source)
        {
            XElement result = new XElement(source.Name);
            foreach (XAttribute attrib in source.Attributes())
            {
                result.Add(attrib);
            }
            return result;

        }

        private XElement PresentationStructure(XElement root, List<PresentationArc> arcs)
        {
            
            int cnt = 0;
            int ttl = arcs.Count();
            Console.WriteLine("Structuring presentation {0} arcs", ttl);
                foreach (PresentationArc arc in arcs)
                {
                    cnt++;
                    Console.WriteLine("     {0}/{1}", cnt, ttl);
                    XElement parent = root.XPathSelectElement(string.Format("//Element[@id='{0}']", arc.From));
                    XElement child = root.XPathSelectElement(string.Format("//Element[@id='{0}']", arc.To));
                    
                    if (child != null && parent != null && child.Parent != parent)
                    {
                        if (child.Parent!=null && child.Parent.Name.LocalName == "root") child.Remove();
                        parent.Add(child);
                    }
                    else if (child != null && parent != null && child.Parent == parent)
                    {
                        System.Diagnostics.Debug.WriteLine("no");
                    }
                }
            
            return root;
        }

    }
}
