﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Xml;
using System.IO;
using JeffFerguson.Gepsio;

namespace EY.com.eBook.BizTaxParser.Test
{
    class Program
    {
        static XmlNameTable table;
        static Dictionary<string, XNamespace> namespaces;

        static List<Presentation> presentations = new List<Presentation>();
        static List<Label> labels = new List<Label>();


       // static List<>

       
        static void Main(string[] args)
        {

            string rootDir = @"C:\Projects_TFS\Data\BizTax\2012-04-30\be-tax-2012-04-30\DTS\";
            //string rootDir = @"C:\Projects_TFS\Data\BizTax\2011-04-30-v1.1\2011-04-30\DTS\";

            // Get all presentation xml's
           /* 
            string[] files = System.IO.Directory.GetFiles(@"C:\Projects_TFS\Data\BizTax\2011-04-30-v1.1\2011-04-30\DTS\", "*presentation.xml");
            foreach (string presFile in files)
            {
                Console.WriteLine("Presentation: " + Path.GetFileNameWithoutExtension(presFile));
                Presentation pr = new Presentation(presFile);
                
            }
            */
            LabelList labels = new LabelList(rootDir);

            Definitions definitions = new Definitions(rootDir);
            
            
            // read all xsd's

            //files = System.IO.Directory.GetFiles(@"C:\Projects_TFS\Data\BizTax\2012-04-30\be-tax-2012-04-30\DTS\", "*.xsd");
            //foreach (string xsdFile in files)
            //{
            //    Xsd.ReadFromFile(xsdFile);
            //}
            
            
            
            // PARAMETERS FILE!! be-tax-f-parameters
            // variables met id startend met 'TaxonomySchemaRef' zijn de top van de schema's
            Parameters prs = null;
            //string[] files = System.IO.Directory.GetFiles(@"C:\Projects_TFS\Data\BizTax\2011-04-30-v1.1\2011-04-30\DTS\", "*parameters*.xml");
            string[] files = System.IO.Directory.GetFiles(rootDir, "*parameters*.xml");
            foreach (string paramFile in files)
            {
                Console.WriteLine("Paramfile: " + Path.GetFileNameWithoutExtension(paramFile));
                prs = new Parameters(paramFile);
                foreach(TaxonomyRoot tr in prs.TaxonomyRoots) {

                    Console.WriteLine("- {0} => {1}", tr.TaxonomyId, tr.RootXsd);
                }

            }

            // NEXT GET XSD
            foreach (TaxonomyRoot mtr in prs.TaxonomyRoots)
            {
                string xs = System.IO.Path.Combine(rootDir, mtr.RootXsd);
                string nm = System.IO.Path.GetFileNameWithoutExtension(xs);
                string dir = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, nm);
                if (!System.IO.Directory.Exists(dir)) System.IO.Directory.CreateDirectory(dir);
                DirectoryInfo d = new DirectoryInfo(dir);

                Xsd main = new Xsd(xs, d.FullName);
                main.GenerateXml(labels, definitions, main.Elements);

                //Console.Clear();
                //Console.WriteLine("ELEMENT ATTRIBUTES");
                //foreach (string att in main.Attributes)
                //{
                //    Console.WriteLine(att);
                //}
                //Console.ReadLine();
            }

          //  XmlReader reader = new XmlTextReader(xs);
          //  XDocument xdoc = XDocument.Load(reader);
          //  table = reader.NameTable;
          //  namespaces = xdoc.Root.Attributes().
          //                  Where(a => a.IsNamespaceDeclaration).
          //                  GroupBy(a => a.Name.Namespace == XNamespace.None ? String.Empty : a.Name.LocalName,
          //                          a => XNamespace.Get(a.Value)).
          //                  ToDictionary(g => g.Key,
          //                               g => g.First());

          ////  XmlNamespaceManager xnsmgr = new XmlNamespaceManager(table);
          //  XmlNamespaceManager xnsmgr = new XmlNamespaceManager(new NameTable());
          //  foreach (KeyValuePair<string, XNamespace> keypair in namespaces)
          //  {
          //      xnsmgr.AddNamespace(keypair.Key, keypair.Value.NamespaceName);
          //      Console.WriteLine("{0} - {1}", keypair.Key, keypair.Value);
          //  }

          //  string presFile = null;
          //  foreach (XElement element in xdoc.Root.XPathSelectElements("//link:linkbaseRef", xnsmgr))
          //  {
          //      XAttribute role = element.Attribute(namespaces["xlink"]+"role");
          //      if (role != null && role.Value.Contains("role/presentationLinkbaseRef"))
          //      {
          //          presFile = element.Attribute(namespaces["xlink"] + "href").Value;
          //          Console.WriteLine(presFile);
          //      }
          //  }

          //  Presentation pr = new Presentation(System.IO.Path.Combine(rootDir,presFile));
          //  pr.GenerateXml();
            //foreach (Reference rf in pr.References)
            //{
            //    Console.WriteLine("{0} > {1}", rf.NodeName, rf.Role);
            //    foreach(PresentationLink pl in pr.References
                
            //}
           // ReadDocument(xdoc);

           // WriteAllElmentNames(xdoc.Root,0);
            Console.WriteLine("");
            Console.WriteLine("DONE");
            Console.ReadLine();
        }

        static void ReadDocument(XDocument xdoc)
        {
            XElement annotation = xdoc.Root.Element(namespaces[""]+"annotation");
            if (annotation != null)
            {
                XElement appinfo = annotation.Element(namespaces[""]+"appinfo");
                if (appinfo != null)
                {
                    ReadRoles(appinfo.Elements(namespaces["link"] + "roleType"));
                }
            }
        }

        static void ReadRoles(IEnumerable<XElement> roletypes)
        {
            foreach (XElement rt in roletypes)
            {

                Console.WriteLine(rt.Attribute("id").Value);
            }
        }

        static void WriteAllElmentNames(XElement element, int indent)
        {
            
            string sindent = "".PadLeft((indent*4),' ');
            Console.WriteLine("{0}{1} {2}", sindent, element.Name.ToString(), table.Get(element.Name.NamespaceName));
            foreach (XElement child in element.Descendants())
            {
                WriteAllElmentNames(child, (indent + 1));
            }
        }
    }
}
