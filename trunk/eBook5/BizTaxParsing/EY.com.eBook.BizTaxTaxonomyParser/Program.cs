﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml.Linq;
using EY.com.eBook.BizTaxTaxonomyParser.Parsers;
using EY.com.eBook.BizTaxTaxonomyParser.Contracts;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using System.Xml;
using EY.com.eBook.BizTaxTaxonomyParser.extensions;
using System.Security.Cryptography;
using System.Text.RegularExpressions;
using EY.com.eBook.BizTax;
using System.Diagnostics;

namespace EY.com.eBook.BizTaxTaxonomyParser
{
    public class Test
    {
        public string Value { get; set; }

    }

    static class Program
    {
        static string PARAMFILENAME = "globalparameters.bin";

        
        

        static void Main_TEST(string[] args)
        {
            string[] seq1 = new string[] { "A", "B", "C" };
            string[] seq2 = new string[] { "1", "2", "3" };
            string[] seq3 = new string[] { "|", "@", "#" };
            string[] seq4 = new string[] { ",", ".", ":" };
/*
            CartesianProduct(from s1 in seq1 from s2 in seq3
             */

            //var t = (new List<IEnumerable<string>> { seq1.ToList(), seq2.ToList(), seq3.ToList(), seq4.ToList() }).ca

            IEnumerable<IEnumerable<string>> t = new List<IEnumerable<string>> { seq1.ToList(), seq2.ToList(), seq3.ToList(), seq4.ToList() };
            var m = t.CartesianProduct();

            
            
            foreach (var line in m)
            {
                foreach (var s in line)
                    Console.Write(s);
                Console.WriteLine();
            }
            //Console.ReadLine();
            
            
        }


        static void Main_TESTFORMULA(string[] args)
        {


            string formula = "if (xfi:is-instant-period(.)) then (xs:date(xfi:period-instant(.)) eq xs:date($PeriodStartDate)) else false()";
            string formula2 = "(for $identifier in $ContextIdentities return (substring($IdentifierValue, string-length($IdentifierValue) - 9,10) eq substring($identifier, string-length($identifier) - 9,10)))";
            string formula3 = "(for $identifier in $ComparableIdentifierValues return  (every $item in $PrepaymentReferenceNumberNotEntityIdentifiers satisfies xs:string($item) ne $identifier))";

            FormulaParser fp = new FormulaParser();
            FormulaParserResult fpr = fp.TransformFormula(formula);
            Console.WriteLine(fpr.Formula.ToString());
            fpr.Formula.Save("formula.xml");

            fp = new FormulaParser();
            fpr = fp.TransformFormula(formula2);
            Console.WriteLine(fpr.Formula.ToString());
            fpr.Formula.Save("formula2.xml");

            fp = new FormulaParser();
            fpr = fp.TransformFormula(formula3);
            Console.WriteLine(fpr.Formula.ToString());
            fpr.Formula.Save("formula3.xml");
            Console.ReadLine();
            

        }

        //static void TestXbrls() //
        //{
        //    //Test[] els = new Test[] { new Test { Value = "test" }, null, new Test { Value = "test" },null, null };
        //    //Console.WriteLine(Count(els));
        //    List<long> times = new List<long>();
        //    string baseDir = @"C:\Projects_TFS\Data\Xbrl2012";
        //   // string baseDir = @"Z:\ACR Central Processing\BizTax\AANSLAGJAAR_2012\3_BizTax Out\Success\LENA.PLAS\Buga-Geel NV\31-12-2011";
        //    string[] fls = System.IO.Directory.GetFiles(baseDir, "*.xbrl", SearchOption.AllDirectories);
        //    int cnt = 0;
        //    foreach (string fle in fls)
        //    {
        //        cnt++;
        //        System.IO.DirectoryInfo di = new DirectoryInfo(System.IO.Path.GetDirectoryName(fle));

        //        Console.WriteLine("{0}/{1} : {2}", cnt, fls.Length, di.Parent.Name);
        //        EY.com.eBook.BizTax.AY2012.BizTaxRenderer btr = new EY.com.eBook.BizTax.AY2012.BizTaxRenderer();
        //        XbrlDataContract xbrl = btr.LoadFromXml(fle);

        //        if (xbrl != null)
        //        {
        //            // Console.WriteLine("NON FLATTENED: " + xbrl.Elements.Count);

        //            double ticks = 0;
        //            Stopwatch timer = new Stopwatch();
        //            /*
        //             for (int i = 0; i < 1000; i++)
        //             {
        //                 timer.Reset();
        //                 timer.Start();
        //                 var t = xbrl.Elements.FlattenHierarchy(x => x.Children);
        //                 timer.Stop();
        //                 Console.WriteLine("FLATTENED: " + t.Count());
        //                 Console.WriteLine("took: " + timer.Elapsed.TotalMilliseconds + "ms");
        //                 if (ticks == 0)
        //                 {
        //                     ticks = timer.Elapsed.TotalMilliseconds;
        //                 }
        //                 else
        //                 {
        //                     ticks = (ticks + timer.Elapsed.TotalMilliseconds) / 2;
        //                 }

        //             }
        //             Console.WriteLine(" ");
        //             Console.WriteLine("Average ms: " + ticks);
            
        //             timer.Start();
        //             var el = xbrl.Elements.FlattenHierarchy(x => x.Children).Where(x => x.Name == "EntityCurrentLegalName").FirstOrDefault();
        //             timer.Stop();
        //             Console.WriteLine("timing selection ms: " + timer.ElapsedMilliseconds);
        //             Console.WriteLine(el.Value);
        //             el.Value = "TEST TEST TEST";
        //             el = xbrl.Elements.FlattenHierarchy(x => x.Children).Where(x => x.Name == "EntityCurrentLegalName").FirstOrDefault();
        //             Console.WriteLine(el.Value);
        //             */




        //            btr.CreateIndexes();
        //            timer.Start();
        //            btr.CalculateAndValidate();
        //            timer.Stop();
        //            times.Add(timer.ElapsedMilliseconds);
        //            Console.WriteLine("took: " + timer.ElapsedMilliseconds + " ms");
        //        }
        //    }
        //    Console.WriteLine("FINISHED");
        //    Console.WriteLine("MAX calc time: {0} ms",times.Max());
        //    Console.WriteLine("MIN calc time: {0} ms", times.Min());
        //    Console.WriteLine("AVG calc time: {0} ms",times.Average());
        //    //ToXml(x,false,null).Save("xbrl.xml");
        //    /*
        //    Console.WriteLine("Index counts:");
        //    Console.WriteLine("================");
        //    Console.WriteLine(" ");
        //    Console.WriteLine("ContextsById: " + btr.Index.ContextsById.Count);
        //    Console.WriteLine("ContextsByPeriod: " + btr.Index.ContextsByPeriod.Count);
        //  //  Console.WriteLine("ContextsByScenarioDefinition: " + btr.Index.ContextsByScenarioDefinition.Count);
        //    Console.WriteLine("ElementsByContext: " + btr.Index.ElementsByContext.Count);
        //    Console.WriteLine("ElementsByName: " + btr.Index.ElementsByName.Count);
        //    Console.WriteLine("ElementsByPeriodId: " + btr.Index.ElementsByPeriodId.Count);
        //    double avgIdx = timer.Elapsed.TotalMilliseconds;

        //    timer.Reset();

        //    double avgNon = 0;
        //    double avgNonFlat = 0;
           
        //    for (int i = 0; i < 1000; i++)
        //    {
        //        List<XbrlElementDataContract> lst2 = null;
        //        timer.Start();
        //        GetElementsByPeriodtype(xbrl.Elements, ref lst2);
        //        timer.Stop();
        //        Console.WriteLine("PERIOD - D :: non-indexed took: " + timer.Elapsed.TotalMilliseconds + " ms");
        //        Console.WriteLine("count: " + lst2.Count);
        //        Console.WriteLine(" ");
        //        avgNon += timer.Elapsed.TotalMilliseconds;

        //        List<XbrlElementDataContract> lst3 = null;
        //        timer.Reset();
        //        timer.Start();
        //        lst3 = xbrl.Elements.FlattenHierarchy(x => x.Children).Where(x => x.Period == "D").ToList();
        //        timer.Stop();
        //        Console.WriteLine("PERIOD - D :: non-indexed flattened took: " + timer.Elapsed.TotalMilliseconds + " ms");
        //        Console.WriteLine("count: " + lst3.Count);
        //        Console.WriteLine(" ");
        //        avgNonFlat += timer.Elapsed.TotalMilliseconds;

        //        timer.Reset();
        //        timer.Start();
        //        List<XbrlElementDataContract> lst = btr.GetElements(new string[] {}, "D").ToList();
        //        timer.Stop();
        //        avgIdx += timer.Elapsed.TotalMilliseconds;
        //        Console.WriteLine("PERIOD - D :: indexed took: " + timer.Elapsed.TotalMilliseconds + " ms");
        //        Console.WriteLine("count: " + lst.Count);
        //        Console.WriteLine(" ");
        //    }

        //    Console.WriteLine(" ");
        //    Console.WriteLine(" TOTALS ");
        //    Console.WriteLine("PERIOD - D :: non-indexed took: " + avgNon + " ms");
        //    Console.WriteLine("PERIOD - D :: non-indexed flattened took: " + avgNonFlat + " ms");
        //    Console.WriteLine("PERIOD - D :: indexed took: " + avgIdx + " ms (incl. indexing)");

        //    avgNon = avgNon / 1000;
        //    avgNonFlat = avgNonFlat / 1000;
        //    avgIdx = avgIdx / 1000;

        //    Console.WriteLine(" ");
        //    Console.WriteLine(" AVERAGES ");
        //    Console.WriteLine("PERIOD - D :: non-indexed took: " + avgNon + " ms");
        //    Console.WriteLine("PERIOD - D :: non-indexed flattened took: " + avgNonFlat + " ms");
        //    Console.WriteLine("PERIOD - D :: indexed took: " + avgIdx + " ms (incl. indexing)");


            
        //    */
        //    Console.WriteLine("DONE");
        //    Console.ReadLine();
        //}


        public static void GetElementsByPeriodtype(List<XbrlElementDataContract> sourceList, ref List<XbrlElementDataContract> returnList)
        {
            if (returnList == null)
            {
                returnList = sourceList.Where(x => x.Period == "D").ToList();
            }
            else
            {
                returnList.AddRange(sourceList.Where(x => x.Period == "D").ToList());
            }
            foreach (XbrlElementDataContract xedc in sourceList.Where(c => c.Children != null && c.Children.Count > 0))
            {
                GetElementsByPeriodtype(xedc.Children, ref returnList);
            }

        }
       

        public static int Count(Test[] elements)
        {

            return elements.Count(e => e != null && !string.IsNullOrEmpty(e.Value));
        }

        static void Main_FO(string[] args)
        {
            // FORMULA TESTING
            string[] files = System.IO.Directory.GetFiles(@"C:\Projects_TFS\EY\Ebook\Main-v5.0\TestXbrlFormula", "*.xml");
            foreach (string file in files)
            {
                Console.WriteLine(" ");
                Console.WriteLine(System.IO.Path.GetFileNameWithoutExtension(file));
                EY.com.eBook.BizTaxTaxonomyParser.Renderers.FormulaRenderer fr = new EY.com.eBook.BizTaxTaxonomyParser.Renderers.FormulaRenderer();
                EY.com.eBook.BizTaxTaxonomyParser.Renderers.Statement stmt = fr.ConvertFormula(XElement.Load(file));
                Console.WriteLine(stmt.ResultType.BroadResultType.ToString());
            }
            Console.ReadLine();
        }

        static void Main(string[] args) //__ORG
        {
            //DateTime test = DateTime.Parse("14/08/2013 16:05:09", System.Globalization.CultureInfo.CurrentCulture);
            RenderTaxonomies();
            //TestXbrls();
        }

        static void RenderTaxonomies() //__ORG
        {
            

            
            //string sourceDir = @"C:\Projects_TFS\Data\BizTax\2012-04-30\be-tax-2012-04-30\DTS";
           // string sourceDir = @"C:\Data\BizTax\FodTaxonomies\2014\Ontwerp\be-tax-2014-04-30-v1.0";
            string sourceDir = @"C:\Data\BizTax\FodTaxonomies\2015\Final\be-tax-2015-04-30-v1.1_tcm306-267742";
            //string sourceFormulaDir = @"C:\Projects_TFS\Data\BizTax\2012-04-30\formula doc 2012-04-30";
            //string sourceFormulaDir = @"C:\Data\BizTax\FodTaxonomies\2014\Ontwerp\be-tax-2014-04-30-FormulaDocumentation-v1.0";
            string sourceFormulaDir = @"C:\Data\BizTax\FodTaxonomies\2015\Final\be-tax-2015-04-30-formulaDocumentationV1.1_tcm306-267492\be-tax-2015-04-30-formulaDocumentationV1.1";
            string targetDir = @"C:\Data\BizTax\Rendered";


            // PARSE FORMULA DOCS
            Console.WriteLine("PROCESSING FORMULA DOCUMENTATIONS TO OBTAIN CALCULATIONS");
            Dictionary<string, FormulaDocumentationContract> formulaDoc = ExcelCalculationsParser.Parse(sourceFormulaDir);

            Console.WriteLine(" ");


            string[] files = System.IO.Directory.GetFiles(sourceDir, "*parameters*.xml");
            if (files.Length > 1)
            {
                Console.WriteLine("MULTIPLE PARAMETER FILES FOUND");
            }
            else
            {
                string paramFile = files.First();
                
                Console.WriteLine("Paramfile: " + Path.GetFileNameWithoutExtension(paramFile));
                GlobalParameters gp = ProcessParameters(paramFile, ref targetDir);

                foreach (string taxonomy in gp.Taxonomies)
                {
                    Console.WriteLine(taxonomy);
                    string tDir = Path.Combine(targetDir, Path.GetFileNameWithoutExtension(taxonomy));
                    if (!System.IO.Directory.Exists(tDir)) System.IO.Directory.CreateDirectory(tDir);
                    new TaxonomyParser().Parse(Path.Combine(sourceDir, taxonomy), tDir, formulaDoc, gp);
                }

                
            }
            Console.ReadLine();
            
        }

        static GlobalParameters ProcessParameters(string sourcefile, ref string targetdir)
        {
            
            GlobalParameters gp = new ParameterParser().Parse(sourcefile);

            

            targetdir = Path.Combine(targetdir, gp.AllParameters["CurrentAssessmentYear"].Value);
            if (Directory.Exists(targetdir))
            {
                try
                {
                    Directory.Delete(targetdir, true);
                }
                catch { }
            }
            Directory.CreateDirectory(targetdir);
            string fname = Path.Combine(targetdir, PARAMFILENAME);
            SerializeObject<GlobalParameters>(fname, gp);
            return gp;
        }

        public static XElement ToXml(object instance, bool removeNs, string fname)
        {
            string result = null;
            DataContractSerializer dcs = new DataContractSerializer(instance.GetType());
            var sw = new System.IO.StringWriter();
            var xw = new XmlTextWriter(sw);
            dcs.WriteObject(xw, instance);
            xw.Close();
            result = sw.ToString();
            sw.Close();
            XElement xe = XElement.Parse(result);
            if (removeNs) xe = xe.StripNamespaces();
            return xe;
        }

        public static XElement StripNamespaces(this XElement rootElement)
        {
            foreach (var element in rootElement.DescendantsAndSelf())
            {
                // update element name if a namespace is available
                if (element.Name.Namespace != XNamespace.None)
                {
                    element.Name = XNamespace.None.GetName(element.Name.LocalName);
                }

                // check if the element contains attributes with defined namespaces (ignore xml and empty namespaces)
                bool hasDefinedNamespaces = element.Attributes().Any(attribute => attribute.IsNamespaceDeclaration ||
                        (attribute.Name.Namespace != XNamespace.None && attribute.Name.Namespace != XNamespace.Xml));

                if (hasDefinedNamespaces)
                {
                    // ignore attributes with a namespace declaration
                    // strip namespace from attributes with defined namespaces, ignore xml / empty namespaces
                    // xml namespace is ignored to retain the space preserve attribute
                    var attributes = element.Attributes()
                                            .Where(attribute => !attribute.IsNamespaceDeclaration)
                                            .Select(attribute =>
                                                (attribute.Name.Namespace != XNamespace.None && attribute.Name.Namespace != XNamespace.Xml) ?
                                                    new XAttribute(XNamespace.None.GetName(attribute.Name.LocalName), attribute.Value) :
                                                    attribute
                                            );

                    // replace with attributes result
                    element.ReplaceAttributes(attributes);
                }
            }
            return rootElement;
        }

        public static T CloneObject<T>(T objectToClone)
        {
            MemoryStream ms = new MemoryStream();
            BinaryFormatter bFormatter = new BinaryFormatter();
            bFormatter.Serialize(ms, objectToClone);
            ms.Position = 0;
            return (T)bFormatter.Deserialize(ms);
        }

        public static void SerializeObject<T>(string filename, T objectToSerialize)
        {
            Stream stream = File.Open(filename, FileMode.Create);
            BinaryFormatter bFormatter = new BinaryFormatter();
            bFormatter.Serialize(stream, objectToSerialize);
            stream.Close();
        }

        public static Guid DeterministicGuid(this string source)
        {
            //use MD5 hash to get a 16-byte hash of the string:
            MD5CryptoServiceProvider provider = new MD5CryptoServiceProvider();
            byte[] inputBytes = Encoding.Default.GetBytes(source);
            byte[] hashBytes = provider.ComputeHash(inputBytes);
            Guid hashGuid = new Guid(hashBytes);

            return hashGuid;
        }

        public static T DeSerializeObject<T>(string filename)
        {
            T objectToSerialize;
            Stream stream = File.Open(filename, FileMode.Open);
            BinaryFormatter bFormatter = new BinaryFormatter();
            objectToSerialize = (T)bFormatter.Deserialize(stream);
            stream.Close();
            return objectToSerialize;
        }

        /*
        public static XElement ToXml(object instance, bool removeNs, string fname)
        {
            string result = null;
            DataContractSerializer dcs = new DataContractSerializer(instance.GetType());
            var sw = new System.IO.StringWriter();
            var xw = new XmlTextWriter(sw);
            dcs.WriteObject(xw, instance);
            xw.Close();
            result = sw.ToString();
            sw.Close();
            XElement xe = XElement.Parse(result);
            if (removeNs) xe = xe.StripNamespaces();
            return xe;
        }
         * */
    }
}
