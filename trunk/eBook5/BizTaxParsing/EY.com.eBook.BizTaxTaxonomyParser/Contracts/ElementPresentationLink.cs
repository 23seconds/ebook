﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.BizTaxTaxonomyParser.Contracts
{

    [Serializable]
    [DataContract]
    public enum ElementPresentationType
    {
        [EnumMember]
        Panel,
        [EnumMember]
        ContextGrid,
        [EnumMember]
        Pivot,
        [EnumMember]
        ValueList,
        [EnumMember]
        ValueOrOther,
        [EnumMember]
        Field
    }

    

    [Serializable]
    [DataContract]
    public class ElementPresentationLink
    {
        [DataMember(Order=1)]
        public string Href { get; set; }

        [DataMember(Order = 2)]
        public ElementDimensionSet Context { get; set; }

        [DataMember(Order = 3)]
        public List<string> PresentationPeriods { get; set; }

        [DataMember(Order = 4)]
        public decimal Order { get; set; }

        [DataMember(Order = 5)]
        public string PreferredLabel { get; set; }

        [DataMember(Order = 6)]
        public List<ElementPresentationLink> Children { get; set; }

        [DataMember(Order = 7)]
        public string LinkRole { get; set; }

        [DataMember(Order = 8)]
        public string LinkRoleHref { get; set; }

        [DataMember(Order=9)]
        public List<Label> Labels { get; set; }

        [DataMember(Order = 10)]
        public string Code { get; set; }

        [DataMember(Order = 11)]
        public string OldCode { get; set; }

        [DataMember(Order = 12)]
        public ElementPresentationType PresentationType { get; set; }

        [DataMember(Order = 13)]
        public string DataType { get; set; }

        [DataMember(Order = 14)]
        public ContextDefinition Scenario { get; set; }

        // ABSTRACT ITEM THAT HAS NO REAL SCENARIOS, INHERITS PARENT
        public ContextDefinition AbstractScenario { get; set; }
        //[DataMember(Order = 9)]
        //public List<Label> Document { get; set; }

        public ElementPresentationLink Parent { get; set; }

       

    }

    
}
