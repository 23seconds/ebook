﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.BizTaxTaxonomyParser.Contracts
{
    [Serializable]
    [DataContract]
    public class LabelSet
    {
        [DataMember(Order = 1)]
        public string Category { get; set; }
        [DataMember(Order = 2)]
        public List<Label> Labels { get; set; }


    }
}
