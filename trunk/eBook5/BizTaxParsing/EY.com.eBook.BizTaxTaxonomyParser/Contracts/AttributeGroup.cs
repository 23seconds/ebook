﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.BizTaxTaxonomyParser.Contracts
{
    [Serializable]
    [DataContract]
    public class AttributeGroup
    {
        [DataMember(Order=1)]
        public string Name { get; set; }

        [DataMember(Order=2)]
        public List<XbrlAttribute> Attributes { get; set; }
    }
}
