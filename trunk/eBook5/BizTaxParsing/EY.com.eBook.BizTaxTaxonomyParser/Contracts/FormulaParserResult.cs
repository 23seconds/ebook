﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Xml.Linq;

namespace EY.com.eBook.BizTaxTaxonomyParser.Contracts
{
    [Serializable]
    [DataContract]
    public class FormulaParserResult
    {
        [DataMember]
        public XElement Formula { get; set; }

        [DataMember]
        public List<string> UsedVariables { get; set; }
    }
}
