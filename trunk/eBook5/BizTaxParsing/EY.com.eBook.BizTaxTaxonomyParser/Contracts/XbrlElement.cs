﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.BizTaxTaxonomyParser.Contracts
{
    [Serializable]
    [DataContract]
    public class XbrlElement
    {
        [DataMember(Order=1)]
        public string Id { get; set; }
        [DataMember(Order = 2)]
        public string Href { get; set; }
        [DataMember(Order = 3)]
        public string Name { get; set; }
        [DataMember(Order = 4)]
        public string SourceUri { get; set; }

        [DataMember(Order = 5)]
        public bool Abstract { get; set; }
        [DataMember(Order = 6)]
        public bool Nillable { get; set; }


        [DataMember(Order = 7)]
        public string TypedDomainRef { get; set; }
        [DataMember(Order = 8)]
        public string Balance { get; set; }
        [DataMember(Order = 9)]
        public string Ref { get; set; }
        [DataMember(Order = 10)]
        public string MinOccurs { get; set; }
        [DataMember(Order = 11)]
        public string MaxOccurs { get; set; }
        [DataMember(Order = 12)]
        public string Fixed { get; set; }
        [DataMember(Order = 13)]
        public string SubstitutionGroup { get; set; }
        [DataMember(Order = 14)]
        public string PeriodType { get; set; }

        [DataMember(Order = 15)]
        //public Dictionary<string, List<HypercubeLink>> HypercubeLinks { get; set; }
        public List<HypercubeLink> HypercubeLinks { get; set; }

        [DataMember(Order = 16)]
        public List<string> Periods { get; set; }
        [DataMember(Order = 17)]
        public List<Inherit> Inherits { get; set; }
        [DataMember(Order = 18)]
        public XbrlDataType DataType { get; set; }
        [DataMember(Order = 19)]
        public bool Levelled { get; set; } // has children cfr. pfs
        [DataMember(Order = 20)]
        public string EstimatedType { get; set; }
        [DataMember(Order = 21)]
        public List<LabelSet> LabelSets { get; set; }

        [DataMember(Order = 22)]
        public List<ElementDimensionSet> DimenstionSets { get; set; }

        [DataMember(Order = 23)]
        public List<XbrlRelation> Relations { get; set; }

        [DataMember(Order = 24)]
        public bool HasDefinitionParent { get; set; }

        [DataMember(Order = 25)]
        public List<ContextDefinition> ScenarioDefs { get; set; }

        [DataMember(Order = 26)]
        public string NameSpace { get; set; }

        [DataMember(Order = 27)]
        public string SuspectedPrefix { get; set; }

    }
}
