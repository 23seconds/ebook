﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.BizTaxTaxonomyParser.Contracts
{
    [Serializable]
    [DataContract]
    public enum DimensionTypes
    {
        [EnumMember]
        TypedDimension,
        [EnumMember]
        ExplicitDimension
    }

    [Serializable]
    [DataContract]
    public class Dimension
    {
        [DataMember(Order=1)]
        public string Id { get; set; }

        [DataMember(Order=2)]
        public DimensionTypes DimensionType { get; set; }

        [DataMember(Order=3)]
        public XbrlDataType DataType { get; set; } //only for typed dimensions

        [DataMember(Order=4)]
        public string Name { get; set; }
        [DataMember(Order=5)]
        public string Href { get; set; }
        [DataMember(Order=6)]
        public List<Domain> Domains { get; set; }
        [DataMember(Order=7)]
        public string DefRole { get; set; }

        public string ToString()
        {
            StringBuilder sb = new StringBuilder(Id);
            if (Domains != null)
            {
                foreach (Domain dom in Domains)
                {
                    sb.Append(dom.ToString());
                }
            }
            return sb.ToString();
        }
        
    }

    [Serializable]
    [DataContract]
    public class Domain
    {
        [DataMember(Order=1)]
        public string Id { get; set; }
        [DataMember(Order=2)]
        public List<DomainMember> DomainMembers { get; set; }
        [DataMember(Order=3)]
        public string Name {get;set;}

        public string ToString()
        {
            StringBuilder sb = new StringBuilder(Id);
            if (DomainMembers != null)
            {
                foreach (DomainMember member in DomainMembers)
                {
                    sb.Append(member.ToString());
                }
            }
            return sb.ToString();
        }
    }

    [Serializable]
    [DataContract]
    public class DomainMember
    {
        [DataMember(Order=1)]
        public string Id { get; set; }
        [DataMember(Order=2)]
        public string Name { get; set; }
        [DataMember(Order=3)]
        public string Href { get; set; }
        [DataMember(Order=4)]
        public XbrlElement Element { get; set; }

        public string ToString()
        {
            return Id;
        }
    }

}
