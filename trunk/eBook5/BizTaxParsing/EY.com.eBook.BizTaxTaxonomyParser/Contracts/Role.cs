﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.BizTaxTaxonomyParser.Contracts
{
    [Serializable]
    [DataContract]
    public class Role
    {
        [DataMember]
        public string Href {get;set;}
        [DataMember]
        public string Uri { get; set; }
        [DataMember]
        public string Id { get; set; }
        [DataMember]
        public string Definition { get; set; }
        [DataMember]
        public List<RoleUses> UsedOn { get; set; }
    }

    [Serializable]
    [DataContract]
    public enum RoleUses
    {
        [EnumMember]
        PresentationLink,
        [EnumMember]
        DefinitionLink,
        [EnumMember]
        Label,
        [EnumMember]
        GenArc,
        [EnumMember]
        GenLink,
        [EnumMember]
        Unknown

    }

    [Serializable]
    [DataContract]
    public class Link
    {
        [DataMember]
        public string Role { get; set; }
        [DataMember]
        public string ArcRole { get; set; }
        [DataMember]
        public string File { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string Type { get; set; }
        [DataMember]
        public LinkType LinkType { get; set; }
        [DataMember]
        public string Source { get; set; }

    }

    [Serializable]
    [DataContract]
    public enum LinkType
    {
        [EnumMember]
        Presentation,
        [EnumMember]
         Definition,
        [EnumMember]
         Label,
        [EnumMember]
         Parameters,
        [EnumMember]
         Assertion,
        [EnumMember]
         Reference,
    [EnumMember]
         Unknown
    }
}
