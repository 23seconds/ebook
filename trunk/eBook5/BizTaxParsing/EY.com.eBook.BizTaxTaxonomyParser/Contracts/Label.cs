﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.BizTaxTaxonomyParser.Contracts
{
    [Serializable]
    [DataContract]
    public class Label
    {
        [DataMember(Order = 1)]
        public string Language { get; set; }
        [DataMember(Order = 2)]
        public string Text { get; set; }
    }
}
