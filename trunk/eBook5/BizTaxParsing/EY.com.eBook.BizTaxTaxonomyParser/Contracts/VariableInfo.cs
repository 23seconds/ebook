﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EY.com.eBook.BizTaxTaxonomyParser.Renderers;

namespace EY.com.eBook.BizTaxTaxonomyParser.Contracts
{
    public class VariableInfo
    {
        public string Name { get; set; }

        public ResultType Type { get; set; }

        public string FallBack { get; set; }

    }
}
