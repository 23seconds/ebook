﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.BizTaxTaxonomyParser.Contracts
{
    [Serializable]
    [DataContract]
    public class Inherit
    {
        [DataMember]
        public string Role { get; set; }
        [DataMember]
        public string Type { get; set; } // DOMAIN-MEMBER / HYPERCUBE
        [DataMember]
        public string Href { get; set; }
        [DataMember]
        public int AtOrder { get; set; }
        [DataMember]
        public bool Closed { get; set; }
        [DataMember]
        public string SourceXsd { get; set; }

        [DataMember]
        public string DefFile { get; set; }
    }
}
