﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.BizTaxTaxonomyParser.Contracts
{
    [Serializable]
    [DataContract]
    public class HypercubeLink
    {
        [DataMember]
        public string TargetRole { get; set; }
        [DataMember]
        public string LinkType { get; set; }

        [DataMember]
        public string Href { get; set; }

        [DataMember]
        public string DefRole { get; set; }

        [DataMember]
        public string DefFile { get; set; }

        [DataMember]
        public string SourceXSD { get; set; }

        [DataMember]
        public bool Closed { get; set; }
    }
}
