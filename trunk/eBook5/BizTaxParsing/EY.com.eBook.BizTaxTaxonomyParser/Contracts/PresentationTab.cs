﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.BizTaxTaxonomyParser.Contracts
{
    [Serializable]
    [DataContract]
    public class PresentationTab
    {
        [DataMember]
        public string Role { get; set; }
        [DataMember]
        public string Href { get; set; }
        [DataMember]
        public List<LabelSet> LabelSets { get; set; }
        [DataMember]
        public List<ElementPresentationLink> Elements { get; set; }
    }
}
