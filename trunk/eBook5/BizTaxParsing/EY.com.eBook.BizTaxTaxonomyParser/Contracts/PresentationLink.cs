﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EY.com.eBook.BizTaxTaxonomyParser.Contracts
{
    public class PresentationLink
    {
        public string Id { get; set; }

        public string Presfile { get; set; }

        public bool HasFrom { get; set; }

        public bool HasTo { get; set; }

        public decimal Order { get; set; }

        public ElementPresentationLink BareElementPresentationLink { get; set; }

        public List<PresentationLink> Children { get; set; }
    }
}
