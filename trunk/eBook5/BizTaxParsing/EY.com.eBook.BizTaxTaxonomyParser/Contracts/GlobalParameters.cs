﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.BizTaxTaxonomyParser.Contracts
{
    [Serializable]
    [DataContract]
    public class GlobalParameters
    {
        [DataMember]
        public Dictionary<string,Parameter> AllParameters { get; set; }

        [DataMember]
        public List<string> Taxonomies { get; set; }

    }


}
