﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace EY.com.eBook.BizTaxTaxonomyParser.Contracts
{
    public class FunctionDataContract
    {
        public string Name { get; set; }

        public XElement Body { get; set; }
    }
}
