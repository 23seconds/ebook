﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.BizTaxTaxonomyParser.Contracts
{
    [Serializable]
    [DataContract]
    public class ElementDimensionSet
    {
        [DataMember(Order=1)]
        public string Id { get; set; }
        [DataMember(Order = 2)]
        public string SourceXsd { get; set; }
        [DataMember(Order = 3)]
        public string Role { get; set; }
        [DataMember(Order = 4)]
        public List<Dimension> Dimensions { get; set; }

        public string ToString()
        {
            StringBuilder sb = new StringBuilder();
            if (Dimensions != null)
            {
                foreach (Dimension dim in Dimensions)
                {
                    sb.Append(dim.ToString());
                }
            }
            return sb.ToString();
        }
    }
}
