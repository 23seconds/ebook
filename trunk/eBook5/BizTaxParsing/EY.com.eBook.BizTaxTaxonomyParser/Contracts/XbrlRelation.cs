﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.BizTaxTaxonomyParser.Contracts
{
    [Serializable]
    [DataContract]
    public class XbrlRelation
    {
        [DataMember(Order = 1)]
        public string Role { get; set; }

        [DataMember(Order = 2)]
        public string ArcRole { get; set; }

        [DataMember(Order = 3)]
        public string To { get; set; }

        [DataMember(Order = 4)]
        public string SourceXsd { get; set; }

        [DataMember(Order = 5)]
        public string TargetRole { get; set; }

        [DataMember(Order = 6)]
        public bool? Closed { get; set; }

        [DataMember(Order = 7)]
        public string ContextElement { get; set; }

        [DataMember(Order = 8)]
        public decimal Order { get; set; }

        [DataMember(Order = 9)]
        public string Deffile { get; set; }

    }
}
