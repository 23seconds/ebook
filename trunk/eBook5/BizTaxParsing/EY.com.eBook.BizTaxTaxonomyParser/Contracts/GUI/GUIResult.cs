﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.BizTaxTaxonomyParser.Contracts
{
    [Serializable]
    [DataContract]
    public class GUIResult
    {
        public string Result { get; set; }

        public List<string> Periods { get; set; }

        public bool PanesOnly { get; set; }

        
    }
}
