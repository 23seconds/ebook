﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.BizTaxTaxonomyParser.Contracts
{
    [Serializable]
    [DataContract]
    public class Parameter
    {
        [DataMember]
        public string Id { get; set; }
        [DataMember]
        public string LabelId { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string ValueType { get; set; }
        [DataMember]
        public string Value { get; set; }
        [DataMember]
        public decimal? DecimalValue { get; set; }
        [DataMember]
        public DateTime? DateValue { get; set; }
    }
}
