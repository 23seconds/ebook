﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EY.com.eBook.BizTaxTaxonomyParser.Contracts
{
    public class ElementSelection
    {
        public string VarName { get; set; }

        public List<XbrlElement> Definitions { get; set; }

        public List<string> QNames { get; set; }

        public List<string> Periods { get; set; }

        public List<string> Scenarios { get; set; }

        public List<string> ScenarioDims { get; set; }

        public PeriodFilter PeriodFilter { get; set; }

        public List<ExplicitDimensionFilter> ExplicitDimensionFilters { get; set; }

        public List<TypedDimensionFilter> TypedDimensionFilters { get; set; }

    }
}
