﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.BizTaxTaxonomyParser.Contracts
{
    [Serializable]
    [DataContract]
    public class Taxonomy
    {
        [DataMember]
        public string Id { get; set; }

        [DataMember]
        public string Key { get; set; }

        [DataMember]
        public string SchemaRef { get; set; } // same as id, just for clarity
        [DataMember]
        public string SourceDir { get; set; }
        [DataMember]
        public Dictionary<string,XbrlElement> Elements { get; set; }
        [DataMember]
        public Dictionary<string, XbrlDataType> DataTypes { get; set; }
        [DataMember]
        public List<Role> Roles { get; set; }
        [DataMember]
        public List<Link> Links { get; set; }
        [DataMember]
        public Dictionary<string, Hypercube> Hypercubes { get; set; }
        [DataMember]
        public Dictionary<string, ContextContent> ContextDefinitions { get; set; }

        [DataMember]
        public List<XbrlNamespace> Namespaces { get; set; }

        [DataMember]
        public List<ElementPresentationLink> Presentation { get; set; }

        [DataMember]
        public List<AssertionSet> Assertions { get; set; }

        [DataMember]
        public bool IncludedFormula { get; set; }
    }
}
