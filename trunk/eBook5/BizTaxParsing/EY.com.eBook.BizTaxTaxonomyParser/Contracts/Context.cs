﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.BizTaxTaxonomyParser.Contracts
{
    [Serializable]
    [DataContract]
    public class ContextDefinition
    {
        [DataMember(Order=1)]
        public string AppliedRole { get; set; }

        [DataMember(Order = 2)]
        public string HypercubeRole { get; set; }

        [DataMember(Order = 3)]
        public string Def { get; set; }
       
        [DataMember(Order=4)]
        public List<ContextContent> Scenario { get; set; }
        [DataMember(Order=5)]
        public List<ContextContent> Segment { get; set; }

        public string Id
        {
            get
            {
                StringBuilder sb = new StringBuilder("");
                if (Scenario != null)
                {
                    foreach (ContextContent cc in Scenario.OrderBy(s=>s.Id))
                    {
                        sb.Append(cc.Id);
                    }
                }
                if (Segment != null)
                {
                    foreach (ContextContent cc in Segment.OrderBy(s => s.Id))
                    {
                        sb.Append(cc.Id);
                    }
                }
                if (sb.Length == 0) return Guid.Empty.ToString();
                return sb.ToString().ToString();
            }
        }

        public string DefId
        {
            get
            {
                StringBuilder sb = new StringBuilder("");
                if (Scenario != null)
                {
                    foreach (string defid in Scenario.OrderBy(s => s.DefId).Select(s=>s.DefId).Distinct())
                    {
                        sb.Append(defid);
                    }
                }
                if (Segment != null)
                {
                    foreach (string defid in Segment.OrderBy(s => s.DefId).Select(s => s.DefId).Distinct())
                    {
                        sb.Append(defid);
                    }
                }
                if (sb.Length == 0) return Guid.Empty.ToString();
                return sb.ToString().ToString();
            }
        }

        public string DefName
        {
            get
            {
                StringBuilder sb = new StringBuilder("");
                if (Scenario != null)
                {
                    foreach (string defName in Scenario.OrderBy(s => s.DefName).Select(s => s.DefName).Distinct())
                    {
                        sb.Append(defName);
                    }
                }
                if (Segment != null)
                {
                    foreach (string defName in Segment.OrderBy(s => s.DefName).Select(s => s.DefName).Distinct())
                    {
                        sb.Append(defName);
                    }
                }
                if (sb.Length == 0) return Guid.Empty.ToString();
                return sb.ToString().ToString();
            }
        }

    }

    [Serializable]
    [DataContract]
    public class ContextContent
    {
        // unique combined set

        [DataMember(Order=1)]
        public List<ContextDimension> Dimensions { get; set; }

        public string GetXbrlId()
        {
            //return string.Empty;
            return string.Join("/", Dimensions.OrderBy(c => c.GetXbrlId()).Select(d => d.GetXbrlId()).ToArray());
        }

        public string Id
        {
            get
            {
                return string.Join("/", Dimensions.OrderBy(c=>c.Id).Select(d => d.Id).ToArray());
            }
        }

        public string DefId
        {
            get
            {
                return string.Join("/", Dimensions.OrderBy(c => c.XbrlId).Select(d => d.XbrlId).Distinct().ToArray());
            }
        }

        public string DefName
        {
            get
            {
                return string.Join("/", Dimensions.OrderBy(c => c.XbrlName).Select(d => d.XbrlName).Distinct().ToArray());
            }
        }

        public string DefValue
        {
            get
            {
                return string.Join("/", Dimensions.OrderBy(c => c.XbrlName).Select(d => d.DefValue).Distinct().ToArray());
            }
        }


    }

    [Serializable]
    [DataContract]
    public class ContextDimension
    {

        [DataMember(Order = 1)]
        public string XbrlId { get; set; }

        [DataMember(Order = 2)]
        public DimensionTypes DimensionType { get; set; }

        [DataMember(Order = 3)]
        public XbrlDataType DataType { get; set; } //only for typed dimensions

        [DataMember(Order = 4)]
        public string Name { get; set; }
        [DataMember(Order = 5)]
        public string Href { get; set; }
        [DataMember(Order = 6)]
        public ContextDomain Domain { get; set; } // only for explicit dimensions
        [DataMember(Order = 7)]
        public string DefRole { get; set; }

        public string Id
        {
            get
            {
                return XbrlId + "::" + (Domain != null ? Domain.Id : "");
            }
        }

        public string XbrlName
        {
            get
            {
                return XbrlId.Split(new char[] { '#' })[1].Replace("_",":");
            }
        }


        public string DefValue
        {
            get
            {
                string xname = XbrlName;
                if (DimensionType == DimensionTypes.ExplicitDimension)
                {
                    xname += "::" + Domain.DomainMember.XbrlName;
                }
                return xname;
            }
        }


        public string GetXbrlId()
        {
            if (Domain != null)
            {
                return XbrlName + "::" + Domain.DomainMember.XbrlName;
            }
            return XbrlName;
        }
    }

    [Serializable]
    [DataContract]
    public class ContextDomain
    {
        [DataMember(Order = 1)]
        public string XbrlId { get; set; }
        [DataMember(Order = 2)]
        public ContextDomainMember DomainMember { get; set; }
        [DataMember(Order = 3)]
        public string Name { get; set; }

        public string Id 
        {
            get
            {
                return XbrlId + "::" + (DomainMember != null ? DomainMember.Id : "");
            }
        }

        public string XbrlName
        {
            get
            {
                return XbrlId.Split(new char[] { '#' })[1].Replace("_", ":");
            }
        }
    }

    [Serializable]
    [DataContract]
    public class ContextDomainMember
    {
        [DataMember(Order = 1)]
        public string XbrlId { get; set; }
        [DataMember(Order = 2)]
        public string Name { get; set; }
        [DataMember(Order = 3)]
        public string Href { get; set; }
        [DataMember(Order = 4)]
        public XbrlElement Element { get; set; }

        public string Id
        {
            get
            {
                return XbrlId;
            }
        }

        public string XbrlName
        {
            get
            {
                return XbrlId.Split(new char[] { '#' })[1].Replace("_", ":");
            }
        }
    }
}
