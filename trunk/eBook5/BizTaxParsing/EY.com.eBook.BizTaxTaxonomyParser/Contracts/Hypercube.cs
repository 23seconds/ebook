﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.BizTaxTaxonomyParser.Contracts
{
    [Serializable]
    [DataContract]
    public class Hypercube
    {
        [DataMember]
        public string Id { get; set; }
        [DataMember]
        public string RoleId { get; set; }
        [DataMember]
        public string Href { get; set; }
        [DataMember]
        public bool Closed { get; set; }
        [DataMember]
        public List<Dimension> Scenario { get; set; }
        [DataMember]
        public List<Dimension> Segment { get; set; }

        [DataMember]
        public ContextDefinition ScenarioDef { get; set; }

        [DataMember]
        public ContextDefinition SegmentDef { get; set; }
    }
}
