﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.BizTaxTaxonomyParser.Contracts
{
    [DataContract]
    public class Inheritance
    {
        public string Href { get; set; }

        public string SourceXsd { get; set; }

        public List<Inheritance> Members { get; set; }
    }
}
