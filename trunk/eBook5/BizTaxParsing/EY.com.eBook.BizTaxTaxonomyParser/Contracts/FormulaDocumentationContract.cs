﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.BizTaxTaxonomyParser.Contracts
{
    [Serializable]
    [DataContract]
    public class FormulaDocumentationContract
    {
        [DataMember]
        public string SourceExcel { get; set; }

        [DataMember]
        public string Id { get; set; }

        [DataMember]
        public string BusinessRule { get; set; }

        [DataMember]
        public string Formula { get; set; }

        [DataMember]
        public string Target { get; set; }
    }
}
