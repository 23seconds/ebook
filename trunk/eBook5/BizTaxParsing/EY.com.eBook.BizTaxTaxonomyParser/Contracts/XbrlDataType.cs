﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.BizTaxTaxonomyParser.Contracts
{
    [Serializable]
    [DataContract]
    public class XbrlDataType
    {
        [DataMember]
        public string RootType { get; set; }
        [DataMember]
        public string TypeCode { get; set; }
        [DataMember]
        public List<Restriction> Restrictions { get; set; }
        [DataMember]
        public Dictionary<string,XbrlAttribute> Attributes { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string SourceURI { get; set; }
        [DataMember]
        public string Id { get; set; }
        [DataMember]
        public string BaseSchemaType { get; set; }
        [DataMember]
        public string ContentType { get; set; }

        [DataMember]
        public bool Used { get; set; }

    }
}
