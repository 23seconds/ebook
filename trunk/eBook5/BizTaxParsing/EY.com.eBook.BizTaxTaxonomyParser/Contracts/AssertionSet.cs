﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Xml.Linq;

namespace EY.com.eBook.BizTaxTaxonomyParser.Contracts
{

    [Serializable]
    [DataContract]
    public class FormulaSet
    {
        [DataMember]
        public string Label { get; set; }
        [DataMember]
        public string Role { get; set; }
        [DataMember]
        public string Id { get; set; }
        [DataMember]
        public XbrFormula Formula { get; set; }

        [DataMember]
        public ValueAssertion Preconditions { get; set; }

        [DataMember]
        public List<Variable> Variables { get; set; }

        [DataMember]
        public Variable Target { get; set; }

    }

    [Serializable]
    [DataContract]
    public class XbrFormula
    {
        [DataMember]
        public string Type { get; set; }
        [DataMember]
        public string Label { get; set; }
        [DataMember]
        public string Id { get; set; }
        [DataMember]
        public string AspectModel { get; set; }
        [DataMember]
        public bool ImplicitFiltering { get; set; }
        [DataMember]
        public string Value { get; set; }
        [DataMember]
        public string Source { get; set; }
        [DataMember]
        public FormulaParserResult Formula { get; set; }
    }

    [Serializable]
    [DataContract]
    public class AssertionSet
    {
        [DataMember]
        public string Label { get; set; }
        [DataMember]
        public string Role { get; set; }
        [DataMember]
        public string Id { get; set; }
        [DataMember]
        public List<ValueAssertion> Assertions { get; set; }

        [DataMember]
        public List<ValueAssertion> Preconditions { get; set; }

        [DataMember]
        public List<Variable> Variables { get; set; }

        [DataMember]
        public FormulaSet Formula { get; set; }

    }

    [Serializable]
    [DataContract]
    public class ValueAssertion
    {
        [DataMember]
        public string Type { get; set; }
        [DataMember]
        public string Label { get; set; }
        [DataMember]
        public string Id { get; set; }
        [DataMember]
        public string AspectModel { get; set; }
        [DataMember]
        public bool ImplicitFiltering { get; set; }
        [DataMember]
        public string Test { get; set; }
        [DataMember]
        public FormulaParserResult Formula { get; set; }

        [DataMember]
        public FormulaDocumentationContract Documentation { get; set; }

        [DataMember]
        public FormulaParserResult Calculation { get; set; }

        [DataMember]
        public List<AssertionMessage> Messages { get; set; }

    }

    [DataContract]
    public class AssertionMessage
    {
        [DataMember]
        public string Type { get; set; }
        [DataMember]
        public string Label { get; set; }

        [DataMember]
        public string Role { get; set; }

        [DataMember]
        public string Language { get; set; }

        [DataMember]
        public string BareMessage { get; set; }

        [DataMember]
        public FormattedMessage FormattedMessage { get; set; } 
    }

    [DataContract]
    public class FormattedMessage
    {
        [DataMember]
        public List<string> Variables { get; set; }

        [DataMember]
        public string Message { get; set; }

    }

     [DataContract]
    public enum VariableTypes
    {
        [EnumMember]
        General,
        [EnumMember]
        Fact,
        [EnumMember]
        Parameter
    }

    [Serializable]
    [DataContract]
    public class Variable
    {
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string OriginalName { get; set; }
        [DataMember]
        public string Origin { get; set; }
        [DataMember]
        public string Select { get; set; }
        [DataMember]
        public FormulaParserResult SelectFormula { get; set; }
        [DataMember]
        public VariableTypes Type { get; set; }
        
        [DataMember]
        public bool BindAsSequence { get; set; }
       
        [DataMember]
        public bool Matches { get; set; }

        [DataMember]
        public bool Nils { get; set; }


        [DataMember]
        public string FallBackvalue { get; set; }
        [DataMember]
        public List<Filter> Filters { get; set; }

        public List<string> UsesVariables { get; set; }
    }

    [Serializable]
    [DataContract]
    [KnownType(typeof(ConceptFilter))]
    [KnownType(typeof(ExplicitDimensionFilter))]
    [KnownType(typeof(TypedDimensionFilter))]
    [KnownType(typeof(PeriodFilter))]
    public class Filter
    {
        [DataMember]
        public string Label { get; set; }
        [DataMember]
        public bool Complement { get; set; }
        [DataMember]
        public bool Cover { get; set; }
    }

    [Serializable]
    [DataContract]
    public class ConceptFilter : Filter
    {
        [DataMember]
        public List<string> QNames { get; set; }


    }

    [Serializable]
    [DataContract]
    public class ExplicitDimensionFilter : Filter
    {
        [DataMember]
        public string Dimension { get; set; }

        [DataMember]
        public string Test { get; set; }

        [DataMember]
        public List<string> Members { get; set; }

    }

    [Serializable]
    [DataContract]
    public class TypedDimensionFilter : Filter
    {
        [DataMember]
        public string Dimension { get; set; }

        [DataMember]
        public string Test { get; set; }

    }

    [Serializable]
    [DataContract]
    public class PeriodFilter : Filter
    {
        [DataMember]
        public string Test { get; set; }

        [DataMember]
        public string PeriodTest { get; set; }
    }
}
