﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Schema;
using System.IO;
using System.Xml.Linq;
using EY.com.eBook.BizTaxTaxonomyParser.Contracts;
using System.Collections;
using EY.com.eBook.BizTaxTaxonomyParser.Renderers;
using EY.com.eBook.BizTaxTaxonomyParser.extensions;
using System.Diagnostics;
using System.Text.RegularExpressions;

namespace EY.com.eBook.BizTaxTaxonomyParser.Parsers
{
    public class TaxonomyParser
    {
        public string Id { get; set; }

        public Dictionary<string, XmlSchema> SchemaList { get; set; }

        private Taxonomy Taxonomy = new Taxonomy
        {
            Elements = new Dictionary<string, XbrlElement>()
            , DataTypes = new Dictionary<string,XbrlDataType>()
            , Roles = new List<Role>()
            , Links = new List<Link>()
            , Assertions = new List<AssertionSet>()
        };

        private XName xlinkTo = XName.Get("to", "http://www.w3.org/1999/xlink");
        private XName xlinkFrom = XName.Get("from", "http://www.w3.org/1999/xlink");
        private XName xlinkLabel = XName.Get("label", "http://www.w3.org/1999/xlink");
        private XName xlinkType = XName.Get("type", "http://www.w3.org/1999/xlink");
        private XName xlinkRole = XName.Get("role", "http://www.w3.org/1999/xlink");

        private List<string> periodTests = new List<string>();

        private Dictionary<string,AttributeGroup> AttributeGroups = new Dictionary<string,AttributeGroup>();

        private string GetCodeLabel(XbrlElement el)
        {
            if(el.LabelSets==null) return string.Empty;
            if (el.Id.StartsWith("pfs-")) return string.Empty; // MANUAL EXCLUDE NBB
            LabelSet ls = el.LabelSets.FirstOrDefault(l => l.Category == "http://www.xbrl.org/2003/role/documentation");
            if (ls != null)
            {
                Label lbl = ls.Labels.FirstOrDefault(l => l.Language == "en");
                if (lbl != null) return lbl.Text;
            }
            return string.Empty;
        }

        private string GetOldCodeLabel(XbrlElement el)
        {
            if (el.LabelSets == null) return string.Empty;
            if (el.Id.StartsWith("pfs-")) return string.Empty; // MANUAL EXCLUDE NBB
            LabelSet ls = el.LabelSets.FirstOrDefault(l => l.Category == "http://www.xbrl.org/2003/role/documentation");
            if (ls != null)
            {
                Label lbl = ls.Labels.FirstOrDefault(l => l.Language == "en-US");
                if (lbl != null) return lbl.Text;
            }
            return string.Empty;
        }

        public static string CleanMessage(string value)
        {
            if (String.IsNullOrEmpty(value))
            {
                return value;
            }
            string lineSeparator = ((char)0x2028).ToString();
            string paragraphSeparator = ((char)0x2029).ToString();

            return value.Replace("\"","\\\"").Replace("\r\n", string.Empty).Replace("\n", string.Empty).Replace("\r", string.Empty).Replace(lineSeparator, string.Empty).Replace(paragraphSeparator, string.Empty);
        }

        public void Parse(string fname, string targetdir, Dictionary<string, FormulaDocumentationContract> formulaDoc, GlobalParameters parameters)
        {
            

            Taxonomy.Id = Path.GetFileNameWithoutExtension(fname); // == schemaref
            Taxonomy.SchemaRef = Taxonomy.Id;
            Taxonomy.SourceDir = Path.GetDirectoryName(fname);

            System.IO.DirectoryInfo di = new System.IO.DirectoryInfo(Taxonomy.SourceDir);
            Taxonomy.IncludedFormula = di.GetFiles("*-formula.xml").Length > 0;
            if (Taxonomy.IncludedFormula)
            {
                Console.WriteLine("Taxonomy has formula definitions");
            }
            else
            {
                Console.WriteLine("Taxonomy has NO formula definitions, fallback on Excel documentation");
            }
            #region Retrieve target namespace

            XmlTextReader reader = new XmlTextReader(fname);
            XmlSchema lschema = XmlSchema.Read(reader, SchemaSet_ValidationEventHandler);
            string targetns = lschema.TargetNamespace;
            lschema = null;
            reader.Close();
            reader = null;

            #endregion

            #region Load-in schema set
            XmlSchemaSet set = new XmlSchemaSet();
            set.XmlResolver = new LocalFolderResolver { Folder =Taxonomy.SourceDir };
            set.ValidationEventHandler += new ValidationEventHandler(SchemaSet_ValidationEventHandler);
            set.Add(targetns, fname);

            #endregion

            // CLEANUP DOUBLES (MS bug, circular references)
            #region Remove double schemas, enumerate links and datatypes
            List<string> reloads = new List<string>();
            List<XmlSchema> removeSchemas = new List<XmlSchema>();
            Dictionary<string, XmlQualifiedName> ns = new Dictionary<string, XmlQualifiedName>();

            foreach (XmlSchema sch in set.Schemas())
            {

                Uri suri = new Uri(sch.SourceUri);
                string schFname = suri.Segments[suri.Segments.Length - 1];
               // SchemaList.Add(fname, sch);
                bool isCurrent = suri.LocalPath == fname;

                foreach (XmlSchemaObject xso in sch.Items)
                {
                    XmlSchemaAnnotation xsa = xso as XmlSchemaAnnotation;

                    if (xsa != null)
                    {
                        XmlSchemaAppInfo appinfo = xsa.Items[0] as XmlSchemaAppInfo;
                        if (appinfo != null)
                        {
                            XmlAttribute attrib = null;
                            foreach (XmlNode appNode in appinfo.Markup)
                            {
                                switch (appNode.Name)
                                {
                                    case "link:roleType":
                                        Role role = new Role();
                                        attrib = (XmlAttribute)appNode.Attributes.GetNamedItem("roleURI");
                                        role.Uri = attrib != null ? attrib.Value : null;
                                        attrib = (XmlAttribute)appNode.Attributes.GetNamedItem("id");
                                        role.Id = attrib != null ? string.Format("{0}#{1}", fname, attrib.Value) : null;
                                        foreach (XmlNode childNode in appNode.ChildNodes)
                                        {
                                            switch (childNode.Name)
                                            {
                                                case "link:definition":
                                                    role.Definition = childNode.InnerText;
                                                    break;
                                                case "link:usedOn":
                                                    if (role.UsedOn == null)
                                                    {
                                                        role.UsedOn = new List<RoleUses>();
                                                    }
                                                    switch (childNode.InnerText.ToLower())
                                                    {
                                                        case "link:definitionlink":
                                                            role.UsedOn.Add(RoleUses.DefinitionLink);
                                                            break;
                                                        case "link:presentationlink":
                                                            role.UsedOn.Add(RoleUses.PresentationLink);
                                                            break;
                                                        case "gen:arc":
                                                            role.UsedOn.Add(RoleUses.GenArc);
                                                            break;
                                                        case "gen:link":
                                                            role.UsedOn.Add(RoleUses.GenLink);
                                                            break;
                                                        case "label:label":
                                                            role.UsedOn.Add(RoleUses.Label);
                                                            break;
                                                        default:
                                                            role.UsedOn.Add(RoleUses.Unknown);
                                                            break;
                                                    }
                                                    break;
                                            }
                                        }
                                        role.Href = string.Format("{0}#{1}", fname, role.Definition);
                                        Taxonomy.Roles.Add(role);
                                        break;
                                    case "link:linkbaseRef":
                                        Link link = new Link();
                                        link.Source = sch.SourceUri;
                                        string axl_role = string.Empty;
                                        string axl_arcrole = string.Empty;
                                        string axl_href = string.Empty;
                                        attrib = (XmlAttribute)appNode.Attributes.GetNamedItem("xlink:role");
                                        if (attrib != null) link.Role = attrib.Value;
                                        attrib = (XmlAttribute)appNode.Attributes.GetNamedItem("xlink:arcrole");
                                        if (attrib != null) link.ArcRole = attrib.Value;
                                        attrib = (XmlAttribute)appNode.Attributes.GetNamedItem("xlink:href");
                                        if (attrib != null) link.File = attrib.Value;
                                        attrib = (XmlAttribute)appNode.Attributes.GetNamedItem("xlink:type");
                                        if (attrib != null && attrib.Value != string.Empty)
                                        {
                                            link.Type = attrib.Value;
                                            
                                        }
                                        link.LinkType = LinkType.Unknown;
                                        switch (link.Role)
                                        {
                                            case "http://www.xbrl.org/2003/role/presentationLinkbaseRef":
                                                link.LinkType = LinkType.Presentation;
                                                break;
                                            case "http://www.xbrl.org/2003/role/labelLinkbaseRef":
                                                link.LinkType = LinkType.Label;
                                                break;
                                            case "http://www.xbrl.org/2003/role/definitionLinkbaseRef":
                                                link.LinkType = LinkType.Definition;
                                                break;
                                            case "http://www.xbrl.org/2003/role/referenceLinkbaseRef":
                                                link.LinkType = LinkType.Reference;
                                                break;
                                            default:
                                                if (link.File.Contains("-parameters-"))
                                                {
                                                    link.LinkType = LinkType.Parameters;
                                                }
                                                if (link.File.EndsWith("-assertion.xml"))
                                                {
                                                    link.LinkType = LinkType.Assertion;
                                                }
                                                break;

                                        }
                                        attrib = (XmlAttribute)appNode.Attributes.GetNamedItem("xlink:title");
                                        if (attrib != null) link.Title = attrib.Value;
                                        Taxonomy.Links.Add(link);
                                        break;
                                }
                            }
                        }
                    }

                }

                foreach (XmlSchemaType xstype in sch.SchemaTypes.Values)
                {

                    if (xstype.Name != null)
                    {
                        Console.WriteLine(xstype.Name);

                        switch (xstype.GetType().ToString())
                        {
                            case "System.Xml.Schema.XmlSchemaComplexType":
                                ReadComplexType(xstype as XmlSchemaComplexType);
                                break;
                            case "System.Xml.Schema.XmlSchemaSimpleType":
                                ReadSimpleType(xstype as XmlSchemaSimpleType);
                                break;
                            default:
                                Console.WriteLine("UNKNOWN   > " + xstype.GetType().ToString());
                                break;
                        }
                        Console.WriteLine("   > " + xstype.GetType().ToString());
                    }
                    else
                    {
                        Console.WriteLine("   NO NAME > " + xstype.GetType().ToString());
                    }
                    // Console.ReadLine();

                }
                
                foreach (XmlSchemaAttributeGroup xag in sch.AttributeGroups.Values)
                {

                    //Console.WriteLine("att group");
                    AttributeGroup ag = new AttributeGroup { Name = xag.Name, Attributes = new List<XbrlAttribute>() };
                    foreach (XmlSchemaObject xatobj in xag.Attributes)
                    {
                        XmlSchemaAttribute xat = xatobj as XmlSchemaAttribute;
                        XmlSchemaAttributeGroupRef xagref = xatobj as XmlSchemaAttributeGroupRef;
                        if (xat != null)
                        {
                            ag.Attributes.Add(new XbrlAttribute { Name = xat.Name, Required = xat.Use == XmlSchemaUse.Required, Type = xat.SchemaTypeName.ToString(), });
                        }
                        else if (xagref != null)
                        {
                            if (AttributeGroups.ContainsKey(xagref.RefName.Name))
                            {
                                ag.Attributes.AddRange(AttributeGroups[xagref.RefName.Name].Attributes);
                            }
                        }

                    }
                    if (!AttributeGroups.ContainsKey(ag.Name))
                    {
                        AttributeGroups.Add(ag.Name, ag);
                    }
                }

                XmlQualifiedName[] qualifiedNames = sch.Namespaces.ToArray();
                foreach (XmlQualifiedName qname in qualifiedNames)
                {
                    if (!qname.IsEmpty && !ns.ContainsKey(qname.Name)) ns.Add(qname.Name, qname);
                }

                string id = sch.SourceUri + "#" + sch.TargetNamespace;
                if (!reloads.Contains(id))
                {
                    Uri ur = new Uri(sch.SourceUri);

                    int cnt = 0;
                    foreach (XmlSchema schSub in set.Schemas(sch.TargetNamespace))
                    {

                        cnt++;
                    }
                    if (cnt > 1)
                    {
                        reloads.Add(id);
                        removeSchemas.Add(sch);

                    }
                }
            }
            foreach (XmlSchema sch in removeSchemas)
            {
                set.Remove(sch);
            }
            foreach (string schid in reloads)
            {
                string[] splitted = schid.Split(new char[] { '#' });
                string tns = splitted[1];
                string fle = splitted[0];
                if (set.Schemas(tns).Count == 0)
                {
                    set.Add(tns, fle);
                }
            }

            #endregion

            // compile the schemaset.
            set.Compile();

            #region Get Namespaces
            List<XmlQualifiedName> nsDels = ns.Values.Where(v => v.Namespace.Contains("XMLSchema")).ToList();
            foreach (XmlQualifiedName qname in nsDels)
            {
                ns.Remove(qname.Name);
            }


            Taxonomy.Namespaces = new List<XbrlNamespace>();
            //XElement xns = new XElement("namespaces");
            foreach (string key in ns.Keys)
            {
                Taxonomy.Namespaces.Add(new XbrlNamespace { Name = ns[key].Name, Uri = ns[key].Namespace });
                //xns.Add(new XElement("namespace", new XAttribute("name", ns[key].Name)
                //                                , new XAttribute("namespace", ns[key].Namespace)
                //                    )
                //);
                //Console.WriteLine("{0} : {1}", key, ns[key]);
            }
            #endregion
            // xns.Save("namespaces.xml");

            Taxonomy.Key = Taxonomy.Namespaces.First(n => n.Uri == targetns).Name;
            #region Travers all elements and (element) datatypes (accross set)

            foreach (XmlSchemaElement element in set.GlobalElements.Values)
            {
              //  XmlSchema
                Uri uri = new Uri(element.SourceUri);
                string origin = uri.Segments[uri.Segments.Length - 1];
                string href = string.Format("{0}#{1}", origin, element.Id);
                bool elAndDataType = false;
                string schemaType = element.SchemaType !=null ? element.SchemaType.GetType().ToString() : string.Empty;
                    //elAndDataType = true;

                if (Taxonomy.DataTypes.ContainsKey(element.Name))
                {
                    Console.WriteLine("known type ");
                }
                    switch (schemaType)
                    {
                        case "System.Xml.Schema.XmlSchemaComplexType":
                           
                            ReadComplexType(element.SchemaType as XmlSchemaComplexType, element.Name);
                            break;
                        case "System.Xml.Schema.XmlSchemaSimpleType":
                            ReadSimpleType(element.SchemaType as XmlSchemaSimpleType, element.Name);
                            break;
                    }
                

                    string datatypename = element.SchemaTypeName != null ? element.SchemaTypeName.Name : null;
                    if (element.ElementSchemaType!=null && !string.IsNullOrEmpty(datatypename) && !Taxonomy.DataTypes.ContainsKey(datatypename))
                    {
                        switch (element.ElementSchemaType.GetType().ToString())
                        {
                            case "System.Xml.Schema.XmlSchemaComplexType":

                                ReadComplexType(element.ElementSchemaType as XmlSchemaComplexType, datatypename);
                                break;
                            case "System.Xml.Schema.XmlSchemaSimpleType":
                                ReadSimpleType(element.ElementSchemaType as XmlSchemaSimpleType, datatypename);
                                break;
                        }
                       // ReadSimpleType(element.ElementSchemaType as XmlSchemaSimpleType, datatypename);
                        //AddSimpleDatatype(datatypename, element.d
                    }
                    //element.SchemaTypeName.Name
                    // ELEMENT - always
                            if (!Taxonomy.Elements.ContainsKey(href)) // && !(Taxonomy.DataTypes.ContainsKey(element.Name) && !elAndDataType)
                            {
                                XbrlElement xed = new XbrlElement
                                {
                                    Href = href
                                    ,
                                    Name = element.Name
                                    ,
                                    Id = element.Id
                                    ,
                                    Abstract = element.IsAbstract
                                    ,
                                    Nillable = element.IsNillable
                                    ,
                                    MaxOccurs = element.MaxOccursString
                                    ,
                                    MinOccurs = element.MinOccursString
                                    ,
                                    SubstitutionGroup = element.SubstitutionGroup.ToString()
                                    ,
                                    
                                    SourceUri = origin
                                    ,
                                    Fixed = element.FixedValue
                                    ,
                                    Ref = element.RefName.ToString()
                                    , EstimatedType = GetEstimatedType(element.Name)
                                    , LabelSets = new List<LabelSet>()
                                    , NameSpace = element.QualifiedName.Namespace
                                    , SuspectedPrefix = !string.IsNullOrEmpty(element.Id) && element.Id.Contains("_") ? element.Id.Split(new char[] {'_'})[0] : null

                                };
                                if(datatypename !=null && Taxonomy.DataTypes.ContainsKey(datatypename)) {
                                    Taxonomy.DataTypes[datatypename].Used = true;
                                }
                                xed.DataType = datatypename != null && Taxonomy.DataTypes.ContainsKey(datatypename) ? Taxonomy.DataTypes[datatypename] : null;
                                if (element.UnhandledAttributes != null)
                                {
                                    foreach (XmlAttribute att in element.UnhandledAttributes)
                                    {
                                        switch (att.Name)
                                        {
                                            case "xbrli:periodType":
                                                xed.PeriodType = att.Value;
                                                break;
                                            case "xbrli:balance":
                                                xed.Balance = att.Value;
                                                break;
                                            case "xbrldt:typedDomainRef":
                                                xed.TypedDomainRef = att.Value;
                                                break;
                                        }
                                    }
                                }

                                //if (element.ElementSchemaType != null)
                                //{
                                //    XmlSchemaComplexType complexType = element.ElementSchemaType as XmlSchemaComplexType;
                                //    XmlSchemaSimpleType simpleType = element.ElementSchemaType as XmlSchemaSimpleType;
                                //    if (complexType = null)
                                //    {
                                //        xed.Type = 
                                //    }
                                //}
                                Taxonomy.Elements.Add(href, xed);

                            }

                
            }

            #endregion

            // REPROCESS DATATYPES - ROOTTYPE
            foreach (XbrlDataType datatype in Taxonomy.DataTypes.Values.Where(d=>d.RootType.StartsWith("RELOAD_")).ToList()) 
            {
                string replacement = datatype.RootType.Replace("RELOAD_","");
                if (Taxonomy.DataTypes.ContainsKey(replacement))
                {
                    XbrlDataType sourceType = Taxonomy.DataTypes[replacement];
                    datatype.RootType = sourceType.RootType;
                    Taxonomy.DataTypes[datatype.Name] = datatype;
                }
            }

            #region Process all labels
            Console.WriteLine("Process Labels (" + Taxonomy.Links.Count(l => l.LinkType == LinkType.Label) + ")");
            foreach (Link lblLink in Taxonomy.Links.Where(l => l.LinkType == LinkType.Label))
            {
                Dictionary<string,string> locators = new Dictionary<string,string>();
                Dictionary<string, string> arcs = new Dictionary<string, string>();
                string lblPath = Path.Combine(Taxonomy.SourceDir, lblLink.File);
                Console.WriteLine(lblPath);
                //XDocument xdoc = XDocument.Load(lblPath);
                //foreach (XElement el in xdoc.Root.Elements())
                //{
                //}

                XmlReader lblreader = XmlReader.Create(lblPath);
                while (lblreader.Read())
                {
                    if (lblreader.NodeType == XmlNodeType.Element)
                    {
                        if (lblreader.Name == "loc")
                        {
                            
                            if (lblreader.HasAttributes)
                            {
                                string locLabel = null;
                                string lblhref = null;
                                
                                for (int i = 0; i < lblreader.AttributeCount; i++)
                                {
                                    lblreader.MoveToAttribute(i);
                                    if (lblreader.Name == "xlink:label")
                                    {
                                        locLabel = lblreader.Value;
                                    }
                                    if (lblreader.Name == "xlink:href")
                                    {
                                        lblhref = lblreader.Value;
                                    }
                                }
                                if (!string.IsNullOrEmpty(locLabel) && !string.IsNullOrEmpty(lblhref))
                                {
                                    locators.Add(locLabel, lblhref);
                                }
                            }
                        
                        }
                        if (lblreader.Name == "labelArc")
                        {
                            if (lblreader.HasAttributes)
                            {

                                string lblFrom = null;
                                string lblTo = null;
                                for (int i = 0; i < lblreader.AttributeCount; i++)
                                {
                                    lblreader.MoveToAttribute(i);
                                    if (lblreader.Name == "xlink:from")
                                    {
                                        lblFrom = lblreader.Value;
                                    }
                                    if (lblreader.Name == "xlink:to")
                                    {
                                        lblTo = lblreader.Value;
                                    }
                                }
                                if (!string.IsNullOrEmpty(lblFrom) && !string.IsNullOrEmpty(lblTo))
                                {
                                    arcs.Add(lblTo, lblFrom);
                                }
                            }
                        }
                        if (lblreader.Name == "label")
                        {
                           
                         
                            if (lblreader.HasAttributes)
                            {

                                string lblRole = lblreader.GetAttribute("xlink:role");
                                string lblID = lblreader.GetAttribute("xlink:label");
                                string lblLang = lblreader.GetAttribute("xml:lang");
                                /*
                                for (int i = 0; i < lblreader.AttributeCount; i++)
                                {
                                    
                                    lblreader.MoveToAttribute(i);
                                    if (lblreader.Name == "")
                                    {
                                        lblRole = lblreader.Value;
                                    }
                                    if (lblreader.Name == "xlink:label")
                                    {
                                        lblID = lblreader.Value;
                                    }
                                    if (lblreader.Name == "xml:lang")
                                    {
                                        lblLang = lblreader.Value;
                                    }
                                }
                                 * */
                                string lblText = lblreader.ReadInnerXml();
                                if (!string.IsNullOrEmpty(lblRole) && !string.IsNullOrEmpty(lblID) && !string.IsNullOrEmpty(lblLang))
                                {
                                    if (arcs.ContainsKey(lblID))
                                    {
                                        string lhref = locators[arcs[lblID]];
                                        if (Taxonomy.Elements.ContainsKey(lhref))
                                        {
                                            XbrlElement xel = Taxonomy.Elements[lhref];
                                            if (xel.LabelSets == null) xel.LabelSets = new List<LabelSet>();
                                            LabelSet ls = xel.LabelSets.FirstOrDefault(l => l.Category == lblRole);
                                            if (ls == null)
                                            {
                                                ls = new LabelSet { Category = lblRole, Labels = new List<Label>() };
                                                xel.LabelSets.Add(ls);
                                                ls = xel.LabelSets.FirstOrDefault(l => l.Category == lblRole);
                                            }
                                            Label lbl = ls.Labels.FirstOrDefault(l => l.Language == lblLang);
                                            if (lbl == null)
                                            {
                                                lbl = new Label { Language = lblLang, Text = lblText };
                                                ls.Labels.Add(lbl);
                                            }
                                            else
                                            {
                                                lbl.Text = lblText;
                                            }
                                        }
                                    }
                                }
                            }

                        }
                    }
                    
                }
            }
            #endregion
            // inheritance structure by definition

            Console.WriteLine("Process periods");
            foreach (XbrlElement el in Taxonomy.Elements.Values)
            {
                el.Periods = new List<string>();
                if (el.PeriodType == "duration")
                {
                    el.Periods.Add("D");
                }
                else
                {
                    if (el.LabelSets.Where(l => l.Category.Contains("periodStartLabel")).Count() > 0)
                    {
                        el.Periods.Add("I-Start");
                    }
                    if (el.LabelSets.Where(l => l.Category.Contains("periodEndLabel")).Count() > 0)
                    {
                        el.Periods.Add("I-End");
                    }
                }
            }

            #region Process definitions
            Console.WriteLine("Gathering hypercubes");
            Taxonomy.Hypercubes = new Dictionary<string, Hypercube>();
            /*
            foreach (XbrlElement hc in Taxonomy.Elements.Values.Where(v => v.SubstitutionGroup == "http://xbrl.org/2005/xbrldt:hypercubeItem"))
            {
                Taxonomy.Hypercubes.Add(hc.Href, new Hypercube { Href = hc.Href });
            }
             */
             

            Console.WriteLine("Traverse definitions");
            foreach (Link lnk in Taxonomy.Links.Where(l => l.LinkType == LinkType.Definition).ToList())
            {
                XElement defRoot = new XElement("defroot", new XAttribute("fname",Path.GetFileName(lnk.File)));
                string sourceXsd = Path.GetFileName(new Uri(lnk.Source).LocalPath);
                Console.WriteLine("Link " + lnk.File);
                string lnkPath = Path.Combine(Taxonomy.SourceDir, lnk.File);
                string deffile = lnk.File.Replace("-definition.xml", "");

                XDocument xdoc = XDocument.Load(lnkPath);
                foreach (XElement defel in xdoc.Root.Elements(XName.Get("definitionLink", "http://www.xbrl.org/2003/linkbase")))
                {
                    
                    string delRole = defel.Attribute(XName.Get("role", "http://www.w3.org/1999/xlink")).Value;
                    string hypercubeId = string.Empty;
                    string dimensionId = string.Empty;
                    string domainId = string.Empty;

                    XElement defLink = new XElement("deflink", new XAttribute("role", delRole));

                    Dictionary<string, string> locators = new Dictionary<string, string>();
                    foreach (XElement loc in defel.Elements(XName.Get("loc", "http://www.xbrl.org/2003/linkbase")))
                    {
                        if( !locators.ContainsKey(loc.Attribute(XName.Get("label", "http://www.w3.org/1999/xlink")).Value)) locators.Add(loc.Attribute(XName.Get("label", "http://www.w3.org/1999/xlink")).Value, loc.Attribute(XName.Get("href", "http://www.w3.org/1999/xlink")).Value);
                    }
                    
                        Console.WriteLine("HYPERCUBE DEFINITION");
                        // dimensie
                        // dimensie domein
                        // domain members

                       
                        foreach (XElement defarc in defel.Elements(XName.Get("definitionArc", "http://www.xbrl.org/2003/linkbase")))
                        {

                            
                            string from = defarc.Attribute(XName.Get("from", "http://www.w3.org/1999/xlink")).Value;
                            string to = defarc.Attribute(XName.Get("to", "http://www.w3.org/1999/xlink")).Value;
                            string arcrole = GetDefType(defarc.Attribute(XName.Get("arcrole", "http://www.w3.org/1999/xlink")).Value);
                            string order = defarc.Attribute("order").Value;
                            bool? closedLink = defarc.Attribute(XName.Get("closed", "http://xbrl.org/2005/xbrldt")) !=null ? defarc.Attribute(XName.Get("closed", "http://xbrl.org/2005/xbrldt")).Value=="true" : (bool?)null;
                            string targetRole = defarc.Attribute(XName.Get("targetRole", "http://xbrl.org/2005/xbrldt")) != null ? defarc.Attribute(XName.Get("targetRole", "http://xbrl.org/2005/xbrldt")).Value : delRole;
                            string contextElement = defarc.Attribute(XName.Get("targetRole", "http://xbrl.org/2005/xbrldt")) != null ? defarc.Attribute(XName.Get("contextElement", "http://xbrl.org/2005/xbrldt")).Value : null;

                            // GET FROM ELEMENT
                            XbrlElement xelFrom = Taxonomy.Elements[locators[from]];
                            XbrlElement xelTo = Taxonomy.Elements[locators[to]];
                            if(xelFrom.Relations==null) xelFrom.Relations= new List<XbrlRelation>();
                            xelFrom.Relations.Add(new XbrlRelation
                            {
                                Role = delRole
                                ,
                                ArcRole = arcrole
                                ,
                                Order = decimal.Parse(order)
                                ,
                                Closed = closedLink
                                ,
                                ContextElement = contextElement
                                ,
                                SourceXsd = sourceXsd
                                ,
                                TargetRole = targetRole
                                ,
                                To = locators[to]
                                , Deffile = deffile
                            });
                            xelTo.HasDefinitionParent = true;
                            
                            /*
                            //if (Taxonomy.Hypercubes.ContainsKey(locators[from]))
                            //{
                            //    // HYPERCUBE
                            //    hypercubeId = locators[from];
                            //    if (!hypercubeId.StartsWith(sourceXsd))
                            //    {
                            //        throw new Exception("hypercube located in different XSD then def source");
                            //    }
                            //}
                            if (arcrole == "HYPERCUBE-DIMENSION")
                            {
                                Hypercube hc = null;
                                if (!Taxonomy.Hypercubes.ContainsKey(delRole))
                                {
                                    defLink.Add(new XElement("newHypercube", new XAttribute("href", locators[from])));
                                    hc = new Hypercube
                                    {
                                        RoleId = delRole
                                        ,
                                        Href = locators[from]
                                        ,
                                        Scenario = new List<Dimension>()
                                        ,
                                        Segment = new List<Dimension>()
                                        ,
                                        Id = delRole + "::" + locators[from]
                                    };
                                    Taxonomy.Hypercubes.Add(delRole, hc);
                                }
                                else
                                {
                                    hc = Taxonomy.Hypercubes[delRole];
                                }
                                
                                dimensionId = locators[to];
                                domainId = string.Empty;
                                if (hc.Scenario == null) hc.Scenario = new List<Dimension>();
                                Dimension ndim = new Dimension { DefRole = delRole, Href = dimensionId, Id = to, Domains = new List<Domain>() };
                                hc.Scenario.Add(ndim);

                                defLink.Add(new XElement("addDimension",new XAttribute("id",dimensionId), new XAttribute("to", hc.Href)));

                            }
                            else if (arcrole == "DIMENSION-DOMAIN")
                            {
                                Hypercube hc = Taxonomy.Hypercubes[delRole];
                                if (string.IsNullOrEmpty(dimensionId)) throw new Exception("UNKNOWN DIMENSION");
                                if (dimensionId != locators[from]) throw new Exception("ERROR LINKING DOMAIN TO DIMENSION");
                                Dimension dim = hc.Scenario.FirstOrDefault(d => d.Href == dimensionId);
                                if (dim==null) throw new Exception("UNKNOWN DIMENSION OBJECT");
                                dim.Domains.Add(new Domain { Id = locators[to], DomainMembers = new List<DomainMember>() });
                                domainId = locators[to];
                                defLink.Add(new XElement("addDomain", new XAttribute("id", domainId), new XAttribute("dimensionId", dimensionId)));
                            }
                            else if (arcrole == "DOMAIN-MEMBER" && !string.IsNullOrEmpty(domainId) && !string.IsNullOrEmpty(dimensionId) )
                            {
                                Hypercube hc = Taxonomy.Hypercubes[delRole];
                                if (string.IsNullOrEmpty(dimensionId)) throw new Exception("UNKNOWN DIMENSION");
                                if (string.IsNullOrEmpty(domainId)) throw new Exception("UNKNOWN DOMAIN");
                                if (domainId != locators[from]) throw new Exception("ERROR LINKING MEMBER TO DOMAIN");
                                Dimension dim = hc.Scenario.FirstOrDefault(d => d.Href == dimensionId);
                                if (dim == null) throw new Exception("UNKNOWN DIMENSION OBJECT");
                                Domain dom = dim.Domains.FirstOrDefault(d => d.Id == domainId);
                                if (dom == null) throw new Exception("UNKNOWN DOMAIN OBJECT");
                                dom.DomainMembers.Add(new DomainMember { Href = locators[to], Id = locators[to] });
                                defLink.Add(new XElement("addDomainMember", new XAttribute("id", locators[to]), new XAttribute("domainId", domainId)));

                            }
                            else if (arcrole == "HYPERCUBE-ALL" || arcrole == "HYPERCUBE-NOTALL")
                            {

                                XbrlElement xel = Taxonomy.Elements[locators[from]];
                                //
                                //if(xel.HypercubeLinks==null) xel.HypercubeLinks = new Dictionary<string,List<HypercubeLink>>();
                                //if (!xel.HypercubeLinks.ContainsKey(delRole)) xel.HypercubeLinks.Add(delRole, new List<HypercubeLink>());

                                //xel.HypercubeLinks[delRole].Add(new HypercubeLink {Closed=closedLink, SourceXSD=sourceXsd, DefFile = lnk.File, Href = locators[to], LinkType = arcrole, DefRole = delRole });
                                // 
                                if (xel.HypercubeLinks == null) xel.HypercubeLinks = new List<HypercubeLink>();
                               
                                xel.HypercubeLinks.Add(new HypercubeLink { 
                                    TargetRole=targetRole, 
                                    Closed = closedLink, 
                                    SourceXSD = sourceXsd, 
                                    DefFile = lnk.File,
                                    Href = locators[to], 
                                    LinkType = arcrole, 
                                    DefRole = delRole }
                                    );
                                defLink.Add(new XElement("addHypercubeLink", new XAttribute("target", targetRole), new XAttribute("closed", closedLink), new XAttribute("role", arcrole)));
                            }
                            
                            //else if (arcrole == "DOMAIN-MEMBER")
                            //{
                                
                            //    hypercubeId = string.Empty;
                            //    dimensionId = string.Empty;
                            //    domainId = string.Empty;
                            //    if (!defstructure.ContainsKey(locators[from])) defstructure.Add(locators[from], new List<string>());
                            //    if(!defstructure[locators[from]].Contains(locators[to])) defstructure[locators[from]].Add(locators[to]
                            //}
                            else
                            {
                                // not in use normally
                                hypercubeId = string.Empty;
                                dimensionId = string.Empty;
                                domainId = string.Empty;
                                XbrlElement xel = Taxonomy.Elements[locators[from]];
                                if (xel.Inherits == null) xel.Inherits = new List<Inherit>();
                                xel.Inherits.Add(new Inherit { 
                                    DefFile = lnk.File, 
                                    SourceXsd = sourceXsd, 
                                    Closed = closedLink, 
                                    AtOrder = int.Parse(order), 
                                    Href = locators[to], 
                                    Role = delRole, 
                                    Type = arcrole });
                                defLink.Add(new XElement("addInheritance", new XAttribute("parent", locators[from]), new XAttribute("child", locators[to])));
                            }
                            */
                        } 
                    defRoot.Add(defLink);
                }

                defRoot.Save(System.IO.Path.Combine(targetdir, System.IO.Path.GetFileName(lnkPath)));
            }
            #endregion
           
            #region Enrich hypercubes (dimensions,domains,members)
            /* // Conso
            Console.WriteLine("Enriching hypercubes");
            foreach (Hypercube hc in Taxonomy.Hypercubes.Values)
            {
                if (hc.Scenario != null)
                {
                    foreach (Dimension dim in hc.Scenario)
                    {
                        XbrlElement dimel = Taxonomy.Elements[dim.Href];
                        if (!string.IsNullOrEmpty(dimel.TypedDomainRef))
                        {
                            dim.DimensionType = DimensionTypes.TypedDimension;
                            dim.DataType = GetDataType(dimel.TypedDomainRef, null);
                        }
                        else
                        {
                            dim.DimensionType = DimensionTypes.ExplicitDimension;
                            if (dim.Domains != null)
                            {
                                foreach (Domain dom in dim.Domains)
                                {
                                    if (dom.DomainMembers != null)
                                    {
                                        foreach (DomainMember mem in dom.DomainMembers)
                                        {
                                            mem.Element = Taxonomy.Elements[mem.Href];
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            
            Program.ToXml(Taxonomy.Hypercubes.Values.ToList(), false, "").Save(System.IO.Path.Combine(targetdir, "hypercubes-" + System.IO.Path.GetFileNameWithoutExtension(fname) + ".xml"));
            */
             #endregion

            Console.WriteLine("Building hypercubes");
            foreach (XbrlElement hcEl in Taxonomy.Elements.Values.Where(v => v.SubstitutionGroup == "http://xbrl.org/2005/xbrldt:hypercubeItem").ToList())
            {
                if (hcEl.Relations == null)
                {
                   // empty hypercube Detect role...
                }
                else
                {
                    foreach (string hcRole in hcEl.Relations.Select(r => r.Role).Distinct())
                    {
                        string key = string.Format("{0}::{1}", hcEl.Href, hcRole);
                        Taxonomy.Hypercubes.Add(key, CreateHyperCube(key, hcRole, hcEl));
                    }
                }
            }
            Program.ToXml(Taxonomy.Hypercubes.Values.ToList(), false, "").Save(System.IO.Path.Combine(targetdir, "hypercubes-" + System.IO.Path.GetFileNameWithoutExtension(fname) + ".xml"));


            Console.WriteLine("Process definitions (applying relations)");

            // /!\ TODO !!!
            //
            // TRAVERSE ALL ELEMENTS APPLYING RELATIONS
            // ROOT CONCEPTS FIRST, CFR ALL NOT IN RELATION LINKS (ONLY FROM)
            // APPLY HYPERCUBES IN DIRECT RELATION (OWN RELATIONS)
            // TRAVERSE ROOT CONCEPTS DOMAIN-MEMBER RELATIONS {n} DEEP
            // FOREACH MEMBER, APPLY PARENT, THEN OWN HC RELATIONS, THEN OWN MEMBERS
            // --> CFR INHERITANCE BY DEFINITION
            //
            // -> ELEMENT HYPERCUBE APPLYING = DIFFERENT IMPLEMENTATION THEN NOW.
            // INSTEAD: CONSTRUCT ALL POSSIBLE POSSIBILITIES, THEN DESTRUCT APPLY notAll RELATIONS SPECIFIC
            // SEE: WRITTEN DOC

            foreach (XbrlElement xel in Taxonomy.Elements.Values.Where(v => v.SubstitutionGroup != "http://xbrl.org/2005/xbrldt:hypercubeItem" && !v.HasDefinitionParent ).ToList())
            {
                  ApplyRelations(xel,null);
                
            }




            #region Apply inheritance by definitions
            /*
            foreach (XbrlElement el in Taxonomy.Elements.Values)
            {
                Console.WriteLine(el.Href);
                el.Periods= new List<string>();
                if (!string.IsNullOrEmpty(el.PeriodType))
                {
                    if (el.PeriodType == "duration")
                    {
                        el.Periods.Add("D");
                    }
                    if (el.PeriodType == "instant")
                    {
                        el.Periods.Add("I-Start");
                        el.Periods.Add("I-End");

                    }
                }
                if (el.Inherits != null)
                {
                    foreach (Inherit inh in el.Inherits)
                    {
                        XbrlElement toEl = Taxonomy.Elements[inh.Href];
                        if (toEl.HypercubeLinks == null) toEl.HypercubeLinks = new List<HypercubeLink>();
                        toEl.HypercubeLinks.AddRange(el.HypercubeLinks.Where(h=>h.DefRole==inh.Role));
                        
                    }
                }
            }
             * */
            #endregion

            /* CREATE CONTEXTDEFINITIONS */
            /* TO REVIEW !! 
             * multiple context possible in one xsd source. Context per defrule?, what about notall links in different def
             * */

            #region Apply contexts to elements (dimension sets)
            /*
            foreach (XbrlElement el in Taxonomy.Elements.Values)
            {
                if (el.HypercubeLinks != null)
                {
                    if (el.DimenstionSets == null) el.DimenstionSets = new List<ElementDimensionSet>();
                    foreach (string role in el.HypercubeLinks.Select(h=>h.DefRole).Distinct().ToList())  // KEY = definition ROLE
                    {
                        List<HypercubeLink> hcls = el.HypercubeLinks.Where(h => h.DefRole == role).ToList();
                        int cls = hcls.Count(h => h.Closed == true);
                        ElementDimensionSet eds = new ElementDimensionSet
                        {
                            Role = role
                            ,
                            Dimensions = new List<Dimension>()
                            ,
                            Id = string.Empty
                            ,
                            SourceXsd =hcls.First().SourceXSD
                        };
                        

                        foreach (HypercubeLink hcl in hcls.Where(h => h.LinkType == "HYPERCUBE-ALL"))
                        {
                            if (Taxonomy.Hypercubes.ContainsKey(hcl.TargetRole))
                            {
                                
                                    Hypercube hc = Taxonomy.Hypercubes[hcl.TargetRole];
                                    if (hc.Scenario != null) eds.Dimensions.AddRange(ProtoBuf.Serializer.DeepClone<List<Dimension>>(hc.Scenario));
                                
                            }
                            // if (hc.Segment != null && hc.Segment.Count > 0) contxt.Segment.AddRange(hc.Segment);

                        }
                        foreach (HypercubeLink hcl in hcls.Where(h =>h.LinkType == "HYPERCUBE-NOTALL"))
                        {

                            Hypercube hc = Taxonomy.Hypercubes[hcl.TargetRole];

                            foreach (Dimension dim in hc.Scenario)
                            {
                                Dimension ctxdim = eds.Dimensions.FirstOrDefault(d => d.Href == dim.Href);
                                if (ctxdim != null)
                                {
                                    if (dim.DimensionType == DimensionTypes.TypedDimension)
                                    {
                                        eds.Dimensions.Remove(ctxdim);
                                    }
                                    else
                                    {
                                        foreach (Domain dom in dim.Domains)
                                        {
                                            Domain ctxdom = ctxdim.Domains.FirstOrDefault(d => d.Id == dom.Id);
                                            if (ctxdom != null)
                                            {
                                                foreach (DomainMember mem in dom.DomainMembers)
                                                {
                                                    DomainMember ctxmem = ctxdom.DomainMembers.FirstOrDefault(d => d.Id == mem.Id);
                                                    if (ctxmem != null) ctxdom.DomainMembers.Remove(ctxmem);
                                                }
                                                if (ctxdom.DomainMembers == null || ctxdom.DomainMembers.Count == 0)
                                                    ctxdim.Domains.Remove(ctxdom);
                                            }
                                        }
                                        if (ctxdim.Domains == null || ctxdim.Domains.Count == 0)
                                            eds.Dimensions.Remove(ctxdim);
                                    }
                                }
                            }
                        }
                        // CREATE DOMAINSET

                        eds.Id = string.Join("#", eds.Dimensions.Select(d => d.Id).ToArray());
                        el.DimenstionSets.Add(eds);
                        //  string.Format("{0}::{1}",role, );

                    }
                }
            }
             */
            #endregion

            // Program.ToXml(Taxonomy.Elements.Values.Where(e=>e.ContextDefinitions!=null).SelectMany(e=>e.ContextDefinitions).Distinct().ToList(), false, "").Save(System.IO.Path.Combine(targetdir, "CONTEXTS-" + System.IO.Path.GetFileNameWithoutExtension(fname) + ".xml"));



            #region Process presentation definitions
            Console.WriteLine("Create presentation");
        
            //XElement allpresentations = new XElement("presentation");
           // Taxonomy.Presentation = new List<PresentationTab>();
            Taxonomy.Presentation = new List<ElementPresentationLink>();
            int globalOrder = 0;
            foreach (Link lnk in Taxonomy.Links.Where(l => l.LinkType == LinkType.Presentation).ToList())
            {
                string sourceXsd = Path.GetFileName(new Uri(lnk.Source).LocalPath);
                Console.WriteLine("Link " + lnk.File);
                string lnkPath = Path.Combine(Taxonomy.SourceDir, lnk.File);
                XDocument xdoc = XDocument.Load(lnkPath);

                string presfile = lnk.File.Replace("-presentation.xml", "");
                

                /*
                foreach(XElement presrole in xdoc.Root.Elements(XName.Get("roleRef", "http://www.xbrl.org/2003/linkbase"))) {
                    string prHref =  presrole.Attribute(XName.Get("href", "http://www.w3.org/1999/xlink")).Value;
                  //  XbrlElement xelPr = Taxonomy.Elements[prHref];
                    Taxonomy.Presentation.Add(new PresentationTab
                    {
                        Href = prHref
                        ,
                        Role = presrole.Attribute("roleURI").Value
                        //,
                        //LabelSets = xelPr.LabelSets
                        , Elements = new List<ElementPresentationLink>()
                    });
                }
                 */


                foreach (XElement presel in xdoc.Root.Elements(XName.Get("presentationLink", "http://www.xbrl.org/2003/linkbase")))
                {
                    globalOrder++;
                    string presRole = presel.Attribute(XName.Get("role", "http://www.w3.org/1999/xlink")).Value;
                    if (presRole.StartsWith("http://www.minfin.fgov.be"))
                    {
                        XElement presLink = new XElement("presentationLink", new XAttribute("role", presRole));

                        // STEP 1: CREATE DICTIONARY OF EACH PARENT WITH CHILDREN WITHIN PRESENTATION LINK (only within)
                        Dictionary<string, string> locators = new Dictionary<string, string>();
                        foreach (XElement loc in presel.Elements(XName.Get("loc", "http://www.xbrl.org/2003/linkbase")))
                        {
                            if(!locators.ContainsKey(loc.Attribute(XName.Get("label", "http://www.w3.org/1999/xlink")).Value)) locators.Add(loc.Attribute(XName.Get("label", "http://www.w3.org/1999/xlink")).Value, loc.Attribute(XName.Get("href", "http://www.w3.org/1999/xlink")).Value);
                        }

                        Dictionary<string, PresentationLink> preselements = new Dictionary<string, PresentationLink>();
                        ElementPresentationLink xfrom = null;
                        foreach (XElement presarc in presel.Elements(XName.Get("presentationArc", "http://www.xbrl.org/2003/linkbase")))
                        {
                            string from = presarc.Attribute(XName.Get("from", "http://www.w3.org/1999/xlink")).Value;
                            string to = presarc.Attribute(XName.Get("to", "http://www.w3.org/1999/xlink")).Value;
                            string arcrole = presarc.Attribute(XName.Get("arcrole", "http://www.w3.org/1999/xlink")).Value;
                            string order = presarc.Attribute("order").Value;
                            string preferredLabel = presarc.Attribute("preferredLabel") != null ? presarc.Attribute("preferredLabel").Value : null;

                            string locFrom = locators[from];
                            string locTo = locators[to];

                            if (arcrole == "http://www.xbrl.org/2003/arcrole/parent-child")
                            {
                                XbrlElement xbelTo = Taxonomy.Elements[locTo];
                                XbrlElement xbelFrom = Taxonomy.Elements[locFrom];
                                
                                if (!preselements.ContainsKey(locFrom))
                                {
                                    string lsF = !string.IsNullOrEmpty(preferredLabel) ? preferredLabel : "http://www.xbrl.org/2003/role/label";
                                    LabelSet lsetF = xbelFrom.LabelSets.FirstOrDefault(l => l.Category == lsF);
                                   
                                    preselements.Add(locFrom, new PresentationLink
                                    {
                                        Id=locFrom,
                                        Presfile = presfile,
                                        HasFrom = true,
                                        HasTo = false,
                                        BareElementPresentationLink = new ElementPresentationLink
                                        {
                                            Children = new List<ElementPresentationLink>()
                                            ,
                                            Context = null
                                            ,
                                            Href = locFrom
                                            ,
                                            LinkRole = presRole
                                            ,
                                            LinkRoleHref = null
                                            ,
                                            Order = globalOrder
                                            ,
                                            Labels = lsetF != null ? lsetF.Labels : null
                                            ,
                                            PreferredLabel = preferredLabel
                                            ,
                                            PresentationPeriods = GetPresentationLocations(xbelFrom.LabelSets)
                                            ,
                                            Code = GetCodeLabel(xbelFrom)
                                            ,
                                            OldCode = GetOldCodeLabel(xbelFrom)
                                            ,
                                            Scenario = xbelFrom.ScenarioDefs != null ? xbelFrom.ScenarioDefs.FirstOrDefault() : null
                                        },
                                        Children = new List<PresentationLink>(),
                                        Order = globalOrder
                                    });

                                }

                                if (xfrom == null || xfrom.Href != locFrom)
                                {
                                    xfrom = preselements[locFrom].BareElementPresentationLink;
                                }
                               // ElementPresentationLink xFrom = preselements[locFrom].BareElementPresentationLink;


                                ElementPresentationLink xTo = new ElementPresentationLink
                                {
                                    Children = new List<ElementPresentationLink>()
                                    ,
                                    Context = null
                                    ,
                                    Href = locTo
                                    ,
                                    LinkRole = presRole
                                    ,
                                    LinkRoleHref = null
                                    ,
                                    Order = decimal.Parse(order)
                                    ,
                                    PreferredLabel = preferredLabel
                                    ,
                                    PresentationPeriods = GetPresentationLocations(xbelTo.LabelSets)
                                    ,
                                    Code = GetCodeLabel(xbelTo)
                                    ,
                                    OldCode = GetOldCodeLabel(xbelTo)
                                };

                                // SET LABELS
                                string ls = !string.IsNullOrEmpty(xTo.PreferredLabel) ? xTo.PreferredLabel : "http://www.xbrl.org/2003/role/label";
                                LabelSet lset = xbelTo.LabelSets.FirstOrDefault(l => l.Category == ls);
                                xTo.Labels = lset != null ? lset.Labels : null;
                                // string ctxIDs = xbel.Contexts != null ? string.Join(",", xbel.Contexts.Where(c => c.SourceXsd == sourceXsd).Select(c => c.Id).Distinct().ToArray()) : "NONE";
                                xTo.Parent = xfrom;


                                PresentationLink plTo = new PresentationLink
                                     {
                                         Id = locTo
                                         , Presfile = presfile
                                         ,
                                         Order = decimal.Parse(order)
                                         ,
                                         Children = null
                                         ,
                                         BareElementPresentationLink = xTo
                                         ,
                                         HasTo = true
                                         ,
                                         HasFrom = false
                                     };

                                if (!preselements.ContainsKey(locTo))
                                {
                                    preselements.Add(locTo,plTo);
                                }
                                else
                                {
                                    preselements[locTo].HasTo = true;
                                }

                                if (preselements[locFrom].Children == null) preselements[locFrom].Children = new List<PresentationLink>();

                                if (preselements[locFrom].Children.Count(c => c.Id == locTo) == 0)
                                {
                                    preselements[locFrom].Children.Add(plTo);
                                }

                                
                               

                            }

                        }

                        List<PresentationLink> pls = preselements.Values.Where(v => v.HasTo == false && v.HasFrom==true).ToList();

                        
                        // STEP 2: RE-TRAVERSE CREATING TREES AND APPLYING CONTEXT
                        foreach (PresentationLink pl in pls)
                        {
                            Console.WriteLine(pl.Id);
                            ElementPresentationLink epl = ParsePresentationLinks(preselements, pl);
                            Taxonomy.Presentation.Add(epl);
                        }
                        
                    }
                }
                //root.Save(System.IO.Path.Combine(targetdir, System.IO.Path.GetFileName(lnkPath)));

            }
            //allpresentations.Save(System.IO.Path.Combine(targetdir, "presentation-" + System.IO.Path.GetFileNameWithoutExtension(fname) + ".xml"));
            
            // GATHER DOUBLES
            List<string> doublesPres = Taxonomy.Presentation.Where(p => Taxonomy.Presentation.Count(s => s.Href == p.Href) > 1).Select(s => s.Href).Distinct().ToList();


            foreach (string eplHref in doublesPres)
            {
                ElementPresentationLink epl = Taxonomy.Presentation.First(p => p.Href == eplHref);
                ElementPresentationLink newEpl = new ElementPresentationLink
                {
                    Children = new List<ElementPresentationLink>(),
                    AbstractScenario = epl.AbstractScenario,
                    Code = epl.Code,
                    Context = epl.Context,
                    DataType = epl.DataType,
                    Href = epl.Href,
                    Labels = epl.Labels,
                    LinkRole = epl.LinkRole,
                    LinkRoleHref = epl.LinkRoleHref,
                    OldCode = epl.OldCode,
                    Order = epl.Order,
                    Parent = epl.Parent,
                    PreferredLabel = epl.PreferredLabel,
                    PresentationPeriods = epl.PresentationPeriods,
                    PresentationType = epl.PresentationType,
                    Scenario = epl.Scenario
                };
                int idx = Taxonomy.Presentation.IndexOf(epl);

                foreach (ElementPresentationLink eplDoubles in Taxonomy.Presentation.Where(p => p.Href == epl.Href).ToList())
                {
                    newEpl.Children.AddRange(eplDoubles.Children);
                    Taxonomy.Presentation.Remove(eplDoubles);
                }
                Taxonomy.Presentation.Insert(idx, newEpl);
            }


            #endregion

           

            XbrlElement ExpensesSharesPEExemptIncomeMovableAssets = Taxonomy.Elements.Values.First(e => e.Name == "ExpensesSharesPEExemptIncomeMovableAssets");
            XbrlElement IdentityTradeDebtor = Taxonomy.Elements.Values.First(e => e.Name == "IdentityTradeDebtor");
            XbrlElement TaxableReserves = Taxonomy.Elements.Values.First(e => e.Name == "TaxableReserves");
            
            // FIRST NODE IS ALWAYS ASSERTION SET
            List<XName> nodenames = new List<XName>();
            #region Process Assertions 
             Console.WriteLine("Traverse definitions");

            Console.WriteLine("DETECTION AND CLEANING REDUNDANT LINKS");
            // detect and cleanup doubles
             var tlnks = Taxonomy.Links.Where(l => l.LinkType == LinkType.Assertion).ToList();
             var lnkdbls = tlnks.GroupBy(t => t.File).Where(t => t.Count() > 1).ToDictionary(t => t.Key, t => t.ToList());

             foreach (string key in lnkdbls.Keys)
             {
                 var mxindex = lnkdbls[key].Max(t => tlnks.IndexOf(t));
                 var idxremove = lnkdbls[key].Where(t => tlnks.IndexOf(t) != mxindex).Select(t => tlnks.IndexOf(t)).OrderByDescending(t => t).ToList();
                 foreach (int rmidx in idxremove)
                 {
                     tlnks.RemoveAt(rmidx);
                 }
             }



             foreach (Link lnk in tlnks)
             {
                 int varCount = 0;
                string sourceXsd = Path.GetFileName(new Uri(lnk.Source).LocalPath);
                Console.WriteLine("Link " + lnk.File);
                string lnkPath = Path.Combine(Taxonomy.SourceDir, lnk.File);
                string formulaPath = lnkPath.Replace("-assertion", "-formula");
                XDocument xdoc = XDocument.Load(lnkPath);
                XDocument xformulaDoc = null;
                if (System.IO.File.Exists(formulaPath))
                {
                    xformulaDoc = XDocument.Load(formulaPath);
                }
                
                int cnt = 0;
                foreach (XElement defel in xdoc.Root.Elements(XName.Get("link", "http://xbrl.org/2008/generic")))
                {
                    // set :
                    XElement xaset = defel.Element(XName.Get("assertionSet", "http://xbrl.org/2008/validation"));


                    AssertionSet aset = new AssertionSet
                    {
                        Id = xaset.Attribute("id").Value
                        ,
                        Label = xaset.Attribute(XName.Get("label", "http://www.w3.org/1999/xlink")).Value
                        ,
                        Role = xaset.Attribute(XName.Get("role", "http://www.w3.org/1999/xlink")).Value
                        ,
                        Variables = new List<Variable>()
                        ,
                        Preconditions = new List<ValueAssertion>()
                        ,
                        Assertions = new List<ValueAssertion>()
                    };
                    Uri rle = new Uri(aset.Role);
                    string tdir = rle.Segments[rle.Segments.Length - 1];

                    

                    foreach (XElement genericArc in defel.Elements(XName.Get("arc", "http://xbrl.org/2008/generic")).Where(x => x.Attribute(xlinkFrom) != null && x.Attribute(xlinkFrom).Value == aset.Label))
                    {
                        string vato = genericArc.Attribute(xlinkTo).Value;
                        FormulaDocumentationContract fdc = null;
                        if (formulaDoc.ContainsKey(vato))
                        {
                            Console.WriteLine("found formula documentation");
                            fdc = formulaDoc[vato];
                            if (fdc.Formula.Contains("identical to formula "))
                            {
                                string identicalKey = fdc.Formula.Replace("identical to formula ","");
                                if (formulaDoc.ContainsKey(identicalKey))
                                {
                                    fdc.Formula = formulaDoc[identicalKey].Formula;
                                }

                            }
                        }


                        // SHOULD ALWAYS BE JUST ONE
                        foreach (XElement vass in defel.Elements().Where(x => x.Attribute(xlinkLabel) != null && x.Attribute(xlinkLabel).Value == vato))
                        {
                            if (vass.Name.LocalName == "valueAssertion")
                            {
                                /*
                                ValueAssertion va = new ValueAssertion
                                {
                                    Id = vass.Attribute("id").Value
                                    ,
                                    AspectModel = vass.Attribute("aspectModel").Value
                                    ,
                                    ImplicitFiltering = vass.Attribute("implicitFiltering").Value == "true"
                                    ,
                                    Label = vass.Attribute(xlinkLabel).Value
                                    ,
                                    Test = vass.Attribute("test").Value
                                    ,
                                    Type = vass.Attribute(xlinkType).Value
                                    ,
                                    Formula = tdir != "correctness-assertion" ? new FormulaParser().TransformFormula(vass.Attribute("test").Value) : null
                                    ,
                                    Documentation = fdc
                                    ,
                                    Calculation = tdir != "correctness-assertion" && fdc!=null && !string.IsNullOrEmpty(fdc.Formula) ? new FormulaParser().TransformFormula(fdc.Formula, fdc.Target) : null
                                };*/
                                ValueAssertion va = new ValueAssertion
                                {
                                    Id = vass.Attribute("id").Value
                                    ,
                                    AspectModel = vass.Attribute("aspectModel").Value
                                    ,
                                    ImplicitFiltering = vass.Attribute("implicitFiltering").Value == "true"
                                    ,
                                    Label = vass.Attribute(xlinkLabel).Value
                                    ,
                                    Test = vass.Attribute("test").Value
                                    ,
                                    Type = vass.Attribute(xlinkType).Value
                                    ,
                                    Formula = new FormulaParser().TransformFormula(vass.Attribute("test").Value)
                                    ,
                                    Documentation = fdc
                                    ,
                                    Calculation = fdc != null && !string.IsNullOrEmpty(fdc.Formula) ? new FormulaParser().TransformFormula(fdc.Formula, fdc.Target) : null
                                };

                                aset.Assertions.Add(va);
                            }
                            else
                            {
                                Console.WriteLine("SOMETHING ELSE");
                            }
                           

                        }
                    }
                    if (aset.Assertions.Count > 1)
                    {
                        Console.WriteLine("MORE THE ONE ASSERTION");
                    }
                    // variable discovering
                    foreach (ValueAssertion vass in aset.Assertions)
                    {
                        vass.Messages = new List<AssertionMessage>();
                        foreach (XElement garcs in defel.Elements(XName.Get("arc", "http://xbrl.org/2008/generic")).Where(x => x.Attribute(xlinkFrom) != null && x.Attribute(xlinkFrom).Value == vass.Label))
                        {
                            string to = garcs.Attribute(xlinkTo).Value;
                            foreach (XElement prec in defel.Elements().Where(x => x.Attribute(xlinkLabel) != null && x.Attribute(xlinkLabel).Value == to))
                            {
                                if (prec != null && prec.Name.LocalName == "message")
                                {
                                    vass.Messages.Add(new AssertionMessage
                                    {
                                        Type = prec.Attribute(xlinkType).Value
                                        ,
                                        Label = prec.Attribute(xlinkLabel).Value
                                        , Role = prec.Attribute(xlinkRole).Value
                                        , Language = prec.Attribute(XName.Get("lang",XNamespace.Xml.NamespaceName)).Value
                                        ,
                                        BareMessage = CleanMessage(prec.Value)
                                        , FormattedMessage = GetFormattedMessage(CleanMessage(prec.Value))
                                    });
                                }
                                if (prec != null && prec.Name.LocalName == "precondition")
                                {
                                    ValueAssertion va = new ValueAssertion
                                    {
                                        Id = prec.Attribute(xlinkLabel).Value
                                        ,
                                        Label = prec.Attribute(xlinkLabel).Value
                                        ,
                                        Test = prec.Attribute("test").Value
                                        ,
                                        Type = "PRECONDITION"
                                        ,
                                        Formula = new FormulaParser().TransformFormula(prec.Attribute("test").Value)
                                    }; 
                                    aset.Preconditions.Add(va);
                                }
                            }
                        }

                        foreach (XElement varcs in defel.Elements(XName.Get("variableArc", "http://xbrl.org/2008/variable")).Where(x => x.Attribute(xlinkFrom) != null && x.Attribute(xlinkFrom).Value == vass.Label))
                        {
                            Variable va = new Variable
                            {
                                Name = varcs.Attribute("name").Value
                                , Origin = varcs.Attribute(xlinkTo).Value
                                , FallBackvalue=null
                                , Filters= new List<Filter>()
                                , UsesVariables = new List<string>()
                            };
                            XName vafilterarc = XName.Get("variableFilterArc","http://xbrl.org/2008/variable");
                            foreach (XElement varel in defel.Elements().Where(x => x.Attribute(xlinkLabel) != null && x.Attribute(xlinkLabel).Value == va.Origin))
                            {
                                if (varel.Name.LocalName == "factVariable")
                                {
                                    va.Type = VariableTypes.Fact;
                                    if (varel.Attribute("fallbackValue") != null) va.FallBackvalue = varel.Attribute("fallbackValue").Value;
                                    va.BindAsSequence = varel.Attribute("bindAsSequence") != null && varel.Attribute("bindAsSequence").Value == "true";
                                    va.Matches = varel.Attribute("matches") != null && varel.Attribute("matches").Value == "true";
                                    va.Nils = varel.Attribute("nils") != null && varel.Attribute("nils").Value == "true";
                                    // find filter arcs
                                    foreach (XElement varFilter in defel.Elements(vafilterarc).Where(x => x.Attribute(xlinkFrom) != null && x.Attribute(xlinkFrom).Value == va.Origin))
                                    {
                                        bool complement = varFilter.Attribute("complement")!=null && varFilter.Attribute("complement").Value=="true";
                                        bool cover = varFilter.Attribute("cover")!=null && varFilter.Attribute("cover").Value=="true";

                                        string fto = varFilter.Attribute(xlinkTo).Value;
                                       
                                        foreach(XElement el in defel.Elements().Where(x => x.Attribute(xlinkLabel) != null && x.Attribute(xlinkLabel).Value == fto)) 
                                        {
                                            switch (el.Name.LocalName)
                                            {
                                                case "conceptName":
                                                    ConceptFilter cf = new ConceptFilter
                                                    {
                                                        Complement = complement
                                                        ,
                                                        Cover = cover
                                                        ,
                                                        Label = fto
                                                        ,
                                                        QNames = new List<string>()
                                                    };
                                                    foreach (XElement cfel in el.Elements(XName.Get("concept", "http://xbrl.org/2008/filter/concept")))
                                                    {
                                                        cf.QNames.Add(cfel.Element(XName.Get("qname", "http://xbrl.org/2008/filter/concept")).Value);
                                                    }
                                                    va.Filters.Add(cf);
                                                    break;
                                                case "explicitDimension":
                                                    va.Filters.Add(new ExplicitDimensionFilter
                                                    {
                                                        Complement = complement
                                                        ,
                                                        Cover = cover
                                                        ,
                                                        Label = fto
                                                        ,
                                                        Dimension = el.Element(XName.Get("dimension", "http://xbrl.org/2008/filter/dimension")).Element(XName.Get("qname", "http://xbrl.org/2008/filter/dimension")).Value
                                                        ,
                                                        Test = el.Attribute("test") != null ? el.Attribute("test").Value : null
                                                        ,
                                                        Members = el.Elements(XName.Get("member", "http://xbrl.org/2008/filter/dimension")).Count() > 0
                                                                    ?  el.Elements(XName.Get("member", "http://xbrl.org/2008/filter/dimension")).Select(e=>e.Element(XName.Get("qname", "http://xbrl.org/2008/filter/dimension")).Value).ToList()
                                                                    : null
                                                        /*Member = el.Element(XName.Get("member", "http://xbrl.org/2008/filter/dimension"))!=null 
                                                                    ? el.Element(XName.Get("member", "http://xbrl.org/2008/filter/dimension")).Element(XName.Get("qname", "http://xbrl.org/2008/filter/dimension")).Value
                                                                    : null*/
                                                    });
                                                    break;
                                                case "typedDimension":
                                                    va.Filters.Add(new TypedDimensionFilter
                                                    {
                                                        Complement = complement
                                                        ,
                                                        Cover = cover
                                                        ,
                                                        Label = fto
                                                        ,
                                                        Dimension = el.Element(XName.Get("dimension", "http://xbrl.org/2008/filter/dimension")).Element(XName.Get("qname", "http://xbrl.org/2008/filter/dimension")).Value
                                                        ,
                                                        Test = el.Attribute("test") != null ? el.Attribute("test").Value : null
                                                    });
                                                    break;
                                                case "period":
                                                    
                                                    PeriodFilter pf = new PeriodFilter
                                                    {
                                                        Complement = complement
                                                        ,
                                                        Cover = cover
                                                        ,
                                                        Label = fto
                                                        ,
                                                        Test = el.Attribute("test").Value
                                                        ,
                                                        PeriodTest = "duration"
                                                    };
                                                    if (!periodTests.Contains(pf.Test)) periodTests.Add(pf.Test);
                                                    if (pf.Test == "true()")
                                                    {
                                                        pf.PeriodTest = "ALL";
                                                    }
                                                    else
                                                    {
                                                        if (pf.Test.Contains("xfi:is-instant-period"))
                                                        {
                                                            if (pf.Test.Contains("PeriodStartDate"))
                                                            {
                                                                pf.PeriodTest = "I-Start";
                                                            }
                                                            else if (pf.Test.Contains("PeriodEndDate"))
                                                            {
                                                                pf.PeriodTest = "I-End";
                                                            }
                                                            else
                                                            {
                                                                pf.PeriodTest = "instant";
                                                            }
                                                        }
                                                    }
                                                    va.Filters.Add(pf);
                                                    break;
                                                default:
                                                    va.Filters.Add(new Filter
                                                    {
                                                        Complement = complement
                                                        ,
                                                        Cover = cover
                                                        ,
                                                        Label = fto
                                                    });
                                                    break;
                                            }
                                        }

                                    }
                                }
                                else if (varel.Name.LocalName == "generalVariable")
                                {
                                    
                                    va.Type = VariableTypes.General;
                                    va.Select = varel.Attribute("select").Value;

                                    FormulaParserResult fpr = new FormulaParser().TransformFormula(va.Select);
                                    va.SelectFormula = fpr;
                                   
                                    va.BindAsSequence = varel.Attribute("bindAsSequence") != null && varel.Attribute("bindAsSequence").Value == "true";
                                }
                                else if (varel.Name.LocalName == "loc")
                                {
                                    va.Type = VariableTypes.Parameter;
                                    va.Select = varel.Attribute(XName.Get("href","http://www.w3.org/1999/xlink")).Value;
                                }
                            }
                            aset.Variables.Add(va);

                        }
                        foreach (Variable vari in aset.Variables.Where(v => v.UsesVariables.Count > 0).ToList())
                        {
                            int idx = aset.Variables.IndexOf(vari);
                            foreach (string name in vari.UsesVariables)
                            {
                                Variable svari = aset.Variables.FirstOrDefault(v=>v.Name==name);
                                if(svari!=null) {
                                    int vidx = aset.Variables.IndexOf(svari);
                                    if (vidx > idx)
                                    {
                                        aset.Variables.RemoveAt(idx);
                                        idx = vidx;
                                        aset.Variables.Insert(idx, vari);
                                    }
                                }
                            }
                        }
                    }


                    //foreach aset.Variables
                    /*
                    foreach (XElement varc in defel.Elements(XName.Get("variableArc", "http://xbrl.org/2008/variable")))
                    {
                        Variable va = new Variable
                        {
                            Name = varc.Attribute("name").Value
                            ,
                            Origin = varc.Attribute(XName.Get("to", "http://www.w3.org/1999/xlink")).Value
                        };
                        aset.Variables.Add(va);

                    }

                    foreach (XElement garc in defel.Elements(XName.Get("generalVariable", "http://xbrl.org/2008/variable")))
                    {
                        string label = garc.Attribute(XName.Get("label", "http://www.w3.org/1999/xlink")).Value;
                        
                        Variable va = aset.Variables.FirstOrDefault(v => v.Origin == label);
                        if (va != null)
                        {
                            va.BindAsSequence = garc.Attribute("bindAsSequence").Value == "true";
                            va.Type = VariableTypes.General;
                            va.Select = garc.Attribute("select").Value;
                        }
                    }
                    foreach (XElement garc in defel.Elements(XName.Get("factVariable", "http://xbrl.org/2008/variable")))
                    {
                        string label = garc.Attribute(XName.Get("label", "http://www.w3.org/1999/xlink")).Value;

                        Variable va = aset.Variables.FirstOrDefault(v => v.Origin == label);
                        if (va != null)
                        {
                            va.BindAsSequence = garc.Attribute("bindAsSequence").Value == "true";
                            va.Type = VariableTypes.Fact;
                            va.Select = null;
                        }
                    }

                    foreach (XElement garc in defel.Elements(XName.Get("variableFilterArc", "http://xbrl.org/2008/variable")))
                    {
                        string from = garc.Attribute(XName.Get("from", "http://www.w3.org/1999/xlink")).Value;
                        string to = garc.Attribute(XName.Get("to", "http://www.w3.org/1999/xlink")).Value;
                        Variable va = aset.Variables.FirstOrDefault(v => v.Origin == from);
                        if (va != null)
                        {
                            XName lbl = XName.Get("label", "http://www.w3.org/1999/xlink");
                            foreach (XElement el in defel.Elements().Where(x => x.Attribute(lbl) != null && x.Attribute(lbl).Value == to))
                            {
                                if (el.Name.LocalName == "conceptName")
                                {
                                    va.Concept = new Concept();
                                    XElement qname = el.Element(XName.Get("concept", "http://xbrl.org/2008/filter/concept")).Element(XName.Get("qname", "http://xbrl.org/2008/filter/concept"));
                                    va.Concept.QName = qname.Value;

                                }
                            }
                          
                        }
                    }
                     * */
                    
                    tdir = System.IO.Path.Combine(targetdir, tdir);
                    if (!System.IO.Directory.Exists(tdir)) System.IO.Directory.CreateDirectory(tdir);
                    Program.ToXml(aset, false, "").Save(System.IO.Path.Combine(tdir, System.IO.Path.GetFileNameWithoutExtension(lnk.File) + cnt + ".xml"));

                    if (xformulaDoc != null)
                    {
                        aset.Assertions.First().Calculation = null;
                        // PARSE CALCULATION FROM FORMULA
                        FormulaSet fset = ParseFormulaXml(xformulaDoc);
                        aset.Formula = fset;
                        if (fset.Variables.Count(v => aset.Variables.Count(va => va.Name == v.Name) == 0) > 0)
                        {
                            
                            Console.WriteLine("non conforming variables found: " + string.Join(",", fset.Variables.Where(v => aset.Variables.Count(va => va.Name == v.Name) == 0).Select(v => v.Name).ToArray()));
                        }
                    }
                    else
                    {
                        if (aset.Assertions.Count>0 && aset.Assertions.First().Calculation != null)
                        {
                            Console.WriteLine("CALC IN EXCEL, NO FORMULA");
                        }
                    }

                    Taxonomy.Assertions.Add(aset);
                    cnt++;

                    //defel.Elements(XName.Get("factVariable", "http://xbrl.org/2008/variable"));
                    
                    //Console.WriteLine(defel.ToString());
                    
                    foreach (XElement el in defel.Elements())
                    {
                        XName nme = el.Name;
                        if (!nodenames.Contains(nme))
                        {
                            nodenames.Add(nme);
                        }
                    }
                     
                }

             }
            #endregion

             Program.ToXml(FormulaParser.PossibleUnkowns, true, "").Save(System.IO.Path.Combine(targetdir, "Possible_unknown_formulastatements.xml"));
             Program.ToXml(FormulaParser.Functions, true, "").Save(System.IO.Path.Combine(targetdir, "FunctionList.xml"));
             Console.Clear();
            /*
             XElement assertions = new XElement("assertionNodes");
             foreach (XName nme in nodenames)
             {
                 assertions.Add(new XElement("node", new XAttribute("name", nme.LocalName), new XAttribute("ns", nme.NamespaceName)));
                 Console.WriteLine(nme.NamespaceName + "::" + nme.LocalName);
             }
             assertions.Save(System.IO.Path.Combine(targetdir, "assertionnodes.xml"));
             Console.ReadLine();
             * */

            #region Compile summary
            //Console.WriteLine(Taxonomy.Elements.Select(el => el.Value).Where(el => el.LabelSets != null).SelectMany(el => el.LabelSets).Where(l => l.Labels != null).SelectMany(l => l.Labels).Count());
            //Console.WriteLine("Creating summary");
            //StreamWriter tw = System.IO.File.CreateText(System.IO.Path.Combine(targetdir, "summary-" + System.IO.Path.GetFileNameWithoutExtension(fname) + ".txt"));

            //tw.WriteLine("SUMMARY");
            //tw.WriteLine("========");
            //tw.WriteLine("");
            //tw.WriteLine("subgroups");
            //tw.WriteLine("---------");
            //foreach (string subgroups in Taxonomy.Elements.Select(e => e.Value).Select(e => e.SubstitutionGroup).Distinct().ToList())
            //{
            //    tw.WriteLine("> " + subgroups);
            //}

            //tw.WriteLine("");
            //tw.WriteLine("periodtypes");
            //tw.WriteLine("-----------");
            //foreach (string periodtype in Taxonomy.Elements.Select(e => e.Value).Select(e => e.PeriodType).Distinct().ToList())
            //{
            //    tw.WriteLine("> " + periodtype);
            //}

            //tw.WriteLine("");
            //tw.WriteLine("datatypes");
            //tw.WriteLine("-----------");
            //foreach (string datatype in Taxonomy.DataTypes.Keys.ToList())
            //{
            //    tw.WriteLine("> " + datatype);
            //}


            //tw.WriteLine("");
            //tw.WriteLine("roles");
            //tw.WriteLine("-----------");
            //foreach (Role rle in Taxonomy.Roles)
            //{
            //    tw.WriteLine("> " + rle.Href + ": " + rle.Uri);
            //}

            //tw.WriteLine("");
            //tw.WriteLine("links");
            //tw.WriteLine("-----------");
            //foreach (Link lnk in Taxonomy.Links)
            //{
            //    tw.WriteLine("> " + lnk.LinkType.ToString() + ": " + lnk.File);
            //}



            //tw.WriteLine("");
            //tw.WriteLine("HYPERCUBES");
            //tw.WriteLine("-----------");
            //foreach (Hypercube hc in Taxonomy.Hypercubes.Values)
            //{
             
            //    tw.WriteLine("> " + hc.Href); // + ": " + lnk.File);
            //    if (hc.Scenario != null)
            //    {
            //        foreach (Dimension dim in hc.Scenario)
            //        {
            //            tw.WriteLine("   > " + dim.Href);
            //            foreach (Domain dom in dim.Domains)
            //            {
            //                tw.WriteLine("      > " + dom.Id);
            //                foreach (DomainMember dm in dom.DomainMembers)
            //                {
            //                    tw.WriteLine("         > " + dm.Href);
            //                }
            //            }
            //        }
            //    }
            //}

            //tw.WriteLine("");
            //tw.WriteLine("PRIMARYCONCEPTS");
            //tw.WriteLine("---------------");
            //foreach (XbrlElement hc in Taxonomy.Elements.Values.Where(v => v.SubstitutionGroup == "http://www.xbrl.org/2003/instance:item"))
            //{
                
            //    tw.WriteLine("> " + hc.Href); // + ": " + lnk.File);
            //}

            //tw.WriteLine("");
            //tw.WriteLine("ELEMENS");
            //tw.WriteLine("-----------");
            //foreach (string href in Taxonomy.Elements.Keys)
            //{
            //    int cnt = Taxonomy.Elements[href].LabelSets!=null ? Taxonomy.Elements[href].LabelSets.Count : 0;
            //    tw.WriteLine("> " + href + " LABELSETS:"+ Taxonomy.Elements[href].SubstitutionGroup); // + ": " + lnk.File);
            //}

            //tw.Flush();
            //tw.Close();

            #endregion

            Taxonomy.Presentation = ApplyPresentationType(Taxonomy.Presentation);

            List<ContextDefinition> elcontexts = Taxonomy.Elements.Values.Where(v => v.ScenarioDefs != null).SelectMany(v => v.ScenarioDefs).ToList();
            List<ContextContent> elcc = elcontexts.Where(c=>c.Scenario!=null).SelectMany(c => c.Scenario).ToList();
            elcc.Add(new ContextContent { Dimensions = new List<ContextDimension>() });

            Taxonomy.ContextDefinitions = new Dictionary<string, ContextContent>();

            elcc.ForEach(c =>
            {
                if (!Taxonomy.ContextDefinitions.ContainsKey(c.GetXbrlId()))
                {
                    Taxonomy.ContextDefinitions.Add(c.GetXbrlId(), c);
                }
            }
            );

            Console.WriteLine("START GENERATING BACKEND");
            //BizTaxRenderer btr = new BizTaxRenderer(Taxonomy, parameters);
            BizTaxNewRenderer btr = new BizTaxNewRenderer(Taxonomy, parameters);
            Dictionary<string,List<string>> calcs = btr.Render(targetdir, true);

            // RENDER HTML's TO CHECK PRESENTATION
            
            string xmlDir = System.IO.Path.Combine(targetdir, "xml");
            if (!System.IO.Directory.Exists(xmlDir))
            {
                System.IO.Directory.CreateDirectory(xmlDir);
            }

            string htmlDir = System.IO.Path.Combine(targetdir, "htmlfiches");
            if (!System.IO.Directory.Exists(htmlDir))
            {
                System.IO.Directory.CreateDirectory(htmlDir);
            }

            string xsltDir = System.IO.Path.Combine(targetdir, "xsltfiches");
            if (!System.IO.Directory.Exists(xsltDir))
            {
                System.IO.Directory.CreateDirectory(xsltDir);
            }

            PresentationRenderer pr = new PresentationRenderer(Taxonomy, xmlDir,htmlDir,xsltDir, calcs);

            /*
            string htmlDir = System.IO.Path.Combine(targetdir, "html");
            if (!System.IO.Directory.Exists(htmlDir))
            {
                System.IO.Directory.CreateDirectory(htmlDir);
            }


            foreach (ElementPresentationLink epl in Taxonomy.Presentation)
            {

                XElement result = PresentationToHtml(epl);

                result.Save(System.IO.Path.Combine(htmlDir, epl.Code + "_" + epl.Href.Replace("#", "--") + ".html"));

            }
             * */


            Console.WriteLine("DONE PRESENTATION XML");


           // XbrlElement te = Taxonomy.Elements.Values.Where(x => x.Name.ToLower().Contains("AssociatedForeignCompanyCorporationCodeCurrentTaxPeriod".ToLower())).FirstOrDefault();
            
            Console.WriteLine("DONE reading taxonomy: " + Taxonomy.Id);


            int assy = int.Parse(parameters.AllParameters["CurrentAssessmentYear"].Value);
            
            Console.WriteLine("Generating GUI: " + Taxonomy.Id);
            GuiRenderer gr = new GuiRenderer(Taxonomy, assy, "nl", pr.GetValueLists());
            gr.Render(targetdir, true);

            gr = new GuiRenderer(Taxonomy, assy, "fr", pr.GetValueLists());
            gr.Render(targetdir, true);

            gr = new GuiRenderer(Taxonomy, assy, "en", pr.GetValueLists());
            gr.Render(targetdir, true);
             

            

            

            //Console.ReadLine();

        }

        private Regex reCode = new Regex(@"{(?<code>([^}]*))}");
        private Regex reQname = new Regex(@"QName\(([^\)]*)\)");
        private Regex reOperators = new Regex(@"[\(\)\+\-\*\/\.]"); //dumb

        private FormattedMessage GetFormattedMessage(string msg)
        {
            FormattedMessage fm = new FormattedMessage {
                Variables = new List<string>()
                
            };
          //  return fm;

            // TODO: Handle inline calculations with variables
            // Now (in order to move ahead) strip calc (VERY simplistic)

            MatchCollection mc = reCode.Matches(msg);
            for(int i = mc.Count-1; i>-1;i--)
            {
                Match ma = mc[i];
                if (ma.Groups["code"].Success == true)
                {
                    Group group =ma.Groups["code"];
                    string replacement = "";
                    if (group.Value.Trim().StartsWith("$"))
                    {
                        string var = group.Value.Trim().Substring(1);
                        if (var.Contains("IncreasedRateSMEsAllowanceCorporateEquityCurrentTaxPeriod"))
                        {
                            Console.WriteLine("How I miss right click here");
                        }
                        var splittedvar = reOperators.Split(var);
                        var = splittedvar[0];
                        // variable
                        if (!fm.Variables.Contains(var))
                        {
                            fm.Variables.Add(var);
                        }
                        replacement = "{" + fm.Variables.IndexOf(var) + "}"; ;
                    }
                    if (group.Value.StartsWith("xfi:concept-label"))
                    {
                        string clStr = ma.Value;
                        MatchCollection mcQ = reQname.Matches(clStr);
                        if (mcQ.Count==1)
                        {
                            string val = mcQ[0].Groups[1].Value;
                            string[] parts = val.Split(new char[] { ',' });
                            parts = parts.Select(s => s.Replace("'", "")).ToArray();
                            XbrlElement xel = Taxonomy.Elements.Values.FirstOrDefault(e => e.Name == parts[1].Trim() && e.NameSpace == parts[0].Trim());
                            clStr = reQname.Replace(clStr, "");
                            string[] xfiparts = clStr.Split(new char[] { ',' });
                            string lang = xfiparts.Last().Replace("'", "").Replace("}", "").Replace(")", "").Trim() ;
                            string role = xfiparts[xfiparts.Length - 2].Replace("'", "").Trim();
                            if (string.IsNullOrEmpty(role)) role = "http://www.xbrl.org/2003/role/label";
                            if (!role.Contains("http://www.xbrl.org/2003/role/")) role = "http://www.xbrl.org/2003/role/" + role;
                            if (xel != null)
                            {
                                replacement = xel.LabelSets.First(l=>l.Category==role).Labels.First(l => l.Language == lang).Text; //l=>l.Category=="
                            }
                        }
                    }

                    msg = msg.Substring(0,group.Index-1) + replacement + msg.Substring(group.Index+1+group.Length);
                    //ma.Groups["code"].Index
                }
                string conceptLabel = ma.Groups["concept"].Value;
                Console.WriteLine(conceptLabel);
            }

            fm.Message = msg;
            return fm;
        }

        private FormulaSet ParseFormulaXml(XDocument xdoc)
        {
            XElement defel = xdoc.Root.Element(XName.Get("link", "http://xbrl.org/2008/generic"));

            XElement xaset = defel.Element(XName.Get("formula", "http://xbrl.org/2008/formula"));
            XElement xPrecondition = defel.Element(XName.Get("precondition", "http://xbrl.org/2008/variable"));


            FormulaSet aset = new FormulaSet
            {
                Id = xaset.Attribute("id").Value
                ,
                Label = xaset.Attribute(XName.Get("label", "http://www.w3.org/1999/xlink")).Value
                ,
                Variables = new List<Variable>()
                ,
                Preconditions = xPrecondition != null
                                ? new ValueAssertion
                                {
                                    Id = xPrecondition.Attribute(XName.Get("label", "http://www.w3.org/1999/xlink")).Value
                                    ,
                                    Label = xPrecondition.Attribute(XName.Get("label", "http://www.w3.org/1999/xlink")).Value
                                    ,
                                    Test = xPrecondition.Attribute("test").Value
                                    ,
                                    Type = "PRECONDITION"
                                    ,
                                    Formula = new FormulaParser().TransformFormula(xPrecondition.Attribute("test").Value)
                                }
                                : null
                ,
                Formula = new XbrFormula
                {
                    Id = xaset.Attribute("id").Value
                    ,
                    AspectModel = xaset.Attribute("aspectModel").Value
                    ,
                    ImplicitFiltering = xaset.Attribute("implicitFiltering").Value == "true"
                    ,
                    Value = xaset.Attribute("value").Value
                    ,
                    Source = xaset.Attribute("source").Value
                    ,
                    Type = xaset.Attribute(XName.Get("type", "http://www.w3.org/1999/xlink")).Value
                    ,
                    Formula = new FormulaParser().TransformFormula(xaset.Attribute("value").Value)

                }
            };
            //Uri rle = new Uri(aset.Role);
            //string tdir = rle.Segments[rle.Segments.Length - 1];
            aset.Target = GetFormulaTarget(xaset);
            

           




            foreach (XElement varcs in defel.Elements(XName.Get("variableArc", "http://xbrl.org/2008/variable")).Where(x => x.Attribute(xlinkFrom) != null && x.Attribute(xlinkFrom).Value == aset.Id))
            {
                Variable va = new Variable
                {
                    Name = varcs.Attribute("name").Value
                    ,
                    Origin = varcs.Attribute(xlinkTo).Value
                    ,
                    FallBackvalue = null
                    ,
                    Filters = new List<Filter>()
                    ,
                    UsesVariables = new List<string>()
                };
                XName vafilterarc = XName.Get("variableFilterArc", "http://xbrl.org/2008/variable");
                foreach (XElement varel in defel.Elements().Where(x => x.Attribute(xlinkLabel) != null && x.Attribute(xlinkLabel).Value == va.Origin))
                {
                    if (varel.Name.LocalName == "factVariable")
                    {
                        va.Type = VariableTypes.Fact;
                        if (varel.Attribute("fallbackValue") != null) va.FallBackvalue = varel.Attribute("fallbackValue").Value;
                        va.BindAsSequence = varel.Attribute("bindAsSequence") != null && varel.Attribute("bindAsSequence").Value == "true";

                        // find filter arcs
                        foreach (XElement varFilter in defel.Elements(vafilterarc).Where(x => x.Attribute(xlinkFrom) != null && x.Attribute(xlinkFrom).Value == va.Origin))
                        {
                            bool complement = varFilter.Attribute("complement") != null && varFilter.Attribute("complement").Value == "true";
                            bool cover = varFilter.Attribute("cover") != null && varFilter.Attribute("cover").Value == "true";

                            string fto = varFilter.Attribute(xlinkTo).Value;

                            foreach (XElement el in defel.Elements().Where(x => x.Attribute(xlinkLabel) != null && x.Attribute(xlinkLabel).Value == fto))
                            {
                                switch (el.Name.LocalName)
                                {
                                    case "conceptName":
                                        ConceptFilter cf = new ConceptFilter
                                        {
                                            Complement = complement
                                            ,
                                            Cover = cover
                                            ,
                                            Label = fto
                                            ,
                                            QNames = new List<string>()
                                        };
                                        foreach (XElement cfel in el.Elements(XName.Get("concept", "http://xbrl.org/2008/filter/concept")))
                                        {
                                            cf.QNames.Add(cfel.Element(XName.Get("qname", "http://xbrl.org/2008/filter/concept")).Value);
                                        }
                                        va.Filters.Add(cf);
                                        break;
                                    case "explicitDimension":
                                        va.Filters.Add(new ExplicitDimensionFilter
                                        {
                                            Complement = complement
                                            ,
                                            Cover = cover
                                            ,
                                            Label = fto
                                            ,
                                            Dimension = el.Element(XName.Get("dimension", "http://xbrl.org/2008/filter/dimension")).Element(XName.Get("qname", "http://xbrl.org/2008/filter/dimension")).Value
                                            ,
                                            Test = el.Attribute("test") != null ? el.Attribute("test").Value : null
                                            ,
                                            Members = el.Elements(XName.Get("member", "http://xbrl.org/2008/filter/dimension")).Count() > 0
                                                        ? el.Elements(XName.Get("member", "http://xbrl.org/2008/filter/dimension")).Select(e => e.Element(XName.Get("qname", "http://xbrl.org/2008/filter/dimension")).Value).ToList()
                                                        : null
                                            /*Member = el.Element(XName.Get("member", "http://xbrl.org/2008/filter/dimension"))!=null 
                                                        ? el.Element(XName.Get("member", "http://xbrl.org/2008/filter/dimension")).Element(XName.Get("qname", "http://xbrl.org/2008/filter/dimension")).Value
                                                        : null*/
                                        });
                                        break;
                                    case "typedDimension":
                                        va.Filters.Add(new TypedDimensionFilter
                                        {
                                            Complement = complement
                                            ,
                                            Cover = cover
                                            ,
                                            Label = fto
                                            ,
                                            Dimension = el.Element(XName.Get("dimension", "http://xbrl.org/2008/filter/dimension")).Element(XName.Get("qname", "http://xbrl.org/2008/filter/dimension")).Value
                                            ,
                                            Test = el.Attribute("test") != null ? el.Attribute("test").Value : null
                                        });
                                        break;
                                    case "period":

                                        PeriodFilter pf = new PeriodFilter
                                        {
                                            Complement = complement
                                            ,
                                            Cover = cover
                                            ,
                                            Label = fto
                                            ,
                                            Test = el.Attribute("test").Value
                                            ,
                                            PeriodTest = "duration"
                                        };
                                        if (!periodTests.Contains(pf.Test)) periodTests.Add(pf.Test);
                                        if (pf.Test == "true()")
                                        {
                                            pf.PeriodTest = "ALL";
                                        }
                                        else
                                        {
                                            if (pf.Test.Contains("xfi:is-instant-period"))
                                            {
                                                if (pf.Test.Contains("PeriodStartDate"))
                                                {
                                                    pf.PeriodTest = "I-Start";
                                                }
                                                else if (pf.Test.Contains("PeriodEndDate"))
                                                {
                                                    pf.PeriodTest = "I-End";
                                                }
                                                else
                                                {
                                                    pf.PeriodTest = "instant";
                                                }
                                            }
                                        }
                                        va.Filters.Add(pf);
                                        break;
                                    default:
                                        va.Filters.Add(new Filter
                                        {
                                            Complement = complement
                                            ,
                                            Cover = cover
                                            ,
                                            Label = fto
                                        });
                                        break;
                                }
                            }

                        }
                    }
                    else if (varel.Name.LocalName == "generalVariable")
                    {

                        va.Type = VariableTypes.General;
                        va.Select = varel.Attribute("select").Value;

                        FormulaParserResult fpr = new FormulaParser().TransformFormula(va.Select);
                        va.SelectFormula = fpr;

                        va.BindAsSequence = varel.Attribute("bindAsSequence") != null && varel.Attribute("bindAsSequence").Value == "true";
                    }
                    else if (varel.Name.LocalName == "loc")
                    {
                        va.Type = VariableTypes.Parameter;
                        va.Select = varel.Attribute(XName.Get("href", "http://www.w3.org/1999/xlink")).Value;
                    }
                }
                aset.Variables.Add(va);

            }
            foreach (Variable vari in aset.Variables.Where(v => v.UsesVariables.Count > 0).ToList())
            {
                int idx = aset.Variables.IndexOf(vari);
                foreach (string name in vari.UsesVariables)
                {
                    Variable svari = aset.Variables.FirstOrDefault(v => v.Name == name);
                    if (svari != null)
                    {
                        int vidx = aset.Variables.IndexOf(svari);
                        if (vidx > idx)
                        {
                            aset.Variables.RemoveAt(idx);
                            idx = vidx;
                            aset.Variables.Insert(idx, vari);
                        }
                    }
                }
            }

            return  aset;
        }

        private Variable GetFormulaTarget(XElement xaset)
        {
            Variable va = new Variable
            {
                Name = "FormulaTarget"
                ,
                Filters = new List<Filter>()
                ,
                Type = VariableTypes.Fact
            };
            XElement aspects = xaset.Element(XName.Get("aspects", "http://xbrl.org/2008/formula"));
            if (aspects != null)
            {
                foreach (XElement el in aspects.Elements())
                {
                    switch (el.Name.LocalName)
                    {
                        case "concept":
                            va.Filters.Add(new ConceptFilter
                            {
                                Complement = false
                                ,
                                Cover = false
                                ,
                                Label = ""
                                ,
                                QNames = new List<string>
                                {
                                    el.Element(XName.Get("qname", "http://xbrl.org/2008/formula")).Value
                                }
                            });
                            break;
                        case "explicitDimension":
                            ExplicitDimensionFilter edf = new ExplicitDimensionFilter
                            {
                                Complement=false,
                                Cover=false,
                                Dimension=el.Attribute("dimension").Value,
                                Label = "",
                                Members= new List<string>()
                            };
                            if (el.Element(XName.Get("omit", "http://xbrl.org/2008/formula")) != null)
                            {
                                edf.Complement = true;
                            }
                            foreach (XElement xemem in el.Elements(XName.Get("member", "http://xbrl.org/2008/formula")))
                            {
                                edf.Members.Add(xemem.Element(XName.Get("qname", "http://xbrl.org/2008/formula")).Value);
                            }
                            va.Filters.Add(edf);
                            break;
                        case "typedDimension":
                            TypedDimensionFilter tdf = new TypedDimensionFilter
                            {
                                Complement = false,
                                Cover = false,
                                Dimension = el.Attribute("dimension").Value,
                                Label = ""
                            };
                            if (el.Element(XName.Get("omit", "http://xbrl.org/2008/formula")) != null)
                            {
                                tdf.Complement = true;
                            }
                            va.Filters.Add(tdf);
                            break;
                        case "period":
                            PeriodFilter pf = new PeriodFilter
                            {
                                Complement = false
                                ,
                                Cover = false
                                ,
                                Label = ""
                                ,
                                PeriodTest = ""
                                ,
                                Test = ""
                            };
                            XElement child = el.Elements().First();

                           
                            
                            if (child.Name.LocalName == "duration")
                            {
                                pf.PeriodTest = "D";
                                pf.Test = "D";
                                if (child.Element(XName.Get("omit", "http://xbrl.org/2008/formula")) != null)
                                {
                                    pf.Complement = true;
                                }
                            }
                            else
                            {
                                if (child.Name.LocalName == "instant")
                                {
                                    pf.Test = child.Attribute("value").Value;
                                    if (pf.Test.Contains("PeriodStartDate"))
                                    {
                                        pf.PeriodTest = "I-Start";
                                    }
                                    else
                                    {
                                        pf.PeriodTest = "I-End";
                                    }
                                    if (child.Element(XName.Get("omit", "http://xbrl.org/2008/formula")) != null)
                                    {
                                        pf.Complement = true;
                                    }
                                }
                            }
                            va.Filters.Add(pf);
                            break;
                        case "unit":
                            break;
                        default:
                            Console.WriteLine("Unknown aspect element in target");
                            break;
                    }
                }

            }
            return va;
        }

        private XElement PresentationToHtml(ElementPresentationLink epl)
        {
            XElement html = new XElement("html", new XElement("head"));
            XElement body = new XElement("body");
            body.Add(RenderToHtml(epl));
            html.Add(body);
            return html;
        }

        private XElement RenderToHtml(ElementPresentationLink epl)
        {
            int cells = 3;
            XElement table = new XElement("table", new XAttribute("style", "width:100%;border-left:1px dashed #CCC;")
                ,new XAttribute("xbrlid",epl.Href));
            XElement row = new XElement("tr"
                ,new XAttribute("onmouseover","this.style.backgroundColor='#F0F000';")
                , new XAttribute("onmouseout", "this.style.backgroundColor='transparent';")
            );

            row.Add(new XElement("td", new XAttribute("style", "width:100px;border-bottom:1px solid #CCC;"), epl.Code + " "));
            row.Add(new XElement("td", new XAttribute("style","text-align:left;border-bottom:1px solid #CCC;"), epl.Labels.First(l => l.Language == "nl").Text));

            if (epl.Scenario != null)
            {
                if (epl.Scenario.Scenario != null)
                {
                    foreach (ContextContent cc in epl.Scenario.Scenario)
                    {
                        row.Add(new XElement("td", new XAttribute("style", "width:100px;border-bottom:1px solid #CCC;color:#C00;"), cc.Id));
                        cells++;
                    }
                }
            }


            XElement periods = new XElement("td", new XAttribute("style", "width:200px;text-align:right;color:#0000CC;border-bottom:1px solid #CCC;"));

            

            XbrlElement xel = Taxonomy.Elements[epl.Href];
            List<string> periodlst = GetPresentationLocations(xel.LabelSets);
            periods.Add(string.Join(",",periodlst.ToArray()));
            row.Add(periods);
                //, new XElement("div", epl.Labels.First(l => l.Language == "nl").Text + "(" + epl.Code+")"));
            table.Add(row);
            if (epl.Children != null && epl.Children.Count > 0)
            {
                row = new XElement("tr");
                XElement children = new XElement("td", new XAttribute("colspan", cells), new XAttribute("style","padding-left:10px"));
                foreach (ElementPresentationLink eplChild in epl.Children)
                {
                    children.Add(RenderToHtml(eplChild));
                }
                row.Add(children);
                table.Add(row);
            }

            return table;
        }

        private void ApplyRelations(XbrlElement xel, XbrlElement parent)
        {
            //HYPERCUBE-ALL" || arcrole == "HYPERCUBE-NOTALL"
            // apply parent relations
            string href = xel.Href;
            //xel.Contexts
            if (parent != null)
            {
                List<XbrlRelation> rels = parent.Relations.Where(x => x.ArcRole == "DOMAIN-MEMBER" && x.To == href).ToList();
                foreach (XbrlRelation xrel in parent.Relations.Where(x => x.ArcRole == "DOMAIN-MEMBER" && x.To == href))
                {
                    List<ContextDefinition> parCtx = parent.ScenarioDefs.Where(s => s.AppliedRole == xrel.Role).ToList();
                    if (xel.ScenarioDefs == null) xel.ScenarioDefs = new List<ContextDefinition>();
                    // remove existing same applied roles
                    xel.ScenarioDefs.RemoveAll(s => s.AppliedRole == xrel.Role);
                    
                    xel.ScenarioDefs.AddRange(ProtoBuf.Serializer.DeepClone(parCtx));
                    //xel.con
                }
            }

            // apply positives
            if (xel.Relations != null)
            {
                foreach (XbrlRelation xlrel in xel.Relations.Where(r => r.ArcRole == "HYPERCUBE-ALL"))
                {
                    string hckey = string.Format("{0}::{1}", xlrel.To, xlrel.TargetRole);

                    Hypercube hc = null;
                    if (!Taxonomy.Hypercubes.ContainsKey(hckey))
                    {
                        hc = new Hypercube { Href = xlrel.To, Id = hckey, RoleId = xlrel.TargetRole, Closed = true, Scenario = new List<Dimension>(), Segment = new List<Dimension>() };
                        hc.ScenarioDef = new ContextDefinition
                        {
                            HypercubeRole = xlrel.TargetRole
                            ,
                            Scenario = new List<ContextContent>()
                            ,
                            Segment = new List<ContextContent>()
                        };
                        Taxonomy.Hypercubes.Add(hckey, hc);
                    }
                    else
                    {
                        hc = Taxonomy.Hypercubes[hckey];
                    }

                     
                        // xel.con
                    ContextDefinition hcd = hc.ScenarioDef;
                    if (xel.ScenarioDefs == null) xel.ScenarioDefs = new List<ContextDefinition>();

                    // FOR NOW: REMOVE, should propbably be an added possibility ?
                    // although, normally the full hypercube is applied and then slightened down using notAll relations
                    if (xel.ScenarioDefs.Count(c => c.AppliedRole == xlrel.Role) > 0) xel.ScenarioDefs.RemoveAll(c => c.AppliedRole == xlrel.Role);

                    xel.ScenarioDefs.Add(new ContextDefinition
                    {
                        AppliedRole = xlrel.Role
                        ,
                        HypercubeRole = hc.ScenarioDef.HypercubeRole
                        , Def = xlrel.Deffile
                        ,
                        Scenario = hc.ScenarioDef.Scenario != null ? ProtoBuf.Serializer.DeepClone(hc.ScenarioDef.Scenario) : new List<ContextContent>()
                        ,
                        Segment = hc.ScenarioDef.Segment != null ? ProtoBuf.Serializer.DeepClone(hc.ScenarioDef.Segment) : new List<ContextContent>()
                    });
                    xel.ScenarioDefs.Add(hc.ScenarioDef);

                }
                // apply negatives
                foreach (XbrlRelation xlrel in xel.Relations.Where(r => r.ArcRole == "HYPERCUBE-NOTALL"))
                {
                    string hckey = string.Format("{0}::{1}", xlrel.To, xlrel.TargetRole);
                    
                    if (Taxonomy.Hypercubes.ContainsKey(hckey))
                    {
                        Hypercube hc = Taxonomy.Hypercubes[hckey];
                        // xel.con
                        ContextDefinition hcd = hc.ScenarioDef;
                        if (xel.ScenarioDefs == null) xel.ScenarioDefs = new List<ContextDefinition>();

                        // FOR NOW: REMOVE, should propbably be an added possibility ?
                        // although, normally the full hypercube is applied and then slightened down using notAll relations
                        
                            foreach (ContextDefinition cd in xel.ScenarioDefs.Where(s=>s.AppliedRole==xlrel.Role).ToList())
                            {
                                foreach (ContextContent cc in hcd.Scenario)
                                {
                                    cd.Scenario.RemoveAll(c => c.Id == cc.Id);
                                }
                            }
                        
                    }
                    else
                    {
                        Console.WriteLine("COULD NOT FIND HYPERCUBE '" + xlrel.To + "' WITH ROLE '" + xlrel.TargetRole + "'");
                    }
                }

                // apply inheritance
                foreach (XbrlRelation xlrel in xel.Relations.Where(r => r.ArcRole == "DOMAIN-MEMBER"))
                {
                    ApplyRelations(Taxonomy.Elements[xlrel.To], xel);
                }
            }
        }

        

        private Hypercube CreateHyperCube(string key, string hcRole, XbrlElement hcEl)
        {
            Hypercube hc = new Hypercube { Href = hcEl.Href, Id = key, RoleId = hcRole, Closed = false, Scenario = new List<Dimension>(), Segment = new List<Dimension>() };

            foreach (XbrlRelation hcDim in hcEl.Relations.Where(r => r.Role == hcRole && r.ArcRole == "HYPERCUBE-DIMENSION"))
            {
                //scenario or segment
                List<Dimension> contextContainer = hc.Scenario;
                if (hcDim.ContextElement == "segment") contextContainer = hc.Segment;

                Dimension ndim = new Dimension { DefRole = hcDim.Role, Href = hcDim.To, Id = hcDim.To, Domains = new List<Domain>() };
                string dimensionRole = hcDim.TargetRole;
                XbrlElement dimel = Taxonomy.Elements[ndim.Href];
                if (!string.IsNullOrEmpty(dimel.TypedDomainRef))
                {
                    ndim.DimensionType = DimensionTypes.TypedDimension;
                    ndim.DataType = GetDataType(dimel.TypedDomainRef, null);
                    dimel.DataType = ndim.DataType;
                }
                else
                {
                    //DIMENSION-DOMAIN DOMAIN-MEMBER
                    ndim.DimensionType = DimensionTypes.ExplicitDimension;
                    ndim.Domains = new List<Domain>();

                    if (dimel.Relations != null)
                    {
                        foreach (XbrlRelation hcDoms in dimel.Relations.Where(r => r.Role == dimensionRole && r.ArcRole == "DIMENSION-DOMAIN"))
                        {
                            XbrlElement domEl =  Taxonomy.Elements[hcDoms.To];
                            Domain dom = new Domain { Id = hcDoms.To, DomainMembers = new List<DomainMember>() };
                            
                            string domRole = hcDoms.TargetRole;
                            if (domEl.Relations != null)
                            {
                                foreach (XbrlRelation hcDomMember in domEl.Relations.Where(r => r.Role == domRole && r.ArcRole == "DOMAIN-MEMBER"))
                                {
                                    dom.DomainMembers.Add(new DomainMember { Href = hcDomMember.To, Id = hcDomMember.To, Element = Taxonomy.Elements[hcDomMember.To] });
                                }
                            }

                            ndim.Domains.Add(dom);
                        }
                    }
                }
                contextContainer.Add(ndim);
                               
            }
            CreateContext(ref hc);
            return hc;

        }

        private string GetPresParentContexts(ElementPresentationLink xfrom)
        {
            if (xfrom.Context != null) return xfrom.Context.Id;
            if (xfrom.Parent != null) return GetPresParentContexts(xfrom.Parent);
            return null;
        }

        private ElementPresentationLink ParsePresentationLinks(Dictionary<string, PresentationLink> preselements, PresentationLink pl)
        {
            return ParsePresentationLinks(preselements, pl, null);
        }

        private ElementPresentationLink ParsePresentationLinks(Dictionary<string, PresentationLink> preselements, PresentationLink pl, ElementPresentationLink parent)
        {
            ElementPresentationLink epl = ProtoBuf.Serializer.DeepClone(pl.BareElementPresentationLink);
            epl.Parent = parent;
            if (epl.Labels == null)
            {
            }

            ApplyContextToPresentation(ref epl,pl);

            if (epl.Children == null) epl.Children = new List<ElementPresentationLink>();

            if (pl.Children == null) pl.Children = new List<PresentationLink>();
            foreach (string plChildId in pl.Children.Select(c=>c.Id).Distinct())
            {
                PresentationLink plChild = preselements.ContainsKey(plChildId) ? preselements[plChildId] : pl.Children.First(c=>c.Id==plChildId);
                decimal order = pl.Children.First(c => c.Id == plChildId).Order;
                plChild.BareElementPresentationLink.Order = order;
                ElementPresentationLink eplChild = ParsePresentationLinks(preselements, plChild,epl);
                
                // SHOULD NEVER CONTAIN DOUBLES...
                //if(epl.Children.Count(c=>c.Href==eplChild.Href)==0)
                epl.Children.Add(eplChild);
            }
            epl.Children = epl.Children.OrderBy(e => e.Order).ToList();
            return epl;
        }

        
        private void ApplyContextToPresentation(ref ElementPresentationLink epl, PresentationLink pl) {

            XbrlElement xbel = Taxonomy.Elements[epl.Href];

            int cntCtx = xbel.ScenarioDefs != null ? xbel.ScenarioDefs.Count : 0;
            ContextDefinition cd = null;
            if (cntCtx == 0)
            {
                if (epl.Parent != null && xbel.Abstract)
                {
                    if (epl.Parent.Scenario != null)
                    {
                        epl.AbstractScenario = epl.Parent.Scenario;
                    }
                    else if (epl.Parent.AbstractScenario != null)
                    {
                        epl.AbstractScenario = epl.Parent.AbstractScenario;
                    }
                   
                }
                // element has no scenario = normal.
                //Console.WriteLine("No scenario's found");
            }
            else if (cntCtx == 1)
            {
                cd = xbel.ScenarioDefs.First();
            }
            else
            {
                // get same as def file
                // FIRST search filtering by same file tag (?)
              
                List<ContextDefinition> applyableList = xbel.ScenarioDefs.Where(s => s.Def == pl.Presfile).ToList();
                if (applyableList.Count == 1)
                {
                    cd = applyableList.First();
                }
                else
                {
                    // FIND USING PARENT's AND fellow children
                    if(epl.Parent!=null) {
                        ContextDefinition cdparent = null;

                        if (epl.Parent.Scenario != null)
                        {
                            cdparent = epl.Parent.Scenario;
                        }
                        else if (epl.Parent.AbstractScenario != null)
                        {
                            cdparent = epl.Parent.AbstractScenario;
                        }

                        // find using parent
                        if (cdparent != null)
                        {
                            // CHECK EXACT DIMENSIONS AND CONTENT
                            cd = xbel.ScenarioDefs.FirstOrDefault(c => c.Id == cdparent.Id);
                            if (cd == null)
                            {
                                // CHECK EXACT DIMENSIONS ONLY
                                cd = applyableList.FirstOrDefault(c => c.DefId == cdparent.DefId);
                            }
                        }
                        if (cd == null)
                        {
                            // try previous childs
                            if (epl.Parent.Children != null)
                            {
                                for (int i = epl.Parent.Children.Count - 1; i > -1; i--)
                                {
                                    ElementPresentationLink prevChild = epl.Parent.Children[i];
                                    if (prevChild.Scenario != null)
                                    {
                                        
                                       // CHECK EXACT DIMENSIONS AND CONTENT
                                        cd = xbel.ScenarioDefs.FirstOrDefault(c => c.Id == prevChild.Scenario.Id);
                                        if (cd == null)
                                        {
                                            // CHECK EXACT DIMENSIONS ONLY
                                            cd = applyableList.FirstOrDefault(c => c.DefId == prevChild.Scenario.DefId);
                                        }
                                        if (cd != null) break;
                                        
                                    }
                                }
                            }
                        }

                    }
                    if (cd == null)
                    {
                        List<string> check = xbel.ScenarioDefs.Select(c => c.Id).Distinct().ToList();
                        if (check.Count == 1)
                        {
                            cd = xbel.ScenarioDefs.First();
                        }
                        else
                        {
                            Debug.Fail("HELP, can't find from's scenario :-(");
                        }
                    }
                    
                    
                }




            }

            // FIX FOR MISSING DEFINITIONS IN FOD TAXONOMY
            if (epl.Parent!=null && epl.Parent.AbstractScenario != null && cd!=null && cd.Scenario!=null)
            {
                ContextDefinition cdp = epl.Parent.AbstractScenario;
                if (cdp.DefId == cd.DefId)
                {

                    if (cdp.Scenario.Count == cd.Scenario.Count(c => cdp.Scenario.Count(s=>s.Id==c.Id)==1))
                    {
                        
                        Console.WriteLine("FIX FOR MISSING DEFINITIONS IN FOD TAXONOMY: " + epl.Href);
                        cd = ProtoBuf.Serializer.DeepClone(cdp);
                        
                    }
                }
            }

            epl.Scenario = cd;
            if (epl.AbstractScenario == null && xbel.Abstract) epl.AbstractScenario = epl.Scenario;
            
        }
        

        private List<ElementPresentationLink> ApplyPresentationType(List<ElementPresentationLink> list)
        {
            foreach (ElementPresentationLink epl in list)
            {
                XbrlElement xel = Taxonomy.Elements[epl.Href];
                if(epl.Children!=null && epl.Children.Count>0) {
                    epl.Children = ApplyPresentationType(epl.Children);

                    if (epl.Children.Count == 2 && epl.Children[0].PresentationType == ElementPresentationType.ValueList
                        )
                    {
                        // valuelist or other container
                        epl.PresentationType = ElementPresentationType.ValueOrOther;
                    }
                    else
                    {
                        epl.PresentationType = ElementPresentationType.Panel;
                        if (epl.Href.StartsWith("pfs-vl") && epl.Href.EndsWith("CodeHead"))
                        {
                            epl.PresentationType = ElementPresentationType.ValueList;
                            epl.DataType = "ELEMENT";
                        }
                        else
                        {
                            // CHECK ELEMENT AND CHILD DIMENSIONS.
                            bool iamset = false;
                            
                            List<ElementPresentationLink> panelChilds = epl.Children.Where(c => c.Children != null && c.Children.Count > 0).ToList();
                            List<ElementPresentationType> childPresTypes = panelChilds.Select(c => c.PresentationType).Distinct().ToList();

                            if (epl.Children.Count == panelChilds.Count)
                            {
                                if (childPresTypes.Count == 1)
                                {
                                    epl.PresentationType = childPresTypes.First();
                                    epl.Context = GetCombinedDimensionSet(epl.Children);
                                }
                                else
                                {
                                    epl.PresentationType = ElementPresentationType.Panel;
                                }

                            }
                            else
                            {
                                if (childPresTypes.Count > 1)
                                {
                                    epl.PresentationType = ElementPresentationType.Panel;
                                }
                                else
                                {
                                    ElementDimensionSet eds = null;
                                    if (childPresTypes.Count == 0)
                                    {
                                        eds = GetCombinedDimensionSet(epl.Children.Where(c => c.Children == null || c.Children.Count == 0).ToList());
                                    }
                                    else
                                    {
                                        eds = GetCombinedDimensionSet(epl.Children.Where(c => c.Children == null || c.Children.Count == 0).ToList(), GetCombinedDimensionSet(epl.Children));
                                    }

                                    if (eds != null)
                                    {
                                        int emptyDimensions = eds.Dimensions.Count(d=>d.Id==null);
                                        eds.Dimensions.RemoveAll(d => d.Id == null);
                                        int dimensions = eds.Dimensions.Count();
                                        int typedDimensions = eds.Dimensions.Count(c => c.DimensionType == DimensionTypes.TypedDimension);
                                        int explicitDimensions = eds.Dimensions.Count(c => c.DimensionType == DimensionTypes.ExplicitDimension);
                                        int multiMemberedExplicitDimensions = eds.Dimensions.Count(c => c.DimensionType == DimensionTypes.ExplicitDimension && c.Domains != null && c.Domains.Count(d => d.DomainMembers != null && d.DomainMembers.Count > 1) > 0);

                                        if (dimensions > 0 && emptyDimensions == 0)
                                        {
                                            epl.PresentationType = ElementPresentationType.ContextGrid;

                                            if (typedDimensions == 0)
                                            {
                                                if (explicitDimensions == 1 && dimensions == 1) epl.PresentationType = ElementPresentationType.Pivot;
                                                if (explicitDimensions > 1 && multiMemberedExplicitDimensions == 1) epl.PresentationType = ElementPresentationType.Pivot;

                                            }
                                            //if(epl.
                                            epl.Context = eds;
                                        }


                                    }
                                }
                                // construct gathered dimensionset of field children
                                 

                            }
                            
                            /*
                            if (epl.Children.Count(c => c.PresentationType == ElementPresentationType.Pivot) == epl.Children.Count)
                            {
                                epl.PresentationType = ElementPresentationType.Pivot;
                            }
                            if (epl.Children.Count(c => c.PresentationType == ElementPresentationType.ContextGrid) == epl.Children.Count)
                            {
                                epl.PresentationType = ElementPresentationType.ContextGrid;
                            }
                            


                            if(epl.Context== null || epl.Context.Dimensions==null) {

                                // create new concatenated dimension list
                                
                            }

                            int emptyDimensions = epl.Children.Count(c => c.Context == null || c.Context.Dimensions == null);
                            int dimensions = epl.Context.Dimensions.Count();
                            int typedDimensions = epl.Context.Dimensions.Count(c=>c.DimensionType== DimensionTypes.TypedDimension);
                            int explicitDimensions = epl.Context.Dimensions.Count(c=>c.DimensionType== DimensionTypes.ExplicitDimension);
                            int multiMemberedExplicitDimensions = epl.Context.Dimensions.Count(c=>c.DimensionType==DimensionTypes.ExplicitDimension && c.Domains!=null && c.Domains.Count(d=>d.DomainMembers!=null && d.DomainMembers.Count>1) > 0);

                            if (dimensions > 0 && emptyDimensions==0)
                            {
                                epl.PresentationType = ElementPresentationType.ContextGrid;

                                if (typedDimensions == 0)
                                {
                                    if (explicitDimensions == 1 && dimensions == 1) epl.PresentationType = ElementPresentationType.Pivot;
                                    if (explicitDimensions > 1 && multiMemberedExplicitDimensions == 1) epl.PresentationType = ElementPresentationType.Pivot;

                                }
                            }
                            /*
                            

                            if ((epl.Context != null && epl.Context.Dimensions != null && epl.Context.Dimensions.Count > 0)
                                || epl.Children.Count(c => c.Context != null
                                                                && c.Context.Dimensions != null
                                                                && c.Context.Dimensions.Count > 0) > 0
                                )
                            {
                                epl.PresentationType = ElementPresentationType.ContextGrid;

                               // if( (epl.Context != null && epl.Context.Dimensions!=null && epl.Context.Dimensions
                            }
                            if (epl.Context != null && epl.Context.Dimensions != null && epl.Context.Dimensions.Count > 0)
                            {
                                if (epl.Context.Dimensions.Count(d => d.DimensionType == DimensionTypes.TypedDimension) > 0)
                                {
                                    epl.PresentationType = ElementPresentationType.ContextGrid;
                                }
                                else if (epl.Context.Dimensions.Count > 1 && epl.Context.Dimensions.Count(d => d.Domains.Count(dom => dom.DomainMembers.Count > 1) > 0) > 1)
                                {
                                    epl.PresentationType = ElementPresentationType.ContextGrid;
                                }
                                else
                                {
                                    epl.PresentationType = ElementPresentationType.Pivot;
                                }
                            }
                            else if (epl.Children.Count(c => c.Context != null
                                                                && c.Context.Dimensions != null
                                                                && c.Context.Dimensions.Count(d => d.DimensionType == DimensionTypes.TypedDimension) > 0) > 0)
                            {
                                epl.PresentationType = ElementPresentationType.ContextGrid;
                            }
                            else if (epl.Children.Count(c => c.Context != null
                                                        && c.Context.Dimensions !=null
                                                        && c.Context.Dimensions.Count(d => d.DimensionType == DimensionTypes.ExplicitDimension) > 0) == epl.Children.Count
                                && epl.Children.Where(c=>c.Context != null ).Select(c =>c.Context.ToString()).Distinct().Count()<=1
                                ) 
                                
                            {
                                epl.PresentationType = ElementPresentationType.Pivot;
                                epl.Context = epl.Children.Select(c => c.Context).Distinct().First();
                            }
                            */
                        }
                        
                    }

                    if (epl.PresentationType != ElementPresentationType.ValueOrOther && !xel.Abstract)
                    {
                        if (xel.DataType != null)
                        {
                            epl.DataType = xel.DataType.Name;
                        }
                        else
                        {
                            if (xel.EstimatedType == "ELEMENT")
                            {
                                epl.DataType = "ELEMENT";
                            }
                        }
                    }

                   
                } else {
                    if (epl.Href.StartsWith("pfs-vl") && epl.Href.EndsWith("CodeHead"))
                    {
                        epl.PresentationType = ElementPresentationType.ValueList;
                        epl.DataType = "ELEMENT";
                    }
                    else 
                    {
                        epl.PresentationType = ElementPresentationType.Field;
                        epl.DataType = xel.DataType != null ? xel.DataType.Name : xel.EstimatedType;
                    }
                }
                /*
                if (xel.DataType == null)
                {
                    if(xel.Abstract
                }
                else
                {
                }
                 */
            }
            return list;
        }

        private ElementDimensionSet GetCombinedDimensionSet(List<ElementPresentationLink> list, ElementDimensionSet eds)
        {


            List<Dimension> dims = list.Where(c => c.Context != null && c.Context.Dimensions != null).SelectMany(c => c.Context.Dimensions).Distinct().ToList();
            if (list.Count(c => c.Context == null || c.Context.Dimensions == null || c.Context.Dimensions.Count == 0) > 0)
            {
                dims.Add(new Dimension());
            }

            foreach (Dimension dim in dims)
            {
                if (eds.Dimensions.Count(d => d.Id == dim.Id) == 0)
                {
                    eds.Dimensions.Add(dim);
                }
                else
                {
                    Dimension foundDim = eds.Dimensions.First(d => d.Id == dim.Id);
                    if (foundDim.Domains == null)
                    {
                        if (dim.Domains != null)
                        {
                            foundDim.Domains = dim.Domains;
                        }

                    }
                    else
                    {
                        foreach (Domain dom in dim.Domains)
                        {
                            if (foundDim.Domains.Count(d => d.Id == dom.Id) == 0)
                            {
                                foundDim.Domains.Add(dom);
                            }
                            else
                            {
                                Domain foundDom = foundDim.Domains.First(d => d.Id == dom.Id);
                                if (foundDom.DomainMembers == null)
                                {
                                    if (dom.DomainMembers != null)
                                    {
                                        foundDom.DomainMembers = dom.DomainMembers;
                                    }

                                }
                                else
                                {
                                    foreach (DomainMember mem in dom.DomainMembers)
                                    {
                                        if (foundDom.DomainMembers.Count(m => m.Id == mem.Id) == 0)
                                            foundDom.DomainMembers.Add(mem);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return eds;
        }

        private ElementDimensionSet GetCombinedDimensionSet(List<ElementPresentationLink> list)
        {
            ElementDimensionSet eds = new ElementDimensionSet
            {
                Dimensions = new List<Dimension>()
            };

            return GetCombinedDimensionSet(list, eds);


        }

        private XElement ToPresentationElement(string href,string order, string arcrole)
        {
            XbrlElement xbel = Taxonomy.Elements[href];

           // Context ctx = xbel.Contexts != null ? xbel.Contexts.FirstOrDefault(c => c.SourceXsd == sourceXsd) : null;
          
            return new XElement("item"
                        , new XAttribute("id", href)
                        , new XAttribute("lbl", GetLabel(xbel.LabelSets))
                        , new XAttribute("order", order)
                        , new XAttribute("isAbstract", xbel.Abstract)
                        , new XAttribute("isTuple", xbel.SubstitutionGroup == "http://www.xbrl.org/2003/instance:tuple")
                        , new XAttribute("isItem", xbel.SubstitutionGroup == "http://www.xbrl.org/2003/instance:item")
                        , new XAttribute("type", xbel.DataType!=null ? xbel.DataType.Name : string.Empty)
                        , new XAttribute("presentationlocations", GetPresentationLocations(xbel.LabelSets))
                        , new XAttribute("period", !string.IsNullOrEmpty(xbel.PeriodType) ? xbel.PeriodType : "")
                        );
        }

        private void CreateContext(ref Hypercube hc)
        {
            hc.ScenarioDef = new ContextDefinition
            {
                HypercubeRole = hc.RoleId
                ,
                Scenario = new List<ContextContent>()
                ,
                Segment = new List<ContextContent>()
            };

            // gather all data
            List<IEnumerable<string>> dimensions = new List<IEnumerable<string>>();
            foreach (Dimension dim in hc.Scenario)
            {
                List<string> dimData = new List<string>();
                string id = dim.Id;
                if (dim.DimensionType == DimensionTypes.ExplicitDimension)
                {
                    if (dim.Domains != null)
                    {
                        foreach (Domain dom in dim.Domains)
                        {
                            id = dim.Id + "|" + dom.Id;
                            if (dom.DomainMembers != null)
                            {
                                foreach (DomainMember mem in dom.DomainMembers)
                                {
                                    id = dim.Id + "|" + dom.Id + "|" + mem.Id;
                                    dimData.Add(id);
                                }
                            }
                        }
                    }
                }
                else
                {
                    dimData.Add(id);
                }
                dimensions.Add(dimData);
            }

            // perform cartesian product (all possible combinations)
            var cp = dimensions.CartesianProduct();

            // Create contexts
            foreach (var line in cp)
            {
                ContextContent cc = new ContextContent { Dimensions = new List<ContextDimension>() };
                foreach (string dimId in line)
                {
                    string[] href = dimId.Split(new char[] { '|' });
                    Dimension dim = hc.Scenario.First(d => d.Id == href[0]);
                    ContextDimension cdim = new ContextDimension {
                            DataType = dim.DataType
                            , DefRole = dim.DefRole
                            , DimensionType = dim.DimensionType
                            , Domain = null
                            , Href = dim.Href
                            , Name = dim.Name
                            , XbrlId = dim.Id
                    };
                    if (dim.DimensionType== DimensionTypes.ExplicitDimension)
                    {
                        if (href.Length > 1)
                        {
                            Domain dom = dim.Domains.First(d=>d.Id==href[1]);
                            cdim.Domain = new ContextDomain
                            {
                                DomainMember = null
                                ,
                                Name = dom.Name
                                ,
                                XbrlId = dom.Id
                            };

                            if (href.Length > 2)
                            {
                                DomainMember dommem = dom.DomainMembers.First(d => d.Id == href[2]);
                                cdim.Domain.DomainMember = new ContextDomainMember
                                {
                                    Element = dommem.Element
                                    , Href = dommem.Href
                                    , Name = dommem.Name
                                    , XbrlId = dommem.Id
                                };

                            }
                                
                        }
                    }
                    
                    cc.Dimensions.Add(cdim);
                }
                hc.ScenarioDef.Scenario.Add(cc);
            }

            

        }
        
        private List<string> GetPresentationLocations(List<LabelSet> sets)
        {
            string defaultPeriod = "D";
            List<string> periods = new List<string>();

            if (sets.Count(s => s.Category == "http://www.xbrl.org/2003/role/periodStartLabel") > 0)
            {
                periods.Add("I-Start");
            }
            if (sets.Count(s => s.Category == "http://www.xbrl.org/2003/role/periodEndLabel") > 0)
            {
                periods.Add("I-End");
            }
            if (periods.Count == 0) periods.Add("D");
            return periods;

        }

        private string GetLabel(List<LabelSet> list)
        {
            LabelSet lset = list.FirstOrDefault(ls => ls.Category == "http://www.xbrl.org/2003/role/label");
            if (lset == null) lset = list.FirstOrDefault();
            if (lset == null) return string.Empty;
            if (lset.Labels == null || lset.Labels.Count==0) return string.Empty;
            Label lbl = lset.Labels.FirstOrDefault(l => l.Language == "nl");
            if (lbl == null) lbl = lset.Labels.First();
            if (lbl == null) return string.Empty;
            return lbl.Text;
        }

        private XbrlDataType GetDataType(string id,string sourceuri)
        {
            string href = string.Empty;
            if (!string.IsNullOrEmpty(sourceuri))
            {
                Uri suri = new Uri(sourceuri);
                href = Path.GetFileName(suri.LocalPath);
            }
            if (id.Contains("#"))
            {
                href=id;
                id = id.Split(new char[] { '#' })[1];
            } else {
                href = string.Format("{0}#{1}",href,id);
            }

            if (Taxonomy.DataTypes.ContainsKey(id))
            {
                Taxonomy.DataTypes[id].Used = true;
                return Taxonomy.DataTypes[id];
            }
            else if(Taxonomy.Elements.ContainsKey(href)) 
            {
                Taxonomy.DataTypes[Taxonomy.Elements[href].DataType.Name].Used = true;
                return Taxonomy.Elements[href].DataType;
            }
            return null;


            
        }

        private string GetEstimatedType(string name)
        {
            if(name.ToLower().EndsWith("typedid")) 
                return "TYPE";
            if(name.ToLower().EndsWith("dimension")) 
                return "DIMENSION";
            if(name.ToLower().EndsWith("hypercube")) 
                return "HYPERCUBE";
            if(name.ToLower().EndsWith("primaryconcepts")) 
                return "PRIMARYCONCEPT";
            return "ELEMENT";
        }



        public void SchemaSet_ValidationEventHandler(object sender, ValidationEventArgs e)
        {
            if (e.Exception != null)
                Console.WriteLine(e.Message);
            // perform no validation handling at this point
            //throw new NotImplementedException();
        }

        private string GetComplexValueType(XmlSchemaType xstype)
        {
            if (xstype.Datatype != null)
            {
                return xstype.Datatype.ValueType.ToString();
            }
            else if (xstype.BaseXmlSchemaType != null)
            {
                return GetComplexValueType(xstype.BaseXmlSchemaType);
            }
            else
            {
                return typeof(string).ToString();
            }
        }

        private void ReadComplexType(XmlSchemaComplexType complexType)
        {
            ReadComplexType(complexType, complexType.Name);
        }

        private void ReadComplexType(XmlSchemaComplexType complexType, string name)
        {
            if (!Taxonomy.DataTypes.ContainsKey(name) && complexType.Datatype!=null)
            {
                XbrlDataType dt = new XbrlDataType { Name = name, SourceURI = complexType.SourceUri, TypeCode = complexType.TypeCode.ToString(), RootType = complexType.Datatype != null ? complexType.Datatype.ValueType.ToString() : GetComplexValueType(complexType.BaseXmlSchemaType), BaseSchemaType = complexType.BaseXmlSchemaType.Name, ContentType = complexType.ContentType.ToString(), Restrictions = new List<Restriction>(), Attributes = new Dictionary<string,XbrlAttribute>() };
                dt.Id = string.Format("{0}{1}", dt.SourceURI, dt.Name);
                //XbrlDataType dt = new XbrlDataType { Origin = complexType.SourceUri, TypeCode = complexType.TypeCode.ToString(), Name = name, Restrictions = new List<Restriction>(), ValueType = complexType.Datatype != null ? complexType.Datatype.ValueType.ToString() : GetComplexValueType(complexType.BaseXmlSchemaType), BaseType = complexType.BaseXmlSchemaType != null ? complexType.BaseXmlSchemaType.Name : null };

                if (complexType.AttributeUses.Count > 0)
                {
                    IDictionaryEnumerator enumerator =
                        complexType.AttributeUses.GetEnumerator();
                    dt.Attributes = new Dictionary<string,XbrlAttribute>();
                    while (enumerator.MoveNext())
                    {
                        XmlSchemaAttribute attribute =
                            (XmlSchemaAttribute)enumerator.Value;
                        string atname = attribute.Name == null ? attribute.RefName.ToString() : attribute.Name;
                        dt.Attributes.Add(atname, new XbrlAttribute { Name = atname, Value = string.IsNullOrEmpty(attribute.FixedValue) ? null : attribute.FixedValue, Type = attribute.AttributeSchemaType.Name, Required = attribute.Use == XmlSchemaUse.Required });
                        if (atname == "unitRef")
                        {
                            dt.Attributes[atname].Value = dt.BaseSchemaType == null ? null : dt.BaseSchemaType.ToLower().Contains("monetary") ? "EUR" : "U-Pure";
                        }
                        Console.WriteLine("Attribute: {0} {1}", attribute.QualifiedName, enumerator.Value.GetType().Name);
                    }
                }
                Console.WriteLine("Base schematype " + complexType.BaseXmlSchemaType.Name);
                Console.WriteLine("Contenttype " + complexType.ContentType.ToString()); //XmlSchemaContentType.
                
                
                
                //complexType.ContentModel
                //Console.WriteLine(complexType.ContentType.ToString());

                //XmlSchemaSimpleContent simpleContent = complexType.ContentTypeParticle as XmlSchemaSimpleContent
                if (complexType.ContentModel != null)
                {
                    string cotype = complexType.ContentModel.GetType().ToString();
                    //   if (!complexContentTypes.Contains(cotype)) complexContentTypes.Add(cotype);

                    XmlSchemaSimpleContent simpleContent = complexType.ContentModel as XmlSchemaSimpleContent;
                    if (simpleContent != null)
                    {

                        ReadSimpleContent(simpleContent, ref dt);
                    }
                    else
                    {
                        Console.WriteLine(cotype);
                    }
                }
                else
                {
                    Console.WriteLine("no content");
                }

                // Get the sequence particle of the complex type.
                //XmlSchemaSequence sequence = complexType.ContentTypeParticle as XmlSchemaSequence;

                //// Iterate over each XmlSchemaElement in the Items collection.
                //foreach (XmlSchemaElement childElement in sequence.Items)
                //{
                //    Console.WriteLine("Element: {0}", childElement.Name);
                //}
                Taxonomy.DataTypes.Add(dt.Name, dt);
            }
        }

        private void ReadSimpleType(XmlSchemaSimpleType simpleType)
        {
            ReadSimpleType(simpleType, simpleType.Name);
        }

        private void ReadSimpleType(XmlSchemaSimpleType simpleType, string name)
        {
            if (!Taxonomy.DataTypes.ContainsKey(name))
            {
                 

                XbrlDataType dt = new XbrlDataType { Name = name, SourceURI = simpleType.SourceUri, TypeCode = simpleType.TypeCode.ToString(), RootType = simpleType.Datatype != null ? simpleType.Datatype.ValueType.ToString() : simpleType.Name, BaseSchemaType = simpleType.BaseXmlSchemaType != null ? simpleType.BaseXmlSchemaType.Name : simpleType.Name, ContentType = "", Restrictions = new List<Restriction>(), Attributes = new Dictionary<string, XbrlAttribute>() };
                if (string.IsNullOrEmpty(dt.SourceURI)) dt.SourceURI = string.Empty;
                dt.Id = string.Format("{0}{1}", dt.SourceURI, dt.Name);



                XmlSchemaSimpleTypeRestriction restriction = simpleType.Content as XmlSchemaSimpleTypeRestriction;

                if (restriction != null)
                {
                    if (string.IsNullOrEmpty(dt.RootType) || dt.RootType==dt.Name) dt.RootType = "RELOAD_" + restriction.BaseTypeName.Name;
                    Console.WriteLine("restriction : {0}", restriction.BaseTypeName.Name);

                    foreach (XmlSchemaObject facet in restriction.Facets)
                    {
                        Restriction restrict = new Restriction { Name = facet.GetType().ToString() };

                        // if (!facets.Contains(facet.GetType().ToString())) facets.Add(facet.GetType().ToString());
                        Console.WriteLine("  > {0}", facet.ToString());
                        if (facet is XmlSchemaFacet)
                        {
                            restrict.Value = ((XmlSchemaFacet)facet).Value;
                        }
                        else if (facet is XmlSchemaEnumerationFacet)
                        {
                            restrict.Value = ((XmlSchemaEnumerationFacet)facet).Value;
                        }
                        else
                        {
                            Console.WriteLine("unknown facet");
                        }

                        dt.Restrictions.Add(restrict);
                    }
                }
                Taxonomy.DataTypes.Add(dt.Name, dt);
            }
        }


        private void ReadSimpleContent(XmlSchemaSimpleContent cnt, ref XbrlDataType dt)
        {
            XmlSchemaSimpleContentRestriction restriction = cnt.Content as XmlSchemaSimpleContentRestriction;


            if (restriction != null)
            {
                if (string.IsNullOrEmpty(dt.RootType) || dt.RootType == dt.Name) dt.RootType = "RELOAD_" + restriction.BaseTypeName.Name;
                Console.WriteLine("restriction : {0}", restriction.BaseTypeName.Name);

                foreach (XmlSchemaObject facet in restriction.Facets)
                {
                    Restriction restrict = new Restriction { Name = facet.GetType().ToString() };

                    //  if (!facets.Contains(facet.GetType().ToString())) facets.Add(facet.GetType().ToString());
                    Console.WriteLine("  > {0}", facet.ToString());
                    if (facet is XmlSchemaFacet)
                    {
                        restrict.Value = ((XmlSchemaFacet)facet).Value;
                    }
                    else if (facet is XmlSchemaEnumerationFacet)
                    {
                        restrict.Value = ((XmlSchemaEnumerationFacet)facet).Value;
                    }
                    else
                    {
                        Console.WriteLine("unknown facet");
                    }
                    dt.Restrictions.Add(restrict);
                }
            }

            XmlSchemaSimpleContentExtension extension = cnt.Content as XmlSchemaSimpleContentExtension;

            if (cnt.Content != null)
            {
                Console.WriteLine("extension type: {0}", cnt.Content.GetType().Name);
            }

            if (extension != null)
            {

               // Console.WriteLine("extension : {0}", extension.BaseTypeName);

                foreach (XmlSchemaObject obj in extension.Attributes)
                {
                    XmlSchemaAttribute xat = obj as XmlSchemaAttribute;
                    XmlSchemaAttributeGroupRef xagref = obj as XmlSchemaAttributeGroupRef;
                    if (xat != null)
                    {
                        if (!dt.Attributes.ContainsKey(xat.Name))
                        {
                            string val = null;
                            if (xat.Name == "unitRef")
                            {
                                val = dt.BaseSchemaType.ToLower().Contains("monetary") ? "EUR" : "U-Pure";
                            }
                            dt.Attributes.Add(xat.Name, new XbrlAttribute { Name = xat.Name, Required = xat.Use == XmlSchemaUse.Required, Type = xat.SchemaTypeName.ToString(),Value=val });
                        }
                        
                    }
                    else if (xagref != null)
                    {
                        if (AttributeGroups.ContainsKey(xagref.RefName.Name))
                        {
                            foreach (XbrlAttribute xgat in AttributeGroups[xagref.RefName.Name].Attributes)
                            {
                                if (!dt.Attributes.ContainsKey(xgat.Name))
                                {
                                    if (xgat.Name == "unitRef")
                                    {
                                        xgat.Value = dt.BaseSchemaType.ToLower().Contains("monetary") ? "EUR" : "U-Pure";
                                    }
                                    dt.Attributes.Add(xgat.Name, xgat);
                                }
                                else
                                {

                                    if (xgat.Name == "unitRef")
                                    {
                                        xgat.Value = dt.BaseSchemaType == null ? null : dt.BaseSchemaType.ToLower().Contains("monetary") ? "EUR" : "U-Pure";
                                    }
                                }
                            }
                        }
                    }
                }
                Console.WriteLine("extension : {0}", extension.BaseTypeName);
                //foreach (XmlSchemaAttribute att in extension.Attributes)
                //{
                //    Console.WriteLine(att.Name);
                //    ReadSimpleType(att.SchemaType, att.SchemaType.Name);
                //}
                ////foreach (XmlSchemaObject facet in extension.Facets)
                //{
                //    Console.WriteLine("  > {0}", facet.ToString());
                //    if (facet is XmlSchemaFacet)
                //        Console.WriteLine("  > {0}", ((XmlSchemaFacet)facet).Value);
                //    if (facet is XmlSchemaEnumerationFacet)

                //        Console.WriteLine("Element: {0}", ((XmlSchemaEnumerationFacet)facet).Value);
                //}
            }

        }

        private ElementPresentationLink GetParentEPLByHref(ElementPresentationLink el, string href)
        {
            while (el.Parent != null)
            {
                if (el.Parent.Href== href) return el.Parent;
                el = el.Parent;
            }
            return null;
        }

        private XElement GetParentByHref(XElement el, string href, string nodeName)
        {
            while (el.Parent != null && el.Parent.Name.LocalName == nodeName)
            {
                if (el.Parent.Attribute("id").Value == href) return el.Parent;
                el = el.Parent;
            }
            return null;
        }

        public string GetDefType(string arcrole)
        {
            switch (arcrole)
            {
                case "http://xbrl.org/int/dim/arcrole/domain-member":
                    return "DOMAIN-MEMBER";
                case "http://xbrl.org/int/dim/arcrole/all":
                    return "HYPERCUBE-ALL";
                case "http://xbrl.org/int/dim/arcrole/notAll":
                    return "HYPERCUBE-NOTALL";
                case "http://xbrl.org/int/dim/arcrole/hypercube-dimension":
                    return "HYPERCUBE-DIMENSION";
                case "http://xbrl.org/int/dim/arcrole/dimension-domain":
                    return "DIMENSION-DOMAIN";   
            }
            return arcrole;
        }

    }

    public class LocalFolderResolver : XmlUrlResolver
    {
        public string Folder { get; set; }

        private Dictionary<string, FileStream> resolves = new Dictionary<string, FileStream>();


        public override object GetEntity(Uri absoluteUri, string role, Type ofObjectToReturn)
        {

            string fname = absoluteUri.Segments[absoluteUri.Segments.Length - 1];
            //if (!resolves.ContainsKey(fname))
            //{
            FileStream fs = new FileStream(Path.Combine(Folder, fname), FileMode.Open, FileAccess.Read);
            // resolves.Add(fname,fs);
            return fs;
            //}
            //else
            //{
            //    return resolves[fname];
            //}
            //return base.GetEntity(absoluteUri, role, ofObjectToReturn);
        }

    }
}
