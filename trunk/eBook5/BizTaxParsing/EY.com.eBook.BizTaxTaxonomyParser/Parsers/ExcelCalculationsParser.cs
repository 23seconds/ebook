﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LinqToExcel;
using EY.com.eBook.BizTaxTaxonomyParser.Contracts;

namespace EY.com.eBook.BizTaxTaxonomyParser.Parsers
{
    public static class ExcelCalculationsParser
    {
        public static Dictionary<string, FormulaDocumentationContract> Parse(string dir)
        {
            Dictionary<string, FormulaDocumentationContract> formulaDoc = new Dictionary<string, FormulaDocumentationContract>();
            string cachedSet = System.IO.Path.Combine(dir, "formulaContents.bin");
            if (System.IO.File.Exists(cachedSet))
            {
                formulaDoc = Program.DeSerializeObject<Dictionary<string, FormulaDocumentationContract>>(cachedSet);
                return formulaDoc;
            }
            

            

            foreach (string file in System.IO.Directory.GetFiles(dir, "*.xls*"))
            {
                var excel = new ExcelQueryFactory(file);
                Console.WriteLine(System.IO.Path.GetFileName(file));


                foreach (string sheetName in excel.GetWorksheetNames())
                {
                    if (sheetName != "general")
                    {
                        Console.WriteLine("  " + sheetName);
                        Cell cellBusinessRule = excel.WorksheetRangeNoHeader("A10", "A10", sheetName).First().First();
                        Cell cellFormula = excel.WorksheetRangeNoHeader("C10", "C10", sheetName).First().First();
                        Cell cellFormulaTarget = excel.WorksheetRangeNoHeader("C13", "C13", sheetName).First().First();

                        Console.WriteLine("    BR: " + cellBusinessRule.Value);
                        Console.WriteLine("    Formule: " + cellFormula.Value);
                        Console.WriteLine("    Target: " + cellFormulaTarget.Value);

                        formulaDoc.Add(sheetName, new FormulaDocumentationContract
                        {
                            SourceExcel = file,
                            Id = sheetName,
                            BusinessRule = cellBusinessRule.Value.ToString(),
                            Formula = cellFormula.Value.ToString(),
                            Target = cellFormulaTarget.Value.ToString()
                        });
                    }
                    
                }

              
            }
            Console.WriteLine("");
            Console.WriteLine("TOTAL: " + formulaDoc.Count);
            Console.WriteLine("SERIALIZING TO SOURCE DIR - CACHE");
            Program.SerializeObject(cachedSet, formulaDoc);
            Console.WriteLine("SERIALIZED.");
            return formulaDoc;
        }
    }
}
