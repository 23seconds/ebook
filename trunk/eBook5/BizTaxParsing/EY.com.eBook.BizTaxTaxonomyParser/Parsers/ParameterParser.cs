﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using EY.com.eBook.BizTaxTaxonomyParser.Contracts;
using System.Text.RegularExpressions;

namespace EY.com.eBook.BizTaxTaxonomyParser.Parsers
{
    public class ParameterParser
    {
        private Regex DateRegex = new Regex(@"\'[0-9]{4}\-[0-9]{2}\-[0-9]{2}\'");
        public GlobalParameters Parse(string sourceFile)
        {
            GlobalParameters gp = new GlobalParameters
            {
                AllParameters = new Dictionary<string, Parameter>()
                ,
                Taxonomies = new List<string>()
            };
            XmlReader reader = XmlTextReader.Create(sourceFile);
            while (reader.Read())
            {
                switch (reader.NodeType)
                {
                    case System.Xml.XmlNodeType.Element:
                        {
                            switch (reader.LocalName.ToLower())
                            {
                                case "parameter":

                                    ReadParameter(reader, ref gp);

                                    break;

                            }
                        }
                        break;
                }
            }
            reader.Close();
            reader = null;
            return gp;
        }

        private void ReadParameter(XmlReader reader, ref GlobalParameters gp)
        {
            Parameter param = new Parameter();
            if (reader.HasAttributes)
            {
                for (int i = 0; i < reader.AttributeCount; i++)
                {
                    reader.MoveToAttribute(i);
                    switch (reader.LocalName.ToLower())
                    {
                        case "id":
                            param.Id = reader.Value;
                            break;
                        case "label":
                            param.LabelId = reader.Value;
                            break;
                        case "name":
                            param.Name = reader.Value;
                            break;
                        case "select":
                            
                            param.Value = reader.Value;
                            if (param.Value.StartsWith("'"))
                            {
                                if (DateRegex.IsMatch(param.Value))
                                {
                                    param.Value = param.Value.Replace("'", "");
                                    param.DateValue = DateTime.ParseExact(param.Value, "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);
                                    param.ValueType = typeof(DateTime).Name;
                                }
                                else
                                {
                                    param.Value = param.Value.Replace("'", "");
                                    param.ValueType = typeof(string).Name;
                                }
                                
                            }
                            else
                            {
                                param.ValueType = typeof(decimal).Name;
                                param.DecimalValue = decimal.Parse(param.Value, System.Globalization.NumberStyles.Any);
                            }
                            
                            break;
                    }
                }
                gp.AllParameters.Add(param.Id, param);
                if (param.ValueType == typeof(string).Name && param.Id.Contains("TaxonomySchemaRef"))
                {
                    gp.Taxonomies.Add(param.Value);
                }
            }
        }
    }
}
