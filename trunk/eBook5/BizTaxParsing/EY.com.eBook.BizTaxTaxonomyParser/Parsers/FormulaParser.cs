﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml.Linq;
using System.Runtime.Serialization;
using EY.com.eBook.BizTaxTaxonomyParser.Contracts;

namespace EY.com.eBook.BizTaxTaxonomyParser.Parsers
{
    public class FormulaParser
    {
        public static List<string> PossibleUnkowns = new List<string>();

        public static Dictionary<string, List<XElement>> Functions = new Dictionary<string, List<XElement>>();

        // xs:date => todate
        // xs:dayTimeDuration => toDuration
        // 
        // P1D => One day
        // P1Y => One year

        // NEW PARSER


        //(\+)|(\-{0,1}\$[\w]*)|(\s.{1}\s)|(,){1}|(([0-9]|\.){1,})|

        //private readonly Regex itemsRegEx = new Regex(@"(\+)|(\-{0,1}\$[\w]*)|(\-)|([0-9]+\.{0,1}[0-9]+){1}|(,){1}|(\({1})|(\){1})|(\w+)|\*{1}", RegexOptions.Compiled);
        public static readonly Regex itemsRegEx = new Regex(@"(\*)|(\+)|(\-{0,1}\$[\w|\-]*)|(\s.{1}\s)|(,){1}|(\'){1}|(([0-9]|\.){1,})|((?!\$)(\[|\]|\@|\/|\w|\-|\:){1,})|\(|\)", RegexOptions.IgnoreCase | RegexOptions.Compiled);
        private static readonly Regex reNumeric = new Regex(@"[0-9]|\.");

         public FormulaParserResult TransformFormula(string formula)
        {
            return TransformFormula(formula, null);
        }

         private Queue<string> _matches = new Queue<string>();

         private FormulaParserResult fresult = new FormulaParserResult { Formula = null, UsedVariables = new List<string>() };

         public FormulaParserResult TransformFormula(string formula, string target)
         {
             if (!string.IsNullOrEmpty(target))
             {
                 //formula = target + " eq " + formula;
             }
             formula = formula.Replace("distinct-values", "distinctvalues");
             MatchCollection coll = FormulaParser.itemsRegEx.Matches(formula);
             //List<string> items = new List<string>();
             XElement xformula = new XElement("Formula");

            
             foreach (Match match in coll)
             {
                 if (match.Value.Trim().Length > 0)
                 {
                     _matches.Enqueue(match.Value.Trim());
                 }
             }

             ReadQueue(ref xformula);
             fresult.Formula = xformula;

             return fresult;
         }

         private void ReadQueue(ref XElement xeParent)
         {
             ReadQueue(ref xeParent, null);
         }

         private void ReadQueue(ref XElement xeParent,string until)
         {
             while (_matches.Count > 0 && (string.IsNullOrEmpty(until) || (!string.IsNullOrEmpty(until) && _matches.Peek() != until)) )
             {
                 string peek = _matches.Dequeue();
                 if (peek.Trim().Length > 1)
                 {
                     XElement operat = GetOperator(peek.Trim()[0].ToString());
                     if (operat != null)
                     {
                         xeParent.Add(operat);
                         peek = peek.Substring(1);
                     }
                 }
                 xeParent.Add(HandleItem(peek));
               
             }
             if (_matches.Count > 0 && !string.IsNullOrEmpty(until) && _matches.Peek() == until)
             {
                 _matches.Dequeue();
             }
         }

         private XElement GetOperator(string item)
         {
             XElement result = null;
             switch (item.Trim())
             {
                 case "+":
                     result = new XElement("plus");
                     // PLUS OPERATOR
                     break;
                 case "-":
                     result = new XElement("minus");
                     // MINUS OPERATOR
                     break;
                 case "*":
                     result = new XElement("multiply");
                     // MULTIPLIER OPERATOR
                     break;
             }
             return result;
         }
        

         private XElement HandleItem(string item)
         {
             XElement result = GetOperator(item);
             if (result != null) return result;
             switch (item)
             {
                 case "'":
                     result = new XElement("string");
                     StringBuilder sb = new StringBuilder("");
                     while (_matches.Count > 0 && _matches.Peek() != "'")
                     {
                         if(sb.Length>0) sb.Append(" ");
                         sb.Append(_matches.Dequeue());
                     }
                     _matches.Dequeue(); // == end '
                     result.Add(sb.ToString());
                     break;
                 case ".":
                     result = new XElement("current");
                     break;
                 case "(":
                     //start group
                     result = new XElement("group");
                     ReadQueue(ref result, ")");
                     break;
                 case ")":
                     //end group
                     break;
                 case "+":
                     result = new XElement("plus");
                     // PLUS OPERATOR
                     break;
                 case "-":
                     result = new XElement("minus");
                     // MINUS OPERATOR
                     break;
                 case "*":
                     result = new XElement("multiply");
                     // MULTIPLIER OPERATOR
                     break;
                 case "div":
                     result = new XElement("divide");
                     // DIVIDER OPERATOR
                     break;
                 case "mod":
                     result = new XElement("modular");
                     // MODULAR OPERATOR
                     break;
                 case "eq":
                     result = new XElement("equals");
                     // EQUALS OPERATOR
                     break;
                 case "ne":
                     result = new XElement("notequals");
                     // EQUALS OPERATOR
                     break;
                 case "gt":
                     result = new XElement("greaterThen");
                     // GREATER THEN OPERATOR
                     break;
                 case "lt":
                     result = new XElement("lowerThen");
                     // LOWER THEN OPERATOR
                     break;
                 case "ge":
                     result = new XElement("greaterOrEqualThen");
                     // GREATER THEN OPERATOR
                     break;
                 case "le":
                     result = new XElement("lowerOrEqualThen");
                     // LOWER THEN OPERATOR
                     break;
                 case ",":
                     result = new XElement("comma");
                     // LOWER THEN OPERATOR
                     break;
                /* HANDLE TRUE/FALSE AS FUNCTION (XPath notation)
                 * case "true":
                     result = new XElement("true");
                     break;
                 case "false":
                     result = new XElement("false");
                     break;*/
                 case "or":
                     result = new XElement("or");
                     break;
                 case "and":
                     result = new XElement("and");
                     break;
                 case "for":
                     // handle for
                     // first is resulting var
                     // second is statement IN
                     // third is IN body
                     // on end of IN body, next is return
                     // X in X return
                     // IMPORTANT: At this point FLWOR isn't implemented in full
                     // since FOD Taxonomy only utilizes
                     result = new XElement("foreach");
                     item = _matches.Dequeue();
                     result.Add(new XElement("each", new XAttribute("name", item.Substring(1))));
                     item = _matches.Dequeue(); // == in
                     XElement xin = new XElement("in");
                     ReadQueue(ref xin, "return");
                    // _matches.Dequeue();
                     result.Add(xin);
                     XElement xreturn = new XElement("return");
                     if (_matches.Peek() == "(")
                     {
                         _matches.Dequeue();
                         ReadQueue(ref xreturn, ")");
                     }
                     else
                     {
                         ReadQueue(ref xreturn);
                     }
                     result.Add(xreturn);
                     
                     break;
                 case "every":
                     // handle for
                     // first is resulting var
                     // second is statement IN
                     // third is IN body
                     // on end of IN body, next is return
                     // X in X return
                     // IMPORTANT: At this point FLWOR isn't implemented in full
                     // since FOD Taxonomy only utilizes
                     result = new XElement("every");
                     item = _matches.Dequeue();
                     result.Add(new XElement("each", new XAttribute("name", item.Substring(1))));
                     item = _matches.Dequeue(); // == in
                     XElement xein = new XElement("in");
                     ReadQueue(ref xein, "satisfies");
                     // _matches.Dequeue();
                     result.Add(xein);
                     XElement xwhere = new XElement("where");
                     if (_matches.Peek() == "(")
                     {
                         _matches.Dequeue();
                         
                     }
                     ReadQueue(ref xwhere, ")");
                     result.Add(xwhere);

                     break;
                 case "if":
                     // handle for
                     // first is resulting var
                     // second is statement IN
                     // third is IN body
                     // on end of IN body, next is return
                     // X in X return
                     // IMPORTANT: At this point FLWOR isn't implemented in full
                     // since FOD Taxonomy only utilizes
                     result = new XElement("if");
                     XElement xifwhen = new XElement("when");
                     ReadQueue(ref xifwhen, "then");
                     result.Add(xifwhen);


                    
                     XElement xifthen = new XElement("then");
                     ReadQueue(ref xifthen, "else");
                     result.Add(xifthen);

                     
                     XElement xifelse = new XElement("else");
                     

                     if (_matches.Peek() == "(")
                     {
                         ReadQueue(ref xifelse, ")");
                     }
                     else
                     {
                         ReadQueue(ref xifelse, ")");
                     }


                     result.Add(xifelse);

                     break;
                 /*case "tax-inc:PeriodStartDate":
                     result = new XElement("function", new XAttribute("name",fnName ), new XAttribute("namespace",ns))
                     result = new XElement("GetPeriodStartDate");
                     break;
                 case "tax-inc:PeriodEndDate":
                     result = new XElement("GetPeriodEndDate");
                     break;*/
                 default:
                    
                     if (item.Contains("/"))
                     {
                         //Path
                         result = new XElement("nodepath", new XAttribute("path", item));
                     } else if (item.StartsWith("$"))
                     {
                         result = new XElement("variable", new XAttribute("name", item.Substring(1)));
                         if (!fresult.UsedVariables.Contains(item.Substring(1))) fresult.UsedVariables.Add(item.Substring(1).Replace("-","_"));
                     }
                     else if (item.StartsWith("-$"))
                     {
                         result = new XElement("group", new XElement("minus"), new XAttribute("name", item.Substring(2)));
                         if (!fresult.UsedVariables.Contains(item.Substring(2))) fresult.UsedVariables.Add(item.Substring(2).Replace("-","_"));
                     }
                     else if (_matches.Count() > 0 && _matches.Peek() == "(")
                     {
                         // remove open brackets from queue
                         _matches.Dequeue();
                         // FUNCTION
                         string fnName = item;
                         string[] splitted = fnName.Split(new char[] {':'});
                         
                         string ns = string.Empty;
                         if (splitted.Length>1)
                         {
                             ns = splitted[0];
                             fnName = splitted[1];
                             
                         }
                         result = new XElement("function", new XAttribute("name",fnName ), new XAttribute("namespace",ns));
                         XElement tempArguments = new XElement("args");
                         // handle arguments?
                         ReadQueue(ref tempArguments, ")");

                         if (tempArguments.Elements().Count() == 1 && tempArguments.Elements().First().Name=="group")
                         {
                             tempArguments = tempArguments.Elements().First();
                         }
                         // post handle arguments.
                         XElement xArgument = new XElement("argument");
                         foreach (XElement xe in tempArguments.Elements())
                         {
                             if (xe.Name == "comma")
                             {
                                 result.Add(xArgument);
                                 xArgument = new XElement("argument");
                             }
                             else
                             {
                                 xArgument.Add(xe);
                             }
                         }
                         if (xArgument.HasElements)
                         {
                             result.Add(xArgument);
                         }

                         if (!Functions.ContainsKey(item))
                         {
                             Functions.Add(item, new List<XElement>());
                         }
                         Functions[item].Add(result);
                         
                     }
                     else if(reNumeric.IsMatch(item)) {
                         result = new XElement("number", item);
                     }
                     else
                     {

                         string[] selectors = item.Split(new char[] { ':' });
                         if (selectors.Length == 2)
                         {
                             result = new XElement("function", new XAttribute("name", "GetNodeByName"));
                             result.Add(new XElement("argument", new XElement("string", selectors[0])));
                             result.Add(new XElement("argument", new XElement("string", selectors[1])));
                         }
                         else
                         {
                             Console.WriteLine("KEY: " + item);
                             if (!FormulaParser.PossibleUnkowns.Contains(item)) FormulaParser.PossibleUnkowns.Add(item);
                             result = new XElement("Key", new XAttribute("name", item));
                         }
                     }
                     break;
             }
             return result;
         }

        /* OLD
        public FormulaParserResult TransformFormula(string formula)
        {
            return TransformFormula(formula, null);
        }

        public FormulaParserResult TransformFormula(string formula,string target)
        {
            if (!string.IsNullOrEmpty(target))
            {
                formula = target + " eq " + formula;
            }
            formula = formula.Replace("distinct-values", "distinctvalues");
            formula = formula.Replace("days-from-duration", "daysfromduration");
            formula = formula.Replace("xs:yearMonthDuration('P1Y')", "oneyear");
            formula = formula.Replace("xs:dayTimeDuration('P1D')", "oneday");
            formula = formula.Replace("xs:", "To").Replace("string-length", "StringLength");
            XElement xformula = new XElement("formula");
            List<string> usedVars = new List<string>();
            //  Console.WriteLine(formula);
            StringBuilder sb = new StringBuilder("");
            MatchCollection coll = itemsRegEx.Matches(formula);
            List<string> items = new List<string>();
            foreach (Match match in coll)
            {
                if (match.Value.Trim().Length > 0)
                {
                    items.Add(match.Value.Trim());
                }
            }
            RenderXml(0, items, ref xformula, ref usedVars);

            return new FormulaParserResult
            {
                Formula = xformula
                , UsedVariables = usedVars
            };
        }


        private int RenderXml(int startfrom, List<string> items, ref XElement parent, ref List<string> usedVars) //, string stop
        {
            for (int i = startfrom; i < items.Count; i++)
            {
                string item = items[i];
                if (string.IsNullOrEmpty(item)) //!string.IsNullOrEmpty(stop) && item == stop)
                {
                    return i;
                }
                else
                {
                    switch (item.ToLower())
                    {
                        case "daysfromduration":
                            XElement xdaysDur = new XElement("days-from-duration"); //, new XAttribute("isGrouped",true)
                            int xddStart = items.IndexOf("(", i);
                            i = RenderXml(xddStart + 1, items, ref xdaysDur, ref usedVars);
                            parent.Add(xdaysDur);
                            break;
                        case "*":
                            parent.Add(new XElement("multiply"));
                            break;
                        case "+":
                            parent.Add(new XElement("plus"));
                            break;
                        case "-":
                            //if (items[i - 1] != "(" && items[i - 1] != ",")
                                parent.Add(new XElement("minus"));
                            break;
                        case "(":
                            XElement xgroup = new XElement("group");
                            i = RenderXml(i + 1, items, ref xgroup, ref usedVars);
                            parent.Add(xgroup);
                            break;
                        case ")":
                            return i;
                            break;
                        case "stringlength":
                            XElement xstrl = new XElement("StringLength"); //, new XAttribute("isGrouped",true)
                            int xmstrlStart = items.IndexOf("(", i);
                            i = RenderXml(xmstrlStart + 1, items, ref xstrl, ref usedVars);
                            parent.Add(xstrl);
                            break;
                        case "tostring":
                            XElement xstr = new XElement("ToString"); //, new XAttribute("isGrouped",true)
                            int xmstrStart = items.IndexOf("(", i);
                            i = RenderXml(xmstrStart + 1, items, ref xstr, ref usedVars);
                            parent.Add(xstr);
                            break;
                        case "todate":
                            XElement xdte = new XElement("ToDate"); //, new XAttribute("isGrouped",true)
                            int xmdteStart = items.IndexOf("(", i);
                            i = RenderXml(xmdteStart + 1, items, ref xdte, ref usedVars);
                            parent.Add(xdte);
                            break;
                        case "max":
                            XElement xmax = new XElement("max"); //, new XAttribute("isGrouped",true)
                            int xmaStart = items.IndexOf("(", i);
                            i = RenderXml(xmaStart + 1, items, ref xmax, ref usedVars);
                            parent.Add(xmax);
                            break;
                        case "min":
                            XElement xmin = new XElement("max"); //, new XAttribute("isGrouped",true)
                            int xmiStart = items.IndexOf("(", i);
                            i = RenderXml(xmiStart + 1, items, ref xmin, ref usedVars);
                            parent.Add(xmin);
                            break;
                        case "round":
                            XElement xround = new XElement("round"); //, new XAttribute("isGrouped",true)
                            int xrStart = items.IndexOf("(", i);
                            i = RenderXml(xrStart + 1, items, ref xround, ref usedVars);
                            parent.Add(xround);
                            break;
                        case "exists":
                            XElement xexists = new XElement("exists"); //, new XAttribute("isGrouped",true)
                            int xeStart = items.IndexOf("(", i);
                            i = RenderXml(xeStart + 1, items, ref xexists, ref usedVars);
                            parent.Add(xexists);
                            break;
                        case "count":
                            XElement xcount = new XElement("count"); //, new XAttribute("isGrouped",true)
                            int xcStart = items.IndexOf("(", i);
                            i = RenderXml(xcStart + 1, items, ref xcount, ref usedVars);
                            parent.Add(xcount);
                            break;
                        case "sum":
                            XElement xsum = new XElement("sum"); //, new XAttribute("isGrouped",true)
                            int xgStart = items.IndexOf("(", i);
                            i = RenderXml(xgStart + 1, items, ref xsum, ref usedVars);
                            parent.Add(xsum);
                            break;
                        case "for":
                            parent.Add(new XElement(item));
                            break;
                        case "return":
                            XElement xreturn = new XElement("return"); //, new XAttribute("isGrouped",true)
                            int xretStart = items.IndexOf("(", i);
                            i = RenderXml(xretStart + 1, items, ref xreturn, ref usedVars);
                            parent.Add(xreturn);
                            break;
                        case "substring":
                            XElement xsubstring = new XElement("substring"); //, new XAttribute("isGrouped",true)
                            int xsubstrStart = items.IndexOf("(", i);
                            i = RenderXml(xsubstrStart + 1, items, ref xsubstring, ref usedVars);
                            parent.Add(xsubstring);
                            break;
                        default:
                            if (item.StartsWith("$") || item.StartsWith("-$"))
                            {
                                bool negative = false;
                                if (item.StartsWith("-"))
                                {
                                    negative = true;
                                    item = item.Substring(1);
                                }
                                if (!usedVars.Contains(item.Substring(1))) usedVars.Add(item.Substring(1));
                                parent.Add(new XElement("variable", new XAttribute("name", item.Substring(1)), new XAttribute("negative", negative)));
                            }
                            else if (item == ",")
                            {
                            }
                            else if (reNumeric.IsMatch(item))
                            {
                                parent.Add(new XElement("number", item));
                            }
                            else
                            {
                                if (!FormulaParser.PossibleUnkowns.Contains(item)) FormulaParser.PossibleUnkowns.Add(item);
                                parent.Add(new XElement(item));
                            }
                            break;
                    }
                }
            }
            return items.Count + 1;
        }
         */
    }

   
}
