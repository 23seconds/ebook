﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Xml.XPath;
using EY.com.eBook.BizTaxTaxonomyParser.Contracts;
using System.IO;
using System.Text.RegularExpressions;

namespace EY.com.eBook.BizTaxTaxonomyParser.Renderers
{
    public class _NOTUSED_BizTaxRenderer
    {
        private StreamWriter _writer;
        private Taxonomy _taxonomy;
        private int _assessmentyear;
        private Dictionary<string, string> _templates;
        private GlobalParameters _parameters;
        private bool? lastFormulaNumeric = false;
        private List<string> numericVariables;

        private XElement allFormulas = new XElement("formulas");

        public _NOTUSED_BizTaxRenderer(Taxonomy taxonomy, GlobalParameters parameters)
        {
            _taxonomy = taxonomy;
            _assessmentyear = int.Parse(parameters.AllParameters["CurrentAssessmentYear"].Value);
            _parameters = parameters;
            _templates = new Dictionary<string, string>();
            string searchPath = Path.Combine("", @"Templates\Code");
            string[] files = Directory.GetFiles(searchPath, "*.tpl", SearchOption.AllDirectories);
            foreach (string file in files)
            {
                string key = file.Replace(searchPath + @"\", "");
                StreamReader sr = new StreamReader(file);
                _templates.Add(key, sr.ReadToEnd());
                sr.Close();
            }
            
        }

        public void Render(string targetPath, bool debug)
        {
            
            Console.WriteLine("RENDERING BACKEND");
            if (_writer != null)
            {
                _writer.Close();
                _writer.Dispose();
            }
            _writer = System.IO.File.CreateText(Path.Combine(targetPath, string.Format("BizTax{0}Renderer.cs", _taxonomy.Key)));

            string template = _templates[@"xbrl\biztaxrenderer.tpl"];
            template = template.Replace("<!ASSESSMENTYEAR!>", _assessmentyear.ToString());
            template = template.Replace("<!BIZTAXTYPE!>", _taxonomy.Key);
            template = template.Replace("<!GLOBALPARAMS!>", GetGlobalParams());

            RenderCalculations(ref template);
            
            _writer.Write(template);
            _writer.Close();
            allFormulas.Save("formulas.xml");
            Console.WriteLine("DONE RENDERING BACKEND");

        }

        private string GetGlobalParams()
        {
            StringBuilder sb = new StringBuilder();

            foreach (Parameter param in _parameters.AllParameters.Values)
            {
                string nm = param.Name.Replace("-", "_");
                sb.Append(Environment.NewLine).Append("\t");
                sb.Append("private ");
                if (param.ValueType == typeof(string).Name)
                {
                    sb.Append("string ").Append(nm).Append(" = ");
                    sb.Append("\"").Append(param.Value).Append("\"");
                }
                else if (param.ValueType == typeof(DateTime).Name)
                {
                    sb.Append("DateTime ").Append(nm).Append(" = ");
                    DateTime dt = param.DateValue.Value;
                    sb.Append(string.Format("new DateTime({0},{1},{2})", dt.Year,dt.Month,dt.Day));
                }
                else
                {
                    sb.Append("decimal ").Append(nm).Append(" = (decimal)");
                    sb.Append(param.Value);
                }
                sb.Append(";");
            }
            sb.Append(Environment.NewLine);
            return sb.ToString();
        }

        private void RenderCalculations(ref string template)
        {
            StringBuilder sbCalls = new StringBuilder();
            StringBuilder sbFunctions = new StringBuilder();

            /*
             * Variabelen zijn verzamelingen van elementen die voldoen aan de bijhorende filters
             * ze starten dus steeds als "alle" elementen (facts) en worden gefilterd
             * 
             * Wanneer implicit filtering wordt toegepast, dan moeten alle aspecten (attributen) gelijk zijn
             * tenminste de gekende attributen (aspects / dimensies). Lege dimensies filtert niet op leeg, maar alle (?)
             * D/I is onderdeel van period ! niet van dimensie (do not forget)
             * 
             * Wordt dit toegepast op basis van de eerste gebruikte variable en zo verder door getrokken ?
             * In dat geval:
             * Neem eerste variabele en onderzoek filters. 
             * Neem daar bij de definitie van dat element
             * Bvb.: TaxableReserves in f-rcorp-2101
             * In de definitie van TaxableReserves is er enkel een lege dimensie, unitref euro en period instant
             * period instant = I-Start & I-End
             * Daar er geen bijkomende filter op de variabele is geldt de impliciete filter dan:
             * Periode moet gelijk zijn
             * Dimensies eender welke (geen test)
             * 
             * Bvb.: GrossPEExemptIncomeMovableAssets
             * In de definitie van GrossPEExemptIncomeMovableAssets is er sprake van period duration én is er een dimensie
             * van type BranchDimension.
             * Daar er geen bijkomend filter op de variabele is geldt de impliciete filter dan:
             * periode moet gelijk zijn
             * Context dimensie mot OOK gelijk zijn.
             * 
             */
            
            
            // first setup parameters ?
            //_taxonomy.Presentation

            foreach (AssertionSet aset in _taxonomy.Assertions)
            {
                XElement asetFormula = new XElement("assertionSet", new XAttribute("name", aset.Label));
                XElement asetGenVarFormula = new XElement("generalVariables");
                XElement asetValueAssertionFormula = new XElement("valueAssertions");
                XElement asetCalcFormula = new XElement("calculations");

                asetFormula.Add(asetGenVarFormula);
                asetFormula.Add(asetValueAssertionFormula);
                asetFormula.Add(asetCalcFormula);

                
                allFormulas.Add(asetFormula);
                // obtain information
              //  if (aset.Assertions.Count(a => a.Calculation == null) > 0) break; // leave out non calcs for now.
                aset.Variables.Where(v => v.Type == VariableTypes.Parameter);
                aset.Variables.Where(v => v.Type == VariableTypes.General);
                aset.Variables.Where(v => v.Type == VariableTypes.Fact);

                Dictionary<string, string> variables = new Dictionary<string, string>();
                Dictionary<string, List<XbrlElement>> factVariableDefinitions = new Dictionary<string, List<XbrlElement>>();
                Dictionary<string, Variable> variableDefinitions = new Dictionary<string, Variable>();

                List<string> factVariables = new List<string>();
                List<string> generalVariables = new List<string>();
                List<string> parameterVariables = new List<string>();
                List<string> calculatedVariabels = new List<string>();
                numericVariables = new List<string>();
                string fname = "Calc_" + aset.Label.Replace("-", "_");

                Dictionary<string, ResultType> vartypes = new Dictionary<string, ResultType>();

                bool multiContext = false;
                bool multiPeriod = false;
                bool definedPeriod = false;

                // ORDER VARIABLES ON USED VARIABLES
                List<Variable> lstVars = new List<Variable>();
                if (aset.Variables.Count(c => c.SelectFormula == null) > 0) 
                    lstVars.AddRange(aset.Variables.Where(c => c.SelectFormula == null));
                if (aset.Variables.Count(c => c.SelectFormula != null && c.SelectFormula.UsedVariables == null) > 0) 
                    lstVars.AddRange(aset.Variables.Where(c => c.SelectFormula != null && c.SelectFormula.UsedVariables == null));
                if (aset.Variables.Count(c => c.SelectFormula != null && c.SelectFormula.UsedVariables != null && c.SelectFormula.UsedVariables.Count==0) > 0)
                    lstVars.AddRange(aset.Variables.Where(c => c.SelectFormula != null && c.SelectFormula.UsedVariables != null && c.SelectFormula.UsedVariables.Count == 0));

                if (aset.Variables.Count(c => c.SelectFormula != null && c.SelectFormula.UsedVariables != null && c.SelectFormula.UsedVariables.Count>0) > 0)
                    lstVars.AddRange(aset.Variables.Where(c => c.SelectFormula != null && c.SelectFormula.UsedVariables != null && c.SelectFormula.UsedVariables.Count > 0));

               
                
                foreach (Variable var in lstVars.Where(v=>v.SelectFormula!=null && v.SelectFormula.UsedVariables!=null).ToList())
                {
                    int myIdx = lstVars.IndexOf(var);
                    foreach (string usedVar in var.SelectFormula.UsedVariables)
                    {
                        
                        if (lstVars.Count(v => v.Name == usedVar) > 0)
                        {
                            int uidx = lstVars.IndexOf(lstVars.First(v => v.Name == usedVar));
                            if (myIdx < uidx)
                            {
                                lstVars.Remove(var);
                                uidx = lstVars.IndexOf(lstVars.First(v => v.Name == usedVar));
                                lstVars.Insert(uidx + 1, var);
                            }
                        }
                    }
                }

                
                //List<string> variablePeriods = new List<string>();

                foreach (Variable vari in lstVars)
                {
                    vari.Name = vari.Name.Replace("-", "_");
                    StringBuilder svar = new StringBuilder();
                    variableDefinitions.Add(vari.Name, vari);
                    
                    if (vari.Type == VariableTypes.General)
                    {
                        if (!generalVariables.Contains(vari.Name)) generalVariables.Add(vari.Name);
                        if (!calculatedVariabels.Contains(vari.Name)) calculatedVariabels.Add(vari.Name);

                        // nothing => is parameter, or set equal to param
                        
                        FormulaRenderer frGen = new FormulaRenderer();
                        asetGenVarFormula.Add(vari.SelectFormula.Formula);
                        frGen.variableTypes = vartypes;
                        Statement fresGen = frGen.ConvertFormula(vari.SelectFormula.Formula);
                        svar.Append("var ").Append(vari.Name).Append(" = ").Append(fresGen.Body).Append(";");
                        vartypes.Add(vari.Name, fresGen.ResultType);
                        if (lastFormulaNumeric.GetValueOrDefault())
                        {
                            numericVariables.Add(vari.Name);
                        }
                    }
                    else if (vari.Type == VariableTypes.Parameter)
                    {
                        if (!parameterVariables.Contains(vari.Name)) parameterVariables.Add(vari.Name);
                        // check name equal to param name
                    }
                    else
                    {
                        if (!factVariables.Contains(vari.Name)) factVariables.Add(vari.Name);
                        factVariableDefinitions.Add(vari.Name, new List<XbrlElement>());
                        // fact variables are lists (enumerables/queryables) of XElements
                        XbrlElement xel = null;

                        
                        
                        svar.Append("var lst").Append(vari.Name).Append(" = GetElements(new string[] { ");

                        StringBuilder sfilters = new StringBuilder();
                        
                        StringBuilder sWhere = new StringBuilder();

                        StringBuilder sConcepts = new StringBuilder();

                        List<XbrlElement> varElDefs = new List<XbrlElement>();

                        foreach (ConceptFilter cf in vari.Filters.OfType<ConceptFilter>())
                        {
                            foreach (string qname in cf.QNames)
                            {
                                if (sConcepts.Length > 0) sConcepts.Append(", ");
                                sConcepts.Append(string.Format("\"{0}\"", qname));
                                string[] Qselectors = qname.Split(new char[] { ':' });

                                xel = _taxonomy.Elements.Values.FirstOrDefault(x => x.Id == string.Join("_", Qselectors));
                                if(xel!=null) varElDefs.Add(xel);
                                factVariableDefinitions[vari.Name].Add(xel);
                            }
                        }
                        if (sConcepts.Length > 0)
                            svar.Append(sConcepts.ToString());
                        svar.Append("}, ");

                        bool vmultiperiod = true;
                        bool vmultiContext = true;

                        PeriodFilter pf = vari.Filters.OfType<PeriodFilter>().FirstOrDefault();
                        string period = null;
                        if (pf != null && pf.Test != "true()")
                        {
                            period = pf.PeriodTest == "ALL" ? null
                                            : pf.PeriodTest == "duration" ? "D"
                                            : pf.PeriodTest == "instant" ? "I-"
                                            : pf.PeriodTest;
                            
                        }
                        if (string.IsNullOrEmpty(period))
                        {
                            vmultiperiod = varElDefs.SelectMany(e => e.Periods).Distinct().Count() > 1;
                            definedPeriod = definedPeriod || true;
                        }
                        else
                        {
                            vmultiperiod = period == "I-" && varElDefs.SelectMany(e=>e.Periods).Distinct().Count()>1;
                        }
                        multiPeriod = multiPeriod || vmultiperiod;

                        svar.Append(!string.IsNullOrEmpty(period) ? string.Format("\"{0}\"",period) : "null");
                        svar.Append(", ");

                        StringBuilder sContextWhere = new StringBuilder();
                        // string[] dims = Scenario.OrderBy(s => s.Dimension).Select(s => s.Dimension).ToArray();
                        //return string.Join("#", dims);
                        List<ContextContent> scens = varElDefs.Where(v=>v.ScenarioDefs!=null && v.ScenarioDefs.Count>0).SelectMany(v => v.ScenarioDefs).Where(s=>s.Scenario!=null && s.Scenario.Count>0).SelectMany(c => c.Scenario).Distinct().ToList();

                        List<string> dimensions = new List<string>();
                        List<string> dimensionValues = new List<string>();
                        foreach (ExplicitDimensionFilter edf in vari.Filters.OfType<ExplicitDimensionFilter>())
                        {
                            /*
                            if (edf.Complement)
                            {
                                dimensions.Add("!"+edf.Dimension);
                                scens = scens.Where(s => s.Dimensions.Count(d => d.XbrlName == edf.Dimension) == 0).ToList();
                            }
                            else
                            {
                                dimensions.Add(edf.Dimension);
                                scens = scens.Where(s => s.Dimensions.Count(d => d.XbrlName == edf.Dimension) > 0).ToList();
                            }*/

                            if (edf.Members != null && edf.Members.Count>0)
                            {
                                if (edf.Complement)
                                {

                                    dimensionValues.AddRange(edf.Members.Select(m => string.Format("!{0}!{1}", edf.Dimension, m)).ToList());
                                    scens = scens.Where(s => s.Dimensions.Count(d => d.XbrlName == edf.Dimension && edf.Members.Contains(d.Domain.DomainMember.XbrlName)) == 0).ToList();
                                }
                                else
                                {
                                    dimensionValues.AddRange(edf.Members.Select(m => string.Format("{0}!{1}", edf.Dimension, m)).ToList());
                                    scens = scens.Where(s => s.Dimensions.Count(d => d.XbrlName == edf.Dimension && edf.Members.Contains(d.Domain.DomainMember.XbrlName)) > 0).ToList();
                                }
                            }
                            else
                            {
                                if (edf.Complement)
                                {
                                    dimensions.Add("!" + edf.Dimension);
                                    scens = scens.Where(s => s.Dimensions.Count(d => d.XbrlName == edf.Dimension) == 0).ToList();
                                }
                                else
                                {
                                    dimensions.Add(edf.Dimension);
                                    scens = scens.Where(s => s.Dimensions.Count(d => d.XbrlName == edf.Dimension) > 0).ToList();
                                }
                            }
                            
                           // sContextWhere.Append("c.Scenario.Count(s => s.Name == \"explicitMember\" && ")
                           //     .Append(string.Format("s.Dimension == \"{0}\" && s.Value == \"{1}\")>0", edf.Dimension, edf.Member));
                        }


                        foreach (TypedDimensionFilter tdf in vari.Filters.OfType<TypedDimensionFilter>())
                        {
                            //if (sContextWhere.Length > 0) sContextWhere.Append(" && ");
                            //sContextWhere.Append("c.Scenario.Count(s => s.Name == \"typedMember\" && ")
                             //   .Append(string.Format("s.Dimension == \"{0}\")>0", tdf.Dimension));
                            if (tdf.Complement)
                            {
                                dimensions.Add("!" + tdf.Dimension);
                                scens = scens.Where(s => s.Dimensions.Count(d => d.XbrlName == tdf.Dimension) == 0).ToList();
                            }
                            else
                            {
                                dimensions.Add(tdf.Dimension);
                                scens = scens.Where(s => s.Dimensions.Count(d => d.XbrlName == tdf.Dimension) > 0).ToList();
                            }
                            
                        }

                        // if a typed dimension is included in de definition, always multi context. (free typed value)
                        vmultiContext = scens.Count > 1 || scens.Count(s=>s.Dimensions.Count(d=>d.DimensionType== DimensionTypes.TypedDimension)>0)>0;

                        multiContext = multiContext || (vmultiContext || vmultiperiod);

                        svar.Append("new string[] {");
                        if (dimensions.Count > 0)
                        {
                            svar.Append(string.Join(",",dimensions.OrderBy(d => d).Select(d=>string.Format("\"{0}\"", d)).ToArray()));
                        }
                        svar.Append("}, ");

                        svar.Append("new string[] {");
                        if (dimensionValues.Count > 0)
                        {
                            svar.Append(string.Join(",", dimensionValues.OrderBy(d => d).Select(d => string.Format("\"{0}\"", d)).ToArray()));
                        }
                        svar.Append("});");

                        List<string> dtetypes = factVariableDefinitions[vari.Name].Where(x=>x!=null).Select(x => x.DataType.RootType).Distinct().ToList();
                        if (dtetypes.Count == 1)
                        {
                            XbrlElement xeld = factVariableDefinitions[vari.Name].Where(x => x != null).First();
                            ResultType resType = null;
                            switch (dtetypes.First().ToLower())
                            {
                                case "system.decimal":
                                    if (!string.IsNullOrEmpty(xeld.DataType.TypeCode) && xeld.DataType.TypeCode.ToLower().Contains("integer"))
                                    {
                                        resType = new ResultType { BroadResultType = BroadResultType.Numeric, DetailedResultType = DetailResultType.Integer };
                                    }
                                    else
                                    {
                                        resType = new ResultType { BroadResultType = BroadResultType.Numeric, DetailedResultType = DetailResultType.Number };
                                    }
                                    break;
                                case "system.int":
                                case "system.int32":
                                case "system.int16":
                                case "system.integer":
                                    resType = new ResultType { BroadResultType = BroadResultType.Numeric, DetailedResultType = DetailResultType.Integer };
                                    break;
                                case "system.string":
                                    resType = new ResultType { BroadResultType = BroadResultType.String, DetailedResultType = DetailResultType.String };
                                    break;
                                case "system.boolean":
                                case "system.bool":
                                    resType = new ResultType { BroadResultType = BroadResultType.Bool, DetailedResultType = DetailResultType.Bool };
                                    break;
                                case "system.datetime":
                                case "system.date":
                                    resType = new ResultType { BroadResultType = BroadResultType.Date, DetailedResultType = DetailResultType.Date };
                                    break;
                                default:
                                    break;

                            }
                            if (resType != null) 
                            {
                                if (factVariableDefinitions[vari.Name].Count(x => x != null) > 1)
                                {
                                    resType.List = true;
                                }
                                vartypes.Add(vari.Name, resType);
                            }
                            
                        }

                    }
                    variables.Add(vari.Name, svar.ToString());
                }

               // valst.First().Calculation
                
                sbCalls.Append(fname).Append("();");
                sbCalls.Append(Environment.NewLine).Append("\t").Append("\t").Append("\t");

                // create function start
                sbFunctions.Append(Environment.NewLine).Append("\t").Append("\t").Append("\t");
                sbFunctions.Append("private void ").Append(fname).Append("()");
                sbFunctions.Append(Environment.NewLine).Append("\t").Append("\t").Append("{");
                sbFunctions.Append(Environment.NewLine).Append("\t").Append("\t").Append("\t");

                sbFunctions.Append(Environment.NewLine).Append("\t").Append("\t").Append("\t");
                sbFunctions.Append("// general variables");
                // variable declarations
                foreach (string var in generalVariables.Where(c => variableDefinitions[c].SelectFormula == null || variableDefinitions[c].SelectFormula.UsedVariables == null || variableDefinitions[c].SelectFormula.UsedVariables.Count == 0))
                {
                    sbFunctions.Append(Environment.NewLine).Append("\t").Append("\t").Append("\t");
                    sbFunctions.Append(variables[var]);
                }
                //foreach(string var in parameterVariables)

                sbFunctions.Append(Environment.NewLine).Append("\t").Append("\t").Append("\t");
                sbFunctions.Append("// fact variables");

                foreach (string var in factVariables)
                {
                    sbFunctions.Append(Environment.NewLine).Append("\t").Append("\t").Append("\t");
                    sbFunctions.Append(variables[var]);
                }

                sbFunctions.Append(Environment.NewLine).Append("\t").Append("\t").Append("\t");
                sbFunctions.Append(Environment.NewLine).Append("\t").Append("\t").Append("\t");

               

                sbFunctions.Append("// Value assertions");
                sbFunctions.Append(Environment.NewLine).Append("\t").Append("\t").Append("\t");
               // sbFunctions.Append("bool assertionResult = true;");
                foreach (ValueAssertion vas in aset.Assertions)
                {
                    sbFunctions.Append(Environment.NewLine).Append("\t").Append("\t").Append("\t");
                    if (vas.Formula != null)
                    {

                        if (!vas.ImplicitFiltering || !multiContext)
                        {
                            foreach (string var in factVariables)
                            {
                                sbFunctions.Append(Environment.NewLine).Append("\t").Append("\t").Append("\t");
                                sbFunctions.Append("var ").Append(var).Append(" = ");
                                if (factVariableDefinitions[var].Count > 1)
                                {
                                    sbFunctions.Append("lst").Append(var).Append(";");
                                }
                                else
                                {
                                    if (!string.IsNullOrEmpty(variableDefinitions[var].FallBackvalue) && variableDefinitions[var].FallBackvalue != "()" && vartypes.ContainsKey(var))
                                    {
                                        string stringFnc = "String";
                                        switch (vartypes[var].BroadResultType.ToString().ToLower())
                                        {
                                            case "bool":
                                                stringFnc = "Bool";
                                                break;
                                            case "date":
                                                stringFnc = "Date";
                                                break;
                                            case "numeric":
                                                stringFnc = "Number";
                                                break;
                                        }

                                        string fbval = variableDefinitions[var].FallBackvalue;
                                        if (fbval == "false()") fbval = "false";
                                        if (fbval == "true()") fbval = "true";


                                        sbFunctions.Append("lst").Append(var).Append(".FirstOrDefault() != null ? ");
                                        sbFunctions.Append(stringFnc).Append("(").Append("lst").Append(var).Append(".FirstOrDefault()) : ");
                                        sbFunctions.Append(stringFnc).Append("(").Append(variableDefinitions[var].FallBackvalue).Append(");");

                                    }
                                    else
                                    {
                                        sbFunctions.Append("lst").Append(var).Append(".FirstOrDefault();");
                                    }
                                }
                            }

                            foreach (string var in generalVariables.Where(c => variableDefinitions[c].SelectFormula != null && variableDefinitions[c].SelectFormula.UsedVariables != null && variableDefinitions[c].SelectFormula.UsedVariables.Count > 0))
                            {
                                sbFunctions.Append(Environment.NewLine).Append("\t").Append("\t").Append("\t");
                                sbFunctions.Append(variables[var]);
                            }

                            sbFunctions.Append(Environment.NewLine).Append("\t").Append("\t").Append("\t");

                            if (aset.Preconditions != null && aset.Preconditions.Count > 0)
                            {
                                ValueAssertion vapre = aset.Preconditions.First();
                                sbFunctions.Append("// Precondition");
                                FormulaRenderer frVasPre = new FormulaRenderer();
                                frVasPre.variableTypes = vartypes;
                                Statement stePre = frVasPre.ConvertFormula(vapre.Formula.Formula);

                                sbFunctions.Append(Environment.NewLine).Append("\t").Append("\t").Append("\t");

                                StringBuilder sbPre = new StringBuilder();
                                foreach (string varpp in vapre.Formula.UsedVariables)
                                {
                                    string varp = varpp.Replace("-", "_");
                                    if (sbPre.Length > 0) sbPre.Append(" && ");
                                    sbPre.Append("Exists(");
                                    if (factVariableDefinitions.ContainsKey(varp))
                                    {
                                        sbPre.Append("lst").Append(varp).Append(".FirstOrDefault()");
                                    }
                                    else
                                    {
                                        sbPre.Append(varp);
                                    }
                                    sbPre.Append(")");
                                }

                                sbFunctions.Append("if(").Append(sbPre.ToString()).Append(" && ").Append(stePre.Body).Append(") {");
                                sbFunctions.Append(Environment.NewLine).Append("\t").Append("\t").Append("\t");

                            }

                            //sbFunctions.Append("assertionResult &= true; // none implicit filtering... TODO");
                            if (vas.Formula != null && vas.Formula.Formula != null)
                            {
                                sbFunctions.Append(Environment.NewLine).Append("\t").Append("\t").Append("\t");
                                FormulaRenderer frVas = new FormulaRenderer();
                                frVas.variableTypes = vartypes;
                                asetValueAssertionFormula.Add(vas.Formula.Formula);
                                Statement fresVas = frVas.ConvertFormula(vas.Formula.Formula);
                                sbFunctions.Append("bool assertionResult = ").Append(fresVas.Body).Append(";");

                                sbFunctions.Append(Environment.NewLine).Append("\t").Append("\t").Append("\t").Append("\t");
                                sbFunctions.Append("if(!assertionResult) {");
                                sbFunctions.Append(Environment.NewLine).Append("\t").Append("\t").Append("\t").Append("\t").Append("\t");
                                sbFunctions.Append("this.HasErrors=true;");
                                sbFunctions.Append(Environment.NewLine).Append("\t").Append("\t").Append("\t").Append("\t").Append("\t");
                                sbFunctions.Append("AddMessage(new BizTaxErrorDataContract {");
                                sbFunctions.Append(Environment.NewLine).Append("\t").Append("\t").Append("\t").Append("\t").Append("\t").Append("\t");
                                sbFunctions.Append("FileId = _fileId, Id = _errId++, ");
                                sbFunctions.Append(Environment.NewLine).Append("\t").Append("\t").Append("\t").Append("\t").Append("\t").Append("\t");
                                sbFunctions.Append("Messages = new List<BizTaxErrorMessageDataContract> { ");
                                sbFunctions.Append(Environment.NewLine).Append("\t").Append("\t").Append("\t").Append("\t").Append("\t").Append("\t");
                                sbFunctions.Append(" new BizTaxErrorMessageDataContract { Culture = \"nl-BE\", Message = \"\"}  ");
                                sbFunctions.Append(Environment.NewLine).Append("\t").Append("\t").Append("\t").Append("\t").Append("\t").Append("\t");
                                sbFunctions.Append(" ,new BizTaxErrorMessageDataContract { Culture = \"fr-FR\", Message = \"\"}  ");
                                sbFunctions.Append(Environment.NewLine).Append("\t").Append("\t").Append("\t").Append("\t").Append("\t").Append("\t");
                                sbFunctions.Append(" ,new BizTaxErrorMessageDataContract { Culture = \"de-DE\", Message = \"\"}  ");
                                sbFunctions.Append(Environment.NewLine).Append("\t").Append("\t").Append("\t").Append("\t").Append("\t").Append("\t");
                                sbFunctions.Append(" ,new BizTaxErrorMessageDataContract { Culture = \"en-US\", Message = \"\"}  ");
                                sbFunctions.Append(Environment.NewLine).Append("\t").Append("\t").Append("\t").Append("\t").Append("\t").Append("\t");
                                sbFunctions.Append(" } ");
                                sbFunctions.Append(Environment.NewLine).Append("\t").Append("\t").Append("\t").Append("\t").Append("\t").Append("\t");
                                sbFunctions.Append(", Fields = new List<string>()");
                                sbFunctions.Append(Environment.NewLine).Append("\t").Append("\t").Append("\t").Append("\t").Append("\t").Append("\t");
                                sbFunctions.Append("}");
                                sbFunctions.Append(Environment.NewLine).Append("\t").Append("\t").Append("\t").Append("\t").Append("\t");
                                sbFunctions.Append(");");
                                sbFunctions.Append(Environment.NewLine).Append("\t").Append("\t").Append("\t").Append("\t");
                                sbFunctions.Append("}");

                            }

                            if (aset.Preconditions != null && aset.Preconditions.Count > 0)
                            {
                                sbFunctions.Append(Environment.NewLine).Append("\t").Append("\t").Append("\t");
                                sbFunctions.Append("}");
                            }
                        }
                        else
                        {
                            // scenario only (not full context)
                            sbFunctions.Append("IEnumerable<ContextElementDataContract> contexts = Xbrl.Contexts;");


                            string firstVar = vas.Formula.UsedVariables.FirstOrDefault();
                            if (string.IsNullOrEmpty(firstVar))
                            {
                                Console.WriteLine("CHECK ASSERSION:: " + vas.Id + ", " + aset.Label);
                            }
                            else
                            {
                                XbrlElement xbel = factVariableDefinitions[firstVar].First();
                                Variable vari = variableDefinitions[firstVar];

                                string period = xbel.PeriodType;
                                if (period == "duration")
                                {
                                    sbFunctions.Append(Environment.NewLine).Append("\t").Append("\t").Append("\t");
                                    sbFunctions.Append("contexts = contexts.Where(c => !string.IsNullOrEmpty(c.Period.StartDate) && !string.IsNullOrEmpty(c.Period.EndDate));");
                                }
                                else if (period == "instant")
                                {
                                    sbFunctions.Append(Environment.NewLine).Append("\t").Append("\t").Append("\t");
                                    sbFunctions.Append("contexts = contexts.Where(c => !string.IsNullOrEmpty(c.Period.Instant));");
                                }


                                // REPLACE WITH DEFINITIONS !!
                                // WHAT TO DO WHEN MORE THEN ONE SCENARIO DEF POSSIBLE?
                                StringBuilder sbDimensionFilters = new StringBuilder();
                                foreach (TypedDimensionFilter tdf in vari.Filters.OfType<TypedDimensionFilter>())
                                {
                                    sbDimensionFilters.Append(Environment.NewLine).Append("\t").Append("\t").Append("\t");
                                    sbDimensionFilters.Append("contexts = contexts.Where(c => c.Scenario != null && c.Scenario.Count(s=>s.Dimension==\"").Append(tdf.Dimension).Append("\")==1);");
                                }
                                int edfCnt = 0;
                                foreach (ExplicitDimensionFilter edf in vari.Filters.OfType<ExplicitDimensionFilter>())
                                {
                                    sbDimensionFilters.Append(Environment.NewLine).Append("\t").Append("\t").Append("\t");
                                    if (edf.Members==null || edf.Members.Count==0)
                                    {
                                        sbDimensionFilters.Append("contexts = contexts.Where(c => c.Scenario != null && c.Scenario.Count(s=>s.Dimension==\"").Append(edf.Dimension).Append("\")==1);");
                                    }
                                    else
                                    {
                                        if (edf.Members.Count > 1)
                                        {
                                            string[] t = new string[] { "" };
                                            sbDimensionFilters.Append("string[] cmembers").Append(edfCnt)
                                                .Append(" = new string[] { ")
                                                .Append(string.Join(",", edf.Members.Select(m => "\"" + m + "\"").ToArray())).Append(" };").Append(Environment.NewLine);

                                            sbDimensionFilters.Append("contexts = contexts.Where(c => c.Scenario != null && c.Scenario.Count(s=>s.Dimension==\"").Append(edf.Dimension)
                                                .Append("\" && cmembers").Append(edfCnt).Append(".Contains(s.Value)==1));");
                                        }
                                        else
                                        {
                                            sbDimensionFilters.Append("contexts = contexts.Where(c => c.Scenario != null && c.Scenario.Count(s=>s.Dimension==\"").Append(edf.Dimension)
                                            .Append("\" && s.Value==\"").Append(edf.Members.First()).Append("\")==1);");
                                        }
                                    }
                                    edfCnt++;
                                }


                                List<string> uniqueCombos = new List<string>();

                                // transform scenario def to qry
                                ContextDefinition cd = xbel.ScenarioDefs.First();
                                List<string> scenarioSelections = new List<string>();
                                bool isEmpty = false;
                                if (cd.Id == Guid.Empty.ToString() || cd.Scenario == null || cd.Scenario.Count == 0)
                                {
                                    uniqueCombos.Add(cd.Id);
                                    isEmpty =true;
                                    scenarioSelections.Add("{0}.Scenario == null || {0}.Scenario.Count == 0");
                                }
                                else
                                {
                                    uniqueCombos = xbel.ScenarioDefs.First().Scenario.Select(s => s.DefName).Distinct().ToList();
                                    StringBuilder scScenTtl = new StringBuilder();
                                    scScenTtl.Append("{0}.Scenario != null && {0}.Scenario.Count > 0");
                                    foreach (string scenario in uniqueCombos)
                                    {
                                        StringBuilder sbScen = new StringBuilder();
                                        string[] dimensions = scenario.Split(new char[] { '/' });
                                        foreach (string dimension in dimensions)
                                        {
                                            if (sbScen.Length > 0) sbScen.Append(" && ");
                                            sbScen.Append("{0}.Scenario.Count(s=>s.Dimension==\"").Append(dimension).Append("\")==1");

                                        }
                                        scScenTtl.Append(" && (").Append(sbScen.ToString()).Append(")");
                                    }
                                    scenarioSelections.Add(scScenTtl.ToString());
                                }

                                if (sbDimensionFilters.Length == 0)
                                {

                                    foreach (string scenselect in scenarioSelections)
                                    {
                                        sbDimensionFilters.Append(Environment.NewLine).Append("\t").Append("\t").Append("\t");
                                        sbDimensionFilters.Append("contexts = contexts.Where(c =>").Append(string.Format(scenselect, "c")).Append(");");
                                    }

                                }

                                

                                sbFunctions.Append(sbDimensionFilters.ToString());

                                sbFunctions.Append(Environment.NewLine).Append("\t").Append("\t").Append("\t");
                                sbFunctions.Append(Environment.NewLine).Append("\t").Append("\t").Append("\t");
                               /* if (!multiPeriod)
                                {
                                   
                                    sbFunctions.Append("foreach (string ScenDef in contexts.Select(c=>c.ScenarioDef).Distinct()))");
                                }
                                else
                                {*/
                                    sbFunctions.Append("foreach (ContextElementDataContract cedc in contexts)");
                                //}
                                sbFunctions.Append(Environment.NewLine).Append("\t").Append("\t").Append("\t").Append("{");
                                // fact variables
                                sbFunctions.Append(Environment.NewLine).Append("\t").Append("\t").Append("\t").Append("\t");

                               
                                foreach (string var in factVariables)
                                {
                                    sbFunctions.Append(Environment.NewLine).Append("\t").Append("\t").Append("\t").Append("\t");
                                    sbFunctions.Append("var ").Append(var).Append(" = ").Append("lst").Append(var);
                                    StringBuilder sbInlineFact = new StringBuilder();
                                    Variable vardef = variableDefinitions[var];
                                    XbrlElement xbelDef = factVariableDefinitions[var].First();


                                    bool hasScenarioEquality = false;
                                    if (xbelDef != null)
                                    {
                                        if (xbelDef.ScenarioDefs != null)
                                        {
                                            hasScenarioEquality = xbelDef.ScenarioDefs.Count(s => uniqueCombos.Contains(s.Id) || (s.Scenario != null && s.Scenario.Count(d => uniqueCombos.Contains(d.DefName)) > 0)) > 0;
                                        }
                                        else
                                        {
                                            hasScenarioEquality = uniqueCombos.Contains(Guid.Empty.ToString());
                                        }
                                    }
                                    else
                                    {
                                        Console.WriteLine("DEFINITION NOT FOUND: " + var);
                                    }
                                    if (multiPeriod)
                                    {

                                    }
                                    else
                                    {
                                    }
                                    if (vardef.Filters.OfType<PeriodFilter>().Count() == 0)
                                    {

                                        if (vardef.Filters.OfType<TypedDimensionFilter>().Count() > 0 || vardef.Filters.OfType<ExplicitDimensionFilter>().Count() > 0)
                                        {
                                            // Only period
                                            if (period == "instant")
                                            {
                                                sbInlineFact.Append("(GetContext(e.ContextRef).Period.Instant == cedc.Period.Instant)");
                                            }
                                            else
                                            {
                                                sbInlineFact.Append("(GetContext(e.ContextRef).Period.StartDate == cedc.Period.StartDate && GetContext(e.ContextRef).Period.EndDate == cedc.Period.EndDate)");
                                            }
                                        }
                                        else
                                        {
                                            // complete context (?) 
                                            if (!hasScenarioEquality)
                                            {
                                                // Only period
                                                if (period == "instant")
                                                {
                                                    sbInlineFact.Append("(GetContext(e.ContextRef).Period.Instant == cedc.Period.Instant)");
                                                }
                                                else
                                                {
                                                    sbInlineFact.Append("(GetContext(e.ContextRef).Period.StartDate == cedc.Period.StartDate && GetContext(e.ContextRef).Period.EndDate == cedc.Period.EndDate)");
                                                }
                                            }
                                            else
                                            {
                                                sbInlineFact.Append("(e.ContextRef == cedc.Id)");
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (vardef.Filters.OfType<TypedDimensionFilter>().Count() == 0 && vardef.Filters.OfType<ExplicitDimensionFilter>().Count() == 0)
                                        {
                                            // only on context definition?
                                            foreach (string ssel in scenarioSelections)
                                            {
                                                if (sbInlineFact.Length > 0) sbInlineFact.Append(" && ");
                                                sbInlineFact.Append("(").Append(string.Format(ssel, "GetContext(e.ContextRef)")).Append(")");
                                            }
                                        }
                                        else
                                        {
                                            if (hasScenarioEquality && sbInlineFact.Length==0)
                                            {
                                                sbInlineFact.Append("(e.ContextRef == cedc.Id)");
                                            }
                                        }
                                    }
                                    if (sbInlineFact.Length > 0)
                                    {
                                        sbFunctions.Append(".Where(e=> ").Append(sbInlineFact.ToString()).Append(");");
                                    }
                                    else
                                    {
                                        sbFunctions.Append(";");
                                    }

                                }

                                sbFunctions.Append(Environment.NewLine).Append("\t").Append("\t").Append("\t").Append("\t");
                                List<string> genVars = new List<string>();

                                foreach (string var in generalVariables.Where(c => variableDefinitions[c].SelectFormula != null && variableDefinitions[c].SelectFormula.UsedVariables != null && variableDefinitions[c].SelectFormula.UsedVariables.Count > 0))
                                {
                                    if (genVars.Count > 0)
                                    {
                                        Variable vdef = variableDefinitions[var];
                                        if (vdef.SelectFormula != null && vdef.SelectFormula.UsedVariables != null)
                                        {
                                            if (vdef.SelectFormula.UsedVariables.Count == 0)
                                            {
                                                genVars.Insert(0, var);
                                            }
                                            else
                                            {
                                                int mx = 0;
                                                foreach (string usedvar in vdef.SelectFormula.UsedVariables)
                                                {
                                                    if (genVars.Contains(usedvar))
                                                    {
                                                        mx = Math.Max(mx, genVars.IndexOf(usedvar) + 1);

                                                    }
                                                }
                                                genVars.Insert(mx, var);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        genVars.Add(var);
                                    }
                                    
                                }

                                foreach (string var in genVars)
                                {
                                    sbFunctions.Append(Environment.NewLine).Append("\t").Append("\t").Append("\t");
                                    sbFunctions.Append(variables[var]);
                                }
                                // calculation here ? (what if precondition was met before and not now... calculated field still needs to update
                                string calEl = null;
                                if (vas.Calculation != null && vas.Calculation.Formula != null)
                                {
                                    sbFunctions.Append(Environment.NewLine).Append("\t").Append("\t").Append("\t").Append("\t");
                                    if (string.IsNullOrEmpty(vas.Documentation.Target))
                                    {
                                        Console.WriteLine("ERROR: CALCULATION WITHOUT TARGET!");
                                    }
                                    else
                                    {
                                        string target = vas.Documentation.Target.Replace("$", "");
                                        calEl = target ;
                                        XbrlElement xbelTarget = null;
                                        if (variableDefinitions.ContainsKey(target))
                                        {
                                            xbelTarget = factVariableDefinitions[target].First();

                                            string sourceVar = vas.Calculation.UsedVariables.Where(v => factVariables.Contains(v)).FirstOrDefault();
                                            if (string.IsNullOrEmpty(sourceVar))
                                            {
                                                // what if null, and use filters to suggest
                                                sourceVar = factVariables.First();
                                            }
                                            string decimals = "null";
                                            string unit = "null";
                                            if (xbelTarget.DataType != null)
                                            {
                                                if (xbelTarget.DataType.Attributes.ContainsKey("decimals"))
                                                {
                                                    decimals = "\"INF\"";
                                                }
                                                if (xbelTarget.DataType.Attributes.ContainsKey("unitRef"))
                                                {
                                                    unit = string.Format("\"{0}\"", xbelTarget.DataType.Attributes["unitRef"].Value);
                                                }
                                            }

                                            sbFunctions.Append("var el").Append(target).Append(" = ").Append(target).Append(".FirstOrDefault();");
                                            sbFunctions.Append(Environment.NewLine).Append("\t").Append("\t").Append("\t").Append("\t");
                                            sbFunctions.Append("if(el").Append(target).Append(" == null)");
                                            sbFunctions.Append(Environment.NewLine).Append("\t").Append("\t").Append("\t").Append("\t");
                                            sbFunctions.Append("{");
                                            sbFunctions.Append(Environment.NewLine).Append("\t").Append("\t").Append("\t").Append("\t").Append("\t");
                                            sbFunctions.Append("el").Append(target).Append(" = ").Append(string.Format("CreateNewElement(\"{0}\",\"{1}\",\"{2}\",{3},{4},cedc,", xbelTarget.Name, xbelTarget.NameSpace, xbelTarget.PeriodType,decimals,unit));
                                            sbFunctions.Append(sourceVar).Append(");");
                                            sbFunctions.Append(Environment.NewLine).Append("\t").Append("\t").Append("\t").Append("\t").Append("\t");
                                            sbFunctions.Append("this.Xbrl.Elements.Add(el").Append(target).Append(");");
                                            sbFunctions.Append(Environment.NewLine).Append("\t").Append("\t").Append("\t").Append("\t");
                                            sbFunctions.Append("}");
                                            sbFunctions.Append(Environment.NewLine).Append("\t").Append("\t").Append("\t").Append("\t");

                                        
                                            FormulaRenderer frVas2 = new FormulaRenderer();
                                            frVas2.variableTypes = vartypes;
                                            Statement fresVas2 = frVas2.ConvertFormula(vas.Calculation.Formula);
                                            asetCalcFormula.Add(vas.Calculation.Formula);
                                            sbFunctions.Append("el").Append(target).Append(".SetNumber(").Append(fresVas2.Body).Append(");");

                                           
                                        }
                                        else
                                        {
                                            sbFunctions.Append("// unknown target " + vas.Documentation.Target);
                                            Console.WriteLine("Unknown target");
                                        }
                                    }
                                }

                                // precondition here

                                // or calculation here ?

                                // validation here
                                if (vas.Formula != null && vas.Formula.Formula!=null)
                                {
                                    if (aset.Preconditions != null && aset.Preconditions.Count > 0)
                                    {
                                        ValueAssertion vapre = aset.Preconditions.First();
                                        sbFunctions.Append("// Precondition");
                                        FormulaRenderer frVasPre = new FormulaRenderer();
                                        frVasPre.variableTypes = vartypes;
                                        Statement stePre = frVasPre.ConvertFormula(vapre.Formula.Formula);

                                        sbFunctions.Append(Environment.NewLine).Append("\t").Append("\t").Append("\t");

                                        StringBuilder sbPre = new StringBuilder();
                                        foreach (string varp in vapre.Formula.UsedVariables)
                                        {
                                            if (sbPre.Length > 0) sbPre.Append(" && ");
                                            sbPre.Append("Exists(").Append(varp).Append(")");
                                        }

                                        sbFunctions.Append("if(").Append(sbPre.ToString()).Append(" && ").Append(stePre.Body).Append(") {");
                                
                                       // sbFunctions.Append("if(").Append(stePre.Body).Append(") {");
                                        sbFunctions.Append(Environment.NewLine).Append("\t").Append("\t").Append("\t");

                                    }
                                    FormulaRenderer frVas3 = new FormulaRenderer();
                                    frVas3.variableTypes = vartypes;
                                    Statement fresVas3 = frVas3.ConvertFormula(vas.Formula.Formula);
                                    asetValueAssertionFormula.Add(vas.Formula.Formula);
                                    sbFunctions.Append(Environment.NewLine).Append("\t").Append("\t").Append("\t").Append("\t");
                                    string fbody = fresVas3.Body;
                                    if (!string.IsNullOrEmpty(calEl))
                                    {
                                        Regex re = new Regex(calEl + @"\b");
                                        fbody = re.Replace(fbody, "el" + calEl);
                                    }
                                    sbFunctions.Append("bool assertionResult = ").Append(fbody).Append(";");

                                    sbFunctions.Append(Environment.NewLine).Append("\t").Append("\t").Append("\t").Append("\t");
                                    sbFunctions.Append("if(!assertionResult) {");
                                    sbFunctions.Append(Environment.NewLine).Append("\t").Append("\t").Append("\t").Append("\t").Append("\t");
                                    sbFunctions.Append("this.HasErrors=true;");
                                    sbFunctions.Append(Environment.NewLine).Append("\t").Append("\t").Append("\t").Append("\t").Append("\t");
                                    sbFunctions.Append("AddMessage(new BizTaxErrorDataContract {");
                                    sbFunctions.Append(Environment.NewLine).Append("\t").Append("\t").Append("\t").Append("\t").Append("\t").Append("\t");
                                    sbFunctions.Append("FileId = _fileId, Id = _errId++, ");
                                    sbFunctions.Append(Environment.NewLine).Append("\t").Append("\t").Append("\t").Append("\t").Append("\t").Append("\t");
                                    sbFunctions.Append("Messages = new List<BizTaxErrorMessageDataContract> { ");
                                    sbFunctions.Append(Environment.NewLine).Append("\t").Append("\t").Append("\t").Append("\t").Append("\t").Append("\t");
                                    sbFunctions.Append(" new BizTaxErrorMessageDataContract { Culture = \"nl-BE\", Message = \"\"}  ");
                                    sbFunctions.Append(Environment.NewLine).Append("\t").Append("\t").Append("\t").Append("\t").Append("\t").Append("\t");
                                    sbFunctions.Append(" ,new BizTaxErrorMessageDataContract { Culture = \"fr-FR\", Message = \"\"}  ");
                                    sbFunctions.Append(Environment.NewLine).Append("\t").Append("\t").Append("\t").Append("\t").Append("\t").Append("\t");
                                    sbFunctions.Append(" ,new BizTaxErrorMessageDataContract { Culture = \"de-DE\", Message = \"\"}  ");
                                    sbFunctions.Append(Environment.NewLine).Append("\t").Append("\t").Append("\t").Append("\t").Append("\t").Append("\t");
                                    sbFunctions.Append(" ,new BizTaxErrorMessageDataContract { Culture = \"en-US\", Message = \"\"}  ");
                                    sbFunctions.Append(Environment.NewLine).Append("\t").Append("\t").Append("\t").Append("\t").Append("\t").Append("\t");
                                    sbFunctions.Append(" } ");
                                    sbFunctions.Append(Environment.NewLine).Append("\t").Append("\t").Append("\t").Append("\t").Append("\t").Append("\t");
                                    sbFunctions.Append(", Fields = new List<string>()");
                                    sbFunctions.Append(Environment.NewLine).Append("\t").Append("\t").Append("\t").Append("\t").Append("\t").Append("\t");
                                    sbFunctions.Append("}");
                                    sbFunctions.Append(Environment.NewLine).Append("\t").Append("\t").Append("\t").Append("\t").Append("\t");
                                    sbFunctions.Append(");");
                                    sbFunctions.Append(Environment.NewLine).Append("\t").Append("\t").Append("\t").Append("\t");
                                    sbFunctions.Append("}");
                                    if (aset.Preconditions != null && aset.Preconditions.Count > 0)
                                    {
                                        sbFunctions.Append(Environment.NewLine).Append("\t").Append("\t").Append("\t").Append("\t");
                                        sbFunctions.Append("}");
                                    }
                                }

                                sbFunctions.Append(Environment.NewLine).Append("\t").Append("\t").Append("\t").Append("}");

                                
                            }
                        }
                    }
                }
                sbFunctions.Append(Environment.NewLine).Append("\t").Append("\t").Append("}");
                sbFunctions.Append(Environment.NewLine).Append("\t").Append("\t");
                

                // foreach
                

                /*
                if(aset.Assertions.Count(a=>a.Calculation!=null)>0) {
                    

                  
                    

                    

                    StringBuilder sbVariables = new StringBuilder();

                   

                    StringBuilder sbCalc = new StringBuilder();
                    foreach (ValueAssertion vas in aset.Assertions)
                    {
                       
                        if (vas.ImplicitFiltering)
                        {
                            // check if for each period or contextdef or period/contextdef

                            string firstVar = vas.Formula.UsedVariables.First();
                            XbrlElement xbel = factVariableDefs[firstVar].First();
                            Variable vari = factVariables[firstVar];
                            string period = "DEF";
                            List<string> periods = new List<string>();
                            List<Filter> dimensionFilters = new List<Filter>();
                            if (vari.Filters.OfType<PeriodFilter>().Count() > 0)
                            {
                                // has period filter
                                PeriodFilter pf = vari.Filters.OfType<PeriodFilter>().First();
                                if (pf.PeriodTest == "duration" || pf.PeriodTest.StartsWith("I-"))
                                {
                                    // Fixed period.
                                    periods.Add(pf.PeriodTest == "duration" ? "D" : pf.PeriodTest);
                                }
                            } 

                            period = xbel.PeriodType;

                            IEnumerable<ContextDefinition> cdefs = xbel.ScenarioDefs;
                            ContextDefinition cdToSearch = null;
                            if (cdefs.Select(c => c.DefName).Distinct().Count() == 1)
                            {
                                cdToSearch = cdefs.First();
                            }
                            else
                            {

                                if (vari.Filters.OfType<TypedDimensionFilter>().Count() > 0)
                                {
                                    foreach (TypedDimensionFilter tdf in vari.Filters.OfType<TypedDimensionFilter>())
                                    {
                                        cdefs = cdefs.Where(c => c.DefName.Contains(tdf.Dimension));
                                    }
                                }
                                if (vari.Filters.OfType<ExplicitDimensionFilter>().Count() > 0)
                                {
                                    foreach (ExplicitDimensionFilter edf in vari.Filters.OfType<ExplicitDimensionFilter>())
                                    {
                                        cdefs = cdefs.Where(c => c.DefName.Contains(edf.Dimension));
                                        if (!string.IsNullOrEmpty(edf.Member))
                                        {
                                            cdefs = cdefs.Where(c => c.Scenario.Count(s => s.DefName.Contains(edf.Dimension) && s.Dimensions.Count(d => d.Domain != null && d.Domain.DomainMember != null && d.Domain.DomainMember.XbrlName == edf.Member) > 0) > 0);
                                        }
                                    }
                                }
                                
                                cdToSearch = cdefs.FirstOrDefault();
                            }
                            if (cdToSearch!=null && cdToSearch.DefId == Guid.Empty.ToString()) cdToSearch = null;

                            Console.WriteLine(aset.Label + "::");
                            if (periods.Count > 0) Console.WriteLine("foreach period in " + string.Join(",", periods.ToArray()));
                            if (cdToSearch != null) Console.WriteLine("foreach context in " + cdToSearch.DefId);
                            /*
                            IEnumerable<ContextElementDataContract> context = Xbrl.Contexts;
                            if (period == "duration")
                            {
                                context = context.Where(c => !string.IsNullOrEmpty(c.Period.StartDate) && !string.IsNullOrEmpty(c.Period.EndDate));
                            }
                            else
                            {
                                context = context.Where(c => !string.IsNullOrEmpty(c.Period.Instant));
                            }

                            if (cdToSearch != null)
                            {
                                context = context.Where(c=>);
                            }
                            

                        }

                       
                        if (vas.Calculation != null)
                        {
                            if (vas.Calculation.UsedVariables.Count(v => !variables.Contains(v)) > 0)
                            {
                                Console.WriteLine(fname + " VARIABLES NOT FOUND: ");
                                Console.WriteLine(string.Join(",",vas.Calculation.UsedVariables.Where(v => !variables.Contains(v)).ToArray()));
                                Console.WriteLine(" ");
                                sbVariables.Append(Environment.NewLine).Append("\t").Append("\t").Append("\t");
                                sbVariables.Append("// MISSING CALCULATION VARIABLES");
                                foreach (string variable in vas.Calculation.UsedVariables.Where(v => !variables.Contains(v)))
                                {
                                    sbVariables.Append(Environment.NewLine).Append("\t").Append("\t").Append("\t");
                                    sbVariables.Append("var ").Append(variable).Append(" = Xbrl.Elements");
                                    var eldefs = _taxonomy.Elements.Values.Where(e => e.Name == variable).ToList();
                                    if (eldefs == null || eldefs.Count == 0)
                                    {
                                        Console.WriteLine("CAN'T FIND ELEMENT FOR VARIABLE " + variable);
                                        sbVariables.Append(" = null; // NO ELEMENT FOUND");
                                    }
                                    else if (eldefs.Count > 1)
                                    {
                                        Console.WriteLine("MORE THEN 1 POSSIBLE ELEMENT FOR VARIABLE " + variable);
                                        sbVariables.Append(" = null; // MULTIPLE ELEMENTS");
                                    }
                                    else
                                    {
                                        sbVariables.Append("var ").Append(variable).Append(" = Xbrl.Elements;");
                                        
                                    }
                                }
                            }
                            
                            string target = vas.Documentation.Target.Replace("$","");
                            sbCalc.Append(Environment.NewLine).Append("\t").Append("\t").Append("\t");
                            sbCalc.Append(string.Format("foreach(string period in el{0}.Select(e=>e.Period))",vas.Calculation.UsedVariables.First())).Append(" { ");
                            foreach (string elVar in vas.Calculation.UsedVariables.Where(v => !genvariables.Contains(v)))
                            {
                                sbCalc.Append(Environment.NewLine).Append("\t").Append("\t").Append("\t").Append("\t");
                                sbCalc.Append("var ").Append(elVar).Append(" = el").Append(elVar).Append(".Where(e=>e.Period==period);");
                            }

                            sbCalc.Append(Environment.NewLine).Append("\t").Append("\t").Append("\t").Append("\t");
                            sbCalc.Append("var calcTarget = ").Append(target).Append(".FirstOrDefault();");
                            sbCalc.Append(Environment.NewLine).Append("\t").Append("\t").Append("\t").Append("\t");
                            sbCalc.Append("if(calcTarget==null) {");
                            sbCalc.Append(Environment.NewLine).Append("\t").Append("\t").Append("\t").Append("\t").Append("\t");
                            sbCalc.Append("calcTarget = new XbrlElementDataContract();");
                            sbCalc.Append(Environment.NewLine).Append("\t").Append("\t").Append("\t").Append("\t").Append("}");
                            sbCalc.Append(Environment.NewLine).Append("\t").Append("\t").Append("\t").Append("\t");
                            sbCalc.Append("calcTarget.SetNumber(").Append(ConvertFormula(vas.Calculation.Formula)).Append(");");
                            sbCalc.Append(Environment.NewLine).Append("\t").Append("\t").Append("\t");
                        }
                    }

                    sbFunctions.Append(sbVariables.ToString());
                    sbFunctions.Append(sbCalc.ToString());

                    sbFunctions.Append(Environment.NewLine).Append("\t").Append("\t").Append("}");
                    
                    
                }
                */
            }


            template = template.Replace("<!CALCULATION_CALLS!>", sbCalls.ToString());
            template = template.Replace("<!CALCULATIONS!>", sbFunctions.ToString());
        }

        private string ConvertFunction(XElement el)
        {
            bool expectNumeric = false;
            if(el.Name!="function") return string.Empty;
            StringBuilder sb = new StringBuilder();
            switch (el.Attribute("name").Value)
            {
                case "yearMonthDuration":
                    sb.Append("YearMonthDuration");
                    break;
                case "dayTimeDuration":
                    sb.Append("DayTimeDuration");
                    break;
                case "date":
                    sb.Append("Date");
                    break;
                case "days-from-duration":
                    sb.Append("DaysFromDuration");
                    break;
                case "count":
                    sb.Append("Count");
                    break;
                case "not":
                    sb.Append("!");
                    break;
                case "fact-explicit-scenario-dimension-value":

                    sb.Append("FactExplicitScenarioDimensionValue");
                    break;
                case "data":

                    sb.Append("Data");
                    break;
                case "local-name-from-QName":

                    sb.Append("LocalNameFromQname");
                    break;
                case "substring":
                    sb.Append("Substring");
                    break;
                case "string-length":
                    sb.Append("StringLength");
                    break;
                case "substring-before":

                    sb.Append("SubstringBefore");
                    break;
                case "substring-after":

                    sb.Append("SubstringAfter");
                    break;
                case "QName":
                    sb.Append("QName");
                    break;
                case "max":
                    expectNumeric = true;
                    lastFormulaNumeric = true;
                    sb.Append("Max");
                    break;
                case "min":
                    expectNumeric = true;
                    lastFormulaNumeric = true;
                    sb.Append("Min");
                    break;
                case "number":
                    sb.Append("Number");
                    break;
                case "round":
                    expectNumeric = true;
                    lastFormulaNumeric = true;
                    sb.Append("Math.Round");
                    break;
                case "sum":
                    expectNumeric = true;
                    lastFormulaNumeric = true;
                    sb.Append("Sum");
                    break;
                case "string":
                    expectNumeric = false;;
                    sb.Append("String");
                    break;
                default:
                    sb.Append(el.Attribute("name").Value);
                    break;
            }
            sb.Append("(");
            List<string> args = new List<string>();
            foreach (XElement arg in el.Elements("argument"))
            {
                args.Add(ConvertFormula(arg,expectNumeric));
            }
            if (args.Count > 0)
            {
                sb.Append(string.Join(",", args.ToArray()));
            }
            sb.Append(")");
            if (el.ElementsAfterSelf().Count()>0 && el.ElementsAfterSelf().First().Name.LocalName == "comma")
            {
                XElement after = el.ElementsAfterSelf().First();
                after.ReplaceWith(new XElement("plus"));
            }
            return sb.ToString();
        }

        private string StartConvertFormula(XElement el)
        {
            lastFormulaNumeric = null;
            return ConvertFormula(el, false);
            
        }

        private string StartConvertFormula(XElement el,bool numeric)
        {
            lastFormulaNumeric = numeric;
            return ConvertFormula(el, numeric);

        }

        private string ConvertFormula(XElement el)
        {
            return ConvertFormula(el, false);
        }

        private bool IsNumeric(IEnumerable<XElement> el)
        {
            bool isnum = false;
            foreach (XElement cel in el)
            {
                isnum = isnum || IsNumeric(cel);
                if (cel.HasElements)
                {
                    isnum = isnum || IsNumeric(cel.Elements());
                }
            }
            return isnum;
        }


        private bool IsNumeric(XElement el)
        {
            if (el == null) return false;
            switch (el.Name.LocalName)
            {
                case "plus":
                    
                case "minus":
                    
                case "multiply":
                    
                case "divide":
                    
                case "mod":
               
                case "greaterThen":
                
                case "lowerThen":
                
                case "greaterOrEqualThen":
                
                case "lowerOrEqualThen":
                  
                case "number":
                    return true;
                case "variable":
                    return numericVariables.Contains(el.Attribute("name").Value.Replace("-","_"));
                    
                case "function":
                    switch (el.Attribute("name").Value)
                    {
                        case "string-length":
                        case "max":
                        case "min":
                        case "round":
                        case "sum":
                            return true;
                        default:
                            return false;
                    }
                    break;
                default:
                    return false;
            }
        }

        private string ConvertFormula(XElement el, bool expectNumeric)
        {
            StringBuilder sb = new StringBuilder();
            switch (el.Name.LocalName)
            {
                case "group":
                    if (el.Elements().Count() > 0)
                    {
                        sb.Append("(");
                        foreach (XElement child in el.Elements())
                        {
                            sb.Append(ConvertFormula(child));
                            sb.Append(" ");
                        }
                        sb.Append(")");
                    }
                    return sb.ToString();
                    break;
                case "plus":
                    return " + ";
                case "minus":
                    return " - ";
                case "multiply":
                    return " * ";
                case "divide":
                    return " / ";
                case "mod":
                    return " % ";
                case "notequals":
                    return "!=";
                case "greaterThen":
                    return " > ";
                case "lowerThen":
                    return " < ";
                case "greaterOrEqualThen":
                    return " >= ";
                case "lowerOrEqualThen":
                    return " <= ";
                case "comma":
                    return ",";
                case "or":
                    return " || ";
                case "and":
                    return " && ";
                case "equals":
                    return "==";
                case "variable":
                    if (expectNumeric || (!expectNumeric && (IsNumeric(el.ElementsBeforeSelf().Take(3)) || IsNumeric(el.ElementsAfterSelf().Take(3)))))
                    {
                        return "Number(" + el.Attribute("name").Value.Replace("-","_") + ")";
                    }
                    return el.Attribute("name").Value;
                case "string":
                    return string.Format("\"{0}\"", el.Value);
                case "nodepath":
                    string npath = el.Attribute("path").Value;
                    StringBuilder npSb = new StringBuilder();
                    string[] npathSplitted = npath.Split(new char[] { '/' });

                    for(int i=0;i<npathSplitted.Length;i++) 
                    {
                        switch (npathSplitted[i])
                        {
                            case "xbrli:xbrl":
                                break;
                            case "xbrli:context":
                                npSb.Append(".Contexts");
                                break;
                            case "xbrli:entity":
                                npSb.Append(".Select(c=>c.Entity)");
                                break;
                            case "xbrli:identifier":
                                npSb.Append(".Select(e=>e.IdentifierValue)");
                                break;
                            case "xbrli:period":
                                npSb.Append(".Select(c=>c.Period)");
                                break;
                            case "xbrli:instant":
                                npSb.Append(".Select(p=>p.Instant)");
                                break;
                            case "xbrli:startDate":
                                npSb.Append(".Select(p=>p.StartDate)");
                                break;
                            case "xbrli:endDate":
                                npSb.Append(".Select(p=>p.EndDate)");
                                break;
                            case "xbrli:unit":
                                npSb.Append(".Units");
                                break;
                            case "xbrli:measure":
                                npSb.Append(".Select(u=>u.Measure)");
                                break;
                        }
                    }
                    if (npSb.Length > 0)
                    {
                        return string.Format("Xbrl{0}.ToList()", npSb.ToString());
                    }
                    else
                    {
                        return string.Format("null /* {0} */", npath);
                    }
                    return string.Format("\"{0}\"", npath);
                case "number":
                    return "(decimal)"+el.Value;
                case "function":
                    return ConvertFunction(el);
                    break;
                case "if":
                    sb.Append(ConvertFormula(el.Element("when")));
                    sb.Append(" ? ").Append(ConvertFormula(el.Element("then")));
                    sb.Append(" : ").Append(ConvertFormula(el.Element("else")));
                    return sb.ToString();
                case "foreach":
                    string forIn = ConvertFormula(el.Element("in"));
                    string foreachvar = el.Element("each").Attribute("name").Value;
                    string forselect = ConvertFormula(el.Element("return"));
                    sb.Append(forIn.Trim()).Append(".Select(e=>e.Value).Select(").Append(foreachvar).Append("=>").Append(forselect).Append(").ToList()");
                    return sb.ToString();
                default:
                    if (el.HasElements)
                    {
                        if (el.Name.LocalName.ToLower() == "formula")
                        {
                            List<XElement> ors = el.XPathSelectElements("//*[name()='or']").ToList();
                            foreach (XElement or in ors)
                            {
                                if (IsNumeric(or.ElementsBeforeSelf().Take(1)) && IsNumeric(or.ElementsAfterSelf().Take(1)))
                                {
                                    XElement func = new XElement("function",new XAttribute("name","NumericOr"));
                                    func.Add(new XElement("argument", or.ElementsBeforeSelf().ToList()));
                                    func.Add(new XElement("argument", or.ElementsAfterSelf().ToList()));
                                    XElement par = or.Parent;
                                    par.RemoveNodes();
                                    par.Add(func);
                                }
                            }
                        }
                        foreach (XElement child in el.Elements())
                        {
                            sb.Append(ConvertFormula(child,expectNumeric));
                            sb.Append(" ");
                        }
                        return sb.ToString();
                    }
                    break;
            }

            return string.Empty;
        }
    }


}
