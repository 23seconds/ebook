﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Xml.XPath;
using EY.com.eBook.BizTaxTaxonomyParser.Contracts;

namespace EY.com.eBook.BizTaxTaxonomyParser.Renderers
{
    public class FormulaRenderer
    {
        // CHANGE RENDERING.
        // FIRST OF: DETECT RESULT TYPES (functions, statements)
        // SECOND: DETERMINE EXPECTED RESULTTYPES.
        // THIRD: RENDER

        // TODO VARIABLES AND THEIR TYPES 
        // DEFAULT NULL
        // POSSIBLE: LIST STRINGS / LIST OBJECTS

        private Dictionary<string, Statement> _cachedIndex = new Dictionary<string, Statement>();
        private Dictionary<string, List<string>> _parentChildren = new Dictionary<string, List<string>>();
        
        // default actions

        //list of variables

        private XElement _structure = new XElement("structure");

        public Dictionary<string, ResultType> variableTypes { get; set; }


        public List<VariableInfo> VariableInfos { get; set; }

        private XElement FixCommaLists(XElement el)
        {
            var els = el.XPathSelectElements("//comma");
            if (els.Count() > 0)
            {
                var parels = els.Select(e => e.Parent).Distinct().ToList();
                els.ToList().ForEach(x=> { x.Remove(); });
                foreach (XElement parel in parels)
                {
                    var cels = parel.Elements().ToList();
                    parel.RemoveNodes();
                    XElement list = new XElement("function", new XAttribute("name", "list"));
                    cels.ForEach(c=> {
                        list.Add(new XElement("argument", c));
                    });
                    parel.Add(list);
                }
            }
            return el;
        }

        public Statement ConvertFormula(XElement el)
        {
            return ConvertFormula(el, false);
        }

        public Statement ConvertFormula(XElement el,bool isAssertion)
        {
            // el should always be formula, but doesn't matter. ;-)
            el = XElement.Parse(el.ToString());
            el = FixCommaLists(el);
            //variableTypes = new Dictionary<string, ResultType>();
            //variableTypes.Add("RateShippingResultNotTonnageBased", new ResultType { BroadResultType= BroadResultType.Numeric, DetailedResultType = DetailResultType.Number});
           
            // FIRST PROCESSING:
            // DISCOVER FUNCTIONS AND STATEMENTS
            // WHERE RESULT TYPE IS KNOWN SET RESULTTYPE
            ProcessItem(el,new List<int>());
            ProcessStringToNumberComparisons();
            ProcessTypes();


            // CONSTRUCT BODY (combine + check typing)


            Statement formula = ConstructBody(_cachedIndex["1"]);
            if (isAssertion && (formula.ResultType.BroadResultType != BroadResultType.Bool || formula.ResultType.List))
            {
                formula.Body = string.Format("Bool({0})", formula.Body);
            }

     //       PostProcess();
         //   FunctionResult fr = ProcessItem(el, new FunctionResultType { BroadResultType = BroadResultType.ANY }, null);


            // for each return: f-corp-1417
           // (for $identifier in $ComparableIdentifierValues return 
		   // (every $item in $PrepaymentReferenceNumberNotEntityIdentifiers satisfies xs:string($item) ne $identifier))
           // ComparableIdentifierValues.ToList().ForEach(identifier=> { 
            //    return PrepaymentReferenceNumberNotEntityIdentifiers.Where(

           // fr.Body = CompileFormulaBody("1");
           // return fr;
            return formula;
        }

        private void ProcessStringToNumberComparisons()
        {
           // throw new NotImplementedException();
        }

        private Statement ConstructBody(Statement statement)
        {
            List<Statement> children = new List<Statement>();
            if (_parentChildren.ContainsKey(statement.IndexId))
            {
                foreach (string child in _parentChildren[statement.IndexId])
                {
                    children.Add(ConstructBody(_cachedIndex[child]));
                }


                if (statement.StatementType == StatementType.CombinedStatement)
                {
                    string[] arr = children.Select(c => c.Body).ToArray();
                    statement.Body = string.Format(statement.Original, arr);

                }
                else if (statement.StatementType == StatementType.Function)
                {
                    string replace = string.Empty;
                    if (children.Count > 0)
                    {
                        replace = string.Join(",", children.Select(c => c.Body).ToArray());
                    }
                    statement.Body = statement.Original.Replace("[{ARGUMENTS}]", replace);
                }
                else if (statement.StatementType == StatementType.Variable)
                {
                    VariableInfo vi = VariableInfos.FirstOrDefault(v => v.Name == statement.Original);
                    string fb = vi != null ? vi.FallBack : null;

                    if (!string.IsNullOrEmpty(fb))
                    {
                        if (statement.DefaultResultType != null && statement.DefaultResultType.BroadResultType == BroadResultType.Bool)
                        {
                            fb = fb.Replace("()","");
                        }
                        switch (statement.ResultType.BroadResultType)
                        {
                            case BroadResultType.Bool:
                                statement.Body = "Bool({0},\"" + fb + "\")";
                                break;
                            case BroadResultType.Numeric:
                                if (vi.Type != null && vi.Type.BroadResultType == BroadResultType.Date)
                                {
                                    statement.Body = "DateNumber({0})";
                                }
                                else
                                {
                                    statement.Body = "Number({0},\"" + fb + "\")";
                                }
                               
                                break;
                            case BroadResultType.Date:
                                statement.Body = "Date({0},\"" + fb + "\")";
                                break;
                            case BroadResultType.String:
                                statement.Body = "String({0},\"" + fb + "\")";
                                break;
                            default:
                                statement.Body = "{0}";
                                break;
                        }
                    }
                    else
                    {
                        switch (statement.ResultType.BroadResultType)
                        {
                            case BroadResultType.Bool:
                                statement.Body = "Bool({0})";
                                break;
                            case BroadResultType.Numeric:
                                if (vi.Type != null && vi.Type.BroadResultType == BroadResultType.Date)
                                {
                                    statement.Body = "DateNumber({0})";
                                }
                                else
                                {
                                    statement.Body = "Number({0})";
                                }
                                break;
                            case BroadResultType.Date:
                                statement.Body = "Date({0})";
                                break;
                            case BroadResultType.String:
                                statement.Body = "String({0})";
                                break;
                            default:
                                statement.Body = "{0}";
                                break;
                        }
                    }
                    statement.Body = string.Format(statement.Body, statement.Original);
                }
                else
                {
                    string replace = string.Empty;
                    if (children.Count > 0)
                    {
                        replace = string.Join(" ", children.Select(c => c.Body).ToArray());
                    }
                    statement.Body = statement.Original.Replace("{BODY}", replace);

                }
            }
            else
            {
                statement.Body = statement.Original;
            }
            return statement;
        }

        public string CompileFormulaBody(string key)
        {
            //string id = string.IsNullOrEmpty(index) ? "1" : string.Format("{0}/", index), exactNr);
           
            

            Statement fr = _cachedIndex[key];
            if (string.IsNullOrEmpty(fr.Body)) return string.Empty;
            if (fr.Body.Contains("[{BODY}]"))
            {
                return fr.Body.Replace("[{BODY}]", CompileFormulaBodyChildren(key, " "));

            } else if(fr.Body.Contains("[{ARGUMENTS}]")) {
                return fr.Body.Replace("[{ARGUMENTS}]", CompileFormulaBodyChildren(key, ", "));
            }
            return fr.Body; 
        }

        public string CompileFormulaBodyChildren(string key, string joiner)
        {
            if (string.IsNullOrEmpty(joiner)) joiner = " ";
            List<string> keys = _cachedIndex.Keys.Where(k => k.StartsWith(key+"/") && k.ToCharArray().Count(c => c == '/') == key.ToCharArray().Count(c => c == '/') + 1).OrderBy(c=>c).ToList();
            List<string> result = new List<string>();
            foreach (string subKey in keys)
            {
                result.Add(CompileFormulaBody(subKey));
            }
            return string.Join(joiner, result.ToArray());
        }

        public Statement ProcessItem(XElement item, List<int> parentId)
        {
            int exactNr = item.ElementsBeforeSelf().Count() + 1; // ONE BASED !
            List<int> myId = new List<int>();
            myId.AddRange(parentId);
            myId.Add(exactNr);
            string id = string.Join("/", myId.Select(e => e.ToString()).ToArray());
            string parid = string.Join("/", parentId.Select(e => e.ToString()).ToArray());

            string lname = item.Name.LocalName;

            bool traverseChildren = true;

            Statement stmt = new Statement
                {
                    Body = null
                    ,
                    Source = item
                    ,
                    IndexId = id
                    ,
                    ResultType = null
                    ,
                    Name = item.Name.LocalName
                    , StatementType = StatementType.Statement
                    ,
                    ParentIndexId = parid
                };

                _cachedIndex.Add(id, stmt);
                _parentChildren.Add(id, new List<string>());

                if (parentId.Count > 0)
                {
                    _parentChildren[parid].Add(id);
                }




                switch (item.Name.LocalName)
                {
                    // GROUPS
                    case "Formula":
                        stmt.StatementType = StatementType.Group;
                        stmt.Original = "{BODY}";
                        break;
                    case "group":
                        stmt.StatementType = StatementType.Group;
                        
                        if (item.Elements().Count() > 0)
                        {
                            stmt.Original = "({BODY})";
                        }

                        break;
                    // FUNCTIONS
                    case "function":
                        stmt = ProcessFunction(item, myId, stmt);
                        break;
                    
                    // OPERATORS
                    case "plus":

                        stmt.StatementType = StatementType.Operator;
                        stmt.Original = " + ";
//                        stmt.ResultType = new ResultType { BroadResultType = BroadResultType.ECHO };
                        break;
                    case "minus":
                        stmt.StatementType = StatementType.Operator;
                        stmt.Original = " - ";
                        break;
                    case "multiply":
                        stmt.StatementType = StatementType.Operator;
                        stmt.Original = " * ";
                        stmt.ResultTypeBefore = new ResultType { BroadResultType = BroadResultType.Numeric};
                        stmt.ResultTypeAfter = new ResultType { BroadResultType = BroadResultType.Numeric};
                        stmt.ResultType = new ResultType { BroadResultType=BroadResultType.Numeric};
                        
                        break;
                    case "divide":
                        stmt.StatementType = StatementType.Operator;
                        stmt.Original = " / ";
                        stmt.ResultTypeBefore = new ResultType { BroadResultType = BroadResultType.Numeric};
                        stmt.ResultTypeAfter = new ResultType { BroadResultType = BroadResultType.Numeric};
                        stmt.ResultType = new ResultType { BroadResultType=BroadResultType.Numeric};
                        break;
                    case "mod":
                    case "modular":
                        stmt.StatementType = StatementType.Operator;
                        stmt.Original = " % ";
                        stmt.ResultTypeBefore = new ResultType { BroadResultType = BroadResultType.Numeric};
                        stmt.ResultTypeAfter = new ResultType { BroadResultType = BroadResultType.Numeric};
                        stmt.ResultType = new ResultType { BroadResultType=BroadResultType.Numeric};
                        break;
                     case "equals":
                        stmt.StatementType = StatementType.Operator;
                        stmt.Original = " == ";
                        stmt.ResultType = new ResultType { BroadResultType=BroadResultType.Bool, DetailedResultType= DetailResultType.Bool};
                        break;
                    case "notequals":
                        stmt.StatementType = StatementType.Operator;
                        stmt.Original = " != ";
                        stmt.ResultType = new ResultType { BroadResultType=BroadResultType.Bool, DetailedResultType= DetailResultType.Bool};
                        break;
                    case "greaterThen":
                        stmt.StatementType = StatementType.Operator;
                        stmt.Original = " > ";
                        stmt.ResultTypeBefore = new ResultType { BroadResultType = BroadResultType.Numeric };
                        stmt.ResultTypeAfter = new ResultType { BroadResultType = BroadResultType.Numeric };
                        stmt.ResultType = new ResultType { BroadResultType=BroadResultType.Bool, DetailedResultType= DetailResultType.Bool};
                        break;
                    case "lowerThen":
                        stmt.StatementType = StatementType.Operator;
                        stmt.Original = " < ";
                        stmt.ResultTypeBefore = new ResultType { BroadResultType = BroadResultType.Numeric };
                        stmt.ResultTypeAfter = new ResultType { BroadResultType = BroadResultType.Numeric };
                        stmt.ResultType = new ResultType { BroadResultType=BroadResultType.Bool, DetailedResultType= DetailResultType.Bool};
                        break;
                    case "greaterOrEqualThen":
                        stmt.StatementType = StatementType.Operator;
                        stmt.Original = " >= ";
                        stmt.ResultTypeBefore = new ResultType { BroadResultType = BroadResultType.Numeric };
                        stmt.ResultTypeAfter = new ResultType { BroadResultType = BroadResultType.Numeric };
                        stmt.ResultType = new ResultType { BroadResultType=BroadResultType.Bool, DetailedResultType= DetailResultType.Bool};
                        break;
                    case "lowerOrEqualThen":
                        stmt.StatementType = StatementType.Operator;
                        stmt.Original = " <= ";
                        stmt.ResultTypeBefore = new ResultType { BroadResultType = BroadResultType.Numeric };
                        stmt.ResultTypeAfter = new ResultType { BroadResultType = BroadResultType.Numeric };
                        stmt.ResultType = new ResultType { BroadResultType=BroadResultType.Bool, DetailedResultType= DetailResultType.Bool};
                        break;
                    case "or":
                        stmt.StatementType = StatementType.Operator;
                        stmt.Original = " || ";
                        stmt.ResultTypeBefore = new ResultType { BroadResultType = BroadResultType.Bool};
                        stmt.ResultTypeAfter = new ResultType { BroadResultType = BroadResultType.Bool};
                        stmt.ResultType = new ResultType { BroadResultType=BroadResultType.Bool, DetailedResultType= DetailResultType.Bool};
                        break;
                    case "and":
                        stmt.StatementType = StatementType.Operator;
                        stmt.Original = " && ";
                        stmt.ResultTypeBefore = new ResultType { BroadResultType = BroadResultType.Bool};
                        stmt.ResultTypeAfter = new ResultType { BroadResultType = BroadResultType.Bool};
                        stmt.ResultType = new ResultType { BroadResultType=BroadResultType.Bool, DetailedResultType= DetailResultType.Bool};
                        break;
                   
                    // VARABLE
                    case "variable":
                    case  "Key":
                        stmt.StatementType = StatementType.Variable;
                        string varName =item.Attribute("name").Value.Replace("-", "_");
                        stmt.Original = varName;
                        VariableInfo vi = VariableInfos.FirstOrDefault(v => v.Name == varName);
                        if (vi != null)
                        {
                            stmt.DefaultResultType = vi.Type;
                        }

                        break;

                    // FIXED VARS
                     case "number":
                        stmt.Original ="(decimal)" + item.Value;
                        stmt.ResultType = new ResultType { BroadResultType = BroadResultType.Numeric, DetailedResultType = DetailResultType.Number };
                        break;
                    case "string":
                        stmt.Original = string.Format("\"{0}\"", item.Value);
                        stmt.ResultType = new ResultType { BroadResultType = BroadResultType.String, DetailedResultType = DetailResultType.String};

                        break;

                    case "nodepath":
                        string npath = item.Attribute("path").Value;
                        StringBuilder npSb = new StringBuilder();
                        string[] npathSplitted = npath.Split(new char[] { '/' });
                        bool indexedElsByName = false;
                        for (int i = 0; i < npathSplitted.Length; i++)
                        {
                            switch (npathSplitted[i])
                            {
                                case "xbrli:xbrl":
                                    if (npathSplitted.Length >= i + 1)
                                    {
                                        if (npathSplitted[i + 1] != "xbrli:context"
                                            && npathSplitted[i + 1] != "xbrli:unit")
                                        {
                                            npSb.Append(".Elements");
                                            indexedElsByName = true;
                                        }
                                    }
                                    break;
                                case "xbrli:context":
                                    npSb.Append(".Contexts");
                                    break;
                                case "xbrli:entity":
                                    npSb.Append(".Select(c=>c.Entity)");
                                    break;
                                case "xbrli:identifier":
                                    npSb.Append(".Select(e=>e.IdentifierValue)");
                                    break;
                                case "xbrli:period":
                                    npSb.Append(".Select(c=>c.Period)");
                                    break;
                                case "xbrli:instant":
                                    npSb.Append(".Select(p=>p.Instant)");
                                    break;
                                case "xbrli:startDate":
                                    npSb.Append(".Select(p=>p.StartDate)");
                                    break;
                                case "xbrli:endDate":
                                    npSb.Append(".Select(p=>p.EndDate)");
                                    break;
                                case "xbrli:unit":
                                    npSb.Append(".Units");
                                    break;
                                case "xbrli:measure":
                                    npSb.Append(".Select(u=>u.Measure)");
                                    break;
                                default:
                                    if (!string.IsNullOrEmpty(npathSplitted[i]))
                                    {
                                        if (indexedElsByName)
                                        {
                                            string[] eln = npathSplitted[i].Split(new char[] {':'});

                                            npSb.Append(".Where(e=>e.Name==\"").Append(eln[eln.Length-1]).Append("\")");
                                        }
                                        else
                                        {
                                            npSb.Append(".Where(e=>e.Name==\"").Append(npathSplitted[i]).Append("\")");
                                        }
                                    }
                                    break;
                            }
                        }
                        if (npSb.Length > 0)
                        {
                            stmt.Original = string.Format("Xbrl{0}.ToList()", npSb.ToString());

                            stmt.ResultType = new ResultType { List = true, BroadResultType = BroadResultType.Object, DetailedResultType = DetailResultType.XbrlFact };
                        }
                        else
                        {
                            //return string.Format("null /* {0} */", npath);
                            throw new Exception("Unknown node path: " + npath);
                        }
                        break;
                   case "argument":
                        stmt.StatementType = StatementType.Argument;
                        stmt.Original = "{BODY}";
                        break;

                    case "if":
                        stmt.StatementType = StatementType.CombinedStatement;
                        stmt.Original = "{0} ? {1} : {2}";
                       
                        /*
                        ProcessItem(item.Element("when"), myId);
                        ProcessItem(item.Element("then"), myId);
                        ProcessItem(item.Element("else"), myId);
                         * */
                        break;
                    case "when":
                        stmt.ResultType = new ResultType { BroadResultType = BroadResultType.Bool, DetailedResultType = DetailResultType.Bool };
                        stmt.Original = "{BODY}";
                        break;
                    case "then":
                        stmt.Original = "{BODY}";
                        break;
                    case "else":
                        stmt.Original = "{BODY}";
                        break;
                    case "foreach":
                        stmt.StatementType = StatementType.CombinedStatement;
                        //stmt.ResultType = new ResultType { BroadResultType=BroadResultType.ECHO, List = true };
                        stmt.Original = "{1}.Select({0} => {2}).ToList()";

                     /*   stmt.Original = "!!1!!.ForEach(!!0!! => return !!2!!)"; // POST PROCESSING ?
                        XElement fe_each = item.Element("each");
                        XElement fe_in = item.Element("in");
                        XElement fe_return = item.Element("return");
                        */
                        // POST PROCESS
                        break;
                    case "each":
                        stmt.Original = item.Attribute("name").Value;
                        stmt.ResultType = new ResultType { BroadResultType = BroadResultType.Object };
                        break;
                    case "in":
                        stmt.Original = "{BODY}";
                       // stmt.ResultType = new ResultType { BroadResultType = BroadResultType.Object, List=true };
                        break;
                    case "return":
                        stmt.Original = "{BODY}";
                        break;

                    case "some":
                        stmt.StatementType = StatementType.CombinedStatement;
                        stmt.Original = "{1}.Count({0}=> {2}) > 0";
                        stmt.ResultType = new ResultType { BroadResultType = BroadResultType.Bool, DetailedResultType = DetailResultType.Bool };
                        break;
                    case "every":
                        stmt.StatementType = StatementType.CombinedStatement;
                        stmt.Original = "{1}.Count() == {1}.Count({0}=> {2})";
                        stmt.ResultType = new ResultType { BroadResultType = BroadResultType.Bool, DetailedResultType = DetailResultType.Bool};
                        break;
                  
                    case "satisfies":
                        stmt.Original = "{BODY}";
                        stmt.ResultType = new ResultType { BroadResultType = BroadResultType.Bool, DetailedResultType = DetailResultType.Bool };
                      //  fr.ResultType = new FunctionResultType { BroadResultType = BroadResultType.Bool, DetailedResultType = ResultType.Bool };
                        break;
                   
                   
                    case "where":
                        stmt.Original = "{BODY}";
                        stmt.ResultType = new ResultType { BroadResultType = BroadResultType.Bool, DetailedResultType = DetailResultType.Bool };
                        //  fr.ResultType = new FunctionResultType { BroadResultType = BroadResultType.Bool, DetailedResultType = ResultType.Bool };
                        break;
                    case "queriedlist":
                        stmt.Original = null;
                        
                        break;
                    
                    default:
                        throw new Exception("Unknown formula element:: " + item.Name.LocalName);
                        break;
                }

                _cachedIndex[id] = stmt;

                if (traverseChildren)
                {
                    foreach (XElement childEl in item.Elements())
                    {
                        ProcessItem(childEl, myId);
                    }
                }
                return stmt;
           
        }

        private Statement ProcessFunction(XElement item, List<int> myId, Statement stmt)
        {
            
            Statement fnc = stmt;
            fnc.StatementType = StatementType.Function;
            fnc.ExpectedArguments = new Dictionary<int,ResultType>();

            switch (item.Attribute("name").Value)
            {
                case "yearMonthDuration":
                    fnc.Original = "YearMonthDuration([{ARGUMENTS}])";
                    fnc.ResultType = new ResultType
                    {
                        BroadResultType = BroadResultType.Date,
                        DetailedResultType = DetailResultType.TimeSpan
                    };
                    
                    fnc.ExpectedArguments.Add(1,new ResultType { BroadResultType = BroadResultType.String });

                    break;
                case "dayTimeDuration":
                    fnc.Original = "DayTimeDuration([{ARGUMENTS}])";
                    fnc.ResultType = new ResultType
                    {
                        BroadResultType = BroadResultType.Date,
                        DetailedResultType = DetailResultType.TimeSpan
                    };
                    fnc.ExpectedArguments.Add(1,new ResultType { BroadResultType = BroadResultType.String });
                    break;
                case "date":
                    fnc.Original = "Date([{ARGUMENTS}])";
                    fnc.ResultType = new ResultType
                    {
                        BroadResultType = BroadResultType.Date,
                        DetailedResultType = DetailResultType.Date
                    };
                    break;
                case "days-from-duration":
                    fnc.Original = "DaysFromDuration([{ARGUMENTS}])";
                    fnc.ResultType = new ResultType
                    {
                        BroadResultType = BroadResultType.Numeric,
                        DetailedResultType = DetailResultType.Integer
                    };
                    fnc.ExpectedArguments.Add(1,new ResultType { BroadResultType = BroadResultType.Date });
                    break;
                case "count":
                    fnc.Original = "Count([{ARGUMENTS}])";
                     fnc.ResultType = new ResultType
                    {
                        BroadResultType = BroadResultType.Numeric,
                        DetailedResultType = DetailResultType.Integer
                    };
                    break;
                case "not":
                     fnc.Original = "!([{ARGUMENTS}])";
                     fnc.ResultType = new ResultType
                    {
                        BroadResultType = BroadResultType.Bool,
                        DetailedResultType = DetailResultType.Bool
                    };
                    fnc.ExpectedArguments.Add(1, new ResultType { BroadResultType = BroadResultType.Bool });

                    break;
                case "fact-explicit-scenario-dimension-value":
                    fnc.Original = "FactExplicitScenarioDimensionValue([{ARGUMENTS}])";
                    fnc.ResultType = new ResultType
                    {
                        BroadResultType = BroadResultType.String,
                        DetailedResultType = DetailResultType.String
                    };
                    break;
                case "data":
                    // ?????
                     fnc.Original = "Data([{ARGUMENTS}])";
                    fnc.ResultType = new ResultType
                    {
                        BroadResultType = BroadResultType.String,
                        DetailedResultType = DetailResultType.String
                    };
                    
                    break;
                case "local-name-from-QName":
                    fnc.Original = "LocalNameFromQname([{ARGUMENTS}])";
                    fnc.ResultType = new ResultType
                    {
                        BroadResultType = BroadResultType.String,
                        DetailedResultType = DetailResultType.String
                    };
                    break;
                case "substring":
                    fnc.Original = "Substring([{ARGUMENTS}])";
                    fnc.ResultType = new ResultType
                    {
                        BroadResultType = BroadResultType.String,
                        DetailedResultType = DetailResultType.String
                    };
                    break;
                case "string-length":
                    fnc.Original = "StringLength([{ARGUMENTS}])";
                    fnc.ResultType = new ResultType
                    {
                        BroadResultType = BroadResultType.Numeric,
                        DetailedResultType = DetailResultType.Integer
                    };
                    fnc.ExpectedArguments.Add(1, new ResultType { BroadResultType = BroadResultType.String, DetailedResultType = DetailResultType.String });

                    break;
                case "substring-before":
                    fnc.Original = "SubstringBefore([{ARGUMENTS}])";
                    fnc.ResultType = new ResultType
                    {
                        BroadResultType = BroadResultType.String,
                        DetailedResultType = DetailResultType.String
                    };
                    
                    break;
                case "substring-after":
                    fnc.Original = "SubstringAfter([{ARGUMENTS}])";
                    fnc.ResultType = new ResultType
                    {
                        BroadResultType = BroadResultType.String,
                        DetailedResultType = DetailResultType.String
                    };
                    break;
                case "QName":
                    fnc.Original = "QName([{ARGUMENTS}])";
                    fnc.ResultType = new ResultType
                    {
                        BroadResultType = BroadResultType.String,
                        DetailedResultType = DetailResultType.String
                    };
                    break;
                case "max":
                   
                    fnc.Original = "Max([{ARGUMENTS}])";
                    fnc.ResultType = new ResultType
                    {
                        BroadResultType = BroadResultType.Numeric,
                        DetailedResultType = DetailResultType.Number
                    };
                    fnc.ExpectedArguments.Add(-1, new ResultType { BroadResultType = BroadResultType.Numeric});
                    break;
                case "min":
                    fnc.Original = "Min([{ARGUMENTS}])";
                    fnc.ResultType = new ResultType
                    {
                        BroadResultType = BroadResultType.Numeric,
                        DetailedResultType = DetailResultType.Number
                    };
                    fnc.ExpectedArguments.Add(-1, new ResultType { BroadResultType = BroadResultType.Numeric});
                    break;
                case "number":
                    fnc.Original = "Number([{ARGUMENTS}])";
                    fnc.ResultType = new ResultType
                    {
                        BroadResultType = BroadResultType.Numeric,
                        DetailedResultType = DetailResultType.Number
                    };
                    break;
                case "round":
                    fnc.Original = "Math.Round([{ARGUMENTS}],MidpointRounding.AwayFromZero)";
                    fnc.ResultType = new ResultType
                    {
                        BroadResultType = BroadResultType.Numeric,
                        DetailedResultType = DetailResultType.Number
                    };

                    fnc.ExpectedArguments.Add(1, new ResultType { BroadResultType = BroadResultType.Numeric});
                    fnc.ExpectedArguments.Add(2, new ResultType { BroadResultType = BroadResultType.Numeric});
                   
                    break;
                case "sum":
                    fnc.Original = "Sum([{ARGUMENTS}])";
                    fnc.ResultType = new ResultType
                    {
                        BroadResultType = BroadResultType.Numeric,
                        DetailedResultType = DetailResultType.Number
                    };
                    fnc.ExpectedArguments.Add(-1, new ResultType { BroadResultType = BroadResultType.Numeric});
                    break;
                case "string":
                     fnc.Original = "String([{ARGUMENTS}])";
                    fnc.ResultType = new ResultType
                    {
                        BroadResultType = BroadResultType.String,
                        DetailedResultType = DetailResultType.String
                    };
                    break;
                case "distinctvalues":
                    fnc.Original = "distinctvalues([{ARGUMENTS}])";
                    fnc.ResultType = new ResultType
                    {
                        BroadResultType = BroadResultType.String,
                        DetailedResultType = DetailResultType.String,
                        List=true
                    };
                    break;
                case "exists":
                    fnc.Original = "Exists([{ARGUMENTS}])";
                    fnc.ExpectedArguments.Add(1, new ResultType { BroadResultType = BroadResultType.Object, DetailedResultType = DetailResultType.XbrlFact });
                    fnc.ResultType = new ResultType
                    {
                        BroadResultType = BroadResultType.Bool,
                        DetailedResultType = DetailResultType.Bool
                    };
                    break;
                case "true":
                    fnc.Original = "true";
                    fnc.ResultType = new ResultType
                    {
                        BroadResultType = BroadResultType.Bool,
                        DetailedResultType = DetailResultType.Bool
                    };
                    break;
                case "false":
                    fnc.Original = "false";
                    fnc.ResultType = new ResultType
                    {
                        BroadResultType = BroadResultType.Bool,
                        DetailedResultType = DetailResultType.Bool
                    };
                    break;
                case "integer":
                    fnc.Original = "Integer([{ARGUMENTS}])";
                    fnc.ResultType = new ResultType
                    {
                        BroadResultType = BroadResultType.Numeric,
                        DetailedResultType = DetailResultType.Integer
                    };
                    break;
                case "ends-with":
                    fnc.Original = "EndsWith([{ARGUMENTS}])";
                    fnc.ResultType = new ResultType
                    {
                        BroadResultType = BroadResultType.Bool,
                        DetailedResultType = DetailResultType.Bool
                    };
                    fnc.ExpectedArguments.Add(1, new ResultType { BroadResultType = BroadResultType.String });
                    fnc.ExpectedArguments.Add(2, new ResultType { BroadResultType = BroadResultType.String });
                   
                    break;
                case "GetNodeByName":
                    fnc.Original = "GetNodeByName([{ARGUMENTS}])";
                    fnc.ResultType = new ResultType
                    {
                        BroadResultType = BroadResultType.Object,
                        DetailedResultType = DetailResultType.XbrlFact
                    };
                    break;
                case "list":
                    fnc.Original = "ToList([{ARGUMENTS}])";
                    fnc.ResultType = new ResultType
                    {
                        BroadResultType = BroadResultType.ANY,
                        DetailedResultType = DetailResultType.ANY,
                        List=true
                    };
                    break;
                default:
                    throw new Exception("Formula renderer: unknown function: " + item.Attribute("name").Value);
                    break;
            }


            return fnc;
        }

        private void PostProcess()
        {

            /*
            List<FunctionResult> fors = _cachedIndex.Values.Where(f => f.Name == "foreach").ToList();
            List<FunctionResult> queriedlist = _cachedIndex.Values.Where(f => f.Name == "queriedlist").ToList();

            foreach (FunctionResult qr in queriedlist)
            {
                List<string> forKeys = _parentChildren[qr.IndexId];
                
                List<FunctionResult> childs = _cachedIndex.Values.Where(c => forKeys.Contains(c.IndexId)).ToList();
                

                
            }

            foreach (FunctionResult forstmt in fors)
            {
                List<string> forKeys = _parentChildren[forstmt.IndexId];
                List<FunctionResult> childs = _cachedIndex.Values.Where(c => forKeys.Contains(c.IndexId)).ToList();
            }
             * */
        }
        /*
        private FunctionResultType CombineResultTypes(FunctionResultType source, FunctionResultType target)
        {
            
            if (source.BroadResultType == BroadResultType.ANY) return target;
            FunctionResultType fr = new FunctionResultType { BroadResultType = source.BroadResultType, DetailedResultType = source.DetailedResultType };
            if (source.BroadResultType == target.BroadResultType)
            {
                fr.DetailedResultType = target.DetailedResultType;
            }
            else
            {
                if (target.BroadResultType == BroadResultType.Bool)
                {
                    return target;
                }
                else if (source.BroadResultType == BroadResultType.Bool)
                {
                    return source;
                }
                else
                {

                    throw new Exception("Broad result types do not match?");
                }
            }

            return fr;
            
        }
        */
       
        /*
        private FunctionResultType GetCombinedResultType(List<string> keys) 
        {
            FunctionResultType frt = new FunctionResultType { 
                BroadResultType=BroadResultType.NULL, 
                DetailedResultType= ResultType.NULL 
            };


            foreach (FunctionResult fr in _cachedIndex.Where(c => keys.Contains(c.Key)).Select(c => c.Value))
            {
                Console.WriteLine(fr.ResultType.BroadResultType.ToString());
            }
            return frt;
        }
        */

        public void ProcessTypes(List<string> keys)
        {
            /*
            foreach (string key in keys)
            {
                FunctionResult fr = _cachedIndex[key];
                fr.Original = fr.Body;
                if (fr.ResultType == null)
                {

                    List<string> children = _parentChildren[key];
                    if (children.Count > 0)
                    {
                        fr.ResultType = _cachedIndex[children.First()].ResultType;
                        if (!fr.Function && children.Count > 1)
                        {
                            List<FunctionResult> frs = _cachedIndex.Where(c => children.Contains(c.Key)).Select(c => c.Value).ToList();
                            if (frs.Where(f=>f.ResultType!=null).Select(f =>f.ResultType.BroadResultType).Distinct().Count() > 1)
                            {
                                Console.WriteLine("combine multiple resulttypes");
                                if (frs.Count(f => f.ResultType.BroadResultType == BroadResultType.Bool) > 0)
                                {
                                    fr.ResultType = frs.First(f => f.ResultType.BroadResultType == BroadResultType.Bool).ResultType;
                                }
                                // loop through
                            }
                        }
                    }
                    else
                    {
                        if (!fr.Function)
                        {
                            // loop through fellow objects
                            string[] idsplit = fr.IndexId.Split(new char[] { '/' });
                            int myidx = int.Parse(idsplit[idsplit.Length - 1]);
                            string parentId = string.Join("/", idsplit, 0, idsplit.Length - 1);
                            children = _parentChildren[parentId];
                            FunctionResult[] childfrs = _cachedIndex.Values.Where(c => c.IndexId.StartsWith(parentId) && c.IndexId.LastIndexOf('/') == fr.IndexId.LastIndexOf('/')).ToArray();


                            FunctionResultType frt = null;

                            FunctionResult prev = null;
                            FunctionResult next = childfrs.FirstOrDefault(c => c.IndexId.EndsWith((myidx + 1).ToString()));
                            if (next != null && next.ResultType != null && next.ResultType.BroadResultType == BroadResultType.Bool)
                            {
                                next = childfrs.FirstOrDefault(c => c.IndexId.EndsWith((myidx + 2).ToString()));
                            }
                            if (myidx > 1)
                            {
                                prev = childfrs.First(c => c.IndexId.EndsWith((myidx - 1).ToString()));
                                if (prev != null && prev.ResultType != null && prev.ResultType.BroadResultType == BroadResultType.Bool)
                                {
                                    if (myidx > 2)
                                    {
                                        prev = childfrs.First(c => c.IndexId.EndsWith((myidx - 2).ToString()));
                                    }
                                    else
                                    {
                                        // previous item is bool => can only be a "not" negation, so this has to be boolean
                                        frt = new FunctionResultType { BroadResultType = BroadResultType.Bool, DetailedResultType = ResultType.Bool };
                                    }
                                }
                                else
                                {
                                    if (prev.Body == " - " || prev.Body == " + ")
                                    {
                                        frt = new FunctionResultType { BroadResultType = BroadResultType.Numeric, DetailedResultType = ResultType.Number };
                                    }
                                }
                            }
                            if (frt == null)
                            {
                                if (prev != null)
                                {
                                    frt = prev.ResultType;
                                }
                                else if (next != null)
                                {
                                    frt = next.ResultType;
                                }
                                else
                                {
                                    frt = _cachedIndex[parentId].ResultType;
                                    if (frt == null)
                                    {
                                        frt = _cachedIndex[string.Join("/", idsplit, 0, idsplit.Length - 2)].ResultType;
                                    }
                                }
                            }
                            if (frt == null &&
                                    (
                                        (prev != null && prev.Source.Name.LocalName == "variable")
                                        || (next != null && next.Source.Name.LocalName == "variable")
                                    )
                                )
                            {
                                frt = new FunctionResultType { BroadResultType = BroadResultType.Numeric, DetailedResultType = ResultType.Number };
                            }
                            fr.ResultType = frt;
                            
                            
                        }
                    }

                    if (fr.ResultType.BroadResultType == BroadResultType.Numeric)
                    {
                        fr.Body = string.Format("Number({0})", fr.Original);
                    }
                    else if (fr.ResultType.BroadResultType == BroadResultType.String)
                    {
                        fr.Body = string.Format("String({0})", fr.Original);
                    }
             


                    /// UNKNOWN RESULT BY FIRST GLANCE
                    // GetCombinedResultType(_parentChildren[key]);
                    _cachedIndex[key] = fr;
                }
                else
                {

                }
            }
            */
        }

        public void ProcessTypes()
        {
            int cnt = 1;
            Console.Write("...{0}", cnt);
            _ProcessTypes();

            List<Statement> stmts = _cachedIndex.Values.Where(f => f.ResultType == null && f.StatementType != StatementType.Operator).OrderByDescending(f => f.IndexId.ToCharArray().Count(c => c == '/')).ToList();
            while (stmts.Count > 0)
            {
                cnt++;
                Console.Write("...{0}", cnt);
                _ProcessTypes();
                stmts = _cachedIndex.Values.Where(f => f.ResultType == null && f.StatementType != StatementType.Operator).OrderByDescending(f => f.IndexId.ToCharArray().Count(c => c == '/')).ToList();
            }
           
        }

        public void _ProcessTypes()
        {
            List<Statement> stmts = _cachedIndex.Values.Where(f => f.ResultType == null ).OrderByDescending(f => f.IndexId.ToCharArray().Count(c => c == '/')).ToList();
            //|| (f.ResultType!=null && f.ResultType.BroadResultType == BroadResultType.ECHO)

            foreach (Statement stmt in stmts)
            {
                List<string> children = _parentChildren[stmt.IndexId];
                // parentStmt only for formula/group
                Statement parentStmt = !string.IsNullOrEmpty(stmt.ParentIndexId) ? _cachedIndex[stmt.ParentIndexId] : null;
                List<string> parentChildren = parentStmt != null ? _parentChildren[parentStmt.IndexId] : new List<string>();
                
                Statement before = null;
                Statement after = null;

                switch (stmt.StatementType)
                {
                    case StatementType.Operator:
                        // operator isn't used
                        // only within group
                        break;
                    case StatementType.Variable:
                        int idx = parentChildren.IndexOf(stmt.IndexId);

                        if (parentStmt != null && parentStmt.Name == "in")
                        {
                           
                                stmt.ResultType = new ResultType { BroadResultType = BroadResultType.Object, DetailedResultType = DetailResultType.XbrlFact };
                            
                        }

                        if (idx > 0 && parentChildren.Count > 2 && idx <= parentChildren.Count-1)
                        {
                            before = _cachedIndex[parentChildren[idx-1]];
                        }
                        if (parentChildren.Count > 2 && idx < parentChildren.Count - 1)
                        {
                            after = _cachedIndex[parentChildren[idx + 1]];
                        }

                        if (before != null && before.StatementType == StatementType.Operator 
                            && before.ResultTypeBefore !=null && before.ResultTypeBefore.BroadResultType!= BroadResultType.ANY)
                        {

                            if (before.ResultType != null && before.ResultType.BroadResultType == BroadResultType.Bool)
                            {
                                Statement bebbefore = _cachedIndex[parentChildren[idx - 2]];
                                if (bebbefore != null)
                                {
                                    if (bebbefore.StatementType != StatementType.Operator
                                           && bebbefore.ResultType != null
                                           )
                                    {
                                        if (bebbefore.ResultType.BroadResultType != BroadResultType.ECHO)
                                        {
                                            stmt.ResultType = bebbefore.ResultType;
                                        }
                                    }
                                }
                            }
                         
                            if(stmt.ResultType==null) {
                                stmt.ResultType = before.ResultTypeAfter;
                            }
                        }


                        if (after != null && after.StatementType == StatementType.Operator
                            && after.ResultTypeBefore != null && after.ResultTypeBefore.BroadResultType != BroadResultType.ANY)
                        {
                            if (stmt.ResultType == null)
                            {
                                if (after.ResultType.BroadResultType == BroadResultType.Bool)
                                {
                                    Statement afaafter = _cachedIndex[parentChildren[idx + 2]];
                                    if (afaafter.StatementType != StatementType.Operator
                                        && afaafter.ResultType != null
                                        )
                                    {
                                        if (afaafter.ResultType.BroadResultType != BroadResultType.ECHO)
                                        {
                                            stmt.ResultType = afaafter.ResultType;
                                        }
                                    }
                                }

                                if (stmt.ResultType == null)
                                {
                                    stmt.ResultType = after.ResultTypeBefore;
                                }

                            }
                            else
                            {
                                if (stmt.ResultType.BroadResultType != after.ResultTypeBefore.BroadResultType)
                                {
                                    Console.WriteLine(" ERROR ");
                                }
                            }
                        }
                        
                        if (stmt.ResultType == null && parentStmt != null && parentStmt.StatementType == StatementType.Argument)
                        {
                            // get function parent
                            Statement funcStmt = _cachedIndex[parentStmt.ParentIndexId];
                            if (funcStmt.Source.Attribute("name").Value == "string")
                            {
                                stmt.ResultType = new ResultType { BroadResultType = BroadResultType.String, DetailedResultType = DetailResultType.String };
                            }
                            else
                            {
                                stmt.ResultType = GetFunctionArgumentResulttype(funcStmt, parentStmt.IndexId);
                            }
                            
                            
                        } 
                            
                        if (stmt.ResultType == null && before!= null && before.StatementType== StatementType.Operator && idx >= 2)
                        {
                            
                            Statement bebefore = _cachedIndex[parentChildren[idx-2]];
                            if (bebefore.StatementType != StatementType.Operator
                                && bebefore.ResultType != null
                                )
                            {
                                if (bebefore.ResultType.BroadResultType != BroadResultType.ECHO)
                                {
                                    stmt.ResultType = bebefore.ResultType;
                                }
                            }
                            if (stmt.ResultType == null && before.ResultType!=null 
                                && before.ResultType.BroadResultType== BroadResultType.Bool 
                                && bebefore.StatementType == StatementType.Variable)
                            {
                                // DEFAULT VARIABLE TYPE IN AN EQUATION WITH TWO VARIABLES
                                stmt.ResultType = new ResultType { BroadResultType=  BroadResultType.String };
                            }
                        }

                        if (stmt.ResultType == null && after != null && after.StatementType == StatementType.Operator && idx < parentChildren.Count - 2)
                        {
                            Statement afafter = _cachedIndex[parentChildren[idx + 2]];
                            if (afafter.StatementType != StatementType.Operator
                                && afafter.ResultType != null
                                )
                            {
                                if (afafter.ResultType.BroadResultType != BroadResultType.ECHO)
                                {
                                    stmt.ResultType = afafter.ResultType;
                                }
                            }
                            if (stmt.ResultType == null && after.ResultType != null
                               && after.ResultType.BroadResultType == BroadResultType.Bool
                               && afafter.StatementType == StatementType.Variable)
                            {
                                // DEFAULT VARIABLE TYPE IN AN EQUATION WITH TWO VARIABLES
                                stmt.ResultType = new ResultType { BroadResultType = BroadResultType.String };
                            }
                        }

                        if (stmt.ResultType == null && stmt.DefaultResultType != null)
                        {
                            stmt.ResultType = stmt.DefaultResultType;
                        }

                        if (stmt.ResultType == null && VariableInfos.Count(v => v.Name == stmt.Original) > 0)
                        {
                            stmt.ResultType = VariableInfos.First(v => v.Name == stmt.Original).Type;
                        }

                        if (stmt.ResultType == null)
                        {
                            Console.WriteLine("STILL NO TYPE");
                        }
                       
                        
                        // YOU DUMB SHIT... EACH ELEMENT IN XSD HAS A TYPE AS WELL, WHY NOT UTILISE THAT AS WELL!!!
                        // SO: TODO ! NOW!

                        

                        break;
                        
                    case StatementType.Argument:
                        // look in children
                        if (children.Count == 1)
                        {
                            stmt.ResultType = _cachedIndex[children.First()].ResultType;
                        }
                        else
                        {
                            stmt.ResultType = GetGroupResulttype(children);
                        }
                        if (stmt.ResultType != null)
                        {
                            //CHECK WITH FUNCTION EXPECTATIONS
                            
                        }
                        break;
                    case StatementType.Function:
                        break;
                    case StatementType.CombinedStatement:
                        List<Statement> childStmts = _cachedIndex.Values.Where(v => children.Contains(v.IndexId)).ToList();
                        switch (stmt.Name)
                        {
                            case "if":
                                Statement then = childStmts.First(c => c.Name == "then");
                                stmt.ResultType = then.ResultType;
                                break;
                            case "foreach":
                                // ECHO
                                Statement ret = childStmts.Last();
                                if (ret.ResultType != null)
                                {
                                    stmt.ResultType = new ResultType
                                    {
                                        BroadResultType = ret.ResultType.BroadResultType,
                                        DetailedResultType = ret.ResultType.DetailedResultType,
                                        List = true
                                    };
                                }
                                
                                break;
                                
                        }
                        break;
                    case StatementType.Group:
                        stmt.ResultType = GetGroupResulttype(children);
                        break;
                    case StatementType.Statement:
                        if (parentStmt != null && parentStmt.StatementType == StatementType.CombinedStatement)
                        {
                            stmt.ResultType = GetGroupResulttype(children);
                        }
                        break;


                }
            }

            

            

        }

        private ResultType GetFunctionArgumentResulttype(Statement funcStmt, string argId)
        {
            int idx = _parentChildren[funcStmt.IndexId].IndexOf(argId) + 1;
            if (funcStmt.ExpectedArguments == null) return null;
            if (funcStmt.ExpectedArguments.ContainsKey(idx))
            {
                return funcStmt.ExpectedArguments[idx];
            }
            if (funcStmt.ExpectedArguments.ContainsKey(-1))
            {
                return funcStmt.ExpectedArguments[-1];
            }
            if(funcStmt.ResultType!=null && funcStmt.ResultType.BroadResultType!= BroadResultType.ANY)
                return new ResultType { BroadResultType = BroadResultType.ANY };

            // Function's resulttype is depending on argument types, so argument type is required.
            return null;
        }

        public ResultType GetGroupResulttype(List<string> childKeys)
        {
            if (childKeys.Count == 1)
            {
                return _cachedIndex[childKeys.First()].ResultType;
            }
            List<Statement> children = _cachedIndex.Values.Where(c => childKeys.Contains(c.IndexId)).OrderBy(c => c.IndexId).ToList();
            int cnt1 = children.Count(c => c.ResultType == null && c.StatementType != StatementType.Operator);
           
            if (cnt1 > 0) 
                return null;

            cnt1 = children.Count(c => c.StatementType == StatementType.Operator && c.ResultType != null);
            if (cnt1 > 0)
            {
                var ch = children.Where(c => c.StatementType == StatementType.Operator && c.ResultType != null);
                if (ch.Select(c => c.ResultType.BroadResultType).Distinct().Count() == 1)
                {
                    return ch.Select(c => c.ResultType).First();
                }
                else
                {
                    if (ch.Count(c => c.ResultType.BroadResultType == BroadResultType.Bool) > 0)
                    {
                        return ch.Where(c => c.ResultType.BroadResultType == BroadResultType.Bool).Select(c => c.ResultType).First();
                    }
                    else
                    {
                        Console.WriteLine("CONTRADICTING OPERATORS ON SAME LEVEL ??? -> Compiler error");
                    }
                }
            }

            int cnt2 = children.Where(c => c.ResultType != null && c.StatementType != StatementType.Operator).Select(c => c.ResultType.BroadResultType).Distinct().Count();
            if (cnt2 == 1)
            {
                return children.Where(c => c.ResultType != null && c.StatementType != StatementType.Operator).Select(c => c.ResultType).First();
            }
            

            return null;
        }

    }

    public enum BroadResultType
    {
        Bool,
        Numeric,
        String,
        Date,
        Object,
        NULL,
        ANY,
        ECHO

    }

    public enum DetailResultType
    {
        NULL,
        ANY,
        Bool,
        String,
        Number,
        Integer,
        Date,
        TimeSpan,
        XbrlFact

    }


    public class Statement
    {
        public StatementType StatementType { get; set; }
        public string Name { get; set; }

        public string IndexId { get; set; }

        public string ParentIndexId { get; set; }

        public ResultType ResultType { get; set; }

        public ResultType DefaultResultType { get; set; }

        public XElement Source { get; set; }

        public string Body { get; set; }

        public string Original { get; set; }

        public ResultType ResultTypeBefore { get; set; }
        public ResultType ResultTypeAfter { get; set; }

        public Dictionary<int, ResultType> ExpectedArguments { get; set; }

    }

    public enum StatementType
    {
        Statement,
        Group,
        Argument,
        Variable,
        Operator,
        Function,
        CombinedStatement
    }

 


    public class FunctionResult
    {
        public string Name { get; set; }

        public string IndexId { get; set; }

        public bool ResultIsList { get; set; }

    
        

        public bool Function { get; set; }

        public bool FixedArguments { get; set; }

        
    }



    public class ResultType
    {
        public bool List { get; set; }

        public BroadResultType BroadResultType { get; set; }

        public DetailResultType? DetailedResultType { get; set; }
    }


}
