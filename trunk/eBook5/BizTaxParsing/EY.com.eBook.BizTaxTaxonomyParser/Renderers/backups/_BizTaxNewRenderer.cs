﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Xml.XPath;
using EY.com.eBook.BizTaxTaxonomyParser.Contracts;
using System.IO;
using System.Text.RegularExpressions;

namespace EY.com.eBook.BizTaxTaxonomyParser.Renderers
{
    public class BizTaxNewRenderer_BC
    {
        private StreamWriter _writer;
        private Taxonomy _taxonomy;
        private int _assessmentyear;
        private Dictionary<string, string> _templates;
        private GlobalParameters _parameters;
        private bool? lastFormulaNumeric = false;
        private List<string> numericVariables;
        private Dictionary<string, List<string>> _calculationElements = new Dictionary<string, List<string>>();
        private XElement allFormulas = new XElement("formulas");


        private Dictionary<string, List<string>> ScenarioElements = new Dictionary<string, List<string>>();

        private Dictionary<string, List<string>> ElementScenarios = new Dictionary<string, List<string>>();

        public BizTaxNewRenderer_BC(Taxonomy taxonomy, GlobalParameters parameters)
        {
            _taxonomy = taxonomy;
            _assessmentyear = int.Parse(parameters.AllParameters["CurrentAssessmentYear"].Value);
            _parameters = parameters;
            _templates = new Dictionary<string, string>();
            string searchPath = Path.Combine("", @"Templates\Code");
            string[] files = Directory.GetFiles(searchPath, "*.tpl", SearchOption.AllDirectories);
            foreach (string file in files)
            {
                string key = file.Replace(searchPath + @"\", "");
                StreamReader sr = new StreamReader(file);
                _templates.Add(key, sr.ReadToEnd());
                sr.Close();
            }
            
            foreach (XbrlElement xel in _taxonomy.Elements.Values)
            {
                string xelstr = string.Format("tax-inc:{0}", xel.Name);
                if (!ElementScenarios.ContainsKey(xelstr))
                {
                    ElementScenarios.Add(xelstr, new List<string>());
                }
                if (xel.ScenarioDefs != null)
                {
                    foreach (ContextDefinition cd in xel.ScenarioDefs)
                    {
                        if (cd.Scenario != null && cd.Scenario.Count > 0)
                        {
                            foreach (ContextContent cc in cd.Scenario)
                            {
                                if (!ScenarioElements.ContainsKey(cc.GetXbrlId()))
                                {
                                    ScenarioElements.Add(cc.GetXbrlId(), new List<string>());
                                }
                                if (!ScenarioElements[cc.GetXbrlId()].Contains(xelstr))
                                {
                                    ScenarioElements[cc.GetXbrlId()].Add(xelstr);
                                }
                                if (!ElementScenarios[xelstr].Contains(cc.GetXbrlId()))
                                {
                                    ElementScenarios[xelstr].Add(cc.GetXbrlId());
                                }
                            }
                        }
                        else
                        {

                            if (!ScenarioElements.ContainsKey(""))
                            {
                                ScenarioElements.Add("", new List<string>());
                            }
                            if (!ScenarioElements[""].Contains(xelstr))
                            {
                                ScenarioElements[""].Add(xelstr);
                            }
                            if (!ElementScenarios[xelstr].Contains(""))
                            {
                                ElementScenarios[xelstr].Add("");
                            }

                        }
                    }
                }
                else
                {
                    if (!ScenarioElements.ContainsKey(""))
                    {
                        ScenarioElements.Add("", new List<string>());
                    }
                    if (!ScenarioElements[""].Contains(xelstr))
                    {
                        ScenarioElements[""].Add(xelstr);
                    }
                    if (!ElementScenarios[xelstr].Contains(""))
                    {
                        ElementScenarios[xelstr].Add("");
                    }

                }
            }
            
        }

        public Dictionary<string, List<string>> Render(string targetPath, bool debug)
        {

            Console.WriteLine("RENDERING BACKEND");
            if (_writer != null)
            {
                _writer.Close();
                _writer.Dispose();
            }
            string[] keyparts = _taxonomy.Key.Split(new char[] { '-' });
            string Keyed = keyparts[keyparts.Length - 1].ToUpperInvariant();
            _writer = System.IO.File.CreateText(Path.Combine(targetPath, string.Format("BizTax{0}Renderer.cs", Keyed)));

           

            string template = _templates[@"xbrl\biztaxrenderer.tpl"];
            template = template.Replace("<!ASSESSMENTYEAR!>", _assessmentyear.ToString());
            template = template.Replace("<!BIZTAXTYPE!>", Keyed);
            template = template.Replace("<!DEFAULTCONTEXTS!>", GetDefaultContexts());
            template = template.Replace("<!GLOBALPARAMS!>", GetGlobalParams());

            RenderCalculations(ref template);

            _writer.Write(template);
            _writer.Close();
            allFormulas.Save("formulas.xml");
            Console.WriteLine("DONE RENDERING BACKEND");
            return _calculationElements;
        }

        private string GetDefaultContexts()
        {
            StringBuilder sb = new StringBuilder();
            List<ContextContent> cds = _taxonomy.ContextDefinitions.Values.Where(c => c.Dimensions.Count(d => d.DimensionType == DimensionTypes.ExplicitDimension) == c.Dimensions.Count).ToList();

            List<string> cids = new List<string>();
            foreach (ContextContent cc in cds)
            {
                StringBuilder sbcc = new StringBuilder("");
                string scenId = "";
                foreach (ContextDimension d in cc.Dimensions)
                {
                    ContextDomainMember dm = d.Domain.DomainMember;
                    string[] spn = dm.XbrlName.Split(new char[] { ':' });
                    if (string.IsNullOrEmpty(scenId))
                    {
                        scenId = "id__" + spn[1];
                    }
                    else
                    {
                        scenId += "__id__" + spn[1];
                    }
                    if (sbcc.Length > 0) sbcc.Append(" ,");
                    sbcc.Append(" new ContextScenarioExplicitDataContract {  Dimension=\"").Append(d.XbrlName).Append("\", Value=\"").Append(dm.XbrlName).Append("\"}");
                }
                if (!cids.Contains(scenId))
                {
                    cids.Add(scenId);
                    sb.Append(Environment.NewLine);
                    sb.Append("AddContextsFor(new List<ContextScenarioExplicitDataContract> { ").Append(sbcc.ToString()).Append(" }, identifier, periodStart, periodEnd);");
                }
            }
            return sb.ToString();
        }

        private string GetGlobalParams()
        {
            StringBuilder sb = new StringBuilder();

            foreach (Parameter param in _parameters.AllParameters.Values)
            {
                string nm = param.Name.Replace("-", "_");
                sb.Append(Environment.NewLine).Append("\t");
                sb.Append("internal ");
                if (param.ValueType == typeof(string).Name)
                {
                    sb.Append("string ").Append(nm).Append(" = ");
                    sb.Append("\"").Append(param.Value).Append("\"");
                }
                else if (param.ValueType == typeof(DateTime).Name)
                {
                    sb.Append("DateTime ").Append(nm).Append(" = ");
                    DateTime dt = param.DateValue.Value;
                    sb.Append(string.Format("new DateTime({0},{1},{2})", dt.Year, dt.Month, dt.Day));
                }
                else
                {
                    sb.Append("decimal ").Append(nm).Append(" = (decimal)");
                    sb.Append(param.Value);
                }
                sb.Append(";");
            }
            sb.Append(Environment.NewLine);
            return sb.ToString();
        }


        private void RenderCalculations(ref string template)
        {
            StringBuilder sbCalls = new StringBuilder();
            StringBuilder sbFunctions = new StringBuilder();

            foreach (AssertionSet aset in _taxonomy.Assertions)
            {
                XElement asetFormula = new XElement("assertionSet", new XAttribute("name", aset.Label));
                XElement asetGenVarFormula = new XElement("generalVariables");
                XElement asetValueAssertionFormula = new XElement("valueAssertions");
                XElement asetCalcFormula = new XElement("calculations");

                string fname = "Calc_" + aset.Label.Replace("-", "_");
                sbCalls.Append(fname).Append("();");
                sbCalls.Append(Environment.NewLine).Append("\t").Append("\t").Append("\t");

                sbFunctions.Append(Environment.NewLine).Append("\t").Append("\t").Append("\t");
                sbFunctions.Append("internal void ").Append(fname).Append("()");
                sbFunctions.Append(Environment.NewLine).Append("\t").Append("\t").Append("{");
                sbFunctions.Append(Environment.NewLine).Append("\t").Append("\t").Append("\t");

                asetFormula.Add(asetGenVarFormula);
                asetFormula.Add(asetValueAssertionFormula);
                asetFormula.Add(asetCalcFormula);


                allFormulas.Add(asetFormula);

                aset.Variables.ForEach(v =>
                {
                    v.Name = v.Name.Replace("-", "_");
                });

                // obtain information
                //  if (aset.Assertions.Count(a => a.Calculation == null) > 0) break; // leave out non calcs for now.
           //     aset.Variables.Where(v => v.Type == VariableTypes.Parameter);
             //   aset.Variables.Where(v => v.Type == VariableTypes.General);
               // aset.Variables.Where(v => v.Type == VariableTypes.Fact);


                // create list of all variables, standard order:
                //  -->  1st unused variables in assertion formula
                //  -->  2nd used variables in assertion formula in sequence used
                //  --> check variable formula used variables
                //
                // Create context's filter (scenariovals? Id minus period)

                List<Variable> varList = new List<Variable>();

                Dictionary<string, ElementSelection> elementSelections = new Dictionary<string, ElementSelection>();

/*
                varList.AddRange(aset.Variables.Where(v => v.Type == VariableTypes.Parameter).ToList());
                varList.AddRange(aset.Variables.Where(v => v.Type == VariableTypes.General && v.SelectFormula == null || (v.SelectFormula !=null && ( v.SelectFormula.UsedVariables == null || (v.SelectFormula.UsedVariables != null && v.SelectFormula.UsedVariables.Count == 0)))).ToList());
                varList.AddRange(aset.Variables.Where(v => v.Type == VariableTypes.Fact).ToList());
                varList.AddRange(aset.Variables.Where(v => v.Type == VariableTypes.General && v.SelectFormula != null && v.SelectFormula.UsedVariables != null && v.SelectFormula.UsedVariables.Count > 0).ToList());
                */

                varList.AddRange(aset.Variables.ToList());
                Variable calculationTarget = null;
                if (_taxonomy.IncludedFormula)
                {
                    if (aset.Formula != null)
                    {
                        varList.AddRange(aset.Formula.Variables.Where(v => varList.Count(va => va.Name == v.Name) == 0).ToList());
                        calculationTarget = aset.Formula.Target;
                        varList.Add(calculationTarget);
                    }
                    else
                    {
                        // NOTHING => NO FORMULA ATTACHED.
                    }
                }
                else
                {
                    ValueAssertion vasC = aset.Assertions.First();
                    if (vasC.Documentation != null && !string.IsNullOrEmpty(vasC.Documentation.Target) && vasC.Calculation != null && vasC.Calculation.Formula != null)
                    {
                        string cTarget = vasC.Documentation.Target.Trim();
                            cTarget = cTarget.Replace("$", "");
                            if (varList.Count(v => v.Name == cTarget) > 0)
                            {
                                calculationTarget = varList.First(v => v.Name == cTarget);
                            }
                            else
                            {
                                System.Diagnostics.Debug.Fail(vasC.Id + " Can't find target " + cTarget);
                            }
                    }
                }
                


                // PRETEST
                ValueAssertion preTest = (aset.Preconditions!=null && aset.Preconditions.Count>0) 
                    ? aset.Preconditions.First()
                    : null;
                
                           

                //
                foreach (ValueAssertion vas in aset.Assertions)
                {
                    if (vas.Formula != null)
                    {
                        bool first = true;
                        bool hasPeriodFilter = false;
                        // CHECK PERIODS !! (instant/duration) => filters?

                        

                        
                        List<string> scenarios = new List<string>();
                        List<string> periods = new List<string>();

                        if (vas.Calculation != null && !_taxonomy.IncludedFormula)
                        {
                            // find calculation
                            foreach (Variable var in varList.Where(v => v.Type == VariableTypes.Fact && v.Filters.OfType<ConceptFilter>().Where(c => c.QNames.Count > 1).Count() > 0).ToList())
                            {
                                ConceptFilter cf = var.Filters.OfType<ConceptFilter>().First();
                                foreach (string qn in cf.QNames)
                                {
                                    string[] qnsplit = qn.Split(new char[] { ':' });
                                    string nwVarName = qnsplit[qnsplit.Length - 1];
                                    if (aset.Variables.Count(v => v.Name == nwVarName) == 0 && vas.Calculation.UsedVariables.Contains(nwVarName))
                                    {
                                        Variable varNew = new Variable
                                        {
                                            BindAsSequence = var.BindAsSequence
                                            ,
                                            FallBackvalue = var.FallBackvalue
                                            ,
                                            Name = nwVarName
                                            ,
                                            Origin = var.Origin
                                            ,
                                            Select = null
                                            ,
                                            SelectFormula = null
                                            ,
                                            Type = VariableTypes.Fact
                                            ,
                                            UsesVariables = new List<string>()
                                            ,
                                            Filters = new List<Filter>()
                                        };
                                        varNew.Filters.AddRange(var.Filters);
                                        varNew.Filters.Remove(cf);
                                        varNew.Filters.Add(new ConceptFilter
                                        {
                                            Complement = cf.Complement
                                            ,
                                            Cover = cf.Cover
                                            ,
                                            Label = cf.Label
                                            ,
                                            QNames = new List<string> { qn }
                                        });
                                        aset.Variables.Add(varNew);
                                        varList.Add(varNew);
                                    }

                                }
                            }
                        }

                        // create info's, adding all parameters
                        List<VariableInfo> variableInfo = new List<VariableInfo>();
                        variableInfo.AddRange(_parameters.AllParameters.Values.Select(p => new VariableInfo
                        {
                            FallBack = p.Value
                            ,
                            Name = p.Name
                            ,
                            Type = GetResultType(p.ValueType)
                        }).ToList());

                        bool hasTypedDim;




                        Variable prevVar = null;

                        // loop through all fact filters, concepts and such to limit all possible matching scenario defs
                        foreach (Variable var in varList.Where(v => v.Type == VariableTypes.Fact))
                        {
                            string varType = "string";
                            ElementSelection es = new ElementSelection
                            {
                                Definitions = new List<XbrlElement>()
                                ,
                                Periods = new List<string>()
                                ,
                                QNames = new List<string>()
                                ,
                                Scenarios = new List<string>()
                                , ScenarioDims = new List<string>()
                                , TypedDimensionFilters = new List<TypedDimensionFilter>()
                                , ExplicitDimensionFilters = new List<ExplicitDimensionFilter>()
                                
                            };

                            /* IMPLICIT ?
                            if (prevVar != null && vas.ImplicitFiltering)
                            {
                                if (var.Filters.OfType<PeriodFilter>().FirstOrDefault() == null && prevVar.Filters.OfType<PeriodFilter>().FirstOrDefault() != null)
                                {
                                    var.Filters.Add(prevVar.Filters.OfType<PeriodFilter>().FirstOrDefault());
                                }
                                if (var.Filters.OfType<ExplicitDimensionFilter>().Count() == 0 && var.Filters.OfType<TypedDimensionFilter>().Count() == 0)
                                {

                                    var.Filters.AddRange(prevVar.Filters.Where(f => f.GetType() == typeof(ExplicitDimensionFilter) || f.GetType() == typeof(TypedDimensionFilter)).ToList());
                                        
                                        //&& var.Filters.OfType<TypedDimensionFilter>().Count() == 0
                                }
                            }
                            */
                            List<string> subscens = new List<string>();
                            foreach (ConceptFilter cf in var.Filters.OfType<ConceptFilter>())
                            {
                                bool firstq = true;
                                foreach (string qname in cf.QNames)
                                {
                                    string[] Qselectors = qname.Split(new char[] { ':' });
                                    es.QNames.Add(qname);
                                    XbrlElement xel = _taxonomy.Elements.Values.FirstOrDefault(x => x.Id == string.Join("_", Qselectors));
                                    if (xel != null)
                                    {
                                        es.Definitions.Add(xel);

                                        if ((xel.Periods == null || xel.Periods.Count == 0) && xel.PeriodType.ToLower() == "instant")
                                        {
                                            xel.Periods = new List<string> { "I-Start", "I-End" };
                                        }

                                        if (firstq)
                                        {
                                            es.Periods.AddRange(xel.Periods);

                                            if (ElementScenarios.ContainsKey(qname))
                                            {
                                                es.Scenarios.AddRange(ElementScenarios[qname]);
                                            }
                                            else
                                            {
                                                es.Scenarios.Add("");
                                            }
                                            firstq = false;
                                        }
                                        else
                                        {
                                            es.Periods = es.Periods.Intersect(xel.Periods).ToList();
                                            if (ElementScenarios.ContainsKey(qname))
                                            {
                                                es.Scenarios.AddRange(ElementScenarios[qname]);
                                                es.Scenarios = es.Scenarios.Intersect(ElementScenarios[qname]).ToList();
                                            }
                                            else
                                            {
                                                es.Scenarios = es.Scenarios.Where(s => s == "").ToList();
                                            }
                                        }
                                    }
                                    else
                                    {
                                        Console.WriteLine("Tries to select unexisting element " + qname);
                                    }
                                    

                                    
                                  
                                }
                            }
                            List<string> tpes = es.Definitions.Select(e => e.DataType.RootType).Distinct().ToList();
                            if(tpes.Count>1) {
                                Console.WriteLine("multiple types");
                            } else {
                                varType = tpes.FirstOrDefault();
                            }

                            if (es.QNames.Count > 0)
                            {
                                PeriodFilter pf = var.Filters.OfType<PeriodFilter>().FirstOrDefault();
                                if (pf != null)
                                {
                                    hasPeriodFilter = true;
                                    if (pf.Test.Contains("$PeriodStartDate") && (var.Name == "FormulaTarget" || (var.Name != "FormulaTarget" && pf.Test.Contains("is-instant-period"))))
                                    {
                                        es.Periods = es.Periods.Where(p => p.ToLower() == "i-start").ToList();
                                    }
                                    if (pf.Test.Contains("$PeriodEndDate") && (var.Name == "FormulaTarget" || (var.Name != "FormulaTarget" && pf.Test.Contains("is-instant-period"))))
                                    {
                                        es.Periods = es.Periods.Where(p => p.ToLower() == "i-end").ToList();
                                    }
                                    if (pf.PeriodTest == "ALL")
                                    {
                                        pf.PeriodTest = es.Periods.First();
                                    }
                                   
                                    es.PeriodFilter = pf;
                                }

                                foreach (ExplicitDimensionFilter edf in var.Filters.OfType<ExplicitDimensionFilter>())
                                {
                                    es.ExplicitDimensionFilters.Add(edf);
                                    string selector = edf.Dimension;
                                    if (edf.Members != null && edf.Members.Count > 0)
                                    {
                                        List<string> selectors = edf.Members.Select(m => string.Format("{0}::{1}", edf.Dimension, m)).ToList();
                                        List<string> tmpScenarios = new List<string>();

                                        if (edf.Complement)
                                        {
                                            selectors.ForEach(sel =>
                                            {
                                                es.Scenarios.RemoveAll(s => s.ToLower().Contains(sel.ToLower()));
                                            });
                                        }

                                        else
                                        {
                                            selectors.ForEach(sel =>
                                                {
                                                    if (es.Scenarios.Count(s => s.ToLower().Contains(sel.ToLower())) > 0)
                                                    {
                                                        tmpScenarios.AddRange(es.Scenarios.Where(s => s.ToLower().Contains(sel.ToLower())).ToList());
                                                    }
                                                });
                                            es.Scenarios = tmpScenarios;
                                        }
                                      
                                    }
                                    else
                                    {
                                        if (edf.Complement)
                                        {
                                            es.Scenarios = es.Scenarios.Where(s => !s.ToLower().Contains(selector.ToLower())).ToList();
                                        }
                                        else
                                        {
                                            es.Scenarios = es.Scenarios.Where(s => s.ToLower().Contains(selector.ToLower())).ToList();
                                        }
                                    }
                                }

                                foreach (TypedDimensionFilter tdf in var.Filters.OfType<TypedDimensionFilter>())
                                {
                                    hasTypedDim = true;
                                    es.TypedDimensionFilters.Add(tdf);
                                    if (tdf.Complement)
                                    {
                                        es.Scenarios = es.Scenarios.Where(s => !s.ToLower().Contains(tdf.Dimension.ToLower())).ToList();
                                    }
                                    else
                                    {
                                        es.Scenarios = es.Scenarios.Where(s => s.ToLower().Contains(tdf.Dimension.ToLower())).ToList();
                                    }
                                }


                                es.ScenarioDims = new List<string>();
                                foreach (string scen in es.Scenarios)
                                {
                                    string dimScen = "";
                                    string[] spscens = scen.Split(new char[] {'/'});
                                    foreach (string spscen in spscens)
                                    {
                                        string[] dms = spscen.Split(new string[] { "::" }, StringSplitOptions.None);
                                        if (dimScen.Length > 0) dimScen += "/";
                                        dimScen += dms[0];
                                    }
                                    if (!es.ScenarioDims.Contains(dimScen))
                                    {
                                        es.ScenarioDims.Add(dimScen);
                                    }
                                }

                                elementSelections.Add(var.Name, es);

                                if (var.Name != "FormulaTarget")
                                {
                                    if (first)
                                    {
                                        periods.AddRange(es.Periods);
                                        scenarios.AddRange(es.Scenarios);
                                    }
                                    else
                                    {
                                        periods = periods.Intersect(es.Periods).ToList();
                                        scenarios = scenarios.Intersect(es.Scenarios).ToList();
                                    }
                                }
                            }
                            else
                            {
                                
                            }
                            if (!string.IsNullOrEmpty(varType) && es.Definitions.Count != 0)
                            {
                                variableInfo.Add(new VariableInfo
                                    {
                                        Name = var.Name,
                                        FallBack = var.FallBackvalue,
                                        Type = GetResultType(varType)
                                    }
                                );
                            }
                            else
                            {
                                Console.WriteLine("Unknown element : " + var.Name);
                            }
                        }

                        

                        // Got all possible scenariodefs

                        StringBuilder sb = new StringBuilder();

                        // Established all element selections
                        // 
                        foreach (Variable var in aset.Variables.Where(v => v.Type == VariableTypes.Parameter))
                        {
                            // NOTHING, GLOBALLY DEFINED
                        }


                       
                        string calculationCreateArguments = string.Empty;
                        

                        List<Variable> genvars = varList.Where(v => v.Type == VariableTypes.General).ToList();

                        

                        foreach (Variable gvar in genvars.ToList())
                        {
                            int myIdx = genvars.IndexOf(gvar);
                            List<Variable> usedin = genvars.Where(v=>v.SelectFormula!=null && v.SelectFormula.UsedVariables!=null && v.SelectFormula.UsedVariables.Contains(gvar.Name)).ToList();
                            if (usedin.Count > 0)
                            {
                                foreach (Variable uivar in usedin)
                                {
                                    int uiIdx = genvars.IndexOf(uivar);
                                    if (uiIdx < myIdx)
                                    {
                                        genvars.Remove(uivar);
                                        int newIdx = myIdx >= genvars.Count ? genvars.Count : myIdx + 1;
                                        genvars.Insert(newIdx, uivar);
                                        myIdx = genvars.IndexOf(gvar);
                                    }
                                }
                            }
                           
                        }


                        scenarios = scenarios.Distinct().ToList();


                        bool isForEach = false;
                        List<string> variables = new List<string>();
                        if (vas.Formula.UsedVariables.Count > 0) variables.AddRange(vas.Formula.UsedVariables);
                        if (varList.Count(v => v.Type == VariableTypes.Fact && !variables.Contains(v.Name)) > 0)
                            variables.AddRange(varList.Where(v => v.Type == VariableTypes.Fact && !variables.Contains(v.Name)).Select(v => v.Name).ToList());
                        if (aset.Formula != null && aset.Formula.Target != null)
                        {
                            //  variables.Add(aset.Formula.Target.Name);

                        }
                        if (vas.ImplicitFiltering)
                        {
                            



                            if (vas.Formula.UsedVariables.Count > 0)
                            {
                                Variable var = null;
                                string varName = string.Empty;
                                ElementSelection varEs;
                                ElementSelection varCtxEs;
                                int cnt = 0;
                                while (var == null && cnt < vas.Formula.UsedVariables.Count)
                                {
                                    varName = vas.Formula.UsedVariables[cnt];
                                    var = aset.Variables.FirstOrDefault(v => v.Type == VariableTypes.Fact && v.Name == varName && var.BindAsSequence);
                                    cnt++;
                                }

                                varEs = elementSelections[varName];

                                if (var != null && !string.IsNullOrEmpty(varName))
                                {
                                    if (elementSelections.ContainsKey(varName))
                                    {

                                        sb.Append("List<ContextElementDataContract> contexts = ");
                                        sb.Append("GetContexts(new List<string> { ");
                                        varCtxEs = varEs;
                                        if (varEs.PeriodFilter != null)
                                        {
                                            sb.Append("\"").Append(varEs.PeriodFilter.PeriodTest).Append("\"");
                                            // periodCovered = true;
                                        }
                                        else if (varEs.Periods.Count > 0)
                                        {
                                            sb.Append(string.Join(",", varEs.Periods.Select(q => string.Format("\"{0}\"", q)).ToArray()));
                                        }
                                        sb.Append("}, new List<string> { ");
                                        // SCENARIODEFS
                                        sb.Append(string.Join(",", varEs.Scenarios.Select(q => string.Format("\"{0}\"", q)).ToArray()));
                                        sb.Append("});");
                                        sb.Append(Environment.NewLine);


                                        string qnames;
                                        string speriods;
                                        List<string> dims = new List<string>();
                                        dims.AddRange(varEs.ScenarioDims);
                                        if (elementSelections.ContainsKey("FormulaTarget")) dims.AddRange(elementSelections["FormulaTarget"].ScenarioDims);


                                        if (scenarios.Count > 1 && periods.Distinct().Count() == 1 && dims.Distinct().Count() > 1)
                                        {
                                            // any context (selection in element selection)

                                            if (vas.Formula.UsedVariables.Count > 0)
                                            {
                                                sb.Append("if(contexts.Count>0) { ").Append(Environment.NewLine);


                                                foreach (string nextVarName in variables)
                                                {

                                                    var = varList.FirstOrDefault(v => v.Type == VariableTypes.Fact && v.Name == nextVarName);
                                                    if (var != null)
                                                    {
                                                        
                                                        varEs = elementSelections[nextVarName];
                                                        sb.Append("var ").Append(nextVarName).Append(" = FilterElementsByContext(");
                                                        if (var.BindAsSequence)
                                                        {
                                                            sb.Append(" contexts.FirstOrDefault(),");
                                                        }
                                                        else
                                                        {
                                                            sb.Append("null,");
                                                        }
                                                        // qnames
                                                        qnames = string.Join(",", varEs.QNames.Select(q => string.Format("\"{0}\"", q)).ToArray());
                                                        sb.Append("new List<string> {").Append(qnames).Append(" }, new List<string> { ");
                                                        // periods
                                                        if (varEs.PeriodFilter != null)
                                                        {
                                                            speriods = string.Join(",", varEs.Periods.Select(q => string.Format("\"{0}\"", q)).ToArray());
                                                            sb.Append(speriods);
                                                        }
                                                        sb.Append(" }, new List<DimensionFilter> {");
                                                        if (varEs.ExplicitDimensionFilters.Count > 0 || varEs.TypedDimensionFilters.Count > 0)
                                                        {
                                                            sb.Append(string.Join(",", GetDimensionFiltersList(var, varEs)));
                                                        }
                                                        // dimensionfilters
                                                        sb.Append(" }, new List<string> { ");
                                                        // scenariodefs
                                                        string sscendefs = string.Join(",", varEs.Scenarios.Select(q => string.Format("\"{0}\"", q)).ToArray());
                                                        sb.Append(sscendefs);
                                                        sb.Append(" });");
                                                        sb.Append(Environment.NewLine);

                                                    }
                                                }



                                                

                                                StringBuilder sbIf = new StringBuilder();
                                                foreach (Variable varf in varList.Where(v => v.Type == VariableTypes.Fact))
                                                {
                                                    if (sbIf.Length > 0) sbIf.Append(" || ");
                                                    sbIf.Append(varf.Name).Append(".Count > 0");
                                                }
                                                sb.Append("if(").Append(sbIf.ToString()).Append(")").Append(Environment.NewLine);
                                                sb.Append("{").Append(Environment.NewLine);


                                                foreach (Variable varGen in genvars)
                                                {

                                                    // TODO + order?
                                                    FormulaRenderer frGen = new FormulaRenderer();
                                                    // asetGenVarFormula.Add(vari.SelectFormula.Formula);
                                                    frGen.VariableInfos = variableInfo;
                                                    Statement fresGen = frGen.ConvertFormula(varGen.SelectFormula.Formula);
                                                    variableInfo.Add(new VariableInfo
                                                    {
                                                        FallBack = null,
                                                        Name = varGen.Name,
                                                        Type = fresGen.ResultType
                                                    });

                                                    sb.Append("var ").Append(varGen.Name).Append(" = ").Append(fresGen.Body).Append(";").Append(Environment.NewLine);

                                                }
                                                if (preTest != null)
                                                {
                                                    FormulaRenderer frPreTest = new FormulaRenderer();
                                                    frPreTest.VariableInfos = variableInfo;
                                                    Statement fresPreTest = frPreTest.ConvertFormula(preTest.Formula.Formula);
                                                    sb.Append("if(").Append(fresPreTest.Body).Append(")").Append(Environment.NewLine);
                                                    sb.Append("{").Append(Environment.NewLine);
                                                }

                                                // CALCULATION
                                                sb.Append("//CALCULATION HERE").Append(Environment.NewLine);

                                                if (calculationTarget != null) //&& _taxonomy.IncludedFormula
                                                {
                                                    
                                                    ElementSelection varTgEs = elementSelections[calculationTarget.Name];
                                                    List<string> tgscens = new List<string>();
                                                    tgscens.AddRange(varTgEs.Scenarios);
                                                    if (tgscens.Count(c => varCtxEs.Scenarios.Contains(c)) > 0)
                                                    {
                                                        tgscens = tgscens.Where(c => varCtxEs.Scenarios.Contains(c)).ToList();
                                                    }
                                                    if (!_calculationElements.ContainsKey(varTgEs.QNames.First()))
                                                    {
                                                        _calculationElements.Add(varTgEs.QNames.First(), tgscens);
                                                    }
                                                    else
                                                    {
                                                        List<string> cscens = _calculationElements[varTgEs.QNames.First()];
                                                        cscens.AddRange(tgscens);
                                                        cscens = cscens.Distinct().ToList();
                                                        _calculationElements[varTgEs.QNames.First()] = cscens;
                                                    }
                                                    string sscendefs = string.Join(",", tgscens.Select(q => string.Format("\"{0}\"", q)).ToArray());
                                                    speriods = string.Join(",", varTgEs.Periods.Select(q => string.Format("\"{0}\"", q)).ToArray());

                                                    sb.Append("if(").Append(calculationTarget.Name).Append(".Count==0)").Append(Environment.NewLine);
                                                    sb.Append("{").Append(Environment.NewLine);
                                                    sb.Append(calculationTarget.Name).Append(" = CreateNewCalculationElement(contexts.FirstOrDefault(),\"")
                                                        //.Append(calculationCreateArguments)
                                                             .Append(varTgEs.QNames.First()).Append("\", new List<string> { ");
                                                    if (varTgEs.PeriodFilter != null)
                                                    {
                                                        sb.Append(speriods);
                                                    }
                                                    sb.Append(" } , new List<DimensionFilter> {");
                                                    if (varTgEs.ExplicitDimensionFilters.Count > 0 || varTgEs.TypedDimensionFilters.Count > 0)
                                                    {
                                                        sb.Append(string.Join(",", GetDimensionFiltersList(calculationTarget, varTgEs)));
                                                    }
                                                    sb.Append(" }, new List<string> { ")
                                                        .Append(sscendefs)
                                                        .Append(" });")

                                                        // TODO: ADD UNIT and such
                                                        .Append(Environment.NewLine);
                                                    sb.Append("}").Append(Environment.NewLine);

                                                    sb.Append("if(").Append(calculationTarget.Name).Append(".Count>1)").Append(Environment.NewLine);
                                                    sb.Append("{").Append(Environment.NewLine);
                                                    sb.Append("throw new Exception(\"More then one target for ").Append(calculationTarget.Name).Append(" in assertion ").Append(vas.Id).Append("\");")
                                                        .Append(Environment.NewLine);
                                                    sb.Append("}").Append(Environment.NewLine);


                                                    FormulaRenderer frVas2 = new FormulaRenderer();
                                                    frVas2.VariableInfos = variableInfo;
                                                    Statement fresVas2 = frVas2.ConvertFormula(aset.Formula.Formula.Formula.Formula);


                                                    sb.Append(calculationTarget.Name).Append(".First().SetNumber(").Append(fresVas2.Body).Append(");")
                                                        .Append(Environment.NewLine);
                                                }





                                                // FORMULA
                                                sb.Append("//FORMULA HERE").Append(Environment.NewLine);
                                                FormulaRenderer frVas = new FormulaRenderer();
                                                frVas.VariableInfos = variableInfo;
                                                Statement fresVas = frVas.ConvertFormula(vas.Formula.Formula);

                                                sb.Append("bool test = ").Append(fresVas.Body).Append(";").Append(Environment.NewLine);
                                                sb.Append("if (!test)").Append(Environment.NewLine);
                                                sb.Append("{").Append(Environment.NewLine);
                                                //MESSAGE
                                                //sb.Append("AddMessage(new BizTaxErrorDataContract { FileId=Guid.Empty,Id=1,Messages=new List<BizTaxErrorMessageDataContract> { new BizTaxErrorMessageDataContract { Message=\"").Append(vas.Id).Append("\"} } });").Append(Environment.NewLine);
                                                sb.Append(CreateMessages(varList, vas.Messages, vas.Id));
                                                sb.Append("}").Append(Environment.NewLine);

                                                sb.Append("}").Append(Environment.NewLine);

                                                //  sb.Append("}").Append(Environment.NewLine);

                                                if (preTest != null)
                                                {
                                                    sb.Append("}").Append(Environment.NewLine);
                                                }

                                                sb.Append("}").Append(Environment.NewLine);
                                            }

                                        }
                                        else
                                        {
                                            // by contexts

                                            sb.Append("foreach(ContextElementDataContract context in contexts)").Append(Environment.NewLine);
                                            sb.Append("{").Append(Environment.NewLine);


                                            /*
                                            string qnames = string.Join(",", varEs.QNames.Select(q => string.Format("\"{0}\"", q)).ToArray());
                                            string speriods = string.Join(",", varEs.Periods.Select(q => string.Format("\"{0}\"", q)).ToArray());

                                            sb.Append("var ").Append(varName)
                                                .Append(" = FilterElementsByContext(context,new List<string> { ")
                                                .Append(qnames).Append(" }, new List<string> { ")
                                                .Append(speriods).Append(" });")
                                                .Append(Environment.NewLine);
                                             * */

                                            List<string> ctxScens = new List<string>();
                                            ctxScens.AddRange(varEs.Scenarios.ToList());
                                            //variables = variables.Where(v => v != varName).ToList();


                                            foreach (string nextVarName in variables)
                                            {

                                                var = varList.FirstOrDefault(v => v.Type == VariableTypes.Fact && v.Name == nextVarName);
                                                if (var != null)
                                                {
                                                    varEs = elementSelections[nextVarName];
                                                    if (nextVarName == "FormulaTarget")
                                                    {

                                                        sb.Append("var ").Append(nextVarName).Append(" = GetCalcElementsByContext(context,");
                                                    }
                                                    else
                                                    {
                                                        sb.Append("var ").Append(nextVarName).Append(" = FilterElementsByContext(context,");
                                                    }
                                                    // qnames
                                                    qnames = string.Join(",", varEs.QNames.Select(q => string.Format("\"{0}\"", q)).ToArray());
                                                    sb.Append("new List<string> {").Append(qnames).Append(" }, new List<string> { ");
                                                    // periods
                                                    if (varEs.PeriodFilter != null)
                                                    {
                                                        speriods = string.Join(",", varEs.Periods.Select(q => string.Format("\"{0}\"", q)).ToArray());
                                                        sb.Append(speriods);
                                                    }
                                                    sb.Append(" }, new List<DimensionFilter> {");
                                                    if (varEs.ExplicitDimensionFilters.Count > 0 || varEs.TypedDimensionFilters.Count > 0)
                                                    {
                                                        sb.Append(string.Join(",", GetDimensionFiltersList(var, varEs)));
                                                    }
                                                    // dimensionfilters
                                                    sb.Append(" }, new List<string> { ");
                                                    // scenariodefs
                                                    string sscendefs = string.Join(",", varEs.Scenarios.Select(q => string.Format("\"{0}\"", q)).ToArray());
                                                    sb.Append(sscendefs).Append(" });");
                                                    sb.Append(Environment.NewLine);

                                                }
                                            }




                                            StringBuilder sbIf = new StringBuilder();
                                            foreach (Variable varf in varList.Where(v => v.Type == VariableTypes.Fact))
                                            {
                                                if (sbIf.Length > 0) sbIf.Append(" || ");
                                                sbIf.Append(varf.Name).Append(".Count > 0");
                                            }
                                            sb.Append("if(").Append(sbIf.ToString()).Append(")").Append(Environment.NewLine);
                                            sb.Append("{").Append(Environment.NewLine);




                                            foreach (Variable varGen in genvars)
                                            {

                                                // TODO + order?
                                                FormulaRenderer frGen = new FormulaRenderer();
                                                // asetGenVarFormula.Add(vari.SelectFormula.Formula);
                                                frGen.VariableInfos = variableInfo;
                                                Statement fresGen = frGen.ConvertFormula(varGen.SelectFormula.Formula);
                                                variableInfo.Add(new VariableInfo
                                                {
                                                    FallBack = null,
                                                    Name = varGen.Name,
                                                    Type = fresGen.ResultType
                                                });

                                                sb.Append("var ").Append(varGen.Name).Append(" = ").Append(fresGen.Body).Append(";").Append(Environment.NewLine);

                                            }


                                            if (preTest != null)
                                            {
                                                FormulaRenderer frPreTest = new FormulaRenderer();
                                                frPreTest.VariableInfos = variableInfo;
                                                Statement fresPreTest = frPreTest.ConvertFormula(preTest.Formula.Formula);
                                                sb.Append("if(").Append(fresPreTest.Body).Append(")").Append(Environment.NewLine);
                                                sb.Append("{").Append(Environment.NewLine);
                                            }

                                            // CALCULATION
                                            sb.Append("//CALCULATION HERE").Append(Environment.NewLine);

                                            if (calculationTarget != null) //&& _taxonomy.IncludedFormula
                                            {
                                                ElementSelection varTgEs = elementSelections[calculationTarget.Name];
                                                List<string> tgscens = new List<string>();
                                                tgscens.AddRange(varTgEs.Scenarios);
                                                if (tgscens.Count(c => varCtxEs.Scenarios.Contains(c)) > 0)
                                                {
                                                    tgscens = tgscens.Where(c => varCtxEs.Scenarios.Contains(c)).ToList();
                                                }
                                                if (!_calculationElements.ContainsKey(varTgEs.QNames.First()))
                                                {
                                                    _calculationElements.Add(varTgEs.QNames.First(), tgscens);
                                                }
                                                else
                                                {
                                                    List<string> cscens = _calculationElements[varTgEs.QNames.First()];
                                                    cscens.AddRange(tgscens);
                                                    cscens = cscens.Distinct().ToList();
                                                    _calculationElements[varTgEs.QNames.First()] = cscens;
                                                }

                                                string sscendefs = string.Join(",", tgscens.Select(q => string.Format("\"{0}\"", q)).ToArray());
                                                speriods = string.Join(",", varTgEs.Periods.Select(q => string.Format("\"{0}\"", q)).ToArray());

                                                sb.Append("if(").Append(calculationTarget.Name).Append(".Count==0)").Append(Environment.NewLine);
                                                sb.Append("{").Append(Environment.NewLine);
                                                sb.Append(calculationTarget.Name).Append(" = CreateNewCalculationElement(context,\"")
                                                    //.Append(calculationCreateArguments)
                                                    .Append(varTgEs.QNames.First()).Append("\", new List<string> { ");
                                                if (varTgEs.PeriodFilter != null)
                                                {
                                                    sb.Append(speriods);
                                                }
                                                sb.Append(" } , new List<DimensionFilter> {");
                                                if (varTgEs.ExplicitDimensionFilters.Count > 0 || varTgEs.TypedDimensionFilters.Count > 0)
                                                {
                                                    sb.Append(string.Join(",", GetDimensionFiltersList(calculationTarget, varTgEs)));
                                                }
                                                sb.Append(" }, new List<string> { ")
                                                    .Append(sscendefs)
                                                    .Append(" });")
                                                    // TODO: ADD UNIT and such
                                                    .Append(Environment.NewLine);
                                                sb.Append("}").Append(Environment.NewLine);

                                                sb.Append("if(").Append(calculationTarget.Name).Append(".Count>1)").Append(Environment.NewLine);
                                                sb.Append("{").Append(Environment.NewLine);
                                                sb.Append("throw new Exception(\"More then one target for ").Append(calculationTarget.Name).Append(" in assertion ").Append(vas.Id).Append("\");")
                                                    .Append(Environment.NewLine);
                                                sb.Append("}").Append(Environment.NewLine);


                                                FormulaRenderer frVas2 = new FormulaRenderer();
                                                frVas2.VariableInfos = variableInfo;
                                                Statement fresVas2 = frVas2.ConvertFormula(aset.Formula.Formula.Formula.Formula);


                                                sb.Append(calculationTarget.Name).Append(".First().SetNumber(").Append(fresVas2.Body).Append(");")
                                                    .Append(Environment.NewLine);
                                            }
                                            /*else if (calculationTarget != null && !_taxonomy.IncludedFormula)
                                            {
                                                sb.Append("if(").Append(calculationTarget.Name).Append(".Count==0)").Append(Environment.NewLine);
                                                sb.Append("{").Append(Environment.NewLine);
                                                sb.Append(calculationTarget.Name).Append(" = CreateNewCalculationElement(").Append(calculationCreateArguments).Append(");")
                                                    .Append(Environment.NewLine);
                                                sb.Append("}").Append(Environment.NewLine);

                                                sb.Append("if(").Append(calculationTarget.Name).Append(".Count>1)").Append(Environment.NewLine);
                                                sb.Append("{").Append(Environment.NewLine);
                                                sb.Append("throw new Exception(\"More then one target for ").Append(calculationTarget.Name).Append(" in assertion ").Append(vas.Id).Append("\");")
                                                    .Append(Environment.NewLine);
                                                sb.Append("}").Append(Environment.NewLine);


                                                FormulaRenderer frVas2 = new FormulaRenderer();
                                                frVas2.VariableInfos = variableInfo;
                                                Statement fresVas2 = frVas2.ConvertFormula(vas.Calculation.Formula);


                                                sb.Append(calculationTarget.Name).Append(".First().SetNumber(").Append(fresVas2.Body).Append(");")
                                                    .Append(Environment.NewLine);
                                            }*/




                                            // FORMULA
                                            sb.Append("//FORMULA HERE").Append(Environment.NewLine);
                                            FormulaRenderer frVas = new FormulaRenderer();
                                            frVas.VariableInfos = variableInfo;
                                            Statement fresVas = frVas.ConvertFormula(vas.Formula.Formula);

                                            sb.Append("bool test = ").Append(fresVas.Body).Append(";").Append(Environment.NewLine);
                                            sb.Append("if (!test)").Append(Environment.NewLine);
                                            sb.Append("{").Append(Environment.NewLine);
                                            //MESSAGE
                                            //sb.Append("AddMessage(new BizTaxErrorDataContract { FileId=Guid.Empty,Id=1,Messages=new List<BizTaxErrorMessageDataContract> { new BizTaxErrorMessageDataContract { Message=\"").Append(vas.Id).Append("\"} } });").Append(Environment.NewLine);
                                            sb.Append(CreateMessages(varList, vas.Messages, vas.Id));
                                            sb.Append("}").Append(Environment.NewLine);

                                            sb.Append("}").Append(Environment.NewLine);

                                            //  sb.Append("}").Append(Environment.NewLine);

                                            if (preTest != null)
                                            {
                                                sb.Append("}").Append(Environment.NewLine);
                                            }

                                            sb.Append("}").Append(Environment.NewLine);


                                            // end of foreach




                                        }

                                    }
                                }
                            }
                        }
                        else
                        {
                            // NON IMPLICIT
                            /*
                            foreach (string nextVarName in variables)
                            {

                                Variable var = varList.FirstOrDefault(v => v.Type == VariableTypes.Fact && v.Name == nextVarName);
                                if (var != null)
                                {
                                    ElementSelection varEs = elementSelections[nextVarName];
                                    sb.Append("var ").Append(nextVarName).Append(" = GetElements(");
                                    // qnames
                                    string qnames = string.Join(",", varEs.QNames.Select(q => string.Format("\"{0}\"", q)).ToArray());
                                    sb.Append("new List<string> {").Append(qnames).Append(" }, new List<string> { ");
                                    // periods
                                    if (varEs.PeriodFilter != null)
                                    {
                                        string speriods = string.Join(",", varEs.Periods.Select(q => string.Format("\"{0}\"", q)).ToArray());
                                        sb.Append(speriods);
                                    }
                                    sb.Append(" }, new List<DimensionFilter> {");
                                    if (varEs.ExplicitDimensionFilters.Count > 0 || varEs.TypedDimensionFilters.Count > 0)
                                    {
                                        sb.Append(string.Join(",", GetDimensionFiltersList(var, varEs)));
                                    }
                                    // dimensionfilters
                                    sb.Append(" }, new List<string> { ");
                                    // scenariodefs
                                    string sscendefs = string.Join(",", varEs.Scenarios.Select(q => string.Format("\"{0}\"", q)).ToArray());
                                    sb.Append(sscendefs);
                                    sb.Append(" });");
                                    sb.Append(Environment.NewLine);

                                }
                            }





                            StringBuilder sbIf = new StringBuilder();
                            foreach (Variable varf in varList.Where(v => v.Type == VariableTypes.Fact))
                            {
                                if (sbIf.Length > 0) sbIf.Append(" || ");
                                sbIf.Append(varf.Name).Append(".Count > 0");
                            }
                            sb.Append("if(").Append(sbIf.ToString()).Append(")").Append(Environment.NewLine);
                            sb.Append("{").Append(Environment.NewLine);


                            foreach (Variable varGen in genvars)
                            {

                                // TODO + order?
                                FormulaRenderer frGen = new FormulaRenderer();
                                // asetGenVarFormula.Add(vari.SelectFormula.Formula);
                                frGen.VariableInfos = variableInfo;
                                Statement fresGen = frGen.ConvertFormula(varGen.SelectFormula.Formula);
                                variableInfo.Add(new VariableInfo
                                {
                                    FallBack = null,
                                    Name = varGen.Name,
                                    Type = fresGen.ResultType
                                });

                                sb.Append("var ").Append(varGen.Name).Append(" = ").Append(fresGen.Body).Append(";").Append(Environment.NewLine);

                            }
                            if (preTest != null)
                            {
                                FormulaRenderer frPreTest = new FormulaRenderer();
                                frPreTest.VariableInfos = variableInfo;
                                Statement fresPreTest = frPreTest.ConvertFormula(preTest.Formula.Formula);
                                sb.Append("if(").Append(fresPreTest.Body).Append(")").Append(Environment.NewLine);
                                sb.Append("{").Append(Environment.NewLine);
                            }

                            // CALCULATION
                            sb.Append("//CALCULATION HERE").Append(Environment.NewLine);

                            if (calculationTarget != null) //&& _taxonomy.IncludedFormula
                            {

                                ElementSelection varTgEs = elementSelections[calculationTarget.Name];
                                if (!_calculationElements.ContainsKey(varTgEs.QNames.First()))
                                {
                                    _calculationElements.Add(varTgEs.QNames.First(), varTgEs.ScenarioDims);
                                }
                                else
                                {
                                    List<string> cscens = _calculationElements[varTgEs.QNames.First()];
                                    cscens.AddRange(varTgEs.ScenarioDims);
                                    cscens = cscens.Distinct().ToList();
                                    _calculationElements[varTgEs.QNames.First()] = cscens;
                                }
                                string sscendefs = string.Join(",", varTgEs.Scenarios.Select(q => string.Format("\"{0}\"", q)).ToArray());
                                string speriods = string.Join(",", varTgEs.Periods.Select(q => string.Format("\"{0}\"", q)).ToArray());

                                sb.Append("if(").Append(calculationTarget.Name).Append(".Count==0)").Append(Environment.NewLine);
                                sb.Append("{").Append(Environment.NewLine);
                                sb.Append(calculationTarget.Name).Append(" = CreateNewCalculationElement(\"")
                                    //.Append(calculationCreateArguments)
                                         .Append(varTgEs.QNames.First()).Append("\", new List<string> { ");
                                if (varTgEs.PeriodFilter != null)
                                {
                                    sb.Append(speriods);
                                }
                                sb.Append(" } , new List<DimensionFilter> {");
                                if (varTgEs.ExplicitDimensionFilters.Count > 0 || varTgEs.TypedDimensionFilters.Count > 0)
                                {
                                    sb.Append(string.Join(",", GetDimensionFiltersList(calculationTarget, varTgEs)));
                                }
                                sb.Append(" }, new List<string> { ")
                                    .Append(sscendefs)
                                    .Append(" });")

                                    // TODO: ADD UNIT and such
                                    .Append(Environment.NewLine);
                                sb.Append("}").Append(Environment.NewLine);

                                sb.Append("if(").Append(calculationTarget.Name).Append(".Count>1)").Append(Environment.NewLine);
                                sb.Append("{").Append(Environment.NewLine);
                                sb.Append("throw new Exception(\"More then one target for ").Append(calculationTarget.Name).Append(" in assertion ").Append(vas.Id).Append("\");")
                                    .Append(Environment.NewLine);
                                sb.Append("}").Append(Environment.NewLine);


                                FormulaRenderer frVas2 = new FormulaRenderer();
                                frVas2.VariableInfos = variableInfo;
                                Statement fresVas2 = frVas2.ConvertFormula(aset.Formula.Formula.Formula.Formula);


                                sb.Append(calculationTarget.Name).Append(".First().SetNumber(").Append(fresVas2.Body).Append(");")
                                    .Append(Environment.NewLine);
                            }





                            // FORMULA
                            sb.Append("//FORMULA HERE").Append(Environment.NewLine);
                            FormulaRenderer frVas = new FormulaRenderer();
                            frVas.VariableInfos = variableInfo;
                            Statement fresVas = frVas.ConvertFormula(vas.Formula.Formula);

                            sb.Append("bool test = ").Append(fresVas.Body).Append(";").Append(Environment.NewLine);
                            sb.Append("if (!test)").Append(Environment.NewLine);
                            sb.Append("{").Append(Environment.NewLine);
                            //MESSAGE
                            //sb.Append("AddMessage(new BizTaxErrorDataContract { FileId=Guid.Empty,Id=1,Messages=new List<BizTaxErrorMessageDataContract> { new BizTaxErrorMessageDataContract { Message=\"").Append(vas.Id).Append("\"} } });").Append(Environment.NewLine);
                            sb.Append(CreateMessages(varList, vas.Messages, vas.Id));
                            sb.Append("}").Append(Environment.NewLine);

                            sb.Append("}").Append(Environment.NewLine);

                            //  sb.Append("}").Append(Environment.NewLine);

                            if (preTest != null)
                            {
                                sb.Append("}").Append(Environment.NewLine);
                            }
                            */

                        }

                            
                        /*   
                        }
                        else
                        {
                            
                            foreach (Variable varFact in varList.Where(v => v.Type == VariableTypes.Fact))
                            {
                                ElementSelection es = elementSelections[varFact.Name];

                                sb.Append("var ").Append(varFact.Name).Append(" = ");
                                sb.Append("GetElementsByScenDefs(new List<string> {");
                                sb.Append(string.Join(",", es.QNames.Select(q => string.Format("\"{0}\"", q)).ToArray()));
                                sb.Append("}, new List<string> { ");
                               if(calculationTarget!=null && varFact.Name== calculationTarget.Name) 
                                {
                                    calculationCreateArguments = string.Format("\"{0}\"", es.QNames.First());
                                }
                                
                                if (es.PeriodFilter != null)
                                {
                                    sb.Append("\"").Append(es.PeriodFilter.PeriodTest).Append("\"");
                                   if(calculationTarget!=null && varFact.Name== calculationTarget.Name) 
                                    {
                                        calculationCreateArguments += string.Format(",\"{0}\"", es.PeriodFilter.PeriodTest);
                                        
                                    }
                                }
                                else
                                {
                                    sb.Append(string.Join(",", es.Periods.Select(p => string.Format("\"{0}\"", p)).ToArray()));
                                   if(calculationTarget!=null && varFact.Name== calculationTarget.Name) 
                                    {
                                        calculationCreateArguments += string.Format(",\"{0}\"", es.Periods.First());
                                    }
                                }
                                sb.Append("}, new List<string> { ");
                                sb.Append(string.Join(",", es.Scenarios.Select(s => string.Format("\"{0}\"", s)).ToArray()));
                                sb.Append("});").Append(Environment.NewLine);
                               if(calculationTarget!=null && varFact.Name== calculationTarget.Name) 
                                {
                                    calculationCreateArguments += ",new List<DimensionFilter>() , new List<string> { " + string.Join(",", es.Scenarios.Select(s => string.Format("\"{0}\"", s)).ToArray()) + "}";
                                }


                            }
                            foreach (Variable varGen in genvars)
                            {
                                FormulaRenderer frVasGen2 = new FormulaRenderer();
                                frVasGen2.VariableInfos = variableInfo;
                                Statement fresVasGen2 = frVasGen2.ConvertFormula(varGen.SelectFormula.Formula);
                                sb.Append("var ").Append(varGen.Name).Append(" = ").Append(fresVasGen2.Body).Append(";").Append(Environment.NewLine);
                            }
                            sb.Append(Environment.NewLine);
                            // PRETEST
                            if (preTest != null)
                            {
                                FormulaRenderer frPreTest = new FormulaRenderer();
                                frPreTest.VariableInfos = variableInfo;
                                Statement fresPreTest = frPreTest.ConvertFormula(preTest.Formula.Formula);
                                sb.Append("if(").Append(fresPreTest.Body).Append(")").Append(Environment.NewLine);
                                sb.Append("{").Append(Environment.NewLine);
                            }
                           
                            StringBuilder sbIf = new StringBuilder();
                            foreach (Variable varf in aset.Variables.Where(v => v.Type == VariableTypes.Fact))
                            {
                                if (sbIf.Length > 0) sbIf.Append(" || ");
                                sbIf.Append(varf.Name).Append(".Count > 0");
                            }
                            sb.Append("if(").Append(sbIf.ToString()).Append(")").Append(Environment.NewLine);
                            sb.Append("{").Append(Environment.NewLine);

                            // CALCULATION
                            sb.Append("//CALCULATION HERE").Append(Environment.NewLine);
                            if (calculationTarget != null && _taxonomy.IncludedFormula)
                            {
                                sb.Append("if(").Append(calculationTarget.Name).Append(".Count==0)").Append(Environment.NewLine);
                                sb.Append("{").Append(Environment.NewLine);
                                sb.Append(calculationTarget.Name).Append(" = CreateNewCalculationElement(").Append(calculationCreateArguments).Append(");")
                                    .Append(Environment.NewLine);
                                sb.Append("}").Append(Environment.NewLine);

                                sb.Append("if(").Append(calculationTarget.Name).Append(".Count>1)").Append(Environment.NewLine);
                                sb.Append("{").Append(Environment.NewLine);
                                sb.Append("throw new Exception(\"More then one target for ").Append(calculationTarget.Name).Append(" in assertion ").Append(vas.Id).Append("\");")
                                    .Append(Environment.NewLine);
                                sb.Append("}").Append(Environment.NewLine);


                                FormulaRenderer frVas2 = new FormulaRenderer();
                                frVas2.VariableInfos = variableInfo;
                                Statement fresVas2 = frVas2.ConvertFormula(aset.Formula.Formula.Formula.Formula);


                                sb.Append(calculationTarget.Name).Append(".First().SetNumber(").Append(fresVas2.Body).Append(");")
                                    .Append(Environment.NewLine);
                            } else if (calculationTarget != null && !_taxonomy.IncludedFormula)
                            
                            {
                                sb.Append("if(").Append(calculationTarget.Name).Append(".Count==0)").Append(Environment.NewLine);
                                sb.Append("{").Append(Environment.NewLine);
                                sb.Append(calculationTarget.Name).Append(" = CreateNewCalculationElement(").Append(calculationCreateArguments).Append(");")
                                    .Append(Environment.NewLine);
                                sb.Append("}").Append(Environment.NewLine);

                                sb.Append("if(").Append(calculationTarget.Name).Append(".Count>1)").Append(Environment.NewLine);
                                sb.Append("{").Append(Environment.NewLine);
                                sb.Append("throw new Exception(\"More then one target for ").Append(calculationTarget.Name).Append(" in assertion ").Append(vas.Id).Append("\");")
                                    .Append(Environment.NewLine);
                                sb.Append("}").Append(Environment.NewLine);


                                FormulaRenderer frVas2 = new FormulaRenderer();
                                frVas2.VariableInfos = variableInfo;
                                Statement fresVas2 = frVas2.ConvertFormula(vas.Calculation.Formula);


                                sb.Append(calculationTarget.Name).Append(".First().SetNumber(").Append(fresVas2.Body).Append(");")
                                    .Append(Environment.NewLine);
                                // sb.Append(calculationTarget.Name).Append(" = new List<XbrlElementDataContract> { ").Append(

                            }
                            // FORMULA
                            sb.Append("//FORMULA HERE").Append(Environment.NewLine);
                            FormulaRenderer frVas = new FormulaRenderer();
                            frVas.VariableInfos = variableInfo;
                            Statement fresVas = frVas.ConvertFormula(vas.Formula.Formula);

                            sb.Append("bool test = ").Append(fresVas.Body).Append(";").Append(Environment.NewLine);

                            sb.Append("if (!test)").Append(Environment.NewLine);
                            sb.Append("{").Append(Environment.NewLine);
                            sb.Append("AddMessage(new BizTaxErrorDataContract { FileId=Guid.Empty,Id=1,Messages=new List<BizTaxErrorMessageDataContract> { new BizTaxErrorMessageDataContract { Message=\"").Append(vas.Id).Append("\"} } });").Append(Environment.NewLine);
                            sb.Append("}").Append(Environment.NewLine);

                            sb.Append("}").Append(Environment.NewLine);

                            if (preTest != null)
                            {
                                sb.Append("}").Append(Environment.NewLine);
                            }

                        }
                        */
                       
                        Console.WriteLine("ok");
                        sbFunctions.Append(sb.ToString());
                    }
                    
                    
                }

                

                sbFunctions.Append(Environment.NewLine).Append("\t").Append("\t").Append("}");
                sbFunctions.Append(Environment.NewLine).Append("\t").Append("\t");

            }
            template = template.Replace("<!CALCULATION_CALLS!>", sbCalls.ToString());
            template = template.Replace("<!CALCULATIONS!>", sbFunctions.ToString());
        }

        private string[] GetDimensionFiltersList(Variable var, ElementSelection varEs)
        {
            List<string> dimFilters = new List<string>();
            if (varEs.ExplicitDimensionFilters.Count > 0)
            {
                foreach (ExplicitDimensionFilter edf in varEs.ExplicitDimensionFilters)
                {
                    StringBuilder sbEdf = new StringBuilder();
                    sbEdf.Append("new DimensionFilter { Invert = ").Append(edf.Complement.ToString().ToLower());
                    sbEdf.Append(", Dimension=\"").Append(edf.Dimension).Append("\"");
                    if (edf.Members != null && edf.Members.Count > 0)
                    {
                        sbEdf.Append(", Values= new List<string> { ").Append(string.Join(",", edf.Members.Select(m => "\"" + m + "\"").ToArray())).Append(" }");
                    }
                    sbEdf.Append("}");
                    dimFilters.Add(sbEdf.ToString());
                }
            }
            if (varEs.TypedDimensionFilters.Count > 0)
            {
                foreach (TypedDimensionFilter tdf in varEs.TypedDimensionFilters)
                {
                    StringBuilder sbTdf = new StringBuilder();
                    sbTdf.Append("new DimensionFilter { Invert = ").Append(tdf.Complement.ToString().ToLower());
                    sbTdf.Append(", Dimension=\"").Append(tdf.Dimension).Append("\"");
                    sbTdf.Append("}");
                    dimFilters.Add(sbTdf.ToString());
                }
            }
            return dimFilters.ToArray();
        }

        private string CreateMessages(List<Variable> variables, List<AssertionMessage> messages,string id)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("// MESSAGES");
            sb.Append(Environment.NewLine);
            sb.Append("AddMessage(new BizTaxErrorDataContract {");
            sb.Append(Environment.NewLine);
            sb.Append("FileId = _fileId, Id = \"").Append(id).Append("\", ");
            sb.Append(Environment.NewLine);
            sb.Append("Messages = new List<BizTaxErrorMessageDataContract> { ");
            sb.Append(Environment.NewLine);
            sb.Append(" new BizTaxErrorMessageDataContract { Culture = \"nl-BE\", Message = \"");
            if (messages.Count(m => m.Language == "nl")>0)
            {
                sb.Append(messages.First(m => m.Language == "nl").FormattedMessage.Message);
            }
            sb.Append("\"}  ");
            sb.Append(Environment.NewLine);
            sb.Append(", new BizTaxErrorMessageDataContract { Culture = \"fr-FR\", Message = \"");
            if (messages.Count(m => m.Language == "fr") > 0)
            {
                sb.Append(messages.First(m => m.Language == "fr").FormattedMessage.Message);
            }
            sb.Append("\"}  ");
            sb.Append(Environment.NewLine);
            sb.Append(", new BizTaxErrorMessageDataContract { Culture = \"de-DE\", Message = \"");
            if (messages.Count(m => m.Language == "de") > 0)
            {
                sb.Append(messages.First(m => m.Language == "de").FormattedMessage.Message);
            }
            sb.Append("\"}  ");
            sb.Append(Environment.NewLine);
            sb.Append(", new BizTaxErrorMessageDataContract { Culture = \"en-US\", Message = \"");
            if (messages.Count(m => m.Language == "nl") > 0)
            {
                sb.Append(messages.First(m => m.Language == "nl").FormattedMessage.Message);
            }
            sb.Append("\"}  ");
          //  sb.Append(" ,new BizTaxErrorMessageDataContract { Culture = \"en-US\", Message = \"\"}  ");
            sb.Append(Environment.NewLine);
            sb.Append(" } ");
            sb.Append(Environment.NewLine);
            sb.Append(", Fields = new List<string>()");
            sb.Append(Environment.NewLine);
            sb.Append("}");
            sb.Append(Environment.NewLine);
            sb.Append(");");
          
            return sb.ToString();
        }

        private ResultType GetResultType(string tpe)
        {
            return GetResultType(tpe, null);
        }

        private ResultType GetResultType(string tpe, XbrlElement xeld)
        {
            tpe = tpe.ToLower().Replace("system.", "");
            switch (tpe.ToLower())
            {
                case "decimal":
                    if (xeld != null)
                    {
                        if (!string.IsNullOrEmpty(xeld.DataType.TypeCode) && xeld.DataType.TypeCode.ToLower().Contains("integer"))
                        {
                            return new ResultType { BroadResultType = BroadResultType.Numeric, DetailedResultType = DetailResultType.Integer };
                        }
                        else
                        {
                           return new ResultType { BroadResultType = BroadResultType.Numeric, DetailedResultType = DetailResultType.Number };
                        }
                    }
                    else
                    {
                        return new ResultType { BroadResultType = BroadResultType.Numeric, DetailedResultType = DetailResultType.Number };
                    }
                    break;
                case "int":
                case "int32":
                case "int16":
                case "integer":
                    return new ResultType { BroadResultType = BroadResultType.Numeric, DetailedResultType = DetailResultType.Integer };
                    break;
                case "string":
               
                    return new ResultType { BroadResultType = BroadResultType.String, DetailedResultType = DetailResultType.String };
                    break;
                case "boolean":
                case "bool":
                    return new ResultType { BroadResultType = BroadResultType.Bool, DetailedResultType = DetailResultType.Bool };
                    break;

                case "datetime":
                case "date":
                    return new ResultType { BroadResultType = BroadResultType.Date, DetailedResultType = DetailResultType.Date };
                    break;
                default:
                    Console.WriteLine(tpe);
                    break;
            }
            return null;
        }


        private string ConvertFunction(XElement el)
        {
            bool expectNumeric = false;
            if (el.Name != "function") return string.Empty;
            StringBuilder sb = new StringBuilder();
            switch (el.Attribute("name").Value)
            {
                case "yearMonthDuration":
                    sb.Append("YearMonthDuration");
                    break;
                case "dayTimeDuration":
                    sb.Append("DayTimeDuration");
                    break;
                case "date":
                    sb.Append("Date");
                    break;
                case "days-from-duration":
                    sb.Append("DaysFromDuration");
                    break;
                case "count":
                    sb.Append("Count");
                    break;
                case "not":
                    sb.Append("!");
                    break;
                case "fact-explicit-scenario-dimension-value":

                    sb.Append("FactExplicitScenarioDimensionValue");
                    break;
                case "data":

                    sb.Append("Data");
                    break;
                case "local-name-from-QName":

                    sb.Append("LocalNameFromQname");
                    break;
                case "substring":
                    sb.Append("Substring");
                    break;
                case "string-length":
                    sb.Append("StringLength");
                    break;
                case "substring-before":

                    sb.Append("SubstringBefore");
                    break;
                case "substring-after":

                    sb.Append("SubstringAfter");
                    break;
                case "QName":
                    sb.Append("QName");
                    break;
                case "max":
                    expectNumeric = true;
                    lastFormulaNumeric = true;
                    sb.Append("Max");
                    break;
                case "min":
                    expectNumeric = true;
                    lastFormulaNumeric = true;
                    sb.Append("Min");
                    break;
                case "number":
                    sb.Append("Number");
                    break;
                case "round":
                    expectNumeric = true;
                    lastFormulaNumeric = true;
                    sb.Append("Math.Round");
                    break;
                case "sum":
                    expectNumeric = true;
                    lastFormulaNumeric = true;
                    sb.Append("Sum");
                    break;
                case "string":
                    expectNumeric = false; ;
                    sb.Append("String");
                    break;
                default:
                    sb.Append(el.Attribute("name").Value);
                    break;
            }
            sb.Append("(");
            List<string> args = new List<string>();
            foreach (XElement arg in el.Elements("argument"))
            {
                args.Add(ConvertFormula(arg, expectNumeric));
            }
            if (args.Count > 0)
            {
                sb.Append(string.Join(",", args.ToArray()));
            }
            sb.Append(")");
            if (el.ElementsAfterSelf().Count() > 0 && el.ElementsAfterSelf().First().Name.LocalName == "comma")
            {
                XElement after = el.ElementsAfterSelf().First();
                after.ReplaceWith(new XElement("plus"));
            }
            return sb.ToString();
        }

        private string StartConvertFormula(XElement el)
        {
            lastFormulaNumeric = null;
            return ConvertFormula(el, false);

        }

        private string StartConvertFormula(XElement el, bool numeric)
        {
            lastFormulaNumeric = numeric;
            return ConvertFormula(el, numeric);

        }

        private string ConvertFormula(XElement el)
        {
            return ConvertFormula(el, false);
        }

        private bool IsNumeric(IEnumerable<XElement> el)
        {
            bool isnum = false;
            foreach (XElement cel in el)
            {
                isnum = isnum || IsNumeric(cel);
                if (cel.HasElements)
                {
                    isnum = isnum || IsNumeric(cel.Elements());
                }
            }
            return isnum;
        }


        private bool IsNumeric(XElement el)
        {
            if (el == null) return false;
            switch (el.Name.LocalName)
            {
                case "plus":

                case "minus":

                case "multiply":

                case "divide":

                case "mod":

                case "greaterThen":

                case "lowerThen":

                case "greaterOrEqualThen":

                case "lowerOrEqualThen":

                case "number":
                    return true;
                case "variable":
                    return numericVariables.Contains(el.Attribute("name").Value.Replace("-", "_"));

                case "function":
                    switch (el.Attribute("name").Value)
                    {
                        case "string-length":
                        case "max":
                        case "min":
                        case "round":
                        case "sum":
                            return true;
                        default:
                            return false;
                    }
                    break;
                default:
                    return false;
            }
        }

        private string ConvertFormula(XElement el, bool expectNumeric)
        {
            StringBuilder sb = new StringBuilder();
            switch (el.Name.LocalName)
            {
                case "group":
                    if (el.Elements().Count() > 0)
                    {
                        sb.Append("(");
                        foreach (XElement child in el.Elements())
                        {
                            sb.Append(ConvertFormula(child));
                            sb.Append(" ");
                        }
                        sb.Append(")");
                    }
                    return sb.ToString();
                    break;
                case "plus":
                    return " + ";
                case "minus":
                    return " - ";
                case "multiply":
                    return " * ";
                case "divide":
                    return " / ";
                case "mod":
                    return " % ";
                case "notequals":
                    return "!=";
                case "greaterThen":
                    return " > ";
                case "lowerThen":
                    return " < ";
                case "greaterOrEqualThen":
                    return " >= ";
                case "lowerOrEqualThen":
                    return " <= ";
                case "comma":
                    return ",";
                case "or":
                    return " || ";
                case "and":
                    return " && ";
                case "equals":
                    return "==";
                case "variable":
                    if (expectNumeric || (!expectNumeric && (IsNumeric(el.ElementsBeforeSelf().Take(3)) || IsNumeric(el.ElementsAfterSelf().Take(3)))))
                    {
                        return "Number(" + el.Attribute("name").Value.Replace("-", "_") + ")";
                    }
                    return el.Attribute("name").Value;
                case "string":
                    return string.Format("\"{0}\"", el.Value);
                case "nodepath":
                    string npath = el.Attribute("path").Value;
                    StringBuilder npSb = new StringBuilder();
                    string[] npathSplitted = npath.Split(new char[] { '/' });

                    for (int i = 0; i < npathSplitted.Length; i++)
                    {
                        switch (npathSplitted[i])
                        {
                            case "xbrli:xbrl":
                                break;
                            case "xbrli:context":
                                npSb.Append(".Contexts");
                                break;
                            case "xbrli:entity":
                                npSb.Append(".Select(c=>c.Entity)");
                                break;
                            case "xbrli:identifier":
                                npSb.Append(".Select(e=>e.IdentifierValue)");
                                break;
                            case "xbrli:period":
                                npSb.Append(".Select(c=>c.Period)");
                                break;
                            case "xbrli:instant":
                                npSb.Append(".Select(p=>p.Instant)");
                                break;
                            case "xbrli:startDate":
                                npSb.Append(".Select(p=>p.StartDate)");
                                break;
                            case "xbrli:endDate":
                                npSb.Append(".Select(p=>p.EndDate)");
                                break;
                            case "xbrli:unit":
                                npSb.Append(".Units");
                                break;
                            case "xbrli:measure":
                                npSb.Append(".Select(u=>u.Measure)");
                                break;
                        }
                    }
                    if (npSb.Length > 0)
                    {
                        return string.Format("Xbrl{0}.ToList()", npSb.ToString());
                    }
                    else
                    {
                        return string.Format("null /* {0} */", npath);
                    }
                    return string.Format("\"{0}\"", npath);
                case "number":
                    return "(decimal)" + el.Value;
                case "function":
                    return ConvertFunction(el);
                    break;
                case "if":
                    sb.Append(ConvertFormula(el.Element("when")));
                    sb.Append(" ? ").Append(ConvertFormula(el.Element("then")));
                    sb.Append(" : ").Append(ConvertFormula(el.Element("else")));
                    return sb.ToString();
                case "foreach":
                    string forIn = ConvertFormula(el.Element("in"));
                    string foreachvar = el.Element("each").Attribute("name").Value;
                    string forselect = ConvertFormula(el.Element("return"));
                    sb.Append(forIn.Trim()).Append(".Select(e=>e.Value).Select(").Append(foreachvar).Append("=>").Append(forselect).Append(").ToList()");
                    return sb.ToString();
                default:
                    if (el.HasElements)
                    {
                        if (el.Name.LocalName.ToLower() == "formula")
                        {
                            List<XElement> ors = el.XPathSelectElements("//*[name()='or']").ToList();
                            foreach (XElement or in ors)
                            {
                                if (IsNumeric(or.ElementsBeforeSelf().Take(1)) && IsNumeric(or.ElementsAfterSelf().Take(1)))
                                {
                                    XElement func = new XElement("function", new XAttribute("name", "NumericOr"));
                                    func.Add(new XElement("argument", or.ElementsBeforeSelf().ToList()));
                                    func.Add(new XElement("argument", or.ElementsAfterSelf().ToList()));
                                    XElement par = or.Parent;
                                    par.RemoveNodes();
                                    par.Add(func);
                                }
                            }
                        }
                        foreach (XElement child in el.Elements())
                        {
                            sb.Append(ConvertFormula(child, expectNumeric));
                            sb.Append(" ");
                        }
                        return sb.ToString();
                    }
                    break;
            }

            return string.Empty;
        }
    }
}
