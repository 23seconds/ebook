﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using EY.com.eBook.BizTaxTaxonomyParser.Contracts;
using System.IO;
using System.Text.RegularExpressions;

namespace EY.com.eBook.BizTaxTaxonomyParser.Renderers
{
    public class GuiRenderer
    {
        private Regex _keyReg = new Regex(@"\<\!(?<keyword>([A-Z]|_|-){1,})\!\>", RegexOptions.Compiled);
        private static string _classNameFormat = "eBook.BizTax.AY{0}_{1}";
        private string _className;
        private int _assessmentyear;
        private Taxonomy _taxonomy;
        private string _language;

        private StreamWriter _writer;
        private Dictionary<string, string> _templates;

        private Dictionary<string, string> _fieldTypes;
        private Dictionary<string, string> _fieldAttribType;
        private Dictionary<string, string> _valueLists;

        private void LoadTemplate()
        {
            
        }

        public GuiRenderer(Taxonomy taxonomy, int assessmentyear,string language, Dictionary<string,List<XElement>> valuelists)
        {
            _valueLists = new Dictionary<string, string>();
            _assessmentyear = assessmentyear;
            _className = string.Format(_classNameFormat, assessmentyear, taxonomy.Id);
            _taxonomy = taxonomy;
            _language = language;
            _globalKeys = new Dictionary<string, string>();
           
            _globalKeys.Add("CONTAINER_NAME", string.Format("eBook.BizTax.AY{0}_{1}", _assessmentyear, _taxonomy.Key.Replace("tax-inc-", "")));
           
            _templates = new Dictionary<string, string>();
            string searchPath = Path.Combine("",@"Templates\GUI");
            string[] files  = Directory.GetFiles(searchPath,"*.tpl",SearchOption.AllDirectories);
            foreach (string file in files)
            {
                string key = file.Replace(searchPath + @"\", "");
                StreamReader sr = new StreamReader(file);
                _templates.Add(key, sr.ReadToEnd());
                sr.Close();
            }
            RenderDatatypes(valuelists);
        }

        private void RenderDatatypes(Dictionary<string, List<XElement>> valuelists)
        {
            _fieldTypes = new Dictionary<string, string>();
            _fieldAttribType = new Dictionary<string, string>();

            foreach (string key in valuelists.Keys)
            {
                _fieldTypes.Add(key, RenderValueList(valuelists[key]));
               
            }

            foreach (string key in _taxonomy.DataTypes.Keys)
            {
                XbrlDataType xdt = _taxonomy.DataTypes[key];
                if (xdt.Used)
                {
                    RenderDatatype(_taxonomy.DataTypes[key]);
                }
             
            }
            Program.ToXml(_fieldTypes, false, "").Save("fieldtypes.xml");
            Console.WriteLine("--end datatypes--");
            //Console.ReadLine();
        }

        private void RenderDatatype(XbrlDataType xdt)
        {
            string template = "null";
            string result = "null";
            switch (xdt.RootType)
            {
                case "System.Int64":
                case "System.Int32":
                case "System.Int16":
                case "System.UInt64":
                case "System.UInt32":
                case "System.UInt16":
                case "System.Integer":
                    template = _templates[@"fieldtypes\number-fieldtype.tpl"];
                    _fieldAttribType.Add(xdt.Name, "PURE");
                    break;
                case "System.Decimal":
                    template = _templates[@"fieldtypes\number-fieldtype.tpl"];

                    if (xdt.Name.ToLower().Contains("monetary"))
                    {
                        template = _templates[@"fieldtypes\currency-fieldtype.tpl"];
                        _fieldAttribType.Add(xdt.Name, "EUR");
                    }
                    else if (xdt.Name.ToLower().Contains("percent"))
                    {
                        template = _templates[@"fieldtypes\percentage-fieldtype.tpl"];
                        _fieldAttribType.Add(xdt.Name, "EUR");
                    }
                    else
                    {
                        _fieldAttribType.Add(xdt.Name, "PURE");
                    }
                    break;
                case "System.String":
                    template = _templates[@"fieldtypes\text-fieldtype.tpl"];
                    _fieldAttribType.Add(xdt.Name, "STR");
                    break;
                case "System.Byte[]":
                    template = _templates[@"fieldtypes\upload-fieldtype.tpl"];
                    _fieldAttribType.Add(xdt.Name, "BTE");
                    break;
                case "System.DateTime":
                    template = _templates[@"fieldtypes\date-fieldtype.tpl"];
                    _fieldAttribType.Add(xdt.Name, "DTE");
                break;
                case "System.Uri":
                    template = _templates[@"fieldtypes\text-fieldtype.tpl"];
                    _fieldAttribType.Add(xdt.Name, "STR");
                break;
                case "System.Boolean":
                    template = _templates[@"fieldtypes\checkbox-fieldtype.tpl"];
                    _fieldAttribType.Add(xdt.Name, "BOOL");
                    break;
            }
            string restrictions = GetFieldRestrictions(xdt);
            if (string.IsNullOrEmpty(restrictions)) restrictions = "noRestrictions:true";

             MatchCollection mc = _keyReg.Matches(template);
             foreach (Match keyWord in mc)
             {
                 string keyWordStr = keyWord.Groups["keyword"].Value;
                 switch (keyWord.Groups["keyword"].Value)
                 {
                     case "RESTRICTIONS":
                         template = template.Replace(keyWord.Value, restrictions);
                         break;
                     case "ISREQUIRED":
                         template = template.Replace(keyWord.Value, (xdt.Name.ToLower().Contains("nonempty")).ToString().ToLower());
                         break;

                 }
             }
             _fieldTypes.Add(xdt.Name, template);



             

            Console.WriteLine("{0} : USED[{1}] base[{2}]", xdt.Name, xdt.Used,xdt.RootType);
        }

        private string RenderValueList(List<XElement> els)
        {
            string template = ""+ _templates[@"fieldtypes\render_listfield.tpl"];
            List<string> list = new List<string>();

            foreach (XElement el in els)
            {
                //"{ id: 'pfs-vl_XCode_LegalFormCode_001', name: 'XCode_LegalFormCode_001', fixed: '001', text:"));
                list.Add("{" + string.Format(" id: '{0}', name: '{1}', fixed: '{2}', text:'{3}'"
                    ,el.Attribute("value").Value
                    ,el.Attribute("name").Value
                    ,el.Attribute("fixed").Value
                    , el.Element("labels").Elements().First(e=>e.Element("Language").Value==_language).Element("Text").Value.Replace("'",@"\'")
                )+"}");

            }

            template = template.Replace("<!DATA!>", string.Join(",", list.ToArray()));
            

            return template;
        }

        public string GetFieldRestrictions(XbrlDataType xdt)
        {
            List<string> jsRestricts = new List<string>();
            foreach (Restriction restriction in xdt.Restrictions)
            {
                switch (restriction.Name)
                {
                    case "System.Xml.Schema.XmlSchemaTotalDigitsFacet":
                        jsRestricts.Add("maxLength:" + restriction.Value);
                        break;
                    case "System.Xml.Schema.XmlSchemaFractionDigitsFacet":
                        jsRestricts.Add("decimalPrecision:" + restriction.Value);
                        break;
                    case "System.Xml.Schema.XmlSchemaMinInclusiveFacet":
                        jsRestricts.Add("minValue:" + restriction.Value);
                        break;
                    case "System.Xml.Schema.XmlSchemaMaxInclusiveFacet":
                        jsRestricts.Add("maxValue:" + restriction.Value);
                        break;
                     case "System.Xml.Schema.XmlSchemaPatternFacet":
                        jsRestricts.Add("regex:new RegExp('" + restriction.Value + "')");
                        break;
                     case "System.Xml.Schema.XmlSchemaMinLengthFacet":
                        jsRestricts.Add("minLength:" + restriction.Value);
                        break;
                     case "System.Xml.Schema.XmlSchemaMaxLengthFacet":
                        jsRestricts.Add("maxLength:" + restriction.Value);
                        break;
                }
           
            }
            return string.Join(",", jsRestricts.ToArray());
        }

        private Dictionary<string, string> _globalKeys;

        private string _targetPath;

        public void Render(string targetPath, bool debug)
        {
            _targetPath = targetPath;
            Console.WriteLine("RENDERING GUI");
            if (_writer != null)
            {
                _writer.Close();
                _writer.Dispose();
            }
            _writer = System.IO.File.CreateText(Path.Combine(targetPath, string.Format("{0}-{1}.js", _taxonomy.Key,_language)));


            RenderMain();
            _writer.Close();
            Console.WriteLine("DONE RENDERING GUI");

        }

        private void RenderMain()
        {
            string template = _templates["main-module.tpl"];
            template = template.Replace("<!TABS!>", RenderTabs());
            MatchCollection mc = _keyReg.Matches(template);
            foreach (Match keyWord in mc)
            {
                string keyWordStr=keyWord.Groups["keyword"].Value;
                if(_globalKeys.ContainsKey(keyWordStr)) {
                    template= template.Replace(keyWord.Value, _globalKeys[keyWordStr]);
                }
                switch (keyWord.Groups["keyword"].Value)
                {
                   /* case "CALCULATOR_PATH":
                        template = template.Replace(keyWord.Value, "calculator.js");
                        break;
                    case "VALIDATOR_PATH":
                        template = template.Replace(keyWord.Value, "validator.js");
                        break;
                        */
                    case "VALUELISTS_CONTENT":
                       // template = template.Replace(keyWord.Value, RenderValueLists());
                        break;

                    case "FIELDTYPES":
                        List<string> ftypes = new List<string>();
                        foreach(string key in _fieldTypes.Keys) {
                            ftypes.Add("'"+key+"':" + _fieldTypes[key]);
                        }
                        template = template.Replace(keyWord.Value, string.Join(",",ftypes.ToArray()));
                        break;
                    default:
                        template = template.Replace(keyWord.Value,string.Format("/* unknown keyword: {0} */",keyWordStr));
                        break;
                }
            }
            _writer.Write(template);

            
        }

       

        private string RenderTabs()
        {
            string template = _templates["render_tabs.tpl"];
           
            List<string> result = new List<string>();

            foreach (ElementPresentationLink epl in _taxonomy.Presentation)
            {
                //StreamWriter sw = System.IO.File.CreateText(Path.Combine(_targetPath, string.Format("{0}.js", epl.Href.Replace("#","-"))));

                string tab = RenderTab(template, epl);
               // sw.Write(tab);
               // sw.Close();
                result.Add(tab);
            }
            return string.Join(",", result.ToArray());
        }

        private string RenderTab(string template, ElementPresentationLink epl)
        {
            GUIResult result = new GUIResult { Periods = new List<string>() };

            Label lbl = epl.Labels.FirstOrDefault(l=>l.Language==_language);
            if(lbl==null) lbl =epl.Labels.First();

             MatchCollection mc = _keyReg.Matches(template);
             foreach (Match keyWord in mc)
             {
                 string keyWordStr = keyWord.Groups["keyword"].Value;
                 if (_globalKeys.ContainsKey(keyWordStr))
                 {
                     template = template.Replace(keyWord.Value, _globalKeys[keyWordStr]);
                 }
                
                 switch (keyWord.Groups["keyword"].Value)
                 {
                     case "TITLE":
                         template = template.Replace(keyWord.Value, lbl.Text.Replace("'",@"\'"));
                         break;
                     case "CODE":
                         template = template.Replace(keyWord.Value,  epl.Code);
                         break;
                     case "TABURL":
                         template = template.Replace(keyWord.Value, string.Format("Biztax/{0}/{1}/{2}-{3}.html",_assessmentyear, _taxonomy.Key.Replace("tax-inc-", ""),epl.Code,_language));
                         break;
                     /*case "DISABLED":
                         template = template.Replace(keyWord.Value, "calculator.js");
                         break;
                     case "RENDER_PANES":
                         GUIResult panes = RenderPane(epl,null);
                         
                         template = template.Replace(keyWord.Value, panes.Result);
                        
                         result.Periods.AddRange(panes.Periods);
                         result.Periods = result.Periods.Distinct().ToList();
                         break;
                         
                     default:*/
                         //template = template.Replace(keyWord.Value, string.Format("/* unknown keyword: {0} */", keyWordStr));
                        //break;
                 }
                 
             }
             result.Result = template;

            return template;
        }

        private GUIResult RenderPanes(ElementPresentationLink eplParent)
        {
            GUIResult result = new GUIResult { Periods = new List<string>() };
            List<string> combResult = new List<string>();

            foreach (ElementPresentationLink epl in eplParent.Children)
            {
                GUIResult pane = RenderPane(epl, null);
                combResult.Add(pane.Result);
                result.Periods.AddRange(pane.Periods);
                result.Periods = result.Periods.Distinct().ToList();
            }
            result.Result = string.Join(",", combResult.ToArray());
            return result;
        }

        private string ApplyCommonElementKeyWords(ElementPresentationLink epl,string template)
        {
            Label lbl = epl.Labels.FirstOrDefault(l => l.Language == _language);
            if (lbl == null) lbl = epl.Labels.First();

            XbrlElement xbrlElement = _taxonomy.Elements[epl.Href];
            MatchCollection mc = _keyReg.Matches(template);
            foreach (Match keyWord in mc)
            {
                string keyWordStr = keyWord.Groups["keyword"].Value;
                if (_globalKeys.ContainsKey(keyWordStr))
                {
                    template = template.Replace(keyWord.Value, _globalKeys[keyWordStr]);
                }

                switch (keyWord.Groups["keyword"].Value)
                {
                    case "TITLE":
                        template = template.Replace(keyWord.Value, lbl.Text.Replace("'", @"\'"));
                        break;
                    case "CODE":
                        template = template.Replace(keyWord.Value, epl.Code);
                        break;
                    case "OLDCODE":
                        template = template.Replace(keyWord.Value, epl.OldCode);
                        break;
                    case "DOCUMENTATION":
                        template = template.Replace(keyWord.Value, "/*DOCUMENTATION*/");
                        break;
                    case "XBRL_ID":
                        template = template.Replace(keyWord.Value, xbrlElement.Id);
                        break;
                    case "XBRL_NAME":
                        template = template.Replace(keyWord.Value, xbrlElement.Name);
                        break;
                    case "XBRL_ITEMTYPE":
                        template = template.Replace(keyWord.Value, xbrlElement.DataType != null ? xbrlElement.DataType.Id : xbrlElement.EstimatedType);
                        break;
                    case "ABSTRACT":
                        template = template.Replace(keyWord.Value,  xbrlElement.Abstract.ToString().ToLower());
                        break;

                }
            }
            return template;
        }

        private string RenderCommonPanes(ElementPresentationLink epl,GUIResult items)
        {
            string viewtpl = "panesOnly";
            if (epl.PresentationType == ElementPresentationType.Panel || epl.PresentationType == ElementPresentationType.ContextGrid)
            {
                if (epl.Children.Count == epl.Children.Count(e => e.PresentationType == ElementPresentationType.Panel || e.PresentationType == ElementPresentationType.ContextGrid || e.PresentationType == ElementPresentationType.Pivot))
                {
                    viewtpl = "panesOnly";
                }
                else
                {
                    if (items.Periods.Contains("D") && items.Periods.Count == 1)
                    {
                        viewtpl = "duration";
                    }
                    else 
                    {
                        viewtpl = "instant";
                    }
                    

                }
            }
            else
            {
                viewtpl = "pivot";
            }
            
            string template = ApplyCommonElementKeyWords(epl, _templates[@"panes\commonpaneattributes.tpl"]);
            MatchCollection mc = _keyReg.Matches(template);
            foreach (Match keyWord in mc)
            {
                string keyWordStr = keyWord.Groups["keyword"].Value;
                if (_globalKeys.ContainsKey(keyWordStr))
                {
                    template = template.Replace(keyWord.Value, _globalKeys[keyWordStr]);
                }
                switch (keyWord.Groups["keyword"].Value)
                {
                        /*
                    case "MULTIPLECONTEXTS":
                        template = template.Replace(keyWord.Value, "false==true");
                        break;*/
                    case "VIEWTPLTYPE":
                        template = template.Replace(keyWord.Value, viewtpl);
                        break;
                
                    case "PANEFIELD":
                        if (!string.IsNullOrEmpty(epl.DataType) && epl.DataType != "ELEMENT")
                        {
                            //GUIResult gr = new GUIResult { Periods = new List<string>() };
                            template = template.Replace(keyWord.Value, RenderField(epl).Result);
                        }
                        else
                        {
                            template = template.Replace(keyWord.Value,"null");
                        }
                        break;

                }
                
                   
                
            }
            return template;
        }

        private GUIResult RenderField(ElementPresentationLink epl)
        {
            GUIResult result = new GUIResult { Periods = new List<string>() };
            
            string template = _templates[@"render_field.tpl"];

            XbrlElement xbrlElement = _taxonomy.Elements[epl.Href];
            if (xbrlElement.Abstract)
            {
                template = _templates[@"render_label.tpl"];
            }

            MatchCollection mc = _keyReg.Matches(template);
            if (epl.DataType == "ELEMENT")
            {
                result.Result = "{dataType:'ELEMENT' /*TODO*/}";
                return result;
            }
            string fieldDef = _fieldTypes[epl.DataType];
            string decimalAttrib = "null";
            string unitAttrib = "null";
            switch (_fieldAttribType[epl.DataType])
            {
                case "EUR":
                    decimalAttrib = "INF";
                    unitAttrib = "EUR";
                    break;
                case "PURE":
                    decimalAttrib = "INF";
                    unitAttrib = "PURE";
                    break;
            }

            foreach (Match keyWord in mc)
            {
                string keyWordStr = keyWord.Groups["keyword"].Value;
                if (_globalKeys.ContainsKey(keyWordStr))
                {
                    template = template.Replace(keyWord.Value, _globalKeys[keyWordStr]);
                }

                switch (keyWord.Groups["keyword"].Value)
                {
                    case "FIELD_DEFINITION":
                        template = template.Replace(keyWord.Value, fieldDef);
                        break;
                    case "CONTEXT_ID_LIST":
                        //template = template.Replace(keyWord.Value, RenderContextsIdList(epl));

                        template = template.Replace(keyWord.Value, "/* TODO */");
                        //template = template.Replace(keyWord.Value, string.Join(",", epl.Context..Select(p => string.Format("'{0}'", p)).ToArray()));
                        break;

                    case "PERIOD_ID_LIST":
                        result.Periods.AddRange(epl.PresentationPeriods);
                        template = template.Replace(keyWord.Value, string.Join(",", result.Periods.Select(p => string.Format("'{0}'", p)).ToArray()));
                        break;
                    case "COMMON_ATTRIBUTES":
                        template = template.Replace(keyWord.Value, RenderCommons(epl));
                        break;
                    case "DECIMAL_ATTRIBUTE":
                        template = template.Replace(keyWord.Value, decimalAttrib);
                        break;
                    case "UNIT_ATTRIBUTE":
                        template = template.Replace(keyWord.Value, unitAttrib);
                        break;
                    default:
                        template = template.Replace(keyWord.Value, string.Format("/* unknown keyword: {0} */", keyWordStr));
                        break;
                }

            }
            result.Result = template;
            return result;

            // return "null /*RENDER FIELD*/";
        }

        private GUIResult RenderValueList(ElementPresentationLink epl)
        {
            GUIResult result = new GUIResult { Periods = new List<string>() };
            string template = _templates[@"render_listfield.tpl"];
            string listId = epl.Href.Split(new char[] {'#'})[1];
            MatchCollection mc = _keyReg.Matches(template);
            //if (epl.DataType == "ELEMENT") return "{dataType:'ELEMENT' /*TODO*/}";

            CreateValueList(listId, epl.Children);

            foreach (Match keyWord in mc)
            {
                string keyWordStr = keyWord.Groups["keyword"].Value;
                if (_globalKeys.ContainsKey(keyWordStr))
                {
                    template = template.Replace(keyWord.Value, _globalKeys[keyWordStr]);
                }

                switch (keyWord.Groups["keyword"].Value)
                {
                    case "CONTEXT_ID_LIST":
                        //template = template.Replace(keyWord.Value, RenderContextsIdList(epl));
                        template = template.Replace(keyWord.Value, "/* TODO */");
                        //template = template.Replace(keyWord.Value, string.Join(",", epl.Context..Select(p => string.Format("'{0}'", p)).ToArray()));
                        break;
                    
                    case "PERIOD_ID_LIST":
                        result.Periods.AddRange(epl.PresentationPeriods);
                        template = template.Replace(keyWord.Value, string.Join(",", result.Periods.Select(p => string.Format("'{0}'", p)).ToArray()));
                        break;
                    case "COMMON_ATTRIBUTES":
                        template = template.Replace(keyWord.Value, RenderCommons(epl));
                        break;
                    case "LIST_ID":
                        template = template.Replace(keyWord.Value, listId);
                        break;
                    default:
                        template = template.Replace(keyWord.Value, string.Format("/* unknown keyword: {0} */", keyWordStr));
                        break;
                }

            }

            result.Result = template;

            return result;

           // return "null /*RENDER FIELD*/";
        }

        private void CreateValueList(string listId, List<ElementPresentationLink> list)
        {
           
            if (!_valueLists.ContainsKey(listId))
            { 
                List<string> items = new List<string>();
                string template =_templates["valuelist_item.tpl"];
                foreach (ElementPresentationLink epl in list)
                {
                    string result = ApplyCommonElementKeyWords(epl, template);
                    XbrlElement xel = _taxonomy.Elements[epl.Href];
                    MatchCollection mc = _keyReg.Matches(result);
                    foreach (Match keyWord in mc)
                    {
                        switch (keyWord.Groups["keyword"].Value)
                        {
                            case "ID":
                                result = result.Replace(keyWord.Value, xel.Id);
                                break;
                            case "NAME":
                                result = result.Replace(keyWord.Value, xel.Name);
                                break;
                            case "FIXED":
                                result = result.Replace(keyWord.Value, xel.Fixed);
                                break;
                            default:
                               // result = result.Replace(keyWord.Value, string.Format("/* unknown keyword: {0} */", keyWordStr));
                                break;
                        }
                    }
                    items.Add(result);
                }
                _valueLists.Add(listId, string.Join(",", items.ToArray()));
            }
        }

        private string RenderValueLists()
        {
            List<string> lists = new List<string>();
            string template = _templates["valuelists_content.tpl"];
            foreach (string key in _valueLists.Keys)
            {
                string result = template;
                MatchCollection mc = _keyReg.Matches(template);
                foreach (Match keyWord in mc)
                {
                    switch (keyWord.Groups["keyword"].Value)
                    {
                        case "ID":
                            result = result.Replace(keyWord.Value, key);
                            break;
                        case "LISTITEMS":
                            result = result.Replace(keyWord.Value, _valueLists[key]);
                            break;
                    }
                }
                lists.Add(result);
            }
            return string.Join(",", lists.ToArray());
        }

        private string RenderCommons(ElementPresentationLink epl)
        {
            string template = ApplyCommonElementKeyWords(epl, _templates[@"commonattributes.tpl"]);
            MatchCollection mc = _keyReg.Matches(template);
            foreach (Match keyWord in mc)
            {
                string keyWordStr = keyWord.Groups["keyword"].Value;
                if (_globalKeys.ContainsKey(keyWordStr))
                {
                    template = template.Replace(keyWord.Value, _globalKeys[keyWordStr]);
                }
                switch (keyWord.Groups["keyword"].Value)
                {
                    case "DISABLED":
                        template = template.Replace(keyWord.Value, "false /*MULTIPLECONTEXTS*/");
                        break;
                }
            }
            return template;
        }




        private GUIResult RenderPane(ElementPresentationLink epl, ElementPresentationType? forcePresentationType)
        {
            GUIResult result = new GUIResult { Periods = new List<string>() };
            string template = _templates[@"panes\render_defaultpane.tpl"]; ;
            if (forcePresentationType.HasValue)
            {
                epl.PresentationType = forcePresentationType.Value;
            }
            if (epl.PresentationType == ElementPresentationType.Pivot)
            {
                template = _templates[@"panes\render_pivotpane.tpl"];
                forcePresentationType = ElementPresentationType.Panel;
            }
            else if (epl.PresentationType == ElementPresentationType.ContextGrid)
            {
                template = _templates[@"panes\render_contextgrid.tpl"];
                forcePresentationType = ElementPresentationType.Panel;
            }

            GUIResult items = RenderItems(epl, forcePresentationType);
            

           // string template = _templates[@"panes\render_defaultpane.tpl"];
            
            MatchCollection mc = _keyReg.Matches(template);
            foreach (Match keyWord in mc)
            {
                string keyWordStr = keyWord.Groups["keyword"].Value;
                if (_globalKeys.ContainsKey(keyWordStr))
                {
                    template = template.Replace(keyWord.Value, _globalKeys[keyWordStr]);
                }

                switch (keyWord.Groups["keyword"].Value)
                {
                    case "FIXED_CONTEXTS":
                        template = template.Replace(keyWord.Value, RenderFixedContexts(epl));
                        break;
                    case "CONTEXT_HEADERS":
                        template = template.Replace(keyWord.Value, RenderContextHeaders(epl));
                        break;
                    case "COMMON_ATTRIBUTES":
                        template = template.Replace(keyWord.Value, RenderCommons(epl));
                        break;
                    case "COMMONPANE_ATTRIBUTES":
                        template = template.Replace(keyWord.Value, RenderCommonPanes(epl,items));
                        break;
                    case "BINARY_ONLY":
                        template = template.Replace(keyWord.Value, "false /* TO IMPLEMENT IS BINARY CHECK */");
                        break;
                    case "PANE_ITEMS":
                        template = template.Replace(keyWord.Value, items.Result);
                       // template = template.Replace(keyWord.Value, "");
                        result.Periods.AddRange(items.Periods);
                        result.Periods = result.Periods.Distinct().ToList();
                        break;
                    default:
                        template = template.Replace(keyWord.Value, string.Format("/* unknown keyword: {0} */", keyWordStr));
                        break;
                }

            }



            result.Result = template;
            return result;
        }

        private string RenderContextHeaders(ElementPresentationLink epl)
        {

            List<string> headers = new List<string>();


            foreach (Dimension dim in epl.Context.Dimensions)
            {
                foreach (Domain dom in dim.Domains)
                {
                    if (dom.DomainMembers.Count > 1)
                    {
                        foreach (DomainMember member in dom.DomainMembers)
                        {
                            headers.Add(RenderContextHeader(dim, dom, member));
                        }
                    }
                }
            }
            if (headers.Count == 0)
            {
                Dimension dim = epl.Context.Dimensions.Last();
                Domain dom = dim.Domains.Last();
                headers.Add(RenderContextHeader(dim, dom, dom.DomainMembers.Last()));
            }
            return string.Join(",", headers.ToArray());
            
        }

        private string RenderFixedContexts(ElementPresentationLink epl)
        {

            List<string> fixedContexts = new List<string>();

            if (epl.Context != null && epl.Context.Dimensions != null)
            {
                foreach (Dimension dim in epl.Context.Dimensions.Where(d => d.DimensionType == DimensionTypes.ExplicitDimension))
                {
                    if (dim.Domains != null)
                    {
                        foreach (Domain dom in dim.Domains)
                        {
                            if (dom.DomainMembers !=null && dom.DomainMembers.Count == 1)
                            {
                                foreach (DomainMember member in dom.DomainMembers)
                                {
                                    fixedContexts.Add(RenderFixedContext(dim, dom, member));
                                }
                            }
                        }
                    }
                }
            }
            return string.Join(",", fixedContexts.ToArray());

        }

        private string RenderFixedContext(Dimension dim, Domain dom, DomainMember member)
        {
            string template = _templates[@"panes\contextheader.tpl"];
            MatchCollection mc = _keyReg.Matches(template);
            foreach (Match keyWord in mc)
            {
                string keyWordStr = keyWord.Groups["keyword"].Value;
                if (_globalKeys.ContainsKey(keyWordStr))
                {
                    template = template.Replace(keyWord.Value, _globalKeys[keyWordStr]);
                }
                switch (keyWord.Groups["keyword"].Value)
                {
                    case "ID":
                        template = template.Replace(keyWord.Value, member.Id);
                        break;
                    case "NAME":
                        string nme = member.Element.LabelSets.First().Labels.First(c => c.Language == _language).Text;
                        template = template.Replace(keyWord.Value, nme);
                        break;
                    case "DOMAINMEMBER":
                        template = template.Replace(keyWord.Value, member.Href);
                        break;
                    case "DIMENSION":
                        template = template.Replace(keyWord.Value, dim.Id);
                        break;
                    case "DOMAIN":
                        template = template.Replace(keyWord.Value, dom.Id);
                        break;
                    case "DIMENSIONTYPE":
                        template = template.Replace(keyWord.Value, dim.DimensionType.ToString());
                        break;

                }



            }

            return template;
        }


        private string RenderContextHeader(Dimension dim, Domain dom, DomainMember member)
        {
            string template = _templates[@"panes\contextheader.tpl"];
            MatchCollection mc = _keyReg.Matches(template);
            foreach (Match keyWord in mc)
            {
                string keyWordStr = keyWord.Groups["keyword"].Value;
                if (_globalKeys.ContainsKey(keyWordStr))
                {
                    template = template.Replace(keyWord.Value, _globalKeys[keyWordStr]);
                }
                switch (keyWord.Groups["keyword"].Value)
                {
                    case "ID":
                        template = template.Replace(keyWord.Value, member.Id);
                        break;
                    case "NAME":
                        string nme = member.Element.LabelSets.First().Labels.First(c => c.Language == _language).Text;
                        template = template.Replace(keyWord.Value, nme);
                        break;
                    case "DOMAINMEMBER":
                        template = template.Replace(keyWord.Value, member.Href);
                        break;
                    case "DIMENSION":
                        template = template.Replace(keyWord.Value, dim.Id);
                        break;
                    case "DOMAIN":
                        template = template.Replace(keyWord.Value, dom.Id);
                        break;
                    case "DIMENSIONTYPE":
                        template = template.Replace(keyWord.Value, dim.DimensionType.ToString());
                        break;

                }



            }

            return template;
        }

        private GUIResult RenderItems(ElementPresentationLink eplParent, ElementPresentationType? forcePresentationType)
        {
            return RenderItems(eplParent.Children, forcePresentationType);
          
        }

        private GUIResult RenderItems(List<ElementPresentationLink> epls, ElementPresentationType? forcePresentationType)
        {
            GUIResult result = new GUIResult { Periods = new List<string>() };

            List<string> combresult = new List<string>();

            foreach (ElementPresentationLink epl in epls)
            {
                GUIResult item = null;
                switch (epl.PresentationType.ToString())
                {
                    case "Field":
                        item = RenderField(epl);

                        break;
                    case "ValueList":
                        item = RenderValueList(epl);

                        Console.WriteLine("VALUELIST");
                        break;
                    case "ValueOrOther":
                        item = RenderValueOrOther(epl);
                        break;
                    default:
                        item = RenderPane(epl, forcePresentationType);
                        break;
                }
                if (item != null)
                {
                    combresult.Add(item.Result);
                    result.Periods.AddRange(item.Periods);
                    result.Periods = result.Periods.Distinct().ToList();
                }

            }
            result.Result = string.Join(",", combresult.ToArray());
            return result;

        }

        private GUIResult RenderValueOrOther(ElementPresentationLink epl)
        {
            GUIResult result = new GUIResult { Periods = new List<string>() };
            string template = _templates[@"panes\render_valueorother.tpl"]; ;

            GUIResult items = RenderItems(epl, null);
            GUIResult valuelist = RenderItems(epl.Children.Where(c=>c.PresentationType== ElementPresentationType.ValueList).ToList(),null);
            GUIResult other = RenderItems(epl.Children.Where(c => c.PresentationType != ElementPresentationType.ValueList).ToList(), null);


            // string template = _templates[@"panes\render_defaultpane.tpl"];

            MatchCollection mc = _keyReg.Matches(template);
            foreach (Match keyWord in mc)
            {
                string keyWordStr = keyWord.Groups["keyword"].Value;
                if (_globalKeys.ContainsKey(keyWordStr))
                {
                    template = template.Replace(keyWord.Value, _globalKeys[keyWordStr]);
                }

                switch (keyWord.Groups["keyword"].Value)
                {
                    case "FIXED_CONTEXTS":
                        template = template.Replace(keyWord.Value, RenderFixedContexts(epl));
                        break;
                    case "CONTEXT_HEADERS":
                        template = template.Replace(keyWord.Value, RenderContextHeaders(epl));
                        break;
                    case "COMMON_ATTRIBUTES":
                        template = template.Replace(keyWord.Value, RenderCommons(epl));
                        break;
                    case "COMMONPANE_ATTRIBUTES":
                        template = template.Replace(keyWord.Value, RenderCommonPanes(epl, items));
                        break;
                    case "RENDER_VALUELIST":
                        template = template.Replace(keyWord.Value, valuelist.Result);
                        // template = template.Replace(keyWord.Value, "");
                        result.Periods.AddRange(items.Periods);
                        result.Periods = result.Periods.Distinct().ToList();
                        break;
                    case "RENDER_OTHER":
                        template = template.Replace(keyWord.Value, other.Result);
                        // template = template.Replace(keyWord.Value, "");
                        result.Periods.AddRange(items.Periods);
                        result.Periods = result.Periods.Distinct().ToList();
                        break;
                    default:
                        template = template.Replace(keyWord.Value, string.Format("/* unknown keyword: {0} */", keyWordStr));
                        break;
                }

            }



            result.Result = template;
            return result;
        }


    }
}
