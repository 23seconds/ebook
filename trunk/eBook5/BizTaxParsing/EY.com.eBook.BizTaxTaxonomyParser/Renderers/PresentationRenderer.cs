﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EY.com.eBook.BizTaxTaxonomyParser.Contracts;
using System.IO;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Text.RegularExpressions;
using System.Xml.Xsl;
using System.Xml;

namespace EY.com.eBook.BizTaxTaxonomyParser.Renderers
{
    /*
     * elementen in presentatie tree doorlopen
     * context definities groeperen
     * context definitie doorgeven
     * Enkel periode (lege dimensie) stelt alle mogelijke subdimensies (voorrang op lege?) 
     */

    public class PresentationRenderer
    {
        private List<string> _numTypes = new List<string> { "System.Decimal", "System.Double","System.Single","System.Int64","System.Int32","System.Int16","System.UInt64","System.UInt32","System.UInt16"};
        private Taxonomy _taxonomy;
        private string _exportDir;
        private string _exportHTMLDir;
        private string _xsltDir;
        private Dictionary<string, List<string>> _calcs;
        private Dictionary<string, List<XElement>> _valuelists;
        private XslCompiledTransform xslt;
        private XsltArgumentList xargsNL;
        private XsltArgumentList xargsFR;
        private XsltArgumentList xargsEN;

        private XslCompiledTransform xsltPrint;

        private List<XElement> _contextGrids = new List<XElement>();

        public PresentationRenderer(Taxonomy taxonomy, string exportDir, string exportHTMLDir,string xsltDir, Dictionary<string, List<string>> calcs)
        {
            Console.WriteLine("PRELOADING XSLTS");
            xslt = new XslCompiledTransform();
            xslt.Load(@"Templates\GUI\GuiRender.xslt", XsltSettings.TrustedXslt, null);

            xsltPrint = new XslCompiledTransform();
            XsltSettings settings = new XsltSettings
            {
                EnableDocumentFunction = true
                ,
                EnableScript = true
            };
            xsltPrint.Load(@"Templates\print\PrintTemplateRender.xslt", settings, null);


            xargsNL = new XsltArgumentList();
            xargsNL.AddParam("lang", string.Empty, "nl");
            
            xargsFR = new XsltArgumentList();
            xargsFR.AddParam("lang", string.Empty, "fr");

            xargsEN = new XsltArgumentList();
            xargsEN.AddParam("lang", string.Empty, "en");

            _calcs = calcs;
            _taxonomy = taxonomy;
            _exportDir = exportDir;
            _exportHTMLDir = exportHTMLDir;
            _xsltDir = xsltDir;
            _valuelists = new Dictionary<string, List<XElement>>();
            RenderFiches(_taxonomy.Presentation);

            
        }

        

        public Dictionary<string, List<XElement>> GetValueLists()
        {
            return _valuelists;
        }


        public int maxCols = 0;


        private XElement RenderedFiches = new XElement("FICHES");

        private void RenderFiches(List<ElementPresentationLink> list)
        {
             StreamReader sr = new StreamReader(Path.Combine("",@"Templates\GUI\BundleItem.tpl"));
             string template = sr.ReadToEnd();
             sr.Close();

             

            

             StreamWriter sw = System.IO.File.CreateText(Path.Combine(_exportDir, "bundleitems.txt"));

             Console.WriteLine("Prerendering");
             foreach (ElementPresentationLink epl in list)
             {
                 maxCols = 0;
                 XElement xeFiche = RenderFiche(epl);
                 xeFiche.Add(new XAttribute("maxcolumns", maxCols));
                 RenderedFiches.Add(new XElement("FICHE",new XAttribute("href",epl.Href),xeFiche));
             }


             Console.WriteLine("Handle reused contextgrids");
             foreach (XElement xe in _contextGrids)
             {
                 string dimset = xe.Attribute("dimensionSet").Value;
                 if (xe.XPathSelectElements("ancestor::panel[@type='contextgrid']").Count() == 0)
                 {

                     List<string> cgIds = new List<string>();
                     cgIds.Add(xe.Attribute("id").Value);
                     List<string> fields = xe.XPathSelectElements("columns//column[@type='field']").Select(x => x.Attribute("fieldid").Value).ToList();

                     Dictionary<string, List<string>> dims = GetDimensionInfos(xe);

                     List<XElement> sameCgs = RenderedFiches.XPathSelectElements(string.Format("//panel[@dimensionSet='{0}' and not(@id='{1}') and @type='contextgrid' and not(ancestor::panel[@type='contextgrid'])]", dimset, xe.Attribute("id").Value)).ToList();

                     foreach (XElement sameCg in sameCgs)
                     {

                         // check if all explicit dimensions and members match

                         

                         List<string> sfields = sameCg.XPathSelectElements("columns//column[@type='field']").Select(x => x.Attribute("fieldid").Value).ToList();

                         sfields = sfields.Intersect(fields).ToList();

                         if (sfields.Count > 0)
                         {
                             Dictionary<string, List<string>> cdims = GetDimensionInfos(sameCg);
                             bool match = true;
                             foreach (string dim in dims.Keys)
                             {
                                 if (!cdims.ContainsKey(dim))
                                 {
                                     match = false;
                                     break;
                                 }
                                 else
                                 {
                                     if (dims[dim].Intersect(cdims[dim]).Count() == 0)
                                     {
                                         match = false;
                                         break;
                                     }
                                 }
                             }
                             if (match)
                             {
                                 cgIds.Add(sameCg.Attribute("id").Value);
                                 Console.WriteLine("Found couple: {0}:{1} and {2}:{3}", xe.XPathSelectElement("ancestor::fiche").Attribute("id").Value, xe.Attribute("id").Value, sameCg.XPathSelectElement("ancestor::fiche").Attribute("id").Value, sameCg.Attribute("id").Value);
                             }
                         }
                     }
                     xe.Add(new XAttribute("cgids", string.Join(",", cgIds.ToArray())));
                     xe.Element("details").Add(new XElement("prescontextids", cgIds.Select(x => new XElement("prescontextid", x)).ToList()));
                 }

             }





             Console.WriteLine("Rendering end results");
            foreach (ElementPresentationLink epl in list)
            {
                XElement xeFiche = RenderedFiches.XPathSelectElement("FICHE[@href='" + epl.Href + "']/fiche");
                XElement vls = new XElement("lists");
                foreach (string key in xeFiche.XPathSelectElements(".//list").Where(x=>x.Attribute("id")!=null).Select(x=>x.Attribute("id").Value).ToList())
                {
                    
                    vls.Add(_valuelists[key]);
                }
                List<string> knownlst = new List<string>();
                foreach (XElement it in xeFiche.XPathSelectElements("//column/dimensionfield/list/item").ToList())
                {
                    if(!knownlst.Contains(it.Attribute("value").Value)) {
                        knownlst.Add(it.Attribute("value").Value);
                        vls.Add(it);
                    }
                }

                XsltArgumentList xargsVL = new XsltArgumentList();
                xargsVL.AddParam("valuelists", string.Empty, vls.CreateNavigator());

                string fName = Path.Combine(_exportHTMLDir,epl.Code+"-nl.html");
                string fprintName = Path.Combine(_xsltDir,epl.Code+".xslt");

                string t = "" + template;

                t = t.Replace("<!CODE!>", epl.Code);
                t = t.Replace("<!NL!>", epl.Labels.First(l => l.Language == "nl").Text);
                t = t.Replace("<!FR!>", epl.Labels.First(l => l.Language == "fr").Text);
                t = t.Replace("<!EN!>", epl.Labels.First(l => l.Language == "en").Text);
                sw.Write(t);
                sw.WriteLine(" ");
                XDocument docResult = new XDocument();

                
                using (XmlWriter xWriter = new XmlTextWriter(fprintName, Encoding.UTF8))
                {

                    xsltPrint.Transform(xeFiche.CreateReader(), xargsVL, xWriter);
                }
              //  docResult.Root.Save(fprintName);

                docResult = new XDocument();
                using (XmlWriter xwriter = docResult.CreateWriter())
                {
                    xslt.Transform(xeFiche.CreateReader(), xargsNL, xwriter);
                }
                StreamWriter writer = System.IO.File.CreateText(fName);
                docResult.Root.Save(writer, SaveOptions.None);

                fName = Path.Combine(_exportHTMLDir, epl.Code + "-fr.html");

                docResult = new XDocument();
                using (XmlWriter xwriter = docResult.CreateWriter())
                {
                    xslt.Transform(xeFiche.CreateReader(), xargsFR, xwriter);
                }
                writer = System.IO.File.CreateText(fName);
                docResult.Root.Save(writer, SaveOptions.None);

                fName = Path.Combine(_exportHTMLDir, epl.Code + "-en.html");

                docResult = new XDocument();
                using (XmlWriter xwriter = docResult.CreateWriter())
                {
                    xslt.Transform(xeFiche.CreateReader(),xargsEN, xwriter);
                }
                writer = System.IO.File.CreateText(fName);
                docResult.Root.Save(writer, SaveOptions.None);
               
            }
            sw.Close();
        }

        private Dictionary<string, List<string>> GetDimensionInfos(XElement xe)
        {
            Dictionary<string, List<string>> diminfos = new Dictionary<string, List<string>>();
            List<string> dims = xe.Element("details").Element("Scenario").XPathSelectElements("dimension[@type='ExplicitDimension']").Select(x=>x.Attribute("id").Value).Distinct().ToList();

            foreach(string dim in dims) {
                diminfos.Add(dim, xe.Element("details").Element("Scenario").XPathSelectElements("dimension[@id='" + dim + "']/dimensionfield/list/item").Select(x => x.Attribute("value").Value).Distinct().ToList());
            }

            return diminfos;
        }

        private XElement RenderFiche(ElementPresentationLink epl)
        {


            XElement xeFiche = new XElement("fiche", new XAttribute("id", epl.Code));

            XbrlElement xel = _taxonomy.Elements[epl.Href];
            AddDetails(ref xeFiche, epl, xel);
            XElement children = new XElement("children");
            if (epl.Children.Count(c => c.Children == null || c.Children.Count == 0)>0)
            {
                ElementPresentationLink eplFiche = Program.CloneObject(epl);
                eplFiche.Children = new List<ElementPresentationLink> { epl };
                children.Add(RenderChildren(eplFiche, null));
            }
            else
            {
                children.Add(RenderChildren(epl, null));
            }
            xeFiche.Add(children);


            xeFiche.Save(System.IO.Path.Combine(_exportDir, epl.Code + "_" + epl.Href.Replace("#", "--") + ".xml"));

            return xeFiche;


        }

        private List<XElement> RenderChildren(ElementPresentationLink epl,ContextDefinition edsParent)
        {
           
            List<XElement> lst = new List<XElement>();
            int childCounter = 0;
            foreach (ElementPresentationLink eplChild in epl.Children)
            {
                Console.WriteLine("presentation: " + eplChild.Href);
                childCounter++;
                ContextDefinition eds = edsParent;
                XbrlElement xel = _taxonomy.Elements[eplChild.Href];
                
                if (xel.ScenarioDefs == null)
                {
                    //eds = null;
                }
                else
                {
                    if (edsParent == null)
                    {
                        if (xel.ScenarioDefs.Count == 0) xel.ScenarioDefs.Add(new ContextDefinition { });
                        if (epl.Scenario == null && epl.Children.Count(c => c.Scenario != null) == 0)
                        {
                            eds = new ContextDefinition { };
                            epl.Children.ForEach(c =>
                            {
                                c.Scenario = new ContextDefinition { };
                            });
                        }
                        if ((epl.Scenario == null || epl.Scenario.Id == Guid.Empty.ToString()) && childCounter > 1 && epl.Children[childCounter - 1].Scenario.Id != Guid.Empty.ToString())
                        {
                            if (xel.ScenarioDefs.Count(c => c.DefId == epl.Children[childCounter - 1].Scenario.DefId) > 0)
                            {
                                eds = xel.ScenarioDefs.First(c => c.DefId == epl.Children[childCounter - 1].Scenario.DefId);
                            }
                        } else if (xel.ScenarioDefs.Count(c => string.IsNullOrEmpty(c.DefId) || c.DefId == Guid.Empty.ToString()) > 0)
                        {
                            eds = xel.ScenarioDefs.First(c => string.IsNullOrEmpty(c.DefId) || c.DefId == Guid.Empty.ToString());
                        }
                        else
                        {
                            eds = xel.ScenarioDefs.First();
                        }
                    }
                    else
                    {
                        if (epl.Scenario.Id == Guid.Empty.ToString() && childCounter > 1 && epl.Children[childCounter - 1].Scenario.DefId != epl.Scenario.DefId)
                        {
                            if (xel.ScenarioDefs.Count(c => c.DefId == epl.Children[childCounter - 1].Scenario.DefId) > 0)
                            {
                                eds = xel.ScenarioDefs.First(c => c.DefId == epl.Children[childCounter - 1].Scenario.DefId);
                            }
                        }
                        else
                        {
                            if (xel.ScenarioDefs.Count(c => c.Id == edsParent.Id) > 0)
                            {
                                eds = xel.ScenarioDefs.First(c => c.Id == edsParent.Id);
                            }
                            else
                            {
                                if (xel.ScenarioDefs.Count(c => c.DefId == edsParent.DefId) > 0)
                                {
                                    eds = xel.ScenarioDefs.First(c => c.DefId == edsParent.DefId);
                                }
                                else
                                {
                                    eds = xel.ScenarioDefs.First();
                                }
                            }
                        }
                    }
                }
                if (eds != null)
                {
                    eplChild.Scenario = Program.CloneObject(eds);
                }

                if (eplChild.Scenario != null && epl.Scenario != null && eplChild.Scenario.DefName == epl.Scenario.DefName && epl.Scenario.Scenario!=null && eplChild.Scenario.Scenario!=null)
                {
                    eplChild.Scenario.Scenario = eplChild.Scenario.Scenario.Where(s=>epl.Scenario.Scenario.Count(es=>es.Id==s.Id)>0).Distinct().ToList();
                    
                }
                eds = eplChild.Scenario;
                XElement xChild = null;



                if (eplChild.Children == null || eplChild.Children.Count == 0)
                {
                    string unit = xel.DataType.Name.ToLower().Contains("monetary") ? "EUR" : "";
                    if (string.IsNullOrEmpty(unit) && _numTypes.Contains(xel.DataType.RootType))
                    {
                        unit = "U-Pure";
                    }
                    string decimals = string.IsNullOrEmpty(unit) ? "" : "INF";

                        
                      
                    

                    
                    xChild = new XElement("field");
                    xChild.Add(new XAttribute("id", xel.Name));
                    xChild.Add(new XAttribute("ns", xel.SuspectedPrefix));
                    xChild.Add(new XAttribute("decimals", decimals));
                    xChild.Add(new XAttribute("unitref", unit));
                    if (eds != null)
                    {
                        xChild.Add(new XAttribute("dimensionSet", eds.DefName));
                    }
                    AddDetails(ref xChild, eplChild, xel);


                }
                else
                {
                    xChild = new XElement("panel");
                    xChild.Add(new XAttribute("id", xel.Name));
                    xChild.Add(new XAttribute("ns", xel.SuspectedPrefix));
                    if (eds != null)
                    {
                        xChild.Add(new XAttribute("dimensionSet", eds.DefName));
                    }
                    List<XElement> childLst = RenderChildren(eplChild, eds);

                    List<string> types = childLst.Select(c => c.Element("details").Element("Scenario").Attribute("type").Value).Distinct().ToList();
                    if (types.Contains("contextgrid") && types.Contains("grid") && childLst.Count== childLst.Count(c=>c.Name=="field"))
                    {
                        XElement xefirstCG = childLst.First(c => c.Element("details").Element("Scenario").Attribute("type").Value == "contextgrid");
                        string xcgId = xefirstCG.Attribute("id").Value;
                        ElementPresentationLink eplCG = eplChild.Children.First(c => c.Href.Contains(xcgId));
                        eplCG.Children.Add(ProtoBuf.Serializer.DeepClone(eplCG));
                        eplCG.DataType = null;
                        childLst = RenderChildren(eplChild, eds);
                        types = childLst.Select(c => c.Element("details").Element("Scenario").Attribute("type").Value).Distinct().ToList();
                    }

                    List<string> presPeriods = eplChild.Children.SelectMany(e => e.PresentationPeriods).Distinct().ToList();
                    eplChild.PresentationPeriods = presPeriods;
                    if (eplChild.PresentationPeriods.Count == 3)
                    {
                        eplChild.PresentationPeriods = eplChild.PresentationPeriods.Where(e => e.StartsWith("I-")).ToList();
                    }

                    epl.PresentationPeriods = epl.Children.SelectMany(e => e.PresentationPeriods).Distinct().ToList();
                    if (epl.PresentationPeriods.Count == 3)
                    {
                        epl.PresentationPeriods = epl.PresentationPeriods.Where(e => e.StartsWith("I-")).ToList();
                    }

                    if (eplChild.Children.Count == 1 && eplChild.Children.Count(c => c.Href == eplChild.Href) == 1)
                    {
                        xel = ProtoBuf.Serializer.DeepClone(xel);
                        xel.DataType = null;
                    }
                    AddDetails(ref xChild, eplChild, xel);



                    if (!xel.Abstract)
                    {
                        xChild.Add(new XElement("fieldDefinition"));
                    }

                    
                    xChild.Add(new XAttribute("hasFields", childLst.Count(c => c.Name.LocalName == "field") > 0));
                    

                    string mytype = "normal";

                    if (types.Count > 1)
                    {
                        if (childLst.Count(e => e.Name.LocalName == "field" && e.Element("details").Element("Scenario").Attribute("empty").Value == "true") == 0)
                        {
                            types = types.Where(t => t != "normal").ToList();
                        }
                        else
                        {
                            types = new List<string> { "normal" }; // ==mixed
                        }
                    }

                    if (types.Count < 2)
                    {

                        if (types.Count == 0)
                        {
                            xChild.Add(new XAttribute("type", "normal"));

                        }
                        else
                        {
                            /* if (types.Count == 1 && types.Contains("grid") && eplChild.Children.Count == eplChild.Children.Count(c => c.Children != null && c.Children.Count > 0))
                             {
                                 xChild.Add(new XAttribute("type", mytype));
                             }
                             else
                             {*/

                            xChild.Add(new XAttribute("type", types.First()));
                            mytype = types.First();
                            /* }*/
                        }

                        /*if (childLst.Count == childLst.Count(c => c.Name.LocalName == "panel") && types.Count < 2 && types.Contains("normal"))
                        {
                            xChild.Add(new XAttribute("type", "panelsOnly"));
                        }
                        else
                        {
                         *
                    }*/
                    }
                    else
                    {
                        mytype = types.First();
                    }


                   

                    //xel.DimenstionSets
                    XElement children = new XElement("children");
                    children.Add(childLst);

                    xChild.Add(children);

                    if (mytype != "normal" && xChild.XPathSelectElements("details/Scenario/dimension").Count() == 0)
                    {



                        if (mytype == "grid" && types.Count >= 2)
                        {
                            xChild.Element("details").Element("Scenario").Add(xChild.XPathSelectElements("children/*/details/Scenario[@type='grid']/dimension").Select(t=>t.ToString()).OrderBy(t=>t).Distinct().Select(x=>XElement.Parse(x)).ToList());
                        }
                        else
                        {
                            xChild.Element("details").Element("Scenario").Add(xChild.XPathSelectElements("children/*/details/Scenario/dimension").Distinct().ToList());
                        }
                                                                         
                    }

                    XElement xeColumns = new XElement("columns");
                    bool code = xChild.XPathSelectElements("children//details[@code]").Count() > 0;
                    bool oldcode = xChild.XPathSelectElements("children//details[@oldcode]").Count() > 0;

                    List<int> cwidths = new List<int>();

                    // gather presentation columns
                    switch (mytype)
                    {
                        case "normal":
                            xeColumns.Add(new XElement("column", new XAttribute("type", "name")));
                            cwidths.Add(44);
                            if (oldcode)
                            {
                                cwidths.Add(10);
                                xeColumns.Add(new XElement("column", new XAttribute("type", "oldcode")));
                            }
                            if (code)
                            {
                                cwidths.Add(10);
                                xeColumns.Add(new XElement("column", new XAttribute("type", "code")));
                            }
                            foreach (string period in eplChild.PresentationPeriods)
                            {
                                xeColumns.Add(new XElement("column", new XAttribute("type", "period"), new XAttribute("period",period)));
                                cwidths.Add(20);
                            }
                            xeColumns.Add(new XAttribute("depth", 1));
                            
                            break;
                        case "grid":
                            xeColumns.Add(new XElement("column", new XAttribute("type", "name")));
                            cwidths.Add(44);
                            if (oldcode)
                            {
                                xeColumns.Add(new XElement("column", new XAttribute("type", "oldcode")));
                                cwidths.Add(10);
                            }
                            if (code)
                            {
                                xeColumns.Add(new XElement("column", new XAttribute("type", "code")));
                                cwidths.Add(10);
                            }
                            xeColumns.Add(new XAttribute("depth", 1));
                            List<XElement> xdimensions = xChild.Element("details").Element("Scenario").Elements("dimension").Distinct().ToList();
                            string ignoreDimension = string.Empty;
                            if (xdimensions.Count > 1)
                            {
                                if (xdimensions.Count(d => d.Element("dimensionfield").Element("list").Elements("item").Count() == 1) > 0)
                                {
                                    ignoreDimension = xdimensions.First(d => d.Element("dimensionfield").Element("list").Elements("item").Count() == 1).Attribute("id").Value;
                                }

                                // 
                                if (xdimensions.Count() > 1 && xdimensions.Count(x => x.Attribute("id").Value != ignoreDimension) == 0)
                                {
                                    ignoreDimension = string.Empty;
                                }
                            }
                            List<string> dimsDone = new List<string>();


                            foreach (XElement xdimension in xdimensions.Where(x => x.Attribute("id").Value != ignoreDimension))
                            {
                                foreach (XElement xdommem in xdimension.Element("dimensionfield").Element("list").Elements("item"))
                                {
                                    if (!dimsDone.Contains(xdimension.Attribute("id").Value + xdommem.Attribute("value").Value))
                                    {
                                        cwidths.Add(20);
                                        xeColumns.Add(new XElement("column"
                                            , new XAttribute("type", "dimension")
                                            , new XAttribute("dimensionid", xdimension.Attribute("id").Value)
                                            , new XAttribute("domid", xdommem.Attribute("value").Value)
                                            , xdommem.Element("labels")
                                            )
                                         );
                                        dimsDone.Add(xdimension.Attribute("id").Value + xdommem.Attribute("value").Value);
                                    }
                                }
                            }
                            break;
                        case "contextgrid":
                             List<XElement> xdimensionscg = xChild.Element("details").Element("Scenario").Elements("dimension").Distinct().ToList();

                             if (xChild.Attribute("dimensionSet") == null)
                             {
                                 xChild.Add(new XAttribute("dimensionSet", string.Join("/", xdimensionscg.Select(d => d.Attribute("id").Value).Distinct().ToArray())));
                             }

                            List<string> cgdims = new List<string>();
                           // cwidths.Add(0);
                            xeColumns.Add(new XElement("column"
                                        , new XAttribute("type", "action")
                                        )
                                    );

                            foreach (XElement xdimensioncg in xdimensionscg)
                            {
                                if (!cgdims.Contains(xdimensioncg.Attribute("id").Value))
                                {
                                    cwidths.Add(30);
                                    xeColumns.Add(new XElement("column"
                                        , new XAttribute("type", "contextfield")
                                        , new XAttribute("dimensionid", xdimensioncg.Attribute("id").Value)
                                        , xdimensioncg.Element("dimensionfield")
                                        , xdimensioncg.Element("labels")
                                        )
                                    );
                                    cgdims.Add(xdimensioncg.Attribute("id").Value);
                                }
                            }

                            int depth = AddContextGridColumns(ref xeColumns, childLst, ref cwidths);



                            xeColumns.Add(new XAttribute("depth", depth));
                            break;
                    }

                    int colCount =  xeColumns.XPathSelectElements("column").Count();
                    colCount -= xeColumns.XPathSelectElements("column[./column]").Count();
                    colCount += xeColumns.XPathSelectElements("column/column").Count();
                    colCount -=  xeColumns.XPathSelectElements("column/column[./column]").Count();
                    colCount += xeColumns.XPathSelectElements("column/column/column").Count();
                    xeColumns.Add(new XAttribute("count",colCount));
                    xChild.Add(new XAttribute("columncount", mytype == "contextgrid" ? colCount - 1 : colCount));
                    xChild.Add(new XAttribute("columnwidths", string.Join(";", cwidths.Select(c => c.ToString()).ToArray())));

                    if (maxCols < colCount) maxCols = colCount;

                    
                    List<string> cols = xChild.XPathSelectElements("children/panel/columns").ToList().Select(t => t.ToString()).ToList();
                    if (cols.Distinct().Count() == 1 && xChild.XPathSelectElements("children/panel[@type='grid']").Count() == xChild.XPathSelectElements("children/panel").Count())
                    {
                        xChild.Attribute("type").Value = "grid";
                        xChild.Add(XElement.Parse(cols.First()));
                    }
                    else
                    {

                        xChild.Add(xeColumns);
                    }


                    if (xel.Name.EndsWith("CodeHead") && eplChild.Children.Count == eplChild.Children.Count(c => c.Href.Contains("XCode_")))
                    {
                        // transform to value list
                        XElement vlx = new XElement("field", new XAttribute("fieldtype","valuelist"), 
                            xChild.Attributes(),
                            new XElement("list", new XAttribute("id", xel.Name)),
                            xChild.Element("details"));
                        
                        if (!_valuelists.ContainsKey(xel.Name))
                        {
                            List<XElement> vlIts = new List<XElement>();
                            xChild.Element("children").Elements().ToList().ForEach(x =>
                            {
                                string[] addendums = x.Attribute("id").Value.Split(new char[] { '_' });

                                XElement vlItem = new XElement("item",
                                    new XAttribute("value", string.Format("{0}:{1}",x.Attribute("ns").Value, x.Attribute("id").Value))
                                    , new XAttribute("name", x.Attribute("id").Value)
                                    , new XAttribute("fixed", addendums[addendums.Length-1])
                                    , x.Element("details").Element("labels"), x.Element("details").Element("periods")
                                    );
                                vlItem.Element("labels").Elements().ToList().ForEach(l =>
                                {
                                    l.Element("Text").Value = string.Format("{0} ({1})", l.Element("Text").Value, addendums[addendums.Length - 1]);
                                });
                                vlIts.Add(vlItem);

                            });
                            _valuelists.Add(xel.Name, vlIts);
                        }
                        xChild = vlx;
                    }

                    if (xChild.XPathSelectElements("children/*").Count() == 2 && xChild.XPathSelectElements("children/field[@fieldtype='valuelist']").Count() == 1 && xChild.XPathSelectElements("children/field[not(@fieldtype)]").Count() == 1)
                    {

                       // xChild.Attribute("type").Value = "valuelistOrOther";

                        XElement xValOrOther = new XElement("valueListOrOther", xChild.Attributes(), xChild.Element("details"));
                        XElement xval = new XElement("valuelist", xChild.XPathSelectElements("children/field[@fieldtype='valuelist']").First());
                        XElement xother = new XElement("other", xChild.XPathSelectElements("children/field[not(@fieldtype)]").ToList());
                        xValOrOther.Add(xval);
                        xValOrOther.Add(xother);
                        xChild.Element("children").RemoveAll();
                        xChild.Element("children").Add(xValOrOther);
                    }
                    if (xChild.XPathSelectElements("children/*").Count() == 2 && xChild.XPathSelectElements("children/field[@fieldtype='valuelist']").Count() == 1 && xChild.XPathSelectElements("children/panel[@id='PostalCodeCityOther']").Count() == 1)
                    {
                        XElement xValOrOther = new XElement("valueListOrOther", xChild.Attributes(), xChild.Element("details"));
                        XElement xval = new XElement("valuelist", xChild.XPathSelectElements("children/field[@fieldtype='valuelist']").First());
                        XElement xother = new XElement("other", xChild.XPathSelectElements("children/panel/children/field").ToList());
                        xValOrOther.Add(xval);
                        xValOrOther.Add(xother);
                        xChild.Element("children").RemoveAll();
                        xChild.Element("children").Add(xValOrOther);
                    }

                    /*
                    if (eplChild.Children.Count == 2 && eplChild.Children.Count(c => c.Children != null && c.Children.Count > 0) == 1 && eplChild.Children.Count(c => c.Children == null || c.Children.Count == 0) == 1)
                    {

                    }*/

                    if (xChild.Attribute("type")!=null && xChild.Attribute("type").Value == "contextgrid")
                    {
                        _contextGrids.Add(xChild);
                    } 
                }

                xChild.Add(new XAttribute("abstract", xel.Abstract));
                xChild.Add(new XAttribute("istuple", xel.SubstitutionGroup == "http://www.xbrl.org/2003/instance:tuple"));
                bool calced = false;

                if (_calcs.ContainsKey(string.Format("{0}:{1}", xel.SuspectedPrefix, xel.Name)))
                {
                    List<string> posCalcs = _calcs[string.Format("{0}:{1}", xel.SuspectedPrefix, xel.Name)];
                   // eplChild.Scenario.DefId
                    string dset = (xChild.Attribute("dimensionSet") == null) ? Guid.Empty.ToString() : xChild.Attribute("dimensionSet").Value;
                     
                    if (eplChild.Scenario != null && eplChild.Scenario.Scenario!=null)
                    {
                        calced = eplChild.Scenario.Scenario.Count(s => posCalcs.Contains(s.DefValue)) > 0;
                    }
                    else
                    {
                        calced = posCalcs.Contains("");
                    }
                }
                    //Dictionary<string,List<string>>
                xChild.Add(new XAttribute("calculated",calced));

                

                lst.Add(xChild);
            }
            

            

            return lst;
        }

        private int AddContextGridColumns(ref XElement xeColumns, List<XElement> children, ref List<int> cwidths)
        {
            int depth = 1;
            foreach (XElement child in children)
            {
                if (child.Name.LocalName == "field")
                {
                    List<string> periods = child.Element("details").Element("presperiods").Elements("string").Select(x => x.Value).ToList();
                    XElement xeColumn = new XElement("column", new XAttribute("type", "field"));
                    xeColumn.Add(new XAttribute("fieldid",child.Attribute("id").Value));
                    if (periods.Count > 1)
                    {
                        if (depth < 2) depth = 2;
                        xeColumn.Add(new XAttribute("period", "multiple"));
                        foreach (string period in periods)
                        {
                            cwidths.Add(20);
                            xeColumn.Add(new XElement("column", new XAttribute("type", "period"), new XAttribute("period", period)));
                        }

                    }
                    else
                    {
                        cwidths.Add(20);
                        xeColumn.Add(new XAttribute("period", periods.First()));
                    }
                    xeColumn.Add(child.Element("details").Element("labels"));
                    xeColumns.Add(xeColumn);
                }
                else if (child.Name.LocalName == "panel")
                {
                    XElement xePanelColumn = new XElement("column", new XAttribute("type", "panel"));
                    xePanelColumn.Add(child.Element("details").Element("labels"));
                    int d = 1 + AddContextGridColumns(ref xePanelColumn, child.Element("children").Elements().ToList(),ref cwidths);
                    if (d > depth) depth = d;
                    xeColumns.Add(xePanelColumn);
                }
            }

            return depth;
        }

        private static Regex reSquares = new Regex("\\[.*\\]", RegexOptions.IgnoreCase);

        private void AddDetails(ref XElement element, ElementPresentationLink epl, XbrlElement xel)
        {
            XElement details = new XElement("details");
            if (!string.IsNullOrEmpty(epl.Code)) details.Add(new XAttribute("code", epl.Code));
            if (!string.IsNullOrEmpty(epl.OldCode)) details.Add(new XAttribute("oldcode", epl.OldCode));
            epl.Labels.Add(new Label
            {
                Language = "en"
                ,
                Text = string.Join(" ", SplitOnCapitals(xel.Name).ToArray())
            });
            if (xel.DataType!=null)
            {
                details.Add(new XElement("fieldType", xel.DataType.Name));
            }

            string context = "";

            XElement labels = Program.ToXml(epl.Labels, true, null);
            details.Add(new XElement("labels", labels.Elements()));
            XElement presPeriods = Program.ToXml(epl.PresentationPeriods, true, null);
            details.Add(new XElement("presperiods", presPeriods.Elements()));
            XElement periods = Program.ToXml(xel.Periods, true, null);
            details.Add(new XElement("periods", periods.Elements()));
            XElement xscen = new XElement("Scenario");
            if (epl.Scenario != null && epl.Scenario.Scenario!=null && epl.Scenario.Scenario.Count>0)
            {
                Dictionary<string, Dimension> dimensions = new Dictionary<string, Dimension>();
                foreach (ContextDimension cd in epl.Scenario.Scenario.SelectMany(s=>s.Dimensions))
                {
                    Dimension dim = null;
                    if (!dimensions.ContainsKey(cd.XbrlName))
                    {
                        dim = new Dimension
                        {
                            DataType = cd.DataType
                            ,
                            DefRole = cd.DefRole
                            ,
                            DimensionType = cd.DimensionType
                            ,
                            Href = cd.Href
                            ,
                            Id = cd.Id
                            ,
                            Name = cd.XbrlName
                            ,
                            Domains = new List<Domain>()
                        };
                        dimensions.Add(cd.XbrlName, dim);
                    }
                    dim = dimensions[cd.XbrlName];
                    if (cd.Domain != null)
                    {
                        Domain dom = dim.Domains.FirstOrDefault(d => d.Name == cd.Domain.XbrlName);
                        if (dom == null)
                        {
                            dom = new Domain
                            {
                                DomainMembers = new List<DomainMember>()
                                ,
                                Name = cd.Domain.XbrlName
                                ,
                                Id = cd.Href
                            };
                            dim.Domains.Add(dom);
                        }
                        DomainMember dm = dom.DomainMembers.FirstOrDefault(d => d.Name == cd.Domain.DomainMember.XbrlName);
                        if (dm == null)
                        {
                            dm = new DomainMember
                            {
                                Href = cd.Domain.DomainMember.Href
                                ,
                                Name = cd.Domain.DomainMember.XbrlName
                                ,
                                Element = cd.Domain.DomainMember.Element
                                ,
                                Id = cd.Domain.DomainMember.Id
                            };
                            dom.DomainMembers.Add(dm);
                        }
                    }

                }
                xscen.Add(new XAttribute("empty", false));
               
               // List<ContextDimension> dimensions = epl.Scenario.Scenario.SelectMany(d => d.Dimensions).ToList();
                if (dimensions.Values.Count(d => d.DimensionType == DimensionTypes.TypedDimension) > 0)
                {
                    xscen.Add(new XAttribute("type", "contextgrid"));

                    
                    foreach (Dimension dim in dimensions.Values)
                    {
                       // ContextDimension cd = dimensions.First(d => d.Href == dim);
                        XbrlElement xdel = _taxonomy.Elements[dim.Href];
                        XElement xdim = new XElement("dimension", 
                            new XAttribute("id",dim.Name),
                            new XAttribute("type", dim.DimensionType));
                        XElement xeField = new XElement("dimensionfield");
                        if (dim.DimensionType == DimensionTypes.ExplicitDimension)
                        {
                            xeField.Add(new XAttribute("type", "select"));
                            
                            XElement xeList = new XElement("list");
                            dim.Domains.First().DomainMembers.ForEach(m=> {
                                XbrlElement memXel = _taxonomy.Elements[m.Href];
                                xeList.Add(new XElement("item",new XAttribute("value",m.Name), new XElement("labels", Program.ToXml(memXel.LabelSets.First().Labels,true,null).Elements())));
                            });
                            xeField.Add(xeList);
                        }
                        else
                        {
                            //xdel.TypedDomainRef
                            xeField.Add(new XAttribute("type", xdel.DataType.TypeCode));
                            xeField.Add(Program.ToXml(xdel.DataType, true, null));
                        }
                        xdim.Add(xeField);
                        xdel.LabelSets.First().Labels.ForEach(l => { l.Text = reSquares.Replace(l.Text,"").Trim(); });
                        xdim.Add(new XElement("labels",Program.ToXml(xdel.LabelSets.First().Labels,true,null).Elements()));
                        xscen.Add(xdim);
                    }

                }
                else
                {
                    Dictionary<string,List<string>> dimIds = epl.Scenario.Scenario.SelectMany(s => s.Dimensions).Where(d => d.DimensionType == DimensionTypes.ExplicitDimension).GroupBy(d => d.Href).ToDictionary(d => d.Key, d => d.Select(a=>a.Id).Distinct().ToList());


                    if (dimensions.Values.Count(c => c.Domains.First().DomainMembers.Count > 1) > 1)
                    {
                        xscen.Add(new XAttribute("type", "contextgrid"));
                         foreach (Dimension dim in dimensions.Values)
                    {
                       // ContextDimension cd = dimensions.First(d => d.Href == dim);
                        XbrlElement xdel = _taxonomy.Elements[dim.Href];
                        XElement xdim = new XElement("dimension", 
                            new XAttribute("id",dim.Name),
                            new XAttribute("type", dim.DimensionType));
                        XElement xeField = new XElement("dimensionfield");
                        if (dim.DimensionType == DimensionTypes.ExplicitDimension)
                        {
                            xeField.Add(new XAttribute("type", "select"));
                            
                            XElement xeList = new XElement("list");
                            dim.Domains.First().DomainMembers.ForEach(m=> {
                                XbrlElement memXel = _taxonomy.Elements[m.Href];
                                xeList.Add(new XElement("item",new XAttribute("value",m.Name), new XElement("labels", Program.ToXml(memXel.LabelSets.First().Labels,true,null).Elements())));
                            });
                            xeField.Add(xeList);
                        }
                        else
                        {
                            //xdel.TypedDomainRef
                            xeField.Add(new XAttribute("type", xdel.DataType.TypeCode));
                            xeField.Add(Program.ToXml(xdel.DataType, true, null));
                        }
                        xdim.Add(xeField);
                        xdel.LabelSets.First().Labels.ForEach(l => { l.Text = reSquares.Replace(l.Text,"").Trim(); });
                        xdim.Add(new XElement("labels",Program.ToXml(xdel.LabelSets.First().Labels,true,null).Elements()));
                        xscen.Add(xdim);
                    }
                    }
                    else
                    {
                        xscen.Add(new XAttribute("type", "grid"));
                         foreach (Dimension dim in dimensions.Values)
                    {
                       // ContextDimension cd = dimensions.First(d => d.Href == dim);
                        XbrlElement xdel = _taxonomy.Elements[dim.Href];
                        XElement xdim = new XElement("dimension", 
                            new XAttribute("id",dim.Name),
                            new XAttribute("type", dim.DimensionType));
                        XElement xeField = new XElement("dimensionfield");
                        if (dim.DimensionType == DimensionTypes.ExplicitDimension)
                        {
                            xeField.Add(new XAttribute("type", "select"));
                            
                            XElement xeList = new XElement("list");
                            dim.Domains.First().DomainMembers.ForEach(m=> {
                                XbrlElement memXel = _taxonomy.Elements[m.Href];
                                xeList.Add(new XElement("item",new XAttribute("value",m.Name), new XAttribute("name",m.Element.Name), new XElement("labels", Program.ToXml(memXel.LabelSets.First().Labels,true,null).Elements())));
                            });
                            xeField.Add(xeList);
                            
                        }
                        else
                        {
                            //xdel.TypedDomainRef
                            xeField.Add(new XAttribute("type", xdel.DataType.TypeCode));
                            xeField.Add(Program.ToXml(xdel.DataType, true, null));
                        }
                        xdim.Add(xeField);
                        xdel.LabelSets.First().Labels.ForEach(l => { l.Text = reSquares.Replace(l.Text,"").Trim(); });
                        xdim.Add(new XElement("labels",Program.ToXml(xdel.LabelSets.First().Labels,true,null).Elements()));
                        xscen.Add(xdim);
                    }

                    }


                    
                  //  List<string> dimIds = dims.Select(d => d.Href).Distinct().ToList();

                    

                    
                }
                
            }
            else
            {
                xscen.Add(new XAttribute("empty", true));
                xscen.Add(new XAttribute("type", "normal"));
            }
            /*
            if(epl.Scenario!=null) details.Add(Program.ToXml(epl.Scenario, true, null));
            if (epl.Scenario != null)
            {
                details.Add(new XAttribute("defid", epl.Scenario.DefId));
            }*/

            
            details.Add(xscen);
            element.Add(details);
            
            
        }

        public static IEnumerable<string> SplitOnCapitals(string text)
        {
            Regex regex = new Regex(@"\p{Lu}\p{Ll}*");
            foreach (Match match in regex.Matches(text))
            {
                yield return match.Value;
            }
        }
    }
}
