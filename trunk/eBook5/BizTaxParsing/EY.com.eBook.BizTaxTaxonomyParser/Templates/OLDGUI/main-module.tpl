﻿<!CONTAINER_NAME!> = {
      Calculator:'<!CALCULATOR_PATH!>'
      ,Validator:'<!VALIDATOR_PATH!>'
      ,Lists: {<!VALUELISTS_CONTENT!>}
};

<!CONTAINER_NAME!>.Items = [
	<!RENDER_TABS!>
];