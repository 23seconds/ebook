﻿{xtype:'biztax-field'
	, <!COMMON_ATTRIBUTES!>
	, fieldDefinition:<!FIELD_DEFINITION!>
	, contexts:[<!CONTEXT_ID_LIST!>]
    , periods:[<!PERIOD_ID_LIST!>]
    , decimalAttribute:'<!DECIMAL_ATTRIBUTE!>'
    , unitAttribute:'<!UNIT_ATTRIBUTE!>'
}
    