﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl"
                
>
  <xsl:output method="text" indent="yes"/>

  <xsl:param name="assess">2012</xsl:param>
  <xsl:param name="culture">fr</xsl:param>
  <xsl:param name="target">nrcorp</xsl:param>
  <xsl:variable name="smallcase" select="'abcdefghijklmnopqrstuvwxyz'" />
  <xsl:variable name="uppercase" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZ'" />
  <xsl:variable name="_crlf"><xsl:text>
</xsl:text></xsl:variable>
  <xsl:variable name="crlf" select="string($_crlf)"/>
  <xsl:variable name="jscontainer">eBook.BizTax.AY<xsl:value-of select="$assess"/>_<xsl:value-of select="$target"/></xsl:variable>
  
  
  <xsl:template match="root">
    <xsl:apply-templates select="Presentation"/>
  </xsl:template>
  
    <xsl:template match="Presentation">
// FIND OUT WHICH (rcorp/nrcorp/rle)

<xsl:value-of select="$jscontainer"/> = {
      Calculator:'js/Taxonomy/BizTax-calc-<xsl:value-of select="$target"/>-<xsl:value-of select="$assess"/>-<xsl:value-of select="$culture"/>.js'
      ,Lists: {
        <xsl:for-each select="//ValueLists/List">
          <xsl:if test="position()>1">,</xsl:if>'<xsl:value-of select="@id"/>':[<xsl:for-each select="ListItem"><xsl:if test="position()>1">,</xsl:if>{ id:'<xsl:value-of select="@xbrlId"/>',name:'<xsl:value-of select="@xbrlName"/>',fixed:'<xsl:value-of select="@Fixed"/>',text:'<xsl:call-template name="ClearOutApos"><xsl:with-param name="txt" select="labels/label/translation[@lang=$culture]"/></xsl:call-template>&#160;(<xsl:value-of select="@Fixed"/>)'}</xsl:for-each>]
        </xsl:for-each>
      }
};

<xsl:value-of select="$jscontainer"/>.Items=[
      <xsl:for-each select="Tab[@hidden='false']">
        <xsl:if test="position()>1">,</xsl:if>
        <xsl:apply-templates select="."/>
      </xsl:for-each>
      ];

    </xsl:template>

  <xsl:template match="Tab">
    { xtype:'biztax-tab'
      ,title:'<xsl:apply-templates select="." mode="GetLabel"><xsl:with-param name="type">label</xsl:with-param></xsl:apply-templates>'
      ,code:'<xsl:apply-templates select="." mode="GetCode"/>'
    <!--<xsl:apply-templates select="." mode="DEFAULT_ATTRIBS"/>//-->
      , items:[
        <xsl:for-each select="Pane[@hidden='false']|Field[@hidden='false']">
          <xsl:if test="position()>1">,</xsl:if>
          <xsl:apply-templates select="."/>
        </xsl:for-each>]
        <xsl:variable name="id" select="@xbrlId"/>
    <xsl:variable name="lockitem" select="//MetaData/itemlocks/itemlock[@id=$id]"/>
    <xsl:if test="$lockitem">
      <xsl:choose>
        <xsl:when test="$lockitem/@locktype='always'">, elementsDisabled:true</xsl:when>
        <xsl:otherwise>, elementsDisabledFn:function() { return eBook.Interface.hasService('<xsl:value-of select="$lockitem/service/@id"/>'); } </xsl:otherwise>
      </xsl:choose>
    </xsl:if>
    }
  </xsl:template>

  <xsl:template match="Pane">
    <xsl:choose>
      <xsl:when test="@paneType='contextGrid'">
        <xsl:apply-templates select="." mode="ContextGrid"/>
      </xsl:when>
      <xsl:when test="@paneType='pivot'">
        <xsl:apply-templates select="." mode="Pivot"/>
      </xsl:when>
      <xsl:when test="@paneType='ListOrOther'">
        <xsl:apply-templates select="." mode="ListOrOther"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:apply-templates select="." mode="Normal"/>
      </xsl:otherwise>
      <!--xsl:when test="count(Field)>0">
        <xsl:apply-templates select="." mode="ValuePane"/>
      </xsl:when-->
    </xsl:choose>
  </xsl:template>

  <xsl:template match="Pane" mode="ListOrOther">
    { xtype:'biztax-listorother'
    <xsl:apply-templates select="." mode="CommonPaneAttribs"/>
      ,valueList:<xsl:apply-templates select="ValueList"/>
      ,otherField:<xsl:apply-templates select="Field|Pane"/>
    }
  </xsl:template>
  
  <xsl:template match="Pane" mode="Normal">
    { xtype:'biztax-pane'
      <xsl:apply-templates select="." mode="CommonPaneAttribs"/>
    <xsl:variable name="binFields" select="count(Field[contains(@itemType,'BinaryItemType')])"/>
    <xsl:variable name="labelFields" select="count(Field[@isAbstract='true'])"/>
    ,isBinaryPane:<xsl:value-of select="count(Field) = ($binFields+$labelFields) and $binFields>0"/>
      ,items:[
        <xsl:for-each select="Pane[@hidden='false']|Field[@hidden='false']|ValueList[@hidden='false']">
          <xsl:if test="position()>1">,</xsl:if>
          <xsl:apply-templates select=".">
          </xsl:apply-templates>
        </xsl:for-each>
      ]
    }
  </xsl:template>

  <xsl:template match="Pane" mode="Pivot">
    { xtype:'biztax-pane-pivot'
      <xsl:apply-templates select="." mode="CommonPaneAttribs"/>
      , contextHeaders:[<xsl:apply-templates select="." mode="PivotContextHeaders"/>] 
     // , contexts:[]
      ,items:[
        <xsl:for-each select="Pane|Field">
          <xsl:if test="position()>1">,</xsl:if>
          <xsl:choose>
            <xsl:when test="local-name()='Pane'">
              <xsl:apply-templates select="." mode="PivotSubPane"/>
            </xsl:when>
            <xsl:when test="local-name()='Field'">
              <xsl:apply-templates select="." mode="PivotField"/>
            </xsl:when>
            <xsl:otherwise>
              hunk
            </xsl:otherwise>
          </xsl:choose>
        </xsl:for-each>
      ]
    }
  </xsl:template>

  <xsl:template match="Pane" mode="PivotSubPane">
    { xtype:'biztax-pane'
    <xsl:apply-templates select="." mode="CommonPaneAttribs"/>
    ,items:[
    <xsl:for-each select="Pane[@hidden='false']|Field[@hidden='false']|ValueList[@hidden='false']">
      <xsl:if test="position()>1">,</xsl:if>
      <xsl:choose>
        <xsl:when test="local-name()='Pane'">
          <xsl:apply-templates select="." mode="PivotSubPane"/>
        </xsl:when>
        <xsl:when test="local-name()='Field'">
          <xsl:apply-templates select="." mode="PivotField"/>
        </xsl:when>
        <xsl:otherwise>
          hunk
        </xsl:otherwise>
      </xsl:choose>
    </xsl:for-each>
    ]
    }
  </xsl:template>
  
  <xsl:template match="Field" mode="PivotField">
    {xtype:'<xsl:choose><xsl:when test="@isAbstract='true'">biztax-label</xsl:when><xsl:otherwise>biztax-field</xsl:otherwise></xsl:choose>'
      <xsl:apply-templates select="." mode="CommonFieldAttribs"/>
      <xsl:if test="not(@isAbstract='true')">,fieldType:<xsl:apply-templates select="." mode="GetFieldType"/></xsl:if>
      , contexts:[
        <xsl:for-each select="Context/ContextDefinition/Fields/FieldDefinition/TypeElement/Id">
          <xsl:if test="position()>1">,</xsl:if>'<xsl:value-of select="."/>'
        </xsl:for-each>
      ]
    }      
  </xsl:template>

  <xsl:template match="Pane" mode="PivotContextHeaders">
    <xsl:for-each select="PivotHeaders/FieldDefinition">
      <xsl:if test="position()>1">,</xsl:if>{id:'<xsl:value-of select="TypeElement/Id"/>'
        ,name:'<xsl:value-of select="TypeElement/Labels/ElementLabel/Descriptions/Translation[Language=$culture]/Description"/>'
        ,dimension:'<xsl:value-of select="Dimension"/>'
        ,domain:'<xsl:value-of select="ListData/FieldListItem[1]/Domain"/>'
        ,domainMember:'<xsl:value-of select="ListData/FieldListItem[1]/DomainMember"/>'
        ,dimensionType:'<xsl:value-of select="DimensionType"/>'
      }
    </xsl:for-each>
  </xsl:template>

  <xsl:template match="Pane" mode="ContextGrid">
    { xtype:'biztax-contextgrid'
      <xsl:apply-templates select="." mode="CommonPaneAttribs"/>
      //create TPL
      , xtplMain:[<xsl:apply-templates select="." mode="RenderContextTplMain"/>]
      , xtplLine:[<xsl:apply-templates select="." mode="RenderContextTplLine"/>]
      , contextFields: [<xsl:for-each select="ElementContextDef/FieldDefinition"><xsl:if test="position()>1">,</xsl:if><xsl:apply-templates select="."/></xsl:for-each>]
      , lineFields:[<xsl:for-each select=".//Field"><xsl:if test="position()>1">,</xsl:if><xsl:apply-templates select="."/></xsl:for-each>
      ]
     <!-- , items:[
        <xsl:for-each select="Pane|Field|ValueList">
          <xsl:if test="position()>1">,</xsl:if>
          <xsl:apply-templates select=".">
            <xsl:with-param name="parentPane">contextgrid</xsl:with-param>
          </xsl:apply-templates>
        </xsl:for-each>
      ]-->
    }
  </xsl:template>

  <xsl:template match="FieldDefinition" mode="GetContextLabel"><xsl:call-template name="ClearOutApos"><xsl:with-param name="txt" select="./ArrayOfElementLabel/ElementLabel/Descriptions/Translation[Language=$culture]/Description"/></xsl:call-template></xsl:template>
  
  <xsl:template match="Pane" mode="RenderContextTplMain">
    <xsl:variable name="hasgroups" select="Field[Context/period[@start='true' and @end='true']]|Pane"/>
    '&lt;tpl for="."&gt;'
          ,'&lt;table cellspacing="0" border="0" colspan="0" width="100%"&gt;'
          ,'&lt;thead&gt;'
              ,'&lt;tr&gt; '
              <xsl:for-each select="ElementContextDef/FieldDefinition">,'&lt;th<xsl:if test="$hasgroups"> rowspan="2"</xsl:if> class="biztax-context-single-head"&gt;<xsl:apply-templates mode="GetContextLabel" select="."/>*&lt;/th&gt;'</xsl:for-each>
              <xsl:for-each select="Field|Pane">,'&lt;th<xsl:choose><xsl:when test="Context/period[@start='true' and @end='true'] and local-name()='Field'"> colspan="2"</xsl:when><xsl:when test="local-name()='Pane'"> colspan="<xsl:value-of select="count(Field)"/>"</xsl:when><xsl:when test="$hasgroups"> rowspan="2" class="biztax-context-single-head"</xsl:when><xsl:otherwise> class="biztax-context-single-head"</xsl:otherwise></xsl:choose>&gt;<xsl:apply-templates select="." mode="GetLabel"><xsl:with-param name="type" select="'label'"/></xsl:apply-templates>&lt;/th&gt;'</xsl:for-each>
              ,'&lt;/tr&gt;'<xsl:if test="$hasgroups">
              ,'&lt;tr&gt;'
              <xsl:for-each select="Field[Context/period[@start='true' and @end='true']]|Pane">
              <xsl:choose><xsl:when test="local-name()='Field'">,'&lt;th class="biztax-context-single-head"&gt;{[eBook.BizTax.Trans_ISTART]}&lt;/th&gt;'
              ,'&lt;th class="biztax-context-single-head"&gt;{[eBook.BizTax.Trans_IEND]}&lt;/th&gt;'</xsl:when>
              <xsl:otherwise><xsl:for-each select="Field">,'&lt;th class="biztax-context-single-head"&gt;<xsl:apply-templates select="." mode="GetLabel"><xsl:with-param name="type" select="'label'"/></xsl:apply-templates>&lt;/th&gt;'</xsl:for-each></xsl:otherwise>
              </xsl:choose></xsl:for-each>,'&lt;/tr&gt;'</xsl:if>
            ,'&lt;/thead&gt;'
            ,'&lt;tbody&gt;'
            ,'&lt;/tbody&gt;'
          ,'&lt;/table&gt;'
    ,'&lt;/tpl>'
  </xsl:template>

  <xsl:template match="Pane" mode="RenderContextTplLine">
    <xsl:variable name="hasgroups" select="Field[Context/period[@start='true' and @end='true']]|Pane"/>
    '&lt;tpl for="."&gt;'
    ,'&lt;tr&gt; '
    <xsl:apply-templates select="ElementContextDef/FieldDefinition" mode="RenderContextTplLineField"/>
    <xsl:apply-templates select="Field|Pane" mode="RenderContextTplLineField"/>
    ,'&lt;/tr&gt; '
    ,'&lt;/tpl>'
  </xsl:template>

  <xsl:template match="Pane" mode="RenderContextTplLineField">
    <xsl:apply-templates select="Field|Pane" mode="RenderContextTplLineField"/>
  </xsl:template>

  <xsl:template match="FieldDefinition" mode="RenderContextTplLineField">
    ,'&lt;td&gt;&lt;input type="text" dimension="<xsl:value-of select="Dimension"/>"/&gt;&lt;/td&gt;'
  </xsl:template>
  <xsl:template match="Field" mode="RenderContextTplLineField">
    <xsl:choose>
      <xsl:when test="Context/period[@start='true' and @end='true']">
        ,'&lt;td&gt;&lt;input type="text" period="I-Start" xbrlId="<xsl:value-of select="@xbrlId"/>"/&gt;&lt;/td&gt;'
        ,'&lt;td&gt;&lt;input type="text" period="I-End" xbrlId="<xsl:value-of select="@xbrlId"/>"/&gt;&lt;/td&gt;'
      </xsl:when>
      <xsl:otherwise>
        ,'&lt;td&gt;&lt;input type="text" period="D" xbrlId="<xsl:value-of select="@xbrlId"/>"/&gt;&lt;/td&gt;'
      </xsl:otherwise>
    </xsl:choose>

  </xsl:template>
  
  <xsl:template match="Field">
    {xtype:'<xsl:choose><xsl:when test="@isAbstract='true'">biztax-label</xsl:when><xsl:otherwise>biztax-field</xsl:otherwise></xsl:choose>'
      <xsl:apply-templates select="." mode="CommonFieldAttribs"/>
      <xsl:if test="not(@isAbstract='true')">,fieldType:<xsl:apply-templates select="." mode="GetFieldType"/></xsl:if>
    }
  </xsl:template>
  
  <xsl:template match="FieldDefinition">
    {xtype:'<xsl:choose><xsl:when test="ListData/*[1]">biztax-context-list</xsl:when><xsl:otherwise>biztax-field</xsl:otherwise></xsl:choose>'
      <!--<xsl:apply-templates select="." mode="CommonFieldAttribs"/>-->
    ,Dimension:'<xsl:value-of select="Dimension"/>'
    ,DimensionType:'<xsl:value-of select="DimensionType"/>'
    ,TypeId:'<xsl:value-of select="TypeElement/Id"/>'
    <xsl:choose>
      <xsl:when test="DimensionType='TypedDimension'">,fieldType:<xsl:choose><xsl:when test="DataType/*[1]"><xsl:apply-templates select="DataType" mode="GetFieldType"/></xsl:when><xsl:otherwise><xsl:apply-templates select="TypeElement" mode="GetFieldType"/></xsl:otherwise></xsl:choose></xsl:when>
      <xsl:otherwise>
        ,fieldType: {
        xtype:'combo'
        ,typeAhead: false
        ,autoSelect :true
        ,listWidth:300
        , forceSelection: true
        , hideTrigger: false
        , typeAhead: false
        , disableKeyFilter: true
        , editable: false
        , enableKeyEvents: false
        ,triggerAction :'all'
        , mode: 'local'
        ,store:[<xsl:apply-templates select="ListData" mode="FieldDefListData"/>]
        //, valueField: 'id'
        //, displayField: 'name'
        }
      </xsl:otherwise>
    </xsl:choose>
    }
  </xsl:template>

  <xsl:template match="ListData" mode="FieldDefListData"><xsl:for-each select="FieldListItem"><xsl:if test="position()>1">,</xsl:if>['<xsl:value-of select="ElementDef/Id"/>','<xsl:call-template name="ClearOutApos"><xsl:with-param name="txt" select="ElementDef/Labels/ElementLabel[1]/Descriptions/Translation[Language=$culture]/Description"/></xsl:call-template>']</xsl:for-each></xsl:template>
  
  <xsl:template match="Pane" mode="PaneField">
    {xtype:'<xsl:choose><xsl:when test="@isAbstract='true'">biztax-label</xsl:when><xsl:otherwise>biztax-field</xsl:otherwise></xsl:choose>'
      <xsl:apply-templates select="." mode="CommonFieldAttribs"/>
      <xsl:if test="not(@isAbstract='true')">,fieldType:<xsl:apply-templates select="." mode="GetFieldType"/></xsl:if>
    }
  </xsl:template>

  <xsl:template match="ValueList">
    {xtype:'biztax-list'
    <xsl:apply-templates select="." mode="CommonFieldAttribs"/>
    , substitutionGroup: '<xsl:value-of select="@substitutionGroup"/>'
    ,data:<xsl:value-of select="$jscontainer"/>.Lists['<xsl:value-of select="@xbrlId"/>']
    }
  </xsl:template>

  <xsl:template match="Field|ValueList|Pane" mode="CommonFieldAttribs"><xsl:variable name="name" select="@xbrlName"/>
    , title:'<xsl:apply-templates select="." mode="GetLabel"><xsl:with-param name="type" select="'label'"/></xsl:apply-templates>'
    , code:'<xsl:apply-templates select="." mode="GetCode"/>'
    , oldCode:'<xsl:apply-templates select="." mode="GetOldCode"/>'
    , periods:[<xsl:apply-templates select="." mode="GetPeriods"/>]
    <xsl:if test="//calculations/field[@name=$name] and not(../ElementContextDef/FieldDefinition) and not(Context/ContextDefinition/Fields/FieldDefinition)">, calculation:true</xsl:if>
    <xsl:apply-templates select="." mode="FieldTypeAttributes"/>
    <xsl:apply-templates select="." mode="CommonAttribs"/>
  </xsl:template>

  <xsl:template match="*" mode="GetContexts">
    <xsl:for-each select="Context">
      
    </xsl:for-each>
  </xsl:template>

  <xsl:template match="*" mode="GetPeriods">
  <xsl:if test="Context/period"><xsl:choose><xsl:when test="Context/period/@start='true' and Context/period/@end='true'">'I-Start','I-End'</xsl:when><xsl:when test="(Context/period/@start='false' or not(Context/period/@start)) and Context/period/@end='true'">'I-End'</xsl:when><xsl:when test="Context/period/@start='true' and (Context/period/@end='false'  or not(Context/period/@end))">'I-Start'</xsl:when><xsl:when test="not(Context/period/@start) and not(Context/period/@end)">'D'</xsl:when><xsl:otherwise>'D'</xsl:otherwise></xsl:choose></xsl:if>
  </xsl:template>
  
  <xsl:template match="Pane" mode="CommonPaneAttribs">
    ,title:'<xsl:apply-templates select="." mode="GetLabel"><xsl:with-param name="type" select="'label'"/></xsl:apply-templates>'
    ,code:'<xsl:apply-templates select="." mode="GetCode"/>'
    ,documentation:'<xsl:apply-templates select="." mode="GetLabel"><xsl:with-param name="type">documentation</xsl:with-param></xsl:apply-templates>'
    ,multipleContexts:<xsl:value-of select="@multipleContexts='true'"/>
    ,viewTplType:'<xsl:choose><xsl:when test="not(Field[not(@isAbstract='true')])">panesOnly</xsl:when><xsl:when test=".//Field/Context/period[@start='true' or @end='true']">instant</xsl:when><xsl:otherwise >duration</xsl:otherwise></xsl:choose>'
    <xsl:if test="not(@isAbstract='true') and @itemType">
    ,fieldInfo:<xsl:apply-templates select="." mode="PaneField"/>
    </xsl:if>
    <xsl:apply-templates select="." mode="CommonAttribs"/>
  </xsl:template>

  <xsl:template match="*" mode="CommonAttribs">
    , xbrlDef: {
      id:'<xsl:value-of select="@xbrlId"/>'
      , name:'<xsl:value-of select="@xbrlName"/>'
      , itemType:'<xsl:value-of select="@itemType"/>'
    }
    ,isAbstract:<xsl:value-of select="@isAbstract='true'"/>
    <xsl:variable name="id" select="@xbrlId"/>
    <xsl:variable name="lockitem" select="//MetaData/itemlocks/itemlock[@id=$id]"/>
    <xsl:if test="$lockitem">
      <xsl:choose>
        <xsl:when test="$lockitem/@locktype='always'">, elementsDisabled:true</xsl:when>
        <xsl:otherwise>, elementsDisabledFn:function() { return eBook.Interface.hasService('<xsl:value-of select="$lockitem/service/@id"/>'); } </xsl:otherwise>
      </xsl:choose>
    </xsl:if>
    

  </xsl:template>
  
  <xsl:template match="*" mode="GetCode">
    <xsl:if test="not(starts-with(@xbrlId,'pfs-'))">
    <xsl:apply-templates select="." mode="GetLabel">
      <xsl:with-param name="type">documentation</xsl:with-param>
      <xsl:with-param name="myculture">en</xsl:with-param>
    </xsl:apply-templates>
    </xsl:if>
  </xsl:template>

  <xsl:template match="*" mode="GetOldCode">
    <xsl:apply-templates select="." mode="GetLabel">
      <xsl:with-param name="type">documentation</xsl:with-param>
      <xsl:with-param name="myculture">en-US</xsl:with-param>
    </xsl:apply-templates>
  </xsl:template>

  <xsl:template name="ClearOutApos">
    <xsl:param name="txt"/>
    <xsl:variable name="apos">'</xsl:variable>
    <xsl:variable name="aposEsc">\'</xsl:variable>
    <xsl:call-template name="string-replace-all">
      <xsl:with-param name="text" select="string($txt)" />
      <xsl:with-param name="replace" select="$apos" />
      <xsl:with-param name="by" select="$aposEsc" />
    </xsl:call-template>
  </xsl:template>
  
  <xsl:template match="*" mode="GetLabel">
    <xsl:param name="type"/>
    <xsl:param name="myculture"/>
    <xsl:variable name="selCulture"><xsl:choose>
      <xsl:when test="$myculture and not($myculture='')"><xsl:value-of select="$myculture"/></xsl:when>
      <xsl:otherwise><xsl:value-of select="$culture"/></xsl:otherwise></xsl:choose></xsl:variable>
    <xsl:variable name="apos">'</xsl:variable>
    <xsl:variable name="aposEsc">\'</xsl:variable>
    <xsl:variable name="txt">
      <xsl:choose>
        <xsl:when test="labels/label[@type=$type]">
          <xsl:value-of select="labels/label[@type=$type]/translation[@lang=$selCulture]"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:if test="labels/label">
            <xsl:value-of select="labels/label[1]/translation[@lang=$selCulture]"/>
          </xsl:if>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:call-template name="string-replace-all">
      <xsl:with-param name="text" select="string($txt)" />
      <xsl:with-param name="replace" select="$apos" />
      <xsl:with-param name="by" select="$aposEsc" />
    </xsl:call-template>
  </xsl:template>

  <xsl:template match="Field|Pane|ValueList" mode="FieldTypeAttributes">
    <xsl:variable name="valueType"><xsl:choose><xsl:when test="not(@itemType) or @itemType=''"></xsl:when><xsl:otherwise><xsl:variable name="itemType" select="@itemType"/><xsl:variable name="typeEl" select="//XbrlDataType[Name=$itemType]"/><xsl:choose><xsl:when test="$typeEl"><xsl:value-of select="$typeEl/ValueType"/></xsl:when><xsl:otherwise></xsl:otherwise></xsl:choose></xsl:otherwise></xsl:choose></xsl:variable>
    
    <xsl:variable name="setType">
      <xsl:choose>
        <xsl:when test="contains(@itemType,'monetary') or contains(@itemType,'Monetary')">EUR</xsl:when>
        <xsl:otherwise>
        <xsl:choose>
          <xsl:when test="string($valueType)='System.Decimal'">PURE</xsl:when>
          <xsl:when test="string($valueType)='System.Byte[]'">BTE</xsl:when>
          <xsl:when test="string($valueType)='System.DateTime'">DTE</xsl:when>
          <xsl:when test="string($valueType)='System.String'">STR</xsl:when>
          <xsl:when test="string($valueType)='System.Single'">PURE</xsl:when>
          <xsl:when test="string($valueType)='System.Double'">PURE</xsl:when>
          <xsl:when test="string($valueType)='System.Single'">PURE</xsl:when>
          <xsl:when test="string($valueType)='System.Int64'">PURE</xsl:when>
          <xsl:when test="string($valueType)='System.Int32'">PURE</xsl:when>
          <xsl:when test="string($valueType)='System.Int16'">PURE</xsl:when>
          <xsl:when test="string($valueType)='System.UInt64'">PURE</xsl:when>
          <xsl:when test="string($valueType)='System.UInt32'">PURE</xsl:when>
          <xsl:when test="string($valueType)='System.UInt16'">PURE</xsl:when>
          <xsl:when test="string($valueType)='System.SByte'">BTE</xsl:when>
          <xsl:when test="string($valueType)='System.Byte'">BTE</xsl:when>
          <xsl:when test="string($valueType)='System.Boolean'">BOOL</xsl:when>
          <xsl:when test="string($valueType)='System.Uri'">URI</xsl:when>
          <xsl:when test="string($valueType)='System.TimeSpan'">TS</xsl:when>
          <xsl:when test="string($valueType)='System.Object'">OBJ</xsl:when>
          <xsl:otherwise>NULL</xsl:otherwise>
        </xsl:choose>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:choose>
      <xsl:when test="$setType='EUR'">
        ,decimalAttribute:'INF'
        ,unitAttribute:'EUR'
      </xsl:when>
      <xsl:when test="$setType='PURE'">
        ,decimalAttribute:'INF'
        ,unitAttribute:'U-Pure'
      </xsl:when>
      <xsl:otherwise>
        ,decimalAttribute:null
        ,unitAttribute:null
      </xsl:otherwise>
    </xsl:choose>
    
  </xsl:template>

  
 
  
  <xsl:template match="Field|Pane" mode="GetFieldType">
    <xsl:choose>
      <xsl:when test="not(@itemType) or @itemType=''">unknown</xsl:when>
      <xsl:otherwise>
        <xsl:variable name="itemType" select="@itemType"/>
        <xsl:variable name="typeEl" select="//XbrlDataType[Name=$itemType]"/>
        <xsl:choose>
          <xsl:when test="$typeEl">
            <xsl:apply-templates select="$typeEl" mode="GetFieldType"/>
          </xsl:when>
          <xsl:otherwise>
            Type <xsl:value-of select="$itemType"/> NOT FOUND
          </xsl:otherwise>
        </xsl:choose>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

   <xsl:template match="TypeElement" mode="GetFieldType">
      <xsl:choose>
        <xsl:when test="./Type='http://www.w3.org/2001/XMLSchema:date'">{xtype:'datefield'}</xsl:when>
        <xsl:otherwise>,:</xsl:otherwise> <!-- FORCE JS ERROR -->
      </xsl:choose>
  </xsl:template>
  
  <xsl:template match="XbrlDataType|DataType" mode="GetFieldType">
    <xsl:variable name="itemType" select="Name/text()"/>
    <xsl:choose>
        <xsl:when test="./ValueType='System.Decimal'"><xsl:apply-templates select="." mode="DetermineNumberFieldDef"><xsl:with-param name="itemType" select="$itemType"/></xsl:apply-templates></xsl:when>
        <xsl:when test="./ValueType='System.Byte[]'"><xsl:apply-templates select="." mode="BundleBuilderFieldDef"/></xsl:when>
        <xsl:when test="./ValueType='System.DateTime'"><xsl:apply-templates select="." mode="DateFieldDef"/></xsl:when>
        <xsl:when test="./ValueType='System.String'"><xsl:apply-templates select="." mode="TextFieldDef"/></xsl:when>
        <xsl:when test="./ValueType='System.Single'"><xsl:apply-templates select="." mode="DetermineNumberFieldDef"><xsl:with-param name="itemType" select="$itemType"/></xsl:apply-templates></xsl:when>
        <xsl:when test="./ValueType='System.Double'"><xsl:apply-templates select="." mode="DetermineNumberFieldDef"><xsl:with-param name="itemType" select="$itemType"/></xsl:apply-templates></xsl:when>
        <xsl:when test="./ValueType='System.Single'"><xsl:apply-templates select="." mode="DetermineNumberFieldDef"><xsl:with-param name="itemType" select="$itemType"/></xsl:apply-templates></xsl:when>
        <xsl:when test="./ValueType='System.Int64'"><xsl:apply-templates select="." mode="DetermineNumberFieldDef"><xsl:with-param name="itemType" select="$itemType"/></xsl:apply-templates></xsl:when>
        <xsl:when test="./ValueType='System.Int32'"><xsl:apply-templates select="." mode="DetermineNumberFieldDef"><xsl:with-param name="itemType" select="$itemType"/></xsl:apply-templates></xsl:when>
        <xsl:when test="./ValueType='System.Int16'"><xsl:apply-templates select="." mode="DetermineNumberFieldDef"><xsl:with-param name="itemType" select="$itemType"/></xsl:apply-templates></xsl:when>
        <xsl:when test="./ValueType='System.UInt64'"><xsl:apply-templates select="." mode="DetermineNumberFieldDef"><xsl:with-param name="itemType" select="$itemType"/></xsl:apply-templates></xsl:when>
        <xsl:when test="./ValueType='System.UInt32'"><xsl:apply-templates select="." mode="DetermineNumberFieldDef"><xsl:with-param name="itemType" select="$itemType"/></xsl:apply-templates></xsl:when>
        <xsl:when test="./ValueType='System.UInt16'"><xsl:apply-templates select="." mode="DetermineNumberFieldDef"><xsl:with-param name="itemType" select="$itemType"/></xsl:apply-templates></xsl:when>
        <xsl:when test="./ValueType='System.SByte'"><xsl:apply-templates select="." mode="BundleBuilderFieldDef"/></xsl:when>
        <xsl:when test="./ValueType='System.Byte'"><xsl:apply-templates select="." mode="BundleBuilderFieldDef"/></xsl:when>
        <xsl:when test="./ValueType='System.Boolean'"><xsl:apply-templates select="." mode="CheckFieldDef"/></xsl:when>
        <xsl:when test="./ValueType='System.Uri'"><xsl:apply-templates select="." mode="TextFieldDef"/></xsl:when>
        <xsl:when test="./ValueType='System.TimeSpan'"><xsl:apply-templates select="." mode="DetermineNumberFieldDef"><xsl:with-param name="itemType" select="$itemType"/></xsl:apply-templates></xsl:when>
        <xsl:when test="./ValueType='System.Object'">{xtype:'textfield',value:'unknown'}</xsl:when>
      </xsl:choose>
  </xsl:template>
  

  <xsl:template match="XbrlDataType|DataType" mode="DetermineNumberFieldDef">
    <xsl:param name="itemType"/>
    <xsl:choose><xsl:when test="contains($itemType,'monetary') or contains($itemType,'Monetary')"><xsl:apply-templates select="." mode="CurrencyFieldDef"/></xsl:when><xsl:when test="contains($itemType,'percent')"><xsl:apply-templates select="." mode="PercentageFieldDef"/></xsl:when><xsl:otherwise><xsl:apply-templates select="." mode="NumberFieldDef"/></xsl:otherwise></xsl:choose>
  </xsl:template>
  
  <xsl:template match="XbrlDataType|DataType" mode="TextFieldDef">
    {xtype:'textfield'
    <xsl:for-each select="Restrictions/Restriction">
      <xsl:choose>
        <xsl:when test="Name='System.Xml.Schema.XmlSchemaPatternFacet'">,regex:new RegExp('<xsl:value-of select="Value"/>')</xsl:when>
        <xsl:when test="Name='System.Xml.Schema.XmlSchemaMinLengthFacet'">,minLength: <xsl:value-of select="Value"/></xsl:when>
        <xsl:when test="Name='System.Xml.Schema.XmlSchemaMaxLengthFacet'">,maxLength: <xsl:value-of select="Value"/></xsl:when>        
      </xsl:choose>
    </xsl:for-each>
    }
  </xsl:template>

  <xsl:template match="XbrlDataType|DataType" mode="CheckFieldDef">
    {xtype:'checkbox'}
  </xsl:template>

  <xsl:template match="XbrlDataType|DataType" mode="NumberFieldDef">
    {xtype:'numberfield'
    <xsl:apply-templates select="." mode="SetNumberFieldProps"/>
    }
  </xsl:template>

  <xsl:template match="XbrlDataType|DataType" mode="CurrencyFieldDef">
    {xtype:'currencyfield'
      ,currencySymbol: '€'
      ,useThousandSeparator: true
      ,thousandSeparator: '.'
      ,alwaysDisplayDecimals: true
      ,decimalSeparator:','
      <xsl:apply-templates select="." mode="SetNumberFieldProps"/>
    }
  </xsl:template>

  <xsl:template match="XbrlDataType|DataType" mode="PercentageFieldDef">
    {xtype:'currencyfield'
    ,currencySymbol: '%'
    ,useThousandSeparator: false
    ,thousandSeparator: '.'
    ,alwaysDisplayDecimals: true
    ,decimalSeparator:','
    <xsl:apply-templates select="." mode="SetNumberFieldProps"/>
    }
  </xsl:template>

  <xsl:template match="XbrlDataType|DataType" mode="SetNumberFieldProps">
    <xsl:for-each select="Restrictions/Restriction">
      <xsl:choose>
        <xsl:when test="Name='System.Xml.Schema.XmlSchemaTotalDigitsFacet'">,maxLength:<xsl:value-of select="Value"/></xsl:when>
        <xsl:when test="Name='System.Xml.Schema.XmlSchemaFractionDigitsFacet'">,decimalPrecision:<xsl:value-of select="Value"/></xsl:when>
        <xsl:when test="Name='System.Xml.Schema.XmlSchemaMinInclusiveFacet'">,minValue:<xsl:value-of select="Value"/></xsl:when>
        <xsl:when test="Name='System.Xml.Schema.XmlSchemaMaxInclusiveFacet'">,maxValue:<xsl:value-of select="Value"/></xsl:when>
        
      </xsl:choose>
    </xsl:for-each>
    <xsl:if test="TypeCode='Integer' and not(Restrictions/Restriction[Name='System.Xml.Schema.XmlSchemaFractionDigitsFacet'])">
      ,decimalPrecision:0
      ,allowDecimals:false
    </xsl:if>
  </xsl:template>

  <xsl:template match="XbrlDataType|DataType" mode="DateFieldDef">
    {xtype:'datefield',format: 'Y-m-d'
    <xsl:for-each select="Restrictions/Restriction">
      <xsl:choose>
        <xsl:when test="Name='System.Xml.Schema.XmlSchemaMinInclusiveFacet'">,minValue:'<xsl:value-of select="Value"/>'</xsl:when>
        <xsl:when test="Name='System.Xml.Schema.XmlSchemaMaxInclusiveFacet'">,maxValue:'<xsl:value-of select="Value"/>'</xsl:when>
      </xsl:choose>
    </xsl:for-each>
    }
  </xsl:template>

  <xsl:template match="XbrlDataType|DataType" mode="BundleBuilderFieldDef">
    {xtype:'biztax-uploadfield'
    , isRequired:<xsl:value-of select="Name='nonEmptyBase64BinaryItemType'"/>
    }
  </xsl:template>

  <xsl:template name="string-replace-all">
    <xsl:param name="text" />
    <xsl:param name="replace" />
    <xsl:param name="by" />
    <xsl:choose>
      <xsl:when test="contains($text, $replace)">
        <xsl:value-of select="substring-before($text,$replace)" />
        <xsl:value-of select="$by" />
        <xsl:call-template name="string-replace-all">
          <xsl:with-param name="text"
          select="substring-after($text,$replace)" />
          <xsl:with-param name="replace" select="$replace" />
          <xsl:with-param name="by" select="$by" />
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:call-template name="crlf-replace"><xsl:with-param name="subject" select="$text"/></xsl:call-template>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  
  <xsl:template name="crlf-replace">
    <xsl:param name="subject"/>

    <xsl:choose>
      <xsl:when test="contains($subject, $crlf)">
        <xsl:value-of select="substring-before($subject, $crlf)"/><xsl:call-template name="crlf-replace"><xsl:with-param name="subject" select="substring-after($subject, $crlf)"/></xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$subject"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
</xsl:stylesheet>
