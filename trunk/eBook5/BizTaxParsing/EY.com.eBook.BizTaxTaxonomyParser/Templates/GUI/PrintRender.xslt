﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" 
                xmlns:xso="dummy" exclude-result-prefixes="msxsl">
  <xsl:output method="xml" indent="yes" encoding="utf-8" omit-xml-declaration="yes"/>
  <xsl:param name="lang">nl</xsl:param>
  
  
  <xsl:variable name="I-StartTitle">
    <xsl:choose>
      <xsl:when test="$lang='nl'">Bij het begin van het belastbare tijdperk</xsl:when>
      <xsl:when test="$lang='fr'">Au début de la période imposable</xsl:when>
      <xsl:when test="$lang='de'">DE-Bij het begin van het belastbare tijdperk</xsl:when>
      <xsl:otherwise>EN-Bij het begin van het belastbare tijdperk</xsl:otherwise>
    </xsl:choose>
  </xsl:variable>
  <xsl:variable name="I-EndTitle">
    <xsl:choose>
      <xsl:when test="$lang='nl'">Bij het einde van het belastbare tijdperk</xsl:when>
      <xsl:when test="$lang='fr'">A la fin de la période imposable</xsl:when>
      <xsl:when test="$lang='de'">DE-Bij het einde van het belastbare tijdperk</xsl:when>
      <xsl:otherwise>EN-Bij het einde van het belastbare tijdperk</xsl:otherwise>
    </xsl:choose>
  </xsl:variable>
  

  <xsl:variable name="ctxfontsize" select="8" />

  <xsl:variable name="WorksheetLabelFontsize" select="string('10')" />
  <xsl:variable name="WorksheetFontsize" select="string('10')" />
  <xsl:variable name="WorksheetNumbersize" select="string('8')" />
  <xsl:variable name="WorksheetSmallFontsize" select="string('8')" />
  <xsl:variable name="WorksheetTotalsFontsize" select="string('10')" />
  <xsl:variable name="WorksheetSpacing" select="1.1" />
  <xsl:namespace-alias stylesheet-prefix="xso" result-prefix="xsl"/>
  
  <xsl:template match="fiche">
    <xso:stylesheet version="1.0" exclude-result-prefixes="msxsl Helper" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"  xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:Helper="urn:Helper">
      <xso:decimal-format name="NonNumber" NaN="0" zero-digit="0"/>
      <xso:variable name="WorksheetLabelFontsize" select="string('10')"/>
      <xso:variable name="WorksheetFontsize" select="string('10')"/>
      <xso:variable name="WorksheetNumbersize" select="string('8')"/>
      <xso:variable name="WorksheetSmallFontsize" select="string('8')"/>
      <xso:variable name="WorksheetTotalsFontsize" select="string('10')"/>
      <xso:variable name="WorksheetSpacing" select="1.1"/>
      <xso:decimal-format name="euro" decimal-separator="," grouping-separator="."/>
      <xso:variable name="NumberFormatting" select="string('#.##0,00')"/>
      <xso:variable name="NumberFormattingNoDigit" select="string('#.##0')"/>
      <xso:variable name="FONT" select="string('C:\WINDOWS\Fonts\EYInterstate-Light.ttf')"/>
      
      <xso:param name="Culture"/>
      <xso:param name="Binaries"/>

      <xso:template match="BizTaxDataContract">
       
        <Root>
          <paragraph lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}" fontstyle="bold underline">
            <xsl:call-template name="GetLabel">
              <xsl:with-param name="details" select="details"/>
              <xsl:with-param name="code" select="true()"/>
            </xsl:call-template>
          </paragraph>
          <xsl:choose>
            <xsl:when test="count(children/field)=0">
              <xsl:for-each select="children/*">
                <xsl:apply-templates select="."></xsl:apply-templates>
              </xsl:for-each>
            </xsl:when>
            <xsl:otherwise>
              <xsl:apply-templates select="." mode="CREATETABLE"></xsl:apply-templates>
            </xsl:otherwise>
          </xsl:choose>
          
        </Root>
      </xso:template>

      <xso:template name="GetTitleDuration">
        <xso:choose>
          <xso:when test="$Culture='nl-BE'">Belastbaar tijdperk</xso:when>
          <xso:when test="$Culture='fr-FR'">Période imposable</xso:when>
          <xso:otherwise>Taxable Era</xso:otherwise>
        </xso:choose>
      </xso:template>

      <xso:template name="GetTitleIStart">
        <xso:choose>
          <xso:when test="$Culture='nl-BE'">Bij het begin van het belastbare tijdperk</xso:when>
          <xso:when test="$Culture='fr-FR'">Au début de la période imposable</xso:when>
          <xso:otherwise>At start of the taxable era</xso:otherwise>
        </xso:choose>
      </xso:template>

      <xso:template name="GetTitleIEnd">
        <xso:choose>
          <xso:when test="$Culture='nl-BE'">Bij het einde van het belastbare tijdperk</xso:when>
          <xso:when test="$Culture='fr-FR'">A la fin de la période imposable</xso:when>
          <xso:otherwise>At end of the taxable era</xso:otherwise>
        </xso:choose>
      </xso:template>
    </xso:stylesheet>
    
    
  </xsl:template>
 
  
  <xsl:template match="panel">
    <xsl:param name="parentTableInfo"/>
    <xsl:param name="indent" select="0"/>
    <xsl:choose>
      <xsl:when test="not($parentTableInfo)">
        <newline />
        <paragraph lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}" fontstyle="bold underline">
          <xsl:attribute name="fontstyle">
            <xsl:choose>
              <xsl:when test="$indent=0">bold</xsl:when>
              <xsl:otherwise>underline</xsl:otherwise>
            </xsl:choose>
          </xsl:attribute>
          <xsl:call-template name="GetLabel">
            <xsl:with-param name="details" select="details"/>
            <xsl:with-param name="code" select="false()"/>
          </xsl:call-template>
        </paragraph>
        <xsl:choose>
          <xsl:when test="@hasFields='false'">
            <xsl:apply-templates select="children/*"></xsl:apply-templates>
          </xsl:when>
          <xsl:when test="@hasFields='true' and children/*[1]/children//panel/@type='grid' and not(@type='grid')">
            <xsl:variable name="fPos" select="count(children/field[1]/preceding-sibling::*)+1"/>
            <xsl:for-each select="children/*[position() &lt; $fPos]">
              <xsl:apply-templates select=".">
                <xsl:with-param name="parentTableInfo" select="$parentTableInfo"/>
                <xsl:with-param name="indent" select="$indent"/>
              </xsl:apply-templates>
            </xsl:for-each>
            <xsl:apply-templates select="." mode="CREATETABLE">
              <xsl:with-param name="childrenfrom" select="$fPos"/>
            </xsl:apply-templates>
          </xsl:when>
          <xsl:otherwise>
            <!-- construct table -->
            <xsl:apply-templates select="." mode="CREATETABLE"></xsl:apply-templates>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:when>
      <xsl:otherwise>
        <row>
          <cell left="true" bottom="true" top="true" grayfill="0.3" horizontalalign="left" verticalalign="Top">
            <xsl:choose>
              <xsl:when test="fieldDefinition">
                <xsl:attribute name="colspan">
                  <xsl:value-of select="number($parentTableInfo/@count)-1"/>
                </xsl:attribute>
              </xsl:when>
              <xsl:otherwise>
                <xsl:attribute name="colspan">
                  <xsl:value-of select="$parentTableInfo/@count"/>
                </xsl:attribute>
              </xsl:otherwise>
            </xsl:choose>
            <xsl:call-template name="GetLabel">
              <xsl:with-param name="details" select="details"/>
              <xsl:with-param name="code" select="false()"/>
            </xsl:call-template>
          </cell>
   
          <xsl:if test="fieldDefinition">
            <cell left="true" bottom="true" top="true" horizontalalign="left" verticalalign="Top">
              <xsl:apply-templates select="." mode="RENDERFIELD">
                <xsl:with-param name="period" select="details/periods/string[1]"/>
              </xsl:apply-templates>
            </cell>
          </xsl:if>
        </row>
        <xsl:choose>
          <xsl:when test="ancestor::panel/@type=./@type or ./@type='normal'">
            <xsl:apply-templates select="." mode="HANDLECHILDREN">
              <xsl:with-param name="parentTableInfo" select="$parentTableInfo"/>
              <xsl:with-param name="indent" select="number($indent)+20"/>
            </xsl:apply-templates>
          </xsl:when>
          <xsl:otherwise>
            <row>
              <cell>
                <xsl:attribute name="colspan">
                  <xsl:value-of select="$parentTableInfo/@count"/>
                </xsl:attribute>
                <xsl:apply-templates select="." mode="CREATETABLE"></xsl:apply-templates>
              </cell>
            </row>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  <xsl:template match="panel|fiche" mode="CREATETABLE">
    <xsl:param name="parentTableInfo"/>
    <xsl:param name="indent" select="0"/>
    <xsl:param name="childrenfrom" select="0"/>
    <xsl:choose>
      <xsl:when test="./@type='contextgrid'">
        <table tablefitspage="true" convert2pdfp="true" cellsfitpage="true" borderwidth="0.02"  width="100%" align="right" cellspacing="0" cellpadding="3">
          <!-- NOG TOE TE VOEGEN AAN GEN-->
          <xsl:attribute name="columns">
            <xsl:value-of select="@columncount"/>
          </xsl:attribute>
          <xsl:attribute name="widths">
            <xsl:value-of select="@columnwidths"/>
          </xsl:attribute>

          <xsl:variable name="depth" select="number(columns/@depth)"/>
              <row>
                <xsl:for-each select="columns/column">
                  <xsl:apply-templates select=".">
                    <xsl:with-param name="depth" select="$depth"/>
                  </xsl:apply-templates>
                </xsl:for-each>
              </row>
              <xsl:if test="$depth>1">
                <row>
                  <xsl:for-each select="columns/column/column">
                    <xsl:apply-templates select=".">
                      <xsl:with-param name="depth" select="$depth - 1"/>
                    </xsl:apply-templates>
                  </xsl:for-each>
                </row>
              </xsl:if>
              <xsl:if test="$depth>2">
                <row>
                  <xsl:for-each select="columns/column/column/column">
                    <xsl:apply-templates select=".">
                      <xsl:with-param name="depth" select="$depth - 2"/>
                    </xsl:apply-templates>
                  </xsl:for-each>
                </row>
              </xsl:if>
          <xso:for-each>
            <xsl:attribute name="select">
              Helper:Distinct(//Context[PresContextId='<xsl:value-of select="@id"/>']/Id,'.')
            </xsl:attribute>
            <xso:variable name="elements">
              <xso:call-template name="GetContextElements">
                <xso:with-param name="cid" select="."/>
              </xso:call-template>
            </xso:variable>
            <xso:variable name="xels" select="msxsl:node-set(elements)"/>
            <row>
              <xsl:apply-templates select="columns/column" mode="HANDLECGCHILDREN">
                <xsl:with-param name="par" select="."/>
                <!--xsl:with-param name="parentTableInfo" select="columns"/><xsl:with-param name="indent" select="$indent"/><xsl:with-param name="childrenfrom" select="$childrenfrom"/-->
              </xsl:apply-templates>
            </row>
          </xso:for-each>
            </table>
         
      </xsl:when>
      <xsl:otherwise>
        <table tablefitspage="true" convert2pdfp="true" cellsfitpage="true" borderwidth="0.02"  width="100%" align="right" cellspacing="0" cellpadding="3">
          <!-- NOG TOE TE VOEGEN AAN GEN-->
          <xsl:attribute name="columns">
            <xsl:value-of select="@columncount"/>
          </xsl:attribute>
          <xsl:attribute name="widths">
            <xsl:value-of select="@columnwidths"/>
          </xsl:attribute>
         
          <xsl:variable name="depth" select="number(columns/@depth)"/>
          <row>
            <xsl:for-each select="columns/column">
              <xsl:apply-templates select=".">
                <xsl:with-param name="depth" select="$depth"/>
              </xsl:apply-templates>
            </xsl:for-each>
          </row>
          <xsl:if test="$depth>1">
            <row>
              <xsl:for-each select="columns/column/column">
                <xsl:apply-templates select=".">
                  <xsl:with-param name="depth" select="$depth - 1"/>
                </xsl:apply-templates>
              </xsl:for-each>
            </row>
          </xsl:if>
          <xsl:if test="$depth>2">
            <row>
              <xsl:for-each select="columns/column/column/column">
                <xsl:apply-templates select=".">
                  <xsl:with-param name="depth" select="$depth - 2"/>
                </xsl:apply-templates>
              </xsl:for-each>
            </row>
          </xsl:if>
          <xsl:if test="not(./@type='contextgrid')">
            <xsl:apply-templates select="." mode="HANDLECHILDREN">
              <xsl:with-param name="parentTableInfo" select="columns"/>
              <xsl:with-param name="indent" select="$indent"/>
              <xsl:with-param name="childrenfrom" select="$childrenfrom"/>
            </xsl:apply-templates>
          </xsl:if>
          <xsl:if test="./@type='contextgrid'">

            <xso:for-each>
              <xsl:attribute name="select">Helper:Distinct(//Context[PresContextId='<xsl:value-of select="@id"/>']/Id,'.')</xsl:attribute>
              <xso:variable name="id" select="strinn('.')"/>
              <xso:variable name="elements">
                <xso:call-template name="GetContextElements">
                  <xso:with-param name="cid" select="."/>
                </xso:call-template>
              </xso:variable>
              <xso:variable name="xels" select="msxsl:node-set(elements)"/>
              <xso:variable name="contexts" select="//Context[Id=$id]"/>
              <row>
                <xsl:apply-templates select="columns/column" mode="HANDLECGCHILDREN">
                  <xsl:with-param name="par" select="."/>
                  
                  <!--xsl:with-param name="parentTableInfo" select="columns"/><xsl:with-param name="indent" select="$indent"/><xsl:with-param name="childrenfrom" select="$childrenfrom"/-->
                </xsl:apply-templates>
              </row>
            </xso:for-each>
          </xsl:if>
        </table>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  
  <xsl:template match="column" mode="HANDLECGCHILDREN">
    <xsl:param name="par"/>
   
    <xsl:choose>
      <xsl:when test="column">
        <xsl:apply-templates select="column" mode="HANDLECGCHILDREN">
          <xsl:with-param name="par" select="$par"/>
        </xsl:apply-templates>
      </xsl:when>
      <xsl:otherwise>
        <cell>
          <xsl:choose>
            <xsl:when test="@type='action'">
              <space/>
            </xsl:when>
            <xsl:when test="@type='contextfield'">
              <xso:value-of select="$contexts[1]//"/>
              <xsl:apply-templates select="dimensionfield"></xsl:apply-templates>
            </xsl:when>
            <xsl:when test="@type='field'">
              <xsl:variable name="fieldId" select="@fieldid"/>
              <xsl:variable name="field" select="ancestor::columns/../children//field[@id=$fieldId]"/>
              <xsl:apply-templates select="$field" mode="RENDERFIELD">
                <xsl:with-param name="period" select="@period"/>
              </xsl:apply-templates>
            </xsl:when>
            <xsl:when test="@type='period'">
              <xsl:variable name="fieldId" select="parent::column/@fieldid"/>
              <xsl:variable name="field" select="ancestor::columns/../children//field[@id=$fieldId]"/>
              <xsl:apply-templates select="$field" mode="RENDERFIELD">
                <xsl:with-param name="period" select="@period"/>
              </xsl:apply-templates>
            </xsl:when>
            <xsl:otherwise>
              unknown type: <xsl:value-of select="@type"/>
            </xsl:otherwise>
          </xsl:choose>
        </cell>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
 
  <xsl:template match="panel|fiche" mode="HANDLECHILDREN">
    <xsl:param name="parentTableInfo"/>
    <xsl:param name="indent"/>
    <xsl:param name="childrenfrom" select="0"/>
    <xsl:for-each select="children/*[position() &gt;= $childrenfrom]">
      <xsl:apply-templates select=".">
        <xsl:with-param name="parentTableInfo" select="$parentTableInfo"/>
        <xsl:with-param name="indent" select="$indent"/>
      </xsl:apply-templates>
    </xsl:for-each>
  </xsl:template>
  
  <xsl:template match="dimensionfield">
    
    <div class="biztax-field-wrapper biztax-contextfield">
      <xsl:attribute name="data-xbrl-dimension-id">
        <xsl:value-of select="parent::column/@dimensionid"/>
      </xsl:attribute>
      <xsl:attribute name="data-xbrl-datatype">
        <xsl:value-of select="XbrlDataType/Name"/>
      </xsl:attribute>
      <xsl:attribute name="data-xbrl-type">
        <xsl:choose>
          <xsl:when test="list">select</xsl:when>
          <xsl:otherwise>input</xsl:otherwise>
        </xsl:choose>
      </xsl:attribute>
      <xsl:if test="list">
        <xsl:for-each select="list/item">
          <div data-value="{@value}">
            <xsl:call-template name="GetLabel">
              <xsl:with-param name="details" select="."/>
              <xsl:with-param name="code" select="false()"/>
            </xsl:call-template>
          </div>
        </xsl:for-each>
      </xsl:if>
    </div>
  </xsl:template>
 
  <xsl:template match="field">
    <xsl:param name="parentTableInfo"/>
    <xsl:param name="indent"/>
    <xsl:variable name="me" select="."/>
    <xsl:choose>
      <xsl:when test="details/fieldType/text()='base64BinaryItemType' or details/fieldType/text()='nonEmptyBase64BinaryItemType'">
        <tr>
          <td>
            <xsl:attribute name="colspan">
              <xsl:value-of select="$parentTableInfo/@count"/>
            </xsl:attribute>
            <xsl:attribute name="class">biztax-td-name</xsl:attribute>
            <xsl:if test="$indent">
              <xsl:attribute name="style">
                padding-left:<xsl:value-of select="$indent"/>px
              </xsl:attribute>
            </xsl:if>
            <xsl:call-template name="GetLabel">
              <xsl:with-param name="details" select="details"/>
              <xsl:with-param name="code" select="false()"/>
            </xsl:call-template>
          </td>
        </tr>
        <tr>
          <td>
            <xsl:attribute name="colspan">
              <xsl:value-of select="$parentTableInfo/@count"/>
            </xsl:attribute>
            <xsl:for-each select="$parentTableInfo/column[not(@type='name')][1]">
              <xsl:apply-templates select="$me" mode="RENDERFIELD">
                <xsl:with-param name="period" select="$me/details/periods/string[1]"/>
              </xsl:apply-templates>
            </xsl:for-each>
          </td>
        </tr>
      </xsl:when>
      <xsl:otherwise>
        <tr>
          <xsl:for-each select="$parentTableInfo/column">
            <xsl:apply-templates select="." mode="APPLYFIELD">
              <xsl:with-param name="field" select="$me"/>
              <xsl:with-param name="indent" select="$indent"/>
            </xsl:apply-templates>
          </xsl:for-each>
        </tr>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  
  <xsl:template match="valueListOrOther">
    <xsl:param name="parentTableInfo"/>
    <xsl:param name="indent"/>
    <xsl:variable name="me" select="."/>
    <tr>
      <td>
        <xsl:attribute name="colspan">
          <xsl:value-of select="$parentTableInfo/@count"/>
        </xsl:attribute>
        <table width="100%" class="biztax-valuelistorother">
          <xsl:attribute name="data-xbrl-id">
            <xsl:value-of select="@id"/>
          </xsl:attribute>
          <xsl:attribute name="data-xbrl-name">
            <xsl:value-of select="@id"/>
          </xsl:attribute>
          <xsl:attribute name="data-xbrl-dimensionset">
            <xsl:value-of select="@dimensionSet"/>
          </xsl:attribute>
          <xsl:attribute name="data-xbrl-path">
            <xsl:variable name="path" select="count(./ancestor::panel[@istuple='true'])"/>
            <xsl:if test="$path>1">
              <xsl:call-template name="GetSelector">
                <xsl:with-param name="node" select="./ancestor::panel[@istuple='true'][2]"/>
              </xsl:call-template>
            </xsl:if>
          </xsl:attribute>
          <xsl:attribute name="data-xbrl-fieldtype">valuelistorother</xsl:attribute>
          <tbody>
            <xsl:for-each select="valuelist/field">
              <tr>
                <td class="biztax-td-name biztax-valuelistorother-vl">
                  <xsl:attribute name="style">padding-left:10px</xsl:attribute>
                  <input type="radio" value="valuelist"/>
                  <xsl:call-template name="GetLabel">
                    <xsl:with-param name="details" select="details"/>
                    <xsl:with-param name="code" select="false()"/>
                  </xsl:call-template>
                </td>
                <xsl:variable name="vlField" select="."/>
                <xsl:for-each select="$parentTableInfo/column[not(@type='name')]">
                  <xsl:apply-templates select="." mode="APPLYFIELD">
                    <xsl:with-param name="field" select="$vlField"/>
                    <xsl:with-param name="indent" select="$indent"/>
                  </xsl:apply-templates>
                </xsl:for-each>
              </tr>
            </xsl:for-each>
            <xsl:for-each select="other/field">
              <tr>
                <td class="biztax-td-name biztax-valuelistorother-other">
                  <xsl:attribute name="style">padding-left:10px</xsl:attribute>
                  <input type="radio" value="other"/>
                  <xsl:call-template name="GetLabel">
                    <xsl:with-param name="details" select="details"/>
                    <xsl:with-param name="code" select="false()"/>
                  </xsl:call-template>
                </td>
                <xsl:variable name="otField" select="."/>
                <xsl:for-each select="$parentTableInfo/column[not(@type='name')]">
                  <xsl:apply-templates select="." mode="APPLYFIELD">
                    <xsl:with-param name="field" select="$otField"/>
                    <xsl:with-param name="indent" select="$indent"/>
                  </xsl:apply-templates>
                </xsl:for-each>
              </tr>
            </xsl:for-each>
          </tbody>
        </table>
      </td>
    </tr>
  </xsl:template>
  
  <xsl:template match="column" mode="APPLYFIELD">
    <xsl:param name="field"/>
    <xsl:param name="indent"/>
    <xsl:choose>
      <xsl:when test="column">
        <xsl:apply-templates select="column" mode="APPLYFIELD">
          <xsl:with-param name="field" select="$field"/>
        </xsl:apply-templates>
      </xsl:when>
      <xsl:otherwise>
        <td>
          <xsl:choose>
            <xsl:when test="@type='name'">
              <xsl:attribute name="class">biztax-td-name</xsl:attribute>
              <xsl:if test="$indent">
                <xsl:attribute name="style">
                  padding-left:<xsl:value-of select="$indent"/>px
                </xsl:attribute>
              </xsl:if>
              <xsl:call-template name="GetLabel">
                <xsl:with-param name="details" select="$field/details"/>
                <xsl:with-param name="code" select="false()"/>
              </xsl:call-template>
            </xsl:when>
            <xsl:when test="@type='period'">
              <xsl:attribute name="class">biztax-td-period</xsl:attribute>
              <xsl:attribute name="width">100px</xsl:attribute>
              <xsl:choose>
                <xsl:when test="@period='I-Start'">
                  <xsl:choose>
                    <xsl:when test="$field/details/presperiods/string/text()='I-Start'">
                      <xsl:apply-templates select="$field" mode="RENDERFIELD">
                        <xsl:with-param name="period" select="'I-Start'"/>
                      </xsl:apply-templates>
                    </xsl:when>
                    <xsl:otherwise>
                      <span>
                        <xsl:call-template name="space"/>
                      </span>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:when>
                <xsl:when test="@period='I-End'">
                  <xsl:choose>
                    <xsl:when test="$field/details/presperiods/string/text()='I-End'">
                      <xsl:apply-templates select="$field" mode="RENDERFIELD">
                        <xsl:with-param name="period" select="'I-End'"/>
                      </xsl:apply-templates>
                    </xsl:when>
                    <xsl:when test="$field/details/presperiods/string/text()='D'">
                      <xsl:apply-templates select="$field" mode="RENDERFIELD">
                        <xsl:with-param name="period" select="'D'"/>
                      </xsl:apply-templates>
                    </xsl:when>
                    <xsl:otherwise>
                      <span>
                        <xsl:call-template name="space"/>
                      </span>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:when>
                <xsl:when test="@period='D'">
                  <xsl:choose>
                    <xsl:when test="$field/details/presperiods/string/text()='D'">
                      <xsl:apply-templates select="$field" mode="RENDERFIELD">
                        <xsl:with-param name="period" select="'D'"/>
                      </xsl:apply-templates>
                    </xsl:when>
                    <xsl:otherwise>
                      <span>
                        <xsl:call-template name="space"/>
                      </span>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:when>
              </xsl:choose>
            </xsl:when>
            <xsl:when test="@type='code'">
              <xsl:attribute name="class">biztax-td-code</xsl:attribute>
              <xsl:attribute name="width">50px</xsl:attribute>
              <xsl:choose>
                <xsl:when test="$field/details/@code">
                  <xsl:value-of select="$field/details/@code"/>
                </xsl:when>
                <xsl:otherwise>
                  <span>
                    <xsl:call-template name="space"/>
                  </span>
                </xsl:otherwise>
              </xsl:choose>
            </xsl:when>
            <xsl:when test="@type='oldcode'">
              <xsl:attribute name="class">biztax-td-code</xsl:attribute>
              <xsl:attribute name="width">50px</xsl:attribute>
              <xsl:choose>
                <xsl:when test="$field/details/@oldcode">
                  <xsl:value-of select="$field/details/@oldcode"/>
                </xsl:when>
                <xsl:otherwise>
                  <span>
                    <xsl:call-template name="space"/>
                  </span>
                </xsl:otherwise>
              </xsl:choose>
            </xsl:when>
            <xsl:when test="@type='contextfield'">
              <xsl:attribute name="class">biztax-td-context-header</xsl:attribute>
              <xsl:attribute name="width">100px</xsl:attribute>
              <!--xsl:call-template name="GetLabel"><xsl:with-param name="details" select="."/><xsl:with-param name="code" select="false()"/></xsl:call-template-->
            </xsl:when>
            <xsl:when test="@type='field'">
              <xsl:attribute name="class">biztax-td-context-header</xsl:attribute>
              <xsl:attribute name="width">100px</xsl:attribute>
              <!--xsl:call-template name="GetLabel"><xsl:with-param name="details" select="."/><xsl:with-param name="code" select="false()"/></xsl:call-template-->
            </xsl:when>
            <xsl:when test="@type='dimension'">
              <xsl:attribute name="class">biztax-td-grid-item</xsl:attribute>
              <xsl:attribute name="width">100px</xsl:attribute>
              <xsl:variable name="dimid" select="@dimensionid"/>
              <xsl:variable name="domid" select="@domid"/>
              <xsl:choose>
                <xsl:when test="$field/details/Scenario/dimension[@id=$dimid]/dimensionfield/list/item[@value=$domid]">
                  <xsl:apply-templates select="$field" mode="RENDERFIELD">
                    <xsl:with-param name="period" select="'D'"/>
                    <xsl:with-param name="dimension" select="@dimensionid"/>
                    <xsl:with-param name="dimensionmember" select="@domid"/>
                    <xsl:with-param name="context">
                      <xsl:apply-templates select="$field/details" mode="CONTEXT">
                        <xsl:with-param name="dimension" select="@dimensionid"/>
                        <xsl:with-param name="dimensionmember" select="@domid"/>
                      </xsl:apply-templates>
                    </xsl:with-param>
                  </xsl:apply-templates>
                </xsl:when>
                <xsl:otherwise>
                  <span>
                    <xsl:call-template name="space"/>
                  </span>
                </xsl:otherwise>
              </xsl:choose>
            </xsl:when>
          </xsl:choose>
        </td>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  
  <xsl:template match="details" mode="CONTEXT">
    <xsl:param name="dimension"/>
    <xsl:param name="dimensionmember"/>
    <xsl:for-each select="Scenario/dimension">
      <xsl:if test="position()>1">__</xsl:if>id__<xsl:choose>
        <xsl:when test="@id=$dimension">
          <xsl:value-of select="dimensionfield/list/item[@value=$dimensionmember]/@name"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="dimensionfield/list/item[1]/@name"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:for-each>
  </xsl:template>
 
  <xsl:template match="field|panel" mode="RENDERFIELD">
    <xsl:param name="period"/>
    <xsl:param name="dimension"/>
    <xsl:param name="dimensionmember"/>
    <xsl:param name="context"/>
    <xsl:variable name="contentPeriod">
      <xsl:choose>
        <xsl:when test="details/periods/string[./text()=$period]">
          <xsl:value-of select="$period"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="details/periods/string[1]"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:if test="not(@abstract='true') or @fieldtype='valuelist'">
      <xsl:choose>
        <xsl:when test="@fieldtype='valuelist'">
          <xsl:apply-templates select="./ancestor::panel[1]" mode="APPLYDIVFIELD">
            <xsl:with-param name="period" select="$period"/>
            <xsl:with-param name="dimension" select="$dimension"/>
            <xsl:with-param name="dimensionmember" select="$dimensionmember"/>
            <xsl:with-param name="context" select="$context"/>
            <xsl:with-param name="contentPeriod" select="$contentPeriod"/>
            <xsl:with-param name="fieldtype" select="@fieldtype"/>
          </xsl:apply-templates>
        </xsl:when>
        <xsl:otherwise>
          <xsl:apply-templates select="." mode="APPLYDIVFIELD">
            <xsl:with-param name="period" select="$period"/>
            <xsl:with-param name="dimension" select="$dimension"/>
            <xsl:with-param name="dimensionmember" select="$dimensionmember"/>
            <xsl:with-param name="context" select="$context"/>
            <xsl:with-param name="contentPeriod" select="$contentPeriod"/>
            <xsl:with-param name="fieldtype" select="@fieldtype"/>
          </xsl:apply-templates>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:if>
  </xsl:template>
 
  <xsl:template match="*" mode="APPLYDIVFIELD">
    <xsl:param name="period"/>
    <xsl:param name="dimension"/>
    <xsl:param name="dimensionmember"/>
    <xsl:param name="context"/>
    <xsl:param name="contentPeriod"/>
    <xsl:param name="fieldtype"/>
    <div>
      <xsl:attribute name="class">
        biztax-field-wrapper <xsl:if test="@calculated='true'">biztax-field-calculated</xsl:if>
      </xsl:attribute>
      <xsl:attribute name="data-xbrl-calc">
        <xsl:value-of select="@calculated"/>
      </xsl:attribute>
      <xsl:attribute name="data-xbrl-id">
        <xsl:apply-templates select="." mode="GetFullId">
          <xsl:with-param name="period" select="$period"/>
          <xsl:with-param name="context" select="$context"/>
        </xsl:apply-templates>
      </xsl:attribute>
      <xsl:attribute name="data-xbrl-pres-period">
        <xsl:value-of select="$period"/>
      </xsl:attribute>
      <xsl:attribute name="data-xbrl-period">
        <xsl:value-of select="$contentPeriod"/>
      </xsl:attribute>
      <xsl:attribute name="data-xbrl-name">
        <xsl:value-of select="@id"/>
      </xsl:attribute>
      <xsl:attribute name="data-xbrl-context">
        <xsl:value-of select="$context"/>
      </xsl:attribute>
      <xsl:attribute name="data-xbrl-prefix">
        <xsl:value-of select="@ns"/>
      </xsl:attribute>
      <xsl:attribute name="data-xbrl-decimals">
        <xsl:value-of select="@decimals"/>
      </xsl:attribute>
      <xsl:attribute name="data-xbrl-unitref">
        <xsl:value-of select="@unitref"/>
      </xsl:attribute>
      <xsl:attribute name="data-xbrl-dimensionset">
        <xsl:value-of select="@dimensionSet"/>
      </xsl:attribute>
      <xsl:attribute name="data-xbrl-dimension">
        <xsl:value-of select="$dimension"/>
      </xsl:attribute>
      <xsl:attribute name="data-xbrl-dimensionmember">
        <xsl:value-of select="$dimensionmember"/>
      </xsl:attribute>
      <xsl:attribute name="data-xbrl-path">
        <xsl:variable name="path" select="count(./ancestor::panel[@istuple='true'])"/>
        <xsl:if test="$path>0">
          <xsl:call-template name="GetSelector">
            <xsl:with-param name="node" select="./ancestor::panel[@istuple='true'][1]"/>
          </xsl:call-template>
        </xsl:if>
      </xsl:attribute>
      <xsl:attribute name="data-xbrl-fieldtype">
        <xsl:choose>
          <xsl:when test="$fieldtype">
            <xsl:value-of select="$fieldtype"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="details/fieldType/text()"/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:attribute>
      <xsl:if test="$fieldtype='valuelist'">
        <xsl:attribute name="data-xbrl-valuelistid">
          <xsl:value-of select=".//*/field[@fieldtype='valuelist']/list/@id"/>
        </xsl:attribute>
      </xsl:if>
      <!--id:<b><xsl:value-of select="@id"/></b><br/> period:<b><xsl:value-of select="$contentPeriod"/></b><br/> dmSet:<b><xsl:value-of select="@dimensionSet"/></b><br/> dm:<b><xsl:value-of select="$dimension"/></b><br/> dm-member:<b><xsl:value-of select="$dimensionmember"/></b-->
    </div>
  </xsl:template>
  
  <xsl:template name="GetSelector">
    <xsl:param name="node"/><xsl:if test="$node/ancestor::panel[@istuple='true']">
      <xsl:call-template name="GetSelector">
        <xsl:with-param name="node" select="$node/ancestor::panel[@istuple='true'][1]"/>
      </xsl:call-template>
    </xsl:if>/<xsl:value-of select="$node/@id"/>
  </xsl:template>
  
  <xsl:template match="column">
    <xsl:param name="depth"/>
    <th>
      <xsl:if test="$depth>1 and not(column)">
        <xsl:attribute name="rowspan">
          <xsl:value-of select="$depth"/>
        </xsl:attribute>
      </xsl:if>
      <xsl:if test="$depth>1 and column">
        <xsl:attribute name="colspan">
          <xsl:value-of select="count(column)"/>
        </xsl:attribute>
      </xsl:if>
      <xsl:choose>
        <xsl:when test="@type='name'">
          <xsl:attribute name="class">biztax-th-name</xsl:attribute>
          <span>
            <xsl:call-template name="space"/>
          </span>
        </xsl:when>
        <xsl:when test="@type='panel'">
          <xsl:attribute name="class">biztax-th-panel</xsl:attribute>
          <xsl:call-template name="GetLabel">
            <xsl:with-param name="details" select="."/>
            <xsl:with-param name="code" select="false()"/>
          </xsl:call-template>
        </xsl:when>
        <xsl:when test="@type='period'">
          <xsl:attribute name="class">biztax-th-period</xsl:attribute>
          <xsl:attribute name="width">100px</xsl:attribute>
          <xsl:choose>
            <xsl:when test="@period='I-Start'">
              <xsl:value-of select="$I-StartTitle"/>
            </xsl:when>
            <xsl:when test="@period='I-End'">
              <xsl:value-of select="$I-EndTitle"/>
            </xsl:when>
            <xsl:when test="@period='D'">
              
            </xsl:when>
          </xsl:choose>
        </xsl:when>
        <xsl:when test="@type='code'">
          <xsl:attribute name="class">biztax-th-code</xsl:attribute><xsl:attribute name="width">50px</xsl:attribute>Code
        </xsl:when>
        <xsl:when test="@type='oldcode'">
          <xsl:attribute name="class">biztax-th-code</xsl:attribute>
          <xsl:attribute name="width">50px</xsl:attribute>
          <span>
            <xsl:call-template name="space"/>
          </span>
        </xsl:when>
        <xsl:when test="@type='contextfield'">
          <xsl:attribute name="class">biztax-th-context-header</xsl:attribute>
          <xsl:attribute name="width">100px</xsl:attribute>
          <xsl:call-template name="GetLabel">
            <xsl:with-param name="details" select="."/>
            <xsl:with-param name="code" select="false()"/>
          </xsl:call-template>
        </xsl:when>
        <xsl:when test="@type='action'">
          <xsl:attribute name="class">biztax-th-context-action</xsl:attribute>
          <xsl:attribute name="width">50px</xsl:attribute>
          <span>
            <xsl:call-template name="space"/>
          </span>
        </xsl:when>
        <xsl:when test="@type='field'">
          <xsl:attribute name="class">biztax-th-field-header</xsl:attribute>
          <xsl:attribute name="width">100px</xsl:attribute>
          <xsl:call-template name="GetLabel">
            <xsl:with-param name="details" select="."/>
            <xsl:with-param name="code" select="false()"/>
          </xsl:call-template>
        </xsl:when>
        <xsl:when test="@type='dimension'">
          <xsl:attribute name="class">biztax-th-grid-header</xsl:attribute>
          <xsl:attribute name="width">100px</xsl:attribute>
          <xsl:call-template name="GetLabel">
            <xsl:with-param name="details" select="."/>
            <xsl:with-param name="code" select="false()"/>
          </xsl:call-template>
        </xsl:when>
      </xsl:choose>
    </th>
  </xsl:template>
  
  <xsl:template match="*" mode="GetFullId">
    <xsl:param name="period"/><xsl:param name="context"/><xsl:value-of select="@id"/>_<xsl:value-of select="$period"/><xsl:if test="$context">
      __<xsl:value-of select="$context"/>
    </xsl:if>
  </xsl:template>
  <xsl:template match="fiche|panel" mode="CHILDREN_OLD">
    <xsl:param name="table" select="0"/>
    <xsl:variable name="fieldCount" select="count(children/field)"/>
    <xsl:choose>
      <xsl:when test="$table=0">
        <div>
          <div class="biztax-title">
            <xsl:call-template name="GetLabel">
              <xsl:with-param name="details" select="details"/>
              <xsl:with-param name="code" select="false()"/>
            </xsl:call-template>
          </div>
          <xsl:choose>
            <xsl:when test="$fieldCount=0">
              <xsl:apply-templates select="children/panel" mode="CHILDREN">
                <xsl:with-param name="table" select="0"/>
              </xsl:apply-templates>
            </xsl:when>
            <xsl:otherwise>
              <xsl:choose>
                <xsl:when test="not(@dimensionSet) or @dimensionsSet='00000000-0000-0000-0000-000000000000'">
                  <table>
                    <tr>
                      <th>
                        <span>
                          <xsl:call-template name="space"/>
                        </span>
                      </th>
                      <th colspan="2">Codes</th>
                      <xsl:if test="details/presperiods/string/text()='I-Start'">
                        <th>Bij het begin van het belastbare tijdperk</th>
                      </xsl:if>
                      <xsl:choose>
                        <xsl:when test="details/presperiods/string/text()='I-End'">
                          <th>Bij het einde van het belastbare tijdperk</th>
                        </xsl:when>
                        <xsl:when test="details/presperiods/string/text()='D'">
                          <th>Belastbaar tijdperk</th>
                        </xsl:when>
                      </xsl:choose>
                    </tr>
                    <xsl:variable name="fieldcels">
                      <xsl:choose>
                        <xsl:when test="details/presperiods/string/text()='I-End' and details/presperiods/string/text()='D'">
                          <xsl:value-of select="count(details/presperiods/string)-1"/>
                        </xsl:when>
                        <xsl:otherwise>
                          <xsl:value-of select="count(details/presperiods/string)"/>
                        </xsl:otherwise>
                      </xsl:choose>
                    </xsl:variable>
                    <xsl:variable name="perds">
                      <periods>
                        <xsl:if test="details/presperiods/string/text()='I-Start'">
                          <I-Start/>
                        </xsl:if>
                        <xsl:choose>
                          <xsl:when test="details/presperiods/string/text()='I-End'">
                            <I-End/>
                          </xsl:when>
                          <xsl:when test="details/presperiods/string/text()='D'">
                            <D/>
                          </xsl:when>
                        </xsl:choose>
                      </periods>
                    </xsl:variable>
                    <xsl:apply-templates select="children/panel|children/field" >
                      <xsl:with-param name="table" select="number($fieldcels)"/>
                      <xsl:with-param name="periods" select="msxsl:node-set($perds)"/>
                    </xsl:apply-templates>
                  </table>
                </xsl:when>
                <xsl:otherwise>
                  <contextgrid/>
                </xsl:otherwise>
              </xsl:choose>
            </xsl:otherwise>
          </xsl:choose>
        </div>
      </xsl:when>
      <xsl:otherwise>TODO</xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  <xsl:template name="GetLabel">
    <xsl:param name="details"/>
    <xsl:param name="code"/>
    <xsl:if test="$code">
      <xsl:value-of select="$details/@code"/> -
    </xsl:if>
    <xsl:value-of select="$details/labels/Label[Language/text()=$lang]/Text"/>
  </xsl:template>
  <xsl:template name="space">
    <space/>
  </xsl:template>
</xsl:stylesheet>