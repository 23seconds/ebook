﻿{xtype:'currencyfield'
  ,currencySymbol: '€'
  ,useThousandSeparator: true
  ,thousandSeparator: '.'
  ,alwaysDisplayDecimals: true
  ,decimalSeparator:','
  //,decimalPrecision:2
  ,<!RESTRICTIONS!>
}
