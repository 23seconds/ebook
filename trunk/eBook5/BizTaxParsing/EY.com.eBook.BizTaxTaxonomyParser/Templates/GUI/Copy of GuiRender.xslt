﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl"
>
    <xsl:output method="xml" indent="yes" encoding="utf-8"/>
  <xsl:param name="lang">nl</xsl:param>

  <xsl:template match="fiche">
    <div id="{details/@code}_cnt">
    <h1>
      <xsl:call-template name="GetLabel">
        <xsl:with-param name="details" select="details"/>
        <xsl:with-param name="code" select="true()"/>
      </xsl:call-template>
    </h1>

      <xsl:for-each select="children/*">
        <xsl:apply-templates select=".">
          
        </xsl:apply-templates>
      </xsl:for-each>
      
     
    </div>
  </xsl:template>

  <xsl:template match="panel__">
    <xsl:param name="pnlConfig"/>

    <xsl:if test="not($pnlConfig)">
    <div id="{@id}_cnt"  class="biztax-panel">
      <div id="{@id}" class="biztax-title">
        <xsl:call-template name="GetLabel">
          <xsl:with-param name="details" select="details"/>
          <xsl:with-param name="code" select="false()"/>
        </xsl:call-template>
      </div>
      <div id="{@id}_children" class="biztax-panel-children">
        <xsl:choose>
          <xsl:when test="@hasFields='false'">
              <xsl:apply-templates select="children/*">

              </xsl:apply-templates>
          </xsl:when>
          <xsl:otherwise>
            <!-- construct table -->
            <xsl:apply-templates select="." mode="CREATETABLE"></xsl:apply-templates>
          </xsl:otherwise>
        </xsl:choose>
       
      </div>
    </div>
    </xsl:if>
  </xsl:template>

  <xsl:template name="createPanelConfig">
    <xsl:param name="pnl"/>
    <config>
      <presPeriods>
        <xsl:if test="$pnl/details/presperiods/string/text()='I-Start'">
          <I-Start/>
        </xsl:if>
        <xsl:if test="$pnl/details/presperiods/string/text()='I-End'">
          <I-End/>
        </xsl:if>
        <xsl:if test="$pnl/details/presperiods/string/text()='D' and count($pnl/details/presperiods/string[./text()='I-Start' or ./text()='I-End'])=0">
          <D/>
        </xsl:if>
      </presPeriods>
      <dimensionSets>
        <xsl:for-each select="$pnl/children/*[not(@dimensionSet=preceding-sibling::*/@dimensionSet)]">
          <xsl:if test="@dimensionSet and not(@dimensionSet='00000000-0000-0000-0000-000000000000')">
            <dimensionSet>
              <xsl:value-of select="@dimensionSet"/>
            </dimensionSet>
          </xsl:if>
        </xsl:for-each>
      </dimensionSets>
      <oldCode>
        <xsl:value-of select="count($pnl/children//details[@oldcode])>0"/>
      </oldCode>
      <code>
        <xsl:value-of select="count($pnl/children//details[@code])>0"/>
      </code>
      <type>
        <xsl:choose>
          <xsl:when test="count($pnl/children/*/details/Scenario[@type])=0">normal</xsl:when>
          <xsl:otherwise>
            <xsl:for-each select="$pnl/children/*/details/Scenario[@type and not(@type=preceding-sibling::*/details/Scenario/@type)]">
              <type>
                <xsl:value-of select="@type"/>
              </type>
            </xsl:for-each>
          </xsl:otherwise>
        </xsl:choose>
      </type>

    </config>
  </xsl:template>

  <xsl:template match="panel[@type='grid']" mode="CREATETABLE">
    <xsl:variable name="thisPnlConfig">
          <xsl:call-template name="createPanelConfig">
            <xsl:with-param name="pnl" select="."/>
          </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="thisPnlConfigXml" select="msxsl:node-set($thisPnlConfig)"/>
    <table>
      <tr>
        <th>
          <span>
            <xsl:call-template name="space"/>
          </span>
        </th>
        <xsl:if test="$thisPnlConfigXml//code[./text()='true']">
          <th>
            <xsl:choose>
              <xsl:when test="$thisPnlConfigXml//oldCode[./text()='true']">
                <xsl:attribute name="colspan">2</xsl:attribute>CODES
              </xsl:when>
              <xsl:otherwise>CODE</xsl:otherwise>
            </xsl:choose>
          </th>
        </xsl:if>
        <xsl:variable name="dimensions">
          <xsl:choose>
            <xsl:when test="details/Scenario/dimension">
              <xsl:copy-of select="details/Scenario/dimension"/>
            </xsl:when>
            <xsl:otherwise>
              <xsl:copy-of select="children/*[count(./details/Scenario/dimension)>0][1]/details/Scenario/dimension"/>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:variable>
        <xsl:variable name="dimensionsXml" select="msxsl:node-set($dimensions)"/>
        <xsl:for-each select="$dimensionsXml/dimension/dimensionfield/list/item">
          <th>
            <xsl:call-template name="GetLabel">
              <xsl:with-param name="details" select="."/>
              <xsl:with-param name="code" select="false()"/>
            </xsl:call-template>
          </th>
        </xsl:for-each>
      </tr>
    </table>
  </xsl:template>

  <xsl:template match="panel[@type='normal']" mode="CREATETABLE">
    <xsl:param name="pnlConfig"/>
    <xsl:variable name="thisPnlConfig">
      <xsl:choose>
        <xsl:when test="$pnlConfig"></xsl:when>
        <xsl:otherwise>
          <xsl:call-template name="createPanelConfig">
            <xsl:with-param name="pnl" select="."/>
          </xsl:call-template>
          <!--config>
            <presPeriods>
              <xsl:if test="details/presperiods/string/text()='I-Start'">
                <I-Start/>
              </xsl:if>
              <xsl:if test="details/presperiods/string/text()='I-End'">
                <I-End/>
              </xsl:if>
              <xsl:if test="details/presperiods/string/text()='D' and count(details/presperiods/string[./text()='I-Start' or ./text()='I-End'])=0">
                <D/>
              </xsl:if>
            </presPeriods>
            <dimensionSets>
              <xsl:for-each select="children/*[not(@dimensionSet=preceding-sibling::*/@dimensionSet)]">
                <xsl:if test="@dimensionSet and not(@dimensionSet='00000000-0000-0000-0000-000000000000')">
                  <dimensionSet>
                    <xsl:value-of select="@dimensionSet"/>
                  </dimensionSet>
                </xsl:if>
              </xsl:for-each>
            </dimensionSets>
            <oldCode>
              <xsl:value-of select="count(children//details[@oldcode])>0"/>
            </oldCode>
            <code>
              <xsl:value-of select="count(children//details[@code])>0"/>
            </code>
            <type>
              <xsl:choose>
                <xsl:when test="count(children/*/details/Scenario[@type])=0">normal</xsl:when>
                <xsl:otherwise>
                  <xsl:for-each select="children/*/details/Scenario[@type and not(@type=preceding-sibling::*/details/Scenario/@type)]">
                    <type>
                      <xsl:value-of select="@type"/>
                    </type>
                  </xsl:for-each>
                </xsl:otherwise>
              </xsl:choose>
            </type>

          </config-->
        </xsl:otherwise>
      </xsl:choose>

    </xsl:variable>
    <xsl:variable name="thisPnlConfigXml" select="msxsl:node-set($thisPnlConfig)"/>
    
    <table>
      <tr>
        <th>
          <span>
            <xsl:call-template name="space"/>
          </span>
        </th>
        <xsl:if test="$thisPnlConfigXml//code[./text()='true']">
          <th>
            <xsl:choose>
              <xsl:when test="$thisPnlConfigXml//oldCode[./text()='true']">
                <xsl:attribute name="colspan">2</xsl:attribute>CODES
              </xsl:when>
              <xsl:otherwise>CODE</xsl:otherwise>
            </xsl:choose>
          </th>
        </xsl:if>
        <xsl:for-each select="$thisPnlConfigXml//presPeriods/*">
          <xsl:if test="name(.)='I-Start'">
            <th>Bij het begin van het belastbare tijdperk</th>
          </xsl:if>
          <xsl:if test="name(.)='I-End'">
            <th>Bij het einde van het belastbare tijdperk</th>
          </xsl:if>
          <xsl:if test="name(.)='D'">
            <th>Belastbaar tijdperk</th>
          </xsl:if>
        </xsl:for-each>
      </tr>

      <xsl:apply-templates select="." mode="RENDERCHILDREN">
        <xsl:with-param name="pnlConfig" select="$thisPnlConfigXml"/>
      </xsl:apply-templates>
     
    </table>
  </xsl:template>

  <xsl:template match="panel" mode="RENDERCHILDREN">
    <xsl:param name="pnlConfig"/>
    <xsl:variable name="thisPnlConfigXml" select="msxsl:node-set($pnlConfig)"/>
    
    <xsl:for-each select="children/*">
      <xsl:choose>
        <xsl:when test="name(.)='field'">
          <tr>
            <td>
              <xsl:call-template name="GetLabel">
                <xsl:with-param name="details" select="details"/>
                <xsl:with-param name="code" select="false()"/>
              </xsl:call-template>
            </td>
            <xsl:if test="$thisPnlConfigXml//oldCode[./text()='true']">
              <td>
                <xsl:value-of select="details/@oldcode"/>
              </td>
            </xsl:if>
            <td>
              <xsl:value-of select="details/@code"/>
            </td>
            <xsl:variable name="me" select="."/>
            <xsl:for-each select="$thisPnlConfigXml//presPeriods/*">
              <xsl:variable name="per">
                <xsl:choose>
                  <xsl:when test="name(.)='I-End' and not($me/details/presperiods/string[./text()='I-End']) and $me/details/presperiods/string[./text()='D']">D</xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="name(.)"/>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:variable>
              <xsl:variable name="period">
                <xsl:choose>
                  <xsl:when test="$me/details/periods/string[./text()=$per]">
                    <xsl:value-of select="$per"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="$me/details/periods/string[1]/text()"/>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:variable>
              <td>
                <xsl:if test="$me/details/presperiods/string[./text()=$per]">
                  <input type="text">
                    <xsl:attribute name="data-xbrl-id">
                      <xsl:apply-templates select="$me" mode="GetFullId">
                        <xsl:with-param name="period" select="$period"/>
                      </xsl:apply-templates>
                    </xsl:attribute>
                    <xsl:attribute name="data-xbrl-period">
                      <xsl:value-of select="$period"/>
                    </xsl:attribute>
                    <xsl:attribute name="data-xbrl-name">
                      <xsl:value-of select="$me/@id"/>
                    </xsl:attribute>
                    <xsl:attribute name="data-xbrl-dimensionset">
                      <xsl:value-of select="$me/@dimensionSet"/>
                    </xsl:attribute>
                  </input>
                </xsl:if>
              </td>
            </xsl:for-each>
          </tr>
        </xsl:when>
        <xsl:when test="name(.)='panel'">
          <!-- todo, is abstract or calculated? -->
          <tr>
            <td>
              <xsl:attribute name="colspan">
                <xsl:value-of select="count($thisPnlConfigXml//presPeriods/*) + 2 + count($thisPnlConfigXml//oldCode[./text()='true'])"/>
              </xsl:attribute>
              <xsl:call-template name="GetLabel">
                <xsl:with-param name="details" select="details"/>
                <xsl:with-param name="code" select="false()"/>
              </xsl:call-template>
            </td>

          </tr>

          <xsl:choose>
            <xsl:when test="@type='contextgrid' and not(./@type=../../@type)">
              <xsl:choose>
                <xsl:when test="count(children/field[1]/details/Scenario/dimension[@type='TypedDimension'])=1 and count(children/field)=1">
                  <!-- render inline context grid-->
                  <contextgrid>
                    <bodyTpl>
                      <tr>
                        <td>contextfield</td>
                        <xsl:if test="$thisPnlConfigXml//oldCode[./text()='true']">
                          <td>
                            <xsl:value-of select="details/@oldcode"/>
                          </td>
                        </xsl:if>
                        <td>
                          <xsl:value-of select="details/@code"/>
                        </td>
                      <xsl:variable name="me" select="children/field[1]"/>
                        <xsl:for-each select="$thisPnlConfigXml//presPeriods/*">
                          <xsl:variable name="per">
                            <xsl:choose>
                              <xsl:when test="name(.)='I-End' and not($me/details/presperiods/string[./text()='I-End']) and $me/details/presperiods/string[./text()='D']">D</xsl:when>
                              <xsl:otherwise>
                                <xsl:value-of select="name(.)"/>
                              </xsl:otherwise>
                            </xsl:choose>
                          </xsl:variable>
                          <xsl:variable name="period">
                            <xsl:choose>
                              <xsl:when test="$me/details/periods/string[./text()=$per]">
                                <xsl:value-of select="$per"/>
                              </xsl:when>
                              <xsl:otherwise>
                                <xsl:value-of select="$me/details/periods/string[1]/text()"/>
                              </xsl:otherwise>
                            </xsl:choose>
                          </xsl:variable>
                          <td>
                            <xsl:if test="$me/details/presperiods/string[./text()=$per]">
                              <input type="text">
                                <xsl:attribute name="data-xbrl-id">
                                  <xsl:apply-templates select="$me" mode="GetFullId">
                                    <xsl:with-param name="period" select="$period"/>
                                  </xsl:apply-templates>
                                </xsl:attribute>
                                <xsl:attribute name="data-xbrl-period">
                                  <xsl:value-of select="$period"/>
                                </xsl:attribute>
                                <xsl:attribute name="data-xbrl-name">
                                  <xsl:value-of select="$me/@id"/>
                                </xsl:attribute>
                                <xsl:attribute name="data-xbrl-dimensionset">
                                  <xsl:value-of select="$me/@dimensionSet"/>
                                </xsl:attribute>
                              </input>
                            </xsl:if>
                          </td>
                        </xsl:for-each>
                      </tr>
                    </bodyTpl>
                    <buttonTpl>
                      <tr>
                        <td>
                          <xsl:attribute name="colspan">
                            <xsl:value-of select="count($thisPnlConfigXml//presPeriods/*) + 2 + count($thisPnlConfigXml//oldCode[./text()='true'])"/>
                          </xsl:attribute>
                          ADD / REMOVE SELECTED
                        </td>
                      </tr>
                    </buttonTpl>
                  </contextgrid>
                 
                </xsl:when>
              </xsl:choose>
            </xsl:when>
            <xsl:when test="@type='grid' and not(@type=../../@type)">
              <tr>
                <td>
                  <xsl:attribute name="colspan">
                    <xsl:value-of select="count($thisPnlConfigXml//presPeriods/*) + 2 + count($thisPnlConfigXml//oldCode[./text()='true'])"/>
                  </xsl:attribute>
                  <xsl:apply-templates select="." mode="CREATETABLE"/>
                </td>
              </tr>
            </xsl:when>
            <xsl:otherwise>
              <xsl:apply-templates select="." mode="RENDERCHILDREN">
                <xsl:with-param name="pnlConfig" select="$thisPnlConfigXml"/>
              </xsl:apply-templates>
            </xsl:otherwise>
          </xsl:choose>

        </xsl:when>
      </xsl:choose>


    </xsl:for-each>
  </xsl:template>
  
  <!--xsl:template match="*"></xsl:template>-->

  <xsl:template match="fiche" mode="CHILDREN">
    <xsl:choose>
      <xsl:when test="count(children/field)=0">
        <xsl:apply-templates select="children/panel" mode="TABLELESS">
          
        </xsl:apply-templates>
      </xsl:when>
      <xsl:otherwise>
        <xsl:apply-templates select="." mode="TABLE">
          
        </xsl:apply-templates>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="fiche" mode="TABLE">
    <table>
      
    </table>
  </xsl:template>

  <xsl:template match="panel" mode="TABLELESS">
    <xsl:param name="pnlConfig"/>
    <div id="{@id}_cnt"  class="biztax-panel">
      <div id="{@id}" class="biztax-title">
        <xsl:call-template name="GetLabel">
          <xsl:with-param name="details" select="details"/>
          <xsl:with-param name="code" select="false()"/>
        </xsl:call-template>
      </div>
      <div id="{@id}_children" class="biztax-panel-children">
        <xsl:apply-templates select="." mode="CHILDREN">
        </xsl:apply-templates>
      </div>
    </div>
  </xsl:template>

  <xsl:template match="panel" mode="CHILDREN">
    <xsl:param name="pnlConfig"/>

    
    <xsl:choose>
      <xsl:when test="count(children/field)=0">
        <xsl:apply-templates select="children/panel" mode="TABLELESS">

        </xsl:apply-templates>
      </xsl:when>
      <xsl:otherwise>
        
        <xsl:apply-templates select="." mode="TABLE">

        </xsl:apply-templates>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="panel" mode="TABLE">
    <xsl:param name="pnlConfig"/>
    <xsl:variable name="thisPnlConfig">
      <xsl:choose>
        <xsl:when test="$pnlConfig"></xsl:when>
        <xsl:otherwise>
          <config>
            <presPeriods>
              <xsl:if test="details/presperiods/string/text()='I-Start'">
                <I-Start/>
              </xsl:if>
              <xsl:if test="details/presperiods/string/text()='I-End'">
                <I-End/>
              </xsl:if>
              <xsl:if test="details/presperiods/string/text()='D' and count(details/presperiods/string[./text()='I-Start' or ./text()='I-End'])=0">
                <D/>
              </xsl:if>
            </presPeriods>
            <dimensionSets>
              <xsl:for-each select="children/*[not(@dimensionSet=preceding-sibling::*/@dimensionSet)]">
                <xsl:if test="@dimensionSet and not(@dimensionSet='00000000-0000-0000-0000-000000000000')">
                  <dimensionSet>
                    <xsl:value-of select="@dimensionSet"/>
                  </dimensionSet>
                </xsl:if>
              </xsl:for-each>
            </dimensionSets>
            <oldCode>
              <xsl:value-of select="count(children//details[@oldcode])>0"/>
            </oldCode>
            <code>
              <xsl:value-of select="count(children//details[@code])>0"/>
            </code>
            <type>
              <xsl:choose>
                <xsl:when test="count(children/*/details/Scenario[@type])=0">normal</xsl:when>
                <xsl:otherwise>
                  <xsl:for-each select="children/*/details/Scenario[@type and not(@type=preceding-sibling::*/details/Scenario/@type)]">
                    <type>
                      <xsl:value-of select="@type"/>
                    </type>
                  </xsl:for-each>
                </xsl:otherwise>
              </xsl:choose>
            </type>
            
          </config>
        </xsl:otherwise>
      </xsl:choose>
      
    </xsl:variable>
    <xsl:variable name="thisPnlConfigXml" select="msxsl:node-set($thisPnlConfig)"/>
    <xsl:if test="not($pnlConfig)">
      <!--xsl:copy-of select="$thisPnlConfigXml"/-->
    <table>
      <tr>
        <th>
          <span>
            <xsl:call-template name="space"/>
          </span>
        </th>
        <xsl:if test="$thisPnlConfigXml//code[./text()='true']">
        <th>
          <xsl:choose>
            <xsl:when test="$thisPnlConfigXml//oldCode[./text()='true']"><xsl:attribute name="colspan">2</xsl:attribute>CODES</xsl:when>
            <xsl:otherwise>CODE</xsl:otherwise>
          </xsl:choose>
        </th>
        </xsl:if>
        <xsl:for-each select="$thisPnlConfigXml//presPeriods/*">
          <xsl:if test="name(.)='I-Start'">
            <th>Bij het begin van het belastbare tijdperk</th>
          </xsl:if>
          <xsl:if test="name(.)='I-End'">
            <th>Bij het einde van het belastbare tijdperk</th>
          </xsl:if>
          <xsl:if test="name(.)='D'">
            <th>Belastbaar tijdperk</th>
          </xsl:if>
        </xsl:for-each>
      </tr>
      <xsl:apply-templates select="children/*[name(.)='panel' or name(.)='field']">
        <xsl:with-param name="pnlConfig" select="$thisPnlConfigXml"/>
      </xsl:apply-templates>
    </table>
    </xsl:if>
  </xsl:template>

  <xsl:template match="panel_">
    <xsl:param name="pnlConfig"/>
    <xsl:variable name="pnlConfigXml" select="msxsl:node-set($pnlConfig)"/>
    <xsl:variable name="thisPnlConfig">
      <config>
            <presPeriods>
              <xsl:if test="details/presperiods/string/text()='I-Start'">
                <I-Start/>
              </xsl:if>
              <xsl:if test="details/presperiods/string/text()='I-End'">
                <I-End/>
              </xsl:if>
              <xsl:if test="details/presperiods/string/text()='D' and count(details/presperiods/string[./text()='I-Start' or ./text()='I-End'])=0">
                <D/>
              </xsl:if>
            </presPeriods>
            <dimensionSets>
              <xsl:for-each select="children/*[not(@dimensionSet=preceding-sibling::*/@dimensionSet)]">
                <xsl:if test="@dimensionSet and not(@dimensionSet='00000000-0000-0000-0000-000000000000')">
                  <dimensionSet>
                    <xsl:value-of select="@dimensionSet"/>
                  </dimensionSet>
                </xsl:if>
              </xsl:for-each>
            </dimensionSets>
            <oldCode>
              <xsl:value-of select="count(children//details[@oldcode])>0"/>
            </oldCode>
            <code>
              <xsl:value-of select="count(children//details[@code])>0"/>
            </code>
            <type>
              <xsl:choose>
                <xsl:when test="count(children/*/details/Scenario[@type])=0">normal</xsl:when>
                <xsl:otherwise>
                  <xsl:for-each select="children/*/details/Scenario[@type and not(@type=preceding-sibling::*/details/Scenario/@type)]">
                    <type>
                      <xsl:value-of select="@type"/>
                    </type>
                  </xsl:for-each>
                </xsl:otherwise>
              </xsl:choose>
            </type>

          </config>
       
    </xsl:variable>
    <xsl:variable name="thisPnlConfigXml" select="msxsl:node-set($thisPnlConfig)"/>
    <tr>
      <td>
        <xsl:attribute name="colspan">
          <xsl:value-of select="count($pnlConfigXml//presPeriods/*) + 2 + count($pnlConfigXml//oldCode[./text()='true'])"/>
        </xsl:attribute>
        <xsl:call-template name="GetLabel">
          <xsl:with-param name="details" select="details"/>
          <xsl:with-param name="code" select="false()"/>
        </xsl:call-template>
      </td>
    </tr>
    <xsl:copy-of select="$thisPnlConfigXml"/>
    <xsl:choose>
      <xsl:when test="$thisPnlConfigXml//type/text() = 'contextgrid' and not($thisPnlConfigXml//type/text() = $pnlConfigXml//type/text()) ">
        
      </xsl:when>
    </xsl:choose>
    <tr>
      <td>
        <xsl:attribute name="colspan">
          <xsl:value-of select="count($pnlConfigXml//presPeriods/*) + 2 + count($pnlConfigXml//oldCode[./text()='true'])"/>
        </xsl:attribute>
        VELDEN
      </td>
    </tr>
  </xsl:template>

  <xsl:template match="field">
    <xsl:param name="pnlConfig"/>
    <xsl:variable name="pnlConfigXml" select="msxsl:node-set($pnlConfig)"/>
    <tr>
      <td>
        <xsl:call-template name="GetLabel">
          <xsl:with-param name="details" select="details"/>
          <xsl:with-param name="code" select="false()"/>
        </xsl:call-template>
      </td>
     
        <xsl:if test="$pnlConfigXml//oldCode[./text()='true']">
          <td>
            <xsl:value-of select="details/@oldcode"/>
          </td>
        </xsl:if>
      <td>
        <xsl:value-of select="details/@code"/>
      </td>
      <xsl:variable name="me" select="."/>
      <xsl:for-each select="$pnlConfigXml//presPeriods/*">
        <xsl:if test="name(.)='I-Start'">
          <td>
            <xsl:if test="$me/details/presperiods/string[./text()='I-Start']">
              <input type="text">
                <xsl:attribute name="data-xbrl-id">
                  <xsl:apply-templates select="$me" mode="GetFullId">
                    <xsl:with-param name="period">
                      <xsl:choose>
                        <xsl:when test="$me/details/presperiods/string[./text()='I-Start']">I-Start</xsl:when>
                        <xsl:otherwise>D</xsl:otherwise>
                      </xsl:choose>
                    </xsl:with-param>
                  </xsl:apply-templates>
                </xsl:attribute>
                <xsl:attribute name="data-xbrl-period">
                  <xsl:choose>
                    <xsl:when test="$me/details/presperiods/string[./text()='I-Start']">I-Start</xsl:when>
                    <xsl:otherwise>D</xsl:otherwise>
                  </xsl:choose>
                </xsl:attribute>
                <xsl:attribute name="data-xbrl-name">
                  <xsl:value-of select="$me/@id"/>
                </xsl:attribute>
                <xsl:attribute name="data-xbrl-dimensionset">
                  <xsl:value-of select="$me/@dimensionSet"/>
                </xsl:attribute>
              </input>
            </xsl:if>
          </td>
        </xsl:if>
        <xsl:if test="name(.)='I-End'">
          <td>
            <xsl:if test="$me/details/presperiods/string[./text()='I-End'] or $me/details/presperiods/string[./text()='D'] ">
              <input type="text">
                <xsl:attribute name="data-xbrl-id">
                <xsl:apply-templates select="$me" mode="GetFullId">
                  <xsl:with-param name="period">
                    <xsl:choose>
                      <xsl:when test="$me/details/presperiods/string[./text()='I-End']">I-End</xsl:when>
                      <xsl:otherwise>D</xsl:otherwise>
                    </xsl:choose>
                  </xsl:with-param>
                </xsl:apply-templates>
                </xsl:attribute>
                <xsl:attribute name="data-xbrl-period">
                  <xsl:choose>
                    <xsl:when test="$me/details/presperiods/string[./text()='I-End']">I-End</xsl:when>
                    <xsl:otherwise>D</xsl:otherwise>
                  </xsl:choose>
                </xsl:attribute>
                <xsl:attribute name="data-xbrl-name">
                  <xsl:value-of select="$me/@id"/>
                </xsl:attribute>
                <xsl:attribute name="data-xbrl-dimensionset">
                  <xsl:value-of select="$me/@dimensionSet"/>
                </xsl:attribute>
              </input>
            </xsl:if>
          </td>
        </xsl:if>
        <xsl:if test="name(.)='D'">
          <td>
            <xsl:if test="$me/details/presperiods/string[./text()='D']">
              <input type="text">
                <xsl:attribute name="data-xbrl-id">
                  <xsl:apply-templates select="$me" mode="GetFullId">
                    <xsl:with-param name="period" select="'D'">
                    </xsl:with-param>
                  </xsl:apply-templates>
                </xsl:attribute>
                <xsl:attribute name="data-xbrl-period">D</xsl:attribute>
                <xsl:attribute name="data-xbrl-name">
                  <xsl:value-of select="$me/@id"/>
                </xsl:attribute>
                <xsl:attribute name="data-xbrl-dimensionset">
                  <xsl:value-of select="$me/@dimensionSet"/>
                </xsl:attribute>
              </input>
            </xsl:if>
          </td>
        </xsl:if>
      </xsl:for-each>
      
    </tr>
  </xsl:template>

  <xsl:template match="field" mode="GetFullId"><xsl:param name="period"/><xsl:param name="dimension"/><xsl:value-of select="@id"/>_<xsl:value-of select="$period"/><xsl:if test="$dimension">_<xsl:value-of select="$dimension"/></xsl:if></xsl:template>

  <xsl:template match="fiche|panel" mode="CHILDREN_OLD">
    <xsl:param name="table" select="0"/>
    <xsl:variable name="fieldCount" select="count(children/field)"/>
    <xsl:choose>
      <xsl:when test="$table=0">
        <div>
          <div class="biztax-title">
            <xsl:call-template name="GetLabel">
              <xsl:with-param name="details" select="details"/>
              <xsl:with-param name="code" select="false()"/>
            </xsl:call-template>
          </div>
          <xsl:choose>
            <xsl:when test="$fieldCount=0">

              <xsl:apply-templates select="children/panel" mode="CHILDREN">
                <xsl:with-param name="table" select="0"/>
              </xsl:apply-templates>

            </xsl:when>
            <xsl:otherwise>
              <xsl:choose>
                <xsl:when test="not(@dimensionSet) or @dimensionsSet='00000000-0000-0000-0000-000000000000'">
                  <table>
                    <tr>
                      <th>
                        <span>
                          <xsl:call-template name="space"/>
                        </span>
                      </th>
                      <th colspan="2">Codes</th>
                      <xsl:if test="details/presperiods/string/text()='I-Start'">
                        <th>Bij het begin van het belastbare tijdperk</th>
                      </xsl:if>
                      <xsl:choose>
                        <xsl:when test="details/presperiods/string/text()='I-End'">
                          <th>Bij het einde van het belastbare tijdperk</th>
                        </xsl:when>
                        <xsl:when test="details/presperiods/string/text()='D'">
                          <th>Belastbaar tijdperk</th>
                        </xsl:when>
                      </xsl:choose>
                    
                    </tr>

                    <xsl:variable name="fieldcels">
                      <xsl:choose>
                        <xsl:when test="details/presperiods/string/text()='I-End' and details/presperiods/string/text()='D'">
                          <xsl:value-of select="count(details/presperiods/string)-1"/>
                        </xsl:when>
                        <xsl:otherwise>
                          <xsl:value-of select="count(details/presperiods/string)"/>
                        </xsl:otherwise>
                      </xsl:choose>
                    </xsl:variable>
                    <xsl:variable name="perds">
                      <periods>
                        <xsl:if test="details/presperiods/string/text()='I-Start'">
                          <I-Start/>
                        </xsl:if>
                        <xsl:choose>
                          <xsl:when test="details/presperiods/string/text()='I-End'">
                            <I-End/>
                          </xsl:when>
                          <xsl:when test="details/presperiods/string/text()='D'">
                            <D/>
                          </xsl:when>
                        </xsl:choose>
                      </periods>
                    </xsl:variable>
                    <xsl:apply-templates select="children/panel|children/field" >
                      <xsl:with-param name="table" select="number($fieldcels)"/>
                      <xsl:with-param name="periods" select="msxsl:node-set($perds)"/>
                    </xsl:apply-templates>
                  </table>
                </xsl:when>
                <xsl:otherwise>
                  <contextgrid/>
                </xsl:otherwise>
              </xsl:choose>
            </xsl:otherwise>
          </xsl:choose>
        </div>
      </xsl:when>
      <xsl:otherwise>TODO</xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="panelOLD">
    <xsl:param name="table"/>
    <xsl:param name="periods"/>
    <xsl:choose>
      <xsl:when test="$table=0">
        <div>
        <div class="biztax-title">
          <xsl:call-template name="GetLabel">
            <xsl:with-param name="details" select="details"/>
            <xsl:with-param name="code" select="false()"/>
          </xsl:call-template>
        </div>
          <xsl:apply-templates select="." mode="CHILDREN">
          </xsl:apply-templates>
        </div>
      </xsl:when>
      <xsl:otherwise>
        <tr>
          <td colspan="{$table}">
            <span class="biztax-subtitle">
              <xsl:call-template name="GetLabel">
                <xsl:with-param name="details" select="details"/>
                <xsl:with-param name="code" select="false()"/>
              </xsl:call-template>
            </span>
          </td>
        </tr>
        <tr>
         
        </tr>
      </xsl:otherwise>
    </xsl:choose>
    
  </xsl:template>

  <xsl:template match="fieldOLD">
    <xsl:param name="table"/>
    <xsl:param name="periods"/>
        <tr>
          <td>
            <xsl:call-template name="GetLabel">
              <xsl:with-param name="details" select="details"/>
              <xsl:with-param name="false" select="true()"/>
            </xsl:call-template>
          </td>
          <td>
            <xsl:value-of select="details/@oldcode"/>
          </td>
          <td>
            <xsl:value-of select="details/@code"/>
          </td>
          <xsl:for-each select="$periods/*/*">
            <td>
              <xsl:value-of select="name(.)"/>
            </td>
          </xsl:for-each>
         
        </tr>
      

  </xsl:template>

  <xsl:template name="GetLabel">
    <xsl:param name="details"/>
    <xsl:param name="code"/>
    <xsl:if test="$code"><xsl:value-of select="$details/@code"/> - </xsl:if>
    <xsl:value-of select="$details/labels/Label[Language/text()=$lang]/Text"/>
  </xsl:template>

  <xsl:template name="space">
    <xsl:text disable-output-escaping="yes">&amp;#160;</xsl:text>
  </xsl:template>
</xsl:stylesheet>
