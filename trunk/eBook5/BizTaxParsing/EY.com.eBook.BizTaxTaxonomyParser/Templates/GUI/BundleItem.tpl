﻿title = culture == "nl-BE" ? "<!CODE!> - <!NL!>" : culture == "fr-FR" ? "<!CODE!> - <!FR!>" : "<!CODE!> - <!EN!>";
list.Add(new LibraryTreeNodeDataContract
{
    Id = "biztax_fiche_<!CODE!>"
    ,
    Text = title
    ,
    AllowDrag = true
    ,
    AllowChildren = false
    ,
    Class = ""
    ,
    IconCls = "eBook-bundle-tree-biztax"
    ,
    IndexItem = new BizTaxFicheDataContract
        {
            FooterConfig = nofooter
            ,
            HeaderConfig = noheader
            ,
            Id = "BIZTAX_FICHE_<!CODE!>".DeterministicGuid()
            ,
            ShowInIndex = false
            ,
            Title = title
            ,
            IconCls = "eBook-bundle-tree-biztax"
			, Fiche = "<!CODE!>"
        }
    ,
    Leaf = true
});