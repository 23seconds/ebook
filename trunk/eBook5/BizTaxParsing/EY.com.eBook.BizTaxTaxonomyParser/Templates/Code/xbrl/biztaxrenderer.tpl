﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;
using EY.com.eBook.BizTax.Contracts;

namespace EY.com.eBook.BizTax.AY<!ASSESSMENTYEAR!>.<!BIZTAXTYPELOWER!>
{
    public class BizTaxRendererGenerated : EY.com.eBook.BizTax.BizTaxRenderer
    {

	internal CultureInfo DefaultCulture = new CultureInfo("nl-BE");

	#region DefaultContexts
	 public override void ApplyDefaultContexts(string identifier, DateTime periodStart, DateTime periodEnd)
    {
        base.ApplyDefaultContexts(identifier, periodStart, periodEnd);
		<!DEFAULTCONTEXTS!>
		
	}
	#endregion

	#region Global Parameters
		<!GLOBALPARAMS!>
		
	#endregion

		public override BizTaxDataContract Calculate()
        {
			this.Xbrl.Errors = new List<BizTaxErrorDataContract>();
			this.HasErrors=false;
			<!FORMULAONLY_CALLS!>
            return base.Calculate();
        }
		
		public override BizTaxDataContract Validate()
        {
			this.Xbrl.Errors = new List<BizTaxErrorDataContract>();
			this.HasErrors=false;
			<!VALIDATIONONLY_CALLS!>
            return base.Validate();
        }

        public override BizTaxDataContract CalculateAndValidate()
        {
			this.Xbrl.Errors = new List<BizTaxErrorDataContract>();
			this.HasErrors=false;
			Calculate();
            Validate();
            return base.CalculateAndValidate();
        }

	 #region Calculations
	 <!FORMULAONLY!>
	 #endregion

	  #region Validations
	 <!VALIDATIONONLY!>
	 #endregion

    
        
    }
}
