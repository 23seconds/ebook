﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:msxso="urn:schemas-microsoft-com:xslt" xmlns:xso="dummy" exclude-result-prefixes="">
	<xsl:output method="xml" indent="yes" encoding="utf-8" omit-xml-declaration="yes"/>
	<xsl:param name="valuelists"/>
	<xsl:variable name="ctxfontsize" select="8" />
	<xsl:variable name="WorksheetLabelFontsize" select="string('10')" />
	<xsl:variable name="WorksheetFontsize" select="string('10')" />
	<xsl:variable name="WorksheetNumbersize" select="string('8')" />
	<xsl:variable name="WorksheetSmallFontsize" select="string('8')" />
	<xsl:variable name="WorksheetTotalsFontsize" select="string('10')" />
	<xsl:variable name="WorksheetSpacing" select="1.1" />
	<xsl:namespace-alias stylesheet-prefix="xso" result-prefix="xsl"/>
	<xsl:namespace-alias stylesheet-prefix="msxso" result-prefix="msxsl"/>
	<xsl:template match="fiche">
		<xso:stylesheet version="1.0" exclude-result-prefixes="msxsl Helper" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:Helper="urn:Helper">
			<xso:decimal-format name="NonNumber" NaN="0" zero-digit="0"/>
			<xso:variable name="WorksheetLabelFontsize" select="string('10')"/>
			<xso:variable name="WorksheetFontsize" select="string('10')"/>
			<xso:variable name="WorksheetNumbersize" select="string('8')"/>
			<xso:variable name="WorksheetSmallFontsize" select="string('8')"/>
			<xso:variable name="WorksheetTotalsFontsize" select="string('10')"/>
			<xso:variable name="WorksheetSpacing" select="1.1"/>
			<xso:decimal-format name="euro" decimal-separator="," grouping-separator="."/>
			<xso:variable name="NumberFormatting" select="string('#.##0,00')"/>
			<xso:variable name="NumberFormattingNoDigit" select="string('#.##0')"/>
			<xso:variable name="FONT" select="string('C:\WINDOWS\Fonts\EYInterstate-Light.ttf')"/>
			<xso:variable name="xlists">
				<xsl:copy-of select="$valuelists"/>
			</xso:variable>
			<xso:variable name="lists" select="msxsl:node-set($xlists)"/>
			<xso:param name="Culture"/>
			<xso:param name="Binaries"/>
			<xso:template match="BizTaxDataContract">
				<xso:variable name="me" select="."/>
				<Root>
					<xsl:if test="number(@maxcolumns)>6"><xsl:attribute name="orientation">landscape</xsl:attribute>
					</xsl:if>
					<paragraph lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}" fontstyle="bold underline">
						<xsl:call-template name="GetLabel">
							<xsl:with-param name="details" select="details"/>
							<xsl:with-param name="code" select="true()"/>
						</xsl:call-template>
					</paragraph>
					<xsl:choose>
						<xsl:when test="count(children/field)=0">
							<xsl:for-each select="children/*">
								<xsl:apply-templates select="."/>
							</xsl:for-each>
						</xsl:when>
						<xsl:otherwise>
							<xsl:apply-templates select="." mode="CREATETABLE"/>
						</xsl:otherwise>
					</xsl:choose>
				</Root>
			</xso:template>
			<xso:template name="GetTitleDuration">
				<xso:choose>
					<xso:when test="$Culture='nl-BE'">Belastbaar tijdperk</xso:when>
					<xso:when test="$Culture='fr-FR'">Période imposable</xso:when>
					<xso:otherwise>Taxable Era</xso:otherwise>
				</xso:choose>
			</xso:template>
			<xso:template name="GetTitleIStart">
				<xso:choose>
					<xso:when test="$Culture='nl-BE'">Bij het begin van het belastbare tijdperk</xso:when>
					<xso:when test="$Culture='fr-FR'">Au début de la période imposable</xso:when>
					<xso:otherwise>At start of the taxable era</xso:otherwise>
				</xso:choose>
			</xso:template>
			<xso:template name="GetTitleIEnd">
				<xso:choose>
					<xso:when test="$Culture='nl-BE'">Bij het einde van het belastbare tijdperk</xso:when>
					<xso:when test="$Culture='fr-FR'">A la fin de la période imposable</xso:when>
					<xso:otherwise>At end of the taxable era</xso:otherwise>
				</xso:choose>
			</xso:template>
			<xso:template name="GetListValue">
				<xso:param name="xel" />
				<xso:param name="myval" />
				<xso:variable name="value">
					<xso:choose>
						<xso:when test="$xel">
							<xso:value-of select="$xel/FullName"/>
						</xso:when>
						<xso:otherwise>
							<xso:value-of select="$myval"/>
						</xso:otherwise>
					</xso:choose>
				</xso:variable>
				<xso:variable name="ctxlbl" select="$lists//item[@value=$value]"/>
				<xso:choose>
					<xso:when test="$Culture='nl-BE'">
						<xso:value-of select="$ctxlbl/labels/Label[./Language='nl']/Text"/>
					</xso:when>
					<xso:when test="$Culture='fr-FR'">
						<xso:value-of select="$ctxlbl/labels/Label[./Language='fr']/Text"/>
					</xso:when>
					<xso:otherwise>
						<xso:value-of select="$ctxlbl/labels/Label[./Language='en']/Text"/>
					</xso:otherwise>
				</xso:choose>
			</xso:template>
			<xso:template name="BooleanValue">
				<xso:param name="xel" />
				<xso:variable name="value" select ="$xel/Value"/>
				<xso:choose>
					<xso:when test="$value='true'">X</xso:when>
					<xso:when test="$value='false'">/</xso:when>
					<xso:otherwise>&#160;</xso:otherwise>
				</xso:choose>
			</xso:template>
			<xso:template name="BinaryValue">
				<xso:param name="xel" />
				<xso:variable name="value" select ="$xel/BinaryValue"/>
				<xso:choose>
					<xso:when test="not($value) or $value='' or $value='0'">&#160;</xso:when>
					<xso:otherwise>
						<list ordered="false" numbered="false" lettered="false">
							<xso:for-each select="$value/IndexItemBaseDataContract">
								<listitem>- <xso:value-of select="title" />
								</listitem>
							</xso:for-each>
						</list>
					</xso:otherwise>
				</xso:choose>
			</xso:template>
			<xso:template name="MonetaryValue">
				<xso:param name="xel" />
				<xso:variable name="value" select ="$xel/Value"/>
				<xso:choose>
					<xso:when test="not($value) or $value='' or $value='0'">&#160;</xso:when>
					<xso:otherwise>
						<xso:value-of select="format-number($value,$NumberFormatting,'euro')" />€ </xso:otherwise>
				</xso:choose>
			</xso:template>
			<xso:template name="NumberValue">
				<xso:param name="xel" />
				<xso:variable name="value" select ="$xel/Value"/>
				<xso:choose>
					<xso:when test="not($value) or $value='' or $value='0'">&#160;</xso:when>
					<xso:otherwise>
						<xso:value-of select="format-number($value,$NumberFormatting,'euro')" />
					</xso:otherwise>
				</xso:choose>
			</xso:template>
			<xso:template name="PercentValue">
				<xso:param name="xel" />
				<xso:variable name="value" select ="$xel/Value"/>
				<xso:choose>
					<xso:when test="not($value) or $value='' or $value='0'">&#160;</xso:when>
					<xso:otherwise>
						<xso:value-of select="format-number(number($value) * 100,$NumberFormatting,'euro')" />% </xso:otherwise>
				</xso:choose>
			</xso:template>
			<xso:template name="StandardValue">
				<xso:param name="xel" />
				<xso:variable name="value" select ="$xel/Value"/>
				<xso:choose>
					<xso:when test="not($value) or $value='' or $value='0'">&#160;</xso:when>
					<xso:otherwise>
						<xso:value-of select="$value" />
					</xso:otherwise>
				</xso:choose>
			</xso:template>
			<xso:template name="GetContextsOF">
				<xso:param name="prescontext"/>
				<xso:variable name="tmp">
					<xso:for-each select="//ContextElementDataContract[PresContextId=$prescontext]/DefId">
						<xso:sort data-type="text" select="."/>
						<xso:copy-of select="."/>
					</xso:for-each>
				</xso:variable>
				<contextdefs>
					<xso:for-each select="msxsl:node-set($tmp)/DefId">
						<xso:sort data-type="text" select="."/>
						<xso:if test="not(.=preceding-sibling::DefId) or position()=1">
							<defid>
								<xso:value-of select="."/>
							</defid>
						</xso:if>
					</xso:for-each>
				</contextdefs>
			</xso:template>
		</xso:stylesheet>
	</xsl:template>
	<xsl:template match="valueListOrOther">
		<xsl:param name="parentTableInfo"/>
		<xsl:param name="indent" select="0"/>
		<xsl:variable name="me" select="."/>
		<row>
			<cell left="true" bottom="true" top="true" right="true" verticalalign="Top" horizontalalign="left">
				<paragraph lineheight="1.1" size="8">
					<xsl:call-template name="GetLabel">
						<xsl:with-param name="details" select="$me/details"/>
						<xsl:with-param name="code" select="false()"/>
					</xsl:call-template>
				</paragraph>
			</cell>
			<cell left="true" bottom="true" top="true" right="true" verticalalign="Top" horizontalalign="right">
				<xso:if test="0=0">
					<xso:variable name="mel">
						<xsl:attribute name="select">
							<xsl:variable name="path" select="count(./ancestor::panel[@istuple='true'])"/>
							<xsl:choose>
								<xsl:when test="$path>1">//<xsl:for-each select="./ancestor::panel[@istuple='true' and not(@id=$me/@id)]">
										<xsl:sort data-type="number" select="position()" order="ascending"/>XbrlElementDataContract[Prefix='<xsl:value-of select="@ns"/>' and Name='<xsl:value-of select="@id"/>']/Children/</xsl:for-each>XbrlElementDataContract[Prefix='<xsl:value-of select="@ns"/>' and Name='<xsl:value-of select="@id"/>']</xsl:when>
								<xsl:otherwise>//XbrlElementDataContract[Prefix='<xsl:value-of select="@ns"/>' and Name='<xsl:value-of select="@id"/>']</xsl:otherwise>
							</xsl:choose>
						</xsl:attribute>
					</xso:variable>
					<xso:for-each select="$mel/Children/XbrlElementDataContract">
						<paragraph lineheight="1.1" size="8">
							<xso:choose>
								<xsl:for-each select="other/field">
									<xsl:variable name="fieldtype" select="details/fieldType/text()"/>
									<xso:when>
										<xsl:attribute name="test">./Name='<xsl:value-of select="@id"/>'</xsl:attribute>
										<xso:call-template>
											<xsl:attribute name="name">
												<xsl:choose>
													<xsl:when test="$fieldtype='valuelist'">GetListValue</xsl:when>
													<xsl:otherwise>
														<xsl:apply-templates select="." mode="GETELEMENTVALUETRANS"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:attribute>
											<xso:with-param name="xel" select="."/>
										</xso:call-template>
									</xso:when>
								</xsl:for-each>
								<xso:otherwise>
									<xso:call-template name="GetListValue">
										<xso:with-param name="xel" select="."/>
									</xso:call-template>
								</xso:otherwise>
							</xso:choose>
						</paragraph>
					</xso:for-each>
				</xso:if>
			</cell>
		</row>
	</xsl:template>
	<xsl:template match="panel">
		<xsl:param name="parentTableInfo"/>
		<xsl:param name="indent" select="0"/>
		<xsl:choose>
			<xsl:when test="not($parentTableInfo)">
				<xsl:choose>
					<xsl:when test="@hasFields='false'">
						<newline />
						<paragraph lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}" fontstyle="bold underline">
							<xsl:attribute name="fontstyle">
								<xsl:choose>
									<xsl:when test="$indent=0">bold</xsl:when>
									<xsl:otherwise>underline</xsl:otherwise>
								</xsl:choose>
							</xsl:attribute>
							<xsl:call-template name="GetLabel">
								<xsl:with-param name="details" select="details"/>
								<xsl:with-param name="code" select="false()"/>
							</xsl:call-template>
						</paragraph>
						<xsl:apply-templates select="children/*"/>
					</xsl:when>
					<xsl:when test="@hasFields='true' and children/*[1]/children//panel/@type='grid' and not(@type='grid')">
						<xsl:variable name="fPos" select="count(children/field[1]/preceding-sibling::*)+1"/>
						<xsl:for-each select="children/*[position() &lt; $fPos]">
							<xsl:apply-templates select=".">
								<xsl:with-param name="parentTableInfo" select="$parentTableInfo"/>
								<xsl:with-param name="indent" select="$indent"/>
							</xsl:apply-templates>
						</xsl:for-each>
						<xsl:apply-templates select="." mode="CREATETABLE">
							<xsl:with-param name="childrenfrom" select="$fPos"/>
						</xsl:apply-templates>
					</xsl:when>
					<xsl:otherwise>
						<!-- construct table -->
						<newline />
						<xsl:apply-templates select="." mode="CREATETABLE"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:otherwise>
				<row>
					<cell left="true" bottom="true" top="true" right="true" horizontalalign="left" verticalalign="Top">
						<xsl:choose>
							<xsl:when test="fieldDefinition">
								<xsl:attribute name="colspan">
									<xsl:value-of select="number($parentTableInfo/@count)-1"/>
								</xsl:attribute>
							</xsl:when>
							<xsl:otherwise>
								<xsl:attribute name="colspan">
									<xsl:value-of select="$parentTableInfo/@count"/>
								</xsl:attribute>
							</xsl:otherwise>
						</xsl:choose>
						<paragraph lineheight="1.1" size="8" fontstyle="bold">
							<xsl:call-template name="GetLabel">
								<xsl:with-param name="details" select="details"/>
								<xsl:with-param name="code" select="false()"/>
							</xsl:call-template>
						</paragraph>
					</cell>
					<xsl:if test="fieldDefinition">
						<xsl:choose>
							<xsl:when test="details/fieldType">
								<cell left="true" bottom="true" top="true" right="true" horizontalalign="left" verticalalign="Top">
									<xsl:apply-templates select="." mode="RENDERFIELD">
										<xsl:with-param name="period" select="details/periods/string[1]"/>
									</xsl:apply-templates>
								</cell>
							</xsl:when>
							<xsl:otherwise>
								<cell left="true" bottom="true" top="true" right="true" horizontalalign="left" verticalalign="Top">
									<paragraph lineheight="1.1" size="8">
										<xsl:call-template name="space"/>
									</paragraph>
								</cell>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:if>
				</row>
				<xsl:choose>
					<xsl:when test="ancestor::panel/@type=./@type or ./@type='normal'">
						<xsl:apply-templates select="." mode="HANDLECHILDREN">
							<xsl:with-param name="parentTableInfo" select="$parentTableInfo"/>
							<xsl:with-param name="indent" select="number($indent)+20"/>
						</xsl:apply-templates>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text disable-output-escaping="yes">&lt;</xsl:text>/table<xsl:text disable-output-escaping="yes">&gt;</xsl:text>
						<xsl:apply-templates select="." mode="CREATETABLE"/>
						<xsl:apply-templates select="ancestor::panel[1]" mode="CREATETABLESTART"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template match="panel|fiche" mode="CREATETABLESTART">
		<xsl:param name="parentTableInfo"/>
		<xsl:param name="indent" select="0"/>
		<xsl:param name="childrenfrom" select="0"/>
		<xsl:text disable-output-escaping="yes">&lt;</xsl:text>table tablefitspage="true" convert2pdfp="true" cellsfitpage="true" borderwidth="0.02" width="100%" align="right" cellspacing="0" cellpadding="3" columns="<xsl:value-of select="@columncount"/>" widths="<xsl:value-of select="@columnwidths"/>"<xsl:text disable-output-escaping="yes">&gt;</xsl:text>
	</xsl:template>
	<xsl:template match="panel|fiche" mode="CREATETABLE">
		<xsl:param name="parentTableInfo"/>
		<xsl:param name="indent" select="0"/>
		<xsl:param name="childrenfrom" select="0"/>
		<xsl:choose>
			<xsl:when test="./@type='contextgrid'">
				<xsl:variable name="myid" select="@id"/>
				<xsl:if test="$childrenfrom=0 and not($parentTableInfo) and not(//panel[not(@type='normal')][1]/@id=$myid)">
					<newpage/>
				</xsl:if>
				<table tablefitspage="true" convert2pdfp="true" cellsfitpage="true" borderwidth="0.02" width="100%" align="right" cellspacing="0" cellpadding="3">
					<!-- NOG TOE TE VOEGEN AAN GEN-->
					<xsl:attribute name="columns">
						<xsl:value-of select="@columncount"/>
					</xsl:attribute>
					<xsl:attribute name="widths">
						<xsl:value-of select="@columnwidths"/>
					</xsl:attribute>
					<xsl:variable name="depth" select="number(columns/@depth)"/>
					<xsl:if test="$childrenfrom=0 and not($parentTableInfo)">
						<row>
							<cell left="false" bottom="true" top="false" verticalalign="Top">
								<xsl:attribute name="colspan">
									<xsl:value-of select="@columncount"/>
								</xsl:attribute>
								<paragraph lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}" fontstyle="bold underline">
									<xsl:attribute name="fontstyle">
										<xsl:choose>
											<xsl:when test="$indent=0">bold</xsl:when>
											<xsl:otherwise>underline</xsl:otherwise>
										</xsl:choose>
									</xsl:attribute>
									<xsl:call-template name="GetLabel">
										<xsl:with-param name="details" select="details"/>
										<xsl:with-param name="code" select="false()"/>
									</xsl:call-template>
								</paragraph>
							</cell>
							<newline />
						</row>
					</xsl:if>
					<row>
						<xsl:variable name="colCount" select="count(columns/column)"/>
						<xsl:for-each select="columns/column">
							<xsl:apply-templates select=".">
								<xsl:with-param name="depth" select="$depth"/>
								<xsl:with-param name="right">
									<xsl:choose>
										<xsl:when test="position()=$colCount">
											<xsl:value-of select="true()"/>
										</xsl:when>
										<xsl:otherwise>
											<xsl:value-of select="false()"/>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:with-param>
							</xsl:apply-templates>
						</xsl:for-each>
					</row>
					<xsl:if test="$depth>1"><row>
							<xsl:variable name="colCount" select="count(columns/column/column)"/>
							<xsl:for-each select="columns/column/column">
								<xsl:apply-templates select=".">
									<xsl:with-param name="depth" select="$depth - 1"/>
									<xsl:with-param name="right">
										<xsl:choose>
											<xsl:when test="position()=$colCount">
												<xsl:value-of select="true()"/>
											</xsl:when>
											<xsl:otherwise>
												<xsl:value-of select="false()"/>
											</xsl:otherwise>
										</xsl:choose>
									</xsl:with-param>
								</xsl:apply-templates>
							</xsl:for-each>
						</row>
					</xsl:if>
					<xsl:if test="$depth>2"><row>
							<xsl:variable name="colCount" select="count(columns/column/column/column)"/>
							<xsl:for-each select="columns/column/column/column">
								<xsl:apply-templates select=".">
									<xsl:with-param name="depth" select="$depth - 2"/>
									<xsl:with-param name="right">
										<xsl:choose>
											<xsl:when test="position()=$colCount">
												<xsl:value-of select="true()"/>
											</xsl:when>
											<xsl:otherwise>
												<xsl:value-of select="false()"/>
											</xsl:otherwise>
										</xsl:choose>
									</xsl:with-param>
								</xsl:apply-templates>
							</xsl:for-each>
						</row>
					</xsl:if>
					<xsl:variable name="ctxlistname" select="concat(@id,'Ctx')"/>
					<xso:variable>
						<xsl:attribute name="name">
							<xsl:value-of select="$ctxlistname"/>
						</xsl:attribute>
						<xsl:choose>
							<xsl:when test="details/prescontextids">
								<xsl:for-each select="details/prescontextids/prescontextid">
									<xsl:variable name="pid" select="./text()"/>
									<xso:call-template name="GetContextsOF">
										<xso:with-param name="prescontext">
											<xsl:attribute name="select">'<xsl:value-of select="$pid"/>'</xsl:attribute>
										</xso:with-param>
									</xso:call-template>
								</xsl:for-each>
							</xsl:when>
							<xsl:otherwise>
								<xso:call-template name="GetContextsOF">
									<xso:with-param name="prescontext">
										<xsl:attribute name="select">'<xsl:value-of select="@id"/>'</xsl:attribute>
									</xso:with-param>
								</xso:call-template>
							</xsl:otherwise>
						</xsl:choose>
					</xso:variable>
					<xso:for-each select="msxsl:node-set($)">
						<xsl:attribute name="select">msxsl:node-set($<xsl:value-of select="$ctxlistname"/>)//defid</xsl:attribute>
						<xso:variable name="defid" select="./text()"/>
						<xso:variable name="mycontexts" select="$me//ContextElementDataContract[./DefId=$defid]"/>
						<row>
							<xsl:apply-templates select="columns/column" mode="HANDLECGCHILDREN">
								<xsl:with-param name="par" select="."/>
								<!--xsl:with-param name="parentTableInfo" select="columns"/><xsl:with-param name="indent" select="$indent"/><xsl:with-param name="childrenfrom" select="$childrenfrom"/-->
							</xsl:apply-templates>
						</row>
					</xso:for-each>
				</table>
			</xsl:when>
			<xsl:otherwise>
				<table tablefitspage="true" convert2pdfp="true" cellsfitpage="true" borderwidth="0.02" width="100%" align="right" cellspacing="0" cellpadding="3">
					<!-- NOG TOE TE VOEGEN AAN GEN-->
					<xsl:attribute name="columns">
						<xsl:value-of select="@columncount"/>
					</xsl:attribute>
					<xsl:attribute name="widths">
						<xsl:value-of select="@columnwidths"/>
					</xsl:attribute>
					<xsl:if test="$childrenfrom=0 and not($parentTableInfo)">
						<row>
							<cell left="false" bottom="true" top="false" verticalalign="Top">
								<xsl:attribute name="colspan">
									<xsl:value-of select="@columncount"/>
								</xsl:attribute>
								<paragraph lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}" fontstyle="bold underline">
									<xsl:attribute name="fontstyle">
										<xsl:choose>
											<xsl:when test="$indent=0">bold</xsl:when>
											<xsl:otherwise>underline</xsl:otherwise>
										</xsl:choose>
									</xsl:attribute>
									<xsl:call-template name="GetLabel">
										<xsl:with-param name="details" select="details"/>
										<xsl:with-param name="code" select="false()"/>
									</xsl:call-template>
								</paragraph>
							</cell>
							<newline />
						</row>
					</xsl:if>
					<xsl:variable name="depth" select="number(columns/@depth)"/>
					<row>
						<xsl:variable name="colCount" select="count(columns/column)"/>
						<xsl:for-each select="columns/column">
							<xsl:apply-templates select=".">
								<xsl:with-param name="depth" select="$depth"/>
								<xsl:with-param name="right">
									<xsl:choose>
										<xsl:when test="position()=$colCount">
											<xsl:value-of select="true()"/>
										</xsl:when>
										<xsl:otherwise>
											<xsl:value-of select="false()"/>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:with-param>
							</xsl:apply-templates>
						</xsl:for-each>
					</row>
					<xsl:if test="$depth>1"><row>
							<xsl:variable name="colCount" select="count(columns/column/column)"/>
							<xsl:for-each select="columns/column/column">
								<xsl:apply-templates select=".">
									<xsl:with-param name="depth" select="$depth - 1"/>
									<xsl:with-param name="right">
										<xsl:choose>
											<xsl:when test="position()=$colCount">
												<xsl:value-of select="true()"/>
											</xsl:when>
											<xsl:otherwise>
												<xsl:value-of select="false()"/>
											</xsl:otherwise>
										</xsl:choose>
									</xsl:with-param>
								</xsl:apply-templates>
							</xsl:for-each>
						</row>
					</xsl:if>
					<xsl:if test="$depth>2"><row>
							<xsl:variable name="colCount" select="count(columns/column/column/column)"/>
							<xsl:for-each select="columns/column/column/column">
								<xsl:apply-templates select=".">
									<xsl:with-param name="depth" select="$depth - 2"/>
									<xsl:with-param name="right">
										<xsl:choose>
											<xsl:when test="position()=$colCount">
												<xsl:value-of select="true()"/>
											</xsl:when>
											<xsl:otherwise>
												<xsl:value-of select="false()"/>
											</xsl:otherwise>
										</xsl:choose>
									</xsl:with-param>
								</xsl:apply-templates>
							</xsl:for-each>
						</row>
					</xsl:if>
					<xsl:if test="not(./@type='contextgrid')">
						<xsl:apply-templates select="." mode="HANDLECHILDREN">
							<xsl:with-param name="parentTableInfo" select="columns"/>
							<xsl:with-param name="indent" select="$indent"/>
							<xsl:with-param name="childrenfrom" select="$childrenfrom"/>
						</xsl:apply-templates>
					</xsl:if>
					<xsl:if test="./@type='contextgrid'">
						<xso:for-each>
							<xsl:attribute name="select"> Helper:Distinct(//Context[PresContextId='<xsl:value-of select="@id"/>']/Id,'.') </xsl:attribute>
							<xso:variable name="id" select="strinn('.')"/>
							<xso:variable name="elements">
								<xso:call-template name="GetContextElements">
									<xso:with-param name="cid" select="."/>
								</xso:call-template>
							</xso:variable>
							<xso:variable name="xels" select="msxsl:node-set(elements)"/>
							<xso:variable name="contexts" select="//Context[Id=$id]"/>
							<row>
								<xsl:apply-templates select="columns/column" mode="HANDLECGCHILDREN">
									<xsl:with-param name="par" select="."/>
									<!--xsl:with-param name="parentTableInfo" select="columns"/><xsl:with-param name="indent" select="$indent"/><xsl:with-param name="childrenfrom" select="$childrenfrom"/-->
								</xsl:apply-templates>
							</row>
						</xso:for-each>
					</xsl:if>
				</table>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template match="column">
		<xsl:param name="depth"/>
		<xsl:param name="right" select="false()"/>
		<xsl:if test="not(@type='action')">
			<cell left="true" bottom="true" top="true" grayfill="0.9" verticalalign="Top">
				<xsl:if test="string($right)='true'">
					<xsl:attribute name="right">true</xsl:attribute>
				</xsl:if>
				<xsl:if test="$depth>1 and not(column)"><xsl:attribute name="rowspan">
						<xsl:value-of select="$depth"/>
					</xsl:attribute>
				</xsl:if>
				<xsl:if test="$depth>1 and column"><xsl:attribute name="colspan">
						<xsl:value-of select="count(column)"/>
					</xsl:attribute>
				</xsl:if>
				<paragraph lineheight="1.1" size="8">
					<xsl:choose>
						<xsl:when test="@type='name'">
							<xsl:call-template name="space"/>
						</xsl:when>
						<xsl:when test="@type='panel'">
							<xsl:call-template name="GetLabel">
								<xsl:with-param name="details" select="."/>
								<xsl:with-param name="code" select="false()"/>
							</xsl:call-template>
						</xsl:when>
						<xsl:when test="@type='period'">
							<xsl:choose>
								<xsl:when test="@period='I-Start'">
									<xso:call-template name="GetTitleIStart"/>
								</xsl:when>
								<xsl:when test="@period='I-End'">
									<xso:call-template name="GetTitleIEnd"/>
								</xsl:when>
								<xsl:when test="@period='D'">
									<xso:call-template name="GetTitleDuration"/>
								</xsl:when>
							</xsl:choose>
						</xsl:when>
						<xsl:when test="@type='code'">Code</xsl:when>
						<xsl:when test="@type='oldcode'">
							<xsl:call-template name="space"/>
						</xsl:when>
						<xsl:when test="@type='contextfield'">
							<xsl:call-template name="GetLabel">
								<xsl:with-param name="details" select="."/>
								<xsl:with-param name="code" select="false()"/>
							</xsl:call-template>
						</xsl:when>
						<xsl:when test="@type='action'">
							<xsl:call-template name="space"/>
						</xsl:when>
						<xsl:when test="@type='field'">
							<xsl:call-template name="GetLabel">
								<xsl:with-param name="details" select="."/>
								<xsl:with-param name="code" select="false()"/>
							</xsl:call-template>
						</xsl:when>
						<xsl:when test="@type='dimension'">
							<xsl:call-template name="GetLabel">
								<xsl:with-param name="details" select="."/>
								<xsl:with-param name="code" select="false()"/>
							</xsl:call-template>
						</xsl:when>
					</xsl:choose>
				</paragraph>
			</cell>
		</xsl:if>
	</xsl:template>
	<xsl:template match="panel|fiche" mode="HANDLECHILDREN">
		<xsl:param name="parentTableInfo"/>
		<xsl:param name="indent"/>
		<xsl:param name="childrenfrom" select="0"/>
		<xsl:for-each select="children/*[position() &gt;= $childrenfrom]">
			<xsl:apply-templates select=".">
				<xsl:with-param name="parentTableInfo" select="$parentTableInfo"/>
				<xsl:with-param name="indent" select="$indent"/>
			</xsl:apply-templates>
		</xsl:for-each>
	</xsl:template>
	<xsl:template match="column" mode="HANDLECGCHILDREN">
		<xsl:param name="par"/>
		<xsl:choose>
			<xsl:when test="column">
				<xsl:apply-templates select="column" mode="HANDLECGCHILDREN">
					<xsl:with-param name="par" select="$par"/>
				</xsl:apply-templates>
			</xsl:when>
			<xsl:otherwise>
				<xsl:if test="not(@type='action')">
					<cell left="true" bottom="true" top="true" right="true" verticalalign="Top">
						<xsl:choose>
							<xsl:when test="@type='action'">
								<xsl:call-template name="space"/>
							</xsl:when>
							<xsl:when test="@type='contextfield'">
								<paragraph lineheight="1.1" size="8">
									<!-- DUMMY IF FOR VARIABLE CONTEXT-->
									<xso:if test="0=0">
										<xso:variable name="ctxval">
											<xsl:attribute name="select">$mycontexts[1]/Scenario/ContextScenarioDataContract[./Dimension='<xsl:value-of select="@dimensionid"/>']/Value</xsl:attribute>
										</xso:variable>
										<xsl:choose>
											<xsl:when test="dimensionfield[@type='select']">
												<xso:call-template name="GetListValue">
													<xso:with-param name="myval" select="$ctxval"/>
												</xso:call-template>
											</xsl:when>
											<xsl:otherwise>
												<xso:value-of select="$ctxval"/>
											</xsl:otherwise>
										</xsl:choose>
									</xso:if>
								</paragraph>
							</xsl:when>
							<xsl:when test="@type='field'">
								<xsl:variable name="fieldId" select="@fieldid"/>
								<xsl:variable name="field" select="ancestor::columns/../children//field[@id=$fieldId]"/>
								<paragraph lineheight="1.1" size="8">
									<xsl:if test="@calculated='true'">
										<xsl:attribute name="fontstyle">italic</xsl:attribute>
									</xsl:if>
									<xso:if test="0=0">
										<xso:variable name="ctx">
											<xsl:attribute name="select">$mycontexts[PeriodId='<xsl:value-of select="@period"/>']/Id</xsl:attribute>
										</xso:variable>
										<xso:call-template>
											<xsl:attribute name="name">
												<xsl:apply-templates select="$field" mode="GETELEMENTVALUETRANS"/>
											</xsl:attribute>
											<xso:with-param name="xel">
												<xsl:attribute name="select">$me//XbrlElementDataContract[Prefix='<xsl:value-of select="$field/@ns"/>' and Name='<xsl:value-of select="$field/@id"/>' and ContextRef=$ctx]</xsl:attribute>
											</xso:with-param>
										</xso:call-template>
									</xso:if>
								</paragraph>
							</xsl:when>
							<xsl:when test="@type='period'">
								<xsl:variable name="fieldId" select="parent::column/@fieldid"/>
								<xsl:variable name="field" select="ancestor::columns/../children//field[@id=$fieldId]"/>
								<paragraph lineheight="1.1" size="8">
									<xsl:if test="@calculated='true'">
										<xsl:attribute name="fontstyle">italic</xsl:attribute>
									</xsl:if>
									<xso:if test="0=0">
										<xso:variable name="ctx">
											<xsl:attribute name="select">$mycontexts[PeriodId='<xsl:value-of select="@period"/>']/Id</xsl:attribute>
										</xso:variable>
										<xso:call-template>
											<xsl:attribute name="name">
												<xsl:apply-templates select="$field" mode="GETELEMENTVALUETRANS"/>
											</xsl:attribute>
											<xso:with-param name="xel">
												<xsl:attribute name="select">$me//XbrlElementDataContract[Prefix='<xsl:value-of select="$field/@ns"/>' and Name='<xsl:value-of select="$field/@id"/>' and ContextRef=$ctx]</xsl:attribute>
											</xso:with-param>
										</xso:call-template>
									</xso:if>
								</paragraph>
							</xsl:when>
							<xsl:otherwise> unknown type: <xsl:value-of select="@type"/>
							</xsl:otherwise>
						</xsl:choose>
					</cell>
				</xsl:if>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template match="field">
		<xsl:param name="parentTableInfo"/>
		<xsl:param name="indent"/>
		<xsl:variable name="me" select="."/>
		<xsl:choose>
			<xsl:when test="details/fieldType/text()='base64BinaryItemType' or details/fieldType/text()='nonEmptyBase64BinaryItemType'">
				<row>
					<cell left="true" bottom="true" right="true" top="true" verticalalign="Top">
						<xsl:attribute name="colspan">
							<xsl:value-of select="$parentTableInfo/@count"/>
						</xsl:attribute>
						<xsl:call-template name="GetLabel">
							<xsl:with-param name="details" select="details"/>
							<xsl:with-param name="code" select="false()"/>
						</xsl:call-template>
					</cell>
				</row>
				<row>
					<cell left="true" bottom="true" right="true" top="true" verticalalign="Top">
						<xsl:attribute name="colspan">
							<xsl:value-of select="$parentTableInfo/@count"/>
						</xsl:attribute>
						<xsl:for-each select="$parentTableInfo/column[not(@type='name')][1]">
							<xsl:apply-templates select="$me" mode="RENDERFIELD">
								<xsl:with-param name="period" select="$me/details/periods/string[1]"/>
							</xsl:apply-templates>
						</xsl:for-each>
					</cell>
				</row>
			</xsl:when>
			<xsl:otherwise>
				<row>
					<xsl:variable name="colCount" select="count(columns/column)"/>
					<xsl:for-each select="$parentTableInfo/column">
						<xsl:apply-templates select="." mode="APPLYFIELD">
							<xsl:with-param name="field" select="$me"/>
							<xsl:with-param name="indent" select="$indent"/>
							<xsl:with-param name="right">
								<xsl:choose>
									<xsl:when test="position()=$colCount">
										<xsl:value-of select="true()"/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="false()"/>
									</xsl:otherwise>
								</xsl:choose>
							</xsl:with-param>
						</xsl:apply-templates>
					</xsl:for-each>
				</row>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template match="column" mode="APPLYFIELD">
		<xsl:param name="field"/>
		<xsl:param name="indent"/>
		<xsl:param name="right" select="false()"/>
		<xsl:choose>
			<xsl:when test="column">
				<xsl:variable name="colCount" select="count(columns/column)"/>
				<xsl:for-each select="column">
					<xsl:apply-templates select="." mode="APPLYFIELD">
						<xsl:with-param name="field" select="$field"/>
						<xsl:with-param name="right">
							<xsl:choose>
								<xsl:when test="position()=$colCount">
									<xsl:value-of select="true()"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="false()"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:with-param>
					</xsl:apply-templates>
				</xsl:for-each>
			</xsl:when>
			<xsl:otherwise>
				<cell left="true" bottom="true" top="true" right="true" verticalalign="Top">
					<xsl:choose>
						<xsl:when test="@type='name'">
							<xsl:attribute name="horizontalalign">left</xsl:attribute>
							<xsl:if test="$indent"/>
							<paragraph lineheight="1.1" size="8">
								<xsl:call-template name="GetLabel">
									<xsl:with-param name="details" select="$field/details"/>
									<xsl:with-param name="code" select="false()"/>
								</xsl:call-template>
							</paragraph>
						</xsl:when>
						<xsl:when test="@type='period'">
							<xsl:attribute name="horizontalalign">right</xsl:attribute>
							<xsl:choose>
								<xsl:when test="@period='I-Start'">
									<xsl:choose>
										<xsl:when test="$field/details/presperiods/string/text()='I-Start'">
											<xsl:apply-templates select="$field" mode="RENDERFIELD">
												<xsl:with-param name="period" select="'I-Start'"/>
											</xsl:apply-templates>
										</xsl:when>
										<xsl:otherwise>
											<paragraph lineheight="1.1" size="8">
												<xsl:call-template name="space"/>
											</paragraph>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:when>
								<xsl:when test="@period='I-End'">
									<xsl:choose>
										<xsl:when test="$field/details/presperiods/string/text()='I-End'">
											<xsl:apply-templates select="$field" mode="RENDERFIELD">
												<xsl:with-param name="period" select="'I-End'"/>
											</xsl:apply-templates>
										</xsl:when>
										<xsl:when test="$field/details/presperiods/string/text()='D'">
											<xsl:apply-templates select="$field" mode="RENDERFIELD">
												<xsl:with-param name="period" select="'D'"/>
											</xsl:apply-templates>
										</xsl:when>
										<xsl:otherwise>
											<paragraph lineheight="1.1" size="8">
												<xsl:call-template name="space"/>
											</paragraph>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:when>
								<xsl:when test="@period='D'">
									<xsl:choose>
										<xsl:when test="$field/details/presperiods/string/text()='D'">
											<xsl:apply-templates select="$field" mode="RENDERFIELD">
												<xsl:with-param name="period" select="'D'"/>
											</xsl:apply-templates>
										</xsl:when>
										<xsl:otherwise>
											<paragraph lineheight="1.1" size="8">
												<xsl:call-template name="space"/>
											</paragraph>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:when>
							</xsl:choose>
						</xsl:when>
						<xsl:when test="@type='code'">
							<xsl:attribute name="horizontalalign">center</xsl:attribute>
							<paragraph lineheight="1.1" size="8">
								<xsl:choose>
									<xsl:when test="$field/details/@code">
										<xsl:value-of select="$field/details/@code"/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:call-template name="space"/>
									</xsl:otherwise>
								</xsl:choose>
							</paragraph>
						</xsl:when>
						<xsl:when test="@type='oldcode'">
							<xsl:attribute name="horizontalalign">center</xsl:attribute>
							<paragraph lineheight="1.1" size="8">
								<xsl:choose>
									<xsl:when test="$field/details/@oldcode">
										<xsl:value-of select="$field/details/@oldcode"/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:call-template name="space"/>
									</xsl:otherwise>
								</xsl:choose>
							</paragraph>
						</xsl:when>
						<xsl:when test="@type='contextfield'">
							<xsl:attribute name="horizontalalign">right</xsl:attribute>
							<paragraph lineheight="1.1" size="8">CONTEXTFIELD HERE</paragraph>
						</xsl:when>
						<xsl:when test="@type='field'">
							<xsl:attribute name="horizontalalign">right</xsl:attribute>
							<paragraph lineheight="1.1" size="8">FIELD HERE</paragraph>
						</xsl:when>
						<xsl:when test="@type='dimension'">
							<xsl:attribute name="horizontalalign">right</xsl:attribute>
							<xsl:variable name="dimid" select="@dimensionid"/>
							<xsl:variable name="domid" select="@domid"/>
							<xsl:choose>
								<xsl:when test="$field/details/Scenario/dimension[@id=$dimid]/dimensionfield/list/item[@value=$domid]">
									<xsl:apply-templates select="$field" mode="RENDERFIELD">
										<xsl:with-param name="period" select="'D'"/>
										<xsl:with-param name="dimension" select="@dimensionid"/>
										<xsl:with-param name="dimensionmember" select="@domid"/>
										<xsl:with-param name="context">
											<xsl:apply-templates select="$field/details" mode="CONTEXT">
												<xsl:with-param name="dimension" select="@dimensionid"/>
												<xsl:with-param name="dimensionmember" select="@domid"/>
											</xsl:apply-templates>
										</xsl:with-param>
									</xsl:apply-templates>
								</xsl:when>
								<xsl:otherwise>
									<paragraph lineheight="1.1" size="8">
										<xsl:call-template name="space"/>
									</paragraph>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:when>
					</xsl:choose>
				</cell>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template match="field|panel" mode="RENDERFIELD">
		<xsl:param name="period"/>
		<xsl:param name="dimension"/>
		<xsl:param name="dimensionmember"/>
		<xsl:param name="context"/>
		<xsl:variable name="contentPeriod">
			<xsl:choose>
				<xsl:when test="details/periods/string[./text()=$period]">
					<xsl:value-of select="$period"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="details/periods/string[1]"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:if test="not(@abstract='true') or @fieldtype='valuelist'">
			<xsl:choose>
				<xsl:when test="@fieldtype='valuelist'">
					<xsl:apply-templates select="./ancestor::panel[1]" mode="APPLYDIVFIELD">
						<xsl:with-param name="period" select="$period"/>
						<xsl:with-param name="dimension" select="$dimension"/>
						<xsl:with-param name="dimensionmember" select="$dimensionmember"/>
						<xsl:with-param name="context" select="$context"/>
						<xsl:with-param name="contentPeriod" select="$contentPeriod"/>
						<xsl:with-param name="fieldtype" select="@fieldtype"/>
					</xsl:apply-templates>
				</xsl:when>
				<xsl:otherwise>
					<xsl:apply-templates select="." mode="APPLYDIVFIELD">
						<xsl:with-param name="period" select="$period"/>
						<xsl:with-param name="dimension" select="$dimension"/>
						<xsl:with-param name="dimensionmember" select="$dimensionmember"/>
						<xsl:with-param name="context" select="$context"/>
						<xsl:with-param name="contentPeriod" select="$contentPeriod"/>
						<xsl:with-param name="fieldtype" select="@fieldtype"/>
					</xsl:apply-templates>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:if>
	</xsl:template>
	<xsl:template match="details" mode="CONTEXT">
		<xsl:param name="dimension"/>
		<xsl:param name="dimensionmember"/>
		<xsl:for-each select="Scenario/dimension">
			<xsl:if test="position()>1">__</xsl:if>id__<xsl:choose>
				<xsl:when test="@id=$dimension">
					<xsl:value-of select="dimensionfield/list/item[@value=$dimensionmember]/@name"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="dimensionfield/list/item[1]/@name"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:for-each>
	</xsl:template>
	<xsl:template match="*" mode="APPLYDIVFIELD">
		<xsl:param name="period"/>
		<xsl:param name="dimension"/>
		<xsl:param name="dimensionmember"/>
		<xsl:param name="context"/>
		<xsl:param name="contentPeriod"/>
		<xsl:param name="fieldtype"/>
		<paragraph lineheight="1.1" size="8">
			<xsl:if test="@calculated='true'">
				<xsl:attribute name="fontstyle">italic</xsl:attribute>
			</xsl:if>
			<xsl:variable name="contextref">
				<xsl:choose>
					<xsl:when test="@dimensionSet='00000000-0000-0000-0000-000000000000' or not(@dimensionSet) or @dimensionSet=''">
						<xsl:value-of select="$contentPeriod"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="$contentPeriod"/>__<xsl:value-of select="$context"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<xsl:apply-templates select="." mode="GETELEMENTVALUE">
				<xsl:with-param name="contextref" select="$contextref"/>
				<xsl:with-param name="fieldtype" select="$fieldtype"/>
			</xsl:apply-templates>
		</paragraph>
	</xsl:template>
	<xsl:template match="*" mode="GetFullId">
		<xsl:param name="period"/>
		<xsl:param name="context"/>
		<xsl:value-of select="@id"/>_<xsl:value-of select="$period"/>
		<xsl:if test="$context and not($context='')">__<xsl:value-of select="$context"/>
		</xsl:if>
	</xsl:template>
	<xsl:template match="*"/>
	<xsl:template match="field|panel" mode="GETELEMENTVALUE">
		<xsl:param name="contextref"/>
		<xsl:param name="fieldtype"/>
		<xso:call-template>
			<xsl:attribute name="name">
				<xsl:choose>
					<xsl:when test="$fieldtype='valuelist'">GetListValue</xsl:when>
					<xsl:otherwise>
						<xsl:apply-templates select="." mode="GETELEMENTVALUETRANS"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<xso:with-param name="xel">
				<xsl:attribute name="select">
					<xsl:variable name="path" select="count(./ancestor::panel[@istuple='true'])"/>
					<xsl:choose>
						<xsl:when test="$path>1">//<xsl:for-each select="./ancestor::panel[@istuple='true']">
								<xsl:sort data-type="number" select="position()" order="ascending"/>XbrlElementDataContract[Prefix='<xsl:value-of select="@ns"/>' and Name='<xsl:value-of select="@id"/>']/Children/</xsl:for-each>XbrlElementDataContract[Prefix='<xsl:value-of select="@ns"/>' and Name='<xsl:value-of select="@id"/>' and ContextRef='<xsl:value-of select="$contextref"/>']</xsl:when>
						<xsl:otherwise>//XbrlElementDataContract[Prefix='<xsl:value-of select="@ns"/>' and Name='<xsl:value-of select="@id"/>' and ContextRef='<xsl:value-of select="$contextref"/>']</xsl:otherwise>
					</xsl:choose>
				</xsl:attribute>
			</xso:with-param>
		</xso:call-template>
	</xsl:template>
	<xsl:template name="GetSelector">
		<xsl:param name="node"/>
		<xsl:if test="$node/ancestor::panel[@istuple='true']">
			<xsl:call-template name="GetSelector">
				<xsl:with-param name="node" select="$node/ancestor::panel[@istuple='true'][1]"/>
			</xsl:call-template>
		</xsl:if>/<xsl:value-of select="$node/@id"/>
	</xsl:template>
	<xsl:template match="field|panel" mode="GETELEMENTVALUETRANS">
		<xsl:choose>
			<xsl:when test="details/fieldType='monetary14D2ItemType'">MonetaryValue</xsl:when>
			<xsl:when test="details/fieldType='nonNegativeMonetary14D2ItemType'">MonetaryValue</xsl:when>
			<xsl:when test="details/fieldType='nonNegativeDecimal6D2ItemType'">NumberValue</xsl:when>
			<xsl:when test="details/fieldType='nonPositiveMonetary14D2ItemType'">MonetaryValue</xsl:when>
			<xsl:when test="details/fieldType='percentageItemType'">PercentValue</xsl:when>
			<xsl:when test="details/fieldType='booleanItemType'">BooleanValue</xsl:when>
			<xsl:when test="details/fieldType='dateItemType'">StandardValue</xsl:when>
			<xsl:when test="details/fieldType='date'">StandardValue</xsl:when>
			<xsl:when test="details/fieldType='nonNegativeInteger6ItemType'">NumberValue</xsl:when>
			<xsl:when test="details/fieldType='positiveIntegerMin1Max6ItemType'">NumberValue</xsl:when>
			<xsl:when test="details/fieldType='integer6ItemType'">NumberValue</xsl:when>
			<xsl:when test="details/fieldType='integerItemType'">NumberValue</xsl:when>
			<xsl:when test="details/fieldType='decimal'">NumberValue</xsl:when>
			<xsl:when test="details/fieldType='string'">StandardValue</xsl:when>
			<xsl:when test="details/fieldType='anyURI'">StandardValue</xsl:when>
			<xsl:when test="details/fieldType='anyURIItemType'">StandardValue</xsl:when>
			<xsl:when test="details/fieldType='limitedDateItemType'">StandardValue</xsl:when>
			<xsl:when test="details/fieldType='documentationType'">StandardValue</xsl:when>
			<xsl:when test="details/fieldType='nonEmptyBase64BinaryItemType'">BinaryValue</xsl:when>
			<xsl:when test="details/fieldType='base64BinaryItemType'">BinaryValue</xsl:when>
			<xsl:when test="details/fieldType='NationalNumberTypedID'">StandardValue</xsl:when>
			<xsl:when test="details/fieldType='NameTypedID'">StandardValue</xsl:when>
			<xsl:when test="details/fieldType='NatureTypedID'">StandardValue</xsl:when>
			<xsl:when test="details/fieldType='WriteDownDebtClaimTypedID'">StandardValue</xsl:when>
			<xsl:when test="details/fieldType='ProvisionRiskExpenseTypedID'">StandardValue</xsl:when>
			<xsl:when test="details/fieldType='CapitalGainTypedID'">StandardValue</xsl:when>
			<xsl:when test="details/fieldType='ReinvestmentTypedID'">StandardValue</xsl:when>
			<xsl:when test="details/fieldType='DescriptionTypedID'">StandardValue</xsl:when>
			<xsl:when test="details/fieldType='stringItemType'">StandardValue</xsl:when>
			<xsl:otherwise>StandardValue</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="GetLabel">
		<xsl:param name="details"/>
		<xsl:param name="code"/>
		<xsl:if test="$code">
			<xsl:value-of select="$details/@code"/> - </xsl:if>
		<xso:choose>
			<xso:when test="$Culture='nl-BE'">
				<xsl:value-of select="$details/labels/Label[Language/text()='nl']/Text"/>
			</xso:when>
			<xso:when test="$Culture='fr-FR'">
				<xsl:value-of select="$details/labels/Label[Language/text()='fr']/Text"/>
			</xso:when>
			<xso:otherwise>
				<xsl:value-of select="$details/labels/Label[Language/text()='en']/Text"/>
			</xso:otherwise>
		</xso:choose>
	</xsl:template>
	<xsl:template name="space">
		<xsl:text disable-output-escaping="yes">&amp;</xsl:text>#160;</xsl:template>
</xsl:stylesheet>