﻿<xsl:stylesheet version="1.0" exclude-result-prefixes="msxsl Helper" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:Helper="urn:Helper">
  <xsl:decimal-format name="NonNumber" NaN="0" zero-digit="0" />
  <xsl:variable name="WorksheetLabelFontsize" select="string('10')" />
  <xsl:variable name="WorksheetFontsize" select="string('10')" />
  <xsl:variable name="WorksheetNumbersize" select="string('8')" />
  <xsl:variable name="WorksheetSmallFontsize" select="string('8')" />
  <xsl:variable name="WorksheetTotalsFontsize" select="string('10')" />
  <xsl:variable name="WorksheetSpacing" select="1.1" />
  <xsl:decimal-format name="euro" decimal-separator="," grouping-separator="." />
  <xsl:variable name="NumberFormatting" select="string('#.##0,00')" />
  <xsl:variable name="NumberFormattingNoDigit" select="string('#.##0')" />
  <xsl:variable name="FONT" select="string('C:\WINDOWS\Fonts\EYInterstate-Light.ttf')" />
  <xsl:variable name="xlists" />
  <xsl:variable name="lists" select="msxsl:node-set($xlists)" />
  <xsl:param name="Culture" />
  <xsl:param name="Binaries" />
  <xsl:template match="BizTaxDataContract">
    <xsl:variable name="me" select="." />
    <Root>
      <paragraph lineheight="1.1" size="10" fontstyle="bold underline">
        Id - <xsl:choose>
          <xsl:when test="$Culture='nl-BE'">Ondernemingsgegevens</xsl:when>
          <xsl:when test="$Culture='fr-FR'">Signalétique de l'entreprise</xsl:when>
          <xsl:otherwise>Company Information Form</xsl:otherwise>
        </xsl:choose>
      </paragraph>
      <newline />
      <paragraph lineheight="1.1" size="10" fontstyle="bold">
        <xsl:choose>
          <xsl:when test="$Culture='nl-BE'">Identificatiegegevens van de onderneming</xsl:when>
          <xsl:when test="$Culture='fr-FR'">Signalétique de l'entreprise</xsl:when>
          <xsl:otherwise>Entity information</xsl:otherwise>
        </xsl:choose>
      </paragraph>
      <newline />
      <table tablefitspage="true" convert2pdfp="true" cellsfitpage="true" borderwidth="0.02" width="100%" align="right" cellspacing="0" cellpadding="3" columns="2" widths="44;20">
        <row>
          <cell left="false" bottom="true" top="false" verticalalign="Top" colspan="2">
            <paragraph lineheight="1.1" size="10" fontstyle="bold">
              <xsl:choose>
                <xsl:when test="$Culture='nl-BE'">Ondernemingsnummer</xsl:when>
                <xsl:when test="$Culture='fr-FR'">Numéro d'entreprise</xsl:when>
                <xsl:otherwise>Entity number</xsl:otherwise>
              </xsl:choose>
            </paragraph>
          </cell>
          <newline />
        </row>
        <row>
          <cell left="true" bottom="true" top="true" grayfill="0.9" verticalalign="Top">
            <paragraph lineheight="1.1" size="8">&#160;</paragraph>
          </cell>
          <cell left="true" bottom="true" top="true" grayfill="0.9" verticalalign="Top" right="true">
            <paragraph lineheight="1.1" size="8">
              <xsl:call-template name="GetTitleDuration" />
            </paragraph>
          </cell>
        </row>
        <row>
          <cell left="true" bottom="true" top="true" right="true" verticalalign="Top" horizontalalign="left">
            <paragraph lineheight="1.1" size="8">
              <xsl:choose>
                <xsl:when test="$Culture='nl-BE'">Type nummer</xsl:when>
                <xsl:when test="$Culture='fr-FR'">Type de numéro</xsl:when>
                <xsl:otherwise>Kind of number</xsl:otherwise>
              </xsl:choose>
            </paragraph>
          </cell>
          <cell left="true" bottom="true" top="true" right="true" verticalalign="Top" horizontalalign="right">
            <paragraph lineheight="1.1" size="8">
              <xsl:call-template name="StandardValue">
                <xsl:with-param name="xel" select="//XbrlElementDataContract[Prefix='pfs-gcd' and Name='EntityInformation']/Children/XbrlElementDataContract[Prefix='pfs-gcd' and Name='EntityIdentifier']/Children/XbrlElementDataContract[Prefix='pfs-gcd' and Name='IdentifierName' and ContextRef='D']" />
              </xsl:call-template>
            </paragraph>
          </cell>
        </row>
        <row>
          <cell left="true" bottom="true" top="true" right="true" verticalalign="Top" horizontalalign="left">
            <paragraph lineheight="1.1" size="8">
              <xsl:choose>
                <xsl:when test="$Culture='nl-BE'">Opgave nummer</xsl:when>
                <xsl:when test="$Culture='fr-FR'">Valeur</xsl:when>
                <xsl:otherwise>Value</xsl:otherwise>
              </xsl:choose>
            </paragraph>
          </cell>
          <cell left="true" bottom="true" top="true" right="true" verticalalign="Top" horizontalalign="right">
            <paragraph lineheight="1.1" size="8">
              <xsl:call-template name="StandardValue">
                <xsl:with-param name="xel" select="//XbrlElementDataContract[Prefix='pfs-gcd' and Name='EntityInformation']/Children/XbrlElementDataContract[Prefix='pfs-gcd' and Name='EntityIdentifier']/Children/XbrlElementDataContract[Prefix='pfs-gcd' and Name='IdentifierValue' and ContextRef='D']" />
              </xsl:call-template>
            </paragraph>
          </cell>
        </row>
      </table>
      <newline />
      <table tablefitspage="true" convert2pdfp="true" cellsfitpage="true" borderwidth="0.02" width="100%" align="right" cellspacing="0" cellpadding="3" columns="2" widths="44;20">
        <row>
          <cell left="false" bottom="true" top="false" verticalalign="Top" colspan="2">
            <paragraph lineheight="1.1" size="10" fontstyle="bold">
              <xsl:choose>
                <xsl:when test="$Culture='nl-BE'">Naam</xsl:when>
                <xsl:when test="$Culture='fr-FR'">Dénomination</xsl:when>
                <xsl:otherwise>Entity name</xsl:otherwise>
              </xsl:choose>
            </paragraph>
          </cell>
          <newline />
        </row>
        <row>
          <cell left="true" bottom="true" top="true" grayfill="0.9" verticalalign="Top">
            <paragraph lineheight="1.1" size="8">&#160;</paragraph>
          </cell>
          <cell left="true" bottom="true" top="true" grayfill="0.9" verticalalign="Top" right="true">
            <paragraph lineheight="1.1" size="8">
              <xsl:call-template name="GetTitleDuration" />
            </paragraph>
          </cell>
        </row>
        <row>
          <cell left="true" bottom="true" top="true" right="true" verticalalign="Top" horizontalalign="left">
            <paragraph lineheight="1.1" size="8">
              <xsl:choose>
                <xsl:when test="$Culture='nl-BE'">Wettelijke benaming</xsl:when>
                <xsl:when test="$Culture='fr-FR'">Dénomination légale</xsl:when>
                <xsl:otherwise>Entity current legal name</xsl:otherwise>
              </xsl:choose>
            </paragraph>
          </cell>
          <cell left="true" bottom="true" top="true" right="true" verticalalign="Top" horizontalalign="right">
            <paragraph lineheight="1.1" size="8">
              <xsl:call-template name="StandardValue">
                <xsl:with-param name="xel" select="//XbrlElementDataContract[Prefix='pfs-gcd' and Name='EntityInformation']/Children/XbrlElementDataContract[Prefix='pfs-gcd' and Name='EntityName']/Children/XbrlElementDataContract[Prefix='pfs-gcd' and Name='EntityCurrentLegalName' and ContextRef='D']" />
              </xsl:call-template>
            </paragraph>
          </cell>
        </row>
      </table>
      <newline />
      <table tablefitspage="true" convert2pdfp="true" cellsfitpage="true" borderwidth="0.02" width="100%" align="right" cellspacing="0" cellpadding="3" columns="2" widths="44;20">
        <row>
          <cell left="false" bottom="true" top="false" verticalalign="Top" colspan="2">
            <paragraph lineheight="1.1" size="10" fontstyle="bold">
              <xsl:choose>
                <xsl:when test="$Culture='nl-BE'">Rechtsvorm</xsl:when>
                <xsl:when test="$Culture='fr-FR'">Forme juridique</xsl:when>
                <xsl:otherwise>Entity legal form</xsl:otherwise>
              </xsl:choose>
            </paragraph>
          </cell>
          <newline />
        </row>
        <row>
          <cell left="true" bottom="true" top="true" grayfill="0.9" verticalalign="Top">
            <paragraph lineheight="1.1" size="8">&#160;</paragraph>
          </cell>
          <cell left="true" bottom="true" top="true" grayfill="0.9" verticalalign="Top" right="true">
            <paragraph lineheight="1.1" size="8">
              <xsl:call-template name="GetTitleDuration" />
            </paragraph>
          </cell>
        </row>
        <row>
          <cell left="true" bottom="true" top="true" right="true" verticalalign="Top" horizontalalign="left">
            <paragraph lineheight="1.1" size="8">
              <xsl:choose>
                <xsl:when test="$Culture='nl-BE'">Rechtsvorm</xsl:when>
                <xsl:when test="$Culture='fr-FR'">Forme juridique</xsl:when>
                <xsl:otherwise>Entity legal form</xsl:otherwise>
              </xsl:choose>
            </paragraph>
          </cell>
          <cell left="true" bottom="true" top="true" right="true" verticalalign="Top" horizontalalign="right">
            <xsl:if test="0=0">
              <xsl:variable name="mel" select="//XbrlElementDataContract[Prefix='pfs-gcd' and Name='EntityInformation']/Children/XbrlElementDataContract[Prefix='pfs-gcd' and Name='EntityForm']" />
              <xsl:for-each select="$mel/Children/XbrlElementDataContract">
                <paragraph lineheight="1.1" size="8">
                  <xsl:choose>
                    <xsl:when test="./Name='LegalFormOther'">
                      <xsl:call-template name="StandardValue">
                        <xsl:with-param name="xel" select="." />
                      </xsl:call-template>
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:call-template name="GetListValue">
                        <xsl:with-param name="xel" select="." />
                      </xsl:call-template>
                    </xsl:otherwise>
                  </xsl:choose>
                </paragraph>
              </xsl:for-each>
            </xsl:if>
          </cell>
        </row>
      </table>
      <newline />
      <table tablefitspage="true" convert2pdfp="true" cellsfitpage="true" borderwidth="0.02" width="100%" align="right" cellspacing="0" cellpadding="3" columns="2" widths="44;20">
        <row>
          <cell left="false" bottom="true" top="false" verticalalign="Top" colspan="2">
            <paragraph lineheight="1.1" size="10" fontstyle="bold">
              <xsl:choose>
                <xsl:when test="$Culture='nl-BE'">Adres</xsl:when>
                <xsl:when test="$Culture='fr-FR'">Adresse</xsl:when>
                <xsl:otherwise>Address</xsl:otherwise>
              </xsl:choose>
            </paragraph>
          </cell>
          <newline />
        </row>
        <row>
          <cell left="true" bottom="true" top="true" grayfill="0.9" verticalalign="Top">
            <paragraph lineheight="1.1" size="8">&#160;</paragraph>
          </cell>
          <cell left="true" bottom="true" top="true" grayfill="0.9" verticalalign="Top" right="true">
            <paragraph lineheight="1.1" size="8">
              <xsl:call-template name="GetTitleDuration" />
            </paragraph>
          </cell>
        </row>
        <row>
          <cell left="true" bottom="true" top="true" right="true" horizontalalign="left" verticalalign="Top" colspan="1">
            <paragraph lineheight="1.1" size="8" fontstyle="bold">
              <xsl:choose>
                <xsl:when test="$Culture='nl-BE'">Aard adres</xsl:when>
                <xsl:when test="$Culture='fr-FR'">Type d'adresse</xsl:when>
                <xsl:otherwise>Address type</xsl:otherwise>
              </xsl:choose>
            </paragraph>
          </cell>
          <cell left="true" bottom="true" top="true" right="true" horizontalalign="left" verticalalign="Top">
            <paragraph lineheight="1.1" size="8">
              <xsl:call-template name="StandardValue">
                <xsl:with-param name="xel" select="//XbrlElementDataContract[Prefix='pfs-gcd' and Name='EntityInformation']/Children/XbrlElementDataContract[Prefix='pfs-gcd' and Name='EntityAddress']/Children/XbrlElementDataContract[Prefix='pfs-gcd' and Name='AddressType' and ContextRef='']" />
              </xsl:call-template>
            </paragraph>
          </cell>
        </row>
        <row>
          <cell left="true" bottom="true" top="true" right="true" verticalalign="Top" horizontalalign="left">
            <paragraph lineheight="1.1" size="8">
              <xsl:choose>
                <xsl:when test="$Culture='nl-BE'">Aard adres</xsl:when>
                <xsl:when test="$Culture='fr-FR'">Type d'adresse</xsl:when>
                <xsl:otherwise>Address type</xsl:otherwise>
              </xsl:choose>
            </paragraph>
          </cell>
          <cell left="true" bottom="true" top="true" right="true" verticalalign="Top" horizontalalign="right">
            <xsl:if test="0=0">
              <xsl:variable name="mel" select="//XbrlElementDataContract[Prefix='pfs-gcd' and Name='EntityInformation']/Children/XbrlElementDataContract[Prefix='pfs-gcd' and Name='EntityAddress']/Children/XbrlElementDataContract[Prefix='pfs-gcd' and Name='AddressType']" />
              <xsl:for-each select="$mel/Children/XbrlElementDataContract">
                <paragraph lineheight="1.1" size="8">
                  <xsl:choose>
                    <xsl:when test="./Name='AddressTypeOther'">
                      <xsl:call-template name="StandardValue">
                        <xsl:with-param name="xel" select="." />
                      </xsl:call-template>
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:call-template name="GetListValue">
                        <xsl:with-param name="xel" select="." />
                      </xsl:call-template>
                    </xsl:otherwise>
                  </xsl:choose>
                </paragraph>
              </xsl:for-each>
            </xsl:if>
          </cell>
        </row>
        <row>
          <cell left="true" bottom="true" top="true" right="true" verticalalign="Top" horizontalalign="left">
            <paragraph lineheight="1.1" size="8">
              <xsl:choose>
                <xsl:when test="$Culture='nl-BE'">Straat</xsl:when>
                <xsl:when test="$Culture='fr-FR'">Rue</xsl:when>
                <xsl:otherwise>Street</xsl:otherwise>
              </xsl:choose>
            </paragraph>
          </cell>
          <cell left="true" bottom="true" top="true" right="true" verticalalign="Top" horizontalalign="right">
            <paragraph lineheight="1.1" size="8">
              <xsl:call-template name="StandardValue">
                <xsl:with-param name="xel" select="//XbrlElementDataContract[Prefix='pfs-gcd' and Name='EntityInformation']/Children/XbrlElementDataContract[Prefix='pfs-gcd' and Name='EntityAddress']/Children/XbrlElementDataContract[Prefix='pfs-gcd' and Name='Street' and ContextRef='D']" />
              </xsl:call-template>
            </paragraph>
          </cell>
        </row>
        <row>
          <cell left="true" bottom="true" top="true" right="true" verticalalign="Top" horizontalalign="left">
            <paragraph lineheight="1.1" size="8">
              <xsl:choose>
                <xsl:when test="$Culture='nl-BE'">Nr</xsl:when>
                <xsl:when test="$Culture='fr-FR'">N°</xsl:when>
                <xsl:otherwise>Nr</xsl:otherwise>
              </xsl:choose>
            </paragraph>
          </cell>
          <cell left="true" bottom="true" top="true" right="true" verticalalign="Top" horizontalalign="right">
            <paragraph lineheight="1.1" size="8">
              <xsl:call-template name="StandardValue">
                <xsl:with-param name="xel" select="//XbrlElementDataContract[Prefix='pfs-gcd' and Name='EntityInformation']/Children/XbrlElementDataContract[Prefix='pfs-gcd' and Name='EntityAddress']/Children/XbrlElementDataContract[Prefix='pfs-gcd' and Name='Number' and ContextRef='D']" />
              </xsl:call-template>
            </paragraph>
          </cell>
        </row>
        <row>
          <cell left="true" bottom="true" top="true" right="true" verticalalign="Top" horizontalalign="left">
            <paragraph lineheight="1.1" size="8">
              <xsl:choose>
                <xsl:when test="$Culture='nl-BE'">Bus</xsl:when>
                <xsl:when test="$Culture='fr-FR'">Boîte</xsl:when>
                <xsl:otherwise>Box</xsl:otherwise>
              </xsl:choose>
            </paragraph>
          </cell>
          <cell left="true" bottom="true" top="true" right="true" verticalalign="Top" horizontalalign="right">
            <paragraph lineheight="1.1" size="8">
              <xsl:call-template name="StandardValue">
                <xsl:with-param name="xel" select="//XbrlElementDataContract[Prefix='pfs-gcd' and Name='EntityInformation']/Children/XbrlElementDataContract[Prefix='pfs-gcd' and Name='EntityAddress']/Children/XbrlElementDataContract[Prefix='pfs-gcd' and Name='Box' and ContextRef='D']" />
              </xsl:call-template>
            </paragraph>
          </cell>
        </row>
        <row>
          <cell left="true" bottom="true" top="true" right="true" horizontalalign="left" verticalalign="Top" colspan="1">
            <paragraph lineheight="1.1" size="8" fontstyle="bold">
              <xsl:choose>
                <xsl:when test="$Culture='nl-BE'">Postcode en gemeente</xsl:when>
                <xsl:when test="$Culture='fr-FR'">Code postal et commune</xsl:when>
                <xsl:otherwise>Postal code and city</xsl:otherwise>
              </xsl:choose>
            </paragraph>
          </cell>
          <cell left="true" bottom="true" top="true" right="true" horizontalalign="left" verticalalign="Top">
            <paragraph lineheight="1.1" size="8">
              <xsl:call-template name="StandardValue">
                <xsl:with-param name="xel" select="//XbrlElementDataContract[Prefix='pfs-gcd' and Name='EntityInformation']/Children/XbrlElementDataContract[Prefix='pfs-gcd' and Name='EntityAddress']/Children/XbrlElementDataContract[Prefix='pfs-gcd' and Name='PostalCodeCity' and ContextRef='']" />
              </xsl:call-template>
            </paragraph>
          </cell>
        </row>
        <row>
          <cell left="true" bottom="true" top="true" right="true" verticalalign="Top" horizontalalign="left">
            <paragraph lineheight="1.1" size="8">
              <xsl:choose>
                <xsl:when test="$Culture='nl-BE'">Postcode en gemeente</xsl:when>
                <xsl:when test="$Culture='fr-FR'">Code postal et commune</xsl:when>
                <xsl:otherwise>Postal code and city</xsl:otherwise>
              </xsl:choose>
            </paragraph>
          </cell>
          <cell left="true" bottom="true" top="true" right="true" verticalalign="Top" horizontalalign="right">
            <xsl:if test="0=0">
              <xsl:variable name="mel" select="//XbrlElementDataContract[Prefix='pfs-gcd' and Name='EntityInformation']/Children/XbrlElementDataContract[Prefix='pfs-gcd' and Name='EntityAddress']/Children/XbrlElementDataContract[Prefix='pfs-gcd' and Name='PostalCodeCity']" />
              <xsl:for-each select="$mel/Children/XbrlElementDataContract">
                <paragraph lineheight="1.1" size="8">
                  <xsl:choose>
                    <xsl:when test="./Name='PostalCodeOther'">
                      <xsl:call-template name="StandardValue">
                        <xsl:with-param name="xel" select="." />
                      </xsl:call-template>
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:call-template name="GetListValue">
                        <xsl:with-param name="xel" select="." />
                      </xsl:call-template>
                    </xsl:otherwise>
                    <xsl:when test="./Name='City'">
                      <xsl:call-template name="StandardValue">
                        <xsl:with-param name="xel" select="." />
                      </xsl:call-template>
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:call-template name="GetListValue">
                        <xsl:with-param name="xel" select="." />
                      </xsl:call-template>
                    </xsl:otherwise>
                  </xsl:choose>
                </paragraph>
              </xsl:for-each>
            </xsl:if>
          </cell>
        </row>
        <row>
          <cell left="true" bottom="true" top="true" right="true" horizontalalign="left" verticalalign="Top" colspan="1">
            <paragraph lineheight="1.1" size="8" fontstyle="bold">
              <xsl:choose>
                <xsl:when test="$Culture='nl-BE'">Land</xsl:when>
                <xsl:when test="$Culture='fr-FR'">Pays</xsl:when>
                <xsl:otherwise>Country</xsl:otherwise>
              </xsl:choose>
            </paragraph>
          </cell>
          <cell left="true" bottom="true" top="true" right="true" horizontalalign="left" verticalalign="Top">
            <paragraph lineheight="1.1" size="8">
              <xsl:call-template name="StandardValue">
                <xsl:with-param name="xel" select="//XbrlElementDataContract[Prefix='pfs-gcd' and Name='EntityInformation']/Children/XbrlElementDataContract[Prefix='pfs-gcd' and Name='EntityAddress']/Children/XbrlElementDataContract[Prefix='pfs-gcd' and Name='CountryCode' and ContextRef='']" />
              </xsl:call-template>
            </paragraph>
          </cell>
        </row>
        <row>
          <cell left="true" bottom="true" top="true" right="true" verticalalign="Top" horizontalalign="left">
            <paragraph lineheight="1.1" size="8">
              <xsl:choose>
                <xsl:when test="$Culture='nl-BE'">Land</xsl:when>
                <xsl:when test="$Culture='fr-FR'">Pays</xsl:when>
                <xsl:otherwise>Country</xsl:otherwise>
              </xsl:choose>
            </paragraph>
          </cell>
          <cell left="true" bottom="true" top="true" right="true" verticalalign="Top" horizontalalign="right">
            <xsl:if test="0=0">
              <xsl:variable name="mel" select="//XbrlElementDataContract[Prefix='pfs-gcd' and Name='EntityInformation']/Children/XbrlElementDataContract[Prefix='pfs-gcd' and Name='EntityAddress']/Children/XbrlElementDataContract[Prefix='pfs-gcd' and Name='CountryCode']" />
              <xsl:for-each select="$mel/Children/XbrlElementDataContract">
                <paragraph lineheight="1.1" size="8">
                  <xsl:choose>
                    <xsl:when test="./Name='CountryCodeOther'">
                      <xsl:call-template name="StandardValue">
                        <xsl:with-param name="xel" select="." />
                      </xsl:call-template>
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:call-template name="GetListValue">
                        <xsl:with-param name="xel" select="." />
                      </xsl:call-template>
                    </xsl:otherwise>
                  </xsl:choose>
                </paragraph>
              </xsl:for-each>
            </xsl:if>
          </cell>
        </row>
      </table>
      <newline />
      <table tablefitspage="true" convert2pdfp="true" cellsfitpage="true" borderwidth="0.02" width="100%" align="right" cellspacing="0" cellpadding="3" columns="2" widths="44;20">
        <row>
          <cell left="false" bottom="true" top="false" verticalalign="Top" colspan="2">
            <paragraph lineheight="1.1" size="10" fontstyle="bold">
              <xsl:choose>
                <xsl:when test="$Culture='nl-BE'">Bankinformatie</xsl:when>
                <xsl:when test="$Culture='fr-FR'">Information bancaire</xsl:when>
                <xsl:otherwise>Bank information</xsl:otherwise>
              </xsl:choose>
            </paragraph>
          </cell>
          <newline />
        </row>
        <row>
          <cell left="true" bottom="true" top="true" grayfill="0.9" verticalalign="Top">
            <paragraph lineheight="1.1" size="8">&#160;</paragraph>
          </cell>
          <cell left="true" bottom="true" top="true" grayfill="0.9" verticalalign="Top" right="true">
            <paragraph lineheight="1.1" size="8">
              <xsl:call-template name="GetTitleDuration" />
            </paragraph>
          </cell>
        </row>
        <row>
          <cell left="true" bottom="true" top="true" right="true" verticalalign="Top" horizontalalign="left">
            <paragraph lineheight="1.1" size="8">
              <xsl:choose>
                <xsl:when test="$Culture='nl-BE'">BIC</xsl:when>
                <xsl:when test="$Culture='fr-FR'">BIC</xsl:when>
                <xsl:otherwise>BIC</xsl:otherwise>
              </xsl:choose>
            </paragraph>
          </cell>
          <cell left="true" bottom="true" top="true" right="true" verticalalign="Top" horizontalalign="right">
            <paragraph lineheight="1.1" size="8">
              <xsl:call-template name="StandardValue">
                <xsl:with-param name="xel" select="//XbrlElementDataContract[Prefix='pfs-gcd' and Name='EntityInformation']/Children/XbrlElementDataContract[Prefix='pfs-gcd' and Name='BankInformation']/Children/XbrlElementDataContract[Prefix='pfs-gcd' and Name='BankIdentifierCode' and ContextRef='D']" />
              </xsl:call-template>
            </paragraph>
          </cell>
        </row>
        <row>
          <cell left="true" bottom="true" top="true" right="true" verticalalign="Top" horizontalalign="left">
            <paragraph lineheight="1.1" size="8">
              <xsl:choose>
                <xsl:when test="$Culture='nl-BE'">IBAN</xsl:when>
                <xsl:when test="$Culture='fr-FR'">IBAN</xsl:when>
                <xsl:otherwise>IBAN</xsl:otherwise>
              </xsl:choose>
            </paragraph>
          </cell>
          <cell left="true" bottom="true" top="true" right="true" verticalalign="Top" horizontalalign="right">
            <paragraph lineheight="1.1" size="8">
              <xsl:call-template name="StandardValue">
                <xsl:with-param name="xel" select="//XbrlElementDataContract[Prefix='pfs-gcd' and Name='EntityInformation']/Children/XbrlElementDataContract[Prefix='pfs-gcd' and Name='BankInformation']/Children/XbrlElementDataContract[Prefix='pfs-gcd' and Name='InternationalBankAccountNumber' and ContextRef='D']" />
              </xsl:call-template>
            </paragraph>
          </cell>
        </row>
      </table>
      <newline />
      <table tablefitspage="true" convert2pdfp="true" cellsfitpage="true" borderwidth="0.02" width="100%" align="right" cellspacing="0" cellpadding="3" columns="2" widths="44;20">
        <row>
          <cell left="false" bottom="true" top="false" verticalalign="Top" colspan="2">
            <paragraph lineheight="1.1" size="10" fontstyle="bold">
              <xsl:choose>
                <xsl:when test="$Culture='nl-BE'">Identificerend nummer van de zonder vereffening ontbonden vennootschap</xsl:when>
                <xsl:when test="$Culture='fr-FR'">Identifiant de la société dissoute sans liquidation</xsl:when>
                <xsl:otherwise>Entity identifier of the company wound up without liquidation procedure</xsl:otherwise>
              </xsl:choose>
            </paragraph>
          </cell>
          <newline />
        </row>
        <row>
          <cell left="true" bottom="true" top="true" grayfill="0.9" verticalalign="Top">
            <paragraph lineheight="1.1" size="8">&#160;</paragraph>
          </cell>
          <cell left="true" bottom="true" top="true" grayfill="0.9" verticalalign="Top" right="true">
            <paragraph lineheight="1.1" size="8">
              <xsl:call-template name="GetTitleDuration" />
            </paragraph>
          </cell>
        </row>
        <row>
          <cell left="true" bottom="true" top="true" right="true" verticalalign="Top" horizontalalign="left">
            <paragraph lineheight="1.1" size="8">
              <xsl:choose>
                <xsl:when test="$Culture='nl-BE'">Type nummer</xsl:when>
                <xsl:when test="$Culture='fr-FR'">Type de numéro</xsl:when>
                <xsl:otherwise>Kind of number</xsl:otherwise>
              </xsl:choose>
            </paragraph>
          </cell>
          <cell left="true" bottom="true" top="true" right="true" verticalalign="Top" horizontalalign="right">
            <paragraph lineheight="1.1" size="8">
              <xsl:call-template name="StandardValue">
                <xsl:with-param name="xel" select="//XbrlElementDataContract[Prefix='pfs-gcd' and Name='EntityInformation']/Children/XbrlElementDataContract[Prefix='pfs-gcd' and Name='EntityIdentifierCompanyWoundUpWithoutLiquidationProcedure']/Children/XbrlElementDataContract[Prefix='pfs-gcd' and Name='IdentifierName' and ContextRef='D']" />
              </xsl:call-template>
            </paragraph>
          </cell>
        </row>
        <row>
          <cell left="true" bottom="true" top="true" right="true" verticalalign="Top" horizontalalign="left">
            <paragraph lineheight="1.1" size="8">
              <xsl:choose>
                <xsl:when test="$Culture='nl-BE'">Opgave nummer</xsl:when>
                <xsl:when test="$Culture='fr-FR'">Valeur</xsl:when>
                <xsl:otherwise>Value</xsl:otherwise>
              </xsl:choose>
            </paragraph>
          </cell>
          <cell left="true" bottom="true" top="true" right="true" verticalalign="Top" horizontalalign="right">
            <paragraph lineheight="1.1" size="8">
              <xsl:call-template name="StandardValue">
                <xsl:with-param name="xel" select="//XbrlElementDataContract[Prefix='pfs-gcd' and Name='EntityInformation']/Children/XbrlElementDataContract[Prefix='pfs-gcd' and Name='EntityIdentifierCompanyWoundUpWithoutLiquidationProcedure']/Children/XbrlElementDataContract[Prefix='pfs-gcd' and Name='IdentifierValue' and ContextRef='D']" />
              </xsl:call-template>
            </paragraph>
          </cell>
        </row>
      </table>
      <newline />
      <table tablefitspage="true" convert2pdfp="true" cellsfitpage="true" borderwidth="0.02" width="100%" align="right" cellspacing="0" cellpadding="3" columns="3" widths="44;20;20">
        <row>
          <cell left="false" bottom="true" top="false" verticalalign="Top" colspan="3">
            <paragraph lineheight="1.1" size="10" fontstyle="bold">
              <xsl:choose>
                <xsl:when test="$Culture='nl-BE'">Inlichtingen omtrent het betreffende belastbare tijdperk</xsl:when>
                <xsl:when test="$Culture='fr-FR'">Exercice couvert</xsl:when>
                <xsl:otherwise>Periods Covered</xsl:otherwise>
              </xsl:choose>
            </paragraph>
          </cell>
          <newline />
        </row>
        <row>
          <cell left="true" bottom="true" top="true" grayfill="0.9" verticalalign="Top">
            <paragraph lineheight="1.1" size="8">&#160;</paragraph>
          </cell>
          <cell left="true" bottom="true" top="true" grayfill="0.9" verticalalign="Top">
            <paragraph lineheight="1.1" size="8">
              <xsl:call-template name="GetTitleIStart" />
            </paragraph>
          </cell>
          <cell left="true" bottom="true" top="true" grayfill="0.9" verticalalign="Top" right="true">
            <paragraph lineheight="1.1" size="8">
              <xsl:call-template name="GetTitleIEnd" />
            </paragraph>
          </cell>
        </row>
        <row>
          <cell left="true" bottom="true" top="true" right="true" verticalalign="Top" horizontalalign="left">
            <paragraph lineheight="1.1" size="8">
              <xsl:choose>
                <xsl:when test="$Culture='nl-BE'">Begindatum van het belastbare tijdperk</xsl:when>
                <xsl:when test="$Culture='fr-FR'">Date de début d'exercice </xsl:when>
                <xsl:otherwise>Period Start Date</xsl:otherwise>
              </xsl:choose>
            </paragraph>
          </cell>
          <cell left="true" bottom="true" top="true" right="true" verticalalign="Top" horizontalalign="right">
            <paragraph lineheight="1.1" size="8">
              <xsl:call-template name="StandardValue">
                <xsl:with-param name="xel" select="//XbrlElementDataContract[Prefix='tax-inc' and Name='PeriodStartDate' and ContextRef='I-Start']" />
              </xsl:call-template>
            </paragraph>
          </cell>
          <cell left="true" bottom="true" top="true" right="true" verticalalign="Top" horizontalalign="right">
            <paragraph lineheight="1.1" size="8">&#160;</paragraph>
          </cell>
        </row>
        <row>
          <cell left="true" bottom="true" top="true" right="true" verticalalign="Top" horizontalalign="left">
            <paragraph lineheight="1.1" size="8">
              <xsl:choose>
                <xsl:when test="$Culture='nl-BE'">Einddatum van het belastbare tijdperk</xsl:when>
                <xsl:when test="$Culture='fr-FR'">Date de fin d'exercice</xsl:when>
                <xsl:otherwise>Period End Date</xsl:otherwise>
              </xsl:choose>
            </paragraph>
          </cell>
          <cell left="true" bottom="true" top="true" right="true" verticalalign="Top" horizontalalign="right">
            <paragraph lineheight="1.1" size="8">&#160;</paragraph>
          </cell>
          <cell left="true" bottom="true" top="true" right="true" verticalalign="Top" horizontalalign="right">
            <paragraph lineheight="1.1" size="8">
              <xsl:call-template name="StandardValue">
                <xsl:with-param name="xel" select="//XbrlElementDataContract[Prefix='tax-inc' and Name='PeriodEndDate' and ContextRef='I-End']" />
              </xsl:call-template>
            </paragraph>
          </cell>
        </row>
        <row>
          <cell left="true" bottom="true" top="true" right="true" verticalalign="Top" horizontalalign="left">
            <paragraph lineheight="1.1" size="8">
              <xsl:choose>
                <xsl:when test="$Culture='nl-BE'">Aanslagjaar</xsl:when>
                <xsl:when test="$Culture='fr-FR'">Exercice d’imposition</xsl:when>
                <xsl:otherwise>Assessment Year</xsl:otherwise>
              </xsl:choose>
            </paragraph>
          </cell>
          <cell left="true" bottom="true" top="true" right="true" verticalalign="Top" horizontalalign="right">
            <paragraph lineheight="1.1" size="8">&#160;</paragraph>
          </cell>
          <cell left="true" bottom="true" top="true" right="true" verticalalign="Top" horizontalalign="right">
            <paragraph lineheight="1.1" size="8">
              <xsl:call-template name="StandardValue">
                <xsl:with-param name="xel" select="//XbrlElementDataContract[Prefix='tax-inc' and Name='AssessmentYear' and ContextRef='D']" />
              </xsl:call-template>
            </paragraph>
          </cell>
        </row>
        <row>
          <cell left="true" bottom="true" top="true" right="true" verticalalign="Top" horizontalalign="left">
            <paragraph lineheight="1.1" size="8">
              <xsl:choose>
                <xsl:when test="$Culture='nl-BE'">Aangifte</xsl:when>
                <xsl:when test="$Culture='fr-FR'">Déclaration</xsl:when>
                <xsl:otherwise>Tax Return Type</xsl:otherwise>
              </xsl:choose>
            </paragraph>
          </cell>
          <cell left="true" bottom="true" top="true" right="true" verticalalign="Top" horizontalalign="right">
            <paragraph lineheight="1.1" size="8">&#160;</paragraph>
          </cell>
          <cell left="true" bottom="true" top="true" right="true" verticalalign="Top" horizontalalign="right">
            <paragraph lineheight="1.1" size="8">
              <xsl:call-template name="StandardValue">
                <xsl:with-param name="xel" select="//XbrlElementDataContract[Prefix='tax-inc' and Name='TaxReturnType' and ContextRef='D']" />
              </xsl:call-template>
            </paragraph>
          </cell>
        </row>
      </table>
      <newline />
      <paragraph lineheight="1.1" size="10" fontstyle="bold">
        <xsl:choose>
          <xsl:when test="$Culture='nl-BE'">Inlichtingen omtrent het document</xsl:when>
          <xsl:when test="$Culture='fr-FR'">Signalétique du document</xsl:when>
          <xsl:otherwise>Document information</xsl:otherwise>
        </xsl:choose>
      </paragraph>
      <newline />
      <table tablefitspage="true" convert2pdfp="true" cellsfitpage="true" borderwidth="0.02" width="100%" align="right" cellspacing="0" cellpadding="3" columns="2" widths="44;20">
        <row>
          <cell left="false" bottom="true" top="false" verticalalign="Top" colspan="2">
            <paragraph lineheight="1.1" size="10" fontstyle="bold">
              <xsl:choose>
                <xsl:when test="$Culture='nl-BE'">Contactpersoon</xsl:when>
                <xsl:when test="$Culture='fr-FR'">Personne de contact</xsl:when>
                <xsl:otherwise>Contact</xsl:otherwise>
              </xsl:choose>
            </paragraph>
          </cell>
          <newline />
        </row>
        <row>
          <cell left="true" bottom="true" top="true" grayfill="0.9" verticalalign="Top">
            <paragraph lineheight="1.1" size="8">&#160;</paragraph>
          </cell>
          <cell left="true" bottom="true" top="true" grayfill="0.9" verticalalign="Top" right="true">
            <paragraph lineheight="1.1" size="8">
              <xsl:call-template name="GetTitleDuration" />
            </paragraph>
          </cell>
        </row>
        <row>
          <cell left="true" bottom="true" top="true" right="true" horizontalalign="left" verticalalign="Top" colspan="1">
            <paragraph lineheight="1.1" size="8" fontstyle="bold">
              <xsl:choose>
                <xsl:when test="$Culture='nl-BE'">Hoedanigheid van de contactpersoon</xsl:when>
                <xsl:when test="$Culture='fr-FR'">Type de contact</xsl:when>
                <xsl:otherwise>Type of contact</xsl:otherwise>
              </xsl:choose>
            </paragraph>
          </cell>
          <cell left="true" bottom="true" top="true" right="true" horizontalalign="left" verticalalign="Top">
            <paragraph lineheight="1.1" size="8">
              <xsl:call-template name="StandardValue">
                <xsl:with-param name="xel" select="//XbrlElementDataContract[Prefix='pfs-gcd' and Name='DocumentInformation']/Children/XbrlElementDataContract[Prefix='pfs-gcd' and Name='DocumentContact']/Children/XbrlElementDataContract[Prefix='pfs-gcd' and Name='DocumentContactType' and ContextRef='']" />
              </xsl:call-template>
            </paragraph>
          </cell>
        </row>
        <row>
          <cell left="true" bottom="true" top="true" right="true" verticalalign="Top" horizontalalign="left">
            <paragraph lineheight="1.1" size="8">
              <xsl:choose>
                <xsl:when test="$Culture='nl-BE'">Lijst</xsl:when>
                <xsl:when test="$Culture='fr-FR'">Liste</xsl:when>
                <xsl:otherwise>List</xsl:otherwise>
              </xsl:choose>
            </paragraph>
          </cell>
          <cell left="true" bottom="true" top="true" right="true" verticalalign="Top" horizontalalign="right">
            <paragraph lineheight="1.1" size="8">
              <xsl:call-template name="GetListValue">
                <xsl:with-param name="xel" select="//XbrlElementDataContract[Prefix='pfs-gcd' and Name='DocumentInformation']/Children/XbrlElementDataContract[Prefix='pfs-gcd' and Name='DocumentContact']/Children/XbrlElementDataContract[Prefix='pfs-gcd' and Name='DocumentContactType' and ContextRef='D']" />
              </xsl:call-template>
            </paragraph>
          </cell>
        </row>
        <row>
          <cell left="true" bottom="true" top="true" right="true" verticalalign="Top" horizontalalign="left">
            <paragraph lineheight="1.1" size="8">
              <xsl:choose>
                <xsl:when test="$Culture='nl-BE'">Naam</xsl:when>
                <xsl:when test="$Culture='fr-FR'">Nom</xsl:when>
                <xsl:otherwise>Name</xsl:otherwise>
              </xsl:choose>
            </paragraph>
          </cell>
          <cell left="true" bottom="true" top="true" right="true" verticalalign="Top" horizontalalign="right">
            <paragraph lineheight="1.1" size="8">
              <xsl:call-template name="StandardValue">
                <xsl:with-param name="xel" select="//XbrlElementDataContract[Prefix='pfs-gcd' and Name='DocumentInformation']/Children/XbrlElementDataContract[Prefix='pfs-gcd' and Name='DocumentContact']/Children/XbrlElementDataContract[Prefix='pfs-gcd' and Name='ContactName' and ContextRef='D']" />
              </xsl:call-template>
            </paragraph>
          </cell>
        </row>
        <row>
          <cell left="true" bottom="true" top="true" right="true" verticalalign="Top" horizontalalign="left">
            <paragraph lineheight="1.1" size="8">
              <xsl:choose>
                <xsl:when test="$Culture='nl-BE'">Voornaam</xsl:when>
                <xsl:when test="$Culture='fr-FR'">Prénom</xsl:when>
                <xsl:otherwise>First name</xsl:otherwise>
              </xsl:choose>
            </paragraph>
          </cell>
          <cell left="true" bottom="true" top="true" right="true" verticalalign="Top" horizontalalign="right">
            <paragraph lineheight="1.1" size="8">
              <xsl:call-template name="StandardValue">
                <xsl:with-param name="xel" select="//XbrlElementDataContract[Prefix='pfs-gcd' and Name='DocumentInformation']/Children/XbrlElementDataContract[Prefix='pfs-gcd' and Name='DocumentContact']/Children/XbrlElementDataContract[Prefix='pfs-gcd' and Name='ContactFirstName' and ContextRef='D']" />
              </xsl:call-template>
            </paragraph>
          </cell>
        </row>
        <row>
          <cell left="true" bottom="true" top="true" right="true" verticalalign="Top" horizontalalign="left">
            <paragraph lineheight="1.1" size="8">
              <xsl:choose>
                <xsl:when test="$Culture='nl-BE'">Functie</xsl:when>
                <xsl:when test="$Culture='fr-FR'">Fonction</xsl:when>
                <xsl:otherwise>Title or Position</xsl:otherwise>
              </xsl:choose>
            </paragraph>
          </cell>
          <cell left="true" bottom="true" top="true" right="true" verticalalign="Top" horizontalalign="right">
            <paragraph lineheight="1.1" size="8">
              <xsl:call-template name="StandardValue">
                <xsl:with-param name="xel" select="//XbrlElementDataContract[Prefix='pfs-gcd' and Name='DocumentInformation']/Children/XbrlElementDataContract[Prefix='pfs-gcd' and Name='DocumentContact']/Children/XbrlElementDataContract[Prefix='pfs-gcd' and Name='ContactTitlePosition' and ContextRef='D']" />
              </xsl:call-template>
            </paragraph>
          </cell>
        </row>
        <row>
          <cell left="true" bottom="true" top="true" right="true" horizontalalign="left" verticalalign="Top" colspan="1">
            <paragraph lineheight="1.1" size="8" fontstyle="bold">
              <xsl:choose>
                <xsl:when test="$Culture='nl-BE'">Adres</xsl:when>
                <xsl:when test="$Culture='fr-FR'">Adresse</xsl:when>
                <xsl:otherwise>Address</xsl:otherwise>
              </xsl:choose>
            </paragraph>
          </cell>
          <cell left="true" bottom="true" top="true" right="true" horizontalalign="left" verticalalign="Top">
            <paragraph lineheight="1.1" size="8">
              <xsl:call-template name="StandardValue">
                <xsl:with-param name="xel" select="//XbrlElementDataContract[Prefix='pfs-gcd' and Name='DocumentInformation']/Children/XbrlElementDataContract[Prefix='pfs-gcd' and Name='DocumentContact']/Children/XbrlElementDataContract[Prefix='pfs-gcd' and Name='ContactAddress' and ContextRef='']" />
              </xsl:call-template>
            </paragraph>
          </cell>
        </row>
        <row>
          <cell left="true" bottom="true" top="true" right="true" horizontalalign="left" verticalalign="Top" colspan="1">
            <paragraph lineheight="1.1" size="8" fontstyle="bold">
              <xsl:choose>
                <xsl:when test="$Culture='nl-BE'">Aard adres</xsl:when>
                <xsl:when test="$Culture='fr-FR'">Type d'adresse</xsl:when>
                <xsl:otherwise>Address type</xsl:otherwise>
              </xsl:choose>
            </paragraph>
          </cell>
          <cell left="true" bottom="true" top="true" right="true" horizontalalign="left" verticalalign="Top">
            <paragraph lineheight="1.1" size="8">
              <xsl:call-template name="StandardValue">
                <xsl:with-param name="xel" select="//XbrlElementDataContract[Prefix='pfs-gcd' and Name='DocumentInformation']/Children/XbrlElementDataContract[Prefix='pfs-gcd' and Name='DocumentContact']/Children/XbrlElementDataContract[Prefix='pfs-gcd' and Name='ContactAddress']/Children/XbrlElementDataContract[Prefix='pfs-gcd' and Name='AddressType' and ContextRef='']" />
              </xsl:call-template>
            </paragraph>
          </cell>
        </row>
        <row>
          <cell left="true" bottom="true" top="true" right="true" verticalalign="Top" horizontalalign="left">
            <paragraph lineheight="1.1" size="8">
              <xsl:choose>
                <xsl:when test="$Culture='nl-BE'">Aard adres</xsl:when>
                <xsl:when test="$Culture='fr-FR'">Type d'adresse</xsl:when>
                <xsl:otherwise>Address type</xsl:otherwise>
              </xsl:choose>
            </paragraph>
          </cell>
          <cell left="true" bottom="true" top="true" right="true" verticalalign="Top" horizontalalign="right">
            <xsl:if test="0=0">
              <xsl:variable name="mel" select="//XbrlElementDataContract[Prefix='pfs-gcd' and Name='DocumentInformation']/Children/XbrlElementDataContract[Prefix='pfs-gcd' and Name='DocumentContact']/Children/XbrlElementDataContract[Prefix='pfs-gcd' and Name='ContactAddress']/Children/XbrlElementDataContract[Prefix='pfs-gcd' and Name='AddressType']" />
              <xsl:for-each select="$mel/Children/XbrlElementDataContract">
                <paragraph lineheight="1.1" size="8">
                  <xsl:choose>
                    <xsl:when test="./Name='AddressTypeOther'">
                      <xsl:call-template name="StandardValue">
                        <xsl:with-param name="xel" select="." />
                      </xsl:call-template>
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:call-template name="GetListValue">
                        <xsl:with-param name="xel" select="." />
                      </xsl:call-template>
                    </xsl:otherwise>
                  </xsl:choose>
                </paragraph>
              </xsl:for-each>
            </xsl:if>
          </cell>
        </row>
        <row>
          <cell left="true" bottom="true" top="true" right="true" verticalalign="Top" horizontalalign="left">
            <paragraph lineheight="1.1" size="8">
              <xsl:choose>
                <xsl:when test="$Culture='nl-BE'">Straat</xsl:when>
                <xsl:when test="$Culture='fr-FR'">Rue</xsl:when>
                <xsl:otherwise>Street</xsl:otherwise>
              </xsl:choose>
            </paragraph>
          </cell>
          <cell left="true" bottom="true" top="true" right="true" verticalalign="Top" horizontalalign="right">
            <paragraph lineheight="1.1" size="8">
              <xsl:call-template name="StandardValue">
                <xsl:with-param name="xel" select="//XbrlElementDataContract[Prefix='pfs-gcd' and Name='DocumentInformation']/Children/XbrlElementDataContract[Prefix='pfs-gcd' and Name='DocumentContact']/Children/XbrlElementDataContract[Prefix='pfs-gcd' and Name='ContactAddress']/Children/XbrlElementDataContract[Prefix='pfs-gcd' and Name='Street' and ContextRef='D']" />
              </xsl:call-template>
            </paragraph>
          </cell>
        </row>
        <row>
          <cell left="true" bottom="true" top="true" right="true" verticalalign="Top" horizontalalign="left">
            <paragraph lineheight="1.1" size="8">
              <xsl:choose>
                <xsl:when test="$Culture='nl-BE'">Nr</xsl:when>
                <xsl:when test="$Culture='fr-FR'">N°</xsl:when>
                <xsl:otherwise>Nr</xsl:otherwise>
              </xsl:choose>
            </paragraph>
          </cell>
          <cell left="true" bottom="true" top="true" right="true" verticalalign="Top" horizontalalign="right">
            <paragraph lineheight="1.1" size="8">
              <xsl:call-template name="StandardValue">
                <xsl:with-param name="xel" select="//XbrlElementDataContract[Prefix='pfs-gcd' and Name='DocumentInformation']/Children/XbrlElementDataContract[Prefix='pfs-gcd' and Name='DocumentContact']/Children/XbrlElementDataContract[Prefix='pfs-gcd' and Name='ContactAddress']/Children/XbrlElementDataContract[Prefix='pfs-gcd' and Name='Number' and ContextRef='D']" />
              </xsl:call-template>
            </paragraph>
          </cell>
        </row>
        <row>
          <cell left="true" bottom="true" top="true" right="true" verticalalign="Top" horizontalalign="left">
            <paragraph lineheight="1.1" size="8">
              <xsl:choose>
                <xsl:when test="$Culture='nl-BE'">Bus</xsl:when>
                <xsl:when test="$Culture='fr-FR'">Boîte</xsl:when>
                <xsl:otherwise>Box</xsl:otherwise>
              </xsl:choose>
            </paragraph>
          </cell>
          <cell left="true" bottom="true" top="true" right="true" verticalalign="Top" horizontalalign="right">
            <paragraph lineheight="1.1" size="8">
              <xsl:call-template name="StandardValue">
                <xsl:with-param name="xel" select="//XbrlElementDataContract[Prefix='pfs-gcd' and Name='DocumentInformation']/Children/XbrlElementDataContract[Prefix='pfs-gcd' and Name='DocumentContact']/Children/XbrlElementDataContract[Prefix='pfs-gcd' and Name='ContactAddress']/Children/XbrlElementDataContract[Prefix='pfs-gcd' and Name='Box' and ContextRef='D']" />
              </xsl:call-template>
            </paragraph>
          </cell>
        </row>
        <row>
          <cell left="true" bottom="true" top="true" right="true" horizontalalign="left" verticalalign="Top" colspan="1">
            <paragraph lineheight="1.1" size="8" fontstyle="bold">
              <xsl:choose>
                <xsl:when test="$Culture='nl-BE'">Postcode en gemeente</xsl:when>
                <xsl:when test="$Culture='fr-FR'">Code postal et commune</xsl:when>
                <xsl:otherwise>Postal code and city</xsl:otherwise>
              </xsl:choose>
            </paragraph>
          </cell>
          <cell left="true" bottom="true" top="true" right="true" horizontalalign="left" verticalalign="Top">
            <paragraph lineheight="1.1" size="8">
              <xsl:call-template name="StandardValue">
                <xsl:with-param name="xel" select="//XbrlElementDataContract[Prefix='pfs-gcd' and Name='DocumentInformation']/Children/XbrlElementDataContract[Prefix='pfs-gcd' and Name='DocumentContact']/Children/XbrlElementDataContract[Prefix='pfs-gcd' and Name='ContactAddress']/Children/XbrlElementDataContract[Prefix='pfs-gcd' and Name='PostalCodeCity' and ContextRef='']" />
              </xsl:call-template>
            </paragraph>
          </cell>
        </row>
        <row>
          <cell left="true" bottom="true" top="true" right="true" verticalalign="Top" horizontalalign="left">
            <paragraph lineheight="1.1" size="8">
              <xsl:choose>
                <xsl:when test="$Culture='nl-BE'">Postcode en gemeente</xsl:when>
                <xsl:when test="$Culture='fr-FR'">Code postal et commune</xsl:when>
                <xsl:otherwise>Postal code and city</xsl:otherwise>
              </xsl:choose>
            </paragraph>
          </cell>
          <cell left="true" bottom="true" top="true" right="true" verticalalign="Top" horizontalalign="right">
            <xsl:if test="0=0">
              <xsl:variable name="mel" select="//XbrlElementDataContract[Prefix='pfs-gcd' and Name='DocumentInformation']/Children/XbrlElementDataContract[Prefix='pfs-gcd' and Name='DocumentContact']/Children/XbrlElementDataContract[Prefix='pfs-gcd' and Name='ContactAddress']/Children/XbrlElementDataContract[Prefix='pfs-gcd' and Name='PostalCodeCity']" />
              <xsl:for-each select="$mel/Children/XbrlElementDataContract">
                <paragraph lineheight="1.1" size="8">
                  <xsl:choose>
                    <xsl:when test="./Name='PostalCodeOther'">
                      <xsl:call-template name="StandardValue">
                        <xsl:with-param name="xel" select="." />
                      </xsl:call-template>
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:call-template name="GetListValue">
                        <xsl:with-param name="xel" select="." />
                      </xsl:call-template>
                    </xsl:otherwise>
                    <xsl:when test="./Name='City'">
                      <xsl:call-template name="StandardValue">
                        <xsl:with-param name="xel" select="." />
                      </xsl:call-template>
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:call-template name="GetListValue">
                        <xsl:with-param name="xel" select="." />
                      </xsl:call-template>
                    </xsl:otherwise>
                  </xsl:choose>
                </paragraph>
              </xsl:for-each>
            </xsl:if>
          </cell>
        </row>
        <row>
          <cell left="true" bottom="true" top="true" right="true" horizontalalign="left" verticalalign="Top" colspan="1">
            <paragraph lineheight="1.1" size="8" fontstyle="bold">
              <xsl:choose>
                <xsl:when test="$Culture='nl-BE'">Land</xsl:when>
                <xsl:when test="$Culture='fr-FR'">Pays</xsl:when>
                <xsl:otherwise>Country</xsl:otherwise>
              </xsl:choose>
            </paragraph>
          </cell>
          <cell left="true" bottom="true" top="true" right="true" horizontalalign="left" verticalalign="Top">
            <paragraph lineheight="1.1" size="8">
              <xsl:call-template name="StandardValue">
                <xsl:with-param name="xel" select="//XbrlElementDataContract[Prefix='pfs-gcd' and Name='DocumentInformation']/Children/XbrlElementDataContract[Prefix='pfs-gcd' and Name='DocumentContact']/Children/XbrlElementDataContract[Prefix='pfs-gcd' and Name='ContactAddress']/Children/XbrlElementDataContract[Prefix='pfs-gcd' and Name='CountryCode' and ContextRef='']" />
              </xsl:call-template>
            </paragraph>
          </cell>
        </row>
        <row>
          <cell left="true" bottom="true" top="true" right="true" verticalalign="Top" horizontalalign="left">
            <paragraph lineheight="1.1" size="8">
              <xsl:choose>
                <xsl:when test="$Culture='nl-BE'">Land</xsl:when>
                <xsl:when test="$Culture='fr-FR'">Pays</xsl:when>
                <xsl:otherwise>Country</xsl:otherwise>
              </xsl:choose>
            </paragraph>
          </cell>
          <cell left="true" bottom="true" top="true" right="true" verticalalign="Top" horizontalalign="right">
            <xsl:if test="0=0">
              <xsl:variable name="mel" select="//XbrlElementDataContract[Prefix='pfs-gcd' and Name='DocumentInformation']/Children/XbrlElementDataContract[Prefix='pfs-gcd' and Name='DocumentContact']/Children/XbrlElementDataContract[Prefix='pfs-gcd' and Name='ContactAddress']/Children/XbrlElementDataContract[Prefix='pfs-gcd' and Name='CountryCode']" />
              <xsl:for-each select="$mel/Children/XbrlElementDataContract">
                <paragraph lineheight="1.1" size="8">
                  <xsl:choose>
                    <xsl:when test="./Name='CountryCodeOther'">
                      <xsl:call-template name="StandardValue">
                        <xsl:with-param name="xel" select="." />
                      </xsl:call-template>
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:call-template name="GetListValue">
                        <xsl:with-param name="xel" select="." />
                      </xsl:call-template>
                    </xsl:otherwise>
                  </xsl:choose>
                </paragraph>
              </xsl:for-each>
            </xsl:if>
          </cell>
        </row>
        <row>
          <cell left="true" bottom="true" top="true" right="true" horizontalalign="left" verticalalign="Top" colspan="1">
            <paragraph lineheight="1.1" size="8" fontstyle="bold">
              <xsl:choose>
                <xsl:when test="$Culture='nl-BE'">Telefoonnummer of faxnummer</xsl:when>
                <xsl:when test="$Culture='fr-FR'">Téléphone ou Fax</xsl:when>
                <xsl:otherwise>Phone or fax</xsl:otherwise>
              </xsl:choose>
            </paragraph>
          </cell>
          <cell left="true" bottom="true" top="true" right="true" horizontalalign="left" verticalalign="Top">
            <paragraph lineheight="1.1" size="8">
              <xsl:call-template name="StandardValue">
                <xsl:with-param name="xel" select="//XbrlElementDataContract[Prefix='pfs-gcd' and Name='DocumentInformation']/Children/XbrlElementDataContract[Prefix='pfs-gcd' and Name='DocumentContact']/Children/XbrlElementDataContract[Prefix='pfs-gcd' and Name='ContactPhoneFaxNumber' and ContextRef='']" />
              </xsl:call-template>
            </paragraph>
          </cell>
        </row>
        <row>
          <cell left="true" bottom="true" top="true" right="true" verticalalign="Top" horizontalalign="left">
            <paragraph lineheight="1.1" size="8">
              <xsl:choose>
                <xsl:when test="$Culture='nl-BE'">Aanduiding of het een telefoon- dan wel een faxnummer betreft</xsl:when>
                <xsl:when test="$Culture='fr-FR'">Description du téléphone ou fax</xsl:when>
                <xsl:otherwise>Description of the phone or fax number</xsl:otherwise>
              </xsl:choose>
            </paragraph>
          </cell>
          <cell left="true" bottom="true" top="true" right="true" verticalalign="Top" horizontalalign="right">
            <paragraph lineheight="1.1" size="8">
              <xsl:call-template name="StandardValue">
                <xsl:with-param name="xel" select="//XbrlElementDataContract[Prefix='pfs-gcd' and Name='DocumentInformation']/Children/XbrlElementDataContract[Prefix='pfs-gcd' and Name='DocumentContact']/Children/XbrlElementDataContract[Prefix='pfs-gcd' and Name='ContactPhoneFaxNumber']/Children/XbrlElementDataContract[Prefix='pfs-gcd' and Name='PhoneFaxNumber' and ContextRef='D']" />
              </xsl:call-template>
            </paragraph>
          </cell>
        </row>
        <row>
          <cell left="true" bottom="true" top="true" right="true" verticalalign="Top" horizontalalign="left">
            <paragraph lineheight="1.1" size="8">
              <xsl:choose>
                <xsl:when test="$Culture='nl-BE'">Lokaal telefoonnummer</xsl:when>
                <xsl:when test="$Culture='fr-FR'">Numéro local</xsl:when>
                <xsl:otherwise>Local phone number</xsl:otherwise>
              </xsl:choose>
            </paragraph>
          </cell>
          <cell left="true" bottom="true" top="true" right="true" verticalalign="Top" horizontalalign="right">
            <paragraph lineheight="1.1" size="8">
              <xsl:call-template name="StandardValue">
                <xsl:with-param name="xel" select="//XbrlElementDataContract[Prefix='pfs-gcd' and Name='DocumentInformation']/Children/XbrlElementDataContract[Prefix='pfs-gcd' and Name='DocumentContact']/Children/XbrlElementDataContract[Prefix='pfs-gcd' and Name='ContactPhoneFaxNumber']/Children/XbrlElementDataContract[Prefix='pfs-gcd' and Name='LocalPhoneNumber' and ContextRef='D']" />
              </xsl:call-template>
            </paragraph>
          </cell>
        </row>
        <row>
          <cell left="true" bottom="true" top="true" right="true" horizontalalign="left" verticalalign="Top" colspan="1">
            <paragraph lineheight="1.1" size="8" fontstyle="bold">
              <xsl:choose>
                <xsl:when test="$Culture='nl-BE'">E-mail</xsl:when>
                <xsl:when test="$Culture='fr-FR'">E-mail</xsl:when>
                <xsl:otherwise>E-mail</xsl:otherwise>
              </xsl:choose>
            </paragraph>
          </cell>
          <cell left="true" bottom="true" top="true" right="true" horizontalalign="left" verticalalign="Top">
            <paragraph lineheight="1.1" size="8">
              <xsl:call-template name="StandardValue">
                <xsl:with-param name="xel" select="//XbrlElementDataContract[Prefix='pfs-gcd' and Name='DocumentInformation']/Children/XbrlElementDataContract[Prefix='pfs-gcd' and Name='DocumentContact']/Children/XbrlElementDataContract[Prefix='pfs-gcd' and Name='ContactEmail' and ContextRef='']" />
              </xsl:call-template>
            </paragraph>
          </cell>
        </row>
        <row>
          <cell left="true" bottom="true" top="true" right="true" verticalalign="Top" horizontalalign="left">
            <paragraph lineheight="1.1" size="8">
              <xsl:choose>
                <xsl:when test="$Culture='nl-BE'">E-mail adres</xsl:when>
                <xsl:when test="$Culture='fr-FR'">Adresse E-mail</xsl:when>
                <xsl:otherwise>E-mail address</xsl:otherwise>
              </xsl:choose>
            </paragraph>
          </cell>
          <cell left="true" bottom="true" top="true" right="true" verticalalign="Top" horizontalalign="right">
            <paragraph lineheight="1.1" size="8">
              <xsl:call-template name="StandardValue">
                <xsl:with-param name="xel" select="//XbrlElementDataContract[Prefix='pfs-gcd' and Name='DocumentInformation']/Children/XbrlElementDataContract[Prefix='pfs-gcd' and Name='DocumentContact']/Children/XbrlElementDataContract[Prefix='pfs-gcd' and Name='ContactEmail']/Children/XbrlElementDataContract[Prefix='pfs-gcd' and Name='EmailAddress' and ContextRef='D']" />
              </xsl:call-template>
            </paragraph>
          </cell>
        </row>
      </table>
    </Root>
  </xsl:template>
  <xsl:template name="GetTitleDuration">
    <xsl:choose>
      <xsl:when test="$Culture='nl-BE'">Belastbaar tijdperk</xsl:when>
      <xsl:when test="$Culture='fr-FR'">Période imposable</xsl:when>
      <xsl:otherwise>Taxable Era</xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  <xsl:template name="GetTitleIStart">
    <xsl:choose>
      <xsl:when test="$Culture='nl-BE'">Bij het begin van het belastbare tijdperk</xsl:when>
      <xsl:when test="$Culture='fr-FR'">Au début de la période imposable</xsl:when>
      <xsl:otherwise>At start of the taxable era</xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  <xsl:template name="GetTitleIEnd">
    <xsl:choose>
      <xsl:when test="$Culture='nl-BE'">Bij het einde van het belastbare tijdperk</xsl:when>
      <xsl:when test="$Culture='fr-FR'">A la fin de la période imposable</xsl:when>
      <xsl:otherwise>At end of the taxable era</xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  <xsl:template name="GetListValue">
    <xsl:param name="xel" />
    <xsl:param name="myval" />
    <xsl:variable name="value">
      <xsl:choose>
        <xsl:when test="$xel">
          <xsl:value-of select="$xel/FullName" />
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="$myval" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:variable name="ctxlbl" select="$lists//item[@value=$value]" />
    <xsl:choose>
      <xsl:when test="$Culture='nl-BE'">
        <xsl:value-of select="$ctxlbl/labels/Label[./Language='nl']/Text" />
      </xsl:when>
      <xsl:when test="$Culture='fr-FR'">
        <xsl:value-of select="$ctxlbl/labels/Label[./Language='fr']/Text" />
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$ctxlbl/labels/Label[./Language='en']/Text" />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  <xsl:template name="BooleanValue">
    <xsl:param name="xel" />
    <xsl:variable name="value" select="$xel/Value" />
    <xsl:choose>
      <xsl:when test="$value='true'">X</xsl:when>
      <xsl:when test="$value='false'">/</xsl:when>
      <xsl:otherwise> </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  <xsl:template name="BinaryValue">
    <xsl:param name="xel" />
    <xsl:variable name="value" select="$xel/BinaryValue" />
    <xsl:choose>
      <xsl:when test="not($value) or $value='' or $value='0'"> </xsl:when>
      <xsl:otherwise>
        <list ordered="false" numbered="false" lettered="false">
          <xsl:for-each select="$value/IndexItemBaseDataContract">
            <listitem>
              - <xsl:value-of select="title" />
            </listitem>
          </xsl:for-each>
        </list>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  <xsl:template name="MonetaryValue">
    <xsl:param name="xel" />
    <xsl:variable name="value" select="$xel/Value" />
    <xsl:choose>
      <xsl:when test="not($value) or $value='' or $value='0'"> </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="format-number($value,$NumberFormatting,'euro')" />€
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  <xsl:template name="NumberValue">
    <xsl:param name="xel" />
    <xsl:variable name="value" select="$xel/Value" />
    <xsl:choose>
      <xsl:when test="not($value) or $value='' or $value='0'"> </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="format-number($value,$NumberFormatting,'euro')" />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  <xsl:template name="PercentValue">
    <xsl:param name="xel" />
    <xsl:variable name="value" select="$xel/Value" />
    <xsl:choose>
      <xsl:when test="not($value) or $value='' or $value='0'"> </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="format-number(number($value) * 100,$NumberFormatting,'euro')" />%
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  <xsl:template name="StandardValue">
    <xsl:param name="xel" />
    <xsl:variable name="value" select="$xel/Value" />
    <xsl:choose>
      <xsl:when test="not($value) or $value='' or $value='0'"> </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$value" />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  <xsl:template name="GetContextsOF">
    <xsl:param name="prescontext" />
    <xsl:variable name="tmp">
      <xsl:for-each select="//ContextElementDataContract[PresContextId=$prescontext]/DefId">
        <xsl:sort data-type="text" select="." />
        <xsl:copy-of select="." />
      </xsl:for-each>
    </xsl:variable>
    <contextdefs>
      <xsl:for-each select="msxsl:node-set($tmp)/DefId">
        <xsl:sort data-type="text" select="." />
        <xsl:if test="not(.=preceding-sibling::DefId) or position()=1">
          <defid>
            <xsl:value-of select="." />
          </defid>
        </xsl:if>
      </xsl:for-each>
    </contextdefs>
  </xsl:template>
</xsl:stylesheet>