﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EY.com.eBook.BizTaxParser.Test2
{
    public class ConceptDimension
    {
        public string HyperCubeId { get; set; }

        public string RoleUri { get; set; }

        public List<Concept> Concepts { get; set; }

        public List<Dimension> Dimensions { get; set; }
    }
}
