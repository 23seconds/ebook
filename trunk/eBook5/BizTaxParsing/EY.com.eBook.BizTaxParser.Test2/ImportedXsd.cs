﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EY.com.eBook.BizTaxParser.Test2
{
    public class ImportedXsd
    {
        public string FilePath { get; set; }

        public string FileName { get; set; }

        public string TargetNamespace { get; set; }
    }
}
