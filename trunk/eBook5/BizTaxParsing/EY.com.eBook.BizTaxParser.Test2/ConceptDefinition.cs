﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EY.com.eBook.BizTaxParser.Test2
{
    public class ConceptDefinition
    {
        public string ContextFile { get; set; }

        public string ElementHref { get; set; }

        public List<string> Children {get;set;}

        public string HyperCubeHref { get; set; }

        public string HyperCubeTargetRole { get; set; }

        public HyperCubeConceptRole HyperCubeConceptRole { get; set; } // All or NotAll

        public string ContextElement { get; set; }

        // IF NO CONTEXT ELEMENT => empty hyper cube =>  PeriodType based context (duration / Instant:I-Start/I-End)
    }
}
