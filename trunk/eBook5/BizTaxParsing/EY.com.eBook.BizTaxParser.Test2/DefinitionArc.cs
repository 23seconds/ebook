﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace EY.com.eBook.BizTaxParser.Test2
{
    public class DefinitionArc : Arc
    {
        public static DefinitionArc CreateFromElement(XElement element, Dictionary<string, XNamespace> namespaces)
        {
            return CreateFromElement<DefinitionArc>(element, namespaces);
        }

        public static T CreateFromElement<T>(XElement element, Dictionary<string, XNamespace> namespaces) where T : DefinitionArc, new()
        {
            T arc= Arc.CreateFromElement<T>(element, namespaces);
            string order = element.Attribute("order") != null ? element.Attribute("order").Value : null;
            int iorder = -1;
            int.TryParse(order, out iorder);

            arc.Order = iorder;
            arc.Use = element.Attribute("use") != null ? element.Attribute("use").Value : null;

            arc.TargetRole= element.Attribute(namespaces["xbrldt"] + "targetRole") != null ? element.Attribute(namespaces["xbrldt"] + "targetRole").Value : null;
            arc.ContextElement = element.Attribute(namespaces["xbrldt"] + "contextElement") != null ? element.Attribute(namespaces["xbrldt"] + "contextElement").Value : null;
            arc.Closed= element.Attribute(namespaces["xbrldt"] + "closed") != null ? element.Attribute(namespaces["xbrldt"] + "closed").Value=="true" : false;
            return arc;
        }

        public int Order { get; set; }

        public string Use { get; set; }

        public string TargetRole { get; set; }
        public string ContextElement { get; set; }
        public bool Closed { get; set; }


    }
}
