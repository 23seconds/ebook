﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl"
>
  <xsl:output method="text" indent="yes"/>

  <xsl:variable name="assess" select="biztax/@assessment"/>
  <xsl:variable name="culture" select="biztax/@lang"/>
  <xsl:variable name="target" select="biztax/@type"/>
  <xsl:variable name="smallcase" select="'abcdefghijklmnopqrstuvwxyz'" />
  <xsl:variable name="uppercase" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZ'" />
  <xsl:template match="biztax">
    // FIND OUT WHICH (rcorp/nrcorp/rle)
    
    eBook.BizTax.AY<xsl:value-of select="$assess"/> = { xtype:'biztax-module',
      targetCorp:'<xsl:value-of select="$target"/>'
      ,items:[
    <xsl:for-each select="tabs/tab">
      <xsl:if test="position()>1">,</xsl:if>
        <xsl:apply-templates select="." mode="TAB"/>
    </xsl:for-each>
    ] };

  </xsl:template>

  <xsl:template match="*" mode="DEFAULT_ATTRIBS">
    , code: '<xsl:value-of select="@code"/>'
    , oldCode: '<xsl:value-of select="@oldCode"/>'
    , xbrlId: '<xsl:value-of select="@xbrlId"/>'
    , xbrlName: '<xsl:value-of select="@xbrlName"/>'
    , itemType:'<xsl:value-of select="@itemType"/>'
    , isAbstract:<xsl:value-of select="@isAbstract='true'"/>
    <xsl:if test="@periodType and not(@periodType='')">
    ,periodType:'<xsl:value-of select="@periodType"/>'
    </xsl:if>
    <xsl:if test="@totalField">
      ,totalField:<xsl:value-of select="@totalField"/>
    </xsl:if>
  </xsl:template>

  <xsl:template match="tab" mode="TAB">
    {xtype:'biztax-tab',title:'<xsl:value-of select="@title"/>'
    <xsl:apply-templates select="." mode="DEFAULT_ATTRIBS"/>
    , items:[
    <xsl:for-each select="items/*">
      <xsl:if test="position()>1">,</xsl:if>
      <xsl:apply-templates select="." mode="RENDER_ITEM"/>
    </xsl:for-each>
    ]}
  </xsl:template>

  <xsl:template match="pane" mode="RENDER_ITEM">
    <xsl:param name="parentXype"/>
    <xsl:variable name="concepts" select="items/*/Concept[not(. = ../following-sibling::Concept)]"/>
    <xsl:variable name="xtype">
      <xsl:choose>
        <xsl:when test="count($concepts)>0">
          <xsl:choose>
            <xsl:when test="not(not($concepts[count(dimensions/dimension)=count(dimensions/dimension[@type='ExplicitDimension'])]))">biztax-fixedtable</xsl:when>
            <xsl:otherwise>biztax-grid</xsl:otherwise>
          </xsl:choose>
          </xsl:when>
        <xsl:otherwise>biztax-pane</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    {xtype:'<xsl:value-of select="$xtype"/>', title:'<xsl:value-of select="@title"/>'
    <xsl:apply-templates select="." mode="DEFAULT_ATTRIBS"/>
    , hasInstantPeriods:<xsl:value-of select="@hasInstantPeriods='true'"/>
    , hasDurationPeriods:<xsl:value-of select="@hasDurationPeriods='true'"/>
    <xsl:if test="items/*[not(local-name()='pane')][1]">
      ,isValuePane:true        
    </xsl:if>
    , contextDefinitions:[
    <xsl:for-each select="$concepts">
      <xsl:if test="position()>1">,</xsl:if>
      <xsl:apply-templates select="." mode="ContextDef"/>
    </xsl:for-each>
    ]
    ,items:[
    <xsl:for-each select="items/*">
      <xsl:if test="position()>1">,</xsl:if>
      <xsl:apply-templates select="." mode="RENDER_ITEM"/>
    </xsl:for-each>
    ]
    }

  </xsl:template>

  <xsl:template match="Concept" mode="ContextDef">
    {
      ceHref:'<xsl:value-of select="@ConceptElementHref"/>'
      ,contextElement:'<xsl:value-of select="@ContextElement"/>'
      ,role:'<xsl:value-of select="@HyperCubeConceptRole"/>'
      ,hyHref:'<xsl:value-of select="@HyperCubeHref"/>'
      ,targetRole:'<xsl:value-of select="@HyperCubeTargetRole"/>'
      ,dimensions:[<xsl:for-each select="dimensions/*">
        <xsl:if test="position()>1">,</xsl:if><xsl:apply-templates select="." mode="DimensionDef"/>
      </xsl:for-each>]
    }
</xsl:template>

  <xsl:template match="dimension" mode="DimensionDef">
    {
      type:'<xsl:value-of select="@type"/>'
      ,domains:[<xsl:for-each select="dimensionDomain">
    <xsl:if test="position()>1">,</xsl:if>
    <xsl:apply-templates select="." mode="DomainDef"/>
  </xsl:for-each>]
    <xsl:if test="@type='TypedDimension' and definition">
      ,definition:{periodType:'<xsl:value-of select="@periodType"/>',balance:'<xsl:value-of select="@balance"/>'
                  ,fixed:'<xsl:value-of select="@fixed"/>',fixed:'<xsl:value-of select="@fixed"/>'
                  ,name:'<xsl:value-of select="@name"/>',nillable:'<xsl:value-of select="@nillable"/>'
                  ,ref:'<xsl:value-of select="@ref"/>',substitutionGroup:'<xsl:value-of select="@substitutionGroup"/>'
                  ,type:'<xsl:value-of select="@type"/>',typedDomainRef:'<xsl:value-of select="@typedDomainRef"/>'
      }
    </xsl:if>
   }
</xsl:template>

  <xsl:template match="dimensionDomain" mode="DomainDef">
    {periodType:'<xsl:value-of select="@periodType"/>',balance:'<xsl:value-of select="@balance"/>'
    ,fixed:'<xsl:value-of select="@fixed"/>',fixed:'<xsl:value-of select="@fixed"/>'
    ,name:'<xsl:value-of select="@name"/>',nillable:'<xsl:value-of select="@nillable"/>'
    ,ref:'<xsl:value-of select="@ref"/>',substitutionGroup:'<xsl:value-of select="@substitutionGroup"/>'
    ,type:'<xsl:value-of select="@type"/>',typedDomainRef:'<xsl:value-of select="@typedDomainRef"/>'
    , members:[<xsl:for-each select="domainMembers/*">
      <xsl:if test="position()>1">,</xsl:if>
      <xsl:apply-templates select="." mode="MemberDef"/>
    </xsl:for-each>]
    }
  </xsl:template>
  
  <xsl:template match="domainMember" mode="MemberDef">
    {
    periodType:'<xsl:value-of select="@periodType"/>',balance:'<xsl:value-of select="@balance"/>'
    ,fixed:'<xsl:value-of select="@fixed"/>',fixed:'<xsl:value-of select="@fixed"/>'
    ,name:'<xsl:value-of select="@name"/>',nillable:'<xsl:value-of select="@nillable"/>'
    ,ref:'<xsl:value-of select="@ref"/>',substitutionGroup:'<xsl:value-of select="@substitutionGroup"/>'
    ,type:'<xsl:value-of select="@type"/>',typedDomainRef:'<xsl:value-of select="@typedDomainRef"/>'
    ,title:'<xsl:apply-templates select="." mode="GetLabel"><xsl:with-param name="type">label</xsl:with-param></xsl:apply-templates>'
    ,qtip:'<xsl:apply-templates select="." mode="GetLabel"><xsl:with-param name="type">verboseLabel</xsl:with-param></xsl:apply-templates>'
    }
  </xsl:template>
  
  <xsl:template match="*" mode="GetLabel"><xsl:param name="type"/><xsl:variable name="apos">'</xsl:variable><xsl:variable name="aposEsc">-</xsl:variable><xsl:variable name="txt"><xsl:call-template name="string-replace-all"><xsl:with-param name="text" select="string(labels/label[@type=$type]/translation[@lang=$culture])" /><xsl:with-param name="replace" select="$apos" /><xsl:with-param name="by" select="$aposEsc" /></xsl:call-template></xsl:variable><xsl:value-of select="translate($txt,$apos,$aposEsc)"/></xsl:template>

  <xsl:template name="string-replace-all">
    <xsl:param name="text" />
    <xsl:param name="replace" />
    <xsl:param name="by" />
    <xsl:choose>
      <xsl:when test="contains($text, $replace)">
        <xsl:value-of select="substring-before($text,$replace)" />
        <xsl:value-of select="$by" />
        <xsl:call-template name="string-replace-all">
          <xsl:with-param name="text"
          select="substring-after($text,$replace)" />
          <xsl:with-param name="replace" select="$replace" />
          <xsl:with-param name="by" select="$by" />
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$text" />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="valuePane" mode="RENDER_ITEM">
    {xtype:'biztax-pane', title:'<xsl:value-of select="@title"/>'
    <xsl:apply-templates select="." mode="DEFAULT_ATTRIBS"/>
    , hasInstantPeriods:<xsl:value-of select="@hasInstantPeriods='true'"/>
    , hasDurationPeriods:<xsl:value-of select="@hasDurationPeriods='true'"/>
    ,items:[
    <xsl:for-each select="items/*">
      <xsl:if test="position()>1">,</xsl:if>
      <xsl:apply-templates select="." mode="RENDER_ITEM"/>
    </xsl:for-each>
    ]
    }

  </xsl:template>
  

  <xsl:template match="field" mode="RENDER_ITEM">
    {xtype:'biztax-field', label:'<xsl:value-of select="@title"/>'
    <xsl:apply-templates select="." mode="DEFAULT_ATTRIBS"/>
    <xsl:if test="Translations/Role[@id='totalLabel']">,totalField:true</xsl:if>
    }
  </xsl:template>


  <xsl:template match="label" mode="RENDER_ITEM">
    {xtype:'biztax-field', label:'<xsl:value-of select="@title"/>'
    <xsl:apply-templates select="." mode="DEFAULT_ATTRIBS"/>
    <xsl:if test="Translations/Role[@id='totalLabel']">,totalField:true</xsl:if>
    }
  </xsl:template>
  
  <xsl:template match="valueList" mode="RENDER_ITEM">
    {xtype:'biztax-list', label:'<xsl:value-of select="@title"/>'
    <xsl:apply-templates select="." mode="DEFAULT_ATTRIBS"/>
    , substitutionGroup: '<xsl:value-of select="@substitutionGroup"/>'
    , data:[<xsl:apply-templates select="data" mode="LISTITEM_DATA"/>]
    }
  </xsl:template>

  <xsl:template match="data" mode="LISTITEM_DATA">
    <xsl:for-each select="item">
      <xsl:if test="position()>1">,</xsl:if>['<xsl:value-of select="@id"/>','<xsl:value-of select="@text"/>']
    </xsl:for-each>
  </xsl:template>


  <xsl:template match="uploadField" mode="RENDER_ITEM">
    {xtype:'biztax-uploadfield', label:'<xsl:value-of select="@title"/>'
    <xsl:apply-templates select="." mode="DEFAULT_ATTRIBS"/>
    , isRequired:<xsl:value-of select="@itemType='http://www.nbb.be/be/fr/pfs/ci/dt/2012-04-01:nonEmptyBase64BinaryItemType'"/>
    }
  </xsl:template>

</xsl:stylesheet>
