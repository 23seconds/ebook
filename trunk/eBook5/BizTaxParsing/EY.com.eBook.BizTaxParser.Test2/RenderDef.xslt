﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl"
>
    <xsl:output method="xml" indent="yes"/>
  <xsl:param name="assess">2012</xsl:param>
  <xsl:param name="culture">nl</xsl:param>
  <xsl:param name="btype">rcorp</xsl:param>

  <xsl:variable name="smallcase" select="'abcdefghijklmnopqrstuvwxyz'" />
  <xsl:variable name="uppercase" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZ'" />
  <xsl:template match="root">

    <biztax assessment="{$assess}" type="{$btype}" lang="{$culture}">
      <tabs>

        <xsl:for-each select="Tabs/Element[count(Element[not(contains(string(@id),'Head'))])>0]">
          <xsl:for-each select="Element[not(contains(string(@id),'Head'))]">
            <xsl:apply-templates select="." mode="TAB"/>
          </xsl:for-each>
        </xsl:for-each>
      </tabs>
    </biztax>
  </xsl:template>
  
  <xsl:template match="Element" mode="DEFAULT_ATTRIBS">
    <xsl:if test="starts-with(@id,'tax-inc')">
      <xsl:attribute name="code">
        <xsl:value-of select="labels/label[@type='documentation']/translation[@lang='en']"/>
      </xsl:attribute>
      <xsl:attribute name="oldCode">
        <xsl:value-of select="labels/label[@type='documentation']/translation[@lang='en-US']"/>
      </xsl:attribute>
    </xsl:if>
    <xsl:attribute name="xbrlId">
      <xsl:value-of select="@id"/>
    </xsl:attribute>
    <xsl:attribute name="xbrlName">
      <xsl:value-of select="@name"/>
    </xsl:attribute>
    <xsl:attribute name="itemType">
      <xsl:value-of select="@type"/>
    </xsl:attribute>
    <xsl:attribute name="concept">
      <xsl:choose>
        <xsl:when test="not(@primaryConcept='d-hh_AdimensionalPrimaryConcepts' or @primaryConcept='d-hh_AdimensionalPrimaryConcepts')">
          <xsl:value-of select="@primaryConcept"/>
        </xsl:when>
      </xsl:choose>
    </xsl:attribute>
    <xsl:attribute name="isAbstract">
      <xsl:choose>
        <xsl:when test="@abstract">
          <xsl:value-of select="translate(@abstract, $uppercase, $smallcase)"/>
        </xsl:when>
        <xsl:otherwise>false</xsl:otherwise>
      </xsl:choose>
    </xsl:attribute>
  </xsl:template>
  
  <xsl:template match="Element" mode="TAB">
    <xsl:variable name="title">
      <xsl:apply-templates select="." mode="GetLabel"/>
    </xsl:variable>
    <tab id="{@id}" title="{$title}">
      <xsl:apply-templates select="." mode="DEFAULT_ATTRIBS"/>
      <!--xsl:apply-templates select="." mode="CONTEXT"/-->
      <xsl:apply-templates select="." mode="GetDefinition">
        <xsl:with-param name="href" select="string(@href)"/>
      </xsl:apply-templates>
      <!--ContextDefinitions-->

      <items>
        <!--xsl:apply-templates select="Element[not(contains(string(@id),'XCode'))]" mode="PANE"/-->
      </items>
    </tab>
  </xsl:template>
  
  <xsl:template match="Element" mode="GetLabel"><xsl:variable name="apos">'</xsl:variable><xsl:variable name="aposEsc">-</xsl:variable><xsl:variable name="txt"><xsl:call-template name="string-replace-all"><xsl:with-param name="text" select="string(labels/label[@type='label']/translation[@lang=$culture])" /><xsl:with-param name="replace" select="$apos" /><xsl:with-param name="by" select="$aposEsc" /></xsl:call-template></xsl:variable><xsl:value-of select="translate($txt,$apos,$aposEsc)"/></xsl:template>

  <xsl:template name="string-replace-all">
    <xsl:param name="text" />
    <xsl:param name="replace" />
    <xsl:param name="by" />
    <xsl:choose>
      <xsl:when test="contains($text, $replace)">
        <xsl:value-of select="substring-before($text,$replace)" />
        <xsl:value-of select="$by" />
        <xsl:call-template name="string-replace-all">
          <xsl:with-param name="text"
          select="substring-after($text,$replace)" />
          <xsl:with-param name="replace" select="$replace" />
          <xsl:with-param name="by" select="$by" />
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$text" />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="Element" mode="GetDefinition">
    <xsl:param name="href"/>
    <xsl:copy-of select="//Elements/ArrayOfXbrlElementDefinition/XbrlElementDefinition[Href=$href]"/>
  </xsl:template>
  
</xsl:stylesheet>
