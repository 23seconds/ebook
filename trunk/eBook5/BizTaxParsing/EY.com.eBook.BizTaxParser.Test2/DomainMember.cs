﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.BizTaxParser.Test2
{
    [Serializable]
    [DataContract]
    public class DomainMember
    {
        [DataMember]
        public string Id { get; set; }

        [DataMember]
        public XbrlElementDefinition ElementDefinition { get; set; }
    }
}
