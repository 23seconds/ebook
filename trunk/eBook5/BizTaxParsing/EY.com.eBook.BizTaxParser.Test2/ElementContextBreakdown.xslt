﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl"
>
    <xsl:output method="xml" indent="yes"/>

    <xsl:template match="root">
      <root>
      <xsl:apply-templates select="//XbrlElementDefinition[Href='be-tax-inc-2012-04-30.xsd#tax-inc_AllowanceInvestmentDeduction']"/>
      </root>
    </xsl:template>

  <xsl:template match="XbrlElementDefinition">
    <element id="{Id}" href="{Href}">
      <contexts>
        <xsl:apply-templates select="." mode="CONTEXTS"/>
      </contexts>
    </element>
  </xsl:template>


  <xsl:template match="XbrlElementDefinition" mode="CONTEXTS">
    <xsl:variable name="roles" select="Contexts/Context/RoleId[not(. = ../../following-sibling::Context/RoleId)]"/>
    <xsl:for-each select="Contexts/Context/RoleId[not(. = ../following-sibling::Context/RoleId)]">
      <context>
        <xsl:variable name="roleId"><xsl:value-of select="."/></xsl:variable>
        <xsl:attribute name="roleId"><xsl:value-of select="$roleId"/></xsl:attribute>

        <xsl:for-each select="Context[string(RoleId)=$roleId]">
          
        </xsl:for-each>
      </context>
    </xsl:for-each>
  </xsl:template>
</xsl:stylesheet>
