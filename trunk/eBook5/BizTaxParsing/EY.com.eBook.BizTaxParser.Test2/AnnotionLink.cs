﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EY.com.eBook.BizTaxParser.Test2
{
    public class AnnotionLink
    {
        public string Origin { get; set; }
        
        public string RoleUri { get; set; }
        public string Id { get; set; }
        public string Definition { get; set; }

        public string NameHref { get; set; }
        // type of link
        public string UsedOn { get; set; }
    }
}
