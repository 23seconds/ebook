﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl"
>
    <xsl:output method="text" indent="yes"/>
  <xsl:param name="assess">2012</xsl:param>
  <xsl:param name="culture">nl</xsl:param>
  <xsl:variable name="smallcase" select="'abcdefghijklmnopqrstuvwxyz'" />
  <xsl:variable name="uppercase" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZ'" />
  <xsl:template match="root">
    // FIND OUT WHICH (rcorp/nrcorp/rle)
    eBook.BizTax.AY<xsl:value-of select="$assess"/> = { xtype:'biztax-module',
          items:[
            <xsl:for-each select="Tabs/Element[count(Element[not(contains(string(@id),'Head'))])>0]">
              <xsl:variable name="hasComma" select="position()>1"/>
              <xsl:if test="position()>1">,</xsl:if>
              <xsl:for-each select="Element[not(contains(string(@id),'Head'))]">
                <xsl:if test="position()>1 and $hasComma">,</xsl:if>
                <xsl:apply-templates select="." mode="TAB"/>
              </xsl:for-each>
            </xsl:for-each>
        
    
   ] };
    
  </xsl:template>
  
  <xsl:template match="Element" mode="DEFAULT_ATTRIBS">
    , code: '<xsl:value-of select="labels/label[@type='documentation']/translation[@lang='en']"/>'
    , oldCode: '<xsl:value-of select="labels/label[@type='documentation']/translation[@lang='en-US']"/>'
    , xbrlId: '<xsl:value-of select="@id"/>'
    , xbrlName: '<xsl:value-of select="@name"/>'
    , itemType:'<xsl:value-of select="@type"/>'
    , concept:'<xsl:choose><xsl:when test="not(@primaryConcept='d-hh_AdimensionalPrimaryConcepts' or @primaryConcept='d-hh_AdimensionalPrimaryConcepts')"><xsl:value-of select="@primaryConcept"/></xsl:when></xsl:choose>'
    , isAbstract:<xsl:choose><xsl:when test="@abstract"><xsl:value-of select="translate(@abstract, $uppercase, $smallcase)"/></xsl:when><xsl:otherwise>false</xsl:otherwise></xsl:choose>
</xsl:template>
  <xsl:template match="Element" mode="TAB_test">
    <xsl:apply-templates select="." mode="GetLabel"/>
  </xsl:template>
  <xsl:template match="Element" mode="TAB">
    <xsl:variable name="title"><xsl:if test="labels/label[@type='documentation']/translation[@lang='en']"><xsl:value-of select="labels/label[@type='documentation']/translation[@lang='en']"/>&#160;-&#160;</xsl:if><xsl:apply-templates select="." mode="GetLabel"/></xsl:variable>
    {xtype:'biztax-tab',title:'<xsl:value-of select="$title"/>'
    <xsl:apply-templates select="." mode="DEFAULT_ATTRIBS"/>
    , items:[
    <xsl:for-each select="Element[not(contains(string(@id),'XCode'))]">
        <xsl:if test="position()>1">,</xsl:if>
        <xsl:apply-templates select="." mode="PANE"/>
      </xsl:for-each>
    ]}
  </xsl:template>

  <xsl:template match="Element" mode="PANE">
    {xtype:'biztax-pane', title:'<xsl:apply-templates select="." mode="GetLabel"/>'
    <xsl:apply-templates select="." mode="DEFAULT_ATTRIBS"/>
    <xsl:if test="count(descendant::*[@periodType='instant'])">,hasInstantPeriods:true</xsl:if>
    ,items:[
      <xsl:for-each select="Element">
        <xsl:if test="position()>1">,</xsl:if>
        <xsl:apply-templates select="." mode="EITHER_WAY"/>
      </xsl:for-each>
    ]
    }
    
  </xsl:template>

  <xsl:template match="Element" mode="GETPERIODTYPE">
    <xsl:choose>
      <xsl:when test="@periodType='duration'">
        <xsl:choose>
          <xsl:when test="labels/label[@type='periodStartLabel']">instant-start</xsl:when>
          <xsl:when test="labels/label[@type='periodEndLabel']">instant-end</xsl:when>
          <xsl:otherwise>duration</xsl:otherwise>
        </xsl:choose>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="@periodType"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="Element" mode="LISTITEM">
    {xtype:'biztax-list', label:'<xsl:apply-templates select="." mode="GetLabel"/>'
    <xsl:apply-templates select="." mode="DEFAULT_ATTRIBS"/>
    , substitutionGroup: '<xsl:value-of select="@substitutionGroup"/>'
    , periodType:'<xsl:apply-templates select="." mode="GETPERIODTYPE"/>'
    , data:[<xsl:apply-templates select="." mode="LISTITEM_DATA"/>]
    }
  </xsl:template>

  <xsl:template match="Element" mode="LISTITEM_DATA">
    <xsl:for-each select="Element">
      <xsl:if test="position()>1">,</xsl:if>['<xsl:value-of select="@id"/>','<xsl:apply-templates select="." mode="GetLabel"/>']
    </xsl:for-each>
  </xsl:template>
  
  <xsl:template match="Element" mode="FIELD">
    {xtype:'biztax-field', label:'<xsl:apply-templates select="." mode="GetLabel"/>'
    <xsl:apply-templates select="." mode="DEFAULT_ATTRIBS"/>
    <xsl:if test="Translations/Role[@id='totalLabel']">,totalField:true</xsl:if>
    , periodType:'<xsl:apply-templates select="." mode="GETPERIODTYPE"/>'
    }
  </xsl:template>

  <xsl:template match="Element" mode="UPLOADFIELD">
    {xtype:'biztax-uploadfield', label:'<xsl:apply-templates select="." mode="GetLabel"/>'
    <xsl:apply-templates select="." mode="DEFAULT_ATTRIBS"/>
    , periodType:'<xsl:apply-templates select="." mode="GETPERIODTYPE"/>'
    , isRequired:<xsl:value-of select="@type='http://www.nbb.be/be/fr/pfs/ci/dt/2012-04-01:nonEmptyBase64BinaryItemType'"/>
    }
  </xsl:template>

  <xsl:template match="Element" mode="GetLabel"><xsl:variable name="apos">'</xsl:variable><xsl:variable name="aposEsc">-</xsl:variable><xsl:variable name="txt"><xsl:call-template name="string-replace-all"><xsl:with-param name="text" select="string(labels/label[@type='label']/translation[@lang=$culture])" /><xsl:with-param name="replace" select="$apos" /><xsl:with-param name="by" select="$aposEsc" /></xsl:call-template></xsl:variable><xsl:value-of select="translate($txt,$apos,$aposEsc)"/></xsl:template>
  
  <xsl:template match="Element" mode="EITHER_WAY">
    <xsl:variable name="id" select="@id"/>
    <xsl:choose>
      <xsl:when test="count(Element[contains(string(@id),'XCode')])>0">
        <xsl:apply-templates select="." mode="LISTITEM"/>
      </xsl:when>
      <xsl:when test="@type='http://www.xbrl.org/2003/instance:base64BinaryItemType' or @type='http://www.nbb.be/be/fr/pfs/ci/dt/2012-04-01:nonEmptyBase64BinaryItemType'">
        <xsl:apply-templates select="." mode="UPLOADFIELD"/>
      </xsl:when>
      <xsl:when test="count(Element)>0 and count(Element[not('CodeHead'=substring(@id, string-length(@id)- string-length('CodeHead') +1) or 'Other'=substring(@id, string-length(@id)- string-length('Other') +1))]) =0">
        <!--<xsl:apply-templates select="." mode="VALUEPANE"/> -->
        // VALUE PANE
        <xsl:apply-templates select="Element[contains(@id,'CodeHead')]" mode="EITHER_WAY"/>
      </xsl:when>
      <xsl:when test="count(Element)>0">
        <xsl:apply-templates select="." mode="PANE"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:apply-templates select="." mode="FIELD"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  

  <xsl:template name="string-replace-all">
    <xsl:param name="text" />
    <xsl:param name="replace" />
    <xsl:param name="by" />
    <xsl:choose>
      <xsl:when test="contains($text, $replace)">
        <xsl:value-of select="substring-before($text,$replace)" />
        <xsl:value-of select="$by" />
        <xsl:call-template name="string-replace-all">
          <xsl:with-param name="text"
          select="substring-after($text,$replace)" />
          <xsl:with-param name="replace" select="$replace" />
          <xsl:with-param name="by" select="$by" />
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$text" />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

</xsl:stylesheet>
