﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace EY.com.eBook.BizTaxParser.Test2
{
    public class Arc : XType
    {

        public static Arc CreateFromElement(XElement element, Dictionary<string, XNamespace> namespaces)
        {
            return Arc.CreateFromElement<Arc>(element, namespaces);
        }

        public static T CreateFromElement<T>(XElement element, Dictionary<string, XNamespace> namespaces) where T : Arc, new()
        {
            T pl = XType.CreateFromElement<T>(element, namespaces);
            pl.ArcRole = element.Attribute(namespaces["xlink"] + "arcrole") != null ? element.Attribute(namespaces["xlink"] + "arcrole").Value : null;
            pl.From = element.Attribute(namespaces["xlink"] + "from") != null ? element.Attribute(namespaces["xlink"] + "from").Value : null;
            pl.To = element.Attribute(namespaces["xlink"] + "to") != null ? element.Attribute(namespaces["xlink"] + "to").Value : null;
            return pl;
        }

        public string ArcRole { get; set; }

        public string From { get; set; }

        public string To { get; set; }

        public Locator FromLoc { get; set; }

        public Locator ToLoc { get; set; }

    }
}
