﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Schema;
using System.Xml;
using System.Collections;
using System.Xml.Linq;
using System.Xml.XPath;

namespace EY.com.eBook.BizTaxParser.Test2
{
    // TO ALTER => SEARCH IN TAXONOMY FOLDER + XBRL.org - Specification folder
    public class LocalFolderResolver : XmlUrlResolver
    {
        public string Folder { get; set; }

        private Dictionary<string, FileStream> resolves = new Dictionary<string, FileStream>();


        public override object  GetEntity(Uri absoluteUri, string role, Type ofObjectToReturn)
        {

            string fname = absoluteUri.Segments[absoluteUri.Segments.Length - 1];
            //if (!resolves.ContainsKey(fname))
            //{
                FileStream fs = new FileStream(Path.Combine(Folder, fname), FileMode.Open, FileAccess.Read);
               // resolves.Add(fname,fs);
                return fs;
            //}
            //else
            //{
            //    return resolves[fname];
            //}
            //return base.GetEntity(absoluteUri, role, ofObjectToReturn);
        }
       
    }


    public class TaxonomyXsdReader
    {
        public static string ValueOrEmpty(string src)
        {
            if (string.IsNullOrEmpty(src)) return string.Empty;
            return src;
        }

        public TaxonomyXsd Xsd;
       // public List<string> facets = new List<string>();
        public string Path { get; set; }

        public Dictionary<string, XbrlDataType> GlobalDataTypes { get; set; }

        public Dictionary<string, XbrlElementDefinition> GlobalElementDefintions { get; set; }

        public Dictionary<string, string> GlobalNamedRefED { get; set; }

        public Dictionary<string, HyperCube> GlobalHyperCubes { get; set; }

        public List<DefinitionFile> DefinitionFiles { get; set; }

        public Dictionary<string, XmlSchema> SchemaList { get; set; }

        public TaxonomyXsdReader(string pathToMainXsd)
        {
            GlobalDataTypes = new Dictionary<string, XbrlDataType>();
            GlobalElementDefintions = new Dictionary<string, XbrlElementDefinition>();
            DefinitionFiles = new List<DefinitionFile>();
            Path = pathToMainXsd;
        }

        public void Read()
        {
            string filename = System.IO.Path.GetFileName(Path);
            GlobalDataTypes = new Dictionary<string, XbrlDataType>();
            GlobalElementDefintions = new Dictionary<string, XbrlElementDefinition>();

            // Get target namespace of main xsd
            #region Retrieve target namespace

            XmlTextReader reader = new XmlTextReader(Path);
            XmlSchema lschema = XmlSchema.Read(reader, schemaSet_ValidationEventHandler);
            string targetns = lschema.TargetNamespace;
            lschema = null;
            reader.Close();
            reader = null;

            #endregion

            // Load entire associated set (imported xsd's - deep)
            #region Load-in schema set
            XmlSchemaSet set = new XmlSchemaSet();
            set.XmlResolver = new LocalFolderResolver { Folder = System.IO.Path.GetDirectoryName(Path) };
            set.ValidationEventHandler += new ValidationEventHandler(schemaSet_ValidationEventHandler);
            set.Add(targetns, Path);

            #endregion

            // CLEANUP DOUBLES (MS bug, circular references)
            #region Remove double schemas
            List<string> reloads = new List<string>();
            List<XmlSchema> removeSchemas = new List<XmlSchema>();
            Dictionary<string, XmlQualifiedName> ns = new Dictionary<string, XmlQualifiedName>();

            foreach (XmlSchema sch in set.Schemas())
            {
                XmlQualifiedName[] qualifiedNames = sch.Namespaces.ToArray();
                foreach (XmlQualifiedName qname in qualifiedNames)
                {
                    if (!qname.IsEmpty && !ns.ContainsKey(qname.Name)) ns.Add(qname.Name, qname);
                }

                string id = sch.SourceUri + "#" + sch.TargetNamespace;
                if (!reloads.Contains(id))
                {
                    Uri ur = new Uri(sch.SourceUri);

                    int cnt = 0;
                    foreach (XmlSchema schSub in set.Schemas(sch.TargetNamespace))
                    {

                        cnt++;
                    }
                    if (cnt > 1)
                    {
                        reloads.Add(id);
                        removeSchemas.Add(sch);

                    }
                }
            }
            foreach (XmlSchema sch in removeSchemas)
            {
                set.Remove(sch);
            }
            foreach (string schid in reloads)
            {
                string[] splitted = schid.Split(new char[] { '#' });
                string tns = splitted[1];
                string fle = splitted[0];
                if (set.Schemas(tns).Count == 0)
                {
                    set.Add(tns, fle);
                }
            }

            #endregion

            set.Compile();

            //string targetKey = ns.Values.First(v => v.Namespace == targetns).Name;
            //ns.Remove(targetKey);
            List<XmlQualifiedName> nsDels = ns.Values.Where(v => v.Namespace.Contains("XMLSchema")).ToList();
            foreach (XmlQualifiedName qname in nsDels)
            {
                ns.Remove(qname.Name);
            }

            //ns.Add("tax-inc", new XmlQualifiedName("tax-inc",targetns));


            XElement xns = new XElement("namespaces");
            foreach (string key in ns.Keys)
            {
                xns.Add(new XElement("namespace", new XAttribute("name", ns[key].Name)
                                                , new XAttribute("namespace", ns[key].Namespace)
                                    )
                );
                Console.WriteLine("{0} : {1}", key, ns[key]);
            }
            Console.WriteLine("done");
            xns.Save("namespaces.xml");
            Console.ReadLine();
            

            #region Travers all defined xsd types (accross set)
            foreach (XmlSchemaType xstype in set.GlobalTypes.Values)
            {
                
                if (xstype.Name != null)
                {
                    Console.WriteLine(xstype.Name);

                    switch (xstype.GetType().ToString())
                    {
                        case "System.Xml.Schema.XmlSchemaComplexType":
                            ReadComplexType(xstype as XmlSchemaComplexType);
                            break;
                        case "System.Xml.Schema.XmlSchemaSimpleType":
                            ReadSimpleType(xstype as XmlSchemaSimpleType);
                            break;
                        default:
                            Console.WriteLine("UNKNOWN   > " + xstype.GetType().ToString());
                            break;
                    }
                    Console.WriteLine("   > " + xstype.GetType().ToString());
                }
                else
                {
                    Console.WriteLine("   NO NAME > " + xstype.GetType().ToString());
                }

            }
            #endregion
            

            #region Travers all elements (accross set)
            foreach (XmlSchemaElement element in set.GlobalElements.Values)
            {
                Uri uri = new Uri(element.SourceUri);
                string origin = uri.Segments[uri.Segments.Length - 1];
                string href = string.Format("{0}#{1}", origin, element.Id);
                bool elAndDataType = false;
                if (element.SchemaType != null)
                {
                    elAndDataType = true;
                    if (!GlobalDataTypes.ContainsKey(element.Name))
                    {
                        switch (element.SchemaType.GetType().ToString())
                        {
                            case "System.Xml.Schema.XmlSchemaComplexType":
                                ReadComplexType(element.SchemaType as XmlSchemaComplexType,element.Name);
                                break;
                            case "System.Xml.Schema.XmlSchemaSimpleType":
                                ReadSimpleType(element.SchemaType as XmlSchemaSimpleType, element.Name);
                                break;
                            default:
                                Console.WriteLine("UNKNOWN   > " + element.SchemaType.GetType().ToString());
                                break;
                        }
                    }
                }


                if (!GlobalElementDefintions.ContainsKey(href) && !(GlobalDataTypes.ContainsKey(element.Name) && !elAndDataType) )
                {
                    XbrlElementDefinition xed = new XbrlElementDefinition
                    {
                        Href = href
                        ,
                        Name = element.Name
                        ,
                        Id = element.Id
                        ,
                        Abstract = element.IsAbstract
                        ,
                        Nillable = element.IsNillable
                        ,
                        MaxOccurs = element.MaxOccursString
                        ,
                        MinOccurs = element.MinOccursString
                        ,
                        SubstitutionGroup = element.SubstitutionGroup.ToString()
                        ,
                        Type = element.SchemaTypeName.ToString()
                        ,
                        Origin = origin
                        ,
                        Fixed = element.FixedValue
                        , Ref = element.RefName.ToString()
                        
                    };
                    if (element.UnhandledAttributes != null)
                    {
                        foreach (XmlAttribute att in element.UnhandledAttributes)
                        {
                            switch (att.Name)
                            {
                                case "xbrli:periodType":
                                    xed.PeriodType = att.Value;
                                    break;
                                case "xbrli:balance":
                                    xed.Balance = att.Value;
                                    break;
                                case "xbrldt:typedDomainRef":
                                    xed.TypedDomainRef = att.Value;
                                    break;
                            }
                        }
                    }
                    
                    //if (element.ElementSchemaType != null)
                    //{
                    //    XmlSchemaComplexType complexType = element.ElementSchemaType as XmlSchemaComplexType;
                    //    XmlSchemaSimpleType simpleType = element.ElementSchemaType as XmlSchemaSimpleType;
                    //    if (complexType = null)
                    //    {
                    //        xed.Type = 
                    //    }
                    //}
                    GlobalElementDefintions.Add(href, xed);

                }
            }
            #endregion
            Console.WriteLine("DescriptionTypedID found? " + GlobalDataTypes.ContainsKey("DescriptionTypedID"));
            if (GlobalDataTypes.ContainsKey("DescriptionTypedID"))
            {
                XbrlDataType xdt = GlobalDataTypes["DescriptionTypedID"];
                Console.WriteLine(xdt.Name);
                Console.WriteLine(xdt.BaseType);
                Console.WriteLine(xdt.Origin);
                Console.WriteLine(xdt.Role);
                Console.WriteLine(xdt.ValueType);
                Console.WriteLine("restrictions:");
                foreach (Restriction r in xdt.Restrictions)
                {
                    Console.WriteLine(r.Name + " : " + r.Value);
                }
                
            }
            Console.ReadLine();
           
            #region create specific taxonomy xsd
            // distincted list of all types and elements specific to THIS xsd
            Xsd = new TaxonomyXsd
            {
                Name = filename
                ,
                DataTypes = GlobalDataTypes
                ,
                ElementDefintions = GlobalElementDefintions
                ,
                AnnotationLinks = new List<AnnotionLink>()
                ,
                AnnotionXbrlLinks = new List<AnnotionXbrlLink>()
                ,
                Imports = new List<ImportedXsd>()
                , ConceptDefinitions = new List<ConceptDefinition>()
            };

            //foreach (XbrlDataType xdt in GlobalDataTypes.Values.Distinct()) //.Where(v => v.Origin == filename))
            //{
            //    Xsd.DataTypes.Add(xdt.Name, xdt);
            //}
            //foreach (XbrlElementDefinition xed in GlobalElementDefintions.Values.Distinct()) //.Where(v => v.Origin == filename))
            //{
            //    /// name instead of href (since href is combination of xsd filename and element name)
            //    Xsd.ElementDefintions.Add(xed.Name, xed);
            //}
            
            // imported xsd's (direct/indirect) trusts that all imported schemes are in set
            foreach (XmlSchema scheme in set.Schemas())
            {
                Uri sfuri = new Uri(scheme.SourceUri);
                string sfname = sfuri.Segments[sfuri.Segments.Length - 1];
                if (sfname != filename)
                {
                    Xsd.Imports.Add(new ImportedXsd
                    {
                        FilePath = sfuri.LocalPath
                        ,
                        FileName = sfname
                        ,
                        TargetNamespace = scheme.TargetNamespace
                    });
                }
            }


            #endregion

            SchemaList = new Dictionary<string, XmlSchema>();

            foreach (XmlSchema setItem in set.Schemas())
            {
                Uri suri = new Uri(setItem.SourceUri);
                string fname = suri.Segments[suri.Segments.Length - 1];
                SchemaList.Add(fname, setItem);
                bool isCurrent = suri.LocalPath == Path;
                foreach (XmlSchemaObject xso in setItem.Items)
                {
                    XmlSchemaAnnotation xsa = xso as XmlSchemaAnnotation;

                    if (xsa != null)
                    {
                        XmlSchemaAppInfo appinfo = xsa.Items[0] as XmlSchemaAppInfo;
                        if (appinfo != null)
                        {
                            XmlAttribute attrib = null;
                            foreach (XmlNode appNode in appinfo.Markup)
                            {
                                switch (appNode.Name)
                                {
                                    case "link:roleType":
                                        AnnotionLink al = new AnnotionLink { Origin = fname };
                                        attrib = (XmlAttribute)appNode.Attributes.GetNamedItem("roleURI");
                                        al.RoleUri = attrib != null ? attrib.Value : null;
                                        attrib = (XmlAttribute)appNode.Attributes.GetNamedItem("id");
                                        al.Id = attrib != null ? string.Format("{0}#{1}",fname, attrib.Value) : null;
                                        foreach (XmlNode childNode in appNode.ChildNodes)
                                        {
                                            switch (childNode.Name)
                                            {
                                                case "link:definition":
                                                    al.Definition = childNode.InnerText;
                                                    break;
                                                case "link:usedOn":
                                                    al.UsedOn = childNode.InnerText;
                                                    break;
                                            }
                                        }
                                        al.NameHref = string.Format("{0}#{1}", fname, al.Definition);
                                        Xsd.AnnotationLinks.Add(al);
                                        break;
                                    case "link:linkbaseRef":
                                        AnnotionXbrlLink axl = new AnnotionXbrlLink { Origin = fname };
                                        string axl_role = string.Empty;
                                        string axl_arcrole = string.Empty;
                                        string axl_href = string.Empty;
                                        attrib = (XmlAttribute)appNode.Attributes.GetNamedItem("xlink:role");
                                        if (attrib != null) axl_role = attrib.Value;
                                        attrib = (XmlAttribute)appNode.Attributes.GetNamedItem("xlink:arcrole");
                                        if (attrib != null) axl_arcrole = attrib.Value;
                                        attrib = (XmlAttribute)appNode.Attributes.GetNamedItem("xlink:href");
                                        if (attrib != null) axl_href = attrib.Value;
                                        axl.SetType(axl_arcrole, axl_role, axl_href);
                                        attrib = (XmlAttribute)appNode.Attributes.GetNamedItem("xlink:type");
                                        if (attrib != null) axl.XLinkType = attrib.Value;
                                        attrib = (XmlAttribute)appNode.Attributes.GetNamedItem("xlink:title");
                                        if (attrib != null) axl.Title = attrib.Value;
                                        Xsd.AnnotionXbrlLinks.Add(axl);
                                        break;
                                }
                                Console.WriteLine(appNode.Name);
                            }
                        }

                    }
                }
            }

            #region old
            /*
            #region proces current schema for annotations (appinfo)
            XmlSchema currentSchema = null;
            // get current schema
            foreach (XmlSchema setItem in set.Schemas(targetns))
            {
                Uri suri = new Uri(setItem.SourceUri);
                if (suri.LocalPath == Path)
                {
                    currentSchema = setItem;
                    break;
                }
            }
            if (currentSchema == null)
            {
                throw new Exception("Base schema not found ?????");
            }
            else
            {
                foreach (XmlSchemaObject xso in currentSchema.Items)
                {
                    XmlSchemaAnnotation xsa = xso as XmlSchemaAnnotation;

                    if (xsa != null)
                    {
                        XmlSchemaAppInfo appinfo = xsa.Items[0] as XmlSchemaAppInfo;
                        if (appinfo != null)
                        {
                            XmlAttribute attrib = null;
                            foreach (XmlNode appNode in appinfo.Markup)
                            {
                                switch (appNode.Name)
                                {
                                    case "link:roleType":
                                        AnnotionLink al = new AnnotionLink();
                                        attrib = (XmlAttribute)appNode.Attributes.GetNamedItem("roleURI");
                                        al.RoleUri = attrib != null ? attrib.Value : null;
                                        attrib = (XmlAttribute)appNode.Attributes.GetNamedItem("id");
                                        al.Id = attrib != null ? attrib.Value : null;
                                        foreach (XmlNode childNode in appNode.ChildNodes)
                                        {
                                            switch (childNode.Name)
                                            {
                                                case "link:definition":
                                                    al.Definition = childNode.InnerText;
                                                    break;
                                                case "link:usedOn":
                                                    al.UsedOn = childNode.InnerText;
                                                    break;
                                            }
                                        }
                                        Xsd.AnnotationLinks.Add(al);
                                        break;
                                    case "link:linkbaseRef":
                                        AnnotionXbrlLink axl = new AnnotionXbrlLink();
                                        string axl_role = string.Empty;
                                        string axl_arcrole = string.Empty;
                                        string axl_href = string.Empty;
                                        attrib = (XmlAttribute)appNode.Attributes.GetNamedItem("xlink:role");
                                        if (attrib != null) axl_role = attrib.Value;
                                        attrib = (XmlAttribute)appNode.Attributes.GetNamedItem("xlink:arcrole");
                                        if (attrib != null) axl_arcrole = attrib.Value;
                                        attrib = (XmlAttribute)appNode.Attributes.GetNamedItem("xlink:href");
                                        if (attrib != null) axl_href = attrib.Value;
                                        axl.SetType(axl_arcrole,axl_role,axl_href);
                                        attrib = (XmlAttribute)appNode.Attributes.GetNamedItem("xlink:type");
                                        if (attrib != null) axl.XLinkType = attrib.Value;
                                        attrib = (XmlAttribute)appNode.Attributes.GetNamedItem("xlink:title");
                                        if (attrib != null) axl.Title = attrib.Value;
                                        Xsd.AnnotionXbrlLinks.Add(axl);
                                        break;
                                }
                                Console.WriteLine(appNode.Name);
                            }
                        }
                        
                    }
                }
            }
            #endregion
            */
            // fetch definitions (xbrl definition files - arcs

            /*
            List<DefinitionLink> defLinks = new List<DefinitionLink>();

            foreach (AnnotionXbrlLink axl in Xsd.AnnotionXbrlLinks.Where(a => a.Role == "http://www.xbrl.org/2003/role/definitionLinkbaseRef"))
            {
                // load definition link
                // 
                string defPath = System.IO.Path.Combine(System.IO.Path.GetDirectoryName( Path), axl.Href);
                DefinitionFile deffile = new DefinitionFile(defPath);
                foreach (DefinitionLink defLink in deffile.Links)
                {
                    defLinks.Add(defLink);
                }
            }
            */
            /*
            GlobalHyperCubes = new Dictionary<string, HyperCube>();
            
            foreach (XbrlElementDefinition hyperEL in GlobalElementDefintions.Values.Where(el => el.SubstitutionGroup == "http://xbrl.org/2005/xbrldt:hypercubeItem"))
            {
                HyperCube hc = new HyperCube
                {
                    ElementDefinition = hyperEL
                    ,
                    Id = hyperEL.Href
                    , ConceptDimensions = new List<ConceptDimension>()
                };
                GlobalHyperCubes.Add(hyperEL.Href, hc);
            }

            
            foreach (HyperCube hc in GlobalHyperCubes.Values)
            {
                string nameHref = string.Format("{0}#{1}", hc.ElementDefinition.Origin, hc.ElementDefinition.Name);

                foreach (string ruri in Xsd.AnnotationLinks.Where(al => al.NameHref == nameHref).Select(al => al.RoleUri).Distinct())
                {
                    foreach (DefinitionLink deflink in defLinks.Where(dl => dl.Role == ruri))
                    {
                        ConceptDimension cd = new ConceptDimension { 
                            Concepts = new List<Concept>(), 
                            Dimensions = new List<Dimension>(), 
                            HyperCubeId = hc.Id,
                            RoleUri = deflink.Role
                        };
                        foreach (DefinitionArc arc in deflink.DefinitionArcs)
                        {
                            string fromHref = deflink.Locators.First(loc => loc.Label == arc.From).Href;
                            string toHref = deflink.Locators.First(loc => loc.Label == arc.To).Href;

                            // all / notAll -> Concept to hypercube relation


                            switch (arc.ArcRole)
                            {
                                case "http://xbrl.org/int/dim/arcrole/hypercube-dimension": // DIMENSION
                                    // to = dimension
                                    XbrlElementDefinition xed = GlobalElementDefintions[toHref];
                                    Dimension dim = new Dimension { Id = toHref, ElementDefinition = xed, Domains = new List<DimensionDomain>(), Type = DimensionType.ExplicitDimension };
                                    if (cd.Dimensions == null) cd.Dimensions = new List<Dimension>();
                                    if (!string.IsNullOrEmpty(xed.TypedDomainRef))
                                    {
                                        dim.Type = DimensionType.TypedDimension;
                                        dim.TypeElement = GlobalElementDefintions[xed.TypedDomainRef];
                                        cd.Dimensions.Add(dim);
                                    }
                                    else
                                    {
                                        cd.Dimensions.Add(dim);
                                    }
                                    break;
                                case "http://xbrl.org/int/dim/arcrole/dimension-domain":
                                    Dimension fdim = cd.Dimensions.First(d => d.Id == fromHref);
                                    fdim.Domains.Add(new DimensionDomain
                                    {
                                        DomainMembers = new List<DomainMember>()
                                         ,
                                        ElementDefinition = GlobalElementDefintions[toHref]
                                         ,
                                        Id = toHref
                                         ,
                                        Order = arc.Order
                                    });
                                    break;
                                case "http://xbrl.org/int/dim/arcrole/domain-member":
                                    DimensionDomain dd = cd.Dimensions.Where(d => d.Domains.Count(dm => dm.Id == fromHref) > 0).First().Domains.First(dm => dm.Id == fromHref);
                                    dd.DomainMembers.Add(new DomainMember
                                    {
                                        ElementDefinition = GlobalElementDefintions[toHref]
                                        ,
                                        Id = toHref
                                    });
                                    break;
                                case "http://xbrl.org/int/dim/arcrole/notAll":
                                    break;
                                case "http://xbrl.org/int/dim/arcrole/all":
                                    Concept ct = new Concept
                                    {
                                        ElementDefinition = GlobalElementDefintions[fromHref]
                                        ,
                                        Id = fromHref
                                        ,
                                        Role = HyperCubeConceptRole.All
                                        ,
                                        ContextElement = arc.ContextElement
                                        ,
                                        SubElements = new List<XbrlElementDefinition>()
                                    };
                                    cd.Concepts.Add(ct);
                                    break;
                                case "":
                                    break;
                            }
                        }
                        hc.ConceptDimensions.Add(cd);
                    }
                }
            }

            */
            
            /*
            // load in hypercubes
            foreach (XbrlElementDefinition hyperEL in GlobalElementDefintions.Values.Where(el => el.SubstitutionGroup == "http://xbrl.org/2005/xbrldt:hypercubeItem"))
            {
                HyperCube hc = new HyperCube
                {
                    ElementDefinition = hyperEL
                    ,
                    Concepts = new List<Concept>()
                    ,
                    Id = hyperEL.Href
                };

                string nameHref = string.Format("{0}#{1}", hyperEL.Origin, hyperEL.Name);
                // find roles



                foreach (string ruri in Xsd.AnnotationLinks.Where(al => al.Id == hyperEL.Href || al.Id == nameHref).Select(al => al.RoleUri).Distinct())
                {
                    foreach (DefinitionLink deflink in defLinks.Where(dl => dl.Role == ruri))
                    {
                        foreach (DefinitionArc arc in deflink.DefinitionArcs)
                        {
                            string fromHref = deflink.Locators.First(loc => loc.Label == arc.From).Href;
                            string toHref = deflink.Locators.First(loc => loc.Label == arc.To).Href;

                            // all / notAll -> Concept to hypercube relation
                            

                            switch (arc.ArcRole)
                            {
                                case "http://xbrl.org/int/dim/arcrole/hypercube-dimension": // DIMENSION
                                    // to = dimension
                                    XbrlElementDefinition xed = GlobalElementDefintions[toHref];
                                    Dimension dim = new Dimension { Id = toHref, ElementDefinition = xed, Domains = new List<DimensionDomain>(), Type = DimensionType.ExplicitDimension };
                                    if (hc.Dimensions == null) hc.Dimensions = new List<Dimension>();
                                    if (!string.IsNullOrEmpty(xed.TypedDomainRef))
                                    {
                                        dim.Type = DimensionType.TypedDimension;
                                        dim.TypeElement = GlobalElementDefintions[xed.TypedDomainRef];
                                        hc.Dimensions.Add(dim);
                                    }
                                    else
                                    {
                                        hc.Dimensions.Add(dim);
                                    }
                                    break;
                                case "http://xbrl.org/int/dim/arcrole/dimension-domain":
                                    Dimension fdim = hc.Dimensions.First(d => d.Id == fromHref);
                                    fdim.Domains.Add(new DimensionDomain
                                    {
                                        DomainMembers = new List<DomainMember>()
                                         , ElementDefinition = GlobalElementDefintions[toHref]
                                         , Id = toHref
                                         , Order = arc.Order
                                    });
                                    break;
                                case "http://xbrl.org/int/dim/arcrole/domain-member":
                                    DimensionDomain dd = hc.Dimensions.Where(d => d.Domains.Count(dm => dm.Id == fromHref) > 0).First().Domains.First(dm => dm.Id == fromHref);
                                    dd.DomainMembers.Add(new DomainMember
                                    {
                                        ElementDefinition = GlobalElementDefintions[toHref]
                                        ,
                                        Id = toHref
                                    });
                                    break;
                                case "http://xbrl.org/int/dim/arcrole/notAll":
                                    break;
                                case "http://xbrl.org/int/dim/arcrole/all":
                                    Concept ct = new Concept
                                    {
                                        ElementDefinition = GlobalElementDefintions[fromHref]
                                        ,
                                        Id = fromHref
                                        ,
                                        Role = HyperCubeConceptRole.All
                                        ,
                                        ContextElement = arc.ContextElement
                                        ,
                                        SubElements = new List<XbrlElementDefinition>()
                                    };
                                    hc.Concepts.Add(ct);
                                    break;
                                case "":
                                    break;
                            }
                        }
                    }
                }
                GlobalHyperCubes = new Dictionary<string, HyperCube>();
                GlobalHyperCubes.Add(hyperEL.Href, hc);
                
            }

             */
           


            //foreach (DefinitionFile defFile in DefinitionFiles)
            //{
            //    foreach (DefinitionLink defLink in defFile.Links)
            //    {
            //        foreach (DefinitionArc arc in defLink.DefinitionArcs)
            //        {
            //            string fromHref = defLink.Locators.First(loc => loc.Label == arc.From).Href;
            //            string toHref = defLink.Locators.First(loc => loc.Label == arc.To).Href;

            //            /*
            //             * all / notAll -> Concept to hypercube relation
            //             * 
            //             * 
            //             */

            //            switch (arc.ArcRole)
            //            {
            //                case "http://xbrl.org/int/dim/arcrole/notAll":
            //                    break;
            //                case "http://xbrl.org/int/dim/arcrole/all":
            //                    break;
            //                case "":
            //                    break;
            //            }
            //        }
            //    }
            //}
#endregion
            GlobalNamedRefED = new Dictionary<string, string>();
            Xsd.HyperCubes = new Dictionary<string, HyperCube>();
            // Create Named ref
            foreach (string key in GlobalElementDefintions.Keys)
            {
                string nameHref = string.Format("{0}#{1}",GlobalElementDefintions[key].Origin,GlobalElementDefintions[key].Name);
                GlobalNamedRefED.Add(nameHref, key);
            }

            // DEFINITIONS

            ReadDefinition(ref Xsd, Path);

            foreach (ImportedXsd ix in Xsd.Imports)
            {
                ReadDefinition(ref Xsd, ix.FilePath);
            }

            // LABELS
            foreach (AnnotionXbrlLink axl in Xsd.AnnotionXbrlLinks.Where(a => a.LinkType == XbrlLinkTypes.Label && a.Origin == filename))
            {
                string presfile = System.IO.Path.Combine(System.IO.Path.GetDirectoryName(Path), axl.Href);
                //if (!presfile.Contains("pfs-vl"))
                //{
                    ReadLabel(presfile);
                //}
                //presfilesDone.Add(presfile);
            }

            foreach (ImportedXsd ix in Xsd.Imports)
            {

                foreach (AnnotionXbrlLink axl in Xsd.AnnotionXbrlLinks.Where(a => a.LinkType == XbrlLinkTypes.Label && a.Origin == ix.FileName))
                {
                    string presfile = System.IO.Path.Combine(System.IO.Path.GetDirectoryName(Path), axl.Href);
                    //if (!presfile.Contains("pfs-vl"))
                    //{
                        ReadLabel(presfile);
                    //}
                }
            }

            //Xsd.HyperCubes = GlobalHyperCubes;
            
            
            // PRESENTATION
            Console.WriteLine("Read presentations");
            List<string> presfilesDone = new List<string>();
            XElement presroot = new XElement("root");
            XElement tabsRoot = new XElement("Tabs");
            foreach (AnnotionXbrlLink axl in Xsd.AnnotionXbrlLinks.Where(a => a.LinkType == XbrlLinkTypes.Presentation && a.Origin == filename))
            {
                
                string presfile = System.IO.Path.Combine(System.IO.Path.GetDirectoryName(Path),axl.Href);
                Console.WriteLine(presfile);
                ReadPresentation(presfile, ref tabsRoot);
                presfilesDone.Add(presfile);
            }

            foreach (ImportedXsd ix in Xsd.Imports)
            {

                foreach (AnnotionXbrlLink axl in Xsd.AnnotionXbrlLinks.Where(a => a.LinkType == XbrlLinkTypes.Presentation && a.Origin == ix.FileName))
                {
                    string presfile = System.IO.Path.Combine(System.IO.Path.GetDirectoryName(Path), axl.Href);
                    Console.WriteLine(presfile);
                    if (!presfilesDone.Contains(presfile))
                    {
                        ReadPresentation(presfile, ref tabsRoot);
                        presfilesDone.Add(presfile);
                    }
                }
            }
            presroot.Add(tabsRoot);
            //XElement elements = new XElement("Elements");
            /*
            // Add concepts with definitions
            foreach (ConceptDefinition cd in Xsd.ConceptDefinitions)
            {
                // && !cd.HyperCubeHref.Contains("EmptyHypercube")
                if (!string.IsNullOrEmpty(cd.HyperCubeHref))
                {
                    XElement concept = new XElement("Concept",
                            new XAttribute("ContextElement", cd.ContextElement)
                            , new XAttribute("ConceptElementHref", cd.ElementHref)
                            , new XAttribute("HyperCubeConceptRole", cd.HyperCubeConceptRole)
                            , new XAttribute("HyperCubeHref", cd.HyperCubeHref)
                            , new XAttribute("ContextFile", cd.ContextFile)
                            , new XAttribute("HyperCubeTargetRole", ValueOrEmpty(cd.HyperCubeTargetRole))
                            );
                    XElement conceptChildren = new XElement("children");
                    foreach (string child in cd.Children)
                    {
                        conceptChildren.Add(new XElement("child", new XAttribute("id", child)));
                    }
                    concept.Add(conceptChildren);
                    XElement dimensions = new XElement("dimensions");
                    if (!string.IsNullOrEmpty(cd.HyperCubeTargetRole))
                    {
                        
                        if (Xsd.HyperCubes.ContainsKey(cd.HyperCubeTargetRole))
                        {
                            HyperCube hc = Xsd.HyperCubes[cd.HyperCubeTargetRole];
                            foreach (Dimension d in hc.Dimensions.Values)
                            {
                                XElement xDimen = new XElement("dimension", new XAttribute("type", d.Type.ToString()));
                                if (d.Type == DimensionType.ExplicitDimension)
                                {
                                    foreach (DimensionDomain dd in d.Domains)
                                    {
                                        XElement xdd = new XElement("dimensionDomain");
                                        AddElementAttribs(ref xdd, dd.ElementDefinition.Href);
                                        XElement xdms = new XElement("domainMembers");
                                        foreach (DomainMember dm in dd.DomainMembers)
                                        {
                                            XElement xdm = new XElement("domainMember");
                                            AddElementAttribs(ref xdm, dm.Id);
                                            xdm.Add(GetXmlLabels(dm.Id));
                                            xdms.Add(xdm);
                                        }
                                        xdd.Add(xdms);
                                        xDimen.Add(xdd);
                                    }

                                }
                                else
                                {
                                    XElement typedElement = new XElement("definition");
                                    if (GlobalDataTypes.ContainsKey(d.Id))
                                    {
                                        typedElement.Add(new XAttribute("hasDataType", true));
                                    }
                                    if (GlobalElementDefintions.ContainsKey(d.Id))
                                    {
                                        AddElementAttribs(ref typedElement, d.Id);
                                    }
                                    else
                                    {
                                        typedElement.Add(new XAttribute("id", d.Id));
                                    }
                                    xDimen.Add(typedElement);
                                }
                                dimensions.Add(xDimen);

                            }
                        }

                    }
                    concept.Add(dimensions);
                    concepts.Add(concept);

                }
            }
            presroot.Add(concepts);

            presroot.Save(string.Format("{0}-presentation.xml", filename));


            //assertions
            
            List<AssertionSet> sets = new List<AssertionSet>();

            Console.WriteLine("Traversing assertions (to move in taxonomyreader)");
            foreach (AnnotionXbrlLink axl in Xsd.AnnotionXbrlLinks.Where(a => a.LinkType == XbrlLinkTypes.Assertion && a.Origin == filename))
            {
                sets.Add(AssertionSet.ReadXmlFile(System.IO.Path.Combine(System.IO.Path.GetDirectoryName(Path), axl.Href)));
            }
            foreach (ImportedXsd ix in Xsd.Imports)
            {
                foreach (AnnotionXbrlLink axl in Xsd.AnnotionXbrlLinks.Where(a => a.LinkType == XbrlLinkTypes.Assertion && a.Origin == ix.FileName))
                {
                    sets.Add(AssertionSet.ReadXmlFile(System.IO.Path.Combine(System.IO.Path.GetDirectoryName(Path), axl.Href)));
                }
            }

           */
            
            //Program.SaveToXml(sets, true, string.Format("{0}-assertions.xml",System.IO.Path.GetFileNameWithoutExtension(filename)));

            

            /*
            //be-tax-inc-2013-04-30.xsd#tax-inc_AllowanceCorporateEquity
            //be-tax-inc-2013-04-30.xsd#tax-inc_AllowanceInvestmentDeduction
            Console.WriteLine("EMPTY HYPERCUBES (TARGETROLE)");
            foreach (string h in emptyhypercubes.Distinct().ToList())
            {
                HyperCube hc = hypercubes.FirstOrDefault(hx => hx.Href == h);
                if (hc == null)
                {
                    Console.WriteLine("- " + h);
                }
            }

            Console.ReadLine();
             * */
            Console.Clear();
            Console.WriteLine("SET CONTEXTS");
            foreach (string key in GlobalElementDefintions.Keys.ToList())
            {
                Console.WriteLine(key);
                SetElementContexts(key);
            }
            /*
            SetElementContexts("be-tax-inc-2013-04-30.xsd#tax-inc_OtherReserves");
            SetElementContexts("be-tax-inc-2013-04-30.xsd#tax-inc_OriginDistributionTitle");
            SetElementContexts("be-tax-inc-2013-04-30.xsd#tax-inc_RemainingFiscalResult");
            SetElementContexts("be-tax-inc-2013-04-30.xsd#tax-inc_AllowanceCorporateEquity");
            Console.ReadLine();
            SetElementContexts("be-tax-inc-2013-04-30.xsd#tax-inc_AllowanceInvestmentDeduction");
            Console.ReadLine();
            SetElementContexts("be-tax-inc-2013-04-30.xsd#tax-inc_IdentityTradeDebtor");
            //XElement el = root.XPathSelectElement("//XbrlElementDefinition[Href='be-tax-inc-2013-04-30.xsd#tax-inc_AllowanceInvestmentDeduction']");
            */

            foreach (ContextDefinition cd in GlobalElementDefintions["be-tax-inc-2013-04-30.xsd#tax-inc_IdentityTradeDebtor"].ContextDefinitions)
            {
                Console.WriteLine("   Context:");
                foreach (string period in cd.Periods)
                {
                    foreach (FieldDefinition fd in cd.Fields)
                    {
                        if (fd.TypeElement != null)
                        {
                            Console.WriteLine(" -   {0}__{1}", period, fd.TypeElement.Name);
                        }
                        else
                        {
                            Console.WriteLine(" -   {0}__{1}", period, fd.Dimension);
                        }
                    }
                    if (cd.Fields.Count == 0)
                    {
                        Console.WriteLine(" -   {0}", period);
                    }
                }
            }
           // Console.ReadLine();


            XElement xass = new XElement("Assertions");
            foreach (AnnotionXbrlLink axl in Xsd.AnnotionXbrlLinks.Where(a => a.LinkType == XbrlLinkTypes.Parameter && a.Origin == filename))
            {
                xass.Add(new XElement("parameter"
                    , new XAttribute("href", System.IO.Path.Combine(System.IO.Path.GetDirectoryName(Path), axl.Href))
                    , new XAttribute("origin", axl.Origin)
                    ));
            }
            foreach (ImportedXsd ix in Xsd.Imports)
            {
                foreach (AnnotionXbrlLink axl in Xsd.AnnotionXbrlLinks.Where(a => a.LinkType == XbrlLinkTypes.Parameter && a.Origin == ix.FileName))
                {
                    xass.Add(new XElement("parameter"
                    , new XAttribute("href", System.IO.Path.Combine(System.IO.Path.GetDirectoryName(Path), axl.Href))
                    , new XAttribute("origin", axl.Origin)
                    ));
                }
            }

            foreach (AnnotionXbrlLink axl in Xsd.AnnotionXbrlLinks.Where(a => a.LinkType == XbrlLinkTypes.Assertion && a.Origin == filename))
            {
                xass.Add(new XElement("assertion"
                    , new XAttribute("href", System.IO.Path.Combine(System.IO.Path.GetDirectoryName(Path), axl.Href))
                    , new XAttribute("origin", axl.Origin)
                    ));
            }
            foreach (ImportedXsd ix in Xsd.Imports)
            {
                foreach (AnnotionXbrlLink axl in Xsd.AnnotionXbrlLinks.Where(a => a.LinkType == XbrlLinkTypes.Assertion && a.Origin == ix.FileName))
                {
                    xass.Add(new XElement("assertion"
                    , new XAttribute("href", System.IO.Path.Combine(System.IO.Path.GetDirectoryName(Path), axl.Href))
                    , new XAttribute("origin", axl.Origin)
                    ));
                }
            }

            xass.Save(string.Format("{0}-assertions.xml", filename));


            Console.WriteLine("Writing results");

            Program.SerializeObject<List<XbrlElementDefinition>>(string.Format("{0}-Element_Definitions.bin", System.IO.Path.GetFileNameWithoutExtension(filename)), GlobalElementDefintions.Values.ToList());
            Program.SerializeObject<List<XbrlDataType>>(string.Format("{0}-Data_Types.bin", System.IO.Path.GetFileNameWithoutExtension(filename)), GlobalDataTypes.Values.ToList());

            //elements.Add(Program.ToXml(GlobalElementDefintions.Values.ToList(), true, string.Format("{0}-defs.xml", System.IO.Path.GetFileNameWithoutExtension(filename))));
            //presroot.Add(elements);

            presroot.Save(string.Format("{0}-presentation.xml", filename));
            //root.Add(Program.ToXml(hypercubes, true, string.Format("{0}-hypercubes.xml", System.IO.Path.GetFileNameWithoutExtension(filename))));
           // root.Save(string.Format("{0}-result.xml", System.IO.Path.GetFileNameWithoutExtension(filename)));
        }

        private void SetElementContexts(string href)
        {
            Console.WriteLine("");
            Console.WriteLine(href);
            Console.WriteLine("---------------------------------------");
            XbrlElementDefinition gxed = GlobalElementDefintions[href];
            List<string> roles = gxed.Contexts.Select(c => c.RoleId).Distinct().ToList();
            //List<string> roles = el.XPathSelectElements("Contexts/Context/RoleId").Select(r => r.Value).Distinct().ToList();

            if (gxed.Abstract)
            {
                Console.WriteLine("ABSTRACT");
            }

            foreach (string role in roles)
            {
                GetContexts(role, href, gxed);
                /*
                Console.WriteLine(role);
                

                List<HyperCubeLink> hcls = gxed.Contexts.Where(c => c.RoleId == role).SelectMany(c => c.HyperCubes).ToList();
                List<ContextDimension> items = new List<ContextDimension>(); 
                // PER DIMENSION!! THIS IS ONLY VALID WITH ONLY EXPLICIT ONES
                foreach (HyperCubeLink hcl in hcls.Where(h => h.ArcRole == HyperCubeArcRoles.All))
                {
                    if (!string.IsNullOrEmpty(hcl.TargetRole))
                    {
                        HyperCube hc = hypercubes.First(h => h.RoleUri == hcl.TargetRole && h.Href == hcl.HyperCubeHref);
                        foreach (Dimension dim in hc.Dimensions)
                        {
                            if (dim.Type == DimensionType.ExplicitDimension)
                            {
                                foreach (DimensionDomain dd in dim.Domains)
                                {
                                    foreach (DomainMember dm in dd.DomainMembers)
                                    {

                                        items.Add(new ContextDimension
                                        {
                                            Type = DimensionType.ExplicitDimension
                                            ,
                                            Dimension = dd.Id
                                            ,
                                            Value = dm.Id
                                            ,
                                            TypeMember = null
                                        });
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        // standard role?
                        HyperCube hc = hypercubes.FirstOrDefault(h => h.Href == hcl.HyperCubeHref);
                        if (hc == null)
                        {
                            // is not defined seperately, so has no dimensions -> context solely based on periodType
                            Console.WriteLine("Empty hypercube");
                            Console.WriteLine("HYPERCUBE NOT FOUND!!");
                        }
                        else
                        {
                            Console.WriteLine(hc.Dimensions.Count + " dimensions found");
                            
                        }
                    }
                }
                items= items.Distinct().ToList();
                foreach (HyperCubeLink hcl in hcls.Where(h => h.ArcRole == HyperCubeArcRoles.NotAll))
                {
                    if (!string.IsNullOrEmpty(hcl.TargetRole))
                    {
                        HyperCube hc = hypercubes.First(h => h.RoleUri == hcl.TargetRole && h.Href == hcl.HyperCubeHref);
                        foreach (Dimension dim in hc.Dimensions)
                        {
                            if (dim.Type == DimensionType.ExplicitDimension)
                            {
                                foreach (DimensionDomain dd in dim.Domains)
                                {
                                    foreach (DomainMember dm in dd.DomainMembers)
                                    {
                                        ContextDimension cdim = items.FirstOrDefault(cd => cd.Dimension == dd.Id && cd.Value == dm.Id);
                                        if (cdim != null) items.Remove(cdim);

                                    }
                                }
                            }
                        }
                    }
                }
                foreach (ContextDimension it in items)
                {
                    Console.WriteLine("- " + it.Dimension + " > " +it.Value);
                }
                */
            }
            /*
            foreach (ContextDefinition cd in gxed.ContextDefinitions)
            {
                Console.WriteLine("   Context:");
                foreach (string period in cd.Periods)
                {
                    foreach (FieldDefinition fd in cd.Fields)
                    {
                        if (fd.TypeElement != null)
                        {
                            Console.WriteLine(" -   {0}__{1}", period, fd.TypeElement.Name);
                        }
                        else
                        {
                            Console.WriteLine(" -   {0}__{1}", period, fd.Dimension);
                        }
                    }
                    if (cd.Fields.Count == 0)
                    {
                        Console.WriteLine(" -   {0}", period);
                    }
                }
            }
             * */
        }

        private void GetContexts(string role,string elementHref, XbrlElementDefinition gxed)
        {
            Console.WriteLine(" ROLE::" + role);
            List<HyperCubeLink> hcls = gxed.Contexts.Where(c => c.RoleId == role).SelectMany(c => c.HyperCubes).ToList();
            List<ContextDimension> items = new List<ContextDimension>();
            List<HyperCubeLink> positiveCubes = hcls.Where(h => h.ArcRole == HyperCubeArcRoles.All).ToList();
            List<HyperCubeLink> negativeCubes = hcls.Where(h => h.ArcRole == HyperCubeArcRoles.NotAll).ToList();

            if (gxed.ContextDefinitions == null) gxed.ContextDefinitions = new List<ContextDefinition>();

            
            List<string> periods = new List<string>();
            if (gxed.PeriodType == "instant")
            {
                if (gxed.Labels.Count(l => l.Role == "periodStartLabel") > 0)
                {
                    periods.Add("I-Start");
                    // I-Start
                }
                if (gxed.Labels.Count(l => l.Role == "periodEndLabel") > 0)
                {
                    periods.Add("I-End");
                    // I-End
                }
            }
            else if (gxed.PeriodType == "duration")
            {
                periods.Add("D");
                // D (Duration)
            }
            else
            {
                periods.Add("UNKNOWN_PERIOD");
            }
            ContextDefinition cDef = new ContextDefinition
            {
                Role = role
                ,
                Fields = new List<FieldDefinition>()
                ,
                Periods = periods
            };

   
            // PER DIMENSION!! THIS IS ONLY VALID WITH ONLY EXPLICIT ONES
            foreach (HyperCubeLink hcl in hcls.Where(h => h.ArcRole == HyperCubeArcRoles.All))
            {
                if (!string.IsNullOrEmpty(hcl.TargetRole))
                {
                    HyperCube hc = hypercubes.FirstOrDefault(h => h.RoleUri == hcl.TargetRole && h.Href == hcl.HyperCubeHref);
                    if (hc != null)
                    {
                        foreach (Dimension dim in hc.Dimensions)
                        {
                            FieldDefinition fd = new FieldDefinition
                            {
                                DimensionType = dim.Type
                                ,
                                Dimension = dim.Id
                                ,
                                Fixed = false
                                ,
                                ListData = null
                                ,
                                TypeElement = null
                            };
                            if (dim.Type == DimensionType.ExplicitDimension)
                            {
                                foreach (DimensionDomain dd in dim.Domains)
                                {

                                    foreach (DomainMember dm in dd.DomainMembers)
                                    {
                                        if (fd.ListData == null) fd.ListData = new List<FieldListItem>();
                                        fd.ListData.Add(new FieldListItem
                                        {
                                            Domain = dd.Id
                                            ,
                                            DomainMember = dm.Id
                                            ,
                                            ElementDef = dm.ElementDefinition
                                        });

                                    }
                                }
                            }
                            else
                            {
                                XbrlElementDefinition tpeXed = GlobalElementDefintions[dim.Id];
                                if (!string.IsNullOrEmpty(tpeXed.TypedDomainRef))
                                {
                                    if (GlobalElementDefintions.ContainsKey(tpeXed.TypedDomainRef))
                                    {
                                        tpeXed = GlobalElementDefintions[tpeXed.TypedDomainRef];
                                    } 
                                }
                                fd.TypeElement = tpeXed;
                                if (GlobalDataTypes.ContainsKey(tpeXed.Id))
                                {
                                    fd.DataType = GlobalDataTypes[tpeXed.Id];
                                }
                            }
                            cDef.Fields.Add(fd);
                        }
                    }
                    else
                    {
                        Console.WriteLine(hcl.HyperCubeHref + " NOT FOUND");
                    }
                }
                else
                {
                    // standard role?
                    HyperCube hc = hypercubes.FirstOrDefault(h => h.Href == hcl.HyperCubeHref);
                    if (hc == null)
                    {
                        //items.Add(new ContextDimension { Dimension = "", Type = DimensionType.ExplicitDimension, TypeMember = "", Value = "" });
                        // is not defined seperately, so has no dimensions -> context solely based on periodType
                        //Console.WriteLine("Empty hypercube");
                        //Console.WriteLine("HYPERCUBE NOT FOUND!!");
                    }
                    else
                    {
                        //Console.WriteLine(hc.Dimensions.Count + " dimensions found");

                    }
                }
            }
            cDef.Fields = cDef.Fields.Distinct().ToList();
            foreach (HyperCubeLink hcl in hcls.Where(h => h.ArcRole == HyperCubeArcRoles.NotAll))
            {
                if (!string.IsNullOrEmpty(hcl.TargetRole))
                {
                    HyperCube hc = hypercubes.First(h => h.RoleUri == hcl.TargetRole && h.Href == hcl.HyperCubeHref);
                    foreach (Dimension dim in hc.Dimensions)
                    {
                        if (dim.Type == DimensionType.ExplicitDimension)
                        {
                            FieldDefinition fdi = cDef.Fields.FirstOrDefault(f=>f.Dimension==dim.Id);
                            foreach (DimensionDomain dd in dim.Domains)
                            {
                                foreach (DomainMember dm in dd.DomainMembers)
                                {
                                    FieldListItem cdim = fdi.ListData.FirstOrDefault(l => l.Domain == dd.Id && l.DomainMember == dm.Id);
                                    if (cdim != null) fdi.ListData.Remove(cdim);

                                }
                            }
                        }
                    }
                }
            }

            if (cDef.Fields.Count(f => f.DimensionType == DimensionType.ExplicitDimension) == 1 && cDef.Fields.Count == 1)
            {
                FieldDefinition fd = cDef.Fields.First();
                if (fd.ListData != null)
                {
                    foreach (FieldListItem fli in fd.ListData)
                    {
                        ContextDefinition cNew = new ContextDefinition
                        {
                            Fields = new List<FieldDefinition> { new FieldDefinition {Dimension = fd.Dimension, DimensionType = fd.DimensionType
                                , ListData=new List<FieldListItem> {fli } , Fixed=true, TypeElement=fli.ElementDef}
                            }
                            ,
                            Periods = cDef.Periods
                            ,
                            Role = cDef.Role
                        };
                        gxed.ContextDefinitions.Add(cNew);
                    }
                }
            }
            else
            {
                gxed.ContextDefinitions.Add(cDef);
            }

            GlobalElementDefintions.Remove(gxed.Href);
            GlobalElementDefintions.Add(gxed.Href, gxed);
                //int dimCount = items.Count;
                //if (dimCount == 0)
                //{
                //    Console.WriteLine(string.Format("   - {0}", period));
                //} 
                //else if (dimCount == items.Count(i => i.Type == DimensionType.ExplicitDimension))
                //{
                //    foreach (ContextDimension it in items)
                //    {
                //        if (it.Dimension == string.Empty && it.Value == string.Empty)
                //        {
                //            throw new Exception("empty dimension");
                //        }
                //        else
                //        {
                //           ContextSetting cs = new ContextSetting {
                //               Fields=null
                //               , Fixed=true
                //               , 
                //            Console.WriteLine(string.Format("   - {0}__{1} ({2})", period, it.TypeMember, it.Value));
                //        }
                //    }
                //}
                //else
                //{
                //    StringBuilder sb = new StringBuilder(period);
                //    foreach (ContextDimension cd in items)
                //    {
                //        sb.Append("__").Append(cd.TypeMember);
                //    }
                //    Console.WriteLine(string.Format("   - {0} :: MIXED_OR_TYPE_ONLY", sb.ToString()));
                //}
            

        }


        private void ReadDefinition(ref TaxonomyXsd Xsd, string path)
        {
            string fileName = System.IO.Path.GetFileName(path);
            foreach (AnnotionXbrlLink axl in Xsd.AnnotionXbrlLinks.Where(a => a.LinkType == XbrlLinkTypes.Definition && a.Origin == fileName))
            {
                string deffile = System.IO.Path.Combine(System.IO.Path.GetDirectoryName(Path), axl.Href);
                List<AnnotionLink> annotationLinks = Xsd.AnnotationLinks.Where(al => al.Origin == fileName && al.UsedOn == "link:definitionLink").ToList();
                ReadDefinitionFile(deffile, annotationLinks, ref Xsd, path);
            }
        }

        public List<HyperCube> hypercubes = new List<HyperCube>();

        public List<string> emptyhypercubes = new List<string>();

        private void ReadDefinitionFile(string defFile, List<AnnotionLink> links, ref TaxonomyXsd Xsd, string path)
        {
            string defContext = System.IO.Path.GetFileNameWithoutExtension(defFile).Replace("-definition", "");
            XmlReader reader = new XmlTextReader(defFile);
            XmlNamespaceManager xnsm = new XmlNamespaceManager(new NameTable());
            XDocument defDoc = XDocument.Load(defFile);
            Dictionary<string, XNamespace> namespaces = defDoc.Root.Attributes().
                           Where(a => a.IsNamespaceDeclaration).
                           GroupBy(a => a.Name.Namespace == XNamespace.None ? String.Empty : a.Name.LocalName,
                                   a => XNamespace.Get(a.Value)).
                           ToDictionary(g => g.Key,
                                        g => g.First());

            foreach (string key in namespaces.Keys)
            {
                xnsm.AddNamespace(key, namespaces[key].NamespaceName);
            }

            

            foreach (XElement defLink in defDoc.Root.Elements(namespaces[""] + "definitionLink"))
            {
                XElement mainLoc = defLink.Elements(namespaces[""] + "loc").FirstOrDefault();
                string mainHref = mainLoc.Attribute(namespaces["xlink"] + "href").Value;
                bool hyperCubeDef = mainHref.EndsWith("Hypercube");
                XbrlElementDefinition roleDef = GlobalElementDefintions[mainHref];

                DefinitionLink conceptDl = DefinitionLink.CreateFromElement(defLink, namespaces);

                if (hyperCubeDef)
                {

                    HyperCube hc = new HyperCube
                    {
                        ContextFile = defFile
                        ,
                        Dimensions = new List<Dimension>()
                        ,
                        Href = mainHref
                        ,
                        RoleUri = conceptDl.Role
                    };

                    foreach (DefinitionArc hdim in conceptDl.DefinitionArcs.Where(d => d.ArcRole == "http://xbrl.org/int/dim/arcrole/hypercube-dimension"))
                    {
                        XbrlElementDefinition xed = GlobalElementDefintions[hdim.ToLoc.Href];
                        hc.Dimensions.Add(new Dimension
                        {
                            Domains = new List<DimensionDomain>()
                            ,
                            Id = hdim.ToLoc.Href
                            ,
                            Type = string.IsNullOrEmpty(xed.TypedDomainRef) ? DimensionType.ExplicitDimension : DimensionType.TypedDimension
                        });
                    }


                    foreach (DefinitionArc hdim in conceptDl.DefinitionArcs.Where(d => d.ArcRole == "http://xbrl.org/int/dim/arcrole/dimension-domain"))
                    {
                        XbrlElementDefinition xed = GlobalElementDefintions[hdim.ToLoc.Href];

                        Dimension dimens = hc.Dimensions.FirstOrDefault(d => d.Id == hdim.FromLoc.Href);
                        if (dimens!=null)
                        {
                            
                            DimensionDomain dimdo = new DimensionDomain
                            {
                                DomainMembers = new List<DomainMember>()
                                ,
                                ElementDefinition = xed
                                ,
                                Id = hdim.ToLoc.Href
                                ,
                                Order = hdim.Order
                            };
                            foreach (DefinitionArc mdim in conceptDl.DefinitionArcs.Where(d => d.ArcRole == "http://xbrl.org/int/dim/arcrole/domain-member" && d.From == hdim.To))
                            {
                                dimdo.DomainMembers.Add(new DomainMember
                                {
                                    ElementDefinition = GlobalElementDefintions.ContainsKey(mdim.ToLoc.Href) ? GlobalElementDefintions[mdim.ToLoc.Href] :  null
                                    ,
                                    Id = mdim.ToLoc.Href
                                });
                            }
                            dimens.Domains.Add(dimdo);
                        }
                    }
                    hypercubes.Add(hc);
                    
                }
                else
                {

                    Context maintCtx = new Context
                    {
                        RoleId = conceptDl.Role
                        ,
                        HyperCubes = new List<HyperCubeLink>()
                        ,
                        PrimaryConcept = mainHref
                    };

                    Dictionary<string, List<HyperCubeLink>> elementHyperCubeLinks = new Dictionary<string, List<HyperCubeLink>>();

                    foreach (DefinitionArc harc in conceptDl.DefinitionArcs.Where(a => a.ArcRole == "http://xbrl.org/int/dim/arcrole/all"
                                                                                          || a.ArcRole == "http://xbrl.org/int/dim/arcrole/notAll"))
                    {
                        if (!elementHyperCubeLinks.ContainsKey(harc.FromLoc.Href))
                        {
                            elementHyperCubeLinks.Add(harc.FromLoc.Href, new List<HyperCubeLink>());
                        }
                        if (string.IsNullOrEmpty(harc.TargetRole)) emptyhypercubes.Add(harc.ToLoc.Href);
                        elementHyperCubeLinks[harc.FromLoc.Href].Add(new HyperCubeLink
                        {
                            ArcRole = harc.ArcRole == "http://xbrl.org/int/dim/arcrole/all" ? HyperCubeArcRoles.All : HyperCubeArcRoles.NotAll
                            ,
                            Closed = harc.Closed
                            ,
                            ContextElement = harc.ContextElement
                            ,
                            HyperCubeHref = harc.ToLoc.Href
                            ,
                            TargetRole = harc.TargetRole
                        });
                    }

                    foreach (string key in elementHyperCubeLinks.Keys)
                    {
                        GlobalElementDefintions[key].Contexts.Add(new Context
                        {
                            HyperCubes = elementHyperCubeLinks[key]
                            ,
                            PrimaryConcept = key
                            ,
                            RoleId = conceptDl.Role
                        });
                    }

                    if (elementHyperCubeLinks.ContainsKey(mainHref))
                    {
                        maintCtx.HyperCubes = elementHyperCubeLinks[mainHref];
                    }

                    bool domainMembers = false;
                    foreach (DefinitionArc arc in conceptDl.DefinitionArcs)
                    {
                        switch (arc.ArcRole)
                        {
                            case "http://xbrl.org/int/dim/arcrole/domain-member":
                                if (arc.FromLoc.Href == mainHref)
                                {
                                    GlobalElementDefintions[arc.ToLoc.Href].Contexts.Add(maintCtx);
                                    // GlobalElementDefintions[arc.ToLoc.Href].Contexts = GlobalElementDefintions[arc.ToLoc.Href].Contexts.Distinct().ToList();
                                }
                                if (elementHyperCubeLinks.ContainsKey(arc.ToLoc.Href))
                                {
                                    GlobalElementDefintions[arc.ToLoc.Href].Contexts.Add(new Context
                                    {
                                        HyperCubes = elementHyperCubeLinks[arc.ToLoc.Href]
                                        ,
                                        PrimaryConcept = arc.ToLoc.Href
                                        ,
                                        RoleId = conceptDl.Role
                                    });
                                    //GlobalElementDefintions[arc.ToLoc.Href].PrimaryConcepts.Add(arc.FromLoc.Href);
                                }

                                break;
                            //case "http://xbrl.org/int/dim/arcrole/notAll":
                            //    cd.HyperCubeConceptRole = HyperCubeConceptRole.NotAll;
                            //    cd.ContextElement = arc.ContextElement;
                            //    cd.HyperCubeTargetRole = arc.TargetRole;
                            //    cd.HyperCubeHref = arc.ToLoc.Href;
                            //    break;
                            //case "http://xbrl.org/int/dim/arcrole/all":
                            //    cd.HyperCubeConceptRole = HyperCubeConceptRole.All;
                            //    cd.ContextElement = arc.ContextElement;
                            //    cd.HyperCubeTargetRole = arc.TargetRole;
                            //    cd.HyperCubeHref = arc.ToLoc.Href;
                            //    break;
                        }
                    }

                }
            }

            /*
            foreach (AnnotionLink alink in links)
            {
                // Get Role
                XElement roleRef = defDoc.Root.Elements(namespaces[""] + "roleRef").Where(el => el.Attribute( "roleURI").Value == alink.RoleUri).FirstOrDefault();
                if (roleRef != null)
                {
                    // select root element
                    string rolehref = roleRef.Attribute(namespaces["xlink"] + "href").Value;

                    // get corresponding definition link
                    XElement deflink = defDoc.Root.Elements(namespaces[""] + "definitionLink").Where(el => el.Attribute(namespaces["xlink"] + "role").Value == alink.RoleUri).FirstOrDefault();

                    if (deflink != null)
                    {
                        // get first location (=main parent)

                        XElement mainLoc = deflink.Elements(namespaces[""] + "loc").FirstOrDefault();
                        string mainHref = mainLoc.Attribute(namespaces["xlink"] + "href").Value;

                        XbrlElementDefinition roleDef = GlobalElementDefintions[mainHref];

                        switch (roleDef.SubstitutionGroup)
                        {
                            case "http://www.xbrl.org/2003/instance:tuple":
                            case "http://www.xbrl.org/2003/instance:item": // likely a concept
                                ConceptDefinition cd = new ConceptDefinition
                                {
                                    ElementHref = mainHref
                                    ,
                                    ContextFile = defContext
                                    , Children = new List<string>()
                                };
                                DefinitionLink conceptDl = DefinitionLink.CreateFromElement(deflink, namespaces);
                                foreach (DefinitionArc arc in conceptDl.DefinitionArcs)
                                {
                                    switch (arc.ArcRole)
                                    {
                                        case "http://xbrl.org/int/dim/arcrole/domain-member":
                                            cd.Children.Add(arc.ToLoc.Href);
                                            break;
                                        case "http://xbrl.org/int/dim/arcrole/notAll":
                                            cd.HyperCubeConceptRole = HyperCubeConceptRole.NotAll;
                                            cd.ContextElement = arc.ContextElement;
                                            cd.HyperCubeTargetRole = arc.TargetRole;
                                            cd.HyperCubeHref = arc.ToLoc.Href;
                                            break;
                                        case "http://xbrl.org/int/dim/arcrole/all":
                                            cd.HyperCubeConceptRole = HyperCubeConceptRole.All;
                                            cd.ContextElement = arc.ContextElement;
                                            cd.HyperCubeTargetRole = arc.TargetRole;
                                            cd.HyperCubeHref = arc.ToLoc.Href;
                                            break;
                                    }
                                }
                                Xsd.ConceptDefinitions.Add(cd);

                                break;
                            case "http://xbrl.org/2005/xbrldt:hypercubeItem": // hypercube
                                HyperCube hc = new HyperCube
                                {
                                   Href=mainHref
                                   ,
                                   ContextFile = defContext
                                   ,
                                   RoleUri = alink.RoleUri
                                   , Dimensions = new Dictionary<string,Dimension>()
                                };
                                DefinitionLink hyperDl = DefinitionLink.CreateFromElement(deflink, namespaces);

                                foreach (DefinitionArc hdim in hyperDl.DefinitionArcs.Where(d => d.ArcRole == "http://xbrl.org/int/dim/arcrole/hypercube-dimension"))
                                {
                                    XbrlElementDefinition xed = GlobalElementDefintions[hdim.ToLoc.Href];
                                    hc.Dimensions.Add(hdim.ToLoc.Href, new Dimension
                                    {
                                        Domains = new List<DimensionDomain>()
                                        ,
                                        Id = hdim.ToLoc.Href
                                        ,
                                        Type = string.IsNullOrEmpty(xed.TypedDomainRef) ? DimensionType.ExplicitDimension : DimensionType.TypedDimension
                                    });
                                }


                                foreach (DefinitionArc hdim in hyperDl.DefinitionArcs.Where(d => d.ArcRole == "http://xbrl.org/int/dim/arcrole/dimension-domain"))
                                {
                                    XbrlElementDefinition xed = GlobalElementDefintions[hdim.ToLoc.Href];

                                    if (hc.Dimensions.ContainsKey(hdim.FromLoc.Href))
                                    {
                                        Dimension dimens = hc.Dimensions[hdim.FromLoc.Href];
                                        DimensionDomain dimdo = new DimensionDomain
                                        {
                                            DomainMembers = new List<DomainMember>()
                                            ,
                                            ElementDefinition = xed
                                            ,
                                            Id = hdim.ToLoc.Href
                                            ,
                                            Order = hdim.Order
                                        };
                                        foreach (DefinitionArc mdim in hyperDl.DefinitionArcs.Where(d => d.ArcRole == "http://xbrl.org/int/dim/arcrole/domain-member" && d.From==hdim.To))
                                        {
                                            dimdo.DomainMembers.Add(new DomainMember
                                            {
                                                ElementDefinition = null
                                                ,
                                                Id = mdim.ToLoc.Href
                                            });
                                        }
                                        dimens.Domains.Add(dimdo);
                                    }
                                }
                                Xsd.HyperCubes.Add(hc.RoleUri, hc);
                                break;

                        }
                    }
                }

            }
             * */
        }

        private void ReadSimpleType(XmlSchemaSimpleType simpleType)
        {
            ReadSimpleType(simpleType, simpleType.Name);
        }

        private void ReadSimpleType(XmlSchemaSimpleType simpleType,string name)
        {
            if (!GlobalDataTypes.ContainsKey(name))
            {
                Uri sourceXsdUri = new Uri(simpleType.SourceUri);
                string sourceXsd = sourceXsdUri.Segments[sourceXsdUri.Segments.Length-1];
                
                XbrlDataType dt = new XbrlDataType { Origin = sourceXsd, Name = name, Restrictions = new List<Restriction>(), ValueType = simpleType.Datatype.ValueType.ToString(), BaseType = simpleType.BaseXmlSchemaType != null ? simpleType.BaseXmlSchemaType.Name : null };

                

                XmlSchemaSimpleTypeRestriction restriction = simpleType.Content as XmlSchemaSimpleTypeRestriction;

                if (restriction != null)
                {

                    Console.WriteLine("restriction : {0}", restriction.BaseTypeName.Name);

                    foreach (XmlSchemaObject facet in restriction.Facets)
                    {
                       Restriction restrict = new Restriction { Name=facet.GetType().ToString()};
                        
                       // if (!facets.Contains(facet.GetType().ToString())) facets.Add(facet.GetType().ToString());
                        Console.WriteLine("  > {0}", facet.ToString());
                        if (facet is XmlSchemaFacet)
                        {
                            restrict.Value = ((XmlSchemaFacet)facet).Value;
                        }
                        if (facet is XmlSchemaEnumerationFacet)
                            restrict.Value = ((XmlSchemaEnumerationFacet)facet).Value;

                        dt.Restrictions.Add(restrict);
                    }
                }
                GlobalDataTypes.Add(dt.Name, dt);
            }
        }

        private void ReadSimpleContent(XmlSchemaSimpleContent cnt, ref XbrlDataType dt)
        {
            XmlSchemaSimpleContentRestriction restriction = cnt.Content as XmlSchemaSimpleContentRestriction;

            
            if (restriction != null)
            {

                Console.WriteLine("restriction : {0}", restriction.BaseTypeName.Name);

                foreach (XmlSchemaObject facet in restriction.Facets)
                {
                    Restriction restrict = new Restriction { Name = facet.GetType().ToString() };

                  //  if (!facets.Contains(facet.GetType().ToString())) facets.Add(facet.GetType().ToString());
                    Console.WriteLine("  > {0}", facet.ToString());
                    if (facet is XmlSchemaFacet)
                    {
                        restrict.Value = ((XmlSchemaFacet)facet).Value;
                    }
                    if (facet is XmlSchemaEnumerationFacet)
                        restrict.Value = ((XmlSchemaEnumerationFacet)facet).Value;

                    dt.Restrictions.Add(restrict);
                }
            }

            XmlSchemaSimpleContentExtension extension = cnt.Content as XmlSchemaSimpleContentExtension;

            if (extension != null)
            {

                Console.WriteLine("extension : {0}", extension.BaseTypeName);

                //foreach (XmlSchemaAttribute att in extension.Attributes)
                //{
                //    Console.WriteLine(att.Name);
                //    ReadSimpleType(att.SchemaType, att.SchemaType.Name);
                //}
                ////foreach (XmlSchemaObject facet in extension.Facets)
                //{
                //    Console.WriteLine("  > {0}", facet.ToString());
                //    if (facet is XmlSchemaFacet)
                //        Console.WriteLine("  > {0}", ((XmlSchemaFacet)facet).Value);
                //    if (facet is XmlSchemaEnumerationFacet)

                //        Console.WriteLine("Element: {0}", ((XmlSchemaEnumerationFacet)facet).Value);
                //}
            }
        }

        private void ReadLabel(string file)
        {
            Console.WriteLine("Read labelfile: " + file);
            XmlReader reader = new XmlTextReader(file);
            XmlNamespaceManager xnsm = new XmlNamespaceManager(new NameTable());
            XDocument lblDoc = XDocument.Load(reader);
            Dictionary<string, XNamespace> namespaces = lblDoc.Root.Attributes().
                            Where(a => a.IsNamespaceDeclaration).
                            GroupBy(a => a.Name.Namespace == XNamespace.None ? String.Empty : a.Name.LocalName,
                                    a => XNamespace.Get(a.Value)).
                            ToDictionary(g => g.Key,
                                         g => g.First());


            foreach (string key in namespaces.Keys)
            {
                xnsm.AddNamespace(key, namespaces[key].NamespaceName);
            }

            XElement linkBase = lblDoc.Root;
            foreach (XElement labelLink in linkBase.Elements(namespaces[""]+"labelLink"))
            {
                foreach (XElement labelArc in labelLink.Elements(namespaces[""]+"labelArc"))
                {


                    string from = labelArc.Attribute(namespaces["xlink"] + "from").Value;
                    string to = labelArc.Attribute(namespaces["xlink"] + "to").Value;
                    string arcrole = labelArc.Attribute(namespaces["xlink"] + "arcrole").Value;

                    XElement fromLocator = labelLink.XPathSelectElement(string.Format("*[local-name()='loc' and @xlink:label='{0}']",from), xnsm);
                    string fromHref = fromLocator.Attribute(namespaces["xlink"] + "href").Value;

                    Console.WriteLine("LABEL [{0}]", from);
                    List<XElement> xLabels = labelLink.XPathSelectElements(string.Format("*[local-name()='label' and @xlink:label='{0}']", to), xnsm).ToList();

                    foreach (string role in xLabels.Select(x => x.Attribute(namespaces["xlink"] + "role").Value).Distinct())
                    {

                        ElementLabel elLabel = new ElementLabel
                        {
                            Role = role.Replace("http://www.xbrl.org/2003/role/", "")
                            ,
                            Descriptions = new List<Translation>()
                            ,
                            ElementHref = fromHref
                            ,
                            Id = to
                        };

                        foreach (XElement labelEl in labelLink.XPathSelectElements(string.Format("*[local-name()='label' and @xlink:label='{0}' and @xlink:role='{1}']", to, role), xnsm))
                        {
                            elLabel.Descriptions.Add(new Translation
                            {
                                Language = labelEl.Attribute(XNamespace.Xml + "lang").Value
                                ,
                                Description = labelEl.Value
                            });
                        }
                        if (GlobalElementDefintions[fromHref].Labels == null) GlobalElementDefintions[fromHref].Labels = new List<ElementLabel>();
                        GlobalElementDefintions[fromHref].Labels.Add(elLabel);
                    }

                }
            }


        }

        private void AddElementAttribs(ref XElement element, string href)
        {
            if (GlobalElementDefintions.ContainsKey(href))
            {
                XbrlElementDefinition xed = GlobalElementDefintions[href];


                element.Add(new XAttribute("periodType", xed.PeriodType ==null ? string.Empty : xed.PeriodType)
                            , new XAttribute("abstract", xed.Abstract == null ? string.Empty : xed.Abstract.ToString())
                            , new XAttribute("balance", xed.Balance == null ? string.Empty : xed.Balance)
                            , new XAttribute("fixed", xed.Fixed == null ? string.Empty : xed.Fixed)
                            , new XAttribute("name", xed.Name == null ? string.Empty : xed.Name)
                            , new XAttribute("nillable", xed.Nillable == null ? string.Empty : xed.Nillable.ToString())
                            , new XAttribute("ref", xed.Ref == null ? string.Empty : xed.Ref)
                            , new XAttribute("substitutionGroup", xed.SubstitutionGroup == null ? string.Empty : xed.SubstitutionGroup)
                            , new XAttribute("type", xed.Type == null ? string.Empty : xed.Type)
                            , new XAttribute("typedDomainRef", xed.TypedDomainRef == null ? string.Empty : xed.TypedDomainRef)
                            );

            }
        }

        private void ReadPresentation(string file, ref XElement root)
        {
            // TODO => Prefered label
            string contextFile = System.IO.Path.GetFileNameWithoutExtension(file).Replace("-presentation", "");
            XmlReader reader = new XmlTextReader(file);
            XmlNamespaceManager xnsm= new XmlNamespaceManager(new NameTable());
            XDocument presDoc = XDocument.Load(reader);
            Dictionary<string, XNamespace> namespaces = presDoc.Root.Attributes().
                            Where(a => a.IsNamespaceDeclaration).
                            GroupBy(a => a.Name.Namespace == XNamespace.None ? String.Empty : a.Name.LocalName,
                                    a => XNamespace.Get(a.Value)).
                            ToDictionary(g => g.Key,
                                         g => g.First());

           
            foreach (string key in namespaces.Keys)
            {
                xnsm.AddNamespace(key, namespaces[key].NamespaceName);
            }

            foreach (XElement roleRef in presDoc.Root.Elements(namespaces[""] + "roleRef"))
            {
                string xhref = roleRef.Attribute(namespaces["xlink"] + "href").Value;
                string[] splittedHref = xhref.Split(new char[] { '#' });
                string roleUri = roleRef.Attribute("roleURI").Value;
                XElement roleRoot = root.Elements("Element").Where(el => el.Attribute("id").Value == splittedHref[1]).FirstOrDefault();
                bool roleRootExists = true;
                if (roleRoot == null)
                {
                    
                    roleRoot = new XElement("Element"
                                                        , new XAttribute("href", xhref)
                                                        , new XAttribute("id", splittedHref[1])
                                                        , new XAttribute("origin", splittedHref[0])
                                                        , new XAttribute("roleUri", roleUri)
                                                        , new XAttribute("contextFile",contextFile)
                                                        , GetXmlLabels(xhref)
                                                        );
                    AddElementAttribs(ref roleRoot, xhref);
                    roleRootExists = false;
                }
                
                foreach (XElement presLink in presDoc.Root.XPathSelectElements(string.Format("*[name()='presentationLink' and @xlink:role='{0}']", roleUri), xnsm))
                {
                    // get first locator

                    Dictionary<string, XElement> reuseInSameLinkBase = new Dictionary<string, XElement>();

                    XElement firstLoc = presLink.Elements().First();
                    string plXhref = firstLoc.Attribute(namespaces["xlink"] + "href").Value;
                    string[] plSplittedHref = plXhref.Split(new char[] { '#' });

                    XElement presLinkRoot = roleRoot.Elements("Element").Where(el => el.Attribute("id").Value == plSplittedHref[1]).FirstOrDefault();
                    bool presLinkRootExists = true;
                    if (presLinkRoot == null)
                    {
                        
                        presLinkRoot = new XElement("Element"
                                                        , new XAttribute("href", plXhref)
                                                        , new XAttribute("id", plSplittedHref[1])
                                                        , new XAttribute("origin", plSplittedHref[0])
                                                        , new XAttribute("contextFile", contextFile)
                                                        , new XAttribute("roleHref",xhref)
                                                        , GetXmlLabels(plXhref)
                                                        );
                        AddElementAttribs(ref presLinkRoot, plXhref);

                        presLinkRootExists = false;
                    }
                    foreach (XElement presArc in presLink.Elements(namespaces[""] + "presentationArc"))
                    {
                        string from = presArc.Attribute(namespaces["xlink"] + "from").Value;
                        string to = presArc.Attribute(namespaces["xlink"] + "to").Value;
                        string order = presArc.Attribute("order") !=null ? presArc.Attribute("order").Value : "";
                        string preferredLabel = presArc.Attribute("preferredLabel") != null ? presArc.Attribute("preferredLabel").Value.Replace("http://www.xbrl.org/2003/role/", "") : "label";

                        XElement fromLoc = presLink.XPathSelectElement(string.Format("*[name()='loc' and @xlink:label='{0}']", from), xnsm);
                        string fromXhref = fromLoc.Attribute(namespaces["xlink"] + "href").Value;
                        string[] fromSplittedHref = fromXhref.Split(new char[] { '#' });

                        XElement parent = presLinkRoot.XPathSelectElement(string.Format("descendant-or-self::Element[@id='{0}']", fromSplittedHref[1]));

                        // get to locator
                        XElement toLoc = presLink.XPathSelectElement(string.Format("*[name()='loc' and @xlink:label='{0}']",to), xnsm);
                        string toXhref = toLoc.Attribute(namespaces["xlink"] + "href").Value;
                        string[] toSplittedHref = toXhref.Split(new char[] { '#' });

                        if (parent != null)
                        {
                            if (parent.Elements("Element").Where(el => el.Attribute("id").Value == toSplittedHref[1]).FirstOrDefault() == null)
                            {
                                // add reuse in same presentation link ?
                                XElement childEL = null;
                                if (reuseInSameLinkBase.ContainsKey(toXhref))
                                {
                                    childEL= XElement.Parse(reuseInSameLinkBase[toXhref].ToString());
                                }
                                else
                                {
                                    childEL = new XElement("Element"
                                                                , new XAttribute("href", toXhref)
                                                                , new XAttribute("id", toSplittedHref[1])
                                                                , new XAttribute("origin", toSplittedHref[0])
                                                                , new XAttribute("order", order)
                                                                , new XAttribute("roleHref", xhref)
                                                                , new XAttribute("contextFile", contextFile)
                                                                , new XAttribute("preferredLabel", preferredLabel)
                                                                , GetXmlLabels(toXhref)
                                                                );
                                    AddElementAttribs(ref childEL, toXhref);
                                    if (!reuseInSameLinkBase.ContainsKey(toXhref)) reuseInSameLinkBase.Add(toXhref, childEL);
                                }
                                parent.Add(childEL);
                            }
                        }

                    }
                    if(!presLinkRootExists) roleRoot.Add(presLinkRoot);
                }
                if (!roleRootExists) root.Add(roleRoot);
            }
        }

        private XElement GetXmlLabels(string href)
        {
            XElement labels = new XElement("labels");
            if (GlobalElementDefintions.ContainsKey(href))
            {
                XbrlElementDefinition ed = GlobalElementDefintions[href];
                if (ed.Labels != null)
                {
                    foreach (ElementLabel elLabel in ed.Labels)
                    {
                        XElement label = new XElement("label"
                                            , new XAttribute("type", elLabel.Role)
                                            , new XAttribute("href", elLabel.ElementHref)
                                            , new XAttribute("id", elLabel.Id)
                                            );
                        foreach (Translation trans in elLabel.Descriptions)
                        {
                            label.Add(new XElement("translation", new XAttribute("lang", trans.Language), trans.Description));
                        }
                        labels.Add(label);
                    }
                }
            }
            return labels;
        }

        private string GetComplexValueType(XmlSchemaType xstype)
        {
            if (xstype.Datatype != null)
            {
                return xstype.Datatype.ValueType.ToString();
            }
            else if (xstype.BaseXmlSchemaType != null)
            {
                return GetComplexValueType(xstype.BaseXmlSchemaType);
            }
            else
            {
                return typeof(string).ToString();
            }
        }

        private void ReadComplexType(XmlSchemaComplexType complexType)
        {
            ReadComplexType(complexType, complexType.Name);
        }

        private void ReadComplexType(XmlSchemaComplexType complexType, string name)
        {
            if (!GlobalDataTypes.ContainsKey(name))
            {

                XbrlDataType dt = new XbrlDataType { Origin = complexType.SourceUri, TypeCode=complexType.TypeCode.ToString(), Name = name, Restrictions = new List<Restriction>(), ValueType = complexType.Datatype != null ? complexType.Datatype.ValueType.ToString() : GetComplexValueType(complexType.BaseXmlSchemaType), BaseType = complexType.BaseXmlSchemaType != null ? complexType.BaseXmlSchemaType.Name : null };

                if (complexType.AttributeUses.Count > 0)
                {
                    IDictionaryEnumerator enumerator =
                        complexType.AttributeUses.GetEnumerator();
                    dt.Attributes = new List<XbrlAttribute>();
                    while (enumerator.MoveNext())
                    {
                        XmlSchemaAttribute attribute =
                            (XmlSchemaAttribute)enumerator.Value;
                        dt.Attributes.Add(new XbrlAttribute { Name = attribute.Name, Value = string.IsNullOrEmpty(attribute.FixedValue) ? attribute.AttributeSchemaType.Name : attribute.FixedValue });
                        Console.WriteLine("Attribute: {0}", attribute.QualifiedName);
                    }
                }

                //complexType.ContentModel
                //Console.WriteLine(complexType.ContentType.ToString());

                //XmlSchemaSimpleContent simpleContent = complexType.ContentTypeParticle as XmlSchemaSimpleContent
                if (complexType.ContentModel != null)
                {
                    string cotype = complexType.ContentModel.GetType().ToString();
                 //   if (!complexContentTypes.Contains(cotype)) complexContentTypes.Add(cotype);

                    XmlSchemaSimpleContent simpleContent = complexType.ContentModel as XmlSchemaSimpleContent;
                    if (simpleContent != null)
                    {

                        ReadSimpleContent(simpleContent, ref dt);
                    }
                }
                else
                {
                    Console.WriteLine("no content");
                }

                // Get the sequence particle of the complex type.
                //XmlSchemaSequence sequence = complexType.ContentTypeParticle as XmlSchemaSequence;

                //// Iterate over each XmlSchemaElement in the Items collection.
                //foreach (XmlSchemaElement childElement in sequence.Items)
                //{
                //    Console.WriteLine("Element: {0}", childElement.Name);
                //}
                GlobalDataTypes.Add(dt.Name, dt);
            }
        }

        public void schemaSet_ValidationEventHandler(object sender, ValidationEventArgs e)
        {
            if (e.Exception != null)
                Console.WriteLine(e.Message);
            // perform no validation handling at this point
            //throw new NotImplementedException();
        }
    }
}
