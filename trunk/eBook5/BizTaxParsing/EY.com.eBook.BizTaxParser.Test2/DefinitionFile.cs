﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;


namespace EY.com.eBook.BizTaxParser.Test2
{
    public class DefinitionFile
    {
        public List<DefinitionLink> Links { get; set; }

        public DefinitionFile(string path)
        {
           // FileName = System.IO.Path.GetFileNameWithoutExtension(fle);
            ReadFile(path);
        }

        internal void ReadFile(string path)
        {
            //Definitions = new Dictionary<string, Dictionary<string, List<Translation>>>();
            Links = new List<DefinitionLink>();



            Console.WriteLine("Definition: " + Path.GetFileNameWithoutExtension(path));
            XmlReader reader = new XmlTextReader(path);
            XDocument ldoc = XDocument.Load(path);
            Dictionary<string, XNamespace> mnamespaces = ldoc.Root.Attributes().
                        Where(a => a.IsNamespaceDeclaration).
                        GroupBy(a => a.Name.Namespace == XNamespace.None ? String.Empty : a.Name.LocalName,
                                a => XNamespace.Get(a.Value)).
                        ToDictionary(g => g.Key,
                                     g => g.First());

            foreach (XElement element in ldoc.XPathSelectElements("//node()[local-name()='definitionLink']", new XmlNamespaceManager(reader.NameTable)))
            {

                Links.Add(DefinitionLink.CreateFromElement(element, mnamespaces));
            }

        }

       

    }
}
