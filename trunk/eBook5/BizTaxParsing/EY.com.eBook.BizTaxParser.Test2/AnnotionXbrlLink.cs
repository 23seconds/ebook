﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EY.com.eBook.BizTaxParser.Test2
{
    public enum XbrlLinkTypes
    {
        Definition
        , Presentation
        , Label
        , Assertion
        , Parameter
        , UNKNOWN
    }

    // XBRL specific link (role/arcrole) (points to XML file)
    public class AnnotionXbrlLink
    {
        public XbrlLinkTypes LinkType { get; set; }

        public string Origin { get; set; }
        // defaults
        public string XLinkType { get; set; }
        public string Href { get; set; }
        public string ArcRole { get; set; }

        // extras
        public string Role { get; set; }
        public string Title { get; set; }

        
        public void SetType(string arcrole, string role,string href )
        {
            ArcRole = arcrole;
            Role = role;
            Href = href;
            switch (role)
            {
                case "http://www.xbrl.org/2003/role/presentationLinkbaseRef":
                    LinkType = XbrlLinkTypes.Presentation;
                    break;
                case "http://www.xbrl.org/2003/role/labelLinkbaseRef":
                    LinkType = XbrlLinkTypes.Label;
                    break;
                case "http://www.xbrl.org/2003/role/definitionLinkbaseRef":
                    LinkType = XbrlLinkTypes.Definition;
                    break;
                default:
                    // no role, detect in href
                    if (href.Contains("parameters"))
                    {
                        LinkType = XbrlLinkTypes.Parameter;
                    }
                    else if (href.Contains("assertion"))
                    {
                        LinkType = XbrlLinkTypes.Assertion;
                    }
                    else
                    {
                        LinkType = XbrlLinkTypes.UNKNOWN;
                    }
                    break;
            }
        }
    }
}
