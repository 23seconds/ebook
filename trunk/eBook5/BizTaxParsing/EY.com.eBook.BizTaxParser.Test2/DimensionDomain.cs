﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.BizTaxParser.Test2
{
    [Serializable]
    [DataContract]
    public class DimensionDomain
    {
        [DataMember]
        public string Id { get; set; }

        [DataMember]
        public int Order { get; set; }

        [DataMember]
        public XbrlElementDefinition ElementDefinition { get; set; }

        [DataMember]
        public List<DomainMember> DomainMembers { get; set; }
    }
}
