﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace EY.com.eBook.BizTaxParser.Test2
{

    public class TaxonomyRoot
    {
        public string VariableId { get; set; }

        public string TaxonomyId { get; set; }

        public string RootXsd { get; set; }

        public static TaxonomyRoot LoadFromVariable(XElement element, Dictionary<string, XNamespace> namespaces)
        {
            TaxonomyRoot tr = new TaxonomyRoot();
            tr.VariableId = element.Attribute("id").Value;
            tr.TaxonomyId = tr.VariableId.Replace("TaxonomySchemaRef-", "");
            tr.RootXsd = element.Attribute("select").Value.Replace("'","");
            return tr;
        }


        public override string ToString()
        {
            return VariableId;
        }
    }

    public class Parameters
    {
        
        private XDocument _doc;
        private Dictionary<string, XNamespace> namespaces;

        public List<TaxonomyRoot> TaxonomyRoots = new List<TaxonomyRoot>();
      


        public Parameters(string fname)
        {
            
            _doc = XDocument.Load(fname);

            namespaces = _doc.Root.Attributes().
                            Where(a => a.IsNamespaceDeclaration).
                            GroupBy(a => a.Name.Namespace == XNamespace.None ? String.Empty : a.Name.LocalName,
                                    a => XNamespace.Get(a.Value)).
                            ToDictionary(g => g.Key,
                                         g => g.First());

            Travers(_doc.Root.Elements());

        }

        private void Travers(IEnumerable<XElement> elements)
        {
            foreach (XElement element in elements)
            {
                switch (element.Name.LocalName)
                {
                    case "parameter":
                        if (element.Attribute("id").Value.StartsWith("TaxonomySchemaRef"))
                        {
                            TaxonomyRoots.Add(TaxonomyRoot.LoadFromVariable(element, namespaces));
                        }
                        break;
                    //case "presentationArc":
                    //    PresentationLinks.Add(PresentationLink.CreateFromElement(element, namespaces));
                    //    break;
                    //case "loc":
                    //    Locators.Add(Locator.CreateFromElement(element, namespaces));
                    //    break;
                }
                if (element.HasElements) Travers(element.Descendants());
            }
        }


    }
}
