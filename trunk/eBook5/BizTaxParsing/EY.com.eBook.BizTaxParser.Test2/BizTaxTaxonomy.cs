﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace EY.com.eBook.BizTaxParser.Test2
{
    public class BizTaxTaxonomy
    {

        public List<Parameters> ParameterFiles = new List<Parameters>();

        public List<TaxonomyRoot> TaxonomyRoots = new List<TaxonomyRoot>();

        // unique xsds
        public Dictionary<string, TaxonomyXsd> Xsds = new Dictionary<string, TaxonomyXsd>(); 

        // global datatypes
        public Dictionary<string, XbrlDataType> DataTypes = new Dictionary<string, XbrlDataType>();

        // global elements
        public Dictionary<string, XbrlElementDefinition> Elements = new Dictionary<string, XbrlElementDefinition>();

        public void ReadTaxonomyFolder(string folder)
        {
            
            #region Obtain parameter file(s)
            Parameters prs = null;
            //string[] files = System.IO.Directory.GetFiles(@"C:\Projects_TFS\Data\BizTax\2011-04-30-v1.1\2011-04-30\DTS\", "*parameters*.xml");

            // SHOULD BE JUST ONE, just in case...
            string[] files = System.IO.Directory.GetFiles(folder, "*parameters*.xml");
            foreach (string paramFile in files)
            {
                Console.WriteLine("Paramfile: " + Path.GetFileNameWithoutExtension(paramFile));
                prs = new Parameters(paramFile);
                ParameterFiles.Add(prs);
                TaxonomyRoots.AddRange(prs.TaxonomyRoots);
            }
            #endregion

            // READ ALL(!) XSD's and construct global lists (datatypes - elements)
            #region Read-in all xsd files in folder
            
            files = System.IO.Directory.GetFiles(folder, "*.xsd");
            TaxonomyRoot rt = TaxonomyRoots.First();
           // foreach(TaxonomyRoot rt in TaxonomyRoots)
          // foreach (string fle in files)
           // {
                string fle = System.IO.Path.Combine(folder, rt.RootXsd);
                TaxonomyXsdReader txr = new TaxonomyXsdReader(fle);
                txr.Read();
                foreach (string key in txr.GlobalDataTypes.Keys)
                {
                    if (!DataTypes.ContainsKey(key)) DataTypes.Add(key, txr.GlobalDataTypes[key]);
                }
                foreach (string key in txr.GlobalElementDefintions.Keys)
                {
                    if (!Elements.ContainsKey(key)) Elements.Add(key, txr.GlobalElementDefintions[key]);
                }
                Xsds.Add(txr.Xsd.Name, txr.Xsd);
            //}
            #endregion

            // Append datatype informations to elements
            // and/or domain(s)

        }
    }
}
