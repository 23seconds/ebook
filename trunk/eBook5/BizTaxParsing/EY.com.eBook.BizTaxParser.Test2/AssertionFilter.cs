﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EY.com.eBook.BizTaxParser.Test2
{
    public class AssertionFilter
    {
        public bool isPeriod { get; set; }

        public bool isGeneralMeasure { get; set; }

        public bool isGeneral { get; set; }

        public bool isExplicitDimension { get; set; }

        public bool isTypedDimension{ get; set; }

        public bool isParentFilter { get; set; }

        public string Test { get; set; }


    }
}
