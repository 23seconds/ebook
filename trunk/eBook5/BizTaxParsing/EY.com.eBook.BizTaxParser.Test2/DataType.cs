﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.BizTaxParser.Test2
{
    [Serializable]
    [DataContract]
    public class XbrlDataType
    {
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Origin { get; set; }
        [DataMember]
        public string Role { get; set; }
        [DataMember]
        public List<Restriction> Restrictions { get; set; }
        [DataMember]
        public string ValueType { get; set; }
        [DataMember]
        public string TypeCode { get; set; }
        [DataMember]
        public string BaseType { get; set; }

        [DataMember]
        public List<XbrlAttribute> Attributes { get; set; }
    }

    [Serializable]
    [DataContract]
    public class XbrlAttribute
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }
}
