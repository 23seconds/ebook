﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;

namespace EY.com.eBook.BizTaxParser.Test2
{
    public class AssertionSet
    {
        public string Id { get; set; }

        public List<Assertion> Assertions { get; set; }

        public static AssertionSet ReadXmlFile(string file)
        {
            AssertionSet aset = new AssertionSet
            {
                Assertions = new List<Assertion>()
            };
            
            XmlReader reader = new XmlTextReader(file);
            XmlNamespaceManager xnsm= new XmlNamespaceManager(new NameTable());
            XDocument ldoc = XDocument.Load(file);
            Dictionary<string, XNamespace> namespaces = ldoc.Root.Attributes().
                        Where(a => a.IsNamespaceDeclaration).
                        GroupBy(a => a.Name.Namespace == XNamespace.None ? String.Empty : a.Name.LocalName,
                                a => XNamespace.Get(a.Value)).
                        ToDictionary(g => g.Key,
                                     g => g.First());

            foreach (string key in namespaces.Keys)
            {
                xnsm.AddNamespace(key, namespaces[key].NamespaceName);
            }
            XElement rootEl = ldoc.Root.Element(namespaces["generic"] + "link");
            XElement assEl = rootEl.Element(namespaces["validation"] + "assertionSet");
            if (assEl!=null)
            {
                aset.Id = assEl.Attribute(namespaces["xlink"] + "label").Value;
                foreach (XElement linkedAss in rootEl.XPathSelectElements(string.Format("//*[@xlink:from='{0}']", aset.Id), xnsm))
                {
                    // add assertions.
                    string toAssert = linkedAss.Attribute(namespaces["xlink"] + "to").Value;
                    XElement vass = rootEl.XPathSelectElement(string.Format("*[@id='{0}']", toAssert), xnsm);
                    //if (vass != null)
                    //{

                    Assertion asert = new Assertion
                    {
                        Id = toAssert
                        ,
                        AspectModel = vass.Attribute("aspectModel") != null ? vass.Attribute("aspectModel").Value : null
                        ,
                        ImplicitFiltering = vass.Attribute("implicitFiltering") != null ? vass.Attribute("implicitFiltering").Value == "true" : false
                        ,
                        Test = vass.Attribute("test") != null ? vass.Attribute("test").Value : null
                        ,
                        Variables = new Dictionary<string, AssertionVariable>()
                        ,
                        NotSatisfiedMessages = new List<AssertionMessage>()
                            ,
                        SatisfiedMessages = new List<AssertionMessage>()
                            ,
                        References = new List<string>()
                    };

                    foreach (XElement varArcEl in rootEl.XPathSelectElements(string.Format("*[@xlink:from='{0}']", asert.Id), xnsm))
                    {
                        string varArcRole = varArcEl.Attribute(namespaces["xlink"] + "arcrole").Value;

                        if (varArcRole == "http://xbrl.org/arcrole/2008/variable-set")
                        {

                            string toVarEl = varArcEl.Attribute(namespaces["xlink"] + "to").Value;
                            XElement varEl = rootEl.XPathSelectElement(string.Format("*[@xlink:label='{0}']", toVarEl), xnsm);

                            string toFilterEl = toVarEl; // varEl.Attribute(namespaces["xlink"] + "to").Value;

                            AssertionVariable asvar = new AssertionVariable
                            {
                                ArcRole = varArcEl.Attribute(namespaces["xlink"] + "arcrole") != null ? varArcEl.Attribute(namespaces["xlink"] + "arcrole").Value : null
                                ,
                                Type = varArcEl.Attribute(namespaces["xlink"] + "type") != null ? varArcEl.Attribute(namespaces["xlink"] + "type").Value : null
                                ,
                                Name = vass.Attribute("name") != null ? vass.Attribute("name").Value : null

                                ,
                                Filters = new List<AssertionFilter>()

                                ,
                                IsGeneral = varEl.Name.LocalName == "generalVariable"
                                ,
                                IsFact = varEl.Name.LocalName == "factVariable"
                                ,
                                BindAsSequence = varEl.Attribute("bindAsSequence") != null ? varEl.Attribute("bindAsSequence").Value == "true" : false,
                                FallbackValue = 0
                                ,
                                ConceptName = varEl.Attribute("select") != null ? varEl.Attribute("select").Value : null
                            };



                            if (varEl.Attribute("fallbackValue") != null)
                            {
                                decimal fv = 0;
                                decimal.TryParse(varEl.Attribute("fallbackValue").Value, out fv);
                                asvar.FallbackValue = fv;
                            }

                            foreach (XElement filterEl in rootEl.XPathSelectElements(string.Format("*[@xlink:from='{0}']", toFilterEl), xnsm))
                            {
                                XElement filterActEl = rootEl.XPathSelectElement(string.Format("*[@xlink:label='{0}']", filterEl.Attribute(namespaces["xlink"] + "to").Value), xnsm);
                                switch (filterActEl.Name.LocalName)
                                {
                                    case "conceptName":
                                        XElement conceptEl = filterActEl.Descendants(namespaces["cf"] + "qname").FirstOrDefault();
                                        if (conceptEl != null)
                                        {
                                            asvar.ConceptName = conceptEl.Value;
                                        }
                                        break;
                                    default:
                                        AssertionFilter assFilt = new AssertionFilter { };
                                        assFilt.isGeneralMeasure= filterActEl.Name.LocalName == "generalMeasures";
                                        assFilt.isPeriod = filterActEl.Name.LocalName == "period";
                                        assFilt.isGeneral = filterActEl.Name.LocalName == "general";
                                        assFilt.isExplicitDimension = filterActEl.Name.LocalName == "explicitDimension";
                                        assFilt.isTypedDimension = filterActEl.Name.LocalName == "typedDimension";
                                        assFilt.isParentFilter = filterActEl.Name.LocalName == "parentFilter";

                                        switch (filterActEl.Name.LocalName)
                                        {
                                            case "explicitDimension":
                                            case "typedDimension":
                                            case "parentFilter":
                                                XElement qnameEl = filterActEl.XPathSelectElement("*[local-name()='qname']", xnsm);
                                                if (qnameEl != null)
                                                {
                                                    assFilt.Test = qnameEl.Value;
                                                }
                                                break;
                                            case "general":
                                            case "generalMeasures":
                                            case "period":
                                               assFilt.Test = filterActEl.Attribute("test").Value;
                                                break;
                                            default:
                                                Console.WriteLine("unknown filter " + filterActEl.Name.LocalName);
                                                assFilt.Test = filterActEl.Name.LocalName;
                                                break;
                                        }
                                        asvar.Filters.Add(assFilt);
                                        
                                        break;
                                }
                            }

                            asert.Variables.Add(toVarEl, asvar);
                        }



                       

                        // }
                    }
                    foreach (XElement labelEl in rootEl.Elements(namespaces["label"] + "label"))
                    {
                        bool isSatisfied = labelEl.Attribute(namespaces["xlink"] + "role").Value == "http://www.xbrl.org/2008/role/label/satisfied";
                        AssertionMessage am = new AssertionMessage
                        {
                            Culture = labelEl.Attribute(XNamespace.Xml + "lang").Value
                            ,
                            Message = labelEl.Value
                            ,
                            XLinkLabel = labelEl.Attribute(namespaces["xlink"] + "label").Value
                        };
                        if (isSatisfied)
                        {
                            asert.SatisfiedMessages.Add(am);
                        }
                        else
                        {
                            asert.NotSatisfiedMessages.Add(am);
                        }
                    }


                    foreach (XElement refEl in rootEl.Elements(namespaces["reference"] + "reference"))
                    {
                        asert.References.Add(refEl.Value);
                    }

                    aset.Assertions.Add(asert);

                }
            }

            reader.Close();
            return aset;

        }
    }
}
