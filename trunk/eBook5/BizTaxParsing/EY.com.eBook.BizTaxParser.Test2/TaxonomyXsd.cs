﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Schema;
using System.Xml;
using System.Collections;

namespace EY.com.eBook.BizTaxParser.Test2
{
   
    public class TaxonomyXsd
    {
        public string Name { get; set; }

        public Dictionary<string, XbrlDataType> DataTypes { get; set; }

        public Dictionary<string, XbrlElementDefinition> ElementDefintions { get; set; }

        

        public List<AnnotionLink> AnnotationLinks { get; set; }

        public List<AnnotionXbrlLink> AnnotionXbrlLinks { get; set; }

        public List<ImportedXsd> Imports { get; set; }

        public Dictionary<string, HyperCube> HyperCubes { get; set; }

        public List<ConceptDefinition> ConceptDefinitions { get; set; }
 
        /*
        public List<string> complexContentTypes = new List<string>();
        public List<string> facets = new List<string>();
        public string Path { get; set; }

        public Dictionary<string, XbrlDataType> DataTypes { get; set; }

        public Dictionary<string, XbrlElementDefinition> ElementDefintions { get; set; }

        public TaxonomyXsd(string path)
        {
            Path = path;
            DataTypes = new Dictionary<string, XbrlDataType>();
            ElementDefintions = new Dictionary<string, XbrlElementDefinition>();
        }



        public static TaxonomyXsd Read(string path)
        {
            TaxonomyXsd ts = new TaxonomyXsd();
            ts.ReadFile(path);
            return ts;
        }

        public void ReadExplicit()
        {

        }

        public void ReadFile(string path)
        {
            DataTypes = new Dictionary<string, XbrlDataType>();
            ElementDefintions = new Dictionary<string, XbrlElementDefinition>();
            Console.WriteLine(path);
            XmlTextReader reader = new XmlTextReader(path);
            //reader.XmlResolver = new LocalFolderResolver { Folder = System.IO.Path.GetDirectoryName(path) }; 
            XmlSchema lschema = XmlSchema.Read(reader, schemaSet_ValidationEventHandler);
            string targetns = lschema.TargetNamespace;
            lschema = null;
            reader.Close();
            reader = null;
           // Console.WriteLine(lschema.TargetNamespace);
            XmlSchemaSet set = new XmlSchemaSet();
            set.XmlResolver = new LocalFolderResolver { Folder = System.IO.Path.GetDirectoryName(path) };
            set.ValidationEventHandler += new ValidationEventHandler(schemaSet_ValidationEventHandler);
            set.Add(targetns, path);

            List<string> reloads = new List<string>();
            List<XmlSchema> removeSchemas = new List<XmlSchema>();
            foreach (XmlSchema sch in set.Schemas())
            {
                string id = sch.SourceUri + "#" + sch.TargetNamespace;
                if (!reloads.Contains(id))
                {
                    Uri ur = new Uri(sch.SourceUri);

                    int cnt = 0;
                    foreach (XmlSchema schSub in set.Schemas(sch.TargetNamespace))
                    {

                        cnt++;
                    }
                    if (cnt > 1)
                    {
                        reloads.Add(id);
                        removeSchemas.Add(sch);

                    }
                }
            }
            foreach (XmlSchema sch in removeSchemas)
            {
                set.Remove(sch);
            }
            foreach (string schid in reloads)
            {
                string[] splitted = schid.Split(new char[] { '#' });
                string tns = splitted[1];
                string fle = splitted[0];
                if (set.Schemas(tns).Count == 0)
                {
                    set.Add(tns, fle);
                }
            }

        //    set.CompilationSettings.EnableUpaCheck = false;
            set.Compile();
            
           // set.
            //XmlSchema schema = null;
            //foreach (XmlSchema sc in set.Schemas())
            //{
            //    if (sc.SourceUri == lschema.SourceUri)
            //    {
            //        schema = sc;
            //        break;
            //    }
            //}

           // XmlTextReader reader = new XmlTextReader(path);
           // reader.XmlResolver = new LocalFolderResolver { Folder = System.IO.Path.GetDirectoryName(path) };
           // XmlSchema schema = XmlSchema.Read(reader, schemaSet_ValidationEventHandler);
           //// schema.xml
           // schema.Compile(schemaSet_ValidationEventHandler);


            

            foreach (XmlSchemaType xstype in set.GlobalTypes.Values)
            {
                if (xstype.Name != null)
                {
                    Console.WriteLine(xstype.Name);

                    switch (xstype.GetType().ToString())
                    {
                        case "System.Xml.Schema.XmlSchemaComplexType":
                            ReadComplexType(xstype as XmlSchemaComplexType);
                            break;
                        case "System.Xml.Schema.XmlSchemaSimpleType":
                            ReadSimpleType(xstype as XmlSchemaSimpleType);
                            break;
                        default:
                            Console.WriteLine("UNKNOWN   > " + xstype.GetType().ToString());
                            break;
                    }
                    Console.WriteLine("   > " + xstype.GetType().ToString());
                }
                else
                {
                    Console.WriteLine("   NO NAME > " + xstype.GetType().ToString());
                }

            }

            
            
                foreach (XmlSchemaElement element in set.GlobalElements.Values)
                {
                    Uri uri = new Uri(element.SourceUri);
                    string origin = uri.Segments[uri.Segments.Length-1];
                    string href = string.Format("{0}#{1}", origin,element.Name);
                    if (!ElementDefintions.ContainsKey(href) && !DataTypes.ContainsKey(element.Name))
                    {
                        XbrlElementDefinition xed = new XbrlElementDefinition {
                            Href= href
                            , Name = element.Name
                            , Id = element.Id
                            , Abstract = element.IsAbstract
                            , Nillable = element.IsNillable
                            , MaxOccurs = element.MaxOccursString
                            , MinOccurs = element.MinOccursString
                            , SubstitutionGroup = element.SubstitutionGroup.ToString()
                            , Type = element.SchemaTypeName.ToString()
                            , Origin= origin
                        };
                       
                        //if (element.ElementSchemaType != null)
                        //{
                        //    XmlSchemaComplexType complexType = element.ElementSchemaType as XmlSchemaComplexType;
                        //    XmlSchemaSimpleType simpleType = element.ElementSchemaType as XmlSchemaSimpleType;
                        //    if (complexType = null)
                        //    {
                        //        xed.Type = 
                        //    }
                        //}
                        ElementDefintions.Add(href, xed);
                        
                    }
                }
                    
                        //Console.WriteLine(element.SourceUri);
                        //Console.WriteLine("Element: {0}", element.Name);
                        //if (element.ElementSchemaType != null)
                        //{
                        //    if (element.ElementSchemaType.Datatype != null)
                        //    {
                        //        Console.WriteLine("  > Type: {0}", element.ElementSchemaType.Datatype);
                        //    }
                        //}
                        //XmlSchemaComplexType complexType = element.ElementSchemaType as XmlSchemaComplexType;
                        //if (complexType == null)
                        //{
                        //    XmlSchemaSimpleType simpleType = element.ElementSchemaType as XmlSchemaSimpleType;
                        //    if (simpleType == null)
                        //    {
                        //        Console.WriteLine("... NO TYPE DEF");

                        //    }
                        //    else
                        //    {
                        //        Console.WriteLine("... simple");
                        //        ReadSimpleType(simpleType);

                        //    }
                        //}
                        //else
                        //{
                        //    ReadComplexType(complexType);
                        //    Console.WriteLine("... complex");
                        //}
                    
               // }

                Console.WriteLine("");
                Console.WriteLine("complex content types");
                foreach (string tpe in complexContentTypes)
                {
                    Console.WriteLine(tpe);
                }

           // return new TaxonomyXsd();
            //XmlSchemaElement xse = sx.Elements[new XmlQualifiedName("")] as XmlSchemaElement
        }

       

        private void ReadSimpleType(XmlSchemaSimpleType simpleType)
        {
            if (!DataTypes.ContainsKey(simpleType.Name))
            {
                Uri sourceXsdUri = new Uri(simpleType.SourceUri);
                string sourceXsd = sourceXsdUri.Segments[sourceXsdUri.Segments.Length-1];
                
                XbrlDataType dt = new XbrlDataType { Origin = sourceXsd, Name = simpleType.Name, Restrictions = new List<Restriction>(), ValueType = simpleType.Datatype.ValueType.ToString(), BaseType = simpleType.BaseXmlSchemaType != null ? simpleType.BaseXmlSchemaType.Name : null };
                
                XmlSchemaSimpleTypeRestriction restriction = simpleType.Content as XmlSchemaSimpleTypeRestriction;

                if (restriction != null)
                {

                    Console.WriteLine("restriction : {0}", restriction.BaseTypeName.Name);

                    foreach (XmlSchemaObject facet in restriction.Facets)
                    {
                       Restriction restrict = new Restriction { Name=facet.GetType().ToString()};
                        
                        if (!facets.Contains(facet.GetType().ToString())) facets.Add(facet.GetType().ToString());
                        Console.WriteLine("  > {0}", facet.ToString());
                        if (facet is XmlSchemaFacet)
                        {
                            restrict.Value = ((XmlSchemaFacet)facet).Value;
                        }
                        if (facet is XmlSchemaEnumerationFacet)
                            restrict.Value = ((XmlSchemaEnumerationFacet)facet).Value;

                        dt.Restrictions.Add(restrict);
                    }
                }
                DataTypes.Add(dt.Name, dt);
            }
        }

        private void ReadSimpleContent(XmlSchemaSimpleContent cnt, ref XbrlDataType dt)
        {
            XmlSchemaSimpleContentRestriction restriction = cnt.Content as XmlSchemaSimpleContentRestriction;

            if (restriction != null)
            {

                Console.WriteLine("restriction : {0}", restriction.BaseTypeName.Name);

                foreach (XmlSchemaObject facet in restriction.Facets)
                {
                    Restriction restrict = new Restriction { Name = facet.GetType().ToString() };

                    if (!facets.Contains(facet.GetType().ToString())) facets.Add(facet.GetType().ToString());
                    Console.WriteLine("  > {0}", facet.ToString());
                    if (facet is XmlSchemaFacet)
                    {
                        restrict.Value = ((XmlSchemaFacet)facet).Value;
                    }
                    if (facet is XmlSchemaEnumerationFacet)
                        restrict.Value = ((XmlSchemaEnumerationFacet)facet).Value;

                    dt.Restrictions.Add(restrict);
                }
            }

            XmlSchemaSimpleContentExtension extension = cnt.Content as XmlSchemaSimpleContentExtension;

            if (extension != null)
            {

                Console.WriteLine("extension : {0}", extension.BaseTypeName);

                //foreach (XmlSchemaObject facet in extension.Facets)
                //{
                //    Console.WriteLine("  > {0}", facet.ToString());
                //    if (facet is XmlSchemaFacet)
                //        Console.WriteLine("  > {0}", ((XmlSchemaFacet)facet).Value);
                //    if (facet is XmlSchemaEnumerationFacet)

                //        Console.WriteLine("Element: {0}", ((XmlSchemaEnumerationFacet)facet).Value);
                //}
            }
        }

        private string GetComplexValueType(XmlSchemaType xstype)
        {
            if (xstype.Datatype != null)
            {
                return xstype.Datatype.ValueType.ToString();
            }
            else if (xstype.BaseXmlSchemaType != null)
            {
                return GetComplexValueType(xstype.BaseXmlSchemaType);
            }
            else
            {
                return typeof(string).ToString();
            }
        }

        private void ReadComplexType(XmlSchemaComplexType complexType)
        {
            if (!DataTypes.ContainsKey(complexType.Name))
            {

                XbrlDataType dt = new XbrlDataType { Origin = complexType.SourceUri, Name = complexType.Name, Restrictions = new List<Restriction>(), ValueType = complexType.Datatype != null ? complexType.Datatype.ValueType.ToString() : GetComplexValueType( complexType.BaseXmlSchemaType), BaseType = complexType.BaseXmlSchemaType != null ? complexType.BaseXmlSchemaType.Name : null };

                if (complexType.AttributeUses.Count > 0)
                {
                    IDictionaryEnumerator enumerator =
                        complexType.AttributeUses.GetEnumerator();

                    while (enumerator.MoveNext())
                    {
                        XmlSchemaAttribute attribute =
                            (XmlSchemaAttribute)enumerator.Value;

                        Console.WriteLine("Attribute: {0}", attribute.QualifiedName);
                    }
                }

                //complexType.ContentModel
                //Console.WriteLine(complexType.ContentType.ToString());

                //XmlSchemaSimpleContent simpleContent = complexType.ContentTypeParticle as XmlSchemaSimpleContent
                if (complexType.ContentModel != null)
                {
                    string cotype = complexType.ContentModel.GetType().ToString();
                    if (!complexContentTypes.Contains(cotype)) complexContentTypes.Add(cotype);

                    XmlSchemaSimpleContent simpleContent = complexType.ContentModel as XmlSchemaSimpleContent;
                    if (simpleContent != null)
                    {

                        ReadSimpleContent(simpleContent, ref dt);
                    }
                }
                else
                {
                    Console.WriteLine("no content");
                }

                // Get the sequence particle of the complex type.
                //XmlSchemaSequence sequence = complexType.ContentTypeParticle as XmlSchemaSequence;

                //// Iterate over each XmlSchemaElement in the Items collection.
                //foreach (XmlSchemaElement childElement in sequence.Items)
                //{
                //    Console.WriteLine("Element: {0}", childElement.Name);
                //}
                DataTypes.Add(dt.Name, dt);
            }
        }

        public void schemaSet_ValidationEventHandler(object sender, ValidationEventArgs e)
        {
            if (e.Exception != null)
                Console.WriteLine(e.Message);
            // perform no validation handling at this point
            //throw new NotImplementedException();
        }
         */
    }
}
