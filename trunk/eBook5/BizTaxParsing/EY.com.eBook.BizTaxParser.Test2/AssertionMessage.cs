﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EY.com.eBook.BizTaxParser.Test2
{
    public class AssertionMessage
    {
        public string Culture { get; set; } // 2 char based language

        public string XLinkLabel { get; set; }

        public string Message { get; set; }
    }
}
