﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EY.com.eBook.BizTaxParser.Test2
{
    public class ContextDimension
    {
        public DimensionType Type { get; set; }
        public string Dimension { get; set; }
        public string Value { get; set; }
        public string TypeMember { get; set; }
        public XbrlElementDefinition TypeDef { get; set; }
    }
}
