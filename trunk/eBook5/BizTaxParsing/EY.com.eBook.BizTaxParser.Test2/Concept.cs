﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EY.com.eBook.BizTaxParser.Test2
{
    public enum HyperCubeConceptRole
    {
        All,
        NotAll
    }

    public class Concept
    {
        public string Id { get; set; }

        public string ContextElement { get; set; }

        public HyperCubeConceptRole Role { get; set; } // All or NotAll

        public XbrlElementDefinition ElementDefinition { get; set; }

        public List<XbrlElementDefinition> SubElements { get; set; }

        //public List<Dimensions> Dimensions { get; set; }
    }

}
