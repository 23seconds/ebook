﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.BizTaxParser.Test2
{
    [Serializable]
    [DataContract]
    public class ElementLabel
    {
        [DataMember]
        public string Id { get; set; }
        [DataMember]
        public string ElementHref { get; set; }
        [DataMember]
        public string Role { get; set; }
        [DataMember]
        public List<Translation> Descriptions { get; set; }

    }
}
