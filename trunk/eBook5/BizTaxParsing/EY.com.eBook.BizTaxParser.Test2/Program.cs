﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Runtime.Serialization;
using System.Xml;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace EY.com.eBook.BizTaxParser.Test2
{
    class Program
    {
        static void Main(string[] args)
        {
            DateTime dte = DateTime.Now;
            string rootDir = @"C:\Projects_TFS\Data\BizTax\be-tax-2013-04-30\dts\";

            // START TEST ASSERTIONS
            string[] assfiles = System.IO.Directory.GetFiles(rootDir, "*assertion.xml");
            List<AssertionSet> sets = new List<AssertionSet>();
            Console.WriteLine("Traversing assertions (to move in taxonomyreader)");
            foreach (string assFile in assfiles)
            {
                sets.Add(AssertionSet.ReadXmlFile(assFile));
            }
            SaveToXml(sets, true, "assertions.xml");
            Console.WriteLine("End Traversing assertions");
            // END TEST ASSERTIONS
            // GET ALL SEPERATE BIZTAX TAXONOMIES IN THIS SCHEMA

            Parameters prs = null;
            //string[] files = System.IO.Directory.GetFiles(@"C:\Projects_TFS\Data\BizTax\2011-04-30-v1.1\2011-04-30\DTS\", "*parameters*.xml");
            string[] files = System.IO.Directory.GetFiles(rootDir, "*parameters*.xml");
            foreach (string paramFile in files)
            {
                Console.WriteLine("Paramfile: " + Path.GetFileNameWithoutExtension(paramFile));
                prs = new Parameters(paramFile);
                foreach(TaxonomyRoot tr in prs.TaxonomyRoots) {

                    Console.WriteLine("- {0} => {1}", tr.TaxonomyId, tr.RootXsd);
                }

            }

            // NEXT GET XSDs

            BizTaxTaxonomy btt = new BizTaxTaxonomy();
            btt.ReadTaxonomyFolder(rootDir);

            foreach (TaxonomyXsd xsd in btt.Xsds.Values)
            {
                SaveToXml(xsd, true, xsd.Name + ".xml");
            }

            SaveToXml(btt.DataTypes.Values.ToList(), true,  "GlobalDataTypes.xml");
            SaveToXml(btt.Elements.Values.ToList(), true, "GlobalElements.xml");
            TimeSpan ts = (DateTime.Now - dte);
            Console.WriteLine("{0} hour(s), {1} minute(s), {2} seconds , {3} milliseconds", ts.Hours, ts.Minutes, ts.Seconds, ts.Milliseconds);
            Console.ReadLine();
        }

        static void MainOld(string[] args)
        {
           // DateTime dte = DateTime.Now;
           // List<string> facets = new List<string>();
           // Dictionary<string, XbrlDataType> datatypes = new Dictionary<string, XbrlDataType>();
           // Dictionary<string, XbrlElementDefinition> elements = new Dictionary<string, XbrlElementDefinition>();
           // //TaxonomyXsd t = TaxonomyXsd.Read(@"C:\Projects_TFS\Data\BizTax\2012-04-30\be-tax-2012-04-30\DTS\be-tax-inc-rcorp-2012-04-30.xsd");
           // //datatypes = t.DataTypes;
           // string rootDir = @"C:\Projects_TFS\Data\BizTax\2012-04-30\be-tax-2012-04-30\DTS\";
           // string[] files = System.IO.Directory.GetFiles(rootDir, "*.xsd");
           // foreach (string fle in files)
           // {
           //     TaxonomyXsd tx = TaxonomyXsd.Read(fle);
           //     int len = datatypes.Count;
           //     foreach (string key in tx.DataTypes.Keys)
           //     {
           //         if (!datatypes.ContainsKey(key)) datatypes.Add(key, tx.DataTypes[key]);
           //     }
           //     foreach (string key in tx.ElementDefintions.Keys)
           //     {
           //         if (!elements.ContainsKey(key)) elements.Add(key, tx.ElementDefintions[key]);
           //     }
           // }
           //// datatypes = datatypes.Distinct().ToList();
           // TimeSpan ts = (DateTime.Now - dte);
           // Console.WriteLine("{0} hour(s), {1} minute(s), {2} seconds , {3} milliseconds", ts.Hours, ts.Minutes, ts.Seconds, ts.Milliseconds);
           // Console.ReadLine();
           // SerializeToXElement(datatypes.Values.ToList(), false).Save("datatypes.xml");
           // SerializeToXElement(elements.Values.ToList(), false).Save("elements.xml");
            
           // Console.ReadLine();
        }

        public static void SaveToXml(object instance, bool removeNs,string fname)
        {
            string result = null;
            DataContractSerializer dcs = new DataContractSerializer(instance.GetType());
            var sw = new System.IO.StringWriter();
            var xw = new XmlTextWriter(sw);
            dcs.WriteObject(xw, instance);
            xw.Close();
            result = sw.ToString();
            sw.Close();
            XElement xe = XElement.Parse(result);
            if (removeNs) xe = xe.StripNamespaces();
            xe.Save(fname);
        }

        public static void SerializeObject<T>(string filename, T objectToSerialize)
        {
            Stream stream = File.Open(filename, FileMode.Create);
            BinaryFormatter bFormatter = new BinaryFormatter();
            bFormatter.Serialize(stream, objectToSerialize);
            stream.Close();
        }

        public static T DeSerializeObject<T>(string filename)
        {
            T objectToSerialize;
            Stream stream = File.Open(filename, FileMode.Open);
            BinaryFormatter bFormatter = new BinaryFormatter();
            objectToSerialize = (T)bFormatter.Deserialize(stream);
            stream.Close();
            return objectToSerialize;
        } 

        public static XElement ToXml(object instance, bool removeNs, string fname)
        {
            string result = null;
            DataContractSerializer dcs = new DataContractSerializer(instance.GetType());
            var sw = new System.IO.StringWriter();
            var xw = new XmlTextWriter(sw);
            dcs.WriteObject(xw, instance);
            xw.Close();
            result = sw.ToString();
            sw.Close();
            XElement xe = XElement.Parse(result);
            if (removeNs) xe = xe.StripNamespaces();
            return xe;
        }

        
    }

    public static class Exts
    {
        public static XElement StripNamespaces(this XElement root)
        {
            XElement res = new XElement(
                root.Name.LocalName,
                root.HasElements ?
                    root.Elements().Select(el => el.StripNamespaces()) :
                    (object)root.Value
            );

            res.ReplaceAttributes(
                root.Attributes().Where(attr => (!attr.IsNamespaceDeclaration)));

            return res;
        }
    }
}
