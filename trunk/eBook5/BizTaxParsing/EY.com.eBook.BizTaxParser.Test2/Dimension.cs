﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.BizTaxParser.Test2
{
    [Serializable]
    [DataContract]
    public enum DimensionType {
        [EnumMember]
        TypedDimension
        
        , [EnumMember]
        ExplicitDimension
    }

    [Serializable]
    [DataContract]
    public class Dimension
    {
        [DataMember]
        public string Id { get; set; }
        [DataMember]
        public XbrlElementDefinition ElementDefinition { get; set; }
        [DataMember]
        public DimensionType Type { get; set; }
        [DataMember]
        public List<DimensionDomain> Domains { get; set; }
        [DataMember]
        public XbrlElementDefinition TypeElement { get; set; }

    }
}
