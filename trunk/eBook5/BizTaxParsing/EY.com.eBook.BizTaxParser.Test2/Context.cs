﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.BizTaxParser.Test2
{
    [Serializable]
    [DataContract]
    public class Context
    {
        [DataMember]
        public string RoleId { get; set; } // definitionLink
        [DataMember]
        public string PrimaryConcept { get; set; }
        [DataMember]
        public List<HyperCubeLink> HyperCubes { get; set; }
    }
}
