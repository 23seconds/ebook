﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.BizTaxParser.Test2
{
    [Serializable]
    [DataContract]
    public class ContextDefinition
    {
        [DataMember]
        public string Role { get; set; }

        [DataMember]
        public List<string> Periods { get; set; }

        [DataMember]
        public List<FieldDefinition> Fields { get; set; }

    }

    [Serializable]
    [DataContract]
    public class FieldDefinition
    {
        // List (explicit) or field (typed)

        [DataMember]
        public DimensionType DimensionType { get; set; }

        [DataMember]
        public bool Fixed { get; set; }

        [DataMember]
        public string Dimension { get; set; }

        //public string Domain{ get; set; }

        [DataMember]
        public List<FieldListItem> ListData { get; set; }

        [DataMember]
        public XbrlElementDefinition TypeElement { get; set; }

        [DataMember]
        public XbrlDataType DataType { get; set; }
    }

    [Serializable]
    [DataContract]
    public class FieldListItem
    {
        [DataMember]
        public string Domain { get; set; }

        [DataMember]
        public XbrlElementDefinition ElementDef { get; set; }

        [DataMember]
        public string DomainMember { get; set; }
    }
}
