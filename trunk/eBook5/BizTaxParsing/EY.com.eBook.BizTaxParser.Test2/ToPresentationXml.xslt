﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl"
>
    <xsl:output method="xml" indent="yes"/>

  <xsl:param name="assess">2012</xsl:param>
  <xsl:param name="culture">nl</xsl:param>
  <xsl:param name="btype">rcorp</xsl:param>
  
  <xsl:variable name="smallcase" select="'abcdefghijklmnopqrstuvwxyz'" />
  <xsl:variable name="uppercase" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZ'" />
  <xsl:template match="root">
    
    <biztax assessment="{$assess}" type="{$btype}" lang="{$culture}">
      <tabs>

        <xsl:for-each select="Tabs/*">
          <xsl:apply-templates select="." mode="TAB"/>
        </xsl:for-each>
      </tabs>
    </biztax>
  </xsl:template>

  <xsl:template match="Element" mode="DEFAULT_ATTRIBS">
    <xsl:if test="starts-with(@id,'tax-inc')">
      <xsl:attribute name="code">
        <xsl:value-of select="labels/label[@type='documentation']/translation[@lang='en']"/>
      </xsl:attribute>
      <xsl:attribute name="oldCode">
        <xsl:value-of select="labels/label[@type='documentation']/translation[@lang='en-US']"/>
      </xsl:attribute>
    </xsl:if>
    <xsl:attribute name="xbrlId">
      <xsl:value-of select="@id"/>
    </xsl:attribute>
    <xsl:attribute name="xbrlName">
      <xsl:value-of select="@name"/>
    </xsl:attribute>
    <xsl:attribute name="itemType">
      <xsl:value-of select="@type"/>
    </xsl:attribute>
    <xsl:attribute name="concept">
      <xsl:choose>
        <xsl:when test="not(@primaryConcept='d-hh_AdimensionalPrimaryConcepts' or @primaryConcept='d-hh_AdimensionalPrimaryConcepts')">
          <xsl:value-of select="@primaryConcept"/>
        </xsl:when>
      </xsl:choose>
    </xsl:attribute>
    <xsl:attribute name="isAbstract">
      <xsl:choose>
        <xsl:when test="@abstract">
          <xsl:value-of select="translate(@abstract, $uppercase, $smallcase)"/>
        </xsl:when>
        <xsl:otherwise>false</xsl:otherwise>
      </xsl:choose>
    </xsl:attribute>
  </xsl:template>
  <!--@ContextFile = $ContextFile and -->
  <xsl:template match="Element" mode="CONTEXT">
    <xsl:variable name="href" select="@href"/>
    <xsl:variable name="ContextFile" select="@contextFile"/>
    <xsl:variable name="concepts" select="//Concept[count(children/child[@id=$href])>0]"/>
    <!-- TEMPORARY FIX !!-->
    <!--xsl:if test="not(./parent::Element[@id='tax-inc_DisallowedExpensesSection'])"-->

    <xsl:choose>
      <xsl:when test="count($concepts)>1">
        <xsl:for-each select="$concepts[@ContextFile = $ContextFile]">
          <xsl:if test="dimensions/*[1]">
          <xsl:copy-of select="."/>
          </xsl:if>
        </xsl:for-each>
      </xsl:when>
      <xsl:otherwise>
        <xsl:for-each select="$concepts">
          <xsl:if test="dimensions/*[1]">
            <xsl:copy-of select="."/>
          </xsl:if>
        </xsl:for-each>
      </xsl:otherwise>
    </xsl:choose>
    <!--/xsl:if-->
  </xsl:template>
  
  <xsl:template match="Element" mode="TAB">
    <xsl:variable name="title"><xsl:apply-templates select="." mode="GetLabel"/></xsl:variable>
    <tab id="{@id}" title="{$title}">
      <xsl:apply-templates select="." mode="DEFAULT_ATTRIBS"/>
      <xsl:apply-templates select="." mode="CONTEXT"/>
      <items>
        <xsl:apply-templates select="Element[not(contains(string(@id),'XCode'))]" mode="PANE"/>
      </items>
    </tab>
  </xsl:template>

  <xsl:template match="Element" mode="PANE">
    <pane>
      <xsl:attribute name="title"><xsl:apply-templates select="." mode="GetLabel"/></xsl:attribute>
      <xsl:apply-templates select="." mode="DEFAULT_ATTRIBS"/>
      <xsl:attribute name="hasInstantPeriods">
        <xsl:value-of select="not(not(descendant::*[@periodType='instant']))"/>
      </xsl:attribute>
      <xsl:attribute name="hasDurationPeriods">
        <xsl:value-of select="not(not(descendant::*[@periodType='duration']))"/>
      </xsl:attribute>
      <xsl:apply-templates select="." mode="CONTEXT"/>
      <items>
        <xsl:for-each select="Element">
          <xsl:apply-templates select="." mode="EITHER_WAY"/>
        </xsl:for-each>
      </items>
    </pane>
  </xsl:template>

  <xsl:template match="Element" mode="LISTITEM">
    <valueList>
      <xsl:attribute name="title"><xsl:apply-templates select="." mode="GetLabel"/></xsl:attribute>
      <xsl:attribute name="substitutionGroup"><xsl:value-of select="@substitutionGroup"/></xsl:attribute>
      <xsl:attribute name="periodType"><xsl:apply-templates select="." mode="GETPERIODTYPE"/></xsl:attribute>
      <xsl:apply-templates select="." mode="DEFAULT_ATTRIBS"/>
      <xsl:apply-templates select="." mode="CONTEXT"/>
      <data>
        <xsl:attribute name="substitutionGroup"><xsl:value-of select="@substitutionGroup"/></xsl:attribute>
        <xsl:apply-templates select="." mode="LISTITEM_DATA"/>
      </data>
    </valueList>
  </xsl:template>
  

  <xsl:template match="Element" mode="LISTITEM_DATA">
    <xsl:for-each select="Element">
      <item id="{@id}">
        <xsl:attribute name="text"><xsl:apply-templates select="." mode="GetLabel"/></xsl:attribute>
      </item>
    </xsl:for-each>
  </xsl:template>

  <xsl:template match="Element" mode="FIELD">
    <field>
      <xsl:attribute name="title"><xsl:apply-templates select="." mode="GetLabel"/></xsl:attribute>
      <xsl:attribute name="totalField"><xsl:value-of select="not(not(labels/label[@type='totalLabel']))"/> </xsl:attribute>
      <xsl:attribute name="periodType"><xsl:apply-templates select="." mode="GETPERIODTYPE"/></xsl:attribute>
      <xsl:apply-templates select="." mode="DEFAULT_ATTRIBS"/>
      <xsl:apply-templates select="." mode="CONTEXT"/>
    </field>
  </xsl:template>

  <xsl:template match="Element" mode="UPLOADFIELD">
    <uploadField>
      <xsl:attribute name="title"><xsl:apply-templates select="." mode="GetLabel"/></xsl:attribute>
      <xsl:attribute name="totalField"><xsl:value-of select="not(not(labels/label[@type='totalLabel']))"/> </xsl:attribute>
      <xsl:attribute name="periodType"><xsl:apply-templates select="." mode="GETPERIODTYPE"/></xsl:attribute>
      <xsl:attribute name="required"><xsl:value-of select="@type='http://www.nbb.be/be/fr/pfs/ci/dt/2012-04-01:nonEmptyBase64BinaryItemType'"/></xsl:attribute>
      <xsl:apply-templates select="." mode="DEFAULT_ATTRIBS"/>
      <xsl:apply-templates select="." mode="CONTEXT"/>
    </uploadField>
  </xsl:template>

  <xsl:template match="Element" mode="EITHER_WAY">
    <xsl:variable name="id" select="@id"/>
    <xsl:choose>
      <xsl:when test="Element and not(Element[not('CodeHead'=substring(@id, string-length(@id)- string-length('CodeHead') +1) or 'Other'=substring(@id, string-length(@id)- string-length('Other') +1))])">
        <!--<xsl:apply-templates select="." mode="VALUEPANE"/> -->
        <valuePane>
          <xsl:attribute name="title"><xsl:apply-templates select="." mode="GetLabel"/></xsl:attribute>
          <xsl:apply-templates select="." mode="DEFAULT_ATTRIBS"/>
          <xsl:apply-templates select="." mode="CONTEXT"/>
          <items>
            <xsl:apply-templates select="Element" mode="EITHER_WAY"/>
          </items>
        </valuePane>
      </xsl:when>
      <xsl:when test="count(Element[contains(string(@id),'XCode')])>0">
        <xsl:apply-templates select="." mode="LISTITEM"/>
      </xsl:when>
      <xsl:when test="count(Element)>0">
        <xsl:apply-templates select="." mode="PANE"/>
      </xsl:when>
      <xsl:when test="@type='http://www.xbrl.org/2003/instance:base64BinaryItemType' or @type='http://www.nbb.be/be/fr/pfs/ci/dt/2012-04-01:nonEmptyBase64BinaryItemType'">
        <xsl:apply-templates select="." mode="UPLOADFIELD"/>
      </xsl:when>
      <xsl:when test="translate(@abstract, $uppercase, $smallcase)='true'">
        <label>
           <xsl:attribute name="title"><xsl:apply-templates select="." mode="GetLabel"/></xsl:attribute>
          <xsl:apply-templates select="." mode="DEFAULT_ATTRIBS"/>
        </label>
      </xsl:when>
      <xsl:otherwise>
        <xsl:apply-templates select="." mode="FIELD"/>
      </xsl:otherwise>
      <!--
      <xsl:when test="count(Element[contains(string(@id),'XCode')])>0">
        <xsl:apply-templates select="." mode="LISTITEM"/>
      </xsl:when>
      <xsl:when test="@type='http://www.xbrl.org/2003/instance:base64BinaryItemType' or @type='http://www.nbb.be/be/fr/pfs/ci/dt/2012-04-01:nonEmptyBase64BinaryItemType'">
        <xsl:apply-templates select="." mode="UPLOADFIELD"/>
      </xsl:when>
      <xsl:when test="count(Element)>0 and count(Element[not('CodeHead'=substring(@id, string-length(@id)- string-length('CodeHead') +1) or 'Other'=substring(@id, string-length(@id)- string-length('Other') +1))]) =0">
        
        // VALUE PANE
        <xsl:apply-templates select="Element[contains(@id,'CodeHead')]" mode="EITHER_WAY"/>
      </xsl:when>
      <xsl:when test="count(Element)>0">
        <xsl:apply-templates select="." mode="PANE"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:apply-templates select="." mode="FIELD"/>
      </xsl:otherwise>
      //-->
    </xsl:choose>
  </xsl:template>

  <xsl:template match="Element" mode="GETPERIODTYPE">
    <xsl:choose>
      <xsl:when test="@periodType='duration'">
        <xsl:choose>
          <xsl:when test="labels/label[@type='periodStartLabel'] and labels/label[@type='periodEndLabel']">instant</xsl:when>
          <xsl:when test="labels/label[@type='periodStartLabel']">instant-start</xsl:when>
          <xsl:when test="labels/label[@type='periodEndLabel']">instant-end</xsl:when>
          <xsl:otherwise>duration</xsl:otherwise>
        </xsl:choose>
      </xsl:when>
      <xsl:otherwise>
        <xsl:choose>
          <xsl:when test="labels/label[@type='periodStartLabel'] and labels/label[@type='periodEndLabel']">instant</xsl:when>
          <xsl:when test="labels/label[@type='periodStartLabel']">instant-start</xsl:when>
          <xsl:when test="labels/label[@type='periodEndLabel']">instant-end</xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="@periodType"/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  
  <xsl:template match="Element" mode="GetLabel"><xsl:variable name="apos">'</xsl:variable><xsl:variable name="aposEsc">-</xsl:variable><xsl:variable name="txt"><xsl:call-template name="string-replace-all"><xsl:with-param name="text" select="string(labels/label[@type='label']/translation[@lang=$culture])" /><xsl:with-param name="replace" select="$apos" /><xsl:with-param name="by" select="$aposEsc" /></xsl:call-template></xsl:variable><xsl:value-of select="translate($txt,$apos,$aposEsc)"/></xsl:template>

  <xsl:template name="string-replace-all">
    <xsl:param name="text" />
    <xsl:param name="replace" />
    <xsl:param name="by" />
    <xsl:choose>
      <xsl:when test="contains($text, $replace)">
        <xsl:value-of select="substring-before($text,$replace)" />
        <xsl:value-of select="$by" />
        <xsl:call-template name="string-replace-all">
          <xsl:with-param name="text"
          select="substring-after($text,$replace)" />
          <xsl:with-param name="replace" select="$replace" />
          <xsl:with-param name="by" select="$by" />
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$text" />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
</xsl:stylesheet>
