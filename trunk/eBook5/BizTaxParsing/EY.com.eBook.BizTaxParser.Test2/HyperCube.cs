﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.BizTaxParser.Test2
{
    [Serializable]
    [DataContract]
    public class HyperCube
    {
        /*
        public string Id { get; set; }

        public XbrlElementDefinition ElementDefinition { get; set; }

        public List<ConceptDimension> ConceptDimensions { get; set; } // = definitionLink
        */

        [DataMember]
        public string RoleUri { get; set; }
        [DataMember]
        public string Href { get; set; }
        [DataMember]
        public string ContextFile { get; set; }

        [DataMember]
        public List<Dimension> Dimensions { get; set; }
        
    }
    [Serializable]
    [DataContract]
    public enum HyperCubeArcRoles {
        [EnumMember]
        All,
        [EnumMember]
        NotAll
    }

    [Serializable]
    [DataContract]
    public class HyperCubeLink
    {
        [DataMember]
        public string HyperCubeHref { get; set; }
        [DataMember]
        public string TargetRole { get; set; }
        [DataMember]
        public HyperCubeArcRoles ArcRole { get; set; }
        [DataMember]
        public string ContextElement { get; set; }
        [DataMember]
        public bool Closed { get; set; }
    }
}
