﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.BizTaxParser.Test2
{
    [Serializable]
    [DataContract]
    public class Translation
    {
        [DataMember]
        public string Language { get; set; }
        [DataMember]
        public string Description { get; set; }
    }
}
