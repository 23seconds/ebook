﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EY.com.eBook.BizTaxParser.Test2
{
    public class AssertionVariable
    {
        public bool IsGeneral { get; set; }

        public bool IsFact { get; set; }

        public bool BindAsSequence { get; set; }

        public string Type { get; set; } // arc

        public string ArcRole { get; set; }

        public string Name { get; set; }

        public string ConceptName { get; set; }

        public decimal FallbackValue { get; set; }

        public List<AssertionFilter> Filters { get; set; }

        

    }
}
