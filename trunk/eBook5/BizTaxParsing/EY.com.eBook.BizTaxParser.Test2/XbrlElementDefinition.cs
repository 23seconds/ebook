﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.BizTaxParser.Test2
{
    [Serializable]
    [DataContract]
    public class XbrlElementDefinition
    {
        public XbrlElementDefinition()
        {
            Contexts = new List<Context>();
        }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Href { get; set; }
        [DataMember]
        public string Origin { get; set; }
        [DataMember]
        public string Id { get; set; }
        [DataMember]
        public string Type { get; set; }
        [DataMember]
        public string SubstitutionGroup { get; set; }
        [DataMember]
        public string PeriodType { get; set; }
        [DataMember]
        public bool Abstract { get; set; }
        [DataMember]
        public bool Nillable { get; set; }

        [DataMember]
        public string TypedDomainRef { get; set; }
        [DataMember]
        public string Balance { get; set; }
        [DataMember]
        public string Ref { get; set; }
        [DataMember]
        public string MinOccurs { get; set; }
        [DataMember]
        public string MaxOccurs { get; set; }
        [DataMember]
        public string Fixed { get; set; }

        [DataMember]
        public List<ElementLabel> Labels { get; set; }

        [DataMember]
        public List<Context> Contexts { get; set; }

        [DataMember]
        public List<ContextDefinition> ContextDefinitions { get; set; }
    }
}
