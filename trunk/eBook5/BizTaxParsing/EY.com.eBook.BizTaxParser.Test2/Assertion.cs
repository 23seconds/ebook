﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EY.com.eBook.BizTaxParser.Test2
{
    public class Assertion
    {
        public string Id { get; set; }

        public string AspectModel { get; set; }

        public bool ImplicitFiltering { get; set; }

        public string Test { get; set; }

        public Dictionary<string, AssertionVariable> Variables { get; set; }

        public List<string> References { get; set; }

        public List<AssertionMessage> NotSatisfiedMessages { get; set; }

        public List<AssertionMessage> SatisfiedMessages { get; set; }
    }
}
