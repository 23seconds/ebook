﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace EY.com.eBook.BizTaxParser.Test2
{
    public class Link : XType
    {

        public static Link CreateFromElement(XElement element, Dictionary<string, XNamespace> namespaces)
        {
            return CreateFromElement<Link>(element, namespaces);
        }

        public static T CreateFromElement<T>(XElement element, Dictionary<string, XNamespace> namespaces) where T : Link, new()
        {
            T l = XType.CreateFromElement<T>(element, namespaces);
            l.Href = element.Attribute(namespaces["xlink"] + "href").Value;
            string[] href = l.Href.Split(new char[] { '#' });
            l.Xsd = href[0];
            l.ItemId = href[1];
            return l;
        }

        public string Role { get; set; }

        public string Xsd { get; set; }

        public string ItemId { get; set; }

        public string Href { get; set; }
    }
}
