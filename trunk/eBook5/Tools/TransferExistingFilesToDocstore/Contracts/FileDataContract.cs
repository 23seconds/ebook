﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace TransferExistingFilesToDocstore.Contracts
{
    [DataContract]
    public class FileDataContract
    {
        [DataMember]
        public Guid Id { get; set; }

        [DataMember]
        public string FileName { get; set; }

        [DataMember]
        public string EnterpriseNumber { get; set; }

        [DataMember]
        public string Period { get; set; }

        [DataMember]
        public string AssessmentYear { get; set; }

        [DataMember]
        public string Type { get; set; }

        [DataMember]
        public string Path { get; set; }
    }
}
