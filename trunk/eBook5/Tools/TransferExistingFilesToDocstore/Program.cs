﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using TransferExistingFilesToDocstore.Contracts;
using System.Net;
using EY.com.eBook.Core;
using EY.com.eBook.Core.EF;
using EY.com.eBook.Core.EF.Write;
using EY.com.eBook.Core.EF.Read;
using EY.com.eBook.API.Contracts;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.API.BL;
using EY.com.eBook.API.BL.Write;
using EY.com.eBook.API.BL.Read;
//using Newtonsoft.Json;
using System.ServiceModel;
using EY.com.eBook.Core.Data;
using System.Data.Entity;
using System.Data.Linq;
using System.Web.Configuration;

namespace TransferExistingFilesToDocstore
{
    public class Program
    {
        static void Main(string[] args)
        {
            ProcessIntervat("2014-11");
            //ProcessBiztax(@"\\bemechmfiles01.eurw.ey.net\Projects\ACR Central Processing\BizTax\AANSLAGJAAR_2013\3_BizTax Out\Success _ Documents to be downloaded");
            //ProcessBiztax(@"\\bemechmfiles01.eurw.ey.net\Projects\ACR Central Processing\BizTax\AANSLAGJAAR_2013\3_BizTax Out\Success");
            //ProcessRepo(new Guid("3787F2A4-112D-4589-A231-E9B95BDCFCD9")); //logische repository structuur “4. Global Talent Hub” > “National Bank of Belgium – Statutory Accounts”
           // ProcessRepoEA(new Guid("32f0391e-e0bd-41f9-a9c1-c04eac3c8cb1")); // Engagement agreement/letter
           // ProcessBiztax(@"\\bemechmfiles01.eurw.ey.net\Projects\ACR Central Processing\BizTax\AANSLAGJAAR_2014\3_BizTax Out\Success",2014);
           

        }

        public static void ProcessIntervat(string subFolderOnly) {
            Console.WriteLine("start");
            string path = @"\\bemechmfiles01.eurw.ey.net\Projects\ACR Central Processing\Central filing - Intervat\BTW aangifte - Déclaration TVA\3. OUT - BTW\Ontvangstbevestigingen - Receipts de confirmation\2014";
            
            if(!string.IsNullOrEmpty(subFolderOnly)) {
                path = Path.Combine(path,subFolderOnly);
            }
            string[] files = System.IO.Directory.GetFiles(path, "*.pdf", System.IO.SearchOption.AllDirectories);
            //List<FileDataContract> lfdc = new List<FileDataContract>();
            List<BulkDocstore> lbds = new List<BulkDocstore>();
            int counter = 0;
            
            foreach (string f in files)
            {
                counter++;
                Console.WriteLine(counter + "/" + files.Length);
                //string[] fsplit = f.Split('\\');
                DirectoryInfo networkDir = new DirectoryInfo(@f);
                string filenameWithoutExt = Path.GetFileNameWithoutExtension(networkDir.FullName);
                string filenameWithExt = Path.GetFileName(networkDir.FullName);
                string period = networkDir.Parent.Name;
                string year = networkDir.Parent.Parent.Name;
                string type = networkDir.Parent.Parent.Parent.Name;
                string stage = networkDir.Parent.Parent.Parent.Parent.Name;
                string typeBtw = networkDir.Parent.Parent.Parent.Parent.Parent.Name;
                string typeProcess = networkDir.Parent.Parent.Parent.Parent.Parent.Parent.Name;
                string folder = networkDir.Parent.Parent.Parent.Parent.Parent.Parent.Parent.Name;
                if (folder == "ACR Central Processing")
                {
                    string enterpriseNumber = string.Join("", filenameWithoutExt.Split('.'));
                    enterpriseNumber = string.Join("", enterpriseNumber.Split(' '));
                    if (enterpriseNumber.Substring(0, 2) == "BE")
                    {
                        enterpriseNumber = enterpriseNumber.Substring(2, 12);
                    }
                    else
                    {
                        enterpriseNumber = enterpriseNumber.Substring(0, 10);
                    }
                    Guid newId = Guid.NewGuid();
                    string gfisCode = eBookReadManager.Context.ClientSet.Where(x => (x.EnterpriseNumber == enterpriseNumber || x.EnterpriseNumber == "BE" + enterpriseNumber)
                                                                                                    || (x.VatNumber == enterpriseNumber || x.VatNumber == "BE" + enterpriseNumber))
                                                                                            .Select(y => y.GFISCode)
                                                                                            .FirstOrDefault();
                    BulkDocstore bds = new BulkDocstore()
                    {
                        Id = newId,
                        OriginalName = filenameWithExt,
                        NewName = period + " - BTW Aangifte - Déclarations TVA - VAT Return" + ".pdf",
                        DocumentType = "VAT",
                        MetaYear = Int32.Parse(year),
                        MetaPeriod = period,
                        ClientEnterprise = enterpriseNumber,
                        ClientGfis = gfisCode,//gfisCode != null ? gfisCode : "No client found in eBook with this enterprise number",
                        DocStoreId = null,
                        LastSent = DateTime.Now
                    };
                    lbds.Add(bds);

                    try{
                        BulkDocstore bulkds = eBookWriteManager.Context.BulkDocstores.FirstOrDefault(x => x.OriginalName == bds.OriginalName &&
                                                                                                        x.NewName == bds.NewName &&
                                                                                                        x.MetaYear == bds.MetaYear &&
                                                                                                        x.MetaPeriod == bds.MetaPeriod &&
                                                                                                        x.ClientEnterprise == bds.ClientEnterprise);
                        if (bulkds == null)
                        {
                            System.IO.File.Copy(@f, @"C:\temp\BulkDocStore\VAT\" + newId + ".pdf");
                            eBookWriteManager.Context.AddToBulkDocstores(bds);
                            eBookWriteManager.Context.SaveChanges();
                        }
                    }catch (Exception ex){
                        Console.WriteLine(ex.Message);
                        Console.ReadLine();
                    }
                }
                else
                {
                    // error
                    Console.WriteLine("error");
                }

            }
            Console.WriteLine("Done");
        }

        public static void ProcessBiztax(string path,int assessment)
        {
            string[] files = System.IO.Directory.GetFiles(@path, "*.eBook", System.IO.SearchOption.AllDirectories);
            List<BulkDocstore> lbds = new List<BulkDocstore>();
            int counter = 0;
            int counterFiles = 0;
            foreach (string f in files)
            {               
                counter++;
                Console.WriteLine(counter + "/" + files.Length);
                string fileNameWithoutExt = Path.GetFileNameWithoutExtension(f);
                string fileName = Path.GetFileName(f);
                DirectoryInfo networkDir = new DirectoryInfo(@f);
                DirectoryInfo parentFolder = networkDir.Parent;
                string[] pdfVanDeAangifte = System.IO.Directory.GetFiles(parentFolder.FullName, "PDF_van_de_aangifte*.pdf", System.IO.SearchOption.AllDirectories);//.Where(x=>x.StartsWith("PDF_van_de_aangifte"));
                string[] ontvangstBewijs = System.IO.Directory.GetFiles(parentFolder.FullName, "Ontvangstbewijs*.pdf", System.IO.SearchOption.AllDirectories);

                Guid ebookFileId = new Guid(fileNameWithoutExt);
                EY.com.eBook.Core.EF.Read.File file = eBookReadManager.Context.Files.Include("Client").FirstOrDefault(x => x.Id == ebookFileId);

                if (pdfVanDeAangifte.Length > 0)
                {
                    foreach (string p in pdfVanDeAangifte)
                    {
                        
                        DirectoryInfo networkDirPdfVanDeAangifte = new DirectoryInfo(@p);
                        string fileNameAangifte = Path.GetFileNameWithoutExtension(networkDirPdfVanDeAangifte.FullName);
                        Guid newId = Guid.NewGuid();
                        BulkDocstore bds = null;
                        if (file != null)
                        {
                            bds = new BulkDocstore()
                            {
                                Id = newId,
                                OriginalName = fileNameAangifte,
                                NewName = assessment.ToString() + " - Vennootschapsbelasting - Impôt sur les sociétés - Corporate income tax return" + ".pdf",
                                DocumentType = "TAX",
                                MetaYear = assessment,
                                MetaPeriod = null,
                                ClientEnterprise = file.Client.EnterpriseNumber != null ? file.Client.EnterpriseNumber : null,
                                ClientGfis = file.Client.GFISCode != null ? file.Client.GFISCode : null,
                                DocStoreId = null,
                                LastSent = DateTime.Now
                            };

                        }
                        else
                        {
                            bds = new BulkDocstore()
                            {
                                Id = newId,
                                OriginalName = fileNameAangifte,
                                NewName = assessment.ToString() + " - Vennootschapsbelasting - Impôt sur les sociétés - Corporate income tax return" + ".pdf",
                                DocumentType = "TAX",
                                MetaYear = assessment,
                                MetaPeriod = null,
                                ClientEnterprise = null,
                                ClientGfis = "No client found in eBook with this enterprise number",
                                DocStoreId = null,
                                LastSent = DateTime.Now
                            };
                        }
                        lbds.Add(bds);

                        try
                        {
                            BulkDocstore bulkds = eBookWriteManager.Context.BulkDocstores.FirstOrDefault(x => x.OriginalName == bds.OriginalName &&
                                                                                                        x.NewName == bds.NewName &&
                                                                                                        x.MetaPeriod == bds.MetaPeriod &&
                                                                                                        x.ClientEnterprise == bds.ClientEnterprise &&
                                                                                                        x.ClientGfis == bds.ClientGfis);
                            if (bulkds == null)
                            {
                                System.IO.File.Copy(@p, @"C:\temp\BulkDocStore\TAX\Success\" + newId + ".pdf");
                                eBookWriteManager.Context.AddToBulkDocstores(bds);
                                eBookWriteManager.Context.SaveChanges();
                            }
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex);
                            Console.ReadLine();
                        }
                        counterFiles++;
                    }
                }

                if (ontvangstBewijs.Length > 0)
                {
                    foreach (string o in ontvangstBewijs)
                    {
                        DirectoryInfo networkDirOntvangstBewijs = new DirectoryInfo(@o);
                        string fileNameOntvangstBewijs = Path.GetFileNameWithoutExtension(networkDirOntvangstBewijs.FullName);
                        Guid newId = Guid.NewGuid();
                        BulkDocstore bds = null;
                        if (file != null)
                        {
                            bds = new BulkDocstore()
                            {
                                Id = newId,
                                OriginalName = fileNameOntvangstBewijs,
                                NewName = assessment.ToString() + " - Ontvangstbewijs fiscale aangifte - Accusé de réception déclaration à l’impôt - Receipt Tax Return",
                                DocumentType = "TAX",
                                MetaYear = assessment,
                                MetaPeriod = null,
                                ClientEnterprise = file.Client.EnterpriseNumber != null ? file.Client.EnterpriseNumber : null,
                                ClientGfis = file.Client.GFISCode != null ? file.Client.GFISCode : null,
                                DocStoreId = null,
                                LastSent = DateTime.Now
                            };

                        }
                        else
                        {
                            bds = new BulkDocstore()
                            {
                                Id = newId,
                                OriginalName = fileNameOntvangstBewijs,
                                NewName = assessment.ToString() + " - Ontvangstbewijs fiscale aangifte - Accusé de réception déclaration à l’impôt - Receipt Tax Return",
                                DocumentType = "TAX",
                                MetaYear = assessment,
                                MetaPeriod = null,
                                ClientEnterprise = null,
                                ClientGfis = "No client found in eBook with this enterprise number",
                                DocStoreId = null,
                                LastSent = DateTime.Now
                            };
                        }
                        lbds.Add(bds);

                        try
                        {
                            BulkDocstore bulkds = eBookWriteManager.Context.BulkDocstores.FirstOrDefault(x => x.OriginalName == bds.OriginalName &&
                                                                                                        x.NewName == bds.NewName &&
                                                                                                        x.MetaYear == bds.MetaYear &&
                                                                                                        x.ClientEnterprise == bds.ClientEnterprise &&
                                                                                                        x.ClientGfis == bds.ClientGfis);
                            if (bulkds == null)
                            {
                                System.IO.File.Copy(@o, @"C:\temp\BulkDocStore\TAX\Success\" + newId + ".pdf");
                                eBookWriteManager.Context.AddToBulkDocstores(bds);
                                eBookWriteManager.Context.SaveChanges();
                            }
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex);
                            Console.ReadLine();
                        }
                        counterFiles++;
                    }
                }
            }
            Console.WriteLine("done");
            Console.WriteLine("#Files = " + counterFiles);
            Console.ReadLine();
        }

        public static void ProcessRepo(Guid structureId)
        {
            using (System.IO.StreamWriter file = new System.IO.StreamWriter(@WebConfigurationManager.AppSettings["eBook.Resources"].ToString() + "\\TransferExistingFilesToDocstore_ProcessRepo_log.txt", true))
            {
                Console.WriteLine("Start");
                /* LOG */ file.WriteLine(DateTime.Now.ToString("HH:mm:ss.fff") + " :: START");
                //Guid structureId = new Guid("3787F2A4-112D-4589-A231-E9B95BDCFCD9");
                /* LOG */ file.WriteLine(DateTime.Now.ToString("HH:mm:ss.fff") + " :: Start generating list of files");
                List<RepositoryItem> lr = eBookReadManager.Context.RepositoryItems.Where(x => x.StructureId == structureId && x.Extension.ToLower() == ".pdf" && x.Deleted == false).ToList();
                /* LOG */ file.WriteLine(DateTime.Now.ToString("HH:mm:ss.fff") + " :: List generated (" + lr.Count + " files)");
                if (lr.Count > 0)
                {
                    int counter = 0;
                    foreach (RepositoryItem r in lr)
                    {
                        counter++;
                        Console.WriteLine(counter + "/" + lr.Count);
                        EY.com.eBook.Core.EF.Read.Client c = eBookReadManager.Context.ClientSet.FirstOrDefault(x => x.Id == r.ClientId);

                        BulkDocstore bds = null;
                        Guid newId = Guid.NewGuid();
                        if (c != null)
                        {
                            bds = new BulkDocstore()
                            {
                                Id = newId,
                                OriginalName = r.Name,
                                NewName = r.EndDate.Year + " - Jaarrekening - Comptes annuels - Statutory Accounts" + ".pdf",
                                DocumentType = "STATUTORY",
                                MetaYear = r.EndDate.Year,
                                MetaPeriod = null,
                                ClientEnterprise = c.EnterpriseNumber != null ? c.EnterpriseNumber : null,
                                ClientGfis = c.GFISCode != null ? c.GFISCode : null,
                                DocStoreId = null,
                                LastSent = DateTime.Now
                            };
                            string clientId = string.Join("", r.ClientId.ToString().Split('-'));
                            var clientRepoPath = new StringBuilder();
                            int count = 0;
                            foreach (var ch in clientId)
                            {
                                clientRepoPath.Append(ch);
                                if ((++count % 2) == 0)
                                {
                                    clientRepoPath.Append("\\");
                                }
                            }
                            try
                            {
                                BulkDocstore bulkds = eBookWriteManager.Context.BulkDocstores.FirstOrDefault(x => x.OriginalName == bds.OriginalName &&
                                                                                                        x.NewName == bds.NewName &&
                                                                                                        x.MetaYear == bds.MetaYear &&
                                                                                                        x.ClientEnterprise == bds.ClientEnterprise &&
                                                                                                        x.ClientGfis == bds.ClientGfis);
                                if (bulkds == null)
                                {
                                    //string path = @"\v:\data\ebook5\public\repository\";
                                    string pathFrom = @"V:\DATA\eBook5\Public\Repository\" + clientRepoPath + "\\" + r.ItemId.ToString().ToLower() + r.Extension;
                                    string pathTo = @"V:\APPS\eBook.50\BulkDocstoreFiles\STATUTORY\" + newId + ".pdf";
                                    System.IO.File.Copy(pathFrom, pathTo);
                                    eBookWriteManager.Context.AddToBulkDocstores(bds);
                                    eBookWriteManager.Context.SaveChanges();
                                    /* LOG */
                                    file.WriteLine(DateTime.Now.ToString("HH:mm:ss.fff") + " :: File copied from " + pathFrom + " TO: " + pathTo);
                                }
                            }
                            catch (Exception ex) {
                                /* LOG */ file.WriteLine(DateTime.Now.ToString("HH:mm:ss.fff") + " :: Exception: " + ex.Message);
                                Console.WriteLine(ex.Message);
                                Console.ReadLine();
                            }
                        }

                    }
                }
                Console.WriteLine("Done");
                Console.ReadLine();
            }
        }

        public static void ProcessRepoEA(Guid structureId)
        {
            try
            {
                using (System.IO.StreamWriter file = new System.IO.StreamWriter(@WebConfigurationManager.AppSettings["eBook.Resources"].ToString() + "\\TransferExistingFilesToDocstore_ProcessRepoEA_log.txt", true))
                {
                    Console.WriteLine("Start");
                    /* LOG */
                    file.WriteLine(DateTime.Now.ToString("HH:mm:ss.fff") + " :: START");
                    //Guid structureId = new Guid("3787F2A4-112D-4589-A231-E9B95BDCFCD9");
                    /* LOG */
                    file.WriteLine(DateTime.Now.ToString("HH:mm:ss.fff") + " :: Start generating list of files");
                    int cnt = 0;
                    Guid statusSent = new Guid("6a9821f5-8724-4926-a815-6cb02c9ca83d");
                    Guid statusSigned = new Guid("fcf7a560-3d74-4341-a32b-310c8fa336cf");
                    List<Guid> EAclientGuids = eBookReadManager.Context.RepositoryItems.Where(x => x.StructureId == structureId && x.Deleted == false).Select(y => y.ClientId).Distinct().ToList();
                    Console.WriteLine("Start collecting EA Sent");
                    foreach (Guid ea in EAclientGuids)
                    {
                        cnt++;
                        List<RepositoryItem> lrSent = eBookReadManager.Context.RepositoryItems.Where(x => x.StructureId == structureId && x.Extension.ToLower() == ".pdf" && x.Deleted == false && x.Status == statusSent && x.ClientId == ea).OrderByDescending(o => o.StartDate).ToList();
                        List<RepositoryItem> lrSigned = eBookReadManager.Context.RepositoryItems.Where(x => x.StructureId == structureId && x.Extension.ToLower() == ".pdf" && x.Deleted == false && x.Status == statusSigned && x.ClientId == ea).OrderByDescending(o => o.StartDate).ToList();
                        Guid newId = Guid.NewGuid();
                        BulkDocstore bds = null;
                        if (lrSent.Count > 0 && lrSigned.Count == 0)
                        {
                            var sent = lrSent[0];
                            EY.com.eBook.Core.EF.Read.Client c = eBookReadManager.Context.ClientSet.FirstOrDefault(x => x.Id == sent.ClientId);
                            if (c != null)
                            {
                                
                                bds = new BulkDocstore()
                                {
                                    Id = newId,
                                    OriginalName = sent.Name,
                                    NewName = sent.StartDate + " - " + sent.EndDate + " - Opdrachtbrief - Lettre de mission – Engagement agreement" + ".pdf",
                                    DocumentType = "ENGAGEMENT AGREEMENTS",
                                    MetaYear = null,
                                    MetaPeriod = sent.StartDate + " - " + sent.EndDate,
                                    ClientEnterprise = c.EnterpriseNumber != null ? c.EnterpriseNumber : null,
                                    ClientGfis = c.GFISCode != null ? c.GFISCode : null,
                                    DocStoreId = null,
                                    LastSent = DateTime.Now
                                };
                            }
                        }
                        if (lrSent.Count > 0 && lrSigned.Count > 0)
                        {
                            if (lrSent[0].StartDate > lrSigned[0].StartDate)
                            {
                                var sent = lrSent[0];
                                EY.com.eBook.Core.EF.Read.Client c = eBookReadManager.Context.ClientSet.FirstOrDefault(x => x.Id == sent.ClientId);
                                if (c != null)
                                {
                                    
                                    bds = new BulkDocstore()
                                    {
                                        Id = newId,
                                        OriginalName = sent.Name,
                                        NewName = sent.StartDate + " - " + sent.EndDate + " - Opdrachtbrief - Lettre de mission – Engagement agreement" + ".pdf",
                                        DocumentType = "ENGAGEMENT AGREEMENTS",
                                        MetaYear = null,
                                        MetaPeriod = sent.StartDate + " - " + sent.EndDate,
                                        ClientEnterprise = c.EnterpriseNumber != null ? c.EnterpriseNumber : null,
                                        ClientGfis = c.GFISCode != null ? c.GFISCode : null,
                                        DocStoreId = null,
                                        LastSent = DateTime.Now
                                    };
                                }
                            }
                        }
                        if (bds != null)
                        {
                            string clientId = string.Join("", ea.ToString().Split('-'));
                            var clientRepoPath = new StringBuilder();
                            int count = 0;
                            foreach (var ch in clientId)
                            {
                                clientRepoPath.Append(ch);
                                if ((++count % 2) == 0)
                                {
                                    clientRepoPath.Append("\\");
                                }
                            }
                            try
                            {
                                BulkDocstore bulkds = eBookWriteManager.Context.BulkDocstores.FirstOrDefault(x => x.OriginalName == bds.OriginalName &&
                                                                                                        x.NewName == bds.NewName &&
                                                                                                        x.MetaPeriod == bds.MetaPeriod &&
                                                                                                        x.ClientEnterprise == bds.ClientEnterprise &&
                                                                                                        x.ClientGfis == bds.ClientGfis);
                                if (bulkds == null)
                                {
                                    //string path = @"\v:\data\ebook5\public\repository\";
                                    string pathFrom = @"V:\DATA\eBook5\Public\Repository\" + clientRepoPath + "\\" + lrSent[0].ItemId.ToString().ToLower() + lrSent[0].Extension;
                                    string pathTo = @"V:\APPS\eBook.50\BulkDocstoreFiles\EA\" + newId + ".pdf";
                                    System.IO.File.Copy(pathFrom, pathTo);
                                    eBookWriteManager.Context.AddToBulkDocstores(bds);
                                    eBookWriteManager.Context.SaveChanges();
                                    /* LOG */
                                    file.WriteLine(DateTime.Now.ToString("HH:mm:ss.fff") + " :: File copied from " + pathFrom + " TO: " + pathTo);
                                }
                            }
                            catch (Exception ex)
                            {
                                /* LOG */
                                file.WriteLine(DateTime.Now.ToString("HH:mm:ss.fff") + " :: Exception: " + ex.Message);
                                Console.WriteLine(ex.Message);
                                Console.ReadLine();
                            }
                        }
                        Console.WriteLine(cnt + " / " + EAclientGuids.Count);
                    }



                    Console.WriteLine("Start collecting EA Signed");
                    cnt = 0;
                    List<RepositoryItem> EAsigned = eBookReadManager.Context.RepositoryItems.Where(x => x.StructureId == structureId && x.Deleted == false && x.Status == statusSigned).ToList();
                    foreach (RepositoryItem eas in EAsigned)
                    {
                        cnt++;
                        Guid newId = Guid.NewGuid();
                        BulkDocstore bds = null;
                        EY.com.eBook.Core.EF.Read.Client c = eBookReadManager.Context.ClientSet.FirstOrDefault(x => x.Id == eas.ClientId);
                        if (c != null)
                        {
                            bds = new BulkDocstore()
                            {
                                Id = newId,
                                OriginalName = eas.Name,
                                NewName = eas.StartDate + " - " + eas.EndDate + " - Opdrachtbrief - Lettre de mission – Engagement agreement" + ".pdf",
                                DocumentType = "ENGAGEMENT AGREEMENTS",
                                MetaYear = null,
                                MetaPeriod = eas.StartDate + " - " + eas.EndDate,
                                ClientEnterprise = c.EnterpriseNumber != null ? c.EnterpriseNumber : null,
                                ClientGfis = c.GFISCode != null ? c.GFISCode : null,
                                DocStoreId = null,
                                LastSent = DateTime.Now
                            };
                        
                            string clientId = string.Join("", c.Id.ToString().Split('-'));
                            var clientRepoPath = new StringBuilder();
                            int count = 0;
                            foreach (var ch in clientId)
                            {
                                clientRepoPath.Append(ch);
                                if ((++count % 2) == 0)
                                {
                                    clientRepoPath.Append("\\");
                                }
                            }
                            try
                            {
                                BulkDocstore bulkds = eBookWriteManager.Context.BulkDocstores.FirstOrDefault(x => x.OriginalName == bds.OriginalName &&
                                                                                                        x.NewName == bds.NewName &&
                                                                                                        x.MetaPeriod == bds.MetaPeriod &&
                                                                                                        x.ClientEnterprise == bds.ClientEnterprise &&
                                                                                                        x.ClientGfis == bds.ClientGfis);
                                if (bulkds == null)
                                {
                                    //string path = @"\v:\data\ebook5\public\repository\";
                                    string pathFrom = @"V:\DATA\eBook5\Public\Repository\" + clientRepoPath + "\\" + eas.ItemId.ToString().ToLower() + eas.Extension;
                                    string pathTo = @"V:\APPS\eBook.50\BulkDocstoreFiles\EA\" + newId + ".pdf";
                                    System.IO.File.Copy(pathFrom, pathTo);
                                    eBookWriteManager.Context.AddToBulkDocstores(bds);
                                    eBookWriteManager.Context.SaveChanges();
                                    /* LOG */
                                    file.WriteLine(DateTime.Now.ToString("HH:mm:ss.fff") + " :: File copied from " + pathFrom + " TO: " + pathTo);
                                }
                            }
                            catch (Exception ex)
                            {
                                /* LOG */
                                file.WriteLine(DateTime.Now.ToString("HH:mm:ss.fff") + " :: Exception: " + ex.Message);
                                Console.WriteLine(ex.Message);
                                Console.ReadLine();
                            }
                        }
                        Console.WriteLine(cnt + " / " + EAsigned.Count);
                    }
                    Console.WriteLine("Done");
                    Console.ReadLine();
                    /* LOG */
                }
            }
            catch (Exception ex) {
                Console.WriteLine(ex);
                Console.ReadLine();
            }
        }
    }
}
