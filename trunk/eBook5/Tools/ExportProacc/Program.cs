﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EY.com.eBook.BL.ProAcc;
using EY.com.eBook.ProAcc.API.Contracts;
using EY.com.eBook.API.Contracts;
using EY.com.eBook.API.Contracts.Data;
using System.Xml.Linq;

namespace ExportProacc
{
    class Program
    {
        static void Main(string[] args)
        {

            NewSharedDataContext nsdc = new NewSharedDataContext();

            foreach(ProaccExact pe in nsdc.ProaccExacts.ToList()) {
                Console.WriteLine("Downloading Proacc data for {0} ", pe.database);
                ProAccRepository pr = new ProAccRepository(pe.acc_tool_server, pe.database);
                List<ProAccAccountDataContract> accounts = pr.GetAccountsBare();

                XElement xe = EY.com.eBook.Core.CoreHelper.SerializeToXElement(accounts, true);
                xe.Save(pe.database + ".xml");
                Console.WriteLine(" {0} - downloaded & saved", pe.database);
            }
            Console.ReadLine();
            
        }
    }
}
