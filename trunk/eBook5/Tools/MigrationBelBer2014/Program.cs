﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EY.com.eBook.Caching;
using EY.com.eBook.Core.EF;
using EY.com.eBook.RuleEngine;
using System.Xml.Linq;

namespace MigrationBelBer2014
{
    public class Program
    {
        private static Dictionary<string, List<Type>> _iWorksheetClassTypes = new Dictionary<string, List<Type>>();

        static void Main(string[] args)
        {
            int amount = 400;
            int skip = 0;
            int throttle = 100;
            int cnt = 0;
            int ttl = 0;

            if (args != null && args.Length > 0)
            {
                int.TryParse(args[0], out skip);
                cnt = skip;
            }

            PrepCaching();
            // Get file ids
            List<Guid> files = eBookReadManager.Context.Files.Include("Client").ToList().Where(f => f.Client != null && f.EndDate.AddDays(1).Year == 2014).Select(f => f.Id).Skip(skip).Take(amount).ToList();
            ttl = eBookReadManager.Context.Files.Include("Client").ToList().Where(f => f.Client != null && f.EndDate.AddDays(1).Year == 2014).Count();
            CacheRepository cr = new CacheRepository();
            RuleEngine2014 re = new RuleEngine2014();
            foreach (Guid fileId in files)
            {
                cnt++;
                if (cnt % throttle == 0)
                {
                    Console.WriteLine("MEM:" + GC.GetTotalMemory(true));
                    Console.WriteLine("Clearing memory");
                    eBookWriteManager.SetContextNull();
                    FileCache.Current.ClearComplete();
                    Console.WriteLine("Waiting for garbage collector");
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                    GC.Collect();
                    Console.WriteLine("MEM:" + GC.GetTotalMemory(true));
                    Console.WriteLine("Sleeping 6 seconds");
                    System.Threading.Thread.Sleep(6000);
                }
                Console.WriteLine("{0}/{1}", cnt, ttl);
                //RemoveOLD275C(fileId);
                
                cr.LoadFileInCache(fileId);


                var set = re.GetSet(fileId);

                /* CLEAN */
             /*   MoveWorksheetData(ref set);
                set.Save();
                cr.RemoveFileFromCache(fileId);

                RemoveTabVerreVoorhefAndOverzichtBelBerekening(fileId);
                */
                /* RECALC (FIRST RUN CLEAN, THEN REMOVE DATACONTRACTS, then RECALC) */
                try
                {
                    set.ForceRecalculation(null);
                }
                catch (Exception e)
                {
                    Console.WriteLine("EXC: " + e.Message);
                    Console.WriteLine(e.StackTrace);

                }
                set = null;
            }
         
        }

   
      
        private static void PrepCaching()
        {
            string dte = string.Format("{0}_{1}_{2}-{3}_{4}_{5}", DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second);
            FileCache.Current.CacheRefreshFrequency = new TimeSpan(0, 1, 0);

            Console.WriteLine("Loading IWORKSHEET CLASSES...");


            _iWorksheetClassTypes = new EY.com.eBook.RuleEngine.Data.Prepper().PrepareProtobuf();

            Console.WriteLine("Loading COEFFICIENTS...");
            new CacheRepository().LoadCoeffsFromDB();
            Console.WriteLine("Loading TRANSLATIONS...");
            new CacheRepository().LoadIWorksheetClasses(_iWorksheetClassTypes);
            new CacheRepository().LoadWorksheetTypeTranslations();
            new CacheRepository().LoadMessageTranslations();
            Console.WriteLine("Loading MAPPINGS...");
            new CacheRepository().LoadMappings();
            Console.WriteLine("DONE PREPARING CACHE...");
            Console.WriteLine("Loading files...");
        }

        /*
         * AJ 2014
         * Move data from worksheet aangiftevenb, tab Verrekenbare Voorheffing 
         * to worksheet belasting berekening
         * Move data from worksheet belasting berekening, tab belasting berekening
         * to tab belasting overzicht (Exit Tarief)
         */
        static void MoveWorksheetData(ref EY.com.eBook.RuleEngine.AY2014.WorksheetSet set)
        {
            /*
            var belber = set._datacontainer.BerekeningVenBApp;
            var aangifte = set._datacontainer.AangifteVenBApp.Data.VerrekenbareVoorheffing;

            belber.Data.VerrekenbareVoorheffing = new EY.com.eBook.RuleEngine.AY2014.BerekeningVenBAppTypes.VerrekenbareVoorheffingEntity
            {
                Id=Guid.NewGuid()
                , IsValid =true
                , LastUpdated= DateTime.Now
                , PreviousImported=false
                ,
                TitleNietTerugbetaalbaar = aangifte.TitleNietTerugbetaalbaar
                ,
                TitleTerugbetaalbaar = aangifte.TitleTerugbetaalbaar
                ,
                VBelastingkredietHuiidTijdperk = aangifte.VBelastingkredietHuiidTijdperk
                ,
                VVNTBelastingKredietOnderzoekOntw = aangifte.VVNTBelastingKredietOnderzoekOntw
                ,
                VVNTFictieveRoerendeVoorheffing = aangifte.VVNTFictieveRoerendeVoorheffing
                ,
                VVNTForfaitairBuitenlands = aangifte.VVNTForfaitairBuitenlands
                ,
                VVNTTotaal = aangifte.VVNTTotaal
                ,
                VVTAndere = aangifte.VVTAndere
                ,
                VVTAndereBuitenlands = aangifte.VVTAndereBuitenlands
                ,
                VVTAndereEigen = aangifte.VVTAndereEigen
                ,
                VVTAndereRoerendeVoorheffing = aangifte.VVTAndereRoerendeVoorheffing
                ,
                VVTBelgisch = aangifte.VVTBelgisch
                ,
                VVTEigenBuitenlands = aangifte.VVTEigenBuitenlands
                ,
                VVTTotaal = aangifte.VVTTotaal
                ,
                VVTTotaalExpected = aangifte.VVTTotaalExpected
               
            };

            if (belber.Data.BerekeningOverzicht == null)
            {
                belber.Data.BerekeningOverzicht = new EY.com.eBook.RuleEngine.AY2014.BerekeningVenBAppTypes.BerekeningOverzichtItem { Id = Guid.NewGuid() };
            }
            belber.Data.BerekeningOverzicht.ExitTarif = belber.Data.BelastingsBerekening.ExitTarif;
            belber.Data.OpdelingNaarOorsprong.WIZeeSchip = set._datacontainer.AangifteVenBApp.Data.UiteenzettingWinst.WIZeeschip;
             */
        }

        /*
         * AJ 2014
         * Remove tab verrekenbare voorheffing from worksheet aangiftevenb
         * Remove tabs BelastingBerekening, BelastingBerekening2, Overzicht
         */
        static void RemoveTabVerreVoorhefAndOverzichtBelBerekening(Guid fileId)
        {

            Guid typeid = new Guid("65BF5CBD-187B-4C41-AAD9-9793357F222B"); // aangifte venb
            var ws = eBookWriteManager.Context.Worksheets.FirstOrDefault(w => w.FileId == fileId && w.TypeId == typeid);
            XElement xe = XElement.Parse(ws.Data);
            var ns = xe.GetDefaultNamespace();
            var x = xe.Element(ns + "VerrekenbareVoorheffing");
            if (x != null)
            {
                x.Remove();
            }
            ws.Data = xe.ToString();
            eBookWriteManager.Context.SaveChanges();

            typeid = new Guid("803D6A06-352E-4E28-ADE5-214DEAAC76CA");
            ws = eBookWriteManager.Context.Worksheets.FirstOrDefault(w => w.FileId == fileId && w.TypeId == typeid);
            xe = XElement.Parse(ws.Data);
            ns = xe.GetDefaultNamespace();
            x = xe.Element(ns + "Overzicht");
            if (x != null)
            {
                x.Remove();
            }
            x = xe.Element(ns + "BelastingsBerekening");
            if (x != null)
            {
                x.Remove();
            }
            x = xe.Element(ns + "BelastingsBerekening2");
            if (x != null)
            {
                x.Remove();
            }
            ws.Data = xe.ToString();
            eBookWriteManager.Context.SaveChanges();

        }


        static void RemoveOLD275C(Guid fileId)
        {

            Guid typeid = new Guid("A733B82A-E152-4916-9D77-2ED1B39E2D36"); // risico kap
            var ws = eBookWriteManager.Context.Worksheets.FirstOrDefault(w => w.FileId == fileId && w.TypeId == typeid);
           
            var xe = XElement.Parse(ws.Data);
            if (xe != null)
            {
                var ns = xe.GetDefaultNamespace();
                var x = xe.Element(ns + "Fiche");
                if (x != null)
                {
                    var xs = xe.Element(ns + "AandelenBelegging");
                    if (xs != null) xs.Remove();
                    xs = xe.Element(ns + "InrichtingVerdrag");
                    if (xs != null) xs.Remove();
                    xs = xe.Element(ns + "OnroerendVerdrag");
                    if (xs != null) xs.Remove();
                }


                ws.Data = xe.ToString();
                eBookWriteManager.Context.SaveChanges();
            }

            typeid = new Guid("E4C15BB2-CDD4-4D9D-A7C9-7D1FD4080776"); // risico kap
            ws = eBookWriteManager.Context.Worksheets.FirstOrDefault(w => w.FileId == fileId && w.TypeId == typeid);

            xe = XElement.Parse(ws.Data);
            if (xe != null)
            {
                var ns = xe.GetDefaultNamespace();
                var x = xe.Element(ns + "Fiche");
                if (x != null)
                {
                    var xs = xe.Element(ns + "AandelenBelegging");
                    if (xs != null) xs.Remove();
                    xs = xe.Element(ns + "InrichtingVerdrag");
                    if (xs != null) xs.Remove();
                    xs = xe.Element(ns + "OnroerendVerdrag");
                    if (xs != null) xs.Remove();
                }


                ws.Data = xe.ToString();
                eBookWriteManager.Context.SaveChanges();
            }
        }

     
    }
}
