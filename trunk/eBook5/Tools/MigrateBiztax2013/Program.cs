﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EY.com.eBook.Core.EF;
using EY.com.eBook.Core.EF.Write;
using bzt = EY.com.eBook.BizTax;
using nw = EY.com.eBook.BizTax.Contracts;
using System.Data.Entity;
using old = EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.Core;
using System.Text.RegularExpressions;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Xml;
using EY.com.eBook.BizTax;



namespace MigrateBiztax2013
{
    public static class Extensions
    {
        public static string CleanXbrlId(this string val)
        {
            Regex rgx = new Regex("[^a-zA-Z0-9 \\-]");
            val = rgx.Replace(val, "").Replace(" ", "");
            return val;
        }

        
    }
    class Program
    {
        public static string ToIso8601DateOnly(DateTime dte)
        {

            return dte.ToString("yyyy-MM-dd");
        }

        public static int total = 100;
        public static int totalDB = 0;
        public static int i = 0;

        private static DateTime minEndDateIncl2012 = new DateTime(2011, 12, 31);
        private static DateTime minEndDate = new DateTime(2012, 12, 31);
        private static DateTime maxEndDate = (new DateTime(2014, 12, 30)).AddMilliseconds(-1);


        private static Guid biztaxauto = new Guid("BD7C2EAE-8500-4103-8984-0131E73D07FA");

        static void Main(string[] args)
        {
            
            if (args.Length > 1)
            {
                i = int.Parse(args[0]);
                total = int.Parse(args[1]);
            }


            
            totalDB = eBookWriteManager.Context.Files.Count(f =>!f.CloseDate.HasValue && !f.Deleted && !f.MarkDelete && f.EndDate >= minEndDate && f.EndDate <= maxEndDate);
                //eBookReadManager.Context.Files.Include("Services").Count(f => !f.Deleted && !f.MarkDelete && f.CloseDate == null && f.EndDate >= minEndDate && f.EndDate <=maxEndDate && f.Services.Count(c => c.ServiceId == biztaxauto) > 0);
            
            int to = 100;

            
            Console.WriteLine("Total:" + totalDB);
            while (i < total-1 && i <totalDB-1)
            {
                Console.WriteLine("Next 100 from " + i + " onwards.");
                //RunFor(i, 100, i+1);
                RunFor(i, 100, i + 1);

                eBookReadManager.SetContextNull();
                eBookWriteManager.SetContextNull();
                GC.Collect();
                GC.WaitForPendingFinalizers();
                Console.WriteLine("sleep 1000");
                System.Threading.Thread.Sleep(1000);
            }
            
           // Guid id = new Guid("0fd14db6-dc1a-4416-b2ea-bbb054e38872");
          //  CleanDubProv(id);
           
            //CleanInvalidStartDatesAndDateType(eBookWriteManager.Context.Files.First(f=>f.Id ==id));
           //FileXbrl fx = eBookWriteManager.Context.FileXbrlSet.First(f => f.FileId == id);
          // CorrectMissingPrefix(fx);
            //HandleFX(fx);
            
            Console.WriteLine("done");

        }
        static Guid testid = new Guid("3ca3928a-310e-4125-abec-4bea8df702e7");
        static void RunFor(int start, int take, int counter) {


            foreach (Guid fileId in eBookReadManager.Context.Files.Where(f => !f.CloseDate.HasValue && !f.Deleted && !f.MarkDelete && f.EndDate >= minEndDate && f.EndDate <= maxEndDate).OrderByDescending(f => f.EndDate).Skip(start).Take(take).Select(f => f.Id))
            {
               
                Console.WriteLine("");
                Console.Write("{0}/{1}", (i+1), totalDB);
                EY.com.eBook.Core.EF.Write.File file = eBookWriteManager.Context.Files.FirstOrDefault(f => f.Id == fileId);
                FileXbrl fx = eBookWriteManager.Context.FileXbrlSet.FirstOrDefault(f => f.FileId == fileId);
                if (fx != null && fx.StatusId < 99)
                {
                    Console.Write(" status:" + fx.StatusId + " ");
                    CleanPrefix(fx);
                    //fx = CleanInvalidDates(file, fx);
                   
                    //OpenLocks(fx);
                    /*bool isauto = eBookWriteManager.Context.FileServices.Count(f => f.FileId == fileId && f.ServiceId == biztaxauto) > 0;
                    if (fx.StatusId < 10)
                    {
                        CorrectMissingPrefix(fx);

                        if (isauto)
                            CleanDubProv(fileId);
                    }*/
                }
                i++;
            }
           
            
        }

        private static void CleanPrefix(FileXbrl fx)
        {
            EY.com.eBook.BizTax.Contracts.BizTaxDataContract bzt = CoreHelper.DeserializeFromString<EY.com.eBook.BizTax.Contracts.BizTaxDataContract>(fx.XbrlData);
            var f = new FileBizTaxRepository();
            bzt.Elements.ForEach(x => f.CleanPrefix(x,null));
            fx.XbrlData = CoreHelper.SerializeToString(bzt);
            eBookWriteManager.Context.SaveChanges();

        }

        private static void CleanPrefix(XbrlElementDataContract current, XbrlElementDataContract parent)
        {
            if (current == null) return;
            if (string.IsNullOrEmpty(current.Prefix) && parent != null && !string.IsNullOrEmpty(parent.Prefix)) current.Prefix = parent.Prefix;
            if (current.Children != null) current.Children.ForEach(x => CleanPrefix(x, current));
        }

        static void RunForAllFiles(int start, int take, int counter)
        {


            foreach (EY.com.eBook.Core.EF.Write.File file in eBookWriteManager.Context.Files.Where(f => !f.Deleted && !f.MarkDelete && f.CloseDate == null && f.EndDate >= minEndDateIncl2012 && f.EndDate <= maxEndDate).OrderBy(f => f.EndDate).Skip(start).Take(take).ToList())
            {

                Console.WriteLine("");
                Console.Write("{0}/{1}", (i + 1), totalDB);
                //CleanInvalidStartDatesAndDateType(file);
                i++;
            }


        }

        static void OpenLocks(FileXbrl fx)
        {

            
            //     FileXbrl fx = eBookWriteManager.Context.FileXbrlSet.FirstOrDefault(f => f.FileId == file.Id);
            if (fx != null)
            {
                //x.Locked &&
                EY.com.eBook.BizTax.Contracts.BizTaxDataContract bzt = CoreHelper.DeserializeFromString<EY.com.eBook.BizTax.Contracts.BizTaxDataContract>(fx.XbrlData);
                bool save = false;
                var xe = bzt.Elements.Where(x => x.Locked && (
                    x.Name == "AssociatedCompanyCorporationCodeCurrentTaxPeriod"
                    || x.Name == "AnnualWorkForceAverageCorporationCodeCurrentTaxPeriod"
                    || x.Name == "AnnualTurnoverExcludingVATCorporationCodeCurrentTaxPeriod"
                    || x.Name == "BalanceSheetTotalCorporationCodeCurrentTaxPeriod"
                    )
                    ).ToList();
                if (xe!=null && xe.Count>0 )
                {
                    xe.ForEach(x => x.Locked = false);
                    fx.XbrlData = CoreHelper.SerializeToString(bzt);
                    eBookWriteManager.Context.SaveChanges();
                    Console.Write("... unlocked grootte venb");
                }
                

            }
        }

        static FileXbrl CleanInvalidDates(EY.com.eBook.Core.EF.Write.File file, FileXbrl fx)
        {
            
            string startDate = ToIso8601DateOnly(file.StartDate);
            string endDate = ToIso8601DateOnly(file.EndDate);
            string iStart = ToIso8601DateOnly(file.StartDate.AddDays(-1));
       //     FileXbrl fx = eBookWriteManager.Context.FileXbrlSet.FirstOrDefault(f => f.FileId == file.Id);
            if (fx != null)
            {
                EY.com.eBook.BizTax.Contracts.BizTaxDataContract bzt = CoreHelper.DeserializeFromString<EY.com.eBook.BizTax.Contracts.BizTaxDataContract>(fx.XbrlData);
                List<EY.com.eBook.BizTax.ContextElementDataContract> cedcs = bzt.Contexts.Where(c => !string.IsNullOrEmpty(c.Period.StartDate) && c.Period.StartDate!=startDate).ToList();
                List<EY.com.eBook.BizTax.ContextElementDataContract> cedce = bzt.Contexts.Where(c => !string.IsNullOrEmpty(c.Period.EndDate) && c.Period.EndDate!=endDate).ToList();
                List<EY.com.eBook.BizTax.ContextElementDataContract> cedcis = bzt.Contexts.Where(c => c.Id.StartsWith("I-Start") && c.Period.Instant != iStart).ToList();
                List<EY.com.eBook.BizTax.ContextElementDataContract> cedcie = bzt.Contexts.Where(c => c.Id.StartsWith("I-End") && c.Period.Instant != endDate).ToList();

                bool save = false;
                if (cedcs.Count > 0)
                {
                    cedcs.ForEach(c => { c.Period.StartDate = startDate; });
                    
                    Console.Write("...cleaned period start dates");
                    save = true;
                }
                 if (cedce.Count > 0)
                {
                    cedce.ForEach(c => { c.Period.EndDate = endDate; });
                    
                    Console.Write("...cleaned period end dates");
                    save = true;
                }
                if (cedcis.Count > 0)
                {
                    cedcis.ForEach(c => { c.Period.Instant = iStart; });
                    
                    Console.Write("...cleaned instant start dates");
                    save = true;
                }
                if (cedcie.Count > 0)
                {
                    cedcie.ForEach(c => { c.Period.Instant = endDate; });
                    
                    Console.Write("...cleaned instant end dates");
                    save = true;
                }
                var xe = bzt.Elements.FirstOrDefault(x => x.Name == "PeriodStartDate");
                if (xe != null && xe.Value != startDate)
                {
                    xe.Value = startDate;
                    save = true;
                }
                xe = bzt.Elements.FirstOrDefault(x => x.Name == "PeriodEndDate");
                if (xe != null && xe.Value != endDate)
                {
                    xe.Value = endDate;
                    save = true;
                }


                /*
                cedcs = bzt.Contexts.Where(c => c.Scenario!=null && c.Scenario.Count(s=>s.Type=="d-ty:date")>0).ToList();
                if (cedcs.Count > 0)
                {
                    cedcs.ForEach(c => { 
                        c.Scenario.Where(s=>s.Type=="d-ty:date").ToList().ForEach(s=> { s.Type="d-ty:DateTypedID"; });
                    });
                    fx.XbrlData = CoreHelper.SerializeToString(bzt);
                    Console.Write("...cleaned d-ty:date to DateTypedID");
                    save = true;
                } * */

                if (save)
                {
                    fx.XbrlData = CoreHelper.SerializeToString(bzt);
                    eBookWriteManager.Context.SaveChanges();
                }
                

            }
            return fx;

        }

        static void CleanDubProv(Guid fileId)
        {
            
            FileXbrl fx = eBookWriteManager.Context.FileXbrlSet.FirstOrDefault(f => f.FileId == fileId);
            if (fx != null && fx.StatusId < 10)
            {
                EY.com.eBook.BizTax.Contracts.BizTaxDataContract bzt = CoreHelper.DeserializeFromString<EY.com.eBook.BizTax.Contracts.BizTaxDataContract>(fx.XbrlData);
                bzt.LastSaved = null;
                //&& string.IsNullOrEmpty(c.PresContextId)
                List<EY.com.eBook.BizTax.ContextElementDataContract> cedcs = bzt.Contexts.Where(c => !string.IsNullOrEmpty(c.Id) && (c.Id.Contains("WriteDownDebt") || c.Id.Contains("ProvisionRisk")) ).ToList();
                foreach (EY.com.eBook.BizTax.ContextElementDataContract cedc in cedcs)
                {
                    bzt.Elements.RemoveAll(e => e.ContextRef == cedc.Id);
                    bzt.Contexts.RemoveAll(c => c.Id == cedc.Id);
                }
                bzt.Errors = new List<EY.com.eBook.BizTax.BizTaxErrorDataContract>();
                fx.XbrlData = CoreHelper.SerializeToString(bzt);
                eBookWriteManager.Context.SaveChanges();
                Console.Write("...cleaned");
            }
        }

        static void CorrectMissingPrefix(FileXbrl fx)
        {
          //  nw.BizTaxDataContract nwBtEx = CoreHelper.DeserializeFromString<nw.BizTaxDataContract>(fx.XbrlData);
            XNamespace xn = XNamespace.Get("http://schemas.datacontract.org/2004/07/EY.com.eBook.BizTax");
            XNamespace xni = XNamespace.Get("http://www.w3.org/2001/XMLSchema-instance");
            XDocument xdoc = XDocument.Parse(fx.XbrlData);
            XmlReader xr = xdoc.CreateReader();
            XmlNamespaceManager xnm = new XmlNamespaceManager(xr.NameTable);
            xr.Close();
            xnm.AddNamespace("d2p1","http://schemas.datacontract.org/2004/07/EY.com.eBook.BizTax");
            xnm.AddNamespace("i", "http://www.w3.org/2001/XMLSchema-instance");
            List<XElement> els = xdoc.XPathSelectElements("//d2p1:XbrlElementDataContract[not(./d2p1:Prefix/text()) or ./d2p1:Prefix[@i:nil]]", xnm).ToList();
            if (els.Count > 0)
            {
               
                Console.Write(fx.FileId.ToString() +  " : found " + els.Count + " occcurences");
                foreach (XElement el in els)
                {
                    XElement prel = el.Element(xn + "Prefix");
                    if (el.Parent != null && el.Parent.Parent != null && el.Parent.Parent.Element(xn + "Prefix") != null)
                    {
                        prel.Value = el.Parent.Parent.Element(xn + "Prefix").Value;
                        Console.Write(", " + el.Name.LocalName + " set val ");
                    }
                    if (prel.Attribute(xni + "nil")!=null)
                    {
                        prel.Attribute(xni + "nil").Remove();
                        Console.Write(", " + el.Name.LocalName + " removed @nil ");
                    }
                    
                }

                fx.XbrlData = xdoc.ToString();
                eBookWriteManager.Context.SaveChanges();
                Console.Write("... saved ");
            }

        }

        static void HandleFX(FileXbrl fx)
        {
            File fle = eBookWriteManager.Context.Files.FirstOrDefault(f => f.Id == fx.FileId);
            Client cle = eBookWriteManager.Context.ClientSet.FirstOrDefault(f => f.Id == fle.ClientId);
            FxMig ofx = eBookWriteManager.Context.FxMigs.FirstOrDefault(f => f.Id == fx.FileId);
            if (ofx != null)
            {
                nw.BizTaxDataContract nwBtEx = CoreHelper.DeserializeFromString<nw.BizTaxDataContract>(fx.XbrlData);
                old.BizTaxDataContract oldBt = CoreHelper.DeserializeFromString<old.BizTaxDataContract>(ofx.Data);

                nw.BizTaxDataContract nwBt = new nw.BizTaxDataContract
                {
                    AssessmentYear = int.Parse(oldBt.AssessmentYear),
                    Contexts = new List<bzt.ContextElementDataContract>(),
                    Elements = new List<bzt.XbrlElementDataContract>(),
                    EntityIdentifier = new bzt.ContextEntityDataContract
                    {
                        IdentifierScheme = oldBt.EntityIdentifier.IdentifierScheme
                        ,
                        IdentifierValue = oldBt.EntityIdentifier.IdentifierValue
                    }
                    ,
                    Errors = new List<bzt.BizTaxErrorDataContract>()
                    ,
                    LastCalculated = null
                    ,
                    LastSaved = fx.StatusId > 10 ? (DateTime?)DateTime.Now : null
                    ,
                    NameSpaces = new List<bzt.XbrlNamespace>()
                    ,
                    Partner = nwBtEx.Partner
                    ,
                    ProxyId = nwBtEx.ProxyId
                    ,
                    ProxyTitle = nwBtEx.ProxyTitle
                    ,
                    SchemaRef = nwBtEx.SchemaRef
                    ,
                    Units = new List<bzt.UnitElementDataContract>
                    {
                          new bzt.UnitElementDataContract { Measure="pure",Id="U-Pure"}
                        ,new bzt.UnitElementDataContract { Measure="iso4217:EUR",Id="EUR"}
                    }
                };

                if (cle == null || !cle.BNI)
                {
                    nwBt.SchemaRef = string.Format("be-tax-inc-rcorp-{0}-04-30.xsd", nwBt.AssessmentYear.ToString());

                }
                else
                {
                    nwBt.SchemaRef = string.Format("be-tax-inc-nrcorp-{0}-04-30.xsd", nwBt.AssessmentYear.ToString());
                }

                nwBt.Elements = TransformElements(oldBt.Elements);
                nwBt.Contexts = TransformContexts(oldBt.Contexts, nwBt.EntityIdentifier);

                string enddate = ToIso8601DateOnly(fle.EndDate);



                // CLEAN IDS
                foreach (bzt.ContextElementDataContract ce in nwBt.Contexts.Where(c => c.Scenario != null && c.Scenario.Count > 0).ToList())
                {
                    string period = "D";
                    string oldid = "";
                    string oldid2 = "";

                    if (ce.Period.Instant != null)
                    {
                        if (ce.Period.Instant == enddate)
                        {
                            period = "I-End";
                        }
                        else
                        {
                            period = "I-Start";
                        }
                    }

                    string ctxId = "";
                    foreach (bzt.ContextScenarioDataContract csdc in ce.Scenario)
                    {

                        if (string.IsNullOrEmpty(ctxId))
                        {
                            ctxId = "id__";
                            oldid = "";
                            oldid2 = "";
                        }
                        else
                        {

                            ctxId += "__id__";
                            oldid += "__";
                            oldid2 += "_";
                        }
                        if (csdc is bzt.ContextScenarioTypeDataContract)
                        {
                            ctxId += csdc.Value.CleanXbrlId();
                            oldid += csdc.Value.CleanXbrlId();
                            oldid2 += csdc.Value.CleanXbrlId();
                        }
                        else
                        {
                            ctxId += csdc.Value.Split(new char[] { ':' })[1];
                            oldid += csdc.Value.Split(new char[] { ':' })[1];
                            oldid2 += csdc.Value.Split(new char[] { ':' })[1];
                        }

                    }

                    string nwid = period + "__" + ctxId;

                    nwBt.Elements.Where(e => e.Context == oldid2).ToList()
                       .ForEach(e =>
                       {
                           e.Context = ctxId;
                       });

                    nwBt.Elements.Where(e => e.Context == oldid).ToList()
                        .ForEach(e =>
                        {
                            e.Context = ctxId;
                        });

                    nwBt.Elements.Where(e => e.ContextRef == ce.Id).ToList()
                        .ForEach(e =>
                        {
                            e.Context = ctxId;
                        });

                    ce.Id = nwid;
                }

                if (nwBt.AssessmentYear == 2013)
                {
                    bzt.AY2013.rcorp.BizTaxRenderer btr = new bzt.AY2013.rcorp.BizTaxRenderer();
                    btr.Xbrl = new nw.BizTaxDataContract { Contexts = new List<bzt.ContextElementDataContract>(), Elements = new List<bzt.XbrlElementDataContract>(), EntityIdentifier = nwBt.EntityIdentifier };
                    btr.ApplyDefaultContexts(nwBt.EntityIdentifier.IdentifierValue, fle.StartDate, fle.EndDate);

                    List<string> crefs = nwBt.Contexts.Select(c => c.Id).ToList();

                    List<bzt.ContextElementDataContract> newels = btr.Xbrl.Contexts.Where(c => !crefs.Contains(c.Id)).ToList();

                    if (newels.Count > 0)
                    {
                        nwBt.Contexts.AddRange(newels);
                    }

                    foreach (bzt.ContextElementDataContract ce in nwBt.Contexts.Where(c => c.Scenario != null && c.Scenario.Count > 0).ToList())
                    {
                        string period = "D";
                        string oldid = "";
                        string oldid2 = "";

                        if (ce.Period.Instant != null)
                        {
                            if (ce.Period.Instant == enddate)
                            {
                                period = "I-End";
                            }
                            else
                            {
                                period = "I-Start";
                            }
                        }

                        string ctxId = "";
                        foreach (bzt.ContextScenarioDataContract csdc in ce.Scenario)
                        {

                            if (string.IsNullOrEmpty(ctxId))
                            {
                                ctxId = "id__";
                                oldid = "";
                                oldid2 = "";
                            }
                            else
                            {

                                ctxId += "__id__";
                                oldid += "__";
                                oldid2 += "_";
                            }
                            if (csdc is bzt.ContextScenarioTypeDataContract)
                            {
                                ctxId += csdc.Value.CleanXbrlId();
                                oldid += csdc.Value.CleanXbrlId();
                                oldid2 += csdc.Value.CleanXbrlId();
                            }
                            else
                            {
                                ctxId += csdc.Value.Split(new char[] { ':' })[1];
                                oldid += csdc.Value.Split(new char[] { ':' })[1];
                                oldid2 += csdc.Value.Split(new char[] { ':' })[1];
                            }

                        }

                        string nwid = period + "__" + ctxId;

                        nwBt.Elements.Where(e => e.Context == oldid2).ToList()
                           .ForEach(e =>
                           {
                               e.Context = ctxId;
                           });

                        nwBt.Elements.Where(e => e.Context == oldid).ToList()
                            .ForEach(e =>
                            {
                                e.Context = ctxId;
                            });

                        nwBt.Elements.Where(e => e.ContextRef == ce.Id).ToList()
                            .ForEach(e =>
                            {
                                e.Context = ctxId;
                            });

                        ce.Id = nwid;
                    }
                }

                if (nwBt.AssessmentYear == 2012)
                {
                    bzt.AY2012.rcorp.BizTaxRenderer btr = new bzt.AY2012.rcorp.BizTaxRenderer();
                    btr.Xbrl = new nw.BizTaxDataContract { Contexts = new List<bzt.ContextElementDataContract>(), Elements = new List<bzt.XbrlElementDataContract>(), EntityIdentifier = nwBt.EntityIdentifier };
                    btr.ApplyDefaultContexts(nwBt.EntityIdentifier.IdentifierValue, fle.StartDate, fle.EndDate);

                    List<string> crefs = nwBt.Contexts.Select(c => c.Id).ToList();

                    List<bzt.ContextElementDataContract> newels = btr.Xbrl.Contexts.Where(c => !crefs.Contains(c.Id)).ToList();

                    if (newels.Count > 0)
                    {
                        nwBt.Contexts.AddRange(newels);
                    }

                    foreach (bzt.ContextElementDataContract ce in nwBt.Contexts.Where(c => c.Scenario != null && c.Scenario.Count > 0).ToList())
                    {
                        string period = "D";
                        string oldid = "";
                        string oldid2 = "";

                        if (ce.Period.Instant != null)
                        {
                            if (ce.Period.Instant == enddate)
                            {
                                period = "I-End";
                            }
                            else
                            {
                                period = "I-Start";
                            }
                        }

                        string ctxId = "";
                        foreach (bzt.ContextScenarioDataContract csdc in ce.Scenario)
                        {

                            if (string.IsNullOrEmpty(ctxId))
                            {
                                ctxId = "id__";
                                oldid = "";
                                oldid2 = "";
                            }
                            else
                            {

                                ctxId += "__id__";
                                oldid += "__";
                                oldid2 += "_";
                            }
                            if (csdc is bzt.ContextScenarioTypeDataContract)
                            {
                                ctxId += csdc.Value.CleanXbrlId();
                                oldid += csdc.Value.CleanXbrlId();
                                oldid2 += csdc.Value.CleanXbrlId();
                            }
                            else
                            {
                                ctxId += csdc.Value.Split(new char[] { ':' })[1];
                                oldid += csdc.Value.Split(new char[] { ':' })[1];
                                oldid2 += csdc.Value.Split(new char[] { ':' })[1];
                            }

                        }

                        string nwid = period + "__" + ctxId;

                        nwBt.Elements.Where(e => e.Context == oldid2).ToList()
                           .ForEach(e =>
                           {
                               e.Context = ctxId;
                           });

                        nwBt.Elements.Where(e => e.Context == oldid).ToList()
                            .ForEach(e =>
                            {
                                e.Context = ctxId;
                            });

                        nwBt.Elements.Where(e => e.ContextRef == ce.Id).ToList()
                            .ForEach(e =>
                            {
                                e.Context = ctxId;
                            });

                        ce.Id = nwid;
                    }
                }

                foreach (bzt.XbrlElementDataContract el in nwBt.Elements.Where(e => e.Name == "OtherReserves").ToList())
                {
                    bzt.ContextElementDataContract cedc = nwBt.Contexts.FirstOrDefault(c => c.Id == el.ContextRef);
                    if (cedc != null)
                    {
                        Guid nwguid = string.Join("/", cedc.Scenario.Select(s => s.Value).ToArray()).DeterministicGuid();
                        cedc.PresContextId = "OtherReservesTitle";
                        cedc.DefId = nwguid;
                    }
                    else
                    {
                        nwBt.Elements.RemoveAll(e => e.ContextRef == el.ContextRef);
                    }
                }

                foreach (bzt.XbrlElementDataContract el in nwBt.Elements.Where(e => e.Name == "OtherTaxableReserves").ToList())
                {
                    bzt.ContextElementDataContract cedc = nwBt.Contexts.FirstOrDefault(c => c.Id == el.ContextRef);
                    if (cedc != null)
                    {
                        Guid nwguid = string.Join("/", cedc.Scenario.Select(s => s.Value).ToArray()).DeterministicGuid();
                        cedc.PresContextId = "OtherTaxableReservesTitle";
                        cedc.DefId = nwguid;
                    }
                    else
                    {
                        nwBt.Elements.RemoveAll(e => e.ContextRef == el.ContextRef);
                    }
                }

                foreach (bzt.XbrlElementDataContract el in nwBt.Elements.Where(e => e.Name == "IdentityTradeDebtor").ToList())
                {
                    bzt.ContextElementDataContract cedc = nwBt.Contexts.FirstOrDefault(c => c.Id == el.ContextRef);
                    if (cedc != null)
                    {
                        Guid nwguid = string.Join("/", cedc.Scenario.Select(s => s.Value).ToArray()).DeterministicGuid();
                        cedc.PresContextId = "WriteDownsDebtClaimsSection";
                        cedc.DefId = nwguid;

                        nwBt.Contexts.Where(c => c.GetMyDefId() == cedc.GetMyDefId()).ToList().ForEach(d =>
                        {
                            d.PresContextId = "WriteDownsDebtClaimsSection";
                            d.DefId = nwguid;
                        });
                    }
                    else
                    {
                        nwBt.Elements.RemoveAll(e => e.ContextRef == el.ContextRef);
                    }
                }

                foreach (bzt.XbrlElementDataContract el in nwBt.Elements.Where(e => e.Name == "ProbableCost").ToList())
                {
                    bzt.ContextElementDataContract cedc = nwBt.Contexts.FirstOrDefault(c => c.Id == el.ContextRef);
                    if (cedc != null)
                    {
                        Guid nwguid = string.Join("/", cedc.Scenario.Select(s => s.Value).ToArray()).DeterministicGuid();
                        cedc.PresContextId = "ProvisionsRisksExpensesSection";
                        cedc.DefId = nwguid;
                        nwBt.Contexts.Where(c => c.GetMyDefId() == cedc.GetMyDefId()).ToList().ForEach(d =>
                        {
                            d.PresContextId = "ProvisionsRisksExpensesSection";
                            d.DefId = nwguid;
                        });
                    }
                    else
                    {
                        nwBt.Elements.RemoveAll(e => e.ContextRef == el.ContextRef);
                    }
                }


                foreach (bzt.XbrlElementDataContract el in nwBt.Elements.Where(e => e.Name == "CalculationWeightedAverageMovementEquityAfterDeductionsAllowanceCorporateEquity" || e.Name == "MovementEquityAfterDeductionsAllowanceCorporateEquity").ToList())
                {
                    if (!string.IsNullOrEmpty(el.Context))
                    {
                        bzt.ContextElementDataContract cedc = nwBt.Contexts.FirstOrDefault(c => c.Id == el.ContextRef);
                        if (cedc != null)
                        {
                            Guid nwguid = string.Join("/", cedc.Scenario.Select(s => s.Value).ToArray()).DeterministicGuid();
                            cedc.PresContextId = "ApplicableDurationMovementEquityAllowanceCorporateEquity";
                            cedc.DefId = nwguid;
                        }
                        else
                        {
                            nwBt.Elements.RemoveAll(e => e.ContextRef == el.ContextRef);
                        }
                    }
                }


                List<bzt.ContextElementDataContract> newCtxs = nwBtEx.Contexts.Where(c => nwBt.Contexts.Count(d => d.Id == c.Id) == 0).ToList();
                List<bzt.XbrlElementDataContract> newEls = nwBtEx.Elements.Where(e => nwBt.Elements.Count(d => d.Id == e.Id) == 0).ToList();

                if (newCtxs.Count > 0) nwBt.Contexts.AddRange(newCtxs);
                if (newEls.Count > 0) nwBt.Elements.AddRange(newEls);

                nwBt.Contexts.RemoveAll(c => c.Scenario != null && c.Scenario.Count > 0 && c.Scenario.Count(s => s.Name == "typedMember") > 0
                                    && nwBt.Elements.Count(e => e.ContextRef == c.Id) == 0);

                fx.XbrlData = CoreHelper.SerializeToString(nwBt);
                eBookWriteManager.Context.SaveChanges();
            }
        }


        private static List<bzt.ContextElementDataContract> TransformContexts(List<old.ContextElementDataContract> list, bzt.ContextEntityDataContract ent)
        {
            List<bzt.ContextElementDataContract> newlist = new List<bzt.ContextElementDataContract>();

            foreach(old.ContextElementDataContract oc in list) {
                bzt.ContextElementDataContract ce = new bzt.ContextElementDataContract
                {
                    Id = oc.Id
                    ,
                    DefId = null
                    ,
                    Entity = ent
                    ,
                    Period = new bzt.ContextPeriodDataContract
                    {
                        Instant = oc.Period.Instant
                        , EndDate = oc.Period.EndDate
                        , StartDate = oc.Period.EndDate
                    }
                    , Scenario = new List<bzt.ContextScenarioDataContract>()
                    , PresContextId =null
                };

                if (oc.Scenario != null && oc.Scenario.Count > 0)
                {
                    foreach (old.ContextScenarioDataContract os in oc.Scenario)
                    {
                        if (os.Name=="explicitMember")
                        {
                            ce.Scenario.Add(new bzt.ContextScenarioExplicitDataContract
                            {
                                Dimension = os.Dimension
                                , Type = os.Type
                                , Value= os.Value
                            });
                        }
                        else 
                        {
                            ce.Scenario.Add(new bzt.ContextScenarioTypeDataContract
                            {
                                Dimension = os.Dimension
                                ,
                                Type = os.Type
                                ,
                                Value = os.Value
                            });
                        }
                    }
                }

                newlist.Add(ce);
            }


            return newlist;
        }

        private static List<bzt.XbrlElementDataContract> TransformElements(List<old.XbrlElementDataContract> list)
        {
            List<bzt.XbrlElementDataContract> newlist = new List<bzt.XbrlElementDataContract>();

            foreach (old.XbrlElementDataContract oxel in list)
            {
                newlist.Add(new bzt.XbrlElementDataContract
                {
                    AutoRendered = oxel.AutoRendered,
                    BinaryValue = oxel.BinaryValue,
                    Calculated = false,
                    Locked = false,
                    Decimals = oxel.Decimals,
                    UnitRef = oxel.UnitRef,
                    Prefix = oxel.Prefix,
                    Name = oxel.Name,
                    NameSpace = oxel.NameSpace,
                    Value = oxel.Value,
                    Context = oxel.Context,
                    Period = oxel.Period,
                    Children = oxel.Children == null ? null : TransformElements(oxel.Children)
                });
            }

            return newlist;
        }


        

        /*
         <AutoRendered>false</AutoRendered>
      <Prefix>tax-inc</Prefix>
      <NameSpace />
      <Name>ExemptWriteDownDebtClaim</Name>
      <ContextRef>I-Start</ContextRef>
      <Decimals>INF</Decimals>
      <UnitRef>EUR</UnitRef>
      <Value>0</Value>
      <Children i:nil="true" />
      <Id>tax-inc_ExemptWriteDownDebtClaim_I-Start</Id>
      <Context />
      <Period>I-Start</Period>
      <BinaryValue i:nil="true" />
         * 
          <d2p1:AutoRendered>true</d2p1:AutoRendered>
      <d2p1:Prefix>tax-inc</d2p1:Prefix>
      <d2p1:NameSpace />
      <d2p1:Name>PeriodStartDate</d2p1:Name>
      <d2p1:ContextRef>I-Start</d2p1:ContextRef>
      <d2p1:Decimals />
      <d2p1:UnitRef />
      <d2p1:Value>2012-01-01</d2p1:Value>
      <d2p1:Children i:nil="true" />
      <d2p1:FullName>tax-inc:PeriodStartDate</d2p1:FullName>
      <d2p1:Id>tax-inc_PeriodStartDate_I-Start</d2p1:Id>
      <d2p1:Calculated>false</d2p1:Calculated>
      <d2p1:Context />
      <d2p1:Locked>true</d2p1:Locked>
      <d2p1:Period>I-Start</d2p1:Period>
      <d2p1:BinaryValue xmlns:d4p1="http://schemas.datacontract.org/2004/07/EY.com.eBook.API.Contracts.Data" i:nil="true" />
   
         */
    }
}
