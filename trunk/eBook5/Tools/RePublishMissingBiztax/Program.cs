﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EY.com.eBook.Core.EF;
using EY.com.eBook.Core.EF.Write;

namespace RePublishMissingBiztax
{
    class Fi
    {
        public string Path { get; set; }
        public string Id { get; set; }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Searching for files");
            string[] files = System.IO.Directory.GetFiles(@"\\bemechmfiles01.eurw.ey.net\Projects\ACR Central Processing\BizTax\AANSLAGJAAR_2013", "*.ebook", System.IO.SearchOption.AllDirectories);

            List<Fi> fileinfs = files.Select(f => new Fi { Id = System.IO.Path.GetFileNameWithoutExtension(f).ToLower(), Path = f }).ToList();

            files = fileinfs.Select(f=>f.Id).ToArray();
            Console.WriteLine("{0} files found", files.Length);

            DateTime dte = new DateTime(2012,12,30);
            List<Guid> fids2013 = eBookWriteManager.Context.Files.Where(f => f.EndDate > dte).Select(f=>f.Id).ToList();

            
            List<Guid> fids = eBookWriteManager.Context.FileXbrlSet.Where(f=>f.StatusId >= 10 && f.DeclaringDepartment=="ACR").Select(f=>f.FileId).ToList();
            fids = fids.Where(f => fids2013.Contains(f)).ToList();
            Console.WriteLine("{0} send ACR files", fids.Count);
            fids = fids.Where(f=>!files.Contains(f.ToString().ToLower())).ToList();

             Console.WriteLine("Found {0} missing", fids.Count);

            Console.WriteLine("Getting detailed list (client names)");
            List<EY.com.eBook.Core.EF.Read.File> filesCl = eBookReadManager.Context.Files.Include("Client").ToList().Where(f => fids.Contains(f.Id)).ToList();

            int i = 0;
            foreach (EY.com.eBook.Core.EF.Read.File fle in filesCl.OrderBy(f=>f.Client.Name))
             {
                 i++;
                List<Fi> fiss = fileinfs.Where(f=>f.Id==fle.Id.ToString().ToLower()).ToList();
                 Console.WriteLine("- {0}", fle.Client.Name);
                 foreach (Fi fi in fiss)
                 {
                     Console.WriteLine("   > {0}", fi.Path.Replace(@"\\bemechmfiles01.eurw.ey.net\Projects\ACR Central Processing\BizTax\AANSLAGJAAR_2013",""));
                 }
                 if (i % 20 == 0)
                 {
                     Console.WriteLine("--- pres key ---");
                     Console.ReadLine();
                 }
             }
            Console.WriteLine("Found {0} missing", fids.Count);


             Console.WriteLine("PRESS ANY KEY TO START REPUBLISH, X to QUIT without");
             string key = Console.ReadKey().KeyChar.ToString();
             if (key.ToLower() != "x")
             {
                 Console.WriteLine("Start republish");
                 foreach (EY.com.eBook.Core.EF.Read.File fle in filesCl)
                 {
                     int ay = fle.EndDate.AddDays(1).Year;
                     FileXbrl fx = eBookWriteManager.Context.FileXbrlSet.First(f => f.FileId == fle.Id);
                     Console.WriteLine("- {0} (AJ {1} {2})", fle.Client.Name, ay, fx.XbrlType);

                     switch (fx.XbrlType)
                     {
                         case "nrcorp":
                             EY.com.eBook.BizTax.AY2013.nrcorp.FileBizTaxRepository nr = new EY.com.eBook.BizTax.AY2013.nrcorp.FileBizTaxRepository();
                             nr.Publish(new EY.com.eBook.BizTax.Contracts.Criteria.CriteriaBiztaxDataContract
                             {
                                 FileId = fx.FileId
                                 , Culture = fle.Culture
                                 , Type ="nrcorp"
                             });
                             break;
                         case "rcorp":
                             EY.com.eBook.BizTax.AY2013.rcorp.FileBizTaxRepository rc = new EY.com.eBook.BizTax.AY2013.rcorp.FileBizTaxRepository();
                             rc.Publish(new EY.com.eBook.BizTax.Contracts.Criteria.CriteriaBiztaxDataContract
                             {
                                 FileId = fx.FileId
                                 ,
                                 Culture = fle.Culture
                                 ,
                                 Type = "rcorp"
                             });
                             break;

                     }
                 }
             }
            

             Console.WriteLine("Done");
        }
    }
}
