﻿/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Created by:   Timothy Haerinck                                                                                       //
//Use:          Execute and answer questions regarding Assessment Year. Check log afterwards! (see CreateNewAY/log)    //
//              If you want to restart, you could simply remove the newly created RuleEngine<year>.svc as              //
//              the system uses the svc file with highest year it can find in order to determine the year.             //
//              MANUAL CHANGES STILL REQUIRED, SEE OUTPUT AFTER EXECUTION.                                             //
//Purpose:      Execution will create the necessary folders and cs-files for a new Assessment Year                     //
//              based on the previous year. It will replace any year string found in a cs file or foldername.          //
//              Since this is a pure string replacement, it means that the script isn't bulletproof,                   //
//              but it beats having to do it manually.                                                                 //
//              The script also updates config files for the cachign and ruleengine projects.                          //
//Data:         Source files and folders are dynamically defined WITHIN this script. Please review.                    //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.XPath;

namespace CreateNewAY
{
    class Program
    {

        static void Main(string[] args)
        {
            new CreateNewAY().Execute();
        }
    }

    class CreateNewAY
    {
        string appDir = @"Tools\CreateNewAY";
        string rootDir = @"C:\Projects_svn\eBook\eBook5\";
        int iPreviousYear = 0;
        int iCurrentYear = 0;
        int iNewYear = 0;
        string sPreviousYear = string.Empty;
        string sCurrentYear = string.Empty;
        string sNewYear = string.Empty;
        List<string> log = new List<string>();
        List<string> created = new List<string>();
        List<string> filesContentUpdated = new List<string>();
        List<string> linesUpdated = new List<string>();

        public void Execute()
        {
            Console.WriteLine("/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////");
            Console.WriteLine("//Created by:   Timothy Haerinck                                                                                       //");
            Console.WriteLine("//Use:          Execute and answer questions regarding Assessment Year. Check log afterwards! (see CreateNewAY/log)    //");
            Console.WriteLine("//              If you want to restart, you could simply remove the newly created RuleEngine<year>.svc as              //");
            Console.WriteLine("//              the system uses the svc file with highest year it can find in order to determine the year              //");
            Console.WriteLine("//Purpose:      Execution will create the necessary folders and cs-files for a new Assessment Year                     //");
            Console.WriteLine("//              based on the previous year. It will replace any year string found in a cs file or foldername.          //");
            Console.WriteLine("//              Since this is a pure string replacement, it means that the script isn't bulletproof,                   //");
            Console.WriteLine("//              but it beats having to do it manually.                                                                 //");
            Console.WriteLine("//              The script also updates config files for the cachign and ruleengine projects.                          //");
            Console.WriteLine("//Data:         Source files and folders are dynamically defined WITHIN this script. Please review.                    //");
            Console.WriteLine("/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////");
            Console.WriteLine(Environment.NewLine);
            Console.WriteLine("This will create the next AY. Do you want to continue? (y/n)");
            string userInput = Console.ReadLine();
            string serviceFilePath = string.Empty;

            if (userInput == "y" || userInput == "Y")
            {
                if (System.IO.Directory.Exists(@"C:\Projects_svn\eBook\eBook5\RuleEngine\EY.com.eBook.RuleEngine"))
                {
                    //////////RETRIEVE CURRENT ASSESSMENT YEAR (using rule engine service file)///////////////
                    System.IO.DirectoryInfo diRuleEngine = new System.IO.DirectoryInfo(@"C:\Projects_svn\eBook\eBook5\RuleEngine\EY.com.eBook.RuleEngine");

                    foreach (FileInfo fi in diRuleEngine.GetFiles())
                    {
                        if (fi.Extension == ".svc" && fi.Name.StartsWith("RuleEngine")) //if service<year>.svc file
                        {
                            string sServiceYear = Regex.Match(fi.Name, @"\d{4}").Value;
                            int iServiceYear = 0;

                            if (!string.IsNullOrEmpty(sServiceYear) && Int32.TryParse(sServiceYear, out iServiceYear) && iCurrentYear < iServiceYear) //if year in filename is higher, set year
                            {
                                iCurrentYear = iServiceYear;
                                sCurrentYear = sServiceYear;
                                serviceFilePath = fi.FullName;
                            }
                        }
                    }

                    if (iCurrentYear > 0) //year retrieved
                    {
                        iPreviousYear = iCurrentYear - 1;
                        sPreviousYear = iPreviousYear.ToString();
                        iNewYear = iCurrentYear + 1;
                        sNewYear = iNewYear.ToString();

                        Console.WriteLine(string.Format("Is the new assessment year {0}? (y/n)", sNewYear));
                        userInput = Console.ReadLine();

                        if (userInput == "y" || userInput == "Y")
                        {
                            int missingPath = 0;
                            List<string> toCopy = new List<string>();
                            List<string[]> toUpdate = new List<string[]>();

                            //////////ITEMS (files or folders) TO COPY///////////////
                            toCopy.Add(serviceFilePath); //Backend: RuleEngine service file
                            toCopy.Add(Path.Combine(rootDir,@"RuleEngine\EY.com.eBook.RuleEngine.Contracts", String.Concat("AY", sCurrentYear))); //Backend: RuleEngine contracts folder
                            toCopy.Add(Path.Combine(rootDir,@"RuleEngine\EY.com.eBook.RuleEngine.Data", String.Concat("AY", sCurrentYear))); //Backend: Ruleengine data folder
                            toCopy.Add(Path.Combine(rootDir,@"Content\APPS\Resources\Templates\Worksheets",sCurrentYear)); //Backend: Worksheet templates
                            toCopy.Add(Path.Combine(rootDir,@"Web\EY.com.eBook.Web\js\Worksheets", String.Concat(sCurrentYear,"-en-US.js"))); //Frontend: Worksheet localizations
                            toCopy.Add(Path.Combine(rootDir,@"Web\EY.com.eBook.Web\js\Worksheets", String.Concat(sCurrentYear,"-fr-FR.js"))); //Frontend: Worksheet localizations
                            toCopy.Add(Path.Combine(rootDir,@"Web\EY.com.eBook.Web\js\Worksheets", String.Concat(sCurrentYear,"-nl-BE.js"))); //Frontend: Worksheet localizations

                            //////////ITEMS AND VALUE (files) TO UPDATE//////////////
                            ///XML: Location, targetElement, ElementToAdd (with year dynamicallly added)
                            toUpdate.Add(new string[3] { Path.Combine(rootDir, @"Caching\EY.com.eBook.CachingService\App.config"), @"system.serviceModel/client", @"<endpoint address=""net.pipe://localhost/RuleEngine" + sNewYear + @""" binding=""netNamedPipeBinding"" bindingConfiguration=""cacheBinding"" contract=""EY.com.eBook.RuleEngine.IRuleEngine" + sNewYear + @""" name=""WebServiceProxyFactory"" behaviorConfiguration=""protobuf"" />" }); //Backend: Caching app.config
                            toUpdate.Add(new string[3] { Path.Combine(rootDir, @"Caching\EY.com.eBook.CachingService\App.config"), @"system.serviceModel/services", @"<service name=""EY.com.eBook.RuleEngine.RuleEngine" + sNewYear + @""" behaviorConfiguration=""RuleEngineBehavior""><endpoint binding=""netNamedPipeBinding"" bindingConfiguration=""cacheBinding"" behaviorConfiguration=""CachingBehavior"" contract=""EY.com.eBook.RuleEngine.IRuleEngine" + sNewYear + @""" address=""net.pipe://localhost/RuleEngine" + sNewYear + @""" /><endpoint address=""net.pipe://localhost/RuleEngine" + sNewYear + @"/Mex"" binding=""mexNamedPipeBinding"" contract=""EY.com.eBook.RuleEngine.IRuleEngine" + sNewYear + @""" /></service>" }); //Backend: Caching app.config
                            toUpdate.Add(new string[3] { Path.Combine(rootDir, @"RuleEngine\EY.com.eBook.RuleEngine\Web.config"), @"system.serviceModel/client", @"<endpoint address=""net.pipe://localhost/RuleEngine" + sNewYear + @""" binding=""netNamedPipeBinding"" bindingConfiguration=""cacheBinding"" contract=""EY.com.eBook.RuleEngine.IRuleEngine" + sNewYear + @""" name=""WebServiceProxyFactory"" behaviorConfiguration=""protobuf"" />" }); //Backend: RuleEngine web.config
                            toUpdate.Add(new string[3] { Path.Combine(rootDir, @"RuleEngine\EY.com.eBook.RuleEngine\Web.config"), @"system.serviceModel/services", @"<service behaviorConfiguration=""eBookWebServiceBehavior"" name=""EY.com.eBook.Utils.Services.Clients.IRuleEngine" + sNewYear + @"Proxy""><endpoint address=""http://localhost/EY.com.eBook.RuleEngine.5/RuleEngine" + sNewYear + @".svc"" behaviorConfiguration=""eBookWebEndPointBehavior"" binding=""webHttpBinding"" bindingConfiguration=""eBookServiceBinding"" contract=""EY.com.eBook.RuleEngine.IRuleEngine" + sNewYear + @""" /></service>" }); //Backend: RuleEngine web.config

                            Console.WriteLine("The following items will be copied:");
                            Console.WriteLine("------------------------------------------------------------------");

                            foreach (string path in toCopy)
                            {
                                if (System.IO.Directory.Exists(path) || System.IO.File.Exists(path))
                                {
                                    Console.WriteLine(path.Replace(rootDir, ""));
                                }
                                else
                                {
                                    missingPath++;

                                    if (missingPath == 1)
                                    {
                                        Console.WriteLine(Environment.NewLine);
                                        Console.WriteLine("The following items are missing:");
                                        Console.WriteLine("------------------------------------------------------------------");
                                    }

                                    Console.WriteLine(path.Replace(rootDir, ""));
                                }
                            }

                            Console.WriteLine(Environment.NewLine);
                            Console.WriteLine("------------------------------------------------------------------");
                            Console.WriteLine("Do you want to copy above files? No will continue the script. (y/n)");
                            Console.WriteLine("------------------------------------------------------------------");
                            userInput = Console.ReadLine();

                            if (userInput == "y" || userInput == "Y")
                            {
                                //////////LOOP THE TO BE CREATED FILES AND FOLDERS///////////////
                                foreach (string path in toCopy)
                                {
                                    if (System.IO.Directory.Exists(path)) //is dir?
                                    {
                                        copyDirForAY(new DirectoryInfo(path), null); //copy and create dir with all year occurences replaced in subfiles and subfolders
                                    }
                                    else if (System.IO.File.Exists(path)) //is file
                                    {
                                        replaceAllYearOccurencesInFile(copyFileForAY(new FileInfo(path), null)); //copy and create file with all year occurences replaced
                                    }
                                }
                            }
                            Console.WriteLine(Environment.NewLine);
                            Console.WriteLine("The following items will be updated:");
                            Console.WriteLine("------------------------------------------------------------------");

                            foreach (string[] lItem in toUpdate)
                            {
                                string path = lItem[0];

                                if (System.IO.Directory.Exists(path) || System.IO.File.Exists(path))
                                {
                                    Console.WriteLine(path.Replace(rootDir, ""));
                                }
                                else
                                {
                                    missingPath++;

                                    if (missingPath == 1)
                                    {
                                        Console.WriteLine(Environment.NewLine);
                                        Console.WriteLine("The following items are missing:");
                                        Console.WriteLine("------------------------------------------------------------------");
                                    }

                                    Console.WriteLine(path.Replace(rootDir, ""));
                                }
                            }
                            Console.WriteLine(Environment.NewLine);
                            Console.WriteLine("------------------------------------------------------------------");
                            Console.WriteLine("Do you want to update above files? No will continue the script. (y/n)");
                            Console.WriteLine("------------------------------------------------------------------");
                            userInput = Console.ReadLine();

                            if (userInput == "y" || userInput == "Y")
                            {
                                //////////LOOP THE TO BE UPDATED FILES///////////////
                                foreach (string[] lList in toUpdate)
                                {
                                    string path = lList[0];
                                    string target = lList[1];
                                    string value = lList[2];

                                    if (System.IO.File.Exists(path)) //is file
                                    {
                                        FileInfo fi = new FileInfo(path);

                                        /////////////////UPDATE XML'S//////////////////
                                        if (fi.Extension == ".xml" || fi.Extension == ".config") //is XML
                                        {
                                            updateXML(path, target, value);
                                        }
                                    }
                                }
                            }

                            Console.WriteLine("");
                            Console.WriteLine("-----------------------------IMPORTANT--------------------------------");

                            if (created.Count() > 0)
                            {
                                Console.WriteLine("Include the following files and folders in your solution (in solution explorer, click 'show all files'):");
                        
                                foreach (string iItem in created)
                                {
                                    string path = iItem;

                                    if (System.IO.Directory.Exists(path))
                                    {
                                        Console.WriteLine("FOLDER: " + path.Replace(rootDir, ""));
                                        log.Add("FOLDER: " + path.Replace(rootDir, ""));
                                    }
                                    else if(System.IO.Directory.Exists(path))
                                    {
                                        Console.WriteLine("FILE: " + path.Replace(rootDir, ""));
                                        log.Add("FILE: " + path.Replace(rootDir, ""));
                                    }
                                }
                                Console.WriteLine("-----------------------------IMPORTANT--------------------------------");
                                Console.WriteLine("Manually update the following files:");
                                Console.WriteLine("Manually update Caching/EY.com.eBook.CachingService/eBookCacheService.cs (add type)");
                                Console.WriteLine("Manually update RuleEngine.AY"+sNewYear+".BerekeningVenBApp.cs (Calculation of IPT)");
                                //Console.WriteLine("Mannally update Web/EY.com.eBook.Web/js/eBook/Service.js"); Made it dynamic using a function as property. See file.
                            }

                            Console.WriteLine("-----------------------------IMPORTANT--------------------------------");

                            if (log.Count() > 0)
                            {
                                //////////CREATE LOG/////////////
                                File.WriteAllLines(Path.Combine(rootDir, appDir, "log", DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss") + "LogAY" + sNewYear + ".txt"), log);
                                Console.WriteLine("A log file (CreateNewAY/log) has been created showing all files and lines created...");
                            }
                            Console.WriteLine("End of script reached.");
                            System.Console.ReadKey(true); //pause
                        }
                    }
                    else
                    {
                        Console.WriteLine("Could not retrieve the year!");
                    }
                }
                else
                {
                    Console.WriteLine("Could not find project!");
                }
            }
        }

        /// <summary>
        /// Replaces the year in the filename
        /// </summary>
        /// <param name="currentFileName"></param>
        /// <returns></returns>
        string replaceYearInName(string currentName)
        {
            if (currentName.Contains(sCurrentYear))
            {
                return currentName.Replace(sCurrentYear, sNewYear);
            }
            else
            {
                return currentName;
            }
        }

        /// <summary>
        /// Repaces all occurences of the year within the filename
        /// </summary>
        /// <param name="currentFile"></param>
        /// <returns></returns>
        bool replaceAllYearOccurencesInFile(FileInfo fi)
        {
            try
            {
                List<string> linesAltered = new List<string>();

                foreach (string line in File.ReadAllLines(fi.FullName))
                {
                    if (line.Contains(sCurrentYear) || line.Contains(sPreviousYear))
                    {
                        linesAltered.Add(line);
                    }
                }

                //Replace year strings up to two year in the past from new year
                File.WriteAllText(fi.FullName, Regex.Replace(File.ReadAllText(fi.FullName), sCurrentYear, sNewYear)); //Replaces current year with new
                File.WriteAllText(fi.FullName, Regex.Replace(File.ReadAllText(fi.FullName), sPreviousYear, sCurrentYear)); //Replaces previous year with current

                //log
                foreach (string line in linesAltered)
                {
                    filesContentUpdated.Add(line);
                    Console.WriteLine(string.Format("     Line found: {0}", line));
                    log.Add(string.Format("     Line found: {0}", line));
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(string.Format("Could not replace occurences in {0}!", fi.FullName));
                System.Console.ReadKey(true);
                throw e;
            }

            return true;
        }

        /// <summary>
        /// Creates a copy of the file, replacing the year. newFullName is optional
        /// </summary>
        /// <param name="fi"></param>
        /// /// <param name="newFullName"></param>
        /// <returns></returns>
        FileInfo copyFileForAY(FileInfo fi, string destDirName)
        {
            try
            {
                string newFullName = Path.Combine(!string.IsNullOrEmpty(destDirName) ? destDirName : fi.DirectoryName, replaceYearInName(fi.Name)); //if destination not given, copy in same dir
                fi.CopyTo(newFullName);

                //log
                Console.WriteLine("------------------------------------------------------------------");
                Console.WriteLine(string.Format("Created: {0}", newFullName.Replace(rootDir, "")));
                Console.WriteLine("------------------------------------------------------------------");
                created.Add(newFullName.Replace("rootDir", ""));
                log.Add("");
                log.Add(string.Format("Created: {0}", newFullName.Replace(rootDir, "")));

                return new FileInfo(newFullName);
            }
            catch (Exception e)
            {
                Console.WriteLine(string.Format("Could not copy file {0}!", fi.FullName));
                Console.WriteLine(e.ToString());
                System.Console.ReadKey(true);
                throw new SystemException(string.Format("Could not copy file {0}!", fi.FullName));
            }
        }

        /// <summary>
        /// Creates a copy of the directory, replacing the year.
        /// </summary>
        /// <param name="fi"></param>
        /// <returns></returns>
        DirectoryInfo copyDirForAY(DirectoryInfo di, string destDirName)
        {
            try
            {
                string newFullName = Path.Combine(!string.IsNullOrEmpty(destDirName) ? destDirName : Directory.GetParent(di.FullName).FullName, replaceYearInName(di.Name)); //if destination not given, copy in same dir
                copyDir(di.FullName, newFullName, true);

                //log
                Console.WriteLine("------------------------------------------------------------------");
                Console.WriteLine(string.Format("Created: {0}", newFullName.Replace(rootDir, "")));
                Console.WriteLine("------------------------------------------------------------------");
                created.Add(newFullName.Replace("rootDir", ""));
                log.Add(string.Format("Created: {0}", newFullName.Replace(rootDir, "")));

                return new DirectoryInfo(newFullName);
            }
            catch (Exception e)
            {
                Console.WriteLine(string.Format("Could not copy directory {0}!", di.FullName));
                System.Console.ReadKey(true);
                throw new SystemException(string.Format("Could not copy directory {0}!", di.FullName));
            }
        }

        /// <summary>
        /// Copies directory and its content (adapted from msdn)
        /// </summary>
        /// <param name="sourceDirName"></param>
        /// <param name="destDirName"></param>
        /// <param name="copySubDirs"></param>
        private void copyDir(string sourceDirName, string destDirName, bool copySubDirs)
        {
            // Get the subdirectories for the specified directory.
            DirectoryInfo dir = new DirectoryInfo(sourceDirName);

            if (!dir.Exists)
            {
                Console.WriteLine("Source directory does not exist or could not be found: " + sourceDirName);
                throw new DirectoryNotFoundException("Source directory does not exist or could not be found: " + sourceDirName);
            }

            DirectoryInfo[] dirs = dir.GetDirectories();
            // If the destination directory doesn't exist, create it.
            if (!Directory.Exists(destDirName))
            {
                Directory.CreateDirectory(destDirName);
            }

            // Get the files in the directory and copy them to the new location.
            // Replace any year occurence
            FileInfo[] files = dir.GetFiles();
            foreach (FileInfo fi in files)
            {
                //string temppath = Path.Combine(destDirName, file.Name);
                //file.CopyTo(temppath, false);
                replaceAllYearOccurencesInFile(copyFileForAY(fi, destDirName));
            }

            // If copying subdirectories, copy them and their contents to new location.
            if (copySubDirs)
            {
                foreach (DirectoryInfo subdir in dirs)
                {
                    //string temppath = Path.Combine(destDirName, subdir.Name);
                    //DirectoryCopy(subdir.FullName, temppath, copySubDirs);
                    copyDirForAY(subdir, destDirName);
                }
            }
        }

        FileInfo updateXML(string targetFullName, string targetElement, string value)
        {
            try
            {
                bool valueExistsInElement = false;
                XmlTextReader xmlReader = new XmlTextReader(targetFullName);
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(xmlReader); //load xml file
                xmlReader.Close();

                XmlElement root = xmlDoc.DocumentElement;
                XmlNode targetNode = root.SelectSingleNode(targetElement); //Get target
                XmlDocumentFragment fragment = xmlDoc.CreateDocumentFragment();
                fragment.InnerXml = value; //Create element
                
                foreach(XmlNode node in targetNode.ChildNodes)
                {
                    if (node.OuterXml == fragment.InnerXml) //Element already added?
                    {
                        valueExistsInElement = true;
                    }
                }
                if (!valueExistsInElement)
                {
                    targetNode.AppendChild(fragment); //Add element

                    xmlDoc.Save(targetFullName);

                    //log
                    Console.WriteLine("------------------------------------------------------------------");
                    Console.WriteLine(string.Format("XML updated: {0}", targetFullName.Replace(rootDir, "")));
                    Console.WriteLine("------------------------------------------------------------------");
                    Console.WriteLine(string.Format("     Element added: {0}", value));
                    Console.WriteLine(string.Format("     to: {0}", targetElement));
                    created.Add(targetFullName.Replace("rootDir", ""));
                    log.Add("");
                    log.Add(string.Format("Updated: {0}", targetFullName.Replace(rootDir, "")));
                    log.Add(string.Format("     Element added: {0}", value));
                    log.Add(string.Format("     to: {0}", targetElement));
                }
                else
                {
                    //log
                    Console.WriteLine("------------------------------------------------------------------");
                    Console.WriteLine(string.Format("XML not updated: {0}", targetFullName.Replace(rootDir, "")));
                    Console.WriteLine("------------------------------------------------------------------");
                    Console.WriteLine(string.Format("     Element already exists: {0}", value));
                    Console.WriteLine(string.Format("     in: {0}", targetElement));
                    log.Add("");
                    log.Add(string.Format("Not updated: {0}", targetFullName.Replace(rootDir, "")));
                    log.Add(string.Format("     Element already exists: {0}", value));
                    log.Add(string.Format("     in: {0}", targetElement));
                }

                return new FileInfo(targetFullName);
            }
            catch (Exception e)
            {
                Console.WriteLine(string.Format("Could not update XML file {0}!", targetFullName));
                Console.WriteLine(e.ToString());
                System.Console.ReadKey(true);
                throw new SystemException(string.Format("Could not update XML file {0}!", targetFullName));
            }
        }
    }
}
