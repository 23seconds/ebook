﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EY.com.eBook.API.BL.Write;
using EY.com.eBook.Core;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.Core.EF.Write;
using EY.com.eBook.Core.EF;
//using System.Data.Objects;
using System.ServiceModel;
using EY.com.eBook.Core.Data;
using EY.com.eBook.Caching;
using System.Xml.Linq;
using EY.com.eBook.RuleEngine;
using System.IO;
using EY.com.eBook.RuleEngine.AY2012;
using EY.com.PMT.API.Contracts.Data.Criteria;
using EY.com.PMT.API.BL.Repository;

namespace UpdateBizTaxDeclaration
{
    class Program
    {
        static Guid[] _clientsError = new Guid[]
        {
	        new Guid("9FE3494D-A49A-434B-8E44-012AEDC51560"),new Guid("2D901A34-022F-42E0-8C15-0153BB66FDD2"),new Guid("41BB975C-2563-4622-B68C-01FBDBE1974A"),new Guid("9B3D98DF-C408-4214-A322-09D100F931EA"),new Guid("3D913928-6705-403D-8BF5-0ADFEDEA895E"),new Guid("BFD567E9-0F1A-4A3D-A877-0ECBBE9FDAB2"),new Guid("20E2DC57-5D24-45D9-8032-17B4D44DB2B3"),new Guid("D34082BA-A3EE-4FE9-A266-18FCEDF80931"),new Guid("BC84E669-3650-47BD-AF33-1BF77337C6E4"),new Guid("5F9ED6FE-6D03-4F7B-B970-1C79B139E5D7"),new Guid("26417AE8-2D0D-4EBD-8313-1D76E68183DB"),new Guid("CB69FED4-27D2-48E6-9745-1F25C77BCEF9"),new Guid("5015AB5E-B818-4421-90FE-24BD8EEAE1ED"),new Guid("63AF44BA-A56B-4A83-BD83-2B9ED0296E7F"),new Guid("C67F41AB-FF04-419F-8296-31A9973A3BC7"),new Guid("B9746876-69CA-40ED-B41C-37FA116C31EB"),new Guid("22305E13-E762-4844-9FF0-3AD3F5768EB7"),new Guid("13C8E885-60F3-4FB6-8A40-3D46A7A8263A"),new Guid("80107CCE-BBE1-41FD-8814-3E9EB7CC0A73"),new Guid("2EC8D7F3-A43B-4D91-BBC1-411A2A3C4357"),new Guid("1C0AA207-2436-417E-B539-417B03390AB1"),new Guid("008A945E-C44B-4D23-BF0C-489ADB3E7790"),new Guid("36533982-99C8-4794-8812-4E70E0EC80B1"),new Guid("CE89B4DD-D810-4776-BD97-4F1BB42105E8"),new Guid("9EA943BE-3586-4262-8E51-4FF4B2EE5FF6"),new Guid("A66C57C5-6F4C-4CA3-812E-54A8E3C55A50"),new Guid("5F9F8720-4FAA-4A50-9594-5BD62E42F5C8"),new Guid("2388CD05-54D6-426C-8785-5F4A20EB99C6"),new Guid("2FC1B80B-5904-45AE-B889-611C1AE89A33"),new Guid("FC268F75-595C-4955-BCD5-62F7AC4726E3"),new Guid("0847B42B-6CF9-4278-AAE7-6381E2DE221A"),new Guid("BCA2DBEB-8D43-44BE-9704-63AC6BD9D659"),new Guid("4FB51044-CBE4-475A-8781-6BAB1D681AE0"),new Guid("40D2BC0A-3482-451E-9133-6E3AB35FE7E6"),new Guid("50D842C4-8615-42B1-9E64-6E98500F482C"),new Guid("7F26EDED-0272-4C3A-A805-6F5B84F3BFFD"),new Guid("9293B3BA-948D-4E04-821C-701E3E39ECA2"),new Guid("EABF1DFD-BE2D-4A0C-9999-723BDFA7372E"),new Guid("FEA9BBC2-2B1F-43FB-8900-74F3388BF507"),new Guid("A697FCE7-685B-4960-A93F-757E81547F59"),new Guid("3851562A-16CA-4F8B-9466-7703AE909ABD"),new Guid("3BA42BB9-4E85-4DF7-9889-7CB93F43905A"),new Guid("6C85E8DB-78DA-481D-9AB0-7F9440D7DE7A"),new Guid("2CB56CB5-A2E7-4F46-AE1B-80C3FE144D28"),new Guid("9F063AC2-03EC-4F8A-9146-8140F2D9F47B"),new Guid("152F6B0C-DF4A-4857-AA46-82FB1A1F30C7"),new Guid("D50650EB-973E-4F23-95C5-8436D8877411"),new Guid("D934BEAE-329F-438F-9438-87D870502B81"),new Guid("C44DEB92-3AFC-44BB-BA1A-8EB653F5D1C2"),new Guid("DCF76F99-F393-4FAE-BBF0-8FB350EC7A95"),new Guid("2EAB60EE-5180-481D-92AF-93E86C4311C2"),new Guid("E3815CC0-C7E6-4B62-8654-979F5CB97A25"),new Guid("2C1C09F7-2495-4276-86F0-9D7D62A7688D"),new Guid("55A3C891-83A7-493E-BD68-9E189BDA165F"),new Guid("FAA19723-5C23-4016-90D9-9FCE77432FBA"),new Guid("FD1FF1F4-942C-49EC-B75B-A3EC51DD7439"),new Guid("A6B271DC-4237-4DC2-9212-A6E971DF11BD"),new Guid("C6FEE455-BE57-4D30-BFD0-AB28940F46FA"),new Guid("4501E05F-4909-459A-AAAB-AB376E37148C"),new Guid("222A7DC2-1C24-4B6A-8FD7-ABBD77BC2E90"),new Guid("AC459678-94A6-4605-8240-AE028CF73B7C"),new Guid("A57C766B-BD41-4CA8-B4DC-B0B836305A0E"),new Guid("8B82692A-7944-4EEC-92AB-B0D377A245AA"),new Guid("D0BD81A5-15AF-4034-BB67-B1395355F649"),new Guid("AC4EA196-AC72-4D7A-9819-B1593F349DAB"),new Guid("C3BF5C7B-E310-4F43-893A-B237745F1B35"),new Guid("566E8A7B-2437-463F-A2C1-B85C40937685"),new Guid("85CBAD09-533A-4646-B528-B895EDAC5BA8"),new Guid("BBC5D7A3-09B3-44DF-BC65-BAA3DEF62705"),new Guid("201BBDA3-76AC-4939-8391-BEF7BE7B1EB8"),new Guid("D41FF509-2924-4EEB-A057-C3B4E7CF4587"),new Guid("E7AAF06A-C6E0-4A48-B871-C84A2C855D65"),new Guid("18D28685-752F-4788-8905-CA7A5C774435"),new Guid("7FEC6064-A716-480E-8ADF-D8B850B987F2"),new Guid("1059EC62-BB68-4438-9D20-D97567BD878E"),new Guid("C34715EE-E8FC-454F-A71C-E2CDA8B84190"),new Guid("7F6655F9-36F1-4FF7-A7E8-EDC70EC77FCE"),new Guid("FD5FF7F3-C52F-4AB7-AFA5-F300EB87BA6A"),new Guid("254236AB-DCF2-4BA2-8579-F3E6B560892B"),new Guid("A55B2549-C078-4B5E-97DC-FB4010130E8C")
        };

        static void Main(string[] args)
        {
            using (System.IO.StreamWriter file = new System.IO.StreamWriter(Directory.GetCurrentDirectory() + "\\BiztaxDeclaration_log.txt", true))
            {
                file.WriteLine(DateTime.Now.ToString("HH:mm:ss.fff") + " :: Map network drive");
            }
                string dte = string.Format("{0}_{1}_{2}-{3}_{4}_{5}", DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second);

                
                NetworkDrive nd = new NetworkDrive();
                try
                {
                    System.IO.DriveInfo[] drives = System.IO.DriveInfo.GetDrives();
                    System.IO.DriveInfo xdrive = drives.FirstOrDefault(d => d.Name == "X:\\");// StartsWith("X"));

                    /*
                        try
                        {
                            nd.LocalDrive = "X:";
                            nd.ShareName = @"\\bemechmfiles01.eurw.ey.net\Projects\ACR Central Processing\BizTax\AANSLAGJAAR_2014";
                            nd.MapDrive();
                        }
                        catch (Exception e)
                        {
                            nd.UnMapDrive();
                            nd.ShareName = @"\\bemechmfiles01.eurw.ey.net\Projects\ACR Central Processing\BizTax\AANSLAGJAAR_2014";
                            nd.MapDrive();
                        }
                    
                        foreach (string d in Directory.GetDirectories(@"X:\3_BizTax Out\").ToList().Where(di => di.Contains("Success")))
                        {
                            file.WriteLine(DateTime.Now.ToString("HH:mm:ss.fff") + " :: Update status ACR ("+d+")");
                            UpdateStatuses(@d, 99, "ACR", "LENA PLAS");
                        }
                        */

                    UpdateStatuses(@"\\bemechmfiles01.eurw.ey.net\Projects\ACR Central Processing\BizTax\AANSLAGJAAR_2014\3_BizTax Out\Success_No simulation_No download", 99, "ACR", "LENA PLAS");

                    UpdateStatuses(@"\\bemechmfiles01.eurw.ey.net\Projects\ACR Central Processing\BizTax\AANSLAGJAAR_2014\3_BizTax Out\Success_Documents to be downloaded", 99, "ACR", "LENA PLAS");
                    UpdateStatuses(@"\\bemechmfiles01.eurw.ey.net\Projects\ACR Central Processing\BizTax\AANSLAGJAAR_2014\3_BizTax Out\Success", 99, "ACR", "LENA PLAS");
                    //nd.UnMapDrive();
                    

                    /*
                    try
                    {
                        nd.ShareName = @"\\bemechmfiles01.eurw.ey.net\Projects\TAX Central Processing\BizTax\AANSLAGJAAR_2014";
                        nd.MapDrive();
                    }
                    catch
                    {
                        nd.UnMapDrive();
                        nd.ShareName = @"\\bemechmfiles01.eurw.ey.net\Projects\TAX Central Processing\BizTax\AANSLAGJAAR_2014";
                        nd.MapDrive();
                    }
                    */
                    using (System.IO.StreamWriter file = new System.IO.StreamWriter(Directory.GetCurrentDirectory() + "\\BiztaxDeclaration_log.txt", true))
                    {
                        file.WriteLine(DateTime.Now.ToString("HH:mm:ss.fff") + " :: Update status TAX");
                    }
                    UpdateStatuses(@"\\bemechmfiles01.eurw.ey.net\Projects\TAX CENTRAL PROCESSING\BizTax\AANSLAGJAAR_2014\3_BizTax Out", 99, "TAX", null);

                    Console.ReadLine();
                    //nd.UnMapDrive();

                }
                catch (Exception ex)
                {
                    using (System.IO.StreamWriter file = new System.IO.StreamWriter(Directory.GetCurrentDirectory() + "\\BiztaxDeclaration_log.txt", true))
                    {
                        file.WriteLine(DateTime.Now.ToString("HH:mm:ss.fff") + " :: Exception trying to map network drive: " + ex.Message);
                    }
                    //nd.UnMapDrive();
                    
                }
                using (System.IO.StreamWriter file = new System.IO.StreamWriter(Directory.GetCurrentDirectory() + "\\BiztaxDeclaration_log.txt", true))
                {
                    file.WriteLine(DateTime.Now.ToString("HH:mm:ss.fff") + " :: Done");
                }
            
        }

        private static void UpdateStatuses(string path, int status,string department, string defaultPartner)
         {
            //List<Guid> files = new List<Guid>();
            
            List<File> myFiles = new List<File>();
            using (System.IO.StreamWriter log = new System.IO.StreamWriter(Directory.GetCurrentDirectory() + "\\BiztaxDeclaration_log.txt", true))
            {
                log.WriteLine(DateTime.Now.ToString("HH:mm:ss.fff") + " :: Gather files");
                // Update the records
                string[] bfilesSuccess = Directory.GetFiles(path, "*.ebook", SearchOption.AllDirectories);
                int cnt = 1;
                foreach (string bfile in bfilesSuccess)
                {
                    Console.WriteLine("{0}/{1} - {2}", cnt, bfilesSuccess.Length, bfile);
                    if (!System.IO.Path.GetFileNameWithoutExtension(bfile).Contains("$"))
                    {
                        FileInfo f = new FileInfo(bfile);
                        DirectoryInfo dir = f.Directory;

                        DirectoryInfo dInfo = new DirectoryInfo(dir.FullName);
                        FileInfo[] fileInfo = dInfo.GetFiles();
                        var query = from fi in fileInfo
                                    where fi.LastWriteTime == fileInfo.Max(t => t.LastWriteTime)
                                    select fi;
                        var maxModDate = query.FirstOrDefault().LastWriteTime;
                        
                        Guid bfileID = new Guid(System.IO.Path.GetFileNameWithoutExtension(bfile)); //new Guid(bfileFull.Substring(0, bfileFull.Length - 5));
                        

                        File myFile = new File(bfileID, maxModDate, dir.FullName);
                        myFiles.Add(myFile);
                    }
                    else
                    {
                        Console.WriteLine("File with $ avoided => " + bfile);
                    }
                    cnt++;
                }
                string dte = string.Format("{0}_{1}_{2}-{3}_{4}_{5}", DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second);


                /*
                File myFile =  new File(new Guid("70AB293D-0356-4343-A527-6A81AEA4D026"), DateTime.Now, "test");
                myFiles.Add(myFile);
                */



                log.WriteLine(DateTime.Now.ToString("HH:mm:ss.fff") + " :: Processing files");
                try
                {
                    cnt = 1;
                    int filesClosed = 0;
                    int filesAlreadyClosed = 0;
                    int ttl = myFiles.Count;
                    XElement root = new XElement("root");
                    FileXbrl fx = new FileXbrl();
                    foreach (File file in myFiles) //.Skip(start).Take(end - start))
                    {
                        Console.WriteLine("{0}/{1}", cnt, ttl);
                        BizTaxDeclaration bzt = eBookWriteManager.Context.BizTaxDeclarationSet
                                            .Where(b =>
                                                    b.Id == file.id)
                                            .OrderBy(b => b.LastChanged)
                                            .FirstOrDefault();
                        //Guid errorClientId = new Guid("2D901A34-022F-42E0-8C15-0153BB66FDD2");
                        EY.com.eBook.Core.EF.Write.File fle = eBookWriteManager.Context.Files.FirstOrDefault(f => f.Id == file.id);
                        if (file != null)
                        {
                            if (bzt == null)
                            {
                                string partner = !string.IsNullOrEmpty(defaultPartner) ? defaultPartner : new DirectoryInfo(file.path).Parent.Parent.Name.Replace(".", " ");
                                EY.com.eBook.Core.EF.Write.Client cle = eBookWriteManager.Context.ClientSet.First(f => f.Id == fle.ClientId);

                                bzt = new BizTaxDeclaration
                                {
                                    AssessmentYear = 2014
                                    ,
                                    ClientEnterprise = cle.EnterpriseNumber
                                    ,
                                    ClientId = cle.Id
                                    ,
                                    ClientName = cle.Name
                                    ,
                                    Department = department
                                    ,
                                    CurrentPath = file.path
                                    ,
                                    LastChanged = file.date
                                    ,
                                    InProgressBy = ""
                                    ,
                                    Id = fle.Id
                                    ,
                                    PartnerId = Guid.Empty
                                    ,
                                    FileType = "M"
                                    ,
                                    PartnerName = partner
                                    ,
                                    SenderDate = file.date
                                    ,
                                    Status = 99
                                    ,
                                    SenderName = "AUTOMATION"
                                };
                                eBookWriteManager.Context.AddToBizTaxDeclarationSet(bzt);
                                eBookWriteManager.Context.SaveChanges();
                                Console.WriteLine("ADDED");

                            }
                            else if (bzt.Status == 99) {
                                filesAlreadyClosed++;
                            }
                            else if (bzt.Status != 99)
                            {
                                Client cl = eBookWriteManager.Context.ClientSet.FirstOrDefault(c => c.Id == bzt.ClientId);
                                Boolean clientErrorMatch = false;
                                foreach (Guid g in _clientsError) {
                                    if (g == cl.Id)
                                    {
                                        clientErrorMatch = true;
                                        break;
                                    }
                                }
                                if (!clientErrorMatch)
                                {
                                    bzt.Status = status;
                                    fx = eBookWriteManager.Context.FileXbrlSet.FirstOrDefault(f => f.FileId == fle.Id);
                                    if (fx != null)
                                    {
                                        fx.StatusId = status;
                                    }
                                    eBookWriteManager.Context.SaveChanges();
                                    filesClosed++;
                                    if (!fle.CloseDate.HasValue)
                                    {
                                        Console.WriteLine("..Closed file");
                                        fle.CloseDate = DateTime.Now;
                                        fle.ClosedBy = "eBook Automation";
                                        eBookWriteManager.Context.SaveChanges();
                                    }

                                    if (bzt.Status == 99)
                                    {


                                        DateTime sd = new DateTime(2014, 1, 1);
                                        DateTime ed = new DateTime(2014, 12, 31);

                                        ValidateNewPMTByGfis(cl.GFISCode, new Guid("E66DB040-549A-45A3-97BA-5D661147262B"), sd, ed, false);
                                        ValidateNewPMTByGfis(cl.GFISCode, new Guid("35335FE7-BEC7-46DD-BD0E-E1585A8B2346"), sd, ed, false);
                                        ValidateNewPMTByGfis(cl.GFISCode, new Guid("AD9D3880-3C15-4D22-B11D-B855AF1AEB14"), sd, ed, false);

                                        // WRITE SUCCESSFULL BIZTAX DECLARATIONS TO TLOG_DECLARATIONS_ACR
                                        if (path.Contains("ACR Central Processing"))
                                        {
                                            //Client cl = eBookWriteManager.Context.ClientSet.FirstOrDefault(c => c.Id == bzt.ClientId);
                                            //Console.WriteLine("{0} - {1} - {2} - {3}", cl.Name, cl.VatNumber, cl.GFISCode, file.date);

                                            TLOG_DECLARATIONS_ACR decl = new TLOG_DECLARATIONS_ACR
                                            {
                                                DeclarationType = "BIZTAX",
                                                DeclarationSubType = " ",
                                                DeclarationTimeStamp = file.date,
                                                ClientName = cl.Name,
                                                ClientVat = cl.VatNumber,
                                                ClientGfis = cl.GFISCode,
                                                ClientEngagementCode = null
                                            };
                                            New_Shared_Data_DeclarationsACRDataContext db = new New_Shared_Data_DeclarationsACRDataContext();
                                            db.TLOG_DECLARATIONS_ACRs.InsertOnSubmit(decl);
                                            db.SubmitChanges();
                                        }
                                    }
                                }
                            }
                            /*else if (bzt.Status == 99) {
                                Console.WriteLine("File already filed -> validating PMT");
                                Client cl = eBookWriteManager.Context.ClientSet.FirstOrDefault(c => c.Id == bzt.ClientId);

                                //DateTime sd = file.date.AddMonths(-6);
                                //DateTime ed = file.date.AddYears(1);

                                DateTime sd = new DateTime(2013, 1, 1);
                                DateTime ed = new DateTime(2013,12,31);

                                ValidateNewPMTByGfis(cl.GFISCode, new Guid("E66DB040-549A-45A3-97BA-5D661147262B"), sd, ed, false);
                                ValidateNewPMTByGfis(cl.GFISCode, new Guid("35335FE7-BEC7-46DD-BD0E-E1585A8B2346"), sd, ed, false);
                                ValidateNewPMTByGfis(cl.GFISCode, new Guid("AD9D3880-3C15-4D22-B11D-B855AF1AEB14"), sd, ed, false);
                            }*/
                        }

                        cnt++;



                    }
                    log.WriteLine(DateTime.Now.ToString("HH:mm:ss.fff") + " :: Done");
                    log.WriteLine(DateTime.Now.ToString("HH:mm:ss.fff") + " :: Files that were already closed -> " + filesAlreadyClosed + "/" + ttl);
                    log.WriteLine(DateTime.Now.ToString("HH:mm:ss.fff") + " :: Files that were closed now -> " + filesClosed + "/" + ttl);
                }
                catch (Exception ex)
                {
                    log.WriteLine(DateTime.Now.ToString("HH:mm:ss.fff") + " :: Exception during processing files: " + ex.Message);
                }
            }
             
        }

        public static void ValidateNewPMTByGfis(string clientGfisCode, Guid serviceTemplateId, DateTime startdate, DateTime enddate, bool createIfNonExisting)
        {

            CriteriaAutomatedTaskValidationGfisCodeDataContract catvvdc = new CriteriaAutomatedTaskValidationGfisCodeDataContract
            {
                GfisCode = clientGfisCode
                ,
                ServiceTemplateId = serviceTemplateId
                ,
                StartDate = startdate
                ,
                EndDate = enddate
                ,
                CreateIfNonExisting = createIfNonExisting
            };
            //TaskValidationResponseDataContract response = new AutomationRepository().ValidateTask(catvvdc);
            try
            {
                TaskValidationResponseDataContract response = new AutomationRepository().ValidateTaskBySchedule(catvvdc);
            }
            catch (Exception e) {
                Console.WriteLine(e.Message);
                Console.ReadLine();
            }
        }

  


        private static void EnrichEBookCalc()
        {
            Guid sid = new Guid("BD7C2EAE-8500-4103-8984-0131E73D07FA"); // automated biztax
            //List<Guid> files = new List<Guid> { new Guid("1aaf8aed-c5d9-4e16-aa66-b6f82253a1aa") };
            List<Guid> files = eBookWriteManager.Context.BizTaxDeclarationSet.Select(d => d.Id).ToList();


            string dte = string.Format("{0}_{1}_{2}-{3}_{4}_{5}", DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second);


            int cnt = 0;
            int ttl = files.Count;
            XElement root = new XElement("root");
            foreach (Guid file in files) //.Skip(start).Take(end - start))
            {
                cnt++;
                CacheRepository cr = new CacheRepository();
                if (cnt % 100 == 0)
                {
                    eBookWriteManager.SetContextNull();
                    Console.WriteLine("Waiting for garbage collector");
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                    System.Threading.Thread.Sleep(6000);
                    Console.WriteLine("MEM:" + GC.GetTotalMemory(true));

                }
                Console.WriteLine("{0}/{1}", cnt, ttl);
                BizTaxDeclaration bzt = eBookWriteManager.Context.BizTaxDeclarationSet
                                   .FirstOrDefault(b => b.Id == file);
                if (bzt != null)
                {
                    if (bzt.eBookCalc == null)
                    {
                        RuleEngine2012 re = new RuleEngine2012();

                        cr.LoadFileInCache(file);
                        try
                        {

                            WorksheetSet set = re.GetSet(file);
                            if (set.DataContainer.DataContract.Services.Count(s => s.ServiceId == sid) == 1)
                            {
                                bzt.eBookCalc = set.GetTaxCalculation();
                                eBookWriteManager.Context.SaveChanges();
                            }
                        }

                        catch (Exception e)
                        {
                            root.Add(new XElement("failed", new XAttribute("fileid", file.ToString()), new XElement("Msg", e.Message), new XElement("Stack", e.StackTrace)));
                            root.Save(string.Format("failures_{0}.xml", dte));
                            Console.WriteLine("FAILED");

                        }
                        finally
                        {
                            FileCache.Current.Remove(FileCache.SchemaKey + file.ToString());
                            FileCache.Current.Remove(FileCache.WorksheetskKey + file.ToString());

                        }
                        cr = null;
                        re = null;
                       
                    }






                }
            }
        }
        
    }
}
