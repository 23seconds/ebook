﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UpdateBizTaxDeclaration
{
    class Declaration
    {
        public Guid id;
        public string type;
        public string subtype;
        public string clientName;
        public string clientVat;
        public string clientGFIS;
        public string clientEngagement;
        public DateTime date;
        public Declaration(Guid DeclarationId, string DeclarationType, string DeclarationSubType, DateTime DeclarationTimeStamp, string ClientName, string ClientVat, string ClientGFIS, string ClientEngagement)
         {
             this.id = DeclarationId;
            this.type = DeclarationType;
             this.subtype = DeclarationSubType;
             this.date = DeclarationTimeStamp;
             this.clientName = ClientName;
             this.clientVat = ClientVat;
             this.clientGFIS = ClientGFIS;
             this.clientEngagement = ClientEngagement;
         }
    }
}
