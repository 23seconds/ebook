﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl"
>
  <xsl:output method="text" indent="no"/>

  <xsl:template match="Root">
    <xsl:apply-templates select="." mode="RENDER_WORKSHEET">
      <xsl:with-param name="wtype">DubieuzeDebiteurenApp</xsl:with-param>
    </xsl:apply-templates>
    <xsl:apply-templates select="." mode="RENDER_WORKSHEET">
      <xsl:with-param name="wtype">VoorzieningenApp</xsl:with-param>
    </xsl:apply-templates>
  </xsl:template>

  <xsl:template match="Root" mode="RENDER_WORKSHEET">
    <xsl:param name="wtype"/>
    <xsl:apply-templates select="Worksheettype[@name=$wtype]">
      <xsl:with-param name="culture">nl-BE</xsl:with-param>
    </xsl:apply-templates>
    <xsl:apply-templates select="Worksheettype[@name=$wtype]">
      <xsl:with-param name="culture">en-US</xsl:with-param>
    </xsl:apply-templates>
    <xsl:apply-templates select="Worksheettype[@name=$wtype]">
      <xsl:with-param name="culture">fr-FR</xsl:with-param>
    </xsl:apply-templates>
  </xsl:template>

  <xsl:template match="Worksheettype">
    <xsl:param name="culture"/>
    <xsl:for-each select="Types/Sub/*/*">
     
        '<xsl:value-of select="ancestor::Worksheettype/@TypeId"/>';'<xsl:value-of select="local-name(parent::*)"/>';'<xsl:value-of select="local-name(.)"/>';'<xsl:value-of select="$culture"/>';'TRANSLATION'
      
    </xsl:for-each>
  </xsl:template>
</xsl:stylesheet>
