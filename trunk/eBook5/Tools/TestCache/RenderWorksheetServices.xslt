﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl"
    xmlns:Helper="urn:Helper"
>
    <xsl:output method="text" indent="yes"/>
  <xsl:param name="ruleApp"/>
  <xsl:param name="lang2Char"/>
  <xsl:param name="lang5Char"/>

  <xsl:variable name="rootent" select="//Types/@rootType"/>


  <xsl:template match="Root">
    
    #region <xsl:value-of select="$ruleApp"/>
    [OperationContract]
    [FaultContract(typeof(DefaultFaultContract))]
    EY.com.eBook.RuleEngine.AY2012.<xsl:value-of select="$ruleApp"/>Types.<xsl:value-of select="$rootent"/>&#160;<xsl:value-of select="$ruleApp"/>GetData(CriteriaFileDataContract cfdc);

    [OperationContract]
    [FaultContract(typeof(DefaultFaultContract))]
    EY.com.eBook.RuleEngine.AY2012.<xsl:value-of select="$ruleApp"/>Types.<xsl:value-of select="$rootent"/>&#160;<xsl:value-of select="$ruleApp"/>Calculate(CriteriaFileDataContract cfdc);

    #region Entity validation
    <xsl:for-each select="Types/*[not(name()='Rules') and not(name()=$rootent)]">
    [OperationContract]
    [FaultContract(typeof(DefaultFaultContract))]
    List&lt;InvalidField&gt;&#160;<xsl:value-of select="$ruleApp"/><xsl:value-of select="name()"/>Validate(EY.com.eBook.RuleEngine.Data.AY2012.Criteria<xsl:value-of select="$ruleApp"/><xsl:value-of select="name()"/>DataContract cedc);
    
    </xsl:for-each>

    #endregion



    #region Add/Remove

    <xsl:for-each select="Types/*[name()=$rootent]/*[not(name()='Rules') and @type='Entity']">

    <xsl:if test="@list='true'">
    [OperationContract]
    [FaultContract(typeof(DefaultFaultContract))]
    EY.com.eBook.RuleEngine.AY2012.<xsl:value-of select="$ruleApp"/>Types.<xsl:value-of select="$rootent"/>&#160;<xsl:value-of select="$ruleApp"/><xsl:value-of select="name()"/>Add(EY.com.eBook.RuleEngine.Data.AY2012.Criteria<xsl:value-of select="$ruleApp"/><xsl:value-of select="@entity"/>DataContract cedc);

    [OperationContract]
    [FaultContract(typeof(DefaultFaultContract))]
    EY.com.eBook.RuleEngine.AY2012.<xsl:value-of select="$ruleApp"/>Types.<xsl:value-of select="$rootent"/>&#160;<xsl:value-of select="$ruleApp"/><xsl:value-of select="name()"/>Remove(CriteriaFileAndRowDataContract cfrdc);
    </xsl:if>
      
    [OperationContract]
    [FaultContract(typeof(DefaultFaultContract))]
    EY.com.eBook.RuleEngine.AY2012.<xsl:value-of select="$ruleApp"/>Types.<xsl:value-of select="$rootent"/>&#160;<xsl:value-of select="$ruleApp"/><xsl:value-of select="name()"/>Update(EY.com.eBook.RuleEngine.Data.AY2012.Criteria<xsl:value-of select="$ruleApp"/><xsl:value-of select="@entity"/>DataContract cedc);
    </xsl:for-each>
    #endregion

  #endregion
        
        

    //Service Implementation
    #region <xsl:value-of select="$ruleApp"/>
    [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
    RequestFormat = WebMessageFormat.Json,
    ResponseFormat = WebMessageFormat.Json,
    UriTemplate = "/<xsl:value-of select="$ruleApp"/>GetData")]
    public EY.com.eBook.RuleEngine.AY2012.<xsl:value-of select="$ruleApp"/>Types.<xsl:value-of select="$rootent"/><xsl:text>&#160;</xsl:text><xsl:value-of select="$ruleApp"/>GetData(CriteriaFileDataContract cfdc) {
        EY.com.eBook.RuleEngine.AY2012.WorksheetSet set = GetSet(cfdc.Id);
        EY.com.eBook.RuleEngine.AY2012.WorksheetSetData ws = (EY.com.eBook.RuleEngine.AY2012.WorksheetSetData)set.setHelper;

        return ws.<xsl:value-of select="$ruleApp"/>.Data;

    }

    [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
    RequestFormat = WebMessageFormat.Json,
    ResponseFormat = WebMessageFormat.Json,
    UriTemplate = "/<xsl:value-of select="$ruleApp"/>Calculate")]
    public EY.com.eBook.RuleEngine.AY2012.<xsl:value-of select="$ruleApp"/>Types.<xsl:value-of select="$rootent"/><xsl:text>&#160;</xsl:text><xsl:value-of select="$ruleApp"/>Calculate(CriteriaFileDataContract cfdc) {
        EY.com.eBook.RuleEngine.AY2012.WorksheetSet set = GetSet(cfdc.Id);
        EY.com.eBook.RuleEngine.AY2012.WorksheetSetData ws = (EY.com.eBook.RuleEngine.AY2012.WorksheetSetData)set.setHelper;
        ws.<xsl:value-of select="$ruleApp"/>.Calculate(ref ws);
        return ws.<xsl:value-of select="$ruleApp"/>.Data;

    }

    #region Entity validation
    <xsl:for-each select="Types/*[not(name()='Rules') and not(name()=$rootent)]">
      [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
      RequestFormat = WebMessageFormat.Json,
      ResponseFormat = WebMessageFormat.Json,
      UriTemplate = "/<xsl:value-of select="$ruleApp"/><xsl:value-of select="name()"/>Validate")]
      public List&lt;InvalidField&gt;<xsl:text>&#160;</xsl:text><xsl:value-of select="$ruleApp"/><xsl:value-of select="name()"/>Validate(EY.com.eBook.RuleEngine.Data.AY2012.Criteria<xsl:value-of select="$ruleApp"/><xsl:value-of select="name()"/>DataContract cedc) {
      EY.com.eBook.RuleEngine.AY2012.WorksheetSet set = GetSet(cedc.Id);
      EY.com.eBook.RuleEngine.AY2012.WorksheetSetData ws = (EY.com.eBook.RuleEngine.AY2012.WorksheetSetData)set.setHelper;
      ListList&lt;InvalidField&gt; res = ws.<xsl:value-of select="$ruleApp"/>.Validate(cedc.Entity,cedc.CollectionPath, ref ws);
         return res;
      }

    </xsl:for-each>

    #endregion



    #region Add/Remove

    <xsl:for-each select="Types/*[name()=$rootent]/*[not(name()='Rules') and @type='Entity']">
      [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
      RequestFormat = WebMessageFormat.Json,
      ResponseFormat = WebMessageFormat.Json,
      UriTemplate = "/<xsl:value-of select="$ruleApp"/><xsl:value-of select="name()"/>Add")]
      public EY.com.eBook.RuleEngine.AY2012.<xsl:value-of select="$ruleApp"/>Types.<xsl:value-of select="$rootent"/><xsl:text>&#160;</xsl:text><xsl:value-of select="$ruleApp"/><xsl:value-of select="name()"/>Add(EY.com.eBook.RuleEngine.Data.AY2012.Criteria<xsl:value-of select="$ruleApp"/><xsl:value-of select="@entity"/>DataContract cedc) {
          EY.com.eBook.RuleEngine.AY2012.WorksheetSet set = GetSet(cedc.Id);
          EY.com.eBook.RuleEngine.AY2012.WorksheetSetData ws = (EY.com.eBook.RuleEngine.AY2012.WorksheetSetData)set.setHelper;
          ws.<xsl:value-of select="$ruleApp"/>.Data.<xsl:value-of select="name()"/>.Add(cedc.Entity);
          ws.<xsl:value-of select="$ruleApp"/>.Calculate(ref ws);
          return ws.<xsl:value-of select="$ruleApp"/>.Data;
      }

      [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
      RequestFormat = WebMessageFormat.Json,
      ResponseFormat = WebMessageFormat.Json,
      UriTemplate = "/<xsl:value-of select="$ruleApp"/><xsl:value-of select="name()"/>Update")]
      public EY.com.eBook.RuleEngine.AY2012.<xsl:value-of select="$ruleApp"/>Types.<xsl:value-of select="$rootent"/><xsl:text>&#160;</xsl:text><xsl:value-of select="$ruleApp"/><xsl:value-of select="name()"/>Update(EY.com.eBook.RuleEngine.Data.AY2012.Criteria<xsl:value-of select="$ruleApp"/><xsl:value-of select="@entity"/>DataContract cedc) {
          EY.com.eBook.RuleEngine.AY2012.WorksheetSet set = GetSet(cedc.Id);
          EY.com.eBook.RuleEngine.AY2012.WorksheetSetData ws = (EY.com.eBook.RuleEngine.AY2012.WorksheetSetData)set.setHelper;
          int idx = ws.<xsl:value-of select="$ruleApp"/>.Data.<xsl:value-of select="name()"/>.FindIndex(f => f.Id == cedc.Entity.Id);
            if (idx > -1)
            {
                ws.<xsl:value-of select="$ruleApp"/>.Data.<xsl:value-of select="name()"/>.RemoveAt(idx);
                ws.<xsl:value-of select="$ruleApp"/>.Data.<xsl:value-of select="name()"/>.Insert(idx, cedc.Entity);
            }
          ws.<xsl:value-of select="$ruleApp"/>.Calculate(ref ws);
          return ws.<xsl:value-of select="$ruleApp"/>.Data;
      }

      [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
      RequestFormat = WebMessageFormat.Json,
      ResponseFormat = WebMessageFormat.Json,
      UriTemplate = "/<xsl:value-of select="$ruleApp"/><xsl:value-of select="name()"/>Remove")]
      public EY.com.eBook.RuleEngine.AY2012.<xsl:value-of select="$ruleApp"/>Types.<xsl:value-of select="$rootent"/><xsl:text>&#160;</xsl:text><xsl:value-of select="$ruleApp"/><xsl:value-of select="name()"/>Remove(CriteriaFileAndRowDataContract cfrdc) {
          EY.com.eBook.RuleEngine.AY2012.WorksheetSet set = GetSet(cfrdc.Id);
          EY.com.eBook.RuleEngine.AY2012.WorksheetSetData ws = (EY.com.eBook.RuleEngine.AY2012.WorksheetSetData)set.setHelper;
          int idx = ws.<xsl:value-of select="$ruleApp"/>.Data.<xsl:value-of select="name()"/>.FindIndex(f => f.Id == cfrdc.RowId);
          if (idx > -1)
          {
              ws.<xsl:value-of select="$ruleApp"/>.Data.<xsl:value-of select="name()"/>.RemoveAt(idx);
          }
          ws.<xsl:value-of select="$ruleApp"/>.Calculate(ref ws);
          return ws.<xsl:value-of select="$ruleApp"/>.Data;
      }
    </xsl:for-each>
    #endregion

    #endregion

  </xsl:template>
  
</xsl:stylesheet>
