﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;
using System.Xml.XPath;
using System.Xml.Linq;
using EY.com.eBook.Core;
using EY.com.eBook.Core.EF.Read;

namespace TestCache
{
    public class XsltHelper
    {
        public List<WorksheetTypeTranslation> Translations { get; set; }

        private Dictionary<string, decimal> _sums = new Dictionary<string, decimal>();
        public CultureInfo Culture { get; set; }
        public string CultureStr { get; set; }

        public string escapeQuotes(string val)
        {
            return val.Replace("'", "");
        }

        public string GetTranslation(string entitydef,string collection,string field, string tpe,string defaultVal)
        {

            WorksheetTypeTranslation wt = Translations.FirstOrDefault(t => t.Culture == CultureStr && t.EntityOrCollection == collection
                                    && t.FieldName == field && t.OutputType.ToLower() == tpe.ToLower());

            if (wt == null)
            {
                wt = Translations.FirstOrDefault(t => t.Culture == CultureStr && t.EntityOrCollection == entitydef
                                    && t.FieldName == field && t.OutputType.ToLower() == tpe.ToLower());
                if(wt==null) return defaultVal;
            }
            return escapeQuotes(wt.Translation);
        }

        public string GetFontSize(int filledColumns)
        {
            string fontSize = "10";

            if (filledColumns > 6 && filledColumns <= 8)
            {
                fontSize = "8";
            }
            if (filledColumns > 8)
            {
                fontSize = "6";
            }

            return fontSize;
        }

        public string GetColumnSizes(int total, int columns)
        {
            string spacing = "1;";
            if (columns <= 15)
            {
                spacing = "5;";
            }

            int realColumns = (columns) / 2;
            realColumns = realColumns + 1;
            decimal columnSize = total / realColumns;
            string sizes = "";
            for (int i = 0; i < columns; i++)
            {
                sizes += columnSize.ToString() + ";";
                if (i < columns - 1)
                {
                    sizes += spacing;
                }
                i = i + 1;
            }
            return sizes;
        }

        public string ConvertDate(string input)
        {
            if (!string.IsNullOrEmpty(input))
            {

                DateTime dte;
                try
                {
                    dte = System.DateTime.Parse(input, Culture.DateTimeFormat);
                    return dte.ToString(Culture.DateTimeFormat.ShortDatePattern);
                }
                catch
                {
                    try
                    {
                        dte = System.DateTime.Parse(input, CultureInfo.InvariantCulture.DateTimeFormat);
                        return dte.ToString(Culture.DateTimeFormat.ShortDatePattern);
                    }
                    catch { return string.Empty; }
                }
            }
            else
            {
                return string.Empty;
            }
        }

        public string ConvertDateLongFormat(string input)
        {
            if (!string.IsNullOrEmpty(input))
            {
                try
                {
                    DateTime dte = System.DateTime.Parse(input, CultureInfo.InvariantCulture.DateTimeFormat);
                    return dte.ToString(Culture.DateTimeFormat.LongDatePattern);
                }
                catch { return string.Empty; }
            }
            else
            {
                return string.Empty;
            }
        }

        public decimal CheckNaN(string nr)
        {
            decimal nrd = 0;
            decimal.TryParse(nr, out nrd);
            return nrd;
        }

        public decimal CheckNaNAbsolute(string nr)
        {
            decimal nrd = 0;
            decimal.TryParse(nr, out nrd);
            return Math.Abs(nrd);
        }

        public decimal AddSum(string key, decimal value)
        {
            if (!_sums.ContainsKey(key)) _sums[key] = 0;
            _sums[key] = _sums[key] + value;
            return _sums[key];
        }

        public decimal ResetSum(string key)
        {
            if (!_sums.ContainsKey(key)) _sums[key] = 0;
            _sums[key] = 0;
            return 0;
        }

        //public XPathNavigator[] 


        // XPathNodeIterator
        public XPathNavigator[] Distinct(XPathNavigator[] list, string xpathToCompare)
        {

            XPathNavigator[] results = list.Distinct(new XsltDistinctComparer(xpathToCompare)).ToArray<XPathNavigator>();
            return results;
        }

        public XPathNavigator[] SortNumericValue(XPathNavigator[] list, string xpathToSortOn)
        {
            XPathNavigator[] results = list.OrderBy(x => x.SelectSingleNode(xpathToSortOn).ValueAsDouble).ToArray();
            return results;
        }

        public XPathNavigator[] Sort(XPathNavigator[] list, string xpathToSortOn)
        {
            XPathNavigator[] results = list.OrderBy(x => x.SelectSingleNode(xpathToSortOn).Value).ToArray();
            return results;
        }

        public XPathNavigator[] DistinctNumeric(XPathNavigator[] list, string xpathToCompare)
        {

            XPathNavigator[] results = list.Distinct(new XsltDistinctComparer(xpathToCompare, false, true)).ToArray<XPathNavigator>();
            return results;
        }

        public XPathNavigator[] DistinctCaseInsensitive(XPathNavigator[] list, string xpathToCompare)
        {

            XPathNavigator[] results = list.Distinct(new XsltDistinctComparer(xpathToCompare, true)).ToArray<XPathNavigator>();
            return results;
        }

        public XPathNavigator GetColumns(double columns)
        {

            XElement root = new XElement("root");
            for (int i = 1; i <= columns; i++)
            {
                root.Add(new XElement("Column", i));
            }
            return root.CreateNavigator();
        }

        public XPathNavigator Split(string input, string splitChar)
        {
            string[] items = input.Split(new string[] { splitChar }, StringSplitOptions.RemoveEmptyEntries);
            XElement root = new XElement("root");
            for (int i = 0; i < items.Length; i++)
            {
                root.Add(new XElement("Item", items[i]));
            }
            return root.CreateNavigator();
        }

        public string Replace(string input, string find, string replacement)
        {
            return input.Replace(find, replacement);
        }
    }
}
