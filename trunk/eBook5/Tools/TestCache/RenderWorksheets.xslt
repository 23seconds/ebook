﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl"
    xmlns:Helper="urn:Helper"
>
    <xsl:output method="text" indent="yes"/>
  <xsl:param name="AY"/>
  <xsl:param name="ruleApp"/>
  <xsl:param name="lang2Char"/>
  <xsl:param name="lang5Char"/>
<!--
FUNCTION:
<Function name="CopyFields" 
            useCurrentField="true" 
            formWide="true"
            worksheetWide="false"
/>


useCurrentField : fld  (==js field object)
formWide: entity (==js form data) 
                 -> AT START: entity=form.getValues();
                 -> AT END: form.setValues(entity);
worksheetWide: worksheet
                 -> AT START: worksheet = worksheet selector class?

//-->
  <xsl:template match="Root">
    <xsl:variable name="worksheetEntityName" select="Types/@rootType"/>


      Ext.ns('eBook.Language.Worksheets.<xsl:value-of select="$ruleApp"/>');
      <xsl:for-each select="Types/*[name()=$worksheetEntityName]/*[@type='Entity']">
        <xsl:variable name="entity" select="@entity"/>
        <xsl:variable name="collectionName" select="name()"/>
        Ext.ns('eBook.Language.Worksheets.<xsl:value-of select="$ruleApp"/>.<xsl:value-of select="$collectionName"/>');
        eBook.Language.Worksheets.<xsl:value-of select="$ruleApp"/>.<xsl:value-of select="$collectionName"/>.title = '<xsl:value-of select="Helper:GetTranslation(string($worksheetEntityName),string($entity),string($collectionName),'label',string($collectionName))"/>';
        <xsl:for-each select="//Types/*[name()=$entity]/*">
          eBook.Language.Worksheets.<xsl:value-of select="$ruleApp"/>.<xsl:value-of select="$collectionName"/>['<xsl:value-of select="name()"/>']='<xsl:value-of select="Helper:GetTranslation(string($entity),string($collectionName),string(name()),'label',string(name()))"/>';
          eBook.Language.Worksheets.<xsl:value-of select="$ruleApp"/>.<xsl:value-of select="$collectionName"/>['<xsl:value-of select="name()"/>Status']='<xsl:value-of select="Helper:GetTranslation(string($entity),string($collectionName),string(name()),'status','')"/>';
        </xsl:for-each>
        <!--xsl:if test="Attributes/anyType/Collection/anyType[@Key='GlobalActions']/@Value">
          <xsl:variable name="GlobalActions" select="Helper:Split(string(Attributes/anyType/Collection/anyType[@Key='GlobalActions']/@Value),',')"/>
          <xsl:for-each select="$GlobalActions//Item">
            <xsl:variable name="actionName" select="text()"/>
            eBook.Language.Worksheets.<xsl:value-of select="$ruleApp"/>.<xsl:value-of select="$collectionName"/>['<xsl:value-of select="."/>'] = '<xsl:value-of select="Translations:GetTranslation(string($entity),string($collectionName),string($actionName),'label',string($actionName))"/>';
          </xsl:for-each>
        </xsl:if-->
      </xsl:for-each>
   
    eBook.Worksheet.<xsl:value-of select="$ruleApp"/> = new eBook.Worksheet.WorksheetItem({
    ruleApp:'<xsl:value-of select="$ruleApp"/>'
    , ruleEngine: {
      url: '/EY.com.eBook.RuleEngine.5/RuleEngine<xsl:value-of select="$AY"/>.svc/'
      , baseMethod: '<xsl:value-of select="$ruleApp"/>'
    }
    ,typeId:'<xsl:value-of select="@TypeId"/>'
    ,title:'<xsl:value-of select="$ruleApp"/>TITLE'
    ,myType:'<xsl:value-of select="@Category"/>'
    
    ,historyImport:0
    ,btwImport:0
    ,excelExport:0
    ,meta: {
    Collections:[<xsl:for-each select="Types/*[name()=$worksheetEntityName]/*[@type='Entity']">
      <xsl:if test="position()>1">,</xsl:if>'<xsl:value-of select="name()"/>'
    </xsl:for-each>]
    
    <xsl:for-each select="Types/*[name()=$worksheetEntityName]/*[@type='Entity']">
      <xsl:variable name="CollectionName" select="name()"/>
      <xsl:variable name="CollectionEntity" select="@entity"/>
      ,'<xsl:value-of select="$CollectionName"/>':{
          entity:'<xsl:value-of select="$CollectionEntity"/>'
          ,
      
      
      <xsl:apply-templates select="//Types/*[name()=$CollectionEntity]" mode="RenderTab">
        <xsl:with-param name="entityField" select="."/>
      </xsl:apply-templates>
      }
    </xsl:for-each>
    }
    });
  </xsl:template>

  <xsl:template match="*" mode="RenderTab">
    <xsl:param name="entityField"/>
    
    columns:<xsl:choose>
      <xsl:when test="$entityField/@list='true'">
        <xsl:apply-templates select="." mode="RenderGridColumns">
          <xsl:with-param name="collectionName" select="name($entityField)"/>
        </xsl:apply-templates>
      </xsl:when>
      <xsl:otherwise>null</xsl:otherwise>
    </xsl:choose>
    ,hidden:<xsl:choose>
      <xsl:when test="$entityField/Attributes/Attribute[@name='Hidden' and @value='true']">true</xsl:when>
      <xsl:otherwise>false</xsl:otherwise>
    </xsl:choose>
    ,editable:<xsl:choose>
      <xsl:when test="$entityField/Attributes/Attribute[@name='ReadOnly' and @value='true']">0</xsl:when>
      <xsl:otherwise>1</xsl:otherwise>
    </xsl:choose>
    ,recordType:<xsl:apply-templates select="." mode="RenderRecordtype"/>
    ,formConfig:<xsl:choose>
      <xsl:when test="not($entityField/Attributes/Attribute[@name='ReadOnly' and @value='true'])">
        <xsl:apply-templates select="." mode="RenderFormConfig">
          <xsl:with-param name="collectionName" select="name($entityField)"/>
          <xsl:with-param name="entityField" select="$entityField"/>
        </xsl:apply-templates>
      </xsl:when>
      <xsl:otherwise>null</xsl:otherwise>
    </xsl:choose>
    ,attributes:<xsl:apply-templates select="$entityField" mode="GetAttributesList"/>
    ,defaultOnAdd:false
  </xsl:template>

  <xsl:template match="*" mode="RenderRecordtype">
    [<xsl:for-each select="*[not(name()='Rules')]">
      <xsl:variable name="isList">
        <xsl:apply-templates select="." mode="IsList"/>
      </xsl:variable>
      <xsl:if test="position()>1">,</xsl:if>{
      name:'<xsl:value-of select="name()"/>'
      ,type:<xsl:apply-templates select="." mode="GetRecordDataType"/>
      }
    </xsl:for-each>,{name:'status',type:Ext.data.Types.INT}]
  </xsl:template>
  
  <xsl:template match="*" mode="RenderGridColumns">
    <xsl:param name="collectionName"/>[<xsl:for-each select="*[not(name()='Rules') and (not(Attributes/Attribute[@name='Grid']) or Attributes/Attribute[@name='Grid' and @value='true'])]">
      <xsl:if test="position()>1">,</xsl:if>{ <xsl:variable name="isList"><xsl:apply-templates select="." mode="IsList"/></xsl:variable>
      
      header: eBook.Language.Worksheets.<xsl:value-of select="$ruleApp"/>.<xsl:value-of select="$collectionName"/>['<xsl:value-of select="name()"/>'],
      dataIndex: '<xsl:value-of select="name()"/>'
      <xsl:if test="Attributes/Attribute[@name='IsNoteField' and @value='true']">,isNote:true</xsl:if>
      <xsl:choose>
        <xsl:when test="$isList='Y'">
          ,renderer:function(value, metaData, record, rowIndex, colIndex, store) { if(value!=null) { return value.<xsl:value-of select="$lang2Char"/>; } else { return 'nihil';} }
        </xsl:when>
        <xsl:when test="Attributes/Attribute[@name='Interface' and @value='percentfield']">
          ,xtype:'ebookpercentcolumn',align:'right',format:'0.000,00%/i',emptyIfZero:<xsl:choose><xsl:when test="Attributes/Attribute[@name='EmptyIfZero' and @value='true']">true</xsl:when><xsl:otherwise>false</xsl:otherwise></xsl:choose>
        </xsl:when>
        <xsl:otherwise>
          <xsl:choose>
            <xsl:when test="(@type='Date' or @type='DateTime') and not($isList='Y')">,xtype:'datecolumn',format:'d/m/Y'</xsl:when>
            <xsl:when test="@type='Number' and not($isList='Y')">
              ,xtype:'ebooknumbercolumn',align:'right',format:'0.000,00/i',emptyIfZero:<xsl:choose><xsl:when test="Attributes/Attribute[@name='EmptyIfZero' and @value='true']">true</xsl:when><xsl:otherwise>false</xsl:otherwise></xsl:choose>
            </xsl:when>
            <xsl:when test="@type='Integer' and not($isList='Y') and Attributes/Attribute[@name='thousands' and @value='false']">
              ,xtype:'ebooknumbercolumn',align:'right',format:'0/i',emptyIfZero:<xsl:choose><xsl:when test="Attributes/Attribute[@name='EmptyIfZero' and @value='true']">true</xsl:when><xsl:otherwise>false</xsl:otherwise></xsl:choose>
            </xsl:when>
            <xsl:when test="@type='Integer' and not($isList='Y')">
              ,xtype:'ebooknumbercolumn',align:'right',format:'0.000/i',emptyIfZero:<xsl:choose><xsl:when test="Attributes/Attribute[@name='EmptyIfZero' and @value='true']">true</xsl:when><xsl:otherwise>false</xsl:otherwise></xsl:choose>
            </xsl:when>
            <xsl:when test="@type='Boolean' and not($isList='Y')">,xtype:'booleancolumn',falseText:'-',trueText:'v'</xsl:when>
          </xsl:choose>
        </xsl:otherwise>
      </xsl:choose>
      }
    </xsl:for-each>]
  </xsl:template>


  <xsl:template match="*" mode="RenderFormConfig">
    <xsl:param name="collectionName"/>
    <xsl:param name="entityField"/>
    {
    items: [<xsl:apply-templates select="." mode="RenderFormFields">
      <xsl:with-param name="collectionName" select="$collectionName"/>
      <xsl:with-param name="entityField" select="$entityField"/>
    </xsl:apply-templates>]
    ,layout: <xsl:apply-templates select="$entityField" mode="GetFormLayout"/>
    ,events: <xsl:apply-templates select="." mode="GetFormEvents"/>
    }
  </xsl:template>

  <xsl:template match="*" mode="GetFormEvents">
    <xsl:variable name="fields" select="*[not(name()='Rules')]"/>
    <xsl:choose>
      <xsl:when test="count($fields/Attributes/Attribute[@name='InterfaceEvents'])>0">
        {
        <xsl:variable name="apos">
          <xsl:text>'</xsl:text>
        </xsl:variable>
        <xsl:variable name="selection" select="concat('Attributes/Attribute[@name=',$apos,'InterfaceEvents',$apos,']/@value')"/>
        <xsl:for-each select="Helper:Distinct($fields/Attributes/Attribute[@name='InterfaceEvents'],'@value')">
          <xsl:variable name="event" select="@value"/>
          <xsl:if test="position()>1">,</xsl:if>'<xsl:value-of select="$event"/>': {
          <xsl:for-each select="Helper:Distinct($fields[count(Attributes/Attribute[@name='InterfaceEvents' and @value=$event])>0]/Attributes/Attribute[@name='InterfaceEventVisibility_Field'],'@value')">
            <xsl:variable name="field" select="@value"/>
            <xsl:if test="position()>1">,</xsl:if>'<xsl:value-of select="$field"/>': [
            <xsl:for-each select="Helper:Distinct($fields[count(Attributes/Attribute[@name='InterfaceEvents' and @value=$event])>0 and count(Attributes/Attribute[@name='InterfaceEventVisibility_Field' and @value=$field])>0]/Attributes/Attribute[@name='InterfaceEventVisibility_Values'],'@value')" >
              <xsl:variable name="values" select="@value"/>
              <xsl:if test="position()>1">,</xsl:if>{values:'<xsl:value-of select="$values"/>',fields:[<xsl:for-each select="$fields[count(Attributes/Attribute[@name='InterfaceEvents' and @value=$event])>0 and count(Attributes/Attribute[@name='InterfaceEventVisibility_Field' and @value=$field])>0 and count(Attributes/Attribute[@name='InterfaceEventVisibility_Values' and @value=$values])>0]">
                <xsl:if test="position()>1">,</xsl:if>'<xsl:value-of select="name()"/>'
              </xsl:for-each>]}
            </xsl:for-each>
            ]
          </xsl:for-each>
          }
        </xsl:for-each>
        }
      </xsl:when>
      <xsl:otherwise>null</xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="*" mode="GetFormLayout"><xsl:choose><xsl:when test="Attributes/Attribute[@name='Layout']">'<xsl:value-of select="Attributes/Attribute[@name='Layout']/@value"/>'</xsl:when><xsl:otherwise>null</xsl:otherwise></xsl:choose></xsl:template>

  <xsl:template match="*" mode="RenderFormFields">
    <xsl:param name="collectionName"/>
    <xsl:param name="entityField"/>

    <xsl:choose>
      <xsl:when test="$entityField/Attributes/Attribute[@name='Layout' and @value='columnform']">
        <xsl:variable name="columnCount" select="number($entityField/Attributes/Attribute[@name='Columns']/@value)"/>
        {
        layout:'column'
        ,defaults: {
        columnWidth:<xsl:value-of select="1 div number($columnCount)"/>
        ,layout:'form'
        ,border:false
        }
        ,items: [
        <xsl:variable name="me" select="."/>
        <xsl:for-each select="Helper:GetColumns($columnCount)/Column">
          <xsl:if test="position()>1">,</xsl:if>{ // column <xsl:value-of select="./text()"/>
          items:[
          <xsl:apply-templates select="$me" mode="RenderFormColumn">
            <xsl:with-param name="columnNumber" select="number(./text())"/>
            <xsl:with-param name="columnCount" select="$columnCount"/>
            <xsl:with-param name="collectionName" select="$collectionName"/>
          </xsl:apply-templates>
          ]
          }
        </xsl:for-each>
        <xsl:for-each select="*[not(name()='Rules') and count(Attributes/Attribute[@name='Form' and @value='false'])>0]">
          <xsl:variable name="xtype">
            <xsl:apply-templates select="." mode="GetXtype"/>
          </xsl:variable>
          ,{<xsl:choose>
            <xsl:when test="$xtype='ewdatefield'">xtype:'ewdatefield',hidden:true</xsl:when>
            <xsl:otherwise>xtype:'hidden'</xsl:otherwise>
          </xsl:choose>
          ,name:'<xsl:value-of select="name()"/>'
          ,value:''
          ,manual:false
          }
        </xsl:for-each>
        ]
        }
      </xsl:when>
      <xsl:when test="$entityField/Attributes/Attributes[@name='Layout' and @value='tableform']">
        <xsl:variable name="columnCount" select="number($entityField/Attributes/Attribute[@name='Columns']/@value)"/>
        {xtype:'panel'
        , layout:'tableform'
        , layoutConfig:{
        columns:<xsl:value-of select="$columnCount"/>
        }
        , defaults : {
        borders:false
        }
        , items:[
        <xsl:for-each select="*[not(name()='Rules') and (not(Attributes/Attribute[@name='Form']) or count(Attributes/Attribute[@name='Form' and @value='false'])=0)]">
          <xsl:if test="position()>1">,</xsl:if>
          <xsl:apply-templates select="." mode="RenderFormField">
            <xsl:with-param name="collectionName" select="$collectionName"/>
          </xsl:apply-templates>
        </xsl:for-each>
        ]
        }
        <xsl:for-each select="*[not(name()='Rules') and count(Attributes/Attribute[@name='Form' and @value='false'])>0]">
          <xsl:variable name="xtype">
            <xsl:apply-templates select="." mode="GetXtype"/>
          </xsl:variable>
          ,{<xsl:choose>
            <xsl:when test="$xtype='ewdatefield'">xtype:'ewdatefield',hidden:true</xsl:when>
            <xsl:otherwise>xtype:'hidden'</xsl:otherwise>
          </xsl:choose>
          ,name:'<xsl:value-of select="name()"/>'
          ,value:''
          ,manual:false
          }
        </xsl:for-each>
      </xsl:when>
      <xsl:otherwise>
        <xsl:for-each select="*[not(name()='Rules') and (not(Attributes/Attribute[@name='Form']) or count(Attributes/Attribute[@name='Form' and @value='false'])=0)]">
          <xsl:if test="position()>1">
            ,
          </xsl:if>
          <xsl:apply-templates select="." mode="RenderFormField">
            <xsl:with-param name="collectionName" select="$collectionName"/>
          </xsl:apply-templates>
        </xsl:for-each>
        <xsl:for-each select="*[not(name()='Rules') and count(Attributes/Attribute[@name='Form' and @value='false'])>0]">
          <xsl:variable name="xtype">
            <xsl:apply-templates select="." mode="GetXtype"/>
          </xsl:variable>
          ,{<xsl:choose>
            <xsl:when test="$xtype='ewdatefield'">xtype:'ewdatefield',hidden:true</xsl:when>
            <xsl:otherwise>xtype:'hidden'</xsl:otherwise>
          </xsl:choose>
          ,name:'<xsl:value-of select="name()"/>'
          ,value:''
          ,manual:false
          }
        </xsl:for-each>

      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="*" mode="RenderFormColumn">
    <xsl:param name="columnNumber"/>
    <xsl:param name="columnCount"/>
    <xsl:param name="collectionName"/>
    <xsl:for-each select="*[not(name()='Rules') and ((not(Attributes/Attribute[@name='Form']) or count(Attributes/Attribute[@name='Form' and @value='false'])=0) and count(Attributes/Attribute[@name='column' and @value=string($columnNumber)])>0)]">
      <xsl:if test="position()>1">,</xsl:if>
      <xsl:apply-templates select="." mode="RenderFormField">
        <xsl:with-param name="collectionName" select="$collectionName"/>
      </xsl:apply-templates>
    </xsl:for-each>
  </xsl:template>

  <xsl:template match="*" mode="RenderFormField">
    <xsl:param name="collectionName"/>
    <xsl:variable name="name" select="name()"/>
    <xsl:variable name="xtype">
      <xsl:apply-templates select="." mode="GetXtype"/>
    </xsl:variable>
    <xsl:if test="Attributes/Attribute[@name='LabelSeperate']/@value='true'">
      {
      xtype:'ewLabelField'
      ,name:'<xsl:value-of select="$name"/>_label'
      ,entityCollection:'<xsl:value-of select="$collectionName"/>'
      ,text:eBook.Language.Worksheets.<xsl:value-of select="$ruleApp"/>.<xsl:value-of select="$collectionName"/>['<xsl:value-of select="$name"/>']
      ,attributes:<xsl:apply-templates select="." mode="GetAttributesList"/>
      ,ruleApp:'<xsl:value-of select="$ruleApp"/>'
      <xsl:if test="count(../*/Attributes/Attribute[@name='InterfaceEventVisibility_Field' and @value=$name])>0">,hasInterfaceEvents:true</xsl:if>
      },
    </xsl:if>
    {
    xtype:'<xsl:value-of select="$xtype"/>'
    ,name:'<xsl:value-of select="$name"/>'
    ,entityCollection:'<xsl:value-of select="$collectionName"/>'
    ,fieldLabel:eBook.Language.Worksheets.<xsl:value-of select="$ruleApp"/>.<xsl:value-of select="$collectionName"/>['<xsl:value-of select="$name"/>']
    ,attributes:<xsl:apply-templates select="." mode="GetAttributesList"/>
    ,ruleApp:'<xsl:value-of select="$ruleApp"/>'
    ,manual:false
    <xsl:if test="Attributes/Attribute[@name='LabelSeperate']/@value='true'">,hideLabel:true</xsl:if>
    <xsl:if test="count(../*/Attributes/Attribute[@name='InterfaceEventVisibility_Field' and @value=$name])>0">,hasInterfaceEvents:true</xsl:if>
    }

  </xsl:template>

  <xsl:template match="*" mode="GetAttributesList">
    {<xsl:for-each select="Attributes/Attribute">
      <xsl:if test="position()>1">,</xsl:if>'<xsl:value-of select="@name"/>':'<xsl:value-of select="@value"/>'
    </xsl:for-each>}
  </xsl:template>

  <xsl:template match="*" mode="GetRecordDataType"><xsl:choose><xsl:when test="Attributes/Attribute[@name='List']">Ext.data.Types.AUTO</xsl:when><xsl:when test="@type='String'">Ext.data.Types.STRING</xsl:when><xsl:when test="@type='Number'">Ext.data.Types.NUMBER</xsl:when><xsl:when test="@type='Boolean'">Ext.data.Types.BOOL</xsl:when><xsl:when test="@type='Integer'">Ext.data.Types.INT</xsl:when><xsl:when test="@type='Date' or @type='DateTime'">Ext.data.Types.WCFDATE</xsl:when><xsl:otherwise>WTF</xsl:otherwise></xsl:choose></xsl:template>
  
  <xsl:template match="*" mode="IsList"><xsl:choose><xsl:when test="Attributes/Attribute[@name='List']">Y</xsl:when><xsl:otherwise>N</xsl:otherwise></xsl:choose></xsl:template>

  <xsl:template match="*" mode="GetXtype"><xsl:choose><xsl:when test="Attributes/Attribute[@name='Interface']">ew<xsl:value-of select="Attributes/Attribute[@name='Interface']/@value"/></xsl:when><xsl:when test="Attributes/Attribute[@name='List']"><xsl:variable name="listName" select="Attributes/Attribute[@name='List']/@value"/>ewList_<xsl:value-of select="$listName"/></xsl:when><xsl:when test="Attributes/Attribute[@name='UIControlInfo' and @value='DropDownEdit']">unidentifiedlist</xsl:when><xsl:otherwise><xsl:choose><xsl:when test="@type='String'">ewtextfield</xsl:when><xsl:when test="@type='Number'">ewnumberfield</xsl:when><xsl:when test="@type='Boolean'">ewcheckbox</xsl:when><xsl:when test="@type='Integer'">ewnumberfield</xsl:when><xsl:when test="@type='Date' or @type='DateTime'">ewdatefield</xsl:when><xsl:otherwise>WTF</xsl:otherwise></xsl:choose></xsl:otherwise></xsl:choose></xsl:template>
  
</xsl:stylesheet>
