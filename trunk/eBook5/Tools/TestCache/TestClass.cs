﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace TestCache
{
    [DataContract]
    public class TestClass
    {
        [DataMember]
        public Guid Id { get; set; }
        [DataMember]
        public string Culture { get; set; }
        [DataMember]
        public string Description { get; set; }
    }
}
