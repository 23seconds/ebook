﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EY.com.eBook.Core.EF;
using System.ServiceModel;
using EY.com.eBook.API.Contracts.Services;
using EY.com.eBook.API.Contracts.Proxies;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.API.BL.Read;
using EY.com.eBook.Core;
using System.Xml.Linq;
using EY.com.eBook.Caching;
using EY.com.eBook.API.Contracts.Data.Cache;
using System.Xml.Xsl;
using System.Xml.XPath;
using System.IO;
using EY.com.eBook.RuleEngine;
using EY.com.eBook.RuleEngine.AY2012;
using EY.com.eBook.RuleEngine.Data;
using System.Threading;
using System.Globalization;
using ProtoBuf.Meta;
using System.Reflection;
using EY.com.eBook.RuleEngine.Core.Interfaces;
using EY.com.eBook.Core.EF.Read;
using iTextSharp.text.pdf;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.API.BL;
using System.Text.RegularExpressions;


namespace TestCache
{
    class Program
    {
        static ServiceHost serviceHost;

        static void PressAnyKey()
        {
            Console.WriteLine("-------------------- // --------------------");
            Console.WriteLine("-------------- press any key ---------------");
            Console.WriteLine("-------------------- // --------------------");
            Console.ReadLine();
            
        }

        public static void RemoveRestrictions(string SourcePdfPath, string OutputPdfPath, string Password)
        {
            try
            {
                PdfReader reader = new PdfReader(SourcePdfPath,true);
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    PdfStamper stamper = new PdfStamper(reader, memoryStream);
                    //stamper.w
                    stamper.Close();
                    reader.Close();
                    System.IO.File.WriteAllBytes(OutputPdfPath, memoryStream.ToArray());
                }
            }
            catch (Exception err)
            {
                Console.WriteLine(err.Message);
            }
        }

        static void GenerateWorksheetsService()
        {
            string[] files = Directory.GetFiles(@"C:\Projects_TFS\EY\Ebook\Main-v5.0\Worksheets\cfg\2012", "*.xml");
            string resultPath = @"C:\Projects_TFS\EY\Ebook\Main-v5.0\Worksheets\";

            XslCompiledTransform transform = new XslCompiledTransform(false);
            transform.Load("RenderWorksheetServices.xslt");


            string cult = "nl-BE";
            string cult2 = "NL";

            foreach (string fle in files)
            {
                string fname = Path.GetFileNameWithoutExtension(fle);

                Console.WriteLine(fname);
                XsltArgumentList argsList = new XsltArgumentList();
                argsList.AddParam("ruleApp", "", fname);
                argsList.AddExtensionObject("urn:Helper", new XsltHelperClass());


                XPathDocument xpd = new XPathDocument(fle);
                StreamWriter sw = new StreamWriter(Path.Combine(resultPath, string.Format("{0}-service.cs", fname)), false, Encoding.UTF8);

                transform.Transform(xpd.CreateNavigator(), argsList, sw);

                sw.Close();
            }
        }

        static void GenerateWorksheetGui()
        {
            string[] files = Directory.GetFiles(@"C:\Projects_TFS\EY\Ebook\Main-v5.0\Worksheets\cfg\", "*.xml", SearchOption.AllDirectories);
            string resultPath = @"C:\Projects_TFS\EY\Ebook\Main-v5.0\Worksheets\js\";

            XslCompiledTransform transform = new XslCompiledTransform(false);
            transform.Load("RenderWorksheets.xslt");

            List<WorksheetType> wtypes = eBookReadManager.Context.WorksheetTypes.Include("Names").ToList();

            List<WorksheetTypeTranslation> wtranslations = eBookReadManager.Context.WorksheetTypeTranslations.ToList();
            

            
            string cult = "nl-BE";
            string cult2 = "nl";
            foreach (string fle in files)
            {
                string fname = Path.GetFileNameWithoutExtension(fle);
                if (fname.Contains("combined")) goto mnext;
                Console.WriteLine(fname);
                string dir = new DirectoryInfo(Path.GetDirectoryName(fle)).Name;

                string dpath = Path.Combine(resultPath, dir);
                if (!Directory.Exists(dpath))
                {
                    Directory.CreateDirectory(dpath);
                }

                XDocument xpd = XDocument.Load(fle);

                Guid typeId = new Guid(xpd.Root.Attribute("TypeId").Value);

               
                XsltArgumentList argsList = new XsltArgumentList();
                argsList.AddParam("AY", "", dir);
                argsList.AddParam("ruleApp", "", fname);
                argsList.AddParam("lang2Char", "", cult2);
                argsList.AddParam("lang5Char", "", cult);
                argsList.AddExtensionObject("urn:Helper", new XsltHelper
                {
                    Culture = CultureInfo.CreateSpecificCulture(cult),
                    CultureStr = cult,
                    Translations = wtranslations.Where(t => t.WorksheetTypeId == typeId).ToList()
                });


                
                StreamWriter sw = new StreamWriter(Path.Combine(resultPath, string.Format(@"{0}\{1}.{2}.js",dir, fname, cult)), false, Encoding.UTF8);

                transform.Transform(xpd.CreateNavigator(), argsList, sw);

                sw.Close();
            mnext:
                Console.Write("");
            }


            cult = "fr-Fr";
            cult2 = "fr";
            foreach (string fle in files)
            {
                string fname = Path.GetFileNameWithoutExtension(fle);
                if (fname.Contains("combined")) goto mnext2;
                Console.WriteLine(fname);
                string dir = new DirectoryInfo(Path.GetDirectoryName(fle)).Name;

                string dpath = Path.Combine(resultPath, dir);
                if (!Directory.Exists(dpath))
                {
                    Directory.CreateDirectory(dpath);
                }

                XDocument xpd = XDocument.Load(fle);

                Guid typeId = new Guid(xpd.Root.Attribute("TypeId").Value);


                XsltArgumentList argsList = new XsltArgumentList();
                argsList.AddParam("AY", "", dir);
                argsList.AddParam("ruleApp", "", fname);
                argsList.AddParam("lang2Char", "", cult2);
                argsList.AddParam("lang5Char", "", cult);
                argsList.AddExtensionObject("urn:Helper", new XsltHelper
                {
                    Culture = CultureInfo.CreateSpecificCulture(cult),
                    CultureStr = cult,
                    Translations = wtranslations.Where(t => t.WorksheetTypeId == typeId).ToList()
                });



                StreamWriter sw = new StreamWriter(Path.Combine(resultPath, string.Format(@"{0}\{1}.{2}.js", dir, fname, cult)), false, Encoding.UTF8);

                transform.Transform(xpd.CreateNavigator(), argsList, sw);

                sw.Close();
            mnext2:
                Console.Write("");
            }

            cult = "en-US";
            cult2 = "en";
            foreach (string fle in files)
            {
                string fname = Path.GetFileNameWithoutExtension(fle);
                if (fname.Contains("combined")) goto mnext3;
                Console.WriteLine(fname);
                string dir = new DirectoryInfo(Path.GetDirectoryName(fle)).Name;

                string dpath = Path.Combine(resultPath, dir);
                if (!Directory.Exists(dpath))
                {
                    Directory.CreateDirectory(dpath);
                }

                XDocument xpd = XDocument.Load(fle);

                Guid typeId = new Guid(xpd.Root.Attribute("TypeId").Value);


                XsltArgumentList argsList = new XsltArgumentList();
                argsList.AddParam("AY", "", dir);
                argsList.AddParam("ruleApp", "", fname);
                argsList.AddParam("lang2Char", "", cult2);
                argsList.AddParam("lang5Char", "", cult);
                argsList.AddExtensionObject("urn:Helper", new XsltHelper
                {
                    Culture = CultureInfo.CreateSpecificCulture(cult),
                    CultureStr = cult,
                    Translations = wtranslations.Where(t => t.WorksheetTypeId == typeId).ToList()
                });



                StreamWriter sw = new StreamWriter(Path.Combine(resultPath, string.Format(@"{0}\{1}.{2}.js", dir, fname, cult)), false, Encoding.UTF8);

                transform.Transform(xpd.CreateNavigator(), argsList, sw);

                sw.Close();
            mnext3:
                Console.Write("");
            }
             

        }

        static void UpdateLists()
        {
            string src = @"C:\Projects_TFS\EY\Ebook\Main-v5.0\EY.com.eBook.RuleEngine\lists.xml";
            string dest = @"C:\Projects_TFS\EY\Ebook\Main-v5.0\EY.com.eBook.RuleEngine\lists_2.xml";

            List<EY.com.eBook.Core.EF.Write.List> lsts = eBookWriteManager.Context.ListSet.ToList();
            List<EY.com.eBook.Core.EF.Write.ListItem> items = eBookWriteManager.Context.ListItemSet.ToList();
            List<EY.com.eBook.Core.EF.Write.Names> names = eBookWriteManager.Context.NamesSet.ToList();

            XDocument xdoc = XDocument.Load(src);

            foreach (EY.com.eBook.Core.EF.Write.List lt in lsts)
            {
                XElement xe = xdoc.XPathSelectElement(string.Format("//Lists/List[@id='{0}']", lt.Id.ToString().ToUpper()));
                if (xe == null)
                {
                    xe = xdoc.XPathSelectElement(string.Format("//Lists/List[@id='{0}']", lt.Id.ToString().ToLower()));
                }
                if (xe == null)
                {
                    xe = new XElement("List", new XAttribute("id", lt.Id.ToString()), new XAttribute("key", lt.Name));

                    foreach (EY.com.eBook.Core.EF.Write.ListItem li in items.Where(l=>l.ListId==lt.Id))
                    {
                        XElement xli = new XElement("ListItem", new XAttribute("id", li.Id.ToString()));
                        foreach (EY.com.eBook.Core.EF.Write.Names name in names.Where(n => n.ObjectId == li.Id))
                        {
                            xli.Add(new XElement("culture", new XAttribute("id", name.Culture), name.Description));
                        }
                        xe.Add(xli);

                    }
                    xdoc.Root.Add(xe);
                }
            }
            xdoc.Save(dest);
        }

        static void ImportLists()
        {
            string src = @"C:\Projects_TFS\EY\Ebook\Main-v5.0\EY.com.eBook.RuleEngine\lists.xml";

            List<EY.com.eBook.Core.EF.Write.List> lsts = eBookWriteManager.Context.ListSet.ToList();
            List<EY.com.eBook.Core.EF.Write.ListItem> items = eBookWriteManager.Context.ListItemSet.ToList();
            List<EY.com.eBook.Core.EF.Write.Names> names = eBookWriteManager.Context.NamesSet.ToList();

            XDocument xdoc = XDocument.Load(src);

            foreach (XElement el in xdoc.Root.Elements("List"))
            {
                Guid id = new Guid(el.Attribute("id").Value);
                if (lsts.Count(l => l.Id == id) == 0)
                {
                    EY.com.eBook.Core.EF.Write.List li = new EY.com.eBook.Core.EF.Write.List
                    {
                        Id = id
                        ,
                        Description = el.Attribute("key").Value
                        ,
                        Name = el.Attribute("key").Value
                    };
                    if (li.EntityState == System.Data.EntityState.Detached)
                        eBookWriteManager.Context.AddToListSet(li);
                    eBookWriteManager.Context.SaveChanges();

                    
                }

                foreach (XElement eli in el.Elements("ListItem"))
                {
                    Guid lid = new Guid(eli.Attribute("id").Value);
                    if (items.Count(l => l.Id == lid) == 0)
                    {
                        EY.com.eBook.Core.EF.Write.ListItem lit = new EY.com.eBook.Core.EF.Write.ListItem
                        {
                            Id = lid
                            ,
                            AssessmentYear = null
                            ,
                            EndDate = null
                            ,
                            StartDate = null
                            ,
                            ListId = id
                        };
                        if (lit.EntityState == System.Data.EntityState.Detached)
                            eBookWriteManager.Context.AddToListItemSet(lit);
                        eBookWriteManager.Context.SaveChanges();

                    }

                    foreach (XElement tel in eli.Elements("culture"))
                    {
                        string c = tel.Attribute("id").Value;
                        if (names.Count(n => n.ObjectId == lid && n.Culture == c) == 0)
                        {
                            EY.com.eBook.Core.EF.Write.Names nm = new EY.com.eBook.Core.EF.Write.Names
                            {
                                ObjectId = lid
                                ,
                                Culture = c
                                ,
                                Description = tel.Value
                            };
                            if (nm.EntityState == System.Data.EntityState.Detached)
                                eBookWriteManager.Context.AddToNamesSet(nm);
                            eBookWriteManager.Context.SaveChanges();

                        }
                    }
                }
            }
        }

        static void TestRuleEngine()
        {
            MetaType mt = RuntimeTypeModel.Default[typeof(IWorksheet)];
            int nr = 300;
            Assembly ass = Assembly.Load("EY.com.eBook.RuleEngine.Contracts");
            foreach (Type t in ass.GetTypes())
            {
                if (t.GetInterface("IWorksheet") != null)
                {
                    mt.AddSubType(nr, t);
                    nr++;
                }
            }

            Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;
            Thread.CurrentThread.CurrentUICulture = CultureInfo.InvariantCulture;
            new CacheRepository().LoadCoeffsFromDB();
            new CacheRepository().LoadWorksheetTypeTranslations();
            new CacheRepository().LoadMessageTranslations();

            Console.WriteLine("Start ruleEngine test");
            Guid fileId = new Guid("0C614B5A-E957-431D-847F-93C9A4CE4C36");

            //CacheProxy cp = new CacheProxy();
            //cp.Open();
            CacheRepository cp = new CacheRepository();
            //cp.Initialize();
            //cp.Close();
            Console.WriteLine("Cache initialized");

            DateTime beforeCache = DateTime.Now;
            //cp = new CacheProxy();
            //cp.Open();
            cp.ReloadFileInCache(fileId,true);
            //cp.Close();
            DateTime afterCache = DateTime.Now;

            RuleEngine2012 re = new RuleEngine2012();
            Console.WriteLine("TEST: empty set");
           // PressAnyKey();
            DateTime beforeGet = DateTime.Now;
            WorksheetSet ws = re.GetSet(fileId);
            DateTime afterGet = DateTime.Now;
            Console.WriteLine("TEST: Get set from cache");
            DateTime beforeGetc = DateTime.Now;
            ws = re.GetSet(fileId);
            DateTime afterGetc = DateTime.Now;
            Console.WriteLine("TEST: Calculate all");
            //PressAnyKey();
            re.CalculateAll(ws);
            
            DateTime afterCall = DateTime.Now;

            EY.com.eBook.RuleEngine.AY2012.FacturatieAppTypes.FacturatieKlant fk = new EY.com.eBook.RuleEngine.AY2012.FacturatieAppTypes.FacturatieKlant
            {
                Id = Guid.Empty
                ,
                Bedrag = 3400
                ,
                Boeken = true
                ,
                FacJaar = new StringListItemDataContract { Id = "2010", NL = "2010" }
                ,
                FactuurNr = "45454"
                ,
                Grootboek = new StringListItemDataContract { Id = "440000000000", NL = "Leveranciers" }
                ,
                Klant = new StringListItemDataContract { Id = Guid.Empty.ToString(), NL = "Joske vermeulen" }

            };

            EY.com.eBook.RuleEngine.AY2012.FacturatieAppTypes.FacturatieKlantCN fk2 = new EY.com.eBook.RuleEngine.AY2012.FacturatieAppTypes.FacturatieKlantCN
            {
                Id = Guid.Empty
                ,
                Bedrag = 3400
                ,
                Boeken = true
                ,
                FacJaar = new StringListItemDataContract { Id = "2010", NL = "2010" }
                ,
                FactuurNr = "45454"
                ,
                Grootboek = new StringListItemDataContract { Id = "440000000000", NL = "Leveranciers" }
                ,
                Klant = new StringListItemDataContract { Id = Guid.Empty.ToString(), NL = "Joske vermeulen" }

            };
            EY.com.eBook.RuleEngine.Data.AY2012.CriteriaFacturatieAppFacturatieKlantDataContract cf = new EY.com.eBook.RuleEngine.Data.AY2012.CriteriaFacturatieAppFacturatieKlantDataContract
            {
                Id = fileId
                ,
                CollectionPath = "OpTeMakenFacturen"
                ,
                Culture = "nl-BE"
                ,
                Entity = fk
            };

            EY.com.eBook.RuleEngine.Data.AY2012.CriteriaFacturatieAppFacturatieKlantCNDataContract cf2 = new EY.com.eBook.RuleEngine.Data.AY2012.CriteriaFacturatieAppFacturatieKlantCNDataContract
            {
                Id = fileId
                ,
                CollectionPath = "OpTeMakenCN"
                ,
                Culture = "nl-BE"
                ,
                Entity = fk2
            };

            //List<InvalidField> ivs = re.FacturatieAppFacturatieKlantValidate(cf);
            DateTime afterValid = DateTime.Now;
            //Console.WriteLine("Invalid? " + ivs.Count());

            //re.FacturatieAppOpTeMakenFacturenAdd(cf);
           
            DateTime afterAddAndCalc = DateTime.Now;

            //re.FacturatieAppOpTeMakenCNAdd(cf2);

            DateTime beforeSer = DateTime.Now;
            string xs = ""; // CoreHelper.SerializeToString(ws.setHelper.WorksheetData[FacturatieApp.TypeId].WorksheetData);
            DateTime afterSer = DateTime.Now;

            DateTime beforeSer2 = DateTime.Now;
            string bs = ""; //CoreHelper.SerializeToStringProto(ws.setHelper.WorksheetData[FacturatieApp.TypeId].WorksheetData);
            DateTime afterSer2 = DateTime.Now;

            Console.WriteLine("DONE::");
            Console.WriteLine("Time to load file in cache: {0}ms", (afterCache - beforeCache).TotalMilliseconds);
            Console.WriteLine("Time to get set: {0}ms", (afterGet - beforeGet).TotalMilliseconds);
            Console.WriteLine("Time to get cached set: {0}ms", (afterGetc - beforeGetc).TotalMilliseconds);
            Console.WriteLine("Time to calculate all: {0}ms", (afterCall - afterGet).TotalMilliseconds);
            Console.WriteLine("Time to validate: {0}ms", (afterValid - afterCall).TotalMilliseconds);
            Console.WriteLine("Time to Add: {0}ms", (afterAddAndCalc - afterValid).TotalMilliseconds);
            //Console.WriteLine("Time to Recalculate: {0}ms", (afterValid - afterCall).Milliseconds);
            Console.WriteLine("Time to serialize datacontract: {0}ms", (afterSer - beforeSer).TotalMilliseconds);
            Console.WriteLine("Time to serialize & base64 encode: {0}ms", (afterSer2 - beforeSer2).TotalMilliseconds);
            Console.WriteLine(" ");
            Console.WriteLine(xs);
            Console.WriteLine(bs);

            PressAnyKey();

        }

        static void RenderWorksheettypes()
        {
            WorksheetType wt;
            //wt.Names

            List<WorksheetType> types = eBookReadManager.Context.WorksheetTypes.Include("Names")
                                            .OrderBy(w => w.Type)
                                            .ThenBy(w => w.Order)
                                            .ToList();

            StreamWriter sw = System.IO.File.CreateText("worksheettypes-AL-nl-BE.json");
            sw.Write(BuildTypeList(types.Where(t => t.Type == "AL").ToList(), "nl-BE"));
            sw.Close();

            sw = System.IO.File.CreateText("worksheettypes-FI-nl-BE.json");
            sw.Write(BuildTypeList(types.Where(t => t.Type == "FI").ToList(), "nl-BE"));
            sw.Close();

            sw = System.IO.File.CreateText("worksheettypes--nl-BE.json");
            sw.Write(BuildTypeList(types, "nl-BE"));
            sw.Close();

            sw = System.IO.File.CreateText("worksheettypes-AL-fr-FR.json");
            sw.Write(BuildTypeList(types.Where(t => t.Type == "AL").ToList(), "fr-FR"));
            sw.Close();

            sw = System.IO.File.CreateText("worksheettypes-FI-fr-FR.json");
            sw.Write(BuildTypeList(types.Where(t => t.Type == "FI").ToList(), "fr-FR"));
            sw.Close();

            sw = System.IO.File.CreateText("worksheettypes--fr-FR.json");
            sw.Write(BuildTypeList(types, "fr-FR"));
            sw.Close();

            sw = System.IO.File.CreateText("worksheettypes-AL-en-US.json");
            sw.Write(BuildTypeList(types.Where(t => t.Type == "AL").ToList(), "en-US"));
            sw.Close();

            sw = System.IO.File.CreateText("worksheettypes-FI-en-US.json");
            sw.Write(BuildTypeList(types.Where(t => t.Type == "FI").ToList(), "en-US"));
            sw.Close();

            sw = System.IO.File.CreateText("worksheettypes--en-US.json");
            sw.Write(BuildTypeList(types, "en-US"));
            sw.Close();
            

        }

        static string BuildTypeList(List<WorksheetType> types, string culture)
        {
            StringBuilder sb = new StringBuilder("{types:[");
            int cnt = 0;
            foreach (WorksheetType wt in types)
            {
                if (wt.Type == "AL" || wt.Type == "FI")
                {
                    if (cnt > 0) sb.Append(",");
                    sb.Append("{");
                    sb.Append(string.Format("id:'{0}',name:'{1}',ruleApp:'{2}', type:'{3}'"
                                , wt.Id.ToString(), wt.Names.First(n => n.Culture == culture).Description.Replace("'","\\'")
                                , wt.RuleApp,wt.Type));
                    sb.Append("}");
                    cnt++;
                }
            }
            sb.Append("]}");
            return sb.ToString();
        }
        public void CheckThisOut()
        {
            Guid puntEbookid = Guid.NewGuid();


            EY.com.eBook.Core.EF.Write.File file =  eBookWriteManager.Context.Files.First(f => f.Id == puntEbookid);
            EY.com.eBook.Core.EF.Write.Client client= eBookWriteManager.Context.ClientSet.First(c => c.Id == file.ClientId);
            EY.com.eBook.Core.EF.Write.FileXbrl fx = eBookWriteManager.Context.FileXbrlSet.First(f=>f.FileId==puntEbookid);
            BizTaxDataContract btdc = CoreHelper.DeserializeFromString<BizTaxDataContract>(fx.XbrlData);

            EY.com.eBook.Core.EF.Write.BizTaxDeclaration btd = new EY.com.eBook.Core.EF.Write.BizTaxDeclaration
            {
                Id=puntEbookid
                , BiztaxCalc=null
                , ClientName = client.Name
                , Department = fx.DeclaringDepartment
                , eBookCalc =0
                , ClientEnterprise = client.EnterpriseNumber
                , CurrentPath =""
                
                 , AssessmentYear = file.EndDate.AddDays(1).Year
                 , ClientId = client.Id
                 , Errors = null
                 , FileType = "F"
                 , InProgressBy = null
                 , LastChanged = DateTime.Now
                 , PartnerId = fx.DeclaringPartnerId.Value
                 , PartnerName = fx.DeclaringPartnerName
                 , Proxy = true
                 , SenderDate = btdc.LastSaved.Value
                 , SenderName = ""
                 , Status = 10 // 20 / 30/99/-99
            };
            eBookWriteManager.Context.AddToBizTaxDeclarationSet(btd);
            eBookWriteManager.Context.SaveChanges();
        }

        static void PostMigration2012s(int start , int end)
        {
            /*
            string dte = string.Format("{0}_{1}_{2}-{3}_{4}_{5}", DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second);
            FileCache.Current.CacheRefreshFrequency = new TimeSpan(0, 1, 0);

            Console.WriteLine("Loading IWORKSHEET CLASSES...");
            Dictionary<string, List<Type>> _iWorksheetClassTypes = new Dictionary<string, List<Type>>();

           
            _iWorksheetClassTypes = new EY.com.eBook.RuleEngine.Data.Prepper().PrepareProtobuf();
          
           

            Console.WriteLine("Loading COEFFICIENTS...");
            new CacheRepository().LoadCoeffsFromDB();
            Console.WriteLine("Loading TRANSLATIONS...");
            new CacheRepository().LoadIWorksheetClasses(_iWorksheetClassTypes);
            new CacheRepository().LoadWorksheetTypeTranslations();
            new CacheRepository().LoadMessageTranslations();
            Console.WriteLine("Loading MAPPINGS...");
            new CacheRepository().LoadMappings();
            Console.WriteLine("DONE PREPARING CACHE...");
            Console.WriteLine("Loading files...");
            List<Guid> files = eBookReadManager.Context.Files.Include("Client").ToList().Where(f =>f.Client!=null && f.EndDate.AddDays(1).Year == 2013 && !f.CloseDate.HasValue && !f.Deleted).Select(f=>f.Id).ToList();
           
          //  List<Guid> files = new List<Guid> { new Guid("b1737862-4f4e-41df-92ce-6bb984e11510") };
            if (end == 0) end = files.Count - start;
            Console.WriteLine("end: " + end);
            int cnt = start+1;
            int ttl = files.Count;
            XElement root = new XElement("root");
            foreach (Guid file in files.Skip(start-1).Take(end+1 - start)) //
            {
                CacheRepository cr = new CacheRepository();
                if (cnt % 100 == 0)
                {
                    eBookWriteManager.SetContextNull();
                    Console.WriteLine("Waiting for garbage collector");
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                    System.Threading.Thread.Sleep(6000);
                    Console.WriteLine("MEM:" + GC.GetTotalMemory(true));
                    
                }
                Console.WriteLine("{0}/{1}", cnt, ttl);
                RuleEngine2012 re2012 = new RuleEngine2012();
                RuleEngine2013 re2013 = new RuleEngine2013();
               try
               {
                cr.LoadFileInCache(file);
                int ay = eBookWriteManager.Context.Files.First(f => f.Id == file).EndDate.AddDays(1).Year;
                
                   
                    if (ay == 2013)
                    {
                        EY.com.eBook.RuleEngine.AY2013.WorksheetSet set3 = re2013.GetSet(file);
                        EY.com.eBook.RuleEngine.AY2012.WorksheetSet set2 = re2013.GetPreviousSet(file);
                        //set.DataContainer.Client.Name
                        var va = set3.DataContainer.VoorzieningenApp;
                        var vaPrev = set2.DataContainer.VoorzieningenApp;
                        va.Data.ToestandEindVorigBoekjaar.ForEach(t => { t.Nr = null;});
                        va.Data.ToestandEindVorigBoekjaar.ForEach(t =>
                        {
                            var vprev = vaPrev.Data.ToestandEindHuidigBoekjaar.FirstOrDefault(v => v.Id == t.Id);
                            if (vprev!=null)
                            {
                                if (vprev.Assesmentyear != null && t.Assesmentyear == null)
                                {
                                    t.Assesmentyear = vprev.Assesmentyear;
                                }
                                if (vprev.Nr != null)
                                {
                                    if (va.Data.ToestandEindVorigBoekjaar.Count(c => c.Id != t.Id && c.Assesmentyear == t.Assesmentyear && c.Nr == vprev.Nr) == 0)
                                    {
                                        t.Nr = vprev.Nr;
                                    }
                                }
                                
                            }
                            if (t.Assesmentyear == null)
                            {
                                t.Assesmentyear = ay - 1;
                            }
                        });
                        set3.ForceRecalculation(null);
                        set3 = null;
                    }
               }
                catch (Exception e)
                {
                    root.Add(new XElement("failed", new XAttribute("fileid", file.ToString()), new XElement("Msg", e.Message), new XElement("Stack", e.StackTrace)));
                    root.Save(string.Format("failures_{0}.xml",dte));
                    Console.WriteLine("FAILED");

                }
                finally
                {
                    FileCache.Current.Remove(FileCache.SchemaKey + file.ToString());
                    FileCache.Current.Remove(FileCache.WorksheetskKey + file.ToString());

                }
             
                cr = null;
                re2012 = null;
                re2013 = null;
                cnt++;
            }*/
        }

       

        static void MainRecalcONE(string[] args)
        {
            Console.WriteLine("Loading COEFFICIENTS...");
            new CacheRepository().LoadCoeffsFromDB();
            Console.WriteLine("Loading TRANSLATIONS...");
            new CacheRepository().LoadWorksheetTypeTranslations();
            new CacheRepository().LoadMessageTranslations();
            Console.WriteLine("Loading MAPPINGS...");
            new CacheRepository().LoadMappings();
            Console.WriteLine("DONE PREPARING CACHE...");
            Console.WriteLine("Loading files...");
            Console.WriteLine("");
      //      Console.WriteLine("Enter File Id:");
          // string sf = Console.ReadLine();
            //Guid file = new Guid(args[0]);//new Guid("28da0605-0435-4377-8278-949dad9c1cee"); //
            Guid file = new Guid("a2700d0a-2b01-41fa-aa04-8bc5f6d734e4"); //
            CacheRepository cr = new CacheRepository();
            Console.WriteLine("Loading file in local cache");
            cr.LoadFileInCache(file);
            RuleEngine2012 re = new RuleEngine2012();
            //try
            //{
                Console.WriteLine("Get set of worksheets");
                WorksheetSet set = re.GetSet(file);
                Console.WriteLine("Force recalc");
                set.ForceRecalculation(null);
                set = null;
           /* }
            catch (Exception e)
            {
                
                Console.WriteLine("FAILED");
                Console.WriteLine(e.Message);
            }
            finally
            {
                Console.WriteLine("Update active eBook Cache");
                //CacheProxy cp = new CacheProxy();
                //cp.Open();
                //cp.ReloadOrAddFileInCacheForce(file, true);
                //cp.Close();
            }*/
        }

        static void MainTestCachingUptime(string[] args)
        {
            // test caching up time.
            Console.WriteLine("Press any key to stop");
            do
            {
                while (!Console.KeyAvailable)
                {
                    Thread.Sleep(5 * 1000);
                    try
                    {
                        CacheProxy cp = new CacheProxy();
                        cp.Open();
                        cp.TestException(0);
                        cp.Close();
                        Console.WriteLine("{0} Success comm", DateTime.Now.ToLongTimeString());
                    }
                    catch
                    {
                        Console.WriteLine("{0} Communication failed", DateTime.Now.ToLongTimeString());
                    }
                    // Do something
                }
            } while (Console.ReadKey(true).Key != ConsoleKey.Escape);
            Console.WriteLine("Stopped");
        }

        static void MainBizTaxClean(string[] args) //BizTaxClean
        {
            //.Where(f=>f.StatusId<10)
            
            foreach (EY.com.eBook.Core.EF.Write.FileXbrl fx in eBookWriteManager.Context.FileXbrlSet.ToList())
            {

                Console.WriteLine("Cleaning {0}", fx.FileId.ToString());
                BizTaxDataContract btdc = CoreHelper.DeserializeFromString<BizTaxDataContract>(fx.XbrlData);

                btdc = CleanDoubles(btdc);
                btdc = CleanContexts(btdc);
                foreach (XbrlElementDataContract xedc in btdc.Elements.Where(x =>
                    x.Name == "CapitalGainsShares"
                    || x.Name == "CapitalGainsSharesReversalPreviousExemptionsAdjustmentReservesPlus"
                    || x.Name == "DefinitiveExemptionTaxShelterAuthorisedAudiovisualWorksAdjustmentReservesPlus"
                    || x.Name == "ExemptionRegionalGrantsCapitalSubsidiesInterestSubsidiesAdjustmentReservesPlus"
                    || x.Name == "FinalExemptionProfitHomologationReorganizationPlanAmicableSettlement"
                    || x.Name == "OtherAdjustmentsReservesPlus"
                    || x.Name == "AdjustmentsReservesMinus"))
                {
                    xedc.Period = "D";

                }
                foreach (XbrlElementDataContract xedc in btdc.Elements.Where(x => x.Name == "DeductionsEquityAllowanceCorporateEquity" && x.Period == "D"))
                {
                    xedc.Period = "I-Start";

                }
                fx.XbrlData = CoreHelper.SerializeToString(btdc);
                eBookWriteManager.Context.SaveChanges();
            }
          
            Console.WriteLine("DONE");
            Console.ReadLine();
        }

        static BizTaxDataContract CleanContexts(BizTaxDataContract btdc)
        {
            foreach (ContextElementDataContract cedc in btdc.Contexts)
            {
                if (cedc.Scenario != null)
                {
                    foreach (ContextScenarioDataContract csdc in cedc.Scenario)
                    {
                        switch (csdc.Dimension)
                        {
                            case "d-ty:WriteDownDebtClaimTypedDimension":
                                csdc.Type = "d-ty:WritedownDebtClaimTypedID";
                                break;
                            case "d-ty:ProvisionRiskExpenseTypedDimension":
                                csdc.Type = "d-ty:ProvisionRiskExpenseTypedID";
                                break;
                            case "d-ty:DescriptionTypedDimension":
                                csdc.Type = "d-ty:DescriptionTypedID";
                                break;
                            case "d-ty:DateTypedDimension":
                                csdc.Type = "d-ty:DateTypedID";
                                break;
                        }
                    }
                }
            }



            return btdc;
        }

        static BizTaxDataContract CleanDoubles(BizTaxDataContract btdc)
        {
            List<XbrlElementDataContract> elements = new List<XbrlElementDataContract>();
            foreach (string id in btdc.Elements.Select(e => e.Id).Distinct())
            {
                elements.Add(btdc.Elements.First(e => e.Id == id));
            }
            btdc.Elements = elements;
            return btdc;
        }

        private static void GenerateMappingsJS()
        {
          /*  MappingRepository mr = new MappingRepository();
            List<MappingDataContract> mappings = mr.GetMappings("nl-BE");
            XDocument xdoc = XDocument.Parse(CoreHelper.SerializeToXElement(mappings,true).ToString());
            xdoc.Save("mappings-nl.xml");

            mappings = mr.GetMappings("fr-FR");
            xdoc = XDocument.Parse(CoreHelper.SerializeToXElement(mappings,true).ToString());
            xdoc.Save("mappings-fr.xml");

            mappings = mr.GetMappings("en-US");
            xdoc = XDocument.Parse(CoreHelper.SerializeToXElement(mappings,true).ToString());
            xdoc.Save("mappings-en.xml");*/

            XslCompiledTransform transform = new XslCompiledTransform(true);
            transform.Load("mappings.xslt");

            XsltArgumentList argsList = new XsltArgumentList();

            argsList.AddExtensionObject("urn:Helper", new XsltHelper());

            XDocument xpd = XDocument.Load(@"mappings-nl.xml");

            XElement root = xpd.Root.StripNamespaces();
            StreamWriter sw = new StreamWriter(@"mappings-nl-BE.js", false, Encoding.UTF8);

            transform.Transform(root.CreateNavigator(), argsList, sw);
            sw.Close();

            xpd = XDocument.Load(@"mappings-fr.xml");
            root = xpd.Root.StripNamespaces();
            sw = new StreamWriter(@"mappings-fr-FR.js", false, Encoding.UTF8);

            transform.Transform(root.CreateNavigator(), argsList, sw);
            sw.Close();

            xpd = XDocument.Load(@"mappings-en.xml");
            root = xpd.Root.StripNamespaces();
            sw = new StreamWriter(@"mappings-en-US.js", false, Encoding.UTF8);

            transform.Transform(root.CreateNavigator(), argsList, sw);
            sw.Close();
        }

        static void Main_test(string[] args)
        {
            DateTime inschrijf = new DateTime(2011, 6, 21);
            DateTime astart = new DateTime(2011, 10, 10);
            DateTime aend = new DateTime(2013, 12, 31);

            DateTime inschrijfDate = new DateTime(inschrijf.Year, inschrijf.Month, 1);

            DateRange inschrijfPeriod = new DateRange();
            inschrijfPeriod.Start = new DateTime(astart.Year, inschrijfDate.Month, 1);
            inschrijfPeriod.End = inschrijfPeriod.Start.AddYears(1).AddDays(-1);

            List<DateRange> ranges = new List<DateRange>();

            if (inschrijfPeriod.Start > astart) ranges.Add(new DateRange(astart, inschrijfPeriod.Start.AddDays(-1)));
            if (inschrijfPeriod.Start < astart) inschrijfPeriod.Start = astart;
            DateTime yearEnd = new DateTime(astart.Year, 12, 31);

            while (inschrijfPeriod.Start < aend)
            {
                if (inschrijfPeriod.End > yearEnd)
                {
                    ranges.Add(new DateRange(inschrijfPeriod.Start, yearEnd));
                    inschrijfPeriod.Start = yearEnd.AddDays(1);
                    yearEnd = yearEnd.AddYears(1);
                }
                else
                {
                    ranges.Add(new DateRange(inschrijfPeriod.Start, inschrijfPeriod.End));
                    inschrijfPeriod = new DateRange(inschrijfPeriod.End.AddDays(1), inschrijfPeriod.End.AddYears(1));
                }
            }
            foreach (DateRange dr in ranges)
            {
                Console.WriteLine("{0} - {1}", dr.Start.ToShortDateString(), dr.End.ToShortDateString());
            }
            Console.ReadLine();
        }

        static void RecalcAllFiles(int[] assessmentyears, bool reimportPrevious)
        {
            FileCache.Current.CacheRefreshFrequency = new TimeSpan(0, 1, 0);

            Dictionary<string, List<Type>> _iWorksheetClassTypes = new Dictionary<string, List<Type>>();

         
            _iWorksheetClassTypes = new EY.com.eBook.RuleEngine.Data.Prepper().PrepareProtobuf();


            CacheRepository cr = new CacheRepository();
            cr.LoadCoeffsFromDB();
            int mcnt = cr.LoadIWorksheetClasses(_iWorksheetClassTypes);
            mcnt = cr.LoadWorksheetTypeTranslations();
            mcnt = cr.LoadMessageTranslations();
            mcnt = cr.LoadMappings();
            Console.WriteLine("CACHE Mappings loaded " + mcnt);
          
           
            // clear old auto tab 
           

            var files = eBookWriteManager.Context.Files.Where(f=>!f.Deleted).ToList().Where(f => assessmentyears.Contains(f.EndDate.AddDays(1).Year))                .Select(f => new { Id =f.Id,AssessmentYear = f.EndDate.AddDays(1).Year }).ToList();

           
            int ttl = files.Count;
            int cnt = 0;
            foreach (var file in files)
            {
                cnt++;
                if (cnt % 100 == 0)
                {
                    eBookWriteManager.SetContextNull();
                    Console.WriteLine("Waiting for garbage collector");
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                    System.Threading.Thread.Sleep(1000);
                    Console.WriteLine("MEM:" + GC.GetTotalMemory(true));
                    Thread.Sleep(100);
                }

                Console.WriteLine("{0}/{1}", cnt, ttl);

                cr.LoadFileInCache(file.Id);


                try
                {

                    switch (file.AssessmentYear)
                    {
                        case 2014:
                            RuleEngine2014 re2014 = new RuleEngine2014();
                            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = re2014.GetSet(file.Id);

                            if (reimportPrevious)
                            {
                               // set.ImportPrevious();
                            }
                            else
                            {
                               
                            }
                            set.ForceRecalculation(null);
                            set = null;
                            break;
                        case 2015:
                            RuleEngine2015 re2015 = new RuleEngine2015();
                            EY.com.eBook.RuleEngine.AY2015.WorksheetSet set15 = re2015.GetSet(file.Id);
                            if (reimportPrevious)
                            {
                               // set15.ImportPrevious();
                            }
                            else
                            {
                                
                            }
                            set15.ForceRecalculation(null);
                            set15 = null;

                            break;
                        default:
                            Console.WriteLine("Assessmentyear {0}, not implemented in recalc, add if required", file.AssessmentYear);
                            break;
                    }

                }
                catch (Exception e)
                {

                    Console.WriteLine("FAILED");
                    Console.WriteLine(e.Message);
                }
                // keep mem a bit clean
                cr.RemoveFileFromCache(file.Id);
              
             
                /* }                 catch (Exception e)                 {                                     Console.WriteLine("FAILED");                     Console.WriteLine(e.Message);                 }                 finally                 {                     Console.WriteLine("Update active eBook Cache");                     //CacheProxy cp = new CacheProxy();                     //cp.Open();                     //cp.ReloadOrAddFileInCacheForce(file, true);                     //cp.Close();                 }*/
            }
        }

        static void Main_CORRECTVA(string[] args)
        {
            Guid id = new Guid("67855d3d-4e11-4c7b-96a3-e6602ddaaa6f");
            // clear old auto tab 
            List<EY.com.eBook.Core.EF.Write.File> files = eBookWriteManager.Context.Files.ToList().Where(f => f.EndDate.AddDays(1).Year > 2012).ToList();
            int ttl = files.Count;
            int cnt = 0;
            foreach (EY.com.eBook.Core.EF.Write.File file in files )
            {
                cnt++;
                Console.WriteLine("{0}/{1}", cnt, ttl);
                foreach (EY.com.eBook.Core.EF.Write.Worksheet ws in eBookWriteManager.Context.Worksheets.Where(w=>w.FileId==file.Id).ToList())
                {
                    if (ws.Data.Contains("AY2011"))
                    {
                        ws.Data = ws.Data.Replace("AY2011", "AY2013");
                    }
                    if (ws.Data.Contains("AY2012"))
                    {
                        ws.Data = ws.Data.Replace("AY2012", "AY2013");
                    }
                    
                    if (ws.TypeId==id)
                    {
                        XElement el = XElement.Parse(ws.Data);
                        XElement el2 = el.Element(XName.Get("VoordelenVanAlleAardAuto", "AY2013.VoordelenApp"));
                        if (el2 != null)
                        {
                            el2.Remove();
                            ws.Data = el.ToString();
                        }
                        
                    }
                    eBookWriteManager.Context.SaveChanges();
                     
                }
            }
        }
        static void Main(string[] args)
        {

            if (args != null && args.Length > 0)
            {
                if (args.Any(a => a.ToUpper() == "RECALC") && args.Length==2)
                {
                    string[] asses = args[1].Split(new char[] { ',' },StringSplitOptions.RemoveEmptyEntries);
                    int[] iasses = asses.Select(a => int.Parse(a)).ToArray();
                    RecalcAllFiles(iasses, false);
                    Console.WriteLine("Done");
                    Console.ReadLine();
                }
            }
            else
            {
                // write out specs
                Console.WriteLine("Arguments:");
                Console.WriteLine("RECALC {comma delimited list assessmentyears}");

            }

           // GenerateMappingsJS();
           /*

            int start = 250;
            int end = 270;
            if (args.Length == 1)
            {
                start = int.Parse(args[0]);
            }
            else if (args.Length > 1)
            {
                start = int.Parse(args[0]);
                end = int.Parse(args[1]);
            }

            Console.WriteLine("Start from:" + start);
            Console.WriteLine("To:" + end);
          //  PressAnyKey();

           PostMigration2012s(start,end);

        */
            
          
            

            /*
            Guid fid = new Guid("2B25100C-6EEB-4187-AA5A-E676B95A5B12");
            CacheProxy cp = new CacheProxy();
            cp.Open();
            Console.WriteLine("is in cache:" + cp.FileInCache(fid));
            
            Console.ReadLine();
            DateTime nw = DateTime.Now;
            
            cp.ReloadOrAddFileInCache(fid);
            
            Console.WriteLine("TOOK: {0}ms", (DateTime.Now - nw).Milliseconds);
            
            Console.WriteLine("is in cache:" + cp.FileInCache(fid));


            Console.WriteLine("Press any to load entire accountschema");
            Console.ReadLine();

            nw = DateTime.Now;
            List<AccountDataContract> acs = cp.GetAllAccounts(fid,true,true,"nl-BE");
            Console.WriteLine("TOOK: {0}ms", (DateTime.Now - nw).Milliseconds);
            cp.Close();
            Console.WriteLine("");
            Console.WriteLine("Press any to show first 100");
            Console.ReadLine();
            foreach(AccountDataContract adc in acs.Take(100)) {
                Console.WriteLine("{0} {1}",adc.VisualNr, adc.DefaultDescription);
            }

            /*
            ProAccServiceProxy psp = new ProAccServiceProxy();
            psp.Open();
            List<ProAccAccountDataContract> pacs= psp.ProAccGetAccounts(new EY.com.eBook.ProAcc.API.Contracts.Data.CriteriaProAccStartAndEndDatesDataContract
            {
                Server = @"DEFRANMCEASPV26\ASPVS26"
                ,
                Database = "BRCALACAPITALBVBA"
                , End = new DateTime(2011,12,31)
                , Start = new DateTime(2011,1,1)
                , StartPrevious= new DateTime(2010,1,1)
                , EndPrevious = new DateTime(2010,12,31)
            });
            psp.Close();

            foreach (ProAccAccountDataContract pac in pacs.Where(a=>a.CurrentSaldo!=a.PreviousSaldo).Take(20))
            {
                Console.WriteLine("{0} {1}: current[{2}]  previous[{3}]", pac.InternalNr, pac.Translations.First().Description, pac.CurrentSaldo, pac.PreviousSaldo);
            }

            Console.ReadLine();
            // net.tcp://defranmceebo01.eurw.ey.net:8080/ProAccService


            /*
            if (serviceHost != null)
            {
                serviceHost.Close();
            }
            serviceHost = new ServiceHost(typeof(ICacheService));
            //serviceHost.ex

            serviceHost.AddServiceEndpoint(typeof(ICacheService), new NetNamedPipeBinding(), "net.pipe://localhost/CacheService");
           
            // serviceHost.AddServiceEndpoint(typeof(IRuleService), new NetNamedPipeBinding(), "net.pipe://localhost/InRuleService");
            serviceHost.Open();

            Console.ReadLine();

            serviceHost.Close();
            */
           
           // Console.ReadLine();

        }

        private static void MigrateBundles()
        {
            int ttl = eBookWriteManager.Context.FileReportLayouts.Where(r => !r.Deleted).Count();
            int cnt = 1;
            foreach (EY.com.eBook.Core.EF.Write.FileReportLayout rl in eBookWriteManager.Context.FileReportLayouts.Where(r => !r.Deleted))
            {
                Console.WriteLine("{0}/{1}", cnt, ttl);
                ReportDataContract rdc = CoreHelper.DeserializeFromString<ReportDataContract>(rl.ReportLayout);

                Bundle bundle = new Bundle
                {
                    Id = rl.Id
                    ,
                    Name = rl.Name
                    ,
                    Contents = CoreHelper.SerializeToString(rdc.Index.Items)
                    ,
                    Culture = rl.Culture
                    ,
                    Deleted = rl.Deleted
                    ,
                    Draft = !rl.Closed
                    ,
                    FileId = rl.FileId
                    ,
                    HeaderFooterType = "ACR"
                    ,
                    RectoVerso = true
                    ,
                    TemplateId = rl.BaseTypeId
                };
                eBookReadManager.Context.AddToBundles(bundle);
                eBookReadManager.Context.SaveChanges();
                cnt++;

            }
        }

        private static Random random = new Random((int)DateTime.Now.Ticks);
        private static string RandomString(int size)
        {
            StringBuilder builder = new StringBuilder();
            char ch;
            for (int i = 0; i < size; i++)
            {
                ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
                builder.Append(ch);
            }

            return builder.ToString();
        }

        private static void CopyFiles()
        {

            string uploadeddocs = @"v:\data\eBook\UploadedDocuments";
            string closedfiles = @"v:\data\eBook\ClosedFiles";
            string temupload  = @"v:\data\eBook\TemporaryUploadDir";

            List<RepositoryItem> items = eBookReadManager.Context.RepositoryItems.ToList();
            foreach (RepositoryItem item in items)
            {
                string path = System.IO.Path.Combine(Config.RepositoryLocal,item.ClientId.ToPath());
                path = System.IO.Path.Combine(path,string.Format("{0}{1}", item.ItemId.ToString(),item.Extension));
                if (!System.IO.File.Exists(path))
                {
                    string spath = System.IO.Path.Combine(uploadeddocs, System.IO.Path.GetFileName(path));
                    if (System.IO.File.Exists(spath))
                    {
                        System.IO.File.Copy(spath, path, true);
                        Console.WriteLine("Copy " + item.ItemId.ToString());
                    }
                    else
                    {
                        spath = System.IO.Path.Combine(closedfiles, System.IO.Path.GetFileName(path));
                        if (System.IO.File.Exists(spath))
                        {
                            System.IO.File.Copy(spath, path, true);
                            Console.WriteLine("Copy " + item.ItemId.ToString());
                        }
                        else
                        {
                            spath = System.IO.Path.Combine(temupload, System.IO.Path.GetFileName(path));
                            if (System.IO.File.Exists(spath))
                            {
                                System.IO.File.Copy(spath, path, true);
                                Console.WriteLine("Copy " + item.ItemId.ToString());
                            }
                            else
                            {
                                Console.WriteLine("NOT FOUND " + item.ItemId.ToString());
                            }
                        }
                    }
                }
            }
        }


    }
}
