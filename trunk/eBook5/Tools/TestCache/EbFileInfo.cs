﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TestCache
{
    public class EbFileInfo
    {
        public Guid Id { get; set; }

        public int AssessmentYear { get; set; }
    }
}
