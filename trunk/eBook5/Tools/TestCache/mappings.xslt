﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl"
                  xmlns:Helper="urn:Helper"
>
    <xsl:output method="text" indent="yes"/>

  <xsl:template match="ArrayOfMappingDataContract">
    
    eBook.Accounts.Mappings.Menus = {
        <xsl:for-each select="MappingDataContract">
          <xsl:if test="not(position()=1)">,</xsl:if><xsl:value-of select="d"/> : <xsl:apply-templates select="."><xsl:with-param name="ass" select="2012"/></xsl:apply-templates>
        </xsl:for-each>
    };

    eBook.Accounts.Mappings.Descs = {
    <xsl:for-each select="//MappingListItemDataContract[@type='MappingItemDataContract']|//MappingItemDataContract">
      <xsl:if test="not(position()=1)">,</xsl:if>'M<xsl:value-of select="Id"/>':'<xsl:value-of select="Helper:escapeQuotes(d/text())"/>'
    </xsl:for-each>
    };

  </xsl:template>

  <xsl:template match="MappingDataContract"><xsl:param name="ass"/>new eBook.Accounts.MappingMenu({
        id:'eb-mapping-<xsl:value-of select="$ass" />-<xsl:value-of select="d"/>'
        ,items:[<xsl:for-each select="its/*"><xsl:if test="not(position()=1)">,</xsl:if><xsl:apply-templates select="."/></xsl:for-each>
        ]
    })
  </xsl:template>

  <xsl:template match="MappingListItemDataContract[@type='MappingMetaDataContract']">
         {
         text:'<xsl:value-of select="Helper:escapeQuotes(d/text())"/>'
          ,meta:'<xsl:value-of select="Helper:escapeQuotes(m/text())"/>'
          ,unselectable:true
          ,hideOnClick:false
          ,menu:{xtype:'mappingmenu', items :[<xsl:for-each select="its/child::*"><xsl:if test="not(position()=1)">,</xsl:if><xsl:apply-templates select="."/></xsl:for-each>
            ] }
          }
  </xsl:template>
    
  <xsl:template match="MappingListItemDataContract[@type='MappingItemDataContract']|MappingItemDataContract">
          {text:'<xsl:value-of select="Helper:escapeQuotes(d/text())"/>'
          ,meta: <xsl:choose><xsl:when test="m/@nil='true'">null</xsl:when><xsl:otherwise>'<xsl:value-of select="m"/>'</xsl:otherwise></xsl:choose>
          ,key:'<xsl:value-of select="Helper:escapeQuotes(k/text())"/>'
          ,mapId:'<xsl:value-of select="Id"/>'
          ,listObj: {
              id:'<xsl:value-of select="Id"/>'
              ,nl:'<xsl:value-of select="Helper:escapeQuotes(lo/nl/text())"/>'
              ,fr:'<xsl:value-of select="Helper:escapeQuotes(lo/fr/text())"/>'
              ,en:'<xsl:value-of select="Helper:escapeQuotes(lo/en/text())"/>'
          }
          ,handler:eBook.Accounts.Mappings.handleClick
          ,scope:this
          }
  </xsl:template>
   
</xsl:stylesheet>
