﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Data.Linq;
using System.Text;
using EY.com.eBook.Core;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.Core.EF;
using EY.com.eBook.API.Contracts.Data.Logic;
using EY.com.eBook.Core.EF.Read;
using System.Data.Objects;
using EY.com.eBook.Core.EntityTranslation;
using EY.com.eBook.API.BL.Translators;
using EY.com.eBook.Core.Data;
using EY.com.eBook.API.Contracts.Proxies;
using EY.com.eBook.API.Contracts.Data.Cache;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.Linq;
using EY.com.eBook.API.Contracts.Services;
//using Newtonsoft.Json;
using System.Net;
using System.IO;
using EY.com.eBook.API.BL.Read;
using Newtonsoft.Json;
namespace DailyCheckDocstore
{
    class Program
    {
        public static string _token = "";

        static void Main(string[] args)
        {
            System.Net.WebClient client = new DocstoreRepository().getWebClient();
            if (string.IsNullOrEmpty(_token))
            {
                _token = new DocstoreRepository().Login();
            }
            authenticationTokenDataContract token = JsonConvert.DeserializeObject<authenticationTokenDataContract>(_token);
            client.Headers.Add("AuthenticationToken", token.AuthenticationToken);
            var docStatusses = "";
            try
            {

                docStatusses =  Encoding.ASCII.GetString(client.UploadData(new DocstoreRepository()._url + "Document/Status", "POST", Encoding.Default.GetBytes(JsonConvert.SerializeObject(GetListRepositoryItems()))));
                //teamMembers = webclient.UploadData(_url + "TeamMember/All", "POST", Encoding.Default.GetBytes(JsonConvert.SerializeObject(ctmbcdc)));
            }
            catch (Exception ex)
            {
                var wex = GetNestedException<WebException>(ex);

                // If there is no nested WebException, re-throw the exception.
                if (wex == null) { throw; }

                // Get the response object.
                var response = wex.Response as HttpWebResponse;

                // If it's not an HTTP response or is not error 403, re-throw.
                if (response == null || response.StatusCode != HttpStatusCode.Forbidden)
                {
                    throw;
                }

                // The error is 403.  Handle it here.
                //GetTeamMembersByClient(cidc);
            }
            Console.WriteLine(docStatusses);
            Console.ReadLine();
            
        }

        public static List<Guid?> GetListRepositoryItems() {
            List<Guid> lg = eBookReadManager.Context.RepositoryStructureSet.Where(y => y.DocstoreAccess == true).Select(y => y.Id).ToList();
            List<Guid?> list = new List<Guid?>();
            List<List<RepositoryItem>> llri = new List<List<RepositoryItem>>();
            foreach (Guid g in lg)
            {
                List<Guid?> lri = eBookReadManager.Context.RepositoryItems.Where(x => x.DocstoreId != null
                                                                                    //&& x.Status != new Guid("FCF7A560-3D74-4341-A32B-310C8FA336CF")
                                                                                    && x.StructureId == g
                                                                                    && x.Deleted == false)
                                                                            .Select(y=>y.DocstoreId).ToList();
                list = list.Concat(lri).ToList();
            }
            return list;
        }

        static T GetNestedException<T>(Exception ex) where T : Exception
        {
            if (ex == null) { return null; }

            var tEx = ex as T;
            if (tEx != null) { return tEx; }

            return GetNestedException<T>(ex.InnerException);
        }

        
    }
}