﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.Core;
using EY.com.eBook.RuleEngine.Core.Interfaces;
using System.Xml.Linq;
using EY.com.eBook.API.Contracts.Data.Cache;
using System.Runtime.Serialization;
///using EY.com.eBook.RuleEngine.Data;

namespace EY.com.eBook.RuleEngine.Core
{
    
    public interface IWorksheetType
    {
        [DataMember]
        DateTime LastUpdated { get; set; }

        void LoadXml(string xml);
        string ToXml();
        // void ImportPrevious<T>(T entity);
        Guid GetMyType();
        //void Calculate(ref WorksheetSetDataBase wsd);
        [DataMember]
        string[] ImportablePreviousTypes { get; set; }

        IWorksheet GetData();
    }

    public interface IWorksheetType<WT> : IWorksheetType
        where WT:IWorksheet
    {
        [DataMember]
        new WT Data { get; set; }        
        
     }

    //public abstract class WorksheetTypeBase<WT> : IWorksheetType<WT> where WT : IWorksheet
    //{
    //    #region WorksheetType Settings
    //    public static WorksheetTypeSettings Settings = new WorksheetTypeSettings();
    //    public static Guid TypeId;
    //    #endregion

    //    protected string[] ImportablePreviousTypes = new string[] { };

    //    #region Data

    //    public DateTime LastUpdated { get; set; }

    //    public Guid GetMyType() { return TypeId; }

    //    public WT Data { get; set; }

    //    #endregion

    //    public virtual void Calculate<T>() where T : WorksheetType<WT>
    //    {
    //        // Data.LastCalculated = DateTime.Now;
    //    }


    //    public void LoadXml<T>(string xml) where T : WorksheetType<WT>
    //    {
    //        if (string.IsNullOrEmpty(xml)) return;
    //        Data = CoreHelper.DeserializeFromString<WT>(XElement.Parse(xml).StripNamespaces().ToString());

    //    }


    //}

    [DataContract]
    public abstract class WorksheetType<WT> : IWorksheetType<WT>, IWorksheetType where WT : IWorksheet 
    {
        #region WorksheetType Settings
        public static WorksheetTypeSettings Settings = new WorksheetTypeSettings();
      //  public static Guid TypeId;
        #endregion

        #region worksheet type validation
        // method to perform validation based upon type
        #endregion

        public WorksheetType()
        {
            ImportablePreviousTypes = new string[] { };
        }

        //public List<BookingDataContract> Bookings { get; set; }
     //   public List<MessageDataContract> Messages { get; set; }

        #region Data

        [DataMember]
        public DateTime LastUpdated { get; set; }

        public virtual Guid GetMyType() { return Guid.Empty; }

        [DataMember]
        public WT Data { get; set; }

        public IWorksheet GetData()
        {
            return Data;
        }
        

        #endregion


        public virtual void Calculate()
        {
           // Data.LastCalculated = DateTime.Now;
        }


        public void LoadXml(string xml)
        {
            if (string.IsNullOrEmpty(xml)) return;
            Data = CoreHelper.DeserializeFromString<WT>(xml);
            
        }



        public string[] ImportablePreviousTypes {get;set; }

        // to review
        public void ImportPrevious<S>(S entity) where  S : IWorksheetType
        {
            if (ImportablePreviousTypes.Contains(typeof(S).FullName))
            {
                ImportPrevious(entity);
            }
        }

        //dummy
        public void ImportPrevious(object entity) {
            Console.WriteLine("Dummy called");
        }



        #region IWorksheetType<WT> Members


        public virtual string ToXml()
        {
            return CoreHelper.SerializeToString(Data);
        }

        

        #endregion

        
    }

}
