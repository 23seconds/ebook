﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.RuleEngine.Data
{
    [DataContract]
    public class CriteriaFileAndRowDataContract
    {
        [DataMember(Order=1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public Guid RowId { get; set; }
    }
}
