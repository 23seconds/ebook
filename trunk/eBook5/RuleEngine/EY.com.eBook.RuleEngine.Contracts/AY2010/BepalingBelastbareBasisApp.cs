﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.RuleEngine.Core;
using EY.com.eBook.RuleEngine.Core.Interfaces;
using EY.com.eBook.RuleEngine.Data;

    
namespace EY.com.eBook.RuleEngine.AY2010.BepalingBelastbareBasisAppTypes
{
    #region subtypes


      [DataContract(Namespace = "AY2010.BepalingBelastbareBasisApp")]
      public class General : IEntity
      {
          #region IEntity
          
          [DataMember(Name = "ID", Order=1)]
          public Guid Id { get; set; }
          
          [DataMember(Order=2)]
          public DateTime? LastUpdated { get; set; }
          
          [DataMember(Order = 3)]
          public bool IsValid { get; set; }
          
          [DataMember(Order = 4)]
          public bool PreviousImported { get; set; }
          
          #endregion
          
          #region entitymembers
          

        
          [DataMember(Order = 5)]
          public decimal? GereserveerdeWinst {get;set;}


        
          [DataMember(Order = 6)]
          public string Spacer25 {get;set;}


        
          [DataMember(Order = 7)]
          public decimal? Dividenden {get;set;}


        
          [DataMember(Order = 8)]
          public string Spacer26 {get;set;}


        
          [DataMember(Order = 9)]
          public decimal? VerkrijgingEigenAandelen {get;set;}


        
          [DataMember(Order = 10)]
          public string Spacer27 {get;set;}


        
          [DataMember(Order = 11)]
          public decimal? OverlijdenUittredingUitsluitingVennoot {get;set;}


        
          [DataMember(Order = 12)]
          public string Spacer1 {get;set;}


        
          [DataMember(Order = 13)]
          public decimal? VerdelingMaatschappelijkVermogen {get;set;}


        
          [DataMember(Order = 14)]
          public string Spacer2 {get;set;}


        
          [DataMember(Order = 15)]
          public decimal? StatAccountingResult {get;set;}


        
          [DataMember(Order = 16)]
          public decimal? TotalBelasteReserves {get;set;}


        
          [DataMember(Order = 17)]
          public decimal? TotalAanpassingenBelasteReserves {get;set;}


        
          [DataMember(Order = 18)]
          public decimal? NietAftrekbareBelastingen {get;set;}


        
          [DataMember(Order = 19)]
          public decimal? TotaalVerworpenUitgaven {get;set;}


        
          [DataMember(Order = 20)]
          public decimal? BelastbaarResultaat {get;set;}


        
          [DataMember(Order = 21)]
          public decimal? TotaalBelastbareBestandenZonderAftrekMin {get;set;}


        
          [DataMember(Order = 22)]
          public decimal? SubTotaalBelastbaarVoorAftrek {get;set;}


        
          [DataMember(Order = 23)]
          public decimal? OpdelingNaarOorsprongBijVerdragVrijgesteldInkomen {get;set;}


        
          [DataMember(Order = 24)]
          public decimal? BasisVoorFiscaleAftrek {get;set;}


          #endregion
      }

  
      [DataContract(Namespace = "AY2010.BepalingBelastbareBasisApp")]
      public class BelasteReservesItem : IEntity
      {
          #region IEntity
          
          [DataMember(Name = "ID", Order=1)]
          public Guid Id { get; set; }
          
          [DataMember(Order=2)]
          public DateTime? LastUpdated { get; set; }
          
          [DataMember(Order = 3)]
          public bool IsValid { get; set; }
          
          [DataMember(Order = 4)]
          public bool PreviousImported { get; set; }
          
          #endregion
          
          #region entitymembers
          

        
          [DataMember(Order = 5)]
          public string TempDiff {get;set;}


        
          [DataMember(Order = 6)]
          public decimal? TDKapitaalEnUitgifte {get;set;}


        
          [DataMember(Order = 7)]
          public decimal? TDHerwaardering {get;set;}


        
          [DataMember(Order = 8)]
          public decimal? TDTaxableProvisions {get;set;}


        
          [DataMember(Order = 9)]
          public decimal? TDOtherTaxableReserves {get;set;}


        
          [DataMember(Order = 10)]
          public decimal? TDTaxableDoubtfullDebtors {get;set;}


        
          [DataMember(Order = 11)]
          public decimal? TDExcessDeprications {get;set;}


        
          [DataMember(Order = 12)]
          public decimal? TDUnderestimationAssetsOverestimationLiabilities {get;set;}


        
          [DataMember(Order = 13)]
          public decimal? TDTotal {get;set;}


        
          [DataMember(Order = 14)]
          public string OhterAdjustments {get;set;}


        
          [DataMember(Order = 15)]
          public decimal? OACapitalGainsOnShares {get;set;}


        
          [DataMember(Order = 16)]
          public decimal? OADefinitiveExemptionTaxShelter {get;set;}


        
          [DataMember(Order = 17)]
          public decimal? OAExemptionRegionalPrem {get;set;}


        
          [DataMember(Order = 18)]
          public decimal? OAOthers {get;set;}


        
          [DataMember(Order = 19)]
          public decimal? OATotal {get;set;}


        
          [DataMember(Order = 20)]
          public decimal? TDOthers {get;set;}


          #endregion
      }

  
      [DataContract(Namespace = "AY2010.BepalingBelastbareBasisApp")]
      public class VerworpenUitgavenItem : IEntity
      {
          #region IEntity
          
          [DataMember(Name = "ID", Order=1)]
          public Guid Id { get; set; }
          
          [DataMember(Order=2)]
          public DateTime? LastUpdated { get; set; }
          
          [DataMember(Order = 3)]
          public bool IsValid { get; set; }
          
          [DataMember(Order = 4)]
          public bool PreviousImported { get; set; }
          
          #endregion
          
          #region entitymembers
          

        
          [DataMember(Order = 5)]
          public string PermanentDifferences {get;set;}


        
          [DataMember(Order = 6)]
          public string DisallowedExpenses {get;set;}


        
          [DataMember(Order = 7)]
          public decimal? PDDisallowedCurrentIncomeTaxExpenses {get;set;}


        
          [DataMember(Order = 8)]
          public decimal? DERegionalTaxes {get;set;}


        
          [DataMember(Order = 9)]
          public decimal? DEPenalties {get;set;}


        
          [DataMember(Order = 10)]
          public decimal? DEPensions {get;set;}


        
          [DataMember(Order = 11)]
          public decimal? DECar {get;set;}


        
          [DataMember(Order = 12)]
          public decimal? DEReception {get;set;}


        
          [DataMember(Order = 13)]
          public decimal? DERestaurant {get;set;}


        
          [DataMember(Order = 14)]
          public decimal? DEClothes {get;set;}


        
          [DataMember(Order = 15)]
          public decimal? DEExcessInterest {get;set;}


        
          [DataMember(Order = 16)]
          public decimal? DEInterestLoans {get;set;}


        
          [DataMember(Order = 17)]
          public decimal? DEAbnormalAdvantages {get;set;}


        
          [DataMember(Order = 18)]
          public decimal? DESocial {get;set;}


        
          [DataMember(Order = 19)]
          public decimal? DEDinerSportCultureEco {get;set;}


        
          [DataMember(Order = 20)]
          public decimal? DECharity {get;set;}


        
          [DataMember(Order = 21)]
          public decimal? DEReductionValueShares {get;set;}


        
          [DataMember(Order = 22)]
          public decimal? DEReversalsPreviousTax {get;set;}


        
          [DataMember(Order = 23)]
          public decimal? DEEmployeeCapital {get;set;}


        
          [DataMember(Order = 24)]
          public decimal? DEIndemnityMissingCoupon {get;set;}


        
          [DataMember(Order = 25)]
          public decimal? DEExpensesTaxShelter {get;set;}


        
          [DataMember(Order = 26)]
          public decimal? DERegionalPremiumsCapital {get;set;}


        
          [DataMember(Order = 27)]
          public decimal? DEBepaaldeStaten {get;set;}


        
          [DataMember(Order = 28)]
          public decimal? DEOther {get;set;}


        
          [DataMember(Order = 29)]
          public decimal? DETotal {get;set;}


          #endregion
      }

  
      [DataContract(Namespace = "AY2010.BepalingBelastbareBasisApp")]
      public class BelastbareBestZonderAftrekItem : IEntity
      {
          #region IEntity
          
          [DataMember(Name = "ID", Order=1)]
          public Guid Id { get; set; }
          
          [DataMember(Order=2)]
          public DateTime? LastUpdated { get; set; }
          
          [DataMember(Order = 3)]
          public bool IsValid { get; set; }
          
          [DataMember(Order = 4)]
          public bool PreviousImported { get; set; }
          
          #endregion
          
          #region entitymembers
          

        
          [DataMember(Order = 5)]
          public string TaxableElementsWithoutDeductions {get;set;}


        
          [DataMember(Order = 6)]
          public decimal? TEReceivedAbnormal {get;set;}


        
          [DataMember(Order = 7)]
          public decimal? TETaxableInvestmentReserve {get;set;}


        
          [DataMember(Order = 8)]
          public decimal? TEEmployeeParticipationCapital {get;set;}


        
          [DataMember(Order = 9)]
          public decimal? TETaxationEexemptedReservesCapitalGains {get;set;}


        
          [DataMember(Order = 10)]
          public decimal? TETotal {get;set;}


          #endregion
      }

  
      [DataContract(Namespace = "AY2010.BepalingBelastbareBasisApp")]
      public class OpdelingNaarOorsprongItem : IEntity
      {
          #region IEntity
          
          [DataMember(Name = "ID", Order=1)]
          public Guid Id { get; set; }
          
          [DataMember(Order=2)]
          public DateTime? LastUpdated { get; set; }
          
          [DataMember(Order = 3)]
          public bool IsValid { get; set; }
          
          [DataMember(Order = 4)]
          public bool PreviousImported { get; set; }
          
          #endregion
          
          #region entitymembers
          

        
          [DataMember(Order = 5)]
          public string ItemizationResult {get;set;}


        
          [DataMember(Order = 6)]
          public decimal? IRBelgianTaxable {get;set;}


        
          [DataMember(Order = 7)]
          public decimal? IRNonTreatyForeignIncome {get;set;}


        
          [DataMember(Order = 8)]
          public decimal? IRTreatyForeignIncome {get;set;}


        
          [DataMember(Order = 9)]
          public string IRRemainingResult {get;set;}


        
          [DataMember(Order = 10)]
          public decimal? IRRemainingResultBelgium {get;set;}


        
          [DataMember(Order = 11)]
          public string IRRemainingResultSpacer {get;set;}


        
          [DataMember(Order = 12)]
          public decimal? IRRemainingResultNonTreaty {get;set;}


          #endregion
      }

  
      [DataContract(Namespace = "AY2010.BepalingBelastbareBasisApp")]
      public class FiscaleAftrekItem : IEntity
      {
          #region IEntity
          
          [DataMember(Name = "ID", Order=1)]
          public Guid Id { get; set; }
          
          [DataMember(Order=2)]
          public DateTime? LastUpdated { get; set; }
          
          [DataMember(Order = 3)]
          public bool IsValid { get; set; }
          
          [DataMember(Order = 4)]
          public bool PreviousImported { get; set; }
          
          #endregion
          
          #region entitymembers
          

        
          [DataMember(Order = 5)]
          public string NonTaxableElements {get;set;}


        
          [DataMember(Order = 6)]
          public decimal? NTEBECharity {get;set;}


        
          [DataMember(Order = 7)]
          public decimal? NTEBEPersonnel {get;set;}


        
          [DataMember(Order = 8)]
          public decimal? NTEBEPersonnelSME {get;set;}


        
          [DataMember(Order = 9)]
          public decimal? NTEBEInternalshipPremium {get;set;}


        
          [DataMember(Order = 10)]
          public decimal? NTEBEOthers {get;set;}


        
          [DataMember(Order = 11)]
          public decimal? TotalNonTaxableElements {get;set;}


        
          [DataMember(Order = 12)]
          public decimal? ResterendBasisVoorAftrek {get;set;}


          #endregion
      }

  

    #endregion

    #region roottype
    

      [DataContract(Namespace = "AY2010.BepalingBelastbareBasisApp")]
      public class BepalingBasis : IWorksheet
      {
          public BepalingBasis() {
              // Set Minimal

        
              this.Overzicht = new EY.com.eBook.RuleEngine.AY2010.BepalingBelastbareBasisAppTypes.General();
              this.Overzicht.Id = Guid.NewGuid();
        
              this.BelasteReserves = new EY.com.eBook.RuleEngine.AY2010.BepalingBelastbareBasisAppTypes.BelasteReservesItem();
              this.BelasteReserves.Id = Guid.NewGuid();
        
              this.VerworpenUitgaven = new EY.com.eBook.RuleEngine.AY2010.BepalingBelastbareBasisAppTypes.VerworpenUitgavenItem();
              this.VerworpenUitgaven.Id = Guid.NewGuid();
        
              this.BelastbareBestZonderAftrek = new EY.com.eBook.RuleEngine.AY2010.BepalingBelastbareBasisAppTypes.BelastbareBestZonderAftrekItem();
              this.BelastbareBestZonderAftrek.Id = Guid.NewGuid();
        
              this.OpdelingNaarOorsprong = new EY.com.eBook.RuleEngine.AY2010.BepalingBelastbareBasisAppTypes.OpdelingNaarOorsprongItem();
              this.OpdelingNaarOorsprong.Id = Guid.NewGuid();
        
              this.FiscaleAftrek = new EY.com.eBook.RuleEngine.AY2010.BepalingBelastbareBasisAppTypes.FiscaleAftrekItem();
              this.FiscaleAftrek.Id = Guid.NewGuid();
              this.LastUpdated = DateTime.Now;
          }
        
          
          #region entitymembers
          

        
          [DataMember(Order = 1)]
          public EY.com.eBook.RuleEngine.AY2010.BepalingBelastbareBasisAppTypes.General Overzicht {get;set;}


        
          [DataMember(Order = 2)]
          public EY.com.eBook.RuleEngine.AY2010.BepalingBelastbareBasisAppTypes.BelasteReservesItem BelasteReserves {get;set;}


        
          [DataMember(Order = 3)]
          public EY.com.eBook.RuleEngine.AY2010.BepalingBelastbareBasisAppTypes.VerworpenUitgavenItem VerworpenUitgaven {get;set;}


        
          [DataMember(Order = 4)]
          public EY.com.eBook.RuleEngine.AY2010.BepalingBelastbareBasisAppTypes.BelastbareBestZonderAftrekItem BelastbareBestZonderAftrek {get;set;}


        
          [DataMember(Order = 5)]
          public EY.com.eBook.RuleEngine.AY2010.BepalingBelastbareBasisAppTypes.OpdelingNaarOorsprongItem OpdelingNaarOorsprong {get;set;}


        
          [DataMember(Order = 6)]
          public EY.com.eBook.RuleEngine.AY2010.BepalingBelastbareBasisAppTypes.FiscaleAftrekItem FiscaleAftrek {get;set;}


      
          #endregion
          
          #region IWorksheet Members
          [DataMember(Order = 7)]
          public DateTime? LastUpdated { get; set; }

          #endregion
      }

  
   
    #endregion

}
  