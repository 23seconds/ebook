﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.RuleEngine.Core;
using EY.com.eBook.RuleEngine.Core.Interfaces;
using EY.com.eBook.RuleEngine.Data;

    
namespace EY.com.eBook.RuleEngine.AY2010.RisicoKapitaalAppTypes
{
    #region subtypes


      [DataContract(Namespace = "AY2010.RisicoKapitaalApp")]
      public class FicheItem : IEntity
      {
          #region IEntity
          
          [DataMember(Name = "ID", Order=1)]
          public Guid Id { get; set; }
          
          [DataMember(Order=2)]
          public DateTime? LastUpdated { get; set; }
          
          [DataMember(Order = 3)]
          public bool IsValid { get; set; }
          
          [DataMember(Order = 4)]
          public bool PreviousImported { get; set; }
          
          #endregion
          
          #region entitymembers
          

        
          [DataMember(Order = 5)]
          public decimal? SaldoBelgischResterend {get;set;}


        
          [DataMember(Order = 6)]
          public decimal? SaldoNietVerdragVrijgRestWinst {get;set;}


        
          [DataMember(Order = 7)]
          public decimal? EigenVermogen {get;set;}


        
          [DataMember(Order = 8)]
          public decimal? EigenAandelen {get;set;}


        
          [DataMember(Order = 9)]
          public decimal? FinancVasteActiva {get;set;}


        
          [DataMember(Order = 10)]
          public decimal? AandelenBelegging {get;set;}


        
          [DataMember(Order = 11)]
          public decimal? InrichtingVerdrag {get;set;}


        
          [DataMember(Order = 12)]
          public decimal? OnroerendVerdrag {get;set;}


        
          [DataMember(Order = 13)]
          public decimal? MvaOnredelijk {get;set;}


        
          [DataMember(Order = 14)]
          public decimal? BestanddelenBelegging {get;set;}


        
          [DataMember(Order = 15)]
          public decimal? OnroerendeBedrijfsleiders {get;set;}


        
          [DataMember(Order = 16)]
          public decimal? UitgedrukteMeerwaarden {get;set;}


        
          [DataMember(Order = 17)]
          public decimal? Belastingkrediet {get;set;}


        
          [DataMember(Order = 18)]
          public decimal? KapitaalSubsidies {get;set;}


        
          [DataMember(Order = 19)]
          public decimal? Voorraadactualisering {get;set;}


        
          [DataMember(Order = 20)]
          public decimal? Hoofdzetel {get;set;}


        
          [DataMember(Order = 21)]
          public decimal? BelgischeInrichting {get;set;}


        
          [DataMember(Order = 22)]
          public decimal? Totaal {get;set;}


        
          [DataMember(Order = 23)]
          public decimal? PositieveWijzigingen {get;set;}


        
          [DataMember(Order = 24)]
          public decimal? NegatieveWijzigingen {get;set;}


        
          [DataMember(Order = 25)]
          public decimal? RisicoKapitaalBelastbaar {get;set;}


        
          [DataMember(Order = 26)]
          public decimal? RisicoKapitaalBelastbaarOverrule {get;set;}


        
          [DataMember(Order = 27)]
          public bool? TariefKleineVennootschap {get;set;}


        
          [DataMember(Order = 28)]
          public decimal? NotioneelPercentage {get;set;}


        
          [DataMember(Order = 29)]
          public decimal? Aftrekbaar {get;set;}


        
          [DataMember(Order = 30)]
          public decimal? AftrekbaarOverrule {get;set;}


        
          [DataMember(Order = 31)]
          public decimal? WerkelijkAfgetrokken {get;set;}


        
          [DataMember(Order = 32)]
          public decimal? OverdraagbaarSaldo {get;set;}


        
          [DataMember(Order = 33)]
          public decimal? OverdraagbaarSaldoOverrule {get;set;}


        
          [DataMember(Order = 34)]
          public decimal? SaldoVorigeAJGevormd {get;set;}


        
          [DataMember(Order = 35)]
          public decimal? WerkelijkVorigAJ {get;set;}


        
          [DataMember(Order = 36)]
          public decimal? TotaalAftrekAJ {get;set;}


        
          [DataMember(Order = 37)]
          public decimal? TotaalAftrekAJOverrule {get;set;}


        
          [DataMember(Order = 38)]
          public decimal? SaldoRisicoOverdraagbaarForm {get;set;}


        
          [DataMember(Order = 39)]
          public decimal? SaldoRisicoOverdraagbaarOverrule {get;set;}


        
          [DataMember(Order = 40)]
          public decimal? SaldoRisicoOverdraagbaar {get;set;}


        
          [DataMember(Order = 41)]
          public decimal? BelastbareBasisResterend {get;set;}


        
          [DataMember(Order = 42)]
          public decimal? Aanwending {get;set;}


          #endregion
      }

  
      [DataContract(Namespace = "AY2010.RisicoKapitaalApp")]
      public class EigenVermogen : IEntity
      {
          #region IEntity
          
          [DataMember(Name = "ID", Order=1)]
          public Guid Id { get; set; }
          
          [DataMember(Order=2)]
          public DateTime? LastUpdated { get; set; }
          
          [DataMember(Order = 3)]
          public bool IsValid { get; set; }
          
          [DataMember(Order = 4)]
          public bool PreviousImported { get; set; }
          
          #endregion
          
          #region entitymembers
          

        
          [DataMember(Order = 5)]
          public StringListItemDataContract Grootboek {get;set;}


        
          [DataMember(Order = 6)]
          public string Omschrijving {get;set;}


        
          [DataMember(Order = 7)]
          public decimal? VorigBjImported {get;set;}


        
          [DataMember(Order = 8)]
          public decimal? HuidigBjImported {get;set;}


        
          [DataMember(Order = 9)]
          public decimal? VorigBj {get;set;}


        
          [DataMember(Order = 10)]
          public decimal? HuidigBj {get;set;}


        
          [DataMember(Order = 11)]
          public decimal? Evolutie {get;set;}


        
          [DataMember(Order = 12)]
          public string Type {get;set;}


        
          [DataMember(Order = 13)]
          public bool? Auto {get;set;}


          #endregion
      }

  
      [DataContract(Namespace = "AY2010.RisicoKapitaalApp")]
      public class Bestanddeel : IEntity
      {
          #region IEntity
          
          [DataMember(Name = "ID", Order=1)]
          public Guid Id { get; set; }
          
          [DataMember(Order=2)]
          public DateTime? LastUpdated { get; set; }
          
          [DataMember(Order = 3)]
          public bool IsValid { get; set; }
          
          [DataMember(Order = 4)]
          public bool PreviousImported { get; set; }
          
          #endregion
          
          #region entitymembers
          

        
          [DataMember(Order = 5)]
          public StringListItemDataContract Grootboek {get;set;}


        
          [DataMember(Order = 6)]
          public string Omschrijving {get;set;}


        
          [DataMember(Order = 7)]
          public decimal? VorigBjImported {get;set;}


        
          [DataMember(Order = 8)]
          public decimal? HuidigBjImported {get;set;}


        
          [DataMember(Order = 9)]
          public decimal? VorigBj {get;set;}


        
          [DataMember(Order = 10)]
          public decimal? HuidigBj {get;set;}


        
          [DataMember(Order = 11)]
          public decimal? Evolutie {get;set;}


        
          [DataMember(Order = 12)]
          public string Type {get;set;}


        
          [DataMember(Order = 13)]
          public bool? Auto {get;set;}


          #endregion
      }

  
      [DataContract(Namespace = "AY2010.RisicoKapitaalApp")]
      public class WijzigingItem : IEntity
      {
          #region IEntity
          
          [DataMember(Name = "ID", Order=1)]
          public Guid Id { get; set; }
          
          [DataMember(Order=2)]
          public DateTime? LastUpdated { get; set; }
          
          [DataMember(Order = 3)]
          public bool IsValid { get; set; }
          
          [DataMember(Order = 4)]
          public bool PreviousImported { get; set; }
          
          #endregion
          
          #region entitymembers
          

        
          [DataMember(Order = 5)]
          public bool? Bestaand {get;set;}


        
          [DataMember(Order = 6)]
          public StringListItemDataContract Grootboek {get;set;}


        
          [DataMember(Order = 7)]
          public StringListItemDataContract BestaandTeWijzigen {get;set;}


        
          [DataMember(Order = 8)]
          public string Gewijzigde {get;set;}


        
          [DataMember(Order = 9)]
          public int? Wijzigtype {get;set;}


        
          [DataMember(Order = 10)]
          public decimal? VorigBj {get;set;}


        
          [DataMember(Order = 11)]
          public string AfOfToename {get;set;}


        
          [DataMember(Order = 12)]
          public decimal? Wijziging {get;set;}


        
          [DataMember(Order = 13)]
          public string AardToename {get;set;}


        
          [DataMember(Order = 14)]
          public DateTime? DatumTransactie {get;set;}


        
          [DataMember(Order = 15)]
          public decimal? TeWijzigen {get;set;}


        
          [DataMember(Order = 16)]
          public string TypeNID {get;set;}


        
          [DataMember(Order = 17)]
          public string TypeAftrekNID {get;set;}


        
          [DataMember(Order = 18)]
          public string Notitie {get;set;}


          #endregion
      }

  
      [DataContract(Namespace = "AY2010.RisicoKapitaalApp")]
      public class TariefItem : IEntity
      {
          #region IEntity
          
          [DataMember(Name = "ID", Order=1)]
          public Guid Id { get; set; }
          
          [DataMember(Order=2)]
          public DateTime? LastUpdated { get; set; }
          
          [DataMember(Order = 3)]
          public bool IsValid { get; set; }
          
          [DataMember(Order = 4)]
          public bool PreviousImported { get; set; }
          
          #endregion
          
          #region entitymembers
          

        
          [DataMember(Order = 5)]
          public string mainTitle {get;set;}


        
          [DataMember(Order = 6)]
          public bool? laatstebeltijdperk {get;set;}


        
          [DataMember(Order = 7)]
          public bool? voorlaatstebeltijdperk {get;set;}


        
          [DataMember(Order = 8, Name="default")]
          public bool? Default {get;set;}


        
          [DataMember(Order = 9)]
          public DateTime? startDatum {get;set;}


        
          [DataMember(Order = 10)]
          public DateTime? eindDatum {get;set;}


        
          [DataMember(Order = 11)]
          public decimal? tarief {get;set;}


          #endregion
      }

  
      [DataContract(Namespace = "AY2010.RisicoKapitaalApp")]
      public class HistoryItem : IEntity
      {
          #region IEntity
          
          [DataMember(Name = "ID", Order=1)]
          public Guid Id { get; set; }
          
          [DataMember(Order=2)]
          public DateTime? LastUpdated { get; set; }
          
          [DataMember(Order = 3)]
          public bool IsValid { get; set; }
          
          [DataMember(Order = 4)]
          public bool PreviousImported { get; set; }
          
          #endregion
          
          #region entitymembers
          

        
          [DataMember(Order = 5)]
          public StringListItemDataContract AssessmentYear {get;set;}


        
          [DataMember(Order = 6)]
          public decimal? BedragAftrekImport {get;set;}


        
          [DataMember(Order = 7)]
          public decimal? BedragAftrek {get;set;}


        
          [DataMember(Order = 8)]
          public decimal? GebruiktHuidigBJ {get;set;}


        
          [DataMember(Order = 9)]
          public decimal? Saldo {get;set;}


          #endregion
      }

  
      [DataContract(Namespace = "AY2010.RisicoKapitaalApp")]
      public class HistoriekAanpassingItem : IEntity
      {
          #region IEntity
          
          [DataMember(Name = "ID", Order=1)]
          public Guid Id { get; set; }
          
          [DataMember(Order=2)]
          public DateTime? LastUpdated { get; set; }
          
          [DataMember(Order = 3)]
          public bool IsValid { get; set; }
          
          [DataMember(Order = 4)]
          public bool PreviousImported { get; set; }
          
          #endregion
          
          #region entitymembers
          

        
          [DataMember(Order = 5)]
          public StringListItemDataContract AssessmentYear {get;set;}


        
          [DataMember(Order = 6)]
          public decimal? BedragAftrek {get;set;}


        
          [DataMember(Order = 7)]
          public string Notitie {get;set;}


          #endregion
      }

  

    #endregion

    #region roottype
    

      [DataContract(Namespace = "AY2010.RisicoKapitaalApp")]
      public class WPRisicokapitaal : IWorksheet
      {
          public WPRisicokapitaal() {
              // Set Minimal

        
              this.Fiche = new EY.com.eBook.RuleEngine.AY2010.RisicoKapitaalAppTypes.FicheItem();
              this.Fiche.Id = Guid.NewGuid();
        
              this.EigenVermogens = new List<EY.com.eBook.RuleEngine.AY2010.RisicoKapitaalAppTypes.EigenVermogen>();
              
        
              this.Bestanddelen = new List<EY.com.eBook.RuleEngine.AY2010.RisicoKapitaalAppTypes.Bestanddeel>();
              
        
              this.Wijzigingen = new List<EY.com.eBook.RuleEngine.AY2010.RisicoKapitaalAppTypes.WijzigingItem>();
              
        
              this.Tarief = new EY.com.eBook.RuleEngine.AY2010.RisicoKapitaalAppTypes.TariefItem();
              this.Tarief.Id = Guid.NewGuid();
        
              this.Historiek = new List<EY.com.eBook.RuleEngine.AY2010.RisicoKapitaalAppTypes.HistoryItem>();
              
        
              this.AanpassingenHistoriek = new List<EY.com.eBook.RuleEngine.AY2010.RisicoKapitaalAppTypes.HistoriekAanpassingItem>();
              
              this.LastUpdated = DateTime.Now;
          }
        
          
          #region entitymembers
          

        
          [DataMember(Order = 1)]
          public EY.com.eBook.RuleEngine.AY2010.RisicoKapitaalAppTypes.FicheItem Fiche {get;set;}


        
          [DataMember(Order = 2)]
          public List<EY.com.eBook.RuleEngine.AY2010.RisicoKapitaalAppTypes.EigenVermogen> EigenVermogens {get;set;}


        
          [DataMember(Order = 3)]
          public List<EY.com.eBook.RuleEngine.AY2010.RisicoKapitaalAppTypes.Bestanddeel> Bestanddelen {get;set;}


        
          [DataMember(Order = 4)]
          public List<EY.com.eBook.RuleEngine.AY2010.RisicoKapitaalAppTypes.WijzigingItem> Wijzigingen {get;set;}


        
          [DataMember(Order = 5)]
          public EY.com.eBook.RuleEngine.AY2010.RisicoKapitaalAppTypes.TariefItem Tarief {get;set;}


        
          [DataMember(Order = 6)]
          public List<EY.com.eBook.RuleEngine.AY2010.RisicoKapitaalAppTypes.HistoryItem> Historiek {get;set;}


        
          [DataMember(Order = 7)]
          public List<EY.com.eBook.RuleEngine.AY2010.RisicoKapitaalAppTypes.HistoriekAanpassingItem> AanpassingenHistoriek {get;set;}


      
          #endregion
          
          #region IWorksheet Members
          [DataMember(Order = 8)]
          public DateTime? LastUpdated { get; set; }

          #endregion
      }

  
   
    #endregion

}
  