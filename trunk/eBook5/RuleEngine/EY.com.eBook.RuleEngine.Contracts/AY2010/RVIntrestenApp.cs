﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.RuleEngine.Core;
using EY.com.eBook.RuleEngine.Core.Interfaces;
using EY.com.eBook.RuleEngine.Data;

    
namespace EY.com.eBook.RuleEngine.AY2010.RVIntrestenAppTypes
{
    #region subtypes


      [DataContract(Namespace = "AY2010.RVIntrestenApp")]
      public class IntrestItem : IEntity
      {
          #region IEntity
          
          [DataMember(Name = "ID", Order=1)]
          public Guid Id { get; set; }
          
          [DataMember(Order=2)]
          public DateTime? LastUpdated { get; set; }
          
          [DataMember(Order = 3)]
          public bool IsValid { get; set; }
          
          [DataMember(Order = 4)]
          public bool PreviousImported { get; set; }
          
          #endregion
          
          #region entitymembers
          

        
          [DataMember(Order = 5)]
          public string Rekening {get;set;}


        
          [DataMember(Order = 6)]
          public string RekeningOms {get;set;}


        
          [DataMember(Order = 7)]
          public decimal? SaldoAccount {get;set;}


        
          [DataMember(Order = 8)]
          public decimal? Saldo {get;set;}


        
          [DataMember(Order = 9)]
          public decimal? ResterendSaldo {get;set;}


        
          [DataMember(Order = 10)]
          public decimal? Saldo0Percent {get;set;}


        
          [DataMember(Order = 11)]
          public decimal? Percent0 {get;set;}


        
          [DataMember(Order = 12)]
          public decimal? Saldo15Percent {get;set;}


        
          [DataMember(Order = 13)]
          public decimal? Percent15 {get;set;}


        
          [DataMember(Order = 14)]
          public decimal? Saldo25Percent {get;set;}


        
          [DataMember(Order = 15)]
          public decimal? Percent25 {get;set;}


        
          [DataMember(Order = 16)]
          public bool? ToDelete {get;set;}


          #endregion
      }

  
      [DataContract(Namespace = "AY2010.RVIntrestenApp")]
      public class Adjustment : IEntity
      {
          #region IEntity
          
          [DataMember(Name = "ID", Order=1)]
          public Guid Id { get; set; }
          
          [DataMember(Order=2)]
          public DateTime? LastUpdated { get; set; }
          
          [DataMember(Order = 3)]
          public bool IsValid { get; set; }
          
          [DataMember(Order = 4)]
          public bool PreviousImported { get; set; }
          
          #endregion
          
          #region entitymembers
          

        
          [DataMember(Order = 5)]
          public StringListItemDataContract Rekening {get;set;}


        
          [DataMember(Order = 6)]
          public decimal? HerzienSaldo {get;set;}


        
          [DataMember(Order = 7)]
          public string Opmerking {get;set;}


          #endregion
      }

  
      [DataContract(Namespace = "AY2010.RVIntrestenApp")]
      public class VerchilItem : IEntity
      {
          #region IEntity
          
          [DataMember(Name = "ID", Order=1)]
          public Guid Id { get; set; }
          
          [DataMember(Order=2)]
          public DateTime? LastUpdated { get; set; }
          
          [DataMember(Order = 3)]
          public bool IsValid { get; set; }
          
          [DataMember(Order = 4)]
          public bool PreviousImported { get; set; }
          
          #endregion
          
          #region entitymembers
          

        
          [DataMember(Order = 5)]
          public string Omschrijving {get;set;}


        
          [DataMember(Order = 6)]
          public decimal? Bedrag {get;set;}


        
          [DataMember(Order = 7)]
          public string Type {get;set;}


        
          [DataMember(Order = 8)]
          public bool? AutoLine {get;set;}


        
          [DataMember(Order = 9)]
          public int? Order {get;set;}


          #endregion
      }

  

    #endregion

    #region roottype
    

      [DataContract(Namespace = "AY2010.RVIntrestenApp")]
      public class WPRVIntresten : IWorksheet
      {
          public WPRVIntresten() {
              // Set Minimal

        
              this.Intresten = new List<EY.com.eBook.RuleEngine.AY2010.RVIntrestenAppTypes.IntrestItem>();
              
        
              this.Adjustments = new List<EY.com.eBook.RuleEngine.AY2010.RVIntrestenAppTypes.Adjustment>();
              
        
              this.Verschillen = new List<EY.com.eBook.RuleEngine.AY2010.RVIntrestenAppTypes.VerchilItem>();
              
              this.LastUpdated = DateTime.Now;
          }
        
          
          #region entitymembers
          

        
          [DataMember(Order = 1)]
          public List<EY.com.eBook.RuleEngine.AY2010.RVIntrestenAppTypes.IntrestItem> Intresten {get;set;}


        
          [DataMember(Order = 2)]
          public List<EY.com.eBook.RuleEngine.AY2010.RVIntrestenAppTypes.Adjustment> Adjustments {get;set;}


        
          [DataMember(Order = 3)]
          public List<EY.com.eBook.RuleEngine.AY2010.RVIntrestenAppTypes.VerchilItem> Verschillen {get;set;}


      
          #endregion
          
          #region IWorksheet Members
          [DataMember(Order = 4)]
          public DateTime? LastUpdated { get; set; }

          #endregion
      }

  
   
    #endregion

}
  