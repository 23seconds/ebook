﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using EY.com.eBook.API.Contracts;
using EY.com.eBook.API.Contracts.Data.Cache;
using EY.com.eBook.RuleEngine.Core.Interfaces;
using EY.com.eBook.RuleEngine.Core;
using ProtoBuf;

namespace EY.com.eBook.Caching
{
    [DataContract]
    [ProtoContract]
    public class WorksheetWrapper
    {
        [ProtoMember(1)]
        public IWorksheet WorksheetData { get; set; }
    }

    [CollectionDataContract]
    public class WorksheetLibrary : Dictionary<Guid, WorksheetWrapper>
    {
        [DataMember(Order = 1)]
        public DateTime LastSave { get; set; }
    }
}
