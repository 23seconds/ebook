﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.RuleEngine.Core;
using EY.com.eBook.RuleEngine.Core.Interfaces;
using EY.com.eBook.RuleEngine.Data;

    
namespace EY.com.eBook.RuleEngine.AY2008.DBIAppTypes
{
    #region subtypes


      [DataContract(Namespace = "AY2008.DBIApp")]
      public class ParticipatieItem : IEntity
      {
          #region IEntity
          
          [DataMember(Name = "ID", Order=1)]
          public Guid Id { get; set; }
          
          [DataMember(Order=2)]
          public DateTime? LastUpdated { get; set; }
          
          [DataMember(Order = 3)]
          public bool IsValid { get; set; }
          
          [DataMember(Order = 4)]
          public bool PreviousImported { get; set; }
          
          #endregion
          
          #region entitymembers
          

        
          [DataMember(Order = 5)]
          public string Name {get;set;}


        
          [DataMember(Order = 6)]
          public StringListItemDataContract AccountParticipation {get;set;}


        
          [DataMember(Order = 7)]
          public string Country {get;set;}


        
          [DataMember(Order = 8)]
          public DateTime? DateBought {get;set;}


        
          [DataMember(Order = 9)]
          public decimal? AcquisitionAmount {get;set;}


        
          [DataMember(Order = 10)]
          public decimal? PercentageAcquired {get;set;}


        
          [DataMember(Order = 11)]
          public string DisplayName {get;set;}


        
          [DataMember(Order = 12)]
          public bool? overrule {get;set;}


          #endregion
      }

  
      [DataContract(Namespace = "AY2008.DBIApp")]
      public class DividendItem : IEntity
      {
          #region IEntity
          
          [DataMember(Name = "ID", Order=1)]
          public Guid Id { get; set; }
          
          [DataMember(Order=2)]
          public DateTime? LastUpdated { get; set; }
          
          [DataMember(Order = 3)]
          public bool IsValid { get; set; }
          
          [DataMember(Order = 4)]
          public bool PreviousImported { get; set; }
          
          #endregion
          
          #region entitymembers
          

        
          [DataMember(Order = 5)]
          public StringListItemDataContract Participatie {get;set;}


        
          [DataMember(Order = 6)]
          public StringListItemDataContract AccountDividend {get;set;}


        
          [DataMember(Order = 7)]
          public DateTime? DateDividend {get;set;}


        
          [DataMember(Order = 8)]
          public bool? Forex {get;set;}


        
          [DataMember(Order = 9)]
          public bool? Taxation {get;set;}


        
          [DataMember(Order = 10)]
          public decimal? Bruto {get;set;}


        
          [DataMember(Order = 11)]
          public decimal? Perc95 {get;set;}


        
          [DataMember(Order = 12)]
          public decimal? Bedrag {get;set;}


        
          [DataMember(Order = 13)]
          public string ParticipatieNaam {get;set;}


          #endregion
      }

  

    #endregion

    #region roottype
    

      [DataContract(Namespace = "AY2008.DBIApp")]
      public class DBIWorksheet : IWorksheet
      {
          public DBIWorksheet() {
              // Set Minimal

        
              this.Participations = new List<EY.com.eBook.RuleEngine.AY2008.DBIAppTypes.ParticipatieItem>();
              
        
              this.Dividends = new List<EY.com.eBook.RuleEngine.AY2008.DBIAppTypes.DividendItem>();
              
              this.LastUpdated = DateTime.Now;
          }
        
          
          #region entitymembers
          

        
          [DataMember(Order = 1)]
          public List<EY.com.eBook.RuleEngine.AY2008.DBIAppTypes.ParticipatieItem> Participations {get;set;}


        
          [DataMember(Order = 2)]
          public List<EY.com.eBook.RuleEngine.AY2008.DBIAppTypes.DividendItem> Dividends {get;set;}


      
          #endregion
          
          #region IWorksheet Members
          [DataMember(Order = 3)]
          public DateTime? LastUpdated { get; set; }

          #endregion
      }

  
   
    #endregion

}
  