﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.RuleEngine.Core;
using EY.com.eBook.RuleEngine.Core.Interfaces;
using EY.com.eBook.RuleEngine.Data;

    
namespace EY.com.eBook.RuleEngine.AY2008.ErelonenHuurAppTypes
{
    #region subtypes


      [DataContract(Namespace = "AY2008.ErelonenHuurApp")]
      public class ImportItem : IEntity
      {
          #region IEntity
          
          [DataMember(Name = "ID", Order=1)]
          public Guid Id { get; set; }
          
          [DataMember(Order=2)]
          public DateTime? LastUpdated { get; set; }
          
          [DataMember(Order = 3)]
          public bool IsValid { get; set; }
          
          [DataMember(Order = 4)]
          public bool PreviousImported { get; set; }
          
          #endregion
          
          #region entitymembers
          

        
          [DataMember(Order = 5)]
          public string Rekening {get;set;}


        
          [DataMember(Order = 6)]
          public string GrootBoek {get;set;}


        
          [DataMember(Order = 7)]
          public string GrootboekOmschrijving {get;set;}


        
          [DataMember(Order = 8)]
          public StringListItemDataContract Soort {get;set;}


        
          [DataMember(Order = 9)]
          public decimal? Bedrag {get;set;}


        
          [DataMember(Order = 10)]
          public decimal? FiscaleWaarde {get;set;}


        
          [DataMember(Order = 11)]
          public bool? delete {get;set;}


          #endregion
      }

  
      [DataContract(Namespace = "AY2008.ErelonenHuurApp")]
      public class ImportedHistory : IEntity
      {
          #region IEntity
          
          [DataMember(Name = "ID", Order=1)]
          public Guid Id { get; set; }
          
          [DataMember(Order=2)]
          public DateTime? LastUpdated { get; set; }
          
          [DataMember(Order = 3)]
          public bool IsValid { get; set; }
          
          [DataMember(Order = 4)]
          public bool PreviousImported { get; set; }
          
          #endregion
          
          #region entitymembers
          

        
          [DataMember(Order = 5)]
          public string Grootboek {get;set;}


        
          [DataMember(Order = 6)]
          public decimal? Saldo {get;set;}


        
          [DataMember(Order = 7)]
          public DateTime? Datum {get;set;}


        
          [DataMember(Order = 8)]
          public StringListItemDataContract Leverancier {get;set;}


          #endregion
      }

  
      [DataContract(Namespace = "AY2008.ErelonenHuurApp")]
      public class VerdelingHistoriek : IEntity
      {
          #region IEntity
          
          [DataMember(Name = "ID", Order=1)]
          public Guid Id { get; set; }
          
          [DataMember(Order=2)]
          public DateTime? LastUpdated { get; set; }
          
          [DataMember(Order = 3)]
          public bool IsValid { get; set; }
          
          [DataMember(Order = 4)]
          public bool PreviousImported { get; set; }
          
          #endregion
          
          #region entitymembers
          

        
          [DataMember(Order = 5)]
          public StringListItemDataContract ImportedAccount {get;set;}


        
          [DataMember(Order = 6)]
          public StringListItemDataContract Soort {get;set;}


        
          [DataMember(Order = 7)]
          public string SoortMeta {get;set;}


        
          [DataMember(Order = 8)]
          public StringListItemDataContract Leverancier {get;set;}


        
          [DataMember(Order = 9)]
          public decimal? TotaalHistoriek {get;set;}


        
          [DataMember(Order = 10)]
          public decimal? FiscaleWaarde {get;set;}


        
          [DataMember(Order = 11)]
          public decimal? Terugname {get;set;}


        
          [DataMember(Order = 12)]
          public decimal? Toename {get;set;}


        
          [DataMember(Order = 13)]
          public decimal? Betaald {get;set;}


        
          [DataMember(Order = 14)]
          public bool? Fiche {get;set;}


        
          [DataMember(Order = 15)]
          public bool? delete {get;set;}


          #endregion
      }

  
      [DataContract(Namespace = "AY2008.ErelonenHuurApp")]
      public class Fiche281_50 : IEntity
      {
          #region IEntity
          
          [DataMember(Name = "ID", Order=1)]
          public Guid Id { get; set; }
          
          [DataMember(Order=2)]
          public DateTime? LastUpdated { get; set; }
          
          [DataMember(Order = 3)]
          public bool IsValid { get; set; }
          
          [DataMember(Order = 4)]
          public bool PreviousImported { get; set; }
          
          #endregion
          
          #region entitymembers
          

        
          [DataMember(Order = 5)]
          public StringListItemDataContract Leverancier {get;set;}


        
          [DataMember(Order = 6)]
          public string Ondernemingsnummer {get;set;}


        
          [DataMember(Order = 7)]
          public string UitgeoefendBeroep {get;set;}


        
          [DataMember(Order = 8)]
          public string BuitenlandsFiscaalID {get;set;}


        
          [DataMember(Order = 9)]
          public DateTime? Geboortedatum {get;set;}


        
          [DataMember(Order = 10)]
          public decimal? Commissies {get;set;}


        
          [DataMember(Order = 11)]
          public decimal? Erelonen {get;set;}


        
          [DataMember(Order = 12)]
          public decimal? Voordelen {get;set;}


        
          [DataMember(Order = 13)]
          public string VoordelenAlleAardOmschr {get;set;}


        
          [DataMember(Order = 14)]
          public decimal? Kosten {get;set;}


        
          [DataMember(Order = 15)]
          public decimal? Totaal {get;set;}


        
          [DataMember(Order = 16)]
          public decimal? Uitbetaald {get;set;}


        
          [DataMember(Order = 17)]
          public bool? delete {get;set;}


        
          [DataMember(Order = 18)]
          public bool? Fiche {get;set;}


        
          [DataMember(Order = 19)]
          public int? Nr {get;set;}


        
          [DataMember(Order = 20)]
          public string Jaar {get;set;}


        
          [DataMember(Order = 21)]
          public string NaamLeverancier {get;set;}


        
          [DataMember(Order = 22)]
          public string PostcodeLeverancier {get;set;}


        
          [DataMember(Order = 23)]
          public string WoonplaatsLeverancier {get;set;}


        
          [DataMember(Order = 24)]
          public string AdresLeverancier {get;set;}


          #endregion
      }

  

    #endregion

    #region roottype
    

      [DataContract(Namespace = "AY2008.ErelonenHuurApp")]
      public class ErelonenHuur : IWorksheet
      {
          public ErelonenHuur() {
              // Set Minimal

        
              this.Import = new List<EY.com.eBook.RuleEngine.AY2008.ErelonenHuurAppTypes.ImportItem>();
              
        
              this.ImportedHistory = new List<EY.com.eBook.RuleEngine.AY2008.ErelonenHuurAppTypes.ImportedHistory>();
              
        
              this.Historiek = new List<EY.com.eBook.RuleEngine.AY2008.ErelonenHuurAppTypes.VerdelingHistoriek>();
              
        
              this.Fiches281_50 = new List<EY.com.eBook.RuleEngine.AY2008.ErelonenHuurAppTypes.Fiche281_50>();
              
        
              this.Fiche281_50Col = new List<EY.com.eBook.RuleEngine.AY2008.ErelonenHuurAppTypes.Fiche281_50>();
              
              this.LastUpdated = DateTime.Now;
          }
        
          
          #region entitymembers
          

        
          [DataMember(Order = 1)]
          public List<EY.com.eBook.RuleEngine.AY2008.ErelonenHuurAppTypes.ImportItem> Import {get;set;}


        
          [DataMember(Order = 2)]
          public List<EY.com.eBook.RuleEngine.AY2008.ErelonenHuurAppTypes.ImportedHistory> ImportedHistory {get;set;}


        
          [DataMember(Order = 3)]
          public List<EY.com.eBook.RuleEngine.AY2008.ErelonenHuurAppTypes.VerdelingHistoriek> Historiek {get;set;}


        
          [DataMember(Order = 4)]
          public List<EY.com.eBook.RuleEngine.AY2008.ErelonenHuurAppTypes.Fiche281_50> Fiches281_50 {get;set;}


        
          [DataMember(Order = 5)]
          public List<EY.com.eBook.RuleEngine.AY2008.ErelonenHuurAppTypes.Fiche281_50> Fiche281_50Col {get;set;}


      
          #endregion
          
          #region IWorksheet Members
          [DataMember(Order = 6)]
          public DateTime? LastUpdated { get; set; }

          #endregion
      }

  
   
    #endregion

}
  