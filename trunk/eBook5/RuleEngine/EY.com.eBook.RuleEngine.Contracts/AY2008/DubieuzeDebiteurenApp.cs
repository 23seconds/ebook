﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.RuleEngine.Core;
using EY.com.eBook.RuleEngine.Core.Interfaces;
using EY.com.eBook.RuleEngine.Data;

    
namespace EY.com.eBook.RuleEngine.AY2008.DubieuzeDebiteurenAppTypes
{
    #region subtypes


      [DataContract(Namespace = "AY2008.DubieuzeDebiteurenApp")]
      public class DubieuzeDebiteurT : IEntity
      {
          #region IEntity
          
          [DataMember(Name = "ID", Order=1)]
          public Guid Id { get; set; }
          
          [DataMember(Order=2)]
          public DateTime? LastUpdated { get; set; }
          
          [DataMember(Order = 3)]
          public bool IsValid { get; set; }
          
          [DataMember(Order = 4)]
          public bool PreviousImported { get; set; }
          
          #endregion
          
          #region entitymembers
          

        
          [DataMember(Order = 5)]
          public bool? AutoLine {get;set;}


        
          [DataMember(Order = 6)]
          public bool? AlgemeneProvisie {get;set;}


        
          [DataMember(Order = 7)]
          public StringListItemDataContract Klant {get;set;}


        
          [DataMember(Order = 8)]
          public string label {get;set;}


        
          [DataMember(Order = 9)]
          public StringListItemDataContract FactuurJaar {get;set;}


        
          [DataMember(Order = 10)]
          public string InvoiceNumber {get;set;}


        
          [DataMember(Order = 11)]
          public decimal? FactuurBedrag {get;set;}


        
          [DataMember(Order = 12)]
          public decimal? FactuurVAT {get;set;}


        
          [DataMember(Order = 13)]
          public decimal? Vordering {get;set;}


        
          [DataMember(Order = 14)]
          public decimal? Waardevermindering {get;set;}


        
          [DataMember(Order = 15)]
          public decimal? WaardeverminderingVAT {get;set;}


        
          [DataMember(Order = 16)]
          public string BelastbaarOfVrijgesteld {get;set;}


        
          [DataMember(Order = 17)]
          public decimal? Total {get;set;}


        
          [DataMember(Order = 18)]
          public decimal? VorderingStart {get;set;}


        
          [DataMember(Order = 19)]
          public decimal? VorderingResterend {get;set;}


        
          [DataMember(Order = 20)]
          public decimal? BedragDefinitiefVerlies {get;set;}


        
          [DataMember(Order = 21)]
          public decimal? VorderingEinde {get;set;}


          #endregion
      }

  
      [DataContract(Namespace = "AY2008.DubieuzeDebiteurenApp")]
      public class DubieuzeDebiteurW : IEntity
      {
          #region IEntity
          
          [DataMember(Name = "ID", Order=1)]
          public Guid Id { get; set; }
          
          [DataMember(Order=2)]
          public DateTime? LastUpdated { get; set; }
          
          [DataMember(Order = 3)]
          public bool IsValid { get; set; }
          
          [DataMember(Order = 4)]
          public bool PreviousImported { get; set; }
          
          #endregion
          
          #region entitymembers
          

        
          [DataMember(Order = 5)]
          public StringListItemDataContract Bestaande {get;set;}


        
          [DataMember(Order = 6)]
          public bool? AlgemeneProvisie {get;set;}


        
          [DataMember(Order = 7)]
          public StringListItemDataContract Klant {get;set;}


        
          [DataMember(Order = 8)]
          public string label {get;set;}


        
          [DataMember(Order = 9)]
          public StringListItemDataContract FactuurJaar {get;set;}


        
          [DataMember(Order = 10)]
          public string InvoiceNumber {get;set;}


        
          [DataMember(Order = 11)]
          public decimal? FactuurBedrag {get;set;}


        
          [DataMember(Order = 12)]
          public decimal? FactuurVAT {get;set;}


        
          [DataMember(Order = 13)]
          public decimal? Vordering {get;set;}


        
          [DataMember(Order = 14)]
          public decimal? VorderingStart {get;set;}


        
          [DataMember(Order = 15)]
          public decimal? VorderingResterend {get;set;}


        
          [DataMember(Order = 16)]
          public bool? BookDefinitive {get;set;}


        
          [DataMember(Order = 17)]
          public decimal? BedragDefinitiefVerlies {get;set;}


        
          [DataMember(Order = 18)]
          public string RedenDefinitiefVerlies {get;set;}


        
          [DataMember(Order = 19)]
          public string ToenameOfTerugname {get;set;}


        
          [DataMember(Order = 20)]
          public decimal? Waardevermindering {get;set;}


        
          [DataMember(Order = 21)]
          public decimal? WaardeverminderingVAT {get;set;}


        
          [DataMember(Order = 22)]
          public string RedenVanAfname {get;set;}


        
          [DataMember(Order = 23)]
          public string Commentaar {get;set;}


        
          [DataMember(Order = 24)]
          public string BelastbaarOfVrijgesteld {get;set;}


        
          [DataMember(Order = 25)]
          public bool? Book {get;set;}


        
          [DataMember(Order = 26)]
          public decimal? Total {get;set;}


        
          [DataMember(Order = 27)]
          public decimal? VorderingEinde {get;set;}


        
          [DataMember(Order = 28)]
          public decimal? SignedWaardevermindering {get;set;}


        
          [DataMember(Order = 29)]
          public decimal? VorderingResterendEinde {get;set;}


          #endregion
      }

  
      [DataContract(Namespace = "AY2008.DubieuzeDebiteurenApp")]
      public class DubieuzeDebiteurN : IEntity
      {
          #region IEntity
          
          [DataMember(Name = "ID", Order=1)]
          public Guid Id { get; set; }
          
          [DataMember(Order=2)]
          public DateTime? LastUpdated { get; set; }
          
          [DataMember(Order = 3)]
          public bool IsValid { get; set; }
          
          [DataMember(Order = 4)]
          public bool PreviousImported { get; set; }
          
          #endregion
          
          #region entitymembers
          

        
          [DataMember(Order = 5)]
          public bool? AlgemeneProvisie {get;set;}


        
          [DataMember(Order = 6)]
          public StringListItemDataContract Klant {get;set;}


        
          [DataMember(Order = 7)]
          public string label {get;set;}


        
          [DataMember(Order = 8)]
          public StringListItemDataContract FactuurJaar {get;set;}


        
          [DataMember(Order = 9)]
          public string InvoiceNumber {get;set;}


        
          [DataMember(Order = 10)]
          public decimal? FactuurBedrag {get;set;}


        
          [DataMember(Order = 11)]
          public decimal? FactuurVAT {get;set;}


        
          [DataMember(Order = 12)]
          public decimal? Vordering {get;set;}


        
          [DataMember(Order = 13)]
          public decimal? Waardevermindering {get;set;}


        
          [DataMember(Order = 14)]
          public decimal? WaardeverminderingVAT {get;set;}


        
          [DataMember(Order = 15)]
          public string Commentaar {get;set;}


        
          [DataMember(Order = 16)]
          public string BelastbaarOfVrijgesteld {get;set;}


        
          [DataMember(Order = 17)]
          public decimal? VorderingStart {get;set;}


        
          [DataMember(Order = 18)]
          public decimal? VorderingEinde {get;set;}


        
          [DataMember(Order = 19)]
          public decimal? VorderingResterend {get;set;}


        
          [DataMember(Order = 20)]
          public bool? Book {get;set;}


        
          [DataMember(Order = 21)]
          public decimal? Total {get;set;}


          #endregion
      }

  
      [DataContract(Namespace = "AY2008.DubieuzeDebiteurenApp")]
      public class DubieuzeDebiteurH : IEntity
      {
          #region IEntity
          
          [DataMember(Name = "ID", Order=1)]
          public Guid Id { get; set; }
          
          [DataMember(Order=2)]
          public DateTime? LastUpdated { get; set; }
          
          [DataMember(Order = 3)]
          public bool IsValid { get; set; }
          
          [DataMember(Order = 4)]
          public bool PreviousImported { get; set; }
          
          #endregion
          
          #region entitymembers
          

        
          [DataMember(Order = 5)]
          public bool? AlgemeneProvisie {get;set;}


        
          [DataMember(Order = 6)]
          public StringListItemDataContract Klant {get;set;}


        
          [DataMember(Order = 7)]
          public string label {get;set;}


        
          [DataMember(Order = 8)]
          public StringListItemDataContract FactuurJaar {get;set;}


        
          [DataMember(Order = 9)]
          public string InvoiceNumber {get;set;}


        
          [DataMember(Order = 10)]
          public decimal? FactuurBedrag {get;set;}


        
          [DataMember(Order = 11)]
          public decimal? FactuurVAT {get;set;}


        
          [DataMember(Order = 12)]
          public string BelastbaarOfVrijgesteld {get;set;}


        
          [DataMember(Order = 13)]
          public decimal? Vordering {get;set;}


        
          [DataMember(Order = 14)]
          public decimal? VorderingStart {get;set;}


        
          [DataMember(Order = 15)]
          public decimal? VorderingEinde {get;set;}


        
          [DataMember(Order = 16)]
          public decimal? WaardeverminderingVorig {get;set;}


        
          [DataMember(Order = 17)]
          public decimal? Waardevermindering {get;set;}


        
          [DataMember(Order = 18)]
          public decimal? WaardeverminderingTotaal {get;set;}


        
          [DataMember(Order = 19)]
          public decimal? VorderingResterend {get;set;}


        
          [DataMember(Order = 20)]
          public decimal? VorderingResterendEinde {get;set;}


        
          [DataMember(Order = 21)]
          public bool? isNew {get;set;}


        
          [DataMember(Order = 22)]
          public string RedenTotVermindering {get;set;}


        
          [DataMember(Order = 23)]
          public decimal? WaardeverminderingVAT {get;set;}


        
          [DataMember(Order = 24)]
          public decimal? Total {get;set;}


        
          [DataMember(Order = 25)]
          public string ChangedID {get;set;}


          #endregion
      }

  

    #endregion

    #region roottype
    

      [DataContract(Namespace = "AY2008.DubieuzeDebiteurenApp")]
      public class DubieuzeDebiteuren : IWorksheet
      {
          public DubieuzeDebiteuren() {
              // Set Minimal

        
              this.ToestandEindVorigBoekjaar = new List<EY.com.eBook.RuleEngine.AY2008.DubieuzeDebiteurenAppTypes.DubieuzeDebiteurT>();
              
        
              this.WijzigingBestaandeTijdensBoekjaar = new List<EY.com.eBook.RuleEngine.AY2008.DubieuzeDebiteurenAppTypes.DubieuzeDebiteurW>();
              
        
              this.NieuweTijdensBoekjaar = new List<EY.com.eBook.RuleEngine.AY2008.DubieuzeDebiteurenAppTypes.DubieuzeDebiteurN>();
              
        
              this.ToestandEindHuidigBoekjaar = new List<EY.com.eBook.RuleEngine.AY2008.DubieuzeDebiteurenAppTypes.DubieuzeDebiteurH>();
              
              this.LastUpdated = DateTime.Now;
          }
        
          
          #region entitymembers
          

        
          [DataMember(Order = 1)]
          public List<EY.com.eBook.RuleEngine.AY2008.DubieuzeDebiteurenAppTypes.DubieuzeDebiteurT> ToestandEindVorigBoekjaar {get;set;}


        
          [DataMember(Order = 2)]
          public List<EY.com.eBook.RuleEngine.AY2008.DubieuzeDebiteurenAppTypes.DubieuzeDebiteurW> WijzigingBestaandeTijdensBoekjaar {get;set;}


        
          [DataMember(Order = 3)]
          public List<EY.com.eBook.RuleEngine.AY2008.DubieuzeDebiteurenAppTypes.DubieuzeDebiteurN> NieuweTijdensBoekjaar {get;set;}


        
          [DataMember(Order = 4)]
          public List<EY.com.eBook.RuleEngine.AY2008.DubieuzeDebiteurenAppTypes.DubieuzeDebiteurH> ToestandEindHuidigBoekjaar {get;set;}


      
          #endregion
          
          #region IWorksheet Members
          [DataMember(Order = 5)]
          public DateTime? LastUpdated { get; set; }

          #endregion
      }

  
   
    #endregion

}
  