﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.RuleEngine.Data.AY2008
{

    [DataContract]
    public class CriteriaFacturatieAppFacturatieKlantDataContract
    {
        // FileId
        [DataMember(Order=1)]
        public Guid Id { get; set; }

        [DataMember(Order=2)]
        public string Culture { get; set; }

        [DataMember(Order=3)]
        public string CollectionPath { get; set; }

        [DataMember(Order=4)]
        public EY.com.eBook.RuleEngine.AY2008.FacturatieAppTypes.FacturatieKlant Entity { get; set; }
    }

  
    [DataContract]
    public class CriteriaFacturatieAppFacturatieLeverancierCNDataContract
    {
        // FileId
        [DataMember(Order=1)]
        public Guid Id { get; set; }

        [DataMember(Order=2)]
        public string Culture { get; set; }

        [DataMember(Order=3)]
        public string CollectionPath { get; set; }

        [DataMember(Order=4)]
        public EY.com.eBook.RuleEngine.AY2008.FacturatieAppTypes.FacturatieLeverancierCN Entity { get; set; }
    }

  
    [DataContract]
    public class CriteriaFacturatieAppFacturatieLeverancierDataContract
    {
        // FileId
        [DataMember(Order=1)]
        public Guid Id { get; set; }

        [DataMember(Order=2)]
        public string Culture { get; set; }

        [DataMember(Order=3)]
        public string CollectionPath { get; set; }

        [DataMember(Order=4)]
        public EY.com.eBook.RuleEngine.AY2008.FacturatieAppTypes.FacturatieLeverancier Entity { get; set; }
    }

  
    [DataContract]
    public class CriteriaOverlopendeRekeningenAppOverTeDragenKostDataContract
    {
        // FileId
        [DataMember(Order=1)]
        public Guid Id { get; set; }

        [DataMember(Order=2)]
        public string Culture { get; set; }

        [DataMember(Order=3)]
        public string CollectionPath { get; set; }

        [DataMember(Order=4)]
        public EY.com.eBook.RuleEngine.AY2008.OverlopendeRekeningenAppTypes.OverTeDragenKost Entity { get; set; }
    }

  
    [DataContract]
    public class CriteriaOverlopendeRekeningenAppVerkregenOpbrengstDataContract
    {
        // FileId
        [DataMember(Order=1)]
        public Guid Id { get; set; }

        [DataMember(Order=2)]
        public string Culture { get; set; }

        [DataMember(Order=3)]
        public string CollectionPath { get; set; }

        [DataMember(Order=4)]
        public EY.com.eBook.RuleEngine.AY2008.OverlopendeRekeningenAppTypes.VerkregenOpbrengst Entity { get; set; }
    }

  
    [DataContract]
    public class CriteriaOverlopendeRekeningenAppToeTeRekenenKostDataContract
    {
        // FileId
        [DataMember(Order=1)]
        public Guid Id { get; set; }

        [DataMember(Order=2)]
        public string Culture { get; set; }

        [DataMember(Order=3)]
        public string CollectionPath { get; set; }

        [DataMember(Order=4)]
        public EY.com.eBook.RuleEngine.AY2008.OverlopendeRekeningenAppTypes.ToeTeRekenenKost Entity { get; set; }
    }

  
    [DataContract]
    public class CriteriaOverlopendeRekeningenAppOverTeDragenOpbrengstDataContract
    {
        // FileId
        [DataMember(Order=1)]
        public Guid Id { get; set; }

        [DataMember(Order=2)]
        public string Culture { get; set; }

        [DataMember(Order=3)]
        public string CollectionPath { get; set; }

        [DataMember(Order=4)]
        public EY.com.eBook.RuleEngine.AY2008.OverlopendeRekeningenAppTypes.OverTeDragenOpbrengst Entity { get; set; }
    }

  
    [DataContract]
    public class CriteriaVergelijkendeCijfersAppCijferBlokDataContract
    {
        // FileId
        [DataMember(Order=1)]
        public Guid Id { get; set; }

        [DataMember(Order=2)]
        public string Culture { get; set; }

        [DataMember(Order=3)]
        public string CollectionPath { get; set; }

        [DataMember(Order=4)]
        public EY.com.eBook.RuleEngine.AY2008.VergelijkendeCijfersAppTypes.CijferBlok Entity { get; set; }
    }

  
    [DataContract]
    public class CriteriaVoorschottenAppGegevenDataContract
    {
        // FileId
        [DataMember(Order=1)]
        public Guid Id { get; set; }

        [DataMember(Order=2)]
        public string Culture { get; set; }

        [DataMember(Order=3)]
        public string CollectionPath { get; set; }

        [DataMember(Order=4)]
        public EY.com.eBook.RuleEngine.AY2008.VoorschottenAppTypes.Gegeven Entity { get; set; }
    }

  
    [DataContract]
    public class CriteriaVoorschottenAppIntrestVoetDataContract
    {
        // FileId
        [DataMember(Order=1)]
        public Guid Id { get; set; }

        [DataMember(Order=2)]
        public string Culture { get; set; }

        [DataMember(Order=3)]
        public string CollectionPath { get; set; }

        [DataMember(Order=4)]
        public EY.com.eBook.RuleEngine.AY2008.VoorschottenAppTypes.IntrestVoet Entity { get; set; }
    }

  
    [DataContract]
    public class CriteriaVoorschottenAppRentegevendVoorschotDataContract
    {
        // FileId
        [DataMember(Order=1)]
        public Guid Id { get; set; }

        [DataMember(Order=2)]
        public string Culture { get; set; }

        [DataMember(Order=3)]
        public string CollectionPath { get; set; }

        [DataMember(Order=4)]
        public EY.com.eBook.RuleEngine.AY2008.VoorschottenAppTypes.RentegevendVoorschot Entity { get; set; }
    }

  
    [DataContract]
    public class CriteriaErelonenHuurAppImportItemDataContract
    {
        // FileId
        [DataMember(Order=1)]
        public Guid Id { get; set; }

        [DataMember(Order=2)]
        public string Culture { get; set; }

        [DataMember(Order=3)]
        public string CollectionPath { get; set; }

        [DataMember(Order=4)]
        public EY.com.eBook.RuleEngine.AY2008.ErelonenHuurAppTypes.ImportItem Entity { get; set; }
    }

  
    [DataContract]
    public class CriteriaErelonenHuurAppImportedHistoryDataContract
    {
        // FileId
        [DataMember(Order=1)]
        public Guid Id { get; set; }

        [DataMember(Order=2)]
        public string Culture { get; set; }

        [DataMember(Order=3)]
        public string CollectionPath { get; set; }

        [DataMember(Order=4)]
        public EY.com.eBook.RuleEngine.AY2008.ErelonenHuurAppTypes.ImportedHistory Entity { get; set; }
    }

  
    [DataContract]
    public class CriteriaErelonenHuurAppVerdelingHistoriekDataContract
    {
        // FileId
        [DataMember(Order=1)]
        public Guid Id { get; set; }

        [DataMember(Order=2)]
        public string Culture { get; set; }

        [DataMember(Order=3)]
        public string CollectionPath { get; set; }

        [DataMember(Order=4)]
        public EY.com.eBook.RuleEngine.AY2008.ErelonenHuurAppTypes.VerdelingHistoriek Entity { get; set; }
    }

  
    [DataContract]
    public class CriteriaErelonenHuurAppFiche281_50DataContract
    {
        // FileId
        [DataMember(Order=1)]
        public Guid Id { get; set; }

        [DataMember(Order=2)]
        public string Culture { get; set; }

        [DataMember(Order=3)]
        public string CollectionPath { get; set; }

        [DataMember(Order=4)]
        public EY.com.eBook.RuleEngine.AY2008.ErelonenHuurAppTypes.Fiche281_50 Entity { get; set; }
    }

  
    [DataContract]
    public class CriteriaVoordelenAppVAA_AutoDataContract
    {
        // FileId
        [DataMember(Order=1)]
        public Guid Id { get; set; }

        [DataMember(Order=2)]
        public string Culture { get; set; }

        [DataMember(Order=3)]
        public string CollectionPath { get; set; }

        [DataMember(Order=4)]
        public EY.com.eBook.RuleEngine.AY2008.VoordelenAppTypes.VAA_Auto Entity { get; set; }
    }

  
    [DataContract]
    public class CriteriaVoordelenAppVAA_AutoCO2DataContract
    {
        // FileId
        [DataMember(Order=1)]
        public Guid Id { get; set; }

        [DataMember(Order=2)]
        public string Culture { get; set; }

        [DataMember(Order=3)]
        public string CollectionPath { get; set; }

        [DataMember(Order=4)]
        public EY.com.eBook.RuleEngine.AY2008.VoordelenAppTypes.VAA_AutoCO2 Entity { get; set; }
    }

  
    [DataContract]
    public class CriteriaVoordelenAppVAA_NutsDataContract
    {
        // FileId
        [DataMember(Order=1)]
        public Guid Id { get; set; }

        [DataMember(Order=2)]
        public string Culture { get; set; }

        [DataMember(Order=3)]
        public string CollectionPath { get; set; }

        [DataMember(Order=4)]
        public EY.com.eBook.RuleEngine.AY2008.VoordelenAppTypes.VAA_Nuts Entity { get; set; }
    }

  
    [DataContract]
    public class CriteriaVoordelenAppVAA_HUURDataContract
    {
        // FileId
        [DataMember(Order=1)]
        public Guid Id { get; set; }

        [DataMember(Order=2)]
        public string Culture { get; set; }

        [DataMember(Order=3)]
        public string CollectionPath { get; set; }

        [DataMember(Order=4)]
        public EY.com.eBook.RuleEngine.AY2008.VoordelenAppTypes.VAA_HUUR Entity { get; set; }
    }

  
    [DataContract]
    public class CriteriaVoordelenAppVAA_HUURHERKDataContract
    {
        // FileId
        [DataMember(Order=1)]
        public Guid Id { get; set; }

        [DataMember(Order=2)]
        public string Culture { get; set; }

        [DataMember(Order=3)]
        public string CollectionPath { get; set; }

        [DataMember(Order=4)]
        public EY.com.eBook.RuleEngine.AY2008.VoordelenAppTypes.VAA_HUURHERK Entity { get; set; }
    }

  
    [DataContract]
    public class CriteriaVoordelenAppVAA_PCDataContract
    {
        // FileId
        [DataMember(Order=1)]
        public Guid Id { get; set; }

        [DataMember(Order=2)]
        public string Culture { get; set; }

        [DataMember(Order=3)]
        public string CollectionPath { get; set; }

        [DataMember(Order=4)]
        public EY.com.eBook.RuleEngine.AY2008.VoordelenAppTypes.VAA_PC Entity { get; set; }
    }

  
    [DataContract]
    public class CriteriaVoordelenAppVAA_TelefonieDataContract
    {
        // FileId
        [DataMember(Order=1)]
        public Guid Id { get; set; }

        [DataMember(Order=2)]
        public string Culture { get; set; }

        [DataMember(Order=3)]
        public string CollectionPath { get; set; }

        [DataMember(Order=4)]
        public EY.com.eBook.RuleEngine.AY2008.VoordelenAppTypes.VAA_Telefonie Entity { get; set; }
    }

  
    [DataContract]
    public class CriteriaVoordelenAppVAA_OverigeDataContract
    {
        // FileId
        [DataMember(Order=1)]
        public Guid Id { get; set; }

        [DataMember(Order=2)]
        public string Culture { get; set; }

        [DataMember(Order=3)]
        public string CollectionPath { get; set; }

        [DataMember(Order=4)]
        public EY.com.eBook.RuleEngine.AY2008.VoordelenAppTypes.VAA_Overige Entity { get; set; }
    }

  
    [DataContract]
    public class CriteriaVoordelenAppVAA_Interesten_RC_BedrijfsleiderDataContract
    {
        // FileId
        [DataMember(Order=1)]
        public Guid Id { get; set; }

        [DataMember(Order=2)]
        public string Culture { get; set; }

        [DataMember(Order=3)]
        public string CollectionPath { get; set; }

        [DataMember(Order=4)]
        public EY.com.eBook.RuleEngine.AY2008.VoordelenAppTypes.VAA_Interesten_RC_Bedrijfsleider Entity { get; set; }
    }

  
    [DataContract]
    public class CriteriaVoordelenAppVAA_Soc_Bijdr_BedrijfsleiderDataContract
    {
        // FileId
        [DataMember(Order=1)]
        public Guid Id { get; set; }

        [DataMember(Order=2)]
        public string Culture { get; set; }

        [DataMember(Order=3)]
        public string CollectionPath { get; set; }

        [DataMember(Order=4)]
        public EY.com.eBook.RuleEngine.AY2008.VoordelenAppTypes.VAA_Soc_Bijdr_Bedrijfsleider Entity { get; set; }
    }

  
    [DataContract]
    public class CriteriaVoordelenAppVAA_Overige_NutsDataContract
    {
        // FileId
        [DataMember(Order=1)]
        public Guid Id { get; set; }

        [DataMember(Order=2)]
        public string Culture { get; set; }

        [DataMember(Order=3)]
        public string CollectionPath { get; set; }

        [DataMember(Order=4)]
        public EY.com.eBook.RuleEngine.AY2008.VoordelenAppTypes.VAA_Overige_Nuts Entity { get; set; }
    }

  
    [DataContract]
    public class CriteriaPersoneelAppItem325DataContract
    {
        // FileId
        [DataMember(Order=1)]
        public Guid Id { get; set; }

        [DataMember(Order=2)]
        public string Culture { get; set; }

        [DataMember(Order=3)]
        public string CollectionPath { get; set; }

        [DataMember(Order=4)]
        public EY.com.eBook.RuleEngine.AY2008.PersoneelAppTypes.Item325 Entity { get; set; }
    }

  
    [DataContract]
    public class CriteriaPersoneelAppVerschilItemDataContract
    {
        // FileId
        [DataMember(Order=1)]
        public Guid Id { get; set; }

        [DataMember(Order=2)]
        public string Culture { get; set; }

        [DataMember(Order=3)]
        public string CollectionPath { get; set; }

        [DataMember(Order=4)]
        public EY.com.eBook.RuleEngine.AY2008.PersoneelAppTypes.VerschilItem Entity { get; set; }
    }

  
    [DataContract]
    public class CriteriaPersoneelAppWerknemerDataContract
    {
        // FileId
        [DataMember(Order=1)]
        public Guid Id { get; set; }

        [DataMember(Order=2)]
        public string Culture { get; set; }

        [DataMember(Order=3)]
        public string CollectionPath { get; set; }

        [DataMember(Order=4)]
        public EY.com.eBook.RuleEngine.AY2008.PersoneelAppTypes.Werknemer Entity { get; set; }
    }

  
    [DataContract]
    public class CriteriaPersoneelAppAlgemeenDataContract
    {
        // FileId
        [DataMember(Order=1)]
        public Guid Id { get; set; }

        [DataMember(Order=2)]
        public string Culture { get; set; }

        [DataMember(Order=3)]
        public string CollectionPath { get; set; }

        [DataMember(Order=4)]
        public EY.com.eBook.RuleEngine.AY2008.PersoneelAppTypes.Algemeen Entity { get; set; }
    }

  
    [DataContract]
    public class CriteriaPersoneelAppVakantiegeldDataContract
    {
        // FileId
        [DataMember(Order=1)]
        public Guid Id { get; set; }

        [DataMember(Order=2)]
        public string Culture { get; set; }

        [DataMember(Order=3)]
        public string CollectionPath { get; set; }

        [DataMember(Order=4)]
        public EY.com.eBook.RuleEngine.AY2008.PersoneelAppTypes.Vakantiegeld Entity { get; set; }
    }

  
    [DataContract]
    public class CriteriaBTWAppOmzetAangifteDataContract
    {
        // FileId
        [DataMember(Order=1)]
        public Guid Id { get; set; }

        [DataMember(Order=2)]
        public string Culture { get; set; }

        [DataMember(Order=3)]
        public string CollectionPath { get; set; }

        [DataMember(Order=4)]
        public EY.com.eBook.RuleEngine.AY2008.BTWAppTypes.OmzetAangifte Entity { get; set; }
    }

  
    [DataContract]
    public class CriteriaBTWAppOmzetGeboektDataContract
    {
        // FileId
        [DataMember(Order=1)]
        public Guid Id { get; set; }

        [DataMember(Order=2)]
        public string Culture { get; set; }

        [DataMember(Order=3)]
        public string CollectionPath { get; set; }

        [DataMember(Order=4)]
        public EY.com.eBook.RuleEngine.AY2008.BTWAppTypes.OmzetGeboekt Entity { get; set; }
    }

  
    [DataContract]
    public class CriteriaBTWAppOmzetVerklaringDataContract
    {
        // FileId
        [DataMember(Order=1)]
        public Guid Id { get; set; }

        [DataMember(Order=2)]
        public string Culture { get; set; }

        [DataMember(Order=3)]
        public string CollectionPath { get; set; }

        [DataMember(Order=4)]
        public EY.com.eBook.RuleEngine.AY2008.BTWAppTypes.OmzetVerklaring Entity { get; set; }
    }

  
    [DataContract]
    public class CriteriaBTWAppRCAangifteDataContract
    {
        // FileId
        [DataMember(Order=1)]
        public Guid Id { get; set; }

        [DataMember(Order=2)]
        public string Culture { get; set; }

        [DataMember(Order=3)]
        public string CollectionPath { get; set; }

        [DataMember(Order=4)]
        public EY.com.eBook.RuleEngine.AY2008.BTWAppTypes.RCAangifte Entity { get; set; }
    }

  
    [DataContract]
    public class CriteriaBTWAppRCGeboektDataContract
    {
        // FileId
        [DataMember(Order=1)]
        public Guid Id { get; set; }

        [DataMember(Order=2)]
        public string Culture { get; set; }

        [DataMember(Order=3)]
        public string CollectionPath { get; set; }

        [DataMember(Order=4)]
        public EY.com.eBook.RuleEngine.AY2008.BTWAppTypes.RCGeboekt Entity { get; set; }
    }

  
    [DataContract]
    public class CriteriaBTWAppRCVerklaringDataContract
    {
        // FileId
        [DataMember(Order=1)]
        public Guid Id { get; set; }

        [DataMember(Order=2)]
        public string Culture { get; set; }

        [DataMember(Order=3)]
        public string CollectionPath { get; set; }

        [DataMember(Order=4)]
        public EY.com.eBook.RuleEngine.AY2008.BTWAppTypes.RCVerklaring Entity { get; set; }
    }

  
    [DataContract]
    public class CriteriaRVIntrestenAppIntrestItemDataContract
    {
        // FileId
        [DataMember(Order=1)]
        public Guid Id { get; set; }

        [DataMember(Order=2)]
        public string Culture { get; set; }

        [DataMember(Order=3)]
        public string CollectionPath { get; set; }

        [DataMember(Order=4)]
        public EY.com.eBook.RuleEngine.AY2008.RVIntrestenAppTypes.IntrestItem Entity { get; set; }
    }

  
    [DataContract]
    public class CriteriaRVIntrestenAppAdjustmentDataContract
    {
        // FileId
        [DataMember(Order=1)]
        public Guid Id { get; set; }

        [DataMember(Order=2)]
        public string Culture { get; set; }

        [DataMember(Order=3)]
        public string CollectionPath { get; set; }

        [DataMember(Order=4)]
        public EY.com.eBook.RuleEngine.AY2008.RVIntrestenAppTypes.Adjustment Entity { get; set; }
    }

  
    [DataContract]
    public class CriteriaRVIntrestenAppVerchilItemDataContract
    {
        // FileId
        [DataMember(Order=1)]
        public Guid Id { get; set; }

        [DataMember(Order=2)]
        public string Culture { get; set; }

        [DataMember(Order=3)]
        public string CollectionPath { get; set; }

        [DataMember(Order=4)]
        public EY.com.eBook.RuleEngine.AY2008.RVIntrestenAppTypes.VerchilItem Entity { get; set; }
    }

  
    [DataContract]
    public class CriteriaDubieuzeDebiteurenAppDubieuzeDebiteurTDataContract
    {
        // FileId
        [DataMember(Order=1)]
        public Guid Id { get; set; }

        [DataMember(Order=2)]
        public string Culture { get; set; }

        [DataMember(Order=3)]
        public string CollectionPath { get; set; }

        [DataMember(Order=4)]
        public EY.com.eBook.RuleEngine.AY2008.DubieuzeDebiteurenAppTypes.DubieuzeDebiteurT Entity { get; set; }
    }

  
    [DataContract]
    public class CriteriaDubieuzeDebiteurenAppDubieuzeDebiteurWDataContract
    {
        // FileId
        [DataMember(Order=1)]
        public Guid Id { get; set; }

        [DataMember(Order=2)]
        public string Culture { get; set; }

        [DataMember(Order=3)]
        public string CollectionPath { get; set; }

        [DataMember(Order=4)]
        public EY.com.eBook.RuleEngine.AY2008.DubieuzeDebiteurenAppTypes.DubieuzeDebiteurW Entity { get; set; }
    }

  
    [DataContract]
    public class CriteriaDubieuzeDebiteurenAppDubieuzeDebiteurNDataContract
    {
        // FileId
        [DataMember(Order=1)]
        public Guid Id { get; set; }

        [DataMember(Order=2)]
        public string Culture { get; set; }

        [DataMember(Order=3)]
        public string CollectionPath { get; set; }

        [DataMember(Order=4)]
        public EY.com.eBook.RuleEngine.AY2008.DubieuzeDebiteurenAppTypes.DubieuzeDebiteurN Entity { get; set; }
    }

  
    [DataContract]
    public class CriteriaDubieuzeDebiteurenAppDubieuzeDebiteurHDataContract
    {
        // FileId
        [DataMember(Order=1)]
        public Guid Id { get; set; }

        [DataMember(Order=2)]
        public string Culture { get; set; }

        [DataMember(Order=3)]
        public string CollectionPath { get; set; }

        [DataMember(Order=4)]
        public EY.com.eBook.RuleEngine.AY2008.DubieuzeDebiteurenAppTypes.DubieuzeDebiteurH Entity { get; set; }
    }

  
    [DataContract]
    public class CriteriaVerliesHandelsdebiteurenAppVerliesHandelsDebiteurDataContract
    {
        // FileId
        [DataMember(Order=1)]
        public Guid Id { get; set; }

        [DataMember(Order=2)]
        public string Culture { get; set; }

        [DataMember(Order=3)]
        public string CollectionPath { get; set; }

        [DataMember(Order=4)]
        public EY.com.eBook.RuleEngine.AY2008.VerliesHandelsdebiteurenAppTypes.VerliesHandelsDebiteur Entity { get; set; }
    }

  
    [DataContract]
    public class CriteriaVoorzieningenAppVoorzieningenTDataContract
    {
        // FileId
        [DataMember(Order=1)]
        public Guid Id { get; set; }

        [DataMember(Order=2)]
        public string Culture { get; set; }

        [DataMember(Order=3)]
        public string CollectionPath { get; set; }

        [DataMember(Order=4)]
        public EY.com.eBook.RuleEngine.AY2008.VoorzieningenAppTypes.VoorzieningenT Entity { get; set; }
    }

  
    [DataContract]
    public class CriteriaVoorzieningenAppVoorzieningenWDataContract
    {
        // FileId
        [DataMember(Order=1)]
        public Guid Id { get; set; }

        [DataMember(Order=2)]
        public string Culture { get; set; }

        [DataMember(Order=3)]
        public string CollectionPath { get; set; }

        [DataMember(Order=4)]
        public EY.com.eBook.RuleEngine.AY2008.VoorzieningenAppTypes.VoorzieningenW Entity { get; set; }
    }

  
    [DataContract]
    public class CriteriaVoorzieningenAppVoorzieningenNDataContract
    {
        // FileId
        [DataMember(Order=1)]
        public Guid Id { get; set; }

        [DataMember(Order=2)]
        public string Culture { get; set; }

        [DataMember(Order=3)]
        public string CollectionPath { get; set; }

        [DataMember(Order=4)]
        public EY.com.eBook.RuleEngine.AY2008.VoorzieningenAppTypes.VoorzieningenN Entity { get; set; }
    }

  
    [DataContract]
    public class CriteriaVoorzieningenAppVoorzieningenHDataContract
    {
        // FileId
        [DataMember(Order=1)]
        public Guid Id { get; set; }

        [DataMember(Order=2)]
        public string Culture { get; set; }

        [DataMember(Order=3)]
        public string CollectionPath { get; set; }

        [DataMember(Order=4)]
        public EY.com.eBook.RuleEngine.AY2008.VoorzieningenAppTypes.VoorzieningenH Entity { get; set; }
    }

  
    [DataContract]
    public class CriteriaBelastingenAppAdjustmentDataContract
    {
        // FileId
        [DataMember(Order=1)]
        public Guid Id { get; set; }

        [DataMember(Order=2)]
        public string Culture { get; set; }

        [DataMember(Order=3)]
        public string CollectionPath { get; set; }

        [DataMember(Order=4)]
        public EY.com.eBook.RuleEngine.AY2008.BelastingenAppTypes.Adjustment Entity { get; set; }
    }

  
    [DataContract]
    public class CriteriaBelastingenAppBelastingDataContract
    {
        // FileId
        [DataMember(Order=1)]
        public Guid Id { get; set; }

        [DataMember(Order=2)]
        public string Culture { get; set; }

        [DataMember(Order=3)]
        public string CollectionPath { get; set; }

        [DataMember(Order=4)]
        public EY.com.eBook.RuleEngine.AY2008.BelastingenAppTypes.Belasting Entity { get; set; }
    }

  
    [DataContract]
    public class CriteriaBelastingenAppKostItemDataContract
    {
        // FileId
        [DataMember(Order=1)]
        public Guid Id { get; set; }

        [DataMember(Order=2)]
        public string Culture { get; set; }

        [DataMember(Order=3)]
        public string CollectionPath { get; set; }

        [DataMember(Order=4)]
        public EY.com.eBook.RuleEngine.AY2008.BelastingenAppTypes.KostItem Entity { get; set; }
    }

  
    [DataContract]
    public class CriteriaBelasteReservesAppBelasteReservesItemDataContract
    {
        // FileId
        [DataMember(Order=1)]
        public Guid Id { get; set; }

        [DataMember(Order=2)]
        public string Culture { get; set; }

        [DataMember(Order=3)]
        public string CollectionPath { get; set; }

        [DataMember(Order=4)]
        public EY.com.eBook.RuleEngine.AY2008.BelasteReservesAppTypes.BelasteReservesItem Entity { get; set; }
    }

  
    [DataContract]
    public class CriteriaBelasteReservesAppAanpassingBTReservesItemDataContract
    {
        // FileId
        [DataMember(Order=1)]
        public Guid Id { get; set; }

        [DataMember(Order=2)]
        public string Culture { get; set; }

        [DataMember(Order=3)]
        public string CollectionPath { get; set; }

        [DataMember(Order=4)]
        public EY.com.eBook.RuleEngine.AY2008.BelasteReservesAppTypes.AanpassingBTReservesItem Entity { get; set; }
    }

  
    [DataContract]
    public class CriteriaBelastingVrijeReservesAppDetailDataContract
    {
        // FileId
        [DataMember(Order=1)]
        public Guid Id { get; set; }

        [DataMember(Order=2)]
        public string Culture { get; set; }

        [DataMember(Order=3)]
        public string CollectionPath { get; set; }

        [DataMember(Order=4)]
        public EY.com.eBook.RuleEngine.AY2008.BelastingVrijeReservesAppTypes.Detail Entity { get; set; }
    }

  
    [DataContract]
    public class CriteriaDBIAppParticipatieItemDataContract
    {
        // FileId
        [DataMember(Order=1)]
        public Guid Id { get; set; }

        [DataMember(Order=2)]
        public string Culture { get; set; }

        [DataMember(Order=3)]
        public string CollectionPath { get; set; }

        [DataMember(Order=4)]
        public EY.com.eBook.RuleEngine.AY2008.DBIAppTypes.ParticipatieItem Entity { get; set; }
    }

  
    [DataContract]
    public class CriteriaDBIAppDividendItemDataContract
    {
        // FileId
        [DataMember(Order=1)]
        public Guid Id { get; set; }

        [DataMember(Order=2)]
        public string Culture { get; set; }

        [DataMember(Order=3)]
        public string CollectionPath { get; set; }

        [DataMember(Order=4)]
        public EY.com.eBook.RuleEngine.AY2008.DBIAppTypes.DividendItem Entity { get; set; }
    }

  
    [DataContract]
    public class CriteriaRisicoKapitaalAppFicheItemDataContract
    {
        // FileId
        [DataMember(Order=1)]
        public Guid Id { get; set; }

        [DataMember(Order=2)]
        public string Culture { get; set; }

        [DataMember(Order=3)]
        public string CollectionPath { get; set; }

        [DataMember(Order=4)]
        public EY.com.eBook.RuleEngine.AY2008.RisicoKapitaalAppTypes.FicheItem Entity { get; set; }
    }

  
    [DataContract]
    public class CriteriaTransferTaxCreditsAppFiscaalVerliesDataContract
    {
        // FileId
        [DataMember(Order=1)]
        public Guid Id { get; set; }

        [DataMember(Order=2)]
        public string Culture { get; set; }

        [DataMember(Order=3)]
        public string CollectionPath { get; set; }

        [DataMember(Order=4)]
        public EY.com.eBook.RuleEngine.AY2008.TransferTaxCreditsAppTypes.FiscaalVerlies Entity { get; set; }
    }

  
    [DataContract]
    public class CriteriaTransferTaxCreditsAppNIDItemDataContract
    {
        // FileId
        [DataMember(Order=1)]
        public Guid Id { get; set; }

        [DataMember(Order=2)]
        public string Culture { get; set; }

        [DataMember(Order=3)]
        public string CollectionPath { get; set; }

        [DataMember(Order=4)]
        public EY.com.eBook.RuleEngine.AY2008.TransferTaxCreditsAppTypes.NIDItem Entity { get; set; }
    }

  
    [DataContract]
    public class CriteriaTransferTaxCreditsAppDBIItemDataContract
    {
        // FileId
        [DataMember(Order=1)]
        public Guid Id { get; set; }

        [DataMember(Order=2)]
        public string Culture { get; set; }

        [DataMember(Order=3)]
        public string CollectionPath { get; set; }

        [DataMember(Order=4)]
        public EY.com.eBook.RuleEngine.AY2008.TransferTaxCreditsAppTypes.DBIItem Entity { get; set; }
    }

  
    [DataContract]
    public class CriteriaTransferTaxCreditsAppInvestItemDataContract
    {
        // FileId
        [DataMember(Order=1)]
        public Guid Id { get; set; }

        [DataMember(Order=2)]
        public string Culture { get; set; }

        [DataMember(Order=3)]
        public string CollectionPath { get; set; }

        [DataMember(Order=4)]
        public EY.com.eBook.RuleEngine.AY2008.TransferTaxCreditsAppTypes.InvestItem Entity { get; set; }
    }

  
    [DataContract]
    public class CriteriaTransferTaxCreditsAppBelastingkredietItemDataContract
    {
        // FileId
        [DataMember(Order=1)]
        public Guid Id { get; set; }

        [DataMember(Order=2)]
        public string Culture { get; set; }

        [DataMember(Order=3)]
        public string CollectionPath { get; set; }

        [DataMember(Order=4)]
        public EY.com.eBook.RuleEngine.AY2008.TransferTaxCreditsAppTypes.BelastingkredietItem Entity { get; set; }
    }

  
    [DataContract]
    public class CriteriaTransferTaxCreditsAppAanpassingItemDataContract
    {
        // FileId
        [DataMember(Order=1)]
        public Guid Id { get; set; }

        [DataMember(Order=2)]
        public string Culture { get; set; }

        [DataMember(Order=3)]
        public string CollectionPath { get; set; }

        [DataMember(Order=4)]
        public EY.com.eBook.RuleEngine.AY2008.TransferTaxCreditsAppTypes.AanpassingItem Entity { get; set; }
    }

  
    [DataContract]
    public class CriteriaBerekeningVenBAppGeneralDataContract
    {
        // FileId
        [DataMember(Order=1)]
        public Guid Id { get; set; }

        [DataMember(Order=2)]
        public string Culture { get; set; }

        [DataMember(Order=3)]
        public string CollectionPath { get; set; }

        [DataMember(Order=4)]
        public EY.com.eBook.RuleEngine.AY2008.BerekeningVenBAppTypes.General Entity { get; set; }
    }

  
    [DataContract]
    public class CriteriaBerekeningVenBAppBelasteReservesItemDataContract
    {
        // FileId
        [DataMember(Order=1)]
        public Guid Id { get; set; }

        [DataMember(Order=2)]
        public string Culture { get; set; }

        [DataMember(Order=3)]
        public string CollectionPath { get; set; }

        [DataMember(Order=4)]
        public EY.com.eBook.RuleEngine.AY2008.BerekeningVenBAppTypes.BelasteReservesItem Entity { get; set; }
    }

  
    [DataContract]
    public class CriteriaBerekeningVenBAppVerworpenUitgavenItemDataContract
    {
        // FileId
        [DataMember(Order=1)]
        public Guid Id { get; set; }

        [DataMember(Order=2)]
        public string Culture { get; set; }

        [DataMember(Order=3)]
        public string CollectionPath { get; set; }

        [DataMember(Order=4)]
        public EY.com.eBook.RuleEngine.AY2008.BerekeningVenBAppTypes.VerworpenUitgavenItem Entity { get; set; }
    }

  
    [DataContract]
    public class CriteriaBerekeningVenBAppBelastbareBestZonderAftrekItemDataContract
    {
        // FileId
        [DataMember(Order=1)]
        public Guid Id { get; set; }

        [DataMember(Order=2)]
        public string Culture { get; set; }

        [DataMember(Order=3)]
        public string CollectionPath { get; set; }

        [DataMember(Order=4)]
        public EY.com.eBook.RuleEngine.AY2008.BerekeningVenBAppTypes.BelastbareBestZonderAftrekItem Entity { get; set; }
    }

  
    [DataContract]
    public class CriteriaBerekeningVenBAppOpdelingNaarOorsprongItemDataContract
    {
        // FileId
        [DataMember(Order=1)]
        public Guid Id { get; set; }

        [DataMember(Order=2)]
        public string Culture { get; set; }

        [DataMember(Order=3)]
        public string CollectionPath { get; set; }

        [DataMember(Order=4)]
        public EY.com.eBook.RuleEngine.AY2008.BerekeningVenBAppTypes.OpdelingNaarOorsprongItem Entity { get; set; }
    }

  
    [DataContract]
    public class CriteriaBerekeningVenBAppFiscaleAftrekItemDataContract
    {
        // FileId
        [DataMember(Order=1)]
        public Guid Id { get; set; }

        [DataMember(Order=2)]
        public string Culture { get; set; }

        [DataMember(Order=3)]
        public string CollectionPath { get; set; }

        [DataMember(Order=4)]
        public EY.com.eBook.RuleEngine.AY2008.BerekeningVenBAppTypes.FiscaleAftrekItem Entity { get; set; }
    }

  
    [DataContract]
    public class CriteriaBerekeningVenBAppTariefItemDataContract
    {
        // FileId
        [DataMember(Order=1)]
        public Guid Id { get; set; }

        [DataMember(Order=2)]
        public string Culture { get; set; }

        [DataMember(Order=3)]
        public string CollectionPath { get; set; }

        [DataMember(Order=4)]
        public EY.com.eBook.RuleEngine.AY2008.BerekeningVenBAppTypes.TariefItem Entity { get; set; }
    }

  
    [DataContract]
    public class CriteriaBerekeningVenBAppBelastingsBerekeningItemDataContract
    {
        // FileId
        [DataMember(Order=1)]
        public Guid Id { get; set; }

        [DataMember(Order=2)]
        public string Culture { get; set; }

        [DataMember(Order=3)]
        public string CollectionPath { get; set; }

        [DataMember(Order=4)]
        public EY.com.eBook.RuleEngine.AY2008.BerekeningVenBAppTypes.BelastingsBerekeningItem Entity { get; set; }
    }

  
    [DataContract]
    public class CriteriaBerekeningVenBAppBelastingsBerekeningItem2DataContract
    {
        // FileId
        [DataMember(Order=1)]
        public Guid Id { get; set; }

        [DataMember(Order=2)]
        public string Culture { get; set; }

        [DataMember(Order=3)]
        public string CollectionPath { get; set; }

        [DataMember(Order=4)]
        public EY.com.eBook.RuleEngine.AY2008.BerekeningVenBAppTypes.BelastingsBerekeningItem2 Entity { get; set; }
    }

  
    [DataContract]
    public class CriteriaAangifteVenBAppReserveEntityDataContract
    {
        // FileId
        [DataMember(Order=1)]
        public Guid Id { get; set; }

        [DataMember(Order=2)]
        public string Culture { get; set; }

        [DataMember(Order=3)]
        public string CollectionPath { get; set; }

        [DataMember(Order=4)]
        public EY.com.eBook.RuleEngine.AY2008.AangifteVenBAppTypes.ReserveEntity Entity { get; set; }
    }

  
    [DataContract]
    public class CriteriaAangifteVenBAppVrijgesteldeReserveEntityDataContract
    {
        // FileId
        [DataMember(Order=1)]
        public Guid Id { get; set; }

        [DataMember(Order=2)]
        public string Culture { get; set; }

        [DataMember(Order=3)]
        public string CollectionPath { get; set; }

        [DataMember(Order=4)]
        public EY.com.eBook.RuleEngine.AY2008.AangifteVenBAppTypes.VrijgesteldeReserveEntity Entity { get; set; }
    }

  
    [DataContract]
    public class CriteriaAangifteVenBAppVerworpenUitgavenEntityDataContract
    {
        // FileId
        [DataMember(Order=1)]
        public Guid Id { get; set; }

        [DataMember(Order=2)]
        public string Culture { get; set; }

        [DataMember(Order=3)]
        public string CollectionPath { get; set; }

        [DataMember(Order=4)]
        public EY.com.eBook.RuleEngine.AY2008.AangifteVenBAppTypes.VerworpenUitgavenEntity Entity { get; set; }
    }

  
    [DataContract]
    public class CriteriaAangifteVenBAppUitgekeerdeDividendenEntityDataContract
    {
        // FileId
        [DataMember(Order=1)]
        public Guid Id { get; set; }

        [DataMember(Order=2)]
        public string Culture { get; set; }

        [DataMember(Order=3)]
        public string CollectionPath { get; set; }

        [DataMember(Order=4)]
        public EY.com.eBook.RuleEngine.AY2008.AangifteVenBAppTypes.UitgekeerdeDividendenEntity Entity { get; set; }
    }

  
    [DataContract]
    public class CriteriaAangifteVenBAppWinstEntityDataContract
    {
        // FileId
        [DataMember(Order=1)]
        public Guid Id { get; set; }

        [DataMember(Order=2)]
        public string Culture { get; set; }

        [DataMember(Order=3)]
        public string CollectionPath { get; set; }

        [DataMember(Order=4)]
        public EY.com.eBook.RuleEngine.AY2008.AangifteVenBAppTypes.WinstEntity Entity { get; set; }
    }

  
    [DataContract]
    public class CriteriaAangifteVenBAppAfzonderlijkeAanslagenEntityDataContract
    {
        // FileId
        [DataMember(Order=1)]
        public Guid Id { get; set; }

        [DataMember(Order=2)]
        public string Culture { get; set; }

        [DataMember(Order=3)]
        public string CollectionPath { get; set; }

        [DataMember(Order=4)]
        public EY.com.eBook.RuleEngine.AY2008.AangifteVenBAppTypes.AfzonderlijkeAanslagenEntity Entity { get; set; }
    }

  
    [DataContract]
    public class CriteriaAangifteVenBAppDiamantEntityDataContract
    {
        // FileId
        [DataMember(Order=1)]
        public Guid Id { get; set; }

        [DataMember(Order=2)]
        public string Culture { get; set; }

        [DataMember(Order=3)]
        public string CollectionPath { get; set; }

        [DataMember(Order=4)]
        public EY.com.eBook.RuleEngine.AY2008.AangifteVenBAppTypes.DiamantEntity Entity { get; set; }
    }

  
    [DataContract]
    public class CriteriaAangifteVenBAppDbiEntityDataContract
    {
        // FileId
        [DataMember(Order=1)]
        public Guid Id { get; set; }

        [DataMember(Order=2)]
        public string Culture { get; set; }

        [DataMember(Order=3)]
        public string CollectionPath { get; set; }

        [DataMember(Order=4)]
        public EY.com.eBook.RuleEngine.AY2008.AangifteVenBAppTypes.DbiEntity Entity { get; set; }
    }

  
    [DataContract]
    public class CriteriaAangifteVenBAppOverdrachtDBIAftrekEntityDataContract
    {
        // FileId
        [DataMember(Order=1)]
        public Guid Id { get; set; }

        [DataMember(Order=2)]
        public string Culture { get; set; }

        [DataMember(Order=3)]
        public string CollectionPath { get; set; }

        [DataMember(Order=4)]
        public EY.com.eBook.RuleEngine.AY2008.AangifteVenBAppTypes.OverdrachtDBIAftrekEntity Entity { get; set; }
    }

  
    [DataContract]
    public class CriteriaAangifteVenBAppOverdrachtNIDEntityDataContract
    {
        // FileId
        [DataMember(Order=1)]
        public Guid Id { get; set; }

        [DataMember(Order=2)]
        public string Culture { get; set; }

        [DataMember(Order=3)]
        public string CollectionPath { get; set; }

        [DataMember(Order=4)]
        public EY.com.eBook.RuleEngine.AY2008.AangifteVenBAppTypes.OverdrachtNIDEntity Entity { get; set; }
    }

  
    [DataContract]
    public class CriteriaAangifteVenBAppCompenseerbareVerliezenEntityDataContract
    {
        // FileId
        [DataMember(Order=1)]
        public Guid Id { get; set; }

        [DataMember(Order=2)]
        public string Culture { get; set; }

        [DataMember(Order=3)]
        public string CollectionPath { get; set; }

        [DataMember(Order=4)]
        public EY.com.eBook.RuleEngine.AY2008.AangifteVenBAppTypes.CompenseerbareVerliezenEntity Entity { get; set; }
    }

  
    [DataContract]
    public class CriteriaAangifteVenBAppTariefEntityDataContract
    {
        // FileId
        [DataMember(Order=1)]
        public Guid Id { get; set; }

        [DataMember(Order=2)]
        public string Culture { get; set; }

        [DataMember(Order=3)]
        public string CollectionPath { get; set; }

        [DataMember(Order=4)]
        public EY.com.eBook.RuleEngine.AY2008.AangifteVenBAppTypes.TariefEntity Entity { get; set; }
    }

  
    [DataContract]
    public class CriteriaAangifteVenBAppVoorafBetalingEntityDataContract
    {
        // FileId
        [DataMember(Order=1)]
        public Guid Id { get; set; }

        [DataMember(Order=2)]
        public string Culture { get; set; }

        [DataMember(Order=3)]
        public string CollectionPath { get; set; }

        [DataMember(Order=4)]
        public EY.com.eBook.RuleEngine.AY2008.AangifteVenBAppTypes.VoorafBetalingEntity Entity { get; set; }
    }

  
    [DataContract]
    public class CriteriaAangifteVenBAppVerrekenbareVoorheffingEntityDataContract
    {
        // FileId
        [DataMember(Order=1)]
        public Guid Id { get; set; }

        [DataMember(Order=2)]
        public string Culture { get; set; }

        [DataMember(Order=3)]
        public string CollectionPath { get; set; }

        [DataMember(Order=4)]
        public EY.com.eBook.RuleEngine.AY2008.AangifteVenBAppTypes.VerrekenbareVoorheffingEntity Entity { get; set; }
    }

  
    [DataContract]
    public class CriteriaAangifteVenBAppTaxShelterEntityDataContract
    {
        // FileId
        [DataMember(Order=1)]
        public Guid Id { get; set; }

        [DataMember(Order=2)]
        public string Culture { get; set; }

        [DataMember(Order=3)]
        public string CollectionPath { get; set; }

        [DataMember(Order=4)]
        public EY.com.eBook.RuleEngine.AY2008.AangifteVenBAppTypes.TaxShelterEntity Entity { get; set; }
    }

  
    [DataContract]
    public class CriteriaAangifteVenBAppGrootteVennootschapeEntityDataContract
    {
        // FileId
        [DataMember(Order=1)]
        public Guid Id { get; set; }

        [DataMember(Order=2)]
        public string Culture { get; set; }

        [DataMember(Order=3)]
        public string CollectionPath { get; set; }

        [DataMember(Order=4)]
        public EY.com.eBook.RuleEngine.AY2008.AangifteVenBAppTypes.GrootteVennootschapeEntity Entity { get; set; }
    }

  
    [DataContract]
    public class CriteriaAangifteVenBAppOpgavenEntityDataContract
    {
        // FileId
        [DataMember(Order=1)]
        public Guid Id { get; set; }

        [DataMember(Order=2)]
        public string Culture { get; set; }

        [DataMember(Order=3)]
        public string CollectionPath { get; set; }

        [DataMember(Order=4)]
        public EY.com.eBook.RuleEngine.AY2008.AangifteVenBAppTypes.OpgavenEntity Entity { get; set; }
    }

  
    [DataContract]
    public class CriteriaAangifteVenBAppBankEntityDataContract
    {
        // FileId
        [DataMember(Order=1)]
        public Guid Id { get; set; }

        [DataMember(Order=2)]
        public string Culture { get; set; }

        [DataMember(Order=3)]
        public string CollectionPath { get; set; }

        [DataMember(Order=4)]
        public EY.com.eBook.RuleEngine.AY2008.AangifteVenBAppTypes.BankEntity Entity { get; set; }
    }

  
}
  