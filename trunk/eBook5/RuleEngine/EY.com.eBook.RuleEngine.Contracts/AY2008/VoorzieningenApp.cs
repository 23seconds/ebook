﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.RuleEngine.Core;
using EY.com.eBook.RuleEngine.Core.Interfaces;
using EY.com.eBook.RuleEngine.Data;

    
namespace EY.com.eBook.RuleEngine.AY2008.VoorzieningenAppTypes
{
    #region subtypes


      [DataContract(Namespace = "AY2008.VoorzieningenApp")]
      public class VoorzieningenT : IEntity
      {
          #region IEntity
          
          [DataMember(Name = "ID", Order=1)]
          public Guid Id { get; set; }
          
          [DataMember(Order=2)]
          public DateTime? LastUpdated { get; set; }
          
          [DataMember(Order = 3)]
          public bool IsValid { get; set; }
          
          [DataMember(Order = 4)]
          public bool PreviousImported { get; set; }
          
          #endregion
          
          #region entitymembers
          

        
          [DataMember(Order = 5)]
          public string OmschrijvingProvisie {get;set;}


        
          [DataMember(Order = 6)]
          public decimal? WaarschijnlijkeKosten {get;set;}


        
          [DataMember(Order = 7)]
          public decimal? VoorzieningBeginBj {get;set;}


        
          [DataMember(Order = 8)]
          public decimal? VerminderingTijdensBJ {get;set;}


        
          [DataMember(Order = 9)]
          public string RedenTotVermindering {get;set;}


        
          [DataMember(Order = 10)]
          public decimal? VerhogingTijdensBJ {get;set;}


        
          [DataMember(Order = 11)]
          public decimal? VoorzieningEindeBJ {get;set;}


        
          [DataMember(Order = 12)]
          public string Belastbaar {get;set;}


          #endregion
      }

  
      [DataContract(Namespace = "AY2008.VoorzieningenApp")]
      public class VoorzieningenW : IEntity
      {
          #region IEntity
          
          [DataMember(Name = "ID", Order=1)]
          public Guid Id { get; set; }
          
          [DataMember(Order=2)]
          public DateTime? LastUpdated { get; set; }
          
          [DataMember(Order = 3)]
          public bool IsValid { get; set; }
          
          [DataMember(Order = 4)]
          public bool PreviousImported { get; set; }
          
          #endregion
          
          #region entitymembers
          

        
          [DataMember(Order = 5)]
          public string Bestaande {get;set;}


        
          [DataMember(Order = 6)]
          public string OmschrijvingProvisie {get;set;}


        
          [DataMember(Order = 7)]
          public decimal? WaarschijnlijkeKosten {get;set;}


        
          [DataMember(Order = 8)]
          public decimal? VoorzieningBeginBj {get;set;}


        
          [DataMember(Order = 9)]
          public decimal? VerminderingTijdensBJ {get;set;}


        
          [DataMember(Order = 10)]
          public string RedenTotVermindering {get;set;}


        
          [DataMember(Order = 11)]
          public decimal? VerhogingTijdensBJ {get;set;}


        
          [DataMember(Order = 12)]
          public decimal? VoorzieningEindeBJ {get;set;}


        
          [DataMember(Order = 13)]
          public string Belastbaar {get;set;}


        
          [DataMember(Order = 14)]
          public bool? Book {get;set;}


          #endregion
      }

  
      [DataContract(Namespace = "AY2008.VoorzieningenApp")]
      public class VoorzieningenN : IEntity
      {
          #region IEntity
          
          [DataMember(Name = "ID", Order=1)]
          public Guid Id { get; set; }
          
          [DataMember(Order=2)]
          public DateTime? LastUpdated { get; set; }
          
          [DataMember(Order = 3)]
          public bool IsValid { get; set; }
          
          [DataMember(Order = 4)]
          public bool PreviousImported { get; set; }
          
          #endregion
          
          #region entitymembers
          

        
          [DataMember(Order = 5)]
          public string OmschrijvingProvisie {get;set;}


        
          [DataMember(Order = 6)]
          public decimal? WaarschijnlijkeKosten {get;set;}


        
          [DataMember(Order = 7)]
          public decimal? VoorzieningsBedrag {get;set;}


        
          [DataMember(Order = 8)]
          public string Belastbaar {get;set;}


        
          [DataMember(Order = 9)]
          public bool? Book {get;set;}


          #endregion
      }

  
      [DataContract(Namespace = "AY2008.VoorzieningenApp")]
      public class VoorzieningenH : IEntity
      {
          #region IEntity
          
          [DataMember(Name = "ID", Order=1)]
          public Guid Id { get; set; }
          
          [DataMember(Order=2)]
          public DateTime? LastUpdated { get; set; }
          
          [DataMember(Order = 3)]
          public bool IsValid { get; set; }
          
          [DataMember(Order = 4)]
          public bool PreviousImported { get; set; }
          
          #endregion
          
          #region entitymembers
          

        
          [DataMember(Order = 5)]
          public string OmschrijvingProvisie {get;set;}


        
          [DataMember(Order = 6)]
          public decimal? WaarschijnlijkeKosten {get;set;}


        
          [DataMember(Order = 7)]
          public decimal? VoorzieningBeginBj {get;set;}


        
          [DataMember(Order = 8)]
          public decimal? VerminderingTijdensBJ {get;set;}


        
          [DataMember(Order = 9)]
          public string RedenTotVermindering {get;set;}


        
          [DataMember(Order = 10)]
          public decimal? VerhogingTijdensBJ {get;set;}


        
          [DataMember(Order = 11)]
          public decimal? VoorzieningEindeBJ {get;set;}


        
          [DataMember(Order = 12)]
          public string Belastbaar {get;set;}


        
          [DataMember(Order = 13)]
          public bool? isNew {get;set;}


          #endregion
      }

  

    #endregion

    #region roottype
    

      [DataContract(Namespace = "AY2008.VoorzieningenApp")]
      public class Voorzieningen : IWorksheet
      {
          public Voorzieningen() {
              // Set Minimal

        
              this.ToestandEindVorigBoekjaar = new List<EY.com.eBook.RuleEngine.AY2008.VoorzieningenAppTypes.VoorzieningenT>();
              
        
              this.WijzigingBestaandeTijdensBoekjaar = new List<EY.com.eBook.RuleEngine.AY2008.VoorzieningenAppTypes.VoorzieningenW>();
              
        
              this.NieuweTijdensBoekjaar = new List<EY.com.eBook.RuleEngine.AY2008.VoorzieningenAppTypes.VoorzieningenN>();
              
        
              this.ToestandEindHuidigBoekjaar = new List<EY.com.eBook.RuleEngine.AY2008.VoorzieningenAppTypes.VoorzieningenH>();
              
              this.LastUpdated = DateTime.Now;
          }
        
          
          #region entitymembers
          

        
          [DataMember(Order = 1)]
          public List<EY.com.eBook.RuleEngine.AY2008.VoorzieningenAppTypes.VoorzieningenT> ToestandEindVorigBoekjaar {get;set;}


        
          [DataMember(Order = 2)]
          public List<EY.com.eBook.RuleEngine.AY2008.VoorzieningenAppTypes.VoorzieningenW> WijzigingBestaandeTijdensBoekjaar {get;set;}


        
          [DataMember(Order = 3)]
          public List<EY.com.eBook.RuleEngine.AY2008.VoorzieningenAppTypes.VoorzieningenN> NieuweTijdensBoekjaar {get;set;}


        
          [DataMember(Order = 4)]
          public List<EY.com.eBook.RuleEngine.AY2008.VoorzieningenAppTypes.VoorzieningenH> ToestandEindHuidigBoekjaar {get;set;}


      
          #endregion
          
          #region IWorksheet Members
          [DataMember(Order = 5)]
          public DateTime? LastUpdated { get; set; }

          #endregion
      }

  
   
    #endregion

}
  