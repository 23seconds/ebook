﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.RuleEngine.Core;
using EY.com.eBook.RuleEngine.Core.Interfaces;
using EY.com.eBook.RuleEngine.Data;

    
namespace EY.com.eBook.RuleEngine.AY2008.RisicoKapitaalAppTypes
{
    #region subtypes


      [DataContract(Namespace = "AY2008.RisicoKapitaalApp")]
      public class FicheItem : IEntity
      {
          #region IEntity
          
          [DataMember(Name = "ID", Order=1)]
          public Guid Id { get; set; }
          
          [DataMember(Order=2)]
          public DateTime? LastUpdated { get; set; }
          
          [DataMember(Order = 3)]
          public bool IsValid { get; set; }
          
          [DataMember(Order = 4)]
          public bool PreviousImported { get; set; }
          
          #endregion
          
          #region entitymembers
          

        
          [DataMember(Order = 5)]
          public decimal? EigenVermogen {get;set;}


        
          [DataMember(Order = 6)]
          public decimal? EigenAandelen {get;set;}


        
          [DataMember(Order = 7)]
          public decimal? FinancVasteActiva {get;set;}


        
          [DataMember(Order = 8)]
          public decimal? AandelenBelegging {get;set;}


        
          [DataMember(Order = 9)]
          public decimal? InrichtingVerdrag {get;set;}


        
          [DataMember(Order = 10)]
          public decimal? OnroerendVerdrag {get;set;}


        
          [DataMember(Order = 11)]
          public decimal? MvaOnredelijk {get;set;}


        
          [DataMember(Order = 12)]
          public decimal? BestanddelenBelegging {get;set;}


        
          [DataMember(Order = 13)]
          public decimal? OnroerendeBedrijfsleiders {get;set;}


        
          [DataMember(Order = 14)]
          public decimal? UitgedrukteMeerwaarden {get;set;}


        
          [DataMember(Order = 15)]
          public decimal? Belastingkrediet {get;set;}


        
          [DataMember(Order = 16)]
          public decimal? KapitaalSubsidies {get;set;}


        
          [DataMember(Order = 17)]
          public decimal? Voorraadactualisering {get;set;}


        
          [DataMember(Order = 18)]
          public decimal? Totaal {get;set;}


        
          [DataMember(Order = 19)]
          public decimal? TotaalOverrule {get;set;}


        
          [DataMember(Order = 20)]
          public decimal? PositieveWijzigingen {get;set;}


        
          [DataMember(Order = 21)]
          public decimal? NegatieveWijzigingen {get;set;}


        
          [DataMember(Order = 22)]
          public decimal? RisicoKapitaalBelastbaar {get;set;}


        
          [DataMember(Order = 23)]
          public decimal? RisicoKapitaalBelastbaarOverrule {get;set;}


        
          [DataMember(Order = 24)]
          public bool? TariefKleineVennootschap {get;set;}


        
          [DataMember(Order = 25)]
          public decimal? NotioneelPercentage {get;set;}


        
          [DataMember(Order = 26)]
          public decimal? Aftrekbaar {get;set;}


        
          [DataMember(Order = 27)]
          public decimal? AftrekbaarOverrule {get;set;}


        
          [DataMember(Order = 28)]
          public decimal? WerkelijkAfgetrokken {get;set;}


        
          [DataMember(Order = 29)]
          public decimal? OverdraagbaarSaldo {get;set;}


        
          [DataMember(Order = 30)]
          public decimal? OverdraagbaarSaldoOverrule {get;set;}


        
          [DataMember(Order = 31)]
          public decimal? SaldoVorigeAJGevormd {get;set;}


        
          [DataMember(Order = 32)]
          public decimal? WerkelijkVorigAJ {get;set;}


        
          [DataMember(Order = 33)]
          public decimal? TotaalAftrekAJ {get;set;}


        
          [DataMember(Order = 34)]
          public decimal? TotaalAftrekAJOverrule {get;set;}


        
          [DataMember(Order = 35)]
          public decimal? SaldoRisicoOverdraagbaarForm {get;set;}


        
          [DataMember(Order = 36)]
          public decimal? SaldoRisicoOverdraagbaarOverrule {get;set;}


        
          [DataMember(Order = 37)]
          public decimal? SaldoRisicoOverdraagbaar {get;set;}


          #endregion
      }

  

    #endregion

    #region roottype
    

      [DataContract(Namespace = "AY2008.RisicoKapitaalApp")]
      public class WPRisicokapitaal : IWorksheet
      {
          public WPRisicokapitaal() {
              // Set Minimal

        
              this.Fiche = new EY.com.eBook.RuleEngine.AY2008.RisicoKapitaalAppTypes.FicheItem();
              this.Fiche.Id = Guid.NewGuid();
              this.LastUpdated = DateTime.Now;
          }
        
          
          #region entitymembers
          

        
          [DataMember(Order = 1)]
          public EY.com.eBook.RuleEngine.AY2008.RisicoKapitaalAppTypes.FicheItem Fiche {get;set;}


      
          #endregion
          
          #region IWorksheet Members
          [DataMember(Order = 2)]
          public DateTime? LastUpdated { get; set; }

          #endregion
      }

  
   
    #endregion

}
  