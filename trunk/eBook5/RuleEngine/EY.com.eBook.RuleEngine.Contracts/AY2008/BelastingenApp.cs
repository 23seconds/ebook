﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.RuleEngine.Core;
using EY.com.eBook.RuleEngine.Core.Interfaces;
using EY.com.eBook.RuleEngine.Data;

    
namespace EY.com.eBook.RuleEngine.AY2008.BelastingenAppTypes
{
    #region subtypes


      [DataContract(Namespace = "AY2008.BelastingenApp")]
      public class Adjustment : IEntity
      {
          #region IEntity
          
          [DataMember(Name = "ID", Order=1)]
          public Guid Id { get; set; }
          
          [DataMember(Order=2)]
          public DateTime? LastUpdated { get; set; }
          
          [DataMember(Order = 3)]
          public bool IsValid { get; set; }
          
          [DataMember(Order = 4)]
          public bool PreviousImported { get; set; }
          
          #endregion
          
          #region entitymembers
          

        
          [DataMember(Order = 5)]
          public bool? NotInLedger {get;set;}


        
          [DataMember(Order = 6)]
          public StringListItemDataContract Rekening {get;set;}


        
          [DataMember(Order = 7)]
          public string Name {get;set;}


        
          [DataMember(Order = 8)]
          public bool? GeenVerworpenUitgave {get;set;}


        
          [DataMember(Order = 9)]
          public StringListItemDataContract SoortAdjust {get;set;}


        
          [DataMember(Order = 10)]
          public decimal? Bedrag {get;set;}


        
          [DataMember(Order = 11)]
          public decimal? Percent {get;set;}


        
          [DataMember(Order = 12)]
          public decimal? VerworpenUitgaven {get;set;}


          #endregion
      }

  
      [DataContract(Namespace = "AY2008.BelastingenApp")]
      public class Belasting : IEntity
      {
          #region IEntity
          
          [DataMember(Name = "ID", Order=1)]
          public Guid Id { get; set; }
          
          [DataMember(Order=2)]
          public DateTime? LastUpdated { get; set; }
          
          [DataMember(Order = 3)]
          public bool IsValid { get; set; }
          
          [DataMember(Order = 4)]
          public bool PreviousImported { get; set; }
          
          #endregion
          
          #region entitymembers
          

        
          [DataMember(Order = 5)]
          public StringListItemDataContract Aanslagjaar {get;set;}


        
          [DataMember(Order = 6)]
          public string ListDescription {get;set;}


        
          [DataMember(Order = 7)]
          public string GrootBoek {get;set;}


        
          [DataMember(Order = 8)]
          public decimal? Saldo {get;set;}


        
          [DataMember(Order = 9)]
          public string Tekst {get;set;}


        
          [DataMember(Order = 10)]
          public string Artikel {get;set;}


        
          [DataMember(Order = 11)]
          public string Betaling {get;set;}


        
          [DataMember(Order = 12)]
          public int? Soort {get;set;}


        
          [DataMember(Order = 13)]
          public decimal? Bedrag {get;set;}


        
          [DataMember(Order = 14)]
          public decimal? VA1 {get;set;}


        
          [DataMember(Order = 15)]
          public decimal? VA2 {get;set;}


        
          [DataMember(Order = 16)]
          public decimal? VA3 {get;set;}


        
          [DataMember(Order = 17)]
          public decimal? VA4 {get;set;}


        
          [DataMember(Order = 18)]
          public bool? AutoVenB {get;set;}


        
          [DataMember(Order = 19)]
          public decimal? Percent {get;set;}


        
          [DataMember(Order = 20)]
          public string UNID {get;set;}


        
          [DataMember(Order = 21)]
          public bool? Delete {get;set;}


        
          [DataMember(Order = 22)]
          public string SoortID {get;set;}


          #endregion
      }

  
      [DataContract(Namespace = "AY2008.BelastingenApp")]
      public class KostItem : IEntity
      {
          #region IEntity
          
          [DataMember(Name = "ID", Order=1)]
          public Guid Id { get; set; }
          
          [DataMember(Order=2)]
          public DateTime? LastUpdated { get; set; }
          
          [DataMember(Order = 3)]
          public bool IsValid { get; set; }
          
          [DataMember(Order = 4)]
          public bool PreviousImported { get; set; }
          
          #endregion
          
          #region entitymembers
          

        
          [DataMember(Order = 5)]
          public string ListDescription {get;set;}


        
          [DataMember(Order = 6)]
          public string GrootBoek {get;set;}


        
          [DataMember(Order = 7)]
          public decimal? Saldo {get;set;}


        
          [DataMember(Order = 8)]
          public decimal? Percent {get;set;}


        
          [DataMember(Order = 9)]
          public decimal? Bedrag {get;set;}


        
          [DataMember(Order = 10)]
          public string UNID {get;set;}


        
          [DataMember(Order = 11)]
          public string SoortID {get;set;}


        
          [DataMember(Order = 12)]
          public bool? delete {get;set;}


        
          [DataMember(Order = 13)]
          public bool? excludeRegulate {get;set;}


          #endregion
      }

  

    #endregion

    #region roottype
    

      [DataContract(Namespace = "AY2008.BelastingenApp")]
      public class WPBelastingen : IWorksheet
      {
          public WPBelastingen() {
              // Set Minimal

        
              this.Adjustments = new List<EY.com.eBook.RuleEngine.AY2008.BelastingenAppTypes.Adjustment>();
              
        
              this.Belastingen = new List<EY.com.eBook.RuleEngine.AY2008.BelastingenAppTypes.Belasting>();
              
        
              this.GewestBelasting = new List<EY.com.eBook.RuleEngine.AY2008.BelastingenAppTypes.KostItem>();
              
        
              this.Boetes = new List<EY.com.eBook.RuleEngine.AY2008.BelastingenAppTypes.KostItem>();
              
        
              this.Pensioenen = new List<EY.com.eBook.RuleEngine.AY2008.BelastingenAppTypes.KostItem>();
              
        
              this.Auto = new List<EY.com.eBook.RuleEngine.AY2008.BelastingenAppTypes.KostItem>();
              
        
              this.Receptie = new List<EY.com.eBook.RuleEngine.AY2008.BelastingenAppTypes.KostItem>();
              
        
              this.Restaurant = new List<EY.com.eBook.RuleEngine.AY2008.BelastingenAppTypes.KostItem>();
              
        
              this.Kledij = new List<EY.com.eBook.RuleEngine.AY2008.BelastingenAppTypes.KostItem>();
              
        
              this.OverdrevenIntresten = new List<EY.com.eBook.RuleEngine.AY2008.BelastingenAppTypes.KostItem>();
              
        
              this.IntrestenLeningen = new List<EY.com.eBook.RuleEngine.AY2008.BelastingenAppTypes.KostItem>();
              
        
              this.AbnormaleGoedgunstigeVoordelen = new List<EY.com.eBook.RuleEngine.AY2008.BelastingenAppTypes.KostItem>();
              
        
              this.SocialeVoordelen = new List<EY.com.eBook.RuleEngine.AY2008.BelastingenAppTypes.KostItem>();
              
        
              this.Liberaliteiten = new List<EY.com.eBook.RuleEngine.AY2008.BelastingenAppTypes.KostItem>();
              
        
              this.WaardeverminderingAandelen = new List<EY.com.eBook.RuleEngine.AY2008.BelastingenAppTypes.KostItem>();
              
        
              this.TerugnemingVroegereVrijstellingen = new List<EY.com.eBook.RuleEngine.AY2008.BelastingenAppTypes.KostItem>();
              
        
              this.Werknemersparticipatie = new List<EY.com.eBook.RuleEngine.AY2008.BelastingenAppTypes.KostItem>();
              
        
              this.VergoedingCoupon = new List<EY.com.eBook.RuleEngine.AY2008.BelastingenAppTypes.KostItem>();
              
        
              this.KostenTaxShelter = new List<EY.com.eBook.RuleEngine.AY2008.BelastingenAppTypes.KostItem>();
              
        
              this.GewestPremies = new List<EY.com.eBook.RuleEngine.AY2008.BelastingenAppTypes.KostItem>();
              
        
              this.Andere = new List<EY.com.eBook.RuleEngine.AY2008.BelastingenAppTypes.KostItem>();
              
              this.LastUpdated = DateTime.Now;
          }
        
          
          #region entitymembers
          

        
          [DataMember(Order = 1)]
          public List<EY.com.eBook.RuleEngine.AY2008.BelastingenAppTypes.Adjustment> Adjustments {get;set;}


        
          [DataMember(Order = 2)]
          public List<EY.com.eBook.RuleEngine.AY2008.BelastingenAppTypes.Belasting> Belastingen {get;set;}


        
          [DataMember(Order = 3)]
          public List<EY.com.eBook.RuleEngine.AY2008.BelastingenAppTypes.KostItem> GewestBelasting {get;set;}


        
          [DataMember(Order = 4)]
          public List<EY.com.eBook.RuleEngine.AY2008.BelastingenAppTypes.KostItem> Boetes {get;set;}


        
          [DataMember(Order = 5)]
          public List<EY.com.eBook.RuleEngine.AY2008.BelastingenAppTypes.KostItem> Pensioenen {get;set;}


        
          [DataMember(Order = 6)]
          public List<EY.com.eBook.RuleEngine.AY2008.BelastingenAppTypes.KostItem> Auto {get;set;}


        
          [DataMember(Order = 7)]
          public List<EY.com.eBook.RuleEngine.AY2008.BelastingenAppTypes.KostItem> Receptie {get;set;}


        
          [DataMember(Order = 8)]
          public List<EY.com.eBook.RuleEngine.AY2008.BelastingenAppTypes.KostItem> Restaurant {get;set;}


        
          [DataMember(Order = 9)]
          public List<EY.com.eBook.RuleEngine.AY2008.BelastingenAppTypes.KostItem> Kledij {get;set;}


        
          [DataMember(Order = 10)]
          public List<EY.com.eBook.RuleEngine.AY2008.BelastingenAppTypes.KostItem> OverdrevenIntresten {get;set;}


        
          [DataMember(Order = 11)]
          public List<EY.com.eBook.RuleEngine.AY2008.BelastingenAppTypes.KostItem> IntrestenLeningen {get;set;}


        
          [DataMember(Order = 12)]
          public List<EY.com.eBook.RuleEngine.AY2008.BelastingenAppTypes.KostItem> AbnormaleGoedgunstigeVoordelen {get;set;}


        
          [DataMember(Order = 13)]
          public List<EY.com.eBook.RuleEngine.AY2008.BelastingenAppTypes.KostItem> SocialeVoordelen {get;set;}


        
          [DataMember(Order = 14)]
          public List<EY.com.eBook.RuleEngine.AY2008.BelastingenAppTypes.KostItem> Liberaliteiten {get;set;}


        
          [DataMember(Order = 15)]
          public List<EY.com.eBook.RuleEngine.AY2008.BelastingenAppTypes.KostItem> WaardeverminderingAandelen {get;set;}


        
          [DataMember(Order = 16)]
          public List<EY.com.eBook.RuleEngine.AY2008.BelastingenAppTypes.KostItem> TerugnemingVroegereVrijstellingen {get;set;}


        
          [DataMember(Order = 17)]
          public List<EY.com.eBook.RuleEngine.AY2008.BelastingenAppTypes.KostItem> Werknemersparticipatie {get;set;}


        
          [DataMember(Order = 18)]
          public List<EY.com.eBook.RuleEngine.AY2008.BelastingenAppTypes.KostItem> VergoedingCoupon {get;set;}


        
          [DataMember(Order = 19)]
          public List<EY.com.eBook.RuleEngine.AY2008.BelastingenAppTypes.KostItem> KostenTaxShelter {get;set;}


        
          [DataMember(Order = 20)]
          public List<EY.com.eBook.RuleEngine.AY2008.BelastingenAppTypes.KostItem> GewestPremies {get;set;}


        
          [DataMember(Order = 21)]
          public List<EY.com.eBook.RuleEngine.AY2008.BelastingenAppTypes.KostItem> Andere {get;set;}


        
          [DataMember(Order = 22)]
          public int? Client {get;set;}


        
          [DataMember(Order = 23)]
          public string File {get;set;}


        
          [DataMember(Order = 24)]
          public string WorksheetID {get;set;}


        
          [DataMember(Order = 25)]
          public DateTime? EindeBoekjaar {get;set;}


        
          [DataMember(Order = 26)]
          public bool? NietBTWPlichtig {get;set;}


        
          [DataMember(Order = 27)]
          public int? JaarMax {get;set;}


        
          [DataMember(Order = 28)]
          public string SessionID {get;set;}


        
          [DataMember(Order = 29)]
          public string Language {get;set;}


        
          [DataMember(Order = 30)]
          public bool? ReportReady {get;set;}


      
          #endregion
          
          #region IWorksheet Members
          [DataMember(Order = 31)]
          public DateTime? LastUpdated { get; set; }

          #endregion
      }

  
   
    #endregion

}
  