﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.RuleEngine.Core;
using EY.com.eBook.RuleEngine.Core.Interfaces;
using EY.com.eBook.RuleEngine.Data;

    
namespace EY.com.eBook.RuleEngine.AY2008.TransferTaxCreditsAppTypes
{
    #region subtypes


      [DataContract(Namespace = "AY2008.TransferTaxCreditsApp")]
      public class FiscaalVerlies : IEntity
      {
          #region IEntity
          
          [DataMember(Name = "ID", Order=1)]
          public Guid Id { get; set; }
          
          [DataMember(Order=2)]
          public DateTime? LastUpdated { get; set; }
          
          [DataMember(Order = 3)]
          public bool IsValid { get; set; }
          
          [DataMember(Order = 4)]
          public bool PreviousImported { get; set; }
          
          #endregion
          
          #region entitymembers
          

        
          [DataMember(Order = 5)]
          public int? Order {get;set;}


        
          [DataMember(Order = 6)]
          public string Type {get;set;}


        
          [DataMember(Order = 7)]
          public string Omschrijving {get;set;}


        
          [DataMember(Order = 8)]
          public DateTime? Date {get;set;}


        
          [DataMember(Order = 9)]
          public decimal? BedragHuidig {get;set;}


        
          [DataMember(Order = 10)]
          public decimal? Cumul {get;set;}


        
          [DataMember(Order = 11)]
          public bool? Disabled {get;set;}


          #endregion
      }

  
      [DataContract(Namespace = "AY2008.TransferTaxCreditsApp")]
      public class NIDItem : IEntity
      {
          #region IEntity
          
          [DataMember(Name = "ID", Order=1)]
          public Guid Id { get; set; }
          
          [DataMember(Order=2)]
          public DateTime? LastUpdated { get; set; }
          
          [DataMember(Order = 3)]
          public bool IsValid { get; set; }
          
          [DataMember(Order = 4)]
          public bool PreviousImported { get; set; }
          
          #endregion
          
          #region entitymembers
          

        
          [DataMember(Order = 5)]
          public int? Aanslagjaar {get;set;}


        
          [DataMember(Order = 6)]
          public decimal? BedragBeginBJ {get;set;}


        
          [DataMember(Order = 7)]
          public decimal? Aanwending {get;set;}


        
          [DataMember(Order = 8)]
          public decimal? BedragEindeBJ {get;set;}


          #endregion
      }

  
      [DataContract(Namespace = "AY2008.TransferTaxCreditsApp")]
      public class DBIItem : IEntity
      {
          #region IEntity
          
          [DataMember(Name = "ID", Order=1)]
          public Guid Id { get; set; }
          
          [DataMember(Order=2)]
          public DateTime? LastUpdated { get; set; }
          
          [DataMember(Order = 3)]
          public bool IsValid { get; set; }
          
          [DataMember(Order = 4)]
          public bool PreviousImported { get; set; }
          
          #endregion
          
          #region entitymembers
          

        
          [DataMember(Order = 5)]
          public decimal? SaldoStart {get;set;}


        
          [DataMember(Order = 6)]
          public decimal? GebruiktBJ {get;set;}


        
          [DataMember(Order = 7)]
          public decimal? SaldoEinde {get;set;}


          #endregion
      }

  
      [DataContract(Namespace = "AY2008.TransferTaxCreditsApp")]
      public class InvestItem : IEntity
      {
          #region IEntity
          
          [DataMember(Name = "ID", Order=1)]
          public Guid Id { get; set; }
          
          [DataMember(Order=2)]
          public DateTime? LastUpdated { get; set; }
          
          [DataMember(Order = 3)]
          public bool IsValid { get; set; }
          
          [DataMember(Order = 4)]
          public bool PreviousImported { get; set; }
          
          #endregion
          
          #region entitymembers
          

        
          [DataMember(Order = 5)]
          public int? Order {get;set;}


        
          [DataMember(Order = 6)]
          public string Type {get;set;}


        
          [DataMember(Order = 7)]
          public string OmzettingType {get;set;}


        
          [DataMember(Order = 8)]
          public string Omschrijving {get;set;}


        
          [DataMember(Order = 9)]
          public DateTime? Date {get;set;}


        
          [DataMember(Order = 10)]
          public decimal? Gewone {get;set;}


        
          [DataMember(Order = 11)]
          public decimal? OnderzoekOntwikkeling {get;set;}


        
          [DataMember(Order = 12)]
          public decimal? OmzetBelasting {get;set;}


        
          [DataMember(Order = 13)]
          public decimal? Totaal {get;set;}


        
          [DataMember(Order = 14)]
          public bool? Disabled {get;set;}


          #endregion
      }

  
      [DataContract(Namespace = "AY2008.TransferTaxCreditsApp")]
      public class BelastingkredietItem : IEntity
      {
          #region IEntity
          
          [DataMember(Name = "ID", Order=1)]
          public Guid Id { get; set; }
          
          [DataMember(Order=2)]
          public DateTime? LastUpdated { get; set; }
          
          [DataMember(Order = 3)]
          public bool IsValid { get; set; }
          
          [DataMember(Order = 4)]
          public bool PreviousImported { get; set; }
          
          #endregion
          
          #region entitymembers
          

        
          [DataMember(Order = 5)]
          public StringListItemDataContract Aanslagjaar {get;set;}


        
          [DataMember(Order = 6)]
          public decimal? BedragAftrek {get;set;}


        
          [DataMember(Order = 7)]
          public decimal? GebruiktHuidigBJ {get;set;}


        
          [DataMember(Order = 8)]
          public decimal? BedragAftrekImported {get;set;}


        
          [DataMember(Order = 9)]
          public decimal? GebruiktHuidigBJImported {get;set;}


        
          [DataMember(Order = 10)]
          public decimal? Saldo {get;set;}


        
          [DataMember(Order = 11)]
          public bool? Disabled {get;set;}


          #endregion
      }

  
      [DataContract(Namespace = "AY2008.TransferTaxCreditsApp")]
      public class AanpassingItem : IEntity
      {
          #region IEntity
          
          [DataMember(Name = "ID", Order=1)]
          public Guid Id { get; set; }
          
          [DataMember(Order=2)]
          public DateTime? LastUpdated { get; set; }
          
          [DataMember(Order = 3)]
          public bool IsValid { get; set; }
          
          [DataMember(Order = 4)]
          public bool PreviousImported { get; set; }
          
          #endregion
          
          #region entitymembers
          

        
          [DataMember(Order = 5)]
          public string AanTePassenTabblad {get;set;}


        
          [DataMember(Order = 6)]
          public StringListItemDataContract AanTePassenVerlies {get;set;}


        
          [DataMember(Order = 7)]
          public StringListItemDataContract AanTePassenInvest {get;set;}


        
          [DataMember(Order = 8)]
          public StringListItemDataContract AanTePassenBelastingKrediet {get;set;}


        
          [DataMember(Order = 9)]
          public string Reden {get;set;}


        
          [DataMember(Order = 10)]
          public string AardReorganisatie {get;set;}


        
          [DataMember(Order = 11)]
          public decimal? Bedrag {get;set;}


          #endregion
      }

  

    #endregion

    #region roottype
    

      [DataContract(Namespace = "AY2008.TransferTaxCreditsApp")]
      public class TransferTaxCredits : IWorksheet
      {
          public TransferTaxCredits() {
              // Set Minimal

        
              this.FiscaleVerliezen = new List<EY.com.eBook.RuleEngine.AY2008.TransferTaxCreditsAppTypes.FiscaalVerlies>();
              
        
              this.NID = new List<EY.com.eBook.RuleEngine.AY2008.TransferTaxCreditsAppTypes.NIDItem>();
              
        
              this.DBI = new List<EY.com.eBook.RuleEngine.AY2008.TransferTaxCreditsAppTypes.DBIItem>();
              
        
              this.Invest = new List<EY.com.eBook.RuleEngine.AY2008.TransferTaxCreditsAppTypes.InvestItem>();
              
        
              this.Belastingkrediet = new List<EY.com.eBook.RuleEngine.AY2008.TransferTaxCreditsAppTypes.BelastingkredietItem>();
              
        
              this.Aanpassingen = new List<EY.com.eBook.RuleEngine.AY2008.TransferTaxCreditsAppTypes.AanpassingItem>();
              
              this.LastUpdated = DateTime.Now;
          }
        
          
          #region entitymembers
          

        
          [DataMember(Order = 1)]
          public List<EY.com.eBook.RuleEngine.AY2008.TransferTaxCreditsAppTypes.FiscaalVerlies> FiscaleVerliezen {get;set;}


        
          [DataMember(Order = 2)]
          public List<EY.com.eBook.RuleEngine.AY2008.TransferTaxCreditsAppTypes.NIDItem> NID {get;set;}


        
          [DataMember(Order = 3)]
          public List<EY.com.eBook.RuleEngine.AY2008.TransferTaxCreditsAppTypes.DBIItem> DBI {get;set;}


        
          [DataMember(Order = 4)]
          public List<EY.com.eBook.RuleEngine.AY2008.TransferTaxCreditsAppTypes.InvestItem> Invest {get;set;}


        
          [DataMember(Order = 5)]
          public List<EY.com.eBook.RuleEngine.AY2008.TransferTaxCreditsAppTypes.BelastingkredietItem> Belastingkrediet {get;set;}


        
          [DataMember(Order = 6)]
          public List<EY.com.eBook.RuleEngine.AY2008.TransferTaxCreditsAppTypes.AanpassingItem> Aanpassingen {get;set;}


      
          #endregion
          
          #region IWorksheet Members
          [DataMember(Order = 7)]
          public DateTime? LastUpdated { get; set; }

          #endregion
      }

  
   
    #endregion

}
  