
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.RuleEngine.Core;
using EY.com.eBook.RuleEngine.Core.Interfaces;
using EY.com.eBook.RuleEngine.Data;


namespace EY.com.eBook.RuleEngine.AY2014.MeerwaardeAandelenAppTypes
{
    #region subtypes


      [DataContract(Namespace = "AY2014.MeerwaardeAandelenApp")]
    public class MeerwaardeAandelenItem : IEntity
      {
          #region IEntity
          
          [DataMember(Name = "ID", Order=1)]
          public Guid Id { get; set; }
          
          [DataMember(Order=2)]
          public DateTime? LastUpdated { get; set; }
          
          [DataMember(Order = 3)]
          public bool IsValid { get; set; }
          
          [DataMember(Order = 4)]
          public bool PreviousImported { get; set; }
          
          #endregion
          
          #region entitymembers
          

        
          [DataMember(Order = 5)]
          public StringListItemDataContract Grootboek {get;set;}


        
          [DataMember(Order = 6)]
          public string Commentaar { get; set; }
        
          [DataMember(Order = 7)]
          public decimal? Saldo { get; set; }


          [DataMember(Order = 8)]
          public bool? TradingVennootschap { get; set; }


        
          [DataMember(Order = 9)]
          public bool? PermanentieVoorwaarde { get; set; }


        
          [DataMember(Order = 10)]
          public bool? TaxatieVoorwaarde { get; set; }


        
          [DataMember(Order = 11)]
          public GuidListItemDataContract Resultaat { get; set; }


          #endregion
      }

 
  

    #endregion

    #region roottype


      [DataContract(Namespace = "AY2014.MeerwaardeAandelenApp")]
      public class MeerwaardeAandelen : IWorksheet
      {
          public MeerwaardeAandelen()
          {
              // Set Minimal


              this.MeerwaardeAandelenLijst = new List<EY.com.eBook.RuleEngine.AY2014.MeerwaardeAandelenAppTypes.MeerwaardeAandelenItem>();
              
              this.LastUpdated = DateTime.Now;
          }
        
          
          #region entitymembers
          

        
          [DataMember(Order = 1)]
          public List<EY.com.eBook.RuleEngine.AY2014.MeerwaardeAandelenAppTypes.MeerwaardeAandelenItem> MeerwaardeAandelenLijst { get; set; }



      
          #endregion
          
          #region IWorksheet Members
          [DataMember(Order = 3)]
          public DateTime? LastUpdated { get; set; }

          #endregion
      }

  
   
    #endregion

}
  