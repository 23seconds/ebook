﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EY.com.eBook.RuleEngine.Core
{
    public class WorksheetTypeSettings
    {
        public bool NeedsDataToValidate = false;

        public bool Clients = false;
        public bool Suppliers = false;
        public bool MappedAccounts = false;
        public bool AllAccounts = false;

        public bool PerformsBookings = false;


    }
}
