
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.RuleEngine.Core;
using EY.com.eBook.RuleEngine.Core.Interfaces;
using EY.com.eBook.RuleEngine.Data;

    
namespace EY.com.eBook.RuleEngine.AY2016.FacturatieAppTypes
{
    #region subtypes


      [DataContract(Namespace = "AY2016.FacturatieApp")]
      public class FacturatieKlant : IEntity
      {
          #region IEntity
          
          [DataMember(Name = "ID", Order=1)]
          public Guid Id { get; set; }
          
          [DataMember(Order=2)]
          public DateTime? LastUpdated { get; set; }
          
          [DataMember(Order = 3)]
          public bool IsValid { get; set; }
          
          [DataMember(Order = 4)]
          public bool PreviousImported { get; set; }
          
          #endregion
          
          #region entitymembers
          

        
          [DataMember(Order = 5)]
          public StringListItemDataContract Grootboek {get;set;}


        
          [DataMember(Order = 6)]
          public StringListItemDataContract Klant {get;set;}


        
          [DataMember(Order = 7)]
          public StringListItemDataContract FacJaar {get;set;}


        
          [DataMember(Order = 8)]
          public string FactuurNr {get;set;}


        
          [DataMember(Order = 9)]
          public decimal? Bedrag {get;set;}

          [DataMember(Order = 10)]
          public string Omschrijving { get; set; }

          [DataMember(Order = 11)]
          public bool? Boeken { get; set; }


          #endregion
      }

  
      [DataContract(Namespace = "AY2016.FacturatieApp")]
      public class FacturatieLeverancierCN : IEntity
      {
          #region IEntity
          
          [DataMember(Name = "ID", Order=1)]
          public Guid Id { get; set; }
          
          [DataMember(Order=2)]
          public DateTime? LastUpdated { get; set; }
          
          [DataMember(Order = 3)]
          public bool IsValid { get; set; }
          
          [DataMember(Order = 4)]
          public bool PreviousImported { get; set; }
          
          #endregion
          
          #region entitymembers
          

        
          [DataMember(Order = 5)]
          public StringListItemDataContract Grootboek {get;set;}


        
          [DataMember(Order = 6)]
          public StringListItemDataContract Leverancier {get;set;}


        
          [DataMember(Order = 7)]
          public StringListItemDataContract FacJaar {get;set;}


        
          [DataMember(Order = 8)]
          public string FactuurNr {get;set;}


        
          [DataMember(Order = 9)]
          public decimal? Bedrag {get;set;}


          [DataMember(Order = 10)]
          public string Omschrijving { get; set; }

          [DataMember(Order = 11)]
          public bool? Boeken { get; set; }


          #endregion
      }

  
      [DataContract(Namespace = "AY2016.FacturatieApp")]
      public class FacturatieLeverancier : IEntity
      {
          #region IEntity
          
          [DataMember(Name = "ID", Order=1)]
          public Guid Id { get; set; }
          
          [DataMember(Order=2)]
          public DateTime? LastUpdated { get; set; }
          
          [DataMember(Order = 3)]
          public bool IsValid { get; set; }
          
          [DataMember(Order = 4)]
          public bool PreviousImported { get; set; }
          
          #endregion
          
          #region entitymembers
          

        
          [DataMember(Order = 5)]
          public StringListItemDataContract Grootboek {get;set;}


        
          [DataMember(Order = 6)]
          public StringListItemDataContract Leverancier {get;set;}


        
          [DataMember(Order = 7)]
          public StringListItemDataContract FacJaar {get;set;}


        
          [DataMember(Order = 8)]
          public string FactuurNr {get;set;}


        
          [DataMember(Order = 9)]
          public decimal? Bedrag {get;set;}


          [DataMember(Order = 10)]
          public string Omschrijving { get; set; }

          [DataMember(Order = 11)]
          public bool? Boeken { get; set; }


          #endregion
      }

  
      [DataContract(Namespace = "AY2016.FacturatieApp")]
      public class FacturatieKlantCN : IEntity
      {
          #region IEntity
          
          [DataMember(Name = "ID", Order=1)]
          public Guid Id { get; set; }
          
          [DataMember(Order=2)]
          public DateTime? LastUpdated { get; set; }
          
          [DataMember(Order = 3)]
          public bool IsValid { get; set; }
          
          [DataMember(Order = 4)]
          public bool PreviousImported { get; set; }
          
          #endregion
          
          #region entitymembers
          

        
          [DataMember(Order = 5)]
          public StringListItemDataContract Grootboek {get;set;}


        
          [DataMember(Order = 6)]
          public StringListItemDataContract Klant {get;set;}


        
          [DataMember(Order = 7)]
          public StringListItemDataContract FacJaar {get;set;}


        
          [DataMember(Order = 8)]
          public string FactuurNr {get;set;}


        
          [DataMember(Order = 9)]
          public decimal? Bedrag {get;set;}


          [DataMember(Order = 10)]
          public string Omschrijving { get; set; }

          [DataMember(Order = 11)]
          public bool? Boeken { get; set; }


          #endregion
      }

  

    #endregion

    #region roottype
    

      [DataContract(Namespace = "AY2016.FacturatieApp")]
      public class Facturatie : IWorksheet
      {
          public Facturatie() {
              // Set Minimal

        
              this.OpTeMakenFacturen = new List<EY.com.eBook.RuleEngine.AY2016.FacturatieAppTypes.FacturatieKlant>();
              
        
              this.TeOntvangenCN = new List<EY.com.eBook.RuleEngine.AY2016.FacturatieAppTypes.FacturatieLeverancierCN>();
              
        
              this.TeOntvangenFacturen = new List<EY.com.eBook.RuleEngine.AY2016.FacturatieAppTypes.FacturatieLeverancier>();
              
        
              this.OpTeMakenCN = new List<EY.com.eBook.RuleEngine.AY2016.FacturatieAppTypes.FacturatieKlantCN>();
              
              this.LastUpdated = DateTime.Now;
          }
        
          
          #region entitymembers
          

        
          [DataMember(Order = 1)]
          public List<EY.com.eBook.RuleEngine.AY2016.FacturatieAppTypes.FacturatieKlant> OpTeMakenFacturen {get;set;}


        
          [DataMember(Order = 2)]
          public List<EY.com.eBook.RuleEngine.AY2016.FacturatieAppTypes.FacturatieLeverancierCN> TeOntvangenCN {get;set;}


        
          [DataMember(Order = 3)]
          public List<EY.com.eBook.RuleEngine.AY2016.FacturatieAppTypes.FacturatieLeverancier> TeOntvangenFacturen {get;set;}


        
          [DataMember(Order = 4)]
          public List<EY.com.eBook.RuleEngine.AY2016.FacturatieAppTypes.FacturatieKlantCN> OpTeMakenCN {get;set;}


      
          #endregion
          
          #region IWorksheet Members
          [DataMember(Order = 5)]
          public DateTime? LastUpdated { get; set; }

          #endregion
      }

  
   
    #endregion

}
  