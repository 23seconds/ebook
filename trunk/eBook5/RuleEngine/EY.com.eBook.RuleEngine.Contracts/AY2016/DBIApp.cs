
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.RuleEngine.Core;
using EY.com.eBook.RuleEngine.Core.Interfaces;
using EY.com.eBook.RuleEngine.Data;

    
namespace EY.com.eBook.RuleEngine.AY2016.DBIAppTypes
{
    #region subtypes


      [DataContract(Namespace = "AY2016.DBIApp")]
      public class ParticipatieItem : IEntity
      {
          #region IEntity
          
          [DataMember(Name = "ID", Order=1)]
          public Guid Id { get; set; }
          
          [DataMember(Order=2)]
          public DateTime? LastUpdated { get; set; }
          
          [DataMember(Order = 3)]
          public bool IsValid { get; set; }
          
          [DataMember(Order = 4)]
          public bool PreviousImported { get; set; }
          
          #endregion
          
          #region entitymembers
          

        
          [DataMember(Order = 5)]
          public string Name {get;set;}


        
          [DataMember(Order = 6)]
          public StringListItemDataContract AccountParticipation {get;set;}


        
          [DataMember(Order = 7)]
          public GuidListItemDataContract Country {get;set;}


        
          [DataMember(Order = 8)]
          public DateTime? DateBought {get;set;}


        
          [DataMember(Order = 9)]
          public decimal? AcquisitionAmount {get;set;}


        
          [DataMember(Order = 10)]
          public decimal? PercentageAcquired {get;set;}


        
          [DataMember(Order = 11)]
          public string DisplayName {get;set;}


        
          [DataMember(Order = 12)]
          public bool? overrule {get;set;}


          #endregion
      }

  
      [DataContract(Namespace = "AY2016.DBIApp")]
      public class DividendItem : IEntity
      {
          #region IEntity
          
          [DataMember(Name = "ID", Order=1)]
          public Guid Id { get; set; }
          
          [DataMember(Order=2)]
          public DateTime? LastUpdated { get; set; }
          
          [DataMember(Order = 3)]
          public bool IsValid { get; set; }
          
          [DataMember(Order = 4)]
          public bool PreviousImported { get; set; }
          
          #endregion
          
          #region entitymembers
          

        
          [DataMember(Order = 5)]
          public StringListItemDataContract Participatie {get;set;}


        
          [DataMember(Order = 6)]
          public StringListItemDataContract AccountDividend {get;set;}


        
          [DataMember(Order = 7)]
          public DateTime? DateDividend {get;set;}


        
          [DataMember(Order = 8)]
          public bool? Forex {get;set;}


        
          [DataMember(Order = 9)]
          public bool? Taxation {get;set;}


        
          [DataMember(Order = 10)]
          public decimal? Bruto {get;set;}


        
          [DataMember(Order = 11)]
          public decimal? Perc95 {get;set;}


        
          [DataMember(Order = 12)]
          public decimal? Bedrag {get;set;}


        
          [DataMember(Order = 13)]
          public string ParticipatieNaam {get;set;}


          #endregion
      }

  
      [DataContract(Namespace = "AY2016.DBIApp")]
      public class DbiEntity : IEntity
      {
          #region IEntity
          
          [DataMember(Name = "ID", Order=1)]
          public Guid Id { get; set; }
          
          [DataMember(Order=2)]
          public DateTime? LastUpdated { get; set; }
          
          [DataMember(Order = 3)]
          public bool IsValid { get; set; }
          
          [DataMember(Order = 4)]
          public bool PreviousImported { get; set; }
          
          #endregion
          
          #region entitymembers
          

        
          [DataMember(Order = 5)]
          public string TitleWaarden {get;set;}


        
          [DataMember(Order = 6)]
          public string TitleBelgisch {get;set;}


        
          [DataMember(Order = 7)]
          public string TitleBuitenlands {get;set;}


        
          [DataMember(Order = 8)]
          public string TitleTotaal {get;set;}


        
          [DataMember(Order = 9)]
          public string TitleAandelen {get;set;}


        
          [DataMember(Order = 10)]
          public string TitleAandelenEUDochter {get;set;}


        
          [DataMember(Order = 11)]
          public string TitleABelgisch {get;set;}


        
          [DataMember(Order = 12)]
          public decimal? DBIAbeNettoBEI {get;set;}


        
          [DataMember(Order = 13)]
          public decimal? DBIAbeNettoBUI {get;set;}


        
          [DataMember(Order = 14)]
          public decimal? DBIAbeNettoTotaal {get;set;}


        
          [DataMember(Order = 15)]
          public decimal? DBIAbeRoerendBEI {get;set;}


        
          [DataMember(Order = 16)]
          public decimal? DBIAbeRoerendBUI {get;set;}


        
          [DataMember(Order = 17)]
          public decimal? DBIAbeRoerendTotaal {get;set;}


        
          [DataMember(Order = 18)]
          public string TitleABuitenland {get;set;}


        
          [DataMember(Order = 19)]
          public decimal? DBIAbuNettoBEI {get;set;}


        
          [DataMember(Order = 20)]
          public decimal? DBIAbuNettoBUI {get;set;}


        
          [DataMember(Order = 21)]
          public decimal? DBIAbuNettoTotaal {get;set;}


        
          [DataMember(Order = 22)]
          public decimal? DBIAbuRoerendBEI {get;set;}


        
          [DataMember(Order = 23)]
          public decimal? DBIAbuRoerendBUI {get;set;}


        
          [DataMember(Order = 24)]
          public decimal? DBIAbuRoerendTotaal {get;set;}


        
          [DataMember(Order = 25)]
          public string TitleAandelenAndere {get;set;}


        
          [DataMember(Order = 26)]
          public string TitleAABelgisch {get;set;}


        
          [DataMember(Order = 27)]
          public decimal? DBIAAbeNettoBEI {get;set;}


        
          [DataMember(Order = 28)]
          public decimal? DBIAAbeNettoBUI {get;set;}


        
          [DataMember(Order = 29)]
          public decimal? DBIAAbeNettoTotaal {get;set;}


        
          [DataMember(Order = 30)]
          public decimal? DBIAAbeRoerendBEI {get;set;}


        
          [DataMember(Order = 31)]
          public decimal? DBIAAbeRoerendBUI {get;set;}


        
          [DataMember(Order = 32)]
          public decimal? DBIAAbeRoerendTotaal {get;set;}


        
          [DataMember(Order = 33)]
          public string TitleAABuitenland {get;set;}


        
          [DataMember(Order = 34)]
          public decimal? DBIAAbuNettoBEI {get;set;}


        
          [DataMember(Order = 35)]
          public decimal? DBIAAbuNettoBUI {get;set;}


        
          [DataMember(Order = 36)]
          public decimal? DBIAAbuNettoTotaal {get;set;}


        
          [DataMember(Order = 37)]
          public decimal? DBIAAbuRoerendBEI {get;set;}


        
          [DataMember(Order = 38)]
          public decimal? DBIAAbuRoerendBUI {get;set;}


        
          [DataMember(Order = 39)]
          public decimal? DBIAAbuRoerendTotaal {get;set;}


        
          [DataMember(Order = 40)]
          public decimal? DBIVrijgestelRoerendBEI {get;set;}


        
          [DataMember(Order = 41)]
          public decimal? DBIVrijgestelRoerendBUI {get;set;}


        
          [DataMember(Order = 42)]
          public decimal? DBIVrijgestelRoerendTotaal {get;set;}


        
          [DataMember(Order = 43)]
          public decimal? DBISubtotaalBEI {get;set;}


        
          [DataMember(Order = 44)]
          public decimal? DBISubtotaalBUI {get;set;}


        
          [DataMember(Order = 45)]
          public decimal? DBISubtotaalTotaal {get;set;}


        
          [DataMember(Order = 46)]
          public decimal? DBIKostenBEI {get;set;}


        
          [DataMember(Order = 47)]
          public decimal? DBIKostenBUI {get;set;}


        
          [DataMember(Order = 48)]
          public decimal? DBIKostenTotaal {get;set;}


        
          [DataMember(Order = 49)]
          public decimal? DBIVerschilBEI {get;set;}


        
          [DataMember(Order = 50)]
          public decimal? DBIVerschilBUI {get;set;}


        
          [DataMember(Order = 51)]
          public decimal? DBIVerschilTotaal {get;set;}


        
          [DataMember(Order = 52)]
          public decimal? DBILidEuBEI {get;set;}


        
          [DataMember(Order = 53)]
          public decimal? DBILidEuBUI {get;set;}


        
          [DataMember(Order = 54)]
          public decimal? DBILidEuTotaal {get;set;}


        
          [DataMember(Order = 55)]
          public decimal? DBIHerfinancBEI {get;set;}


        
          [DataMember(Order = 56)]
          public decimal? DBIHerfinancBUI {get;set;}


        
          [DataMember(Order = 57)]
          public decimal? DBIHerfinancTotaal {get;set;}


        
          [DataMember(Order = 58)]
          public decimal? DBITotaalBEI {get;set;}


        
          [DataMember(Order = 59)]
          public decimal? DBITotaalBUI {get;set;}


        
          [DataMember(Order = 60)]
          public decimal? DBITotaal {get;set;}


        
          [DataMember(Order = 61)]
          public decimal? BelastbareBasis {get;set;}


        
          [DataMember(Order = 62)]
          public decimal? Aanwending {get;set;}


        
          [DataMember(Order = 63)]
          public decimal? BelastbareBasisResterend {get;set;}


        
          [DataMember(Order = 64)]
          public decimal? SaldoStart {get;set;}


        
          [DataMember(Order = 65)]
          public decimal? SaldoEinde {get;set;}


        
          [DataMember(Order = 66)]
          public decimal? Nieuwe {get;set;}


          #endregion
      }

  
      [DataContract(Namespace = "AY2016.DBIApp")]
      public class HistoryItem : IEntity
      {
          #region IEntity
          
          [DataMember(Name = "ID", Order=1)]
          public Guid Id { get; set; }
          
          [DataMember(Order=2)]
          public DateTime? LastUpdated { get; set; }
          
          [DataMember(Order = 3)]
          public bool IsValid { get; set; }
          
          [DataMember(Order = 4)]
          public bool PreviousImported { get; set; }
          
          #endregion
          
          #region entitymembers
          

        
          [DataMember(Order = 5)]
          public StringListItemDataContract Aanslagjaar {get;set;}


        
          [DataMember(Order = 6)]
          public decimal? SaldoStart {get;set;}


        
          [DataMember(Order = 7)]
          public decimal? GebruiktBJ {get;set;}


        
          [DataMember(Order = 8)]
          public decimal? SaldoEinde {get;set;}

          [DataMember(Order = 9)]
          public decimal? NietOverdraagbaarDeel { get; set; }


          #endregion
      }

  

    #endregion

    #region roottype
    

      [DataContract(Namespace = "AY2016.DBIApp")]
      public class DBIWorksheet : IWorksheet
      {
          public DBIWorksheet() {
              // Set Minimal

        
              this.Participations = new List<EY.com.eBook.RuleEngine.AY2016.DBIAppTypes.ParticipatieItem>();
              
        
              this.Dividends = new List<EY.com.eBook.RuleEngine.AY2016.DBIAppTypes.DividendItem>();
              
        
              this.DBI = new EY.com.eBook.RuleEngine.AY2016.DBIAppTypes.DbiEntity();
              this.DBI.Id = Guid.NewGuid();
        
              this.Historiek = new List<EY.com.eBook.RuleEngine.AY2016.DBIAppTypes.HistoryItem>();
              
              this.LastUpdated = DateTime.Now;
          }
        
          
          #region entitymembers
          

        
          [DataMember(Order = 1)]
          public List<EY.com.eBook.RuleEngine.AY2016.DBIAppTypes.ParticipatieItem> Participations {get;set;}


        
          [DataMember(Order = 2)]
          public List<EY.com.eBook.RuleEngine.AY2016.DBIAppTypes.DividendItem> Dividends {get;set;}


        
          [DataMember(Order = 3)]
          public EY.com.eBook.RuleEngine.AY2016.DBIAppTypes.DbiEntity DBI {get;set;}


        
          [DataMember(Order = 4)]
          public List<EY.com.eBook.RuleEngine.AY2016.DBIAppTypes.HistoryItem> Historiek {get;set;}


      
          #endregion
          
          #region IWorksheet Members
          [DataMember(Order = 5)]
          public DateTime? LastUpdated { get; set; }

          #endregion
      }

  
   
    #endregion

}
  