
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.RuleEngine.Core;
using EY.com.eBook.RuleEngine.Core.Interfaces;
using EY.com.eBook.RuleEngine.Data;

    
namespace EY.com.eBook.RuleEngine.AY2013.OverlopendeRekeningenAppTypes
{
    #region subtypes


      [DataContract(Namespace = "AY2013.OverlopendeRekeningenApp")]
      public class OverTeDragenKost : IEntity
      {
          #region IEntity
          
          [DataMember(Name = "ID", Order=1)]
          public Guid Id { get; set; }
          
          [DataMember(Order=2)]
          public DateTime? LastUpdated { get; set; }
          
          [DataMember(Order = 3)]
          public bool IsValid { get; set; }
          
          [DataMember(Order = 4)]
          public bool PreviousImported { get; set; }
          
          #endregion
          
          #region entitymembers
          

        
          [DataMember(Order = 5)]
          public StringListItemDataContract Grootboek {get;set;}


        
          [DataMember(Order = 6)]
          public StringListItemDataContract Leverancier {get;set;}


        
          [DataMember(Order = 7)]
          public string Omschrijving {get;set;}


        
          [DataMember(Order = 8)]
          public StringListItemDataContract InvoiceYear {get;set;}


        
          [DataMember(Order = 9)]
          public string InvoiceNumber {get;set;}


        
          [DataMember(Order = 10)]
          public DateTime? BeginDate {get;set;}


        
          [DataMember(Order = 11)]
          public DateTime? EndDate {get;set;}


        
          [DataMember(Order = 12)]
          public decimal? Amount {get;set;}


        
          [DataMember(Order = 13)]
          public decimal? DeferredIncome {get;set;}


        
          [DataMember(Order = 14)]
          public bool? Book {get;set;}


        
          [DataMember(Order = 15)]
          public bool? AutoLine {get;set;}


        
          [DataMember(Order = 16)]
          public string AutoKey {get;set;}


        
          [DataMember(Order = 17)]
          public string LeverancierNaam {get;set;}


          #endregion
      }

  
      [DataContract(Namespace = "AY2013.OverlopendeRekeningenApp")]
      public class VerkregenOpbrengst : IEntity
      {
          #region IEntity
          
          [DataMember(Name = "ID", Order=1)]
          public Guid Id { get; set; }
          
          [DataMember(Order=2)]
          public DateTime? LastUpdated { get; set; }
          
          [DataMember(Order = 3)]
          public bool IsValid { get; set; }
          
          [DataMember(Order = 4)]
          public bool PreviousImported { get; set; }
          
          #endregion
          
          #region entitymembers
          

        
          [DataMember(Order = 5)]
          public StringListItemDataContract Grootboek {get;set;}


        
          [DataMember(Order = 6)]
          public string Omschrijving {get;set;}


        
          [DataMember(Order = 7)]
          public StringListItemDataContract InvoiceYear {get;set;}


        
          [DataMember(Order = 8)]
          public string InvoiceNumber {get;set;}


        
          [DataMember(Order = 9)]
          public DateTime? BeginDate {get;set;}


        
          [DataMember(Order = 10)]
          public DateTime? EndDate {get;set;}


        
          [DataMember(Order = 11)]
          public decimal? Amount {get;set;}


        
          [DataMember(Order = 12)]
          public decimal? DeferredIncome {get;set;}


        
          [DataMember(Order = 13)]
          public bool? Book {get;set;}


        
          [DataMember(Order = 14)]
          public bool? AutoLine {get;set;}


        
          [DataMember(Order = 15)]
          public string AutoKey {get;set;}


          #endregion
      }

  
      [DataContract(Namespace = "AY2013.OverlopendeRekeningenApp")]
      public class ToeTeRekenenKost : IEntity
      {
          #region IEntity
          
          [DataMember(Name = "ID", Order=1)]
          public Guid Id { get; set; }
          
          [DataMember(Order=2)]
          public DateTime? LastUpdated { get; set; }
          
          [DataMember(Order = 3)]
          public bool IsValid { get; set; }
          
          [DataMember(Order = 4)]
          public bool PreviousImported { get; set; }
          
          #endregion
          
          #region entitymembers
          

        
          [DataMember(Order = 5)]
          public StringListItemDataContract Grootboek {get;set;}


        
          [DataMember(Order = 6)]
          public string Omschrijving {get;set;}


        
          [DataMember(Order = 7)]
          public StringListItemDataContract InvoiceYear {get;set;}


        
          [DataMember(Order = 8)]
          public string InvoiceNumber {get;set;}


        
          [DataMember(Order = 9)]
          public DateTime? BeginDate {get;set;}


        
          [DataMember(Order = 10)]
          public DateTime? EndDate {get;set;}


        
          [DataMember(Order = 11)]
          public decimal? Amount {get;set;}


        
          [DataMember(Order = 12)]
          public decimal? DeferredIncome {get;set;}


        
          [DataMember(Order = 13)]
          public bool? Book {get;set;}


        
          [DataMember(Order = 14)]
          public bool? AutoLine {get;set;}


        
          [DataMember(Order = 15)]
          public string AutoKey {get;set;}


          #endregion
      }

  
      [DataContract(Namespace = "AY2013.OverlopendeRekeningenApp")]
      public class OverTeDragenOpbrengst : IEntity
      {
          #region IEntity
          
          [DataMember(Name = "ID", Order=1)]
          public Guid Id { get; set; }
          
          [DataMember(Order=2)]
          public DateTime? LastUpdated { get; set; }
          
          [DataMember(Order = 3)]
          public bool IsValid { get; set; }
          
          [DataMember(Order = 4)]
          public bool PreviousImported { get; set; }
          
          #endregion
          
          #region entitymembers
          

        
          [DataMember(Order = 5)]
          public StringListItemDataContract Grootboek {get;set;}


        
          [DataMember(Order = 6)]
          public string Omschrijving {get;set;}


        
          [DataMember(Order = 7)]
          public StringListItemDataContract InvoiceYear {get;set;}


        
          [DataMember(Order = 8)]
          public string InvoiceNumber {get;set;}


        
          [DataMember(Order = 9)]
          public DateTime? BeginDate {get;set;}


        
          [DataMember(Order = 10)]
          public DateTime? EndDate {get;set;}


        
          [DataMember(Order = 11)]
          public decimal? Amount {get;set;}


        
          [DataMember(Order = 12)]
          public decimal? DeferredIncome {get;set;}


        
          [DataMember(Order = 13)]
          public bool? Book {get;set;}


        
          [DataMember(Order = 14)]
          public bool? AutoLine {get;set;}


        
          [DataMember(Order = 15)]
          public string AutoKey {get;set;}


          #endregion
      }

  

    #endregion

    #region roottype
    

      [DataContract(Namespace = "AY2013.OverlopendeRekeningenApp")]
      public class OverlopendeRekeningen : IWorksheet
      {
          public OverlopendeRekeningen() {
              // Set Minimal

        
              this.OverTeDragenKosten = new List<EY.com.eBook.RuleEngine.AY2013.OverlopendeRekeningenAppTypes.OverTeDragenKost>();
              
        
              this.VerkregenOpbrensten = new List<EY.com.eBook.RuleEngine.AY2013.OverlopendeRekeningenAppTypes.VerkregenOpbrengst>();
              
        
              this.ToeTeRekenenKosten = new List<EY.com.eBook.RuleEngine.AY2013.OverlopendeRekeningenAppTypes.ToeTeRekenenKost>();
              
        
              this.OverTeDragenOpbrengsten = new List<EY.com.eBook.RuleEngine.AY2013.OverlopendeRekeningenAppTypes.OverTeDragenOpbrengst>();
              
              this.LastUpdated = DateTime.Now;
          }
        
          
          #region entitymembers
          

        
          [DataMember(Order = 1)]
          public List<EY.com.eBook.RuleEngine.AY2013.OverlopendeRekeningenAppTypes.OverTeDragenKost> OverTeDragenKosten {get;set;}


        
          [DataMember(Order = 2)]
          public List<EY.com.eBook.RuleEngine.AY2013.OverlopendeRekeningenAppTypes.VerkregenOpbrengst> VerkregenOpbrensten {get;set;}


        
          [DataMember(Order = 3)]
          public List<EY.com.eBook.RuleEngine.AY2013.OverlopendeRekeningenAppTypes.ToeTeRekenenKost> ToeTeRekenenKosten {get;set;}


        
          [DataMember(Order = 4)]
          public List<EY.com.eBook.RuleEngine.AY2013.OverlopendeRekeningenAppTypes.OverTeDragenOpbrengst> OverTeDragenOpbrengsten {get;set;}


      
          #endregion
          
          #region IWorksheet Members
          [DataMember(Order = 5)]
          public DateTime? LastUpdated { get; set; }

          #endregion
      }

  
   
    #endregion

}
  