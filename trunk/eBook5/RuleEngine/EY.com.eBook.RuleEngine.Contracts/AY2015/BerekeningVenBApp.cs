
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.RuleEngine.Core;
using EY.com.eBook.RuleEngine.Core.Interfaces;
using EY.com.eBook.RuleEngine.Data;

    
namespace EY.com.eBook.RuleEngine.AY2015.BerekeningVenBAppTypes
{
    #region subtypes


      [DataContract(Namespace = "AY2015.BerekeningVenBApp")]
      public class General : IEntity
      {
          #region IEntity
          
          [DataMember(Name = "ID", Order=1)]
          public Guid Id { get; set; }
          
          [DataMember(Order=2)]
          public DateTime? LastUpdated { get; set; }
          
          [DataMember(Order = 3)]
          public bool IsValid { get; set; }
          
          [DataMember(Order = 4)]
          public bool PreviousImported { get; set; }
          
          #endregion
          
          #region entitymembers
          

        
          [DataMember(Order = 5)]
          public decimal? GereserveerdeWinst {get;set;}


        
          [DataMember(Order = 6)]
          public string Spacer25 {get;set;}


        
          [DataMember(Order = 7)]
          public decimal? Dividenden {get;set;}


        
          [DataMember(Order = 8)]
          public string Spacer26 {get;set;}


        
          [DataMember(Order = 9)]
          public decimal? VerkrijgingEigenAandelen {get;set;}


        
          [DataMember(Order = 10)]
          public string Spacer27 {get;set;}


        
          [DataMember(Order = 11)]
          public decimal? OverlijdenUittredingUitsluitingVennoot {get;set;}


        
          [DataMember(Order = 12)]
          public string Spacer28 {get;set;}


        
          [DataMember(Order = 13)]
          public decimal? VerdelingMaatschappelijkVermogen {get;set;}


        
          [DataMember(Order = 14)]
          public string Spacer29 {get;set;}


        
          [DataMember(Order = 15)]
          public decimal? StatAccountingResult {get;set;}


        
          [DataMember(Order = 16)]
          public decimal? TotalBelasteReserves {get;set;}


        
          [DataMember(Order = 17)]
          public decimal? TotalAanpassingenBelasteReserves {get;set;}


        
          [DataMember(Order = 18)]
          public decimal? NietAftrekbareBelastingen {get;set;}


        
          [DataMember(Order = 19)]
          public decimal? TotaalVerworpenUitgaven {get;set;}


        
          [DataMember(Order = 20)]
          public decimal? BelastbaarResultaat {get;set;}


        
          [DataMember(Order = 21)]
          public decimal? TotaalBelastbareBestandenZonderAftrekMin {get;set;}


        
          [DataMember(Order = 22)]
          public decimal? SubTotaalBelastbaarVoorAftrek {get;set;}


        
          [DataMember(Order = 23)]
          public decimal? OpdelingNaarOorsprongBijVerdragVrijgesteldInkomen {get;set;}


        
          [DataMember(Order = 24)]
          public decimal? TotaalFiscaleAftrek {get;set;}


        
          [DataMember(Order = 25)]
          public decimal? SubTotaalNaAftrek {get;set;}


        
          [DataMember(Order = 26)]
          public decimal? TotaalBelastbareBestandenZonderAftrekPlus {get;set;}


        
          [DataMember(Order = 27)]
          public decimal? BelastbareGrondslag {get;set;}


          #endregion
      }

  
      [DataContract(Namespace = "AY2015.BerekeningVenBApp")]
      public class BelasteReservesItem : IEntity
      {
          #region IEntity
          
          [DataMember(Name = "ID", Order=1)]
          public Guid Id { get; set; }
          
          [DataMember(Order=2)]
          public DateTime? LastUpdated { get; set; }
          
          [DataMember(Order = 3)]
          public bool IsValid { get; set; }
          
          [DataMember(Order = 4)]
          public bool PreviousImported { get; set; }
          
          #endregion
          
          #region entitymembers
          

        
          [DataMember(Order = 5)]
          public string TempDiff {get;set;}


        
          [DataMember(Order = 6)]
          public decimal? TDKapitaalEnUitgifte {get;set;}


        
          [DataMember(Order = 7)]
          public decimal? TDHerwaardering {get;set;}


        
          [DataMember(Order = 8)]
          public decimal? TDTaxableProvisions {get;set;}


        
          [DataMember(Order = 9)]
          public decimal? TDOtherTaxableReserves {get;set;}

          [DataMember(Order = 22)]
          public decimal? TDOtherNonBalanceTaxableReserves { get; set; }
        
          [DataMember(Order = 10)]
          public decimal? TDTaxableDoubtfullDebtors {get;set;}


        
          [DataMember(Order = 11)]
          public decimal? TDExcessDeprications {get;set;}


        
          [DataMember(Order = 12)]
          public decimal? TDUnderestimationAssetsOverestimationLiabilities {get;set;}


        
          [DataMember(Order = 13)]
          public decimal? TDOverestimationLiabilities {get;set;}


        
          [DataMember(Order = 14)]
          public decimal? TDTotal {get;set;}


        
          [DataMember(Order = 15)]
          public string OhterAdjustments {get;set;}


        
          [DataMember(Order = 16)]
          public decimal? OACapitalGainsOnShares {get;set;}

          [DataMember(Order = 24)]
          public decimal? OACapitalGainsSharesReversal { get; set; }
        
          [DataMember(Order = 17)]
          public decimal? OADefinitiveExemptionTaxShelter {get;set;}


        
          [DataMember(Order = 18)]
          public decimal? OAExemptionRegionalPrem {get;set;}

          [DataMember(Order = 25)]
          public decimal? OAFinalExemptionProfit { get; set; }

        
          [DataMember(Order = 19)]
          public decimal? OAOthers {get;set;}

          [DataMember(Order = 23)]
          public decimal? OAInTheMin { get; set; }
        
          [DataMember(Order = 20)]
          public decimal? OATotal {get;set;}


        
          [DataMember(Order = 21)]
          public decimal? TDOthers {get;set;}


          #endregion
      }

  
      [DataContract(Namespace = "AY2015.BerekeningVenBApp")]
      public class VerworpenUitgavenItem : IEntity
      {
          #region IEntity
          
          [DataMember(Name = "ID", Order=1)]
          public Guid Id { get; set; }
          
          [DataMember(Order=2)]
          public DateTime? LastUpdated { get; set; }
          
          [DataMember(Order = 3)]
          public bool IsValid { get; set; }
          
          [DataMember(Order = 4)]
          public bool PreviousImported { get; set; }
          
          #endregion
          
          #region entitymembers
          

        
          [DataMember(Order = 5)]
          public string PermanentDifferences {get;set;}


        
          [DataMember(Order = 6)]
          public string DisallowedExpenses {get;set;}


        
          [DataMember(Order = 7)]
          public decimal? PDDisallowedCurrentIncomeTaxExpenses {get;set;}


        
          [DataMember(Order = 8)]
          public decimal? DERegionalTaxes {get;set;}


        
          [DataMember(Order = 9)]
          public decimal? DEPenalties {get;set;}


        
          [DataMember(Order = 10)]
          public decimal? DEPensions {get;set;}


        
          [DataMember(Order = 11)]
          public decimal? DECar {get;set;}


          [DataMember(Order = 30)]
          public decimal? DECarVAA { get; set; }

        
          [DataMember(Order = 12)]
          public decimal? DEReception {get;set;}


        
          [DataMember(Order = 13)]
          public decimal? DERestaurant {get;set;}


        
          [DataMember(Order = 14)]
          public decimal? DEClothes {get;set;}


        
          [DataMember(Order = 15)]
          public decimal? DEExcessInterest {get;set;}


        
          [DataMember(Order = 16)]
          public decimal? DEInterestLoans {get;set;}


        
          [DataMember(Order = 17)]
          public decimal? DEAbnormalAdvantages {get;set;}


        
          [DataMember(Order = 18)]
          public decimal? DESocial {get;set;}


        
          [DataMember(Order = 19)]
          public decimal? DEDinerSportCultureEco {get;set;}


        
          [DataMember(Order = 20)]
          public decimal? DECharity {get;set;}


        
          [DataMember(Order = 21)]
          public decimal? DEReductionValueShares {get;set;}


        
          [DataMember(Order = 22)]
          public decimal? DEReversalsPreviousTax {get;set;}


        
          [DataMember(Order = 23)]
          public decimal? DEEmployeeCapital {get;set;}


        
          [DataMember(Order = 24)]
          public decimal? DEIndemnityMissingCoupon {get;set;}


        
          [DataMember(Order = 25)]
          public decimal? DEExpensesTaxShelter {get;set;}


        
          [DataMember(Order = 26)]
          public decimal? DERegionalPremiumsCapital {get;set;}


        
          [DataMember(Order = 27)]
          public decimal? DEBepaaldeStaten {get;set;}


        
          [DataMember(Order = 28)]
          public decimal? DEOther {get;set;}

          [DataMember(Order = 31)]
          public decimal? DEUnjustifiedExpenses { get; set; }
        
          [DataMember(Order = 29)]
          public decimal? DETotal {get;set;}


          #endregion
      }

  
      [DataContract(Namespace = "AY2015.BerekeningVenBApp")]
      public class BelastbareBestZonderAftrekItem : IEntity
      {
          #region IEntity
          
          [DataMember(Name = "ID", Order=1)]
          public Guid Id { get; set; }
          
          [DataMember(Order=2)]
          public DateTime? LastUpdated { get; set; }
          
          [DataMember(Order = 3)]
          public bool IsValid { get; set; }
          
          [DataMember(Order = 4)]
          public bool PreviousImported { get; set; }
          
          #endregion
          
          #region entitymembers
          

        
          [DataMember(Order = 5)]
          public string TaxableElementsWithoutDeductions {get;set;}

          [DataMember(Order = 13)]
          public decimal? TECapitalGains0412 { get; set; }
        
          [DataMember(Order = 6)]
          public decimal? TEReceivedAbnormal {get;set;}


        
          [DataMember(Order = 7)]
          public decimal? TETaxableInvestmentReserve {get;set;}


          [DataMember(Order = 11)]
          public decimal? TEVaaCar { get; set; }

        
          [DataMember(Order = 8)]
          public decimal? TEEmployeeParticipationCapital {get;set;}


        
          [DataMember(Order = 9)]
          public decimal? TETaxationEexemptedReservesCapitalGains {get;set;}

          [DataMember(Order = 12)]
          public decimal? TECapitalAgriCulture { get; set; }

        
          [DataMember(Order = 10)]
          public decimal? TETotal {get;set;}


          #endregion
      }

  
      [DataContract(Namespace = "AY2015.BerekeningVenBApp")]
      public class OpdelingNaarOorsprongItem : IEntity
      {
          #region IEntity
          
          [DataMember(Name = "ID", Order=1)]
          public Guid Id { get; set; }
          
          [DataMember(Order=2)]
          public DateTime? LastUpdated { get; set; }
          
          [DataMember(Order = 3)]
          public bool IsValid { get; set; }
          
          [DataMember(Order = 4)]
          public bool PreviousImported { get; set; }
          
          #endregion
          
          #region entitymembers

          [DataMember(Order = 13)]
          public decimal? WIZeeSchip { get; set; }
        
          [DataMember(Order = 5)]
          public string ItemizationResult {get;set;}


        
          [DataMember(Order = 6)]
          public decimal? IRBelgianTaxable {get;set;}


        
          [DataMember(Order = 7)]
          public decimal? IRNonTreatyForeignIncome {get;set;}


        
          [DataMember(Order = 8)]
          public decimal? IRTreatyForeignIncome {get;set;}


        
          [DataMember(Order = 9)]
          public string IRRemainingResult {get;set;}


        
          [DataMember(Order = 10)]
          public decimal? IRRemainingResultBelgium {get;set;}


        
          [DataMember(Order = 11)]
          public string IRRemainingResultSpacer {get;set;}


        
          [DataMember(Order = 12)]
          public decimal? IRRemainingResultNonTreaty {get;set;}


          #endregion
      }

  
      [DataContract(Namespace = "AY2015.BerekeningVenBApp")]
      public class FiscaleAftrekItem : IEntity
      {
          #region IEntity
          
          [DataMember(Name = "ID", Order=1)]
          public Guid Id { get; set; }
          
          [DataMember(Order=2)]
          public DateTime? LastUpdated { get; set; }
          
          [DataMember(Order = 3)]
          public bool IsValid { get; set; }
          
          [DataMember(Order = 4)]
          public bool PreviousImported { get; set; }
          
          #endregion
          
          #region entitymembers
          

        
          [DataMember(Order = 5)]
          public string NonTaxableElements {get;set;}


        
          [DataMember(Order = 6)]
          public decimal? NTEBECharity {get;set;}


        
          [DataMember(Order = 7)]
          public decimal? NTEBEPersonnel {get;set;}


        
          [DataMember(Order = 8)]
          public decimal? NTEBEPersonnelSME {get;set;}


        
          [DataMember(Order = 9)]
          public decimal? NTEBEInternalshipPremium {get;set;}


        
          [DataMember(Order = 10)]
          public decimal? NTEBEOthers {get;set;}


        
          [DataMember(Order = 11)]
          public decimal? TotalNonTaxableElements {get;set;}


        
          [DataMember(Order = 12)]
          public string ParticipationExemption {get;set;}


        
          [DataMember(Order = 13)]
          public decimal? PEBelgium {get;set;}


        
          [DataMember(Order = 14)]
          public string PatentIncomeDeduction {get;set;}


        
          [DataMember(Order = 15)]
          public decimal? PIDBelgium {get;set;}


        
          [DataMember(Order = 16)]
          public string NotionalIntrestDeduction {get;set;}


        
          [DataMember(Order = 17)]
          public decimal? NIDBelgium {get;set;}


        
          [DataMember(Order = 18)]
          public string DeductionTaxLosses {get;set;}


        
          [DataMember(Order = 19)]
          public decimal? DTLBelgium {get;set;}


        
          [DataMember(Order = 20)]
          public string InvestmentDeduction {get;set;}


        
          [DataMember(Order = 21)]
          public decimal? IDBelgium {get;set;}

          [DataMember(Order = 22)]
          public string NotionalIntrestDeductionHistory { get; set; }

          [DataMember(Order = 23)]
          public decimal? NIDHistoryBelgium { get; set; }


        
          [DataMember(Order = 24)]
          public decimal? TotalFiscaleAftrek {get;set;}


          #endregion
      }

      [DataContract(Namespace = "AY2015.BerekeningVenBApp")]
      public class VerrekenbareVoorheffingEntity : IEntity
      {
          #region IEntity

          [DataMember(Name = "ID", Order = 1)]
          public Guid Id { get; set; }

          [DataMember(Order = 2)]
          public DateTime? LastUpdated { get; set; }

          [DataMember(Order = 3)]
          public bool IsValid { get; set; }

          [DataMember(Order = 4)]
          public bool PreviousImported { get; set; }

          #endregion

          #region entitymembers



          [DataMember(Order = 5)]
          public string TitleNietTerugbetaalbaar { get; set; }



          [DataMember(Order = 6)]
          public decimal? VVNTTotaal { get; set; }



          [DataMember(Order = 7)]
          public decimal? VVNTFictieveRoerendeVoorheffing { get; set; }



          [DataMember(Order = 8)]
          public decimal? VVNTForfaitairBuitenlands { get; set; }



          [DataMember(Order = 9)]
          public decimal? VVNTBelastingKredietOnderzoekOntw { get; set; }



          [DataMember(Order = 10)]
          public string TitleTerugbetaalbaar { get; set; }



          [DataMember(Order = 11)]
          public decimal? VVTTotaal { get; set; }



          [DataMember(Order = 12)]
          public decimal? VVTBelgisch { get; set; }



          [DataMember(Order = 13)]
          public decimal? VVTAndereBuitenlands { get; set; }



          [DataMember(Order = 14)]
          public decimal? VVTEigenBuitenlands { get; set; }



          [DataMember(Order = 15)]
          public decimal? VVTAndereEigen { get; set; }



          [DataMember(Order = 16)]
          public decimal? VVTAndereRoerendeVoorheffing { get; set; }



          [DataMember(Order = 17)]
          public decimal? VVTAndere { get; set; }



          [DataMember(Order = 18)]
          public decimal? VBelastingkredietHuiidTijdperk { get; set; }



          [DataMember(Order = 19)]
          public decimal? VVTTotaalExpected { get; set; }


          #endregion
      }

  
      [DataContract(Namespace = "AY2015.BerekeningVenBApp")]
      public class TariefItem : IEntity
      {
          #region IEntity
          
          [DataMember(Name = "ID", Order=1)]
          public Guid Id { get; set; }
          
          [DataMember(Order=2)]
          public DateTime? LastUpdated { get; set; }
          
          [DataMember(Order = 3)]
          public bool IsValid { get; set; }
          
          [DataMember(Order = 4)]
          public bool PreviousImported { get; set; }
          
          #endregion
          
          #region entitymembers
          

        
          [DataMember(Order = 5)]
          public decimal? TariefVennootschapsbelastingTitle {get;set;}


        
          [DataMember(Order = 6)]
          public bool? BezitAandelenMeerDan50Percent {get;set;}


        
          [DataMember(Order = 7)]
          public bool? BezitAandelenDoorVennootschappen {get;set;}


        
          [DataMember(Order = 8)]
          public bool? UitgekeerdDividendHogerDan13 {get;set;}


        
          [DataMember(Order = 9)]
          public bool? ToegekendeBezoldiging {get;set;}


          #endregion
      }


      [DataContract(Namespace = "AY2015.BerekeningVenBApp")]
      public class AanslagItem : IEntity
      {
          #region IEntity

          [DataMember(Name = "ID", Order = 1)]
          public Guid Id { get; set; }

          [DataMember(Order = 2)]
          public DateTime? LastUpdated { get; set; }

          [DataMember(Order = 3)]
          public bool IsValid { get; set; }

          [DataMember(Order = 4)]
          public bool PreviousImported { get; set; }

          #endregion  

          #region entitymembers


          [DataMember(Order = 5)]
          public decimal? KapitaalEnInterestsubsidies { get; set; }


          [DataMember(Order = 6)]
          public decimal? NietVerantwoordeKosten { get; set; }

          [DataMember(Order = 193)]
          public decimal? AfzonderlijkeNietVerantwoordeKosten50Procent { get; set; }

          [DataMember(Order = 194)]
          public decimal? AfzonderlijkeNietVerantwoordeKosten100Procent { get; set; }

          [DataMember(Order = 7)]
          public decimal? AfzonderlijkeAanslag34 { get; set; }


          [DataMember(Order = 8)]
          public decimal? AfzonderlijkeAanslag28 { get; set; }


          [DataMember(Order = 9)]
          public decimal? AfzonderlijkeAanslagDividenden { get; set; }

          [DataMember(Order = 21)]
          public decimal? AfzonderlijkeAanslagRV { get; set; }


          [DataMember(Order = 10)]
          public decimal? VerdelingVermogen33 { get; set; }


          [DataMember(Order = 11)]
          public decimal? VerdelingVermogen16 { get; set; }


          [DataMember(Order = 12)]
          public decimal? VAAVennootschappen { get; set; }


          [DataMember(Order = 13)]
          public decimal? AanvullendeHeffingDiamant { get; set; }


          [DataMember(Order = 14)]
          public decimal? TerugbetalingBelastingskrediet { get; set; }


          [DataMember(Order = 15)]
          public decimal? TotaalBedragVoorzieningen { get; set; }

        
          [DataMember(Order = 16)]
          public bool? AfzonderlijkeAanslagGespreid { get; set; }

          [DataMember(Order = 20)]
          public bool? Taxable2015 { get; set; }

          [DataMember(Order = 17)]
          public decimal? IPT2013 { get; set; }

          [DataMember(Order = 18)]
          public decimal? IPT2014 { get; set; }

          [DataMember(Order = 19)]
          public decimal? IPT2015 { get; set; }

          [DataMember(Order = 22)]
          public decimal? LiquidatieReserve { get; set; }

          

          #endregion
      }

     [DataContract(Namespace = "AY2015.BerekeningVenBApp")]
      public class FairnessTaxItem : IEntity
      {
          #region IEntity

          [DataMember(Name = "ID", Order = 1)]
          public Guid Id { get; set; }

          [DataMember(Order = 2)]
          public DateTime? LastUpdated { get; set; }

          [DataMember(Order = 3)]
          public bool IsValid { get; set; }

          [DataMember(Order = 4)]
          public bool PreviousImported { get; set; }

          #endregion  

          /* Fairness Tax */
          #region entitymembers

          [DataMember(Order = 5)]
          public decimal? Dividenden { get; set; }

          [DataMember(Order = 6)]
          public decimal? TaxableResult { get; set; }

          [DataMember(Order = 7)]
          public decimal? DiffDividendTaxable { get; set; }

          [DataMember(Order = 8)]
          public decimal? PreviousDividends { get; set; }

          [DataMember(Order = 9)]
          public decimal? NidApplied { get; set; }  // aanwnding NID

          [DataMember(Order = 10)]
          public decimal? PreviousLosses { get; set; } // aanwending Gecompenseerde verliezen

          [DataMember(Order = 11)]
          public decimal? TotalRestriction { get; set; } 

          [DataMember(Order = 12)]
          public decimal? ResultFirstOperation { get; set; }

          [DataMember(Order = 13)]
          public decimal? FairnessTaxRestrictionPerc { get; set; }

          [DataMember(Order = 14)]
          public decimal? FairnessTaxBase { get; set; }

          [DataMember(Order = 15)]
          public decimal? FairnessTaxPerc { get; set; }

          [DataMember(Order = 16)]
          public decimal? FairnessTaxResult { get; set; }

          #endregion
          /* END Fairness Tax */

         
      }

    [DataContract(Namespace = "AY2015.BerekeningVenBApp")]
     public class BerekeningOverzichtItem : IEntity
     {
         #region IEntity

         [DataMember(Name = "ID", Order = 1)]
         public Guid Id { get; set; }

         [DataMember(Order = 2)]
         public DateTime? LastUpdated { get; set; }

         [DataMember(Order = 3)]
         public bool IsValid { get; set; }

         [DataMember(Order = 4)]
         public bool PreviousImported { get; set; }

         #endregion

         #region entitymembers

         [DataMember(Order = 5)]
         public bool? Verminderd { get; set; }

          [DataMember(Order = 6)]
          public int? AssessmentYear {get;set;}


          [DataMember(Order = 8)]
          public decimal? StatAccountingResult { get; set; }

          [DataMember(Order = 9)]
          public decimal? TotalTaxableReserves { get; set; }

          [DataMember(Order = 10)]
          public decimal? TotalTaxableReservesPlus { get; set; }

          [DataMember(Order = 11)]
          public decimal? TotalTaxableReservesMinus { get; set; }

          [DataMember(Order = 12)]
          public decimal? TotalNonDeductibleTaxes { get; set; }

          [DataMember(Order = 13)]
          public decimal? TotalDissalowedExpensesWithoutNonDeductibleTax { get; set; }

          [DataMember(Order = 14)]
          public decimal? TaxableResult { get; set; }

          [DataMember(Order = 15)]
          public decimal? TotalDeductionLimit { get; set; } 
            
          [DataMember(Order = 16)]
          public decimal? RemainingTaxableResult { get; set; }

          [DataMember(Order = 17)]
          public decimal? TotalTaxTreaty { get; set; }

          [DataMember(Order = 18)]
          public decimal? TotalFiscalDeductions { get; set; }

          [DataMember(Order = 30)]
          public decimal? RemainingFiscalProfit { get; set; }

          [DataMember(Order = 31)]
          public decimal? CommonRateResult { get; set; }

          [DataMember(Order = 32)]
          public decimal? CommonRateRemainingFiscalProfit { get; set; }

          [DataMember(Order = 33)]
          public decimal? CommonRateDeductionLimit { get; set; }

          // MIN: ? for explanion of result commonrate (= CommonRateRemainingFiscalProfit + CommonRateDeductionLimit - CommonRateCapitalGains25)
          [DataMember(Order = 34)]
          public decimal? CommonRateCapitalGains25 { get; set; } 


          [DataMember(Order = 40)]
          public decimal? EenduidigTariefBedrag { get; set; }



          [DataMember(Order = 41)]
          public decimal? EenduidigTariefPerc { get; set; }



          [DataMember(Order = 42)]
          public decimal? EenduidigTariefTot { get; set; }



          [DataMember(Order = 43)]
          public decimal? VerminderdtariefStap1Base { get; set; }



          [DataMember(Order = 44)]
          public decimal? VerminderdtariefStap1Perc { get; set; }



          [DataMember(Order = 45)]
          public decimal? VerminderdtariefStap1 { get; set; }



          [DataMember(Order = 46)]
          public decimal? VerminderdtariefStap2Base { get; set; }



          [DataMember(Order = 47)]
          public decimal? VerminderdtariefStap2Perc { get; set; }



          [DataMember(Order = 48)]
          public decimal? VerminderdtariefStap2 { get; set; }



          [DataMember(Order = 49)]
          public decimal? VerminderdtariefStap3Base { get; set; }



          [DataMember(Order = 50)]
          public decimal? VerminderdtariefStap3Perc { get; set; }



          [DataMember(Order = 51)]
          public decimal? VerminderdtariefStap3 { get; set; }


          [DataMember(Order = 60)]
          public decimal? CapitalGainsShares25 { get; set; }

          [DataMember(Order = 61)]
          public decimal? CapitalGainsShares25Perc { get; set; }

          [DataMember(Order = 62)]
          public decimal? CapitalGainsShares25Result { get; set; }

          [DataMember(Order = 63)]
          public decimal? CapitalGainsShares0412 { get; set; }

          [DataMember(Order = 64)]
          public decimal? CapitalGainsShares0412Perc { get; set; }

          [DataMember(Order = 65)]
          public decimal? CapitalGainsShares0412Result { get; set; }


          [DataMember(Order = 66)]
          public decimal? ExitTarif { get; set; }

          [DataMember(Order = 67)]
          public decimal? ExitTarifPerc { get; set; }

          [DataMember(Order = 68)]
          public decimal? ExitTarifResult { get; set; }


          [DataMember(Order = 69)]
          public decimal? KapitaalEnInterestsubsidies { get; set; }

          [DataMember(Order = 70)]
          public decimal? KapitaalEnInterestsubsidiesPerc { get; set; }

          [DataMember(Order = 71)]
          public decimal? KapitaalEnInterestsubsidiesResult { get; set; }

          [DataMember(Order = 79)]
          public decimal? CorporateTaxBeforeRepayable { get; set; }


          [DataMember(Order = 80)]
          public decimal? NonRepayableAdvanceLevies { get; set; } //Niet terugbetaalbare voorheffingen.

          [DataMember(Order = 81)]
          public decimal? RepayableAdvanceLevies { get; set; } //Terugbetaalbare voorheffingen. 

          [DataMember(Order = 82)]
          public decimal? RepayableResearchDevelopment { get; set; } //TaxCreditResearchDevelopmentClearablePreviousTaxPeriodsRepayableCurrentAssessmentYear 

          [DataMember(Order = 90)]
          public decimal? RepayableTotal { get; set; }

          [DataMember(Order = 91)]
          public decimal? CorporateTaxAfterRepayable { get; set; }

          [DataMember(Order = 99)]
          public decimal? BaseForIncreaseFairnessTax { get; set; } // == FairnessTaxTotal
          
          [DataMember(Order = 100)]
          public decimal? BaseForIncrease { get; set; }

          [DataMember(Order = 101)]
          public decimal? BaseForIncreasePerc { get; set; }

          [DataMember(Order = 102)]
          public decimal? BaseForIncreaseTotal { get; set; }

          // VOORAFBETALINGEN

          [DataMember(Order = 110)]
          public decimal? AP1Base { get; set; }



          [DataMember(Order = 111)]
          public decimal? AP1Percentage { get; set; }



          [DataMember(Order = 112)]
          public decimal? AP1Total { get; set; }



          [DataMember(Order = 113)]
          public decimal? AP2Base { get; set; }



          [DataMember(Order = 114)]
          public decimal? AP2Percentage { get; set; }



          [DataMember(Order = 115)]
          public decimal? AP2Total { get; set; }



          [DataMember(Order = 116)]
          public decimal? AP3Base { get; set; }



          [DataMember(Order = 117)]
          public decimal? AP3Percentage { get; set; }


          [DataMember(Order = 118)]
          public decimal? AP3Total { get; set; }


          [DataMember(Order = 119)]
          public decimal? AP4Base { get; set; }


          [DataMember(Order = 120)]
          public decimal? AP4Percentage { get; set; }


          [DataMember(Order = 121)]
          public decimal? AP4Total { get; set; }

          [DataMember(Order = 122)]
          public decimal? APBaseTotal { get; set; }

          [DataMember(Order = 123)]
          public decimal? APTotal { get; set; }

          [DataMember(Order = 124)]
          public decimal? AdvancePayments { get; set; } // idem to ApBaseTotal

          [DataMember(Order = 125)]
          public decimal? Test1Percent { get; set; }

          [DataMember(Order = 126)]
          public decimal? BaseForIncreaseApplied { get; set; } // 0/NULL if not applicable, otherwise = BaseForIncreaseTotal

         // AFZONDERLIJKE AANSLAGEN
          //Niet-verantwoorde kosten of voordelen van alle aard, verdoken meerwinsten en financiële voordelen of voordelen van alle aard
          [DataMember(Order = 130)]
          public decimal? UnjustifiedExpensesHiddenSurplusBase { get; set; }

          [DataMember(Order = 131)]
          public decimal? UnjustifiedExpensesHiddenSurplusPerc { get; set; }

         [DataMember(Order = 132)]
         public decimal? UnjustifiedExpensesHiddenSurplusTotal { get; set; }

         //Afzonderlijke aanslag van de niet-verantwoorde kosten of voordelen van alle aard, de verdoken meerwinsten en de financiële voordelen of voordelen van alle aard, tegen 50%
         [DataMember(Order = 197)]
         public decimal? SeparateUnjustifiedExpensesHiddenSurplus50PercentBase { get; set; }

         [DataMember(Order = 198)]
         public decimal? SeparateUnjustifiedExpensesHiddenSurplus50PercentPerc { get; set; }

         [DataMember(Order = 199)]
         public decimal? SeparateUnjustifiedExpensesHiddenSurplus50PercentTotal { get; set; }

         //Afzonderlijke aanslag van de niet-verantwoorde kosten of voordelen van alle aard, de verdoken meerwinsten en de financiële voordelen of voordelen van alle aard, tegen 100%
         [DataMember(Order = 200)]
         public decimal? SeparateUnjustifiedExpensesHiddenSurplus100PercentBase { get; set; }

         [DataMember(Order = 201)]
         public decimal? SeparateUnjustifiedExpensesHiddenSurplus100PercentPerc { get; set; }

         [DataMember(Order = 202)]
         public decimal? SeparateUnjustifiedExpensesHiddenSurplus100PercentTotal { get; set; }

         //Afzonderlijke aanslag van de belaste reserves ten name van erkende kredietinstellingen, tegen het tarief van 34 %
        [DataMember(Order = 133)]
         public decimal? SeparateAssessmentTaxedReserves3400Base { get; set; }

         [DataMember(Order = 134)]
         public decimal? SeparateAssessmentTaxedReserves3400Perc { get; set; }

         [DataMember(Order = 135)]
         public decimal? SeparateAssessmentTaxedReserves3400Total { get; set; }

         //Afzonderlijke aanslag van de belaste reserves ten name van erkende kredietinstellingen, tegen het tarief van 28 %
         [DataMember(Order = 136)]
         public decimal? SeparateAssessmentTaxedReserves2800Base { get; set; }

         [DataMember(Order = 137)]
         public decimal? SeparateAssessmentTaxedReserves2800Perc { get; set; }

         [DataMember(Order = 138)]
         public decimal? SeparateAssessmentTaxedReserves2800Total { get; set; }


          //Afzonderlijke aanslag van de uitgekeerde dividenden ten name van vennootschappen die krediet voor ambachtsoutillage mogen verstrekken en vennootschappen voor huisvesting
         [DataMember(Order = 139)]
         public decimal? SeparateAssessmentDividendsPaidBase { get; set; }

         [DataMember(Order = 140)]
         public decimal? SeparateAssessmentDividendsPaidPerc { get; set; }

         [DataMember(Order = 141)]
         public decimal? SeparateAssessmentDividendsPaidTotal { get; set; }



          [DataMember(Order = 142)]
          public decimal? FairnessTaxBase { get; set; }

          [DataMember(Order = 143)]
          public decimal? FairnessTaxPerc { get; set; }

          [DataMember(Order = 144)]
          public decimal? FairnessTaxTotal { get; set; }

          //Afzonderlijke aanslag in het kader van de overgangsregeling van verlaagde roerende voorheffing op de uitkering van belaste reserves

          [DataMember(Order = 145)]
          public decimal? SeparateAssessmentTaxPaidTaxedReservesBase { get; set; }

          [DataMember(Order = 146)]
          public decimal? SeparateAssessmentTaxPaidTaxedReservesPerc { get; set; }

          [DataMember(Order = 147)]
          public decimal? SeparateAssessmentTaxPaidTaxedReservesTotal { get; set; }


         // Bijzondere aanslagen met betrekking tot verrichtingen die vóór 1.1.1990 hebben plaatsgevonden

          [DataMember(Order = 160)]
          public decimal? DistributionCompanyAssets3300Base { get; set; }

          [DataMember(Order = 161)]
          public decimal? DistributionCompanyAssets3300Perc { get; set; }

          [DataMember(Order = 162)]
          public decimal? DistributionCompanyAssets3300Total { get; set; }


          [DataMember(Order = 163)]
          public decimal? DistributionCompanyAssets1650Base { get; set; }

          [DataMember(Order = 164)]
          public decimal? DistributionCompanyAssets1650Perc { get; set; }

          [DataMember(Order = 165)]
          public decimal? DistributionCompanyAssets1650Total { get; set; }



          [DataMember(Order = 166)]
          public decimal? BenefitsAllKindCompanyLiquidationBase { get; set; }

          [DataMember(Order = 167)]
          public decimal? BenefitsAllKindCompanyLiquidationPerc { get; set; }

          [DataMember(Order = 168)]
          public decimal? BenefitsAllKindCompanyLiquidationTotal { get; set; }


         //Aanvullende heffing erkende diamanthandelaars, terugbetaling van voorheen verleend belastingkrediet voor onderzoek en ontwikkeling en afzonderlijke aanslag op de voorzieningen gevormd in uitvoering van aanvullende individuele pensioentoezeggingen
        // == 10% non crisis
          [DataMember(Order = 180)]
          public decimal? AdditionalDutiesDiamondTradersBase { get; set; }

          [DataMember(Order = 181)]
          public decimal? AdditionalDutiesDiamondTradersPerc { get; set; }

          [DataMember(Order = 182)]
          public decimal? AdditionalDutiesDiamondTradersTotal { get; set; }


          //Terugbetaling van een gedeelte van het voorheen verleende belastingkrediet voor onderzoek en ontwikkeling
          [DataMember(Order = 183)]
          public decimal? RetributionResearchDevelopmentBase { get; set; }

          [DataMember(Order = 184)]
          public decimal? RetributionResearchDevelopmentPerc { get; set; }

          [DataMember(Order = 185)]
          public decimal? RetributionResearchDevelopmentTotal { get; set; }


        // IPT
          [DataMember(Order = 186)]
          public decimal? IPTBase { get; set; }

          [DataMember(Order = 187)]
          public decimal? IPTPerc { get; set; }

          [DataMember(Order = 188)]
          public decimal? IPTTotal { get; set; }

          [DataMember(Order = 190)]
          public decimal? LiquidatieReserveBase { get; set; }

          [DataMember(Order = 191)]
          public decimal? LiquidatieReservePerc { get; set; }

          [DataMember(Order = 192)]
          public decimal? LiquidatieReserveTotal { get; set; }

          [DataMember(Order = 999)]
          public decimal? TaxResult { get; set; }
         #endregion
     }
  
      [DataContract(Namespace = "AY2015.BerekeningVenBApp")]
      public class BelastingsBerekeningItem : IEntity
      {
          #region IEntity
          
          [DataMember(Name = "ID", Order=1)]
          public Guid Id { get; set; }
          
          [DataMember(Order=2)]
          public DateTime? LastUpdated { get; set; }
          
          [DataMember(Order = 3)]
          public bool IsValid { get; set; }
          
          [DataMember(Order = 4)]
          public bool PreviousImported { get; set; }
          
          #endregion
          
          #region entitymembers
          

        
          [DataMember(Order = 5)]
          public string LabelCALCULATIONOTCORPORATEINCOMETAX {get;set;}


        
          [DataMember(Order = 6)]
          public int? AssessmentYear {get;set;}


        
        
          [DataMember(Order = 7)]
          public decimal? CorporateIncomeTaxFromResult {get;set;}

         
          //68
          /* MEERWAARDE AANDELEN Tarief 0,412%*/
          [DataMember(Order = 85)]
          public decimal? CapitalGainsShares0412 { get; set; }

          [DataMember(Order = 86)]
          public decimal? CapitalGainsShares0412Perc { get; set; }

          [DataMember(Order = 87)]
          public decimal? CapitalGainsShares0412Result { get; set; }

          /* END MEERWAARDE AANDELEN Tarief 0,412%**/

          [DataMember(Order = 68)]
          public decimal? CapitalGainsShares25 { get; set; }

          [DataMember(Order = 69)]
          public decimal? CapitalGainsShares25Perc { get; set; }

          [DataMember(Order = 70)]
          public decimal? CapitalGainsShares25Result { get; set; }

          [DataMember(Order = 71)]
          public decimal? ExitTarif { get; set; }

          [DataMember(Order = 72)]
          public decimal? ExitTarifPerc { get; set; }

          [DataMember(Order = 73)]
          public decimal? ExitTarifResult { get; set; }

         
          [DataMember(Order = 74)]
          public decimal? KapitaalEnInterestsubsidies { get; set; }


          [DataMember(Order = 75)]
          public decimal? NietVerantwoordeKosten { get; set; }

          [DataMember(Order = 195)]
          public decimal? AfzonderlijkeNietVerantwoordeKosten50Procent { get; set; }

          [DataMember(Order = 196)]
          public decimal? AfzonderlijkeNietVerantwoordeKosten100Procent { get; set; }


          [DataMember(Order = 76)]
          public decimal? AfzonderlijkeAanslag34 { get; set; }


          [DataMember(Order = 77)]
          public decimal? AfzonderlijkeAanslag28 { get; set; }


          [DataMember(Order = 78)]
          public decimal? AfzonderlijkeAanslagDividenden { get; set; }

          [DataMember(Order = 90)]
          public decimal? AfzonderlijkeAanslagRV { get; set; }

          [DataMember(Order = 79)]
          public decimal? VerdelingVermogen33 { get; set; }


          [DataMember(Order = 80)]
          public decimal? VerdelingVermogen16 { get; set; }


          [DataMember(Order = 81)]
          public decimal? VAAVennootschappen { get; set; }


          [DataMember(Order = 82)]
          public decimal? AanvullendeHeffingDiamant { get; set; }


          [DataMember(Order = 83)]
          public decimal? TerugbetalingBelastingskrediet { get; set; }


          [DataMember(Order = 84)]
          public decimal? TotaalBedragVoorzieningen { get; set; }

          /*
          [DataMember(Order = 74)]
          public decimal? StandardIncomeTax { get; set; }

          [DataMember(Order = 75)]
          public string Spacer8 { get; set; }
           */

        
          [DataMember(Order = 8)]
          public string EenduigTarief {get;set;}


        
          [DataMember(Order = 9)]
          public decimal? EenduidigTariefBedrag {get;set;}


        
          [DataMember(Order = 10)]
          public decimal? EenduidigTariefPerc {get;set;}


        
          [DataMember(Order = 11)]
          public decimal? EenduidigTariefTot {get;set;}


        
          [DataMember(Order = 12)]
          public decimal? VerminderdtariefStap1Base {get;set;}


        
          [DataMember(Order = 13)]
          public decimal? VerminderdtariefStap1Perc {get;set;}


        
          [DataMember(Order = 14)]
          public decimal? VerminderdtariefStap1 {get;set;}


        
          [DataMember(Order = 15)]
          public decimal? VerminderdtariefStap2Base {get;set;}


        
          [DataMember(Order = 16)]
          public decimal? VerminderdtariefStap2Perc {get;set;}


        
          [DataMember(Order = 17)]
          public decimal? VerminderdtariefStap2 {get;set;}


        
          [DataMember(Order = 18)]
          public decimal? VerminderdtariefStap3Base {get;set;}


        
          [DataMember(Order = 19)]
          public decimal? VerminderdtariefStap3Perc {get;set;}


        
          [DataMember(Order = 20)]
          public decimal? VerminderdtariefStap3 {get;set;}


          
        
          [DataMember(Order = 21)]
          public decimal? CorporateIncomeTax {get;set;}


        
          [DataMember(Order = 22)]
          public decimal? NonRefundableWithholdingTaxes_TaxCredit {get;set;}


        
          [DataMember(Order = 23)]
          public decimal? RefundableTaxeCredit {get;set;}


        
          [DataMember(Order = 24)]
          public decimal? RefundableWithholdingTaxes {get;set;}

          [DataMember(Order = 89)]
          public decimal? FairnessTaxForIncrease { get; set; }
        
          [DataMember(Order = 25)]
          public decimal? BasisForIncrease {get;set;}


        
          [DataMember(Order = 26)]
          public bool? JongVennootschap {get;set;}


        
          [DataMember(Order = 27)]
          public string BasisTaxIncreaseInsufficientAdvancePayments {get;set;}


        
          [DataMember(Order = 28)]
          public decimal? BTIBase {get;set;}


        
          [DataMember(Order = 29)]
          public decimal? BTIPercentage {get;set;}


        
          [DataMember(Order = 30)]
          public decimal? BTITotal {get;set;}


        
          [DataMember(Order = 31)]
          public string AdvancePayments {get;set;}


        
          [DataMember(Order = 32)]
          public decimal? AP1Base {get;set;}


        
          [DataMember(Order = 33)]
          public decimal? AP1Percentage {get;set;}


        
          [DataMember(Order = 34)]
          public decimal? AP1Total {get;set;}


        
          [DataMember(Order = 35)]
          public decimal? AP2Base {get;set;}


        
          [DataMember(Order = 36)]
          public decimal? AP2Percentage {get;set;}


        
          [DataMember(Order = 37)]
          public decimal? AP2Total {get;set;}


        
          [DataMember(Order = 38)]
          public decimal? AP3Base {get;set;}


        
          [DataMember(Order = 39)]
          public decimal? AP3Percentage {get;set;}


        
          [DataMember(Order = 40)]
          public decimal? AP3Total {get;set;}


        
          [DataMember(Order = 41)]
          public decimal? AP4Base {get;set;}


        
          [DataMember(Order = 42)]
          public decimal? AP4Percentage {get;set;}


        
          [DataMember(Order = 43)]
          public decimal? AP4Total {get;set;}


        
          [DataMember(Order = 44)]
          public decimal? APBaseTotal {get;set;}


        
          [DataMember(Order = 45)]
          public string Spacer1 {get;set;}


        
          [DataMember(Order = 46)]
          public decimal? APTotal {get;set;}


        
          [DataMember(Order = 47)]
          public decimal? Test1percRule {get;set;}


        
          [DataMember(Order = 48)]
          public string Spacer24 {get;set;}


        
          [DataMember(Order = 49)]
          public decimal? TaxIncreaseInsufficientAdvancePayments {get;set;}


        
          [DataMember(Order = 50)]
          public string labelTotalCorporateTaxDue {get;set;}


        
          [DataMember(Order = 51)]
          public string Spacer18 {get;set;}


        
          [DataMember(Order = 52)]
          public decimal? BasisCorporateIncomeTax {get;set;}


        
          [DataMember(Order = 53)]
          public string Spacer2 {get;set;}


        
          [DataMember(Order = 54)]
          public decimal? ApplicableTaxIncrease {get;set;}


        
          [DataMember(Order = 55)]
          public string Spacer3 {get;set;}


        
          [DataMember(Order = 56)]
          public decimal? NonRefundableWithholdingTaxes_ForeignTaxCredit {get;set;}


        
          [DataMember(Order = 57)]
          public string Spacer4 {get;set;}


        
          [DataMember(Order = 58)]
          public decimal? RefundableTaxCredit {get;set;}


        
          [DataMember(Order = 59)]
          public string Spacer4B {get;set;}


        
          [DataMember(Order = 60)]
          public decimal? RefundableWithholdingTaxesEnd {get;set;}


        
          [DataMember(Order = 61)]
          public string Spacer5 {get;set;}


        
          [DataMember(Order = 62)]
          public decimal? AdvancePaymentsEnd {get;set;}


        
          [DataMember(Order = 63)]
          public string Spacer6 {get;set;}


        
          [DataMember(Order = 64)]
          public decimal? TaxCreditResearchDevelopment {get;set;}

          [DataMember(Order = 88)]
          public decimal? FairnessTax { get; set; }

        
          [DataMember(Order = 65)]
          public string Spacer7 {get;set;}


        
          [DataMember(Order = 66)]
          public decimal? OutstandingLiability_BelastingsBerekening {get;set;}


        
          [DataMember(Order = 67)]
          public bool? Verminderd {get;set;}


          #endregion
      }

  
      [DataContract(Namespace = "AY2015.BerekeningVenBApp")]
      public class BelastingsBerekeningItem2 : IEntity
      {
          #region IEntity
          
          [DataMember(Name = "ID", Order=1)]
          public Guid Id { get; set; }
          
          [DataMember(Order=2)]
          public DateTime? LastUpdated { get; set; }
          
          [DataMember(Order = 3)]
          public bool IsValid { get; set; }
          
          [DataMember(Order = 4)]
          public bool PreviousImported { get; set; }
          
          #endregion
          
          #region entitymembers
          

        
          [DataMember(Order = 5)]
          public string LabelCARRYFORWARDNOTIONALINTREST {get;set;}


        
          [DataMember(Order = 6)]
          public string LabelCarryForwardNotDeductOverdraagbareBestanddelen {get;set;}


        
          [DataMember(Order = 7)]
          public decimal? NIDYearMin7 {get;set;}


        
          [DataMember(Order = 8)]
          public string Spacer23 {get;set;}


        
          [DataMember(Order = 9)]
          public decimal? NIDYearMin6 {get;set;}


        
          [DataMember(Order = 10)]
          public string Spacer22 {get;set;}


        
          [DataMember(Order = 11)]
          public decimal? NIDYearMin5 {get;set;}


        
          [DataMember(Order = 12)]
          public string Spacer21 {get;set;}


        
          [DataMember(Order = 13)]
          public decimal? NIDYearMin4 {get;set;}


        
          [DataMember(Order = 14)]
          public string Spacer20 {get;set;}


        
          [DataMember(Order = 15)]
          public decimal? NIDYearMin3 {get;set;}


        
          [DataMember(Order = 16)]
          public string Spacer19 {get;set;}


        
          [DataMember(Order = 17)]
          public decimal? NIDYearMin2 {get;set;}


        
          [DataMember(Order = 18)]
          public string Spacer8 {get;set;}


        
          [DataMember(Order = 19)]
          public decimal? NIDYearMin1 {get;set;}


        
          [DataMember(Order = 20)]
          public string Spacer9 {get;set;}


        
          [DataMember(Order = 21)]
          public decimal? NIDCurrentYear {get;set;}


        
          [DataMember(Order = 22)]
          public string Spacer10 {get;set;}


        
          [DataMember(Order = 23)]
          public decimal? UtilizedNID {get;set;}


        
          [DataMember(Order = 24)]
          public string Spacer11 {get;set;}


        
          [DataMember(Order = 25)]
          public decimal? NIDAvailable {get;set;}


        
          [DataMember(Order = 26)]
          public string LabelCarryForwardTaxLosses {get;set;}


        
          [DataMember(Order = 27)]
          public decimal? CarryFwdTaxLossesAsPer {get;set;}


        
          [DataMember(Order = 28)]
          public string Spacer12 {get;set;}


        
          [DataMember(Order = 29)]
          public decimal? UtilizedTaxLosses {get;set;}


        
          [DataMember(Order = 30)]
          public string Spacer13 {get;set;}


        
          [DataMember(Order = 31)]
          public decimal? CurrentYearTaxLosses {get;set;}


        
          [DataMember(Order = 32)]
          public string Spacer14 {get;set;}


        
          [DataMember(Order = 33)]
          public decimal? CarryFwdTaxLossesAsPerTotal {get;set;}


        
          [DataMember(Order = 34)]
          public string LabelInvestmentDeduction {get;set;}


        
          [DataMember(Order = 35)]
          public decimal? IDCarryFwdTaxLossesAsPer {get;set;}


        
          [DataMember(Order = 36)]
          public string Spacer15 {get;set;}


        
          [DataMember(Order = 37)]
          public decimal? UtilizedCarryFwd {get;set;}


        
          [DataMember(Order = 38)]
          public string Spacer16 {get;set;}


        
          [DataMember(Order = 39)]
          public decimal? CurrentYearInvestment {get;set;}


        
          [DataMember(Order = 40)]
          public string Spacer17 {get;set;}


        
          [DataMember(Order = 41)]
          public decimal? IDCarryFwdTaxLossesAsPerTotal {get;set;}


        
          [DataMember(Order = 42)]
          public string Verminderd {get;set;}


          #endregion
      }

  

    #endregion

    #region roottype
    

      [DataContract(Namespace = "AY2015.BerekeningVenBApp")]
      public class BerekeningVenB : IWorksheet
      {
          public BerekeningVenB() {
              // Set Minimal
             //this.Overzicht = new EY.com.eBook.RuleEngine.AY2015.BerekeningVenBAppTypes.General();
             // this.Overzicht.Id = Guid.NewGuid();
        
              this.BelasteReserves = new EY.com.eBook.RuleEngine.AY2015.BerekeningVenBAppTypes.BelasteReservesItem();
              this.BelasteReserves.Id = Guid.NewGuid();
        
              this.VerworpenUitgaven = new EY.com.eBook.RuleEngine.AY2015.BerekeningVenBAppTypes.VerworpenUitgavenItem();
              this.VerworpenUitgaven.Id = Guid.NewGuid();
        
              this.BelastbareBestZonderAftrek = new EY.com.eBook.RuleEngine.AY2015.BerekeningVenBAppTypes.BelastbareBestZonderAftrekItem();
              this.BelastbareBestZonderAftrek.Id = Guid.NewGuid();
        
              this.OpdelingNaarOorsprong = new EY.com.eBook.RuleEngine.AY2015.BerekeningVenBAppTypes.OpdelingNaarOorsprongItem();
              this.OpdelingNaarOorsprong.Id = Guid.NewGuid();
        
              this.FiscaleAftrek = new EY.com.eBook.RuleEngine.AY2015.BerekeningVenBAppTypes.FiscaleAftrekItem();
              this.FiscaleAftrek.Id = Guid.NewGuid();
        
              this.Tarief = new EY.com.eBook.RuleEngine.AY2015.BerekeningVenBAppTypes.TariefItem();
              this.Tarief.Id = Guid.NewGuid();

              this.Aanslag = new EY.com.eBook.RuleEngine.AY2015.BerekeningVenBAppTypes.AanslagItem();
              this.Aanslag.Id = Guid.NewGuid();
              // default value
              this.Aanslag.Taxable2015 = true;

              this.FairnessTax = new EY.com.eBook.RuleEngine.AY2015.BerekeningVenBAppTypes.FairnessTaxItem();
              this.FairnessTax.Id = Guid.NewGuid();

              this.VerrekenbareVoorheffing = new EY.com.eBook.RuleEngine.AY2015.BerekeningVenBAppTypes.VerrekenbareVoorheffingEntity();
              this.VerrekenbareVoorheffing.Id = Guid.NewGuid();
        
              /*
              this.BelastingsBerekening = new EY.com.eBook.RuleEngine.AY2015.BerekeningVenBAppTypes.BelastingsBerekeningItem();
              this.BelastingsBerekening.Id = Guid.NewGuid();
        
              this.BelastingsBerekening2 = new EY.com.eBook.RuleEngine.AY2015.BerekeningVenBAppTypes.BelastingsBerekeningItem2();
              this.BelastingsBerekening2.Id = Guid.NewGuid();
              
              */
              this.BerekeningOverzicht = new EY.com.eBook.RuleEngine.AY2015.BerekeningVenBAppTypes.BerekeningOverzichtItem();
              this.BerekeningOverzicht.Id = Guid.NewGuid();
              this.LastUpdated = DateTime.Now;
          }
        
          
          #region entitymembers
          

        
          //[DataMember(Order = 1)]
          //public EY.com.eBook.RuleEngine.AY2015.BerekeningVenBAppTypes.General Overzicht {get;set;}


        
          [DataMember(Order = 2)]
          public EY.com.eBook.RuleEngine.AY2015.BerekeningVenBAppTypes.BelasteReservesItem BelasteReserves {get;set;}


        
          [DataMember(Order = 3)]
          public EY.com.eBook.RuleEngine.AY2015.BerekeningVenBAppTypes.VerworpenUitgavenItem VerworpenUitgaven {get;set;}


        
          [DataMember(Order = 4)]
          public EY.com.eBook.RuleEngine.AY2015.BerekeningVenBAppTypes.BelastbareBestZonderAftrekItem BelastbareBestZonderAftrek {get;set;}


        
          [DataMember(Order = 5)]
          public EY.com.eBook.RuleEngine.AY2015.BerekeningVenBAppTypes.OpdelingNaarOorsprongItem OpdelingNaarOorsprong {get;set;}


        
          [DataMember(Order = 6)]
          public EY.com.eBook.RuleEngine.AY2015.BerekeningVenBAppTypes.FiscaleAftrekItem FiscaleAftrek {get;set;}

          [DataMember(Order = 15)]
          public EY.com.eBook.RuleEngine.AY2015.BerekeningVenBAppTypes.VerrekenbareVoorheffingEntity VerrekenbareVoorheffing { get; set; }
        
          [DataMember(Order = 7)]
          public EY.com.eBook.RuleEngine.AY2015.BerekeningVenBAppTypes.TariefItem Tarief {get;set;}


          [DataMember(Order = 12)]
          public EY.com.eBook.RuleEngine.AY2015.BerekeningVenBAppTypes.FairnessTaxItem FairnessTax { get; set; }

           [DataMember(Order = 14)]
          public EY.com.eBook.RuleEngine.AY2015.BerekeningVenBAppTypes.BerekeningOverzichtItem BerekeningOverzicht { get; set; }

          
          /*
        
          [DataMember(Order = 8)]
          public EY.com.eBook.RuleEngine.AY2015.BerekeningVenBAppTypes.BelastingsBerekeningItem BelastingsBerekening {get;set;}


        
          [DataMember(Order = 9)]
          public EY.com.eBook.RuleEngine.AY2015.BerekeningVenBAppTypes.BelastingsBerekeningItem2 BelastingsBerekening2 {get;set;}
          */

      
          #endregion
          
          #region IWorksheet Members
          [DataMember(Order = 10)]
          public DateTime? LastUpdated { get; set; }

          [DataMember(Order = 11)]
          public EY.com.eBook.RuleEngine.AY2015.BerekeningVenBAppTypes.AanslagItem Aanslag { get; set; }
          #endregion
      }

  
   
    #endregion

}
  