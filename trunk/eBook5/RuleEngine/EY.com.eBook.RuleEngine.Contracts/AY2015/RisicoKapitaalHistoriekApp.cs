﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.RuleEngine.Core;
using EY.com.eBook.RuleEngine.Core.Interfaces;
using EY.com.eBook.RuleEngine.Data;


namespace EY.com.eBook.RuleEngine.AY2015.RisicoKapitaalHistoriekAppTypes
{
    #region subtypes

    [DataContract(Namespace = "AY2015.RisicoKapitaalHistoriekApp")]
    public class HistoryItem : IEntity
    {
        #region IEntity

        [DataMember(Name = "ID", Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public DateTime? LastUpdated { get; set; }

        [DataMember(Order = 3)]
        public bool IsValid { get; set; }

        [DataMember(Order = 4)]
        public bool PreviousImported { get; set; }

        #endregion

        #region entitymembers

        [DataMember(Order = 5)]
        public decimal? OnbeperktOverdraagbaarSaldo { get; set; }

        [DataMember(Order = 6)]
        public decimal? BeperktOverdraagbaarSaldo { get; set; }

        [DataMember(Order = 7)]
        public StringListItemDataContract BerekendInAJ { get; set; }

        [DataMember(Order = 8)]
        public string BerekendInAJString { get; set; }

        [DataMember(Order = 9)]
        public string Overdraagbaar7Jaar { get; set; }

       

        [DataMember(Order = 10)]
        public decimal? GebruiktHuidigAJ { get; set; }

        [DataMember(Order = 11)]
        public decimal? BeperktOverdraagbaarVolgendAJ { get; set; }

        [DataMember(Order = 12)]
        public decimal? OnbeperktOverdraagbaarVolgendAJ { get; set; }

        [DataMember(Order = 13)]
        public decimal? OorspronkelijkSaldoBeperkt { get; set; }

        [DataMember(Order = 14)]
        public decimal? OorspronkelijkSaldoOnbeperkt { get; set; }
 
        [DataMember(Order = 15)]
        public decimal? VervallenHuidigAJ { get; set; }

        [DataMember(Order = 16)]
        public decimal? VervallenVolgendAJ { get; set; }

        #endregion
    }
    
    [DataContract(Namespace = "AY2015.RisicoKapitaalHistoriekApp")]
    public class HistoriekAanpassingItem : IEntity
    {
        #region IEntity

        [DataMember(Name = "ID", Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public DateTime? LastUpdated { get; set; }

        [DataMember(Order = 3)]
        public bool IsValid { get; set; }

        [DataMember(Order = 4)]
        public bool PreviousImported { get; set; }

        #endregion

        #region entitymembers



        [DataMember(Order = 5)]
        public StringListItemDataContract AssessmentYear { get; set; }



        [DataMember(Order = 6)]
        public decimal? BedragAftrekBeperkt { get; set; }

        [DataMember(Order = 7)]
        public decimal? BedragAftrekOnbeperkt { get; set; }

        [DataMember(Order = 8)]
        public string Notitie { get; set; }


        #endregion
    }

    [DataContract(Namespace = "AY2015.RisicoKapitaalHistoriekApp")]
    public class FicheItem : IEntity
    {
        #region IEntity

        [DataMember(Name = "ID", Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public DateTime? LastUpdated { get; set; }

        [DataMember(Order = 3)]
        public bool IsValid { get; set; }

        [DataMember(Order = 4)]
        public bool PreviousImported { get; set; }

        #endregion

        #region entitymembers



        [DataMember(Order = 5)]
        public decimal? SaldoBelgischResterend { get; set; }



        [DataMember(Order = 6)]
        public decimal? SaldoNietVerdragVrijgRestWinst { get; set; }



        [DataMember(Order = 7)]
        public decimal? EigenVermogen { get; set; }

        [DataMember(Order = 37)]
        public string LabelCorrectiesEigenVermogen { get; set; }

        [DataMember(Order = 8)]
        public decimal? EigenAandelen { get; set; }



        [DataMember(Order = 9)]
        public decimal? FinancVasteActiva { get; set; }



        [DataMember(Order = 10)]
        public decimal? AandelenDBI { get; set; }





        [DataMember(Order = 13)]
        public decimal? MvaOnredelijk { get; set; }



        [DataMember(Order = 14)]
        public decimal? BestanddelenBelegging { get; set; }



        [DataMember(Order = 15)]
        public decimal? OnroerendeBedrijfsleiders { get; set; }



        [DataMember(Order = 16)]
        public decimal? UitgedrukteMeerwaarden { get; set; }



        [DataMember(Order = 17)]
        public decimal? Belastingkrediet { get; set; }



        [DataMember(Order = 18)]
        public decimal? KapitaalSubsidies { get; set; }



        [DataMember(Order = 19)]
        public decimal? Voorraadactualisering { get; set; }



        [DataMember(Order = 20)]
        public decimal? Hoofdzetel { get; set; }



        [DataMember(Order = 21)]
        public decimal? BelgischeInrichting { get; set; }



        [DataMember(Order = 22)]
        public decimal? Totaal { get; set; }



        [DataMember(Order = 23)]
        public decimal? PositieveWijzigingen { get; set; }



        [DataMember(Order = 24)]
        public decimal? NegatieveWijzigingen { get; set; }



        [DataMember(Order = 25)]
        public decimal? RisicoKapitaalBelastbaar { get; set; }



        [DataMember(Order = 26)]
        public decimal? RisicoKapitaalBelastbaarOverrule { get; set; }



        [DataMember(Order = 27)]
        public bool? TariefKleineVennootschap { get; set; }



        [DataMember(Order = 28)]
        public decimal? NotioneelPercentage { get; set; }



        [DataMember(Order = 29)]
        public decimal? Aftrekbaar { get; set; }



        [DataMember(Order = 30)]
        public decimal? AftrekbaarOverrule { get; set; }



        [DataMember(Order = 31)]
        public decimal? WerkelijkAfgetrokken { get; set; }



        /* new fiche AY14     (update G.)    
        [DataMember(Order = 32)]
        public decimal? OverdraagbaarSaldo { get; set; }
         
        [DataMember(Order = 33)]
        public decimal? OverdraagbaarSaldoOverrule { get; set; }
        */
        [DataMember(Order = 32)]
        public decimal? GevormdeVrijstellingen { get; set; }

              
        

        /* new fiche AY14  (update H.)
        [DataMember(Order = 34)]
        public decimal? SaldoVorigeAJGevormd { get; set; }
        */
        [DataMember(Order = 33)]
        public decimal? WerkelijkAfgetrokkenVrijstellingen { get; set; }

        [DataMember(Order = 42)]
        public decimal? SaldoBeperking60Perc { get; set; }

        [DataMember(Order = 43)]
        public decimal? SaldoVervallendHuidigAJ { get; set; }

        /* new fiche AY14 (update I.)
        [DataMember(Order = 35)]
        public decimal? WerkelijkVorigAJ { get; set; }
        */
        [DataMember(Order = 34)]
        public decimal? SaldoOvergedragenVrijstellingen { get; set; }


        /* new fiche AY14 (J. valt weg)
        [DataMember(Order = 36)]
        public decimal? TotaalAftrekAJ { get; set; }
        


        [DataMember(Order = 37)]
        public decimal? TotaalAftrekAJOverrule { get; set; }
        */


        /* new fiche AY14 (K. valt weg)
        [DataMember(Order = 38)]
        public decimal? SaldoRisicoOverdraagbaarForm { get; set; }
        


        [DataMember(Order = 39)]
        public decimal? SaldoRisicoOverdraagbaarOverrule { get; set; }



        [DataMember(Order = 40)]
        public decimal? SaldoRisicoOverdraagbaar { get; set; }
        */


        [DataMember(Order = 35)]
        public decimal? BelastbareBasisResterend { get; set; }



        [DataMember(Order = 36)]
        public decimal? Aanwending { get; set; }

        [DataMember(Order = 38)]
        public decimal? WijzigingenCumul { get; set; }

        [DataMember(Order = 39)]
        public decimal? VerminderingInEER { get; set; }

        [DataMember(Order = 40)]
        public decimal? VerminderingBuitenEER { get; set; }

        [DataMember(Order = 41)]
        public decimal? AftrekbaarNaAftrek { get; set; }


        #endregion
    }


    



    #endregion

    #region roottype


    [DataContract(Namespace = "AY2015.RisicoKapitaalHistoriekApp")]
    public class WPRisicokapitaalHistoriek : IWorksheet
    {
        public WPRisicokapitaalHistoriek()
        {
            // Set Minimal


            this.Fiche = new EY.com.eBook.RuleEngine.AY2015.RisicoKapitaalHistoriekAppTypes.FicheItem();
            this.Fiche.Id = Guid.NewGuid();
            /*
            this.EigenVermogens = new List<EY.com.eBook.RuleEngine.AY2015.RisicoKapitaalHistoriekAppTypes.EigenVermogen>();


            this.Bestanddelen = new List<EY.com.eBook.RuleEngine.AY2015.RisicoKapitaalHistoriekAppTypes.Bestanddeel>();


            this.Wijzigingen = new List<EY.com.eBook.RuleEngine.AY2015.RisicoKapitaalHistoriekAppTypes.WijzigingItem>();


            this.Tarief = new EY.com.eBook.RuleEngine.AY2015.RisicoKapitaalHistoriekAppTypes.TariefItem();
            this.Tarief.Id = Guid.NewGuid();
            */
            this.Historiek = new List<EY.com.eBook.RuleEngine.AY2015.RisicoKapitaalHistoriekAppTypes.HistoryItem>();


            this.AanpassingenHistoriek = new List<EY.com.eBook.RuleEngine.AY2015.RisicoKapitaalHistoriekAppTypes.HistoriekAanpassingItem>();

            this.LastUpdated = DateTime.Now;
        }


        #region entitymembers

        [DataMember(Order = 1)]
        public List<EY.com.eBook.RuleEngine.AY2015.RisicoKapitaalHistoriekAppTypes.HistoryItem> Historiek { get; set; }



        [DataMember(Order = 2)]
        public List<EY.com.eBook.RuleEngine.AY2015.RisicoKapitaalHistoriekAppTypes.HistoriekAanpassingItem> AanpassingenHistoriek { get; set; }



        [DataMember(Order = 3)]
        public EY.com.eBook.RuleEngine.AY2015.RisicoKapitaalHistoriekAppTypes.FicheItem Fiche { get; set; }



        

        

        #endregion

        #region IWorksheet Members
        [DataMember(Order = 4)]
        public DateTime? LastUpdated { get; set; }

        #endregion
    }



    #endregion

}
