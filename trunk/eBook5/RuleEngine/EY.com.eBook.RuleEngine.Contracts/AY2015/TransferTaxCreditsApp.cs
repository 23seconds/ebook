
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.RuleEngine.Core;
using EY.com.eBook.RuleEngine.Core.Interfaces;
using EY.com.eBook.RuleEngine.Data;

    
namespace EY.com.eBook.RuleEngine.AY2015.TransferTaxCreditsAppTypes
{
    #region subtypes


      [DataContract(Namespace = "AY2015.TransferTaxCreditsApp")]
      public class FiscaalVerlies : IEntity
      {
          #region IEntity
          
          [DataMember(Name = "ID", Order=1)]
          public Guid Id { get; set; }
          
          [DataMember(Order=2)]
          public DateTime? LastUpdated { get; set; }
          
          [DataMember(Order = 3)]
          public bool IsValid { get; set; }
          
          [DataMember(Order = 4)]
          public bool PreviousImported { get; set; }
          
          #endregion
          
          #region entitymembers
          

          [DataMember(Order = 5)]
          public int? Order {get;set;}


          [DataMember(Order = 6)]
          public string Type {get;set;}


          [DataMember(Order = 7)]
          public string Omschrijving {get;set;}

     
          [DataMember(Order = 8)]
          public DateTime? Date {get;set;}


        
          [DataMember(Order = 9)]
          public decimal? BedragHuidigForm {get;set;}


        
          [DataMember(Order = 10)]
          public decimal? BedragHuidig {get;set;}


        
          [DataMember(Order = 11)]
          public decimal? Cumul {get;set;}


        
          [DataMember(Order = 12)]
          public bool? Disabled {get;set;}


          #endregion
      }

  
    /*  [DataContract(Namespace = "AY2015.TransferTaxCreditsApp")]
      public class NIDItem : IEntity
      {
          #region IEntity
          
          [DataMember(Name = "ID", Order=1)]
          public Guid Id { get; set; }
          
          [DataMember(Order=2)]
          public DateTime? LastUpdated { get; set; }
          
          [DataMember(Order = 3)]
          public bool IsValid { get; set; }
          
          [DataMember(Order = 4)]
          public bool PreviousImported { get; set; }
          
          #endregion
          
          #region entitymembers
          

        
          [DataMember(Order = 5)]
          public int? Aanslagjaar {get;set;}


        
          [DataMember(Order = 6)]
          public decimal? BedragBeginBJ {get;set;}


        
          [DataMember(Order = 7)]
          public decimal? Aanwending {get;set;}


        
          [DataMember(Order = 8)]
          public decimal? BedragEindeBJ {get;set;}


          #endregion
      }
    */
  
      [DataContract(Namespace = "AY2015.TransferTaxCreditsApp")]
      public class DBIItem : IEntity
      {
          #region IEntity
          
          [DataMember(Name = "ID", Order=1)]
          public Guid Id { get; set; }
          
          [DataMember(Order=2)]
          public DateTime? LastUpdated { get; set; }
          
          [DataMember(Order = 3)]
          public bool IsValid { get; set; }
          
          [DataMember(Order = 4)]
          public bool PreviousImported { get; set; }
          
          #endregion
          
          #region entitymembers
          

        
          [DataMember(Order = 5)]
          public decimal? SaldoStart {get;set;}


        
          [DataMember(Order = 6)]
          public decimal? ToevoegingHuidigBJ {get;set;}


        
          [DataMember(Order = 7)]
          public decimal? Aanwending {get;set;}


        
          [DataMember(Order = 8)]
          public decimal? SaldoEinde {get;set;}


          #endregion
      }

  
      [DataContract(Namespace = "AY2015.TransferTaxCreditsApp")]
      public class InvestItem : IEntity
      {
          #region IEntity
          
          [DataMember(Name = "ID", Order=1)]
          public Guid Id { get; set; }
          
          [DataMember(Order=2)]
          public DateTime? LastUpdated { get; set; }
          
          [DataMember(Order = 3)]
          public bool IsValid { get; set; }
          
          [DataMember(Order = 4)]
          public bool PreviousImported { get; set; }
          
          #endregion
          
          #region entitymembers
          

        
          [DataMember(Order = 5)]
          public int? Order {get;set;}


        
          [DataMember(Order = 6)]
          public GuidListItemDataContract Type {get;set;}


        
          [DataMember(Order = 7)]
          public string AutoKey {get;set;}


        
          [DataMember(Order = 8)]
          public GuidListItemDataContract OmzettingType {get;set;}


        
          [DataMember(Order = 9)]
          public string Omschrijving {get;set;}


        
          [DataMember(Order = 10)]
          public DateTime? Date {get;set;}


        
          [DataMember(Order = 11)]
          public decimal? GewoneForm {get;set;}


        
          [DataMember(Order = 12)]
          public decimal? Gewone {get;set;}


        
          [DataMember(Order = 13)]
          public decimal? OnderzoekOntwikkelingForm {get;set;}


        
          [DataMember(Order = 14)]
          public decimal? OnderzoekOntwikkeling {get;set;}


        
          [DataMember(Order = 15)]
          public decimal? OmzetBelasting {get;set;}


        
          [DataMember(Order = 16)]
          public decimal? Totaal {get;set;}


        
          [DataMember(Order = 17)]
          public bool? Disabled {get;set;}


          #endregion
      }

  
      [DataContract(Namespace = "AY2015.TransferTaxCreditsApp")]
      public class BelastingkredietItem : IEntity
      {
          #region IEntity
          
          [DataMember(Name = "ID", Order=1)]
          public Guid Id { get; set; }
          
          [DataMember(Order=2)]
          public DateTime? LastUpdated { get; set; }
          
          [DataMember(Order = 3)]
          public bool IsValid { get; set; }
          
          [DataMember(Order = 4)]
          public bool PreviousImported { get; set; }
          
          #endregion
          
          #region entitymembers
          

        
          [DataMember(Order = 5)]
          public StringListItemDataContract Aanslagjaar {get;set;}


        
          [DataMember(Order = 6)]
          public decimal? BedragAftrek {get;set;}


        
          [DataMember(Order = 7)]
          public decimal? GebruiktHuidigBJ {get;set;}


        
          [DataMember(Order = 8)]
          public decimal? BedragAftrekImported {get;set;}


        
          [DataMember(Order = 9)]
          public decimal? GebruiktHuidigBJImported {get;set;}


        
          [DataMember(Order = 10)]
          public decimal? Saldo {get;set;}


        
          [DataMember(Order = 11)]
          public bool? Disabled {get;set;}


          #endregion
      }

  
      [DataContract(Namespace = "AY2015.TransferTaxCreditsApp")]
      public class AanpassingItem : IEntity
      {
          #region IEntity
          
          [DataMember(Name = "ID", Order=1)]
          public Guid Id { get; set; }
          
          [DataMember(Order=2)]
          public DateTime? LastUpdated { get; set; }
          
          [DataMember(Order = 3)]
          public bool IsValid { get; set; }
          
          [DataMember(Order = 4)]
          public bool PreviousImported { get; set; }
          
          #endregion
          
          #region entitymembers
          

        
          [DataMember(Order = 5)]
          public GuidListItemDataContract AanTePassenTabblad {get;set;}


        
          [DataMember(Order = 6)]
          public StringListItemDataContract AanTePassenVerlies {get;set;}


        
          [DataMember(Order = 7)]
          public StringListItemDataContract AanTePassenInvest {get;set;}


        
          [DataMember(Order = 8)]
          public StringListItemDataContract AanTePassenBelastingKrediet {get;set;}


        
          [DataMember(Order = 9)]
          public GuidListItemDataContract Reden {get;set;}


        
          [DataMember(Order = 10)]
          public GuidListItemDataContract AardReorganisatie {get;set;}


        
          [DataMember(Order = 11)]
          public decimal? BedragHuidig {get;set;}


        
          [DataMember(Order = 12)]
          public decimal? Gewone {get;set;}


        
          [DataMember(Order = 13)]
          public decimal? OnderzoekOntwikkeling {get;set;}


        
          [DataMember(Order = 14)]
          public decimal? BedragAftrek {get;set;}


        
          [DataMember(Order = 15)]
          public string HistoryID {get;set;}


          #endregion
      }

  
      [DataContract(Namespace = "AY2015.TransferTaxCreditsApp")]
      public class Aanwending : IEntity
      {
          #region IEntity
          
          [DataMember(Name = "ID", Order=1)]
          public Guid Id { get; set; }
          
          [DataMember(Order=2)]
          public DateTime? LastUpdated { get; set; }
          
          [DataMember(Order = 3)]
          public bool IsValid { get; set; }
          
          [DataMember(Order = 4)]
          public bool PreviousImported { get; set; }
          
          #endregion
          
          #region entitymembers
          

        
          [DataMember(Order = 5)]
          public decimal? AanwendingDBI {get;set;}


        
          [DataMember(Order = 6)]
          public decimal? AanwendingNID {get;set;}


        
          [DataMember(Order = 7)]
          public decimal? ResterendVoorAanwending {get;set;}


        
          [DataMember(Order = 8)]
          public decimal? AanwendingVerlies {get;set;}


        
          [DataMember(Order = 9)]
          public decimal? AanwendingInvest {get;set;}


        
          [DataMember(Order = 10)]
          public decimal? BepBelGereserveerdWinst {get;set;}


          #endregion
      }

  

    #endregion

    #region roottype
    

      [DataContract(Namespace = "AY2015.TransferTaxCreditsApp")]
      public class TransferTaxCredits : IWorksheet
      {
          public TransferTaxCredits() {
              // Set Minimal

        
              this.FiscaleVerliezen = new List<EY.com.eBook.RuleEngine.AY2015.TransferTaxCreditsAppTypes.FiscaalVerlies>();
              
        
             // this.NID = new List<EY.com.eBook.RuleEngine.AY2015.TransferTaxCreditsAppTypes.NIDItem>();
              
        
              this.DBI = new EY.com.eBook.RuleEngine.AY2015.TransferTaxCreditsAppTypes.DBIItem();
              this.DBI.Id = Guid.NewGuid();
        
              this.Invest = new List<EY.com.eBook.RuleEngine.AY2015.TransferTaxCreditsAppTypes.InvestItem>();
              
        
              this.Belastingkrediet = new List<EY.com.eBook.RuleEngine.AY2015.TransferTaxCreditsAppTypes.BelastingkredietItem>();
              
        
              this.Aanpassingen = new List<EY.com.eBook.RuleEngine.AY2015.TransferTaxCreditsAppTypes.AanpassingItem>();
              
        
              this.Aanwendingen = new EY.com.eBook.RuleEngine.AY2015.TransferTaxCreditsAppTypes.Aanwending();
              this.Aanwendingen.Id = Guid.NewGuid();
              this.LastUpdated = DateTime.Now;
          }
        
          
          #region entitymembers
          

        
          [DataMember(Order = 1)]
          public List<EY.com.eBook.RuleEngine.AY2015.TransferTaxCreditsAppTypes.FiscaalVerlies> FiscaleVerliezen {get;set;}


        
        //  [DataMember(Order = 2)]
        //  public List<EY.com.eBook.RuleEngine.AY2015.TransferTaxCreditsAppTypes.NIDItem> NID {get;set;}


        
          [DataMember(Order = 3)]
          public EY.com.eBook.RuleEngine.AY2015.TransferTaxCreditsAppTypes.DBIItem DBI {get;set;}


        
          [DataMember(Order = 4)]
          public List<EY.com.eBook.RuleEngine.AY2015.TransferTaxCreditsAppTypes.InvestItem> Invest {get;set;}


        
          [DataMember(Order = 5)]
          public List<EY.com.eBook.RuleEngine.AY2015.TransferTaxCreditsAppTypes.BelastingkredietItem> Belastingkrediet {get;set;}


        
          [DataMember(Order = 6)]
          public List<EY.com.eBook.RuleEngine.AY2015.TransferTaxCreditsAppTypes.AanpassingItem> Aanpassingen {get;set;}


        
          [DataMember(Order = 7)]
          public EY.com.eBook.RuleEngine.AY2015.TransferTaxCreditsAppTypes.Aanwending Aanwendingen {get;set;}


      
          #endregion
          
          #region IWorksheet Members
          [DataMember(Order = 8)]
          public DateTime? LastUpdated { get; set; }

          #endregion
      }

  
   
    #endregion

}
  