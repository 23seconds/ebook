﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.RuleEngine.Core;
using EY.com.eBook.RuleEngine.Core.Interfaces;
using EY.com.eBook.RuleEngine.Data;


namespace EY.com.eBook.RuleEngine.AY2015.DetailRekeningKapitaalAppTypes
{

    #region detail types

    [DataContract(Namespace = "AY2015.DetailRekeningKapitaalApp")]
    public class OverzichtDetail
    {


        #region entitymembers



        [DataMember(Order = 1)]
        public DateTime? StartDate { get; set; }



        [DataMember(Order = 2)]
        public DateTime? EndDate { get; set; }



        [DataMember(Order = 3)]
        public int? Assessment { get; set; }



        [DataMember(Order = 4)]
        public decimal? WithholdingTaxPerc { get; set; }


        #endregion
    }

    #endregion

    #region subtypes


    [DataContract(Namespace = "AY2015.DetailRekeningKapitaalApp")]
    public class DetailRekeningKapitaalOverzicht : IEntity
    {
        #region IEntity

        [DataMember(Name = "ID", Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public DateTime? LastUpdated { get; set; }

        [DataMember(Order = 3)]
        public bool IsValid { get; set; }

        [DataMember(Order = 4)]
        public bool PreviousImported { get; set; }

        #endregion

        #region entitymembers

        [DataMember(Order = 5)]
        public DateTime? Datum {get;set;}

        [DataMember(Order = 6)]
        public string Omschrijving {get;set;}

        [DataMember(Order = 7)]
        public decimal? InbrengVanGeld {get;set;}

        [DataMember(Order = 8)]
        public decimal? InbrengVanNatura {get;set;}

        [DataMember(Order = 9)]
        public decimal? BelasteReserves {get;set;}

        [DataMember(Order = 10)]
        public decimal? VrijgesteldeReserves {get;set;}

        [DataMember(Order = 12)]
        public decimal? LiquidatieBonus { get; set; }

        [DataMember(Order = 13)]
        public decimal? VvprRegime { get; set; }

        [DataMember(Order = 11)]
        public string Commentaar {get;set;}

        [DataMember(Order = 14)]
        public List<EY.com.eBook.RuleEngine.AY2015.DetailRekeningKapitaalAppTypes.OverzichtDetail> Details { get; set; }

        [DataMember(Order = 15)]
        public int? Seq { get; set; }
        #endregion
    }
    #endregion

    #region roottype


    [DataContract(Namespace = "AY2015.DetailRekeningKapitaalApp")]
    public class DetailRekeningKapitaal : IWorksheet
    {
        public DetailRekeningKapitaal()
        {
            // Set Minimal

            this.Overzicht = new List<EY.com.eBook.RuleEngine.AY2015.DetailRekeningKapitaalAppTypes.DetailRekeningKapitaalOverzicht>();

            
            this.LastUpdated = DateTime.Now;
        }


        #region entitymembers



        [DataMember(Order = 1)]
        public List<EY.com.eBook.RuleEngine.AY2015.DetailRekeningKapitaalAppTypes.DetailRekeningKapitaalOverzicht> Overzicht { get; set; }




        #endregion

        #region IWorksheet Members
        [DataMember(Order = 2)]
        public DateTime? LastUpdated { get; set; }

        #endregion
    }



    #endregion

}
