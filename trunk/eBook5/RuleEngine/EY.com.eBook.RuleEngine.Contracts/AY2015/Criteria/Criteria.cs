
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.RuleEngine.Data.AY2015
{

    [DataContract]
    public class CriteriaFacturatieAppFacturatieKlantDataContract
    {
        // FileId
        [DataMember(Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public string CollectionPath { get; set; }

        [DataMember(Order = 4)]
        public EY.com.eBook.RuleEngine.AY2015.FacturatieAppTypes.FacturatieKlant Entity { get; set; }
    }


    [DataContract]
    public class CriteriaFacturatieAppFacturatieLeverancierCNDataContract
    {
        // FileId
        [DataMember(Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public string CollectionPath { get; set; }

        [DataMember(Order = 4)]
        public EY.com.eBook.RuleEngine.AY2015.FacturatieAppTypes.FacturatieLeverancierCN Entity { get; set; }
    }


    [DataContract]
    public class CriteriaFacturatieAppFacturatieLeverancierDataContract
    {
        // FileId
        [DataMember(Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public string CollectionPath { get; set; }

        [DataMember(Order = 4)]
        public EY.com.eBook.RuleEngine.AY2015.FacturatieAppTypes.FacturatieLeverancier Entity { get; set; }
    }


    [DataContract]
    public class CriteriaFacturatieAppFacturatieKlantCNDataContract
    {
        // FileId
        [DataMember(Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public string CollectionPath { get; set; }

        [DataMember(Order = 4)]
        public EY.com.eBook.RuleEngine.AY2015.FacturatieAppTypes.FacturatieKlantCN Entity { get; set; }
    }


    [DataContract]
    public class CriteriaOverlopendeRekeningenAppOverTeDragenKostDataContract
    {
        // FileId
        [DataMember(Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public string CollectionPath { get; set; }

        [DataMember(Order = 4)]
        public EY.com.eBook.RuleEngine.AY2015.OverlopendeRekeningenAppTypes.OverTeDragenKost Entity { get; set; }
    }


    [DataContract]
    public class CriteriaOverlopendeRekeningenAppVerkregenOpbrengstDataContract
    {
        // FileId
        [DataMember(Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public string CollectionPath { get; set; }

        [DataMember(Order = 4)]
        public EY.com.eBook.RuleEngine.AY2015.OverlopendeRekeningenAppTypes.VerkregenOpbrengst Entity { get; set; }
    }


    [DataContract]
    public class CriteriaOverlopendeRekeningenAppToeTeRekenenKostDataContract
    {
        // FileId
        [DataMember(Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public string CollectionPath { get; set; }

        [DataMember(Order = 4)]
        public EY.com.eBook.RuleEngine.AY2015.OverlopendeRekeningenAppTypes.ToeTeRekenenKost Entity { get; set; }
    }


    [DataContract]
    public class CriteriaOverlopendeRekeningenAppOverTeDragenOpbrengstDataContract
    {
        // FileId
        [DataMember(Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public string CollectionPath { get; set; }

        [DataMember(Order = 4)]
        public EY.com.eBook.RuleEngine.AY2015.OverlopendeRekeningenAppTypes.OverTeDragenOpbrengst Entity { get; set; }
    }


    [DataContract]
    public class CriteriaVergelijkendeCijfersAppCijferBlokDataContract
    {
        // FileId
        [DataMember(Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public string CollectionPath { get; set; }

        [DataMember(Order = 4)]
        public EY.com.eBook.RuleEngine.AY2015.VergelijkendeCijfersAppTypes.CijferBlok Entity { get; set; }
    }


    [DataContract]
    public class CriteriaVoorschottenAppGegevenDataContract
    {
        // FileId
        [DataMember(Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public string CollectionPath { get; set; }

        [DataMember(Order = 4)]
        public EY.com.eBook.RuleEngine.AY2015.VoorschottenAppTypes.Gegeven Entity { get; set; }
    }


    [DataContract]
    public class CriteriaVoorschottenAppIntrestVoetDataContract
    {
        // FileId
        [DataMember(Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public string CollectionPath { get; set; }

        [DataMember(Order = 4)]
        public EY.com.eBook.RuleEngine.AY2015.VoorschottenAppTypes.IntrestVoet Entity { get; set; }
    }


    [DataContract]
    public class CriteriaVoorschottenAppRentegevendVoorschotDataContract
    {
        // FileId
        [DataMember(Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public string CollectionPath { get; set; }

        [DataMember(Order = 4)]
        public EY.com.eBook.RuleEngine.AY2015.VoorschottenAppTypes.RentegevendVoorschot Entity { get; set; }
    }


    [DataContract]
    public class CriteriaErelonenHuurAppImportItemDataContract
    {
        // FileId
        [DataMember(Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public string CollectionPath { get; set; }

        [DataMember(Order = 4)]
        public EY.com.eBook.RuleEngine.AY2015.ErelonenHuurAppTypes.ImportItem Entity { get; set; }
    }


    [DataContract]
    public class CriteriaErelonenHuurAppImportedHistoryDataContract
    {
        // FileId
        [DataMember(Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public string CollectionPath { get; set; }

        [DataMember(Order = 4)]
        public EY.com.eBook.RuleEngine.AY2015.ErelonenHuurAppTypes.ImportedHistory Entity { get; set; }
    }


    [DataContract]
    public class CriteriaErelonenHuurAppVerdelingHistoriekDataContract
    {
        // FileId
        [DataMember(Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public string CollectionPath { get; set; }

        [DataMember(Order = 4)]
        public EY.com.eBook.RuleEngine.AY2015.ErelonenHuurAppTypes.VerdelingHistoriek Entity { get; set; }
    }


    [DataContract]
    public class CriteriaErelonenHuurAppFiche281_50DataContract
    {
        // FileId
        [DataMember(Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public string CollectionPath { get; set; }

        [DataMember(Order = 4)]
        public EY.com.eBook.RuleEngine.AY2015.ErelonenHuurAppTypes.Fiche281_50 Entity { get; set; }
    }


    [DataContract]
    public class CriteriaVoordelenAppVAA_AutoCO2DataContract
    {
        // FileId
        [DataMember(Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public string CollectionPath { get; set; }

        [DataMember(Order = 4)]
        public EY.com.eBook.RuleEngine.AY2015.VoordelenAppTypes.VAA_AutoCO2 Entity { get; set; }
    }


    [DataContract]
    public class CriteriaVoordelenAppVAA_AutoRupoDataContract
    {
        // FileId
        [DataMember(Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public string CollectionPath { get; set; }

        [DataMember(Order = 4)]
        public EY.com.eBook.RuleEngine.AY2015.VoordelenAppTypes.VAA_AutoRupo Entity { get; set; }
    }


    [DataContract]
    public class CriteriaVoordelenAppVAA_NutsDataContract
    {
        // FileId
        [DataMember(Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public string CollectionPath { get; set; }

        [DataMember(Order = 4)]
        public EY.com.eBook.RuleEngine.AY2015.VoordelenAppTypes.VAA_Nuts Entity { get; set; }
    }


    [DataContract]
    public class CriteriaVoordelenAppVAA_HUURDataContract
    {
        // FileId
        [DataMember(Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public string CollectionPath { get; set; }

        [DataMember(Order = 4)]
        public EY.com.eBook.RuleEngine.AY2015.VoordelenAppTypes.VAA_HUUR Entity { get; set; }
    }


    [DataContract]
    public class CriteriaVoordelenAppVAA_HUURHERKDataContract
    {
        // FileId
        [DataMember(Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public string CollectionPath { get; set; }

        [DataMember(Order = 4)]
        public EY.com.eBook.RuleEngine.AY2015.VoordelenAppTypes.VAA_HUURHERK Entity { get; set; }
    }


    [DataContract]
    public class CriteriaVoordelenAppVAA_PCDataContract
    {
        // FileId
        [DataMember(Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public string CollectionPath { get; set; }

        [DataMember(Order = 4)]
        public EY.com.eBook.RuleEngine.AY2015.VoordelenAppTypes.VAA_PC Entity { get; set; }
    }


    [DataContract]
    public class CriteriaVoordelenAppVAA_TelefonieDataContract
    {
        // FileId
        [DataMember(Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public string CollectionPath { get; set; }

        [DataMember(Order = 4)]
        public EY.com.eBook.RuleEngine.AY2015.VoordelenAppTypes.VAA_Telefonie Entity { get; set; }
    }


    [DataContract]
    public class CriteriaVoordelenAppVAA_OverigeDataContract
    {
        // FileId
        [DataMember(Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public string CollectionPath { get; set; }

        [DataMember(Order = 4)]
        public EY.com.eBook.RuleEngine.AY2015.VoordelenAppTypes.VAA_Overige Entity { get; set; }
    }


    [DataContract]
    public class CriteriaVoordelenAppVAA_Interesten_RC_BedrijfsleiderDataContract
    {
        // FileId
        [DataMember(Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public string CollectionPath { get; set; }

        [DataMember(Order = 4)]
        public EY.com.eBook.RuleEngine.AY2015.VoordelenAppTypes.VAA_Interesten_RC_Bedrijfsleider Entity { get; set; }
    }


    [DataContract]
    public class CriteriaVoordelenAppVAA_Soc_Bijdr_BedrijfsleiderDataContract
    {
        // FileId
        [DataMember(Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public string CollectionPath { get; set; }

        [DataMember(Order = 4)]
        public EY.com.eBook.RuleEngine.AY2015.VoordelenAppTypes.VAA_Soc_Bijdr_Bedrijfsleider Entity { get; set; }
    }


    [DataContract]
    public class CriteriaVoordelenAppVAA_Overige_NutsDataContract
    {
        // FileId
        [DataMember(Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public string CollectionPath { get; set; }

        [DataMember(Order = 4)]
        public EY.com.eBook.RuleEngine.AY2015.VoordelenAppTypes.VAA_Overige_Nuts Entity { get; set; }
    }


    [DataContract]
    public class CriteriaPersoneelAppItem325DataContract
    {
        // FileId
        [DataMember(Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public string CollectionPath { get; set; }

        [DataMember(Order = 4)]
        public EY.com.eBook.RuleEngine.AY2015.PersoneelAppTypes.Item325 Entity { get; set; }
    }


    [DataContract]
    public class CriteriaPersoneelAppVerschilItemDataContract
    {
        // FileId
        [DataMember(Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public string CollectionPath { get; set; }

        [DataMember(Order = 4)]
        public EY.com.eBook.RuleEngine.AY2015.PersoneelAppTypes.VerschilItem Entity { get; set; }
    }


    [DataContract]
    public class CriteriaPersoneelAppWerknemerDataContract
    {
        // FileId
        [DataMember(Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public string CollectionPath { get; set; }

        [DataMember(Order = 4)]
        public EY.com.eBook.RuleEngine.AY2015.PersoneelAppTypes.Werknemer Entity { get; set; }
    }


    [DataContract]
    public class CriteriaPersoneelAppAlgemeenDataContract
    {
        // FileId
        [DataMember(Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public string CollectionPath { get; set; }

        [DataMember(Order = 4)]
        public EY.com.eBook.RuleEngine.AY2015.PersoneelAppTypes.Algemeen Entity { get; set; }
    }


    [DataContract]
    public class CriteriaPersoneelAppVakantiegeldDataContract
    {
        // FileId
        [DataMember(Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public string CollectionPath { get; set; }

        [DataMember(Order = 4)]
        public EY.com.eBook.RuleEngine.AY2015.PersoneelAppTypes.Vakantiegeld Entity { get; set; }
    }


    [DataContract]
    public class CriteriaBTWAppOmzetAangifteDataContract
    {
        // FileId
        [DataMember(Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public string CollectionPath { get; set; }

        [DataMember(Order = 4)]
        public EY.com.eBook.RuleEngine.AY2015.BTWAppTypes.OmzetAangifte Entity { get; set; }
    }


    [DataContract]
    public class CriteriaBTWAppOmzetGeboektDataContract
    {
        // FileId
        [DataMember(Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public string CollectionPath { get; set; }

        [DataMember(Order = 4)]
        public EY.com.eBook.RuleEngine.AY2015.BTWAppTypes.OmzetGeboekt Entity { get; set; }
    }


    [DataContract]
    public class CriteriaBTWAppOmzetVerklaringDataContract
    {
        // FileId
        [DataMember(Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public string CollectionPath { get; set; }

        [DataMember(Order = 4)]
        public EY.com.eBook.RuleEngine.AY2015.BTWAppTypes.OmzetVerklaring Entity { get; set; }
    }


    [DataContract]
    public class CriteriaBTWAppRCAangifteDataContract
    {
        // FileId
        [DataMember(Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public string CollectionPath { get; set; }

        [DataMember(Order = 4)]
        public EY.com.eBook.RuleEngine.AY2015.BTWAppTypes.RCAangifte Entity { get; set; }
    }


    [DataContract]
    public class CriteriaBTWAppRCGeboektDataContract
    {
        // FileId
        [DataMember(Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public string CollectionPath { get; set; }

        [DataMember(Order = 4)]
        public EY.com.eBook.RuleEngine.AY2015.BTWAppTypes.RCGeboekt Entity { get; set; }
    }


    [DataContract]
    public class CriteriaBTWAppRCVerklaringDataContract
    {
        // FileId
        [DataMember(Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public string CollectionPath { get; set; }

        [DataMember(Order = 4)]
        public EY.com.eBook.RuleEngine.AY2015.BTWAppTypes.RCVerklaring Entity { get; set; }
    }


    [DataContract]
    public class CriteriaRVIntrestenAppIntrestItemDataContract
    {
        // FileId
        [DataMember(Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public string CollectionPath { get; set; }

        [DataMember(Order = 4)]
        public EY.com.eBook.RuleEngine.AY2015.RVIntrestenAppTypes.IntrestItem Entity { get; set; }
    }


    [DataContract]
    public class CriteriaRVIntrestenAppAdjustmentDataContract
    {
        // FileId
        [DataMember(Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public string CollectionPath { get; set; }

        [DataMember(Order = 4)]
        public EY.com.eBook.RuleEngine.AY2015.RVIntrestenAppTypes.Adjustment Entity { get; set; }
    }


    [DataContract]
    public class CriteriaRVIntrestenAppVerchilItemDataContract
    {
        // FileId
        [DataMember(Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public string CollectionPath { get; set; }

        [DataMember(Order = 4)]
        public EY.com.eBook.RuleEngine.AY2015.RVIntrestenAppTypes.VerchilItem Entity { get; set; }
    }


    [DataContract]
    public class CriteriaDubieuzeDebiteurenAppDubieuzeDebiteurTDataContract
    {
        // FileId
        [DataMember(Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public string CollectionPath { get; set; }

        [DataMember(Order = 4)]
        public EY.com.eBook.RuleEngine.AY2015.DubieuzeDebiteurenAppTypes.DubieuzeDebiteurT Entity { get; set; }
    }


    [DataContract]
    public class CriteriaDubieuzeDebiteurenAppDubieuzeDebiteurWDataContract
    {
        // FileId
        [DataMember(Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public string CollectionPath { get; set; }

        [DataMember(Order = 4)]
        public EY.com.eBook.RuleEngine.AY2015.DubieuzeDebiteurenAppTypes.DubieuzeDebiteurW Entity { get; set; }
    }


    [DataContract]
    public class CriteriaDubieuzeDebiteurenAppDubieuzeDebiteurNDataContract
    {
        // FileId
        [DataMember(Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public string CollectionPath { get; set; }

        [DataMember(Order = 4)]
        public EY.com.eBook.RuleEngine.AY2015.DubieuzeDebiteurenAppTypes.DubieuzeDebiteurN Entity { get; set; }
    }


    [DataContract]
    public class CriteriaDubieuzeDebiteurenAppDubieuzeDebiteurHDataContract
    {
        // FileId
        [DataMember(Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public string CollectionPath { get; set; }

        [DataMember(Order = 4)]
        public EY.com.eBook.RuleEngine.AY2015.DubieuzeDebiteurenAppTypes.DubieuzeDebiteurH Entity { get; set; }
    }


    [DataContract]
    public class CriteriaVerliesHandelsdebiteurenAppVerliesHandelsDebiteurDataContract
    {
        // FileId
        [DataMember(Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public string CollectionPath { get; set; }

        [DataMember(Order = 4)]
        public EY.com.eBook.RuleEngine.AY2015.VerliesHandelsdebiteurenAppTypes.VerliesHandelsDebiteur Entity { get; set; }
    }


    [DataContract]
    public class CriteriaVoorzieningenAppVoorzieningenTDataContract
    {
        // FileId
        [DataMember(Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public string CollectionPath { get; set; }

        [DataMember(Order = 4)]
        public EY.com.eBook.RuleEngine.AY2015.VoorzieningenAppTypes.VoorzieningenT Entity { get; set; }
    }


    [DataContract]
    public class CriteriaVoorzieningenAppVoorzieningenWDataContract
    {
        // FileId
        [DataMember(Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public string CollectionPath { get; set; }

        [DataMember(Order = 4)]
        public EY.com.eBook.RuleEngine.AY2015.VoorzieningenAppTypes.VoorzieningenW Entity { get; set; }
    }


    [DataContract]
    public class CriteriaVoorzieningenAppVoorzieningenNDataContract
    {
        // FileId
        [DataMember(Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public string CollectionPath { get; set; }

        [DataMember(Order = 4)]
        public EY.com.eBook.RuleEngine.AY2015.VoorzieningenAppTypes.VoorzieningenN Entity { get; set; }
    }


    [DataContract]
    public class CriteriaVoorzieningenAppVoorzieningenHDataContract
    {
        // FileId
        [DataMember(Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public string CollectionPath { get; set; }

        [DataMember(Order = 4)]
        public EY.com.eBook.RuleEngine.AY2015.VoorzieningenAppTypes.VoorzieningenH Entity { get; set; }
    }


    [DataContract]
    public class CriteriaBelastingenAppAdjustmentDataContract
    {
        // FileId
        [DataMember(Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public string CollectionPath { get; set; }

        [DataMember(Order = 4)]
        public EY.com.eBook.RuleEngine.AY2015.BelastingenAppTypes.Adjustment Entity { get; set; }
    }


    [DataContract]
    public class CriteriaBelastingenAppBelastingDataContract
    {
        // FileId
        [DataMember(Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public string CollectionPath { get; set; }

        [DataMember(Order = 4)]
        public EY.com.eBook.RuleEngine.AY2015.BelastingenAppTypes.Belasting Entity { get; set; }
    }


    [DataContract]
    public class CriteriaBelastingenAppKostItemDataContract
    {
        // FileId
        [DataMember(Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public string CollectionPath { get; set; }

        [DataMember(Order = 4)]
        public EY.com.eBook.RuleEngine.AY2015.BelastingenAppTypes.KostItem Entity { get; set; }
    }


    [DataContract]
    public class CriteriaMeerwaardeAandelenAppMeerwaardeAandelenItemDataContract
    {
        // FileId
        [DataMember(Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public string CollectionPath { get; set; }

        [DataMember(Order = 4)]
        public EY.com.eBook.RuleEngine.AY2015.MeerwaardeAandelenAppTypes.MeerwaardeAandelenItem Entity { get; set; }
    }


    [DataContract]
    public class CriteriaBelasteReservesAppBelasteReservesItemDataContract
    {
        // FileId
        [DataMember(Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public string CollectionPath { get; set; }

        [DataMember(Order = 4)]
        public EY.com.eBook.RuleEngine.AY2015.BelasteReservesAppTypes.BelasteReservesItem Entity { get; set; }
    }


    [DataContract]
    public class CriteriaBelasteReservesAppAanpassingBTReservesItemDataContract
    {
        // FileId
        [DataMember(Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public string CollectionPath { get; set; }

        [DataMember(Order = 4)]
        public EY.com.eBook.RuleEngine.AY2015.BelasteReservesAppTypes.AanpassingBTReservesItem Entity { get; set; }
    }


    [DataContract]
    public class CriteriaBelastingVrijeReservesAppDetailDataContract
    {
        // FileId
        [DataMember(Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public string CollectionPath { get; set; }

        [DataMember(Order = 4)]
        public EY.com.eBook.RuleEngine.AY2015.BelastingVrijeReservesAppTypes.Detail Entity { get; set; }
    }


    [DataContract]
    public class CriteriaBepalingBelastbareBasisAppGeneralDataContract
    {
        // FileId
        [DataMember(Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public string CollectionPath { get; set; }

        [DataMember(Order = 4)]
        public EY.com.eBook.RuleEngine.AY2015.BepalingBelastbareBasisAppTypes.General Entity { get; set; }
    }


    [DataContract]
    public class CriteriaBepalingBelastbareBasisAppBelasteReservesItemDataContract
    {
        // FileId
        [DataMember(Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public string CollectionPath { get; set; }

        [DataMember(Order = 4)]
        public EY.com.eBook.RuleEngine.AY2015.BepalingBelastbareBasisAppTypes.BelasteReservesItem Entity { get; set; }
    }


    [DataContract]
    public class CriteriaBepalingBelastbareBasisAppVerworpenUitgavenItemDataContract
    {
        // FileId
        [DataMember(Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public string CollectionPath { get; set; }

        [DataMember(Order = 4)]
        public EY.com.eBook.RuleEngine.AY2015.BepalingBelastbareBasisAppTypes.VerworpenUitgavenItem Entity { get; set; }
    }


    [DataContract]
    public class CriteriaBepalingBelastbareBasisAppBelastbareBestZonderAftrekItemDataContract
    {
        // FileId
        [DataMember(Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public string CollectionPath { get; set; }

        [DataMember(Order = 4)]
        public EY.com.eBook.RuleEngine.AY2015.BepalingBelastbareBasisAppTypes.BelastbareBestZonderAftrekItem Entity { get; set; }
    }


    [DataContract]
    public class CriteriaBepalingBelastbareBasisAppOpdelingNaarOorsprongItemDataContract
    {
        // FileId
        [DataMember(Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public string CollectionPath { get; set; }

        [DataMember(Order = 4)]
        public EY.com.eBook.RuleEngine.AY2015.BepalingBelastbareBasisAppTypes.OpdelingNaarOorsprongItem Entity { get; set; }
    }


    [DataContract]
    public class CriteriaBepalingBelastbareBasisAppFiscaleAftrekItemDataContract
    {
        // FileId
        [DataMember(Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public string CollectionPath { get; set; }

        [DataMember(Order = 4)]
        public EY.com.eBook.RuleEngine.AY2015.BepalingBelastbareBasisAppTypes.FiscaleAftrekItem Entity { get; set; }
    }


    [DataContract]
    public class CriteriaDBIAppParticipatieItemDataContract
    {
        // FileId
        [DataMember(Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public string CollectionPath { get; set; }

        [DataMember(Order = 4)]
        public EY.com.eBook.RuleEngine.AY2015.DBIAppTypes.ParticipatieItem Entity { get; set; }
    }


    [DataContract]
    public class CriteriaDBIAppDividendItemDataContract
    {
        // FileId
        [DataMember(Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public string CollectionPath { get; set; }

        [DataMember(Order = 4)]
        public EY.com.eBook.RuleEngine.AY2015.DBIAppTypes.DividendItem Entity { get; set; }
    }


    [DataContract]
    public class CriteriaDBIAppDbiEntityDataContract
    {
        // FileId
        [DataMember(Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public string CollectionPath { get; set; }

        [DataMember(Order = 4)]
        public EY.com.eBook.RuleEngine.AY2015.DBIAppTypes.DbiEntity Entity { get; set; }
    }


    [DataContract]
    public class CriteriaDBIAppHistoryItemDataContract
    {
        // FileId
        [DataMember(Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public string CollectionPath { get; set; }

        [DataMember(Order = 4)]
        public EY.com.eBook.RuleEngine.AY2015.DBIAppTypes.HistoryItem Entity { get; set; }
    }


    [DataContract]
    public class CriteriaRisicoKapitaalAppFicheItemDataContract
    {
        // FileId
        [DataMember(Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public string CollectionPath { get; set; }

        [DataMember(Order = 4)]
        public EY.com.eBook.RuleEngine.AY2015.RisicoKapitaalAppTypes.FicheItem Entity { get; set; }
    }


    [DataContract]
    public class CriteriaRisicoKapitaalAppEigenVermogenDataContract
    {
        // FileId
        [DataMember(Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public string CollectionPath { get; set; }

        [DataMember(Order = 4)]
        public EY.com.eBook.RuleEngine.AY2015.RisicoKapitaalAppTypes.EigenVermogen Entity { get; set; }
    }


    [DataContract]
    public class CriteriaRisicoKapitaalAppBestanddeelDataContract
    {
        // FileId
        [DataMember(Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public string CollectionPath { get; set; }

        [DataMember(Order = 4)]
        public EY.com.eBook.RuleEngine.AY2015.RisicoKapitaalAppTypes.Bestanddeel Entity { get; set; }
    }


    [DataContract]
    public class CriteriaRisicoKapitaalAppWijzigingItemDataContract
    {
        // FileId
        [DataMember(Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public string CollectionPath { get; set; }

        [DataMember(Order = 4)]
        public EY.com.eBook.RuleEngine.AY2015.RisicoKapitaalAppTypes.WijzigingItem Entity { get; set; }
    }


    [DataContract]
    public class CriteriaRisicoKapitaalAppTariefItemDataContract
    {
        // FileId
        [DataMember(Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public string CollectionPath { get; set; }

        [DataMember(Order = 4)]
        public EY.com.eBook.RuleEngine.AY2015.RisicoKapitaalAppTypes.TariefItem Entity { get; set; }
    }

    [DataContract]
    public class CriteriaRisicoKapitaalAppVerminderingenItemDataContract
    {
        // FileId
        [DataMember(Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public string CollectionPath { get; set; }

        [DataMember(Order = 4)]
        public EY.com.eBook.RuleEngine.AY2015.RisicoKapitaalAppTypes.VerminderingItem Entity { get; set; }
    }

    /*
        * new NID
    [DataContract]
    public class CriteriaRisicoKapitaalAppHistoryItemDataContract
    {
        // FileId
        [DataMember(Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public string CollectionPath { get; set; }

        [DataMember(Order = 4)]
        public EY.com.eBook.RuleEngine.AY2015.RisicoKapitaalAppTypes.HistoryItem Entity { get; set; }
    }


    [DataContract]
    public class CriteriaRisicoKapitaalAppHistoriekAanpassingItemDataContract
    {
        // FileId
        [DataMember(Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public string CollectionPath { get; set; }

        [DataMember(Order = 4)]
        public EY.com.eBook.RuleEngine.AY2015.RisicoKapitaalAppTypes.HistoriekAanpassingItem Entity { get; set; }
    }
    */



    [DataContract]
    public class CriteriaRisicoKapitaalHistoriekAppFicheItemDataContract
    {
        // FileId
        [DataMember(Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public string CollectionPath { get; set; }

        [DataMember(Order = 4)]
        public EY.com.eBook.RuleEngine.AY2015.RisicoKapitaalHistoriekAppTypes.FicheItem Entity { get; set; }
    }

    /*
    [DataContract]
    public class CriteriaRisicoKapitaalHistoriekAppEigenVermogenDataContract
    {
        // FileId
        [DataMember(Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public string CollectionPath { get; set; }

        [DataMember(Order = 4)]
        public EY.com.eBook.RuleEngine.AY2015.RisicoKapitaalHistoriekAppTypes.EigenVermogen Entity { get; set; }
    }


    [DataContract]
    public class CriteriaRisicoKapitaalHistoriekAppBestanddeelDataContract
    {
        // FileId
        [DataMember(Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public string CollectionPath { get; set; }

        [DataMember(Order = 4)]
        public EY.com.eBook.RuleEngine.AY2015.RisicoKapitaalHistoriekAppTypes.Bestanddeel Entity { get; set; }
    }


    [DataContract]
    public class CriteriaRisicoKapitaalHistoriekAppWijzigingItemDataContract
    {
        // FileId
        [DataMember(Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public string CollectionPath { get; set; }

        [DataMember(Order = 4)]
        public EY.com.eBook.RuleEngine.AY2015.RisicoKapitaalHistoriekAppTypes.WijzigingItem Entity { get; set; }
    }


    [DataContract]
    public class CriteriaRisicoKapitaalHistoriekAppTariefItemDataContract
    {
        // FileId
        [DataMember(Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public string CollectionPath { get; set; }

        [DataMember(Order = 4)]
        public EY.com.eBook.RuleEngine.AY2015.RisicoKapitaalHistoriekAppTypes.TariefItem Entity { get; set; }
    }
    */

    [DataContract]
    public class CriteriaRisicoKapitaalHistoriekAppHistoryItemDataContract
    {
        // FileId
        [DataMember(Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public string CollectionPath { get; set; }

        [DataMember(Order = 4)]
        public EY.com.eBook.RuleEngine.AY2015.RisicoKapitaalHistoriekAppTypes.HistoryItem Entity { get; set; }
    }


    [DataContract]
    public class CriteriaRisicoKapitaalHistoriekAppHistoriekAanpassingItemDataContract
    {
        // FileId
        [DataMember(Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public string CollectionPath { get; set; }

        [DataMember(Order = 4)]
        public EY.com.eBook.RuleEngine.AY2015.RisicoKapitaalHistoriekAppTypes.HistoriekAanpassingItem Entity { get; set; }
    }


    [DataContract]
    public class CriteriaTransferTaxCreditsAppFiscaalVerliesDataContract
    {
        // FileId
        [DataMember(Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public string CollectionPath { get; set; }

        [DataMember(Order = 4)]
        public EY.com.eBook.RuleEngine.AY2015.TransferTaxCreditsAppTypes.FiscaalVerlies Entity { get; set; }
    }


   /* [DataContract]
    public class CriteriaTransferTaxCreditsAppNIDItemDataContract
    {
        // FileId
        [DataMember(Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public string CollectionPath { get; set; }

        [DataMember(Order = 4)]
        public EY.com.eBook.RuleEngine.AY2015.TransferTaxCreditsAppTypes.NIDItem Entity { get; set; }
    }
    */

    [DataContract]
    public class CriteriaTransferTaxCreditsAppDBIItemDataContract
    {
        // FileId
        [DataMember(Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public string CollectionPath { get; set; }

        [DataMember(Order = 4)]
        public EY.com.eBook.RuleEngine.AY2015.TransferTaxCreditsAppTypes.DBIItem Entity { get; set; }
    }


    [DataContract]
    public class CriteriaTransferTaxCreditsAppInvestItemDataContract
    {
        // FileId
        [DataMember(Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public string CollectionPath { get; set; }

        [DataMember(Order = 4)]
        public EY.com.eBook.RuleEngine.AY2015.TransferTaxCreditsAppTypes.InvestItem Entity { get; set; }
    }


    [DataContract]
    public class CriteriaTransferTaxCreditsAppBelastingkredietItemDataContract
    {
        // FileId
        [DataMember(Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public string CollectionPath { get; set; }

        [DataMember(Order = 4)]
        public EY.com.eBook.RuleEngine.AY2015.TransferTaxCreditsAppTypes.BelastingkredietItem Entity { get; set; }
    }


    [DataContract]
    public class CriteriaTransferTaxCreditsAppAanpassingItemDataContract
    {
        // FileId
        [DataMember(Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public string CollectionPath { get; set; }

        [DataMember(Order = 4)]
        public EY.com.eBook.RuleEngine.AY2015.TransferTaxCreditsAppTypes.AanpassingItem Entity { get; set; }
    }


    [DataContract]
    public class CriteriaTransferTaxCreditsAppAanwendingDataContract
    {
        // FileId
        [DataMember(Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public string CollectionPath { get; set; }

        [DataMember(Order = 4)]
        public EY.com.eBook.RuleEngine.AY2015.TransferTaxCreditsAppTypes.Aanwending Entity { get; set; }
    }


    [DataContract]
    public class CriteriaBerekeningVenBAppGeneralDataContract
    {
        // FileId
        [DataMember(Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public string CollectionPath { get; set; }

        [DataMember(Order = 4)]
        public EY.com.eBook.RuleEngine.AY2015.BerekeningVenBAppTypes.General Entity { get; set; }
    }


    [DataContract]
    public class CriteriaBerekeningVenBAppBelasteReservesItemDataContract
    {
        // FileId
        [DataMember(Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public string CollectionPath { get; set; }

        [DataMember(Order = 4)]
        public EY.com.eBook.RuleEngine.AY2015.BerekeningVenBAppTypes.BelasteReservesItem Entity { get; set; }
    }


    [DataContract]
    public class CriteriaBerekeningVenBAppVerworpenUitgavenItemDataContract
    {
        // FileId
        [DataMember(Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public string CollectionPath { get; set; }

        [DataMember(Order = 4)]
        public EY.com.eBook.RuleEngine.AY2015.BerekeningVenBAppTypes.VerworpenUitgavenItem Entity { get; set; }
    }


    [DataContract]
    public class CriteriaBerekeningVenBAppBelastbareBestZonderAftrekItemDataContract
    {
        // FileId
        [DataMember(Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public string CollectionPath { get; set; }

        [DataMember(Order = 4)]
        public EY.com.eBook.RuleEngine.AY2015.BerekeningVenBAppTypes.BelastbareBestZonderAftrekItem Entity { get; set; }
    }


    [DataContract]
    public class CriteriaBerekeningVenBAppOpdelingNaarOorsprongItemDataContract
    {
        // FileId
        [DataMember(Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public string CollectionPath { get; set; }

        [DataMember(Order = 4)]
        public EY.com.eBook.RuleEngine.AY2015.BerekeningVenBAppTypes.OpdelingNaarOorsprongItem Entity { get; set; }
    }


    [DataContract]
    public class CriteriaBerekeningVenBAppFiscaleAftrekItemDataContract
    {
        // FileId
        [DataMember(Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public string CollectionPath { get; set; }

        [DataMember(Order = 4)]
        public EY.com.eBook.RuleEngine.AY2015.BerekeningVenBAppTypes.FiscaleAftrekItem Entity { get; set; }
    }


    [DataContract]
    public class CriteriaBerekeningVenBAppTariefItemDataContract
    {
        // FileId
        [DataMember(Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public string CollectionPath { get; set; }

        [DataMember(Order = 4)]
        public EY.com.eBook.RuleEngine.AY2015.BerekeningVenBAppTypes.TariefItem Entity { get; set; }
    }


    [DataContract]
    public class CriteriaBerekeningVenBAppAanslagDataContract
    {
        // FileId
        [DataMember(Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public string CollectionPath { get; set; }

        [DataMember(Order = 4)]
        public EY.com.eBook.RuleEngine.AY2015.BerekeningVenBAppTypes.AanslagItem Entity { get; set; }
    }

    [DataContract]
    public class CriteriaBerekeningVenBAppFairnessTaxItemDataContract
    {
        // FileId
        [DataMember(Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public string CollectionPath { get; set; }

        [DataMember(Order = 4)]
        public EY.com.eBook.RuleEngine.AY2015.BerekeningVenBAppTypes.FairnessTaxItem Entity { get; set; }
    }

    [DataContract]
    public class CriteriaBerekeningVenBAppBerekeningOverzichtItemDataContract
    {
        // FileId
        [DataMember(Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public string CollectionPath { get; set; }

        [DataMember(Order = 4)]
        public EY.com.eBook.RuleEngine.AY2015.BerekeningVenBAppTypes.BerekeningOverzichtItem Entity { get; set; }
    }


   


    [DataContract]
    public class CriteriaAangifteVenBAppReserveEntityDataContract
    {
        // FileId
        [DataMember(Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public string CollectionPath { get; set; }

        [DataMember(Order = 4)]
        public EY.com.eBook.RuleEngine.AY2015.AangifteVenBAppTypes.ReserveEntity Entity { get; set; }
    }


    [DataContract]
    public class CriteriaAangifteVenBAppVrijgesteldeReserveEntityDataContract
    {
        // FileId
        [DataMember(Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public string CollectionPath { get; set; }

        [DataMember(Order = 4)]
        public EY.com.eBook.RuleEngine.AY2015.AangifteVenBAppTypes.VrijgesteldeReserveEntity Entity { get; set; }
    }


    [DataContract]
    public class CriteriaAangifteVenBAppVerworpenUitgavenEntityDataContract
    {
        // FileId
        [DataMember(Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public string CollectionPath { get; set; }

        [DataMember(Order = 4)]
        public EY.com.eBook.RuleEngine.AY2015.AangifteVenBAppTypes.VerworpenUitgavenEntity Entity { get; set; }
    }


    [DataContract]
    public class CriteriaAangifteVenBAppUitgekeerdeDividendenEntityDataContract
    {
        // FileId
        [DataMember(Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public string CollectionPath { get; set; }

        [DataMember(Order = 4)]
        public EY.com.eBook.RuleEngine.AY2015.AangifteVenBAppTypes.UitgekeerdeDividendenEntity Entity { get; set; }
    }


    [DataContract]
    public class CriteriaAangifteVenBAppWinstEntityDataContract
    {
        // FileId
        [DataMember(Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public string CollectionPath { get; set; }

        [DataMember(Order = 4)]
        public EY.com.eBook.RuleEngine.AY2015.AangifteVenBAppTypes.WinstEntity Entity { get; set; }
    }


    [DataContract]
    public class CriteriaAangifteVenBAppAfzonderlijkeAanslagenEntityDataContract
    {
        // FileId
        [DataMember(Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public string CollectionPath { get; set; }

        [DataMember(Order = 4)]
        public EY.com.eBook.RuleEngine.AY2015.AangifteVenBAppTypes.AfzonderlijkeAanslagenEntity Entity { get; set; }
    }


    [DataContract]
    public class CriteriaAangifteVenBAppDiamantEntityDataContract
    {
        // FileId
        [DataMember(Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public string CollectionPath { get; set; }

        [DataMember(Order = 4)]
        public EY.com.eBook.RuleEngine.AY2015.AangifteVenBAppTypes.DiamantEntity Entity { get; set; }
    }


    [DataContract]
    public class CriteriaAangifteVenBAppNietBelastbareBestandelenItemDataContract
    {
        // FileId
        [DataMember(Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public string CollectionPath { get; set; }

        [DataMember(Order = 4)]
        public EY.com.eBook.RuleEngine.AY2015.AangifteVenBAppTypes.NietBelastbareBestandelenItem Entity { get; set; }
    }


    [DataContract]
    public class CriteriaAangifteVenBAppDbiEntityDataContract
    {
        // FileId
        [DataMember(Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public string CollectionPath { get; set; }

        [DataMember(Order = 4)]
        public EY.com.eBook.RuleEngine.AY2015.AangifteVenBAppTypes.DbiEntity Entity { get; set; }
    }


    [DataContract]
    public class CriteriaAangifteVenBAppOverdrachtDBIAftrekEntityDataContract
    {
        // FileId
        [DataMember(Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public string CollectionPath { get; set; }

        [DataMember(Order = 4)]
        public EY.com.eBook.RuleEngine.AY2015.AangifteVenBAppTypes.OverdrachtDBIAftrekEntity Entity { get; set; }
    }


    [DataContract]
    public class CriteriaAangifteVenBAppOverdrachtNIDEntityDataContract
    {
        // FileId
        [DataMember(Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public string CollectionPath { get; set; }

        [DataMember(Order = 4)]
        public EY.com.eBook.RuleEngine.AY2015.AangifteVenBAppTypes.OverdrachtNIDEntity Entity { get; set; }
    }


    [DataContract]
    public class CriteriaAangifteVenBAppCompenseerbareVerliezenEntityDataContract
    {
        // FileId
        [DataMember(Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public string CollectionPath { get; set; }

        [DataMember(Order = 4)]
        public EY.com.eBook.RuleEngine.AY2015.AangifteVenBAppTypes.CompenseerbareVerliezenEntity Entity { get; set; }
    }


    [DataContract]
    public class CriteriaAangifteVenBAppTariefEntityDataContract
    {
        // FileId
        [DataMember(Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public string CollectionPath { get; set; }

        [DataMember(Order = 4)]
        public EY.com.eBook.RuleEngine.AY2015.AangifteVenBAppTypes.TariefEntity Entity { get; set; }
    }


    [DataContract]
    public class CriteriaAangifteVenBAppVoorafBetalingEntityDataContract
    {
        // FileId
        [DataMember(Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public string CollectionPath { get; set; }

        [DataMember(Order = 4)]
        public EY.com.eBook.RuleEngine.AY2015.AangifteVenBAppTypes.VoorafBetalingEntity Entity { get; set; }
    }


    [DataContract]
    public class CriteriaBerekeningVenBAppVerrekenbareVoorheffingEntityDataContract
    {
        // FileId
        [DataMember(Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public string CollectionPath { get; set; }

        [DataMember(Order = 4)]
        public EY.com.eBook.RuleEngine.AY2015.BerekeningVenBAppTypes.VerrekenbareVoorheffingEntity Entity { get; set; }
    }


    [DataContract]
    public class CriteriaAangifteVenBAppTaxShelterEntityDataContract
    {
        // FileId
        [DataMember(Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public string CollectionPath { get; set; }

        [DataMember(Order = 4)]
        public EY.com.eBook.RuleEngine.AY2015.AangifteVenBAppTypes.TaxShelterEntity Entity { get; set; }
    }


    [DataContract]
    public class CriteriaAangifteVenBAppGrootteVennootschapeEntityDataContract
    {
        // FileId
        [DataMember(Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public string CollectionPath { get; set; }

        [DataMember(Order = 4)]
        public EY.com.eBook.RuleEngine.AY2015.AangifteVenBAppTypes.GrootteVennootschapeEntity Entity { get; set; }
    }

    [DataContract]
    public class CriteriaDetailRekeningKapitaalEntityDataContract
    {
        // FileId
        [DataMember(Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public string Culture { get; set; }

        [DataMember(Order = 3)]
        public string CollectionPath { get; set; }

        [DataMember(Order = 4)]
        public EY.com.eBook.RuleEngine.AY2015.DetailRekeningKapitaalAppTypes.DetailRekeningKapitaalOverzicht Entity { get; set; }
    }

}
