﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.RuleEngine.Core;
using EY.com.eBook.RuleEngine.Core.Interfaces;
using EY.com.eBook.RuleEngine.Data;

    
namespace EY.com.eBook.RuleEngine.AY2012.DubieuzeDebiteurenAppTypes
{
    #region subtypes


      [DataContract(Namespace = "AY2012.DubieuzeDebiteurenApp")]
      public class DubieuzeDebiteurT : IEntity
      {
          #region IEntity
          
          [DataMember(Name = "ID", Order=1)]
          public Guid Id { get; set; }
          
          [DataMember(Order=2)]
          public DateTime? LastUpdated { get; set; }
          
          [DataMember(Order = 3)]
          public bool IsValid { get; set; }
          
          [DataMember(Order = 4)]
          public bool PreviousImported { get; set; }
          
          #endregion
          
          #region entitymembers
          
          [DataMember(Order = 5)]
          public bool? AlgemeneProvisie {get;set;}

        
          [DataMember(Order = 6)]
          public StringListItemDataContract Schuldenaar {get;set;}

          [DataMember(Order = 7)]
          public string FactuurJaar { get; set; }

          [DataMember(Order = 8)]
          public string FactuurNummer { get; set; }

          [DataMember(Order = 9)]
          public decimal? OrigineleFactuurWaarde { get; set; }

          [DataMember(Order = 10)]
          public decimal? OrigineleFactuurWaardeBTW { get; set; }

          [DataMember(Order = 11)]
          public decimal? Vordering { get; set; }

          [DataMember(Order = 12)]
          public decimal? Waardevermindering { get; set; }

          [DataMember(Order = 13)]
          public GuidListItemDataContract BelastbaarOfVrijgesteld { get; set; }
        
          [DataMember(Order = 14)]
          public int? Assesmentyear {get;set;}
        
          [DataMember(Order = 15)]
          public int? Nr {get;set;}

          [DataMember(Order = 16)]
          public string Label { get; set; }

          #endregion
      }

  
      [DataContract(Namespace = "AY2012.DubieuzeDebiteurenApp")]
      public class DubieuzeDebiteurW : IEntity
      {
          #region IEntity
          
          [DataMember(Name = "ID", Order=1)]
          public Guid Id { get; set; }
          
          [DataMember(Order=2)]
          public DateTime? LastUpdated { get; set; }
          
          [DataMember(Order = 3)]
          public bool IsValid { get; set; }
          
          [DataMember(Order = 4)]
          public bool PreviousImported { get; set; }
          
          #endregion
          
          #region entitymembers
          

          [DataMember(Order = 5)]
          public StringListItemDataContract Bestaande {get;set;}

          [DataMember(Order = 6)]
          public bool? AlgemeneProvisie { get; set; }

          [DataMember(Order = 7)]
          public StringListItemDataContract Schuldenaar { get; set; }

          [DataMember(Order = 8)]
          public string FactuurJaar { get; set; }

          [DataMember(Order = 9)]
          public string FactuurNummer { get; set; }

          [DataMember(Order = 10)]
          public decimal? OrigineleFactuurWaarde { get; set; }

          [DataMember(Order = 11)]
          public decimal? OrigineleFactuurWaardeBTW { get; set; }

          [DataMember(Order = 12)]
          public bool? DefinitiefVerlies { get; set; }

          [DataMember(Order = 13)]
          public GuidListItemDataContract RedenDefinitiefVerlies { get; set; }

          [DataMember(Order = 14)]
          public decimal? VorderingStart { get; set; }

          [DataMember(Order = 15)]
          public decimal? VorderingEinde { get; set; }

          [DataMember(Order = 16)]
          public decimal? WaardeverminderingStart { get; set; }

          [DataMember(Order = 17)]
          public decimal? WaardeverminderingEinde { get; set; }

          [DataMember(Order = 18)]
          public GuidListItemDataContract ToenameOfTerugname { get; set; }

          [DataMember(Order = 19)]
          public GuidListItemDataContract RedenVanAfname { get; set; }

          [DataMember(Order = 20)]
          public decimal? WaardeverminderingWaarde { get; set; }

          [DataMember(Order = 21)]
          public decimal? VerminderingWVVerlies { get; set; }

          [DataMember(Order = 22)]
          public decimal? VerminderingWVInning { get; set; }

          [DataMember(Order = 23)]
          public decimal? VerminderingWVHerschat { get; set; }

          [DataMember(Order = 24)]
          public decimal? VermeerderingWV { get; set; }

          [DataMember(Order = 25)]
          public string Commentaar { get; set; }

          [DataMember(Order = 26)]
          public GuidListItemDataContract BelastbaarOfVrijgesteld { get; set; }

          [DataMember(Order = 27)]
          public int? Assesmentyear { get; set; }

          [DataMember(Order = 28)]
          public int? Nr { get; set; }

          [DataMember(Order = 29)]
          public bool? Book { get; set; }

          [DataMember(Order = 30)]
          public string Label { get; set; }

          #endregion
      }

  
      [DataContract(Namespace = "AY2012.DubieuzeDebiteurenApp")]
      public class DubieuzeDebiteurN : IEntity
      {
          #region IEntity
          
          [DataMember(Name = "ID", Order=1)]
          public Guid Id { get; set; }
          
          [DataMember(Order=2)]
          public DateTime? LastUpdated { get; set; }
          
          [DataMember(Order = 3)]
          public bool IsValid { get; set; }
          
          [DataMember(Order = 4)]
          public bool PreviousImported { get; set; }
          
          #endregion
          
          #region entitymembers


          [DataMember(Order = 5)]
          public bool? AlgemeneProvisie { get; set; }

          [DataMember(Order = 6)]
          public StringListItemDataContract Schuldenaar { get; set; }

          [DataMember(Order = 7)]
          public string FactuurJaar { get; set; }

          [DataMember(Order = 8)]
          public string FactuurNummer { get; set; }

          [DataMember(Order = 9)]
          public decimal? OrigineleFactuurWaarde { get; set; }

          [DataMember(Order = 10)]
          public decimal? OrigineleFactuurWaardeBTW { get; set; }

          [DataMember(Order = 11)]
          public decimal? VorderingEinde { get; set; }

          [DataMember(Order = 12)]
          public decimal? WaardeverminderingEinde { get; set; }

          [DataMember(Order = 13)]
          public string Commentaar { get; set; }

          [DataMember(Order = 14)]
          public GuidListItemDataContract BelastbaarOfVrijgesteld { get; set; }

          [DataMember(Order = 15)]
          public int? Assesmentyear { get; set; }

          [DataMember(Order = 16)]
          public int? Nr { get; set; }

          [DataMember(Order = 17)]
          public string Label { get; set; }

          [DataMember(Order = 18)]
          public bool? Book { get; set; }

          #endregion
      }

  
      [DataContract(Namespace = "AY2012.DubieuzeDebiteurenApp")]
      public class DubieuzeDebiteurH : IEntity
      {
          #region IEntity
          
          [DataMember(Name = "ID", Order=1)]
          public Guid Id { get; set; }
          
          [DataMember(Order=2)]
          public DateTime? LastUpdated { get; set; }
          
          [DataMember(Order = 3)]
          public bool IsValid { get; set; }
          
          [DataMember(Order = 4)]
          public bool PreviousImported { get; set; }
          
          #endregion
          
          #region entitymembers

          [DataMember(Order = 5)]
          public bool? AlgemeneProvisie { get; set; }

          [DataMember(Order = 6)]
          public StringListItemDataContract Schuldenaar { get; set; }

          [DataMember(Order = 7)]
          public string FactuurJaar { get; set; }

          [DataMember(Order = 8)]
          public string FactuurNummer { get; set; }

          [DataMember(Order = 9)]
          public decimal? OrigineleFactuurWaarde { get; set; }

          [DataMember(Order = 10)]
          public decimal? OrigineleFactuurWaardeBTW { get; set; }

          [DataMember(Order = 11)]
          public decimal? VorderingStart { get; set; }

          [DataMember(Order = 12)]
          public decimal? VorderingEinde { get; set; }

          [DataMember(Order = 13)]
          public decimal? WaardeverminderingStart { get; set; }

          [DataMember(Order = 14)]
          public decimal? WaardeverminderingEinde { get; set; }

          [DataMember(Order = 15)]
          public decimal? VerminderingWVVerlies { get; set; }

          [DataMember(Order = 16)]
          public decimal? VerminderingWVInning { get; set; }

          [DataMember(Order = 17)]
          public decimal? VerminderingWVHerschat { get; set; }

          [DataMember(Order = 18)]
          public decimal? VermeerderingWV { get; set; }

          [DataMember(Order = 19)]
          public string Commentaar { get; set; }

          [DataMember(Order = 20)]
          public GuidListItemDataContract BelastbaarOfVrijgesteld { get; set; }

          [DataMember(Order = 21)]
          public int? Assesmentyear { get; set; }

          [DataMember(Order = 22)]
          public int? Nr { get; set; }

          [DataMember(Order = 23)]
          public bool? IsNew { get; set; }

          [DataMember(Order = 24)]
          public string Label { get; set; }

          #endregion
      }

      [DataContract(Namespace = "AY2012.DubieuzeDebiteurenApp")]
      public class DubieuzeDebiteur2043 : IEntity
      {
          #region IEntity

          [DataMember(Name = "ID", Order = 1)]
          public Guid Id { get; set; }

          [DataMember(Order = 2)]
          public DateTime? LastUpdated { get; set; }

          [DataMember(Order = 3)]
          public bool IsValid { get; set; }

          [DataMember(Order = 4)]
          public bool PreviousImported { get; set; }

          #endregion

          [DataMember(Order = 5)]
          public string TypedMember { get; set; }

          [DataMember(Order = 6)]
          public string ExplicitMember { get; set; }

          [DataMember(Order = 7)]
          public string Schuldenaar { get; set; }

          [DataMember(Order = 8)]
          public string SchuldenaarAdres { get; set; }

          [DataMember(Order = 9)]
          public decimal? VorderingStart { get; set; }
         
          [DataMember(Order = 10)]
          public decimal? VorderingEinde { get; set; }

          [DataMember(Order = 11)]
          public decimal? WaardeverminderingStart { get; set; }

          [DataMember(Order = 12)]
          public decimal? WaardeverminderingEinde { get; set; }

          [DataMember(Order = 13)]
          public decimal? VerminderingWVVerlies { get; set; }

          [DataMember(Order = 14)]
          public decimal? VerminderingWVInning { get; set; }

          [DataMember(Order = 15)]
          public decimal? VerminderingWVHerschat { get; set; }

          [DataMember(Order = 16)]
          public decimal? VermeerderingWV { get; set; }

          [DataMember(Order = 17)]
          public string Commentaar { get; set; }

      }
  

    #endregion

    #region roottype
    

      [DataContract(Namespace = "AY2012.DubieuzeDebiteurenApp")]
      public class DubieuzeDebiteuren : IWorksheet
      {
          public DubieuzeDebiteuren() {
              // Set Minimal

        
              this.ToestandEindVorigBoekjaar = new List<EY.com.eBook.RuleEngine.AY2012.DubieuzeDebiteurenAppTypes.DubieuzeDebiteurT>();
              
        
              this.WijzigingBestaandeTijdensBoekjaar = new List<EY.com.eBook.RuleEngine.AY2012.DubieuzeDebiteurenAppTypes.DubieuzeDebiteurW>();
              
        
              this.NieuweTijdensBoekjaar = new List<EY.com.eBook.RuleEngine.AY2012.DubieuzeDebiteurenAppTypes.DubieuzeDebiteurN>();
              
        
              this.ToestandEindHuidigBoekjaar = new List<EY.com.eBook.RuleEngine.AY2012.DubieuzeDebiteurenAppTypes.DubieuzeDebiteurH>();

              this.List2043 = new List<EY.com.eBook.RuleEngine.AY2012.DubieuzeDebiteurenAppTypes.DubieuzeDebiteur2043>();

              this.LastUpdated = DateTime.Now;
          }
        
          
          #region entitymembers
          

        
          [DataMember(Order = 1)]
          public List<EY.com.eBook.RuleEngine.AY2012.DubieuzeDebiteurenAppTypes.DubieuzeDebiteurT> ToestandEindVorigBoekjaar {get;set;}


        
          [DataMember(Order = 2)]
          public List<EY.com.eBook.RuleEngine.AY2012.DubieuzeDebiteurenAppTypes.DubieuzeDebiteurW> WijzigingBestaandeTijdensBoekjaar {get;set;}


        
          [DataMember(Order = 3)]
          public List<EY.com.eBook.RuleEngine.AY2012.DubieuzeDebiteurenAppTypes.DubieuzeDebiteurN> NieuweTijdensBoekjaar {get;set;}


        
          [DataMember(Order = 4)]
          public List<EY.com.eBook.RuleEngine.AY2012.DubieuzeDebiteurenAppTypes.DubieuzeDebiteurH> ToestandEindHuidigBoekjaar {get;set;}

          [DataMember(Order = 5)]
          public List<EY.com.eBook.RuleEngine.AY2012.DubieuzeDebiteurenAppTypes.DubieuzeDebiteur2043> List2043 { get; set; }

      
          #endregion
          
          #region IWorksheet Members
          [DataMember(Order = 6)]
          public DateTime? LastUpdated { get; set; }

          #endregion
      }

  
   
    #endregion

}
  