﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.RuleEngine.Core;
using EY.com.eBook.RuleEngine.Core.Interfaces;
using EY.com.eBook.RuleEngine.Data;

    
namespace EY.com.eBook.RuleEngine.AY2012.BelasteReservesAppTypes
{
    #region subtypes


      [DataContract(Namespace = "AY2012.BelasteReservesApp")]
      public class BelasteReservesItem : IEntity
      {
          #region IEntity
          
          [DataMember(Name = "ID", Order=1)]
          public Guid Id { get; set; }
          
          [DataMember(Order=2)]
          public DateTime? LastUpdated { get; set; }
          
          [DataMember(Order = 3)]
          public bool IsValid { get; set; }
          
          [DataMember(Order = 4)]
          public bool PreviousImported { get; set; }
          
          #endregion
          
          #region entitymembers
          

        
          [DataMember(Order = 5)]
          public StringListItemDataContract Grootboek {get;set;}


        
          [DataMember(Order = 6)]
          public string Omschrijving {get;set;}


        
          [DataMember(Order = 7)]
          public string ListDescription {get;set;}


        
          [DataMember(Order = 8)]
          public decimal? BeginToestand {get;set;}


        
          [DataMember(Order = 9)]
          public decimal? ToenameTijdensBJ {get;set;}


        
          [DataMember(Order = 10)]
          public decimal? AfnameTijdensBJ {get;set;}


        
          [DataMember(Order = 11)]
          public decimal? EindToestand {get;set;}


        
          [DataMember(Order = 12)]
          public bool? Autoline {get;set;}


        
          [DataMember(Order = 13)]
          public string AutoKey {get;set;}


        
          [DataMember(Order = 14)]
          public int? Order {get;set;}


        
          [DataMember(Order = 15)]
          public GuidListItemDataContract TypeReseve {get;set;}


        
          [DataMember(Order = 16)]
          public bool? Delete {get;set;}


          #endregion
      }

  
      [DataContract(Namespace = "AY2012.BelasteReservesApp")]
      public class AanpassingBTReservesItem : IEntity
      {
          #region IEntity
          
          [DataMember(Name = "ID", Order=1)]
          public Guid Id { get; set; }
          
          [DataMember(Order=2)]
          public DateTime? LastUpdated { get; set; }
          
          [DataMember(Order = 3)]
          public bool IsValid { get; set; }
          
          [DataMember(Order = 4)]
          public bool PreviousImported { get; set; }
          
          #endregion
          
          #region entitymembers
          

        
          [DataMember(Order = 5)]
          public string Omschrijving {get;set;}


        
          [DataMember(Order = 6)]
          public decimal? Bedrag {get;set;}


        
          [DataMember(Order = 7)]
          public GuidListItemDataContract Soort {get;set;}


        
          [DataMember(Order = 8)]
          public decimal? AutoPercentage {get;set;}


        
          [DataMember(Order = 9)]
          public decimal? Percentage {get;set;}


        
          [DataMember(Order = 10)]
          public decimal? BedragNaPercentage {get;set;}


        
          [DataMember(Order = 11)]
          public bool? Auto {get;set;}


        
          [DataMember(Order = 12)]
          public string AutoKey {get;set;}


        
          [DataMember(Order = 13)]
          public GuidListItemDataContract TypeReseve {get;set;}


          #endregion
      }

  

    #endregion

    #region roottype
    

      [DataContract(Namespace = "AY2012.BelasteReservesApp")]
      public class BelasteReserves : IWorksheet
      {
          public BelasteReserves() {
              // Set Minimal

        
              this.AllBelasteReserves = new List<EY.com.eBook.RuleEngine.AY2012.BelasteReservesAppTypes.BelasteReservesItem>();
              
        
              this.AanpassingBTReserves = new List<EY.com.eBook.RuleEngine.AY2012.BelasteReservesAppTypes.AanpassingBTReservesItem>();
              
              this.LastUpdated = DateTime.Now;
          }
        
          
          #region entitymembers
          

        
          [DataMember(Order = 1)]
          public List<EY.com.eBook.RuleEngine.AY2012.BelasteReservesAppTypes.BelasteReservesItem> AllBelasteReserves {get;set;}


        
          [DataMember(Order = 2)]
          public List<EY.com.eBook.RuleEngine.AY2012.BelasteReservesAppTypes.AanpassingBTReservesItem> AanpassingBTReserves {get;set;}


      
          #endregion
          
          #region IWorksheet Members
          [DataMember(Order = 3)]
          public DateTime? LastUpdated { get; set; }

          #endregion
      }

  
   
    #endregion

}
  