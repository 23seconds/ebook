﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.RuleEngine.Core;
using EY.com.eBook.RuleEngine.Core.Interfaces;
using EY.com.eBook.RuleEngine.Data;

    
namespace EY.com.eBook.RuleEngine.AY2012.VergelijkendeCijfersAppTypes
{
    #region subtypes


      [DataContract(Namespace = "AY2012.VergelijkendeCijfersApp")]
      public class CijferBlok : IEntity
      {
          #region IEntity
          
          [DataMember(Name = "ID", Order=1)]
          public Guid Id { get; set; }
          
          [DataMember(Order=2)]
          public DateTime? LastUpdated { get; set; }
          
          [DataMember(Order = 3)]
          public bool IsValid { get; set; }
          
          [DataMember(Order = 4)]
          public bool PreviousImported { get; set; }
          
          #endregion
          
          #region entitymembers
          

        
          [DataMember(Order = 5)]
          public string accountNr {get;set;}


        
          [DataMember(Order = 6)]
          public string accountDescription {get;set;}


        
          [DataMember(Order = 7)]
          public string innerDescription {get;set;}


        
          [DataMember(Order = 8)]
          public DateTime? eindeBoekjaar {get;set;}


        
          [DataMember(Order = 9)]
          public DateTime? eindeVorigBJ {get;set;}


        
          [DataMember(Order = 10)]
          public decimal? Saldo {get;set;}


        
          [DataMember(Order = 11)]
          public decimal? SaldoVorigBJ {get;set;}


        
          [DataMember(Order = 12)]
          public decimal? Beweging {get;set;}


        
          [DataMember(Order = 13)]
          public decimal? PercentBeweging {get;set;}


        
          [DataMember(Order = 14)]
          public string Notitie {get;set;}


        
          [DataMember(Order = 15)]
          public bool? delete {get;set;}


        
          [DataMember(Order = 16)]
          public string Coll {get;set;}


          #endregion
      }

  

    #endregion

    #region roottype
    

      [DataContract(Namespace = "AY2012.VergelijkendeCijfersApp")]
      public class WPVergelijkendeCijfers : IWorksheet
      {
          public WPVergelijkendeCijfers() {
              // Set Minimal

        
              this.Kosten = new List<EY.com.eBook.RuleEngine.AY2012.VergelijkendeCijfersAppTypes.CijferBlok>();
              
        
              this.Opbrengsten = new List<EY.com.eBook.RuleEngine.AY2012.VergelijkendeCijfersAppTypes.CijferBlok>();
              
              this.LastUpdated = DateTime.Now;
          }
        
          
          #region entitymembers
          

        
          [DataMember(Order = 1)]
          public List<EY.com.eBook.RuleEngine.AY2012.VergelijkendeCijfersAppTypes.CijferBlok> Kosten {get;set;}


        
          [DataMember(Order = 2)]
          public List<EY.com.eBook.RuleEngine.AY2012.VergelijkendeCijfersAppTypes.CijferBlok> Opbrengsten {get;set;}


        
          [DataMember(Order = 3)]
          public decimal? TotalGains {get;set;}


        
          [DataMember(Order = 4)]
          public decimal? Treshold {get;set;}


      
          #endregion
          
          #region IWorksheet Members
          [DataMember(Order = 5)]
          public DateTime? LastUpdated { get; set; }

          #endregion
      }

  
   
    #endregion

}
  