﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.RuleEngine.Core;
using EY.com.eBook.RuleEngine.Core.Interfaces;
using EY.com.eBook.RuleEngine.Data;

    
namespace EY.com.eBook.RuleEngine.AY2012.VoorschottenAppTypes
{
    #region subtypes


      [DataContract(Namespace = "AY2012.VoorschottenApp")]
      public class Gegeven : IEntity
      {
          #region IEntity
          
          [DataMember(Name = "ID", Order=1)]
          public Guid Id { get; set; }
          
          [DataMember(Order=2)]
          public DateTime? LastUpdated { get; set; }
          
          [DataMember(Order = 3)]
          public bool IsValid { get; set; }
          
          [DataMember(Order = 4)]
          public bool PreviousImported { get; set; }
          
          #endregion
          
          #region entitymembers
          

        
          [DataMember(Order = 5)]
          public GuidListItemDataContract Soort {get;set;}


        
          [DataMember(Order = 6)]
          public StringListItemDataContract ActiefRekening {get;set;}


        
          [DataMember(Order = 7)]
          public StringListItemDataContract PassiefRekening {get;set;}


        
          [DataMember(Order = 8)]
          public StringListItemDataContract Rekening75 {get;set;}


        
          [DataMember(Order = 9)]
          public StringListItemDataContract Rekening65 {get;set;}


        
          [DataMember(Order = 10)]
          public GuidListItemDataContract Type {get;set;}


        
          [DataMember(Order = 11)]
          public bool? Herkwalificatie {get;set;}


        
          [DataMember(Order = 12)]
          public bool? Boeken {get;set;}


        
          [DataMember(Order = 13)]
          public GuidListItemDataContract BookingType {get;set;}


        
          [DataMember(Order = 14)]
          public StringListItemDataContract VAARekening {get;set;}


        
          [DataMember(Order = 15)]
          public string ListDescription {get;set;}


        
          [DataMember(Order = 16)]
          public decimal? Debet {get;set;}


        
          [DataMember(Order = 17)]
          public decimal? Credit {get;set;}


        
          [DataMember(Order = 18)]
          public decimal? Saldo {get;set;}


        
          [DataMember(Order = 19)]
          public decimal? Interest {get;set;}


        
          [DataMember(Order = 20)]
          public decimal? Dividend {get;set;}


        
          [DataMember(Order = 21)]
          public decimal? Indicair {get;set;}


        
          [DataMember(Order = 22)]
          public decimal? Gemiddeld {get;set;}


        
          [DataMember(Order = 23)]
          public decimal? N {get;set;}


        
          [DataMember(Order = 24)]
          public bool? SubjectToTreshold {get;set;}


          #endregion
      }

  
      [DataContract(Namespace = "AY2012.VoorschottenApp")]
      public class IntrestVoet : IEntity
      {
          #region IEntity
          
          [DataMember(Name = "ID", Order=1)]
          public Guid Id { get; set; }
          
          [DataMember(Order=2)]
          public DateTime? LastUpdated { get; set; }
          
          [DataMember(Order = 3)]
          public bool IsValid { get; set; }
          
          [DataMember(Order = 4)]
          public bool PreviousImported { get; set; }
          
          #endregion
          
          #region entitymembers
          

        
          [DataMember(Order = 5)]
          public string VoorschotOmschrijving {get;set;}


        
          [DataMember(Order = 6)]
          public StringListItemDataContract Voorschot {get;set;}


        
          [DataMember(Order = 7)]
          public string EindMaand {get;set;}


        
          [DataMember(Order = 8)]
          public decimal? CreditIntrest {get;set;}


        
          [DataMember(Order = 9)]
          public decimal? DebetIntrest {get;set;}


        
          [DataMember(Order = 10)]
          public DateTime? StartDate {get;set;}


        
          [DataMember(Order = 11)]
          public DateTime? EndDate {get;set;}


          #endregion
      }

  
      [DataContract(Namespace = "AY2012.VoorschottenApp")]
      public class RentegevendVoorschot : IEntity
      {
          #region IEntity
          
          [DataMember(Name = "ID", Order=1)]
          public Guid Id { get; set; }
          
          [DataMember(Order=2)]
          public DateTime? LastUpdated { get; set; }
          
          [DataMember(Order = 3)]
          public bool IsValid { get; set; }
          
          [DataMember(Order = 4)]
          public bool PreviousImported { get; set; }
          
          #endregion
          
          #region entitymembers
          

        
          [DataMember(Order = 5)]
          public StringListItemDataContract Voorschot {get;set;}


        
          [DataMember(Order = 6)]
          public string VoorschotOmschrijving {get;set;}


        
          [DataMember(Order = 7)]
          public string Omschrijving {get;set;}


        
          [DataMember(Order = 8)]
          public DateTime? EndDate {get;set;}


        
          [DataMember(Order = 9)]
          public decimal? Saldo {get;set;}


        
          [DataMember(Order = 10)]
          public decimal? InterestPercentage {get;set;}


        
          [DataMember(Order = 11)]
          public decimal? N {get;set;}


        
          [DataMember(Order = 12)]
          public decimal? Interest {get;set;}


        
          [DataMember(Order = 13)]
          public decimal? Fluctuatie {get;set;}


        
          [DataMember(Order = 14)]
          public StringListItemDataContract Grootboekrekening {get;set;}


        
          [DataMember(Order = 15)]
          public bool? AutoLine {get;set;}


        
          [DataMember(Order = 16)]
          public decimal? debetRente {get;set;}


        
          [DataMember(Order = 17)]
          public decimal? creditRente {get;set;}


        
          [DataMember(Order = 18)]
          public decimal? Dividend {get;set;}


        
          [DataMember(Order = 19)]
          public GuidListItemDataContract SoortRV {get;set;}


        
          [DataMember(Order = 20)]
          public decimal? RVPercent {get;set;}


        
          [DataMember(Order = 21)]
          public decimal? RoerendeVoorheffing {get;set;}


        
          [DataMember(Order = 22)]
          public decimal? debet {get;set;}


        
          [DataMember(Order = 23)]
          public decimal? credit {get;set;}


        
          [DataMember(Order = 24)]
          public bool? Herkwalificatie {get;set;}


        
          [DataMember(Order = 25)]
          public int? order {get;set;}


        
          [DataMember(Order = 26)]
          public decimal? HTot {get;set;}


        
          [DataMember(Order = 27)]
          public decimal? I {get;set;}


        
          [DataMember(Order = 28)]
          public string AutoKey {get;set;}


          #endregion
      }

  

    #endregion

    #region roottype
    

      [DataContract(Namespace = "AY2012.VoorschottenApp")]
      public class Voorschotten : IWorksheet
      {
          public Voorschotten() {
              // Set Minimal

        
              this.Gegevens = new List<EY.com.eBook.RuleEngine.AY2012.VoorschottenAppTypes.Gegeven>();
              
        
              this.InterestVoeten = new List<EY.com.eBook.RuleEngine.AY2012.VoorschottenAppTypes.IntrestVoet>();
              
        
              this.RentegevendeVoorschotten = new List<EY.com.eBook.RuleEngine.AY2012.VoorschottenAppTypes.RentegevendVoorschot>();
              
              this.LastUpdated = DateTime.Now;
          }
        
          
          #region entitymembers
          

        
          [DataMember(Order = 1)]
          public List<EY.com.eBook.RuleEngine.AY2012.VoorschottenAppTypes.Gegeven> Gegevens {get;set;}


        
          [DataMember(Order = 2)]
          public List<EY.com.eBook.RuleEngine.AY2012.VoorschottenAppTypes.IntrestVoet> InterestVoeten {get;set;}


        
          [DataMember(Order = 3)]
          public List<EY.com.eBook.RuleEngine.AY2012.VoorschottenAppTypes.RentegevendVoorschot> RentegevendeVoorschotten {get;set;}


        
          [DataMember(Order = 4)]
          public decimal? Treshold {get;set;}


      
          #endregion
          
          #region IWorksheet Members
          [DataMember(Order = 5)]
          public DateTime? LastUpdated { get; set; }

          #endregion
      }

  
   
    #endregion

}
  