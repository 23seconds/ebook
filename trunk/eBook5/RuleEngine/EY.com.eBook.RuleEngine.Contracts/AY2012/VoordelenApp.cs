﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.RuleEngine.Core;
using EY.com.eBook.RuleEngine.Core.Interfaces;
using EY.com.eBook.RuleEngine.Data;


namespace EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes
{
    #region detail types

    [DataContract(Namespace = "AY2012.VoordelenApp")]
    public class VAA_DetailProratatie
    {


        #region entitymembers



        [DataMember(Order = 1)]
        public DateTime? CoeffStart { get; set; }



        [DataMember(Order = 2)]
        public DateTime? CoeffEnd { get; set; }



        [DataMember(Order = 3)]
        public int? CoeffDays { get; set; }



        [DataMember(Order = 4)]
        public decimal? CoeffValue { get; set; }



        [DataMember(Order = 5)]
        public DateTime? Start { get; set; }



        [DataMember(Order = 6)]
        public DateTime? End { get; set; }



        [DataMember(Order = 7)]
        public int? Days { get; set; }



        [DataMember(Order = 8)]
        public decimal? Result { get; set; }


        #endregion
    }


    [DataContract(Namespace = "AY2012.VoordelenApp")]
    public class VAA_DetailProratatieAuto
    {


        #region entitymembers



        [DataMember(Order = 1)]
        public DateTime? CoeffStart { get; set; }



        [DataMember(Order = 2)]
        public DateTime? CoeffEnd { get; set; }



        [DataMember(Order = 3)]
        public int? CoeffDays { get; set; }



        [DataMember(Order = 4)]
        public decimal? ReferentieCO2 { get; set; }



        [DataMember(Order = 5)]
        public decimal? CO2BasePercentage { get; set; }



        [DataMember(Order = 6)]
        public decimal? MinCO2Percentage { get; set; }



        [DataMember(Order = 7)]
        public decimal? MaxCO2Percentage { get; set; }



        [DataMember(Order = 8)]
        public decimal? StepCO2Percentage { get; set; }



        [DataMember(Order = 9)]
        public decimal? MinimumVAA { get; set; }



        [DataMember(Order = 10)]
        public DateTime? Start { get; set; }



        [DataMember(Order = 11)]
        public DateTime? End { get; set; }



        [DataMember(Order = 12)]
        public int? Days { get; set; }



        [DataMember(Order = 13)]
        public decimal? ProratedMinimum { get; set; }



        [DataMember(Order = 14)]
        public int? AgeMonths { get; set; }



        [DataMember(Order = 15)]
        public decimal? CatalogusPercentage { get; set; }



        [DataMember(Order = 16)]
        public decimal? Catalogus { get; set; }



        [DataMember(Order = 17)]
        public decimal? Result { get; set; }


        #endregion
    }


    [DataContract(Namespace = "AY2012.VoordelenApp")]
    public class VAA_DetailProratatieHuur
    {


        #region entitymembers



        [DataMember(Order = 1)]
        public DateTime? CoeffStart { get; set; }



        [DataMember(Order = 2)]
        public DateTime? CoeffEnd { get; set; }



        [DataMember(Order = 3)]
        public int? CoeffDays { get; set; }



        [DataMember(Order = 4)]
        public decimal? Basis { get; set; }



        [DataMember(Order = 5)]
        public decimal? IndexKI { get; set; }



        [DataMember(Order = 6)]
        public decimal? Multiplier { get; set; }



        [DataMember(Order = 7)]
        public DateTime? Start { get; set; }



        [DataMember(Order = 8)]
        public DateTime? End { get; set; }



        [DataMember(Order = 9)]
        public int? Days { get; set; }



        [DataMember(Order = 10)]
        public int? MonthsKI { get; set; }



        [DataMember(Order = 11)]
        public decimal? IndexedKI { get; set; }



        [DataMember(Order = 12)]
        public decimal? Result { get; set; }


        #endregion
    }


    #endregion

    #region subtypes


    [DataContract(Namespace = "AY2012.VoordelenApp")]
    public class VAA_Auto : IEntity
    {
        #region IEntity

        [DataMember(Name = "ID", Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public DateTime? LastUpdated { get; set; }

        [DataMember(Order = 3)]
        public bool IsValid { get; set; }

        [DataMember(Order = 4)]
        public bool PreviousImported { get; set; }

        #endregion

        #region entitymembers



        [DataMember(Order = 5)]
        public StringListItemDataContract GrootBoek { get; set; }



        [DataMember(Order = 6)]
        public GuidListItemDataContract Soort { get; set; }



        [DataMember(Order = 7)]
        public string Tekst { get; set; }



        [DataMember(Order = 8)]
        public string GridText { get; set; }



        [DataMember(Order = 9)]
        public DateTime? StartDate { get; set; }



        [DataMember(Order = 10)]
        public DateTime? EndDate { get; set; }



        [DataMember(Order = 11)]
        public GuidListItemDataContract AantalPerJaar { get; set; }



        [DataMember(Order = 12)]
        public decimal? PersoonlijkeBijdrage { get; set; }



        [DataMember(Order = 13)]
        public GuidListItemDataContract BTWPerc { get; set; }



        [DataMember(Order = 14)]
        public string GrootboekRekeningen { get; set; }



        [DataMember(Order = 15)]
        public bool? Book { get; set; }



        [DataMember(Order = 16)]
        public decimal? MvH { get; set; }



        [DataMember(Order = 17)]
        public decimal? BTW { get; set; }



        [DataMember(Order = 18)]
        public decimal? Totaal { get; set; }



        [DataMember(Order = 19)]
        public decimal? MvHRC { get; set; }



        [DataMember(Order = 20)]
        public decimal? BTWRC { get; set; }



        [DataMember(Order = 21)]
        public GuidListItemDataContract BookingType { get; set; }



        [DataMember(Order = 22)]
        public StringListItemDataContract TeBoekenDebetRekening { get; set; }


        #endregion
    }


    [DataContract(Namespace = "AY2012.VoordelenApp")]
    public class VAA_AutoCO2 : IEntity
    {
        #region IEntity

        [DataMember(Name = "ID", Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public DateTime? LastUpdated { get; set; }

        [DataMember(Order = 3)]
        public bool IsValid { get; set; }

        [DataMember(Order = 4)]
        public bool PreviousImported { get; set; }

        #endregion

        #region entitymembers



        [DataMember(Order = 5)]
        public StringListItemDataContract GrootBoek { get; set; }



        [DataMember(Order = 6)]
        public string Tekst { get; set; }



        [DataMember(Order = 7)]
        public DateTime? StartDate { get; set; }



        [DataMember(Order = 8)]
        public DateTime? EndDate { get; set; }



        [DataMember(Order = 9)]
        public bool? LichteVracht { get; set; }



        [DataMember(Order = 10)]
        public bool? Elektrische { get; set; }



        [DataMember(Order = 11)]
        public bool? Hybride { get; set; }



        [DataMember(Order = 12)]
        public GuidListItemDataContract Motortype { get; set; }



        [DataMember(Order = 13)]
        public decimal? CO2 { get; set; }



        [DataMember(Order = 14)]
        public GuidListItemDataContract AantalPerJaar { get; set; }



        [DataMember(Order = 15)]
        public decimal? PersoonlijkeBijdrage { get; set; }



        [DataMember(Order = 16)]
        public bool? Book { get; set; }



        [DataMember(Order = 17)]
        public decimal? MvH { get; set; }



        [DataMember(Order = 18)]
        public decimal? BTW { get; set; }



        [DataMember(Order = 19)]
        public decimal? Totaal { get; set; }



        [DataMember(Order = 20)]
        public GuidListItemDataContract BookingType { get; set; }



        [DataMember(Order = 21)]
        public StringListItemDataContract TeBoekenDebetRekening { get; set; }



        [DataMember(Order = 22)]
        public List<EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_DetailProratatie> Details { get; set; }


        #endregion
    }


    [DataContract(Namespace = "AY2012.VoordelenApp")]
    public class VAA_AutoRupo : IEntity
    {
        #region IEntity

        [DataMember(Name = "ID", Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public DateTime? LastUpdated { get; set; }

        [DataMember(Order = 3)]
        public bool IsValid { get; set; }

        [DataMember(Order = 4)]
        public bool PreviousImported { get; set; }

        #endregion

        #region entitymembers



        [DataMember(Order = 5)]
        public StringListItemDataContract GrootBoek { get; set; }



        [DataMember(Order = 6)]
        public GuidListItemDataContract TypeVoertuig { get; set; }



        [DataMember(Order = 7)]
        public string MerkModel { get; set; }



        [DataMember(Order = 8)]
        public string Verkrijger { get; set; }



        [DataMember(Order = 9)]
        public GuidListItemDataContract Motortype { get; set; }



        [DataMember(Order = 10)]
        public decimal? CO2 { get; set; }



        [DataMember(Order = 11)]
        public decimal? CatalogusWaarde { get; set; }



        [DataMember(Order = 12)]
        public DateTime? DatumInschrijving { get; set; }



        [DataMember(Order = 13)]
        public DateTime? StartDate { get; set; }



        [DataMember(Order = 14)]
        public DateTime? EndDate { get; set; }



        [DataMember(Order = 15)]
        public decimal? WerkelijkeWaarde { get; set; }



        [DataMember(Order = 16)]
        public decimal? PersoonlijkeBijdrage { get; set; }



        [DataMember(Order = 17)]
        public bool? TransferVU { get; set; }



        [DataMember(Order = 18)]
        public bool? Book { get; set; }



        [DataMember(Order = 19)]
        public decimal? MvH { get; set; }



        [DataMember(Order = 20)]
        public decimal? BTW { get; set; }



        [DataMember(Order = 21)]
        public decimal? Totaal { get; set; }



        [DataMember(Order = 22)]
        public decimal? VUWaarde { get; set; }



        [DataMember(Order = 23)]
        public GuidListItemDataContract BookingType { get; set; }



        [DataMember(Order = 24)]
        public StringListItemDataContract TeBoekenDebetRekening { get; set; }



        [DataMember(Order = 25)]
        public List<EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_DetailProratatieAuto> Details { get; set; }


        #endregion
    }


    [DataContract(Namespace = "AY2012.VoordelenApp")]
    public class VAA_Nuts : IEntity
    {
        #region IEntity

        [DataMember(Name = "ID", Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public DateTime? LastUpdated { get; set; }

        [DataMember(Order = 3)]
        public bool IsValid { get; set; }

        [DataMember(Order = 4)]
        public bool PreviousImported { get; set; }

        #endregion

        #region entitymembers



        [DataMember(Order = 5)]
        public StringListItemDataContract GrootBoek { get; set; }



        [DataMember(Order = 6)]
        public GuidListItemDataContract Soort { get; set; }



        [DataMember(Order = 7)]
        public string Tekst { get; set; }



        [DataMember(Order = 8)]
        public string GridText { get; set; }



        [DataMember(Order = 9)]
        public DateTime? StartDate { get; set; }



        [DataMember(Order = 10)]
        public DateTime? EndDate { get; set; }



        [DataMember(Order = 11)]
        public decimal? AantalPerJaar { get; set; }



        [DataMember(Order = 12)]
        public string GrootboekRekeningen { get; set; }



        [DataMember(Order = 13)]
        public bool? WettelijkForfait { get; set; }



        [DataMember(Order = 14)]
        public decimal? BedragFiscus { get; set; }



        [DataMember(Order = 15)]
        public decimal? PersoonlijkeBijdrage { get; set; }



        [DataMember(Order = 16)]
        public decimal? MvH { get; set; }



        [DataMember(Order = 17)]
        public decimal? BTW { get; set; }



        [DataMember(Order = 18)]
        public decimal? Totaal { get; set; }



        [DataMember(Order = 19)]
        public bool? Book { get; set; }



        [DataMember(Order = 20)]
        public StringListItemDataContract DebetRekening { get; set; }



        [DataMember(Order = 21)]
        public List<EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_DetailProratatie> Details { get; set; }


        #endregion
    }


    [DataContract(Namespace = "AY2012.VoordelenApp")]
    public class VAA_HUUR : IEntity
    {
        #region IEntity

        [DataMember(Name = "ID", Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public DateTime? LastUpdated { get; set; }

        [DataMember(Order = 3)]
        public bool IsValid { get; set; }

        [DataMember(Order = 4)]
        public bool PreviousImported { get; set; }

        #endregion

        #region entitymembers



        [DataMember(Order = 5)]
        public StringListItemDataContract GrootBoek { get; set; }



        [DataMember(Order = 6)]
        public GuidListItemDataContract Soort { get; set; }



        [DataMember(Order = 7)]
        public string Tekst { get; set; }



        [DataMember(Order = 8)]
        public string GridText { get; set; }



        [DataMember(Order = 9)]
        public DateTime? StartDate { get; set; }



        [DataMember(Order = 10)]
        public DateTime? EndDate { get; set; }



        [DataMember(Order = 11)]
        public GuidListItemDataContract EigenaarRechtstype { get; set; }



        [DataMember(Order = 12)]
        public decimal? AantalPerJaar { get; set; }



        [DataMember(Order = 13)]
        public decimal? KI { get; set; }



        [DataMember(Order = 14)]
        public decimal? indexedKI { get; set; }



        [DataMember(Order = 15)]
        public decimal? PersoonlijkeBijdrage { get; set; }



        [DataMember(Order = 16)]
        public string GrootboekRekeningen { get; set; }



        [DataMember(Order = 17)]
        public decimal? MvH { get; set; }



        [DataMember(Order = 18)]
        public decimal? BTW { get; set; }



        [DataMember(Order = 19)]
        public decimal? Totaal { get; set; }



        [DataMember(Order = 20)]
        public bool? Book { get; set; }



        [DataMember(Order = 21)]
        public StringListItemDataContract DebetRekening { get; set; }



        [DataMember(Order = 22)]
        public List<EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_DetailProratatieHuur> Details { get; set; }


        #endregion
    }


    [DataContract(Namespace = "AY2012.VoordelenApp")]
    public class VAA_HUURHERK : IEntity
    {
        #region IEntity

        [DataMember(Name = "ID", Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public DateTime? LastUpdated { get; set; }

        [DataMember(Order = 3)]
        public bool IsValid { get; set; }

        [DataMember(Order = 4)]
        public bool PreviousImported { get; set; }

        #endregion

        #region entitymembers



        [DataMember(Order = 5)]
        public StringListItemDataContract GrootBoek { get; set; }



        [DataMember(Order = 6)]
        public string Tekst { get; set; }



        [DataMember(Order = 7)]
        public decimal? KI { get; set; }



        [DataMember(Order = 8)]
        public decimal? KIVerhuurd { get; set; }



        [DataMember(Order = 9)]
        public decimal? OntvangenHuur { get; set; }



        [DataMember(Order = 10)]
        public bool? Gemeenschappelijk { get; set; }



        [DataMember(Order = 11)]
        public decimal? Percent { get; set; }



        [DataMember(Order = 12)]
        public decimal? Grenswaarde { get; set; }



        [DataMember(Order = 13)]
        public decimal? Herkwalificatie { get; set; }



        [DataMember(Order = 14)]
        public bool? Book { get; set; }



        [DataMember(Order = 15)]
        public StringListItemDataContract DebetRekening { get; set; }



        [DataMember(Order = 16)]
        public int? Soort { get; set; }


        #endregion
    }


    [DataContract(Namespace = "AY2012.VoordelenApp")]
    public class VAA_PC : IEntity
    {
        #region IEntity

        [DataMember(Name = "ID", Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public DateTime? LastUpdated { get; set; }

        [DataMember(Order = 3)]
        public bool IsValid { get; set; }

        [DataMember(Order = 4)]
        public bool PreviousImported { get; set; }

        #endregion

        #region entitymembers



        [DataMember(Order = 5)]
        public StringListItemDataContract GrootBoek { get; set; }



        [DataMember(Order = 6)]
        public GuidListItemDataContract Soort { get; set; }



        [DataMember(Order = 7)]
        public string Tekst { get; set; }



        [DataMember(Order = 8)]
        public string GridText { get; set; }



        [DataMember(Order = 9)]
        public DateTime? StartDate { get; set; }



        [DataMember(Order = 10)]
        public DateTime? EndDate { get; set; }



        [DataMember(Order = 11)]
        public decimal? AantalPerJaar { get; set; }



        [DataMember(Order = 12)]
        public decimal? PersoonlijkeBijdrage { get; set; }



        [DataMember(Order = 13)]
        public string GrootboekRekeningen { get; set; }



        [DataMember(Order = 14)]
        public decimal? MvH { get; set; }



        [DataMember(Order = 15)]
        public decimal? BTW { get; set; }



        [DataMember(Order = 16)]
        public decimal? Totaal { get; set; }



        [DataMember(Order = 17)]
        public bool? Book { get; set; }



        [DataMember(Order = 18)]
        public StringListItemDataContract DebetRekening { get; set; }



        [DataMember(Order = 19)]
        public List<EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_DetailProratatie> Details { get; set; }


        #endregion
    }


    [DataContract(Namespace = "AY2012.VoordelenApp")]
    public class VAA_Telefonie : IEntity
    {
        #region IEntity

        [DataMember(Name = "ID", Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public DateTime? LastUpdated { get; set; }

        [DataMember(Order = 3)]
        public bool IsValid { get; set; }

        [DataMember(Order = 4)]
        public bool PreviousImported { get; set; }

        #endregion

        #region entitymembers



        [DataMember(Order = 5)]
        public StringListItemDataContract GrootBoek { get; set; }



        [DataMember(Order = 6)]
        public GuidListItemDataContract Soort { get; set; }



        [DataMember(Order = 7)]
        public string Tekst { get; set; }



        [DataMember(Order = 8)]
        public string GridText { get; set; }



        [DataMember(Order = 9)]
        public DateTime? StartDate { get; set; }



        [DataMember(Order = 10)]
        public DateTime? EndDate { get; set; }



        [DataMember(Order = 11)]
        public decimal? AantalPerJaar { get; set; }



        [DataMember(Order = 12)]
        public decimal? BTWPerc { get; set; }



        [DataMember(Order = 13)]
        public decimal? PersoonlijkeBijdrage { get; set; }



        [DataMember(Order = 14)]
        public string GrootboekRekeningen { get; set; }



        [DataMember(Order = 15)]
        public decimal? MvH { get; set; }



        [DataMember(Order = 16)]
        public decimal? BTW { get; set; }



        [DataMember(Order = 17)]
        public decimal? Totaal { get; set; }



        [DataMember(Order = 18)]
        public bool? Book { get; set; }



        [DataMember(Order = 19)]
        public StringListItemDataContract DebetRekening { get; set; }



        [DataMember(Order = 20)]
        public List<EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_DetailProratatie> Details { get; set; }


        #endregion
    }


    [DataContract(Namespace = "AY2012.VoordelenApp")]
    public class VAA_Overige : IEntity
    {
        #region IEntity

        [DataMember(Name = "ID", Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public DateTime? LastUpdated { get; set; }

        [DataMember(Order = 3)]
        public bool IsValid { get; set; }

        [DataMember(Order = 4)]
        public bool PreviousImported { get; set; }

        #endregion

        #region entitymembers



        [DataMember(Order = 5)]
        public StringListItemDataContract GrootBoek { get; set; }



        [DataMember(Order = 6)]
        public string Tekst { get; set; }



        [DataMember(Order = 7)]
        public string GridText { get; set; }



        [DataMember(Order = 8)]
        public DateTime? StartDate { get; set; }



        [DataMember(Order = 9)]
        public DateTime? EndDate { get; set; }



        [DataMember(Order = 10)]
        public decimal? AantalPerJaar { get; set; }



        [DataMember(Order = 11)]
        public string GrootboekRekeningen { get; set; }



        [DataMember(Order = 12)]
        public decimal? MvH { get; set; }



        [DataMember(Order = 13)]
        public GuidListItemDataContract BTWPerc { get; set; }



        [DataMember(Order = 14)]
        public decimal? BTWBedrag { get; set; }



        [DataMember(Order = 15)]
        public decimal? PersoonlijkeBijdrage { get; set; }



        [DataMember(Order = 16)]
        public decimal? Totaal { get; set; }



        [DataMember(Order = 17)]
        public decimal? Bedragopjaarbasis { get; set; }



        [DataMember(Order = 18)]
        public bool? Book { get; set; }



        [DataMember(Order = 19)]
        public int? Soort { get; set; }



        [DataMember(Order = 20)]
        public StringListItemDataContract DebetRekening { get; set; }


        #endregion
    }


    [DataContract(Namespace = "AY2012.VoordelenApp")]
    public class VAA_Interesten_RC_Bedrijfsleider : IEntity
    {
        #region IEntity

        [DataMember(Name = "ID", Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public DateTime? LastUpdated { get; set; }

        [DataMember(Order = 3)]
        public bool IsValid { get; set; }

        [DataMember(Order = 4)]
        public bool PreviousImported { get; set; }

        #endregion

        #region entitymembers



        [DataMember(Order = 5)]
        public StringListItemDataContract GrootBoek { get; set; }



        [DataMember(Order = 6)]
        public int? Soort { get; set; }



        [DataMember(Order = 7)]
        public string Tekst { get; set; }



        [DataMember(Order = 8)]
        public string GridText { get; set; }



        [DataMember(Order = 9)]
        public DateTime? StartDate { get; set; }



        [DataMember(Order = 10)]
        public DateTime? EndDate { get; set; }



        [DataMember(Order = 11)]
        public decimal? BedragOpJaarbasis { get; set; }



        [DataMember(Order = 12)]
        public decimal? RVPerc { get; set; }



        [DataMember(Order = 13)]
        public decimal? MvH { get; set; }



        [DataMember(Order = 14)]
        public decimal? RV { get; set; }



        [DataMember(Order = 15)]
        public decimal? Totaal { get; set; }



        [DataMember(Order = 16)]
        public bool? Book { get; set; }



        [DataMember(Order = 17)]
        public StringListItemDataContract CreditRekening { get; set; }


        #endregion
    }


    [DataContract(Namespace = "AY2012.VoordelenApp")]
    public class VAA_Soc_Bijdr_Bedrijfsleider : IEntity
    {
        #region IEntity

        [DataMember(Name = "ID", Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public DateTime? LastUpdated { get; set; }

        [DataMember(Order = 3)]
        public bool IsValid { get; set; }

        [DataMember(Order = 4)]
        public bool PreviousImported { get; set; }

        #endregion

        #region entitymembers



        [DataMember(Order = 5)]
        public StringListItemDataContract GrootBoek { get; set; }



        [DataMember(Order = 6)]
        public int? Soort { get; set; }



        [DataMember(Order = 7)]
        public string Tekst { get; set; }



        [DataMember(Order = 8)]
        public string GridText { get; set; }



        [DataMember(Order = 9)]
        public DateTime? StartDate { get; set; }



        [DataMember(Order = 10)]
        public DateTime? EndDate { get; set; }



        [DataMember(Order = 11)]
        public string GrootboekRekeningen { get; set; }



        [DataMember(Order = 12)]
        public decimal? PersoonlijkeBijdrage { get; set; }



        [DataMember(Order = 13)]
        public decimal? BedragOpJaarBasis { get; set; }



        [DataMember(Order = 14)]
        public bool? Book { get; set; }



        [DataMember(Order = 15)]
        public StringListItemDataContract DebetRekening { get; set; }



        [DataMember(Order = 16)]
        public decimal? Totaal { get; set; }


        #endregion
    }


    [DataContract(Namespace = "AY2012.VoordelenApp")]
    public class VAA_Overige_Nuts : IEntity
    {
        #region IEntity

        [DataMember(Name = "ID", Order = 1)]
        public Guid Id { get; set; }

        [DataMember(Order = 2)]
        public DateTime? LastUpdated { get; set; }

        [DataMember(Order = 3)]
        public bool IsValid { get; set; }

        [DataMember(Order = 4)]
        public bool PreviousImported { get; set; }

        #endregion

        #region entitymembers



        [DataMember(Order = 5)]
        public StringListItemDataContract GrootBoek { get; set; }



        [DataMember(Order = 6)]
        public int? Soort { get; set; }



        [DataMember(Order = 7)]
        public string Tekst { get; set; }



        [DataMember(Order = 8)]
        public string GridText { get; set; }



        [DataMember(Order = 9)]
        public DateTime? StartDate { get; set; }



        [DataMember(Order = 10)]
        public DateTime? EndDate { get; set; }



        [DataMember(Order = 11)]
        public string GrootboekRekeningen { get; set; }



        [DataMember(Order = 12)]
        public decimal? PersoonlijkeBijdrage { get; set; }



        [DataMember(Order = 13)]
        public decimal? BedragOpJaarbasis { get; set; }



        [DataMember(Order = 14)]
        public decimal? BTWPerc { get; set; }



        [DataMember(Order = 15)]
        public decimal? MvH { get; set; }



        [DataMember(Order = 16)]
        public decimal? BTW { get; set; }



        [DataMember(Order = 17)]
        public decimal? Totaal { get; set; }



        [DataMember(Order = 18)]
        public bool? Book { get; set; }



        [DataMember(Order = 19)]
        public StringListItemDataContract DebetRekening { get; set; }


        #endregion
    }



    #endregion

    #region roottype


    [DataContract(Namespace = "AY2012.VoordelenApp")]
    public class Voordelen : IWorksheet
    {
        public Voordelen()
        {
            // Set Minimal


            this.VoordelenVanAlleAardAuto = new List<EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_Auto>();


            this.VoordelenVanAlleAardAutoCO2 = new List<EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_AutoCO2>();


            this.VoordelenVanAlleAardAutoRupo = new List<EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_AutoRupo>();


            this.VoordelenVanAlleAardNuts = new List<EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_Nuts>();


            this.VoordelenVanAlleAardHuur = new List<EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_HUUR>();


            this.VoordelenVanAlleAardHuurHerkwalificatie = new List<EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_HUURHERK>();


            this.VoordelenVanAlleAardPC = new List<EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_PC>();


            this.VoordelenVanAlleAardTelefonie = new List<EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_Telefonie>();


            this.VoordelenVanAlleAardOverige = new List<EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_Overige>();


            this.VoordelenInterestenRCBedrijfsleider = new List<EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_Interesten_RC_Bedrijfsleider>();


            this.VoordelenSocialeBijdragenBedrijfsleider = new List<EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_Soc_Bijdr_Bedrijfsleider>();


            this.VoordelenOverigeNuts = new List<EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_Overige_Nuts>();

            this.LastUpdated = DateTime.Now;
        }


        #region entitymembers



        [DataMember(Order = 1)]
        public List<EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_Auto> VoordelenVanAlleAardAuto { get; set; }



        [DataMember(Order = 2)]
        public List<EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_AutoCO2> VoordelenVanAlleAardAutoCO2 { get; set; }



        [DataMember(Order = 3)]
        public List<EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_AutoRupo> VoordelenVanAlleAardAutoRupo { get; set; }



        [DataMember(Order = 4)]
        public List<EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_Nuts> VoordelenVanAlleAardNuts { get; set; }



        [DataMember(Order = 5)]
        public List<EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_HUUR> VoordelenVanAlleAardHuur { get; set; }



        [DataMember(Order = 6)]
        public List<EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_HUURHERK> VoordelenVanAlleAardHuurHerkwalificatie { get; set; }



        [DataMember(Order = 7)]
        public List<EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_PC> VoordelenVanAlleAardPC { get; set; }



        [DataMember(Order = 8)]
        public List<EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_Telefonie> VoordelenVanAlleAardTelefonie { get; set; }



        [DataMember(Order = 9)]
        public List<EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_Overige> VoordelenVanAlleAardOverige { get; set; }



        [DataMember(Order = 10)]
        public List<EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_Interesten_RC_Bedrijfsleider> VoordelenInterestenRCBedrijfsleider { get; set; }



        [DataMember(Order = 11)]
        public List<EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_Soc_Bijdr_Bedrijfsleider> VoordelenSocialeBijdragenBedrijfsleider { get; set; }



        [DataMember(Order = 12)]
        public List<EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_Overige_Nuts> VoordelenOverigeNuts { get; set; }



        [DataMember(Order = 13)]
        public string BetalenBTW { get; set; }



        [DataMember(Order = 14)]
        public string VorderenBTW { get; set; }



        [DataMember(Order = 15)]
        public string RoerendeVoorheffingDividend { get; set; }



        [DataMember(Order = 16)]
        public string RoerendeVoorheffingInterest { get; set; }



        #endregion

        #region IWorksheet Members
        [DataMember(Order = 17)]
        public DateTime? LastUpdated { get; set; }

        #endregion
    }



    #endregion

}
