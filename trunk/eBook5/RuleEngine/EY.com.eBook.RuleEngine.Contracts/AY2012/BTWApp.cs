﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.RuleEngine.Core;
using EY.com.eBook.RuleEngine.Core.Interfaces;
using EY.com.eBook.RuleEngine.Data;

    
namespace EY.com.eBook.RuleEngine.AY2012.BTWAppTypes
{
    #region subtypes


      [DataContract(Namespace = "AY2012.BTWApp")]
      public class OmzetAangifte : IEntity
      {
          #region IEntity
          
          [DataMember(Name = "ID", Order=1)]
          public Guid Id { get; set; }
          
          [DataMember(Order=2)]
          public DateTime? LastUpdated { get; set; }
          
          [DataMember(Order = 3)]
          public bool IsValid { get; set; }
          
          [DataMember(Order = 4)]
          public bool PreviousImported { get; set; }
          
          #endregion
          
          #region entitymembers
          

        
          [DataMember(Order = 5)]
          public StringListItemDataContract Y_M {get;set;}


        
          [DataMember(Order = 6)]
          public decimal? Tarief_0 {get;set;}


        
          [DataMember(Order = 7)]
          public decimal? Tarief_6 {get;set;}


        
          [DataMember(Order = 8)]
          public decimal? Tarief_12 {get;set;}


        
          [DataMember(Order = 9)]
          public decimal? Tarief_21 {get;set;}


        
          [DataMember(Order = 10)]
          public decimal? Tarief_44 {get;set;}


        
          [DataMember(Order = 11)]
          public decimal? Tarief_Medec {get;set;}


        
          [DataMember(Order = 12)]
          public decimal? Tarief_EU {get;set;}


        
          [DataMember(Order = 13)]
          public decimal? Tarief_Uitvoer {get;set;}


        
          [DataMember(Order = 14)]
          public decimal? Tarief_48 {get;set;}


        
          [DataMember(Order = 15)]
          public decimal? Tarief_49 {get;set;}


        
          [DataMember(Order = 16)]
          public decimal? Totaal {get;set;}


          #endregion
      }

  
      [DataContract(Namespace = "AY2012.BTWApp")]
      public class OmzetGeboekt : IEntity
      {
          #region IEntity
          
          [DataMember(Name = "ID", Order=1)]
          public Guid Id { get; set; }
          
          [DataMember(Order=2)]
          public DateTime? LastUpdated { get; set; }
          
          [DataMember(Order = 3)]
          public bool IsValid { get; set; }
          
          [DataMember(Order = 4)]
          public bool PreviousImported { get; set; }
          
          #endregion
          
          #region entitymembers
          

        
          [DataMember(Order = 5)]
          public string GB_NR {get;set;}


        
          [DataMember(Order = 6)]
          public string NaamRekening {get;set;}


        
          [DataMember(Order = 7)]
          public decimal? BeginSaldo {get;set;}


        
          [DataMember(Order = 8)]
          public decimal? Debet {get;set;}


        
          [DataMember(Order = 9)]
          public decimal? Credit {get;set;}


        
          [DataMember(Order = 10)]
          public decimal? Saldo {get;set;}


        
          [DataMember(Order = 11)]
          public bool? AutoLine {get;set;}


        
          [DataMember(Order = 12)]
          public string AutoKey {get;set;}


          #endregion
      }

  
      [DataContract(Namespace = "AY2012.BTWApp")]
      public class OmzetVerklaring : IEntity
      {
          #region IEntity
          
          [DataMember(Name = "ID", Order=1)]
          public Guid Id { get; set; }
          
          [DataMember(Order=2)]
          public DateTime? LastUpdated { get; set; }
          
          [DataMember(Order = 3)]
          public bool IsValid { get; set; }
          
          [DataMember(Order = 4)]
          public bool PreviousImported { get; set; }
          
          #endregion
          
          #region entitymembers
          

        
          [DataMember(Order = 5)]
          public StringListItemDataContract Grootboek {get;set;}


        
          [DataMember(Order = 6)]
          public string Omschrijving {get;set;}


        
          [DataMember(Order = 7)]
          public string BookingReference {get;set;}


        
          [DataMember(Order = 8)]
          public StringListItemDataContract Jaar {get;set;}


        
          [DataMember(Order = 9)]
          public decimal? Bedrag {get;set;}


        
          [DataMember(Order = 10)]
          public bool? AutoLine {get;set;}


        
          [DataMember(Order = 11)]
          public string AutoKey {get;set;}


        
          [DataMember(Order = 12)]
          public int? order {get;set;}


          #endregion
      }

  
      [DataContract(Namespace = "AY2012.BTWApp")]
      public class RCAangifte : IEntity
      {
          #region IEntity
          
          [DataMember(Name = "ID", Order=1)]
          public Guid Id { get; set; }
          
          [DataMember(Order=2)]
          public DateTime? LastUpdated { get; set; }
          
          [DataMember(Order = 3)]
          public bool IsValid { get; set; }
          
          [DataMember(Order = 4)]
          public bool PreviousImported { get; set; }
          
          #endregion
          
          #region entitymembers
          

        
          [DataMember(Order = 5)]
          public StringListItemDataContract Y_M {get;set;}


        
          [DataMember(Order = 6)]
          public decimal? Post54 {get;set;}


        
          [DataMember(Order = 7)]
          public decimal? Post55 {get;set;}


        
          [DataMember(Order = 8)]
          public decimal? Post56 {get;set;}


        
          [DataMember(Order = 9)]
          public decimal? Post57 {get;set;}


        
          [DataMember(Order = 10)]
          public decimal? Post59 {get;set;}


        
          [DataMember(Order = 11)]
          public decimal? Post61 {get;set;}


        
          [DataMember(Order = 12)]
          public decimal? Post62 {get;set;}


        
          [DataMember(Order = 13)]
          public decimal? Post63 {get;set;}


        
          [DataMember(Order = 14)]
          public decimal? Post64 {get;set;}


        
          [DataMember(Order = 15)]
          public decimal? Totaal {get;set;}


          #endregion
      }

  
      [DataContract(Namespace = "AY2012.BTWApp")]
      public class RCGeboekt : IEntity
      {
          #region IEntity
          
          [DataMember(Name = "ID", Order=1)]
          public Guid Id { get; set; }
          
          [DataMember(Order=2)]
          public DateTime? LastUpdated { get; set; }
          
          [DataMember(Order = 3)]
          public bool IsValid { get; set; }
          
          [DataMember(Order = 4)]
          public bool PreviousImported { get; set; }
          
          #endregion
          
          #region entitymembers
          

        
          [DataMember(Order = 5)]
          public string GB_NR {get;set;}


        
          [DataMember(Order = 6)]
          public string NaamRekening {get;set;}


        
          [DataMember(Order = 7)]
          public decimal? BeginSaldo {get;set;}


        
          [DataMember(Order = 8)]
          public decimal? Debet {get;set;}


        
          [DataMember(Order = 9)]
          public decimal? Credit {get;set;}


        
          [DataMember(Order = 10)]
          public decimal? Saldo {get;set;}


        
          [DataMember(Order = 11)]
          public bool? AutoLine {get;set;}


        
          [DataMember(Order = 12)]
          public string AutoKey {get;set;}


          #endregion
      }

  
      [DataContract(Namespace = "AY2012.BTWApp")]
      public class RCVerklaring : IEntity
      {
          #region IEntity
          
          [DataMember(Name = "ID", Order=1)]
          public Guid Id { get; set; }
          
          [DataMember(Order=2)]
          public DateTime? LastUpdated { get; set; }
          
          [DataMember(Order = 3)]
          public bool IsValid { get; set; }
          
          [DataMember(Order = 4)]
          public bool PreviousImported { get; set; }
          
          #endregion
          
          #region entitymembers
          

        
          [DataMember(Order = 5)]
          public GuidListItemDataContract Soort {get;set;}


        
          [DataMember(Order = 6)]
          public StringListItemDataContract Grootboek {get;set;}


        
          [DataMember(Order = 7)]
          public string Omschrijving {get;set;}


        
          [DataMember(Order = 8)]
          public string BookingReference {get;set;}


        
          [DataMember(Order = 9)]
          public StringListItemDataContract Jaar {get;set;}


        
          [DataMember(Order = 10)]
          public decimal? Bedrag {get;set;}


        
          [DataMember(Order = 11)]
          public decimal? Vak61_62 {get;set;}


        
          [DataMember(Order = 12)]
          public bool? Boek {get;set;}


        
          [DataMember(Order = 13)]
          public bool? AutoLine {get;set;}


        
          [DataMember(Order = 14)]
          public string AutoKey {get;set;}


        
          [DataMember(Order = 15)]
          public string Jnl {get;set;}


        
          [DataMember(Order = 16)]
          public int? Nr {get;set;}


        
          [DataMember(Order = 17)]
          public decimal? Vak61 {get;set;}


        
          [DataMember(Order = 18)]
          public decimal? Vak62 {get;set;}


        
          [DataMember(Order = 19)]
          public int? order {get;set;}


          #endregion
      }

  

    #endregion

    #region roottype
    

      [DataContract(Namespace = "AY2012.BTWApp")]
      public class WPBTW : IWorksheet
      {
          public WPBTW() {
              // Set Minimal

        
              this.OmzetAangiftes = new List<EY.com.eBook.RuleEngine.AY2012.BTWAppTypes.OmzetAangifte>();
              
        
              this.OmzettenGeboekt = new List<EY.com.eBook.RuleEngine.AY2012.BTWAppTypes.OmzetGeboekt>();
              
        
              this.OmzetVerklaringen = new List<EY.com.eBook.RuleEngine.AY2012.BTWAppTypes.OmzetVerklaring>();
              
        
              this.RCAangiftes = new List<EY.com.eBook.RuleEngine.AY2012.BTWAppTypes.RCAangifte>();
              
        
              this.RCenGeboekt = new List<EY.com.eBook.RuleEngine.AY2012.BTWAppTypes.RCGeboekt>();
              
        
              this.RCVerklaringen = new List<EY.com.eBook.RuleEngine.AY2012.BTWAppTypes.RCVerklaring>();
              
              this.LastUpdated = DateTime.Now;
          }
        
          
          #region entitymembers
          

        
          [DataMember(Order = 1)]
          public List<EY.com.eBook.RuleEngine.AY2012.BTWAppTypes.OmzetAangifte> OmzetAangiftes {get;set;}


        
          [DataMember(Order = 2)]
          public List<EY.com.eBook.RuleEngine.AY2012.BTWAppTypes.OmzetGeboekt> OmzettenGeboekt {get;set;}


        
          [DataMember(Order = 3)]
          public List<EY.com.eBook.RuleEngine.AY2012.BTWAppTypes.OmzetVerklaring> OmzetVerklaringen {get;set;}


        
          [DataMember(Order = 4)]
          public List<EY.com.eBook.RuleEngine.AY2012.BTWAppTypes.RCAangifte> RCAangiftes {get;set;}


        
          [DataMember(Order = 5)]
          public List<EY.com.eBook.RuleEngine.AY2012.BTWAppTypes.RCGeboekt> RCenGeboekt {get;set;}


        
          [DataMember(Order = 6)]
          public List<EY.com.eBook.RuleEngine.AY2012.BTWAppTypes.RCVerklaring> RCVerklaringen {get;set;}


        
          [DataMember(Order = 7)]
          public decimal? TotaalSaldoOmzettenGeboekt {get;set;}


        
          [DataMember(Order = 8)]
          public decimal? TotaalSaldoRCGeboekt {get;set;}


      
          #endregion
          
          #region IWorksheet Members
          [DataMember(Order = 9)]
          public DateTime? LastUpdated { get; set; }

          #endregion
      }

  
   
    #endregion

}
  