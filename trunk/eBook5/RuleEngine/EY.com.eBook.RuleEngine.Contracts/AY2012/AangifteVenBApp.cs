﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.RuleEngine.Core;
using EY.com.eBook.RuleEngine.Core.Interfaces;
using EY.com.eBook.RuleEngine.Data;

    
namespace EY.com.eBook.RuleEngine.AY2012.AangifteVenBAppTypes
{
    #region subtypes


      [DataContract(Namespace = "AY2012.AangifteVenBApp")]
      public class ReserveEntity : IEntity
      {
          #region IEntity
          
          [DataMember(Name = "ID", Order=1)]
          public Guid Id { get; set; }
          
          [DataMember(Order=2)]
          public DateTime? LastUpdated { get; set; }
          
          [DataMember(Order = 3)]
          public bool IsValid { get; set; }
          
          [DataMember(Order = 4)]
          public bool PreviousImported { get; set; }
          
          #endregion
          
          #region entitymembers
          

        
          [DataMember(Order = 5)]
          public string TitleSpacer {get;set;}


        
          [DataMember(Order = 6)]
          public string TitleBT {get;set;}


        
          [DataMember(Order = 7)]
          public string TitleET {get;set;}


        
          [DataMember(Order = 8)]
          public string TitleBelastGeresWinst {get;set;}


        
          [DataMember(Order = 9)]
          public decimal? AKapitaalBT {get;set;}


        
          [DataMember(Order = 10)]
          public decimal? AKapitaalET {get;set;}


        
          [DataMember(Order = 11)]
          public decimal? AHerwaarderingBT {get;set;}


        
          [DataMember(Order = 12)]
          public decimal? AHerwaarderingET {get;set;}


        
          [DataMember(Order = 13)]
          public decimal? AWetReserveBT {get;set;}


        
          [DataMember(Order = 14)]
          public decimal? AWetReserveET {get;set;}


        
          [DataMember(Order = 15)]
          public decimal? AOnbeschikReserveBT {get;set;}


        
          [DataMember(Order = 16)]
          public decimal? AOnbeschikReserveET {get;set;}


        
          [DataMember(Order = 17)]
          public decimal? ABeschikReserveBT {get;set;}


        
          [DataMember(Order = 18)]
          public decimal? ABeschikReserveET {get;set;}


        
          [DataMember(Order = 19)]
          public decimal? AOvergedragenResWinstVerliesBT {get;set;}


        
          [DataMember(Order = 20)]
          public decimal? AOvergedragenResWinstVerliesET {get;set;}


        
          [DataMember(Order = 21)]
          public decimal? ABelastbareVoorzieningenBT {get;set;}


        
          [DataMember(Order = 22)]
          public decimal? ABelastbareVoorzieningenET {get;set;}


        
          [DataMember(Order = 23)]
          public string TitleAndereInBalans {get;set;}


        
          [DataMember(Order = 24)]
          public string AIBOmschrijving {get;set;}


        
          [DataMember(Order = 25)]
          public decimal? AIBalansBT {get;set;}


        
          [DataMember(Order = 26)]
          public decimal? AIBalansET {get;set;}


        
          [DataMember(Order = 27)]
          public string TitleAndereBelasteReserves {get;set;}


        
          [DataMember(Order = 28)]
          public string AIBROmschrijving {get;set;}


        
          [DataMember(Order = 29)]
          public decimal? AIBReserveBT {get;set;}


        
          [DataMember(Order = 30)]
          public decimal? AIBReserveET {get;set;}


        
          [DataMember(Order = 31)]
          public string TitleOnzichtbareReserves {get;set;}


        
          [DataMember(Order = 32)]
          public decimal? AORBelastbareWVBT {get;set;}


        
          [DataMember(Order = 33)]
          public decimal? AORBelastbareWVET {get;set;}


        
          [DataMember(Order = 34)]
          public decimal? AOROverdrevenAfschrBT {get;set;}


        
          [DataMember(Order = 35)]
          public decimal? AOROverdrevenAfschrET {get;set;}


        
          [DataMember(Order = 36)]
          public decimal? AORAndereActBT {get;set;}


        
          [DataMember(Order = 37)]
          public decimal? AORAndereActET {get;set;}


        
          [DataMember(Order = 38)]
          public decimal? AORAnderePassBT {get;set;}


        
          [DataMember(Order = 39)]
          public decimal? AORAnderePassET {get;set;}


        
          [DataMember(Order = 40)]
          public decimal? AORSubTotaalBT {get;set;}


        
          [DataMember(Order = 41)]
          public decimal? AORSubTotaalET {get;set;}


        
          [DataMember(Order = 42)]
          public string AAanpassingenMeerBTReservesTitle {get;set;}


        
          [DataMember(Order = 43)]
          public decimal? AAMRBTRMeerwaardeAandelenBT {get;set;}


        
          [DataMember(Order = 44)]
          public string Spacer4 {get;set;}


        
          [DataMember(Order = 45)]
          public string Spacer5 {get;set;}


        
          [DataMember(Order = 46)]
          public decimal? AAMRTerugnemingVUAandelen {get;set;}


        
          [DataMember(Order = 47)]
          public string Spacer20 {get;set;}


        
          [DataMember(Order = 48)]
          public string Spacer21 {get;set;}


        
          [DataMember(Order = 49)]
          public decimal? AAMRTaxShelterBT {get;set;}


        
          [DataMember(Order = 50)]
          public string Spacer6 {get;set;}


        
          [DataMember(Order = 51)]
          public string Spacer7 {get;set;}


        
          [DataMember(Order = 52)]
          public decimal? AAMRGewestPremiesBT {get;set;}


        
          [DataMember(Order = 53)]
          public string Spacer8 {get;set;}


        
          [DataMember(Order = 54)]
          public string Spacer9 {get;set;}


        
          [DataMember(Order = 55)]
          public decimal? AAMRDefVrijstellingWinst {get;set;}


        
          [DataMember(Order = 56)]
          public string Spacer22 {get;set;}


        
          [DataMember(Order = 57)]
          public string Spacer23 {get;set;}


        
          [DataMember(Order = 58)]
          public decimal? AAMRAndereBT {get;set;}


        
          [DataMember(Order = 59)]
          public string Spacer10 {get;set;}


        
          [DataMember(Order = 60)]
          public decimal? AAanpassingenMinBTReservesBT {get;set;}


        
          [DataMember(Order = 61)]
          public string Spacer11 {get;set;}


        
          [DataMember(Order = 62)]
          public decimal? ASubTotaalNaAanpassingBT {get;set;}


        
          [DataMember(Order = 63)]
          public string Spacer12 {get;set;}


        
          [DataMember(Order = 64)]
          public decimal? ATotaalET {get;set;}


          #endregion
      }

  
      [DataContract(Namespace = "AY2012.AangifteVenBApp")]
      public class VrijgesteldeReserveEntity : IEntity
      {
          #region IEntity
          
          [DataMember(Name = "ID", Order=1)]
          public Guid Id { get; set; }
          
          [DataMember(Order=2)]
          public DateTime? LastUpdated { get; set; }
          
          [DataMember(Order = 3)]
          public bool IsValid { get; set; }
          
          [DataMember(Order = 4)]
          public bool PreviousImported { get; set; }
          
          #endregion
          
          #region entitymembers
          

        
          [DataMember(Order = 5)]
          public string TitleSpacer {get;set;}


        
          [DataMember(Order = 6)]
          public string TitleBT {get;set;}


        
          [DataMember(Order = 7)]
          public string TitleET {get;set;}


        
          [DataMember(Order = 8)]
          public string TitleVrijgesteldeGeresWinst {get;set;}


        
          [DataMember(Order = 9)]
          public decimal? BWaardevHandelsvBT {get;set;}


        
          [DataMember(Order = 10)]
          public decimal? BWaardevHandelsvET {get;set;}


        
          [DataMember(Order = 11)]
          public decimal? BVoorzieningenBT {get;set;}


        
          [DataMember(Order = 12)]
          public decimal? BVoorzieningenET {get;set;}


        
          [DataMember(Order = 13)]
          public decimal? BNietVerwezMeerwBT {get;set;}


        
          [DataMember(Order = 14)]
          public decimal? BNietVerwezMeerwET {get;set;}


        
          [DataMember(Order = 15)]
          public string TitleVerwezenlijkteMW {get;set;}


        
          [DataMember(Order = 16)]
          public decimal? BGespreidVerwezMwBT {get;set;}


        
          [DataMember(Order = 17)]
          public decimal? BGespreidVerwezMwET {get;set;}


        
          [DataMember(Order = 18)]
          public decimal? BVerwezMeerwBT {get;set;}


        
          [DataMember(Order = 19)]
          public decimal? BVerwezMeerwET {get;set;}


        
          [DataMember(Order = 20)]
          public decimal? BBedrijfsVoertuigenBT {get;set;}


        
          [DataMember(Order = 21)]
          public decimal? BBedrijfsVoertuigenET {get;set;}


        
          [DataMember(Order = 22)]
          public decimal? BBinnenschepenBT {get;set;}


        
          [DataMember(Order = 23)]
          public decimal? BBinnenschepenET {get;set;}


        
          [DataMember(Order = 24)]
          public decimal? BZeeschepenBT {get;set;}


        
          [DataMember(Order = 25)]
          public decimal? BZeeschepenET {get;set;}


        
          [DataMember(Order = 26)]
          public decimal? BInvestReserveBT {get;set;}


        
          [DataMember(Order = 27)]
          public decimal? BInvestReserveET {get;set;}


        
          [DataMember(Order = 28)]
          public decimal? BTaxShelterBT {get;set;}


        
          [DataMember(Order = 29)]
          public decimal? BTaxShelterET {get;set;}


        
          [DataMember(Order = 30)]
          public decimal? BWinstHomologatieBT {get;set;}


        
          [DataMember(Order = 31)]
          public decimal? BWinstHomologatieET {get;set;}


        
          [DataMember(Order = 32)]
          public decimal? BAndereBT {get;set;}


        
          [DataMember(Order = 33)]
          public decimal? BAndereET {get;set;}


        
          [DataMember(Order = 34)]
          public decimal? BTotalenBT {get;set;}


        
          [DataMember(Order = 35)]
          public decimal? BTotalenET {get;set;}


          #endregion
      }

  
      [DataContract(Namespace = "AY2012.AangifteVenBApp")]
      public class VerworpenUitgavenEntity : IEntity
      {
          #region IEntity
          
          [DataMember(Name = "ID", Order=1)]
          public Guid Id { get; set; }
          
          [DataMember(Order=2)]
          public DateTime? LastUpdated { get; set; }
          
          [DataMember(Order = 3)]
          public bool IsValid { get; set; }
          
          [DataMember(Order = 4)]
          public bool PreviousImported { get; set; }
          
          #endregion
          
          #region entitymembers
          

        
          [DataMember(Order = 5)]
          public decimal? VUNietAftrekbareBelasting {get;set;}


        
          [DataMember(Order = 6)]
          public decimal? VUGewestBelast {get;set;}


        
          [DataMember(Order = 7)]
          public decimal? VUBoetes {get;set;}


        
          [DataMember(Order = 8)]
          public decimal? VUPensioenen {get;set;}


        
          [DataMember(Order = 9)]
          public decimal? VUAutokosten {get;set;}

          [DataMember(Order = 28)]
          public decimal? VUAutokostenVAA { get; set; }
        
          [DataMember(Order = 10)]
          public decimal? VUReceptie {get;set;}


        
          [DataMember(Order = 11)]
          public decimal? VURestaurant {get;set;}


        
          [DataMember(Order = 12)]
          public decimal? VUKledij {get;set;}


        
          [DataMember(Order = 13)]
          public decimal? VUOverdrevenIntresten {get;set;}


        
          [DataMember(Order = 14)]
          public decimal? VUBepaaldeLeningen {get;set;}


        
          [DataMember(Order = 15)]
          public decimal? VUAbnormaleGoedgVoordelen {get;set;}


        
          [DataMember(Order = 16)]
          public decimal? VUSocialeVoordelen {get;set;}


        
          [DataMember(Order = 17)]
          public decimal? VUMaaltijdSportCultuurEco {get;set;}


        
          [DataMember(Order = 18)]
          public decimal? VULiberaliteiten {get;set;}


        
          [DataMember(Order = 19)]
          public decimal? VUAandelen {get;set;}


        
          [DataMember(Order = 20)]
          public decimal? VUTerugnemingVrijstellingen {get;set;}


        
          [DataMember(Order = 21)]
          public decimal? VUWerknemerParticipatie {get;set;}


        
          [DataMember(Order = 22)]
          public decimal? VUCoupon {get;set;}


        
          [DataMember(Order = 23)]
          public decimal? VUTaxShelter {get;set;}


        
          [DataMember(Order = 24)]
          public decimal? VUGewestPremies {get;set;}


        
          [DataMember(Order = 25)]
          public decimal? VUBepaaldeStaten {get;set;}


        
          [DataMember(Order = 26)]
          public decimal? VUAndere {get;set;}


        
          [DataMember(Order = 27)]
          public decimal? VUTotaal {get;set;}


          #endregion
      }

  
      [DataContract(Namespace = "AY2012.AangifteVenBApp")]
      public class UitgekeerdeDividendenEntity : IEntity
      {
          #region IEntity
          
          [DataMember(Name = "ID", Order=1)]
          public Guid Id { get; set; }
          
          [DataMember(Order=2)]
          public DateTime? LastUpdated { get; set; }
          
          [DataMember(Order = 3)]
          public bool IsValid { get; set; }
          
          [DataMember(Order = 4)]
          public bool PreviousImported { get; set; }
          
          #endregion
          
          #region entitymembers
          

        
          [DataMember(Order = 5)]
          public string TitleBedragUitgekeerd {get;set;}


        
          [DataMember(Order = 6)]
          public decimal? UDGewone {get;set;}


        
          [DataMember(Order = 7)]
          public string Spacer1 {get;set;}


        
          [DataMember(Order = 8)]
          public decimal? UDAandelen {get;set;}


        
          [DataMember(Order = 9)]
          public string Spacer2 {get;set;}


        
          [DataMember(Order = 10)]
          public decimal? UDOverlijden {get;set;}


        
          [DataMember(Order = 11)]
          public string Spacer3 {get;set;}


        
          [DataMember(Order = 12)]
          public decimal? UDMaatschappelijk {get;set;}


        
          [DataMember(Order = 13)]
          public string Spacer4 {get;set;}


        
          [DataMember(Order = 14)]
          public decimal? UDTotaal {get;set;}


          #endregion
      }

  
      [DataContract(Namespace = "AY2012.AangifteVenBApp")]
      public class WinstEntity : IEntity
      {
          #region IEntity
          
          [DataMember(Name = "ID", Order=1)]
          public Guid Id { get; set; }
          
          [DataMember(Order=2)]
          public DateTime? LastUpdated { get; set; }
          
          [DataMember(Order = 3)]
          public bool IsValid { get; set; }
          
          [DataMember(Order = 4)]
          public bool PreviousImported { get; set; }
          
          #endregion
          
          #region entitymembers
          

        
          [DataMember(Order = 5)]
          public decimal? WIBelastGereservWinst {get;set;}


        
          [DataMember(Order = 6)]
          public decimal? WIVerworpenUitgaven {get;set;}


        
          [DataMember(Order = 7)]
          public decimal? WIUitgekeerdeDividenden {get;set;}


        
          [DataMember(Order = 8)]
          public decimal? WIResultaat {get;set;}


        
          [DataMember(Order = 9)]
          public decimal? WIZeeschip {get;set;}


        
          [DataMember(Order = 10)]
          public decimal? WIActiviteiten {get;set;}


        
          [DataMember(Order = 11)]
          public decimal? WIBestanddelenAftrekBepTotaal {get;set;}


        
          [DataMember(Order = 12)]
          public decimal? WIBABAbnormaalGoedg {get;set;}


          
        
          [DataMember(Order = 13)]
          public decimal? WIBABInvestVerplicht {get;set;}

          [DataMember(Order = 55)]
          public decimal? WIBABAutoVaa { get; set; }
        
          [DataMember(Order = 14)]
          public decimal? WIBABWerknemersPart {get;set;}


        
          [DataMember(Order = 15)]
          public decimal? WIBABKapitaal {get;set;}


        
          [DataMember(Order = 16)]
          public decimal? WIBResterendResultaat {get;set;}


        
          [DataMember(Order = 17)]
          public string WISpacer6 {get;set;}


        
          [DataMember(Order = 18)]
          public string TitleVerdragVrijgesteld {get;set;}


        
          [DataMember(Order = 19)]
          public string TitleNietBijVerdragVrijgesteld {get;set;}


        
          [DataMember(Order = 20)]
          public string TitleBelgisch {get;set;}


        
          [DataMember(Order = 21)]
          public decimal? WVVOResterendResultaatVV {get;set;}


        
          [DataMember(Order = 22)]
          public decimal? WVVOResterendResultaatNVV {get;set;}


        
          [DataMember(Order = 23)]
          public decimal? WVVOResterendResultaatBelg {get;set;}


        
          [DataMember(Order = 24)]
          public string TitleAftrekkenResterendeWinst {get;set;}


        
          [DataMember(Order = 25)]
          public decimal? WARWNietBelastbareBestandNVV {get;set;}


        
          [DataMember(Order = 26)]
          public decimal? WARWNietBelastbareBestandBelg {get;set;}


        
          [DataMember(Order = 27)]
          public decimal? WARWDefinitiefBelasteInkomstenNVV {get;set;}


        
          [DataMember(Order = 28)]
          public decimal? WARWDefinitiefBelasteInkomstenBelg {get;set;}


        
          [DataMember(Order = 29)]
          public decimal? WARWAftrekOctrooiNVV {get;set;}


        
          [DataMember(Order = 30)]
          public decimal? WARWAftrekOctrooiBelg {get;set;}


        
          [DataMember(Order = 31)]
          public decimal? WARWAftrekRisicoNVV {get;set;}


        
          [DataMember(Order = 32)]
          public decimal? WARWAftrekRisicoBelg {get;set;}


        
          [DataMember(Order = 33)]
          public decimal? WARWVorigeVerliezenNVV {get;set;}


        
          [DataMember(Order = 34)]
          public decimal? WARWVorigeVerliezenBelg {get;set;}


        
          [DataMember(Order = 35)]
          public decimal? WARWInvesteringsAftrekNVV {get;set;}


        
          [DataMember(Order = 36)]
          public decimal? WARWInvesteringsAftrekBelg {get;set;}


        
          [DataMember(Order = 37)]
          public decimal? WARWTotaalNVV {get;set;}


        
          [DataMember(Order = 38)]
          public decimal? WARWTotaalBelg {get;set;}


        
          [DataMember(Order = 39)]
          public string WISpacer18 {get;set;}


        
          [DataMember(Order = 40)]
          public string TitleBelastbaarTijdperk {get;set;}


        
          [DataMember(Order = 41)]
          public string TitleBelastbareGrondslag {get;set;}


        
          [DataMember(Order = 42)]
          public decimal? WIBGGewoonTarief {get;set;}


        
          [DataMember(Order = 43)]
          public decimal? WIBGResterendWinst {get;set;}


        
          [DataMember(Order = 44)]
          public decimal? WIBGZeescheepvaarTonnage {get;set;}


        
          [DataMember(Order = 45)]
          public decimal? WIBGAbnormaleGoedgunstig {get;set;}


        
          [DataMember(Order = 46)]
          public decimal? WIBGInvesteringsverplichting {get;set;}

          [DataMember(Order = 56)]
          public decimal? WIBGAutoVaa { get; set; }
        
          [DataMember(Order = 47)]
          public decimal? WIBGWerknemersParticipatie {get;set;}


        
          [DataMember(Order = 48)]
          public string TitleBelastbaarVerlaagdTarief {get;set;}

          [DataMember(Order = 57)]
          public decimal? WIBGMeerwaardeAandelenBelast { get; set; }

          [DataMember(Order = 58)]
          public decimal? WIBGExitTarief { get; set; }
        
          [DataMember(Order = 49)]
          public decimal? WIBGKapitaalInterestLandbouw {get;set;}


        
          [DataMember(Order = 50)]
          public decimal? CalcDivideDBI {get;set;}


        
          [DataMember(Order = 51)]
          public decimal? CalcDivideOctrooi {get;set;}


        
          [DataMember(Order = 52)]
          public decimal? CalcDivideNID {get;set;}


        
          [DataMember(Order = 53)]
          public decimal? CalcDivideVerliezen {get;set;}


        
          [DataMember(Order = 54)]
          public decimal? CalcDivideInvestering {get;set;}


          #endregion
      }

  
      [DataContract(Namespace = "AY2012.AangifteVenBApp")]
      public class AfzonderlijkeAanslagenEntity : IEntity
      {
          #region IEntity
          
          [DataMember(Name = "ID", Order=1)]
          public Guid Id { get; set; }
          
          [DataMember(Order=2)]
          public DateTime? LastUpdated { get; set; }
          
          [DataMember(Order = 3)]
          public bool IsValid { get; set; }
          
          [DataMember(Order = 4)]
          public bool PreviousImported { get; set; }
          
          #endregion
          
          #region entitymembers
          

        
          [DataMember(Order = 5)]
          public decimal? AAVoordelenAlleAard {get;set;}


        
          [DataMember(Order = 6)]
          public string TitleAABelasteReserves {get;set;}


        
          [DataMember(Order = 7)]
          public decimal? AABelasteReserves {get;set;}


        
          [DataMember(Order = 8)]
          public string TitleAAUitgekeerdeDividenden {get;set;}


        
          [DataMember(Order = 9)]
          public decimal? AAUitgekeerdeDividenden {get;set;}


        
          [DataMember(Order = 10)]
          public string TitleAABijzondereAanslagen {get;set;}


        
          [DataMember(Order = 11)]
          public string TitleAABAMaatschVermogen {get;set;}


        
          [DataMember(Order = 12)]
          public decimal? AABAMaatschVermogen33 {get;set;}


        
          [DataMember(Order = 13)]
          public decimal? AABAMaatschVermogen165 {get;set;}


        
          [DataMember(Order = 14)]
          public decimal? AABAVoordelenAlleAard {get;set;}


          #endregion
      }

  
      [DataContract(Namespace = "AY2012.AangifteVenBApp")]
      public class DiamantEntity : IEntity
      {
          #region IEntity
          
          [DataMember(Name = "ID", Order=1)]
          public Guid Id { get; set; }
          
          [DataMember(Order=2)]
          public DateTime? LastUpdated { get; set; }
          
          [DataMember(Order = 3)]
          public bool IsValid { get; set; }
          
          [DataMember(Order = 4)]
          public bool PreviousImported { get; set; }
          
          #endregion
          
          #region entitymembers
          

        
          [DataMember(Order = 5)]
          public decimal? DIAanvullingErkend {get;set;}


        
          [DataMember(Order = 6)]
          public decimal? DIOnderzoekOntwikkeling {get;set;}


          #endregion
      }

  
      [DataContract(Namespace = "AY2012.AangifteVenBApp")]
      public class NietBelastbareBestandelenItem : IEntity
      {
          #region IEntity
          
          [DataMember(Name = "ID", Order=1)]
          public Guid Id { get; set; }
          
          [DataMember(Order=2)]
          public DateTime? LastUpdated { get; set; }
          
          [DataMember(Order = 3)]
          public bool IsValid { get; set; }
          
          [DataMember(Order = 4)]
          public bool PreviousImported { get; set; }
          
          #endregion
          
          #region entitymembers
          

        
          [DataMember(Order = 5)]
          public string NonTaxableElements {get;set;}


        
          [DataMember(Order = 6)]
          public decimal? NTEBECharity {get;set;}


        
          [DataMember(Order = 7)]
          public decimal? NTEBEPersonnel {get;set;}


        
          [DataMember(Order = 8)]
          public decimal? NTEBEPersonnelSME {get;set;}


        
          [DataMember(Order = 9)]
          public decimal? NTEBEInternalshipPremium {get;set;}


        
          [DataMember(Order = 10)]
          public decimal? NTEBEOthers {get;set;}


        
          [DataMember(Order = 11)]
          public decimal? TotalNonTaxableElements {get;set;}


          #endregion
      }

  
      [DataContract(Namespace = "AY2012.AangifteVenBApp")]
      public class DbiEntity : IEntity
      {
          #region IEntity
          
          [DataMember(Name = "ID", Order=1)]
          public Guid Id { get; set; }
          
          [DataMember(Order=2)]
          public DateTime? LastUpdated { get; set; }
          
          [DataMember(Order = 3)]
          public bool IsValid { get; set; }
          
          [DataMember(Order = 4)]
          public bool PreviousImported { get; set; }
          
          #endregion
          
          #region entitymembers
          

        
          [DataMember(Order = 5)]
          public string TitleWaarden {get;set;}


        
          [DataMember(Order = 6)]
          public string TitleBelgisch {get;set;}


        
          [DataMember(Order = 7)]
          public string TitleBuitenlands {get;set;}


        
          [DataMember(Order = 8)]
          public string TitleTotaal {get;set;}


        
          [DataMember(Order = 9)]
          public string TitleAandelen {get;set;}


        
          [DataMember(Order = 10)]
          public string TitleAandelenEUDochter {get;set;}


        
          [DataMember(Order = 11)]
          public string TitleABelgisch {get;set;}


        
          [DataMember(Order = 12)]
          public decimal? DBIAbeNettoBEI {get;set;}


        
          [DataMember(Order = 13)]
          public decimal? DBIAbeNettoBUI {get;set;}


        
          [DataMember(Order = 14)]
          public decimal? DBIAbeNettoTotaal {get;set;}


        
          [DataMember(Order = 15)]
          public decimal? DBIAbeRoerendBEI {get;set;}


        
          [DataMember(Order = 16)]
          public decimal? DBIAbeRoerendBUI {get;set;}


        
          [DataMember(Order = 17)]
          public decimal? DBIAbeRoerendTotaal {get;set;}


        
          [DataMember(Order = 18)]
          public string TitleABuitenland {get;set;}


        
          [DataMember(Order = 19)]
          public decimal? DBIAbuNettoBEI {get;set;}


        
          [DataMember(Order = 20)]
          public decimal? DBIAbuNettoBUI {get;set;}


        
          [DataMember(Order = 21)]
          public decimal? DBIAbuNettoTotaal {get;set;}


        
          [DataMember(Order = 22)]
          public decimal? DBIAbuRoerendBEI {get;set;}


        
          [DataMember(Order = 23)]
          public decimal? DBIAbuRoerendBUI {get;set;}


        
          [DataMember(Order = 24)]
          public decimal? DBIAbuRoerendTotaal {get;set;}


        
          [DataMember(Order = 25)]
          public string TitleAandelenAndere {get;set;}


        
          [DataMember(Order = 26)]
          public string TitleAABelgisch {get;set;}


        
          [DataMember(Order = 27)]
          public decimal? DBIAAbeNettoBEI {get;set;}


        
          [DataMember(Order = 28)]
          public decimal? DBIAAbeNettoBUI {get;set;}


        
          [DataMember(Order = 29)]
          public decimal? DBIAAbeNettoTotaal {get;set;}


        
          [DataMember(Order = 30)]
          public decimal? DBIAAbeRoerendBEI {get;set;}


        
          [DataMember(Order = 31)]
          public decimal? DBIAAbeRoerendBUI {get;set;}


        
          [DataMember(Order = 32)]
          public decimal? DBIAAbeRoerendTotaal {get;set;}


        
          [DataMember(Order = 33)]
          public string TitleAABuitenland {get;set;}


        
          [DataMember(Order = 34)]
          public decimal? DBIAAbuNettoBEI {get;set;}


        
          [DataMember(Order = 35)]
          public decimal? DBIAAbuNettoBUI {get;set;}


        
          [DataMember(Order = 36)]
          public decimal? DBIAAbuNettoTotaal {get;set;}


        
          [DataMember(Order = 37)]
          public decimal? DBIAAbuRoerendBEI {get;set;}


        
          [DataMember(Order = 38)]
          public decimal? DBIAAbuRoerendBUI {get;set;}


        
          [DataMember(Order = 39)]
          public decimal? DBIAAbuRoerendTotaal {get;set;}


        
          [DataMember(Order = 40)]
          public decimal? DBIVrijgestelRoerendBEI {get;set;}


        
          [DataMember(Order = 41)]
          public decimal? DBIVrijgestelRoerendBUI {get;set;}


        
          [DataMember(Order = 42)]
          public decimal? DBIVrijgestelRoerendTotaal {get;set;}


        
          [DataMember(Order = 43)]
          public decimal? DBISubtotaalBEI {get;set;}


        
          [DataMember(Order = 44)]
          public decimal? DBISubtotaalBUI {get;set;}


        
          [DataMember(Order = 45)]
          public decimal? DBISubtotaalTotaal {get;set;}


        
          [DataMember(Order = 46)]
          public decimal? DBIKostenBEI {get;set;}


        
          [DataMember(Order = 47)]
          public decimal? DBIKostenBUI {get;set;}


        
          [DataMember(Order = 48)]
          public decimal? DBIKostenTotaal {get;set;}


        
          [DataMember(Order = 49)]
          public decimal? DBIVerschilBEI {get;set;}


        
          [DataMember(Order = 50)]
          public decimal? DBIVerschilBUI {get;set;}


        
          [DataMember(Order = 51)]
          public decimal? DBIVerschilTotaal {get;set;}


        
          [DataMember(Order = 52)]
          public decimal? DBILidEuBEI {get;set;}


        
          [DataMember(Order = 53)]
          public decimal? DBILidEuBUI {get;set;}


        
          [DataMember(Order = 54)]
          public decimal? DBILidEuTotaal {get;set;}


        
          [DataMember(Order = 55)]
          public decimal? DBIHerfinancBEI {get;set;}


        
          [DataMember(Order = 56)]
          public decimal? DBIHerfinancBUI {get;set;}


        
          [DataMember(Order = 57)]
          public decimal? DBIHerfinancTotaal {get;set;}


        
          [DataMember(Order = 58)]
          public decimal? DBITotaalBEI {get;set;}


        
          [DataMember(Order = 59)]
          public decimal? DBITotaalBUI {get;set;}


        
          [DataMember(Order = 60)]
          public decimal? DBITotaal {get;set;}


          #endregion
      }

  
      [DataContract(Namespace = "AY2012.AangifteVenBApp")]
      public class OverdrachtDBIAftrekEntity : IEntity
      {
          #region IEntity
          
          [DataMember(Name = "ID", Order=1)]
          public Guid Id { get; set; }
          
          [DataMember(Order=2)]
          public DateTime? LastUpdated { get; set; }
          
          [DataMember(Order = 3)]
          public bool IsValid { get; set; }
          
          [DataMember(Order = 4)]
          public bool PreviousImported { get; set; }
          
          #endregion
          
          #region entitymembers
          

        
          [DataMember(Order = 5)]
          public decimal? ODASaldoOvergedragen {get;set;}


        
          [DataMember(Order = 6)]
          public decimal? ODABedragHuidigNaarVolgend {get;set;}


        
          [DataMember(Order = 7)]
          public decimal? ODABedragSub1Werkelijk {get;set;}


        
          [DataMember(Order = 8)]
          public decimal? ODASaldoOverdraagbaarVolgend {get;set;}


          #endregion
      }

  
      [DataContract(Namespace = "AY2012.AangifteVenBApp")]
      public class OverdrachtNIDEntity : IEntity
      {
          #region IEntity
          
          [DataMember(Name = "ID", Order=1)]
          public Guid Id { get; set; }
          
          [DataMember(Order=2)]
          public DateTime? LastUpdated { get; set; }
          
          [DataMember(Order = 3)]
          public bool IsValid { get; set; }
          
          [DataMember(Order = 4)]
          public bool PreviousImported { get; set; }
          
          #endregion
          
          #region entitymembers
          

        
          [DataMember(Order = 5)]
          public decimal? ONIDSaldoOvergedragen {get;set;}


        
          [DataMember(Order = 6)]
          public decimal? ONIDOverdraagbaarVolgend {get;set;}


          #endregion
      }

  
      [DataContract(Namespace = "AY2012.AangifteVenBApp")]
      public class CompenseerbareVerliezenEntity : IEntity
      {
          #region IEntity
          
          [DataMember(Name = "ID", Order=1)]
          public Guid Id { get; set; }
          
          [DataMember(Order=2)]
          public DateTime? LastUpdated { get; set; }
          
          [DataMember(Order = 3)]
          public bool IsValid { get; set; }
          
          [DataMember(Order = 4)]
          public bool PreviousImported { get; set; }
          
          #endregion
          
          #region entitymembers
          

        
          [DataMember(Order = 5)]
          public decimal? CVSaldoVorigeVerliezen {get;set;}


        
          [DataMember(Order = 6)]
          public decimal? CVGecompenseerdeVerliezen {get;set;}


        
          [DataMember(Order = 7)]
          public decimal? CVVerliesBelastbaarTijdperk {get;set;}


        
          [DataMember(Order = 8)]
          public decimal? CVOverTeBrengen {get;set;}


          #endregion
      }

  
      [DataContract(Namespace = "AY2012.AangifteVenBApp")]
      public class TariefEntity : IEntity
      {
          #region IEntity
          
          [DataMember(Name = "ID", Order=1)]
          public Guid Id { get; set; }
          
          [DataMember(Order=2)]
          public DateTime? LastUpdated { get; set; }
          
          [DataMember(Order = 3)]
          public bool IsValid { get; set; }
          
          [DataMember(Order = 4)]
          public bool PreviousImported { get; set; }
          
          #endregion
          
          #region entitymembers
          

        
          [DataMember(Order = 5)]
          public bool? TARUitgeslotenVerminderd {get;set;}


        
          [DataMember(Order = 6)]
          public bool? TAROnderworpen {get;set;}


          #endregion
      }

  
      [DataContract(Namespace = "AY2012.AangifteVenBApp")]
      public class VoorafBetalingEntity : IEntity
      {
          #region IEntity
          
          [DataMember(Name = "ID", Order=1)]
          public Guid Id { get; set; }
          
          [DataMember(Order=2)]
          public DateTime? LastUpdated { get; set; }
          
          [DataMember(Order = 3)]
          public bool IsValid { get; set; }
          
          [DataMember(Order = 4)]
          public bool PreviousImported { get; set; }
          
          #endregion
          
          #region entitymembers
          

        
          [DataMember(Order = 5)]
          public bool? VBJongVennootschap {get;set;}


        
          [DataMember(Order = 6)]
          public decimal? VBEersteKwartaal {get;set;}


        
          [DataMember(Order = 7)]
          public decimal? VBTweedeKwartaal {get;set;}


        
          [DataMember(Order = 8)]
          public decimal? VBDerdeKwartaal {get;set;}


        
          [DataMember(Order = 9)]
          public decimal? VBVierdeKwartaal {get;set;}


        
          [DataMember(Order = 10)]
          public string VBEersteKwartaalReferentie {get;set;}


        
          [DataMember(Order = 11)]
          public string VBTweedeKwartaalReferentie {get;set;}


        
          [DataMember(Order = 12)]
          public string VBDerdeKwartaalReferentie {get;set;}


        
          [DataMember(Order = 13)]
          public string VBVierdeKwartaalReferentie {get;set;}


        
          [DataMember(Order = 14)]
          public decimal? VBTotaal {get;set;}


          #endregion
      }

  
      [DataContract(Namespace = "AY2012.AangifteVenBApp")]
      public class VerrekenbareVoorheffingEntity : IEntity
      {
          #region IEntity
          
          [DataMember(Name = "ID", Order=1)]
          public Guid Id { get; set; }
          
          [DataMember(Order=2)]
          public DateTime? LastUpdated { get; set; }
          
          [DataMember(Order = 3)]
          public bool IsValid { get; set; }
          
          [DataMember(Order = 4)]
          public bool PreviousImported { get; set; }
          
          #endregion
          
          #region entitymembers
          

        
          [DataMember(Order = 5)]
          public string TitleNietTerugbetaalbaar {get;set;}


        
          [DataMember(Order = 6)]
          public decimal? VVNTTotaal {get;set;}


        
          [DataMember(Order = 7)]
          public decimal? VVNTFictieveRoerendeVoorheffing {get;set;}


        
          [DataMember(Order = 8)]
          public decimal? VVNTForfaitairBuitenlands {get;set;}


        
          [DataMember(Order = 9)]
          public decimal? VVNTBelastingKredietOnderzoekOntw {get;set;}


        
          [DataMember(Order = 10)]
          public string TitleTerugbetaalbaar {get;set;}


        
          [DataMember(Order = 11)]
          public decimal? VVTTotaal {get;set;}


        
          [DataMember(Order = 12)]
          public decimal? VVTBelgisch {get;set;}


        
          [DataMember(Order = 13)]
          public decimal? VVTAndereBuitenlands {get;set;}


        
          [DataMember(Order = 14)]
          public decimal? VVTEigenBuitenlands {get;set;}


        
          [DataMember(Order = 15)]
          public decimal? VVTAndereEigen {get;set;}


        
          [DataMember(Order = 16)]
          public decimal? VVTAndereRoerendeVoorheffing {get;set;}


        
          [DataMember(Order = 17)]
          public decimal? VVTAndere {get;set;}


        
          [DataMember(Order = 18)]
          public decimal? VBelastingkredietHuiidTijdperk {get;set;}


        
          [DataMember(Order = 19)]
          public decimal? VVTTotaalExpected {get;set;}


          #endregion
      }

  
      [DataContract(Namespace = "AY2012.AangifteVenBApp")]
      public class TaxShelterEntity : IEntity
      {
          #region IEntity
          
          [DataMember(Name = "ID", Order=1)]
          public Guid Id { get; set; }
          
          [DataMember(Order=2)]
          public DateTime? LastUpdated { get; set; }
          
          [DataMember(Order = 3)]
          public bool IsValid { get; set; }
          
          [DataMember(Order = 4)]
          public bool PreviousImported { get; set; }
          
          #endregion
          
          #region entitymembers
          

        
          [DataMember(Order = 5)]
          public bool? TASTaxShelter {get;set;}


          #endregion
      }

  
      [DataContract(Namespace = "AY2012.AangifteVenBApp")]
      public class GrootteVennootschapeEntity : IEntity
      {
          #region IEntity
          
          [DataMember(Name = "ID", Order=1)]
          public Guid Id { get; set; }
          
          [DataMember(Order=2)]
          public DateTime? LastUpdated { get; set; }
          
          [DataMember(Order = 3)]
          public bool IsValid { get; set; }
          
          [DataMember(Order = 4)]
          public bool PreviousImported { get; set; }
          
          #endregion
          
          #region entitymembers
          

        
          [DataMember(Order = 5)]
          public string TitleInfo {get;set;}


        
          [DataMember(Order = 6)]
          public string TitleBelastbaarTijdperk {get;set;}


        
          [DataMember(Order = 7)]
          public bool? GVBTVerbonden {get;set;}


        
          [DataMember(Order = 8)]
          public string TitleOntkennend {get;set;}


        
          [DataMember(Order = 9)]
          public decimal? GVBTJaargemiddelde {get;set;}


        
          [DataMember(Order = 10)]
          public string Spacer14 {get;set;}


        
          [DataMember(Order = 11)]
          public decimal? GVBTJaarOmzet {get;set;}


        
          [DataMember(Order = 12)]
          public string Spacer15 {get;set;}


        
          [DataMember(Order = 13)]
          public decimal? GVBTBalansTotaal {get;set;}


        
          [DataMember(Order = 14)]
          public string Spacer16 {get;set;}


        
          [DataMember(Order = 15)]
          public bool? GVCGLaatsteTijdperk {get;set;}


        
          [DataMember(Order = 16)]
          public bool? GVCGVoorLaatsteTijdperk {get;set;}


          #endregion
      }

  

    #endregion

    #region roottype
    

      [DataContract(Namespace = "AY2012.AangifteVenBApp")]
      public class AangifteVenB : IWorksheet
      {
          public AangifteVenB() {
              // Set Minimal

        
              this.Reserves = new EY.com.eBook.RuleEngine.AY2012.AangifteVenBAppTypes.ReserveEntity();
              this.Reserves.Id = Guid.NewGuid();
        
              this.VrijgesteldeReserve = new EY.com.eBook.RuleEngine.AY2012.AangifteVenBAppTypes.VrijgesteldeReserveEntity();
              this.VrijgesteldeReserve.Id = Guid.NewGuid();
        
              this.VerworpenUitgaven = new EY.com.eBook.RuleEngine.AY2012.AangifteVenBAppTypes.VerworpenUitgavenEntity();
              this.VerworpenUitgaven.Id = Guid.NewGuid();
        
              this.UitgekeerdeDividenden = new EY.com.eBook.RuleEngine.AY2012.AangifteVenBAppTypes.UitgekeerdeDividendenEntity();
              this.UitgekeerdeDividenden.Id = Guid.NewGuid();
        
              this.UiteenzettingWinst = new EY.com.eBook.RuleEngine.AY2012.AangifteVenBAppTypes.WinstEntity();
              this.UiteenzettingWinst.Id = Guid.NewGuid();
        
              this.AfzonderlijkeAanslagen = new EY.com.eBook.RuleEngine.AY2012.AangifteVenBAppTypes.AfzonderlijkeAanslagenEntity();
              this.AfzonderlijkeAanslagen.Id = Guid.NewGuid();
        
              this.Diamant = new EY.com.eBook.RuleEngine.AY2012.AangifteVenBAppTypes.DiamantEntity();
              this.Diamant.Id = Guid.NewGuid();
        
              this.NietBelastbareBestanddelen = new EY.com.eBook.RuleEngine.AY2012.AangifteVenBAppTypes.NietBelastbareBestandelenItem();
              this.NietBelastbareBestanddelen.Id = Guid.NewGuid();
        
              this.DBI = new EY.com.eBook.RuleEngine.AY2012.AangifteVenBAppTypes.DbiEntity();
              this.DBI.Id = Guid.NewGuid();
        
              this.OverdrachtDBIAftrek = new EY.com.eBook.RuleEngine.AY2012.AangifteVenBAppTypes.OverdrachtDBIAftrekEntity();
              this.OverdrachtDBIAftrek.Id = Guid.NewGuid();
        
              this.OverdrachtNID = new EY.com.eBook.RuleEngine.AY2012.AangifteVenBAppTypes.OverdrachtNIDEntity();
              this.OverdrachtNID.Id = Guid.NewGuid();
        
              this.CompenseerbareVerliezen = new EY.com.eBook.RuleEngine.AY2012.AangifteVenBAppTypes.CompenseerbareVerliezenEntity();
              this.CompenseerbareVerliezen.Id = Guid.NewGuid();
        
              this.TariefBelasting = new EY.com.eBook.RuleEngine.AY2012.AangifteVenBAppTypes.TariefEntity();
              this.TariefBelasting.Id = Guid.NewGuid();
        
              this.VoorafBetalingen = new EY.com.eBook.RuleEngine.AY2012.AangifteVenBAppTypes.VoorafBetalingEntity();
              this.VoorafBetalingen.Id = Guid.NewGuid();
        
              this.VerrekenbareVoorheffing = new EY.com.eBook.RuleEngine.AY2012.AangifteVenBAppTypes.VerrekenbareVoorheffingEntity();
              this.VerrekenbareVoorheffing.Id = Guid.NewGuid();
        
              this.TaxShelter = new EY.com.eBook.RuleEngine.AY2012.AangifteVenBAppTypes.TaxShelterEntity();
              this.TaxShelter.Id = Guid.NewGuid();
        
              this.GrootteVennootschap = new EY.com.eBook.RuleEngine.AY2012.AangifteVenBAppTypes.GrootteVennootschapeEntity();
              this.GrootteVennootschap.Id = Guid.NewGuid();
              this.LastUpdated = DateTime.Now;
          }
        
          
          #region entitymembers
          

        
          [DataMember(Order = 1)]
          public EY.com.eBook.RuleEngine.AY2012.AangifteVenBAppTypes.ReserveEntity Reserves {get;set;}


        
          [DataMember(Order = 2)]
          public EY.com.eBook.RuleEngine.AY2012.AangifteVenBAppTypes.VrijgesteldeReserveEntity VrijgesteldeReserve {get;set;}


        
          [DataMember(Order = 3)]
          public EY.com.eBook.RuleEngine.AY2012.AangifteVenBAppTypes.VerworpenUitgavenEntity VerworpenUitgaven {get;set;}


        
          [DataMember(Order = 4)]
          public EY.com.eBook.RuleEngine.AY2012.AangifteVenBAppTypes.UitgekeerdeDividendenEntity UitgekeerdeDividenden {get;set;}


        
          [DataMember(Order = 5)]
          public EY.com.eBook.RuleEngine.AY2012.AangifteVenBAppTypes.WinstEntity UiteenzettingWinst {get;set;}


        
          [DataMember(Order = 6)]
          public EY.com.eBook.RuleEngine.AY2012.AangifteVenBAppTypes.AfzonderlijkeAanslagenEntity AfzonderlijkeAanslagen {get;set;}


        
          [DataMember(Order = 7)]
          public EY.com.eBook.RuleEngine.AY2012.AangifteVenBAppTypes.DiamantEntity Diamant {get;set;}


        
          [DataMember(Order = 8)]
          public EY.com.eBook.RuleEngine.AY2012.AangifteVenBAppTypes.NietBelastbareBestandelenItem NietBelastbareBestanddelen {get;set;}


        
          [DataMember(Order = 9)]
          public EY.com.eBook.RuleEngine.AY2012.AangifteVenBAppTypes.DbiEntity DBI {get;set;}


        
          [DataMember(Order = 10)]
          public EY.com.eBook.RuleEngine.AY2012.AangifteVenBAppTypes.OverdrachtDBIAftrekEntity OverdrachtDBIAftrek {get;set;}


        
          [DataMember(Order = 11)]
          public EY.com.eBook.RuleEngine.AY2012.AangifteVenBAppTypes.OverdrachtNIDEntity OverdrachtNID {get;set;}


        
          [DataMember(Order = 12)]
          public EY.com.eBook.RuleEngine.AY2012.AangifteVenBAppTypes.CompenseerbareVerliezenEntity CompenseerbareVerliezen {get;set;}


        
          [DataMember(Order = 13)]
          public EY.com.eBook.RuleEngine.AY2012.AangifteVenBAppTypes.TariefEntity TariefBelasting {get;set;}


        
          [DataMember(Order = 14)]
          public EY.com.eBook.RuleEngine.AY2012.AangifteVenBAppTypes.VoorafBetalingEntity VoorafBetalingen {get;set;}


        
          [DataMember(Order = 15)]
          public EY.com.eBook.RuleEngine.AY2012.AangifteVenBAppTypes.VerrekenbareVoorheffingEntity VerrekenbareVoorheffing {get;set;}


        
          [DataMember(Order = 16)]
          public EY.com.eBook.RuleEngine.AY2012.AangifteVenBAppTypes.TaxShelterEntity TaxShelter {get;set;}


        
          [DataMember(Order = 17)]
          public EY.com.eBook.RuleEngine.AY2012.AangifteVenBAppTypes.GrootteVennootschapeEntity GrootteVennootschap {get;set;}


      
          #endregion
          
          #region IWorksheet Members
          [DataMember(Order = 18)]
          public DateTime? LastUpdated { get; set; }

          #endregion
      }

  
   
    #endregion

}
  