﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.RuleEngine.Core;
using EY.com.eBook.RuleEngine.Core.Interfaces;
using EY.com.eBook.RuleEngine.Data;

    
namespace EY.com.eBook.RuleEngine.AY2012.VoorzieningenAppTypes
{
    #region subtypes


      [DataContract(Namespace = "AY2012.VoorzieningenApp")]
      public class VoorzieningenT : IEntity
      {
          #region IEntity
          
          [DataMember(Name = "ID", Order=1)]
          public Guid Id { get; set; }
          
          [DataMember(Order=2)]
          public DateTime? LastUpdated { get; set; }
          
          [DataMember(Order = 3)]
          public bool IsValid { get; set; }
          
          [DataMember(Order = 4)]
          public bool PreviousImported { get; set; }
          
          #endregion
          
          #region entitymembers
        
          [DataMember(Order = 5)]
          public string OmschrijvingProvisie {get;set;}

          [DataMember(Order = 6)]
          public int? Assesmentyear { get; set; }

          [DataMember(Order = 7)]
          public int? Nr { get; set; }
        
          [DataMember(Order = 8)]
          public decimal? WaarschijnlijkeKosten {get;set;}

          [DataMember(Order = 9)]
          public decimal? Voorziening {get;set;}

          [DataMember(Order = 10)]
          public StringListItemDataContract AardVanDeKosten { get; set; }
        
          [DataMember(Order = 11)]
          public GuidListItemDataContract Belastbaar {get;set;}

          #endregion
      }

  
      [DataContract(Namespace = "AY2012.VoorzieningenApp")]
      public class VoorzieningenW : IEntity
      {
          #region IEntity
          
          [DataMember(Name = "ID", Order=1)]
          public Guid Id { get; set; }
          
          [DataMember(Order=2)]
          public DateTime? LastUpdated { get; set; }
          
          [DataMember(Order = 3)]
          public bool IsValid { get; set; }
          
          [DataMember(Order = 4)]
          public bool PreviousImported { get; set; }
          
          #endregion
          
          #region entitymembers

          [DataMember(Order = 5)]
          public StringListItemDataContract Bestaande { get; set; }
        
          [DataMember(Order = 6)]
          public string OmschrijvingProvisie {get;set;}

          [DataMember(Order = 7)]
          public int? Assesmentyear { get; set; }

          [DataMember(Order = 8)]
          public int? Nr { get; set; }
        
          [DataMember(Order = 9)]
          public decimal? WaarschijnlijkeKostenBeginBJ {get;set;}

          [DataMember(Order = 10)]
          public decimal? WaarschijnlijkeKostenEindeBJ {get;set;}

          [DataMember(Order = 11)]
          public decimal? VoorzieningBeginBj {get;set;}

          [DataMember(Order = 12)]
          public decimal? VerminderingTijdensBJAanwending {get;set;}

          [DataMember(Order = 13)]
          public decimal? VerminderingTijdensBJNieuweInschatting { get; set; }

          [DataMember(Order = 14)]
          public decimal? VerhogingTijdensBJ {get;set;}

          [DataMember(Order = 15)]
          public decimal? VoorzieningEindeBJ {get;set;}

          [DataMember(Order = 16)]
          public StringListItemDataContract AardVanDeKosten { get; set; }
        
          [DataMember(Order = 17)]
          public GuidListItemDataContract Belastbaar {get;set;}
        
          [DataMember(Order = 18)]
          public bool? Book {get;set;}


          #endregion
      }

  
      [DataContract(Namespace = "AY2012.VoorzieningenApp")]
      public class VoorzieningenN : IEntity
      {
          #region IEntity
          
          [DataMember(Name = "ID", Order=1)]
          public Guid Id { get; set; }
          
          [DataMember(Order=2)]
          public DateTime? LastUpdated { get; set; }
          
          [DataMember(Order = 3)]
          public bool IsValid { get; set; }
          
          [DataMember(Order = 4)]
          public bool PreviousImported { get; set; }
          
          #endregion
          
          #region entitymembers
          
        
          [DataMember(Order = 5)]
          public string OmschrijvingProvisie {get;set;}

          [DataMember(Order = 6)]
          public int? Assesmentyear { get; set; }

          [DataMember(Order = 7)]
          public int? Nr { get; set; }

        
          [DataMember(Order = 8)]
          public decimal? WaarschijnlijkeKosten {get;set;}

        
          [DataMember(Order = 9)]
          public decimal? Voorziening {get;set;}


        
          [DataMember(Order = 10)]
          public StringListItemDataContract AardVanDeKosten {get;set;}


        
          [DataMember(Order = 11)]
          public GuidListItemDataContract Belastbaar {get;set;}


        
          [DataMember(Order = 12)]
          public bool? Book {get;set;}


          #endregion
      }

  
      [DataContract(Namespace = "AY2012.VoorzieningenApp")]
      public class VoorzieningenH : IEntity
      {
          #region IEntity
          
          [DataMember(Name = "ID", Order=1)]
          public Guid Id { get; set; }
          
          [DataMember(Order=2)]
          public DateTime? LastUpdated { get; set; }
          
          [DataMember(Order = 3)]
          public bool IsValid { get; set; }
          
          [DataMember(Order = 4)]
          public bool PreviousImported { get; set; }
          
          #endregion
          
          #region entitymembers
          

        
          [DataMember(Order = 5)]
          public string OmschrijvingProvisie {get;set;}

          [DataMember(Order = 6)]
          public int? Assesmentyear { get; set; }

          [DataMember(Order = 7)]
          public int? Nr { get; set; }
        
          [DataMember(Order = 8)]
          public decimal? WaarschijnlijkeKosten {get;set;}

        
          [DataMember(Order = 9)]
          public decimal? VoorzieningBeginBj {get;set;}


          [DataMember(Order = 10)]
          public decimal? VerminderingTijdensBJAanwending { get; set; }

          [DataMember(Order = 11)]
          public decimal? VerminderingTijdensBJNieuweInschatting { get; set; }

          [DataMember(Order = 12)]
          public decimal? VerhogingTijdensBJ { get; set; }

          [DataMember(Order = 13)]
          public decimal? VoorzieningEindeBJ { get; set; }
        
        
          [DataMember(Order = 14)]
          public StringListItemDataContract AardVanDeKosten {get;set;}

        
          [DataMember(Order = 15)]
          public GuidListItemDataContract Belastbaar {get;set;}

        
          [DataMember(Order = 16)]
          public bool? isNew {get;set;}


          #endregion
      }

      [DataContract(Namespace = "AY2012.VoorzieningenApp")]
      public class Voorzieningen2043 : IEntity
      {
          #region IEntity

          [DataMember(Name = "ID", Order = 1)]
          public Guid Id { get; set; }

          [DataMember(Order = 2)]
          public DateTime? LastUpdated { get; set; }

          [DataMember(Order = 3)]
          public bool IsValid { get; set; }

          [DataMember(Order = 4)]
          public bool PreviousImported { get; set; }

          #endregion

          [DataMember(Order = 5)]
          public string TypedMember { get; set; }

          [DataMember(Order = 6)]
          public string ExplicitMemberCategory { get; set; }

          [DataMember(Order = 7)]
          public string ExplicitMemberKind { get; set; }

          [DataMember(Order = 8)]
          public decimal? WaarschijnlijkeKosten { get; set; }

          [DataMember(Order = 9)]
          public decimal? VoorzieningBeginBj { get; set; }

          [DataMember(Order = 10)]
          public decimal? VoorzieningEindeBJ { get; set; }

          [DataMember(Order = 11)]
          public decimal? VerminderingTijdensBJAanwending { get; set; }

          [DataMember(Order = 12)]
          public decimal? VerminderingTijdensBJNieuweInschatting { get; set; }

          [DataMember(Order = 13)]
          public decimal? VerhogingTijdensBJ { get; set; }

          [DataMember(Order = 14)]
          public string Commentaar { get; set; }

      }
  

    #endregion

    #region roottype
    

      [DataContract(Namespace = "AY2012.VoorzieningenApp")]
      public class Voorzieningen : IWorksheet
      {
          public Voorzieningen() {
              // Set Minimal

        
              this.ToestandEindVorigBoekjaar = new List<EY.com.eBook.RuleEngine.AY2012.VoorzieningenAppTypes.VoorzieningenT>();
              
        
              this.WijzigingBestaandeTijdensBoekjaar = new List<EY.com.eBook.RuleEngine.AY2012.VoorzieningenAppTypes.VoorzieningenW>();
              
        
              this.NieuweTijdensBoekjaar = new List<EY.com.eBook.RuleEngine.AY2012.VoorzieningenAppTypes.VoorzieningenN>();
              
        
              this.ToestandEindHuidigBoekjaar = new List<EY.com.eBook.RuleEngine.AY2012.VoorzieningenAppTypes.VoorzieningenH>();

              this.List2043 = new List<EY.com.eBook.RuleEngine.AY2012.VoorzieningenAppTypes.Voorzieningen2043>();

              this.LastUpdated = DateTime.Now;
          }
        
          
          #region entitymembers
          

        
          [DataMember(Order = 1)]
          public List<EY.com.eBook.RuleEngine.AY2012.VoorzieningenAppTypes.VoorzieningenT> ToestandEindVorigBoekjaar {get;set;}


        
          [DataMember(Order = 2)]
          public List<EY.com.eBook.RuleEngine.AY2012.VoorzieningenAppTypes.VoorzieningenW> WijzigingBestaandeTijdensBoekjaar {get;set;}


        
          [DataMember(Order = 3)]
          public List<EY.com.eBook.RuleEngine.AY2012.VoorzieningenAppTypes.VoorzieningenN> NieuweTijdensBoekjaar {get;set;}


        
          [DataMember(Order = 4)]
          public List<EY.com.eBook.RuleEngine.AY2012.VoorzieningenAppTypes.VoorzieningenH> ToestandEindHuidigBoekjaar {get;set;}

          [DataMember(Order = 5)]
          public List<EY.com.eBook.RuleEngine.AY2012.VoorzieningenAppTypes.Voorzieningen2043> List2043 { get; set; }


      
          #endregion
          
          #region IWorksheet Members
          [DataMember(Order = 6)]
          public DateTime? LastUpdated { get; set; }

          #endregion
      }

  
   
    #endregion

}
  