﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.RuleEngine.Core;
using EY.com.eBook.RuleEngine.Core.Interfaces;
using EY.com.eBook.RuleEngine.Data;

    
namespace EY.com.eBook.RuleEngine.AY2012.VerliesHandelsdebiteurenAppTypes
{
    #region subtypes


      [DataContract(Namespace = "AY2012.VerliesHandelsdebiteurenApp")]
      public class VerliesHandelsDebiteur : IEntity
      {
          #region IEntity
          
          [DataMember(Name = "ID", Order=1)]
          public Guid Id { get; set; }
          
          [DataMember(Order=2)]
          public DateTime? LastUpdated { get; set; }
          
          [DataMember(Order = 3)]
          public bool IsValid { get; set; }
          
          [DataMember(Order = 4)]
          public bool PreviousImported { get; set; }
          
          #endregion
          
          #region entitymembers
          

        
          [DataMember(Order = 5)]
          public StringListItemDataContract Klant {get;set;}


        
          [DataMember(Order = 6)]
          public StringListItemDataContract GrootBoek {get;set;}


        
          [DataMember(Order = 7)]
          public StringListItemDataContract Jaar {get;set;}


        
          [DataMember(Order = 8)]
          public string FactuurNr {get;set;}


        
          [DataMember(Order = 9)]
          public decimal? Faillissement {get;set;}


        
          [DataMember(Order = 10)]
          public decimal? AndereReden {get;set;}


        
          [DataMember(Order = 11)]
          public decimal? BTW {get;set;}


        
          [DataMember(Order = 12)]
          public bool? Boeken {get;set;}


        
          [DataMember(Order = 13)]
          public string ImportedID {get;set;}


        
          [DataMember(Order = 14)]
          public bool? todelete {get;set;}


          #endregion
      }

  

    #endregion

    #region roottype
    

      [DataContract(Namespace = "AY2012.VerliesHandelsdebiteurenApp")]
      public class VerliesHandelsDebiteuren : IWorksheet
      {
          public VerliesHandelsDebiteuren() {
              // Set Minimal

        
              this.VerliesHD = new List<EY.com.eBook.RuleEngine.AY2012.VerliesHandelsdebiteurenAppTypes.VerliesHandelsDebiteur>();
              
              this.LastUpdated = DateTime.Now;
          }
        
          
          #region entitymembers
          

        
          [DataMember(Order = 1)]
          public List<EY.com.eBook.RuleEngine.AY2012.VerliesHandelsdebiteurenAppTypes.VerliesHandelsDebiteur> VerliesHD {get;set;}


      
          #endregion
          
          #region IWorksheet Members
          [DataMember(Order = 2)]
          public DateTime? LastUpdated { get; set; }

          #endregion
      }

  
   
    #endregion

}
  