﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.RuleEngine.Core;
using EY.com.eBook.RuleEngine.Core.Interfaces;
using EY.com.eBook.RuleEngine.Data;

    
namespace EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes
{
    #region subtypes


      [DataContract(Namespace = "AY2009.PersoneelApp")]
      public class Item325 : IEntity
      {
          #region IEntity
          
          [DataMember(Name = "ID", Order=1)]
          public Guid Id { get; set; }
          
          [DataMember(Order=2)]
          public DateTime? LastUpdated { get; set; }
          
          [DataMember(Order = 3)]
          public bool IsValid { get; set; }
          
          [DataMember(Order = 4)]
          public bool PreviousImported { get; set; }
          
          #endregion
          
          #region entitymembers
          

        
          [DataMember(Order = 5)]
          public StringListItemDataContract Kalenderjaar {get;set;}


        
          [DataMember(Order = 6)]
          public string Onderdeel {get;set;}


        
          [DataMember(Order = 7)]
          public string Omschrijving {get;set;}


        
          [DataMember(Order = 8)]
          public decimal? Bedrag {get;set;}


        
          [DataMember(Order = 9)]
          public decimal? Total {get;set;}


        
          [DataMember(Order = 10)]
          public bool? AutoLine {get;set;}


        
          [DataMember(Order = 11)]
          public int? order {get;set;}


          #endregion
      }

  
      [DataContract(Namespace = "AY2009.PersoneelApp")]
      public class VerschilItem : IEntity
      {
          #region IEntity
          
          [DataMember(Name = "ID", Order=1)]
          public Guid Id { get; set; }
          
          [DataMember(Order=2)]
          public DateTime? LastUpdated { get; set; }
          
          [DataMember(Order = 3)]
          public bool IsValid { get; set; }
          
          [DataMember(Order = 4)]
          public bool PreviousImported { get; set; }
          
          #endregion
          
          #region entitymembers
          

        
          [DataMember(Order = 5)]
          public string Onderdeel {get;set;}


        
          [DataMember(Order = 6)]
          public string Omschrijving {get;set;}


        
          [DataMember(Order = 7)]
          public string PrintOmschrijving {get;set;}


        
          [DataMember(Order = 8)]
          public decimal? Bedrag {get;set;}


        
          [DataMember(Order = 9)]
          public decimal? Total {get;set;}


        
          [DataMember(Order = 10)]
          public bool? AutoLine {get;set;}


        
          [DataMember(Order = 11)]
          public int? order {get;set;}


          #endregion
      }

  
      [DataContract(Namespace = "AY2009.PersoneelApp")]
      public class Werknemer : IEntity
      {
          #region IEntity
          
          [DataMember(Name = "ID", Order=1)]
          public Guid Id { get; set; }
          
          [DataMember(Order=2)]
          public DateTime? LastUpdated { get; set; }
          
          [DataMember(Order = 3)]
          public bool IsValid { get; set; }
          
          [DataMember(Order = 4)]
          public bool PreviousImported { get; set; }
          
          #endregion
          
          #region entitymembers
          

        
          [DataMember(Order = 5)]
          public string Omschrijving {get;set;}


        
          [DataMember(Order = 6)]
          public decimal? RSZ_WG {get;set;}


        
          [DataMember(Order = 7)]
          public decimal? Bruto {get;set;}


        
          [DataMember(Order = 8)]
          public decimal? VAA {get;set;}


        
          [DataMember(Order = 9)]
          public decimal? WW_Bel {get;set;}


        
          [DataMember(Order = 10)]
          public string EffectiefBruto {get;set;}


        
          [DataMember(Order = 11)]
          public decimal? RSZ_WN {get;set;}


        
          [DataMember(Order = 12)]
          public decimal? Belastbaar {get;set;}


        
          [DataMember(Order = 13)]
          public decimal? BV {get;set;}


        
          [DataMember(Order = 14)]
          public decimal? Maaltijd {get;set;}


        
          [DataMember(Order = 15)]
          public decimal? KEW {get;set;}


        
          [DataMember(Order = 16)]
          public decimal? WW_Vrij {get;set;}


        
          [DataMember(Order = 17)]
          public decimal? Bijz_Bijdrage {get;set;}


        
          [DataMember(Order = 18)]
          public decimal? Netto {get;set;}


          #endregion
      }

  
      [DataContract(Namespace = "AY2009.PersoneelApp")]
      public class Algemeen : IEntity
      {
          #region IEntity
          
          [DataMember(Name = "ID", Order=1)]
          public Guid Id { get; set; }
          
          [DataMember(Order=2)]
          public DateTime? LastUpdated { get; set; }
          
          [DataMember(Order = 3)]
          public bool IsValid { get; set; }
          
          [DataMember(Order = 4)]
          public bool PreviousImported { get; set; }
          
          #endregion
          
          #region entitymembers
          

        
          [DataMember(Order = 5)]
          public string Onderdeel {get;set;}


        
          [DataMember(Order = 6)]
          public string Omschrijving {get;set;}


        
          [DataMember(Order = 7)]
          public decimal? Bedrag {get;set;}


        
          [DataMember(Order = 8)]
          public decimal? Total {get;set;}


        
          [DataMember(Order = 9)]
          public bool? AutoLine {get;set;}


        
          [DataMember(Order = 10)]
          public int? order {get;set;}


          #endregion
      }

  
      [DataContract(Namespace = "AY2009.PersoneelApp")]
      public class Vakantiegeld : IEntity
      {
          #region IEntity
          
          [DataMember(Name = "ID", Order=1)]
          public Guid Id { get; set; }
          
          [DataMember(Order=2)]
          public DateTime? LastUpdated { get; set; }
          
          [DataMember(Order = 3)]
          public bool IsValid { get; set; }
          
          [DataMember(Order = 4)]
          public bool PreviousImported { get; set; }
          
          #endregion
          
          #region entitymembers
          

        
          [DataMember(Order = 5)]
          public string emptyLabel {get;set;}


        
          [DataMember(Order = 6)]
          public string labelArbeiders {get;set;}


        
          [DataMember(Order = 7)]
          public string labelBedienden {get;set;}


        
          [DataMember(Order = 8)]
          public string labelVorigBoekjaar {get;set;}


        
          [DataMember(Order = 9)]
          public string spacer1 {get;set;}


        
          [DataMember(Order = 10)]
          public decimal? VBArbeider {get;set;}


        
          [DataMember(Order = 11)]
          public decimal? VBBediende {get;set;}


        
          [DataMember(Order = 12)]
          public string labelHuidigBoekjaar {get;set;}


        
          [DataMember(Order = 13)]
          public string spacer2 {get;set;}


        
          [DataMember(Order = 14)]
          public decimal? BrutoArb {get;set;}


        
          [DataMember(Order = 15)]
          public decimal? BrutoBed {get;set;}


        
          [DataMember(Order = 16)]
          public decimal? PercArb {get;set;}


        
          [DataMember(Order = 17)]
          public decimal? PercBed {get;set;}


        
          [DataMember(Order = 18)]
          public int? maandBetaling {get;set;}


        
          [DataMember(Order = 19)]
          public decimal? theoretischArbeider {get;set;}


        
          [DataMember(Order = 20)]
          public decimal? theoretischBediende {get;set;}


        
          [DataMember(Order = 21)]
          public decimal? gekozenSchuldArbeider {get;set;}


        
          [DataMember(Order = 22)]
          public decimal? gekozenSchuldBediende {get;set;}


        
          [DataMember(Order = 23)]
          public decimal? TeBoekenArbeider {get;set;}


        
          [DataMember(Order = 24)]
          public decimal? TeBoekenBediende {get;set;}


        
          [DataMember(Order = 25)]
          public bool? Book {get;set;}


          #endregion
      }

  

    #endregion

    #region roottype
    

      [DataContract(Namespace = "AY2009.PersoneelApp")]
      public class WPPersoneel : IWorksheet
      {
          public WPPersoneel() {
              // Set Minimal

        
              this._325_20Coll = new List<EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.Item325>();
              
        
              this.TeVerklarenVerschil325_20 = new List<EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.VerschilItem>();
              
        
              this._325_10Coll = new List<EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.Item325>();
              
        
              this.TeVerklarenVerschil325_10 = new List<EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.VerschilItem>();
              
        
              this.ArbeidersForm = new EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.Werknemer();
              this.ArbeidersForm.Id = Guid.NewGuid();
        
              this.BediendeForm = new EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.Werknemer();
              this.BediendeForm.Id = Guid.NewGuid();
        
              this.AansluitingArbeiders = new List<EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.VerschilItem>();
              
        
              this.AansluitingBedienden = new List<EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.VerschilItem>();
              
        
              this.RSZColl = new List<EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.Algemeen>();
              
        
              this.BVsColl = new List<EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.Algemeen>();
              
        
              this.NettoColl = new List<EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.Algemeen>();
              
        
              this.VakantieGeldColl = new EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.Vakantiegeld();
              this.VakantieGeldColl.Id = Guid.NewGuid();
              this.LastUpdated = DateTime.Now;
          }
        
          
          #region entitymembers
          

        
          [DataMember(Order = 1)]
          public List<EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.Item325> _325_20Coll {get;set;}


        
          [DataMember(Order = 2)]
          public List<EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.VerschilItem> TeVerklarenVerschil325_20 {get;set;}


        
          [DataMember(Order = 3)]
          public List<EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.Item325> _325_10Coll {get;set;}


        
          [DataMember(Order = 4)]
          public List<EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.VerschilItem> TeVerklarenVerschil325_10 {get;set;}


        
          [DataMember(Order = 5)]
          public EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.Werknemer ArbeidersForm {get;set;}


        
          [DataMember(Order = 6)]
          public EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.Werknemer BediendeForm {get;set;}


        
          [DataMember(Order = 7)]
          public List<EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.VerschilItem> AansluitingArbeiders {get;set;}


        
          [DataMember(Order = 8)]
          public List<EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.VerschilItem> AansluitingBedienden {get;set;}


        
          [DataMember(Order = 9)]
          public List<EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.Algemeen> RSZColl {get;set;}


        
          [DataMember(Order = 10)]
          public List<EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.Algemeen> BVsColl {get;set;}


        
          [DataMember(Order = 11)]
          public List<EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.Algemeen> NettoColl {get;set;}


        
          [DataMember(Order = 12)]
          public EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.Vakantiegeld VakantieGeldColl {get;set;}


      
          #endregion
          
          #region IWorksheet Members
          [DataMember(Order = 13)]
          public DateTime? LastUpdated { get; set; }

          #endregion
      }

  
   
    #endregion

}
  