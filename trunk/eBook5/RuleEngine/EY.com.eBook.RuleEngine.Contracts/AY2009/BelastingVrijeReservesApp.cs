﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.RuleEngine.Core;
using EY.com.eBook.RuleEngine.Core.Interfaces;
using EY.com.eBook.RuleEngine.Data;

    
namespace EY.com.eBook.RuleEngine.AY2009.BelastingVrijeReservesAppTypes
{
    #region subtypes


      [DataContract(Namespace = "AY2009.BelastingVrijeReservesApp")]
      public class Detail : IEntity
      {
          #region IEntity
          
          [DataMember(Name = "ID", Order=1)]
          public Guid Id { get; set; }
          
          [DataMember(Order=2)]
          public DateTime? LastUpdated { get; set; }
          
          [DataMember(Order = 3)]
          public bool IsValid { get; set; }
          
          [DataMember(Order = 4)]
          public bool PreviousImported { get; set; }
          
          #endregion
          
          #region entitymembers
          

        
          [DataMember(Order = 5)]
          public string Title {get;set;}


        
          [DataMember(Order = 6)]
          public string TitleStart {get;set;}


        
          [DataMember(Order = 7)]
          public string TitlePos {get;set;}


        
          [DataMember(Order = 8)]
          public string TitleNeg {get;set;}


        
          [DataMember(Order = 9)]
          public string TitleEnd {get;set;}


        
          [DataMember(Order = 10)]
          public string ATitle {get;set;}


        
          [DataMember(Order = 11)]
          public decimal? AWaardeverminderingA {get;set;}


        
          [DataMember(Order = 12)]
          public decimal? AWaardeverminderingB {get;set;}


        
          [DataMember(Order = 13)]
          public decimal? AWaardeverminderingC {get;set;}


        
          [DataMember(Order = 14)]
          public decimal? AWaardeverminderingD {get;set;}


        
          [DataMember(Order = 15)]
          public string BTitle {get;set;}


        
          [DataMember(Order = 16)]
          public decimal? BVoorzieningA {get;set;}


        
          [DataMember(Order = 17)]
          public decimal? BVoorzieningB {get;set;}


        
          [DataMember(Order = 18)]
          public decimal? BVoorzieningC {get;set;}


        
          [DataMember(Order = 19)]
          public decimal? BVoorzieningD {get;set;}


        
          [DataMember(Order = 20)]
          public string CTitle {get;set;}


        
          [DataMember(Order = 21)]
          public decimal? CNietVerwezMWA {get;set;}


        
          [DataMember(Order = 22)]
          public decimal? CNietVerwezMWB {get;set;}


        
          [DataMember(Order = 23)]
          public decimal? CNietVerwezMWC {get;set;}


        
          [DataMember(Order = 24)]
          public decimal? CNietVerwezMWD {get;set;}


        
          [DataMember(Order = 25)]
          public decimal? CHerschattingA {get;set;}


        
          [DataMember(Order = 26)]
          public decimal? CHerschattingB {get;set;}


        
          [DataMember(Order = 27)]
          public decimal? CHerschattingC {get;set;}


        
          [DataMember(Order = 28)]
          public decimal? CHerschattingD {get;set;}


        
          [DataMember(Order = 29)]
          public decimal? CInbrengA {get;set;}


        
          [DataMember(Order = 30)]
          public decimal? CInbrengB {get;set;}


        
          [DataMember(Order = 31)]
          public decimal? CInbrengC {get;set;}


        
          [DataMember(Order = 32)]
          public decimal? CInbrengD {get;set;}


        
          [DataMember(Order = 33)]
          public decimal? CAndereA {get;set;}


        
          [DataMember(Order = 34)]
          public decimal? CAndereB {get;set;}


        
          [DataMember(Order = 35)]
          public decimal? CAndereC {get;set;}


        
          [DataMember(Order = 36)]
          public decimal? CAndereD {get;set;}


        
          [DataMember(Order = 37)]
          public decimal? CTotaalA {get;set;}


        
          [DataMember(Order = 38)]
          public decimal? CTotaalB {get;set;}


        
          [DataMember(Order = 39)]
          public decimal? CTotaalC {get;set;}


        
          [DataMember(Order = 40)]
          public decimal? CTotaalD {get;set;}


        
          [DataMember(Order = 41)]
          public string DTitle {get;set;}


        
          [DataMember(Order = 42)]
          public decimal? DMonetairA {get;set;}


        
          [DataMember(Order = 43)]
          public decimal? DMonetairB {get;set;}


        
          [DataMember(Order = 44)]
          public decimal? DMonetairC {get;set;}


        
          [DataMember(Order = 45)]
          public decimal? DMonetairD {get;set;}


        
          [DataMember(Order = 46)]
          public decimal? DNietVerwezenA {get;set;}


        
          [DataMember(Order = 47)]
          public decimal? DNietVerwezenB {get;set;}


        
          [DataMember(Order = 48)]
          public decimal? DNietVerwezenC {get;set;}


        
          [DataMember(Order = 49)]
          public decimal? DNietVerwezenD {get;set;}


        
          [DataMember(Order = 50)]
          public decimal? DBedrijfsAfdelingenA {get;set;}


        
          [DataMember(Order = 51)]
          public decimal? DBedrijfsAfdelingenB {get;set;}


        
          [DataMember(Order = 52)]
          public decimal? DBedrijfsAfdelingenC {get;set;}


        
          [DataMember(Order = 53)]
          public decimal? DBedrijfsAfdelingenD {get;set;}


        
          [DataMember(Order = 54)]
          public decimal? DGedwongenA {get;set;}


        
          [DataMember(Order = 55)]
          public decimal? DGedwongenB {get;set;}


        
          [DataMember(Order = 56)]
          public decimal? DGedwongenC {get;set;}


        
          [DataMember(Order = 57)]
          public decimal? DGedwongenD {get;set;}


        
          [DataMember(Order = 58)]
          public decimal? DNietMonetairA {get;set;}


        
          [DataMember(Order = 59)]
          public decimal? DNietMonetairB {get;set;}


        
          [DataMember(Order = 60)]
          public decimal? DNietMonetairC {get;set;}


        
          [DataMember(Order = 61)]
          public decimal? DNietMonetairD {get;set;}


        
          [DataMember(Order = 62)]
          public decimal? DVastgoedHandelaarsA {get;set;}


        
          [DataMember(Order = 63)]
          public decimal? DVastgoedHandelaarsB {get;set;}


        
          [DataMember(Order = 64)]
          public decimal? DVastgoedHandelaarsC {get;set;}


        
          [DataMember(Order = 65)]
          public decimal? DVastgoedHandelaarsD {get;set;}


        
          [DataMember(Order = 66)]
          public decimal? DProbleemgebiedenA {get;set;}


        
          [DataMember(Order = 67)]
          public decimal? DProbleemgebiedenB {get;set;}


        
          [DataMember(Order = 68)]
          public decimal? DProbleemgebiedenC {get;set;}


        
          [DataMember(Order = 69)]
          public decimal? DProbleemgebiedenD {get;set;}


        
          [DataMember(Order = 70)]
          public decimal? DAndereA {get;set;}


        
          [DataMember(Order = 71)]
          public decimal? DAndereB {get;set;}


        
          [DataMember(Order = 72)]
          public decimal? DAndereC {get;set;}


        
          [DataMember(Order = 73)]
          public decimal? DAndereD {get;set;}


        
          [DataMember(Order = 74)]
          public decimal? DTotaalA {get;set;}


        
          [DataMember(Order = 75)]
          public decimal? DTotaalB {get;set;}


        
          [DataMember(Order = 76)]
          public decimal? DTotaalC {get;set;}


        
          [DataMember(Order = 77)]
          public decimal? DTotaalD {get;set;}


        
          [DataMember(Order = 78)]
          public string ETitle {get;set;}


        
          [DataMember(Order = 79)]
          public decimal? EGespreidVerwezenA {get;set;}


        
          [DataMember(Order = 80)]
          public decimal? EGespreidVerwezenB {get;set;}


        
          [DataMember(Order = 81)]
          public decimal? EGespreidVerwezenC {get;set;}


        
          [DataMember(Order = 82)]
          public decimal? EGespreidVerwezenD {get;set;}


        
          [DataMember(Order = 83)]
          public string FTitle {get;set;}


        
          [DataMember(Order = 84)]
          public decimal? FBedrijfsvoertuigenA {get;set;}


        
          [DataMember(Order = 85)]
          public decimal? FBedrijfsvoertuigenB {get;set;}


        
          [DataMember(Order = 86)]
          public decimal? FBedrijfsvoertuigenC {get;set;}


        
          [DataMember(Order = 87)]
          public decimal? FBedrijfsvoertuigenD {get;set;}


        
          [DataMember(Order = 88)]
          public string GTitle {get;set;}


        
          [DataMember(Order = 89)]
          public decimal? GBinnenschepenA {get;set;}


        
          [DataMember(Order = 90)]
          public decimal? GBinnenschepenB {get;set;}


        
          [DataMember(Order = 91)]
          public decimal? GBinnenschepenC {get;set;}


        
          [DataMember(Order = 92)]
          public decimal? GBinnenschepenD {get;set;}


        
          [DataMember(Order = 93)]
          public string HTitle {get;set;}


        
          [DataMember(Order = 94)]
          public decimal? HZeeschepenA {get;set;}


        
          [DataMember(Order = 95)]
          public decimal? HZeeschepenB {get;set;}


        
          [DataMember(Order = 96)]
          public decimal? HZeeschepenC {get;set;}


        
          [DataMember(Order = 97)]
          public decimal? HZeeschepenD {get;set;}


        
          [DataMember(Order = 98)]
          public string ITitle {get;set;}


        
          [DataMember(Order = 99)]
          public decimal? IInvesteringsreserveA {get;set;}


        
          [DataMember(Order = 100)]
          public decimal? IInvesteringsreserveB {get;set;}


        
          [DataMember(Order = 101)]
          public decimal? IInvesteringsreserveC {get;set;}


        
          [DataMember(Order = 102)]
          public decimal? IInvesteringsreserveD {get;set;}


        
          [DataMember(Order = 103)]
          public string JTitle {get;set;}


        
          [DataMember(Order = 104)]
          public decimal? JTaxShelterAudioVisueelA {get;set;}


        
          [DataMember(Order = 105)]
          public decimal? JTaxShelterAudioVisueelB {get;set;}


        
          [DataMember(Order = 106)]
          public decimal? JTaxShelterAudioVisueelC {get;set;}


        
          [DataMember(Order = 107)]
          public decimal? JTaxShelterAudioVisueelD {get;set;}


        
          [DataMember(Order = 108)]
          public string KTitle {get;set;}


        
          [DataMember(Order = 109)]
          public decimal? KKapitaalSubsidiesA {get;set;}


        
          [DataMember(Order = 110)]
          public decimal? KKapitaalSubsidiesB {get;set;}


        
          [DataMember(Order = 111)]
          public decimal? KKapitaalSubsidiesC {get;set;}


        
          [DataMember(Order = 112)]
          public decimal? KKapitaalSubsidiesD {get;set;}


        
          [DataMember(Order = 113)]
          public decimal? KGeorganisGemVervoerA {get;set;}


        
          [DataMember(Order = 114)]
          public decimal? KGeorganisGemVervoerB {get;set;}


        
          [DataMember(Order = 115)]
          public decimal? KGeorganisGemVervoerC {get;set;}


        
          [DataMember(Order = 116)]
          public decimal? KGeorganisGemVervoerD {get;set;}


        
          [DataMember(Order = 117)]
          public decimal? KBeveiligingA {get;set;}


        
          [DataMember(Order = 118)]
          public decimal? KBeveiligingB {get;set;}


        
          [DataMember(Order = 119)]
          public decimal? KBeveiligingC {get;set;}


        
          [DataMember(Order = 120)]
          public decimal? KBeveiligingD {get;set;}


        
          [DataMember(Order = 121)]
          public decimal? KBedrijfsfietsenA {get;set;}


        
          [DataMember(Order = 122)]
          public decimal? KBedrijfsfietsenB {get;set;}


        
          [DataMember(Order = 123)]
          public decimal? KBedrijfsfietsenC {get;set;}


        
          [DataMember(Order = 124)]
          public decimal? KBedrijfsfietsenD {get;set;}


        
          [DataMember(Order = 125)]
          public decimal? KC02UitstootloosA {get;set;}


        
          [DataMember(Order = 126)]
          public decimal? KC02UitstootloosB {get;set;}


        
          [DataMember(Order = 127)]
          public decimal? KC02UitstootloosC {get;set;}


        
          [DataMember(Order = 128)]
          public decimal? KC02UitstootloosD {get;set;}


        
          [DataMember(Order = 129)]
          public decimal? KFIVReconversieVennA {get;set;}


        
          [DataMember(Order = 130)]
          public decimal? KFIVReconversieVennB {get;set;}


        
          [DataMember(Order = 131)]
          public decimal? KFIVReconversieVennC {get;set;}


        
          [DataMember(Order = 132)]
          public decimal? KFIVReconversieVennD {get;set;}


        
          [DataMember(Order = 133)]
          public decimal? KVrijWinstInnovatieVennA {get;set;}


        
          [DataMember(Order = 134)]
          public decimal? KVrijWinstInnovatieVennB {get;set;}


        
          [DataMember(Order = 135)]
          public decimal? KVrijWinstInnovatieVennC {get;set;}


        
          [DataMember(Order = 136)]
          public decimal? KVrijWinstInnovatieVennD {get;set;}


        
          [DataMember(Order = 137)]
          public decimal? KSociaalPassiefA {get;set;}


        
          [DataMember(Order = 138)]
          public decimal? KSociaalPassiefB {get;set;}


        
          [DataMember(Order = 139)]
          public decimal? KSociaalPassiefC {get;set;}


        
          [DataMember(Order = 140)]
          public decimal? KSociaalPassiefD {get;set;}


        
          [DataMember(Order = 141)]
          public decimal? KAfschrAanschafBelegWaardeA {get;set;}


        
          [DataMember(Order = 142)]
          public decimal? KAfschrAanschafBelegWaardeB {get;set;}


        
          [DataMember(Order = 143)]
          public decimal? KAfschrAanschafBelegWaardeC {get;set;}


        
          [DataMember(Order = 144)]
          public decimal? KAfschrAanschafBelegWaardeD {get;set;}


        
          [DataMember(Order = 145)]
          public decimal? KAfschrAanschBelegArt2A {get;set;}


        
          [DataMember(Order = 146)]
          public decimal? KAfschrAanschBelegArt2B {get;set;}


        
          [DataMember(Order = 147)]
          public decimal? KAfschrAanschBelegArt2C {get;set;}


        
          [DataMember(Order = 148)]
          public decimal? KAfschrAanschBelegArt2D {get;set;}


        
          [DataMember(Order = 149)]
          public decimal? KWetenschappelijkOndA {get;set;}


        
          [DataMember(Order = 150)]
          public decimal? KWetenschappelijkOndB {get;set;}


        
          [DataMember(Order = 151)]
          public decimal? KWetenschappelijkOndC {get;set;}


        
          [DataMember(Order = 152)]
          public decimal? KWetenschappelijkOndD {get;set;}


        
          [DataMember(Order = 153)]
          public decimal? KProvisiesSocVoordA {get;set;}


        
          [DataMember(Order = 154)]
          public decimal? KProvisiesSocVoordB {get;set;}


        
          [DataMember(Order = 155)]
          public decimal? KProvisiesSocVoordC {get;set;}


        
          [DataMember(Order = 156)]
          public decimal? KProvisiesSocVoordD {get;set;}


        
          [DataMember(Order = 157)]
          public decimal? KInvesteringsres1982A {get;set;}


        
          [DataMember(Order = 158)]
          public decimal? KInvesteringsres1982B {get;set;}


        
          [DataMember(Order = 159)]
          public decimal? KInvesteringsres1982C {get;set;}


        
          [DataMember(Order = 160)]
          public decimal? KInvesteringsres1982D {get;set;}


        
          [DataMember(Order = 161)]
          public decimal? KRSZInschakelingA {get;set;}


        
          [DataMember(Order = 162)]
          public decimal? KRSZInschakelingB {get;set;}


        
          [DataMember(Order = 163)]
          public decimal? KRSZInschakelingC {get;set;}


        
          [DataMember(Order = 164)]
          public decimal? KRSZInschakelingD {get;set;}


        
          [DataMember(Order = 165)]
          public decimal? KVoorraadDiamantA {get;set;}


        
          [DataMember(Order = 166)]
          public decimal? KVoorraadDiamantB {get;set;}


        
          [DataMember(Order = 167)]
          public decimal? KVoorraadDiamantC {get;set;}


        
          [DataMember(Order = 168)]
          public decimal? KVoorraadDiamantD {get;set;}


        
          [DataMember(Order = 169)]
          public decimal? KNettoActiefA {get;set;}


        
          [DataMember(Order = 170)]
          public decimal? KNettoActiefB {get;set;}


        
          [DataMember(Order = 171)]
          public decimal? KNettoActiefC {get;set;}


        
          [DataMember(Order = 172)]
          public decimal? KNettoActiefD {get;set;}


        
          [DataMember(Order = 173)]
          public decimal? KVZWA {get;set;}


        
          [DataMember(Order = 174)]
          public decimal? KVZWB {get;set;}


        
          [DataMember(Order = 175)]
          public decimal? KVZWC {get;set;}


        
          [DataMember(Order = 176)]
          public decimal? KVZWD {get;set;}


        
          [DataMember(Order = 177)]
          public decimal? KVerzekeringsOndA {get;set;}


        
          [DataMember(Order = 178)]
          public decimal? KVerzekeringsOndB {get;set;}


        
          [DataMember(Order = 179)]
          public decimal? KVerzekeringsOndC {get;set;}


        
          [DataMember(Order = 180)]
          public decimal? KVerzekeringsOndD {get;set;}


        
          [DataMember(Order = 181)]
          public decimal? KAndereA {get;set;}


        
          [DataMember(Order = 182)]
          public decimal? KAndereB {get;set;}


        
          [DataMember(Order = 183)]
          public decimal? KAndereC {get;set;}


        
          [DataMember(Order = 184)]
          public decimal? KAndereD {get;set;}


        
          [DataMember(Order = 185)]
          public decimal? KTotaalA {get;set;}


        
          [DataMember(Order = 186)]
          public decimal? KTotaalB {get;set;}


        
          [DataMember(Order = 187)]
          public decimal? KTotaalC {get;set;}


        
          [DataMember(Order = 188)]
          public decimal? KTotaalD {get;set;}


        
          [DataMember(Order = 189)]
          public decimal? TotaalVrijgesteldA {get;set;}


        
          [DataMember(Order = 190)]
          public decimal? TotaalVrijgesteldB {get;set;}


        
          [DataMember(Order = 191)]
          public decimal? TotaalVrijgesteldC {get;set;}


        
          [DataMember(Order = 192)]
          public decimal? TotaalVrijgesteldD {get;set;}


          #endregion
      }

  

    #endregion

    #region roottype
    

      [DataContract(Namespace = "AY2009.BelastingVrijeReservesApp")]
      public class BelastingVrijeReserves : IWorksheet
      {
          public BelastingVrijeReserves() {
              // Set Minimal

        
              this.VrijgesteldeReserves = new EY.com.eBook.RuleEngine.AY2009.BelastingVrijeReservesAppTypes.Detail();
              this.VrijgesteldeReserves.Id = Guid.NewGuid();
              this.LastUpdated = DateTime.Now;
          }
        
          
          #region entitymembers
          

        
          [DataMember(Order = 1)]
          public EY.com.eBook.RuleEngine.AY2009.BelastingVrijeReservesAppTypes.Detail VrijgesteldeReserves {get;set;}


      
          #endregion
          
          #region IWorksheet Members
          [DataMember(Order = 2)]
          public DateTime? LastUpdated { get; set; }

          #endregion
      }

  
   
    #endregion

}
  