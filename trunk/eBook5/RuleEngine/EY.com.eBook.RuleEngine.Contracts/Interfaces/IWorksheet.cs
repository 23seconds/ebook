﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ProtoBuf;

namespace EY.com.eBook.RuleEngine.Core.Interfaces
{
 
    [ProtoContract]   
    public interface IWorksheet
    {
        [ProtoMember(200)]
        DateTime? LastUpdated { get; set; }
    }
}
