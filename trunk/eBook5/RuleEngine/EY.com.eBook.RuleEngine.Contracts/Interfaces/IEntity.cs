﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.RuleEngine.Core.Interfaces
{
    public interface IEntity
    {
        [DataMember(Name = "ID")]
        Guid Id { get; set; }

        [DataMember]
        DateTime? LastUpdated { get; set; }

        [DataMember]
        bool IsValid { get; set; }

        [DataMember]
        bool PreviousImported { get; set; }
    }
}
