
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.RuleEngine.Core;
using EY.com.eBook.RuleEngine.Core.Interfaces;
using EY.com.eBook.RuleEngine.Data;
using EY.com.eBook.Core;
using EY.com.eBook.API.Contracts.Data.Cache;

namespace EY.com.eBook.RuleEngine.AY2015
{
    
    public class BTWApp
    {
        #region List-Info
        private Guid[] plusTekenSoorten = new Guid[] {new Guid("829d0f8f-4191-4f05-9055-cdd57a4c0a27"),
                                            new Guid("6ae9bc29-06e4-4614-afa9-c635691236df"),
                                            new Guid("dc525e80-c1e0-423e-ace5-ebb1f8e55d8f"),
                                            new Guid("4c914bde-d03b-41fe-8580-a5bde6e6ca4e"),
                                            new Guid("9de2e6cd-c4c9-4106-b432-13d9995dbeed"),
                                            new Guid("1a4c44f2-e013-4da5-8a7c-e4a230400f19"),
                                            new Guid("670df617-2ca1-4e8e-8e64-31806d7bc0dd")};

        private Guid[] minTekenSoorten = new Guid[] { new Guid("ae4fa422-ece0-4671-a942-fd875dc1325a")
                                             , new Guid("827a06c1-9f17-4fc7-84f5-373cc0519d08")
                                            , new Guid("b9db2b1c-3a46-4d80-94ae-c3dde445255c")
                                            , new Guid("5abdcdd0-67bd-41d6-a2bb-bcb6656895ec")};

        private Guid[] S_TekenSoorten = new Guid[] { new Guid("b4b1928d-fb4f-49f9-bd90-5f373324e9eb")
                                             , new Guid("45115ece-a9ab-410f-a95f-77417ca49f15") };

        private Guid[] boeken = new Guid[] { new Guid("827a06c1-9f17-4fc7-84f5-373cc0519d08")
                                            ,new Guid("dc525e80-c1e0-423e-ace5-ebb1f8e55d8f")
                                            ,new Guid("4c914bde-d03b-41fe-8580-a5bde6e6ca4e")
                                            ,new Guid("b9db2b1c-3a46-4d80-94ae-c3dde445255c")};

        private Guid[] s61 = new Guid[] { new Guid("ae4fa422-ece0-4671-a942-fd875dc1325a")
                                        ,new Guid("827a06c1-9f17-4fc7-84f5-373cc0519d08")
                                        ,new Guid("b9db2b1c-3a46-4d80-94ae-c3dde445255c")
                                        ,new Guid("5abdcdd0-67bd-41d6-a2bb-bcb6656895ec") };

        private Guid[] s62 = new Guid[] { new Guid("6ae9bc29-06e4-4614-afa9-c635691236df")
                                        ,new Guid("dc525e80-c1e0-423e-ace5-ebb1f8e55d8f")
                                        ,new Guid("4c914bde-d03b-41fe-8580-a5bde6e6ca4e")
                                        ,new Guid("9de2e6cd-c4c9-4106-b432-13d9995dbeed")
                                        ,new Guid("1a4c44f2-e013-4da5-8a7c-e4a230400f19")
                                        ,new Guid("670df617-2ca1-4e8e-8e64-31806d7bc0dd")};
        #endregion

        public static WorksheetTypeSettings Settings = new WorksheetTypeSettings
                                                      {
                                                        Clients = true
                                                        ,
                                                        Suppliers = true
                                                        ,
                                                        AllAccounts = true
                                                        , 
                                                        PerformsBookings=true
                                                      };
                                                      
        public static Guid TypeId = new Guid("2ca965ed-eacb-43ff-9a91-cfe7ef09eef9");

        public List<string> ImportablePreviousTypes  = new List<string>() {  "EY.com.eBook.RuleEngine.AY2014.FacturatieApp"  };

        public Guid GetMyType() { return TypeId; }

        public void SetMinimal()
        {
            if (Data == null)
            {
                Data = new EY.com.eBook.RuleEngine.AY2015.BTWAppTypes.WPBTW();
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.OmzetAangiftes == null)
            {
                Data.OmzetAangiftes = new List<EY.com.eBook.RuleEngine.AY2015.BTWAppTypes.OmzetAangifte>(); ;
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.OmzettenGeboekt == null)
            {
                Data.OmzettenGeboekt = new List<EY.com.eBook.RuleEngine.AY2015.BTWAppTypes.OmzetGeboekt>(); ;
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.OmzetVerklaringen == null)
            {
                Data.OmzetVerklaringen = new List<EY.com.eBook.RuleEngine.AY2015.BTWAppTypes.OmzetVerklaring>(); ;
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.RCAangiftes == null)
            {
                Data.RCAangiftes = new List<EY.com.eBook.RuleEngine.AY2015.BTWAppTypes.RCAangifte>(); ;
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.RCenGeboekt == null)
            {
                Data.RCenGeboekt = new List<EY.com.eBook.RuleEngine.AY2015.BTWAppTypes.RCGeboekt>(); ;
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.RCVerklaringen == null)
            {
                Data.RCVerklaringen = new List<EY.com.eBook.RuleEngine.AY2015.BTWAppTypes.RCVerklaring>(); ;
                Data.LastUpdated = DateTime.Now;
            }

        }
        
        public string ToXml() 
        {
            return CoreHelper.SerializeToString(Data);
        }

        public void LoadXml(string xml)
        {
            if (!string.IsNullOrEmpty(xml)) {
                Data = CoreHelper.DeserializeFromString<EY.com.eBook.RuleEngine.AY2015.BTWAppTypes.WPBTW>(xml);
            }
            SetMinimal();
            
        }
        
        public EY.com.eBook.RuleEngine.AY2015.BTWAppTypes.WPBTW Data { get; set; }



        #region VALIDATION

        public EY.com.eBook.RuleEngine.AY2015.BTWAppTypes.OmzetAangifte Validate(EY.com.eBook.RuleEngine.AY2015.BTWAppTypes.OmzetAangifte entity, string location, ref WorksheetSetData worksheetset)
        {
            entity.IsValid = true;
            worksheetset.ClearMessages(TypeId, entity.Id);
            /*
            if (!entity.Bedrag.HasValue || entity.Bedrag.Value==0)
            {
            worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Grootboek");
            entity.IsValid = false;
            }
            */
            entity.Tarief_0 = Math.Abs(entity.Tarief_0.GetValueOrDefault());
            entity.Tarief_6 = Math.Abs(entity.Tarief_6.GetValueOrDefault());
            entity.Tarief_12 = Math.Abs(entity.Tarief_12.GetValueOrDefault());
            entity.Tarief_21 = Math.Abs(entity.Tarief_21.GetValueOrDefault());
            entity.Tarief_Medec = Math.Abs(entity.Tarief_Medec.GetValueOrDefault());
            entity.Tarief_EU = Math.Abs(entity.Tarief_EU.GetValueOrDefault());
            entity.Tarief_Uitvoer = Math.Abs(entity.Tarief_Uitvoer.GetValueOrDefault());
            entity.Tarief_48 = Math.Abs(entity.Tarief_48.GetValueOrDefault());
            entity.Tarief_49 = Math.Abs(entity.Tarief_49.GetValueOrDefault());
            entity.Tarief_44 = Math.Abs(entity.Tarief_44.GetValueOrDefault());
            entity.Totaal = entity.Tarief_0.Value + entity.Tarief_6.Value + entity.Tarief_12.Value + entity.Tarief_21.Value + entity.Tarief_Medec.Value + entity.Tarief_44.Value + entity.Tarief_EU.Value + entity.Tarief_Uitvoer.Value - entity.Tarief_48.Value - entity.Tarief_49.Value;
            entity.LastUpdated = DateTime.Now;

            return entity;
        }

        public EY.com.eBook.RuleEngine.AY2015.BTWAppTypes.OmzetGeboekt Validate(EY.com.eBook.RuleEngine.AY2015.BTWAppTypes.OmzetGeboekt entity, string location, ref WorksheetSetData worksheetset)
        {
            entity.IsValid = true;
            worksheetset.ClearMessages(TypeId, entity.Id);
            /*
            if (!entity.Bedrag.HasValue || entity.Bedrag.Value==0)
            {
            worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Grootboek");
            entity.IsValid = false;
            }
            */

            return entity;
        }

        public EY.com.eBook.RuleEngine.AY2015.BTWAppTypes.OmzetVerklaring Validate(EY.com.eBook.RuleEngine.AY2015.BTWAppTypes.OmzetVerklaring entity, string location, ref WorksheetSetData worksheetset)
        {
            entity.IsValid = true;
            worksheetset.ClearMessages(TypeId, entity.Id);
            if (!entity.Bedrag.HasValue)
            {
                //add message collection, field:"Grootboek", required
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Bedrag");
                entity.IsValid = false;
            }

            return entity;
        }

        public EY.com.eBook.RuleEngine.AY2015.BTWAppTypes.RCAangifte Validate(EY.com.eBook.RuleEngine.AY2015.BTWAppTypes.RCAangifte entity, string location, ref WorksheetSetData worksheetset)
        {
            entity.IsValid = true;
            worksheetset.ClearMessages(TypeId, entity.Id);
            /*
            if (!entity.Bedrag.HasValue || entity.Bedrag.Value==0)
            {
            worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Grootboek");
            entity.IsValid = false;
            }
            */

            entity.Post54 = Math.Abs(entity.Post54.GetValueOrDefault());
            entity.Post55 = Math.Abs(entity.Post55.GetValueOrDefault());
            entity.Post56 = Math.Abs(entity.Post56.GetValueOrDefault());
            entity.Post57 = Math.Abs(entity.Post57.GetValueOrDefault());
            entity.Post59 = Math.Abs(entity.Post59.GetValueOrDefault());
            entity.Post61 = Math.Abs(entity.Post61.GetValueOrDefault());
            entity.Post62 = Math.Abs(entity.Post62.GetValueOrDefault());
            entity.Post63 = Math.Abs(entity.Post63.GetValueOrDefault());
            entity.Post64 = Math.Abs(entity.Post64.GetValueOrDefault());

            entity.Totaal = -entity.Post54.Value - entity.Post55.Value - entity.Post56.Value - entity.Post57.Value + entity.Post59.Value - entity.Post61.Value + entity.Post62.Value - entity.Post63.Value + entity.Post64.Value;

            return entity;
        }

        public EY.com.eBook.RuleEngine.AY2015.BTWAppTypes.RCGeboekt Validate(EY.com.eBook.RuleEngine.AY2015.BTWAppTypes.RCGeboekt entity, string location, ref WorksheetSetData worksheetset)
        {
            entity.IsValid = true;
            worksheetset.ClearMessages(TypeId, entity.Id);
            /*
            if (!entity.Bedrag.HasValue || entity.Bedrag.Value==0)
            {
            worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Grootboek");
            entity.IsValid = false;
            }
            */

            return entity;
        }

        public EY.com.eBook.RuleEngine.AY2015.BTWAppTypes.RCVerklaring Validate(EY.com.eBook.RuleEngine.AY2015.BTWAppTypes.RCVerklaring entity, string location, ref WorksheetSetData worksheetset)
        {
            entity.IsValid = true;
            worksheetset.ClearMessages(TypeId, entity.Id);
            if (!entity.AutoLine.HasValue || entity.AutoLine == false)
            {


                if (entity.Soort == null)
                {
                    //add message collection, field:"Grootboek", required
                    worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Soort");
                    entity.IsValid = false;
                }

                if (entity.Grootboek == null)
                {
                    //add message collection, field:"Grootboek", required
                    worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Grootboek");
                    entity.IsValid = false;
                }

                if (!entity.Bedrag.HasValue)
                {
                    //add message collection, field:"Grootboek", required

                    worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Bedrag");
                    entity.IsValid = false;
                }

                if (entity.IsValid)
                {
                    /*
                    if (!S_TekenSoorten.Contains(entity.Soort.Id))
                    {
                        if (plusTekenSoorten.Contains(entity.Soort.Id))
                        {
                            if (entity.Bedrag.Value < 0)
                            {

                                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "POSITIF", location, entity.Id, "Bedrag");
                                entity.IsValid = false;
                            }
                        }
                        if (minTekenSoorten.Contains(entity.Soort.Id))
                        {
                            if (entity.Bedrag.Value > 0)
                            {

                                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "NEGATIF", location, entity.Id, "Bedrag");
                                entity.IsValid = false;
                            }
                        }
                    }*/

                    if (!boeken.Contains(entity.Soort.Id) && entity.Boek.HasValue && entity.Boek == true)
                    {
                        if (entity.Bedrag.Value > 0)
                        {

                            worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "INVALID", location, entity.Id, "Boek");
                            entity.IsValid = false;
                        }
                    }

                }

                if (entity.IsValid)
                {
                    if (entity.Soort != null)
                    {
                        if (s61.Contains(entity.Soort.Id))
                        {
                            entity.Vak61 = Math.Abs(entity.Bedrag.GetValueOrDefault());
                            entity.Vak62 = 0;
                        }
                        else if (s62.Contains(entity.Soort.Id))
                        {
                            entity.Vak61 = 0;
                            entity.Vak62 = Math.Abs(entity.Bedrag.GetValueOrDefault());
                        }
                        else
                        {
                            entity.Vak61 = 0;
                            entity.Vak62 = 0;
                        }
                        entity.Vak61_62 = entity.Vak62.GetValueOrDefault() - entity.Vak61.GetValueOrDefault();
                    }
                }

            }

            return entity;
        }

        #endregion

        public void ForceRecalculation(ref WorksheetSetData worksheetset)
        {
            Data.LastUpdated = null;
            // clear all automatic lines.
            Data.OmzettenGeboekt.RemoveAll(r => r.AutoLine.HasValue && r.AutoLine.Value);
            Data.OmzetVerklaringen.RemoveAll(r => r.AutoLine.HasValue && r.AutoLine.Value);
            Data.RCenGeboekt.RemoveAll(r => r.AutoLine.HasValue && r.AutoLine.Value);
            Data.RCVerklaringen.RemoveAll(r => r.AutoLine.HasValue && r.AutoLine.Value);

            foreach (BTWAppTypes.OmzetAangifte item in Data.OmzetAangiftes.ToList())
            {
                UpdateOmzetAangiftes(item, ref worksheetset);
            }
            foreach (BTWAppTypes.OmzetVerklaring item in Data.OmzetVerklaringen.ToList())
            {
                UpdateOmzetVerklaringen(item, ref worksheetset);
            }
            foreach (BTWAppTypes.RCAangifte item in Data.RCAangiftes.ToList())
            {
                UpdateRCAangiftes(item, ref worksheetset);
            }
            foreach (BTWAppTypes.RCVerklaring item in Data.RCVerklaringen.ToList())
            {
                UpdateRCVerklaringen(item, ref worksheetset);
            }
            

            Calculate(ref worksheetset);
        }


        #region worksheet type rules
        // Only if worksheet does any calculation.
        public void Calculate(ref WorksheetSetData wsd)
        {
            if (!Data.LastUpdated.HasValue) Data.LastUpdated = new DateTime(1980, 1, 1);
            wsd.ClearMessages(TypeId);

            //foreach (EY.com.eBook.RuleEngine.AY2015.BTWAppTypes.OmzetAangifte item in Data.OmzetAangiftes.Where(b => !b.LastUpdated.HasValue || b.LastUpdated > Data.LastUpdated).ToList())
            //{
            //    int idx = Data.OmzetAangiftes.IndexOf(item);
            //    Data.OmzetAangiftes[idx] = Validate(item, "OmzetAangiftes", ref wsd);
                
            //}

            //foreach (EY.com.eBook.RuleEngine.AY2015.BTWAppTypes.RCAangifte item in Data.RCAangiftes.Where(b => !b.LastUpdated.HasValue || b.LastUpdated > Data.LastUpdated).ToList())
            //{
            //    int idx = Data.RCAangiftes.IndexOf(item);
            //    Data.RCAangiftes[idx] = Validate(item, "RCAangiftes", ref wsd);
                
            //}

            //foreach (EY.com.eBook.RuleEngine.AY2015.BTWAppTypes.RCVerklaring item in Data.RCVerklaringen.Where(b => (!b.AutoLine.HasValue || !b.AutoLine.Value) && !b.LastUpdated.HasValue || b.LastUpdated > Data.LastUpdated).ToList())
            //{
            //    int idx = Data.RCVerklaringen.IndexOf(item);
            //    Data.RCVerklaringen[idx] = Validate(item, "RCVerklaringen", ref wsd);
                
            //}

            ImportOmzettenGeboekt(ref wsd);
            ImportRCGeboekt(ref wsd);
            RenderOmzetVerklaring(ref wsd);
            RenderRCVerklaring(ref wsd);

            Data.LastUpdated = DateTime.Now;
        }

        public void ImportOmzettenGeboekt(ref WorksheetSetData wsd)
        {

            FileDataContract fdc = wsd.DataContract;
            List<AccountDataContract> accounts = wsd.GetAccountsOfMappingMeta("Omzet", "BTW", false, false, true, false, false, fdc.Culture);
            accounts.RemoveAll(a => a.Saldo == 0);
            List<string> nrs = accounts.Select(a => a.InternalNr).ToList();
            List<string> existing = Data.OmzettenGeboekt.Select(a => a.GB_NR).ToList();

            if (Data.OmzettenGeboekt.Count(a => !nrs.Contains(a.GB_NR)) > 0)
            {
                // delete removed mappings
                Data.OmzettenGeboekt.RemoveAll(a => !nrs.Contains(a.GB_NR));
            }

            // Updates
            foreach (EY.com.eBook.RuleEngine.AY2015.BTWAppTypes.OmzetGeboekt item in Data.OmzettenGeboekt.Where(a => nrs.Contains(a.GB_NR)))
            {

                AccountDataContract adc = accounts.First(a => a.InternalNr == item.GB_NR);
                decimal diff = (adc.Saldo - adc.PreviousSaldo);
                item.Saldo = 0 - adc.Saldo;
                item.BeginSaldo = 0 - adc.PreviousSaldo;
                item.Credit = diff < 0 ? Math.Abs(diff) : 0;
                item.Debet = diff > 0 ? Math.Abs(diff) : 0;
                item.NaamRekening = string.Format("{0} {1}", adc.VisualNr, adc.DefaultDescription);
                item.LastUpdated = DateTime.Now;
                Data.LastUpdated = DateTime.Now;
            }

            // inserts
            foreach (AccountDataContract adc in accounts.Where(a => !existing.Contains(a.InternalNr)))
            {
                decimal diff = (adc.Saldo - adc.PreviousSaldo);
                EY.com.eBook.RuleEngine.AY2015.BTWAppTypes.OmzetGeboekt item = new EY.com.eBook.RuleEngine.AY2015.BTWAppTypes.OmzetGeboekt
                {
                    Id = Guid.NewGuid()
                    ,
                    BeginSaldo = 0 - adc.PreviousSaldo
                    ,
                    AutoKey = "OG"
                    ,
                    AutoLine = true
                    ,
                    GB_NR = adc.InternalNr
                    ,
                    NaamRekening = string.Format("{0} {1}", adc.VisualNr, adc.DefaultDescription)
                    ,
                    Saldo = 0 - adc.Saldo
                    ,
                    Credit = diff < 0 ? Math.Abs(diff) : 0
                    ,
                    Debet = diff > 0 ? Math.Abs(diff) : 0
                    ,
                    LastUpdated = DateTime.Now
                };
                Data.OmzettenGeboekt.Add(item);
            }
            Data.TotaalSaldoOmzettenGeboekt = Data.OmzettenGeboekt.Sum(a => a.Saldo.GetValueOrDefault());
            Data.TotaalSaldoOmzettenGeboekt = Data.TotaalSaldoOmzettenGeboekt.GetValueOrDefault();
        }

        public void ImportRCGeboekt(ref WorksheetSetData wsd)
        {
            FileDataContract fdc = wsd.DataContract;
            List<AccountDataContract> accounts = wsd.GetAccountsOfMappingMeta("RC", "BTW", false, false, true, false, false, fdc.Culture);
            accounts.RemoveAll(a => a.Saldo == 0);
            List<string> nrs = accounts.Select(a => a.InternalNr).ToList();
            List<string> existing = Data.RCenGeboekt.Select(a => a.GB_NR).ToList();

            // delete removed mappings
            Data.RCenGeboekt.RemoveAll(a => !nrs.Contains(a.GB_NR));

            // Updates
            foreach (EY.com.eBook.RuleEngine.AY2015.BTWAppTypes.RCGeboekt item in Data.RCenGeboekt.Where(a => nrs.Contains(a.GB_NR)))
            {

                AccountDataContract adc = accounts.First(a => a.InternalNr == item.GB_NR);
                decimal diff = (adc.Saldo - adc.PreviousSaldo);
                item.Saldo = adc.Saldo;
                item.BeginSaldo = adc.PreviousSaldo;
                item.Credit = diff < 0 ? Math.Abs(diff) : 0;
                item.Debet = diff > 0 ? Math.Abs(diff) : 0;
                item.NaamRekening = string.Format("{0} {1}", adc.VisualNr, adc.DefaultDescription);
                item.LastUpdated = DateTime.Now;

            }

            // inserts
            foreach (AccountDataContract adc in accounts.Where(a => !existing.Contains(a.InternalNr)))
            {
                decimal diff = (adc.Saldo - adc.PreviousSaldo);
                EY.com.eBook.RuleEngine.AY2015.BTWAppTypes.RCGeboekt item = new EY.com.eBook.RuleEngine.AY2015.BTWAppTypes.RCGeboekt
                {
                    Id = Guid.NewGuid()
                    ,
                    BeginSaldo = adc.PreviousSaldo
                    ,
                    AutoKey = "OG"
                    ,
                    AutoLine = true
                    ,
                    GB_NR = adc.InternalNr
                    ,
                    NaamRekening = string.Format("{0} {1}", adc.VisualNr, adc.DefaultDescription)
                    ,
                    Saldo = adc.Saldo
                    ,
                    Credit = diff < 0 ? Math.Abs(diff) : 0
                    ,
                    Debet = diff > 0 ? Math.Abs(diff) : 0
                    ,
                    LastUpdated = DateTime.Now
                };
                Data.RCenGeboekt.Add(item);
            }
            Data.TotaalSaldoRCGeboekt = Data.RCenGeboekt.Sum(a => a.Saldo.GetValueOrDefault());
            Data.TotaalSaldoRCGeboekt = Data.TotaalSaldoRCGeboekt.GetValueOrDefault();
        }

        public void RenderOmzetVerklaring(ref WorksheetSetData wsd)
        {
            // cleanup olds
            Data.OmzetVerklaringen.RemoveAll(a => a.AutoKey == "TVV");
            
            // Translations
            string transVerklaring = wsd.GetTranslation(TypeId, "", "", "VerklaringVerschil", "label");
            string transResterend = wsd.GetTranslation(TypeId, "", "", "ResterendVerschil", "label");
            string transGeboekt = wsd.GetTranslation(TypeId, "WPBTW", "OmzettenGeboekt", "label");
            string transAangiftes = wsd.GetTranslation(TypeId, "WPBTW", "OmzetAangiftes", "label");

            decimal omzetten = Data.OmzettenGeboekt.Count > 0 ? Data.OmzettenGeboekt.Sum(a => a.Saldo.GetValueOrDefault()) : 0;
            decimal aangiftes = Data.OmzetAangiftes.Count > 0 ? Data.OmzetAangiftes.Sum(a => a.Totaal.GetValueOrDefault()) : 0;
            decimal verklaringen = Data.OmzetVerklaringen.Where(a => !a.AutoLine.HasValue || a.AutoLine==false).Count() > 0 ?
                                        Data.OmzetVerklaringen.Where(a => !a.AutoLine.HasValue || a.AutoLine == false).Sum(a => a.Bedrag.GetValueOrDefault())
                                        : 0;

            decimal totVerklaring = omzetten - aangiftes;

            decimal totResterend = totVerklaring - verklaringen;


            EY.com.eBook.RuleEngine.AY2015.BTWAppTypes.OmzetVerklaring gb = Data.OmzetVerklaringen.FirstOrDefault(a => a.AutoKey == "GB");
            EY.com.eBook.RuleEngine.AY2015.BTWAppTypes.OmzetVerklaring an = Data.OmzetVerklaringen.FirstOrDefault(a => a.AutoKey == "AN");

            Data.OmzetVerklaringen.RemoveAll(v => v.AutoLine == true && v.AutoKey == "GBREK");

            foreach (BTWAppTypes.OmzetGeboekt geb in Data.OmzettenGeboekt)
            {
                Data.OmzetVerklaringen.Add(new EY.com.eBook.RuleEngine.AY2015.BTWAppTypes.OmzetVerklaring
                {
                    Id = geb.Id
                    ,
                    AutoKey = "GBREK"
                    ,
                    AutoLine = true
                    ,
                    Bedrag = geb.Saldo.GetValueOrDefault()
                    ,
                    Grootboek = wsd.GetListAccount(geb.GB_NR)
                    ,
                    IsValid = true
                    ,
                    Jaar = null
                    ,
                    LastUpdated = geb.LastUpdated
                    ,
                    Omschrijving = geb.NaamRekening
                    ,
                    order = -3
                    ,
                    PreviousImported = false
                    ,
                    BookingReference = null
                });
            }

            if (gb == null)
            {
                gb = new EY.com.eBook.RuleEngine.AY2015.BTWAppTypes.OmzetVerklaring
                {
                    Id = Guid.NewGuid()
                    ,
                    AutoKey = "GB"
                    ,
                    AutoLine = true
                    ,
                    Bedrag = 0
                    ,
                    Grootboek = null
                    ,
                    IsValid = true
                    ,
                    Jaar = null
                    ,
                    LastUpdated = DateTime.Now
                    ,
                    Omschrijving = transGeboekt
                    ,
                    order = -2
                };
                Data.OmzetVerklaringen.Add(gb);

            }
            gb.Omschrijving = transGeboekt;
            gb.Bedrag = omzetten;
            gb.LastUpdated = DateTime.Now;


            if (an == null)
            {
                an = new EY.com.eBook.RuleEngine.AY2015.BTWAppTypes.OmzetVerklaring
                {
                    Id = Guid.NewGuid()
                    ,
                    AutoKey = "AN"
                    ,
                    AutoLine = true
                    ,
                    Bedrag = 0
                    ,
                    Grootboek = null
                    ,
                    IsValid = true
                    ,
                    Jaar = null
                    ,
                    LastUpdated = DateTime.Now
                    ,
                    Omschrijving = transAangiftes
                    ,
                    order = -1
                };
                Data.OmzetVerklaringen.Add(an);

            }
            an.Omschrijving = transAangiftes;
            an.Bedrag = (0-aangiftes);
            an.LastUpdated = DateTime.Now;

            
            EY.com.eBook.RuleEngine.AY2015.BTWAppTypes.OmzetVerklaring tv = Data.OmzetVerklaringen.FirstOrDefault(a => a.AutoKey == "TV");
            EY.com.eBook.RuleEngine.AY2015.BTWAppTypes.OmzetVerklaring rv = Data.OmzetVerklaringen.FirstOrDefault(a => a.AutoKey == "RV");

            if (tv == null)
            {
                tv = new EY.com.eBook.RuleEngine.AY2015.BTWAppTypes.OmzetVerklaring
                {
                    Id = Guid.NewGuid()
                    ,
                    AutoKey = "TV"
                    ,
                    AutoLine = true
                    ,
                    Bedrag = 0
                    ,
                    Grootboek = null
                    ,
                    IsValid = true
                    ,
                    Jaar = null
                    ,
                    LastUpdated = DateTime.Now
                    ,
                    Omschrijving = transVerklaring
                    ,
                    order = 1
                };
                Data.OmzetVerklaringen.Add(tv);

            }
            tv.order = 1;
            tv.Omschrijving = transVerklaring;
            tv.Bedrag = totVerklaring;
            tv.LastUpdated = DateTime.Now;

            if (rv == null)
            {
                rv = new EY.com.eBook.RuleEngine.AY2015.BTWAppTypes.OmzetVerklaring
                {
                    Id = Guid.NewGuid()
                    ,
                    AutoKey = "RV"
                    ,
                    AutoLine = true
                    ,
                    Bedrag = 0
                    ,
                    Grootboek = null
                    ,
                    IsValid = true
                    ,
                    Jaar = null
                    ,
                    LastUpdated = DateTime.Now
                    ,
                    Omschrijving = transResterend
                    ,
                    order = 999999
                };
                Data.OmzetVerklaringen.Add(rv);

            }
            rv.order= 999999;
            rv.Omschrijving = transResterend;
            rv.Bedrag = totResterend;
            rv.LastUpdated = DateTime.Now;

            Data.OmzetVerklaringen = Data.OmzetVerklaringen.OrderBy(i => i.order.GetValueOrDefault()).ToList();


        }

        public void RenderRCVerklaring(ref WorksheetSetData wsd)
        {
            FileDataContract fdc = wsd.DataContract;
            // cleanup olds
            Data.RCVerklaringen.RemoveAll(a => a.AutoKey == "TVV");

            // Translations
            string transVerklaring = wsd.GetTranslation(TypeId, "", "", "VerklaringVerschil", "label");
            string transResterend = wsd.GetTranslation(TypeId, "", "", "ResterendVerschil", "label");
            string transRcGeboekt = wsd.GetTranslation(TypeId, "", "", "RCGeboekt", "label");

            string ym;
            if (fdc.EndDate.Month < 10)
            {
                ym = String.Format("{0}/0{1}", fdc.EndDate.Year, fdc.EndDate.Month);
            }
            else
            {
                ym = String.Format("{0}/{1}", fdc.EndDate.Year, fdc.EndDate.Month);
            }

            decimal totrcAangiftes = Data.RCAangiftes.Where(r => r.Y_M != null && r.Y_M.Id == ym).Count() > 0 ? Data.RCAangiftes.Where(r => r.Y_M != null && r.Y_M.Id == ym).Sum(a => a.Totaal.GetValueOrDefault()) : 0;


            decimal geboekts = Data.RCenGeboekt.Count > 0 ? Data.RCenGeboekt.Sum(a => a.Saldo.GetValueOrDefault()) : 0;
            decimal verklaringen = Data.RCVerklaringen.Where(a => a.AutoKey != "TV" && a.AutoKey != "RV" && a.AutoKey != "YM" && a.AutoKey != "RG").Count() > 0 ?
                                        Data.RCVerklaringen.Where(a => a.AutoKey != "TV" && a.AutoKey != "RV" && a.AutoKey != "YM" && a.AutoKey != "RG").Sum(a => a.Bedrag.GetValueOrDefault())
                                        : 0;



            EY.com.eBook.RuleEngine.AY2015.BTWAppTypes.RCVerklaring tv = Data.RCVerklaringen.FirstOrDefault(a => a.AutoKey == "TV");
            EY.com.eBook.RuleEngine.AY2015.BTWAppTypes.RCVerklaring rv = Data.RCVerklaringen.FirstOrDefault(a => a.AutoKey == "RV");
            EY.com.eBook.RuleEngine.AY2015.BTWAppTypes.RCVerklaring rg = Data.RCVerklaringen.FirstOrDefault(a => a.AutoKey == "RG");
            EY.com.eBook.RuleEngine.AY2015.BTWAppTypes.RCVerklaring ymm = Data.RCVerklaringen.FirstOrDefault(a => a.AutoKey == "YM");

            if (ymm == null)
            {
                ymm = new EY.com.eBook.RuleEngine.AY2015.BTWAppTypes.RCVerklaring
                {
                    Id = Guid.NewGuid()
                    ,
                    AutoKey = "YM"
                    ,
                    AutoLine = true
                    ,
                    Bedrag = 0
                    ,
                    Grootboek = null
                    ,
                    IsValid = true
                    ,
                    Jaar = null
                    ,
                    LastUpdated = DateTime.Now
                    ,
                    Omschrijving = ym
                    ,
                    order = -2
                };
                Data.RCVerklaringen.Add(ymm);
            }
            ymm.Bedrag = totrcAangiftes;

            if (rg == null)
            {
                rg = new EY.com.eBook.RuleEngine.AY2015.BTWAppTypes.RCVerklaring
                {
                    Id = Guid.NewGuid()
                    ,
                    AutoKey = "RG"
                    ,
                    AutoLine = true
                    ,
                    Bedrag = 0
                    ,
                    Grootboek = null
                    ,
                    IsValid = true
                    ,
                    Jaar = null
                    ,
                    LastUpdated = DateTime.Now
                    ,
                    Omschrijving = transRcGeboekt
                    ,
                    order = -2
                };
                Data.RCVerklaringen.Add(rg);
            }
            rg.Bedrag = geboekts;

            if (tv == null)
            {
                tv = new EY.com.eBook.RuleEngine.AY2015.BTWAppTypes.RCVerklaring
                {
                    Id = Guid.NewGuid()
                    ,
                    AutoKey = "TV"
                    ,
                    AutoLine = true
                    ,
                    Bedrag = 0
                    ,
                    Grootboek = null
                    ,
                    IsValid = true
                    ,
                    Jaar = null
                    ,
                    LastUpdated = DateTime.Now
                    ,
                    Omschrijving = transVerklaring
                    ,
                    order = 0
                };
                Data.RCVerklaringen.Add(tv);

            }
            tv.Bedrag = Math.Abs(geboekts - totrcAangiftes);
            tv.LastUpdated = DateTime.Now;

            if (rv == null)
            {
                rv = new EY.com.eBook.RuleEngine.AY2015.BTWAppTypes.RCVerklaring
                {
                    Id = Guid.NewGuid()
                    ,
                    AutoKey = "RV"
                    ,
                    AutoLine = true
                    ,
                    Bedrag = 0
                    ,
                    Grootboek = null
                    ,
                    IsValid = true
                    ,
                    Jaar = null
                    ,
                    LastUpdated = DateTime.Now
                    ,
                    Omschrijving = transResterend
                    ,
                    order = 9999
                };
                Data.RCVerklaringen.Add(rv);

            }
            rv.Bedrag = Math.Abs(geboekts - totrcAangiftes) + verklaringen;
            rv.LastUpdated = DateTime.Now;
            Data.RCVerklaringen = Data.RCVerklaringen.OrderBy(r => r.order.GetValueOrDefault()).ToList();
        }

        #endregion


        #region AddUpdateDelete

        public void AddToOmzetAangiftes(EY.com.eBook.RuleEngine.AY2015.BTWAppTypes.OmzetAangifte entity, ref WorksheetSetData worksheetset)
        {
            entity = Validate(entity, "OmzetAangiftes", ref worksheetset);
            if (entity.IsValid)
            {
                
                entity.LastUpdated = DateTime.Now;
                Data.OmzetAangiftes.Add(entity);
            }
        }

        public void DeleteOmzetAangiftes(Guid rowId, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2015.BTWAppTypes.OmzetAangifte original = Data.OmzetAangiftes.FirstOrDefault(e => e.Id == rowId);
            if (original != null)
            {
                /*
                if (original.Boeken.HasValue && original.Boeken.Value)
                {
                    worksheetset.DeleteBooking(FacturatieApp.TypeId, "OpTeMakenCN", original.Id, null);
                    // DELETE BOOKING
                }*/
                Data.OmzetAangiftes.Remove(original);
                Data.LastUpdated = DateTime.Now;
            }
        }

        public void UpdateOmzetAangiftes(EY.com.eBook.RuleEngine.AY2015.BTWAppTypes.OmzetAangifte entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2015.BTWAppTypes.OmzetAangifte original = Data.OmzetAangiftes.FirstOrDefault(e => e.Id == entity.Id);
            if (original == null)
            {
                AddToOmzetAangiftes(entity, ref worksheetset);
            }
            else
            {
                entity = Validate(entity, "OmzetAangiftes", ref worksheetset);
                if (entity.IsValid)
                {
                    int idx = Data.OmzetAangiftes.IndexOf(original);
                    Data.OmzetAangiftes.RemoveAt(idx);
                    Data.OmzetAangiftes.Insert(idx, entity);
                   // Data.LastUpdated = DateTime.Now;
                    entity.LastUpdated = DateTime.Now;
                }
            }
        }

        public void AddToOmzettenGeboekt(EY.com.eBook.RuleEngine.AY2015.BTWAppTypes.OmzetGeboekt entity, ref WorksheetSetData worksheetset)
        {
            entity = Validate(entity, "OmzettenGeboekt", ref worksheetset);
            if (entity.IsValid)
            {
                //Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
                Data.OmzettenGeboekt.Add(entity);
            }
        }

        public void DeleteOmzettenGeboekt(Guid rowId, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2015.BTWAppTypes.OmzetGeboekt original = Data.OmzettenGeboekt.FirstOrDefault(e => e.Id == rowId);
            if (original != null)
            {
                /*
                if (original.Boeken.HasValue && original.Boeken.Value)
                {
                    worksheetset.DeleteBooking(FacturatieApp.TypeId, "OpTeMakenCN", original.Id, null);
                    // DELETE BOOKING
                }*/
                Data.OmzettenGeboekt.Remove(original);
               // Data.LastUpdated = DateTime.Now;
            }
        }

        public void UpdateOmzettenGeboekt(EY.com.eBook.RuleEngine.AY2015.BTWAppTypes.OmzetGeboekt entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2015.BTWAppTypes.OmzetGeboekt original = Data.OmzettenGeboekt.FirstOrDefault(e => e.Id == entity.Id);
            if (original == null)
            {
                AddToOmzettenGeboekt(entity, ref worksheetset);
            }
            else
            {
                entity = Validate(entity, "OmzettenGeboekt", ref worksheetset);
                if (entity.IsValid)
                {
                    int idx = Data.OmzettenGeboekt.IndexOf(original);
                    Data.OmzettenGeboekt.RemoveAt(idx);
                    Data.OmzettenGeboekt.Insert(idx, entity);
                   // Data.LastUpdated = DateTime.Now;
                    entity.LastUpdated = DateTime.Now;
                }
            }
        }

        public void AddToOmzetVerklaringen(EY.com.eBook.RuleEngine.AY2015.BTWAppTypes.OmzetVerklaring entity, ref WorksheetSetData worksheetset)
        {
            entity = Validate(entity, "OmzetVerklaringen", ref worksheetset);
            if (entity.IsValid)
            {
                entity.order = 3;
                //Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
                Data.OmzetVerklaringen.Add(entity);
            }
        }

        public void DeleteOmzetVerklaringen(Guid rowId, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2015.BTWAppTypes.OmzetVerklaring original = Data.OmzetVerklaringen.FirstOrDefault(e => e.Id == rowId);
            if (original != null)
            {
                /*
                if (original.Boeken.HasValue && original.Boeken.Value)
                {
                    worksheetset.DeleteBooking(FacturatieApp.TypeId, "OpTeMakenCN", original.Id, null);
                    // DELETE BOOKING
                }*/
                Data.OmzetVerklaringen.Remove(original);
               // Data.LastUpdated = DateTime.Now;
            }
        }

        public void UpdateOmzetVerklaringen(EY.com.eBook.RuleEngine.AY2015.BTWAppTypes.OmzetVerklaring entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2015.BTWAppTypes.OmzetVerklaring original = Data.OmzetVerklaringen.FirstOrDefault(e => e.Id == entity.Id);
            if (original == null)
            {
                AddToOmzetVerklaringen(entity, ref worksheetset);
            }
            else
            {
                entity = Validate(entity, "OmzetVerklaringen", ref worksheetset);
                if (entity.IsValid)
                {
                    int idx = Data.OmzetVerklaringen.IndexOf(original);
                    Data.OmzetVerklaringen.RemoveAt(idx);
                    Data.OmzetVerklaringen.Insert(idx, entity);
                  //  Data.LastUpdated = DateTime.Now;
                    entity.LastUpdated = DateTime.Now;
                }
            }
        }

        public void AddToRCAangiftes(EY.com.eBook.RuleEngine.AY2015.BTWAppTypes.RCAangifte entity, ref WorksheetSetData worksheetset)
        {
            entity = Validate(entity, "RCAangiftes", ref worksheetset);
            if (entity.IsValid)
            {
               //Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
                Data.RCAangiftes.Add(entity);
            }
        }

        public void DeleteRCAangiftes(Guid rowId, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2015.BTWAppTypes.RCAangifte original = Data.RCAangiftes.FirstOrDefault(e => e.Id == rowId);
            if (original != null)
            {
                /*
                if (original.Boeken.HasValue && original.Boeken.Value)
                {
                    worksheetset.DeleteBooking(FacturatieApp.TypeId, "OpTeMakenCN", original.Id, null);
                    // DELETE BOOKING
                }*/
                Data.RCAangiftes.Remove(original);
                //Data.LastUpdated = DateTime.Now;
            }
        }

        public void UpdateRCAangiftes(EY.com.eBook.RuleEngine.AY2015.BTWAppTypes.RCAangifte entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2015.BTWAppTypes.RCAangifte original = Data.RCAangiftes.FirstOrDefault(e => e.Id == entity.Id);
            if (original == null)
            {
                AddToRCAangiftes(entity, ref worksheetset);
            }
            else
            {
                entity = Validate(entity, "RCAangiftes", ref worksheetset);
                if (entity.IsValid)
                {
                    int idx = Data.RCAangiftes.IndexOf(original);
                    Data.RCAangiftes.RemoveAt(idx);
                    Data.RCAangiftes.Insert(idx, entity);
                   // Data.LastUpdated = DateTime.Now;
                    entity.LastUpdated = DateTime.Now;
                }
            }
        }

        public void AddToRCenGeboekt(EY.com.eBook.RuleEngine.AY2015.BTWAppTypes.RCGeboekt entity, ref WorksheetSetData worksheetset)
        {
            entity = Validate(entity, "RCenGeboekt", ref worksheetset);
            if (entity.IsValid)
            {
                //Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
                Data.RCenGeboekt.Add(entity);
            }
        }

        public void DeleteRCenGeboekt(Guid rowId, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2015.BTWAppTypes.RCGeboekt original = Data.RCenGeboekt.FirstOrDefault(e => e.Id == rowId);
            if (original != null)
            {
                /*
                if (original.Boeken.HasValue && original.Boeken.Value)
                {
                    worksheetset.DeleteBooking(FacturatieApp.TypeId, "OpTeMakenCN", original.Id, null);
                    // DELETE BOOKING
                }*/
                Data.RCenGeboekt.Remove(original);
               // Data.LastUpdated = DateTime.Now;
            }
        }

        public void UpdateRCenGeboekt(EY.com.eBook.RuleEngine.AY2015.BTWAppTypes.RCGeboekt entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2015.BTWAppTypes.RCGeboekt original = Data.RCenGeboekt.FirstOrDefault(e => e.Id == entity.Id);
            if (original == null)
            {
                AddToRCenGeboekt(entity, ref worksheetset);
            }
            else
            {
                entity = Validate(entity, "RCenGeboekt", ref worksheetset);
                if (entity.IsValid)
                {
                    int idx = Data.RCenGeboekt.IndexOf(original);
                    Data.RCenGeboekt.RemoveAt(idx);
                    Data.RCenGeboekt.Insert(idx, entity);
               //     Data.LastUpdated = DateTime.Now;
                    entity.LastUpdated = DateTime.Now;
                }
            }
        }

        public void AddToRCVerklaringen(EY.com.eBook.RuleEngine.AY2015.BTWAppTypes.RCVerklaring entity, ref WorksheetSetData worksheetset)
        {
            entity = Validate(entity, "RCVerklaringen", ref worksheetset);
            if (entity.IsValid)
            {
              //  Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
                Data.RCVerklaringen.Add(entity);
            }
        }

        public void DeleteRCVerklaringen(Guid rowId, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2015.BTWAppTypes.RCVerklaring original = Data.RCVerklaringen.FirstOrDefault(e => e.Id == rowId);
            if (original != null)
            {
                /*
                if (original.Boeken.HasValue && original.Boeken.Value)
                {
                    worksheetset.DeleteBooking(FacturatieApp.TypeId, "OpTeMakenCN", original.Id, null);
                    // DELETE BOOKING
                }*/
                Data.RCVerklaringen.Remove(original);
              // Data.LastUpdated = DateTime.Now;
            }
        }

        public void UpdateRCVerklaringen(EY.com.eBook.RuleEngine.AY2015.BTWAppTypes.RCVerklaring entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2015.BTWAppTypes.RCVerklaring original = Data.RCVerklaringen.FirstOrDefault(e => e.Id == entity.Id);
            if (original == null)
            {
                AddToRCVerklaringen(entity, ref worksheetset);
            }
            else
            {
                entity = Validate(entity, "RCVerklaringen", ref worksheetset);
                if (entity.IsValid)
                {
                    int idx = Data.RCVerklaringen.IndexOf(original);
                    Data.RCVerklaringen.RemoveAt(idx);
                    Data.RCVerklaringen.Insert(idx, entity);
                //    Data.LastUpdated = DateTime.Now;
                    entity.LastUpdated = DateTime.Now;
                }
            }
        }

        #endregion
        // BOOKINGS
        
        // IMPORT PREVIOUS
        
    }
}
  