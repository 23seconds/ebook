
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.RuleEngine.Core;
using EY.com.eBook.RuleEngine.Core.Interfaces;
using EY.com.eBook.RuleEngine.Data;
using EY.com.eBook.Core;
using EY.com.eBook.API.Contracts.Data.Cache;
using System.Globalization;

namespace EY.com.eBook.RuleEngine.AY2015
{
    
    public class ErelonenHuurApp
    {
        public static WorksheetTypeSettings Settings = new WorksheetTypeSettings
                                                      {
                                                        Clients = true
                                                        ,
                                                        Suppliers = true
                                                        ,
                                                        AllAccounts = true
                                                        , 
                                                        PerformsBookings=true
                                                      };
                                                      
        public static Guid TypeId = new Guid("3e9bb601-ea23-4072-bd49-e5dc61a6b5a8");

        public List<string> ImportablePreviousTypes  = new List<string>() {  "EY.com.eBook.RuleEngine.AY2014.FacturatieApp"  };

        public Guid GetMyType() { return TypeId; }

        public void SetMinimal()
        {
            if (Data == null)
            {
                Data = new EY.com.eBook.RuleEngine.AY2015.ErelonenHuurAppTypes.ErelonenHuur();
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.Import == null)
            {
                Data.Import = new List<EY.com.eBook.RuleEngine.AY2015.ErelonenHuurAppTypes.ImportItem>(); ;
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.ImportedHistory == null)
            {
                Data.ImportedHistory = new List<EY.com.eBook.RuleEngine.AY2015.ErelonenHuurAppTypes.ImportedHistory>(); ;
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.Historiek == null)
            {
                Data.Historiek = new List<EY.com.eBook.RuleEngine.AY2015.ErelonenHuurAppTypes.VerdelingHistoriek>(); ;
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.Fiches281_50 == null)
            {
                Data.Fiches281_50 = new List<EY.com.eBook.RuleEngine.AY2015.ErelonenHuurAppTypes.Fiche281_50>(); ;
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.Fiche281_50Col == null)
            {
                Data.Fiche281_50Col = new List<EY.com.eBook.RuleEngine.AY2015.ErelonenHuurAppTypes.Fiche281_50>(); ;
                Data.LastUpdated = DateTime.Now;
            }

        }
        
        public string ToXml() 
        {
            return CoreHelper.SerializeToString(Data);
        }

        public void LoadXml(string xml)
        {
            if (!string.IsNullOrEmpty(xml)) {
                Data = CoreHelper.DeserializeFromString<EY.com.eBook.RuleEngine.AY2015.ErelonenHuurAppTypes.ErelonenHuur>(xml);
            }
            SetMinimal();
            
        }
        
        public EY.com.eBook.RuleEngine.AY2015.ErelonenHuurAppTypes.ErelonenHuur Data { get; set; }

        #region AddUpdateDelete

        public void AddToHistoriek(EY.com.eBook.RuleEngine.AY2015.ErelonenHuurAppTypes.VerdelingHistoriek entity, ref WorksheetSetData worksheetset)
        {
            entity = Validate(entity, "Historiek", ref worksheetset);
            if (entity.IsValid)
            {
                //Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;

                BusinessRelationDataContract br = worksheetset.GetBusinessRelation(new Guid(entity.Leverancier.Id));
                if (string.IsNullOrEmpty(br.FirstName))
                {
                    entity.AardVerkrijger = new GuidListItemDataContract()
                    {
                        EN = "Legal entity",
                        FR = "Personne morale",
                        NL = "Rechtspersoon",
                        Id = new Guid("8FE420B3-32A2-4DEB-858E-EF961175DE15"),
                        Value = 0
                    };

                }
                else
                {
                    entity.AardVerkrijger = new GuidListItemDataContract()
                    {
                        EN = "Individual",
                        FR = "Personne physique",
                        NL = "Natuurlijk persoon",
                        Id = new Guid("26DEE86A-4648-474A-8050-1261D431404D"),
                        Value = 0
                    };
                }

                Data.Historiek.Add(entity);
            }
        }

        public void DeleteHistoriek(Guid rowId, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2015.ErelonenHuurAppTypes.VerdelingHistoriek original = Data.Historiek.FirstOrDefault(e => e.Id == rowId);
            if (original != null)
            {
                /*
                if (original.Boeken.HasValue && original.Boeken.Value)
                {
                    worksheetset.DeleteBooking(FacturatieApp.TypeId, "OpTeMakenCN", original.Id, null);
                    // DELETE BOOKING
                }*/
                Data.Historiek.Remove(original);
                Data.LastUpdated = DateTime.Now;
            }
        }

        public void UpdateHistoriek(EY.com.eBook.RuleEngine.AY2015.ErelonenHuurAppTypes.VerdelingHistoriek entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2015.ErelonenHuurAppTypes.VerdelingHistoriek original = Data.Historiek.FirstOrDefault(e => e.Id == entity.Id);
            if (original == null)
            {
                AddToHistoriek(entity, ref worksheetset);
            }
            else
            {
                entity = Validate(entity, "Historiek", ref worksheetset);
                if (entity.IsValid)
                {
                    int idx = Data.Historiek.IndexOf(original);
                    Data.Historiek.RemoveAt(idx);
                    Data.Historiek.Insert(idx, entity);
                    BusinessRelationDataContract br = worksheetset.GetBusinessRelation(new Guid(entity.Leverancier.Id));
                    if (string.IsNullOrEmpty(br.FirstName))
                    {
                        entity.AardVerkrijger = new GuidListItemDataContract()
                        {
                            EN = "Legal entity",
                            FR = "Personne morale",
                            NL = "Rechtspersoon",
                            Id = new Guid("8FE420B3-32A2-4DEB-858E-EF961175DE15"),
                            Value = 0
                        };

                    }
                    else
                    {
                        entity.AardVerkrijger = new GuidListItemDataContract()
                        {
                            EN = "Individual",
                            FR = "Personne physique",
                            NL = "Natuurlijk persoon",
                            Id = new Guid("26DEE86A-4648-474A-8050-1261D431404D"),
                            Value = 0
                        };
                    }
                    entity.LastUpdated = DateTime.Now;
                }
            }
        }

        public void AddToFiches281_50(EY.com.eBook.RuleEngine.AY2015.ErelonenHuurAppTypes.Fiche281_50 entity, ref WorksheetSetData worksheetset)
        {
            entity = Validate(entity, "Fiches281_50", ref worksheetset);
            if (entity.IsValid)
            {
                Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
                Data.Fiches281_50.Add(entity);
            }
        }

        public void UpdateFiches281_50(EY.com.eBook.RuleEngine.AY2015.ErelonenHuurAppTypes.Fiche281_50 entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2015.ErelonenHuurAppTypes.Fiche281_50 original = Data.Fiches281_50.FirstOrDefault(e => e.Id == entity.Id);
            if (original == null)
            {
                AddToFiches281_50(entity, ref worksheetset);
            }
            else
            {
                entity = Validate(entity, "Fiches281_50", ref worksheetset);
                if (entity.IsValid)
                {
                    int idx = Data.Fiches281_50.IndexOf(original);
                    Data.Fiches281_50.RemoveAt(idx);
                    Data.Fiches281_50.Insert(idx, entity);
                    Data.LastUpdated = DateTime.Now;
                    entity.LastUpdated = DateTime.Now;
                }
            }
        }

        public void AddToFiche281_50Col(EY.com.eBook.RuleEngine.AY2015.ErelonenHuurAppTypes.Fiche281_50 entity, ref WorksheetSetData worksheetset)
        {
            entity = Validate(entity, "Fiche281_50Col", ref worksheetset);
            if (entity.IsValid)
            {
                Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
                Data.Fiche281_50Col.Add(entity);
            }
        }

        public void DeleteFiche281_50Col(Guid rowId, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2015.ErelonenHuurAppTypes.Fiche281_50 original = Data.Fiche281_50Col.FirstOrDefault(e => e.Id == rowId);
            if (original != null)
            {
                /*
                if (original.Boeken.HasValue && original.Boeken.Value)
                {
                    worksheetset.DeleteBooking(FacturatieApp.TypeId, "OpTeMakenCN", original.Id, null);
                    // DELETE BOOKING
                }*/
                Data.Fiche281_50Col.Remove(original);
                Data.LastUpdated = DateTime.Now;
            }
        }

        public void UpdateFiche281_50Col(EY.com.eBook.RuleEngine.AY2015.ErelonenHuurAppTypes.Fiche281_50 entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2015.ErelonenHuurAppTypes.Fiche281_50 original = Data.Fiche281_50Col.FirstOrDefault(e => e.Id == entity.Id);
            if (original == null)
            {
                AddToFiche281_50Col(entity, ref worksheetset);
            }
            else
            {
                entity = Validate(entity, "Fiche281_50Col", ref worksheetset);
                if (entity.IsValid)
                {
                    int idx = Data.Fiche281_50Col.IndexOf(original);
                    Data.Fiche281_50Col.RemoveAt(idx);
                    Data.Fiche281_50Col.Insert(idx, entity);
                    Data.LastUpdated = DateTime.Now;
                    entity.LastUpdated = DateTime.Now;
                }
            }
        }

        #endregion

        #region VALIDATION

        public EY.com.eBook.RuleEngine.AY2015.ErelonenHuurAppTypes.ImportItem Validate(EY.com.eBook.RuleEngine.AY2015.ErelonenHuurAppTypes.ImportItem entity, string location, ref WorksheetSetData worksheetset)
        {
            entity.IsValid = true;
            worksheetset.ClearMessages(TypeId, entity.Id);
            /*
            if (!entity.Bedrag.HasValue || entity.Bedrag.Value==0)
            {
            worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Grootboek");
            entity.IsValid = false;
            }
            */

            return entity;
        }

        public EY.com.eBook.RuleEngine.AY2015.ErelonenHuurAppTypes.ImportedHistory Validate(EY.com.eBook.RuleEngine.AY2015.ErelonenHuurAppTypes.ImportedHistory entity, string location, ref WorksheetSetData worksheetset)
        {
            entity.IsValid = true;
            worksheetset.ClearMessages(TypeId, entity.Id);
            /*
            if (!entity.Bedrag.HasValue || entity.Bedrag.Value==0)
            {
            worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Grootboek");
            entity.IsValid = false;
            }
            */

            return entity;
        }

        public EY.com.eBook.RuleEngine.AY2015.ErelonenHuurAppTypes.VerdelingHistoriek Validate(EY.com.eBook.RuleEngine.AY2015.ErelonenHuurAppTypes.VerdelingHistoriek entity, string location, ref WorksheetSetData worksheetset)
        {
            entity.IsValid = true;
            worksheetset.ClearMessages(TypeId, entity.Id);
            /*
            if (!entity.Bedrag.HasValue || entity.Bedrag.Value==0)
            {
            worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Grootboek");
            entity.IsValid = false;
            }
            */
            if (entity.IsValid)
            {
                entity.Betaald = entity.Betaald.GetValueOrDefault();
                entity.FiscaleWaarde = entity.FiscaleWaarde.GetValueOrDefault();
                entity.Terugname = entity.Terugname.GetValueOrDefault();
                entity.Toename = entity.Toename.GetValueOrDefault();
            }

            return entity;
        }

        public EY.com.eBook.RuleEngine.AY2015.ErelonenHuurAppTypes.Fiche281_50 Validate(EY.com.eBook.RuleEngine.AY2015.ErelonenHuurAppTypes.Fiche281_50 entity, string location, ref WorksheetSetData worksheetset)
        {
            entity.IsValid = true;
            worksheetset.ClearMessages(TypeId, entity.Id);
            /*
            if (!entity.Bedrag.HasValue || entity.Bedrag.Value==0)
            {
            worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Grootboek");
            entity.IsValid = false;
            }
            */

            return entity;
        }

        #endregion

        #region worksheet type rules
        // Only if worksheet does any calculation.
        public List<string> GetImportableAccounts()
        {
            return Data.Import.Select(i => i.GrootBoek).Distinct().ToList();
        }

        public void ImportAccountHistory(List<AccountHistoryDataContract> hist, ref WorksheetSetData worksheetset)
        {
            Data.ImportedHistory.Clear();
            foreach (AccountHistoryDataContract ahdc in hist)
            {
                StringListItemDataContract sli = worksheetset.GetAccount(ahdc.AccountNr, true, false, worksheetset.DataContract.Culture).GetListItem();
                foreach (AccountHistoryLineDataContract ahldc in ahdc.History)
                {
                    BusinessRelationDataContract br = worksheetset.GetBusinessRelation(ahldc.BusinessRelationProAccId);
                    

                    Data.ImportedHistory.Add(new ErelonenHuurAppTypes.ImportedHistory
                    {
                        Id = Guid.NewGuid()
                        ,
                        Datum = ahldc.Date
                        ,
                        Grootboek = sli
                        ,
                        IsValid = true
                        ,
                        LastUpdated = DateTime.Now
                        ,
                        Leverancier = br !=null ? br.GetListItem():null
                        ,
                        PreviousImported = true
                        ,
                        Saldo = ahldc.Debet - ahldc.Credit
                    });
                }

            }

            Data.Historiek.ForEach(h => h.TotaalHistoriek = 0);
            List<string> leveranciers = Data.ImportedHistory.Where(i => i.Leverancier != null).Select(i => i.Leverancier.Id).Distinct().ToList();
            foreach (ErelonenHuurAppTypes.ImportedHistory ih in Data.ImportedHistory.Where(i => i.Leverancier != null))
            {
                ErelonenHuurAppTypes.ImportItem ii = Data.Import.FirstOrDefault(i=>i.GrootBoek==ih.Grootboek.Id && i.Soort!=null);
                if(ii!=null) {
                    ErelonenHuurAppTypes.VerdelingHistoriek vh = Data.Historiek.FirstOrDefault(h => h.Leverancier.Id == ih.Leverancier.Id && h.Soort.Id == ii.Soort.Id);
                    if (vh == null)
                    {
                        vh = new EY.com.eBook.RuleEngine.AY2015.ErelonenHuurAppTypes.VerdelingHistoriek
                        {
                            Id = Guid.NewGuid()
                            ,
                            Fiche = true
                            ,
                            AardVerkrijger = null
                            ,
                            Betaald = null
                            ,
                            FiscaleWaarde = null
                            ,
                            ImportedAccount = new StringListItemDataContract { Id = ii.Id.ToString(), NL = ii.GrootboekOmschrijving, FR = ii.GrootboekOmschrijving, EN = ii.GrootboekOmschrijving }
                            ,
                            Leverancier = ih.Leverancier
                            ,
                            PreviousImported = false
                            ,
                            Soort = ii.Soort
                            ,
                            Terugname = 0
                            ,
                            Toename = 0
                            ,
                            TotaalHistoriek = ih.Saldo
                        };
                        AddToHistoriek(vh, ref worksheetset);
                    }
                    else
                    {
                        
                        vh.TotaalHistoriek += ih.Saldo;
                        vh.Soort = ii.Soort;
                        UpdateHistoriek(vh, ref worksheetset);
                    }
                }
            }

        }

        public void ForceRecalculation(ref WorksheetSetData worksheetset)
        {
            Data.LastUpdated = new DateTime(1980, 1, 1);
            

            Calculate(ref worksheetset);
        }

        public void Calculate(ref WorksheetSetData helper)
        {
            helper.ClearMessages(TypeId);
            string myMappingKey = "Erelonen";
            List<AccountDataContract> accounts = helper.GetAccountsOfMapping(myMappingKey, false, true, true, helper.DataContract.Culture);
            accounts.RemoveAll(a => a.Saldo == 0);
            List<string> accountNrs = accounts.Select(a => a.InternalNr).ToList();

            // List<string> currentData = Data.Import.Select(a => a.GrootBoek).ToList();

            // DELETE STRAYS (accounts that are no longer mapped)
            Data.Import.RemoveAll(i => !accountNrs.Contains(i.GrootBoek) && !i.PreviousImported);

            List<string> currentData = Data.Import.Select(a => a.GrootBoek).ToList();
            List<string> importIds = Data.Import.Select(a => a.Id.ToString()).ToList();
            List<string> importNrs = Data.Import.Select(a => a.GrootBoek).ToList();

            Data.ImportedHistory.RemoveAll(ih => !importNrs.Contains(ih.Grootboek.Id));
            Data.Historiek.RemoveAll(h =>h.ImportedAccount!=null && !importIds.Contains(h.ImportedAccount.Id));

            // items to update
            foreach (EY.com.eBook.RuleEngine.AY2015.ErelonenHuurAppTypes.ImportItem ii in Data.Import.Where(i => accountNrs.Contains(i.GrootBoek)))
            {
                AccountDataContract adc = accounts.First(a => a.InternalNr == ii.GrootBoek);
                Guid mappingId = adc.Mappings.First(a => a.MappingKey == myMappingKey).MappingItemId.Value;
                // GET MAPPING ITEM WITH TRANSLATIONS!
                MappingItemDataContract midc = helper.GetMappingItem(mappingId);

                StringListItemDataContract sidc = new StringListItemDataContract
                {
                    Id = mappingId.ToString()
                    ,
                    NL = midc.ListObject.NL
                    ,
                    FR = midc.ListObject.FR
                    ,
                    EN = midc.ListObject.EN
                };
                if (ii.FiscaleWaarde.Value == ii.Bedrag.Value || ii.Bedrag.Value == 0)
                {
                    ii.Bedrag = adc.Saldo;
                }
                if (ii.Soort == null) ii.Soort = sidc;
                if (ii.Soort.Id != mappingId.ToString()) ii.Soort = sidc;
                ii.FiscaleWaarde = adc.Saldo;
                ii.GrootBoek = adc.InternalNr;
                ii.GrootboekOmschrijving = adc.DefaultDescription;
                ii.Rekening = string.Format("{0} {1}", adc.VisualNr, adc.DefaultDescription);

            }

            // items to add
            foreach (AccountDataContract adc in accounts.Where(a => !currentData.Contains(a.InternalNr) && a.Mappings.Count(m=>m.MappingKey==myMappingKey)>0))
            {
                Guid mappingId = adc.Mappings.First(a => a.MappingKey == myMappingKey).MappingItemId.Value;
                // GET MAPPING ITEM WITH TRANSLATIONS!
                MappingItemDataContract midc = helper.GetMappingItem(mappingId);

                StringListItemDataContract sidc = new StringListItemDataContract
                {
                    Id = mappingId.ToString()
                    ,
                    NL = midc.ListObject.NL
                    ,
                    FR = midc.ListObject.FR
                    ,
                    EN = midc.ListObject.EN
                };
                EY.com.eBook.RuleEngine.AY2015.ErelonenHuurAppTypes.ImportItem ii = new EY.com.eBook.RuleEngine.AY2015.ErelonenHuurAppTypes.ImportItem
                {
                    Id = Guid.NewGuid()
                    ,
                    GrootBoek = adc.InternalNr
                    ,
                    Bedrag = adc.Saldo
                    ,
                    GrootboekOmschrijving = adc.DefaultDescription
                    ,
                    LastUpdated = DateTime.Now
                    ,
                    Rekening = string.Format("{0} {1}", adc.VisualNr, adc.DefaultDescription)
                    ,
                    Soort = sidc
                    ,
                    FiscaleWaarde = adc.Saldo
                };
                Data.Import.Add(ii);
            }

            // CALCULATE FICHES
            List<string> leverancierIds = Data.Historiek.Where(h => h.Leverancier != null).Select(h => h.Leverancier.Id).Distinct().ToList();

            CultureInfo ci = CultureInfo.CreateSpecificCulture("nl-BE");

            int cnt = 1;
            foreach (string leverancierId in leverancierIds)
            {
                EY.com.eBook.RuleEngine.AY2015.ErelonenHuurAppTypes.Fiche281_50 fiche = Data.Fiches281_50.Where(f => f.Leverancier != null).FirstOrDefault(f => f.Leverancier.Id == leverancierId);

                if (fiche == null)
                {
                    fiche = new EY.com.eBook.RuleEngine.AY2015.ErelonenHuurAppTypes.Fiche281_50
                    {
                        Id = Guid.NewGuid()
                        ,
                        Leverancier = new StringListItemDataContract { Id = leverancierId }
                    };
                    Data.Fiches281_50.Add(fiche);
                }

                if (fiche != null)
                {
                    fiche.Commissies = Data.Historiek.Where(h => h.Leverancier != null && h.Leverancier.Id == leverancierId
                                                                && h.Soort !=null
                                                                && h.Soort.Id.ToUpper() == "EB417EC7-1207-497E-A8B0-CB9AF71A7BCE" 
                                                                && h.Fiche.HasValue && h.Fiche.Value
                                                            ).Sum(h => h.FiscaleWaarde.GetValueOrDefault()
                                                                        + (h.Toename.GetValueOrDefault())
                                                                        - (h.Terugname.GetValueOrDefault())
                                                                );
                    fiche.Erelonen = Data.Historiek.Where(h => h.Leverancier != null && h.Leverancier.Id == leverancierId
                                                                && h.Soort != null
                                                                && h.Soort.Id.ToUpper() == "200ADB0A-B150-46E9-B572-96F21BA431BD"
                                                                && h.Fiche.HasValue && h.Fiche.Value
                                                            ).Sum(h => h.FiscaleWaarde.GetValueOrDefault()
                                                                        + (h.Toename.GetValueOrDefault())
                                                                        - (h.Terugname.GetValueOrDefault())
                                                                );
                    fiche.Voordelen = Data.Historiek.Where(h => h.Leverancier != null && h.Leverancier.Id == leverancierId
                                                                 && h.Soort != null
                                                                && h.Soort.Id.ToUpper() == "4B6FEA84-DAE6-4D50-B666-080A8085DD35"
                                                                && h.Fiche.HasValue && h.Fiche.Value
                                                            ).Sum(h => h.FiscaleWaarde.GetValueOrDefault()
                                                                        + (h.Toename.GetValueOrDefault())
                                                                        - (h.Terugname.GetValueOrDefault())
                                                                );

                    fiche.Kosten = Data.Historiek.Where(h => h.Leverancier != null && h.Leverancier.Id == leverancierId
                                                                && h.Soort != null
                                                                && h.Soort.Id.ToUpper() == "60551E91-59DF-4B70-B5F2-EFC98B15416B"
                                                                && h.Fiche.HasValue && h.Fiche.Value
                                                            ).Sum(h => h.FiscaleWaarde.GetValueOrDefault()
                                                                        + (h.Toename.GetValueOrDefault())
                                                                        - (h.Terugname.GetValueOrDefault())
                                                                );

                    fiche.Uitbetaling = Data.Historiek.Where(h => h.Leverancier != null && h.Leverancier.Id == leverancierId
                                                                    && h.Fiche.HasValue && h.Fiche.Value
                                                                ).Sum(h => h.Betaald.GetValueOrDefault());

                    fiche.Totaal = fiche.Commissies.GetValueOrDefault() + fiche.Erelonen.GetValueOrDefault() + fiche.Kosten.GetValueOrDefault() + fiche.Voordelen.GetValueOrDefault();
                    fiche.Nr = cnt;
                    //fiche.AardVerkrijger = Data.Historiek.First(h => h.Leverancier != null && h.Leverancier.Id == leverancierId).AardVerkrijger;
                    fiche.UitgeoefendBeroep = Data.Historiek.First(h => h.Leverancier != null && h.Leverancier.Id == leverancierId).UitgeoefendBeroep;
                    decimal sum = fiche.Totaal.GetValueOrDefault();
                    fiche.Fiche = true;

                    fiche.Uitbetaald = (!fiche.Uitbetaling.HasValue || fiche.Uitbetaling.GetValueOrDefault() == 0) ? "NIHIL"
                                            : ((fiche.Uitbetaling.GetValueOrDefault() == sum) ? "NvT"
                                                    : fiche.Uitbetaling.GetValueOrDefault().ToString("0,0.00", ci.NumberFormat));


                    if (fiche.Commissies.GetValueOrDefault() == 0 && fiche.Erelonen.GetValueOrDefault() == 0 && fiche.Voordelen.GetValueOrDefault() == 0 && fiche.Kosten.GetValueOrDefault() == 0 && !fiche.PreviousImported)
                    {
                        EY.com.eBook.RuleEngine.AY2015.ErelonenHuurAppTypes.Fiche281_50 fcol = Data.Fiche281_50Col.Where(f=>f.Leverancier!=null).FirstOrDefault(f => f.Leverancier.Id == fiche.Leverancier.Id);
                        if (fcol != null) Data.Fiche281_50Col.Remove(fcol);
                        Data.Fiches281_50.Remove(fiche);
                    }
                    else
                    {
                        BusinessRelationDataContract br = helper.GetBusinessRelation(new Guid(leverancierId));
                        if (br != null)
                        {
                            fiche.NaamLeverancier = br.LastName;
                            fiche.AdresLeverancier = br.Address;
                            fiche.PostcodeLeverancier = br.ZipCode;
                            fiche.WoonplaatsLeverancier = br.City;
                            fiche.Ondernemingsnummer = br.CompanyNr;
                            fiche.LandLeverancier = !string.IsNullOrEmpty(br.Country) ? br.Country : "België";
                            fiche.VoornaamLeverancier = br.FirstName;
                            if (string.IsNullOrEmpty(br.FirstName))
                            {
                                fiche.AardVerkrijger = new GuidListItemDataContract()
                                {
                                    EN = "Legal entity",
                                    FR = "Personne morale",
                                    NL = "Rechtspersoon",
                                    Id = new Guid("8FE420B3-32A2-4DEB-858E-EF961175DE15"),
                                    Value = 0
                                };
                                
                            }
                            else {
                                fiche.AardVerkrijger = new GuidListItemDataContract()
                                {
                                    EN = "Individual",
                                    FR = "Personne physique",
                                    NL = "Natuurlijk persoon",
                                    Id = new Guid("26DEE86A-4648-474A-8050-1261D431404D"),
                                    Value = 0
                                };
                            }
                        }
                        else
                        {
                            fiche.NaamLeverancier = "COULD NOT FIND SUPPLIER";
                        }
                        if (sum >= 125)
                        {

                            int idx = Data.Fiche281_50Col.FindIndex(f =>f.Leverancier!= null && f.Leverancier.Id == fiche.Leverancier.Id);
                            if (idx > -1)
                            {
                                Data.Fiche281_50Col.RemoveAt(idx);
                                Data.Fiche281_50Col.Insert(idx, fiche);
                            }
                            else
                            {
                                Data.Fiche281_50Col.Add(fiche);
                            }

                        }
                        else
                        {
                            EY.com.eBook.RuleEngine.AY2015.ErelonenHuurAppTypes.Fiche281_50 fcol = Data.Fiche281_50Col.FirstOrDefault(f => f.Leverancier.Id == fiche.Leverancier.Id);
                            if (fcol != null) Data.Fiche281_50Col.Remove(fcol);
                            object[] vals = new object[] { fiche.NaamLeverancier };
                            helper.SetMessage(TypeId, MessageTypeDataContract.Warning, "FICHE125", "Fiches281_50", fiche.Id, null, vals);
                        }
                    }
                }

            }
            Data.Fiche281_50Col.RemoveAll(f => f.Leverancier == null);

        }

        #endregion
        
        // BOOKINGS

        #region worksheet type import previous
        
        public void ImportPrevious(ref WorksheetSetData wsd)
        {
            if (wsd.PreviousSet.AssessmentYear == 2013)
            {
                EY.com.eBook.RuleEngine.AY2013.WorksheetSetData pset = (EY.com.eBook.RuleEngine.AY2013.WorksheetSetData)wsd.PreviousSet;

                EY.com.eBook.RuleEngine.AY2013.ErelonenHuurApp previous = pset.ErelonenHuurApp;
                List<EY.com.eBook.RuleEngine.AY2013.ErelonenHuurAppTypes.VerdelingHistoriek> prevHistory = previous.Data.Historiek.Where(h => h.Toename.HasValue && h.Toename != 0).ToList();

                List<string> importIds = prevHistory.Select(h => h.ImportedAccount.Id).ToList();
                List<EY.com.eBook.RuleEngine.AY2013.ErelonenHuurAppTypes.ImportItem> imports = previous.Data.Import.Where(i => importIds.Contains(i.Id.ToString())).ToList();

                List<string> previousAccounts = imports.Select(i => i.GrootBoek).ToList();


                foreach (EY.com.eBook.RuleEngine.AY2013.ErelonenHuurAppTypes.ImportItem ii in imports.Where(i => !previousAccounts.Contains(i.GrootBoek)))
                {
                    Data.Import.Add(new EY.com.eBook.RuleEngine.AY2015.ErelonenHuurAppTypes.ImportItem
                    {
                        Id = ii.Id
                        ,
                        Bedrag = 0
                        ,
                        FiscaleWaarde = 0
                        ,
                        GrootBoek = ii.GrootBoek
                        ,
                        GrootboekOmschrijving = ii.GrootboekOmschrijving
                        ,
                        LastUpdated = DateTime.Now
                        ,
                        PreviousImported = true
                        ,
                        Rekening = ii.Rekening
                        ,
                        Soort = ii.Soort
                    });
                }

                List<string> currentAccounts = Data.Import.Select(i => i.GrootBoek).ToList();

                foreach (EY.com.eBook.RuleEngine.AY2015.ErelonenHuurAppTypes.ImportItem ii in Data.Import.Where(i => previousAccounts.Contains(i.GrootBoek)))
                {
                    EY.com.eBook.RuleEngine.AY2013.ErelonenHuurAppTypes.ImportItem prevIi = imports.First(i => i.GrootBoek == ii.GrootBoek);
                    List<EY.com.eBook.RuleEngine.AY2013.ErelonenHuurAppTypes.VerdelingHistoriek> prevhists = prevHistory.Where(h => h.ImportedAccount.Id == ii.Id.ToString()).ToList();

                    foreach (EY.com.eBook.RuleEngine.AY2013.ErelonenHuurAppTypes.VerdelingHistoriek prevHist in prevhists)
                    {
                        EY.com.eBook.RuleEngine.AY2015.ErelonenHuurAppTypes.VerdelingHistoriek hist = Data.Historiek.FirstOrDefault(h => h.ImportedAccount.Id == ii.Id.ToString()
                                                                                                                                && h.Leverancier.Id == prevHist.Leverancier.Id
                                                                                                                                && h.SoortMeta == prevHist.SoortMeta
                                                                                                                                );
                        if (hist == null)
                        {
                            hist = new EY.com.eBook.RuleEngine.AY2015.ErelonenHuurAppTypes.VerdelingHistoriek
                            {
                                Id = Guid.NewGuid()
                                ,
                                AardVerkrijger = prevHist.AardVerkrijger
                                ,
                                Betaald = 0
                                ,
                                Fiche = true
                                ,
                                FiscaleWaarde = 0
                                ,
                                ImportedAccount = new StringListItemDataContract { Id = ii.Id.ToString(), NL = ii.GrootboekOmschrijving }
                                ,
                                LastUpdated = new DateTime(1980, 1, 1)
                                ,
                                Leverancier = prevHist.Leverancier
                                ,
                                PreviousImported = true
                                ,
                                Soort = prevHist.Soort
                                ,
                                SoortMeta = prevHist.SoortMeta
                                ,
                                Terugname = 0
                                ,
                                Toename = 0
                                ,
                                TotaalHistoriek = 0
                                ,
                                UitgeoefendBeroep = prevHist.UitgeoefendBeroep
                            };
                            Data.Historiek.Add(hist);
                        }
                        hist.Terugname = prevHist.Toename;

                    }

                }
            }
            // Calculate(ref helper);

            if (wsd.PreviousSet.AssessmentYear == 2014)
            {
                EY.com.eBook.RuleEngine.AY2014.WorksheetSetData pset = (EY.com.eBook.RuleEngine.AY2014.WorksheetSetData)wsd.PreviousSet;

                EY.com.eBook.RuleEngine.AY2014.ErelonenHuurApp previous = pset.ErelonenHuurApp;
                List<EY.com.eBook.RuleEngine.AY2014.ErelonenHuurAppTypes.VerdelingHistoriek> prevHistory = previous.Data.Historiek.Where(h => h.Toename.HasValue && h.Toename != 0).ToList();

                List<string> importIds = prevHistory.Select(h => h.ImportedAccount.Id).ToList();
                List<EY.com.eBook.RuleEngine.AY2014.ErelonenHuurAppTypes.ImportItem> imports = previous.Data.Import.Where(i => importIds.Contains(i.Id.ToString())).ToList();

                List<string> previousAccounts = imports.Select(i => i.GrootBoek).ToList();


                foreach (EY.com.eBook.RuleEngine.AY2014.ErelonenHuurAppTypes.ImportItem ii in imports.Where(i => !previousAccounts.Contains(i.GrootBoek)))
                {
                    Data.Import.Add(new EY.com.eBook.RuleEngine.AY2015.ErelonenHuurAppTypes.ImportItem
                    {
                        Id = ii.Id
                        ,
                        Bedrag = 0
                        ,
                        FiscaleWaarde = 0
                        ,
                        GrootBoek = ii.GrootBoek
                        ,
                        GrootboekOmschrijving = ii.GrootboekOmschrijving
                        ,
                        LastUpdated = DateTime.Now
                        ,
                        PreviousImported = true
                        ,
                        Rekening = ii.Rekening
                        ,
                        Soort = ii.Soort
                    });
                }

                List<string> currentAccounts = Data.Import.Select(i => i.GrootBoek).ToList();

                foreach (EY.com.eBook.RuleEngine.AY2015.ErelonenHuurAppTypes.ImportItem ii in Data.Import.Where(i => previousAccounts.Contains(i.GrootBoek)))
                {
                    EY.com.eBook.RuleEngine.AY2014.ErelonenHuurAppTypes.ImportItem prevIi = imports.First(i => i.GrootBoek == ii.GrootBoek);
                    List<EY.com.eBook.RuleEngine.AY2014.ErelonenHuurAppTypes.VerdelingHistoriek> prevhists = prevHistory.Where(h => h.ImportedAccount.Id == ii.Id.ToString()).ToList();

                    foreach (EY.com.eBook.RuleEngine.AY2014.ErelonenHuurAppTypes.VerdelingHistoriek prevHist in prevhists)
                    {
                        EY.com.eBook.RuleEngine.AY2015.ErelonenHuurAppTypes.VerdelingHistoriek hist = Data.Historiek.FirstOrDefault(h => h.ImportedAccount.Id == ii.Id.ToString()
                                                                                                                                && h.Leverancier.Id == prevHist.Leverancier.Id
                                                                                                                                && h.SoortMeta == prevHist.SoortMeta
                                                                                                                                );
                        if (hist == null)
                        {
                            hist = new EY.com.eBook.RuleEngine.AY2015.ErelonenHuurAppTypes.VerdelingHistoriek
                            {
                                Id = Guid.NewGuid()
                                ,
                                AardVerkrijger = prevHist.AardVerkrijger
                                ,
                                Betaald = 0
                                ,
                                Fiche = true
                                ,
                                FiscaleWaarde = 0
                                ,
                                ImportedAccount = new StringListItemDataContract { Id = ii.Id.ToString(), NL = ii.GrootboekOmschrijving }
                                ,
                                LastUpdated = new DateTime(1980, 1, 1)
                                ,
                                Leverancier = prevHist.Leverancier
                                ,
                                PreviousImported = true
                                ,
                                Soort = prevHist.Soort
                                ,
                                SoortMeta = prevHist.SoortMeta
                                ,
                                Terugname = 0
                                ,
                                Toename = 0
                                ,
                                TotaalHistoriek = 0
                                ,
                                UitgeoefendBeroep = prevHist.UitgeoefendBeroep
                            };
                            Data.Historiek.Add(hist);
                        }
                        hist.Terugname = prevHist.Toename;

                    }

                }
            }

        }
        

        /*
        public void ImportPrevious(EY.com.eBook.RuleEngine.AY2010.FacturatieApp previous)
        {
            foreach (EY.com.eBook.RuleEngine.AY2010.FacturatieApp.FacturatieKlant klant 
                        in previous.Data.OpTeMakenCN.Where(e=>e.BookReversed.HasValue && e.BookReversed.Value))
            {
                EY.com.eBook.RuleEngine.AY2013.FacturatieAppTypes.FacturatieKlant newEnt = new EY.com.eBook.RuleEngine.AY2013.FacturatieAppTypes.FacturatieKlant
                {
                    Id = klant.Id
                    , Bedrag = 0-klant.Bedrag
                    , Boeken =true
                    , FacJaar = klant.FacJaar
                    , FactuurNr = klant.FactuurNr
                    , Klant = klant.Klant
                    , Grootboek = klant.Grootboek
                    , PreviousImported=true
                    , BookReversed =false
                    ,LastUpdated = DateTime.Now
                };
                Data.OpTeMakenCN.Add(newEnt);
            }

            foreach (EY.com.eBook.RuleEngine.AY2010.FacturatieApp.FacturatieKlant klant 
                        in previous.Data.OpTeMakenFacturen.Where(e=>e.BookReversed.HasValue && e.BookReversed.Value))
            {
                EY.com.eBook.RuleEngine.AY2013.FacturatieAppTypes.FacturatieKlant newEnt = new EY.com.eBook.RuleEngine.AY2013.FacturatieAppTypes.FacturatieKlant
                {
                    Id = klant.Id
                    , Bedrag = 0-klant.Bedrag
                    , Boeken =true
                    , FacJaar = klant.FacJaar
                    , FactuurNr = klant.FactuurNr
                    , Klant = klant.Klant
                    , Grootboek = klant.Grootboek
                    , PreviousImported=true
                    , BookReversed =false
                    ,LastUpdated = DateTime.Now
                };
                Data.OpTeMakenFacturen.Add(newEnt);
            }

            foreach (EY.com.eBook.RuleEngine.AY2010.FacturatieApp.FacturatieLeverancierCN klant 
                        in previous.Data.TeOntvangenCN.Where(e=>e.BookReversed.HasValue && e.BookReversed.Value))
            {
                EY.com.eBook.RuleEngine.AY2013.FacturatieAppTypes.FacturatieLeverancierCN newEnt = new EY.com.eBook.RuleEngine.AY2013.FacturatieAppTypes.FacturatieLeverancierCN
                {
                    Id = klant.Id
                    , Bedrag = 0-klant.Bedrag
                    , Boeken =true
                    , FacJaar = klant.FacJaar
                    , FactuurNr = klant.FactuurNr
                    , Leverancier = klant.Leverancier
                    , Grootboek = klant.Grootboek
                    , PreviousImported=true
                    , BookReversed =false
                    ,LastUpdated = DateTime.Now
                };
                Data.TeOntvangenCN.Add(newEnt);
            }

            foreach (EY.com.eBook.RuleEngine.AY2010.FacturatieApp.FacturatieLeverancier klant 
                        in previous.Data.TeOntvangenFacturen.Where(e=>e.BookReversed.HasValue && e.BookReversed.Value))
            {
                EY.com.eBook.RuleEngine.AY2013.FacturatieAppTypes.FacturatieLeverancier newEnt = new EY.com.eBook.RuleEngine.AY2013.FacturatieAppTypes.FacturatieLeverancier
                {
                    Id = klant.Id
                    , Bedrag = 0-klant.Bedrag
                    , Boeken =true
                    , FacJaar = klant.FacJaar
                    , FactuurNr = klant.FactuurNr
                    , Leverancier = klant.Leverancier
                    , Grootboek = klant.Grootboek
                    , PreviousImported=true
                    , BookReversed =false
                    ,LastUpdated = DateTime.Now
                };
                Data.TeOntvangenFacturen.Add(newEnt);
            }
        }

   */

        #endregion
        
    }
}
  