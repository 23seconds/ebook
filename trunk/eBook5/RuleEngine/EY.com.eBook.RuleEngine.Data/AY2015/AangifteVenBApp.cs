
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.RuleEngine.Core;
using EY.com.eBook.RuleEngine.Core.Interfaces;
using EY.com.eBook.RuleEngine.Data;
using EY.com.eBook.Core;

namespace EY.com.eBook.RuleEngine.AY2015    
{
    
    public class AangifteVenBApp
    {
        public static WorksheetTypeSettings Settings = new WorksheetTypeSettings
                                                      {
                                                        Clients = true
                                                        ,
                                                        Suppliers = true
                                                        ,
                                                        AllAccounts = true
                                                        , 
                                                        PerformsBookings=true
                                                      };
                                                      
        public static Guid TypeId = new Guid("65bf5cbd-187b-4c41-aad9-9793357f222b");

        public List<string> ImportablePreviousTypes  = new List<string>() {  "EY.com.eBook.RuleEngine.AY2014.FacturatieApp"  };

        public Guid GetMyType() { return TypeId; }

        public void SetMinimal()
        {
            if (Data == null)
            {
                Data = new EY.com.eBook.RuleEngine.AY2015.AangifteVenBAppTypes.AangifteVenB();
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.Reserves == null)
            {
                Data.Reserves = new EY.com.eBook.RuleEngine.AY2015.AangifteVenBAppTypes.ReserveEntity
                {
                    Id = Guid.NewGuid()
                    ,
                    LastUpdated = null
                    ,
                    IsValid = true
                    ,
                    PreviousImported = false

                };
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.VrijgesteldeReserve == null)
            {
                Data.VrijgesteldeReserve = new EY.com.eBook.RuleEngine.AY2015.AangifteVenBAppTypes.VrijgesteldeReserveEntity
                {
                    Id = Guid.NewGuid()
                    ,
                    LastUpdated = null
                    ,
                    IsValid = true
                    ,
                    PreviousImported = false

                };
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.VerworpenUitgaven == null)
            {
                Data.VerworpenUitgaven = new EY.com.eBook.RuleEngine.AY2015.AangifteVenBAppTypes.VerworpenUitgavenEntity
                {
                    Id = Guid.NewGuid()
                    ,
                    LastUpdated = null
                    ,
                    IsValid = true
                    ,
                    PreviousImported = false

                };
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.UitgekeerdeDividenden == null)
            {
                Data.UitgekeerdeDividenden = new EY.com.eBook.RuleEngine.AY2015.AangifteVenBAppTypes.UitgekeerdeDividendenEntity
                {
                    Id = Guid.NewGuid()
                    ,
                    LastUpdated = null
                    ,
                    IsValid = true
                    ,
                    PreviousImported = false

                };
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.UiteenzettingWinst == null)
            {
                Data.UiteenzettingWinst = new EY.com.eBook.RuleEngine.AY2015.AangifteVenBAppTypes.WinstEntity
                {
                    Id = Guid.NewGuid()
                    ,
                    LastUpdated = null
                    ,
                    IsValid = true
                    ,
                    PreviousImported = false

                };
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.AfzonderlijkeAanslagen == null)
            {
                Data.AfzonderlijkeAanslagen = new EY.com.eBook.RuleEngine.AY2015.AangifteVenBAppTypes.AfzonderlijkeAanslagenEntity
                {
                    Id = Guid.NewGuid()
                    ,
                    LastUpdated = null
                    ,
                    IsValid = true
                    ,
                    PreviousImported = false

                };
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.Diamant == null)
            {
                Data.Diamant = new EY.com.eBook.RuleEngine.AY2015.AangifteVenBAppTypes.DiamantEntity
                {
                    Id = Guid.NewGuid()
                    ,
                    LastUpdated = null
                    ,
                    IsValid = true
                    ,
                    PreviousImported = false

                };
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.NietBelastbareBestanddelen == null)
            {
                Data.NietBelastbareBestanddelen = new EY.com.eBook.RuleEngine.AY2015.AangifteVenBAppTypes.NietBelastbareBestandelenItem
                {
                    Id = Guid.NewGuid()
                    ,
                    LastUpdated = null
                    ,
                    IsValid = true
                    ,
                    PreviousImported = false

                };
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.DBI == null)
            {
                Data.DBI = new EY.com.eBook.RuleEngine.AY2015.AangifteVenBAppTypes.DbiEntity
                {
                    Id = Guid.NewGuid()
                    ,
                    LastUpdated = null
                    ,
                    IsValid = true
                    ,
                    PreviousImported = false

                };
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.OverdrachtDBIAftrek == null)
            {
                Data.OverdrachtDBIAftrek = new EY.com.eBook.RuleEngine.AY2015.AangifteVenBAppTypes.OverdrachtDBIAftrekEntity
                {
                    Id = Guid.NewGuid()
                    ,
                    LastUpdated = null
                    ,
                    IsValid = true
                    ,
                    PreviousImported = false

                };
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.OverdrachtNID == null)
            {
                Data.OverdrachtNID = new EY.com.eBook.RuleEngine.AY2015.AangifteVenBAppTypes.OverdrachtNIDEntity
                {
                    Id = Guid.NewGuid()
                    ,
                    LastUpdated = null
                    ,
                    IsValid = true
                    ,
                    PreviousImported = false

                };
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.CompenseerbareVerliezen == null)
            {
                Data.CompenseerbareVerliezen = new EY.com.eBook.RuleEngine.AY2015.AangifteVenBAppTypes.CompenseerbareVerliezenEntity
                {
                    Id = Guid.NewGuid()
                    ,
                    LastUpdated = null
                    ,
                    IsValid = true
                    ,
                    PreviousImported = false

                };
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.TariefBelasting == null)
            {
                Data.TariefBelasting = new EY.com.eBook.RuleEngine.AY2015.AangifteVenBAppTypes.TariefEntity
                {
                    Id = Guid.NewGuid()
                    ,
                    LastUpdated = null
                    ,
                    IsValid = true
                    ,
                    PreviousImported = false

                };
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.VoorafBetalingen == null)
            {
                Data.VoorafBetalingen = new EY.com.eBook.RuleEngine.AY2015.AangifteVenBAppTypes.VoorafBetalingEntity
                {
                    Id = Guid.NewGuid()
                    ,
                    LastUpdated = null
                    ,
                    IsValid = true
                    ,
                    PreviousImported = false

                };
                Data.LastUpdated = DateTime.Now;
            }

            //if (Data.VerrekenbareVoorheffing == null)
            //{
            //    Data.VerrekenbareVoorheffing = new EY.com.eBook.RuleEngine.AY2015.AangifteVenBAppTypes.VerrekenbareVoorheffingEntity
            //    {
            //        Id = Guid.NewGuid()
            //        ,
            //        LastUpdated = null
            //        ,
            //        IsValid = true
            //        ,
            //        PreviousImported = false

            //    };
            //    Data.LastUpdated = DateTime.Now;
            //}

            if (Data.TaxShelter == null)
            {
                Data.TaxShelter = new EY.com.eBook.RuleEngine.AY2015.AangifteVenBAppTypes.TaxShelterEntity
                {
                    Id = Guid.NewGuid()
                    ,
                    LastUpdated = null
                    ,
                    IsValid = true
                    ,
                    PreviousImported = false

                };
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.GrootteVennootschap == null)
            {
                Data.GrootteVennootschap = new EY.com.eBook.RuleEngine.AY2015.AangifteVenBAppTypes.GrootteVennootschapeEntity
                {
                    Id = Guid.NewGuid()
                    ,
                    LastUpdated = null
                    ,
                    IsValid = true
                    ,
                    PreviousImported = false

                };
                Data.LastUpdated = DateTime.Now;
            }

        }
        
        public string ToXml() 
        {
            return CoreHelper.SerializeToString(Data);
        }

        public void LoadXml(string xml)
        {
            if (!string.IsNullOrEmpty(xml)) {
                Data = CoreHelper.DeserializeFromString<EY.com.eBook.RuleEngine.AY2015.AangifteVenBAppTypes.AangifteVenB>(xml);
            }
            SetMinimal();
            
        }
        
        public EY.com.eBook.RuleEngine.AY2015.AangifteVenBAppTypes.AangifteVenB Data { get; set; }

        public void ForceRecalculation(ref WorksheetSetData worksheetset)
        {
            Data.LastUpdated = null;
            Calculate(ref worksheetset);
        }


        public void Calculate(ref WorksheetSetData worksheetset)
        {
            worksheetset.ClearMessages(TypeId);
            if (!Data.LastUpdated.HasValue) Data.LastUpdated = new DateTime(1980,1,1);
            Data.LastUpdated = Data.LastUpdated.Value.AddMinutes(-1);
            //if (!worksheetset.BelasteReservesApp.Data.LastUpdated.HasValue || worksheetset.BelasteReservesApp.Data.LastUpdated > Data.LastUpdated)
            //{
                ImportBelasteReserves(ref worksheetset);
            //}
            //if (!worksheetset.BelastingVrijeReservesApp.Data.LastUpdated.HasValue || worksheetset.BelastingVrijeReservesApp.Data.LastUpdated > Data.LastUpdated)
            //{
                ImportBelastingvrijeReserves(ref worksheetset);
            //}
            //if (!worksheetset.DBIApp.Data.LastUpdated.HasValue || worksheetset.DBIApp.Data.LastUpdated > Data.LastUpdated)
            //{
                ImportDBI(ref worksheetset);
            //}
            if (!worksheetset.BerekeningVenBApp.Data.LastUpdated.HasValue || worksheetset.BerekeningVenBApp.Data.LastUpdated > Data.LastUpdated)
            {
                ImportBerekeningVenB(ref worksheetset);
            }
            if (!worksheetset.TransferTaxCreditsApp.Data.LastUpdated.HasValue || worksheetset.TransferTaxCreditsApp.Data.LastUpdated > Data.LastUpdated)
            {
                ImportTransferTaxCredits(ref worksheetset);
            }

            if (!worksheetset.RisicoKapitaalHistoriekApp.Data.LastUpdated.HasValue || worksheetset.RisicoKapitaalHistoriekApp.Data.LastUpdated > Data.LastUpdated)
            {
                ImportHistoryNID(ref worksheetset);
            }

            Data.GrootteVennootschap.GVCGVoorLaatsteTijdperk= false;
            Data.GrootteVennootschap.GVCGLaatsteTijdperk = false;
            if(worksheetset.Settings!=null) {

                Data.GrootteVennootschap.GVCGVoorLaatsteTijdperk = worksheetset.Settings.BeforePreviousPeriodSmall;
                Data.GrootteVennootschap.GVCGLaatsteTijdperk = worksheetset.Settings.PreviousPeriodSmall;
            }

            //Data.VerworpenUitgaven.VUTotaal = Data.VerworpenUitgaven.VUNietAftrekbareBelasting.GetValueOrDefault() + Data.VerworpenUitgaven.VUGewestBelast.GetValueOrDefault() + Data.VerworpenUitgaven.VUBoetes.GetValueOrDefault() + Data.VerworpenUitgaven.VUPensioenen.GetValueOrDefault() + Data.VerworpenUitgaven.VUAutokosten.GetValueOrDefault() + Data.VerworpenUitgaven.VUReceptie.GetValueOrDefault() + Data.VerworpenUitgaven.VURestaurant.GetValueOrDefault() + Data.VerworpenUitgaven.VUKledij.GetValueOrDefault() + Data.VerworpenUitgaven.VUOverdrevenIntresten.GetValueOrDefault() + Data.VerworpenUitgaven.VUBepaaldeLeningen.GetValueOrDefault() + Data.VerworpenUitgaven.VUAbnormaleGoedgVoordelen.GetValueOrDefault() + Data.VerworpenUitgaven.VUSocialeVoordelen.GetValueOrDefault() + Data.VerworpenUitgaven.VULiberaliteiten.GetValueOrDefault() + Data.VerworpenUitgaven.VUAandelen.GetValueOrDefault() + Data.VerworpenUitgaven.VUTerugnemingVrijstellingen.GetValueOrDefault() + Data.VerworpenUitgaven.VUWerknemerParticipatie.GetValueOrDefault() + Data.VerworpenUitgaven.VUCoupon.GetValueOrDefault() + Data.VerworpenUitgaven.VUTaxShelter.GetValueOrDefault() + Data.VerworpenUitgaven.VUGewestPremies.GetValueOrDefault() + Data.VerworpenUitgaven.VUBepaaldeStaten.GetValueOrDefault() + Data.VerworpenUitgaven.VUAndere.GetValueOrDefault() + Data.VerworpenUitgaven.VUMaaltijdSportCultuurEco.GetValueOrDefault() + Data.VerworpenUitgaven.VUAutokostenVAA.GetValueOrDefault();
            Data.UitgekeerdeDividenden.UDTotaal = Data.UitgekeerdeDividenden.UDGewone.GetValueOrDefault() + Data.UitgekeerdeDividenden.UDAandelen.GetValueOrDefault() + Data.UitgekeerdeDividenden.UDMaatschappelijk.GetValueOrDefault() + Data.UitgekeerdeDividenden.UDOverlijden.GetValueOrDefault();
            Data.UiteenzettingWinst.WIBelastGereservWinst = Data.Reserves.ATotaalET.GetValueOrDefault();
            Data.UiteenzettingWinst.WIVerworpenUitgaven = Data.VerworpenUitgaven.VUTotaal.GetValueOrDefault();
            Data.UiteenzettingWinst.WIUitgekeerdeDividenden = Data.UitgekeerdeDividenden.UDTotaal.GetValueOrDefault();

            Data.UiteenzettingWinst.WIZeeschip = Data.UiteenzettingWinst.WIZeeschip.GetValueOrDefault();
            Data.UiteenzettingWinst.WIBGKapitaalInterestLandbouw = Data.UiteenzettingWinst.WIBGKapitaalInterestLandbouw.GetValueOrDefault();
            Data.UiteenzettingWinst.WIResultaat = (Data.UiteenzettingWinst.WIBelastGereservWinst.GetValueOrDefault() + Data.UiteenzettingWinst.WIVerworpenUitgaven.GetValueOrDefault()) + Data.UiteenzettingWinst.WIUitgekeerdeDividenden.GetValueOrDefault();

            Data.UiteenzettingWinst.WIActiviteiten = Data.UiteenzettingWinst.WIResultaat.GetValueOrDefault() - (Data.UiteenzettingWinst.WIZeeschip.GetValueOrDefault());

            Data.UiteenzettingWinst.WIBABAutoVaa = Data.VerworpenUitgaven.VUAutokostenVAA.GetValueOrDefault();

            Data.UiteenzettingWinst.WIBestanddelenAftrekBepTotaal = ((Data.UiteenzettingWinst.WIBABAutoVaa.GetValueOrDefault() + Data.UiteenzettingWinst.WIBABAbnormaalGoedg.GetValueOrDefault() + Data.UiteenzettingWinst.WIBABInvestVerplicht.GetValueOrDefault()) + Data.UiteenzettingWinst.WIBABKapitaal.GetValueOrDefault()) + Data.UiteenzettingWinst.WIBABWerknemersPart.GetValueOrDefault();

            Data.UiteenzettingWinst.WIBResterendResultaat = Data.UiteenzettingWinst.WIActiviteiten.GetValueOrDefault() - Data.UiteenzettingWinst.WIBestanddelenAftrekBepTotaal.GetValueOrDefault();

            Data.UiteenzettingWinst.WVVOResterendResultaatBelg = Data.UiteenzettingWinst.WVVOResterendResultaatBelg.GetValueOrDefault();

            Data.UiteenzettingWinst.WARWAftrekOctrooiBelg = 0;
            Data.UiteenzettingWinst.WARWAftrekRisicoBelg = 0;
            Data.UiteenzettingWinst.WARWAftrekRisicoHistoriekBelg = 0;
            Data.UiteenzettingWinst.WARWDefinitiefBelasteInkomstenBelg = 0;
            Data.UiteenzettingWinst.WARWInvesteringsAftrekBelg = 0;
            Data.UiteenzettingWinst.WARWNietBelastbareBestandBelg = 0;
            Data.UiteenzettingWinst.WARWVorigeVerliezenBelg = 0;

            Data.UiteenzettingWinst.WARWAftrekOctrooiNVV = 0;
            Data.UiteenzettingWinst.WARWAftrekRisicoNVV = 0;
            Data.UiteenzettingWinst.WARWAftrekRisicoHistoriekNVV = 0;
            Data.UiteenzettingWinst.WARWDefinitiefBelasteInkomstenNVV = 0;
            Data.UiteenzettingWinst.WARWNietBelastbareBestandNVV = 0;
            Data.UiteenzettingWinst.WARWVorigeVerliezenNVV= 0;
            Data.UiteenzettingWinst.WARWTotaalNVV = 0;


            decimal remaining = Data.UiteenzettingWinst.WVVOResterendResultaatBelg.GetValueOrDefault();
            decimal remainingNbb = Data.NietBelastbareBestanddelen.TotalNonTaxableElements.GetValueOrDefault();
            decimal remainingDbi = Data.UiteenzettingWinst.CalcDivideDBI.GetValueOrDefault();
            decimal remainingOctrooi = Data.UiteenzettingWinst.CalcDivideOctrooi.GetValueOrDefault();
            decimal remainingNid = Data.UiteenzettingWinst.CalcDivideNID.GetValueOrDefault();
            decimal remainingNidHistoriek = Data.UiteenzettingWinst.CalcDivideNIDHistoriek.GetValueOrDefault();
            decimal remainingVerliezen = Data.UiteenzettingWinst.CalcDivideVerliezen.GetValueOrDefault();


            if (Data.UiteenzettingWinst.WVVOResterendResultaatBelg.GetValueOrDefault() > 0)
            {
                
                if (remaining > 0 && remainingNbb > 0)
                {
                    if (remaining > remainingNbb)
                    {
                        Data.UiteenzettingWinst.WARWNietBelastbareBestandBelg = remainingNbb;
                        remaining -= remainingNbb;
                        remainingNbb = 0;
                    }
                    else
                    {
                        Data.UiteenzettingWinst.WARWNietBelastbareBestandBelg = remaining;
                        remainingNbb -= remaining;
                        remaining = 0;
                    }
                }

                if (remaining > 0 && remainingDbi > 0)
                {
                    if (remaining > remainingDbi)
                    {
                        Data.UiteenzettingWinst.WARWDefinitiefBelasteInkomstenBelg = remainingDbi;
                        remaining -= remainingDbi;
                        remainingDbi = 0;
                    }
                    else
                    {
                        Data.UiteenzettingWinst.WARWDefinitiefBelasteInkomstenBelg = remaining;
                        remainingDbi -= remaining;
                        remaining = 0;
                    }
                }

                if (remaining > 0 && remainingOctrooi > 0)
                {
                    if (remaining > remainingOctrooi)
                    {
                        Data.UiteenzettingWinst.WARWAftrekOctrooiBelg = remainingOctrooi;
                        remaining -= remainingOctrooi;
                        remainingOctrooi = 0;
                    }
                    else
                    {
                        Data.UiteenzettingWinst.WARWAftrekOctrooiBelg = remaining;
                        remainingOctrooi -= remaining;
                        remaining = 0;
                    }
                }

                if (remaining > 0 && remainingNid > 0)
                {
                    if (remaining > remainingNid)
                    {
                        Data.UiteenzettingWinst.WARWAftrekRisicoBelg = remainingNid;
                        remaining -= remainingNid;
                        remainingNid = 0;
                        
                    }
                    else
                    {
                        Data.UiteenzettingWinst.WARWAftrekRisicoBelg = remaining;
                        remainingNid -= remaining;
                        remaining = 0;
                    }
                }

               

                if (remaining > 0 && remainingVerliezen > 0)
                {
                    if (remaining > remainingVerliezen)
                    {
                        Data.UiteenzettingWinst.WARWVorigeVerliezenBelg = remainingVerliezen;
                        remaining -= remainingVerliezen;
                        remainingVerliezen = 0;
                    }
                    else
                    {
                        Data.UiteenzettingWinst.WARWVorigeVerliezenBelg = remaining;
                        remainingVerliezen -= remaining;
                        remaining = 0;
                    }
                }

                if (remaining > 0 && Data.UiteenzettingWinst.CalcDivideInvestering.GetValueOrDefault() > 0)
                {
                    if (remaining > Data.UiteenzettingWinst.CalcDivideInvestering.GetValueOrDefault())
                    {
                        Data.UiteenzettingWinst.WARWInvesteringsAftrekBelg = Math.Round(Data.UiteenzettingWinst.CalcDivideInvestering.GetValueOrDefault(), 2, MidpointRounding.AwayFromZero);
                        remaining -= Data.UiteenzettingWinst.CalcDivideInvestering.GetValueOrDefault();
                    }
                    else
                    {
                        Data.UiteenzettingWinst.WARWInvesteringsAftrekBelg = remaining;
                        remaining = 0;
                    }
                }

                if (remaining > 0 && remainingNidHistoriek > 0)
                {
                    if (remaining > remainingNidHistoriek)
                    {
                        Data.UiteenzettingWinst.WARWAftrekRisicoHistoriekBelg = remainingNidHistoriek;
                        remaining -= remainingNidHistoriek;
                        remainingNidHistoriek = 0;

                    }
                    else
                    {
                        Data.UiteenzettingWinst.WARWAftrekRisicoHistoriekBelg = remaining;
                        remainingNidHistoriek -= remaining;
                        remaining = 0;
                    }
                }

            }
            Data.UiteenzettingWinst.WARWTotaalBelg = remaining;
            decimal remainingNVV = Data.UiteenzettingWinst.WVVOResterendResultaatNVV.GetValueOrDefault();
            if (Data.UiteenzettingWinst.WVVOResterendResultaatNVV > 0)
            {
                
                if (remainingNVV > 0 && remainingNbb > 0)
                {
                    if (remainingNbb > remainingNVV)
                    {
                        Data.UiteenzettingWinst.WARWNietBelastbareBestandNVV = remainingNVV;
                        remainingNVV=0;
                    } else {
                        Data.UiteenzettingWinst.WARWNietBelastbareBestandNVV = remainingNbb;
                        remainingNVV = remainingNVV - remainingNbb;
                    }
                }


                if (remainingNVV > 0 && remainingDbi > 0)
                {
                    if (remainingDbi > remainingNVV)
                    {
                        Data.UiteenzettingWinst.WARWDefinitiefBelasteInkomstenNVV= remainingNVV;
                        remainingNVV = 0;
                    }
                    else
                    {
                        Data.UiteenzettingWinst.WARWDefinitiefBelasteInkomstenNVV = remainingDbi;
                        remainingNVV = remainingNVV - remainingDbi;
                    }
                }

                if (remainingNVV > 0 && remainingOctrooi > 0)
                {
                    if (remainingOctrooi > remainingNVV)
                    {
                        Data.UiteenzettingWinst.WARWAftrekOctrooiNVV = remainingNVV;
                        remainingNVV = 0;
                    }
                    else
                    {
                        Data.UiteenzettingWinst.WARWAftrekOctrooiNVV = remainingOctrooi;
                        remainingNVV = remainingNVV - remainingOctrooi;
                    }
                }

                if (remainingNVV > 0 && remainingNid > 0)
                {
                    if (remainingNid > remainingNVV)
                    {
                        Data.UiteenzettingWinst.WARWAftrekRisicoNVV = remainingNVV;                        
                        remainingNVV = 0;
                    }
                    else
                    {
                        Data.UiteenzettingWinst.WARWAftrekRisicoNVV= remainingNid;
                        remainingNVV = remainingNVV - remainingNid;
                    }
                }

                if (remainingNVV > 0 && remainingVerliezen > 0)
                {
                    if (remainingVerliezen > remainingNVV)
                    {
                        Data.UiteenzettingWinst.WARWVorigeVerliezenNVV = remainingNVV;
                        remainingNVV = 0;
                    }
                    else
                    {
                        Data.UiteenzettingWinst.WARWVorigeVerliezenNVV = remainingVerliezen;
                        remainingNVV = remainingNVV - remainingVerliezen;
                    }
                }


                if (remainingNVV > 0 && remainingNidHistoriek > 0)
                {
                    if (remainingNidHistoriek > remainingNVV)
                    {
                        Data.UiteenzettingWinst.WARWAftrekRisicoHistoriekNVV = remainingNVV;
                        remainingNVV = 0;
                    }
                    else
                    {
                        Data.UiteenzettingWinst.WARWAftrekRisicoHistoriekNVV = remainingNidHistoriek;
                        remainingNVV = remainingNVV - remainingNidHistoriek;
                    }
                }
                
            }

            Data.UiteenzettingWinst.WARWTotaalNVV = remainingNVV;

            if (Data.UiteenzettingWinst.WARWTotaalBelg.GetValueOrDefault() < 0) Data.UiteenzettingWinst.WARWTotaalBelg = 0;
            if (Data.UiteenzettingWinst.WARWTotaalNVV.GetValueOrDefault() < 0) Data.UiteenzettingWinst.WARWTotaalNVV = 0;
            
            Data.UiteenzettingWinst.WIBGZeescheepvaarTonnage = Data.UiteenzettingWinst.WIBGZeescheepvaarTonnage.GetValueOrDefault();
            Data.UiteenzettingWinst.WIBGAbnormaleGoedgunstig = Data.UiteenzettingWinst.WIBABAbnormaalGoedg.GetValueOrDefault();
            Data.UiteenzettingWinst.WIBGInvesteringsverplichting = Data.UiteenzettingWinst.WIBABInvestVerplicht.GetValueOrDefault();
            Data.UiteenzettingWinst.WIBGWerknemersParticipatie = Data.UiteenzettingWinst.WIBABWerknemersPart.GetValueOrDefault();
            /*
             * Ticket#19 situatie 4 */
            Data.UiteenzettingWinst.WIBGResterendWinst = (Data.UiteenzettingWinst.WARWTotaalBelg.GetValueOrDefault() + Data.UiteenzettingWinst.WARWTotaalNVV.GetValueOrDefault()) - Data.UiteenzettingWinst.WIBGMeerwaardeAandelenBelast.GetValueOrDefault();
            /**/
            Data.UiteenzettingWinst.WIBGAutoVaa = Data.UiteenzettingWinst.WIBABAutoVaa.GetValueOrDefault();
            
            if (Data.UiteenzettingWinst.WIBGResterendWinst.GetValueOrDefault() < 0) Data.UiteenzettingWinst.WIBGResterendWinst = 0;

            Data.UiteenzettingWinst.WIBGGewoonTarief = (((Data.UiteenzettingWinst.WIBGZeescheepvaarTonnage.GetValueOrDefault() + Data.UiteenzettingWinst.WIBGAbnormaleGoedgunstig.GetValueOrDefault()) + Data.UiteenzettingWinst.WIBGInvesteringsverplichting.GetValueOrDefault()) + Data.UiteenzettingWinst.WIBGWerknemersParticipatie.GetValueOrDefault()) + Data.UiteenzettingWinst.WIBGResterendWinst.GetValueOrDefault() + Data.UiteenzettingWinst.WIBGAutoVaa.GetValueOrDefault();
            Data.UiteenzettingWinst.WIBGKapitaalInterestLandbouw = Data.UiteenzettingWinst.WIBABKapitaal.GetValueOrDefault();

            if (Data.UiteenzettingWinst.WIBGGewoonTarief.GetValueOrDefault() < 0)
            {
                Data.UiteenzettingWinst.WIBGGewoonTarief = 0;
            }

            Data.UiteenzettingWinst.WIBGExitTarief = Data.UiteenzettingWinst.WIBGExitTarief.GetValueOrDefault();
            Data.UiteenzettingWinst.WIBGMeerwaardeAandelenBelast = Data.UiteenzettingWinst.WIBGMeerwaardeAandelenBelast.GetValueOrDefault();

            /*
             * Ticket#19 4 */
            Data.UiteenzettingWinst.WIBGGewoonTarief = Data.UiteenzettingWinst.WIBGGewoonTarief.GetValueOrDefault() - (Data.UiteenzettingWinst.WIBGExitTarief.GetValueOrDefault() /*+ Data.UiteenzettingWinst.WIBGMeerwaardeAandelenBelast.GetValueOrDefault()*/);


            /* TIJDELIJK IN COMMENT */
            
            Data.UiteenzettingWinst.WIBGKapitaalInterestLandbouw = worksheetset.BerekeningVenBApp.Data.Aanslag.KapitaalEnInterestsubsidies.GetValueOrDefault();

            Data.AfzonderlijkeAanslagen.AAVoordelenAlleAard = worksheetset.BerekeningVenBApp.Data.Aanslag.NietVerantwoordeKosten.GetValueOrDefault();
            Data.AfzonderlijkeAanslagen.AAVoordelenAlleAard50 = worksheetset.BerekeningVenBApp.Data.Aanslag.AfzonderlijkeNietVerantwoordeKosten50Procent.GetValueOrDefault();
            Data.AfzonderlijkeAanslagen.AAVoordelenAlleAard100 = worksheetset.BerekeningVenBApp.Data.Aanslag.AfzonderlijkeNietVerantwoordeKosten100Procent.GetValueOrDefault();
            Data.AfzonderlijkeAanslagen.AABelasteReserves34 = worksheetset.BerekeningVenBApp.Data.Aanslag.AfzonderlijkeAanslag34.GetValueOrDefault();
            Data.AfzonderlijkeAanslagen.AABelasteReserves28 = worksheetset.BerekeningVenBApp.Data.Aanslag.AfzonderlijkeAanslag28.GetValueOrDefault();
            Data.AfzonderlijkeAanslagen.AAUitgekeerdeDividenden = worksheetset.BerekeningVenBApp.Data.Aanslag.AfzonderlijkeAanslagDividenden.GetValueOrDefault();
            Data.AfzonderlijkeAanslagen.AABAMaatschVermogen33 = worksheetset.BerekeningVenBApp.Data.Aanslag.VerdelingVermogen33.GetValueOrDefault();
            Data.AfzonderlijkeAanslagen.AABAMaatschVermogen165 = worksheetset.BerekeningVenBApp.Data.Aanslag.VerdelingVermogen16.GetValueOrDefault();
            Data.AfzonderlijkeAanslagen.AABAVoordelenAlleAard = worksheetset.BerekeningVenBApp.Data.Aanslag.VAAVennootschappen.GetValueOrDefault();
            Data.Diamant.DIAanvullingErkend = worksheetset.BerekeningVenBApp.Data.Aanslag.AanvullendeHeffingDiamant.GetValueOrDefault();
            Data.Diamant.DIOnderzoekOntwikkeling = worksheetset.BerekeningVenBApp.Data.Aanslag.TerugbetalingBelastingskrediet.GetValueOrDefault();
            Data.Diamant.DIVoorzieningenVoor2013 = worksheetset.BerekeningVenBApp.Data.Aanslag.TotaalBedragVoorzieningen.GetValueOrDefault();
            Data.Diamant.DIVoorzieningenSpreiding = worksheetset.BerekeningVenBApp.Data.Aanslag.AfzonderlijkeAanslagGespreid;
             


            /*
            Data.AfzonderlijkeAanslagen.AABAMaatschVermogen165 = Data.AfzonderlijkeAanslagen.AABAMaatschVermogen165.GetValueOrDefault();
            Data.AfzonderlijkeAanslagen.AABAMaatschVermogen33 = Data.AfzonderlijkeAanslagen.AABAMaatschVermogen33.GetValueOrDefault();
            Data.AfzonderlijkeAanslagen.AABAVoordelenAlleAard = Data.AfzonderlijkeAanslagen.AABAVoordelenAlleAard.GetValueOrDefault();
            Data.AfzonderlijkeAanslagen.AABelasteReserves = Data.AfzonderlijkeAanslagen.AABelasteReserves.GetValueOrDefault();
            Data.AfzonderlijkeAanslagen.AAUitgekeerdeDividenden = Data.AfzonderlijkeAanslagen.AAUitgekeerdeDividenden.GetValueOrDefault();
            Data.AfzonderlijkeAanslagen.AAVoordelenAlleAard = Data.AfzonderlijkeAanslagen.AAVoordelenAlleAard.GetValueOrDefault();
            */



            Data.Diamant.DIAanvullingErkend = Data.Diamant.DIAanvullingErkend.GetValueOrDefault();
            Data.Diamant.DIOnderzoekOntwikkeling = Data.Diamant.DIOnderzoekOntwikkeling.GetValueOrDefault();

            

            Data.CompenseerbareVerliezen.CVOverTeBrengen = (Data.CompenseerbareVerliezen.CVSaldoVorigeVerliezen.GetValueOrDefault() + Data.CompenseerbareVerliezen.CVGecompenseerdeVerliezen.GetValueOrDefault()) + Data.CompenseerbareVerliezen.CVVerliesBelastbaarTijdperk.GetValueOrDefault();
            Data.VoorafBetalingen.VBTotaal = ((Data.VoorafBetalingen.VBEersteKwartaal.GetValueOrDefault() + Data.VoorafBetalingen.VBTweedeKwartaal.GetValueOrDefault()) + Data.VoorafBetalingen.VBDerdeKwartaal.GetValueOrDefault()) + Data.VoorafBetalingen.VBVierdeKwartaal.GetValueOrDefault();
            /*
            Data.VerrekenbareVoorheffing.VVNTBelastingKredietOnderzoekOntw = Data.VerrekenbareVoorheffing.VVNTBelastingKredietOnderzoekOntw.GetValueOrDefault();
            Data.VerrekenbareVoorheffing.VVNTFictieveRoerendeVoorheffing = Data.VerrekenbareVoorheffing.VVNTFictieveRoerendeVoorheffing.GetValueOrDefault();
            Data.VerrekenbareVoorheffing.VVNTForfaitairBuitenlands = Data.VerrekenbareVoorheffing.VVNTForfaitairBuitenlands.GetValueOrDefault();
            Data.VerrekenbareVoorheffing.VVNTBelastingKredietOnderzoekOntw = Data.VerrekenbareVoorheffing.VVNTBelastingKredietOnderzoekOntw.GetValueOrDefault();


            Data.VerrekenbareVoorheffing.VVNTTotaal = (Data.VerrekenbareVoorheffing.VVNTBelastingKredietOnderzoekOntw.GetValueOrDefault() + Data.VerrekenbareVoorheffing.VVNTFictieveRoerendeVoorheffing.GetValueOrDefault()) + Data.VerrekenbareVoorheffing.VVNTForfaitairBuitenlands.GetValueOrDefault();
            Data.VerrekenbareVoorheffing.VVTAndere = Math.Abs(Data.VerrekenbareVoorheffing.VVTAndere.GetValueOrDefault());
            Data.VerrekenbareVoorheffing.VVTAndereBuitenlands = Math.Abs(Data.VerrekenbareVoorheffing.VVTAndereBuitenlands.GetValueOrDefault());
            Data.VerrekenbareVoorheffing.VVTAndereEigen = Math.Abs(Data.VerrekenbareVoorheffing.VVTAndereEigen.GetValueOrDefault());
            Data.VerrekenbareVoorheffing.VVTAndereRoerendeVoorheffing = Math.Abs((Data.VerrekenbareVoorheffing.VVTAndereRoerendeVoorheffing.GetValueOrDefault()));
            Data.VerrekenbareVoorheffing.VVTBelgisch = Math.Abs((Data.VerrekenbareVoorheffing.VVTBelgisch.GetValueOrDefault()));
            Data.VerrekenbareVoorheffing.VVTEigenBuitenlands = Math.Abs((Data.VerrekenbareVoorheffing.VVTEigenBuitenlands.GetValueOrDefault()));
            Data.VerrekenbareVoorheffing.VBelastingkredietHuiidTijdperk = Data.VerrekenbareVoorheffing.VBelastingkredietHuiidTijdperk.GetValueOrDefault();
            Data.VerrekenbareVoorheffing.VVTTotaal = ((((Data.VerrekenbareVoorheffing.VVTAndere.GetValueOrDefault() + Data.VerrekenbareVoorheffing.VVTBelgisch.GetValueOrDefault()) + Data.VerrekenbareVoorheffing.VVTAndereBuitenlands.GetValueOrDefault()) + Data.VerrekenbareVoorheffing.VVTAndereEigen.GetValueOrDefault()) + Data.VerrekenbareVoorheffing.VVTAndereRoerendeVoorheffing.GetValueOrDefault()) + Data.VerrekenbareVoorheffing.VVTEigenBuitenlands.GetValueOrDefault();

            if (Data.VerrekenbareVoorheffing.VVTTotaal.GetValueOrDefault() != Data.VerrekenbareVoorheffing.VVTTotaalExpected.GetValueOrDefault())
            {
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Warning, "UNEQUAL_TAXCALCULATION", "VerrekenbareVoorheffing", Data.VerrekenbareVoorheffing.Id, "VVTTotaal", new object[] { Data.VerrekenbareVoorheffing.VVTTotaalExpected.GetValueOrDefault() });
            }*/

            Data.GrootteVennootschap.GVBTBalansTotaal = Data.GrootteVennootschap.GVBTBalansTotaal.GetValueOrDefault();
            Data.GrootteVennootschap.GVBTJaargemiddelde = Data.GrootteVennootschap.GVBTJaargemiddelde.GetValueOrDefault();
            Data.GrootteVennootschap.GVBTJaarOmzet = Data.GrootteVennootschap.GVBTJaarOmzet.GetValueOrDefault();

            Data.LastUpdated = DateTime.Now;
        }

        private void ImportBelasteReserves(ref WorksheetSetData worksheetset)
        {
            Data.Reserves.AKapitaalBT = worksheetset.BelasteReservesApp.Data.AllBelasteReserves.Where(e => e.TypeReseve != null && e.TypeReseve.Id.ToString().ToLower() == "bbc12ef9-3e15-4c70-92d8-11f7388ed64a").Sum(e => e.BeginToestand.GetValueOrDefault());
            if (!Data.Reserves.AKapitaalBT.HasValue) Data.Reserves.AKapitaalBT = 0;
            Data.Reserves.AKapitaalET = worksheetset.BelasteReservesApp.Data.AllBelasteReserves.Where(e => e.TypeReseve != null && e.TypeReseve.Id.ToString().ToLower() == "bbc12ef9-3e15-4c70-92d8-11f7388ed64a").Sum(e => e.EindToestand.GetValueOrDefault());
            if (!Data.Reserves.AKapitaalET.HasValue) Data.Reserves.AKapitaalET = 0;

            Data.Reserves.AHerwaarderingBT = worksheetset.BelasteReservesApp.Data.AllBelasteReserves.Where(e => e.TypeReseve != null && e.TypeReseve.Id.ToString().ToLower() == "8ca508c3-b59f-4106-9afa-30b1acda92b0").Sum(e => e.BeginToestand.GetValueOrDefault());
            if (!Data.Reserves.AHerwaarderingBT.HasValue) Data.Reserves.AHerwaarderingBT = 0;
            Data.Reserves.AHerwaarderingET = worksheetset.BelasteReservesApp.Data.AllBelasteReserves.Where(e => e.TypeReseve != null && e.TypeReseve.Id.ToString().ToLower() == "8ca508c3-b59f-4106-9afa-30b1acda92b0").Sum(e => e.EindToestand.GetValueOrDefault());
            if (!Data.Reserves.AHerwaarderingET.HasValue) Data.Reserves.AHerwaarderingET = 0;

            Data.Reserves.AWetReserveBT = worksheetset.BelasteReservesApp.Data.AllBelasteReserves.Where(e => e.TypeReseve != null && e.TypeReseve.Id.ToString().ToLower() == "85ef3af8-230f-4c81-b10d-4ff87534492c").Sum(e => e.BeginToestand.GetValueOrDefault());
            if (!Data.Reserves.AWetReserveBT.HasValue) Data.Reserves.AWetReserveBT = 0;
            Data.Reserves.AWetReserveET = worksheetset.BelasteReservesApp.Data.AllBelasteReserves.Where(e => e.TypeReseve != null && e.TypeReseve.Id.ToString().ToLower() == "85ef3af8-230f-4c81-b10d-4ff87534492c").Sum(e => e.EindToestand.GetValueOrDefault());
            if (!Data.Reserves.AWetReserveET.HasValue) Data.Reserves.AWetReserveET = 0;

            Data.Reserves.AOnbeschikReserveBT = worksheetset.BelasteReservesApp.Data.AllBelasteReserves.Where(e => e.TypeReseve != null && e.TypeReseve.Id.ToString().ToLower() == "04f61c72-243d-4673-80b2-5e8dfd576f8d").Sum(e => e.BeginToestand.GetValueOrDefault());
            if (!Data.Reserves.AOnbeschikReserveBT.HasValue) Data.Reserves.AOnbeschikReserveBT = 0;
            Data.Reserves.AOnbeschikReserveET = worksheetset.BelasteReservesApp.Data.AllBelasteReserves.Where(e => e.TypeReseve != null && e.TypeReseve.Id.ToString().ToLower() == "04f61c72-243d-4673-80b2-5e8dfd576f8d").Sum(e => e.EindToestand.GetValueOrDefault());
            if (!Data.Reserves.AOnbeschikReserveET.HasValue) Data.Reserves.AOnbeschikReserveET = 0;

            Data.Reserves.ABeschikReserveBT = worksheetset.BelasteReservesApp.Data.AllBelasteReserves.Where(e => e.TypeReseve != null && e.TypeReseve.Id.ToString().ToLower() == "43d09724-06db-4985-8f7c-aa03f6a4736e").Sum(e => e.BeginToestand.GetValueOrDefault());
            if (!Data.Reserves.ABeschikReserveBT.HasValue) Data.Reserves.ABeschikReserveBT = 0;
            Data.Reserves.ABeschikReserveET = worksheetset.BelasteReservesApp.Data.AllBelasteReserves.Where(e => e.TypeReseve != null && e.TypeReseve.Id.ToString().ToLower() == "43d09724-06db-4985-8f7c-aa03f6a4736e").Sum(e => e.EindToestand.GetValueOrDefault());
            if (!Data.Reserves.ABeschikReserveET.HasValue) Data.Reserves.ABeschikReserveET = 0;

            Data.Reserves.AOvergedragenResWinstVerliesBT = worksheetset.BelasteReservesApp.Data.AllBelasteReserves.Where(e => e.TypeReseve != null && e.TypeReseve.Id.ToString().ToLower() == "b9048cb3-37f2-42b3-9fcd-e8d1b7277bd2").Sum(e => e.BeginToestand.GetValueOrDefault());
            if (!Data.Reserves.AOvergedragenResWinstVerliesBT.HasValue) Data.Reserves.AOvergedragenResWinstVerliesBT = 0;
            Data.Reserves.AOvergedragenResWinstVerliesET = worksheetset.BelasteReservesApp.Data.AllBelasteReserves.Where(e => e.TypeReseve != null && e.TypeReseve.Id.ToString().ToLower() == "b9048cb3-37f2-42b3-9fcd-e8d1b7277bd2").Sum(e => e.EindToestand.GetValueOrDefault());
            if (!Data.Reserves.AOvergedragenResWinstVerliesET.HasValue) Data.Reserves.AOvergedragenResWinstVerliesET = 0;

            Data.Reserves.ABelastbareVoorzieningenBT = worksheetset.BelasteReservesApp.Data.AllBelasteReserves.Where(e => e.TypeReseve != null && e.TypeReseve.Id.ToString().ToLower() == "0f5ff0f1-df29-4342-9e8c-1630304da12f").Sum(e => e.BeginToestand.GetValueOrDefault());
            if (!Data.Reserves.ABelastbareVoorzieningenBT.HasValue) Data.Reserves.ABelastbareVoorzieningenBT = 0;
            Data.Reserves.ABelastbareVoorzieningenET = worksheetset.BelasteReservesApp.Data.AllBelasteReserves.Where(e => e.TypeReseve != null && e.TypeReseve.Id.ToString().ToLower() == "0f5ff0f1-df29-4342-9e8c-1630304da12f").Sum(e => e.EindToestand.GetValueOrDefault());
            if (!Data.Reserves.ABelastbareVoorzieningenET.HasValue) Data.Reserves.ABelastbareVoorzieningenET = 0;

            Data.Reserves.AIBalansBT = worksheetset.BelasteReservesApp.Data.AllBelasteReserves.Where(e => e.TypeReseve != null && e.TypeReseve.Id.ToString().ToLower() == "244403c0-219f-4a3a-94da-c9eb19950b4f").Sum(e => e.BeginToestand.GetValueOrDefault());
            if (!Data.Reserves.AIBalansBT.HasValue) Data.Reserves.AIBalansBT = 0;
            Data.Reserves.AIBalansET = worksheetset.BelasteReservesApp.Data.AllBelasteReserves.Where(e => e.TypeReseve != null && e.TypeReseve.Id.ToString().ToLower() == "244403c0-219f-4a3a-94da-c9eb19950b4f").Sum(e => e.EindToestand.GetValueOrDefault());
            if (!Data.Reserves.AIBalansET.HasValue) Data.Reserves.AIBalansET = 0;

            Data.Reserves.AIBReserveBT = worksheetset.BelasteReservesApp.Data.AllBelasteReserves.Where(e => e.TypeReseve != null && e.TypeReseve.Id.ToString().ToLower() == "ea78dbb2-acd3-4fad-812e-029b66f8fecc").Sum(e => e.BeginToestand.GetValueOrDefault());
            if (!Data.Reserves.AIBReserveBT.HasValue) Data.Reserves.AIBReserveBT = 0;
            Data.Reserves.AIBReserveET = worksheetset.BelasteReservesApp.Data.AllBelasteReserves.Where(e => e.TypeReseve != null && e.TypeReseve.Id.ToString().ToLower() == "ea78dbb2-acd3-4fad-812e-029b66f8fecc").Sum(e => e.EindToestand.GetValueOrDefault());
            if (!Data.Reserves.AIBReserveET.HasValue) Data.Reserves.AIBReserveET = 0;

            Data.Reserves.AORBelastbareWVBT = worksheetset.BelasteReservesApp.Data.AllBelasteReserves.Where(e => e.TypeReseve != null && e.TypeReseve.Id.ToString().ToLower() == "b97e3772-4090-4bad-a623-a3e0c6044855").Sum(e => e.BeginToestand.GetValueOrDefault());
            if (!Data.Reserves.AORBelastbareWVBT.HasValue) Data.Reserves.AORBelastbareWVBT = 0;
            Data.Reserves.AORBelastbareWVET = worksheetset.BelasteReservesApp.Data.AllBelasteReserves.Where(e => e.TypeReseve != null && e.TypeReseve.Id.ToString().ToLower() == "b97e3772-4090-4bad-a623-a3e0c6044855").Sum(e => e.EindToestand.GetValueOrDefault());
            if (!Data.Reserves.AORBelastbareWVET.HasValue) Data.Reserves.AORBelastbareWVET = 0;

            Data.Reserves.AOROverdrevenAfschrBT = worksheetset.BelasteReservesApp.Data.AllBelasteReserves.Where(e => e.TypeReseve != null && e.TypeReseve.Id.ToString().ToLower() == "17e42920-cba9-43c4-90c5-2b74d75daec7").Sum(e => e.BeginToestand.GetValueOrDefault());
            if (!Data.Reserves.AOROverdrevenAfschrBT.HasValue) Data.Reserves.AOROverdrevenAfschrBT = 0;
            Data.Reserves.AOROverdrevenAfschrET = worksheetset.BelasteReservesApp.Data.AllBelasteReserves.Where(e => e.TypeReseve != null && e.TypeReseve.Id.ToString().ToLower() == "17e42920-cba9-43c4-90c5-2b74d75daec7").Sum(e => e.EindToestand.GetValueOrDefault());
            if (!Data.Reserves.AOROverdrevenAfschrET.HasValue) Data.Reserves.AOROverdrevenAfschrET = 0;


            Data.Reserves.AORAndereActBT = worksheetset.BelasteReservesApp.Data.AllBelasteReserves.Where(e => e.TypeReseve != null && e.TypeReseve.Id.ToString().ToLower() == "f67a17d7-8f7d-43d9-9955-b8a9693587a1").Sum(e => e.BeginToestand.GetValueOrDefault());
            if (!Data.Reserves.AORAndereActBT.HasValue) Data.Reserves.AORAndereActBT = 0;
            Data.Reserves.AORAndereActET = worksheetset.BelasteReservesApp.Data.AllBelasteReserves.Where(e => e.TypeReseve != null && e.TypeReseve.Id.ToString().ToLower() == "f67a17d7-8f7d-43d9-9955-b8a9693587a1").Sum(e => e.EindToestand.GetValueOrDefault());
            if (!Data.Reserves.AORAndereActET.HasValue) Data.Reserves.AORAndereActET = 0;

            Data.Reserves.AORAnderePassBT = worksheetset.BelasteReservesApp.Data.AllBelasteReserves.Where(e => e.TypeReseve != null && e.TypeReseve.Id.ToString().ToLower() == "4b49ee2c-90c4-4649-93db-df950d9b90e3").Sum(e => e.BeginToestand.GetValueOrDefault());
            if (!Data.Reserves.AORAnderePassBT.HasValue) Data.Reserves.AORAnderePassBT = 0;
            Data.Reserves.AORAnderePassET = worksheetset.BelasteReservesApp.Data.AllBelasteReserves.Where(e => e.TypeReseve != null && e.TypeReseve.Id.ToString().ToLower() == "4b49ee2c-90c4-4649-93db-df950d9b90e3").Sum(e => e.EindToestand.GetValueOrDefault());
            if (!Data.Reserves.AORAnderePassET.HasValue) Data.Reserves.AORAnderePassET = 0;

            Data.Reserves.AORSubTotaalBT = Data.Reserves.AKapitaalBT.GetValueOrDefault() + Data.Reserves.AHerwaarderingBT.GetValueOrDefault() + Data.Reserves.AWetReserveBT.GetValueOrDefault() + Data.Reserves.AOnbeschikReserveBT.GetValueOrDefault() + Data.Reserves.ABeschikReserveBT.GetValueOrDefault() + Data.Reserves.AOvergedragenResWinstVerliesBT.GetValueOrDefault() + Data.Reserves.ABelastbareVoorzieningenBT.GetValueOrDefault() + Data.Reserves.AIBalansBT + Data.Reserves.AIBReserveBT.GetValueOrDefault() + Data.Reserves.AORBelastbareWVBT.GetValueOrDefault() + Data.Reserves.AOROverdrevenAfschrBT.GetValueOrDefault() + Data.Reserves.AORAndereActBT.GetValueOrDefault() + Data.Reserves.AORAnderePassBT.GetValueOrDefault();
            Data.Reserves.AORSubTotaalET = Data.Reserves.AKapitaalET.GetValueOrDefault() + Data.Reserves.AHerwaarderingET.GetValueOrDefault() + Data.Reserves.AWetReserveET.GetValueOrDefault() + Data.Reserves.AOnbeschikReserveET.GetValueOrDefault() + Data.Reserves.ABeschikReserveET.GetValueOrDefault() + Data.Reserves.AOvergedragenResWinstVerliesET.GetValueOrDefault() + Data.Reserves.ABelastbareVoorzieningenET.GetValueOrDefault() + Data.Reserves.AIBalansET.GetValueOrDefault() + Data.Reserves.AIBReserveET.GetValueOrDefault() + Data.Reserves.AORBelastbareWVET.GetValueOrDefault() + Data.Reserves.AOROverdrevenAfschrET.GetValueOrDefault() + Data.Reserves.AORAndereActET.GetValueOrDefault() + Data.Reserves.AORAnderePassET.GetValueOrDefault();


            // AANPASSINGEN IN DE MIN

            Data.Reserves.AAMRBTRMeerwaardeAandelenBT = worksheetset.BelasteReservesApp.Data.AanpassingBTReserves.Where(e => e.TypeReseve != null && e.TypeReseve.Id.ToString().ToLower() == "f34c7d2b-428d-494d-a29b-f2bd2910a7a9").Sum(e => e.BedragNaPercentage.GetValueOrDefault());
            Data.Reserves.AAMRBTRMeerwaardeAandelenBT = Math.Abs(Data.Reserves.AAMRBTRMeerwaardeAandelenBT.GetValueOrDefault());


            Data.Reserves.AAMRTerugnemingVUAandelen = worksheetset.BelasteReservesApp.Data.AanpassingBTReserves.Where(e => e.TypeReseve != null && e.TypeReseve.Id.ToString().ToLower() == "9deb5faf-a095-420d-8f40-1becdcde72fc").Sum(e => e.BedragNaPercentage.GetValueOrDefault());
            Data.Reserves.AAMRTerugnemingVUAandelen = Math.Abs(Data.Reserves.AAMRTerugnemingVUAandelen.GetValueOrDefault());


            Data.Reserves.AAMRTaxShelterBT = worksheetset.BelasteReservesApp.Data.AanpassingBTReserves.Where(e => e.TypeReseve != null && e.TypeReseve.Id.ToString().ToLower() == "f1875c70-b0dc-41be-8ada-46c702a954c1").Sum(e => e.BedragNaPercentage.GetValueOrDefault());
            Data.Reserves.AAMRTaxShelterBT = Math.Abs(Data.Reserves.AAMRTaxShelterBT.GetValueOrDefault());

            Data.Reserves.AAMRGewestPremiesBT = worksheetset.BelasteReservesApp.Data.AanpassingBTReserves.Where(e => e.TypeReseve != null && e.TypeReseve.Id.ToString().ToLower() == "6a716040-5635-4e9d-8161-f5eaaaff5db2").Sum(e => e.BedragNaPercentage.GetValueOrDefault());
            Data.Reserves.AAMRGewestPremiesBT = Math.Abs(Data.Reserves.AAMRGewestPremiesBT.GetValueOrDefault());

            Data.Reserves.AAMRDefVrijstellingWinst = worksheetset.BelasteReservesApp.Data.AanpassingBTReserves.Where(e => e.TypeReseve != null && e.TypeReseve.Id.ToString().ToLower() == "609700e4-ebba-44b2-8ed3-f47ac30bcd12").Sum(e => e.BedragNaPercentage.GetValueOrDefault());
            Data.Reserves.AAMRDefVrijstellingWinst = Math.Abs(Data.Reserves.AAMRDefVrijstellingWinst.GetValueOrDefault());

            Data.Reserves.AAMRAndereBT = worksheetset.BelasteReservesApp.Data.AanpassingBTReserves.Where(e => e.TypeReseve != null && e.TypeReseve.Id.ToString().ToLower() == "0355b0d0-46be-46d7-a59e-6858c6f7afa4").Sum(e => e.BedragNaPercentage.GetValueOrDefault());
            Data.Reserves.AAMRAndereBT = Math.Abs(Data.Reserves.AAMRAndereBT.GetValueOrDefault());


            Data.Reserves.AAanpassingenMinBTReservesBT = worksheetset.BelasteReservesApp.Data.AanpassingBTReserves.Where(e => e.TypeReseve != null && e.TypeReseve.Id.ToString().ToLower() == "a73d7698-fc2e-434f-9652-e8d1ed496b8f").Sum(e => e.BedragNaPercentage.GetValueOrDefault());
            Data.Reserves.AAanpassingenMinBTReservesBT = Math.Abs(Data.Reserves.AAanpassingenMinBTReservesBT.GetValueOrDefault());


            Data.Reserves.ASubTotaalNaAanpassingBT = Data.Reserves.AORSubTotaalBT.GetValueOrDefault() + Data.Reserves.AAMRBTRMeerwaardeAandelenBT.GetValueOrDefault() + Data.Reserves.AAMRTerugnemingVUAandelen.GetValueOrDefault() + Data.Reserves.AAMRTaxShelterBT.GetValueOrDefault() + Data.Reserves.AAMRGewestPremiesBT.GetValueOrDefault() + Data.Reserves.AAMRDefVrijstellingWinst.GetValueOrDefault() + Data.Reserves.AAMRAndereBT.GetValueOrDefault() - Data.Reserves.AAanpassingenMinBTReservesBT.GetValueOrDefault();

            Data.Reserves.ATotaalET = Data.Reserves.AORSubTotaalET.GetValueOrDefault() - Data.Reserves.ASubTotaalNaAanpassingBT.GetValueOrDefault();
        }

        private void ImportBelastingvrijeReserves(ref WorksheetSetData worksheetset)
        {

            Data.VrijgesteldeReserve.BWaardevHandelsvBT = worksheetset.BelastingVrijeReservesApp.Data.VrijgesteldeReserves.AWaardeverminderingA.GetValueOrDefault();
            Data.VrijgesteldeReserve.BWaardevHandelsvET = worksheetset.BelastingVrijeReservesApp.Data.VrijgesteldeReserves.AWaardeverminderingD.GetValueOrDefault();
            Data.VrijgesteldeReserve.BVoorzieningenBT = worksheetset.BelastingVrijeReservesApp.Data.VrijgesteldeReserves.BVoorzieningA.GetValueOrDefault();
            Data.VrijgesteldeReserve.BVoorzieningenET = worksheetset.BelastingVrijeReservesApp.Data.VrijgesteldeReserves.BVoorzieningD.GetValueOrDefault();
            Data.VrijgesteldeReserve.BNietVerwezMeerwBT = worksheetset.BelastingVrijeReservesApp.Data.VrijgesteldeReserves.CTotaalA.GetValueOrDefault();
            Data.VrijgesteldeReserve.BNietVerwezMeerwET = worksheetset.BelastingVrijeReservesApp.Data.VrijgesteldeReserves.CTotaalD.GetValueOrDefault();
            Data.VrijgesteldeReserve.BVerwezMeerwBT = worksheetset.BelastingVrijeReservesApp.Data.VrijgesteldeReserves.DTotaalA.GetValueOrDefault();
            Data.VrijgesteldeReserve.BVerwezMeerwET = worksheetset.BelastingVrijeReservesApp.Data.VrijgesteldeReserves.DTotaalD.GetValueOrDefault();
            Data.VrijgesteldeReserve.BGespreidVerwezMwBT = worksheetset.BelastingVrijeReservesApp.Data.VrijgesteldeReserves.EGespreidVerwezenA.GetValueOrDefault();
            Data.VrijgesteldeReserve.BGespreidVerwezMwET = worksheetset.BelastingVrijeReservesApp.Data.VrijgesteldeReserves.EGespreidVerwezenD.GetValueOrDefault();
            Data.VrijgesteldeReserve.BGespreidVerwezMwMatImmatVasteActivaBT = worksheetset.BelastingVrijeReservesApp.Data.VrijgesteldeReserves.EMatImmatVasteActivaA.GetValueOrDefault();
            Data.VrijgesteldeReserve.BGespreidVerwezMwMatImmatVasteActivaET = worksheetset.BelastingVrijeReservesApp.Data.VrijgesteldeReserves.EMatImmatVasteActivaD.GetValueOrDefault();
            Data.VrijgesteldeReserve.BBedrijfsVoertuigenBT = worksheetset.BelastingVrijeReservesApp.Data.VrijgesteldeReserves.FBedrijfsvoertuigenA.GetValueOrDefault();
            Data.VrijgesteldeReserve.BBedrijfsVoertuigenET = worksheetset.BelastingVrijeReservesApp.Data.VrijgesteldeReserves.FBedrijfsvoertuigenD.GetValueOrDefault();
            Data.VrijgesteldeReserve.BBinnenschepenBT = worksheetset.BelastingVrijeReservesApp.Data.VrijgesteldeReserves.GBinnenschepenA.GetValueOrDefault();
            Data.VrijgesteldeReserve.BBinnenschepenET = worksheetset.BelastingVrijeReservesApp.Data.VrijgesteldeReserves.GBinnenschepenD.GetValueOrDefault();
            Data.VrijgesteldeReserve.BZeeschepenBT = worksheetset.BelastingVrijeReservesApp.Data.VrijgesteldeReserves.HZeeschepenA.GetValueOrDefault();
            Data.VrijgesteldeReserve.BZeeschepenET = worksheetset.BelastingVrijeReservesApp.Data.VrijgesteldeReserves.HZeeschepenD.GetValueOrDefault();
            Data.VrijgesteldeReserve.BInvestReserveBT = worksheetset.BelastingVrijeReservesApp.Data.VrijgesteldeReserves.IInvesteringsreserveA.GetValueOrDefault();
            Data.VrijgesteldeReserve.BInvestReserveET = worksheetset.BelastingVrijeReservesApp.Data.VrijgesteldeReserves.IInvesteringsreserveD.GetValueOrDefault();
            Data.VrijgesteldeReserve.BTaxShelterBT = worksheetset.BelastingVrijeReservesApp.Data.VrijgesteldeReserves.JTaxShelterAudioVisueelA.GetValueOrDefault();
            Data.VrijgesteldeReserve.BTaxShelterET = worksheetset.BelastingVrijeReservesApp.Data.VrijgesteldeReserves.JTaxShelterAudioVisueelD.GetValueOrDefault();
            Data.VrijgesteldeReserve.BAndereBT = worksheetset.BelastingVrijeReservesApp.Data.VrijgesteldeReserves.KTotaalA.GetValueOrDefault();
            Data.VrijgesteldeReserve.BAndereET = worksheetset.BelastingVrijeReservesApp.Data.VrijgesteldeReserves.KTotaalD.GetValueOrDefault();
            Data.VrijgesteldeReserve.BWinstHomologatieBT = worksheetset.BelastingVrijeReservesApp.Data.VrijgesteldeReserves.JWinstHomologatieA.GetValueOrDefault();
            Data.VrijgesteldeReserve.BWinstHomologatieET = worksheetset.BelastingVrijeReservesApp.Data.VrijgesteldeReserves.JWinstHomologatieD.GetValueOrDefault();
            Data.VrijgesteldeReserve.BTotalenBT = Data.VrijgesteldeReserve.BWaardevHandelsvBT.GetValueOrDefault() + Data.VrijgesteldeReserve.BVoorzieningenBT.GetValueOrDefault() + Data.VrijgesteldeReserve.BNietVerwezMeerwBT.GetValueOrDefault() + Data.VrijgesteldeReserve.BVerwezMeerwBT.GetValueOrDefault() + Data.VrijgesteldeReserve.BGespreidVerwezMwBT.GetValueOrDefault() + Data.VrijgesteldeReserve.BGespreidVerwezMwMatImmatVasteActivaBT.GetValueOrDefault() + Data.VrijgesteldeReserve.BBedrijfsVoertuigenBT.GetValueOrDefault() + Data.VrijgesteldeReserve.BBinnenschepenBT.GetValueOrDefault() + Data.VrijgesteldeReserve.BZeeschepenBT.GetValueOrDefault() + Data.VrijgesteldeReserve.BInvestReserveBT.GetValueOrDefault() + Data.VrijgesteldeReserve.BTaxShelterBT.GetValueOrDefault() + Data.VrijgesteldeReserve.BAndereBT.GetValueOrDefault() + Data.VrijgesteldeReserve.BWinstHomologatieBT.GetValueOrDefault();
            Data.VrijgesteldeReserve.BTotalenET = Data.VrijgesteldeReserve.BWaardevHandelsvET.GetValueOrDefault() + Data.VrijgesteldeReserve.BVoorzieningenET.GetValueOrDefault() + Data.VrijgesteldeReserve.BNietVerwezMeerwET.GetValueOrDefault() + Data.VrijgesteldeReserve.BVerwezMeerwET.GetValueOrDefault() + Data.VrijgesteldeReserve.BGespreidVerwezMwET.GetValueOrDefault() + Data.VrijgesteldeReserve.BGespreidVerwezMwMatImmatVasteActivaET.GetValueOrDefault() + Data.VrijgesteldeReserve.BBedrijfsVoertuigenET.GetValueOrDefault() + Data.VrijgesteldeReserve.BBinnenschepenET.GetValueOrDefault() + Data.VrijgesteldeReserve.BZeeschepenET.GetValueOrDefault() + Data.VrijgesteldeReserve.BInvestReserveET.GetValueOrDefault() + Data.VrijgesteldeReserve.BTaxShelterET.GetValueOrDefault() + Data.VrijgesteldeReserve.BAndereET.GetValueOrDefault() + Data.VrijgesteldeReserve.BWinstHomologatieET.GetValueOrDefault();
        }

        private void ImportDBI(ref WorksheetSetData worksheetset)
        {

            Data.DBI.DBIAAbeNettoBEI = worksheetset.DBIApp.Data.DBI.DBIAAbeNettoBEI.GetValueOrDefault();
            Data.DBI.DBIAAbeNettoBUI = worksheetset.DBIApp.Data.DBI.DBIAAbeNettoBUI.GetValueOrDefault();
            Data.DBI.DBIAAbeNettoTotaal = worksheetset.DBIApp.Data.DBI.DBIAAbeNettoTotaal.GetValueOrDefault();
            Data.DBI.DBIAAbeRoerendBEI = worksheetset.DBIApp.Data.DBI.DBIAAbeRoerendBEI.GetValueOrDefault();
            Data.DBI.DBIAAbeRoerendBUI = worksheetset.DBIApp.Data.DBI.DBIAAbeRoerendBUI.GetValueOrDefault();
            Data.DBI.DBIAAbeRoerendTotaal = worksheetset.DBIApp.Data.DBI.DBIAAbeRoerendTotaal.GetValueOrDefault();
            Data.DBI.DBIAAbuNettoBEI = worksheetset.DBIApp.Data.DBI.DBIAAbuNettoBEI.GetValueOrDefault();
            Data.DBI.DBIAAbuNettoBUI = worksheetset.DBIApp.Data.DBI.DBIAAbuNettoBUI.GetValueOrDefault();
            Data.DBI.DBIAAbuRoerendTotaal = worksheetset.DBIApp.Data.DBI.DBIAAbuRoerendTotaal.GetValueOrDefault();
            Data.DBI.DBIAbeNettoBEI = worksheetset.DBIApp.Data.DBI.DBIAbeNettoBEI.GetValueOrDefault();
            Data.DBI.DBIAbeNettoBUI = worksheetset.DBIApp.Data.DBI.DBIAbeNettoBUI.GetValueOrDefault();
            Data.DBI.DBIAbeNettoTotaal = worksheetset.DBIApp.Data.DBI.DBIAbeNettoTotaal.GetValueOrDefault();
            Data.DBI.DBIAbeRoerendBEI = worksheetset.DBIApp.Data.DBI.DBIAbeRoerendBEI.GetValueOrDefault();
            Data.DBI.DBIAbeRoerendBUI = worksheetset.DBIApp.Data.DBI.DBIAbeRoerendBUI.GetValueOrDefault();
            Data.DBI.DBIAbeRoerendTotaal = worksheetset.DBIApp.Data.DBI.DBIAbeRoerendTotaal.GetValueOrDefault();
            Data.DBI.DBIAbuNettoBEI = worksheetset.DBIApp.Data.DBI.DBIAbuNettoBEI.GetValueOrDefault();
            Data.DBI.DBIAbuNettoBUI = worksheetset.DBIApp.Data.DBI.DBIAbuNettoBUI.GetValueOrDefault();
            Data.DBI.DBIAbuNettoTotaal = worksheetset.DBIApp.Data.DBI.DBIAbuNettoTotaal.GetValueOrDefault();
            Data.DBI.DBIAbuRoerendBEI = worksheetset.DBIApp.Data.DBI.DBIAbuRoerendBEI.GetValueOrDefault();
            Data.DBI.DBIAbuRoerendBUI = worksheetset.DBIApp.Data.DBI.DBIAbuRoerendBUI.GetValueOrDefault();
            Data.DBI.DBIAbuRoerendTotaal = worksheetset.DBIApp.Data.DBI.DBIAbuRoerendTotaal.GetValueOrDefault();
            Data.DBI.DBIHerfinancBEI = worksheetset.DBIApp.Data.DBI.DBIHerfinancBEI.GetValueOrDefault();
            Data.DBI.DBIHerfinancBUI = worksheetset.DBIApp.Data.DBI.DBIHerfinancBUI.GetValueOrDefault();
            Data.DBI.DBIHerfinancTotaal = worksheetset.DBIApp.Data.DBI.DBIHerfinancTotaal.GetValueOrDefault();
            Data.DBI.DBIKostenBEI = worksheetset.DBIApp.Data.DBI.DBIKostenBEI.GetValueOrDefault();
            Data.DBI.DBIKostenBUI = worksheetset.DBIApp.Data.DBI.DBIKostenBUI.GetValueOrDefault();
            Data.DBI.DBIKostenTotaal = worksheetset.DBIApp.Data.DBI.DBIKostenTotaal.GetValueOrDefault();
            Data.DBI.DBILidEuBEI = worksheetset.DBIApp.Data.DBI.DBILidEuBEI.GetValueOrDefault();
            Data.DBI.DBILidEuBUI = worksheetset.DBIApp.Data.DBI.DBILidEuBUI.GetValueOrDefault();
            Data.DBI.DBILidEuTotaal = worksheetset.DBIApp.Data.DBI.DBILidEuTotaal.GetValueOrDefault();
            Data.DBI.DBISubtotaalBEI = worksheetset.DBIApp.Data.DBI.DBISubtotaalBEI.GetValueOrDefault();
            Data.DBI.DBISubtotaalBUI = worksheetset.DBIApp.Data.DBI.DBISubtotaalBUI.GetValueOrDefault();
            Data.DBI.DBISubtotaalTotaal = worksheetset.DBIApp.Data.DBI.DBISubtotaalTotaal.GetValueOrDefault();
            Data.DBI.DBITotaal = worksheetset.DBIApp.Data.DBI.DBITotaal.GetValueOrDefault();
            Data.DBI.DBITotaalBEI = worksheetset.DBIApp.Data.DBI.DBITotaalBEI.GetValueOrDefault();
            Data.DBI.DBITotaalBUI = worksheetset.DBIApp.Data.DBI.DBITotaalBUI.GetValueOrDefault();
            Data.DBI.DBIVerschilBEI = worksheetset.DBIApp.Data.DBI.DBIVerschilBEI.GetValueOrDefault();
            Data.DBI.DBIVerschilBUI = worksheetset.DBIApp.Data.DBI.DBIVerschilBUI.GetValueOrDefault();
            Data.DBI.DBIVerschilTotaal = worksheetset.DBIApp.Data.DBI.DBIVerschilTotaal.GetValueOrDefault();
            Data.DBI.DBIVrijgestelRoerendBEI = worksheetset.DBIApp.Data.DBI.DBIVrijgestelRoerendBEI.GetValueOrDefault();
            Data.DBI.DBIVrijgestelRoerendBUI = worksheetset.DBIApp.Data.DBI.DBIVrijgestelRoerendBUI.GetValueOrDefault();
            Data.DBI.DBIVrijgestelRoerendTotaal = worksheetset.DBIApp.Data.DBI.DBIVrijgestelRoerendTotaal.GetValueOrDefault();

            string ass = worksheetset.DataContract.AssessmentYear.ToString();
            string assPrev = (worksheetset.DataContract.AssessmentYear - 1 ).ToString();

            //CHANGE 21/06/14
            // Op regel 1701 het saldo van de DBI-aftrek van het vorige aj. vermelden
            Data.OverdrachtDBIAftrek.ODASaldoOvergedragen = worksheetset.DBIApp.Data.Historiek.Where(e => e.Aanslagjaar != null && e.Aanslagjaar.Id != ass).Sum(e => e.SaldoStart.GetValueOrDefault());

            /* Op regel 1702 het bedrag vermelden van de DBIaftrek van het belastbare tijdperk dat niet kan worden 
             * afgetrokken wegens het gebrek aan restwinst en dat in aanmerking komt om overgedragen te worden 
             * naar het volgende belastbare tijdperk. 
             */
            Data.OverdrachtDBIAftrek.ODABedragHuidigNaarVolgend = worksheetset.DBIApp.Data.Historiek.Where(e => e.Aanslagjaar != null && e.Aanslagjaar.Id == ass).Sum(e => e.SaldoEinde.GetValueOrDefault() - e.NietOverdraagbaarDeel.GetValueOrDefault());

            /* Op regel 1703 het bedrag vermelden van de overgedragen DBI-aftrek dat werkelijk wordt afgetrokken 
             * tijdens het belastbare tijdperk
             */
            Data.OverdrachtDBIAftrek.ODABedragSub1Werkelijk = worksheetset.DBIApp.Data.Historiek.Where(e => e.Aanslagjaar != null && e.Aanslagjaar.Id != ass).Sum(e => e.GebruiktBJ.GetValueOrDefault());

            /* Op regel 1704 het saldo van de DBI-aftrek vermelden dat overdraagbaar is naar het volgende 
             * belastbare tijdperk (art. 205, § 3, WIB 92)
             */
            Data.OverdrachtDBIAftrek.ODASaldoOverdraagbaarVolgend = (Data.OverdrachtDBIAftrek.ODABedragHuidigNaarVolgend.GetValueOrDefault() + Data.OverdrachtDBIAftrek.ODASaldoOvergedragen.GetValueOrDefault()) - Data.OverdrachtDBIAftrek.ODABedragSub1Werkelijk.GetValueOrDefault();
           

            /* PREVIOUSLY 
            Data.OverdrachtDBIAftrek.ODABedragHuidigNaarVolgend = worksheetset.DBIApp.Data.Historiek.Where(e => e.Aanslagjaar != null && e.Aanslagjaar.Id == ass).Sum(e => e.SaldoEinde.GetValueOrDefault());
            Data.OverdrachtDBIAftrek.ODABedragSub1Werkelijk = worksheetset.DBIApp.Data.Historiek.Where(e => e.Aanslagjaar != null && e.Aanslagjaar.Id != ass).Sum(e => e.GebruiktBJ.GetValueOrDefault());
             * 
            Data.OverdrachtDBIAftrek.ODASaldoOverdraagbaarVolgend = (Data.OverdrachtDBIAftrek.ODABedragHuidigNaarVolgend.GetValueOrDefault() + Data.OverdrachtDBIAftrek.ODASaldoOvergedragen.GetValueOrDefault()) - Data.OverdrachtDBIAftrek.ODABedragSub1Werkelijk.GetValueOrDefault();
            */
        }

        private void ImportBerekeningVenB(ref WorksheetSetData worksheetset)
        {

            // VU
            Data.VerworpenUitgaven.VUNietAftrekbareBelasting = worksheetset.BerekeningVenBApp.Data.VerworpenUitgaven.PDDisallowedCurrentIncomeTaxExpenses.GetValueOrDefault();
            Data.VerworpenUitgaven.VUGewestBelast = worksheetset.BerekeningVenBApp.Data.VerworpenUitgaven.DERegionalTaxes.GetValueOrDefault();
            Data.VerworpenUitgaven.VUBoetes = worksheetset.BerekeningVenBApp.Data.VerworpenUitgaven.DEPenalties.GetValueOrDefault();
            Data.VerworpenUitgaven.VUPensioenen = worksheetset.BerekeningVenBApp.Data.VerworpenUitgaven.DEPensions.GetValueOrDefault();
            Data.VerworpenUitgaven.VUAutokosten = worksheetset.BerekeningVenBApp.Data.VerworpenUitgaven.DECar.GetValueOrDefault();
            Data.VerworpenUitgaven.VUAutokostenVAA = worksheetset.BerekeningVenBApp.Data.VerworpenUitgaven.DECarVAA.GetValueOrDefault();
            Data.VerworpenUitgaven.VUReceptie = worksheetset.BerekeningVenBApp.Data.VerworpenUitgaven.DEReception.GetValueOrDefault();
            Data.VerworpenUitgaven.VURestaurant = worksheetset.BerekeningVenBApp.Data.VerworpenUitgaven.DERestaurant.GetValueOrDefault();
            Data.VerworpenUitgaven.VUKledij = worksheetset.BerekeningVenBApp.Data.VerworpenUitgaven.DEClothes.GetValueOrDefault();
            Data.VerworpenUitgaven.VUOverdrevenIntresten = worksheetset.BerekeningVenBApp.Data.VerworpenUitgaven.DEExcessInterest.GetValueOrDefault();
            Data.VerworpenUitgaven.VUBepaaldeLeningen = worksheetset.BerekeningVenBApp.Data.VerworpenUitgaven.DEInterestLoans.GetValueOrDefault();
            Data.VerworpenUitgaven.VUAbnormaleGoedgVoordelen = worksheetset.BerekeningVenBApp.Data.VerworpenUitgaven.DEAbnormalAdvantages.GetValueOrDefault();
            Data.VerworpenUitgaven.VUSocialeVoordelen = worksheetset.BerekeningVenBApp.Data.VerworpenUitgaven.DESocial.GetValueOrDefault();
            Data.VerworpenUitgaven.VULiberaliteiten = worksheetset.BerekeningVenBApp.Data.VerworpenUitgaven.DECharity.GetValueOrDefault();
            Data.VerworpenUitgaven.VUAandelen = worksheetset.BerekeningVenBApp.Data.VerworpenUitgaven.DEReductionValueShares.GetValueOrDefault();
            Data.VerworpenUitgaven.VUTerugnemingVrijstellingen = worksheetset.BerekeningVenBApp.Data.VerworpenUitgaven.DEReversalsPreviousTax.GetValueOrDefault();
            Data.VerworpenUitgaven.VUWerknemerParticipatie = worksheetset.BerekeningVenBApp.Data.VerworpenUitgaven.DEEmployeeCapital.GetValueOrDefault();
            Data.VerworpenUitgaven.VUCoupon = worksheetset.BerekeningVenBApp.Data.VerworpenUitgaven.DEIndemnityMissingCoupon.GetValueOrDefault();
            Data.VerworpenUitgaven.VUTaxShelter = worksheetset.BerekeningVenBApp.Data.VerworpenUitgaven.DEExpensesTaxShelter.GetValueOrDefault();
            Data.VerworpenUitgaven.VUGewestPremies = worksheetset.BerekeningVenBApp.Data.VerworpenUitgaven.DERegionalPremiumsCapital.GetValueOrDefault();
            Data.VerworpenUitgaven.VUBepaaldeStaten = worksheetset.BerekeningVenBApp.Data.VerworpenUitgaven.DEBepaaldeStaten.GetValueOrDefault();
            Data.VerworpenUitgaven.VUAndere = worksheetset.BerekeningVenBApp.Data.VerworpenUitgaven.DEOther.GetValueOrDefault();
            Data.VerworpenUitgaven.VUMaaltijdSportCultuurEco = worksheetset.BerekeningVenBApp.Data.VerworpenUitgaven.DEDinerSportCultureEco.GetValueOrDefault();
            Data.VerworpenUitgaven.VUTotaal = worksheetset.BerekeningVenBApp.Data.VerworpenUitgaven.DETotal.GetValueOrDefault();
            Data.VerworpenUitgaven.VUTotaal = Data.VerworpenUitgaven.VUTotaal.GetValueOrDefault() + Data.VerworpenUitgaven.VUNietAftrekbareBelasting.GetValueOrDefault();
            

            // UitgekeerdeDividenden
            /*
            Data.UitgekeerdeDividenden.UDGewone = worksheetset.BerekeningVenBApp.Data.Overzicht.Dividenden.GetValueOrDefault();

            Data.UitgekeerdeDividenden.UDAandelen = worksheetset.BerekeningVenBApp.Data.Overzicht.VerkrijgingEigenAandelen.GetValueOrDefault();
            Data.UitgekeerdeDividenden.UDOverlijden = worksheetset.BerekeningVenBApp.Data.Overzicht.OverlijdenUittredingUitsluitingVennoot.GetValueOrDefault();
            Data.UitgekeerdeDividenden.UDMaatschappelijk = worksheetset.BerekeningVenBApp.Data.Overzicht.VerdelingMaatschappelijkVermogen.GetValueOrDefault();
            */
            // UiteenzettingWinst
                //BelastbareBestZonderAftrek
            Data.UiteenzettingWinst.WIBABAbnormaalGoedg = worksheetset.BerekeningVenBApp.Data.BelastbareBestZonderAftrek.TEReceivedAbnormal.GetValueOrDefault();
            
            Data.UiteenzettingWinst.WIBABInvestVerplicht = worksheetset.BerekeningVenBApp.Data.BelastbareBestZonderAftrek.TETaxableInvestmentReserve.GetValueOrDefault();
            Data.UiteenzettingWinst.WIBABAutoVaa = worksheetset.BerekeningVenBApp.Data.BelastbareBestZonderAftrek.TEVaaCar.GetValueOrDefault();
            Data.UiteenzettingWinst.WIBABWerknemersPart = worksheetset.BerekeningVenBApp.Data.BelastbareBestZonderAftrek.TEEmployeeParticipationCapital.GetValueOrDefault();
            Data.UiteenzettingWinst.WIBABKapitaal = worksheetset.BerekeningVenBApp.Data.BelastbareBestZonderAftrek.TECapitalAgriCulture.GetValueOrDefault();


                // OpdelingNaarOorsprong
            Data.UiteenzettingWinst.WVVOResterendResultaatVV = worksheetset.BerekeningVenBApp.Data.OpdelingNaarOorsprong.IRTreatyForeignIncome.GetValueOrDefault();

            Data.UiteenzettingWinst.WVVOResterendResultaatNVV = worksheetset.BerekeningVenBApp.Data.OpdelingNaarOorsprong.IRNonTreatyForeignIncome.GetValueOrDefault();

            Data.UiteenzettingWinst.WVVOResterendResultaatBelg = worksheetset.BerekeningVenBApp.Data.OpdelingNaarOorsprong.IRBelgianTaxable.GetValueOrDefault();
            

                // FiscaleAftrek

            Data.UiteenzettingWinst.CalcDivideDBI = worksheetset.BerekeningVenBApp.Data.FiscaleAftrek.PEBelgium.GetValueOrDefault();

            Data.UiteenzettingWinst.CalcDivideOctrooi = worksheetset.BerekeningVenBApp.Data.FiscaleAftrek.PIDBelgium.GetValueOrDefault();

            Data.UiteenzettingWinst.CalcDivideNID = worksheetset.BerekeningVenBApp.Data.FiscaleAftrek.NIDBelgium.GetValueOrDefault();

            Data.UiteenzettingWinst.CalcDivideNIDHistoriek = worksheetset.BerekeningVenBApp.Data.FiscaleAftrek.NIDHistoryBelgium.GetValueOrDefault();

            Data.UiteenzettingWinst.CalcDivideVerliezen = worksheetset.BerekeningVenBApp.Data.FiscaleAftrek.DTLBelgium.GetValueOrDefault();

            Data.UiteenzettingWinst.CalcDivideInvestering = worksheetset.BerekeningVenBApp.Data.FiscaleAftrek.IDBelgium.GetValueOrDefault();


            Data.UiteenzettingWinst.WIBGExitTarief = worksheetset.BerekeningVenBApp.Data.BerekeningOverzicht.ExitTarif.GetValueOrDefault();
            Data.UiteenzettingWinst.WIBGMeerwaardeAandelenBelast = worksheetset.BerekeningVenBApp.Data.BerekeningOverzicht.CapitalGainsShares25.GetValueOrDefault();


            // NietBelastbareBestanddelen

            Data.NietBelastbareBestanddelen.NTEBEPersonnel = worksheetset.BerekeningVenBApp.Data.FiscaleAftrek.NTEBEPersonnel.GetValueOrDefault();

            Data.NietBelastbareBestanddelen.NTEBEPersonnelSME = worksheetset.BerekeningVenBApp.Data.FiscaleAftrek.NTEBEPersonnelSME.GetValueOrDefault();

            Data.NietBelastbareBestanddelen.NTEBEInternalshipPremium = worksheetset.BerekeningVenBApp.Data.FiscaleAftrek.NTEBEInternalshipPremium.GetValueOrDefault();

            Data.NietBelastbareBestanddelen.NTEBEOthers = worksheetset.BerekeningVenBApp.Data.FiscaleAftrek.NTEBEOthers.GetValueOrDefault();

            Data.NietBelastbareBestanddelen.NTEBECharity = worksheetset.BerekeningVenBApp.Data.FiscaleAftrek.NTEBECharity.GetValueOrDefault();

            Data.NietBelastbareBestanddelen.TotalNonTaxableElements = worksheetset.BerekeningVenBApp.Data.FiscaleAftrek.TotalNonTaxableElements.GetValueOrDefault();


            Data.TariefBelasting.TARUitgeslotenVerminderd = worksheetset.BerekeningVenBApp.Data.BerekeningOverzicht.Verminderd == true;

            Data.VoorafBetalingen.VBJongVennootschap =worksheetset.Settings.CurrentPeriodSmall == true;
            Data.VoorafBetalingen.VBEersteKwartaal = worksheetset.BerekeningVenBApp.Data.BerekeningOverzicht.AP1Base.GetValueOrDefault();
            Data.VoorafBetalingen.VBTweedeKwartaal = worksheetset.BerekeningVenBApp.Data.BerekeningOverzicht.AP2Base.GetValueOrDefault();
            Data.VoorafBetalingen.VBDerdeKwartaal = worksheetset.BerekeningVenBApp.Data.BerekeningOverzicht.AP3Base.GetValueOrDefault();
            Data.VoorafBetalingen.VBVierdeKwartaal = worksheetset.BerekeningVenBApp.Data.BerekeningOverzicht.AP4Base.GetValueOrDefault();
            //Data.VoorafBetalingen.VBTotaal = Data.VoorafBetalingen.VBEersteKwartaal.GetValueOrDefault() + Data.VoorafBetalingen.VBTweedeKwartaal.GetValueOrDefault() + Data.VoorafBetalingen.VBDerdeKwartaal.GetValueOrDefault() + Data.VoorafBetalingen.VBVierdeKwartaal.GetValueOrDefault();

          



        }

        private void ImportTransferTaxCredits(ref WorksheetSetData worksheetset)
        {
//            Data.OverdrachtDBIAftrek.ODASaldoOvergedragen = 0;
            Data.OverdrachtNID.ONIDSaldoOvergedragen = 0;
            Data.OverdrachtNID.ONIDOverdraagbaarVolgend = 0;
            Data.CompenseerbareVerliezen.CVSaldoVorigeVerliezen = 0;
            Data.CompenseerbareVerliezen.CVGecompenseerdeVerliezen = 0;
            Data.CompenseerbareVerliezen.CVVerliesBelastbaarTijdperk = 0;

        //    Data.OverdrachtDBIAftrek.ODASaldoOvergedragen = worksheetset.TransferTaxCreditsApp.Data.DBI.SaldoStart.GetValueOrDefault();
           // Data.OverdrachtNID.ONIDOverdraagbaarVolgend = worksheetset.TransferTaxCreditsApp.Data.NID.Where(e => e.BedragEindeBJ.HasValue).Sum(e => e.BedragEindeBJ.GetValueOrDefault());

            

            int ass = worksheetset.DataContract.AssessmentYear;



            if (worksheetset.TransferTaxCreditsApp.Data.FiscaleVerliezen.Count(e => e.Type == "saldostart") > 0) Data.CompenseerbareVerliezen.CVSaldoVorigeVerliezen = 0 - Math.Abs(worksheetset.TransferTaxCreditsApp.Data.FiscaleVerliezen.Where(e => e.Type == "saldostart").Sum(e => e.Cumul.GetValueOrDefault()));

            if (worksheetset.TransferTaxCreditsApp.Data.FiscaleVerliezen.Count(e => e.Type == "compenseerdverlieshuidig") > 0) Data.CompenseerbareVerliezen.CVGecompenseerdeVerliezen = worksheetset.TransferTaxCreditsApp.Data.FiscaleVerliezen.Where(e => e.Type == "compenseerdverlieshuidig" && e.Cumul.HasValue).Sum(e => e.Cumul.GetValueOrDefault());

            if (worksheetset.TransferTaxCreditsApp.Data.FiscaleVerliezen.Count(e => e.Type == "verlieshuidig") > 0) Data.CompenseerbareVerliezen.CVVerliesBelastbaarTijdperk = 0 - Math.Abs(worksheetset.TransferTaxCreditsApp.Data.FiscaleVerliezen.Where(e => e.Type == "verlieshuidig").Sum(e => e.Cumul.GetValueOrDefault()));

        }

        private void ImportHistoryNID(ref WorksheetSetData worksheetset)
        {
            Data.OverdrachtNID.ONIDOverdraagbaarVolgend = worksheetset.RisicoKapitaalHistoriekApp.Data.Historiek.Sum(h => h.BeperktOverdraagbaarVolgendAJ.GetValueOrDefault() + h.OnbeperktOverdraagbaarVolgendAJ.GetValueOrDefault());
            Data.OverdrachtNID.ONIDSaldoOvergedragen = worksheetset.RisicoKapitaalHistoriekApp.Data.Historiek.Sum(h => h.BeperktOverdraagbaarSaldo.GetValueOrDefault() + h.OnbeperktOverdraagbaarSaldo.GetValueOrDefault());

        }

        #region AddUpdateDelete

        public void UpdateReserves(EY.com.eBook.RuleEngine.AY2015.AangifteVenBAppTypes.ReserveEntity entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2015.AangifteVenBAppTypes.ReserveEntity original = Data.Reserves;

            entity = Validate(entity, "Reserves", ref worksheetset);
            if (entity.IsValid)
            {
                Data.Reserves = entity;
                Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
            }
        }

        public void UpdateVrijgesteldeReserve(EY.com.eBook.RuleEngine.AY2015.AangifteVenBAppTypes.VrijgesteldeReserveEntity entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2015.AangifteVenBAppTypes.VrijgesteldeReserveEntity original = Data.VrijgesteldeReserve;

            entity = Validate(entity, "VrijgesteldeReserve", ref worksheetset);
            if (entity.IsValid)
            {

                Data.VrijgesteldeReserve = entity;
                Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
            }
        }

        public void UpdateVerworpenUitgaven(EY.com.eBook.RuleEngine.AY2015.AangifteVenBAppTypes.VerworpenUitgavenEntity entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2015.AangifteVenBAppTypes.VerworpenUitgavenEntity original = Data.VerworpenUitgaven;

            entity = Validate(entity, "VerworpenUitgaven", ref worksheetset);
            if (entity.IsValid)
            {

                Data.VerworpenUitgaven = entity;
                Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
            }
        }

        public void UpdateUitgekeerdeDividenden(EY.com.eBook.RuleEngine.AY2015.AangifteVenBAppTypes.UitgekeerdeDividendenEntity entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2015.AangifteVenBAppTypes.UitgekeerdeDividendenEntity original = Data.UitgekeerdeDividenden;

            entity = Validate(entity, "UitgekeerdeDividenden", ref worksheetset);
            if (entity.IsValid)
            {

                Data.UitgekeerdeDividenden = entity;
                Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
            }
        }

        public void UpdateUiteenzettingWinst(EY.com.eBook.RuleEngine.AY2015.AangifteVenBAppTypes.WinstEntity entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2015.AangifteVenBAppTypes.WinstEntity original = Data.UiteenzettingWinst;

            entity = Validate(entity, "UiteenzettingWinst", ref worksheetset);
            if (entity.IsValid)
            {

                Data.UiteenzettingWinst = entity;
                Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
            }
        }

        public void UpdateAfzonderlijkeAanslagen(EY.com.eBook.RuleEngine.AY2015.AangifteVenBAppTypes.AfzonderlijkeAanslagenEntity entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2015.AangifteVenBAppTypes.AfzonderlijkeAanslagenEntity original = Data.AfzonderlijkeAanslagen;

            entity = Validate(entity, "AfzonderlijkeAanslagen", ref worksheetset);
            if (entity.IsValid)
            {

                Data.AfzonderlijkeAanslagen = entity;
                Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
            }
        }

        public void UpdateDiamant(EY.com.eBook.RuleEngine.AY2015.AangifteVenBAppTypes.DiamantEntity entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2015.AangifteVenBAppTypes.DiamantEntity original = Data.Diamant;

            entity = Validate(entity, "Diamant", ref worksheetset);
            if (entity.IsValid)
            {

                Data.Diamant = entity;
                Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
            }
        }

        public void UpdateNietBelastbareBestanddelen(EY.com.eBook.RuleEngine.AY2015.AangifteVenBAppTypes.NietBelastbareBestandelenItem entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2015.AangifteVenBAppTypes.NietBelastbareBestandelenItem original = Data.NietBelastbareBestanddelen;

            entity = Validate(entity, "NietBelastbareBestanddelen", ref worksheetset);
            if (entity.IsValid)
            {

                Data.NietBelastbareBestanddelen = entity;
                Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
            }
        }

        public void UpdateDBI(EY.com.eBook.RuleEngine.AY2015.AangifteVenBAppTypes.DbiEntity entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2015.AangifteVenBAppTypes.DbiEntity original = Data.DBI;

            entity = Validate(entity, "DBI", ref worksheetset);
            if (entity.IsValid)
            {

                Data.DBI = entity;
                Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
            }
        }

        public void UpdateOverdrachtDBIAftrek(EY.com.eBook.RuleEngine.AY2015.AangifteVenBAppTypes.OverdrachtDBIAftrekEntity entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2015.AangifteVenBAppTypes.OverdrachtDBIAftrekEntity original = Data.OverdrachtDBIAftrek;

            entity = Validate(entity, "OverdrachtDBIAftrek", ref worksheetset);
            if (entity.IsValid)
            {

                Data.OverdrachtDBIAftrek = entity;
                Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
            }
        }

        public void UpdateOverdrachtNID(EY.com.eBook.RuleEngine.AY2015.AangifteVenBAppTypes.OverdrachtNIDEntity entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2015.AangifteVenBAppTypes.OverdrachtNIDEntity original = Data.OverdrachtNID;

            entity = Validate(entity, "OverdrachtNID", ref worksheetset);
            if (entity.IsValid)
            {

                Data.OverdrachtNID = entity;
                Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
            }
        }

        public void UpdateCompenseerbareVerliezen(EY.com.eBook.RuleEngine.AY2015.AangifteVenBAppTypes.CompenseerbareVerliezenEntity entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2015.AangifteVenBAppTypes.CompenseerbareVerliezenEntity original = Data.CompenseerbareVerliezen;

            entity = Validate(entity, "CompenseerbareVerliezen", ref worksheetset);
            if (entity.IsValid)
            {

                Data.CompenseerbareVerliezen = entity;
                Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
            }
        }

        public void UpdateTariefBelasting(EY.com.eBook.RuleEngine.AY2015.AangifteVenBAppTypes.TariefEntity entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2015.AangifteVenBAppTypes.TariefEntity original = Data.TariefBelasting;

            entity = Validate(entity, "TariefBelasting", ref worksheetset);
            if (entity.IsValid)
            {

                Data.TariefBelasting = entity;
                Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
            }
        }

        public void UpdateVoorafBetalingen(EY.com.eBook.RuleEngine.AY2015.AangifteVenBAppTypes.VoorafBetalingEntity entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2015.AangifteVenBAppTypes.VoorafBetalingEntity original = Data.VoorafBetalingen;

            entity = Validate(entity, "VoorafBetalingen", ref worksheetset);
            if (entity.IsValid)
            {

                Data.VoorafBetalingen = entity;
                Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
            }
        }
 /*
        public void UpdateVerrekenbareVoorheffing(EY.com.eBook.RuleEngine.AY2015.AangifteVenBAppTypes.VerrekenbareVoorheffingEntity entity, ref WorksheetSetData worksheetset)
        {
           
            EY.com.eBook.RuleEngine.AY2015.AangifteVenBAppTypes.VerrekenbareVoorheffingEntity original = Data.VerrekenbareVoorheffing;

            entity = Validate(entity, "VerrekenbareVoorheffing", ref worksheetset);
            if (entity.IsValid)
            {

                Data.VerrekenbareVoorheffing = entity;
                Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
            }
             * 
        }*/

        public void UpdateTaxShelter(EY.com.eBook.RuleEngine.AY2015.AangifteVenBAppTypes.TaxShelterEntity entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2015.AangifteVenBAppTypes.TaxShelterEntity original = Data.TaxShelter;

            entity = Validate(entity, "TaxShelter", ref worksheetset);
            if (entity.IsValid)
            {

                Data.TaxShelter = entity;
                Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
            }
        }

        public void UpdateGrootteVennootschap(EY.com.eBook.RuleEngine.AY2015.AangifteVenBAppTypes.GrootteVennootschapeEntity entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2015.AangifteVenBAppTypes.GrootteVennootschapeEntity original = Data.GrootteVennootschap;

            entity = Validate(entity, "GrootteVennootschap", ref worksheetset);
            if (entity.IsValid)
            {

                Data.GrootteVennootschap = entity;
                Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
            }
        }

        #endregion

        #region VALIDATION

        public EY.com.eBook.RuleEngine.AY2015.AangifteVenBAppTypes.ReserveEntity Validate(EY.com.eBook.RuleEngine.AY2015.AangifteVenBAppTypes.ReserveEntity entity, string location, ref WorksheetSetData worksheetset)
        {
            entity.IsValid = true;
            worksheetset.ClearMessages(TypeId, entity.Id);
            /*
            if (!entity.Bedrag.HasValue || entity.Bedrag.Value==0)
            {
            worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Grootboek");
            entity.IsValid = false;
            }
            */

            return entity;
        }

        public EY.com.eBook.RuleEngine.AY2015.AangifteVenBAppTypes.VrijgesteldeReserveEntity Validate(EY.com.eBook.RuleEngine.AY2015.AangifteVenBAppTypes.VrijgesteldeReserveEntity entity, string location, ref WorksheetSetData worksheetset)
        {
            entity.IsValid = true;
            worksheetset.ClearMessages(TypeId, entity.Id);
            /*
            if (!entity.Bedrag.HasValue || entity.Bedrag.Value==0)
            {
            worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Grootboek");
            entity.IsValid = false;
            }
            */

            return entity;
        }

        public EY.com.eBook.RuleEngine.AY2015.AangifteVenBAppTypes.VerworpenUitgavenEntity Validate(EY.com.eBook.RuleEngine.AY2015.AangifteVenBAppTypes.VerworpenUitgavenEntity entity, string location, ref WorksheetSetData worksheetset)
        {
            entity.IsValid = true;
            worksheetset.ClearMessages(TypeId, entity.Id);
            /*
            if (!entity.Bedrag.HasValue || entity.Bedrag.Value==0)
            {
            worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Grootboek");
            entity.IsValid = false;
            }
            */

            return entity;
        }

        public EY.com.eBook.RuleEngine.AY2015.AangifteVenBAppTypes.UitgekeerdeDividendenEntity Validate(EY.com.eBook.RuleEngine.AY2015.AangifteVenBAppTypes.UitgekeerdeDividendenEntity entity, string location, ref WorksheetSetData worksheetset)
        {
            entity.IsValid = true;
            worksheetset.ClearMessages(TypeId, entity.Id);
            /*
            if (!entity.Bedrag.HasValue || entity.Bedrag.Value==0)
            {
            worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Grootboek");
            entity.IsValid = false;
            }
            */

            return entity;
        }

        public EY.com.eBook.RuleEngine.AY2015.AangifteVenBAppTypes.WinstEntity Validate(EY.com.eBook.RuleEngine.AY2015.AangifteVenBAppTypes.WinstEntity entity, string location, ref WorksheetSetData worksheetset)
        {
            entity.IsValid = true;
            worksheetset.ClearMessages(TypeId, entity.Id);
            /*
            if (!entity.Bedrag.HasValue || entity.Bedrag.Value==0)
            {
            worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Grootboek");
            entity.IsValid = false;
            }
            */

            return entity;
        }

        public EY.com.eBook.RuleEngine.AY2015.AangifteVenBAppTypes.AfzonderlijkeAanslagenEntity Validate(EY.com.eBook.RuleEngine.AY2015.AangifteVenBAppTypes.AfzonderlijkeAanslagenEntity entity, string location, ref WorksheetSetData worksheetset)
        {
            entity.IsValid = true;
            worksheetset.ClearMessages(TypeId, entity.Id);
            /*
            if (!entity.Bedrag.HasValue || entity.Bedrag.Value==0)
            {
            worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Grootboek");
            entity.IsValid = false;
            }
            */

            return entity;
        }

        public EY.com.eBook.RuleEngine.AY2015.AangifteVenBAppTypes.DiamantEntity Validate(EY.com.eBook.RuleEngine.AY2015.AangifteVenBAppTypes.DiamantEntity entity, string location, ref WorksheetSetData worksheetset)
        {
            entity.IsValid = true;
            worksheetset.ClearMessages(TypeId, entity.Id);
            /*
            if (!entity.Bedrag.HasValue || entity.Bedrag.Value==0)
            {
            worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Grootboek");
            entity.IsValid = false;
            }
            */

            return entity;
        }

        public EY.com.eBook.RuleEngine.AY2015.AangifteVenBAppTypes.NietBelastbareBestandelenItem Validate(EY.com.eBook.RuleEngine.AY2015.AangifteVenBAppTypes.NietBelastbareBestandelenItem entity, string location, ref WorksheetSetData worksheetset)
        {
            entity.IsValid = true;
            worksheetset.ClearMessages(TypeId, entity.Id);
            /*
            if (!entity.Bedrag.HasValue || entity.Bedrag.Value==0)
            {
            worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Grootboek");
            entity.IsValid = false;
            }
            */

            return entity;
        }

        public EY.com.eBook.RuleEngine.AY2015.AangifteVenBAppTypes.DbiEntity Validate(EY.com.eBook.RuleEngine.AY2015.AangifteVenBAppTypes.DbiEntity entity, string location, ref WorksheetSetData worksheetset)
        {
            entity.IsValid = true;
            worksheetset.ClearMessages(TypeId, entity.Id);
            /*
            if (!entity.Bedrag.HasValue || entity.Bedrag.Value==0)
            {
            worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Grootboek");
            entity.IsValid = false;
            }
            */

            return entity;
        }

        public EY.com.eBook.RuleEngine.AY2015.AangifteVenBAppTypes.OverdrachtDBIAftrekEntity Validate(EY.com.eBook.RuleEngine.AY2015.AangifteVenBAppTypes.OverdrachtDBIAftrekEntity entity, string location, ref WorksheetSetData worksheetset)
        {
            entity.IsValid = true;
            worksheetset.ClearMessages(TypeId, entity.Id);
            /*
            if (!entity.Bedrag.HasValue || entity.Bedrag.Value==0)
            {
            worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Grootboek");
            entity.IsValid = false;
            }
            */

            return entity;
        }

        public EY.com.eBook.RuleEngine.AY2015.AangifteVenBAppTypes.OverdrachtNIDEntity Validate(EY.com.eBook.RuleEngine.AY2015.AangifteVenBAppTypes.OverdrachtNIDEntity entity, string location, ref WorksheetSetData worksheetset)
        {
            entity.IsValid = true;
            worksheetset.ClearMessages(TypeId, entity.Id);
            /*
            if (!entity.Bedrag.HasValue || entity.Bedrag.Value==0)
            {
            worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Grootboek");
            entity.IsValid = false;
            }
            */

            return entity;
        }

        public EY.com.eBook.RuleEngine.AY2015.AangifteVenBAppTypes.CompenseerbareVerliezenEntity Validate(EY.com.eBook.RuleEngine.AY2015.AangifteVenBAppTypes.CompenseerbareVerliezenEntity entity, string location, ref WorksheetSetData worksheetset)
        {
            entity.IsValid = true;
            worksheetset.ClearMessages(TypeId, entity.Id);
            /*
            if (!entity.Bedrag.HasValue || entity.Bedrag.Value==0)
            {
            worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Grootboek");
            entity.IsValid = false;
            }
            */

            return entity;
        }

        public EY.com.eBook.RuleEngine.AY2015.AangifteVenBAppTypes.TariefEntity Validate(EY.com.eBook.RuleEngine.AY2015.AangifteVenBAppTypes.TariefEntity entity, string location, ref WorksheetSetData worksheetset)
        {
            entity.IsValid = true;
            worksheetset.ClearMessages(TypeId, entity.Id);
            /*
            if (!entity.Bedrag.HasValue || entity.Bedrag.Value==0)
            {
            worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Grootboek");
            entity.IsValid = false;
            }
            */

            return entity;
        }

        public EY.com.eBook.RuleEngine.AY2015.AangifteVenBAppTypes.VoorafBetalingEntity Validate(EY.com.eBook.RuleEngine.AY2015.AangifteVenBAppTypes.VoorafBetalingEntity entity, string location, ref WorksheetSetData worksheetset)
        {
            entity.IsValid = true;
            worksheetset.ClearMessages(TypeId, entity.Id);
            /*
            if (!entity.Bedrag.HasValue || entity.Bedrag.Value==0)
            {
            worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Grootboek");
            entity.IsValid = false;
            }
            */

            return entity;
        }

        public EY.com.eBook.RuleEngine.AY2015.AangifteVenBAppTypes.VerrekenbareVoorheffingEntity Validate(EY.com.eBook.RuleEngine.AY2015.AangifteVenBAppTypes.VerrekenbareVoorheffingEntity entity, string location, ref WorksheetSetData worksheetset)
        {
            entity.IsValid = true;
            worksheetset.ClearMessages(TypeId, entity.Id);
            /*
            if (!entity.Bedrag.HasValue || entity.Bedrag.Value==0)
            {
            worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Grootboek");
            entity.IsValid = false;
            }
            */

            return entity;
        }

        public EY.com.eBook.RuleEngine.AY2015.AangifteVenBAppTypes.TaxShelterEntity Validate(EY.com.eBook.RuleEngine.AY2015.AangifteVenBAppTypes.TaxShelterEntity entity, string location, ref WorksheetSetData worksheetset)
        {
            entity.IsValid = true;
            worksheetset.ClearMessages(TypeId, entity.Id);
            /*
            if (!entity.Bedrag.HasValue || entity.Bedrag.Value==0)
            {
            worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Grootboek");
            entity.IsValid = false;
            }
            */

            return entity;
        }

        public EY.com.eBook.RuleEngine.AY2015.AangifteVenBAppTypes.GrootteVennootschapeEntity Validate(EY.com.eBook.RuleEngine.AY2015.AangifteVenBAppTypes.GrootteVennootschapeEntity entity, string location, ref WorksheetSetData worksheetset)
        {
            entity.IsValid = true;
            worksheetset.ClearMessages(TypeId, entity.Id);
            /*
            if (!entity.Bedrag.HasValue || entity.Bedrag.Value==0)
            {
            worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Grootboek");
            entity.IsValid = false;
            }
            */

            return entity;
        }

        #endregion
        // VALIDATION
        
        // CALCULATION
        
        // BOOKINGS
        
        // IMPORT PREVIOUS


        
    }
}
  