
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.RuleEngine.Core;
using EY.com.eBook.RuleEngine.Core.Interfaces;
using EY.com.eBook.RuleEngine.Data;
using EY.com.eBook.Core;

namespace EY.com.eBook.RuleEngine.AY2015
{
    
    public class RisicoKapitaalApp
    {
        public static WorksheetTypeSettings Settings = new WorksheetTypeSettings
                                                      {
                                                        Clients = true
                                                        ,
                                                        Suppliers = true
                                                        ,
                                                        AllAccounts = true
                                                        , 
                                                        PerformsBookings=true
                                                      };
                                                      
        public static Guid TypeId = new Guid("a733b82a-e152-4916-9d77-2ed1b39e2d36");

        public List<string> ImportablePreviousTypes  = new List<string>() {  "EY.com.eBook.RuleEngine.AY2015.FacturatieApp"  };

        public Guid GetMyType() { return TypeId; }

        public void SetMinimal()
        {
            if (Data == null)
            {
                Data = new EY.com.eBook.RuleEngine.AY2015.RisicoKapitaalAppTypes.WPRisicokapitaal();
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.Fiche == null)
            {
                Data.Fiche = new EY.com.eBook.RuleEngine.AY2015.RisicoKapitaalAppTypes.FicheItem
                {
                    Id = Guid.NewGuid()
                    ,
                    LastUpdated = null
                    ,
                    IsValid = true
                    ,
                    PreviousImported = false

                };
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.EigenVermogens == null)
            {
                Data.EigenVermogens = new List<EY.com.eBook.RuleEngine.AY2015.RisicoKapitaalAppTypes.EigenVermogen>(); ;
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.Bestanddelen == null)
            {
                Data.Bestanddelen = new List<EY.com.eBook.RuleEngine.AY2015.RisicoKapitaalAppTypes.Bestanddeel>(); ;
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.Wijzigingen == null)
            {
                Data.Wijzigingen = new List<EY.com.eBook.RuleEngine.AY2015.RisicoKapitaalAppTypes.WijzigingItem>(); ;
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.Tarief == null)
            {
                Data.Tarief = new EY.com.eBook.RuleEngine.AY2015.RisicoKapitaalAppTypes.TariefItem
                {
                    Id = Guid.NewGuid()
                    ,
                    LastUpdated = null
                    ,
                    IsValid = true
                    ,
                    PreviousImported = false

                };
                Data.LastUpdated = DateTime.Now;
            }
            if (Data.Verminderingen == null)
            {
                Data.Verminderingen = new EY.com.eBook.RuleEngine.AY2015.RisicoKapitaalAppTypes.VerminderingItem
                {
                    Id = Guid.NewGuid()
                    ,
                    LastUpdated = null
                    ,
                    IsValid = true
                    ,
                    PreviousImported = false

                };
                Data.LastUpdated = DateTime.Now;
            }

            /*
        * new NID
            if (Data.Historiek == null)
            {
                Data.Historiek = new List<EY.com.eBook.RuleEngine.AY2015.RisicoKapitaalAppTypes.HistoryItem>(); ;
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.AanpassingenHistoriek == null)
            {
                Data.AanpassingenHistoriek = new List<EY.com.eBook.RuleEngine.AY2015.RisicoKapitaalAppTypes.HistoriekAanpassingItem>(); ;
                Data.LastUpdated = DateTime.Now;
            }
            */
        }
        
        public string ToXml() 
        {
            return CoreHelper.SerializeToString(Data);
        }

        public void LoadXml(string xml)
        {
            if (!string.IsNullOrEmpty(xml)) {
                Data = CoreHelper.DeserializeFromString<EY.com.eBook.RuleEngine.AY2015.RisicoKapitaalAppTypes.WPRisicokapitaal>(xml);
            }
            SetMinimal();
            
        }
        
        public EY.com.eBook.RuleEngine.AY2015.RisicoKapitaalAppTypes.WPRisicokapitaal Data { get; set; }


        public void ForceRecalculation(ref WorksheetSetData worksheetset)
        {
            Data.LastUpdated = new DateTime(1980, 1, 1);
            // Cleanup old history with invalid data (empty history assessment)
            /*
        * new NID
            Data.Historiek.RemoveAll(h => h.AssessmentYear == null);
             * */
            Data.Wijzigingen.ForEach(e => e.LastUpdated = null);
            

            Calculate(ref worksheetset);
        }

        public void Calculate(ref WorksheetSetData worksheetset)
        {
            
            foreach (RisicoKapitaalAppTypes.WijzigingItem wijziging in Data.Wijzigingen.Where(w=>!w.LastUpdated.HasValue ||w.LastUpdated > Data.LastUpdated).ToList())
            {
                UpdateWijzigingen(wijziging, ref worksheetset);
            }
            /*
        * new NID
            foreach (RisicoKapitaalAppTypes.HistoryItem history in Data.Historiek.ToList())
            {
                UpdateHistoriek(history, ref worksheetset);
            }
            */
            //worksheetset.Messages.RemoveAll(m => m.ConnectionGuid == TypeId);
            CalculateTarif(ref worksheetset);
            CalculateVerminderingen(ref worksheetset);

            Data.Fiche.NotioneelPercentage = Data.Tarief.tarief;

            CalculateFiche(ref worksheetset);

            Data.LastUpdated = DateTime.Now;
        }

        private void CalculateVerminderingen(ref WorksheetSetData worksheetset)
        {
            Data.Verminderingen.VerminderingInEER = Data.Verminderingen.VerminderingInEER.HasValue ? Data.Verminderingen.VerminderingInEER.Value : 0;
            Data.Verminderingen.VerminderingBuitenEER = Data.Verminderingen.VerminderingBuitenEER.HasValue ? Data.Verminderingen.VerminderingBuitenEER.Value : 0;
        }

        private void CalculateTarif(ref WorksheetSetData worksheetset)
        {
            Data.Tarief.startDatum = worksheetset.DataContract.StartDate;
            Data.Tarief.eindDatum = worksheetset.DataContract.EndDate;
            Data.Tarief.laatstebeltijdperk = worksheetset.Settings.PreviousPeriodSmall;
            Data.Tarief.voorlaatstebeltijdperk = worksheetset.Settings.BeforePreviousPeriodSmall;


            CoefficientDataContract cdc = null;

            if ((worksheetset.Settings.PreviousPeriodSmall && worksheetset.Settings.BeforePreviousPeriodSmall) || (worksheetset.Settings.CurrentPeriodSmall && worksheetset.Settings.FirstBookyear))
            {

                cdc = worksheetset.GetCoefficient("NID_VERHOOGD", worksheetset.DataContract.AssessmentYear);
                Data.Fiche.TariefKleineVennootschap = true;
            }
            else
            {
                cdc = worksheetset.GetCoefficient("NID", worksheetset.DataContract.AssessmentYear);
                Data.Fiche.TariefKleineVennootschap = false;
            }

            DateTime enddate = Data.Tarief.eindDatum.Value;
            DateTime calcEnd = Data.Tarief.startDatum.Value.AddDays(-1).AddYears(1);
            if (calcEnd.Year == enddate.Year && calcEnd.Month == enddate.Month && calcEnd.Day == enddate.Day)
            {
                Data.Tarief.Default = true;
                Data.Tarief.tarief = cdc.Value;
            }
            else
            {
                // FIX FOR BIZTAX CALC
                int days = (worksheetset.DataContract.EndDate - worksheetset.DataContract.EndDate.AddDays(1).AddYears(-1).AddDays(-1)).Days;
                Data.Tarief.tarief = ((cdc.Value / days) * (decimal)(enddate.AddDays(1) - Data.Tarief.startDatum.Value).TotalDays);
                Data.Tarief.Default = false;
            }
            Data.Fiche.NotioneelPercentage = Data.Tarief.tarief;
            
            
        }

        private void CalculateFiche(ref WorksheetSetData worksheetset)
        {
            Data.Fiche.EigenVermogen = Data.EigenVermogens.Sum(d => d.VorigBj);
            Data.Fiche.EigenAandelen = Data.Bestanddelen.Where(e => e.Type!=null && e.Type.Id.ToString().ToLower() == "96f3de0b-8a57-4d47-a4ef-0a0aa4c1bea8").Sum(d => d.VorigBj);

            Data.Fiche.FinancVasteActiva = Data.Bestanddelen.Where(e => e.Type != null && e.Type.Id.ToString().ToLower() == "a1219a9b-6355-4c5b-a43d-dc1931ce1a9a").Sum(d => d.VorigBj);
            Data.Fiche.AandelenDBI = Data.Bestanddelen.Where(e => e.Type != null && e.Type.Id.ToString().ToLower() == "60d0a7c4-1553-485c-85be-3e6228736650").Sum(d => d.VorigBj);
            
            /*
            Data.Fiche.AandelenBelegging = Data.Bestanddelen.Where(e => e.Type != null && e.Type.Id.ToString().ToLower() == "86169623-3b27-435c-bf65-7932204b31bc").Sum(d => d.VorigBj);
            Data.Fiche.InrichtingVerdrag = Data.Bestanddelen.Where(e => e.Type != null && e.Type.Id.ToString().ToLower() == "b5c8aa77-a3d0-43a9-9798-33cd865f9340").Sum(d => d.VorigBj);
            Data.Fiche.OnroerendVerdrag = Data.Bestanddelen.Where(e => e.Type != null && e.Type.Id.ToString().ToLower() == "803b2f0f-7ce2-4d62-9675-bb7eecf04598").Sum(d => d.VorigBj);
            */

            Data.Fiche.MvaOnredelijk = Data.Bestanddelen.Where(e => e.Type != null && e.Type.Id.ToString().ToLower() == "97f9d1aa-364e-493b-940f-769cee502bf2").Sum(d => d.VorigBj);
            Data.Fiche.BestanddelenBelegging = Data.Bestanddelen.Where(e => e.Type!=null && e.Type.Id.ToString().ToLower() == "a4982a60-b8f5-40e9-af6f-659ebf27d8f6").Sum(d => d.VorigBj);
            Data.Fiche.OnroerendeBedrijfsleiders = Data.Bestanddelen.Where(e => e.Type != null && e.Type.Id.ToString().ToLower() == "5223b43c-f421-4222-9480-a5750e736835").Sum(d => d.VorigBj);
            Data.Fiche.UitgedrukteMeerwaarden = Data.Bestanddelen.Where(e => e.Type != null && e.Type.Id.ToString().ToLower() == "748adf93-8ac7-42de-baf2-e9b3b59868dc").Sum(d => d.VorigBj);
            Data.Fiche.Belastingkrediet = Data.Bestanddelen.Where(e => e.Type != null && e.Type.Id.ToString().ToLower() == "6c5f1e7f-d8a8-4ee1-9422-32a4b9ef1a1e").Sum(d => d.VorigBj);
            Data.Fiche.KapitaalSubsidies = Data.Bestanddelen.Where(e => e.Type != null && e.Type.Id.ToString().ToLower() == "45270189-35ca-4d6e-8b3d-0e27fc06b314").Sum(d => d.VorigBj);
            Data.Fiche.Voorraadactualisering = Data.Bestanddelen.Where(e => e.Type != null && e.Type.Id.ToString().ToLower() == "fd102075-f991-4aea-97a2-700d1b84ea6a").Sum(d => d.VorigBj);
            Data.Fiche.Hoofdzetel = Data.Bestanddelen.Where(e => e.Type != null && e.Type.Id.ToString().ToLower() == "6a1c6084-6d02-4ee7-aac9-011835593c0f").Sum(d => d.VorigBj);
            Data.Fiche.BelgischeInrichting = Data.Bestanddelen.Where(e => e.Type != null && e.Type.Id.ToString().ToLower() == "dcf6e302-459c-46ff-8b83-6a865d7d2212").Sum(d => d.VorigBj);

            Data.Fiche.Totaal = Data.Bestanddelen.Sum(d => d.VorigBj);

            Data.Fiche.PositieveWijzigingen = Data.Wijzigingen.Where(e => e.PosNeg == "+").Sum(e => Math.Abs(e.TeWijzigen.GetValueOrDefault()));
            Data.Fiche.NegatieveWijzigingen = Data.Wijzigingen.Where(e => e.PosNeg == "-").Sum(e => Math.Abs(e.TeWijzigen.GetValueOrDefault()));

            Data.Fiche.WijzigingenCumul = Data.Fiche.PositieveWijzigingen.GetValueOrDefault() - Data.Fiche.NegatieveWijzigingen.GetValueOrDefault();


            Data.Fiche.RisicoKapitaalBelastbaar = Data.Fiche.EigenVermogen.GetValueOrDefault() - Data.Fiche.Totaal.GetValueOrDefault() + Data.Fiche.PositieveWijzigingen.GetValueOrDefault() - Data.Fiche.NegatieveWijzigingen.GetValueOrDefault();

            if (Data.Fiche.RisicoKapitaalBelastbaar < 0) Data.Fiche.RisicoKapitaalBelastbaar = 0;


            Data.Fiche.Aftrekbaar = Math.Round(Data.Fiche.RisicoKapitaalBelastbaar.GetValueOrDefault() * Data.Fiche.NotioneelPercentage.GetValueOrDefault() / 100, 2, MidpointRounding.AwayFromZero);

            Data.Fiche.VerminderingInEER = Data.Verminderingen.VerminderingInEER;
            Data.Fiche.VerminderingBuitenEER = Data.Verminderingen.VerminderingBuitenEER;
            Data.Fiche.AftrekbaarNaAftrek = Data.Fiche.Aftrekbaar.GetValueOrDefault() - Data.Fiche.VerminderingInEER.GetValueOrDefault() - Data.Fiche.VerminderingBuitenEER.GetValueOrDefault();

            if (Data.Fiche.AftrekbaarNaAftrek < 0) Data.Fiche.AftrekbaarNaAftrek = 0;
            int assess = worksheetset.DataContract.AssessmentYear;

           
            Data.Fiche.SaldoBelgischResterend = worksheetset.DBIApp.Data.DBI.BelastbareBasisResterend;

            decimal toDistribute = Data.Fiche.SaldoBelgischResterend.HasValue ? Data.Fiche.SaldoBelgischResterend.Value : 0;

            decimal huidigAftrekbaar = 0;
            if(toDistribute > 0 ) {
                //huidigAftrekbaar = Data.Fiche.Aftrekbaar.GetValueOrDefault() > toDistribute ? toDistribute : Data.Fiche.Aftrekbaar.GetValueOrDefault();
                huidigAftrekbaar = Data.Fiche.AftrekbaarNaAftrek.GetValueOrDefault() > toDistribute ? toDistribute : Data.Fiche.AftrekbaarNaAftrek.GetValueOrDefault();
            }
            
            Data.Fiche.Aanwending = Data.Fiche.SaldoBelgischResterend.GetValueOrDefault() - Data.Fiche.BelastbareBasisResterend.GetValueOrDefault();
            Data.Fiche.WerkelijkAfgetrokken = huidigAftrekbaar;

            /* old NID
            Data.Fiche.SaldoVorigeAJGevormd = 0;
            Data.Fiche.WerkelijkVorigAJ = 0;
            
            
          */
            Data.Fiche.OverdraagbaarSaldo = Data.Fiche.Aftrekbaar.GetValueOrDefault() - Data.Fiche.WerkelijkAfgetrokken.GetValueOrDefault();
            Data.Fiche.BelastbareBasisResterend = toDistribute - huidigAftrekbaar;
            /* MOVE TO HISTORY
            Data.Fiche.TotaalAftrekAJ = Data.Fiche.WerkelijkAfgetrokken + Data.Fiche.WerkelijkVorigAJ;

            Data.Fiche.SaldoRisicoOverdraagbaarForm = Data.Fiche.OverdraagbaarSaldo + Data.Fiche.SaldoVorigeAJGevormd - Data.Fiche.WerkelijkVorigAJ;
            Data.Fiche.SaldoRisicoOverdraagbaar = Data.Fiche.SaldoRisicoOverdraagbaarForm;
            */

        }

        #region AddUpdateDelete

        public void UpdateFiche(EY.com.eBook.RuleEngine.AY2015.RisicoKapitaalAppTypes.FicheItem entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2015.RisicoKapitaalAppTypes.FicheItem original = Data.Fiche;

            entity = Validate(entity, "Fiche", ref worksheetset);
            if (entity.IsValid)
            {

                Data.Fiche = entity;
                Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
            }
        }

        public void AddToEigenVermogens(EY.com.eBook.RuleEngine.AY2015.RisicoKapitaalAppTypes.EigenVermogen entity, ref WorksheetSetData worksheetset)
        {
            entity = Validate(entity, "EigenVermogens", ref worksheetset);
            if (entity.IsValid)
            {
                entity.VorigBjImported = entity.VorigBj;
                entity.HuidigBjImported = entity.HuidigBj;
                entity.Evolutie = entity.HuidigBj - entity.VorigBj;
                Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
                Data.EigenVermogens.Add(entity);
            }
        }

        public void DeleteEigenVermogens(Guid rowId, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2015.RisicoKapitaalAppTypes.EigenVermogen original = Data.EigenVermogens.FirstOrDefault(e => e.Id == rowId);
            if (original != null)
            {
                
                /*
                if (original.Boeken.HasValue && original.Boeken.Value)
                {
                    worksheetset.DeleteBooking(FacturatieApp.TypeId, "OpTeMakenCN", original.Id, null);
                    // DELETE BOOKING
                }*/
                Data.EigenVermogens.Remove(original);
                Data.LastUpdated = DateTime.Now;
            }
        }

        public void UpdateEigenVermogens(EY.com.eBook.RuleEngine.AY2015.RisicoKapitaalAppTypes.EigenVermogen entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2015.RisicoKapitaalAppTypes.EigenVermogen original = Data.EigenVermogens.FirstOrDefault(e => e.Id == entity.Id);
            if (original == null)
            {
                AddToEigenVermogens(entity, ref worksheetset);
            }
            else
            {
                entity = Validate(entity, "EigenVermogens", ref worksheetset);
                if (entity.IsValid)
                {
                    RisicoKapitaalAppTypes.WijzigingItem witem = Data.Wijzigingen.FirstOrDefault(w => w.Bestaand == true && w.BestaandTeWijzigen.Id == entity.Id.ToString());
                    if (witem == null)
                    {
                        entity.VorigBj = entity.VorigBjImported;
                        entity.HuidigBj = entity.HuidigBjImported;
                    }
                    entity.Evolutie = entity.HuidigBj - entity.VorigBj;
                    int idx = Data.EigenVermogens.IndexOf(original);
                    Data.EigenVermogens.RemoveAt(idx);
                    Data.EigenVermogens.Insert(idx, entity);
                    Data.LastUpdated = DateTime.Now;
                    entity.LastUpdated = DateTime.Now;
                }
            }
        }

        public void AddToBestanddelen(EY.com.eBook.RuleEngine.AY2015.RisicoKapitaalAppTypes.Bestanddeel entity, ref WorksheetSetData worksheetset)
        {
            entity = Validate(entity, "Bestanddelen", ref worksheetset);
            if (entity.IsValid)
            {
                entity.VorigBjImported = entity.VorigBj;
                entity.HuidigBjImported = entity.HuidigBj;
                entity.Evolutie = entity.HuidigBj - entity.VorigBj;
                Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
                Data.Bestanddelen.Add(entity);
            }
        }

        public void DeleteBestanddelen(Guid rowId, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2015.RisicoKapitaalAppTypes.Bestanddeel original = Data.Bestanddelen.FirstOrDefault(e => e.Id == rowId);
            if (original != null)
            {
                /*
                if (original.Boeken.HasValue && original.Boeken.Value)
                {
                    worksheetset.DeleteBooking(FacturatieApp.TypeId, "OpTeMakenCN", original.Id, null);
                    // DELETE BOOKING
                }*/
                Data.Bestanddelen.Remove(original);
                Data.LastUpdated = DateTime.Now;
            }
        }

        public void UpdateBestanddelen(EY.com.eBook.RuleEngine.AY2015.RisicoKapitaalAppTypes.Bestanddeel entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2015.RisicoKapitaalAppTypes.Bestanddeel original = Data.Bestanddelen.FirstOrDefault(e => e.Id == entity.Id);
            if (original == null)
            {
                AddToBestanddelen(entity, ref worksheetset);
            }
            else
            {
                entity = Validate(entity, "Bestanddelen", ref worksheetset);
                if (entity.IsValid)
                {
                    RisicoKapitaalAppTypes.WijzigingItem witem = Data.Wijzigingen.FirstOrDefault(w => w.Bestaand == true && w.BestaandTeWijzigen.Id == entity.Id.ToString());
                    if (witem == null)
                    {
                        entity.VorigBj = entity.VorigBjImported;
                        entity.HuidigBj = entity.HuidigBjImported;
                    }
                    entity.Evolutie = entity.HuidigBj - entity.VorigBj;
                    int idx = Data.Bestanddelen.IndexOf(original);
                    Data.Bestanddelen.RemoveAt(idx);
                    Data.Bestanddelen.Insert(idx, entity);
                    Data.LastUpdated = DateTime.Now;
                    entity.LastUpdated = DateTime.Now;
                }
            }
        }

        private void ResetEigenVermogen(ref RisicoKapitaalAppTypes.EigenVermogen ev)
        {
            ev.VorigBj = ev.VorigBjImported;
            ev.HuidigBj = ev.HuidigBjImported;
            ev.Evolutie = ev.HuidigBj - ev.VorigBj;
            ev.WerkelijkeWijziging = null;
        }

        private void ResetBestanddeel(ref RisicoKapitaalAppTypes.Bestanddeel bd)
        {
            bd.VorigBj = bd.VorigBjImported;
            bd.HuidigBj = bd.HuidigBjImported;
            bd.Evolutie = bd.HuidigBj - bd.VorigBj;
            bd.WerkelijkeWijziging = null;
        }
        

        private RisicoKapitaalAppTypes.WijzigingItem Calculatewijziging(RisicoKapitaalAppTypes.WijzigingItem entity, ref WorksheetSetData worksheetset)
        {
            if (entity.Bestaand == true)
            {
                RisicoKapitaalAppTypes.EigenVermogen ev = Data.EigenVermogens.FirstOrDefault(e => e.Id.ToString().ToLower() == entity.BestaandTeWijzigen.Id.ToLower());
                RisicoKapitaalAppTypes.Bestanddeel bd = Data.Bestanddelen.FirstOrDefault(e => e.Id.ToString().ToLower() == entity.BestaandTeWijzigen.Id.ToLower());
                if (ev != null)
                {
                    entity.Gewijzigde = ev.Omschrijving;
                }
                else if (bd != null)
                {
                    entity.Gewijzigde = bd.Omschrijving;
                }
            }
            else
            {
                switch (worksheetset.DataContract.Culture)
                {
                    case "nl-BE":
                        entity.Gewijzigde = entity.Grootboek.NL;
                        break;
                    case "fr-FR":
                        entity.Gewijzigde = entity.Grootboek.FR;
                        break;
                    case "en-US":
                        entity.Gewijzigde = entity.Grootboek.EN;
                        break;
                }
            }

            if (entity.Wijzigtype.Id.ToString().ToLower() == "310ac564-d0c0-4909-ab68-526e47d7e7bd") //1 Wijziging in de loop van het belastbare tijdperk
            {
                if (worksheetset.Settings.FirstBookyear)
                {
                    entity.TeWijzigen = entity.Wijziging;
                }
                else
                {
                    DateTime yearend = worksheetset.DataContract.StartDate.AddDays(-1).AddYears(1);
                    decimal quot = 12;
                    if (yearend > worksheetset.DataContract.EndDate)
                    {
                        quot -= worksheetset.DataContract.EndDate.FullMonthsBetween(yearend);
                    }
                    else if (yearend < worksheetset.DataContract.EndDate)
                    {
                        quot = yearend.MonthsBetween(worksheetset.DataContract.EndDate);

                        yearend = yearend.AddMonths((int)quot);
                        decimal days = 0;
                        if (yearend > worksheetset.DataContract.EndDate)
                        {
                            days = (decimal)Math.Abs((yearend - worksheetset.DataContract.EndDate).TotalDays);
                            quot -= Math.Round(days / DateTime.DaysInMonth(yearend.Year, yearend.Month), 3, MidpointRounding.AwayFromZero);
                        }
                        else if (yearend < worksheetset.DataContract.EndDate)
                        {
                            days = (decimal)Math.Abs((worksheetset.DataContract.EndDate - yearend).TotalDays);
                            quot += Math.Round(days / DateTime.DaysInMonth(yearend.Year, yearend.Month), 3, MidpointRounding.AwayFromZero);
                        }
                        quot += 12;
                    }
                    quot = entity.DatumTransactie.Value.FullMonthsBetween(worksheetset.DataContract.EndDate) / quot;
                    entity.TeWijzigen = Math.Round(entity.Wijziging.Value * quot, 2, MidpointRounding.AwayFromZero);
                }

            }

            return entity;
        }

        private RisicoKapitaalAppTypes.WijzigingItem PerformWijziging(RisicoKapitaalAppTypes.WijzigingItem entity, ref WorksheetSetData worksheetset, bool delete)
        {
            RisicoKapitaalAppTypes.EigenVermogen ev=null;
            RisicoKapitaalAppTypes.Bestanddeel bd=null; 
            if (entity.BestaandTeWijzigen != null)
            {
                ev = Data.EigenVermogens.FirstOrDefault(e => e.Id.ToString() == entity.BestaandTeWijzigen.Id);
                bd = Data.Bestanddelen.FirstOrDefault(e => e.Id.ToString() == entity.BestaandTeWijzigen.Id);
                if (delete)
                {
                    if (entity.Bestaand == true)
                    {
                        if (ev != null)
                        {
                            ResetEigenVermogen(ref ev);
                        }
                        if (bd != null)
                        {
                            ResetBestanddeel(ref bd);
                        }
                    }
                    else
                    {
                        if (ev != null) Data.EigenVermogens.Remove(ev);
                        if (bd != null) Data.Bestanddelen.Remove(bd);
                    }
                    return null;
                }
            }

            decimal werkwijziging = entity.Wijziging.GetValueOrDefault();
            //decimal wijziging = entity.TeWijzigen.Value;
            if (entity.Bestaand == true)
            {
                
                switch (entity.Wijzigtype.Id.ToString().ToLower())
                {
                    case "310ac564-d0c0-4909-ab68-526e47d7e7bd": //1 Wijziging in de loop van het belastbare tijdperk
                        decimal wijziging = entity.TeWijzigen.GetValueOrDefault();
                        
                        switch (entity.AfOfToename.Id.ToString().ToLower())
                        {
                            case "e5c1abfa-47bc-4e0d-b287-3f10e5a752ed": // + toename
                                wijziging = Math.Abs(wijziging);
                                werkwijziging = Math.Abs(werkwijziging);
                                break;
                            case "396aee1e-878d-4ffe-8524-1b22a49607b3": // - afname
                                wijziging = 0-Math.Abs(wijziging);
                                werkwijziging = 0-Math.Abs(werkwijziging);
                                break;
                        }
                        if (ev != null)
                        {
                            if (!ev.WerkelijkeWijziging.HasValue || ev.WerkelijkeWijziging == 0)
                            {
                                ev.HuidigBj = ev.HuidigBj + wijziging;
                                ev.Evolutie = ev.Evolutie + wijziging;
                                ev.WerkelijkeWijziging = werkwijziging;
                            }
                            else
                            {
                                ev.HuidigBj = wijziging;
                                ev.Evolutie = wijziging;
                                ev.WerkelijkeWijziging = werkwijziging;
                            }
                            entity.PosNeg = wijziging > 0 ? "+" : "-";
                        }
                        else if (bd != null)
                        {
                            if (!bd.WerkelijkeWijziging.HasValue || bd.WerkelijkeWijziging == 0)
                            {
                                bd.HuidigBj = bd.HuidigBj + wijziging;
                                bd.Evolutie = bd.Evolutie + wijziging;
                                bd.WerkelijkeWijziging = werkwijziging;
                            }
                            else
                            {
                                bd.HuidigBj = wijziging;
                                bd.Evolutie = wijziging;
                                bd.WerkelijkeWijziging = werkwijziging;
                            }
                            entity.PosNeg = wijziging > 0 ? "-" : "+";
                        }
                        Data.Fiche.PositieveWijzigingen = Data.Wijzigingen.Where(e => e.PosNeg == "+").Sum(e => Math.Abs(e.Wijziging.Value));
                        Data.Fiche.NegatieveWijzigingen = Data.Wijzigingen.Where(e => e.PosNeg == "-").Sum(e => Math.Abs(e.Wijziging.Value));
                        break;
                    case "7edb61e5-fa4f-4115-887b-d644c3b36a6a": //2 Aanpassing van de begintoestand
                        if (ev != null)
                        {
                            ev.VorigBj = entity.VorigBj;
                            ev.Evolutie = ev.HuidigBj - ev.VorigBj;
                        }
                        else if (bd != null)
                        {
                            bd.VorigBj = entity.VorigBj;
                            bd.Evolutie = bd.HuidigBj - bd.VorigBj;
                        }
                        entity.PosNeg = string.Empty;
                        break;
                }
            }
            else
            {
                switch (entity.Wijzigtype.Id.ToString().ToLower())
                {
                    case "310ac564-d0c0-4909-ab68-526e47d7e7bd": //1 Wijziging in de loop van het belastbare tijdperk
                        decimal wijziging = entity.TeWijzigen.Value;
                       

                        if (entity.AfOfToename.Id.ToString().ToLower() == "396aee1e-878d-4ffe-8524-1b22a49607b3"
                            || (entity.AardToename!=null && entity.AardToename.Id.ToString().ToLower() == "c5a3de38-65cb-4800-9742-a2b12954d92f"))
                        {
                            wijziging = 0 - Math.Abs(wijziging);
                            werkwijziging = 0 - Math.Abs(werkwijziging);
                        }
                        else
                        {
                            wijziging = Math.Abs(wijziging);
                            werkwijziging = Math.Abs(werkwijziging);
                        }
                        if (entity.TypeNID != null)
                        {
                            if (bd != null) ResetBestanddeel(ref bd);
                            if (ev == null)
                            {
                                ev = new EY.com.eBook.RuleEngine.AY2015.RisicoKapitaalAppTypes.EigenVermogen
                                {
                                    Id = Guid.NewGuid()
                                    ,
                                    Omschrijving = entity.Gewijzigde
                                    ,
                                    Type = entity.TypeNID
                                    ,
                                    Grootboek = entity.Grootboek
                                    ,
                                    HuidigBjImported = wijziging
                                    ,
                                    VorigBj = 0
                                    ,
                                    VorigBjImported = 0
                                };
                                entity.BestaandTeWijzigen = new StringListItemDataContract {
                                    Id = ev.Id.ToString()
                                    , NL = ev.Omschrijving
                                    , EN = ev.Omschrijving
                                    , FR = ev.Omschrijving
                                };
                                Data.EigenVermogens.Add(ev);
                            }
                            ev.Type = entity.TypeNID;
                            ev.HuidigBj = wijziging;
                            ev.Evolutie = wijziging;
                            ev.WerkelijkeWijziging = werkwijziging;
                            entity.PosNeg = wijziging > 0 ? "+" : "-";

                        }
                        else if (entity.TypeAftrekNID != null)
                        {
                            if (ev != null) ResetEigenVermogen(ref ev);
                            if (bd == null)
                            {
                                bd = new EY.com.eBook.RuleEngine.AY2015.RisicoKapitaalAppTypes.Bestanddeel
                                {
                                    Id = Guid.NewGuid()
                                    ,
                                    Omschrijving = entity.Gewijzigde
                                    ,
                                    Type = entity.TypeAftrekNID
                                    ,
                                    Grootboek = entity.Grootboek
                                    ,
                                    HuidigBjImported = wijziging
                                    ,
                                    VorigBj = 0
                                    ,
                                    VorigBjImported = 0
                                };
                                entity.BestaandTeWijzigen = new StringListItemDataContract
                                {
                                    Id = bd.Id.ToString()
                                    ,
                                    NL = bd.Omschrijving
                                    ,
                                    EN = bd.Omschrijving
                                    ,
                                    FR = bd.Omschrijving
                                };
                                Data.Bestanddelen.Add(bd);
                            }
                            bd.Type = entity.TypeAftrekNID;
                            bd.HuidigBj = wijziging;
                            bd.Evolutie = wijziging;
                            bd.Overdracht = werkwijziging;
                            entity.PosNeg = wijziging > 0 ? "-" : "+";
                        }
                        Data.Fiche.PositieveWijzigingen = Data.Wijzigingen.Where(e => e.PosNeg == "+").Sum(e => Math.Abs(e.Wijziging.Value));
                        Data.Fiche.NegatieveWijzigingen = Data.Wijzigingen.Where(e => e.PosNeg == "-").Sum(e => Math.Abs(e.Wijziging.Value));
                        break;
                    case "7edb61e5-fa4f-4115-887b-d644c3b36a6a": //2 Aanpassing van de begintoestand
                        if (entity.TypeNID != null)
                        {
                            if (bd != null) ResetBestanddeel(ref bd);
                            if (ev == null)
                            {
                                ev = new EY.com.eBook.RuleEngine.AY2015.RisicoKapitaalAppTypes.EigenVermogen
                                {
                                    Id = Guid.NewGuid()
                                    ,
                                    Omschrijving = entity.Gewijzigde
                                    ,
                                    Type = entity.TypeNID
                                    ,
                                    Grootboek = entity.Grootboek
                                    ,
                                    HuidigBjImported = 0
                                    ,
                                    VorigBj = 0
                                    ,
                                    VorigBjImported = 0
                                };
                                entity.BestaandTeWijzigen = new StringListItemDataContract
                                {
                                    Id = ev.Id.ToString()
                                    ,
                                    NL = ev.Omschrijving
                                    ,
                                    EN = ev.Omschrijving
                                    ,
                                    FR = ev.Omschrijving
                                };
                                Data.EigenVermogens.Add(ev);
                            }
                            ev.Type = entity.TypeNID;
                            ev.VorigBj = entity.VorigBj;
                            ev.Evolutie = (ev.HuidigBj.HasValue ? ev.HuidigBj.Value : 0) - ev.VorigBj;
                            ev.WerkelijkeWijziging = entity.Wijziging;
                        }
                        else if (entity.TypeAftrekNID != null)
                        {
                            if (bd == null)
                            {
                                bd = new EY.com.eBook.RuleEngine.AY2015.RisicoKapitaalAppTypes.Bestanddeel
                                {
                                    Id = Guid.NewGuid()
                                    ,
                                    Omschrijving = entity.Gewijzigde
                                    ,
                                    Type = entity.TypeAftrekNID
                                    ,
                                    Grootboek = entity.Grootboek
                                    ,
                                    HuidigBjImported = 0
                                    ,
                                    VorigBj = 0
                                    ,
                                    VorigBjImported = 0
                                };
                                entity.BestaandTeWijzigen = new StringListItemDataContract
                                {
                                    Id = bd.Id.ToString()
                                    ,
                                    NL = bd.Omschrijving
                                    ,
                                    EN = bd.Omschrijving
                                    ,
                                    FR = bd.Omschrijving
                                };
                                Data.Bestanddelen.Add(bd);
                            }
                            bd.Type = entity.TypeAftrekNID;
                            bd.VorigBj = entity.VorigBj;
                            bd.Evolutie = (bd.HuidigBj.HasValue ? bd.HuidigBj.Value : 0) - bd.VorigBj;
                            bd.WerkelijkeWijziging = entity.Wijziging;
                        }
                        entity.PosNeg = string.Empty;
                        break;
                }
            }
            entity.XbrlValue = entity.PosNeg == "-" ? 0 - Math.Abs(entity.TeWijzigen.GetValueOrDefault()) : Math.Abs(entity.TeWijzigen.GetValueOrDefault());
            return entity;
        }

        public void AddToWijzigingen(EY.com.eBook.RuleEngine.AY2015.RisicoKapitaalAppTypes.WijzigingItem entity, ref WorksheetSetData worksheetset)
        {
            entity = Validate(entity, "Wijzigingen", ref worksheetset);
            if (entity.IsValid)
            {
                entity = Calculatewijziging(entity, ref worksheetset);
                entity = PerformWijziging(entity, ref worksheetset,false);
                Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
                Data.Wijzigingen.Add(entity);
            }
        }

        public void DeleteWijzigingen(Guid rowId, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2015.RisicoKapitaalAppTypes.WijzigingItem original = Data.Wijzigingen.FirstOrDefault(e => e.Id == rowId);
            if (original != null)
            {
                PerformWijziging(original, ref worksheetset, true);

                /*
                if (original.Boeken.HasValue && original.Boeken.Value)
                {
                    worksheetset.DeleteBooking(FacturatieApp.TypeId, "OpTeMakenCN", original.Id, null);
                    // DELETE BOOKING
                }*/
                Data.Wijzigingen.Remove(original);
                Data.LastUpdated = DateTime.Now;
            }
        }

        public void UpdateWijzigingen(EY.com.eBook.RuleEngine.AY2015.RisicoKapitaalAppTypes.WijzigingItem entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2015.RisicoKapitaalAppTypes.WijzigingItem original = Data.Wijzigingen.FirstOrDefault(e => e.Id == entity.Id);
            if (original == null)
            {
                AddToWijzigingen(entity, ref worksheetset);
            }
            else
            {
                entity = Validate(entity, "Wijzigingen", ref worksheetset);
                if (entity.IsValid)
                {
                    entity = Calculatewijziging(entity, ref worksheetset);
                    PerformWijziging(entity, ref worksheetset, true);
                    entity = PerformWijziging(entity, ref worksheetset, false);
                    int idx = Data.Wijzigingen.IndexOf(original);
                    Data.Wijzigingen.RemoveAt(idx);
                    Data.Wijzigingen.Insert(idx, entity);
                    Data.LastUpdated = DateTime.Now;
                    entity.LastUpdated = DateTime.Now;
                }
            }
        }

        public void UpdateTarief(EY.com.eBook.RuleEngine.AY2015.RisicoKapitaalAppTypes.TariefItem entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2015.RisicoKapitaalAppTypes.TariefItem original = Data.Tarief;

            entity = Validate(entity, "Tarief", ref worksheetset);
            if (entity.IsValid)
            {

                Data.Tarief = entity;
                Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
            }
        }
        /*
        public void AddToVerminderingen(EY.com.eBook.RuleEngine.AY2015.RisicoKapitaalAppTypes.VerminderingItem entity, ref WorksheetSetData worksheetset)
        {
            entity = Validate(entity, "Verminderingen", ref worksheetset);
            if (entity.IsValid)
            {
                Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
                Data.Verminderingen.//.Add(entity);
            }
        }*/

        public void UpdateVerminderingen(EY.com.eBook.RuleEngine.AY2015.RisicoKapitaalAppTypes.VerminderingItem entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2015.RisicoKapitaalAppTypes.VerminderingItem original = Data.Verminderingen;

            entity = Validate(entity, "Verminderingen", ref worksheetset);
            if (entity.IsValid)
            {

                Data.Verminderingen = entity;
                Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
            }
        }

        /*
        * new NID
        public void AddToHistoriek(EY.com.eBook.RuleEngine.AY2015.RisicoKapitaalAppTypes.HistoryItem entity, ref WorksheetSetData worksheetset)
        {
            entity = Validate(entity, "Historiek", ref worksheetset);
            if (entity.IsValid)
            {
                
                Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
                Data.Historiek.Add(entity);
            }
        }

        public void DeleteHistoriek(Guid rowId, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2015.RisicoKapitaalAppTypes.HistoryItem original = Data.Historiek.FirstOrDefault(e => e.Id == rowId);
            if (original != null)
            {
                
                Data.Historiek.Remove(original);
                Data.LastUpdated = DateTime.Now;
            }
        }

        public void UpdateHistoriek(EY.com.eBook.RuleEngine.AY2015.RisicoKapitaalAppTypes.HistoryItem entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2015.RisicoKapitaalAppTypes.HistoryItem original = Data.Historiek.FirstOrDefault(e => e.Id == entity.Id);
            if (original == null)
            {
                AddToHistoriek(entity, ref worksheetset);
            }
            else
            {
                entity = Validate(entity, "Historiek", ref worksheetset);
                if (entity.IsValid)
                {
                    int idx = Data.Historiek.IndexOf(original);
                    Data.Historiek.RemoveAt(idx);
                    Data.Historiek.Insert(idx, entity);
                    Data.LastUpdated = DateTime.Now;
                    entity.LastUpdated = DateTime.Now;
                }
            }
        }

        public void AddToAanpassingenHistoriek(EY.com.eBook.RuleEngine.AY2015.RisicoKapitaalAppTypes.HistoriekAanpassingItem entity, ref WorksheetSetData worksheetset)
        {
            entity = Validate(entity, "AanpassingenHistoriek", ref worksheetset);
            if (entity.IsValid)
            {
                PerformAdjustHistory(entity, false);
                Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
                Data.AanpassingenHistoriek.Add(entity);
            }
        }
        
        private void PerformAdjustHistory(RisicoKapitaalAppTypes.HistoriekAanpassingItem entity, bool delete)
        {
            RisicoKapitaalAppTypes.HistoryItem item = Data.Historiek.First(h => h.AssessmentYear.Id == entity.AssessmentYear.Id);
            if (item!=null)
            {
                item.BedragAftrek = item.BedragAftrekImport;
                item.Saldo = item.BedragAftrek - item.GebruiktHuidigBJ;
            }
            if (!delete)
            {
                if (item==null)
                {
                    item = new EY.com.eBook.RuleEngine.AY2015.RisicoKapitaalAppTypes.HistoryItem
                    {
                        Id = Guid.NewGuid()
                        ,
                        AssessmentYear = entity.AssessmentYear
                        ,
                        BedragAftrekImport = 0
                        ,
                        GebruiktHuidigBJ = 0
                        ,
                        LastUpdated = DateTime.Now
                    };
                    Data.Historiek.Add(item);
                }
                item.BedragAftrek = entity.BedragAftrek;
                item.Saldo = item.BedragAftrek - item.GebruiktHuidigBJ;
            }
        }

        public void DeleteAanpassingenHistoriek(Guid rowId, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2015.RisicoKapitaalAppTypes.HistoriekAanpassingItem original = Data.AanpassingenHistoriek.FirstOrDefault(e => e.Id == rowId);
            if (original != null)
            {
                PerformAdjustHistory(original, true);
                
                Data.AanpassingenHistoriek.Remove(original);
                Data.LastUpdated = DateTime.Now;
            }
        }

        public void UpdateAanpassingenHistoriek(EY.com.eBook.RuleEngine.AY2015.RisicoKapitaalAppTypes.HistoriekAanpassingItem entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2015.RisicoKapitaalAppTypes.HistoriekAanpassingItem original = Data.AanpassingenHistoriek.FirstOrDefault(e => e.Id == entity.Id);
            if (original == null)
            {
                AddToAanpassingenHistoriek(entity, ref worksheetset);
            }
            else
            {
                entity = Validate(entity, "AanpassingenHistoriek", ref worksheetset);
                if (entity.IsValid)
                {
                    PerformAdjustHistory(entity, false);
                    int idx = Data.AanpassingenHistoriek.IndexOf(original);
                    Data.AanpassingenHistoriek.RemoveAt(idx);
                    Data.AanpassingenHistoriek.Insert(idx, entity);
                    Data.LastUpdated = DateTime.Now;
                    entity.LastUpdated = DateTime.Now;
                }
            }
        }
        */
        #endregion

        #region VALIDATION

        public EY.com.eBook.RuleEngine.AY2015.RisicoKapitaalAppTypes.FicheItem Validate(EY.com.eBook.RuleEngine.AY2015.RisicoKapitaalAppTypes.FicheItem entity, string location, ref WorksheetSetData worksheetset)
        {
            entity.IsValid = true;
            worksheetset.ClearMessages(TypeId, entity.Id);
            /*
            if (!entity.Bedrag.HasValue || entity.Bedrag.Value==0)
            {
            worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Grootboek");
            entity.IsValid = false;
            }
            */

            return entity;
        }

        public EY.com.eBook.RuleEngine.AY2015.RisicoKapitaalAppTypes.EigenVermogen Validate(EY.com.eBook.RuleEngine.AY2015.RisicoKapitaalAppTypes.EigenVermogen entity, string location, ref WorksheetSetData worksheetset)
        {
            entity.IsValid = true;
            worksheetset.ClearMessages(TypeId, entity.Id);


            if (!entity.HuidigBjImported.HasValue)
            {
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "HuidigBjImported");
                entity.IsValid = false;
            }
            if (!entity.VorigBjImported.HasValue)
            {
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "VorigBjImported");
                entity.IsValid = false;
            }
            if (entity.Type==null)
            {
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Type");
                entity.IsValid = false;
            }
            if (entity.IsValid)
            {
                entity.HuidigBj = entity.HuidigBjImported;
                entity.VorigBj = entity.VorigBjImported;
                entity.Evolutie = entity.HuidigBj - entity.VorigBj;
            }
            

            return entity;
        }

        public EY.com.eBook.RuleEngine.AY2015.RisicoKapitaalAppTypes.Bestanddeel Validate(EY.com.eBook.RuleEngine.AY2015.RisicoKapitaalAppTypes.Bestanddeel entity, string location, ref WorksheetSetData worksheetset)
        {
            entity.IsValid = true;
            worksheetset.ClearMessages(TypeId, entity.Id);

            if (!entity.HuidigBj.HasValue && !entity.HuidigBjImported.HasValue)
            {
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "HuidigBjImported");
                entity.IsValid = false;
            }
            if (!entity.VorigBj.HasValue && !entity.VorigBjImported.HasValue)
            {
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "VorigBjImported");
                entity.IsValid = false;
            }
            if (entity.Type==null)
            {
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Type");
                entity.IsValid = false;
            }

            if (entity.IsValid)
            {
                entity.HuidigBj = entity.HuidigBjImported;
                entity.VorigBj = entity.VorigBjImported;
                entity.Evolutie = entity.HuidigBj - entity.VorigBj;
            }

            return entity;
        }

        public EY.com.eBook.RuleEngine.AY2015.RisicoKapitaalAppTypes.WijzigingItem Validate(EY.com.eBook.RuleEngine.AY2015.RisicoKapitaalAppTypes.WijzigingItem entity, string location, ref WorksheetSetData worksheetset)
        {
            entity.IsValid = true;
            worksheetset.ClearMessages(TypeId, entity.Id);

            if (entity.Bestaand.HasValue && entity.Bestaand.Value)
            {
                if (entity.BestaandTeWijzigen == null)
                {
                    worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "BestaandTeWijzigen");
                    entity.IsValid = false;
                }
            }
            else
            {
                if (entity.Grootboek == null)
                {
                    worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Grootboek");
                    entity.IsValid = false;
                }
            }

            if (entity.Wijzigtype == null)
            {
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Wijzigtype");
                entity.IsValid = false;
            }
            else
            {
                switch (entity.Wijzigtype.Id.ToString().ToLower())
                {
                    case "310ac564-d0c0-4909-ab68-526e47d7e7bd": //1 Wijziging in de loop van het belastbare tijdperk
                        if (entity.AfOfToename == null)
                        {
                            worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "AfOfToename");
                            entity.IsValid = false;
                        }
                        if ((!entity.Bestaand.HasValue || entity.Bestaand==false))
                        {
                            if (entity.TypeAftrekNID == null && entity.TypeNID == null)
                            {
                                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "ONEOREMPTY");
                                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "ONEOREMPTY");
                                entity.IsValid = false;
                            }
                            if (entity.TypeAftrekNID != null && entity.TypeNID != null)
                            {
                                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "ONEOREMPTY");
                                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "ONEOREMPTY");
                                entity.IsValid = false;
                            }
                        }
                        if (!entity.Wijziging.HasValue  || entity.Wijziging==0)
                        {
                            worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Wijziging");
                            entity.IsValid = false;
                        }
                        if (!entity.DatumTransactie.HasValue)
                        {
                            worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "DatumTransactie");
                            entity.IsValid = false;
                        }
                        else
                        {
                            if (entity.DatumTransactie.Value > worksheetset.DataContract.EndDate)
                            {
                                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "GREATERTHENENDDATE", location, entity.Id, "DatumTransactie");
                                entity.IsValid = false;
                            }
                        }

                        break;
                    case "7edb61e5-fa4f-4115-887b-d644c3b36a6a": //2 Aanpassing van de begintoestand
                        if (!entity.VorigBj.HasValue)
                        {
                            worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "VorigBj");
                            entity.IsValid = false;
                        }
                        break;
                }
            }
            if (entity.AardToename == null && entity.Bestaand==true)
            {
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "AardToename");
                entity.IsValid = false;
            }
            if (entity.Notitie == null)
            {
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Notitie");
                entity.IsValid = false;
            }

            return entity;
        }

        public EY.com.eBook.RuleEngine.AY2015.RisicoKapitaalAppTypes.TariefItem Validate(EY.com.eBook.RuleEngine.AY2015.RisicoKapitaalAppTypes.TariefItem entity, string location, ref WorksheetSetData worksheetset)
        {
            entity.IsValid = true;
            worksheetset.ClearMessages(TypeId, entity.Id);
            /*
            if (!entity.Bedrag.HasValue || entity.Bedrag.Value==0)
            {
            worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Grootboek");
            entity.IsValid = false;
            }
            */

            return entity;
        }

        public EY.com.eBook.RuleEngine.AY2015.RisicoKapitaalAppTypes.VerminderingItem Validate(EY.com.eBook.RuleEngine.AY2015.RisicoKapitaalAppTypes.VerminderingItem entity, string location, ref WorksheetSetData worksheetset)
        {
            entity.IsValid = true;
            worksheetset.ClearMessages(TypeId, entity.Id);
            
            if (entity.VerminderingInEER.Value<0)
            {
            worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "NEGATIVE", location, entity.Id, "VerminderingInEER");
            entity.IsValid = false;
            }
            if (entity.VerminderingBuitenEER.Value < 0)
            {
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "NEGATIVE", location, entity.Id, "VerminderingBuitenEER");
                entity.IsValid = false;
            }
            

            return entity;
        }

        /*
        * new NID
        public EY.com.eBook.RuleEngine.AY2015.RisicoKapitaalAppTypes.HistoryItem Validate(EY.com.eBook.RuleEngine.AY2015.RisicoKapitaalAppTypes.HistoryItem entity, string location, ref WorksheetSetData worksheetset)
        {
            entity.IsValid = true;
            worksheetset.ClearMessages(TypeId, entity.Id);

            if (entity.AssessmentYear == null)
            {
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "AssessmentYear");
                entity.IsValid = false;
            }


            if (!entity.BedragAftrekImport.HasValue && !entity.BedragAftrek.HasValue)
            {
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "BedragAftrekImport");
                entity.IsValid = false;
            }
            else
            {
                entity.BedragAftrek = entity.BedragAftrekImport.GetValueOrDefault();
            }
            
           // if (!entity.BedragAftrek.HasValue) entity.BedragAftrek = entity.BedragAftrekImport.GetValueOrDefault();

            return entity;
        }

        public EY.com.eBook.RuleEngine.AY2015.RisicoKapitaalAppTypes.HistoriekAanpassingItem Validate(EY.com.eBook.RuleEngine.AY2015.RisicoKapitaalAppTypes.HistoriekAanpassingItem entity, string location, ref WorksheetSetData worksheetset)
        {
            entity.IsValid = true;
            worksheetset.ClearMessages(TypeId, entity.Id);
            

            return entity;
        }
        */
        #endregion
        // VALIDATION

        public void ImportPrevious(ref WorksheetSetData wsd)
        {

            if (wsd.DataContract.PreviousFileId.HasValue)
            {
                if (wsd.PreviousSet.AssessmentYear == 2014)
                {
                    EY.com.eBook.RuleEngine.AY2014.WorksheetSetData pset = (EY.com.eBook.RuleEngine.AY2014.WorksheetSetData)wsd.PreviousSet;

                    EY.com.eBook.RuleEngine.AY2014.RisicoKapitaalApp previous = pset.RisicoKapitaalApp;

                    Data.EigenVermogens.RemoveAll(h => h.PreviousImported == true);

                    foreach (AY2014.RisicoKapitaalAppTypes.EigenVermogen ev in previous.Data.EigenVermogens)
                    {
                        decimal current = ev.HuidigBjImported.GetValueOrDefault();
                        current += ev.WerkelijkeWijziging.GetValueOrDefault();

                        Data.EigenVermogens.Add(new EY.com.eBook.RuleEngine.AY2015.RisicoKapitaalAppTypes.EigenVermogen
                        {
                            Auto = ev.Auto
                            ,
                            Grootboek = ev.Grootboek
                            ,
                            HuidigBjImported = current
                            ,
                            HuidigBj = current
                            ,
                            Evolutie = 0
                            ,
                            Id = ev.Id
                            ,
                            IsValid = true
                            ,
                            LastUpdated = new DateTime(1980, 1, 1)
                            ,
                            Omschrijving = ev.Omschrijving
                            ,
                            Overdracht = 0
                            ,
                            PreviousImported = true
                            ,
                            Type = ev.Type
                            ,
                            VorigBj = current
                            ,
                            VorigBjImported = current
                            ,
                            WerkelijkeWijziging = 0
                        });
                    }

                    Calculate(ref wsd); // ?
                }

                if (wsd.PreviousSet.AssessmentYear == 2014)
                {
                    EY.com.eBook.RuleEngine.AY2015.WorksheetSetData pset = (EY.com.eBook.RuleEngine.AY2015.WorksheetSetData)wsd.PreviousSet;

                    EY.com.eBook.RuleEngine.AY2015.RisicoKapitaalApp previous = pset.RisicoKapitaalApp;

                    Data.EigenVermogens.RemoveAll(h => h.PreviousImported == true);

                    foreach (AY2015.RisicoKapitaalAppTypes.EigenVermogen ev in previous.Data.EigenVermogens)
                    {
                        decimal current = ev.HuidigBjImported.GetValueOrDefault();
                        current += ev.WerkelijkeWijziging.GetValueOrDefault();

                        Data.EigenVermogens.Add(new EY.com.eBook.RuleEngine.AY2015.RisicoKapitaalAppTypes.EigenVermogen
                        {
                            Auto = ev.Auto
                            ,
                            Grootboek = ev.Grootboek
                            ,
                            HuidigBjImported = current
                            ,
                            HuidigBj = current
                            ,
                            Evolutie = 0
                            ,
                            Id = ev.Id
                            ,
                            IsValid = true
                            ,
                            LastUpdated = new DateTime(1980, 1, 1)
                            ,
                            Omschrijving = ev.Omschrijving
                            ,
                            Overdracht = 0
                            ,
                            PreviousImported = true
                            ,
                            Type = ev.Type
                            ,
                            VorigBj = current
                            ,
                            VorigBjImported = current
                            ,
                            WerkelijkeWijziging = 0
                        });
                    }

                    Calculate(ref wsd); // ?
                }
            }
        }

        // CALCULATION
        
        // BOOKINGS
        
        // IMPORT PREVIOUS


    }
}
  