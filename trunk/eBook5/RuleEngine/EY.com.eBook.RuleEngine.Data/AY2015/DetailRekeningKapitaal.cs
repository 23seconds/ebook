﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.RuleEngine.Core;
using EY.com.eBook.RuleEngine.Core.Interfaces;
using EY.com.eBook.RuleEngine.Data;
using EY.com.eBook.Core;
using System.Xml.Linq;
using System.Xml.Xsl;
using System.Globalization;
using System.IO;
using System.Xml.XPath;
using System.Xml;

namespace EY.com.eBook.RuleEngine.AY2015
{

    public class DetailRekeningKapitaalApp
    {
        /*
        public static WorksheetTypeSettings Settings = new WorksheetTypeSettings
        {
            Clients = true
          ,
            Suppliers = true
          ,
            AllAccounts = true
          ,
            PerformsBookings = true
        };
        */
        public EY.com.eBook.RuleEngine.AY2015.DetailRekeningKapitaalAppTypes.DetailRekeningKapitaal Data { get; set; }

        public static Guid TypeId = new Guid("d55d6cf5-a9d4-421d-a420-0ae970d7bc5f");

        //public List<string> ImportablePreviousTypes = new List<string>() { "EY.com.eBook.RuleEngine.AY2012.FacturatieApp" };

        public Guid GetMyType() { return TypeId; }

        public void SetMinimal()
        {
            if (Data == null)
            {
                Data = new EY.com.eBook.RuleEngine.AY2015.DetailRekeningKapitaalAppTypes.DetailRekeningKapitaal();
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.Overzicht == null)
            {
                Data.Overzicht = new List<EY.com.eBook.RuleEngine.AY2015.DetailRekeningKapitaalAppTypes.DetailRekeningKapitaalOverzicht>();
                Data.LastUpdated = DateTime.Now;
            }
        }

        public string ToXml()
        {
            return CoreHelper.SerializeToString(Data);
        }

        public void LoadXml(string xml)
        {
            if (!string.IsNullOrEmpty(xml))
            {
                Data = CoreHelper.DeserializeFromString<EY.com.eBook.RuleEngine.AY2015.DetailRekeningKapitaalAppTypes.DetailRekeningKapitaal>(xml);
            }
            SetMinimal();

        }

       


        public void ForceRecalculation(ref WorksheetSetData worksheetset)
        {
            Data.LastUpdated = new DateTime(1980, 1, 1);
            Data.Overzicht.ForEach(e => e.LastUpdated = null);

            Calculate(ref worksheetset);
        }

        public void Calculate(ref WorksheetSetData worksheetset)
        {
            worksheetset.ClearMessages(TypeId);
            int cnt = 1;
            foreach (DetailRekeningKapitaalAppTypes.DetailRekeningKapitaalOverzicht entity in Data.Overzicht.ToList())
            {
                UpdateOverzicht(entity, ref worksheetset);
                entity.Seq = null;
                if(entity.Details!=null && entity.Details.Count>0) {
                    entity.Seq = cnt;
                    cnt++;
                }
            }

           

            DateTime lastCalc = DateTime.Now;
            
            Data.LastUpdated = lastCalc;
        }
        /*
        public void AddToOpTeMakenCN(EY.com.eBook.RuleEngine.AY2015.FacturatieAppTypes.FacturatieKlantCN entity, ref WorksheetSetData worksheetset)
        {
            entity = Validate(entity, "OpTeMakenCN", ref worksheetset);
            if (entity.IsValid)
            {
                Data.OpTeMakenCN.Add(entity);
                if (string.IsNullOrEmpty(entity.Omschrijving))
                {
                    entity.Omschrijving = string.Format("{0} {1}", entity.Klant.NL, entity.FactuurNr);
                }
                if (entity.Boeken.HasValue && entity.Boeken.Value)
                {
                    worksheetset.AddBooking(FacturatieApp.TypeId, "OpTeMakenCN", entity.Id, null, new List<BookingLineDataContract>() {
                        new BookingLineDataContract { AccountNr = entity.Grootboek, Amount=entity.Bedrag.Value }
                            , new BookingLineDataContract { AccountNr = worksheetset.GetListAccount("4441"), Amount=0-entity.Bedrag.Value}
                    });
                }
                Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
            }
        }

        public void UpdateOpTeMakenCN(EY.com.eBook.RuleEngine.AY2015.FacturatieAppTypes.FacturatieKlantCN entity, ref WorksheetSetData worksheetset)
        {
            entity = Validate(entity, "OpTeMakenCN", ref worksheetset);
            EY.com.eBook.RuleEngine.AY2015.FacturatieAppTypes.FacturatieKlantCN original = Data.OpTeMakenCN.FirstOrDefault(e => e.Id == entity.Id);
            if (original == null)
            {
                AddToOpTeMakenCN(entity, ref worksheetset);
            }
            else
            {
                if ((entity != original || !entity.LastUpdated.HasValue) && entity.IsValid)
                {
                    if (string.IsNullOrEmpty(entity.Omschrijving))
                    {
                        entity.Omschrijving = string.Format("{0} {1}", entity.Klant.NL, entity.FactuurNr);
                    }
                    entity.Boeken = entity.Boeken.HasValue ? entity.Boeken.Value : false;
                    original.Boeken = original.Boeken.HasValue ? original.Boeken.Value : false;

                    if (original.Boeken.Value && !entity.Boeken.Value)
                    {
                        worksheetset.DeleteBooking(FacturatieApp.TypeId, "OpTeMakenCN", original.Id, null);
                    }
                    else if (!original.Boeken.Value && entity.Boeken.Value)
                    {
                        worksheetset.AddBooking(FacturatieApp.TypeId, "OpTeMakenCN", entity.Id, null, new List<BookingLineDataContract>() {
                            new BookingLineDataContract { AccountNr = entity.Grootboek, Amount=entity.Bedrag.Value }
                            , new BookingLineDataContract { AccountNr = worksheetset.GetListAccount("4441"), Amount=0-entity.Bedrag.Value}
                        });
                    }
                    else if (original.Boeken.Value && entity.Boeken.Value && (original.Bedrag != entity.Bedrag || original.Grootboek != entity.Grootboek))
                    {
                        worksheetset.UpdateBooking(FacturatieApp.TypeId, "OpTeMakenCN", entity.Id, null, new List<BookingLineDataContract>() {
                            new BookingLineDataContract { AccountNr = entity.Grootboek, Amount=entity.Bedrag.Value }
                            , new BookingLineDataContract { AccountNr = worksheetset.GetListAccount("4441"), Amount=0-entity.Bedrag.Value}
                        });
                    }
                    else if (!entity.LastUpdated.HasValue && entity.Boeken.Value)
                    {
                        worksheetset.UpdateBooking(FacturatieApp.TypeId, "OpTeMakenCN", entity.Id, null, new List<BookingLineDataContract>() {
                            new BookingLineDataContract { AccountNr = entity.Grootboek, Amount=entity.Bedrag.Value }
                            , new BookingLineDataContract { AccountNr = worksheetset.GetListAccount("4441"), Amount=0-entity.Bedrag.Value}
                        });
                    }
                    int idx = Data.OpTeMakenCN.IndexOf(original);
                    Data.OpTeMakenCN.RemoveAt(idx);
                    Data.OpTeMakenCN.Insert(idx, entity);
                    Data.LastUpdated = DateTime.Now;
                    entity.LastUpdated = DateTime.Now;
                }

            }

        }

        public void DeleteOpTeMakenCN(Guid rowId, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2015.FacturatieAppTypes.FacturatieKlantCN original = Data.OpTeMakenCN.FirstOrDefault(e => e.Id == rowId);
            if (original != null)
            {
                if (original.Boeken.HasValue && original.Boeken.Value)
                {
                    worksheetset.DeleteBooking(FacturatieApp.TypeId, "OpTeMakenCN", original.Id, null);
                    // DELETE BOOKING
                }
                Data.OpTeMakenCN.Remove(original);
                Data.LastUpdated = DateTime.Now;
            }

        }

        public void AddToOpTeMakenFacturen(EY.com.eBook.RuleEngine.AY2015.FacturatieAppTypes.FacturatieKlant entity, ref WorksheetSetData worksheetset)
        {
            entity = Validate(entity, "OpTeMakenFacturen", ref worksheetset);
            if (entity.IsValid)
            {
                if (string.IsNullOrEmpty(entity.Omschrijving))
                {
                    entity.Omschrijving = string.Format("{0} {1}", entity.Klant.NL, entity.FactuurNr);
                }
                Data.OpTeMakenFacturen.Add(entity);
                if (entity.Boeken.HasValue && entity.Boeken.Value)
                {
                    worksheetset.AddBooking(FacturatieApp.TypeId, "OpTeMakenFacturen", entity.Id, null, new List<BookingLineDataContract>() {
                        new BookingLineDataContract { AccountNr = entity.Grootboek, Amount=0-entity.Bedrag.Value }
                            , new BookingLineDataContract { AccountNr = worksheetset.GetListAccount("404"), Amount=entity.Bedrag.Value}
                    });
                }
                Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
            }
        }

        public void UpdateOpTeMakenFacturen(EY.com.eBook.RuleEngine.AY2015.FacturatieAppTypes.FacturatieKlant entity, ref WorksheetSetData worksheetset)
        {
            entity = Validate(entity, "OpTeMakenFacturen", ref worksheetset);
            EY.com.eBook.RuleEngine.AY2015.FacturatieAppTypes.FacturatieKlant original = Data.OpTeMakenFacturen.FirstOrDefault(e => e.Id == entity.Id);
            if (original == null)
            {
                AddToOpTeMakenFacturen(entity, ref worksheetset);
            }
            else
            {
                if ((entity != original || !entity.LastUpdated.HasValue) && entity.IsValid)
                {
                    if (string.IsNullOrEmpty(entity.Omschrijving))
                    {
                        entity.Omschrijving = string.Format("{0} {1}", entity.Klant.NL, entity.FactuurNr);
                    }
                    entity.Boeken = entity.Boeken.HasValue ? entity.Boeken.Value : false;
                    original.Boeken = original.Boeken.HasValue ? original.Boeken.Value : false;

                    if (original.Boeken.Value && !entity.Boeken.Value)
                    {
                        worksheetset.DeleteBooking(FacturatieApp.TypeId, "OpTeMakenFacturen", original.Id, null);
                    }
                    else if (!original.Boeken.Value && entity.Boeken.Value)
                    {
                        worksheetset.AddBooking(FacturatieApp.TypeId, "OpTeMakenFacturen", entity.Id, null, new List<BookingLineDataContract>() {
                            new BookingLineDataContract { AccountNr = entity.Grootboek, Amount=0-entity.Bedrag.Value }
                            , new BookingLineDataContract { AccountNr = worksheetset.GetListAccount("404"), Amount=entity.Bedrag.Value}
                        });
                    }
                    else if (original.Boeken.Value && entity.Boeken.Value && (original.Bedrag != entity.Bedrag || original.Grootboek != entity.Grootboek))
                    {
                        worksheetset.UpdateBooking(FacturatieApp.TypeId, "OpTeMakenFacturen", entity.Id, null, new List<BookingLineDataContract>() {
                            new BookingLineDataContract { AccountNr = entity.Grootboek, Amount=0-entity.Bedrag.Value }
                            , new BookingLineDataContract { AccountNr = worksheetset.GetListAccount("404"), Amount=entity.Bedrag.Value}
                        });
                    }
                    else if (!entity.LastUpdated.HasValue && entity.Boeken.Value)
                    {
                        worksheetset.UpdateBooking(FacturatieApp.TypeId, "OpTeMakenFacturen", entity.Id, null, new List<BookingLineDataContract>() {
                            new BookingLineDataContract { AccountNr = entity.Grootboek, Amount=0-entity.Bedrag.Value }
                            , new BookingLineDataContract { AccountNr = worksheetset.GetListAccount("404"), Amount=entity.Bedrag.Value}
                        });
                    }
                    int idx = Data.OpTeMakenFacturen.IndexOf(original);
                    Data.OpTeMakenFacturen.RemoveAt(idx);
                    Data.OpTeMakenFacturen.Insert(idx, entity);

                    Data.LastUpdated = DateTime.Now;
                    entity.LastUpdated = DateTime.Now;
                }
            }

        }

        public void DeleteOpTeMakenFacturen(Guid rowId, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2015.FacturatieAppTypes.FacturatieKlant original = Data.OpTeMakenFacturen.FirstOrDefault(e => e.Id == rowId);
            if (original != null)
            {
                if (original.Boeken.HasValue && original.Boeken.Value)
                {
                    worksheetset.DeleteBooking(FacturatieApp.TypeId, "OpTeMakenFacturen", original.Id, null);
                    // DELETE BOOKING
                }
                Data.OpTeMakenFacturen.Remove(original);
                Data.LastUpdated = DateTime.Now;

            }

        }

        public void AddToTeOntvangenCN(EY.com.eBook.RuleEngine.AY2015.FacturatieAppTypes.FacturatieLeverancierCN entity, ref WorksheetSetData worksheetset)
        {
            entity = Validate(entity, "TeOntvangenCN", ref worksheetset);
            if (entity.IsValid)
            {
                if (string.IsNullOrEmpty(entity.Omschrijving))
                {
                    entity.Omschrijving = string.Format("{0} {1}", entity.Leverancier.NL, entity.FactuurNr);
                }
                Data.TeOntvangenCN.Add(entity);
                if (entity.Boeken.HasValue && entity.Boeken.Value)
                {
                    worksheetset.AddBooking(FacturatieApp.TypeId, "TeOntvangenCN", entity.Id, null, new List<BookingLineDataContract>() {
                        new BookingLineDataContract { AccountNr = entity.Grootboek, Amount=0-entity.Bedrag.Value }
                            , new BookingLineDataContract { AccountNr = worksheetset.GetListAccount("4041"), Amount=entity.Bedrag.Value}
                    });
                }
                Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
            }
        }

        public void UpdateTeOntvangenCN(EY.com.eBook.RuleEngine.AY2015.FacturatieAppTypes.FacturatieLeverancierCN entity, ref WorksheetSetData worksheetset)
        {
            entity = Validate(entity, "TeOntvangenCN", ref worksheetset);
            EY.com.eBook.RuleEngine.AY2015.FacturatieAppTypes.FacturatieLeverancierCN original = Data.TeOntvangenCN.FirstOrDefault(e => e.Id == entity.Id);
            if (original == null)
            {
                AddToTeOntvangenCN(entity, ref worksheetset);
            }
            else
            {
                if ((entity != original || !entity.LastUpdated.HasValue) && entity.IsValid)
                {
                    if (string.IsNullOrEmpty(entity.Omschrijving))
                    {
                        entity.Omschrijving = string.Format("{0} {1}", entity.Leverancier.NL, entity.FactuurNr);
                    }
                    entity.Boeken = entity.Boeken.HasValue ? entity.Boeken.Value : false;
                    original.Boeken = original.Boeken.HasValue ? original.Boeken.Value : false;

                    if (original.Boeken.Value && !entity.Boeken.Value)
                    {
                        worksheetset.DeleteBooking(FacturatieApp.TypeId, "TeOntvangenCN", original.Id, null);
                    }
                    else if (!original.Boeken.Value && entity.Boeken.Value)
                    {
                        worksheetset.AddBooking(FacturatieApp.TypeId, "TeOntvangenCN", entity.Id, null, new List<BookingLineDataContract>() {
                            new BookingLineDataContract { AccountNr = entity.Grootboek, Amount=0-entity.Bedrag.Value }
                            , new BookingLineDataContract { AccountNr = worksheetset.GetListAccount("4041"), Amount=entity.Bedrag.Value}
                        });
                    }
                    else if (original.Boeken.Value && entity.Boeken.Value && (original.Bedrag != entity.Bedrag || original.Grootboek != entity.Grootboek))
                    {
                        worksheetset.UpdateBooking(FacturatieApp.TypeId, "TeOntvangenCN", entity.Id, null, new List<BookingLineDataContract>() {
                            new BookingLineDataContract { AccountNr = entity.Grootboek, Amount=0-entity.Bedrag.Value }
                            , new BookingLineDataContract { AccountNr = worksheetset.GetListAccount("4041"), Amount=entity.Bedrag.Value}
                        });
                    }
                    else if (!entity.LastUpdated.HasValue && entity.Boeken.Value)
                    {
                        worksheetset.UpdateBooking(FacturatieApp.TypeId, "TeOntvangenCN", entity.Id, null, new List<BookingLineDataContract>() {
                            new BookingLineDataContract { AccountNr = entity.Grootboek, Amount=0-entity.Bedrag.Value }
                            , new BookingLineDataContract { AccountNr = worksheetset.GetListAccount("4041"), Amount=entity.Bedrag.Value}
                        });
                    }
                    int idx = Data.TeOntvangenCN.IndexOf(original);
                    Data.TeOntvangenCN.RemoveAt(idx);
                    Data.TeOntvangenCN.Insert(idx, entity);
                    Data.LastUpdated = DateTime.Now;
                    entity.LastUpdated = DateTime.Now;
                }
            }

        }

        public void DeleteTeOntvangenCN(Guid rowId, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2015.FacturatieAppTypes.FacturatieLeverancierCN original = Data.TeOntvangenCN.FirstOrDefault(e => e.Id == rowId);
            if (original != null)
            {
                if (original.Boeken.HasValue && original.Boeken.Value)
                {
                    worksheetset.DeleteBooking(FacturatieApp.TypeId, "TeOntvangenCN", original.Id, null);
                    // DELETE BOOKING
                }
                Data.TeOntvangenCN.Remove(original);
                Data.LastUpdated = DateTime.Now;
            }
        }


        public void AddToTeOntvangenFacturen(EY.com.eBook.RuleEngine.AY2015.FacturatieAppTypes.FacturatieLeverancier entity, ref WorksheetSetData worksheetset)
        {
            entity = Validate(entity, "TeOntvangenFacturen", ref worksheetset);
            if (entity.IsValid)
            {
                if (string.IsNullOrEmpty(entity.Omschrijving))
                {
                    entity.Omschrijving = string.Format("{0} {1}", entity.Leverancier.NL, entity.FactuurNr);
                }
                Data.TeOntvangenFacturen.Add(entity);
                if (entity.Boeken.HasValue && entity.Boeken.Value)
                {
                    worksheetset.AddBooking(FacturatieApp.TypeId, "TeOntvangenFacturen", entity.Id, null, new List<BookingLineDataContract>() {
                        new BookingLineDataContract { AccountNr = entity.Grootboek, Amount=entity.Bedrag.Value }
                            , new BookingLineDataContract { AccountNr = worksheetset.GetListAccount("444"), Amount=0-entity.Bedrag.Value}
                    });
                }
                Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
            }
        }

        public void UpdateTeOntvangenFacturen(EY.com.eBook.RuleEngine.AY2015.FacturatieAppTypes.FacturatieLeverancier entity, ref WorksheetSetData worksheetset)
        {
            entity = Validate(entity, "TeOntvangenFacturen", ref worksheetset);
            EY.com.eBook.RuleEngine.AY2015.FacturatieAppTypes.FacturatieLeverancier original = Data.TeOntvangenFacturen.FirstOrDefault(e => e.Id == entity.Id);
            if (original == null)
            {
                AddToTeOntvangenFacturen(entity, ref worksheetset);
            }
            else
            {
                if ((entity != original || !entity.LastUpdated.HasValue) && entity.IsValid)
                {
                    if (string.IsNullOrEmpty(entity.Omschrijving))
                    {
                        entity.Omschrijving = string.Format("{0} {1}", entity.Leverancier.NL, entity.FactuurNr);
                    }
                    entity.Boeken = entity.Boeken.HasValue ? entity.Boeken.Value : false;
                    original.Boeken = original.Boeken.HasValue ? original.Boeken.Value : false;

                    if (original.Boeken.Value && !entity.Boeken.Value)
                    {
                        worksheetset.DeleteBooking(FacturatieApp.TypeId, "TeOntvangenFacturen", original.Id, null);
                    }
                    else if (!original.Boeken.Value && entity.Boeken.Value)
                    {
                        worksheetset.AddBooking(FacturatieApp.TypeId, "TeOntvangenFacturen", entity.Id, null, new List<BookingLineDataContract>() {
                            new BookingLineDataContract { AccountNr = entity.Grootboek, Amount=entity.Bedrag.Value }
                            , new BookingLineDataContract { AccountNr = worksheetset.GetListAccount("444"), Amount=0-entity.Bedrag.Value}
                        });
                    }
                    else if (original.Boeken.Value && entity.Boeken.Value && (original.Bedrag != entity.Bedrag || original.Grootboek != entity.Grootboek))
                    {
                        worksheetset.UpdateBooking(FacturatieApp.TypeId, "TeOntvangenFacturen", entity.Id, null, new List<BookingLineDataContract>() {
                            new BookingLineDataContract { AccountNr = entity.Grootboek, Amount=entity.Bedrag.Value }
                            , new BookingLineDataContract { AccountNr = worksheetset.GetListAccount("444"), Amount=0-entity.Bedrag.Value}
                        });
                    }
                    else if (!entity.LastUpdated.HasValue && entity.Boeken.Value)
                    {
                        worksheetset.UpdateBooking(FacturatieApp.TypeId, "TeOntvangenFacturen", entity.Id, null, new List<BookingLineDataContract>() {
                            new BookingLineDataContract { AccountNr = entity.Grootboek, Amount=entity.Bedrag.Value }
                            , new BookingLineDataContract { AccountNr = worksheetset.GetListAccount("444"), Amount=0-entity.Bedrag.Value}
                        });
                    }
                }
                int idx = Data.TeOntvangenFacturen.IndexOf(original);
                Data.TeOntvangenFacturen.RemoveAt(idx);
                Data.TeOntvangenFacturen.Insert(idx, entity);

                Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
            }

        }

        public void DeleteTeOntvangenFacturen(Guid rowId, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2015.FacturatieAppTypes.FacturatieLeverancier original = Data.TeOntvangenFacturen.FirstOrDefault(e => e.Id == rowId);
            if (original != null)
            {
                if (original.Boeken.HasValue && original.Boeken.Value)
                {
                    worksheetset.DeleteBooking(FacturatieApp.TypeId, "TeOntvangenFacturen", original.Id, null);
                    // DELETE BOOKING
                }
                Data.TeOntvangenFacturen.Remove(original);
                Data.LastUpdated = DateTime.Now;
            }
        }
        */
        public void AddToOverzicht(EY.com.eBook.RuleEngine.AY2015.DetailRekeningKapitaalAppTypes.DetailRekeningKapitaalOverzicht entity, ref WorksheetSetData worksheetSet) { 
            // TODO
            entity = Validate(entity, "Overzicht", ref worksheetSet);
            if (entity.IsValid)
            {
                Data.Overzicht.Add(entity);
                Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
            }




            
        }

        public void UpdateOverzicht(EY.com.eBook.RuleEngine.AY2015.DetailRekeningKapitaalAppTypes.DetailRekeningKapitaalOverzicht entity, ref WorksheetSetData worksheetSet) {



            entity = Validate(entity, "Overzicht", ref worksheetSet);
            EY.com.eBook.RuleEngine.AY2015.DetailRekeningKapitaalAppTypes.DetailRekeningKapitaalOverzicht overzicht = Data.Overzicht.FirstOrDefault(e => e.Id == entity.Id);
            if (overzicht == null)
            {
                AddToOverzicht(entity, ref worksheetSet);
            }
            else
            {
                
                int idx = Data.Overzicht.IndexOf(overzicht);
                Data.Overzicht.RemoveAt(idx);
                Data.Overzicht.Insert(idx, entity);

                Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
            }
        }

        public void DeleteOverzicht(Guid rowId, ref WorksheetSetData worksheetSet)
        {
            EY.com.eBook.RuleEngine.AY2015.DetailRekeningKapitaalAppTypes.DetailRekeningKapitaalOverzicht overzicht = Data.Overzicht.FirstOrDefault(e => e.Id == rowId);


            if (overzicht != null)
            {
                Data.Overzicht.Remove(overzicht);
                Data.LastUpdated = DateTime.Now;
            }
        }

        #region worksheet type validation


        public EY.com.eBook.RuleEngine.AY2015.DetailRekeningKapitaalAppTypes.DetailRekeningKapitaalOverzicht Validate(EY.com.eBook.RuleEngine.AY2015.DetailRekeningKapitaalAppTypes.DetailRekeningKapitaalOverzicht entity, string location, ref WorksheetSetData worksheetset)
        {
            worksheetset.ClearMessages(TypeId, entity.Id);
            entity.IsValid = true;

            if (!entity.Datum.HasValue)
            {
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Datum");
                entity.IsValid = false;
            }

            if (string.IsNullOrEmpty(entity.Omschrijving)) {
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Omschrijving");
                entity.IsValid = false;
            }

            if (!entity.InbrengVanGeld.HasValue && !entity.InbrengVanNatura.HasValue
                && !entity.BelasteReserves.HasValue && !entity.VrijgesteldeReserves.HasValue
                && !entity.LiquidatieBonus.HasValue && !entity.VvprRegime.HasValue) {
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "InbrengVanGeld");
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "InbrengVanNatura");
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "BelasteReserves");
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "VrijgesteldeReserves");
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "LiquidatieBonus");
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "VvprRegime");
                entity.IsValid = false;
            }

            if (entity.IsValid == true)
            {
                decimal totalInbrengVanGeld = worksheetset.DetailRekeningKapitaalApp.Data.Overzicht.Sum(w => w.InbrengVanGeld.GetValueOrDefault());
                decimal totalInbrengVanNatura = worksheetset.DetailRekeningKapitaalApp.Data.Overzicht.Sum(z => z.InbrengVanNatura.GetValueOrDefault());
                decimal totalBelasteReserves = worksheetset.DetailRekeningKapitaalApp.Data.Overzicht.Sum(b => b.BelasteReserves.GetValueOrDefault());
                decimal totalVrijgesteldeReserves = worksheetset.DetailRekeningKapitaalApp.Data.Overzicht.Sum(v => v.VrijgesteldeReserves.GetValueOrDefault());
                decimal totalLiquidatieBonus = worksheetset.DetailRekeningKapitaalApp.Data.Overzicht.Sum(v => v.LiquidatieBonus.GetValueOrDefault());
                decimal totalVvprRegime = worksheetset.DetailRekeningKapitaalApp.Data.Overzicht.Sum(v => v.VvprRegime.GetValueOrDefault());

                if (worksheetset.DetailRekeningKapitaalApp.Data.Overzicht.Count(e=>e.Id== entity.Id)==0)
                {
                    totalInbrengVanGeld += entity.InbrengVanGeld.GetValueOrDefault();
                    totalInbrengVanNatura += entity.InbrengVanNatura.GetValueOrDefault();
                    totalBelasteReserves += entity.BelasteReserves.GetValueOrDefault();
                    totalVrijgesteldeReserves += entity.VrijgesteldeReserves.GetValueOrDefault();
                    totalLiquidatieBonus += entity.LiquidatieBonus.GetValueOrDefault();
                    totalVvprRegime += entity.VvprRegime.GetValueOrDefault();
                }
                decimal grandTotal = totalInbrengVanGeld + totalInbrengVanNatura + totalBelasteReserves 
                    + totalVrijgesteldeReserves + totalLiquidatieBonus + totalVvprRegime ;
                decimal total = worksheetset.GetAccountsInRanges(new List<string> { "10" }, false, true, false, false, "nl-BE").Sum(a => a.Saldo);
                total = Math.Abs(total);
                //IEnumerable<KeyValuePair<string,EY.com.eBook.API.Contracts.Data.Cache.AccountDataContract>> laldc = worksheetset.Schema.Accounts.Where(a => a.Value.InternalNr.StartsWith("10")) ;
              //  decimal total = laldc.Sum(e => e.Value.Saldo);
                if (grandTotal != total)
                {
                    worksheetset.SetMessage(TypeId, MessageTypeDataContract.Warning, "NOTEQUAL", location, entity.Id, "GrandTotal", new object[] { grandTotal, total });
                    //entity.IsValid = false;
                }
                entity.Details = new List<DetailRekeningKapitaalAppTypes.OverzichtDetail>();
                if (entity.LiquidatieBonus.GetValueOrDefault() != 0)
                {
                    if (worksheetset.Settings.CurrentPeriodSmall)
                    {
                        entity.Details.Add(new DetailRekeningKapitaalAppTypes.OverzichtDetail
                        {
                            StartDate = entity.Datum.Value.AddDays(1),
                            EndDate = entity.Datum.Value.AddYears(2),
                            WithholdingTaxPerc = 15
                        });
                        entity.Details.Add(new DetailRekeningKapitaalAppTypes.OverzichtDetail
                        {
                            StartDate = entity.Datum.Value.AddDays(1).AddYears(2),
                            EndDate = entity.Datum.Value.AddYears(3),
                            WithholdingTaxPerc = 10
                        });
                        entity.Details.Add(new DetailRekeningKapitaalAppTypes.OverzichtDetail
                        {
                            StartDate = entity.Datum.Value.AddDays(1).AddYears(3),
                            EndDate = entity.Datum.Value.AddYears(4),
                            WithholdingTaxPerc = 5
                        });
                        entity.Details.Add(new DetailRekeningKapitaalAppTypes.OverzichtDetail
                        {
                            StartDate = entity.Datum.Value.AddDays(1).AddYears(4),
                            EndDate = null,
                            WithholdingTaxPerc = 0
                        });
                    }
                    else
                    {
                        entity.Details.Add(new DetailRekeningKapitaalAppTypes.OverzichtDetail
                        {
                            StartDate = entity.Datum.Value.AddDays(1),
                            EndDate = entity.Datum.Value.AddYears(4),
                            WithholdingTaxPerc = 15
                        });
                        entity.Details.Add(new DetailRekeningKapitaalAppTypes.OverzichtDetail
                        {
                            StartDate = entity.Datum.Value.AddDays(1).AddYears(4),
                            EndDate = entity.Datum.Value.AddYears(6),
                            WithholdingTaxPerc = 10
                        });
                        entity.Details.Add(new DetailRekeningKapitaalAppTypes.OverzichtDetail
                        {
                            StartDate = entity.Datum.Value.AddDays(1).AddYears(6),
                            EndDate = entity.Datum.Value.AddYears(8),
                            WithholdingTaxPerc = 5
                        });
                        entity.Details.Add(new DetailRekeningKapitaalAppTypes.OverzichtDetail
                        {
                            StartDate = entity.Datum.Value.AddDays(1).AddYears(8),
                            EndDate = null,
                            WithholdingTaxPerc = 0
                        });
                    }
                }

                if (entity.VvprRegime.HasValue)
                {
                    entity.Details.Add(new DetailRekeningKapitaalAppTypes.OverzichtDetail
                    {
                        Assessment=worksheetset.DataContract.AssessmentYear + 1,
                        WithholdingTaxPerc = 25
                    });
                    entity.Details.Add(new DetailRekeningKapitaalAppTypes.OverzichtDetail
                    {
                        Assessment = worksheetset.DataContract.AssessmentYear + 2,
                        WithholdingTaxPerc = 10
                    });
                    entity.Details.Add(new DetailRekeningKapitaalAppTypes.OverzichtDetail
                    {
                        Assessment = worksheetset.DataContract.AssessmentYear + 3,
                        WithholdingTaxPerc = 5
                    });
                    
                }

            }
            return entity;
        }        

        #endregion
        
        // BOOKINGS
        public void UpdateBookings()
        {
        }


        #region worksheet type import previous
        /*
        public void ImportPrevious(ref WorksheetSetData wsd)
        {
            EY.com.eBook.RuleEngine.AY2012.FacturatieApp previous = wsd.PreviousSet.FacturatieApp;
            foreach (EY.com.eBook.RuleEngine.AY2012.FacturatieAppTypes.FacturatieKlantCN klant
                        in previous.Data.OpTeMakenCN.Where(e => e.Boeken.HasValue && e.Boeken.Value))
            {
                EY.com.eBook.RuleEngine.AY2015.FacturatieAppTypes.FacturatieKlantCN newEnt = new EY.com.eBook.RuleEngine.AY2015.FacturatieAppTypes.FacturatieKlantCN
                {
                    Id = klant.Id
                    ,
                    Bedrag = 0 - klant.Bedrag
                    ,
                    Boeken = true
                    ,
                    FacJaar = klant.FacJaar
                    ,
                    FactuurNr = klant.FactuurNr
                    ,
                    Klant = klant.Klant
                    ,
                    Grootboek = klant.Grootboek
                    ,
                    PreviousImported = true
                    ,
                    LastUpdated = new DateTime(1980, 1, 1)
                };
                Data.OpTeMakenCN.Add(newEnt);
            }

            foreach (EY.com.eBook.RuleEngine.AY2012.FacturatieAppTypes.FacturatieKlant klant
                        in previous.Data.OpTeMakenFacturen.Where(e => e.Boeken.HasValue && e.Boeken.Value))
            {
                EY.com.eBook.RuleEngine.AY2015.FacturatieAppTypes.FacturatieKlant newEnt = new EY.com.eBook.RuleEngine.AY2015.FacturatieAppTypes.FacturatieKlant
                {
                    Id = klant.Id
                    ,
                    Bedrag = 0 - klant.Bedrag
                    ,
                    Boeken = true
                    ,
                    FacJaar = klant.FacJaar
                    ,
                    FactuurNr = klant.FactuurNr
                    ,
                    Klant = klant.Klant
                    ,
                    Grootboek = klant.Grootboek
                    ,
                    PreviousImported = true
                    ,
                    LastUpdated = new DateTime(1980, 1, 1)
                };
                Data.OpTeMakenFacturen.Add(newEnt);
            }

            foreach (EY.com.eBook.RuleEngine.AY2012.FacturatieAppTypes.FacturatieLeverancierCN klant
                        in previous.Data.TeOntvangenCN.Where(e => e.Boeken.HasValue && e.Boeken.Value))
            {
                EY.com.eBook.RuleEngine.AY2015.FacturatieAppTypes.FacturatieLeverancierCN newEnt = new EY.com.eBook.RuleEngine.AY2015.FacturatieAppTypes.FacturatieLeverancierCN
                {
                    Id = klant.Id
                    ,
                    Bedrag = 0 - klant.Bedrag
                    ,
                    Boeken = true
                    ,
                    FacJaar = klant.FacJaar
                    ,
                    FactuurNr = klant.FactuurNr
                    ,
                    Leverancier = klant.Leverancier
                    ,
                    Grootboek = klant.Grootboek
                    ,
                    PreviousImported = true
                    ,
                    LastUpdated = new DateTime(1980, 1, 1)
                };
                Data.TeOntvangenCN.Add(newEnt);
            }

            foreach (EY.com.eBook.RuleEngine.AY2012.FacturatieAppTypes.FacturatieLeverancier klant
                        in previous.Data.TeOntvangenFacturen.Where(e => e.Boeken.HasValue && e.Boeken.Value))
            {
                EY.com.eBook.RuleEngine.AY2015.FacturatieAppTypes.FacturatieLeverancier newEnt = new EY.com.eBook.RuleEngine.AY2015.FacturatieAppTypes.FacturatieLeverancier
                {
                    Id = klant.Id
                    ,
                    Bedrag = 0 - klant.Bedrag
                    ,
                    Boeken = true
                    ,
                    FacJaar = klant.FacJaar
                    ,
                    FactuurNr = klant.FactuurNr
                    ,
                    Leverancier = klant.Leverancier
                    ,
                    Grootboek = klant.Grootboek
                    ,
                    PreviousImported = true
                    ,
                    LastUpdated = new DateTime(1980, 1, 1)
                };
                Data.TeOntvangenFacturen.Add(newEnt);
            }
        }

        
        public void ImportPrevious(EY.com.eBook.RuleEngine.AY2010.FacturatieApp previous)
        {
            foreach (EY.com.eBook.RuleEngine.AY2010.FacturatieAppTypes.FacturatieKlant klant 
                        in previous.Data.OpTeMakenCN.Where(e=>e.BookReversed.HasValue && e.BookReversed.Value))
            {
                EY.com.eBook.RuleEngine.AY2015.FacturatieAppTypes.FacturatieKlant newEnt = new EY.com.eBook.RuleEngine.AY2015.FacturatieAppTypes.FacturatieKlant
                {
                    Id = klant.Id
                    , Bedrag = 0-klant.Bedrag
                    , Boeken =true
                    , FacJaar = klant.FacJaar
                    , FactuurNr = klant.FactuurNr
                    , Klant = klant.Klant
                    , Grootboek = klant.Grootboek
                    , PreviousImported=true
                    ,LastUpdated = DateTime.Now
                };
                Data.OpTeMakenCN.Add(newEnt);
            }

            foreach (EY.com.eBook.RuleEngine.AY2010.FacturatieApp.FacturatieKlant klant 
                        in previous.Data.OpTeMakenFacturen.Where(e=>e.BookReversed.HasValue && e.BookReversed.Value))
            {
                EY.com.eBook.RuleEngine.AY2015.FacturatieAppTypes.FacturatieKlant newEnt = new EY.com.eBook.RuleEngine.AY2015.FacturatieAppTypes.FacturatieKlant
                {
                    Id = klant.Id
                    , Bedrag = 0-klant.Bedrag
                    , Boeken =true
                    , FacJaar = klant.FacJaar
                    , FactuurNr = klant.FactuurNr
                    , Klant = klant.Klant
                    , Grootboek = klant.Grootboek
                    , PreviousImported=true
                    , BookReversed =false
                    ,LastUpdated = DateTime.Now
                };
                Data.OpTeMakenFacturen.Add(newEnt);
            }

            foreach (EY.com.eBook.RuleEngine.AY2010.FacturatieApp.FacturatieLeverancierCN klant 
                        in previous.Data.TeOntvangenCN.Where(e=>e.BookReversed.HasValue && e.BookReversed.Value))
            {
                EY.com.eBook.RuleEngine.AY2015.FacturatieAppTypes.FacturatieLeverancierCN newEnt = new EY.com.eBook.RuleEngine.AY2015.FacturatieAppTypes.FacturatieLeverancierCN
                {
                    Id = klant.Id
                    , Bedrag = 0-klant.Bedrag
                    , Boeken =true
                    , FacJaar = klant.FacJaar
                    , FactuurNr = klant.FactuurNr
                    , Leverancier = klant.Leverancier
                    , Grootboek = klant.Grootboek
                    , PreviousImported=true
                    , BookReversed =false
                    ,LastUpdated = DateTime.Now
                };
                Data.TeOntvangenCN.Add(newEnt);
            }

            foreach (EY.com.eBook.RuleEngine.AY2010.FacturatieApp.FacturatieLeverancier klant 
                        in previous.Data.TeOntvangenFacturen.Where(e=>e.BookReversed.HasValue && e.BookReversed.Value))
            {
                EY.com.eBook.RuleEngine.AY2015.FacturatieAppTypes.FacturatieLeverancier newEnt = new EY.com.eBook.RuleEngine.AY2015.FacturatieAppTypes.FacturatieLeverancier
                {
                    Id = klant.Id
                    , Bedrag = 0-klant.Bedrag
                    , Boeken =true
                    , FacJaar = klant.FacJaar
                    , FactuurNr = klant.FactuurNr
                    , Leverancier = klant.Leverancier
                    , Grootboek = klant.Grootboek
                    , PreviousImported=true
                    , BookReversed =false
                    ,LastUpdated = DateTime.Now
                };
                Data.TeOntvangenFacturen.Add(newEnt);
            }
        }
        */
        #endregion
        /*
        public XElement GetPdfTemplate(string printLayout, string culture, ref WorksheetSetData worksheetset)
        {

            XElement source = XElement.Parse(this.ToXml()).StripNamespaces();

            string xsltPath = Config.GetWorksheetTemplate("FacturatieApp", worksheetset.DataContract.AssessmentYear, printLayout);

            if (string.IsNullOrEmpty(xsltPath))
            {
                return new XElement("Root", new XElement("Paragraph", string.Format("XSLT - PRINTLAYOUT NOT FOUND, CONTACT SUPPORT WITH CLIENT AND EBOOK FILE NAME AS WELL AS 'WORKSHEET:{0} - LAYOUT:{1} - ASSESSMENTYEAR:{2}'", "FacturatieApp", printLayout, worksheetset.DataContract.AssessmentYear.ToString())));
            }

            XslCompiledTransform xslTransformer = new XslCompiledTransform(Config.DebugXslt());

            XsltSettings settings = new XsltSettings
            {
                EnableDocumentFunction = true
                ,
                EnableScript = false
            };

            xslTransformer.Load(xsltPath, settings, null);

            CultureInfo cObj = CultureInfo.CreateSpecificCulture(culture);

            XsltHelperClass xhc = new XsltHelperClass { Culture = cObj };


            XsltArgumentList arguments = new XsltArgumentList();
            arguments.AddExtensionObject("urn:Helper", xhc);
            arguments.AddExtensionObject("urn:Set", worksheetset);
            arguments.AddExtensionObject("urn:File", worksheetset.DataContract);
            arguments.AddExtensionObject("urn:Client", worksheetset.GetClient());
            //arguments.AddParam("RuleAppDef", "", xdoc.CreateNavigator());


            StringWriter stringWriter = new StringWriter();
            XmlWriterSettings wSettings = new XmlWriterSettings();
            wSettings.NewLineHandling = NewLineHandling.None;
            wSettings.NewLineOnAttributes = false;
            XmlWriter xmlTextWriter = XmlTextWriter.Create(stringWriter, wSettings);

            xslTransformer.Transform(source.CreateNavigator(), arguments, xmlTextWriter);
            XElement el = XElement.Parse(stringWriter.GetStringBuilder().ToString(), LoadOptions.None);

            // CLEANUP
            xslTransformer = null;
            cObj = null;
            xhc = null;
            stringWriter.Close();
            stringWriter.Dispose();
            stringWriter = null;
            wSettings = null;
            xmlTextWriter = null;

            return el;

        }
        */

    }
}
