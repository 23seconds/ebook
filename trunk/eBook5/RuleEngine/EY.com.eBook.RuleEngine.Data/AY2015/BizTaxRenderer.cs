using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.Caching;
using EY.com.eBook.Core;
using System.Xml.Linq;
using System.Text.RegularExpressions;
using System.Xml;
using EY.com.eBook.API.Contracts.Data.Cache;
using EY.com.eBook.API.BL;
using EY.com.eBook.Core.Data;
using System.ServiceModel;
using EY.com.eBook.Core.EF;
using bzt=EY.com.eBook.BizTax;

namespace  EY.com.eBook.RuleEngine.AY2015
{
    

    public class BizTaxRenderer : bzt.BizTaxRenderer
    {

      
        #region Rendered Methods

        #region BizTaxDataContract

        

        public bzt.Contracts.BizTaxDataContract UpdateFromWorksheets(bzt.Contracts.BizTaxDataContract btdc, WorksheetSet set)
        {
            
            // clear all automated rendered items
            bzt.XbrlElementDataContract xedc = null;
            bzt.XbrlElementDataContract pxedc = null;
            string xvalue = null;
           
            string ident = btdc.EntityIdentifier.IdentifierValue;

            var worksheetset = set._datacontainer;

            var belreserves = worksheetset.BelasteReservesApp.Data;
            var belvrijreserves = worksheetset.BelastingVrijeReservesApp.Data;
            var berekening = worksheetset.BerekeningVenBApp.Data;

            #region  Reserves

            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "TaxableReservesCapitalSharePremiums"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value =  belreserves.AllBelasteReserves.Where(e => e.TypeReseve != null && e.TypeReseve.Id.ToString().ToLower() == "bbc12ef9-3e15-4c70-92d8-11f7388ed64a").Sum(e => e.BeginToestand.GetValueOrDefault()).ToXbrlValue()
                , Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                        pxedc.Locked = xedc.Locked ;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-End"
              ,
                Decimals = "INF"
              ,
                Name = "TaxableReservesCapitalSharePremiums"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = belreserves.AllBelasteReserves.Where(e => e.TypeReseve != null && e.TypeReseve.Id.ToString().ToLower() == "bbc12ef9-3e15-4c70-92d8-11f7388ed64a").Sum(e => e.EindToestand.GetValueOrDefault()).ToXbrlValue()
                , Locked =true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                        pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "TaxablePortionRevaluationSurpluses"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = belreserves.AllBelasteReserves.Where(e => e.TypeReseve != null && e.TypeReseve.Id.ToString().ToLower() == "8ca508c3-b59f-4106-9afa-30b1acda92b0").Sum(e => e.BeginToestand.GetValueOrDefault()).ToXbrlValue()
                ,Locked=true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                        pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-End"
              ,
                Decimals = "INF"
              ,
                Name = "TaxablePortionRevaluationSurpluses"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = belreserves.AllBelasteReserves.Where(e => e.TypeReseve != null && e.TypeReseve.Id.ToString().ToLower() == "8ca508c3-b59f-4106-9afa-30b1acda92b0").Sum(e => e.EindToestand.GetValueOrDefault()).ToXbrlValue()
                , Locked=true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                        pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "LegalReserve"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = belreserves.AllBelasteReserves.Where(e => e.TypeReseve != null && e.TypeReseve.Id.ToString().ToLower() == "85ef3af8-230f-4c81-b10d-4ff87534492c").Sum(e => e.BeginToestand.GetValueOrDefault()).ToXbrlValue()
                ,Locked=true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-End"
              ,
                Decimals = "INF"
              ,
                Name = "LegalReserve"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = belreserves.AllBelasteReserves.Where(e => e.TypeReseve != null && e.TypeReseve.Id.ToString().ToLower() == "85ef3af8-230f-4c81-b10d-4ff87534492c").Sum(e => e.EindToestand.GetValueOrDefault()).ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "UnavailableReserves"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = belreserves.AllBelasteReserves.Where(e => e.TypeReseve != null && e.TypeReseve.Id.ToString().ToLower() == "04f61c72-243d-4673-80b2-5e8dfd576f8d").Sum(e => e.BeginToestand.GetValueOrDefault()).ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-End"
              ,
                Decimals = "INF"
              ,
                Name = "UnavailableReserves"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = belreserves.AllBelasteReserves.Where(e => e.TypeReseve != null && e.TypeReseve.Id.ToString().ToLower() == "04f61c72-243d-4673-80b2-5e8dfd576f8d").Sum(e => e.EindToestand.GetValueOrDefault()).ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "AvailableReserves"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = belreserves.AllBelasteReserves.Where(e => e.TypeReseve != null && e.TypeReseve.Id.ToString().ToLower() == "43d09724-06db-4985-8f7c-aa03f6a4736e").Sum(e => e.BeginToestand.GetValueOrDefault()).ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-End"
              ,
                Decimals = "INF"
              ,
                Name = "AvailableReserves"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = belreserves.AllBelasteReserves.Where(e => e.TypeReseve != null && e.TypeReseve.Id.ToString().ToLower() == "43d09724-06db-4985-8f7c-aa03f6a4736e").Sum(e => e.EindToestand.GetValueOrDefault()).ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "AccumulatedProfitsLosses"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = belreserves.AllBelasteReserves.Where(e => e.TypeReseve != null && e.TypeReseve.Id.ToString().ToLower() == "b9048cb3-37f2-42b3-9fcd-e8d1b7277bd2").Sum(e => e.BeginToestand.GetValueOrDefault()).ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-End"
              ,
                Decimals = "INF"
              ,
                Name = "AccumulatedProfitsLosses"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = belreserves.AllBelasteReserves.Where(e => e.TypeReseve != null && e.TypeReseve.Id.ToString().ToLower() == "b9048cb3-37f2-42b3-9fcd-e8d1b7277bd2").Sum(e => e.EindToestand.GetValueOrDefault()).ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "TaxableProvisions"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = belreserves.AllBelasteReserves.Where(e => e.TypeReseve != null && e.TypeReseve.Id.ToString().ToLower() == "0f5ff0f1-df29-4342-9e8c-1630304da12f").Sum(e => e.BeginToestand.GetValueOrDefault()).ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-End"
              ,
                Decimals = "INF"
              ,
                Name = "TaxableProvisions"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = belreserves.AllBelasteReserves.Where(e => e.TypeReseve != null && e.TypeReseve.Id.ToString().ToLower() == "0f5ff0f1-df29-4342-9e8c-1630304da12f").Sum(e => e.EindToestand.GetValueOrDefault()).ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }

            Guid OtherReserves = "OtherReserves".DeterministicGuid();
            btdc.Contexts.RemoveAll(c => c.DefId == OtherReserves);

            
            string scenid = AddContext(new List<bzt.ContextScenarioDataContract> {
                    new bzt.ContextScenarioTypeDataContract {
                        AutoRendered=true
                        , Dimension = "d-ty:DescriptionTypedDimension"
                        , Type="d-ty:DescriptionTypedID"
                        , Value = set._datacontainer.DataContract.Culture=="nl-BE" ? "Andere reserves" : "Autres r�serves"
                    }
                }
                , set._datacontainer.DataContract.StartDate
                , set._datacontainer.DataContract.EndDate
                , OtherReserves
                , "OtherReservesTitle"
                );
            
            


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = scenid
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "OtherReserves"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = belreserves.AllBelasteReserves.Where(e => e.TypeReseve != null && e.TypeReseve.Id.ToString().ToLower() == "244403c0-219f-4a3a-94da-c9eb19950b4f").Sum(e => e.BeginToestand.GetValueOrDefault()).ToXbrlValue()
                , Locked =true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = scenid
              ,
                Period = "I-End"
              ,
                Decimals = "INF"
              ,
                Name = "OtherReserves"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = belreserves.AllBelasteReserves.Where(e => e.TypeReseve != null && e.TypeReseve.Id.ToString().ToLower() == "244403c0-219f-4a3a-94da-c9eb19950b4f").Sum(e => e.EindToestand.GetValueOrDefault()).ToXbrlValue()
                 ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }

            }


            Guid OtherTaxableReserves = "OtherTaxableReserves".DeterministicGuid();
            btdc.Contexts.RemoveAll(c => c.DefId == OtherTaxableReserves);


            scenid = AddContext(new List<bzt.ContextScenarioDataContract> {
                    new bzt.ContextScenarioTypeDataContract {
                        AutoRendered=true
                        , Dimension = "d-ty:DescriptionTypedDimension"
                        , Type="d-ty:DescriptionTypedID"
                        , Value = set._datacontainer.DataContract.Culture=="nl-BE" ? "Andere belastbare reserves" : "Autres r�serves imposable"
                    }
                }
                , set._datacontainer.DataContract.StartDate
                , set._datacontainer.DataContract.EndDate
                , OtherTaxableReserves
                 , "OtherTaxableReservesTitle"
                );

            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = scenid
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "OtherTaxableReserves"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = belreserves.AllBelasteReserves.Where(e => e.TypeReseve != null && e.TypeReseve.Id.ToString().ToLower() == "ea78dbb2-acd3-4fad-812e-029b66f8fecc").Sum(e => e.BeginToestand.GetValueOrDefault()).ToXbrlValue()
                , Locked=true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
               
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = scenid
              ,
                Period = "I-End"
              ,
                Decimals = "INF"
              ,
                Name = "OtherTaxableReserves"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = belreserves.AllBelasteReserves.Where(e => e.TypeReseve != null && e.TypeReseve.Id.ToString().ToLower() == "ea78dbb2-acd3-4fad-812e-029b66f8fecc").Sum(e => e.EindToestand.GetValueOrDefault()).ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "TaxableWriteDownsUndisclosedReserve"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = belreserves.AllBelasteReserves.Where(e => e.TypeReseve != null && e.TypeReseve.Id.ToString().ToLower() == "b97e3772-4090-4bad-a623-a3e0c6044855").Sum(e => e.BeginToestand.GetValueOrDefault()).ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-End"
              ,
                Decimals = "INF"
              ,
                Name = "TaxableWriteDownsUndisclosedReserve"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = belreserves.AllBelasteReserves.Where(e => e.TypeReseve != null && e.TypeReseve.Id.ToString().ToLower() == "b97e3772-4090-4bad-a623-a3e0c6044855").Sum(e => e.EindToestand.GetValueOrDefault()).ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "ExaggeratedDepreciationsUndisclosedReserve"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = belreserves.AllBelasteReserves.Where(e => e.TypeReseve != null && e.TypeReseve.Id.ToString().ToLower() == "17e42920-cba9-43c4-90c5-2b74d75daec7").Sum(e => e.BeginToestand.GetValueOrDefault()).ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-End"
              ,
                Decimals = "INF"
              ,
                Name = "ExaggeratedDepreciationsUndisclosedReserve"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = belreserves.AllBelasteReserves.Where(e => e.TypeReseve != null && e.TypeReseve.Id.ToString().ToLower() == "17e42920-cba9-43c4-90c5-2b74d75daec7").Sum(e => e.EindToestand.GetValueOrDefault()).ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "OtherUnderestimationsAssetsUndisclosedReserve"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = belreserves.AllBelasteReserves.Where(e => e.TypeReseve != null && e.TypeReseve.Id.ToString().ToLower() == "f67a17d7-8f7d-43d9-9955-b8a9693587a1").Sum(e => e.BeginToestand.GetValueOrDefault()).ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-End"
              ,
                Decimals = "INF"
              ,
                Name = "OtherUnderestimationsAssetsUndisclosedReserve"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = belreserves.AllBelasteReserves.Where(e => e.TypeReseve != null && e.TypeReseve.Id.ToString().ToLower() == "f67a17d7-8f7d-43d9-9955-b8a9693587a1").Sum(e => e.EindToestand.GetValueOrDefault()).ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "OtherOverestimationsLiabilitiesUndisclosedReserve"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = belreserves.AllBelasteReserves.Where(e => e.TypeReseve != null && e.TypeReseve.Id.ToString().ToLower() == "4b49ee2c-90c4-4649-93db-df950d9b90e3").Sum(e => e.BeginToestand.GetValueOrDefault()).ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-End"
              ,
                Decimals = "INF"
              ,
                Name = "OtherOverestimationsLiabilitiesUndisclosedReserve"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = belreserves.AllBelasteReserves.Where(e => e.TypeReseve != null && e.TypeReseve.Id.ToString().ToLower() == "4b49ee2c-90c4-4649-93db-df950d9b90e3").Sum(e => e.EindToestand.GetValueOrDefault()).ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "CapitalGainsShares"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = berekening.BelasteReserves.OACapitalGainsOnShares.GetValueOrDefault().ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "CapitalGainsSharesReversalPreviousExemptionsAdjustmentReservesPlus"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = berekening.BelasteReserves.OACapitalGainsSharesReversal.GetValueOrDefault().ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "DefinitiveExemptionTaxShelterAuthorisedAudiovisualWorksAdjustmentReservesPlus"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = berekening.BelasteReserves.OADefinitiveExemptionTaxShelter.GetValueOrDefault().ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "ExemptionRegionalGrantsCapitalSubsidiesInterestSubsidiesAdjustmentReservesPlus"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = berekening.BelasteReserves.OAExemptionRegionalPrem.GetValueOrDefault().ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "FinalExemptionProfitHomologationReorganizationPlanAmicableSettlement"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = berekening.BelasteReserves.OAFinalExemptionProfit.GetValueOrDefault().ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }

           



            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "OtherAdjustmentsReservesPlus"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = berekening.BelasteReserves.OAOthers.GetValueOrDefault().ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "AdjustmentsReservesMinus"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = berekening.BelasteReserves.OAInTheMin.GetValueOrDefault().ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }

            // LIQUIDATIE RESERVE
            //LiquidationReserve
            var liquidatieBegintoestand = belreserves.AllBelasteReserves.Where(e => e.TypeReseve != null && (e.TypeReseve.Id.ToString().ToUpper() == "DD86FCF0-937A-4AE8-A5C9-70CDF794140D" || e.TypeReseve.Id.ToString().ToUpper() == "835AC7D6-1FCA-4443-8AD0-DAD2789A4703")).Sum(e => e.BeginToestand.GetValueOrDefault());
            var liquidatieEindtoestand = belreserves.AllBelasteReserves.Where(e => e.TypeReseve != null && (e.TypeReseve.Id.ToString().ToUpper() == "DD86FCF0-937A-4AE8-A5C9-70CDF794140D" || e.TypeReseve.Id.ToString().ToUpper() == "835AC7D6-1FCA-4443-8AD0-DAD2789A4703")).Sum(e => e.EindToestand.GetValueOrDefault());

            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "LiquidationReserve"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = liquidatieBegintoestand.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc != null)
                    {
                        pxedc.Value = xedc.Value; pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }

            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-End"
              ,
                Decimals = "INF"
              ,
                Name = "LiquidationReserve"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = liquidatieEindtoestand.ToXbrlValue()
                 ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc != null)
                    {
                        pxedc.Value = xedc.Value; pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }

            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "SeparateAssessmentPartCorrespondingFinancialProfitAfterTaxationBroughtForwardToLiquidationReserve"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = liquidatieEindtoestand > liquidatieBegintoestand ? (liquidatieEindtoestand - liquidatieBegintoestand).ToXbrlValue() : null
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc != null)
                    {
                        pxedc.Value = xedc.Value; pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }
            #endregion

            // 275 A
           // LiquidationReserveCurrentTaxPeriod_D__id__TaxPeriod-0Member
            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "id__TaxPeriod-0Member"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "LiquidationReserveCurrentTaxPeriod"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = berekening.Aanslag.LiquidatieReserve.GetValueOrDefault().ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }



            #region VrijgesteldeReserve

            
            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "UnrealisedExpressedCapitalGainsExemptReserve"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = belvrijreserves.VrijgesteldeReserves.CTotaalA.GetValueOrDefault().ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-End"
              ,
                Decimals = "INF"
              ,
                Name = "UnrealisedExpressedCapitalGainsExemptReserve"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = belvrijreserves.VrijgesteldeReserves.CTotaalD.GetValueOrDefault().ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "CapitalGainsSpecificSecuritiesExemptReserve"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = belvrijreserves.VrijgesteldeReserves.EGespreidVerwezenA.GetValueOrDefault().ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-End"
              ,
                Decimals = "INF"
              ,
                Name = "CapitalGainsSpecificSecuritiesExemptReserve"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = belvrijreserves.VrijgesteldeReserves.EGespreidVerwezenD.GetValueOrDefault().ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            
            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "CapitalGainsTangibleIntangibleFixedAssetsExemptReserve"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = belvrijreserves.VrijgesteldeReserves.EMatImmatVasteActivaA.GetValueOrDefault().ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-End"
              ,
                Decimals = "INF"
              ,
                Name = "CapitalGainsTangibleIntangibleFixedAssetsExemptReserve"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = belvrijreserves.VrijgesteldeReserves.EMatImmatVasteActivaD.GetValueOrDefault().ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }
            


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "OtherRealisedCapitalGainsExemptReserve"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = belvrijreserves.VrijgesteldeReserves.DTotaalA.GetValueOrDefault().ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-End"
              ,
                Decimals = "INF"
              ,
                Name = "OtherRealisedCapitalGainsExemptReserve"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = belvrijreserves.VrijgesteldeReserves.DTotaalD.GetValueOrDefault().ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "CapitalGainsCorporateVehiclesExemptReserve"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = belvrijreserves.VrijgesteldeReserves.FBedrijfsvoertuigenA.GetValueOrDefault().ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-End"
              ,
                Decimals = "INF"
              ,
                Name = "CapitalGainsCorporateVehiclesExemptReserve"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = belvrijreserves.VrijgesteldeReserves.FBedrijfsvoertuigenD.GetValueOrDefault().ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "CapitalGainsRiverVesselExemptReserve"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = belvrijreserves.VrijgesteldeReserves.GBinnenschepenA.GetValueOrDefault().ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-End"
              ,
                Decimals = "INF"
              ,
                Name = "CapitalGainsRiverVesselExemptReserve"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = belvrijreserves.VrijgesteldeReserves.GBinnenschepenD.GetValueOrDefault().ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "CapitalGainsSeaVesselExemptReserve"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = belvrijreserves.VrijgesteldeReserves.HZeeschepenA.GetValueOrDefault().ToXbrlValue()

                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-End"
              ,
                Decimals = "INF"
              ,
                Name = "CapitalGainsSeaVesselExemptReserve"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = belvrijreserves.VrijgesteldeReserves.HZeeschepenD.GetValueOrDefault().ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "ExemptInvestmentReserve"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value =belvrijreserves.VrijgesteldeReserves.IInvesteringsreserveA.GetValueOrDefault().ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-End"
              ,
                Decimals = "INF"
              ,
                Name = "ExemptInvestmentReserve"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = belvrijreserves.VrijgesteldeReserves.IInvesteringsreserveD.GetValueOrDefault().ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "TaxShelterAuthorisedAudiovisualWorkExemptReserve"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = belvrijreserves.VrijgesteldeReserves.JTaxShelterAudioVisueelA.GetValueOrDefault().ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-End"
              ,
                Decimals = "INF"
              ,
                Name = "TaxShelterAuthorisedAudiovisualWorkExemptReserve"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = belvrijreserves.VrijgesteldeReserves.JTaxShelterAudioVisueelD.GetValueOrDefault().ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "ProfitHomologationReorganizationPlanAmicableSettlementExemptReserve"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = belvrijreserves.VrijgesteldeReserves.JWinstHomologatieA.GetValueOrDefault().ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-End"
              ,
                Decimals = "INF"
              ,
                Name = "ProfitHomologationReorganizationPlanAmicableSettlementExemptReserve"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = belvrijreserves.VrijgesteldeReserves.JWinstHomologatieD.GetValueOrDefault().ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "OtherExemptReserves"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = belvrijreserves.VrijgesteldeReserves.KTotaalA.GetValueOrDefault().ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-End"
              ,
                Decimals = "INF"
              ,
                Name = "OtherExemptReserves"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = belvrijreserves.VrijgesteldeReserves.KTotaalD.GetValueOrDefault().ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


           


            #endregion


            #region VerworpenUitgaven

            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "NonDeductibleTaxes"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = berekening.VerworpenUitgaven.PDDisallowedCurrentIncomeTaxExpenses.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "NonDeductibleRegionalTaxesDutiesRetributions"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = berekening.VerworpenUitgaven.DERegionalTaxes.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "NonDeductibleFinesConfiscationsPenaltiesAllKind"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = berekening.VerworpenUitgaven.DEPenalties.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "NonDeductiblePensionsCapitalEmployerContributionsEmployerPremiums"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = berekening.VerworpenUitgaven.DEPensions.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "NonDeductibleCarExpensesLossValuesCars"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = berekening.VerworpenUitgaven.DECar.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "NonDeductibleCarExpensesPartBenefitsAllKind"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = berekening.VerworpenUitgaven.DECarVAA.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "NonDeductibleReceptionBusinessGiftsExpenses"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = berekening.VerworpenUitgaven.DEReception.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "NonDeductibleRestaurantExpenses"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = berekening.VerworpenUitgaven.DERestaurant.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "NonDeductibleNonSpecificProfessionalClothsExpenses"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = berekening.VerworpenUitgaven.DEClothes.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "ExaggeratedInterests"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = berekening.VerworpenUitgaven.DEExcessInterest.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "NonDeductibleParticularPortionInterestsLoans"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = berekening.VerworpenUitgaven.DEInterestLoans.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "AbnormalBenevolentAdvantages"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = berekening.VerworpenUitgaven.DEAbnormalAdvantages.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "NonDeductibleSocialAdvantages"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = berekening.VerworpenUitgaven.DESocial.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "NonDeductibleAdvantagesLuncheonSportsCultureEcoVouchers"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = berekening.VerworpenUitgaven.DEDinerSportCultureEco.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "Liberalities"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = berekening.VerworpenUitgaven.DECharity.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "WriteDownsLossValuesShares"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = berekening.VerworpenUitgaven.DEReductionValueShares.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "ReversalPreviousExemptions"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = berekening.VerworpenUitgaven.DEReversalsPreviousTax.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "EmployeeParticipation"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = berekening.VerworpenUitgaven.DEEmployeeCapital.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "IndemnityMissingCoupon"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = berekening.VerworpenUitgaven.DEIndemnityMissingCoupon.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "ExpensesTaxShelterAuthorisedAudiovisualWork"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = berekening.VerworpenUitgaven.DEExpensesTaxShelter.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "RegionalPremiumCapitalSubsidiesInterestSubsidies"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = berekening.VerworpenUitgaven.DERegionalPremiumsCapital.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "NonDeductiblePaymentsCertainStates"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = berekening.VerworpenUitgaven.DEBepaaldeStaten.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }

            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "UnjustifiedExpenses"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = berekening.VerworpenUitgaven.DEUnjustifiedExpenses.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc != null)
                    {
                        pxedc.Value = xedc.Value; pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "OtherDisallowedExpenses"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = berekening.VerworpenUitgaven.DEOther.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }

          
            
           

            #endregion


            #region AangifteVenBApp - UitgekeerdeDividenden

            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "OrdinaryDividends"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = worksheetset.BepalingBelastbareBasisApp.Data.Overzicht.Dividenden.GetValueOrDefault().ToXbrlValue()
                , Locked=true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "AcquisitionOwnShares"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = worksheetset.BepalingBelastbareBasisApp.Data.Overzicht.VerkrijgingEigenAandelen.GetValueOrDefault().ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "DeceaseDepartureExclusionPartner"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = worksheetset.BepalingBelastbareBasisApp.Data.Overzicht.OverlijdenUittredingUitsluitingVennoot.GetValueOrDefault().ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "DistributionCompanyAssets"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = worksheetset.BepalingBelastbareBasisApp.Data.Overzicht.VerdelingMaatschappelijkVermogen.GetValueOrDefault().ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


           


            #endregion


            #region AangifteVenBApp - UiteenzettingWinst

            

            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "ShippingResultTonnageBased"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = berekening.OpdelingNaarOorsprong.WIZeeSchip.GetValueOrDefault().ToXbrlValue()
                , Locked=true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }

            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "CapitalGainsSharesRate0040"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.BerekeningVenBApp.Data.BelastbareBestZonderAftrek.TECapitalGains0412.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc != null)
                    {
                        pxedc.Value = xedc.Value; pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }



            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "BenevolentAbnormalFinancialAdvantagesBenefitsAllKind"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = worksheetset.BerekeningVenBApp.Data.BelastbareBestZonderAftrek.TEReceivedAbnormal.GetValueOrDefault().ToXbrlValue()
                ,Locked=true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "ViolationInvestmentCommitmentIntangibilityConditionInvestmentReserve"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = worksheetset.BerekeningVenBApp.Data.BelastbareBestZonderAftrek.TETaxableInvestmentReserve.GetValueOrDefault().ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "CapitalSubsidiesInterestSubsidiesAgriculturalSupport"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = worksheetset.BerekeningVenBApp.Data.BelastbareBestZonderAftrek.TECapitalAgriCulture.GetValueOrDefault().ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            

            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "id__TaxTreatyMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "RemainingFiscalResult"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = worksheetset.BerekeningVenBApp.Data.OpdelingNaarOorsprong.IRTreatyForeignIncome.GetValueOrDefault().ToXbrlValue()
                , Locked=true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "id__NoTaxTreatyMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "RemainingFiscalResult"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = worksheetset.BerekeningVenBApp.Data.OpdelingNaarOorsprong.IRNonTreatyForeignIncome.GetValueOrDefault().ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "id__BelgiumMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "RemainingFiscalResult"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = worksheetset.BerekeningVenBApp.Data.OpdelingNaarOorsprong.IRBelgianTaxable.GetValueOrDefault().ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "id__NoTaxTreatyMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "MiscellaneousExemptions"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.UiteenzettingWinst.WARWNietBelastbareBestandNVV.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "id__BelgiumMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "MiscellaneousExemptions"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.UiteenzettingWinst.WARWNietBelastbareBestandBelg.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "id__NoTaxTreatyMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "PEExemptIncomeMovableAssets"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.UiteenzettingWinst.WARWDefinitiefBelasteInkomstenNVV.ToXbrlValue()
                 ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "id__BelgiumMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "PEExemptIncomeMovableAssets"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.UiteenzettingWinst.WARWDefinitiefBelasteInkomstenBelg.ToXbrlValue()
                 ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "id__NoTaxTreatyMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "DeductionPatentsIncome"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.UiteenzettingWinst.WARWAftrekOctrooiNVV.ToXbrlValue()
                 ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "id__BelgiumMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "DeductionPatentsIncome"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.UiteenzettingWinst.WARWAftrekOctrooiBelg.ToXbrlValue()
                 ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }

            


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "id__NoTaxTreatyMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "AllowanceCorporateEquity"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.UiteenzettingWinst.WARWAftrekRisicoNVV.ToXbrlValue()
                 ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "id__BelgiumMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "AllowanceCorporateEquity"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.UiteenzettingWinst.WARWAftrekRisicoBelg.ToXbrlValue()
                 ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "id__NoTaxTreatyMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "CompensatedTaxLosses"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.UiteenzettingWinst.WARWVorigeVerliezenNVV.ToXbrlValue()
                 ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "id__BelgiumMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "CompensatedTaxLosses"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.UiteenzettingWinst.WARWVorigeVerliezenBelg.ToXbrlValue()
                 ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "id__BelgiumMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "AllowanceInvestmentDeduction"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.UiteenzettingWinst.WARWInvesteringsAftrekBelg.ToXbrlValue()
                 ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }

            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "id__NoTaxTreatyMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "DeductionExemptionsAllowanceCorporateEquityAccumulatedUpToAndIncludingAssessmentYear2012"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.UiteenzettingWinst.WARWAftrekRisicoHistoriekNVV.ToXbrlValue()
                 ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "id__BelgiumMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "DeductionExemptionsAllowanceCorporateEquityAccumulatedUpToAndIncludingAssessmentYear2012"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.UiteenzettingWinst.WARWAftrekRisicoHistoriekBelg.ToXbrlValue()
                 ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "ShippingProfitTonnageBased"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.UiteenzettingWinst.WIBGZeescheepvaarTonnage.ToXbrlValue()
                ,Locked=true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "CapitalGainsSharesRate2500"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.UiteenzettingWinst.WIBGMeerwaardeAandelenBelast.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "BasicTaxableAmountExitTaxRate"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.UiteenzettingWinst.WIBGExitTarief.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "CapitalSubsidiesInterestSubsidiesAgriculturalSupportRate500"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.UiteenzettingWinst.WIBGKapitaalInterestLandbouw.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            #endregion


            #region AangifteVenBApp - AfzonderlijkeAanslagen

            //xedc = new bzt.XbrlElementDataContract
            //{
            //    AutoRendered = true
            //  ,
            //    Children = null
            //  ,
            //    Context = ""
            //  ,
            //    Period = "D"
            //  ,
            //    Decimals = "INF"
            //  ,
            //    Name = "UnjustifiedExpensesHiddenSurplusProfitsFinancialAdvantagesBenefitsAllKind"
            //  ,
            //    NameSpace = ""
            //  ,
            //    Prefix = "tax-inc"
            //  ,
            //    UnitRef = "EUR"
            //  ,
            //    Value = set._datacontainer.AangifteVenBApp.Data.AfzonderlijkeAanslagen.AAVoordelenAlleAard.ToXbrlValue()
            //    ,
            //    Locked = true
            //};

            //if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            //{
            //    if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
            //    {
            //        pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
            //        if (pxedc!=null)
            //        {
            //            pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
            //        }
            //    }
            //    else
            //    {
            //        pxedc = xedc;
            //        btdc.Elements.Add(pxedc);
            //    }
            //}


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "SeparateAssessmentUnjustifiedExpensesHiddenSurplusProfitsFinancialAdvantagesBenefitsAllKindRate5000"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.AfzonderlijkeAanslagen.AAVoordelenAlleAard50.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc != null)
                    {
                        pxedc.Value = xedc.Value; pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "SeparateAssessmentUnjustifiedExpensesHiddenSurplusProfitsFinancialAdvantagesBenefitsAllKindRate10000"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.AfzonderlijkeAanslagen.AAVoordelenAlleAard100.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc != null)
                    {
                        pxedc.Value = xedc.Value; pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "SeparateAssessmentTaxedReservesCreditAssociationsInstitutionsOtherAuthorisedInstitutionsRate3400"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.AfzonderlijkeAanslagen.AABelasteReserves34.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "SeparateAssessmentTaxedReservesCreditAssociationsInstitutionsOtherAuthorisedInstitutionsRate2800"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.AfzonderlijkeAanslagen.AABelasteReserves28.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "SeparateAssessmentDividendsPaidCreditCorporationTradeEquipmentHousingCorporation"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.AfzonderlijkeAanslagen.AAUitgekeerdeDividenden.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }

            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "FairnessTax"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.BerekeningVenBApp.Data.FairnessTax.FairnessTaxBase.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc != null)
                    {
                        pxedc.Value = xedc.Value; pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }

            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "SeparateAssessmentPartCorrespondingFinancialProfitAfterTaxationBroughtForwardToLiquidationReserve"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.BerekeningVenBApp.Data.Aanslag.AfzonderlijkeAanslagRV.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc != null)
                    {
                        pxedc.Value = xedc.Value; pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "SpecialAssessmentPre1990FullPartialDistributionCompanyAssetsRate3300"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.AfzonderlijkeAanslagen.AABAMaatschVermogen33.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "SpecialAssessmentPre1990FullPartialDistributionCompanyAssetsRate1650"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.AfzonderlijkeAanslagen.AABAMaatschVermogen165.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "SpecialAssessmentPre1990BenefitsAllKindCompanyLiquidation"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.AfzonderlijkeAanslagen.AABAVoordelenAlleAard.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            #endregion


            #region AangifteVenBApp - Diamant

            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "AdditionalDutiesDiamondTraders"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.Diamant.DIAanvullingErkend.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "RetributionTaxCreditResearchDevelopment"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.Diamant.DIOnderzoekOntwikkeling.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }

            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "SeparateAssessmentAdditionalIndividualPensionProvisionsOnEndTaxPeriodClosingDateBefore2012-01-01"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.Diamant.DIVoorzieningenVoor2013.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }

            //1535 not in Biztax 2015
            //xedc = new bzt.XbrlElementDataContract
            //{
            //    AutoRendered = true
            //  ,
            //    Children = null
            //  ,
            //    Context = ""
            //  ,
            //    Period = "D"
            //  ,
            //    Decimals = ""
            //  ,
            //    Name = "NotLiableCorporateIncomeTaxNextTaxPeriodDueToMergerDivisionSimilarTransactions"
            //  ,
            //    NameSpace = ""
            //  ,
            //    Prefix = "tax-inc"
            //  ,
            //    UnitRef = "EUR"
            //  ,
            //    Value = (!set._datacontainer.BerekeningVenBApp.Data.Aanslag.Taxable2015.GetValueOrDefault()).ToXbrlValue()
            //    ,
            //    Locked = true
            //};

            //if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            //{
            //    if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
            //    {
            //        pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
            //        if (pxedc!=null)
            //        {
            //            pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
            //        }
            //    }
            //    else
            //    {
            //        pxedc = xedc;
            //        btdc.Elements.Add(pxedc);
            //    }
            //}


            #endregion


            #region AangifteVenBApp - NietBelastbareBestanddelen

            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "ExemptGifts"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.NietBelastbareBestanddelen.NTEBECharity.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "ExemptionAdditionalPersonnelMiscellaneousExemptions"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.NietBelastbareBestanddelen.NTEBEPersonnel.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "ExemptionAdditionalPersonnelSMEs"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.NietBelastbareBestanddelen.NTEBEPersonnelSME.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "ExemptionTrainingPeriodBonus"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.NietBelastbareBestanddelen.NTEBEInternalshipPremium.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "OtherMiscellaneousExemptions"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.NietBelastbareBestanddelen.NTEBEOthers.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            #endregion


            #region AangifteVenBApp - DBI

            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "id__BelgianBranchMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "NetBelgianIncomeSharesPEExemptIncomeMovableAssets"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.DBI.DBIAbeNettoBEI.ToXbrlValue()
                , Locked=true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "id__ForeignBranchMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "NetBelgianIncomeSharesPEExemptIncomeMovableAssets"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.DBI.DBIAbeNettoBUI.ToXbrlValue()
                ,Locked= true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "id__BelgianBranchMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "WithholdingTaxBelgianIncomeSharesPEExemptIncomeMovableAssets"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.DBI.DBIAbeRoerendBEI.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "id__ForeignBranchMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "WithholdingTaxBelgianIncomeSharesPEExemptIncomeMovableAssets"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.DBI.DBIAbeRoerendBUI.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "id__BelgianBranchMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "NetForeignIncomeSharesPEExemptIncomeMovableAssets"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.DBI.DBIAbuNettoBEI.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "id__ForeignBranchMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "NetForeignIncomeSharesPEExemptIncomeMovableAssets"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.DBI.DBIAbuNettoBUI.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "id__BelgianBranchMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "WithholdingTaxForeignIncomeSharesPEExemptIncomeMovableAssets"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.DBI.DBIAbuRoerendBEI.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "id__ForeignBranchMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "WithholdingTaxForeignIncomeSharesPEExemptIncomeMovableAssets"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.DBI.DBIAbuRoerendBUI.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "id__BelgianBranchMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "NetOtherBelgianIncomeSharesPEExemptIncomeMovableAssets"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.DBI.DBIAAbeNettoBEI.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "id__ForeignBranchMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "NetOtherBelgianIncomeSharesPEExemptIncomeMovableAssets"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.DBI.DBIAAbeNettoBUI.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "id__BelgianBranchMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "WithholdingTaxOtherBelgianIncomeSharesPEExemptIncomeMovableAssets"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.DBI.DBIAAbeRoerendBEI.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "id__ForeignBranchMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "WithholdingTaxOtherBelgianIncomeSharesPEExemptIncomeMovableAssets"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.DBI.DBIAAbeRoerendBUI.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "id__BelgianBranchMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "NetOtherForeignIncomeSharesPEExemptIncomeMovableAssets"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.DBI.DBIAAbuNettoBEI.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "id__ForeignBranchMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "NetOtherForeignIncomeSharesPEExemptIncomeMovableAssets"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.DBI.DBIAAbuNettoBUI.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "id__BelgianBranchMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "WithholdingTaxOtherForeignIncomeSharesPEExemptIncomeMovableAssets"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.DBI.DBIAAbuRoerendBEI.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "id__ForeignBranchMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "WithholdingTaxOtherForeignIncomeSharesPEExemptIncomeMovableAssets"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.DBI.DBIAAbuRoerendBUI.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "id__BelgianBranchMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "OtherExemptIncomeMovableAssets"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.DBI.DBIVrijgestelRoerendBEI.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "id__ForeignBranchMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "OtherExemptIncomeMovableAssets"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.DBI.DBIVrijgestelRoerendBUI.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            

            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "id__BelgianBranchMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "IncomeMergerAcquisitionRetainedSharesSimilarRegulationOtherEUMemberState"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.DBI.DBILidEuBEI.ToXbrlValue()
                , Locked=true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "id__ForeignBranchMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "IncomeMergerAcquisitionRetainedSharesSimilarRegulationOtherEUMemberState"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.DBI.DBILidEuBUI.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "id__BelgianBranchMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "ExemptIncomeMovableAssetsRefinancingLoans"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.DBI.DBIHerfinancBEI.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "id__ForeignBranchMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "ExemptIncomeMovableAssetsRefinancingLoans"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.DBI.DBIHerfinancBUI.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


           


            #endregion


            #region AangifteVenBApp - OverdrachtDBIAftrek

            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "AccumulatedPEExemptIncomeMovableAssets"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.OverdrachtDBIAftrek.ODASaldoOvergedragen.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "CarryOverNextTaxPeriodInsufficientProfitsPEExemptIncomeMovableAssetsCurrentTaxPeriod"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.OverdrachtDBIAftrek.ODABedragHuidigNaarVolgend.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "DeductionPEExemptIncomeMovableAssetsPreviousTaxPeriod"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.OverdrachtDBIAftrek.ODABedragSub1Werkelijk.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


           

            #endregion
   #region AangifteVenBApp - CompenseerbareVerliezen

            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "CompensableTaxLosses"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.CompenseerbareVerliezen.CVSaldoVorigeVerliezen.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "CompensatedTaxLosses"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.CompenseerbareVerliezen.CVGecompenseerdeVerliezen.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }

            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "CompensatedTaxLossesIncludingTaxTreaty"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.UiteenzettingWinst.WARWVorigeVerliezenBelg.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc != null)
                    {
                        pxedc.Value = xedc.Value; pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }

            

            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "LossCurrentTaxPeriod"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.CompenseerbareVerliezen.CVVerliesBelastbaarTijdperk.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


           


            #endregion

            #region AangifteVenBApp - OverdrachtNID
            // Calculated in NID (275C)
            //xedc = new bzt.XbrlElementDataContract
            //{
            //    AutoRendered = true
            //  ,
            //    Children = null
            //  ,
            //    Context = ""
            //  ,
            //    Period = "D"
            //  ,
            //    Decimals = "INF"
            //  ,
            //    Name = "ExemptionsAllowanceCorporateEquityAccumulatedUpToAndIncludingAssessmentYear2012"
            //  ,
            //    NameSpace = ""
            //  ,
            //    Prefix = "tax-inc"
            //  ,
            //    UnitRef = "EUR"
            //  ,
            //    Value = set._datacontainer.AangifteVenBApp.Data.OverdrachtNID.ONIDSaldoOvergedragen.ToXbrlValue()
            //    ,
            //    Locked = true
            //};

            //if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            //{
            //    if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
            //    {
            //        pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
            //        if (pxedc!=null)
            //        {
            //            pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
            //        }
            //    }
            //    else
            //    {
            //        pxedc = xedc;
            //        btdc.Elements.Add(pxedc);
            //    }
            //}

            // Calculated in 275C
            //xedc = new bzt.XbrlElementDataContract
            //{
            //    AutoRendered = true
            //  ,
            //    Children = null
            //  ,
            //    Context = ""
            //  ,
            //    Period = "D"
            //  ,
            //    Decimals = "INF"
            //  ,
            //    Name = "CarryOverExemptionsAllowanceCorporateEquityAccumulatedUpToAndIncludingAssessmentYear2012"
            //  ,
            //    NameSpace = ""
            //  ,
            //    Prefix = "tax-inc"
            //  ,
            //    UnitRef = "EUR"
            //  ,
            //    Value = set._datacontainer.AangifteVenBApp.Data.OverdrachtNID.ONIDOverdraagbaarVolgend.ToXbrlValue()
            //    ,
            //    Locked = true
            //};

            //if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            //{
            //    if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
            //    {
            //        pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
            //        if (pxedc!=null)
            //        {
            //            pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
            //        }
            //    }
            //    else
            //    {
            //        pxedc = xedc;
            //        btdc.Elements.Add(pxedc);
            //    }
            //}


            #endregion


         


            #region AangifteVenBApp - TariefBelasting

            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = ""
              ,
                Name = "ExclusionReducedRate"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = ""
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.TariefBelasting.TARUitgeslotenVerminderd.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = ""
              ,
                Name = "CreditCorporationTradeEquipmentHousingCorporationTaxRate"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = ""
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.TariefBelasting.TAROnderworpen.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            #endregion


            #region AangifteVenBApp - VoorafBetalingen

            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = ""
              ,
                Name = "FirstThreeAccountingYearsSmallCompanyCorporationCode"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = ""
              ,
                //Value = set._datacontainer.Settings.OneOfFirstThree.ToXbrlValue()
                Value = set._datacontainer.Settings.CurrentPeriodSmall == true && set._datacontainer.Settings.OneOfFirstThree == true ? "true" : "false"
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "Prepayments"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VoorafBetalingen.VBTotaal.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "PrepaymentFirstQuarter"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VoorafBetalingen.VBEersteKwartaal.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "PrepaymentSecondQuarter"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VoorafBetalingen.VBTweedeKwartaal.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "PrepaymentThirdQuarter"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VoorafBetalingen.VBDerdeKwartaal.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "PrepaymentFourthQuarter"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VoorafBetalingen.VBVierdeKwartaal.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = ""
              ,
                Name = "PrepaymentReferenceNumberNotEntityIdentifierFirstOccurrence"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = ""
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VoorafBetalingen.VBEersteKwartaalReferentie.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = ""
              ,
                Name = "PrepaymentReferenceNumberNotEntityIdentifierSecondOccurrence"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = ""
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VoorafBetalingen.VBTweedeKwartaalReferentie.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = ""
              ,
                Name = "PrepaymentReferenceNumberNotEntityIdentifierThirdOccurrence"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = ""
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VoorafBetalingen.VBDerdeKwartaalReferentie.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = ""
              ,
                Name = "PrepaymentReferenceNumberNotEntityIdentifierFourthOccurrence"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = ""
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VoorafBetalingen.VBVierdeKwartaalReferentie.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            #endregion


            #region AangifteVenBApp - VerrekenbareVoorheffing

            


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "NonRepayableFictiousWitholdingTax"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.BerekeningVenBApp.Data.VerrekenbareVoorheffing.VVNTFictieveRoerendeVoorheffing.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "NonRepayableLumpSumForeignTaxes"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.BerekeningVenBApp.Data.VerrekenbareVoorheffing.VVNTForfaitairBuitenlands.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "TaxCreditResearchDevelopment"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.BerekeningVenBApp.Data.VerrekenbareVoorheffing.VVNTBelastingKredietOnderzoekOntw.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            

            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "RepayableRealFictitiousWithholdingTaxOtherSharesPEExemptIncomeMovableAssetsBelgium"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.BerekeningVenBApp.Data.VerrekenbareVoorheffing.VVTBelgisch.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "RepayableWithholdingTaxPEExemptionLiquidationBonusesBonusesAcquisitionOwnSharesBelgiumForeign"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.BerekeningVenBApp.Data.VerrekenbareVoorheffing.VVTEigenBuitenlands.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "RepayableWithholdingTaxOtherPEForeign"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.BerekeningVenBApp.Data.VerrekenbareVoorheffing.VVTAndereBuitenlands.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "RepayableWithholdingTaxOtherLiquidationBonusesBonusesAcquisitionOwnShares"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.BerekeningVenBApp.Data.VerrekenbareVoorheffing.VVTAndereEigen.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "RepayableWithholdingTaxOtherDividends"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.BerekeningVenBApp.Data.VerrekenbareVoorheffing.VVTAndereRoerendeVoorheffing.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "OtherRepayableWithholdingTaxes"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.BerekeningVenBApp.Data.VerrekenbareVoorheffing.VVTAndere.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "TaxCreditResearchDevelopmentClearablePreviousTaxPeriodsRepayableCurrentAssessmentYear"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.BerekeningVenBApp.Data.VerrekenbareVoorheffing.VBelastingkredietHuiidTijdperk.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            #endregion


            #region AangifteVenBApp - TaxShelter

            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = ""
              ,
                Name = "CorporationAudiovisualWorksTaxShelterAgreement"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = ""
              ,
                //Value = set._datacontainer.AangifteVenBApp.Data.TaxShelter.TASTaxShelter.ToXbrlValue()
                Value = set._datacontainer.Settings.TaxShelter.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            #endregion

            
            #region AangifteVenBApp - GrootteVennootschap

            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = ""
              ,
                Name = "AssociatedCompanyCorporationCodeCurrentTaxPeriod"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = ""
              //,
               // Value = set._datacontainer.AangifteVenBApp.Data.GrootteVennootschap.GVBTVerbonden.ToXbrlValue()
                ,
                Locked = false
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "AnnualWorkForceAverageCorporationCodeCurrentTaxPeriod"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = ""
              ,
                //Value = set._datacontainer.AangifteVenBApp.Data.GrootteVennootschap.GVBTJaargemiddelde.ToXbrlValue()
                //,
                Locked = false
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "AnnualTurnoverExcludingVATCorporationCodeCurrentTaxPeriod"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              //,
                //Value = set._datacontainer.AangifteVenBApp.Data.GrootteVennootschap.GVBTJaarOmzet.ToXbrlValue()
                ,
                Locked = false
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "BalanceSheetTotalCorporationCodeCurrentTaxPeriod"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                //Value = set._datacontainer.AangifteVenBApp.Data.GrootteVennootschap.GVBTBalansTotaal.ToXbrlValue()
                //,
                Locked = false
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            #endregion
            

            #region DubieuzeDebiteurenApp - List2043


            // CLEANUP EXISTING
            List<string> crefs = btdc.Contexts.Where(c => c.PresContextId == "WriteDownsDebtClaimsSection").Select(c => c.Id).ToList();
            btdc.Elements.RemoveAll(e => crefs.Contains(e.ContextRef));

            btdc.Contexts.RemoveAll(c => c.PresContextId == "WriteDownsDebtClaimsSection");

            foreach (DubieuzeDebiteurenAppTypes.DubieuzeDebiteur2043 entity in set._datacontainer.DubieuzeDebiteurenApp.Data.List2043)
            {

                UpdateGridDubieuzeDebiteurenAppList2043(ref btdc, set, entity);
            }



            #endregion


            #region VoorzieningenApp - List2043
            crefs = btdc.Contexts.Where(c => c.PresContextId == "ProvisionsRisksExpensesSection").Select(c => c.Id).ToList();
            btdc.Elements.RemoveAll(e => crefs.Contains(e.ContextRef));

            btdc.Contexts.RemoveAll(c => c.PresContextId == "ProvisionsRisksExpensesSection");

            foreach (VoorzieningenAppTypes.Voorzieningen2043 entity in set._datacontainer.VoorzieningenApp.Data.List2043)
            {

                UpdateGridVoorzieningenAppList2043(ref btdc, set, entity);
            }



            #endregion


            #region RisicoKapitaalApp - Fiche

            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "Equity"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.RisicoKapitaalApp.Data.Fiche.EigenVermogen.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


           

            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "OwnSharesFiscalValue"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.RisicoKapitaalApp.Data.Fiche.EigenAandelen.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "FinancialFixedAssetsParticipationsOtherShares"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.RisicoKapitaalApp.Data.Fiche.FinancVasteActiva.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }

            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "SharesIncomeEligibleDeductionPEExemptIncomeMovableAssets"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.RisicoKapitaalApp.Data.Fiche.AandelenDBI.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc != null)
                    {
                        pxedc.Value = xedc.Value; pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }

            /* BESTAAN NIET MEER !!!
            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "SharesInvestmentCorporations"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.RisicoKapitaalApp.Data.Fiche.AandelenBelegging.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "BranchesCountryTaxTreaty"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.RisicoKapitaalApp.Data.Fiche.InrichtingVerdrag.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "ImmovablePropertyCountryTaxTreaty"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.RisicoKapitaalApp.Data.Fiche.OnroerendVerdrag.ToXbrlValue()
                ,
                Locked = true
            };
            */
            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "TangibleFixedAssetsUnreasonableRelatedCosts"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.RisicoKapitaalApp.Data.Fiche.MvaOnredelijk.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "InvestmentsNoPeriodicalIncome"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.RisicoKapitaalApp.Data.Fiche.BestanddelenBelegging.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "ImmovablePropertyUseManager"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.RisicoKapitaalApp.Data.Fiche.OnroerendeBedrijfsleiders.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "UnrealisedExpressedCapitalGains"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.RisicoKapitaalApp.Data.Fiche.UitgedrukteMeerwaarden.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "TaxCreditResearchDevelopmentDeductionEquityAllowanceCorporateEquity"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.RisicoKapitaalApp.Data.Fiche.Belastingkrediet.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "InvestmentGrants"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.RisicoKapitaalApp.Data.Fiche.KapitaalSubsidies.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "ActualisationStockRecognisedDiamondTraders"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.RisicoKapitaalApp.Data.Fiche.Voorraadactualisering.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "BorrowedFundsMainEstablishmentInterestChargedTaxableResultBelgianBranch"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.RisicoKapitaalApp.Data.Fiche.BelgischeInrichting.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


           

            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "AllowanceCorporateEquityCurrentTaxPeriod"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.RisicoKapitaalApp.Data.Fiche.RisicoKapitaalBelastbaar.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "DeductibleAllowanceCorporateEquityCurrentAssessmentYear"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.RisicoKapitaalApp.Data.Fiche.Aftrekbaar.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }

            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "DeductionBranchImmovablePropertyInEEATreaty"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.RisicoKapitaalApp.Data.Fiche.VerminderingInEER.ToXbrlValue()
                 ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc != null)
                    {
                        pxedc.Value = xedc.Value; pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }

            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "DeductionBranchImmovablePropertyOutsideEEATreaty"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.RisicoKapitaalApp.Data.Fiche.VerminderingBuitenEER.ToXbrlValue()
                 ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc != null)
                    {
                        pxedc.Value = xedc.Value; pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }

            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "DeductionsBranchImmovablePropertyInEEAOutsideEEATreatyAfterDeductions"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.RisicoKapitaalApp.Data.Fiche.AftrekbaarNaAftrek.ToXbrlValue()
                 ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc != null)
                    {
                        pxedc.Value = xedc.Value; pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }

            

            
            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "AllowanceCorporateEquity"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.RisicoKapitaalApp.Data.Fiche.WerkelijkAfgetrokken.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "ExemptionsAllowanceCorporateEquityAccumulatedUpToAndIncludingAssessmentYear2012"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.RisicoKapitaalHistoriekApp.Data.Fiche.GevormdeVrijstellingen.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "DeductionExemptionsAllowanceCorporateEquityAccumulatedUpToAndIncludingAssessmentYear2012"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.RisicoKapitaalHistoriekApp.Data.Fiche.WerkelijkAfgetrokkenVrijstellingen.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }

            // hier zou eigenlijk de beperking 60% moeten bij komen..


            // Deel vervallen tijdens huidige AJ
            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "NotDeductedPermanentlyLostExceedsSevenYearsExemptionsAllowanceCorporateEquityAccumulatedUpToAndIncludingAssessmentYear2012"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.RisicoKapitaalHistoriekApp.Data.Fiche.SaldoVervallendHuidigAJ.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc != null)
                    {
                        pxedc.Value = xedc.Value; pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }

            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "CarryOverExemptionsAllowanceCorporateEquityAccumulatedUpToAndIncludingAssessmentYear2012"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.RisicoKapitaalHistoriekApp.Data.Fiche.SaldoOvergedragenVrijstellingen.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }

            #endregion


            #region RisicoKapitaalApp - Wijzigingen
            int i = 1;

            crefs = btdc.Contexts.Where(c => c.PresContextId == "ApplicableDurationMovementEquityAllowanceCorporateEquity").Select(c => c.Id).ToList();
            btdc.Elements.RemoveAll(e => crefs.Contains(e.ContextRef));

            btdc.Contexts.RemoveAll(c => c.PresContextId == "ApplicableDurationMovementEquityAllowanceCorporateEquity");


            foreach (RisicoKapitaalAppTypes.WijzigingItem entity in set._datacontainer.RisicoKapitaalApp.Data.Wijzigingen)
            {

                UpdateGridRisicoKapitaalAppWijzigingen(ref btdc, set, entity, i);
                i++;
            }



            #endregion


            return btdc;

        }
        #endregion

        #region Worksheet specific BizTaxDataContract

        private void UpdateGridDubieuzeDebiteurenAppList2043(ref bzt.Contracts.BizTaxDataContract btdc, WorksheetSet set, DubieuzeDebiteurenAppTypes.DubieuzeDebiteur2043 entity)
        {

            bzt.XbrlElementDataContract xedc = null;
            bzt.XbrlElementDataContract pxedc = null;
            string xvalue = null;
            List<string> crefs = btdc.Contexts.Where(c=>c.DefId==entity.Id).Select(c=>c.Id).ToList();

            string ctxid = AddContext(new List<bzt.ContextScenarioDataContract> {
                new bzt.ContextScenarioExplicitDataContract {
                        AutoRendered=true
                        , Dimension = "d-ec:ExemptionCategoryDimension"
                        , Value = entity.ExplicitMember.ToXbrlValue()
                    }    
                ,new bzt.ContextScenarioTypeDataContract {
                        AutoRendered=true
                        , Dimension = "d-ty:WriteDownDebtClaimTypedDimension"
                        , Type="d-ty:WritedownDebtClaimTypedID"
                        , Value =  entity.TypedMember
                    }
                }
                , set._datacontainer.DataContract.StartDate
                , set._datacontainer.DataContract.EndDate
                , entity.Id
                , "WriteDownsDebtClaimsSection"
                );


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ctxid
              ,
                Period = "D"
              ,
                Decimals = ""
              ,
                Name = "IdentityTradeDebtor"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = ""
              ,
                Value = entity.Schuldenaar.ToXbrlValue()
                , Locked=true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                btdc.Elements.Add(xedc);
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ctxid
              ,
                Period = "D"
              ,
                Decimals = ""
              ,
                Name = "AddressTradeDebtor"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = ""
              ,
                Value = entity.SchuldenaarAdres.ToXbrlValue()
                ,Locked=true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                btdc.Elements.Add(xedc);
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ctxid
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "DebtClaim"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = entity.VorderingStart.ToXbrlValue()
                ,Locked=true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                btdc.Elements.Add(xedc);
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ctxid
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "ExemptWriteDownDebtClaim"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = entity.WaardeverminderingStart.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                btdc.Elements.Add(xedc);
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ctxid
              ,
                Period = "I-End"
              ,
                Decimals = "INF"
              ,
                Name = "DebtClaim"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = entity.VorderingEinde.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                btdc.Elements.Add(xedc);
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ctxid
              ,
                Period = "I-End"
              ,
                Decimals = "INF"
              ,
                Name = "ExemptWriteDownDebtClaim"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = entity.WaardeverminderingEinde.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                btdc.Elements.Add(xedc);
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ctxid
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "DecreaseExemptWriteDownDebtClaimFinalWriteDownCurrentTaxPeriod"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = entity.VerminderingWVVerlies.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                btdc.Elements.Add(xedc);
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context =ctxid
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "DecreaseExemptWriteDownDebtClaimFullPartialCollectionCurrentTaxPeriod"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = entity.VerminderingWVInning.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                btdc.Elements.Add(xedc);
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ctxid
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "DecreaseExemptWriteDownDebtClaimNewEstimationProbableLoss"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = entity.VerminderingWVHerschat.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                btdc.Elements.Add(xedc);
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ctxid
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "IncreaseExemptWriteDownDebtClaim"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = entity.VermeerderingWV.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                btdc.Elements.Add(xedc);
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ctxid
              ,
                Period = "D"
              ,
                Decimals = ""
              ,
                Name = "JustificationExemptWriteDown"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = ""
              ,
                Value = entity.Commentaar.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                btdc.Elements.Add(xedc);
            }

        }

        private void UpdateGridVoorzieningenAppList2043(ref bzt.Contracts.BizTaxDataContract btdc, WorksheetSet set, VoorzieningenAppTypes.Voorzieningen2043 entity)
        {

            bzt.XbrlElementDataContract xedc = null;
            bzt.XbrlElementDataContract pxedc = null;
            string xvalue = null;
            List<string> crefs = btdc.Contexts.Where(c => c.DefId == entity.Id).Select(c => c.Id).ToList();

            string ctxid = AddContext(new List<bzt.ContextScenarioDataContract> {
                new bzt.ContextScenarioExplicitDataContract {
                        AutoRendered=true
                        , Dimension = "d-ec:ExemptionCategoryDimension"
                        , Value = entity.ExplicitMemberCategory.ToXbrlValue()
                    }  
                , new bzt.ContextScenarioExplicitDataContract {
                        AutoRendered=true
                        , Dimension = "d-expt:ExpenseTypeDimension"
                        , Value = entity.ExplicitMemberKind.ToXbrlValue()
                    }  
                ,new bzt.ContextScenarioTypeDataContract {
                        AutoRendered=true
                        , Dimension = "d-ty:ProvisionRiskExpenseTypedDimension"
                        , Type="d-ty:ProvisionRiskExpenseTypedID"
                        , Value =  entity.TypedMember.Trim()
                    }
                }
                , set._datacontainer.DataContract.StartDate
                , set._datacontainer.DataContract.EndDate
                , entity.Id
                , "ProvisionsRisksExpensesSection"
                );


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ctxid
              ,
                Period = "I-End"
              ,
                Decimals = "INF"
              ,
                Name = "ProbableCost"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = entity.WaarschijnlijkeKosten.ToXbrlValue()
                ,Locked=true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                btdc.Elements.Add(xedc);
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ctxid
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "ExemptProvisionRisksExpenses"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = entity.VoorzieningBeginBj.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                btdc.Elements.Add(xedc);
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ctxid
              ,
                Period = "I-End"
              ,
                Decimals = "INF"
              ,
                Name = "ExemptProvisionRisksExpenses"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = entity.VoorzieningEindeBJ.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                btdc.Elements.Add(xedc);
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ctxid
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "DecreaseExemptProvisionRisksExpensesBorneExpensesCurrentTaxPeriod"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = entity.VerminderingTijdensBJAanwending.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                btdc.Elements.Add(xedc);
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ctxid
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "DecreaseExemptProvisionRisksExpensesNewEstimationProbableExpenses"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = entity.VerminderingTijdensBJNieuweInschatting.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                btdc.Elements.Add(xedc);
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ctxid
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "IncreaseExemptProvisionRisksExpenses"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = entity.VerhogingTijdensBJ.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                btdc.Elements.Add(xedc);
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ctxid
              ,
                Period = "D"
              ,
                Decimals = ""
              ,
                Name = "JustificationExemptProvision"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = ""
              ,
                Value = entity.Commentaar.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                btdc.Elements.Add(xedc);
            }

        }

        private void UpdateGridRisicoKapitaalAppWijzigingen(ref bzt.Contracts.BizTaxDataContract btdc, WorksheetSet set, RisicoKapitaalAppTypes.WijzigingItem entity,int nr)
        {

            bzt.XbrlElementDataContract xedc = null;
            bzt.XbrlElementDataContract pxedc = null;
            string xvalue = null;
            string description = string.Format("{0}-{1}", nr, entity.Notitie);

            List<string> crefs = btdc.Contexts.Where(c => c.DefId == entity.Id).Select(c => c.Id).ToList();

            string ctxid = AddContext(new List<bzt.ContextScenarioDataContract> {
                new bzt.ContextScenarioTypeDataContract {
                        AutoRendered=true
                        , Dimension = "d-ty:DescriptionTypedDimension"
                        , Type="d-ty:DescriptionTypedID"
                        , Value = description.ToXbrlValue()
                    }
                ,new bzt.ContextScenarioTypeDataContract {
                        AutoRendered=true
                        , Dimension = "d-ty:DateTypedDimension"
                        , Type="d-ty:DateTypedID"
                        , Value =  entity.DatumTransactie.ToXbrlValue()
                    }
                }
                , set._datacontainer.DataContract.StartDate
                , set._datacontainer.DataContract.EndDate
                , entity.Id
                , "ApplicableDurationMovementEquityAllowanceCorporateEquity"
                );


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ctxid
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "MovementEquityAfterDeductionsAllowanceCorporateEquity"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = entity.XbrlValue.ToXbrlValue()
                , Locked=true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                btdc.Elements.Add(xedc);
            }

        }

        #endregion

      

        #endregion
    

    }
}
