
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.RuleEngine.Core;
using EY.com.eBook.RuleEngine.Core.Interfaces;
using EY.com.eBook.RuleEngine.Data;
using EY.com.eBook.Core;
using EY.com.eBook.API.Contracts.Data.Cache;

namespace EY.com.eBook.RuleEngine.AY2014
{
    
    public class BelastingVrijeReservesApp
    {
        public static WorksheetTypeSettings Settings = new WorksheetTypeSettings
                                                      {
                                                        Clients = true
                                                        ,
                                                        Suppliers = true
                                                        ,
                                                        AllAccounts = true
                                                        , 
                                                        PerformsBookings=true
                                                      };
                                                      
        public static Guid TypeId = new Guid("a1b361d5-4170-4241-9dc4-e204bd8f677c");

        public List<string> ImportablePreviousTypes  = new List<string>() {  "EY.com.eBook.RuleEngine.AY2013.FacturatieApp"  };

        public Guid GetMyType() { return TypeId; }

        public void SetMinimal()
        {
            if (Data == null)
            {
                Data = new EY.com.eBook.RuleEngine.AY2014.BelastingVrijeReservesAppTypes.BelastingVrijeReserves();
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.VrijgesteldeReserves == null)
            {
                Data.VrijgesteldeReserves = new EY.com.eBook.RuleEngine.AY2014.BelastingVrijeReservesAppTypes.Detail
                {
                    Id = Guid.NewGuid()
                    ,
                    LastUpdated = null
                    ,
                    IsValid = true
                    ,
                    PreviousImported = false

                };
                Data.LastUpdated = DateTime.Now;
            }

        }
        
        public string ToXml() 
        {
            return CoreHelper.SerializeToString(Data);
        }

        public void LoadXml(string xml)
        {
            if (!string.IsNullOrEmpty(xml)) {
                Data = CoreHelper.DeserializeFromString<EY.com.eBook.RuleEngine.AY2014.BelastingVrijeReservesAppTypes.BelastingVrijeReserves>(xml);
            }
            SetMinimal();
            
        }
        
        public EY.com.eBook.RuleEngine.AY2014.BelastingVrijeReservesAppTypes.BelastingVrijeReserves Data { get; set; }

        public void ForceRecalculation(ref WorksheetSetData worksheetset)
        {
            Data.LastUpdated = null;
            Calculate(ref worksheetset);
        }


        public void Calculate(ref WorksheetSetData worksheetset)
        {
            if (!Data.LastUpdated.HasValue) Data.LastUpdated = new DateTime(1980,1,1);
            //worksheetset.Messages.RemoveAll(m => m.ConnectionGuid == TypeId);
            LoadWorksheets(ref worksheetset);

            LoadMappings(ref worksheetset);
            
            Data.LastUpdated = DateTime.Now;
        }

        private void LoadWorksheets(ref WorksheetSetData worksheetset)
        {
            Guid vrijgesteld = new Guid("9afe7536-2a45-4146-ad96-1f7839fda38c");

            if (Data.LastUpdated.Value <= worksheetset.DubieuzeDebiteurenApp.Data.LastUpdated)
            {
                // DUB DEB
                Data.VrijgesteldeReserves.AWaardeverminderingA = 0;
                Data.VrijgesteldeReserves.AWaardeverminderingB = 0;
                Data.VrijgesteldeReserves.AWaardeverminderingC = 0;
                Data.VrijgesteldeReserves.AWaardeverminderingD = 0;

                foreach (DubieuzeDebiteurenAppTypes.DubieuzeDebiteurH dubdeb in worksheetset.DubieuzeDebiteurenApp.Data.ToestandEindHuidigBoekjaar.Where(d => d.BelastbaarOfVrijgesteld.Id == vrijgesteld))
                {
                    Data.VrijgesteldeReserves.AWaardeverminderingA = Data.VrijgesteldeReserves.AWaardeverminderingA.Value + dubdeb.WaardeverminderingStart.Value;
                    Data.VrijgesteldeReserves.AWaardeverminderingD = Data.VrijgesteldeReserves.AWaardeverminderingD.Value + dubdeb.WaardeverminderingEinde.Value;
                    decimal diff = dubdeb.WaardeverminderingEinde.Value - dubdeb.WaardeverminderingStart.Value;
                    if (diff > 0)
                    {
                        Data.VrijgesteldeReserves.AWaardeverminderingB = Data.VrijgesteldeReserves.AWaardeverminderingB.Value + diff;
                    }
                    else
                    {
                        Data.VrijgesteldeReserves.AWaardeverminderingC = Data.VrijgesteldeReserves.AWaardeverminderingC.Value + Math.Abs(diff);
                    }
                }

            }

            if (Data.LastUpdated.Value <= worksheetset.VoorzieningenApp.Data.LastUpdated)
            {
                // VOORZIENING
                Data.VrijgesteldeReserves.BVoorzieningA = 0;
                Data.VrijgesteldeReserves.BVoorzieningB = 0;
                Data.VrijgesteldeReserves.BVoorzieningC = 0;
                Data.VrijgesteldeReserves.BVoorzieningD = 0;

                foreach (VoorzieningenAppTypes.VoorzieningenH voorz in worksheetset.VoorzieningenApp.Data.ToestandEindHuidigBoekjaar.Where(d =>d.Belastbaar !=null &&  d.Belastbaar.Id == vrijgesteld))
                {
                    Data.VrijgesteldeReserves.BVoorzieningA = Data.VrijgesteldeReserves.BVoorzieningA.Value + voorz.VoorzieningBeginBj.Value;
                    Data.VrijgesteldeReserves.BVoorzieningD = Data.VrijgesteldeReserves.BVoorzieningD.Value + voorz.VoorzieningEindeBJ.Value;
                    decimal diff = voorz.VoorzieningEindeBJ.Value - voorz.VoorzieningBeginBj.Value;
                    if (diff > 0)
                    {
                        Data.VrijgesteldeReserves.BVoorzieningB = Data.VrijgesteldeReserves.BVoorzieningB.Value + diff;
                    }
                    else
                    {
                        Data.VrijgesteldeReserves.BVoorzieningC = Data.VrijgesteldeReserves.BVoorzieningC.Value + Math.Abs(diff);
                    }
                }

            }

        }

        private void LoadMappings(ref WorksheetSetData worksheetset)
        {
            List<AccountDataContract> accounts = worksheetset.GetAccountsOfMapping("Reserves",false,false,true,worksheetset.DataContract.Culture);

            Guid mappingItemId = new Guid("67E6D44C-B46F-4AA4-9FC1-A7EB4C9C7931");

            Data.VrijgesteldeReserves.CNietVerwezMWA = accounts.Where(a=> a.Mappings.Count(m=>m.MappingItemId==mappingItemId)==1).Sum(a=>a.PreviousSaldo);
            Data.VrijgesteldeReserves.CNietVerwezMWD = accounts.Where(a=> a.Mappings.Count(m=>m.MappingItemId==mappingItemId)==1).Sum(a=>a.Saldo);
            Data.VrijgesteldeReserves.CNietVerwezMWA = Data.VrijgesteldeReserves.CNietVerwezMWA.HasValue ? Data.VrijgesteldeReserves.CNietVerwezMWA.Value : 0;
            Data.VrijgesteldeReserves.CNietVerwezMWD = Data.VrijgesteldeReserves.CNietVerwezMWD.HasValue ? Data.VrijgesteldeReserves.CNietVerwezMWD.Value : 0;
            decimal diff  = Data.VrijgesteldeReserves.CNietVerwezMWD.Value - Data.VrijgesteldeReserves.CNietVerwezMWA.Value;
            Data.VrijgesteldeReserves.CNietVerwezMWB = diff > 0 ? diff : 0;
            Data.VrijgesteldeReserves.CNietVerwezMWC = diff < 0 ? Math.Abs(diff) : 0;

            mappingItemId = new Guid("6C629FA6-EB79-4727-A9B8-4FC97984CD48");
            Data.VrijgesteldeReserves.CInbrengA = 0-accounts.Where(a => a.Mappings.Count(m => m.MappingItemId == mappingItemId) == 1).Sum(a => a.PreviousSaldo);
            Data.VrijgesteldeReserves.CInbrengD = 0-accounts.Where(a => a.Mappings.Count(m => m.MappingItemId == mappingItemId) == 1).Sum(a => a.Saldo);
            Data.VrijgesteldeReserves.CInbrengA = Data.VrijgesteldeReserves.CInbrengA.HasValue ? Data.VrijgesteldeReserves.CInbrengA.Value : 0;
            Data.VrijgesteldeReserves.CInbrengD = Data.VrijgesteldeReserves.CInbrengD.HasValue ? Data.VrijgesteldeReserves.CInbrengD.Value : 0;
            diff = Data.VrijgesteldeReserves.CInbrengD.Value - Data.VrijgesteldeReserves.CInbrengA.Value;
            Data.VrijgesteldeReserves.CInbrengB = diff > 0 ? diff : 0;
            Data.VrijgesteldeReserves.CInbrengC = diff < 0 ? Math.Abs(diff) : 0;

            mappingItemId = new Guid("D9AE297D-0294-4319-B704-F9C35D86BAE7");
            Data.VrijgesteldeReserves.CHerschattingA = 0-accounts.Where(a => a.Mappings.Count(m => m.MappingItemId == mappingItemId) == 1).Sum(a => a.PreviousSaldo);
            Data.VrijgesteldeReserves.CHerschattingD = 0-accounts.Where(a => a.Mappings.Count(m => m.MappingItemId == mappingItemId) == 1).Sum(a => a.Saldo);
            Data.VrijgesteldeReserves.CHerschattingA = Data.VrijgesteldeReserves.CHerschattingA.HasValue ? Data.VrijgesteldeReserves.CHerschattingA.Value : 0;
            Data.VrijgesteldeReserves.CHerschattingD = Data.VrijgesteldeReserves.CHerschattingD.HasValue ? Data.VrijgesteldeReserves.CHerschattingD.Value : 0;
            diff = Data.VrijgesteldeReserves.CHerschattingD.Value - Data.VrijgesteldeReserves.CHerschattingA.Value;
            Data.VrijgesteldeReserves.CHerschattingB = diff > 0 ? diff : 0;
            Data.VrijgesteldeReserves.CHerschattingC = diff < 0 ? Math.Abs(diff) : 0;

            mappingItemId = new Guid("F55BE2C9-C14B-4D1B-A677-D15235AC76C6");
            Data.VrijgesteldeReserves.CAndereA = 0-accounts.Where(a => a.Mappings.Count(m => m.MappingItemId == mappingItemId) == 1).Sum(a => a.PreviousSaldo);
            Data.VrijgesteldeReserves.CAndereD = 0-accounts.Where(a => a.Mappings.Count(m => m.MappingItemId == mappingItemId) == 1).Sum(a => a.Saldo);
            Data.VrijgesteldeReserves.CAndereA = Data.VrijgesteldeReserves.CAndereA.HasValue ? Data.VrijgesteldeReserves.CAndereA.Value : 0;
            Data.VrijgesteldeReserves.CAndereD = Data.VrijgesteldeReserves.CAndereD.HasValue ? Data.VrijgesteldeReserves.CAndereD.Value : 0;
            diff = Data.VrijgesteldeReserves.CAndereD.Value - Data.VrijgesteldeReserves.CAndereA.Value;
            Data.VrijgesteldeReserves.CAndereB = diff > 0 ? diff : 0;
            Data.VrijgesteldeReserves.CAndereC = diff < 0 ? Math.Abs(diff) : 0;

            Data.VrijgesteldeReserves.CTotaalA = Data.VrijgesteldeReserves.CNietVerwezMWA
                                                    + Data.VrijgesteldeReserves.CInbrengA
                                                    + Data.VrijgesteldeReserves.CHerschattingA
                                                    + Data.VrijgesteldeReserves.CAndereA;

            Data.VrijgesteldeReserves.CTotaalB = Data.VrijgesteldeReserves.CNietVerwezMWB
                                                    + Data.VrijgesteldeReserves.CInbrengB
                                                    + Data.VrijgesteldeReserves.CHerschattingB
                                                    + Data.VrijgesteldeReserves.CAndereB;

            Data.VrijgesteldeReserves.CTotaalC = Data.VrijgesteldeReserves.CNietVerwezMWC
                                                    + Data.VrijgesteldeReserves.CInbrengC
                                                    + Data.VrijgesteldeReserves.CHerschattingC
                                                    + Data.VrijgesteldeReserves.CAndereC;

            Data.VrijgesteldeReserves.CTotaalD = Data.VrijgesteldeReserves.CNietVerwezMWD
                                                    + Data.VrijgesteldeReserves.CInbrengD
                                                    + Data.VrijgesteldeReserves.CHerschattingD
                                                    + Data.VrijgesteldeReserves.CAndereD;


            mappingItemId = new Guid("6858BFC2-0EDF-49BC-A6E9-B6C2BF9547BB");
            Data.VrijgesteldeReserves.DBedrijfsAfdelingenA = 0-accounts.Where(a => a.Mappings.Count(m => m.MappingItemId == mappingItemId) == 1).Sum(a => a.PreviousSaldo);
            Data.VrijgesteldeReserves.DBedrijfsAfdelingenD = 0-accounts.Where(a => a.Mappings.Count(m => m.MappingItemId == mappingItemId) == 1).Sum(a => a.Saldo);
            Data.VrijgesteldeReserves.DBedrijfsAfdelingenA = Data.VrijgesteldeReserves.DBedrijfsAfdelingenA.HasValue ? Data.VrijgesteldeReserves.DBedrijfsAfdelingenA.Value : 0;
            Data.VrijgesteldeReserves.DBedrijfsAfdelingenD = Data.VrijgesteldeReserves.DBedrijfsAfdelingenD.HasValue ? Data.VrijgesteldeReserves.DBedrijfsAfdelingenD.Value : 0;
            diff = Data.VrijgesteldeReserves.DBedrijfsAfdelingenD.Value - Data.VrijgesteldeReserves.DBedrijfsAfdelingenA.Value;
            Data.VrijgesteldeReserves.DBedrijfsAfdelingenB = diff > 0 ? diff : 0;
            Data.VrijgesteldeReserves.DBedrijfsAfdelingenC = diff < 0 ? Math.Abs(diff) : 0;

            mappingItemId = new Guid("28241DBF-722A-448F-88D7-F8D41A9E6593");
            Data.VrijgesteldeReserves.DGedwongenA = 0-accounts.Where(a => a.Mappings.Count(m => m.MappingItemId == mappingItemId) == 1).Sum(a => a.PreviousSaldo);
            Data.VrijgesteldeReserves.DGedwongenD = 0-accounts.Where(a => a.Mappings.Count(m => m.MappingItemId == mappingItemId) == 1).Sum(a => a.Saldo);
            Data.VrijgesteldeReserves.DGedwongenA = Data.VrijgesteldeReserves.DGedwongenA.HasValue ? Data.VrijgesteldeReserves.DGedwongenA.Value : 0;
            Data.VrijgesteldeReserves.DGedwongenD = Data.VrijgesteldeReserves.DGedwongenD.HasValue ? Data.VrijgesteldeReserves.DGedwongenD.Value : 0;
            diff = Data.VrijgesteldeReserves.DGedwongenD.Value - Data.VrijgesteldeReserves.DGedwongenA.Value;
            Data.VrijgesteldeReserves.DGedwongenB = diff > 0 ? diff : 0;
            Data.VrijgesteldeReserves.DGedwongenC = diff < 0 ? Math.Abs(diff) : 0;


            mappingItemId = new Guid("4624CD9F-492F-481A-A646-05D02D307F9A");
            Data.VrijgesteldeReserves.DMonetairA = 0-accounts.Where(a => a.Mappings.Count(m => m.MappingItemId == mappingItemId) == 1).Sum(a => a.PreviousSaldo);
            Data.VrijgesteldeReserves.DMonetairD = 0-accounts.Where(a => a.Mappings.Count(m => m.MappingItemId == mappingItemId) == 1).Sum(a => a.Saldo);
            Data.VrijgesteldeReserves.DMonetairA = Data.VrijgesteldeReserves.DMonetairA.HasValue ? Data.VrijgesteldeReserves.DMonetairA.Value : 0;
            Data.VrijgesteldeReserves.DMonetairD = Data.VrijgesteldeReserves.DMonetairD.HasValue ? Data.VrijgesteldeReserves.DMonetairD.Value : 0;
            diff = Data.VrijgesteldeReserves.DMonetairD.Value - Data.VrijgesteldeReserves.DMonetairA.Value;
            Data.VrijgesteldeReserves.DMonetairB = diff > 0 ? diff : 0;
            Data.VrijgesteldeReserves.DMonetairC = diff < 0 ? Math.Abs(diff) : 0;

            mappingItemId = new Guid("AD83DDCD-67E3-4DD1-95CF-188B35771D75");
            Data.VrijgesteldeReserves.DNietMonetairA = 0-accounts.Where(a => a.Mappings.Count(m => m.MappingItemId == mappingItemId) == 1).Sum(a => a.PreviousSaldo);
            Data.VrijgesteldeReserves.DNietMonetairD = 0-accounts.Where(a => a.Mappings.Count(m => m.MappingItemId == mappingItemId) == 1).Sum(a => a.Saldo);
            Data.VrijgesteldeReserves.DNietMonetairA = Data.VrijgesteldeReserves.DNietMonetairA.HasValue ? Data.VrijgesteldeReserves.DNietMonetairA.Value : 0;
            Data.VrijgesteldeReserves.DNietMonetairD = Data.VrijgesteldeReserves.DNietMonetairD.HasValue ? Data.VrijgesteldeReserves.DNietMonetairD.Value : 0;
            diff = Data.VrijgesteldeReserves.DNietMonetairD.Value - Data.VrijgesteldeReserves.DNietMonetairA.Value;
            Data.VrijgesteldeReserves.DNietMonetairB = diff > 0 ? diff : 0;
            Data.VrijgesteldeReserves.DNietMonetairC = diff < 0 ? Math.Abs(diff) : 0;

            mappingItemId = new Guid("514703C4-2ED5-4F24-A9CA-B8141FF62F5E");
            Data.VrijgesteldeReserves.DNietVerwezenA = 0-accounts.Where(a => a.Mappings.Count(m => m.MappingItemId == mappingItemId) == 1).Sum(a => a.PreviousSaldo);
            Data.VrijgesteldeReserves.DNietVerwezenD = 0-accounts.Where(a => a.Mappings.Count(m => m.MappingItemId == mappingItemId) == 1).Sum(a => a.Saldo);
            Data.VrijgesteldeReserves.DNietVerwezenA = Data.VrijgesteldeReserves.DNietVerwezenA.HasValue ? Data.VrijgesteldeReserves.DNietVerwezenA.Value : 0;
            Data.VrijgesteldeReserves.DNietVerwezenD = Data.VrijgesteldeReserves.DNietVerwezenD.HasValue ? Data.VrijgesteldeReserves.DNietVerwezenD.Value : 0;
            diff = Data.VrijgesteldeReserves.DNietVerwezenD.Value - Data.VrijgesteldeReserves.DNietVerwezenA.Value;
            Data.VrijgesteldeReserves.DNietVerwezenB = diff > 0 ? diff : 0;
            Data.VrijgesteldeReserves.DNietVerwezenC = diff < 0 ? Math.Abs(diff) : 0;

            mappingItemId = new Guid("6F484FFA-EA68-426A-A8F7-2A08E21892E3");
            Data.VrijgesteldeReserves.DProbleemgebiedenA = 0-accounts.Where(a => a.Mappings.Count(m => m.MappingItemId == mappingItemId) == 1).Sum(a => a.PreviousSaldo);
            Data.VrijgesteldeReserves.DProbleemgebiedenD = 0-accounts.Where(a => a.Mappings.Count(m => m.MappingItemId == mappingItemId) == 1).Sum(a => a.Saldo);
            Data.VrijgesteldeReserves.DProbleemgebiedenA = Data.VrijgesteldeReserves.DProbleemgebiedenA.HasValue ? Data.VrijgesteldeReserves.DProbleemgebiedenA.Value : 0;
            Data.VrijgesteldeReserves.DProbleemgebiedenD = Data.VrijgesteldeReserves.DProbleemgebiedenD.HasValue ? Data.VrijgesteldeReserves.DProbleemgebiedenD.Value : 0;
            diff = Data.VrijgesteldeReserves.DProbleemgebiedenD.Value - Data.VrijgesteldeReserves.DProbleemgebiedenA.Value;
            Data.VrijgesteldeReserves.DProbleemgebiedenB = diff > 0 ? diff : 0;
            Data.VrijgesteldeReserves.DProbleemgebiedenC = diff < 0 ? Math.Abs(diff) : 0;

            mappingItemId = new Guid("7B9F56A8-3F27-46D0-BEC6-4337049DCCC2");
            Data.VrijgesteldeReserves.DVastgoedHandelaarsA = 0-accounts.Where(a => a.Mappings.Count(m => m.MappingItemId == mappingItemId) == 1).Sum(a => a.PreviousSaldo);
            Data.VrijgesteldeReserves.DVastgoedHandelaarsD = 0-accounts.Where(a => a.Mappings.Count(m => m.MappingItemId == mappingItemId) == 1).Sum(a => a.Saldo);
            Data.VrijgesteldeReserves.DVastgoedHandelaarsA = Data.VrijgesteldeReserves.DVastgoedHandelaarsA.HasValue ? Data.VrijgesteldeReserves.DVastgoedHandelaarsA.Value : 0;
            Data.VrijgesteldeReserves.DVastgoedHandelaarsD = Data.VrijgesteldeReserves.DVastgoedHandelaarsD.HasValue ? Data.VrijgesteldeReserves.DVastgoedHandelaarsD.Value : 0;
            diff = Data.VrijgesteldeReserves.DVastgoedHandelaarsD.Value - Data.VrijgesteldeReserves.DVastgoedHandelaarsA.Value;
            Data.VrijgesteldeReserves.DVastgoedHandelaarsB = diff > 0 ? diff : 0;
            Data.VrijgesteldeReserves.DVastgoedHandelaarsC = diff < 0 ? Math.Abs(diff) : 0;

            mappingItemId = new Guid("F5982A3E-F2AC-44A4-8A3D-5C1870D4B712");
            Data.VrijgesteldeReserves.DAndereA = 0-accounts.Where(a => a.Mappings.Count(m => m.MappingItemId == mappingItemId) == 1).Sum(a => a.PreviousSaldo);
            Data.VrijgesteldeReserves.DAndereD = 0-accounts.Where(a => a.Mappings.Count(m => m.MappingItemId == mappingItemId) == 1).Sum(a => a.Saldo);
            Data.VrijgesteldeReserves.DAndereA = Data.VrijgesteldeReserves.DAndereA.HasValue ? Data.VrijgesteldeReserves.DAndereA.Value : 0;
            Data.VrijgesteldeReserves.DAndereD = Data.VrijgesteldeReserves.DAndereD.HasValue ? Data.VrijgesteldeReserves.DAndereD.Value : 0;
            diff = Data.VrijgesteldeReserves.DAndereD.Value - Data.VrijgesteldeReserves.DAndereA.Value;
            Data.VrijgesteldeReserves.DAndereB = diff > 0 ? diff : 0;
            Data.VrijgesteldeReserves.DAndereC = diff < 0 ? Math.Abs(diff) : 0;


            Data.VrijgesteldeReserves.DTotaalA = Data.VrijgesteldeReserves.DBedrijfsAfdelingenA
                                                    + Data.VrijgesteldeReserves.DGedwongenA
                                                    + Data.VrijgesteldeReserves.DMonetairA
                                                    + Data.VrijgesteldeReserves.DNietMonetairA
                                                    + Data.VrijgesteldeReserves.DNietVerwezenA
                                                    + Data.VrijgesteldeReserves.DProbleemgebiedenA
                                                    + Data.VrijgesteldeReserves.DVastgoedHandelaarsA
                                                    + Data.VrijgesteldeReserves.DAndereA;

            Data.VrijgesteldeReserves.DTotaalB = Data.VrijgesteldeReserves.DBedrijfsAfdelingenB
                                                    + Data.VrijgesteldeReserves.DGedwongenB
                                                    + Data.VrijgesteldeReserves.DMonetairB
                                                    + Data.VrijgesteldeReserves.DNietMonetairB
                                                    + Data.VrijgesteldeReserves.DNietVerwezenB
                                                    + Data.VrijgesteldeReserves.DProbleemgebiedenB
                                                    + Data.VrijgesteldeReserves.DVastgoedHandelaarsB
                                                    + Data.VrijgesteldeReserves.DAndereB;

            Data.VrijgesteldeReserves.DTotaalC = Data.VrijgesteldeReserves.DBedrijfsAfdelingenC
                                                    + Data.VrijgesteldeReserves.DGedwongenC
                                                    + Data.VrijgesteldeReserves.DMonetairC
                                                    + Data.VrijgesteldeReserves.DNietMonetairC
                                                    + Data.VrijgesteldeReserves.DNietVerwezenC
                                                    + Data.VrijgesteldeReserves.DProbleemgebiedenC
                                                    + Data.VrijgesteldeReserves.DVastgoedHandelaarsC
                                                    + Data.VrijgesteldeReserves.DAndereC;

            Data.VrijgesteldeReserves.DTotaalD = Data.VrijgesteldeReserves.DBedrijfsAfdelingenD
                                                    + Data.VrijgesteldeReserves.DGedwongenD
                                                    + Data.VrijgesteldeReserves.DMonetairD
                                                    + Data.VrijgesteldeReserves.DNietMonetairD
                                                    + Data.VrijgesteldeReserves.DNietVerwezenD
                                                    + Data.VrijgesteldeReserves.DProbleemgebiedenD
                                                    + Data.VrijgesteldeReserves.DVastgoedHandelaarsD
                                                    + Data.VrijgesteldeReserves.DAndereD;
            

            mappingItemId = new Guid("D8314000-8B2C-4157-A550-3BA74D3C95C2");
            Data.VrijgesteldeReserves.EGespreidVerwezenA = 0-accounts.Where(a => a.Mappings.Count(m => m.MappingItemId == mappingItemId) == 1).Sum(a => a.PreviousSaldo);
            Data.VrijgesteldeReserves.EGespreidVerwezenD = 0-accounts.Where(a => a.Mappings.Count(m => m.MappingItemId == mappingItemId) == 1).Sum(a => a.Saldo);
            Data.VrijgesteldeReserves.EGespreidVerwezenA = Data.VrijgesteldeReserves.EGespreidVerwezenA.HasValue ? Data.VrijgesteldeReserves.EGespreidVerwezenA.Value : 0;
            Data.VrijgesteldeReserves.EGespreidVerwezenD = Data.VrijgesteldeReserves.EGespreidVerwezenD.HasValue ? Data.VrijgesteldeReserves.EGespreidVerwezenD.Value : 0;
            diff = Data.VrijgesteldeReserves.EGespreidVerwezenD.Value - Data.VrijgesteldeReserves.EGespreidVerwezenA.Value;
            Data.VrijgesteldeReserves.EGespreidVerwezenB = diff > 0 ? diff : 0;
            Data.VrijgesteldeReserves.EGespreidVerwezenC = diff < 0 ? Math.Abs(diff) : 0;

            mappingItemId = new Guid("EAEF62CC-FF45-49C1-B371-744E14142561");
            Data.VrijgesteldeReserves.EMatImmatVasteActivaA = 0 - accounts.Where(a => a.Mappings.Count(m => m.MappingItemId == mappingItemId) == 1).Sum(a => a.PreviousSaldo);
            Data.VrijgesteldeReserves.EMatImmatVasteActivaD = 0 - accounts.Where(a => a.Mappings.Count(m => m.MappingItemId == mappingItemId) == 1).Sum(a => a.Saldo);
            Data.VrijgesteldeReserves.EMatImmatVasteActivaA = Data.VrijgesteldeReserves.EMatImmatVasteActivaA.HasValue ? Data.VrijgesteldeReserves.EMatImmatVasteActivaA.Value : 0;
            Data.VrijgesteldeReserves.EMatImmatVasteActivaD = Data.VrijgesteldeReserves.EMatImmatVasteActivaD.HasValue ? Data.VrijgesteldeReserves.EMatImmatVasteActivaD.Value : 0;
            diff = Data.VrijgesteldeReserves.EMatImmatVasteActivaD.Value - Data.VrijgesteldeReserves.EMatImmatVasteActivaA.Value;
            Data.VrijgesteldeReserves.EMatImmatVasteActivaB = diff > 0 ? diff : 0;
            Data.VrijgesteldeReserves.EMatImmatVasteActivaC = diff < 0 ? Math.Abs(diff) : 0;
            
            mappingItemId = new Guid("96F82DEB-C464-45B0-B68F-780DA3A1E724");
            Data.VrijgesteldeReserves.FBedrijfsvoertuigenA = 0-accounts.Where(a => a.Mappings.Count(m => m.MappingItemId == mappingItemId) == 1).Sum(a => a.PreviousSaldo);
            Data.VrijgesteldeReserves.FBedrijfsvoertuigenD = 0-accounts.Where(a => a.Mappings.Count(m => m.MappingItemId == mappingItemId) == 1).Sum(a => a.Saldo);
            Data.VrijgesteldeReserves.FBedrijfsvoertuigenA = Data.VrijgesteldeReserves.FBedrijfsvoertuigenA.HasValue ? Data.VrijgesteldeReserves.FBedrijfsvoertuigenA.Value : 0;
            Data.VrijgesteldeReserves.FBedrijfsvoertuigenD = Data.VrijgesteldeReserves.FBedrijfsvoertuigenD.HasValue ? Data.VrijgesteldeReserves.FBedrijfsvoertuigenD.Value : 0;
            diff = Data.VrijgesteldeReserves.FBedrijfsvoertuigenD.Value - Data.VrijgesteldeReserves.FBedrijfsvoertuigenA.Value;
            Data.VrijgesteldeReserves.FBedrijfsvoertuigenB = diff > 0 ? diff : 0;
            Data.VrijgesteldeReserves.FBedrijfsvoertuigenC = diff < 0 ? Math.Abs(diff) : 0;

            mappingItemId = new Guid("798337D7-93BD-4B7D-B32D-E5BC66A532F5");
            Data.VrijgesteldeReserves.GBinnenschepenA = 0-accounts.Where(a => a.Mappings.Count(m => m.MappingItemId == mappingItemId) == 1).Sum(a => a.PreviousSaldo);
            Data.VrijgesteldeReserves.GBinnenschepenD = 0-accounts.Where(a => a.Mappings.Count(m => m.MappingItemId == mappingItemId) == 1).Sum(a => a.Saldo);
            Data.VrijgesteldeReserves.GBinnenschepenA = Data.VrijgesteldeReserves.GBinnenschepenA.HasValue ? Data.VrijgesteldeReserves.GBinnenschepenA.Value : 0;
            Data.VrijgesteldeReserves.GBinnenschepenD = Data.VrijgesteldeReserves.GBinnenschepenD.HasValue ? Data.VrijgesteldeReserves.GBinnenschepenD.Value : 0;
            diff = Data.VrijgesteldeReserves.GBinnenschepenD.Value - Data.VrijgesteldeReserves.GBinnenschepenA.Value;
            Data.VrijgesteldeReserves.GBinnenschepenB = diff > 0 ? diff : 0;
            Data.VrijgesteldeReserves.GBinnenschepenC = diff < 0 ? Math.Abs(diff) : 0;


            mappingItemId = new Guid("04106D37-50BE-4A8A-A761-5B73F6746C4F");
            Data.VrijgesteldeReserves.HZeeschepenA = 0-accounts.Where(a => a.Mappings.Count(m => m.MappingItemId == mappingItemId) == 1).Sum(a => a.PreviousSaldo);
            Data.VrijgesteldeReserves.HZeeschepenD = 0-accounts.Where(a => a.Mappings.Count(m => m.MappingItemId == mappingItemId) == 1).Sum(a => a.Saldo);
            Data.VrijgesteldeReserves.HZeeschepenA = Data.VrijgesteldeReserves.HZeeschepenA.HasValue ? Data.VrijgesteldeReserves.HZeeschepenA.Value : 0;
            Data.VrijgesteldeReserves.HZeeschepenD = Data.VrijgesteldeReserves.HZeeschepenD.HasValue ? Data.VrijgesteldeReserves.HZeeschepenD.Value : 0;
            diff = Data.VrijgesteldeReserves.HZeeschepenD.Value - Data.VrijgesteldeReserves.HZeeschepenA.Value;
            Data.VrijgesteldeReserves.HZeeschepenB = diff > 0 ? diff : 0;
            Data.VrijgesteldeReserves.HZeeschepenC = diff < 0 ? Math.Abs(diff) : 0;


            mappingItemId = new Guid("7B10A74C-6C24-470E-8284-A678F8CF38CB");
            Data.VrijgesteldeReserves.IInvesteringsreserveA = 0-accounts.Where(a => a.Mappings.Count(m => m.MappingItemId == mappingItemId) == 1).Sum(a => a.PreviousSaldo);
            Data.VrijgesteldeReserves.IInvesteringsreserveD = 0-accounts.Where(a => a.Mappings.Count(m => m.MappingItemId == mappingItemId) == 1).Sum(a => a.Saldo);
            Data.VrijgesteldeReserves.IInvesteringsreserveA = Data.VrijgesteldeReserves.IInvesteringsreserveA.HasValue ? Data.VrijgesteldeReserves.IInvesteringsreserveA.Value : 0;
            Data.VrijgesteldeReserves.IInvesteringsreserveD = Data.VrijgesteldeReserves.IInvesteringsreserveD.HasValue ? Data.VrijgesteldeReserves.IInvesteringsreserveD.Value : 0;
            diff = Data.VrijgesteldeReserves.IInvesteringsreserveD.Value - Data.VrijgesteldeReserves.IInvesteringsreserveA.Value;
            Data.VrijgesteldeReserves.IInvesteringsreserveB = diff > 0 ? diff : 0;
            Data.VrijgesteldeReserves.IInvesteringsreserveC = diff < 0 ? Math.Abs(diff) : 0;


            mappingItemId = new Guid("4C67EA9E-6FDD-4658-BBB6-E34B33B1DBD3");
            Data.VrijgesteldeReserves.JTaxShelterAudioVisueelA = 0-accounts.Where(a => a.Mappings.Count(m => m.MappingItemId == mappingItemId) == 1).Sum(a => a.PreviousSaldo);
            Data.VrijgesteldeReserves.JTaxShelterAudioVisueelD = 0-accounts.Where(a => a.Mappings.Count(m => m.MappingItemId == mappingItemId) == 1).Sum(a => a.Saldo);
            Data.VrijgesteldeReserves.JTaxShelterAudioVisueelA = Data.VrijgesteldeReserves.JTaxShelterAudioVisueelA.HasValue ? Data.VrijgesteldeReserves.JTaxShelterAudioVisueelA.Value : 0;
            Data.VrijgesteldeReserves.JTaxShelterAudioVisueelD = Data.VrijgesteldeReserves.JTaxShelterAudioVisueelD.HasValue ? Data.VrijgesteldeReserves.JTaxShelterAudioVisueelD.Value : 0;
            diff = Data.VrijgesteldeReserves.JTaxShelterAudioVisueelD.Value - Data.VrijgesteldeReserves.JTaxShelterAudioVisueelA.Value;
            Data.VrijgesteldeReserves.JTaxShelterAudioVisueelB = diff > 0 ? diff : 0;
            Data.VrijgesteldeReserves.JTaxShelterAudioVisueelC = diff < 0 ? Math.Abs(diff) : 0;

            mappingItemId = new Guid("371178C4-8B3A-41AD-91B6-5EA12DBE7D18");
            Data.VrijgesteldeReserves.JWinstHomologatieA = 0-accounts.Where(a => a.Mappings.Count(m => m.MappingItemId == mappingItemId) == 1).Sum(a => a.PreviousSaldo);
            Data.VrijgesteldeReserves.JWinstHomologatieD = 0-accounts.Where(a => a.Mappings.Count(m => m.MappingItemId == mappingItemId) == 1).Sum(a => a.Saldo);
            Data.VrijgesteldeReserves.JWinstHomologatieA = Data.VrijgesteldeReserves.JWinstHomologatieA.HasValue ? Data.VrijgesteldeReserves.JWinstHomologatieA.Value : 0;
            Data.VrijgesteldeReserves.JWinstHomologatieD = Data.VrijgesteldeReserves.JWinstHomologatieD.HasValue ? Data.VrijgesteldeReserves.JWinstHomologatieD.Value : 0;
            diff = Data.VrijgesteldeReserves.JWinstHomologatieD.Value - Data.VrijgesteldeReserves.JWinstHomologatieA.Value;
            Data.VrijgesteldeReserves.JWinstHomologatieB = diff > 0 ? diff : 0;
            Data.VrijgesteldeReserves.JWinstHomologatieC = diff < 0 ? Math.Abs(diff) : 0;


            mappingItemId = new Guid("ED435274-FAC7-425D-9D47-043BCD2886BD");
            Data.VrijgesteldeReserves.KAfschrAanschafBelegWaardeA = 0-accounts.Where(a => a.Mappings.Count(m => m.MappingItemId == mappingItemId) == 1).Sum(a => a.PreviousSaldo);
            Data.VrijgesteldeReserves.KAfschrAanschafBelegWaardeD = 0-accounts.Where(a => a.Mappings.Count(m => m.MappingItemId == mappingItemId) == 1).Sum(a => a.Saldo);
            Data.VrijgesteldeReserves.KAfschrAanschafBelegWaardeA = Data.VrijgesteldeReserves.KAfschrAanschafBelegWaardeA.HasValue ? Data.VrijgesteldeReserves.KAfschrAanschafBelegWaardeA.Value : 0;
            Data.VrijgesteldeReserves.KAfschrAanschafBelegWaardeD = Data.VrijgesteldeReserves.KAfschrAanschafBelegWaardeD.HasValue ? Data.VrijgesteldeReserves.KAfschrAanschafBelegWaardeD.Value : 0;
            diff = Data.VrijgesteldeReserves.KAfschrAanschafBelegWaardeD.Value - Data.VrijgesteldeReserves.KAfschrAanschafBelegWaardeA.Value;
            Data.VrijgesteldeReserves.KAfschrAanschafBelegWaardeB = diff > 0 ? diff : 0;
            Data.VrijgesteldeReserves.KAfschrAanschafBelegWaardeC = diff < 0 ? Math.Abs(diff) : 0;

            
            Data.VrijgesteldeReserves.KAfschrAanschBelegArt2A = Data.VrijgesteldeReserves.KAfschrAanschBelegArt2A.HasValue ? Data.VrijgesteldeReserves.KAfschrAanschBelegArt2A.Value : 0;
            Data.VrijgesteldeReserves.KAfschrAanschBelegArt2D = Data.VrijgesteldeReserves.KAfschrAanschBelegArt2D.HasValue ? Data.VrijgesteldeReserves.KAfschrAanschBelegArt2D.Value : 0;
            Data.VrijgesteldeReserves.KAfschrAanschBelegArt2B = 0;
            Data.VrijgesteldeReserves.KAfschrAanschBelegArt2C = 0;

            mappingItemId = new Guid("B6B11987-62DF-4380-BFF0-543E1C0F0C29");
            Data.VrijgesteldeReserves.KAndereA = 0-accounts.Where(a => a.Mappings.Count(m => m.MappingItemId == mappingItemId) == 1).Sum(a => a.PreviousSaldo);
            Data.VrijgesteldeReserves.KAndereD = 0-accounts.Where(a => a.Mappings.Count(m => m.MappingItemId == mappingItemId) == 1).Sum(a => a.Saldo);
            Data.VrijgesteldeReserves.KAndereA = Data.VrijgesteldeReserves.KAndereA.HasValue ? Data.VrijgesteldeReserves.KAndereA.Value : 0;
            Data.VrijgesteldeReserves.KAndereD = Data.VrijgesteldeReserves.KAndereD.HasValue ? Data.VrijgesteldeReserves.KAndereD.Value : 0;
            diff = Data.VrijgesteldeReserves.KAndereD.Value - Data.VrijgesteldeReserves.KAndereA.Value;
            Data.VrijgesteldeReserves.KAndereB = diff > 0 ? diff : 0;
            Data.VrijgesteldeReserves.KAndereC = diff < 0 ? Math.Abs(diff) : 0;

            mappingItemId = new Guid("3C6DAECA-12BA-4CDF-97DD-2DF426759E39");
            Data.VrijgesteldeReserves.KBedrijfsfietsenA = 0-accounts.Where(a => a.Mappings.Count(m => m.MappingItemId == mappingItemId) == 1).Sum(a => a.PreviousSaldo);
            Data.VrijgesteldeReserves.KBedrijfsfietsenD = 0-accounts.Where(a => a.Mappings.Count(m => m.MappingItemId == mappingItemId) == 1).Sum(a => a.Saldo);
            Data.VrijgesteldeReserves.KBedrijfsfietsenA = Data.VrijgesteldeReserves.KBedrijfsfietsenA.HasValue ? Data.VrijgesteldeReserves.KBedrijfsfietsenA.Value : 0;
            Data.VrijgesteldeReserves.KBedrijfsfietsenD = Data.VrijgesteldeReserves.KBedrijfsfietsenD.HasValue ? Data.VrijgesteldeReserves.KBedrijfsfietsenD.Value : 0;
            diff = Data.VrijgesteldeReserves.KBedrijfsfietsenD.Value - Data.VrijgesteldeReserves.KBedrijfsfietsenA.Value;
            Data.VrijgesteldeReserves.KBedrijfsfietsenB = diff > 0 ? diff : 0;
            Data.VrijgesteldeReserves.KBedrijfsfietsenC = diff < 0 ? Math.Abs(diff) : 0;

            mappingItemId = new Guid("9A493A8B-28D7-4BCA-9D74-E74891FB83E1");
            Data.VrijgesteldeReserves.KBeveiligingA = 0-accounts.Where(a => a.Mappings.Count(m => m.MappingItemId == mappingItemId) == 1).Sum(a => a.PreviousSaldo);
            Data.VrijgesteldeReserves.KBeveiligingD = 0-accounts.Where(a => a.Mappings.Count(m => m.MappingItemId == mappingItemId) == 1).Sum(a => a.Saldo);
            Data.VrijgesteldeReserves.KBeveiligingA = Data.VrijgesteldeReserves.KBeveiligingA.HasValue ? Data.VrijgesteldeReserves.KBeveiligingA.Value : 0;
            Data.VrijgesteldeReserves.KBeveiligingD = Data.VrijgesteldeReserves.KBeveiligingD.HasValue ? Data.VrijgesteldeReserves.KBeveiligingD.Value : 0;
            diff = Data.VrijgesteldeReserves.KBeveiligingD.Value - Data.VrijgesteldeReserves.KBeveiligingA.Value;
            Data.VrijgesteldeReserves.KBeveiligingB = diff > 0 ? diff : 0;
            Data.VrijgesteldeReserves.KBeveiligingC = diff < 0 ? Math.Abs(diff) : 0;

            mappingItemId = new Guid("AF272FD3-07F2-4382-BC2D-1DDB58E2005C");
            Data.VrijgesteldeReserves.KC02UitstootloosA = 0-accounts.Where(a => a.Mappings.Count(m => m.MappingItemId == mappingItemId) == 1).Sum(a => a.PreviousSaldo);
            Data.VrijgesteldeReserves.KC02UitstootloosD = 0-accounts.Where(a => a.Mappings.Count(m => m.MappingItemId == mappingItemId) == 1).Sum(a => a.Saldo);
            Data.VrijgesteldeReserves.KC02UitstootloosA = Data.VrijgesteldeReserves.KC02UitstootloosA.HasValue ? Data.VrijgesteldeReserves.KC02UitstootloosA.Value : 0;
            Data.VrijgesteldeReserves.KC02UitstootloosD = Data.VrijgesteldeReserves.KC02UitstootloosD.HasValue ? Data.VrijgesteldeReserves.KC02UitstootloosD.Value : 0;
            diff = Data.VrijgesteldeReserves.KC02UitstootloosD.Value - Data.VrijgesteldeReserves.KC02UitstootloosA.Value;
            Data.VrijgesteldeReserves.KC02UitstootloosB = diff > 0 ? diff : 0;
            Data.VrijgesteldeReserves.KC02UitstootloosC = diff < 0 ? Math.Abs(diff) : 0;


            mappingItemId = new Guid("B7DA956A-D70A-4606-A246-054F9689963D");
            Data.VrijgesteldeReserves.KFIVReconversieVennA = 0-accounts.Where(a => a.Mappings.Count(m => m.MappingItemId == mappingItemId) == 1).Sum(a => a.PreviousSaldo);
            Data.VrijgesteldeReserves.KFIVReconversieVennD = 0-accounts.Where(a => a.Mappings.Count(m => m.MappingItemId == mappingItemId) == 1).Sum(a => a.Saldo);
            Data.VrijgesteldeReserves.KFIVReconversieVennA = Data.VrijgesteldeReserves.KFIVReconversieVennA.HasValue ? Data.VrijgesteldeReserves.KFIVReconversieVennA.Value : 0;
            Data.VrijgesteldeReserves.KFIVReconversieVennD = Data.VrijgesteldeReserves.KFIVReconversieVennD.HasValue ? Data.VrijgesteldeReserves.KFIVReconversieVennD.Value : 0;
            diff = Data.VrijgesteldeReserves.KFIVReconversieVennD.Value - Data.VrijgesteldeReserves.KFIVReconversieVennA.Value;
            Data.VrijgesteldeReserves.KFIVReconversieVennB = diff > 0 ? diff : 0;
            Data.VrijgesteldeReserves.KFIVReconversieVennC = diff < 0 ? Math.Abs(diff) : 0;


            mappingItemId = new Guid("E8BE7D97-C1AD-4DCD-87D2-E4F08D14C6BA");
            Data.VrijgesteldeReserves.KGeorganisGemVervoerA = 0-accounts.Where(a => a.Mappings.Count(m => m.MappingItemId == mappingItemId) == 1).Sum(a => a.PreviousSaldo);
            Data.VrijgesteldeReserves.KGeorganisGemVervoerD = 0-accounts.Where(a => a.Mappings.Count(m => m.MappingItemId == mappingItemId) == 1).Sum(a => a.Saldo);
            Data.VrijgesteldeReserves.KGeorganisGemVervoerA = Data.VrijgesteldeReserves.KGeorganisGemVervoerA.HasValue ? Data.VrijgesteldeReserves.KGeorganisGemVervoerA.Value : 0;
            Data.VrijgesteldeReserves.KGeorganisGemVervoerD = Data.VrijgesteldeReserves.KGeorganisGemVervoerD.HasValue ? Data.VrijgesteldeReserves.KGeorganisGemVervoerD.Value : 0;
            diff = Data.VrijgesteldeReserves.KGeorganisGemVervoerD.Value - Data.VrijgesteldeReserves.KGeorganisGemVervoerA.Value;
            Data.VrijgesteldeReserves.KGeorganisGemVervoerB = diff > 0 ? diff : 0;
            Data.VrijgesteldeReserves.KGeorganisGemVervoerC = diff < 0 ? Math.Abs(diff) : 0;


            mappingItemId = new Guid("93DFCEB7-AB6B-40F2-85E1-F98CAED36F2E");
            Data.VrijgesteldeReserves.KInvesteringsres1982A = 0-accounts.Where(a => a.Mappings.Count(m => m.MappingItemId == mappingItemId) == 1).Sum(a => a.PreviousSaldo);
            Data.VrijgesteldeReserves.KInvesteringsres1982D = 0-accounts.Where(a => a.Mappings.Count(m => m.MappingItemId == mappingItemId) == 1).Sum(a => a.Saldo);
            Data.VrijgesteldeReserves.KInvesteringsres1982A = Data.VrijgesteldeReserves.KInvesteringsres1982A.HasValue ? Data.VrijgesteldeReserves.KInvesteringsres1982A.Value : 0;
            Data.VrijgesteldeReserves.KInvesteringsres1982D = Data.VrijgesteldeReserves.KInvesteringsres1982D.HasValue ? Data.VrijgesteldeReserves.KInvesteringsres1982D.Value : 0;
            diff = Data.VrijgesteldeReserves.KInvesteringsres1982D.Value - Data.VrijgesteldeReserves.KInvesteringsres1982A.Value;
            Data.VrijgesteldeReserves.KInvesteringsres1982B = diff > 0 ? diff : 0;
            Data.VrijgesteldeReserves.KInvesteringsres1982C = diff < 0 ? Math.Abs(diff) : 0;


            mappingItemId = new Guid("AFA2062F-D033-4223-B995-F8E97C089E3B");
            Data.VrijgesteldeReserves.KKapitaalSubsidiesA = 0-accounts.Where(a => a.Mappings.Count(m => m.MappingItemId == mappingItemId) == 1).Sum(a => a.PreviousSaldo);
            Data.VrijgesteldeReserves.KKapitaalSubsidiesD = 0-accounts.Where(a => a.Mappings.Count(m => m.MappingItemId == mappingItemId) == 1).Sum(a => a.Saldo);
            Data.VrijgesteldeReserves.KKapitaalSubsidiesA = Data.VrijgesteldeReserves.KKapitaalSubsidiesA.HasValue ? Data.VrijgesteldeReserves.KKapitaalSubsidiesA.Value : 0;
            Data.VrijgesteldeReserves.KKapitaalSubsidiesD = Data.VrijgesteldeReserves.KKapitaalSubsidiesD.HasValue ? Data.VrijgesteldeReserves.KKapitaalSubsidiesD.Value : 0;
            diff = Data.VrijgesteldeReserves.KKapitaalSubsidiesD.Value - Data.VrijgesteldeReserves.KKapitaalSubsidiesA.Value;
            Data.VrijgesteldeReserves.KKapitaalSubsidiesB = diff > 0 ? diff : 0;
            Data.VrijgesteldeReserves.KKapitaalSubsidiesC = diff < 0 ? Math.Abs(diff) : 0;


            mappingItemId = new Guid("7EB8B1A7-D395-4BB5-B426-103093DEC93D");
            Data.VrijgesteldeReserves.KNettoActiefA = 0-accounts.Where(a => a.Mappings.Count(m => m.MappingItemId == mappingItemId) == 1).Sum(a => a.PreviousSaldo);
            Data.VrijgesteldeReserves.KNettoActiefD = 0-accounts.Where(a => a.Mappings.Count(m => m.MappingItemId == mappingItemId) == 1).Sum(a => a.Saldo);
            Data.VrijgesteldeReserves.KNettoActiefA = Data.VrijgesteldeReserves.KNettoActiefA.HasValue ? Data.VrijgesteldeReserves.KNettoActiefA.Value : 0;
            Data.VrijgesteldeReserves.KNettoActiefD = Data.VrijgesteldeReserves.KNettoActiefD.HasValue ? Data.VrijgesteldeReserves.KNettoActiefD.Value : 0;
            diff = Data.VrijgesteldeReserves.KNettoActiefD.Value - Data.VrijgesteldeReserves.KNettoActiefA.Value;
            Data.VrijgesteldeReserves.KNettoActiefB = diff > 0 ? diff : 0;
            Data.VrijgesteldeReserves.KNettoActiefC = diff < 0 ? Math.Abs(diff) : 0;


            mappingItemId = new Guid("28CD6AEE-27E3-43C0-88B0-F440DB788001");
            Data.VrijgesteldeReserves.KProvisiesSocVoordA = 0-accounts.Where(a => a.Mappings.Count(m => m.MappingItemId == mappingItemId) == 1).Sum(a => a.PreviousSaldo);
            Data.VrijgesteldeReserves.KProvisiesSocVoordD = 0-accounts.Where(a => a.Mappings.Count(m => m.MappingItemId == mappingItemId) == 1).Sum(a => a.Saldo);
            Data.VrijgesteldeReserves.KProvisiesSocVoordA = Data.VrijgesteldeReserves.KProvisiesSocVoordA.HasValue ? Data.VrijgesteldeReserves.KProvisiesSocVoordA.Value : 0;
            Data.VrijgesteldeReserves.KProvisiesSocVoordD = Data.VrijgesteldeReserves.KProvisiesSocVoordD.HasValue ? Data.VrijgesteldeReserves.KProvisiesSocVoordD.Value : 0;
            diff = Data.VrijgesteldeReserves.KProvisiesSocVoordD.Value - Data.VrijgesteldeReserves.KProvisiesSocVoordA.Value;
            Data.VrijgesteldeReserves.KProvisiesSocVoordB = diff > 0 ? diff : 0;
            Data.VrijgesteldeReserves.KProvisiesSocVoordC = diff < 0 ? Math.Abs(diff) : 0;


            mappingItemId = new Guid("4FC1F563-8637-41D1-A63B-913A03E44BEC");
            Data.VrijgesteldeReserves.KRSZInschakelingA = 0-accounts.Where(a => a.Mappings.Count(m => m.MappingItemId == mappingItemId) == 1).Sum(a => a.PreviousSaldo);
            Data.VrijgesteldeReserves.KRSZInschakelingD = 0-accounts.Where(a => a.Mappings.Count(m => m.MappingItemId == mappingItemId) == 1).Sum(a => a.Saldo);
            Data.VrijgesteldeReserves.KRSZInschakelingA = Data.VrijgesteldeReserves.KRSZInschakelingA.HasValue ? Data.VrijgesteldeReserves.KRSZInschakelingA.Value : 0;
            Data.VrijgesteldeReserves.KRSZInschakelingD = Data.VrijgesteldeReserves.KRSZInschakelingD.HasValue ? Data.VrijgesteldeReserves.KRSZInschakelingD.Value : 0;
            diff = Data.VrijgesteldeReserves.KRSZInschakelingD.Value - Data.VrijgesteldeReserves.KRSZInschakelingA.Value;
            Data.VrijgesteldeReserves.KRSZInschakelingB = diff > 0 ? diff : 0;
            Data.VrijgesteldeReserves.KRSZInschakelingC = diff < 0 ? Math.Abs(diff) : 0;


            mappingItemId = new Guid("BE71FF83-148A-4728-94E5-56D6E0962777");
            Data.VrijgesteldeReserves.KSociaalPassiefA = 0-accounts.Where(a => a.Mappings.Count(m => m.MappingItemId == mappingItemId) == 1).Sum(a => a.PreviousSaldo);
            Data.VrijgesteldeReserves.KSociaalPassiefD = 0-accounts.Where(a => a.Mappings.Count(m => m.MappingItemId == mappingItemId) == 1).Sum(a => a.Saldo);
            Data.VrijgesteldeReserves.KSociaalPassiefA = Data.VrijgesteldeReserves.KSociaalPassiefA.HasValue ? Data.VrijgesteldeReserves.KSociaalPassiefA.Value : 0;
            Data.VrijgesteldeReserves.KSociaalPassiefD = Data.VrijgesteldeReserves.KSociaalPassiefD.HasValue ? Data.VrijgesteldeReserves.KSociaalPassiefD.Value : 0;
            diff = Data.VrijgesteldeReserves.KSociaalPassiefD.Value - Data.VrijgesteldeReserves.KSociaalPassiefA.Value;
            Data.VrijgesteldeReserves.KSociaalPassiefB = diff > 0 ? diff : 0;
            Data.VrijgesteldeReserves.KSociaalPassiefC = diff < 0 ? Math.Abs(diff) : 0;


            mappingItemId = new Guid("6CE2122C-0A05-4E1B-AC11-AA2B1C44C8BA");
            Data.VrijgesteldeReserves.KVoorraadDiamantA = 0-accounts.Where(a => a.Mappings.Count(m => m.MappingItemId == mappingItemId) == 1).Sum(a => a.PreviousSaldo);
            Data.VrijgesteldeReserves.KVoorraadDiamantD = 0-accounts.Where(a => a.Mappings.Count(m => m.MappingItemId == mappingItemId) == 1).Sum(a => a.Saldo);
            Data.VrijgesteldeReserves.KVoorraadDiamantA = Data.VrijgesteldeReserves.KVoorraadDiamantA.HasValue ? Data.VrijgesteldeReserves.KVoorraadDiamantA.Value : 0;
            Data.VrijgesteldeReserves.KVoorraadDiamantD = Data.VrijgesteldeReserves.KVoorraadDiamantD.HasValue ? Data.VrijgesteldeReserves.KVoorraadDiamantD.Value : 0;
            diff = Data.VrijgesteldeReserves.KVoorraadDiamantD.Value - Data.VrijgesteldeReserves.KVoorraadDiamantA.Value;
            Data.VrijgesteldeReserves.KVoorraadDiamantB = diff > 0 ? diff : 0;
            Data.VrijgesteldeReserves.KVoorraadDiamantC = diff < 0 ? Math.Abs(diff) : 0;


            mappingItemId = new Guid("446F5EA4-9765-496B-9864-C24B5364E8DE");
            Data.VrijgesteldeReserves.KVrijWinstInnovatieVennA = 0-accounts.Where(a => a.Mappings.Count(m => m.MappingItemId == mappingItemId) == 1).Sum(a => a.PreviousSaldo);
            Data.VrijgesteldeReserves.KVrijWinstInnovatieVennD = 0-accounts.Where(a => a.Mappings.Count(m => m.MappingItemId == mappingItemId) == 1).Sum(a => a.Saldo);
            Data.VrijgesteldeReserves.KVrijWinstInnovatieVennA = Data.VrijgesteldeReserves.KVrijWinstInnovatieVennA.HasValue ? Data.VrijgesteldeReserves.KVrijWinstInnovatieVennA.Value : 0;
            Data.VrijgesteldeReserves.KVrijWinstInnovatieVennD = Data.VrijgesteldeReserves.KVrijWinstInnovatieVennD.HasValue ? Data.VrijgesteldeReserves.KVrijWinstInnovatieVennD.Value : 0;
            diff = Data.VrijgesteldeReserves.KVrijWinstInnovatieVennD.Value - Data.VrijgesteldeReserves.KVrijWinstInnovatieVennA.Value;
            Data.VrijgesteldeReserves.KVrijWinstInnovatieVennB = diff > 0 ? diff : 0;
            Data.VrijgesteldeReserves.KVrijWinstInnovatieVennC = diff < 0 ? Math.Abs(diff) : 0;


            Data.VrijgesteldeReserves.KTotaalA = Data.VrijgesteldeReserves.KAfschrAanschafBelegWaardeA
                                                 + Data.VrijgesteldeReserves.KAfschrAanschBelegArt2A
                                                 + Data.VrijgesteldeReserves.KAndereA
                                                 + Data.VrijgesteldeReserves.KBedrijfsfietsenA
                                                 + Data.VrijgesteldeReserves.KBeveiligingA
                                                 + Data.VrijgesteldeReserves.KC02UitstootloosA
                                                 + Data.VrijgesteldeReserves.KFIVReconversieVennA
                                                 + Data.VrijgesteldeReserves.KGeorganisGemVervoerA
                                                 + Data.VrijgesteldeReserves.KInvesteringsres1982A
                                                 + Data.VrijgesteldeReserves.KKapitaalSubsidiesA
                                                 + Data.VrijgesteldeReserves.KNettoActiefA
                                                 + Data.VrijgesteldeReserves.KProvisiesSocVoordA
                                                 + Data.VrijgesteldeReserves.KRSZInschakelingA
                                                 + Data.VrijgesteldeReserves.KSociaalPassiefA
                                                 + Data.VrijgesteldeReserves.KVoorraadDiamantA
                                                 + Data.VrijgesteldeReserves.KVrijWinstInnovatieVennA;

            Data.VrijgesteldeReserves.KTotaalB = Data.VrijgesteldeReserves.KAfschrAanschafBelegWaardeB
                                                 + Data.VrijgesteldeReserves.KAfschrAanschBelegArt2B
                                                 + Data.VrijgesteldeReserves.KAndereB
                                                 + Data.VrijgesteldeReserves.KBedrijfsfietsenB
                                                 + Data.VrijgesteldeReserves.KBeveiligingB
                                                 + Data.VrijgesteldeReserves.KC02UitstootloosB
                                                 + Data.VrijgesteldeReserves.KFIVReconversieVennB
                                                 + Data.VrijgesteldeReserves.KGeorganisGemVervoerB
                                                 + Data.VrijgesteldeReserves.KInvesteringsres1982B
                                                 + Data.VrijgesteldeReserves.KKapitaalSubsidiesB
                                                 + Data.VrijgesteldeReserves.KNettoActiefB
                                                 + Data.VrijgesteldeReserves.KProvisiesSocVoordB
                                                 + Data.VrijgesteldeReserves.KRSZInschakelingB
                                                 + Data.VrijgesteldeReserves.KSociaalPassiefB
                                                 + Data.VrijgesteldeReserves.KVoorraadDiamantB
                                                 + Data.VrijgesteldeReserves.KVrijWinstInnovatieVennB;

            Data.VrijgesteldeReserves.KTotaalC = Data.VrijgesteldeReserves.KAfschrAanschafBelegWaardeC
                                                 + Data.VrijgesteldeReserves.KAfschrAanschBelegArt2C
                                                 + Data.VrijgesteldeReserves.KAndereC
                                                 + Data.VrijgesteldeReserves.KBedrijfsfietsenC
                                                 + Data.VrijgesteldeReserves.KBeveiligingC
                                                 + Data.VrijgesteldeReserves.KC02UitstootloosC
                                                 + Data.VrijgesteldeReserves.KFIVReconversieVennC
                                                 + Data.VrijgesteldeReserves.KGeorganisGemVervoerC
                                                 + Data.VrijgesteldeReserves.KInvesteringsres1982C
                                                 + Data.VrijgesteldeReserves.KKapitaalSubsidiesC
                                                 + Data.VrijgesteldeReserves.KNettoActiefC
                                                 + Data.VrijgesteldeReserves.KProvisiesSocVoordC
                                                 + Data.VrijgesteldeReserves.KRSZInschakelingC
                                                 + Data.VrijgesteldeReserves.KSociaalPassiefC
                                                 + Data.VrijgesteldeReserves.KVoorraadDiamantC
                                                 + Data.VrijgesteldeReserves.KVrijWinstInnovatieVennC;

            Data.VrijgesteldeReserves.KTotaalD = Data.VrijgesteldeReserves.KAfschrAanschafBelegWaardeD
                                                 + Data.VrijgesteldeReserves.KAfschrAanschBelegArt2D
                                                 + Data.VrijgesteldeReserves.KAndereD
                                                 + Data.VrijgesteldeReserves.KBedrijfsfietsenD
                                                 + Data.VrijgesteldeReserves.KBeveiligingD
                                                 + Data.VrijgesteldeReserves.KC02UitstootloosD
                                                 + Data.VrijgesteldeReserves.KFIVReconversieVennD
                                                 + Data.VrijgesteldeReserves.KGeorganisGemVervoerD
                                                 + Data.VrijgesteldeReserves.KInvesteringsres1982D
                                                 + Data.VrijgesteldeReserves.KKapitaalSubsidiesD
                                                 + Data.VrijgesteldeReserves.KNettoActiefD
                                                 + Data.VrijgesteldeReserves.KProvisiesSocVoordD
                                                 + Data.VrijgesteldeReserves.KRSZInschakelingD
                                                 + Data.VrijgesteldeReserves.KSociaalPassiefD
                                                 + Data.VrijgesteldeReserves.KVoorraadDiamantD
                                                 + Data.VrijgesteldeReserves.KVrijWinstInnovatieVennD;




            Data.VrijgesteldeReserves.TotaalVrijgesteldA = Data.VrijgesteldeReserves.AWaardeverminderingA
                                                           + Data.VrijgesteldeReserves.BVoorzieningA
                                                           + Data.VrijgesteldeReserves.CTotaalA
                                                           + Data.VrijgesteldeReserves.DTotaalA
                                                           + Data.VrijgesteldeReserves.EGespreidVerwezenA
                                                           + Data.VrijgesteldeReserves.EMatImmatVasteActivaA
                                                           + Data.VrijgesteldeReserves.FBedrijfsvoertuigenA
                                                           + Data.VrijgesteldeReserves.GBinnenschepenA
                                                           + Data.VrijgesteldeReserves.HZeeschepenA
                                                           + Data.VrijgesteldeReserves.IInvesteringsreserveA
                                                           + Data.VrijgesteldeReserves.JTaxShelterAudioVisueelA
                                                           + Data.VrijgesteldeReserves.JWinstHomologatieA
                                                           + Data.VrijgesteldeReserves.KTotaalA;

            Data.VrijgesteldeReserves.TotaalVrijgesteldB = Data.VrijgesteldeReserves.AWaardeverminderingB 
                                                           + Data.VrijgesteldeReserves.BVoorzieningB 
                                                           + Data.VrijgesteldeReserves.CTotaalB 
                                                           + Data.VrijgesteldeReserves.DTotaalB 
                                                           + Data.VrijgesteldeReserves.EGespreidVerwezenB 
                                                           + Data.VrijgesteldeReserves.EMatImmatVasteActivaB
                                                           + Data.VrijgesteldeReserves.FBedrijfsvoertuigenB 
                                                           + Data.VrijgesteldeReserves.GBinnenschepenB 
                                                           + Data.VrijgesteldeReserves.HZeeschepenB 
                                                           + Data.VrijgesteldeReserves.IInvesteringsreserveB 
                                                           + Data.VrijgesteldeReserves.JTaxShelterAudioVisueelB 
                                                           + Data.VrijgesteldeReserves.JWinstHomologatieB 
                                                           + Data.VrijgesteldeReserves.KTotaalB ;

            Data.VrijgesteldeReserves.TotaalVrijgesteldC = Data.VrijgesteldeReserves.AWaardeverminderingC
                                                           + Data.VrijgesteldeReserves.BVoorzieningC
                                                           + Data.VrijgesteldeReserves.CTotaalC
                                                           + Data.VrijgesteldeReserves.DTotaalC
                                                           + Data.VrijgesteldeReserves.EGespreidVerwezenC
                                                           + Data.VrijgesteldeReserves.EMatImmatVasteActivaC
                                                           + Data.VrijgesteldeReserves.FBedrijfsvoertuigenC
                                                           + Data.VrijgesteldeReserves.GBinnenschepenC
                                                           + Data.VrijgesteldeReserves.HZeeschepenC
                                                           + Data.VrijgesteldeReserves.IInvesteringsreserveC
                                                           + Data.VrijgesteldeReserves.JTaxShelterAudioVisueelC
                                                           + Data.VrijgesteldeReserves.JWinstHomologatieC
                                                           + Data.VrijgesteldeReserves.KTotaalC;

            Data.VrijgesteldeReserves.TotaalVrijgesteldD = Data.VrijgesteldeReserves.AWaardeverminderingD
                                                           + Data.VrijgesteldeReserves.BVoorzieningD
                                                           + Data.VrijgesteldeReserves.CTotaalD
                                                           + Data.VrijgesteldeReserves.DTotaalD
                                                           + Data.VrijgesteldeReserves.EGespreidVerwezenD
                                                           + Data.VrijgesteldeReserves.EMatImmatVasteActivaD
                                                           + Data.VrijgesteldeReserves.FBedrijfsvoertuigenD
                                                           + Data.VrijgesteldeReserves.GBinnenschepenD
                                                           + Data.VrijgesteldeReserves.HZeeschepenD
                                                           + Data.VrijgesteldeReserves.IInvesteringsreserveD
                                                           + Data.VrijgesteldeReserves.JTaxShelterAudioVisueelD
                                                           + Data.VrijgesteldeReserves.JWinstHomologatieD
                                                           + Data.VrijgesteldeReserves.KTotaalD;
            
            // Data.VrijgesteldeReserves.CNietVerwezMWA = accounts.Where(a=>a.Mappings.Count(m=>m.MappingMeta==""

        }
        
        // VALIDATION
        
        // CALCULATION
        
        // BOOKINGS
        
        // IMPORT PREVIOUS


        #region AddUpdateDelete

        public void UpdateVrijgesteldeReserves(EY.com.eBook.RuleEngine.AY2014.BelastingVrijeReservesAppTypes.Detail entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2014.BelastingVrijeReservesAppTypes.Detail original = Data.VrijgesteldeReserves;

            entity = Validate(entity, "VrijgesteldeReserves", ref worksheetset);
            if (entity.IsValid)
            {

                Data.VrijgesteldeReserves = entity;
                Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
            }
        }

        #endregion

        #region VALIDATION

        public EY.com.eBook.RuleEngine.AY2014.BelastingVrijeReservesAppTypes.Detail Validate(EY.com.eBook.RuleEngine.AY2014.BelastingVrijeReservesAppTypes.Detail entity, string location, ref WorksheetSetData worksheetset)
        {
            entity.IsValid = true;
            worksheetset.ClearMessages(TypeId, entity.Id);
            /*
            if (!entity.Bedrag.HasValue || entity.Bedrag.Value==0)
            {
            worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Grootboek");
            entity.IsValid = false;
            }
            */

            return entity;
        }

        #endregion
    }
}
  