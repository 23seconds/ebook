
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.RuleEngine.Core;
using EY.com.eBook.RuleEngine.Core.Interfaces;
using EY.com.eBook.RuleEngine.Data;
using EY.com.eBook.Core;

namespace EY.com.eBook.RuleEngine.AY2014
{
    
    public class VerliesHandelsdebiteurenApp
    {
        public static WorksheetTypeSettings Settings = new WorksheetTypeSettings
                                                      {
                                                        Clients = true
                                                        ,
                                                        Suppliers = true
                                                        ,
                                                        AllAccounts = true
                                                        , 
                                                        PerformsBookings=true
                                                      };
                                                      
        public static Guid TypeId = new Guid("41def4da-0f30-4d13-ae80-124f4c51a966");

        public List<string> ImportablePreviousTypes  = new List<string>() {  "EY.com.eBook.RuleEngine.AY2013.FacturatieApp"  };

        public Guid GetMyType() { return TypeId; }

        public void SetMinimal()
        {
            if (Data == null)
            {
                Data = new EY.com.eBook.RuleEngine.AY2014.VerliesHandelsdebiteurenAppTypes.VerliesHandelsDebiteuren();
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.VerliesHD == null)
            {
                Data.VerliesHD = new List<EY.com.eBook.RuleEngine.AY2014.VerliesHandelsdebiteurenAppTypes.VerliesHandelsDebiteur>(); ;
                Data.LastUpdated = DateTime.Now;
            }

        }
        
        public string ToXml() 
        {
            return CoreHelper.SerializeToString(Data);
        }

        public void LoadXml(string xml)
        {
            if (!string.IsNullOrEmpty(xml)) {
                Data = CoreHelper.DeserializeFromString<EY.com.eBook.RuleEngine.AY2014.VerliesHandelsdebiteurenAppTypes.VerliesHandelsDebiteuren>(xml);
            }
            SetMinimal();
            
        }
        
        public EY.com.eBook.RuleEngine.AY2014.VerliesHandelsdebiteurenAppTypes.VerliesHandelsDebiteuren Data { get; set; }


        public void ForceRecalculation(ref WorksheetSetData worksheetset)
        {
            Data.LastUpdated = new DateTime(1980, 1, 1);
            Calculate(ref worksheetset);
        }

        public void Calculate(ref WorksheetSetData worksheetset)
        {

            List<Guid> importedItems = Data.VerliesHD.Where(e => !string.IsNullOrEmpty(e.ImportedID)).Select(e => e.Id).ToList();

            List<EY.com.eBook.RuleEngine.AY2014.DubieuzeDebiteurenAppTypes.DubieuzeDebiteurW> dubdebs =worksheetset.DubieuzeDebiteurenApp.Data.WijzigingBestaandeTijdensBoekjaar.Where(e => e.DefinitiefVerlies == true).ToList();
            List<Guid> dubdebids = dubdebs.Select(e => e.Id).ToList();

            List<Guid> deletes = importedItems.Where(i=>!dubdebids.Contains(i)).ToList();
            Data.VerliesHD.RemoveAll(e => deletes.Contains(e.Id));


            // IMPORT DUBIEUZE DEBITEUREN BOEK DEFINITIEF VERLIES
            foreach (EY.com.eBook.RuleEngine.AY2014.DubieuzeDebiteurenAppTypes.DubieuzeDebiteurW dubdeb in dubdebs )
            {

                EY.com.eBook.RuleEngine.AY2014.VerliesHandelsdebiteurenAppTypes.VerliesHandelsDebiteur entity = Data.VerliesHD.FirstOrDefault(e => e.Id == dubdeb.Id);
                if (entity == null)
                {
                    entity = new EY.com.eBook.RuleEngine.AY2014.VerliesHandelsdebiteurenAppTypes.VerliesHandelsDebiteur
                    {
                        Id = Guid.NewGuid()
                        ,
                        BTW = 0
                        ,
                        todelete = false
                        , Boeken=false
                    };
                    Data.VerliesHD.Add(entity);
                }
                entity.AndereReden = null;
                entity.FactuurNr = dubdeb.FactuurNummer;
                entity.Faillissement = dubdeb.VerminderingWVVerlies.Value;
                entity.BTW = dubdeb.OrigineleFactuurWaardeBTW;
                entity.GrootBoek = worksheetset.GetListAccount("642000000000");
                entity.ImportedID = dubdeb.Id.ToString();
                entity.Jaar = new StringListItemDataContract { Id= dubdeb.FactuurJaar, NL=dubdeb.FactuurJaar, FR=dubdeb.FactuurJaar, EN=dubdeb.FactuurJaar};
                entity.Klant = dubdeb.Schuldenaar;
                entity.LastUpdated = DateTime.Now;
                entity.PreviousImported = false;
                
            }

            Data.LastUpdated = DateTime.Now;
        }

        #region AddUpdateDelete

        public void AddToVerliesHD(EY.com.eBook.RuleEngine.AY2014.VerliesHandelsdebiteurenAppTypes.VerliesHandelsDebiteur entity, ref WorksheetSetData worksheetset)
        {
            entity = Validate(entity, "VerliesHD", ref worksheetset);
            if (entity.IsValid)
            {
                if (entity.Boeken.HasValue && entity.Boeken.Value)
                {
                    decimal bkValue = entity.Faillissement.GetValueOrDefault() + entity.AndereReden.GetValueOrDefault();
                    decimal btw = entity.BTW.GetValueOrDefault();
                    List<BookingLineDataContract> lines = new List<BookingLineDataContract>() {
                        new BookingLineDataContract { AccountNr = entity.GrootBoek, Amount=bkValue }
                        , new BookingLineDataContract { AccountNr = worksheetset.GetListAccount("400"), Amount=0-(bkValue+btw) }
                    };

                    if (entity.BTW.GetValueOrDefault() != 0)
                    {
                        if (entity.Faillissement.GetValueOrDefault() != 0)
                        {
                            lines.Add(new BookingLineDataContract { AccountNr = worksheetset.GetListAccount("411"), Amount = btw });
                        }
                        else
                        {
                            lines.Add(new BookingLineDataContract { AccountNr = worksheetset.GetListAccount("64012"), Amount = btw });
                        }
                    }
                    
                    worksheetset.AddBooking(TypeId, "VerliesHD", entity.Id, null, lines);
                }
                Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
                Data.VerliesHD.Add(entity);
            }
        }

        public void DeleteVerliesHD(Guid rowId, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2014.VerliesHandelsdebiteurenAppTypes.VerliesHandelsDebiteur original = Data.VerliesHD.FirstOrDefault(e => e.Id == rowId);
            if (original != null)
            {
                
                if (original.Boeken.HasValue && original.Boeken.Value)
                {
                    worksheetset.DeleteBooking(TypeId, "VerliesHD", original.Id, null);
                    // DELETE BOOKING
                }
                Data.VerliesHD.Remove(original);
                Data.LastUpdated = DateTime.Now;
            }
        }

        public void UpdateVerliesHD(EY.com.eBook.RuleEngine.AY2014.VerliesHandelsdebiteurenAppTypes.VerliesHandelsDebiteur entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2014.VerliesHandelsdebiteurenAppTypes.VerliesHandelsDebiteur original = Data.VerliesHD.FirstOrDefault(e => e.Id == entity.Id);
            if (original == null)
            {
                AddToVerliesHD(entity, ref worksheetset);
            }
            else
            {
                entity = Validate(entity, "VerliesHD", ref worksheetset);
                if (entity.IsValid)
                {
                    if (original.Boeken.HasValue && original.Boeken.Value && (!entity.Boeken.HasValue || !entity.Boeken.Value))
                    {
                        worksheetset.DeleteBooking(TypeId, "VerliesHD", original.Id, null);
                        // DELETE BOOKING
                    }
                    
                    if (entity.Boeken.HasValue && entity.Boeken.Value)
                    {
                        decimal bkValue = entity.Faillissement.GetValueOrDefault() + entity.AndereReden.GetValueOrDefault();
                        decimal btw = entity.BTW.GetValueOrDefault();
                        List<BookingLineDataContract> lines = new List<BookingLineDataContract>() {
                            new BookingLineDataContract { AccountNr = entity.GrootBoek, Amount=bkValue }
                            , new BookingLineDataContract { AccountNr = worksheetset.GetListAccount("400"), Amount=0-(bkValue+btw) }
                        };

                        if (entity.BTW.GetValueOrDefault() != 0)
                        {
                            if (entity.Faillissement.GetValueOrDefault() != 0)
                            {
                                lines.Add(new BookingLineDataContract { AccountNr = worksheetset.GetListAccount("411"), Amount = btw });
                            }
                            else
                            {
                                lines.Add(new BookingLineDataContract { AccountNr = worksheetset.GetListAccount("64012"), Amount = btw });
                            }
                        }
                        if (original.Boeken.HasValue && original.Boeken.Value)
                        {
                            worksheetset.UpdateBooking(TypeId, "VerliesHD", entity.Id, null, lines);
                        }
                        else
                        {
                            worksheetset.AddBooking(TypeId, "VerliesHD", entity.Id, null, lines);
                        }
                    }
                    int idx = Data.VerliesHD.IndexOf(original);
                    Data.VerliesHD.RemoveAt(idx);
                    Data.VerliesHD.Insert(idx, entity);
                    Data.LastUpdated = DateTime.Now;
                    entity.LastUpdated = DateTime.Now;
                }
            }
        }

        #endregion

        #region VALIDATION

        public EY.com.eBook.RuleEngine.AY2014.VerliesHandelsdebiteurenAppTypes.VerliesHandelsDebiteur Validate(EY.com.eBook.RuleEngine.AY2014.VerliesHandelsdebiteurenAppTypes.VerliesHandelsDebiteur entity, string location, ref WorksheetSetData worksheetset)
        {
            entity.IsValid = true;
            worksheetset.ClearMessages(TypeId, entity.Id);
            /*
            if (!entity.Bedrag.HasValue || entity.Bedrag.Value==0)
            {
            worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Grootboek");
            entity.IsValid = false;
            }
            */

            return entity;
        }

        #endregion
        
        // CALCULATION
        
        // BOOKINGS
        
        // IMPORT PREVIOUS


        
    }
}
  