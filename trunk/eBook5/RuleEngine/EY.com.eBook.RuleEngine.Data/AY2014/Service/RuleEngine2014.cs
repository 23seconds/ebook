using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Activation;
using EY.com.eBook.Core.EF;
using System.ServiceModel.Web;
using EY.com.eBook.RuleEngine.Core;
using EY.com.eBook.RuleEngine.Data;
using EY.com.eBook.Core.Data;
using EY.com.eBook.Core;
using EY.com.eBook.Core.EF.Write;
using EY.com.eBook.API.Contracts.Proxies;
using EY.com.eBook.Caching;
using EY.com.eBook.RuleEngine.AY2014;
using EY.com.eBook.API.Contracts.Data;

namespace EY.com.eBook.RuleEngine
{
    [EntityFrameworkServiceBehavior]
    [ServiceBehavior(IncludeExceptionDetailInFaults = true, ConcurrencyMode = ConcurrencyMode.Multiple, InstanceContextMode = InstanceContextMode.PerCall)]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class RuleEngine2014 : IRuleEngine2014
    {

        #region IRuleEngine2014 Members

        public void UpdateBizTax(CriteriaFileTypeDataContract cfdc)
        {
            Guid sId = new Guid("bd7c2eae-8500-4103-8984-0131e73d07fa");
            WorksheetSet set = GetSet(cfdc.FileId);
           
            
            if (cfdc.Type == "nrcorp")
            {

                EY.com.eBook.RuleEngine.AY2014.BNIBizTaxRenderer btr = new EY.com.eBook.RuleEngine.AY2014.BNIBizTaxRenderer();

                FileXbrl fx = EY.com.eBook.Core.EF.eBookWriteManager.Context.FileXbrlSet.FirstOrDefault(x => x.FileId == cfdc.FileId);

                if (fx != null && fx.StatusId < 1)
                {
                    EY.com.eBook.BizTax.Contracts.BizTaxDataContract btdc = CoreHelper.DeserializeFromString<EY.com.eBook.BizTax.Contracts.BizTaxDataContract>(fx.XbrlData);
                    btr.Xbrl = btdc;
                    btdc = btr.UpdateFromWorksheets(btdc, set);
                    fx.XbrlData = CoreHelper.SerializeToString(btdc);
                    EY.com.eBook.Core.EF.eBookWriteManager.Context.SaveChanges();
                }
                
            }
            else
            {
                
                EY.com.eBook.RuleEngine.AY2014.BizTaxRenderer btr = new EY.com.eBook.RuleEngine.AY2014.BizTaxRenderer();

                FileXbrl fx = EY.com.eBook.Core.EF.eBookWriteManager.Context.FileXbrlSet.FirstOrDefault(x => x.FileId == cfdc.FileId);

                if (fx!=null && fx.StatusId < 1)
                {
                    EY.com.eBook.BizTax.Contracts.BizTaxDataContract btdc = CoreHelper.DeserializeFromString<EY.com.eBook.BizTax.Contracts.BizTaxDataContract>(fx.XbrlData);
                    btr.Xbrl = btdc;
                    btdc = btr.UpdateFromWorksheets(btdc, set);
                    fx.XbrlData = CoreHelper.SerializeToString(btdc);
                    EY.com.eBook.Core.EF.eBookWriteManager.Context.SaveChanges();
                }


               
                
               
            }
           
        }

        

        public List<AccountHistoryDataContract> GetAccountsHistory(ClientBaseDataContract cdc, List<string> accountNrs, DateTime start, DateTime end)
        {
            List<AccountHistoryDataContract> hist = new List<AccountHistoryDataContract>();
            ProAccServiceProxy psp = new ProAccServiceProxy();
            try
            {
                hist = psp.ProAccGetAccountsHistory(new ProAcc.API.Contracts.Data.CriteriaProAccAccountsDataContract
                {
                    AccountNrs = accountNrs
                    ,
                    Start = start
                    ,
                    End = end
                    ,
                    Database = cdc.ProAccDatabase
                    ,
                    Server = cdc.ProAccServer
                });
            }
            catch
            {
                //
            }
            finally
            {
                try
                {
                    psp.Close();
                }
                catch
                {
                    psp.Abort();
                }
                
            }
            return hist;
        }

        public List<AccountHistoryLineDataContract> GetAccountHistory(ClientDataContract cdc, string account, DateTime start, DateTime end)
        {
            List<AccountHistoryLineDataContract> hist = new List<AccountHistoryLineDataContract>();
            ProAccServiceProxy psp = new ProAccServiceProxy();
            try
            {
                hist = psp.ProAccGetAccountHistory(new ProAcc.API.Contracts.Data.CriteriaProAccAccountDataContract
                {
                    AccountNr = account
                    ,
                    Start = start
                    ,
                    End = end
                    ,
                    Database = cdc.ProAccDatabase
                    ,
                    Server = cdc.ProAccServer
                });
            }
            catch
            {
                //
            }
            finally
            {
                try
                {
                    psp.Close();
                }
                catch
                {
                    psp.Abort();
                }

            }
            return hist;
        }

        public EY.com.eBook.RuleEngine.AY2014.WorksheetSet GetSet(Guid fileId)
        {
           // CacheProxy cp = new CacheProxy();
           // cp.Open();
            CacheRepository cp = new CacheRepository();
            if (!cp.FileInCache(fileId))
            {
                throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { ErrorCode=-99, Message = "CACHE-MISS" , FileId=fileId });
            }
            WorksheetWorkSetDataContract wwsdc = cp.GetWorksheetSet(fileId);

            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData sh = new EY.com.eBook.RuleEngine.AY2014.WorksheetSetData { 
               Coeffs = wwsdc.Coeffs
               , DataContract = wwsdc.DataContract
               , Client = wwsdc.Client
               , Settings = wwsdc.Settings
               , AutoBookings = wwsdc.AutoBookings== null ? new List<BookingDataContract>() : wwsdc.AutoBookings
               , Schema = wwsdc.Schema
               , Messages = wwsdc.Messages== null ? new List<FileMessageDataContract>() : wwsdc.Messages
               , IsEmpty = wwsdc.Worksheets == null || wwsdc.Worksheets.Count==0
               , LastSave = wwsdc.Worksheets==null || wwsdc.Worksheets.Count==0 ? new DateTime(1980,1,1) : wwsdc.Worksheets.LastSave
            };
            if (wwsdc.Worksheets != null && wwsdc.Worksheets.Count == 0) wwsdc.Worksheets = null;
            sh.Initialize(wwsdc.Worksheets);
            
            
           // set.DataContainer = sh;
            return new EY.com.eBook.RuleEngine.AY2014.WorksheetSet { DataContainer = sh };
            
        }

        public EY.com.eBook.RuleEngine.IWorksheetSet GetPreviousSet(Guid fileId)
        {
            // CacheProxy cp = new CacheProxy();
            // cp.Open();
            CacheRepository cp = new CacheRepository();
            if (!cp.FileInCache(fileId))
            {
                cp.LoadFileInCache(fileId);
            }

            WorksheetWorkSetDataContract wwsdc = cp.GetWorksheetSet(fileId);

            if (wwsdc.DataContract.AssessmentYear == 2013)
            {
                EY.com.eBook.RuleEngine.AY2013.WorksheetSetData sh = new EY.com.eBook.RuleEngine.AY2013.WorksheetSetData
                {
                    Coeffs = wwsdc.Coeffs
                    ,
                    DataContract = wwsdc.DataContract
                    ,
                    AutoBookings = wwsdc.AutoBookings == null ? new List<BookingDataContract>() : wwsdc.AutoBookings
                    ,
                    Schema = wwsdc.Schema
                    ,
                    Messages = wwsdc.Messages == null ? new List<FileMessageDataContract>() : wwsdc.Messages
                    ,
                    IsEmpty = wwsdc.Worksheets == null || wwsdc.Worksheets.Count == 0
                    ,
                    LastSave = wwsdc.Worksheets == null || wwsdc.Worksheets.Count == 0 ? new DateTime(1980, 1, 1) : wwsdc.Worksheets.LastSave
                };
                if (wwsdc.Worksheets != null && wwsdc.Worksheets.Count == 0) wwsdc.Worksheets = null;
                sh.Initialize(wwsdc.Worksheets);


                // set.DataContainer = sh;
                return new EY.com.eBook.RuleEngine.AY2013.WorksheetSet { DataContainer = sh };
            }
            else if (wwsdc.DataContract.AssessmentYear == 2014)
            {
                EY.com.eBook.RuleEngine.AY2014.WorksheetSetData sh = new EY.com.eBook.RuleEngine.AY2014.WorksheetSetData
                {
                    Coeffs = wwsdc.Coeffs
                    ,
                    DataContract = wwsdc.DataContract
                    ,
                    AutoBookings = wwsdc.AutoBookings == null ? new List<BookingDataContract>() : wwsdc.AutoBookings
                    ,
                    Schema = wwsdc.Schema
                    ,
                    Messages = wwsdc.Messages == null ? new List<FileMessageDataContract>() : wwsdc.Messages
                    ,
                    IsEmpty = wwsdc.Worksheets == null || wwsdc.Worksheets.Count == 0
                    ,
                    LastSave = wwsdc.Worksheets == null || wwsdc.Worksheets.Count == 0 ? new DateTime(1980, 1, 1) : wwsdc.Worksheets.LastSave
                };
                if (wwsdc.Worksheets != null && wwsdc.Worksheets.Count == 0) wwsdc.Worksheets = null;
                sh.Initialize(wwsdc.Worksheets);


                // set.DataContainer = sh;
                return new EY.com.eBook.RuleEngine.AY2014.WorksheetSet { DataContainer = sh };
            }
            return null;

        }

        public void CalculateAll(EY.com.eBook.RuleEngine.AY2014.WorksheetSet ws)
        {
            ws.Calculate(null);
        }

        public void Calculate(EY.com.eBook.RuleEngine.AY2014.WorksheetSet ws, string col)
        {
            ws.Calculate(col);
        }

        public void ReCalculate(CriteriaFileDataContract cfdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfdc.FileId);
            set.Calculate(null);
        }

        public void ForceReCalculate(CriteriaFileDataContract cfdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfdc.FileId);
            set.ForceRecalculation(null);
        }

        public void ImportPrevious(CriteriaFileDataContract cfdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfdc.FileId);
            if (!set._datacontainer.DataContract.PreviousFileId.HasValue) return;

            EY.com.eBook.RuleEngine.IWorksheetSet previousSet = GetPreviousSet(set._datacontainer.DataContract.PreviousFileId.Value);
            set._datacontainer.PreviousSet = previousSet.DataContainer;
            set.ImportPrevious();
        }
        
        public decimal? GetTaxCalculation(CriteriaFileDataContract cfdc)
        {
            if (cfdc == null) return null;
            if (cfdc.FileId == Guid.Empty) return null;
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfdc.FileId);

            return set.GetTaxCalculation();
        }

        #region Service implementation



        #region FacturatieApp

        public EY.com.eBook.RuleEngine.AY2014.FacturatieAppTypes.Facturatie FacturatieAppGetData(CriteriaFileDataContract cfdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfdc.FileId);

            return set._datacontainer.FacturatieApp.Data;
        }


        public EY.com.eBook.RuleEngine.AY2014.FacturatieAppTypes.Facturatie FacturatieAppCalculate(CriteriaFileDataContract cfdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfdc.FileId);
            set.Calculate("FacturatieApp");

            return set._datacontainer.FacturatieApp.Data;
        }

        #region Entity validation


        public List<FileMessageDataContract> FacturatieAppFacturatieKlantValidate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaFacturatieAppFacturatieKlantDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ws.FacturatieApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == FacturatieApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> FacturatieAppFacturatieLeverancierCNValidate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaFacturatieAppFacturatieLeverancierCNDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ws.FacturatieApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == FacturatieApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> FacturatieAppFacturatieLeverancierValidate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaFacturatieAppFacturatieLeverancierDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ws.FacturatieApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == FacturatieApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> FacturatieAppFacturatieKlantCNValidate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaFacturatieAppFacturatieKlantCNDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ws.FacturatieApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == FacturatieApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        #endregion



        #region Add/Remove


        public EY.com.eBook.RuleEngine.AY2014.FacturatieAppTypes.Facturatie FacturatieAppOpTeMakenFacturenAdd(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaFacturatieAppFacturatieKlantDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            FacturatieApp rp = ws.FacturatieApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToOpTeMakenFacturen(cedc.Entity, ref ws);
            set.Calculate("FacturatieApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.FacturatieAppTypes.Facturatie FacturatieAppOpTeMakenFacturenRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            FacturatieApp rp = ws.FacturatieApp;
            rp.DeleteOpTeMakenFacturen(cfrdc.RowId, ref ws);
            set.Calculate("FacturatieApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.FacturatieAppTypes.Facturatie FacturatieAppOpTeMakenFacturenUpdate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaFacturatieAppFacturatieKlantDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            FacturatieApp rp = ws.FacturatieApp;
            rp.UpdateOpTeMakenFacturen(cedc.Entity, ref ws);
            set.Calculate("FacturatieApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.FacturatieAppTypes.Facturatie FacturatieAppTeOntvangenCNAdd(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaFacturatieAppFacturatieLeverancierCNDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            FacturatieApp rp = ws.FacturatieApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToTeOntvangenCN(cedc.Entity, ref ws);
            set.Calculate("FacturatieApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.FacturatieAppTypes.Facturatie FacturatieAppTeOntvangenCNRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            FacturatieApp rp = ws.FacturatieApp;
            rp.DeleteTeOntvangenCN(cfrdc.RowId, ref ws);
            set.Calculate("FacturatieApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.FacturatieAppTypes.Facturatie FacturatieAppTeOntvangenCNUpdate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaFacturatieAppFacturatieLeverancierCNDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            FacturatieApp rp = ws.FacturatieApp;
            rp.UpdateTeOntvangenCN(cedc.Entity, ref ws);
            set.Calculate("FacturatieApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.FacturatieAppTypes.Facturatie FacturatieAppTeOntvangenFacturenAdd(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaFacturatieAppFacturatieLeverancierDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            FacturatieApp rp = ws.FacturatieApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToTeOntvangenFacturen(cedc.Entity, ref ws);
            set.Calculate("FacturatieApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.FacturatieAppTypes.Facturatie FacturatieAppTeOntvangenFacturenRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            FacturatieApp rp = ws.FacturatieApp;
            rp.DeleteTeOntvangenFacturen(cfrdc.RowId, ref ws);
            set.Calculate("FacturatieApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.FacturatieAppTypes.Facturatie FacturatieAppTeOntvangenFacturenUpdate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaFacturatieAppFacturatieLeverancierDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            FacturatieApp rp = ws.FacturatieApp;
            rp.UpdateTeOntvangenFacturen(cedc.Entity, ref ws);
            set.Calculate("FacturatieApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.FacturatieAppTypes.Facturatie FacturatieAppOpTeMakenCNAdd(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaFacturatieAppFacturatieKlantCNDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            FacturatieApp rp = ws.FacturatieApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToOpTeMakenCN(cedc.Entity, ref ws);
            set.Calculate("FacturatieApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.FacturatieAppTypes.Facturatie FacturatieAppOpTeMakenCNRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            FacturatieApp rp = ws.FacturatieApp;
            rp.DeleteOpTeMakenCN(cfrdc.RowId, ref ws);
            set.Calculate("FacturatieApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.FacturatieAppTypes.Facturatie FacturatieAppOpTeMakenCNUpdate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaFacturatieAppFacturatieKlantCNDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            FacturatieApp rp = ws.FacturatieApp;
            rp.UpdateOpTeMakenCN(cedc.Entity, ref ws);
            set.Calculate("FacturatieApp");
            return rp.Data;
        }

        #endregion

        #endregion



        #region OverlopendeRekeningenApp

        public EY.com.eBook.RuleEngine.AY2014.OverlopendeRekeningenAppTypes.OverlopendeRekeningen OverlopendeRekeningenAppGetData(CriteriaFileDataContract cfdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfdc.FileId);

            return set._datacontainer.OverlopendeRekeningenApp.Data;
        }


        public EY.com.eBook.RuleEngine.AY2014.OverlopendeRekeningenAppTypes.OverlopendeRekeningen OverlopendeRekeningenAppCalculate(CriteriaFileDataContract cfdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfdc.FileId);
            set.Calculate("OverlopendeRekeningenApp");

            return set._datacontainer.OverlopendeRekeningenApp.Data;
        }

        #region Entity validation


        public List<FileMessageDataContract> OverlopendeRekeningenAppOverTeDragenKostValidate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaOverlopendeRekeningenAppOverTeDragenKostDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ws.OverlopendeRekeningenApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == OverlopendeRekeningenApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> OverlopendeRekeningenAppVerkregenOpbrengstValidate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaOverlopendeRekeningenAppVerkregenOpbrengstDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ws.OverlopendeRekeningenApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == OverlopendeRekeningenApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> OverlopendeRekeningenAppToeTeRekenenKostValidate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaOverlopendeRekeningenAppToeTeRekenenKostDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ws.OverlopendeRekeningenApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == OverlopendeRekeningenApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> OverlopendeRekeningenAppOverTeDragenOpbrengstValidate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaOverlopendeRekeningenAppOverTeDragenOpbrengstDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ws.OverlopendeRekeningenApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == OverlopendeRekeningenApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        #endregion



        #region Add/Remove


        public EY.com.eBook.RuleEngine.AY2014.OverlopendeRekeningenAppTypes.OverlopendeRekeningen OverlopendeRekeningenAppOverTeDragenKostenAdd(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaOverlopendeRekeningenAppOverTeDragenKostDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            OverlopendeRekeningenApp rp = ws.OverlopendeRekeningenApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToOverTeDragenKosten(cedc.Entity, ref ws);
            set.Calculate("OverlopendeRekeningenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.OverlopendeRekeningenAppTypes.OverlopendeRekeningen OverlopendeRekeningenAppOverTeDragenKostenRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            OverlopendeRekeningenApp rp = ws.OverlopendeRekeningenApp;
            rp.DeleteOverTeDragenKosten(cfrdc.RowId, ref ws);
            set.Calculate("OverlopendeRekeningenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.OverlopendeRekeningenAppTypes.OverlopendeRekeningen OverlopendeRekeningenAppOverTeDragenKostenUpdate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaOverlopendeRekeningenAppOverTeDragenKostDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            OverlopendeRekeningenApp rp = ws.OverlopendeRekeningenApp;
            rp.UpdateOverTeDragenKosten(cedc.Entity, ref ws);
            set.Calculate("OverlopendeRekeningenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.OverlopendeRekeningenAppTypes.OverlopendeRekeningen OverlopendeRekeningenAppVerkregenOpbrenstenAdd(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaOverlopendeRekeningenAppVerkregenOpbrengstDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            OverlopendeRekeningenApp rp = ws.OverlopendeRekeningenApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToVerkregenOpbrensten(cedc.Entity, ref ws);
            set.Calculate("OverlopendeRekeningenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.OverlopendeRekeningenAppTypes.OverlopendeRekeningen OverlopendeRekeningenAppVerkregenOpbrenstenRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            OverlopendeRekeningenApp rp = ws.OverlopendeRekeningenApp;
            rp.DeleteVerkregenOpbrensten(cfrdc.RowId, ref ws);
            set.Calculate("OverlopendeRekeningenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.OverlopendeRekeningenAppTypes.OverlopendeRekeningen OverlopendeRekeningenAppVerkregenOpbrenstenUpdate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaOverlopendeRekeningenAppVerkregenOpbrengstDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            OverlopendeRekeningenApp rp = ws.OverlopendeRekeningenApp;
            rp.UpdateVerkregenOpbrensten(cedc.Entity, ref ws);
            set.Calculate("OverlopendeRekeningenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.OverlopendeRekeningenAppTypes.OverlopendeRekeningen OverlopendeRekeningenAppToeTeRekenenKostenAdd(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaOverlopendeRekeningenAppToeTeRekenenKostDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            OverlopendeRekeningenApp rp = ws.OverlopendeRekeningenApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToToeTeRekenenKosten(cedc.Entity, ref ws);
            set.Calculate("OverlopendeRekeningenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.OverlopendeRekeningenAppTypes.OverlopendeRekeningen OverlopendeRekeningenAppToeTeRekenenKostenRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            OverlopendeRekeningenApp rp = ws.OverlopendeRekeningenApp;
            rp.DeleteToeTeRekenenKosten(cfrdc.RowId, ref ws);
            set.Calculate("OverlopendeRekeningenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.OverlopendeRekeningenAppTypes.OverlopendeRekeningen OverlopendeRekeningenAppToeTeRekenenKostenUpdate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaOverlopendeRekeningenAppToeTeRekenenKostDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            OverlopendeRekeningenApp rp = ws.OverlopendeRekeningenApp;
            rp.UpdateToeTeRekenenKosten(cedc.Entity, ref ws);
            set.Calculate("OverlopendeRekeningenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.OverlopendeRekeningenAppTypes.OverlopendeRekeningen OverlopendeRekeningenAppOverTeDragenOpbrengstenAdd(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaOverlopendeRekeningenAppOverTeDragenOpbrengstDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            OverlopendeRekeningenApp rp = ws.OverlopendeRekeningenApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToOverTeDragenOpbrengsten(cedc.Entity, ref ws);
            set.Calculate("OverlopendeRekeningenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.OverlopendeRekeningenAppTypes.OverlopendeRekeningen OverlopendeRekeningenAppOverTeDragenOpbrengstenRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            OverlopendeRekeningenApp rp = ws.OverlopendeRekeningenApp;
            rp.DeleteOverTeDragenOpbrengsten(cfrdc.RowId, ref ws);
            set.Calculate("OverlopendeRekeningenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.OverlopendeRekeningenAppTypes.OverlopendeRekeningen OverlopendeRekeningenAppOverTeDragenOpbrengstenUpdate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaOverlopendeRekeningenAppOverTeDragenOpbrengstDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            OverlopendeRekeningenApp rp = ws.OverlopendeRekeningenApp;
            rp.UpdateOverTeDragenOpbrengsten(cedc.Entity, ref ws);
            set.Calculate("OverlopendeRekeningenApp");
            return rp.Data;
        }

        #endregion

        #endregion



        #region VergelijkendeCijfersApp

        public EY.com.eBook.RuleEngine.AY2014.VergelijkendeCijfersAppTypes.WPVergelijkendeCijfers VergelijkendeCijfersAppGetData(CriteriaFileDataContract cfdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfdc.FileId);

            return set._datacontainer.VergelijkendeCijfersApp.Data;
        }


        public EY.com.eBook.RuleEngine.AY2014.VergelijkendeCijfersAppTypes.WPVergelijkendeCijfers VergelijkendeCijfersAppCalculate(CriteriaFileDataContract cfdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfdc.FileId);
            set.Calculate("VergelijkendeCijfersApp");

            return set._datacontainer.VergelijkendeCijfersApp.Data;
        }

        #region Entity validation


        public List<FileMessageDataContract> VergelijkendeCijfersAppCijferBlokValidate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaVergelijkendeCijfersAppCijferBlokDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ws.VergelijkendeCijfersApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == VergelijkendeCijfersApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        #endregion



        #region Add/Remove

        public EY.com.eBook.RuleEngine.AY2014.VergelijkendeCijfersAppTypes.WPVergelijkendeCijfers VergelijkendeCijfersAppKostenUpdate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaVergelijkendeCijfersAppCijferBlokDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            VergelijkendeCijfersApp rp = ws.VergelijkendeCijfersApp;
            rp.UpdateKosten(cedc.Entity, ref ws);
            set.Calculate("VergelijkendeCijfersApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.VergelijkendeCijfersAppTypes.WPVergelijkendeCijfers VergelijkendeCijfersAppOpbrengstenUpdate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaVergelijkendeCijfersAppCijferBlokDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            VergelijkendeCijfersApp rp = ws.VergelijkendeCijfersApp;
            rp.UpdateOpbrengsten(cedc.Entity, ref ws);
            set.Calculate("VergelijkendeCijfersApp");
            return rp.Data;
        }

        #endregion

        #endregion



        #region VoorschottenApp

        public EY.com.eBook.RuleEngine.AY2014.VoorschottenAppTypes.Voorschotten VoorschottenAppGetData(CriteriaFileDataContract cfdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfdc.FileId);

            return set._datacontainer.VoorschottenApp.Data;
        }


        public EY.com.eBook.RuleEngine.AY2014.VoorschottenAppTypes.Voorschotten VoorschottenAppCalculate(CriteriaFileDataContract cfdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfdc.FileId);
            set.Calculate("VoorschottenApp");

            return set._datacontainer.VoorschottenApp.Data;
        }

        #region Entity validation


        public List<FileMessageDataContract> VoorschottenAppGegevenValidate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaVoorschottenAppGegevenDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ws.VoorschottenApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == VoorschottenApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> VoorschottenAppIntrestVoetValidate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaVoorschottenAppIntrestVoetDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ws.VoorschottenApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == VoorschottenApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> VoorschottenAppRentegevendVoorschotValidate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaVoorschottenAppRentegevendVoorschotDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ws.VoorschottenApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == VoorschottenApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        #endregion



        #region Add/Remove


        public EY.com.eBook.RuleEngine.AY2014.VoorschottenAppTypes.Voorschotten VoorschottenAppGegevensAdd(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaVoorschottenAppGegevenDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            VoorschottenApp rp = ws.VoorschottenApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToGegevens(cedc.Entity, ref ws);
            set.Calculate("VoorschottenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.VoorschottenAppTypes.Voorschotten VoorschottenAppGegevensRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            VoorschottenApp rp = ws.VoorschottenApp;
            rp.DeleteGegevens(cfrdc.RowId, ref ws);
            set.Calculate("VoorschottenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.VoorschottenAppTypes.Voorschotten VoorschottenAppGegevensUpdate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaVoorschottenAppGegevenDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            VoorschottenApp rp = ws.VoorschottenApp;
            rp.UpdateGegevens(cedc.Entity, ref ws);
            set.Calculate("VoorschottenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.VoorschottenAppTypes.Voorschotten VoorschottenAppInterestVoetenAdd(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaVoorschottenAppIntrestVoetDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            VoorschottenApp rp = ws.VoorschottenApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToInterestVoeten(cedc.Entity, ref ws);
            set.Calculate("VoorschottenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.VoorschottenAppTypes.Voorschotten VoorschottenAppInterestVoetenRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            VoorschottenApp rp = ws.VoorschottenApp;
            rp.DeleteInterestVoeten(cfrdc.RowId, ref ws);
            set.Calculate("VoorschottenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.VoorschottenAppTypes.Voorschotten VoorschottenAppInterestVoetenUpdate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaVoorschottenAppIntrestVoetDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            VoorschottenApp rp = ws.VoorschottenApp;
            rp.UpdateInterestVoeten(cedc.Entity, ref ws);
            set.Calculate("VoorschottenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.VoorschottenAppTypes.Voorschotten VoorschottenAppRentegevendeVoorschottenAdd(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaVoorschottenAppRentegevendVoorschotDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            VoorschottenApp rp = ws.VoorschottenApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToRentegevendeVoorschotten(cedc.Entity, ref ws);
            set.Calculate("VoorschottenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.VoorschottenAppTypes.Voorschotten VoorschottenAppRentegevendeVoorschottenRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            VoorschottenApp rp = ws.VoorschottenApp;
            rp.DeleteRentegevendeVoorschotten(cfrdc.RowId, ref ws);
            set.Calculate("VoorschottenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.VoorschottenAppTypes.Voorschotten VoorschottenAppRentegevendeVoorschottenUpdate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaVoorschottenAppRentegevendVoorschotDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            VoorschottenApp rp = ws.VoorschottenApp;
            rp.UpdateRentegevendeVoorschotten(cedc.Entity, ref ws);
            set.Calculate("VoorschottenApp");
            return rp.Data;
        }

        #endregion

        #endregion



        #region ErelonenHuurApp

        public EY.com.eBook.RuleEngine.AY2014.ErelonenHuurAppTypes.ErelonenHuur ErelonenHuurAppGetData(CriteriaFileDataContract cfdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfdc.FileId);

            return set._datacontainer.ErelonenHuurApp.Data;
        }


        public EY.com.eBook.RuleEngine.AY2014.ErelonenHuurAppTypes.ErelonenHuur ErelonenHuurAppCalculate(CriteriaFileDataContract cfdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfdc.FileId);
            set.Calculate("ErelonenHuurApp");

            return set._datacontainer.ErelonenHuurApp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.ErelonenHuurAppTypes.ErelonenHuur ErelonenHuurAppProAccImportHistory(CriteriaFileDataContract cfdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfdc.FileId);
            List<string> nrs = set._datacontainer.ErelonenHuurApp.GetImportableAccounts();
            ClientBaseDataContract cbdc = set._datacontainer.GetClientBase();
            List<AccountHistoryDataContract> hist = GetAccountsHistory(cbdc, nrs, set._datacontainer.DataContract.StartDate, set._datacontainer.DataContract.EndDate);
            WorksheetSetData wsd = set._datacontainer;
            set._datacontainer.ErelonenHuurApp.ImportAccountHistory(hist, ref wsd);
            set.Calculate("ErelonenHuurApp");

            return set._datacontainer.ErelonenHuurApp.Data;
        }

        #region Entity validation


        public List<FileMessageDataContract> ErelonenHuurAppImportItemValidate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaErelonenHuurAppImportItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ws.ErelonenHuurApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == ErelonenHuurApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> ErelonenHuurAppImportedHistoryValidate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaErelonenHuurAppImportedHistoryDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ws.ErelonenHuurApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == ErelonenHuurApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> ErelonenHuurAppVerdelingHistoriekValidate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaErelonenHuurAppVerdelingHistoriekDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ws.ErelonenHuurApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == ErelonenHuurApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> ErelonenHuurAppFiche281_50Validate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaErelonenHuurAppFiche281_50DataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ws.ErelonenHuurApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == ErelonenHuurApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        #endregion



        #region Add/Remove


        public EY.com.eBook.RuleEngine.AY2014.ErelonenHuurAppTypes.ErelonenHuur ErelonenHuurAppHistoriekAdd(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaErelonenHuurAppVerdelingHistoriekDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ErelonenHuurApp rp = ws.ErelonenHuurApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToHistoriek(cedc.Entity, ref ws);
            set.Calculate("ErelonenHuurApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.ErelonenHuurAppTypes.ErelonenHuur ErelonenHuurAppHistoriekRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ErelonenHuurApp rp = ws.ErelonenHuurApp;
            rp.DeleteHistoriek(cfrdc.RowId, ref ws);
            set.Calculate("ErelonenHuurApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.ErelonenHuurAppTypes.ErelonenHuur ErelonenHuurAppHistoriekUpdate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaErelonenHuurAppVerdelingHistoriekDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ErelonenHuurApp rp = ws.ErelonenHuurApp;
            rp.UpdateHistoriek(cedc.Entity, ref ws);
            set.Calculate("ErelonenHuurApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.ErelonenHuurAppTypes.ErelonenHuur ErelonenHuurAppFiches281_50Update(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaErelonenHuurAppFiche281_50DataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ErelonenHuurApp rp = ws.ErelonenHuurApp;
            rp.UpdateFiches281_50(cedc.Entity, ref ws);
            set.Calculate("ErelonenHuurApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.ErelonenHuurAppTypes.ErelonenHuur ErelonenHuurAppFiche281_50ColAdd(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaErelonenHuurAppFiche281_50DataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ErelonenHuurApp rp = ws.ErelonenHuurApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToFiche281_50Col(cedc.Entity, ref ws);
            set.Calculate("ErelonenHuurApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.ErelonenHuurAppTypes.ErelonenHuur ErelonenHuurAppFiche281_50ColRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ErelonenHuurApp rp = ws.ErelonenHuurApp;
            rp.DeleteFiche281_50Col(cfrdc.RowId, ref ws);
            set.Calculate("ErelonenHuurApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.ErelonenHuurAppTypes.ErelonenHuur ErelonenHuurAppFiche281_50ColUpdate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaErelonenHuurAppFiche281_50DataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ErelonenHuurApp rp = ws.ErelonenHuurApp;
            rp.UpdateFiche281_50Col(cedc.Entity, ref ws);
            set.Calculate("ErelonenHuurApp");
            return rp.Data;
        }

        #endregion

        #endregion



        #region VoordelenApp

        public EY.com.eBook.RuleEngine.AY2014.VoordelenAppTypes.Voordelen VoordelenAppGetData(CriteriaFileDataContract cfdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfdc.FileId);

            return set._datacontainer.VoordelenApp.Data;
        }


        public EY.com.eBook.RuleEngine.AY2014.VoordelenAppTypes.Voordelen VoordelenAppCalculate(CriteriaFileDataContract cfdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfdc.FileId);
            set.Calculate("VoordelenApp");

            return set._datacontainer.VoordelenApp.Data;
        }

        #region Entity validation


        public List<FileMessageDataContract> VoordelenAppVAA_AutoCO2Validate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaVoordelenAppVAA_AutoCO2DataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ws.VoordelenApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == VoordelenApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> VoordelenAppVAA_AutoRupoValidate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaVoordelenAppVAA_AutoRupoDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ws.VoordelenApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == VoordelenApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> VoordelenAppVAA_NutsValidate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaVoordelenAppVAA_NutsDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ws.VoordelenApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == VoordelenApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> VoordelenAppVAA_HUURValidate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaVoordelenAppVAA_HUURDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ws.VoordelenApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == VoordelenApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> VoordelenAppVAA_HUURHERKValidate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaVoordelenAppVAA_HUURHERKDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ws.VoordelenApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == VoordelenApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> VoordelenAppVAA_PCValidate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaVoordelenAppVAA_PCDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ws.VoordelenApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == VoordelenApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> VoordelenAppVAA_TelefonieValidate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaVoordelenAppVAA_TelefonieDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ws.VoordelenApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == VoordelenApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> VoordelenAppVAA_OverigeValidate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaVoordelenAppVAA_OverigeDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ws.VoordelenApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == VoordelenApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> VoordelenAppVAA_Interesten_RC_BedrijfsleiderValidate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaVoordelenAppVAA_Interesten_RC_BedrijfsleiderDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ws.VoordelenApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == VoordelenApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> VoordelenAppVAA_Soc_Bijdr_BedrijfsleiderValidate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaVoordelenAppVAA_Soc_Bijdr_BedrijfsleiderDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ws.VoordelenApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == VoordelenApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> VoordelenAppVAA_Overige_NutsValidate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaVoordelenAppVAA_Overige_NutsDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ws.VoordelenApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == VoordelenApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        #endregion



        #region Add/Remove


        public EY.com.eBook.RuleEngine.AY2014.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardAutoCO2Add(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaVoordelenAppVAA_AutoCO2DataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            VoordelenApp rp = ws.VoordelenApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToVoordelenVanAlleAardAutoCO2(cedc.Entity, ref ws);
            set.Calculate("VoordelenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardAutoCO2Remove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            VoordelenApp rp = ws.VoordelenApp;
            rp.DeleteVoordelenVanAlleAardAutoCO2(cfrdc.RowId, ref ws);
            set.Calculate("VoordelenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardAutoCO2Update(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaVoordelenAppVAA_AutoCO2DataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            VoordelenApp rp = ws.VoordelenApp;
            rp.UpdateVoordelenVanAlleAardAutoCO2(cedc.Entity, ref ws);
            set.Calculate("VoordelenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardAutoRupoAdd(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaVoordelenAppVAA_AutoRupoDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            VoordelenApp rp = ws.VoordelenApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToVoordelenVanAlleAardAutoRupo(cedc.Entity, ref ws);
            set.Calculate("VoordelenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardAutoRupoRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            VoordelenApp rp = ws.VoordelenApp;
            rp.DeleteVoordelenVanAlleAardAutoRupo(cfrdc.RowId, ref ws);
            set.Calculate("VoordelenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardAutoRupoUpdate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaVoordelenAppVAA_AutoRupoDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            VoordelenApp rp = ws.VoordelenApp;
            rp.UpdateVoordelenVanAlleAardAutoRupo(cedc.Entity, ref ws);
            set.Calculate("VoordelenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardNutsAdd(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaVoordelenAppVAA_NutsDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            VoordelenApp rp = ws.VoordelenApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToVoordelenVanAlleAardNuts(cedc.Entity, ref ws);
            set.Calculate("VoordelenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardNutsRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            VoordelenApp rp = ws.VoordelenApp;
            rp.DeleteVoordelenVanAlleAardNuts(cfrdc.RowId, ref ws);
            set.Calculate("VoordelenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardNutsUpdate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaVoordelenAppVAA_NutsDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            VoordelenApp rp = ws.VoordelenApp;
            rp.UpdateVoordelenVanAlleAardNuts(cedc.Entity, ref ws);
            set.Calculate("VoordelenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardHuurAdd(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaVoordelenAppVAA_HUURDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            VoordelenApp rp = ws.VoordelenApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToVoordelenVanAlleAardHuur(cedc.Entity, ref ws);
            set.Calculate("VoordelenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardHuurRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            VoordelenApp rp = ws.VoordelenApp;
            rp.DeleteVoordelenVanAlleAardHuur(cfrdc.RowId, ref ws);
            set.Calculate("VoordelenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardHuurUpdate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaVoordelenAppVAA_HUURDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            VoordelenApp rp = ws.VoordelenApp;
            rp.UpdateVoordelenVanAlleAardHuur(cedc.Entity, ref ws);
            set.Calculate("VoordelenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardHuurHerkwalificatieAdd(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaVoordelenAppVAA_HUURHERKDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            VoordelenApp rp = ws.VoordelenApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToVoordelenVanAlleAardHuurHerkwalificatie(cedc.Entity, ref ws);
            set.Calculate("VoordelenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardHuurHerkwalificatieRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            VoordelenApp rp = ws.VoordelenApp;
            rp.DeleteVoordelenVanAlleAardHuurHerkwalificatie(cfrdc.RowId, ref ws);
            set.Calculate("VoordelenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardHuurHerkwalificatieUpdate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaVoordelenAppVAA_HUURHERKDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            VoordelenApp rp = ws.VoordelenApp;
            rp.UpdateVoordelenVanAlleAardHuurHerkwalificatie(cedc.Entity, ref ws);
            set.Calculate("VoordelenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardPCAdd(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaVoordelenAppVAA_PCDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            VoordelenApp rp = ws.VoordelenApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToVoordelenVanAlleAardPC(cedc.Entity, ref ws);
            set.Calculate("VoordelenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardPCRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            VoordelenApp rp = ws.VoordelenApp;
            rp.DeleteVoordelenVanAlleAardPC(cfrdc.RowId, ref ws);
            set.Calculate("VoordelenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardPCUpdate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaVoordelenAppVAA_PCDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            VoordelenApp rp = ws.VoordelenApp;
            rp.UpdateVoordelenVanAlleAardPC(cedc.Entity, ref ws);
            set.Calculate("VoordelenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardTelefonieAdd(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaVoordelenAppVAA_TelefonieDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            VoordelenApp rp = ws.VoordelenApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToVoordelenVanAlleAardTelefonie(cedc.Entity, ref ws);
            set.Calculate("VoordelenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardTelefonieRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            VoordelenApp rp = ws.VoordelenApp;
            rp.DeleteVoordelenVanAlleAardTelefonie(cfrdc.RowId, ref ws);
            set.Calculate("VoordelenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardTelefonieUpdate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaVoordelenAppVAA_TelefonieDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            VoordelenApp rp = ws.VoordelenApp;
            rp.UpdateVoordelenVanAlleAardTelefonie(cedc.Entity, ref ws);
            set.Calculate("VoordelenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardOverigeAdd(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaVoordelenAppVAA_OverigeDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            VoordelenApp rp = ws.VoordelenApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToVoordelenVanAlleAardOverige(cedc.Entity, ref ws);
            set.Calculate("VoordelenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardOverigeRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            VoordelenApp rp = ws.VoordelenApp;
            rp.DeleteVoordelenVanAlleAardOverige(cfrdc.RowId, ref ws);
            set.Calculate("VoordelenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardOverigeUpdate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaVoordelenAppVAA_OverigeDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            VoordelenApp rp = ws.VoordelenApp;
            rp.UpdateVoordelenVanAlleAardOverige(cedc.Entity, ref ws);
            set.Calculate("VoordelenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.VoordelenAppTypes.Voordelen VoordelenAppVoordelenInterestenRCBedrijfsleiderAdd(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaVoordelenAppVAA_Interesten_RC_BedrijfsleiderDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            VoordelenApp rp = ws.VoordelenApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToVoordelenInterestenRCBedrijfsleider(cedc.Entity, ref ws);
            set.Calculate("VoordelenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.VoordelenAppTypes.Voordelen VoordelenAppVoordelenInterestenRCBedrijfsleiderRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            VoordelenApp rp = ws.VoordelenApp;
            rp.DeleteVoordelenInterestenRCBedrijfsleider(cfrdc.RowId, ref ws);
            set.Calculate("VoordelenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.VoordelenAppTypes.Voordelen VoordelenAppVoordelenInterestenRCBedrijfsleiderUpdate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaVoordelenAppVAA_Interesten_RC_BedrijfsleiderDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            VoordelenApp rp = ws.VoordelenApp;
            rp.UpdateVoordelenInterestenRCBedrijfsleider(cedc.Entity, ref ws);
            set.Calculate("VoordelenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.VoordelenAppTypes.Voordelen VoordelenAppVoordelenSocialeBijdragenBedrijfsleiderAdd(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaVoordelenAppVAA_Soc_Bijdr_BedrijfsleiderDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            VoordelenApp rp = ws.VoordelenApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToVoordelenSocialeBijdragenBedrijfsleider(cedc.Entity, ref ws);
            set.Calculate("VoordelenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.VoordelenAppTypes.Voordelen VoordelenAppVoordelenSocialeBijdragenBedrijfsleiderRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            VoordelenApp rp = ws.VoordelenApp;
            rp.DeleteVoordelenSocialeBijdragenBedrijfsleider(cfrdc.RowId, ref ws);
            set.Calculate("VoordelenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.VoordelenAppTypes.Voordelen VoordelenAppVoordelenSocialeBijdragenBedrijfsleiderUpdate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaVoordelenAppVAA_Soc_Bijdr_BedrijfsleiderDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            VoordelenApp rp = ws.VoordelenApp;
            rp.UpdateVoordelenSocialeBijdragenBedrijfsleider(cedc.Entity, ref ws);
            set.Calculate("VoordelenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.VoordelenAppTypes.Voordelen VoordelenAppVoordelenOverigeNutsAdd(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaVoordelenAppVAA_Overige_NutsDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            VoordelenApp rp = ws.VoordelenApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToVoordelenOverigeNuts(cedc.Entity, ref ws);
            set.Calculate("VoordelenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.VoordelenAppTypes.Voordelen VoordelenAppVoordelenOverigeNutsRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            VoordelenApp rp = ws.VoordelenApp;
            rp.DeleteVoordelenOverigeNuts(cfrdc.RowId, ref ws);
            set.Calculate("VoordelenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.VoordelenAppTypes.Voordelen VoordelenAppVoordelenOverigeNutsUpdate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaVoordelenAppVAA_Overige_NutsDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            VoordelenApp rp = ws.VoordelenApp;
            rp.UpdateVoordelenOverigeNuts(cedc.Entity, ref ws);
            set.Calculate("VoordelenApp");
            return rp.Data;
        }

        #endregion

        #endregion



        #region PersoneelApp

        public EY.com.eBook.RuleEngine.AY2014.PersoneelAppTypes.WPPersoneel PersoneelAppGetData(CriteriaFileDataContract cfdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfdc.FileId);

            return set._datacontainer.PersoneelApp.Data;
        }


        public EY.com.eBook.RuleEngine.AY2014.PersoneelAppTypes.WPPersoneel PersoneelAppCalculate(CriteriaFileDataContract cfdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfdc.FileId);
            set.Calculate("PersoneelApp");

            return set._datacontainer.PersoneelApp.Data;
        }

        #region Entity validation


        public List<FileMessageDataContract> PersoneelAppItem325Validate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaPersoneelAppItem325DataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ws.PersoneelApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == PersoneelApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> PersoneelAppVerschilItemValidate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaPersoneelAppVerschilItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ws.PersoneelApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == PersoneelApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> PersoneelAppWerknemerValidate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaPersoneelAppWerknemerDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ws.PersoneelApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == PersoneelApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> PersoneelAppAlgemeenValidate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaPersoneelAppAlgemeenDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ws.PersoneelApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == PersoneelApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> PersoneelAppVakantiegeldValidate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaPersoneelAppVakantiegeldDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ws.PersoneelApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == PersoneelApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        #endregion



        #region Add/Remove


        public EY.com.eBook.RuleEngine.AY2014.PersoneelAppTypes.WPPersoneel PersoneelApp_325_20CollAdd(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaPersoneelAppItem325DataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            PersoneelApp rp = ws.PersoneelApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddTo_325_20Coll(cedc.Entity, ref ws);
            set.Calculate("PersoneelApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.PersoneelAppTypes.WPPersoneel PersoneelApp_325_20CollRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            PersoneelApp rp = ws.PersoneelApp;
            rp.Delete_325_20Coll(cfrdc.RowId, ref ws);
            set.Calculate("PersoneelApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.PersoneelAppTypes.WPPersoneel PersoneelApp_325_20CollUpdate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaPersoneelAppItem325DataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            PersoneelApp rp = ws.PersoneelApp;
            rp.Update_325_20Coll(cedc.Entity, ref ws);
            set.Calculate("PersoneelApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.PersoneelAppTypes.WPPersoneel PersoneelAppTeVerklarenVerschil325_20Add(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaPersoneelAppVerschilItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            PersoneelApp rp = ws.PersoneelApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToTeVerklarenVerschil325_20(cedc.Entity, ref ws);
            set.Calculate("PersoneelApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.PersoneelAppTypes.WPPersoneel PersoneelAppTeVerklarenVerschil325_20Remove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            PersoneelApp rp = ws.PersoneelApp;
            rp.DeleteTeVerklarenVerschil325_20(cfrdc.RowId, ref ws);
            set.Calculate("PersoneelApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.PersoneelAppTypes.WPPersoneel PersoneelAppTeVerklarenVerschil325_20Update(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaPersoneelAppVerschilItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            PersoneelApp rp = ws.PersoneelApp;
            rp.UpdateTeVerklarenVerschil325_20(cedc.Entity, ref ws);
            set.Calculate("PersoneelApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.PersoneelAppTypes.WPPersoneel PersoneelApp_325_10CollAdd(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaPersoneelAppItem325DataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            PersoneelApp rp = ws.PersoneelApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddTo_325_10Coll(cedc.Entity, ref ws);
            set.Calculate("PersoneelApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.PersoneelAppTypes.WPPersoneel PersoneelApp_325_10CollRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            PersoneelApp rp = ws.PersoneelApp;
            rp.Delete_325_10Coll(cfrdc.RowId, ref ws);
            set.Calculate("PersoneelApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.PersoneelAppTypes.WPPersoneel PersoneelApp_325_10CollUpdate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaPersoneelAppItem325DataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            PersoneelApp rp = ws.PersoneelApp;
            rp.Update_325_10Coll(cedc.Entity, ref ws);
            set.Calculate("PersoneelApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.PersoneelAppTypes.WPPersoneel PersoneelAppTeVerklarenVerschil325_10Add(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaPersoneelAppVerschilItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            PersoneelApp rp = ws.PersoneelApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToTeVerklarenVerschil325_10(cedc.Entity, ref ws);
            set.Calculate("PersoneelApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.PersoneelAppTypes.WPPersoneel PersoneelAppTeVerklarenVerschil325_10Remove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            PersoneelApp rp = ws.PersoneelApp;
            rp.DeleteTeVerklarenVerschil325_10(cfrdc.RowId, ref ws);
            set.Calculate("PersoneelApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.PersoneelAppTypes.WPPersoneel PersoneelAppTeVerklarenVerschil325_10Update(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaPersoneelAppVerschilItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            PersoneelApp rp = ws.PersoneelApp;
            rp.UpdateTeVerklarenVerschil325_10(cedc.Entity, ref ws);
            set.Calculate("PersoneelApp");
            return rp.Data;
        }


        public EY.com.eBook.RuleEngine.AY2014.PersoneelAppTypes.WPPersoneel PersoneelAppArbeidersFormUpdate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaPersoneelAppWerknemerDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            PersoneelApp rp = ws.PersoneelApp;
            rp.UpdateArbeidersForm(cedc.Entity, ref ws);
            set.Calculate("PersoneelApp");
            return rp.Data;
        }



        public EY.com.eBook.RuleEngine.AY2014.PersoneelAppTypes.WPPersoneel PersoneelAppBediendeFormUpdate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaPersoneelAppWerknemerDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            PersoneelApp rp = ws.PersoneelApp;
            rp.UpdateBediendeForm(cedc.Entity, ref ws);
            set.Calculate("PersoneelApp");
            return rp.Data;
        }


        public EY.com.eBook.RuleEngine.AY2014.PersoneelAppTypes.WPPersoneel PersoneelAppAansluitingArbeidersAdd(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaPersoneelAppVerschilItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            PersoneelApp rp = ws.PersoneelApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToAansluitingArbeiders(cedc.Entity, ref ws);
            set.Calculate("PersoneelApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.PersoneelAppTypes.WPPersoneel PersoneelAppAansluitingArbeidersRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            PersoneelApp rp = ws.PersoneelApp;
            rp.DeleteAansluitingArbeiders(cfrdc.RowId, ref ws);
            set.Calculate("PersoneelApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.PersoneelAppTypes.WPPersoneel PersoneelAppAansluitingArbeidersUpdate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaPersoneelAppVerschilItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            PersoneelApp rp = ws.PersoneelApp;
            rp.UpdateAansluitingArbeiders(cedc.Entity, ref ws);
            set.Calculate("PersoneelApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.PersoneelAppTypes.WPPersoneel PersoneelAppAansluitingBediendenAdd(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaPersoneelAppVerschilItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            PersoneelApp rp = ws.PersoneelApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToAansluitingBedienden(cedc.Entity, ref ws);
            set.Calculate("PersoneelApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.PersoneelAppTypes.WPPersoneel PersoneelAppAansluitingBediendenRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            PersoneelApp rp = ws.PersoneelApp;
            rp.DeleteAansluitingBedienden(cfrdc.RowId, ref ws);
            set.Calculate("PersoneelApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.PersoneelAppTypes.WPPersoneel PersoneelAppAansluitingBediendenUpdate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaPersoneelAppVerschilItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            PersoneelApp rp = ws.PersoneelApp;
            rp.UpdateAansluitingBedienden(cedc.Entity, ref ws);
            set.Calculate("PersoneelApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.PersoneelAppTypes.WPPersoneel PersoneelAppRSZCollAdd(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaPersoneelAppAlgemeenDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            PersoneelApp rp = ws.PersoneelApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToRSZColl(cedc.Entity, ref ws);
            set.Calculate("PersoneelApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.PersoneelAppTypes.WPPersoneel PersoneelAppRSZCollRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            PersoneelApp rp = ws.PersoneelApp;
            rp.DeleteRSZColl(cfrdc.RowId, ref ws);
            set.Calculate("PersoneelApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.PersoneelAppTypes.WPPersoneel PersoneelAppRSZCollUpdate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaPersoneelAppAlgemeenDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            PersoneelApp rp = ws.PersoneelApp;
            rp.UpdateRSZColl(cedc.Entity, ref ws);
            set.Calculate("PersoneelApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.PersoneelAppTypes.WPPersoneel PersoneelAppBVsCollAdd(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaPersoneelAppAlgemeenDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            PersoneelApp rp = ws.PersoneelApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToBVsColl(cedc.Entity, ref ws);
            set.Calculate("PersoneelApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.PersoneelAppTypes.WPPersoneel PersoneelAppBVsCollRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            PersoneelApp rp = ws.PersoneelApp;
            rp.DeleteBVsColl(cfrdc.RowId, ref ws);
            set.Calculate("PersoneelApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.PersoneelAppTypes.WPPersoneel PersoneelAppBVsCollUpdate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaPersoneelAppAlgemeenDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            PersoneelApp rp = ws.PersoneelApp;
            rp.UpdateBVsColl(cedc.Entity, ref ws);
            set.Calculate("PersoneelApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.PersoneelAppTypes.WPPersoneel PersoneelAppNettoCollAdd(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaPersoneelAppAlgemeenDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            PersoneelApp rp = ws.PersoneelApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToNettoColl(cedc.Entity, ref ws);
            set.Calculate("PersoneelApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.PersoneelAppTypes.WPPersoneel PersoneelAppNettoCollRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            PersoneelApp rp = ws.PersoneelApp;
            rp.DeleteNettoColl(cfrdc.RowId, ref ws);
            set.Calculate("PersoneelApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.PersoneelAppTypes.WPPersoneel PersoneelAppNettoCollUpdate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaPersoneelAppAlgemeenDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            PersoneelApp rp = ws.PersoneelApp;
            rp.UpdateNettoColl(cedc.Entity, ref ws);
            set.Calculate("PersoneelApp");
            return rp.Data;
        }


        public EY.com.eBook.RuleEngine.AY2014.PersoneelAppTypes.WPPersoneel PersoneelAppVakantieGeldCollUpdate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaPersoneelAppVakantiegeldDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            PersoneelApp rp = ws.PersoneelApp;
            rp.UpdateVakantieGeldColl(cedc.Entity, ref ws);
            set.Calculate("PersoneelApp");
            return rp.Data;
        }


        #endregion

        #endregion



        #region BTWApp

        public EY.com.eBook.RuleEngine.AY2014.BTWAppTypes.WPBTW BTWAppGetData(CriteriaFileDataContract cfdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfdc.FileId);

            return set._datacontainer.BTWApp.Data;
        }


        public EY.com.eBook.RuleEngine.AY2014.BTWAppTypes.WPBTW BTWAppCalculate(CriteriaFileDataContract cfdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfdc.FileId);
            set.Calculate("BTWApp");

            return set._datacontainer.BTWApp.Data;
        }

        #region Entity validation


        public List<FileMessageDataContract> BTWAppOmzetAangifteValidate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaBTWAppOmzetAangifteDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ws.BTWApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == BTWApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> BTWAppOmzetGeboektValidate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaBTWAppOmzetGeboektDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ws.BTWApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == BTWApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> BTWAppOmzetVerklaringValidate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaBTWAppOmzetVerklaringDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ws.BTWApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == BTWApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> BTWAppRCAangifteValidate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaBTWAppRCAangifteDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ws.BTWApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == BTWApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> BTWAppRCGeboektValidate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaBTWAppRCGeboektDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ws.BTWApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == BTWApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> BTWAppRCVerklaringValidate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaBTWAppRCVerklaringDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ws.BTWApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == BTWApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        #endregion



        #region Add/Remove


        public EY.com.eBook.RuleEngine.AY2014.BTWAppTypes.WPBTW BTWAppOmzetAangiftesAdd(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaBTWAppOmzetAangifteDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            BTWApp rp = ws.BTWApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToOmzetAangiftes(cedc.Entity, ref ws);
            set.Calculate("BTWApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.BTWAppTypes.WPBTW BTWAppOmzetAangiftesRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            BTWApp rp = ws.BTWApp;
            rp.DeleteOmzetAangiftes(cfrdc.RowId, ref ws);
            set.Calculate("BTWApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.BTWAppTypes.WPBTW BTWAppOmzetAangiftesUpdate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaBTWAppOmzetAangifteDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            BTWApp rp = ws.BTWApp;
            rp.UpdateOmzetAangiftes(cedc.Entity, ref ws);
            set.Calculate("BTWApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.BTWAppTypes.WPBTW BTWAppOmzettenGeboektAdd(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaBTWAppOmzetGeboektDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            BTWApp rp = ws.BTWApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToOmzettenGeboekt(cedc.Entity, ref ws);
            set.Calculate("BTWApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.BTWAppTypes.WPBTW BTWAppOmzettenGeboektRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            BTWApp rp = ws.BTWApp;
            rp.DeleteOmzettenGeboekt(cfrdc.RowId, ref ws);
            set.Calculate("BTWApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.BTWAppTypes.WPBTW BTWAppOmzettenGeboektUpdate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaBTWAppOmzetGeboektDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            BTWApp rp = ws.BTWApp;
            rp.UpdateOmzettenGeboekt(cedc.Entity, ref ws);
            set.Calculate("BTWApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.BTWAppTypes.WPBTW BTWAppOmzetVerklaringenAdd(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaBTWAppOmzetVerklaringDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            BTWApp rp = ws.BTWApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToOmzetVerklaringen(cedc.Entity, ref ws);
            set.Calculate("BTWApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.BTWAppTypes.WPBTW BTWAppOmzetVerklaringenRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            BTWApp rp = ws.BTWApp;
            rp.DeleteOmzetVerklaringen(cfrdc.RowId, ref ws);
            set.Calculate("BTWApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.BTWAppTypes.WPBTW BTWAppOmzetVerklaringenUpdate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaBTWAppOmzetVerklaringDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            BTWApp rp = ws.BTWApp;
            rp.UpdateOmzetVerklaringen(cedc.Entity, ref ws);
            set.Calculate("BTWApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.BTWAppTypes.WPBTW BTWAppRCAangiftesAdd(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaBTWAppRCAangifteDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            BTWApp rp = ws.BTWApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToRCAangiftes(cedc.Entity, ref ws);
            set.Calculate("BTWApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.BTWAppTypes.WPBTW BTWAppRCAangiftesRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            BTWApp rp = ws.BTWApp;
            rp.DeleteRCAangiftes(cfrdc.RowId, ref ws);
            set.Calculate("BTWApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.BTWAppTypes.WPBTW BTWAppRCAangiftesUpdate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaBTWAppRCAangifteDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            BTWApp rp = ws.BTWApp;
            rp.UpdateRCAangiftes(cedc.Entity, ref ws);
            set.Calculate("BTWApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.BTWAppTypes.WPBTW BTWAppRCenGeboektAdd(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaBTWAppRCGeboektDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            BTWApp rp = ws.BTWApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToRCenGeboekt(cedc.Entity, ref ws);
            set.Calculate("BTWApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.BTWAppTypes.WPBTW BTWAppRCenGeboektRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            BTWApp rp = ws.BTWApp;
            rp.DeleteRCenGeboekt(cfrdc.RowId, ref ws);
            set.Calculate("BTWApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.BTWAppTypes.WPBTW BTWAppRCenGeboektUpdate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaBTWAppRCGeboektDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            BTWApp rp = ws.BTWApp;
            rp.UpdateRCenGeboekt(cedc.Entity, ref ws);
            set.Calculate("BTWApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.BTWAppTypes.WPBTW BTWAppRCVerklaringenAdd(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaBTWAppRCVerklaringDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            BTWApp rp = ws.BTWApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToRCVerklaringen(cedc.Entity, ref ws);
            set.Calculate("BTWApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.BTWAppTypes.WPBTW BTWAppRCVerklaringenRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            BTWApp rp = ws.BTWApp;
            rp.DeleteRCVerklaringen(cfrdc.RowId, ref ws);
            set.Calculate("BTWApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.BTWAppTypes.WPBTW BTWAppRCVerklaringenUpdate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaBTWAppRCVerklaringDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            BTWApp rp = ws.BTWApp;
            rp.UpdateRCVerklaringen(cedc.Entity, ref ws);
            set.Calculate("BTWApp");
            return rp.Data;
        }

        #endregion

        #endregion



        #region RVIntrestenApp

        public EY.com.eBook.RuleEngine.AY2014.RVIntrestenAppTypes.WPRVIntresten RVIntrestenAppGetData(CriteriaFileDataContract cfdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfdc.FileId);

            return set._datacontainer.RVIntrestenApp.Data;
        }


        public EY.com.eBook.RuleEngine.AY2014.RVIntrestenAppTypes.WPRVIntresten RVIntrestenAppCalculate(CriteriaFileDataContract cfdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfdc.FileId);
            set.Calculate("RVIntrestenApp");

            return set._datacontainer.RVIntrestenApp.Data;
        }

        #region Entity validation


        public List<FileMessageDataContract> RVIntrestenAppIntrestItemValidate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaRVIntrestenAppIntrestItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ws.RVIntrestenApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == RVIntrestenApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> RVIntrestenAppAdjustmentValidate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaRVIntrestenAppAdjustmentDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ws.RVIntrestenApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == RVIntrestenApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> RVIntrestenAppVerchilItemValidate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaRVIntrestenAppVerchilItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ws.RVIntrestenApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == RVIntrestenApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        #endregion



        #region Add/Remove


        public EY.com.eBook.RuleEngine.AY2014.RVIntrestenAppTypes.WPRVIntresten RVIntrestenAppIntrestenUpdate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaRVIntrestenAppIntrestItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            RVIntrestenApp rp = ws.RVIntrestenApp;
            rp.UpdateIntresten(cedc.Entity, ref ws);
            set.Calculate("RVIntrestenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.RVIntrestenAppTypes.WPRVIntresten RVIntrestenAppAdjustmentsAdd(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaRVIntrestenAppAdjustmentDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            RVIntrestenApp rp = ws.RVIntrestenApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToAdjustments(cedc.Entity, ref ws);
            set.Calculate("RVIntrestenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.RVIntrestenAppTypes.WPRVIntresten RVIntrestenAppAdjustmentsRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            RVIntrestenApp rp = ws.RVIntrestenApp;
            rp.DeleteAdjustments(cfrdc.RowId, ref ws);
            set.Calculate("RVIntrestenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.RVIntrestenAppTypes.WPRVIntresten RVIntrestenAppAdjustmentsUpdate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaRVIntrestenAppAdjustmentDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            RVIntrestenApp rp = ws.RVIntrestenApp;
            rp.UpdateAdjustments(cedc.Entity, ref ws);
            set.Calculate("RVIntrestenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.RVIntrestenAppTypes.WPRVIntresten RVIntrestenAppVerschillenAdd(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaRVIntrestenAppVerchilItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            RVIntrestenApp rp = ws.RVIntrestenApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToVerschillen(cedc.Entity, ref ws);
            set.Calculate("RVIntrestenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.RVIntrestenAppTypes.WPRVIntresten RVIntrestenAppVerschillenRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            RVIntrestenApp rp = ws.RVIntrestenApp;
            rp.DeleteVerschillen(cfrdc.RowId, ref ws);
            set.Calculate("RVIntrestenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.RVIntrestenAppTypes.WPRVIntresten RVIntrestenAppVerschillenUpdate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaRVIntrestenAppVerchilItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            RVIntrestenApp rp = ws.RVIntrestenApp;
            rp.UpdateVerschillen(cedc.Entity, ref ws);
            set.Calculate("RVIntrestenApp");
            return rp.Data;
        }

        #endregion

        #endregion



        #region DubieuzeDebiteurenApp

        public EY.com.eBook.RuleEngine.AY2014.DubieuzeDebiteurenAppTypes.DubieuzeDebiteuren DubieuzeDebiteurenAppGetData(CriteriaFileDataContract cfdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfdc.FileId);

            return set._datacontainer.DubieuzeDebiteurenApp.Data;
        }


        public EY.com.eBook.RuleEngine.AY2014.DubieuzeDebiteurenAppTypes.DubieuzeDebiteuren DubieuzeDebiteurenAppCalculate(CriteriaFileDataContract cfdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfdc.FileId);
            set.Calculate("DubieuzeDebiteurenApp");

            return set._datacontainer.DubieuzeDebiteurenApp.Data;
        }

        #region Entity validation


        public List<FileMessageDataContract> DubieuzeDebiteurenAppDubieuzeDebiteurTValidate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaDubieuzeDebiteurenAppDubieuzeDebiteurTDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ws.DubieuzeDebiteurenApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == DubieuzeDebiteurenApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> DubieuzeDebiteurenAppDubieuzeDebiteurWValidate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaDubieuzeDebiteurenAppDubieuzeDebiteurWDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ws.DubieuzeDebiteurenApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == DubieuzeDebiteurenApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> DubieuzeDebiteurenAppDubieuzeDebiteurNValidate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaDubieuzeDebiteurenAppDubieuzeDebiteurNDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ws.DubieuzeDebiteurenApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == DubieuzeDebiteurenApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> DubieuzeDebiteurenAppDubieuzeDebiteurHValidate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaDubieuzeDebiteurenAppDubieuzeDebiteurHDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ws.DubieuzeDebiteurenApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == DubieuzeDebiteurenApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        #endregion



        #region Add/Remove


        public EY.com.eBook.RuleEngine.AY2014.DubieuzeDebiteurenAppTypes.DubieuzeDebiteuren DubieuzeDebiteurenAppToestandEindVorigBoekjaarAdd(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaDubieuzeDebiteurenAppDubieuzeDebiteurTDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            DubieuzeDebiteurenApp rp = ws.DubieuzeDebiteurenApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToToestandEindVorigBoekjaar(cedc.Entity, ref ws);
            set.Calculate("DubieuzeDebiteurenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.DubieuzeDebiteurenAppTypes.DubieuzeDebiteuren DubieuzeDebiteurenAppToestandEindVorigBoekjaarRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            DubieuzeDebiteurenApp rp = ws.DubieuzeDebiteurenApp;
            rp.DeleteToestandEindVorigBoekjaar(cfrdc.RowId, ref ws);
            set.Calculate("DubieuzeDebiteurenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.DubieuzeDebiteurenAppTypes.DubieuzeDebiteuren DubieuzeDebiteurenAppToestandEindVorigBoekjaarUpdate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaDubieuzeDebiteurenAppDubieuzeDebiteurTDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            DubieuzeDebiteurenApp rp = ws.DubieuzeDebiteurenApp;
            rp.UpdateToestandEindVorigBoekjaar(cedc.Entity, ref ws);
            set.Calculate("DubieuzeDebiteurenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.DubieuzeDebiteurenAppTypes.DubieuzeDebiteuren DubieuzeDebiteurenAppWijzigingBestaandeTijdensBoekjaarAdd(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaDubieuzeDebiteurenAppDubieuzeDebiteurWDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            DubieuzeDebiteurenApp rp = ws.DubieuzeDebiteurenApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToWijzigingBestaandeTijdensBoekjaar(cedc.Entity, ref ws);
            set.Calculate("DubieuzeDebiteurenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.DubieuzeDebiteurenAppTypes.DubieuzeDebiteuren DubieuzeDebiteurenAppWijzigingBestaandeTijdensBoekjaarRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            DubieuzeDebiteurenApp rp = ws.DubieuzeDebiteurenApp;
            rp.DeleteWijzigingBestaandeTijdensBoekjaar(cfrdc.RowId, ref ws);
            set.Calculate("DubieuzeDebiteurenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.DubieuzeDebiteurenAppTypes.DubieuzeDebiteuren DubieuzeDebiteurenAppWijzigingBestaandeTijdensBoekjaarUpdate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaDubieuzeDebiteurenAppDubieuzeDebiteurWDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            DubieuzeDebiteurenApp rp = ws.DubieuzeDebiteurenApp;
            rp.UpdateWijzigingBestaandeTijdensBoekjaar(cedc.Entity, ref ws);
            set.Calculate("DubieuzeDebiteurenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.DubieuzeDebiteurenAppTypes.DubieuzeDebiteuren DubieuzeDebiteurenAppNieuweTijdensBoekjaarAdd(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaDubieuzeDebiteurenAppDubieuzeDebiteurNDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            DubieuzeDebiteurenApp rp = ws.DubieuzeDebiteurenApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToNieuweTijdensBoekjaar(cedc.Entity, ref ws);
            set.Calculate("DubieuzeDebiteurenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.DubieuzeDebiteurenAppTypes.DubieuzeDebiteuren DubieuzeDebiteurenAppNieuweTijdensBoekjaarRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            DubieuzeDebiteurenApp rp = ws.DubieuzeDebiteurenApp;
            rp.DeleteNieuweTijdensBoekjaar(cfrdc.RowId, ref ws);
            set.Calculate("DubieuzeDebiteurenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.DubieuzeDebiteurenAppTypes.DubieuzeDebiteuren DubieuzeDebiteurenAppNieuweTijdensBoekjaarUpdate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaDubieuzeDebiteurenAppDubieuzeDebiteurNDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            DubieuzeDebiteurenApp rp = ws.DubieuzeDebiteurenApp;
            rp.UpdateNieuweTijdensBoekjaar(cedc.Entity, ref ws);
            set.Calculate("DubieuzeDebiteurenApp");
            return rp.Data;
        }

        #endregion

        #endregion



        #region VerliesHandelsdebiteurenApp

        public EY.com.eBook.RuleEngine.AY2014.VerliesHandelsdebiteurenAppTypes.VerliesHandelsDebiteuren VerliesHandelsdebiteurenAppGetData(CriteriaFileDataContract cfdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfdc.FileId);

            return set._datacontainer.VerliesHandelsdebiteurenApp.Data;
        }


        public EY.com.eBook.RuleEngine.AY2014.VerliesHandelsdebiteurenAppTypes.VerliesHandelsDebiteuren VerliesHandelsdebiteurenAppCalculate(CriteriaFileDataContract cfdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfdc.FileId);
            set.Calculate("VerliesHandelsdebiteurenApp");

            return set._datacontainer.VerliesHandelsdebiteurenApp.Data;
        }

        #region Entity validation


        public List<FileMessageDataContract> VerliesHandelsdebiteurenAppVerliesHandelsDebiteurValidate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaVerliesHandelsdebiteurenAppVerliesHandelsDebiteurDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ws.VerliesHandelsdebiteurenApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == VerliesHandelsdebiteurenApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        #endregion



        #region Add/Remove


        public EY.com.eBook.RuleEngine.AY2014.VerliesHandelsdebiteurenAppTypes.VerliesHandelsDebiteuren VerliesHandelsdebiteurenAppVerliesHDAdd(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaVerliesHandelsdebiteurenAppVerliesHandelsDebiteurDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            VerliesHandelsdebiteurenApp rp = ws.VerliesHandelsdebiteurenApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToVerliesHD(cedc.Entity, ref ws);
            set.Calculate("VerliesHandelsdebiteurenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.VerliesHandelsdebiteurenAppTypes.VerliesHandelsDebiteuren VerliesHandelsdebiteurenAppVerliesHDRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            VerliesHandelsdebiteurenApp rp = ws.VerliesHandelsdebiteurenApp;
            rp.DeleteVerliesHD(cfrdc.RowId, ref ws);
            set.Calculate("VerliesHandelsdebiteurenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.VerliesHandelsdebiteurenAppTypes.VerliesHandelsDebiteuren VerliesHandelsdebiteurenAppVerliesHDUpdate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaVerliesHandelsdebiteurenAppVerliesHandelsDebiteurDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            VerliesHandelsdebiteurenApp rp = ws.VerliesHandelsdebiteurenApp;
            rp.UpdateVerliesHD(cedc.Entity, ref ws);
            set.Calculate("VerliesHandelsdebiteurenApp");
            return rp.Data;
        }

        #endregion

        #endregion



        #region VoorzieningenApp

        public EY.com.eBook.RuleEngine.AY2014.VoorzieningenAppTypes.Voorzieningen VoorzieningenAppGetData(CriteriaFileDataContract cfdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfdc.FileId);

            return set._datacontainer.VoorzieningenApp.Data;
        }


        public EY.com.eBook.RuleEngine.AY2014.VoorzieningenAppTypes.Voorzieningen VoorzieningenAppCalculate(CriteriaFileDataContract cfdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfdc.FileId);
            set.Calculate("VoorzieningenApp");

            return set._datacontainer.VoorzieningenApp.Data;
        }

        #region Entity validation


        public List<FileMessageDataContract> VoorzieningenAppVoorzieningenTValidate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaVoorzieningenAppVoorzieningenTDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ws.VoorzieningenApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == VoorzieningenApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> VoorzieningenAppVoorzieningenWValidate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaVoorzieningenAppVoorzieningenWDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ws.VoorzieningenApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == VoorzieningenApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> VoorzieningenAppVoorzieningenNValidate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaVoorzieningenAppVoorzieningenNDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ws.VoorzieningenApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == VoorzieningenApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> VoorzieningenAppVoorzieningenHValidate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaVoorzieningenAppVoorzieningenHDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ws.VoorzieningenApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == VoorzieningenApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        #endregion



        #region Add/Remove


        public EY.com.eBook.RuleEngine.AY2014.VoorzieningenAppTypes.Voorzieningen VoorzieningenAppToestandEindVorigBoekjaarAdd(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaVoorzieningenAppVoorzieningenTDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            VoorzieningenApp rp = ws.VoorzieningenApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToToestandEindVorigBoekjaar(cedc.Entity, ref ws);
            set.Calculate("VoorzieningenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.VoorzieningenAppTypes.Voorzieningen VoorzieningenAppToestandEindVorigBoekjaarRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            VoorzieningenApp rp = ws.VoorzieningenApp;
            rp.DeleteToestandEindVorigBoekjaar(cfrdc.RowId, ref ws);
            set.Calculate("VoorzieningenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.VoorzieningenAppTypes.Voorzieningen VoorzieningenAppToestandEindVorigBoekjaarUpdate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaVoorzieningenAppVoorzieningenTDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            VoorzieningenApp rp = ws.VoorzieningenApp;
            rp.UpdateToestandEindVorigBoekjaar(cedc.Entity, ref ws);
            set.Calculate("VoorzieningenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.VoorzieningenAppTypes.Voorzieningen VoorzieningenAppWijzigingBestaandeTijdensBoekjaarAdd(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaVoorzieningenAppVoorzieningenWDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            VoorzieningenApp rp = ws.VoorzieningenApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToWijzigingBestaandeTijdensBoekjaar(cedc.Entity, ref ws);
            set.Calculate("VoorzieningenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.VoorzieningenAppTypes.Voorzieningen VoorzieningenAppWijzigingBestaandeTijdensBoekjaarRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            VoorzieningenApp rp = ws.VoorzieningenApp;
            rp.DeleteWijzigingBestaandeTijdensBoekjaar(cfrdc.RowId, ref ws);
            set.Calculate("VoorzieningenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.VoorzieningenAppTypes.Voorzieningen VoorzieningenAppWijzigingBestaandeTijdensBoekjaarUpdate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaVoorzieningenAppVoorzieningenWDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            VoorzieningenApp rp = ws.VoorzieningenApp;
            rp.UpdateWijzigingBestaandeTijdensBoekjaar(cedc.Entity, ref ws);
            set.Calculate("VoorzieningenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.VoorzieningenAppTypes.Voorzieningen VoorzieningenAppNieuweTijdensBoekjaarAdd(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaVoorzieningenAppVoorzieningenNDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            VoorzieningenApp rp = ws.VoorzieningenApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToNieuweTijdensBoekjaar(cedc.Entity, ref ws);
            set.Calculate("VoorzieningenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.VoorzieningenAppTypes.Voorzieningen VoorzieningenAppNieuweTijdensBoekjaarRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            VoorzieningenApp rp = ws.VoorzieningenApp;
            rp.DeleteNieuweTijdensBoekjaar(cfrdc.RowId, ref ws);
            set.Calculate("VoorzieningenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.VoorzieningenAppTypes.Voorzieningen VoorzieningenAppNieuweTijdensBoekjaarUpdate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaVoorzieningenAppVoorzieningenNDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            VoorzieningenApp rp = ws.VoorzieningenApp;
            rp.UpdateNieuweTijdensBoekjaar(cedc.Entity, ref ws);
            set.Calculate("VoorzieningenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.VoorzieningenAppTypes.Voorzieningen VoorzieningenAppToestandEindHuidigBoekjaarAdd(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaVoorzieningenAppVoorzieningenHDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            VoorzieningenApp rp = ws.VoorzieningenApp;
            
            set.Calculate("VoorzieningenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.VoorzieningenAppTypes.Voorzieningen VoorzieningenAppToestandEindHuidigBoekjaarRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            VoorzieningenApp rp = ws.VoorzieningenApp;
            set.Calculate("VoorzieningenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.VoorzieningenAppTypes.Voorzieningen VoorzieningenAppToestandEindHuidigBoekjaarUpdate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaVoorzieningenAppVoorzieningenHDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            VoorzieningenApp rp = ws.VoorzieningenApp;
            set.Calculate("VoorzieningenApp");
            return rp.Data;
        }

        #endregion

        #endregion



        #region BelastingenApp

        public EY.com.eBook.RuleEngine.AY2014.BelastingenAppTypes.WPBelastingen BelastingenAppGetData(CriteriaFileDataContract cfdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfdc.FileId);

            return set._datacontainer.BelastingenApp.Data;
        }


        public EY.com.eBook.RuleEngine.AY2014.BelastingenAppTypes.WPBelastingen BelastingenAppCalculate(CriteriaFileDataContract cfdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfdc.FileId);
            set.Calculate("BelastingenApp");

            return set._datacontainer.BelastingenApp.Data;
        }

        #region Entity validation


        public List<FileMessageDataContract> BelastingenAppAdjustmentValidate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaBelastingenAppAdjustmentDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ws.BelastingenApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == BelastingenApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> BelastingenAppBelastingValidate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaBelastingenAppBelastingDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ws.BelastingenApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == BelastingenApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> BelastingenAppKostItemValidate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaBelastingenAppKostItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ws.BelastingenApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == BelastingenApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        #endregion



        #region Add/Remove


        public EY.com.eBook.RuleEngine.AY2014.BelastingenAppTypes.WPBelastingen BelastingenAppAdjustmentsAdd(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaBelastingenAppAdjustmentDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            BelastingenApp rp = ws.BelastingenApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToAdjustments(cedc.Entity, ref ws);
            set.Calculate("BelastingenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.BelastingenAppTypes.WPBelastingen BelastingenAppAdjustmentsRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            BelastingenApp rp = ws.BelastingenApp;
            rp.DeleteAdjustments(cfrdc.RowId, ref ws);
            set.Calculate("BelastingenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.BelastingenAppTypes.WPBelastingen BelastingenAppAdjustmentsUpdate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaBelastingenAppAdjustmentDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            BelastingenApp rp = ws.BelastingenApp;
            rp.UpdateAdjustments(cedc.Entity, ref ws);
            set.Calculate("BelastingenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.BelastingenAppTypes.WPBelastingen BelastingenAppBelastingenUpdate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaBelastingenAppBelastingDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            BelastingenApp rp = ws.BelastingenApp;
            rp.UpdateBelastingen(cedc.Entity, ref ws);
            set.Calculate("BelastingenApp");
            return rp.Data;
        }

        #endregion

        #endregion

        #region MeerwaardeAandelenApp
        public EY.com.eBook.RuleEngine.AY2014.MeerwaardeAandelenAppTypes.MeerwaardeAandelen MeerwaardeAandelenAppGetData(CriteriaFileDataContract cfdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfdc.FileId);

            return set._datacontainer.MeerwaardeAandelenApp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.MeerwaardeAandelenAppTypes.MeerwaardeAandelen MeerwaardeAandelenAppCalculate(CriteriaFileDataContract cfdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfdc.FileId);
            set.Calculate("MeerwaardeAandelenApp");

            return set._datacontainer.MeerwaardeAandelenApp.Data;
        }

        #region Entity validation

        public List<FileMessageDataContract> MeerwaardeAandelenAppMeerwaardeAandelenItemValidate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaMeerwaardeAandelenAppMeerwaardeAandelenItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ws.MeerwaardeAandelenApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == MeerwaardeAandelenApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }

        #endregion



        #region Add/Remove


        public EY.com.eBook.RuleEngine.AY2014.MeerwaardeAandelenAppTypes.MeerwaardeAandelen MeerwaardeAandelenAppMeerwaardeAandelenLijstAdd(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaMeerwaardeAandelenAppMeerwaardeAandelenItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            MeerwaardeAandelenApp rp = ws.MeerwaardeAandelenApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToMeerwaardeAandelenLijst(cedc.Entity, ref ws);
            set.Calculate("MeerwaardeAandelenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.MeerwaardeAandelenAppTypes.MeerwaardeAandelen MeerwaardeAandelenAppMeerwaardeAandelenLijstRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            MeerwaardeAandelenApp rp = ws.MeerwaardeAandelenApp;
            rp.DeleteMeerwaardeAandelenLijst(cfrdc.RowId, ref ws);
            set.Calculate("MeerwaardeAandelenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.MeerwaardeAandelenAppTypes.MeerwaardeAandelen MeerwaardeAandelenAppMeerwaardeAandelenLijstUpdate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaMeerwaardeAandelenAppMeerwaardeAandelenItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            MeerwaardeAandelenApp rp = ws.MeerwaardeAandelenApp;
            rp.UpdateMeerwaardeAandelenLijst(cedc.Entity, ref ws);
            set.Calculate("MeerwaardeAandelenApp");
            return rp.Data;
        }

        #endregion

        #endregion

        #region BelasteReservesApp

        public EY.com.eBook.RuleEngine.AY2014.BelasteReservesAppTypes.BelasteReserves BelasteReservesAppGetData(CriteriaFileDataContract cfdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfdc.FileId);

            return set._datacontainer.BelasteReservesApp.Data;
        }


        public EY.com.eBook.RuleEngine.AY2014.BelasteReservesAppTypes.BelasteReserves BelasteReservesAppCalculate(CriteriaFileDataContract cfdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfdc.FileId);
            set.Calculate("BelasteReservesApp");

            return set._datacontainer.BelasteReservesApp.Data;
        }

        #region Entity validation


        public List<FileMessageDataContract> BelasteReservesAppBelasteReservesItemValidate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaBelasteReservesAppBelasteReservesItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ws.BelasteReservesApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == BelasteReservesApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> BelasteReservesAppAanpassingBTReservesItemValidate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaBelasteReservesAppAanpassingBTReservesItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ws.BelasteReservesApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == BelasteReservesApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        #endregion



        #region Add/Remove


        public EY.com.eBook.RuleEngine.AY2014.BelasteReservesAppTypes.BelasteReserves BelasteReservesAppAllBelasteReservesAdd(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaBelasteReservesAppBelasteReservesItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            BelasteReservesApp rp = ws.BelasteReservesApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToAllBelasteReserves(cedc.Entity, ref ws);
            set.Calculate("BelasteReservesApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.BelasteReservesAppTypes.BelasteReserves BelasteReservesAppAllBelasteReservesRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            BelasteReservesApp rp = ws.BelasteReservesApp;
            rp.DeleteAllBelasteReserves(cfrdc.RowId, ref ws);
            set.Calculate("BelasteReservesApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.BelasteReservesAppTypes.BelasteReserves BelasteReservesAppAllBelasteReservesUpdate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaBelasteReservesAppBelasteReservesItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            BelasteReservesApp rp = ws.BelasteReservesApp;
            rp.UpdateAllBelasteReserves(cedc.Entity, ref ws);
            set.Calculate("BelasteReservesApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.BelasteReservesAppTypes.BelasteReserves BelasteReservesAppAanpassingBTReservesAdd(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaBelasteReservesAppAanpassingBTReservesItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            BelasteReservesApp rp = ws.BelasteReservesApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToAanpassingBTReserves(cedc.Entity, ref ws);
            set.Calculate("BelasteReservesApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.BelasteReservesAppTypes.BelasteReserves BelasteReservesAppAanpassingBTReservesRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            BelasteReservesApp rp = ws.BelasteReservesApp;
            rp.DeleteAanpassingBTReserves(cfrdc.RowId, ref ws);
            set.Calculate("BelasteReservesApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.BelasteReservesAppTypes.BelasteReserves BelasteReservesAppAanpassingBTReservesUpdate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaBelasteReservesAppAanpassingBTReservesItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            BelasteReservesApp rp = ws.BelasteReservesApp;
            rp.UpdateAanpassingBTReserves(cedc.Entity, ref ws);
            set.Calculate("BelasteReservesApp");
            return rp.Data;
        }

        #endregion

        #endregion



        #region BelastingVrijeReservesApp

        public EY.com.eBook.RuleEngine.AY2014.BelastingVrijeReservesAppTypes.BelastingVrijeReserves BelastingVrijeReservesAppGetData(CriteriaFileDataContract cfdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfdc.FileId);

            return set._datacontainer.BelastingVrijeReservesApp.Data;
        }


        public EY.com.eBook.RuleEngine.AY2014.BelastingVrijeReservesAppTypes.BelastingVrijeReserves BelastingVrijeReservesAppCalculate(CriteriaFileDataContract cfdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfdc.FileId);
            set.Calculate("BelastingVrijeReservesApp");

            return set._datacontainer.BelastingVrijeReservesApp.Data;
        }

        #region Entity validation


        public List<FileMessageDataContract> BelastingVrijeReservesAppDetailValidate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaBelastingVrijeReservesAppDetailDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ws.BelastingVrijeReservesApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == BelastingVrijeReservesApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        #endregion



        #region Add/Remove



        public EY.com.eBook.RuleEngine.AY2014.BelastingVrijeReservesAppTypes.BelastingVrijeReserves BelastingVrijeReservesAppVrijgesteldeReservesUpdate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaBelastingVrijeReservesAppDetailDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            BelastingVrijeReservesApp rp = ws.BelastingVrijeReservesApp;
            rp.UpdateVrijgesteldeReserves(cedc.Entity, ref ws);
            set.Calculate("BelastingVrijeReservesApp");
            return rp.Data;
        }


        #endregion

        #endregion



        #region BepalingBelastbareBasisApp

        public EY.com.eBook.RuleEngine.AY2014.BepalingBelastbareBasisAppTypes.BepalingBasis BepalingBelastbareBasisAppGetData(CriteriaFileDataContract cfdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfdc.FileId);

            return set._datacontainer.BepalingBelastbareBasisApp.Data;
        }


        public EY.com.eBook.RuleEngine.AY2014.BepalingBelastbareBasisAppTypes.BepalingBasis BepalingBelastbareBasisAppCalculate(CriteriaFileDataContract cfdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfdc.FileId);
            set.Calculate("BepalingBelastbareBasisApp");

            return set._datacontainer.BepalingBelastbareBasisApp.Data;
        }

        #region Entity validation


        public List<FileMessageDataContract> BepalingBelastbareBasisAppGeneralValidate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaBepalingBelastbareBasisAppGeneralDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ws.BepalingBelastbareBasisApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == BepalingBelastbareBasisApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> BepalingBelastbareBasisAppBelasteReservesItemValidate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaBepalingBelastbareBasisAppBelasteReservesItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ws.BepalingBelastbareBasisApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == BepalingBelastbareBasisApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> BepalingBelastbareBasisAppVerworpenUitgavenItemValidate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaBepalingBelastbareBasisAppVerworpenUitgavenItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ws.BepalingBelastbareBasisApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == BepalingBelastbareBasisApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> BepalingBelastbareBasisAppBelastbareBestZonderAftrekItemValidate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaBepalingBelastbareBasisAppBelastbareBestZonderAftrekItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ws.BepalingBelastbareBasisApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == BepalingBelastbareBasisApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> BepalingBelastbareBasisAppOpdelingNaarOorsprongItemValidate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaBepalingBelastbareBasisAppOpdelingNaarOorsprongItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ws.BepalingBelastbareBasisApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == BepalingBelastbareBasisApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> BepalingBelastbareBasisAppFiscaleAftrekItemValidate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaBepalingBelastbareBasisAppFiscaleAftrekItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ws.BepalingBelastbareBasisApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == BepalingBelastbareBasisApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        #endregion



        #region Add/Remove



        public EY.com.eBook.RuleEngine.AY2014.BepalingBelastbareBasisAppTypes.BepalingBasis BepalingBelastbareBasisAppOverzichtUpdate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaBepalingBelastbareBasisAppGeneralDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            BepalingBelastbareBasisApp rp = ws.BepalingBelastbareBasisApp;
            rp.UpdateOverzicht(cedc.Entity, ref ws);
            set.Calculate("BepalingBelastbareBasisApp");
            return rp.Data;
        }



        public EY.com.eBook.RuleEngine.AY2014.BepalingBelastbareBasisAppTypes.BepalingBasis BepalingBelastbareBasisAppBelasteReservesUpdate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaBepalingBelastbareBasisAppBelasteReservesItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            BepalingBelastbareBasisApp rp = ws.BepalingBelastbareBasisApp;
            rp.UpdateBelasteReserves(cedc.Entity, ref ws);
            set.Calculate("BepalingBelastbareBasisApp");
            return rp.Data;
        }



        public EY.com.eBook.RuleEngine.AY2014.BepalingBelastbareBasisAppTypes.BepalingBasis BepalingBelastbareBasisAppVerworpenUitgavenUpdate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaBepalingBelastbareBasisAppVerworpenUitgavenItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            BepalingBelastbareBasisApp rp = ws.BepalingBelastbareBasisApp;
            rp.UpdateVerworpenUitgaven(cedc.Entity, ref ws);
            set.Calculate("BepalingBelastbareBasisApp");
            return rp.Data;
        }



        public EY.com.eBook.RuleEngine.AY2014.BepalingBelastbareBasisAppTypes.BepalingBasis BepalingBelastbareBasisAppBelastbareBestZonderAftrekUpdate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaBepalingBelastbareBasisAppBelastbareBestZonderAftrekItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            BepalingBelastbareBasisApp rp = ws.BepalingBelastbareBasisApp;
            rp.UpdateBelastbareBestZonderAftrek(cedc.Entity, ref ws);
            set.Calculate("BepalingBelastbareBasisApp");
            return rp.Data;
        }



        public EY.com.eBook.RuleEngine.AY2014.BepalingBelastbareBasisAppTypes.BepalingBasis BepalingBelastbareBasisAppOpdelingNaarOorsprongUpdate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaBepalingBelastbareBasisAppOpdelingNaarOorsprongItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            BepalingBelastbareBasisApp rp = ws.BepalingBelastbareBasisApp;
            rp.UpdateOpdelingNaarOorsprong(cedc.Entity, ref ws);
            set.Calculate("BepalingBelastbareBasisApp");
            return rp.Data;
        }



        public EY.com.eBook.RuleEngine.AY2014.BepalingBelastbareBasisAppTypes.BepalingBasis BepalingBelastbareBasisAppFiscaleAftrekUpdate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaBepalingBelastbareBasisAppFiscaleAftrekItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            BepalingBelastbareBasisApp rp = ws.BepalingBelastbareBasisApp;
            rp.UpdateFiscaleAftrek(cedc.Entity, ref ws);
            set.Calculate("BepalingBelastbareBasisApp");
            return rp.Data;
        }


        #endregion

        #endregion



        #region DBIApp

        public EY.com.eBook.RuleEngine.AY2014.DBIAppTypes.DBIWorksheet DBIAppGetData(CriteriaFileDataContract cfdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfdc.FileId);

            return set._datacontainer.DBIApp.Data;
        }


        public EY.com.eBook.RuleEngine.AY2014.DBIAppTypes.DBIWorksheet DBIAppCalculate(CriteriaFileDataContract cfdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfdc.FileId);
            set.Calculate("DBIApp");

            return set._datacontainer.DBIApp.Data;
        }

        #region Entity validation


        public List<FileMessageDataContract> DBIAppParticipatieItemValidate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaDBIAppParticipatieItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ws.DBIApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == DBIApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> DBIAppDividendItemValidate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaDBIAppDividendItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ws.DBIApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == DBIApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> DBIAppDbiEntityValidate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaDBIAppDbiEntityDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ws.DBIApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == DBIApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> DBIAppHistoryItemValidate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaDBIAppHistoryItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ws.DBIApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == DBIApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        #endregion



        #region Add/Remove


        public EY.com.eBook.RuleEngine.AY2014.DBIAppTypes.DBIWorksheet DBIAppParticipationsAdd(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaDBIAppParticipatieItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            DBIApp rp = ws.DBIApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToParticipations(cedc.Entity, ref ws);
            set.Calculate("DBIApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.DBIAppTypes.DBIWorksheet DBIAppParticipationsRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            DBIApp rp = ws.DBIApp;
            rp.DeleteParticipations(cfrdc.RowId, ref ws);
            set.Calculate("DBIApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.DBIAppTypes.DBIWorksheet DBIAppParticipationsUpdate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaDBIAppParticipatieItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            DBIApp rp = ws.DBIApp;
            rp.UpdateParticipations(cedc.Entity, ref ws);
            set.Calculate("DBIApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.DBIAppTypes.DBIWorksheet DBIAppDividendsAdd(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaDBIAppDividendItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            DBIApp rp = ws.DBIApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToDividends(cedc.Entity, ref ws);
            set.Calculate("DBIApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.DBIAppTypes.DBIWorksheet DBIAppDividendsRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            DBIApp rp = ws.DBIApp;
            rp.DeleteDividends(cfrdc.RowId, ref ws);
            set.Calculate("DBIApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.DBIAppTypes.DBIWorksheet DBIAppDividendsUpdate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaDBIAppDividendItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            DBIApp rp = ws.DBIApp;
            rp.UpdateDividends(cedc.Entity, ref ws);
            set.Calculate("DBIApp");
            return rp.Data;
        }


        public EY.com.eBook.RuleEngine.AY2014.DBIAppTypes.DBIWorksheet DBIAppDBIUpdate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaDBIAppDbiEntityDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            DBIApp rp = ws.DBIApp;
            rp.UpdateDBI(cedc.Entity, ref ws);
            set.Calculate("DBIApp");
            return rp.Data;
        }


        public EY.com.eBook.RuleEngine.AY2014.DBIAppTypes.DBIWorksheet DBIAppHistoriekAdd(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaDBIAppHistoryItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            DBIApp rp = ws.DBIApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToHistoriek(cedc.Entity, ref ws);
            set.Calculate("DBIApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.DBIAppTypes.DBIWorksheet DBIAppHistoriekRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            DBIApp rp = ws.DBIApp;
            rp.DeleteHistoriek(cfrdc.RowId, ref ws);
            set.Calculate("DBIApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.DBIAppTypes.DBIWorksheet DBIAppHistoriekUpdate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaDBIAppHistoryItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            DBIApp rp = ws.DBIApp;
            rp.UpdateHistoriek(cedc.Entity, ref ws);
            set.Calculate("DBIApp");
            return rp.Data;
        }

        #endregion

        #endregion



        #region RisicoKapitaalApp

        public EY.com.eBook.RuleEngine.AY2014.RisicoKapitaalAppTypes.WPRisicokapitaal RisicoKapitaalAppGetData(CriteriaFileDataContract cfdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfdc.FileId);

            return set._datacontainer.RisicoKapitaalApp.Data;
        }


        public EY.com.eBook.RuleEngine.AY2014.RisicoKapitaalAppTypes.WPRisicokapitaal RisicoKapitaalAppCalculate(CriteriaFileDataContract cfdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfdc.FileId);
            set.Calculate("RisicoKapitaalApp");

            return set._datacontainer.RisicoKapitaalApp.Data;
        }

        #region Entity validation


        public List<FileMessageDataContract> RisicoKapitaalAppFicheItemValidate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaRisicoKapitaalAppFicheItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ws.RisicoKapitaalApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == RisicoKapitaalApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> RisicoKapitaalAppEigenVermogenValidate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaRisicoKapitaalAppEigenVermogenDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ws.RisicoKapitaalApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == RisicoKapitaalApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> RisicoKapitaalAppBestanddeelValidate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaRisicoKapitaalAppBestanddeelDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ws.RisicoKapitaalApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == RisicoKapitaalApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> RisicoKapitaalAppWijzigingItemValidate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaRisicoKapitaalAppWijzigingItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ws.RisicoKapitaalApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == RisicoKapitaalApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> RisicoKapitaalAppTariefItemValidate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaRisicoKapitaalAppTariefItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ws.RisicoKapitaalApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == RisicoKapitaalApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }

        public List<FileMessageDataContract> RisicoKapitaalAppVerminderingenItemValidate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaRisicoKapitaalAppVerminderingenItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ws.RisicoKapitaalApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == RisicoKapitaalApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }


        /*
        * new NID
        public List<FileMessageDataContract> RisicoKapitaalAppHistoryItemValidate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaRisicoKapitaalAppHistoryItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ws.RisicoKapitaalApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == RisicoKapitaalApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> RisicoKapitaalAppHistoriekAanpassingItemValidate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaRisicoKapitaalAppHistoriekAanpassingItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ws.RisicoKapitaalApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == RisicoKapitaalApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }
        */


        #endregion



        #region Add/Remove



        public EY.com.eBook.RuleEngine.AY2014.RisicoKapitaalAppTypes.WPRisicokapitaal RisicoKapitaalAppFicheUpdate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaRisicoKapitaalAppFicheItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            RisicoKapitaalApp rp = ws.RisicoKapitaalApp;
            rp.UpdateFiche(cedc.Entity, ref ws);
            set.Calculate("RisicoKapitaalApp");
            return rp.Data;
        }


        public EY.com.eBook.RuleEngine.AY2014.RisicoKapitaalAppTypes.WPRisicokapitaal RisicoKapitaalAppEigenVermogensAdd(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaRisicoKapitaalAppEigenVermogenDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            RisicoKapitaalApp rp = ws.RisicoKapitaalApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToEigenVermogens(cedc.Entity, ref ws);
            set.Calculate("RisicoKapitaalApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.RisicoKapitaalAppTypes.WPRisicokapitaal RisicoKapitaalAppEigenVermogensRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            RisicoKapitaalApp rp = ws.RisicoKapitaalApp;
            rp.DeleteEigenVermogens(cfrdc.RowId, ref ws);
            set.Calculate("RisicoKapitaalApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.RisicoKapitaalAppTypes.WPRisicokapitaal RisicoKapitaalAppEigenVermogensUpdate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaRisicoKapitaalAppEigenVermogenDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            RisicoKapitaalApp rp = ws.RisicoKapitaalApp;
            rp.UpdateEigenVermogens(cedc.Entity, ref ws);
            set.Calculate("RisicoKapitaalApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.RisicoKapitaalAppTypes.WPRisicokapitaal RisicoKapitaalAppBestanddelenAdd(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaRisicoKapitaalAppBestanddeelDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            RisicoKapitaalApp rp = ws.RisicoKapitaalApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToBestanddelen(cedc.Entity, ref ws);
            set.Calculate("RisicoKapitaalApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.RisicoKapitaalAppTypes.WPRisicokapitaal RisicoKapitaalAppBestanddelenRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            RisicoKapitaalApp rp = ws.RisicoKapitaalApp;
            rp.DeleteBestanddelen(cfrdc.RowId, ref ws);
            set.Calculate("RisicoKapitaalApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.RisicoKapitaalAppTypes.WPRisicokapitaal RisicoKapitaalAppBestanddelenUpdate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaRisicoKapitaalAppBestanddeelDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            RisicoKapitaalApp rp = ws.RisicoKapitaalApp;
            rp.UpdateBestanddelen(cedc.Entity, ref ws);
            set.Calculate("RisicoKapitaalApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.RisicoKapitaalAppTypes.WPRisicokapitaal RisicoKapitaalAppWijzigingenAdd(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaRisicoKapitaalAppWijzigingItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            RisicoKapitaalApp rp = ws.RisicoKapitaalApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToWijzigingen(cedc.Entity, ref ws);
            set.Calculate("RisicoKapitaalApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.RisicoKapitaalAppTypes.WPRisicokapitaal RisicoKapitaalAppWijzigingenRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            RisicoKapitaalApp rp = ws.RisicoKapitaalApp;
            rp.DeleteWijzigingen(cfrdc.RowId, ref ws);
            set.Calculate("RisicoKapitaalApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.RisicoKapitaalAppTypes.WPRisicokapitaal RisicoKapitaalAppWijzigingenUpdate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaRisicoKapitaalAppWijzigingItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            RisicoKapitaalApp rp = ws.RisicoKapitaalApp;
            rp.UpdateWijzigingen(cedc.Entity, ref ws);
            set.Calculate("RisicoKapitaalApp");
            return rp.Data;
        }


        public EY.com.eBook.RuleEngine.AY2014.RisicoKapitaalAppTypes.WPRisicokapitaal RisicoKapitaalAppTariefUpdate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaRisicoKapitaalAppTariefItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            RisicoKapitaalApp rp = ws.RisicoKapitaalApp;
            rp.UpdateTarief(cedc.Entity, ref ws);
            set.Calculate("RisicoKapitaalApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.RisicoKapitaalAppTypes.WPRisicokapitaal RisicoKapitaalAppVerminderingenUpdate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaRisicoKapitaalAppVerminderingenItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            RisicoKapitaalApp rp = ws.RisicoKapitaalApp;
            rp.UpdateVerminderingen(cedc.Entity, ref ws);
            set.Calculate("RisicoKapitaalApp");
            return rp.Data;
        }

        /*
        * new NID
        public EY.com.eBook.RuleEngine.AY2014.RisicoKapitaalAppTypes.WPRisicokapitaal RisicoKapitaalAppHistoriekAdd(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaRisicoKapitaalAppHistoryItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            RisicoKapitaalApp rp = ws.RisicoKapitaalApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToHistoriek(cedc.Entity, ref ws);
            set.Calculate("RisicoKapitaalApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.RisicoKapitaalAppTypes.WPRisicokapitaal RisicoKapitaalAppHistoriekRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            RisicoKapitaalApp rp = ws.RisicoKapitaalApp;
            rp.DeleteHistoriek(cfrdc.RowId, ref ws);
            set.Calculate("RisicoKapitaalApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.RisicoKapitaalAppTypes.WPRisicokapitaal RisicoKapitaalAppHistoriekUpdate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaRisicoKapitaalAppHistoryItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            RisicoKapitaalApp rp = ws.RisicoKapitaalApp;
            rp.UpdateHistoriek(cedc.Entity, ref ws);
            set.Calculate("RisicoKapitaalApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.RisicoKapitaalAppTypes.WPRisicokapitaal RisicoKapitaalAppAanpassingenHistoriekAdd(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaRisicoKapitaalAppHistoriekAanpassingItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            RisicoKapitaalApp rp = ws.RisicoKapitaalApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToAanpassingenHistoriek(cedc.Entity, ref ws);
            set.Calculate("RisicoKapitaalApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.RisicoKapitaalAppTypes.WPRisicokapitaal RisicoKapitaalAppAanpassingenHistoriekRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            RisicoKapitaalApp rp = ws.RisicoKapitaalApp;
            rp.DeleteAanpassingenHistoriek(cfrdc.RowId, ref ws);
            set.Calculate("RisicoKapitaalApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.RisicoKapitaalAppTypes.WPRisicokapitaal RisicoKapitaalAppAanpassingenHistoriekUpdate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaRisicoKapitaalAppHistoriekAanpassingItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            RisicoKapitaalApp rp = ws.RisicoKapitaalApp;
            rp.UpdateAanpassingenHistoriek(cedc.Entity, ref ws);
            set.Calculate("RisicoKapitaalApp");
            return rp.Data;
        }
        */
        #endregion

        #endregion


        #region RisicoKapitaalHistoriekApp

        public EY.com.eBook.RuleEngine.AY2014.RisicoKapitaalHistoriekAppTypes.WPRisicokapitaalHistoriek RisicoKapitaalHistoriekAppGetData(CriteriaFileDataContract cfdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfdc.FileId);

            return set._datacontainer.RisicoKapitaalHistoriekApp.Data;
        }


        public EY.com.eBook.RuleEngine.AY2014.RisicoKapitaalHistoriekAppTypes.WPRisicokapitaalHistoriek RisicoKapitaalHistoriekAppCalculate(CriteriaFileDataContract cfdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfdc.FileId);
            set.Calculate("RisicoKapitaalHistoriekApp");

            return set._datacontainer.RisicoKapitaalHistoriekApp.Data;
        }

        #region Entity validation


        public List<FileMessageDataContract> RisicoKapitaalHistoriekAppFicheItemValidate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaRisicoKapitaalHistoriekAppFicheItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ws.RisicoKapitaalHistoriekApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == RisicoKapitaalHistoriekApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }


        /*
        public List<FileMessageDataContract> RisicoKapitaalHistoriekAppEigenVermogenValidate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaRisicoKapitaalHistoriekAppEigenVermogenDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ws.RisicoKapitaalHistoriekApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == RisicoKapitaalHistoriekApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> RisicoKapitaalHistoriekAppBestanddeelValidate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaRisicoKapitaalHistoriekAppBestanddeelDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ws.RisicoKapitaalHistoriekApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == RisicoKapitaalHistoriekApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> RisicoKapitaalHistoriekAppWijzigingItemValidate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaRisicoKapitaalHistoriekAppWijzigingItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ws.RisicoKapitaalHistoriekApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == RisicoKapitaalHistoriekApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> RisicoKapitaalHistoriekAppTariefItemValidate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaRisicoKapitaalHistoriekAppTariefItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ws.RisicoKapitaalHistoriekApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == RisicoKapitaalHistoriekApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }

        */

        public List<FileMessageDataContract> RisicoKapitaalHistoriekAppHistoryItemValidate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaRisicoKapitaalHistoriekAppHistoryItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ws.RisicoKapitaalHistoriekApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == RisicoKapitaalHistoriekApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> RisicoKapitaalHistoriekAppHistoriekAanpassingItemValidate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaRisicoKapitaalHistoriekAppHistoriekAanpassingItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ws.RisicoKapitaalHistoriekApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == RisicoKapitaalHistoriekApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        #endregion



        #region Add/Remove



        public EY.com.eBook.RuleEngine.AY2014.RisicoKapitaalHistoriekAppTypes.WPRisicokapitaalHistoriek RisicoKapitaalHistoriekAppFicheUpdate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaRisicoKapitaalHistoriekAppFicheItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            RisicoKapitaalHistoriekApp rp = ws.RisicoKapitaalHistoriekApp;
            rp.UpdateFiche(cedc.Entity, ref ws);
            set.Calculate("RisicoKapitaalHistoriekApp");
            return rp.Data;
        }

        /*
        public EY.com.eBook.RuleEngine.AY2014.RisicoKapitaalHistoriekAppTypes.WPRisicokapitaalHistoriek RisicoKapitaalHistoriekAppEigenVermogensAdd(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaRisicoKapitaalHistoriekAppEigenVermogenDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            RisicoKapitaalHistoriekApp rp = ws.RisicoKapitaalHistoriekApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToEigenVermogens(cedc.Entity, ref ws);
            set.Calculate("RisicoKapitaalHistoriekApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.RisicoKapitaalHistoriekAppTypes.WPRisicokapitaalHistoriek RisicoKapitaalHistoriekAppEigenVermogensRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            RisicoKapitaalHistoriekApp rp = ws.RisicoKapitaalHistoriekApp;
            rp.DeleteEigenVermogens(cfrdc.RowId, ref ws);
            set.Calculate("RisicoKapitaalHistoriekApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.RisicoKapitaalHistoriekAppTypes.WPRisicokapitaalHistoriek RisicoKapitaalHistoriekAppEigenVermogensUpdate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaRisicoKapitaalHistoriekAppEigenVermogenDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            RisicoKapitaalHistoriekApp rp = ws.RisicoKapitaalHistoriekApp;
            rp.UpdateEigenVermogens(cedc.Entity, ref ws);
            set.Calculate("RisicoKapitaalHistoriekApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.RisicoKapitaalHistoriekAppTypes.WPRisicokapitaalHistoriek RisicoKapitaalHistoriekAppBestanddelenAdd(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaRisicoKapitaalHistoriekAppBestanddeelDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            RisicoKapitaalHistoriekApp rp = ws.RisicoKapitaalHistoriekApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToBestanddelen(cedc.Entity, ref ws);
            set.Calculate("RisicoKapitaalHistoriekApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.RisicoKapitaalHistoriekAppTypes.WPRisicokapitaalHistoriek RisicoKapitaalHistoriekAppBestanddelenRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            RisicoKapitaalHistoriekApp rp = ws.RisicoKapitaalHistoriekApp;
            rp.DeleteBestanddelen(cfrdc.RowId, ref ws);
            set.Calculate("RisicoKapitaalHistoriekApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.RisicoKapitaalHistoriekAppTypes.WPRisicokapitaalHistoriek RisicoKapitaalHistoriekAppBestanddelenUpdate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaRisicoKapitaalHistoriekAppBestanddeelDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            RisicoKapitaalHistoriekApp rp = ws.RisicoKapitaalHistoriekApp;
            rp.UpdateBestanddelen(cedc.Entity, ref ws);
            set.Calculate("RisicoKapitaalHistoriekApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.RisicoKapitaalHistoriekAppTypes.WPRisicokapitaalHistoriek RisicoKapitaalHistoriekAppWijzigingenAdd(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaRisicoKapitaalHistoriekAppWijzigingItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            RisicoKapitaalHistoriekApp rp = ws.RisicoKapitaalHistoriekApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToWijzigingen(cedc.Entity, ref ws);
            set.Calculate("RisicoKapitaalHistoriekApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.RisicoKapitaalHistoriekAppTypes.WPRisicokapitaalHistoriek RisicoKapitaalHistoriekAppWijzigingenRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            RisicoKapitaalHistoriekApp rp = ws.RisicoKapitaalHistoriekApp;
            rp.DeleteWijzigingen(cfrdc.RowId, ref ws);
            set.Calculate("RisicoKapitaalHistoriekApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.RisicoKapitaalHistoriekAppTypes.WPRisicokapitaalHistoriek RisicoKapitaalHistoriekAppWijzigingenUpdate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaRisicoKapitaalHistoriekAppWijzigingItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            RisicoKapitaalHistoriekApp rp = ws.RisicoKapitaalHistoriekApp;
            rp.UpdateWijzigingen(cedc.Entity, ref ws);
            set.Calculate("RisicoKapitaalHistoriekApp");
            return rp.Data;
        }


        public EY.com.eBook.RuleEngine.AY2014.RisicoKapitaalHistoriekAppTypes.WPRisicokapitaalHistoriek RisicoKapitaalHistoriekAppTariefUpdate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaRisicoKapitaalHistoriekAppTariefItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            RisicoKapitaalHistoriekApp rp = ws.RisicoKapitaalHistoriekApp;
            rp.UpdateTarief(cedc.Entity, ref ws);
            set.Calculate("RisicoKapitaalHistoriekApp");
            return rp.Data;
        }

        */
        public EY.com.eBook.RuleEngine.AY2014.RisicoKapitaalHistoriekAppTypes.WPRisicokapitaalHistoriek RisicoKapitaalHistoriekAppHistoriekAdd(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaRisicoKapitaalHistoriekAppHistoryItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            RisicoKapitaalHistoriekApp rp = ws.RisicoKapitaalHistoriekApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToHistoriek(cedc.Entity, ref ws);
            set.Calculate("RisicoKapitaalHistoriekApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.RisicoKapitaalHistoriekAppTypes.WPRisicokapitaalHistoriek RisicoKapitaalHistoriekAppHistoriekRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            RisicoKapitaalHistoriekApp rp = ws.RisicoKapitaalHistoriekApp;
            rp.DeleteHistoriek(cfrdc.RowId, ref ws);
            set.Calculate("RisicoKapitaalHistoriekApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.RisicoKapitaalHistoriekAppTypes.WPRisicokapitaalHistoriek RisicoKapitaalHistoriekAppHistoriekUpdate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaRisicoKapitaalHistoriekAppHistoryItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            RisicoKapitaalHistoriekApp rp = ws.RisicoKapitaalHistoriekApp;
            rp.UpdateHistoriek(cedc.Entity, ref ws);
            set.Calculate("RisicoKapitaalHistoriekApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.RisicoKapitaalHistoriekAppTypes.WPRisicokapitaalHistoriek RisicoKapitaalHistoriekAppAanpassingenHistoriekAdd(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaRisicoKapitaalHistoriekAppHistoriekAanpassingItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            RisicoKapitaalHistoriekApp rp = ws.RisicoKapitaalHistoriekApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToAanpassingenHistoriek(cedc.Entity, ref ws);
            set.Calculate("RisicoKapitaalHistoriekApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.RisicoKapitaalHistoriekAppTypes.WPRisicokapitaalHistoriek RisicoKapitaalHistoriekAppAanpassingenHistoriekRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            RisicoKapitaalHistoriekApp rp = ws.RisicoKapitaalHistoriekApp;
            rp.DeleteAanpassingenHistoriek(cfrdc.RowId, ref ws);
            set.Calculate("RisicoKapitaalHistoriekApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.RisicoKapitaalHistoriekAppTypes.WPRisicokapitaalHistoriek RisicoKapitaalHistoriekAppAanpassingenHistoriekUpdate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaRisicoKapitaalHistoriekAppHistoriekAanpassingItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            RisicoKapitaalHistoriekApp rp = ws.RisicoKapitaalHistoriekApp;
            rp.UpdateAanpassingenHistoriek(cedc.Entity, ref ws);
            set.Calculate("RisicoKapitaalHistoriekApp");
            return rp.Data;
        }

        #endregion

        #endregion


        #region TransferTaxCreditsApp

        public EY.com.eBook.RuleEngine.AY2014.TransferTaxCreditsAppTypes.TransferTaxCredits TransferTaxCreditsAppGetData(CriteriaFileDataContract cfdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfdc.FileId);

            return set._datacontainer.TransferTaxCreditsApp.Data;
        }


        public EY.com.eBook.RuleEngine.AY2014.TransferTaxCreditsAppTypes.TransferTaxCredits TransferTaxCreditsAppCalculate(CriteriaFileDataContract cfdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfdc.FileId);
            set.Calculate("TransferTaxCreditsApp");

            return set._datacontainer.TransferTaxCreditsApp.Data;
        }

        #region Entity validation


        public List<FileMessageDataContract> TransferTaxCreditsAppFiscaalVerliesValidate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaTransferTaxCreditsAppFiscaalVerliesDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ws.TransferTaxCreditsApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == TransferTaxCreditsApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }


        /*
        public List<FileMessageDataContract> TransferTaxCreditsAppNIDItemValidate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaTransferTaxCreditsAppNIDItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ws.TransferTaxCreditsApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == TransferTaxCreditsApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }
        */


        public List<FileMessageDataContract> TransferTaxCreditsAppDBIItemValidate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaTransferTaxCreditsAppDBIItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ws.TransferTaxCreditsApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == TransferTaxCreditsApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> TransferTaxCreditsAppInvestItemValidate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaTransferTaxCreditsAppInvestItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ws.TransferTaxCreditsApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == TransferTaxCreditsApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> TransferTaxCreditsAppBelastingkredietItemValidate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaTransferTaxCreditsAppBelastingkredietItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ws.TransferTaxCreditsApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == TransferTaxCreditsApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> TransferTaxCreditsAppAanpassingItemValidate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaTransferTaxCreditsAppAanpassingItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ws.TransferTaxCreditsApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == TransferTaxCreditsApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> TransferTaxCreditsAppAanwendingValidate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaTransferTaxCreditsAppAanwendingDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ws.TransferTaxCreditsApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == TransferTaxCreditsApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        #endregion



        #region Add/Remove


        public EY.com.eBook.RuleEngine.AY2014.TransferTaxCreditsAppTypes.TransferTaxCredits TransferTaxCreditsAppFiscaleVerliezenAdd(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaTransferTaxCreditsAppFiscaalVerliesDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            TransferTaxCreditsApp rp = ws.TransferTaxCreditsApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToFiscaleVerliezen(cedc.Entity, ref ws);
            set.Calculate("TransferTaxCreditsApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.TransferTaxCreditsAppTypes.TransferTaxCredits TransferTaxCreditsAppFiscaleVerliezenRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            TransferTaxCreditsApp rp = ws.TransferTaxCreditsApp;
            rp.DeleteFiscaleVerliezen(cfrdc.RowId, ref ws);
            set.Calculate("TransferTaxCreditsApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.TransferTaxCreditsAppTypes.TransferTaxCredits TransferTaxCreditsAppFiscaleVerliezenUpdate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaTransferTaxCreditsAppFiscaalVerliesDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            TransferTaxCreditsApp rp = ws.TransferTaxCreditsApp;
            rp.UpdateFiscaleVerliezen(cedc.Entity, ref ws);
            set.Calculate("TransferTaxCreditsApp");
            return rp.Data;
        }


        public EY.com.eBook.RuleEngine.AY2014.TransferTaxCreditsAppTypes.TransferTaxCredits TransferTaxCreditsAppDBIUpdate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaTransferTaxCreditsAppDBIItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            TransferTaxCreditsApp rp = ws.TransferTaxCreditsApp;
            rp.UpdateDBI(cedc.Entity, ref ws);
            set.Calculate("TransferTaxCreditsApp");
            return rp.Data;
        }


        public EY.com.eBook.RuleEngine.AY2014.TransferTaxCreditsAppTypes.TransferTaxCredits TransferTaxCreditsAppInvestAdd(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaTransferTaxCreditsAppInvestItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            TransferTaxCreditsApp rp = ws.TransferTaxCreditsApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToInvest(cedc.Entity, ref ws);
            set.Calculate("TransferTaxCreditsApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.TransferTaxCreditsAppTypes.TransferTaxCredits TransferTaxCreditsAppInvestRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            TransferTaxCreditsApp rp = ws.TransferTaxCreditsApp;
            rp.DeleteInvest(cfrdc.RowId, ref ws);
            set.Calculate("TransferTaxCreditsApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.TransferTaxCreditsAppTypes.TransferTaxCredits TransferTaxCreditsAppInvestUpdate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaTransferTaxCreditsAppInvestItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            TransferTaxCreditsApp rp = ws.TransferTaxCreditsApp;
            rp.UpdateInvest(cedc.Entity, ref ws);
            set.Calculate("TransferTaxCreditsApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.TransferTaxCreditsAppTypes.TransferTaxCredits TransferTaxCreditsAppBelastingkredietAdd(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaTransferTaxCreditsAppBelastingkredietItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            TransferTaxCreditsApp rp = ws.TransferTaxCreditsApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToBelastingkrediet(cedc.Entity, ref ws);
            set.Calculate("TransferTaxCreditsApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.TransferTaxCreditsAppTypes.TransferTaxCredits TransferTaxCreditsAppBelastingkredietRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            TransferTaxCreditsApp rp = ws.TransferTaxCreditsApp;
            rp.DeleteBelastingkrediet(cfrdc.RowId, ref ws);
            set.Calculate("TransferTaxCreditsApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.TransferTaxCreditsAppTypes.TransferTaxCredits TransferTaxCreditsAppBelastingkredietUpdate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaTransferTaxCreditsAppBelastingkredietItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            TransferTaxCreditsApp rp = ws.TransferTaxCreditsApp;
            rp.UpdateBelastingkrediet(cedc.Entity, ref ws);
            set.Calculate("TransferTaxCreditsApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.TransferTaxCreditsAppTypes.TransferTaxCredits TransferTaxCreditsAppAanpassingenAdd(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaTransferTaxCreditsAppAanpassingItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            TransferTaxCreditsApp rp = ws.TransferTaxCreditsApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToAanpassingen(cedc.Entity, ref ws);
            set.Calculate("TransferTaxCreditsApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.TransferTaxCreditsAppTypes.TransferTaxCredits TransferTaxCreditsAppAanpassingenRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            TransferTaxCreditsApp rp = ws.TransferTaxCreditsApp;
            rp.DeleteAanpassingen(cfrdc.RowId, ref ws);
            set.Calculate("TransferTaxCreditsApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.TransferTaxCreditsAppTypes.TransferTaxCredits TransferTaxCreditsAppAanpassingenUpdate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaTransferTaxCreditsAppAanpassingItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            TransferTaxCreditsApp rp = ws.TransferTaxCreditsApp;
            rp.UpdateAanpassingen(cedc.Entity, ref ws);
            set.Calculate("TransferTaxCreditsApp");
            return rp.Data;
        }


        public EY.com.eBook.RuleEngine.AY2014.TransferTaxCreditsAppTypes.TransferTaxCredits TransferTaxCreditsAppAanwendingenUpdate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaTransferTaxCreditsAppAanwendingDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            TransferTaxCreditsApp rp = ws.TransferTaxCreditsApp;
            rp.UpdateAanwendingen(cedc.Entity, ref ws);
            set.Calculate("TransferTaxCreditsApp");
            return rp.Data;
        }


        #endregion

        #endregion



        #region BerekeningVenBApp

        public EY.com.eBook.RuleEngine.AY2014.BerekeningVenBAppTypes.BerekeningVenB BerekeningVenBAppGetData(CriteriaFileDataContract cfdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfdc.FileId);

            return set._datacontainer.BerekeningVenBApp.Data;
        }


        public EY.com.eBook.RuleEngine.AY2014.BerekeningVenBAppTypes.BerekeningVenB BerekeningVenBAppCalculate(CriteriaFileDataContract cfdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfdc.FileId);
            set.Calculate("BerekeningVenBApp");

            return set._datacontainer.BerekeningVenBApp.Data;
        }

        #region Entity validation


        public List<FileMessageDataContract> BerekeningVenBAppGeneralValidate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaBerekeningVenBAppGeneralDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ws.BerekeningVenBApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == BerekeningVenBApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> BerekeningVenBAppBelasteReservesItemValidate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaBerekeningVenBAppBelasteReservesItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ws.BerekeningVenBApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == BerekeningVenBApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> BerekeningVenBAppVerworpenUitgavenItemValidate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaBerekeningVenBAppVerworpenUitgavenItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ws.BerekeningVenBApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == BerekeningVenBApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }

        public List<FileMessageDataContract> BerekeningVenBAppVerrekenbareVoorheffingEntityValidate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaBerekeningVenBAppVerrekenbareVoorheffingEntityDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ws.BerekeningVenBApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == AangifteVenBApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }

        public List<FileMessageDataContract> BerekeningVenBAppBelastbareBestZonderAftrekItemValidate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaBerekeningVenBAppBelastbareBestZonderAftrekItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ws.BerekeningVenBApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == BerekeningVenBApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> BerekeningVenBAppOpdelingNaarOorsprongItemValidate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaBerekeningVenBAppOpdelingNaarOorsprongItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ws.BerekeningVenBApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == BerekeningVenBApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> BerekeningVenBAppFiscaleAftrekItemValidate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaBerekeningVenBAppFiscaleAftrekItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ws.BerekeningVenBApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == BerekeningVenBApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> BerekeningVenBAppTariefItemValidate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaBerekeningVenBAppTariefItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ws.BerekeningVenBApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == BerekeningVenBApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }


        public List<FileMessageDataContract> BerekeningVenBAppAanslagValidate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaBerekeningVenBAppAanslagDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ws.BerekeningVenBApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == BerekeningVenBApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }

        public List<FileMessageDataContract> BerekeningVenBAppFairnessTaxItemValidate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaBerekeningVenBAppFairnessTaxItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ws.BerekeningVenBApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == BerekeningVenBApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            if (cedc.Entity.IsValid) ws.BerekeningVenBApp.Calculate(ref ws);
            return msgs;
        }


        public List<FileMessageDataContract> BerekeningVenBAppBerekeningOverzichtItemValidate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaBerekeningVenBAppBerekeningOverzichtItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ws.BerekeningVenBApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == BerekeningVenBApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            if (cedc.Entity.IsValid) ws.BerekeningVenBApp.Calculate(ref ws);
            return msgs;
        }



        #endregion



        #region Add/Remove

        /*

        public EY.com.eBook.RuleEngine.AY2014.BerekeningVenBAppTypes.BerekeningVenB BerekeningVenBAppOverzichtUpdate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaBerekeningVenBAppGeneralDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            BerekeningVenBApp rp = ws.BerekeningVenBApp;
            rp.UpdateOverzicht(cedc.Entity, ref ws);
            set.Calculate("BerekeningVenBApp");
            return rp.Data;
        }

        */

        public EY.com.eBook.RuleEngine.AY2014.BerekeningVenBAppTypes.BerekeningVenB BerekeningVenBAppBelasteReservesUpdate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaBerekeningVenBAppBelasteReservesItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            BerekeningVenBApp rp = ws.BerekeningVenBApp;
            rp.UpdateBelasteReserves(cedc.Entity, ref ws);
            set.Calculate("BerekeningVenBApp");
            return rp.Data;
        }



        public EY.com.eBook.RuleEngine.AY2014.BerekeningVenBAppTypes.BerekeningVenB BerekeningVenBAppVerworpenUitgavenUpdate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaBerekeningVenBAppVerworpenUitgavenItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            BerekeningVenBApp rp = ws.BerekeningVenBApp;
            rp.UpdateVerworpenUitgaven(cedc.Entity, ref ws);
            set.Calculate("BerekeningVenBApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.BerekeningVenBAppTypes.BerekeningVenB BerekeningVenBAppVerrekenbareVoorheffingUpdate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaBerekeningVenBAppVerrekenbareVoorheffingEntityDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            BerekeningVenBApp rp = ws.BerekeningVenBApp;
            rp.UpdateVerrekenbareVoorheffing(cedc.Entity, ref ws);
            set.Calculate("BerekeningVenBApp");
            return rp.Data;
        }



        public EY.com.eBook.RuleEngine.AY2014.BerekeningVenBAppTypes.BerekeningVenB BerekeningVenBAppBelastbareBestZonderAftrekUpdate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaBerekeningVenBAppBelastbareBestZonderAftrekItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            BerekeningVenBApp rp = ws.BerekeningVenBApp;
            rp.UpdateBelastbareBestZonderAftrek(cedc.Entity, ref ws);
            set.Calculate("BerekeningVenBApp");
            return rp.Data;
        }



        public EY.com.eBook.RuleEngine.AY2014.BerekeningVenBAppTypes.BerekeningVenB BerekeningVenBAppOpdelingNaarOorsprongUpdate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaBerekeningVenBAppOpdelingNaarOorsprongItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            BerekeningVenBApp rp = ws.BerekeningVenBApp;
            rp.UpdateOpdelingNaarOorsprong(cedc.Entity, ref ws);
            set.Calculate("BerekeningVenBApp");
            return rp.Data;
        }



        public EY.com.eBook.RuleEngine.AY2014.BerekeningVenBAppTypes.BerekeningVenB BerekeningVenBAppFiscaleAftrekUpdate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaBerekeningVenBAppFiscaleAftrekItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            BerekeningVenBApp rp = ws.BerekeningVenBApp;
            rp.UpdateFiscaleAftrek(cedc.Entity, ref ws);
            set.Calculate("BerekeningVenBApp");
            return rp.Data;
        }



        public EY.com.eBook.RuleEngine.AY2014.BerekeningVenBAppTypes.BerekeningVenB BerekeningVenBAppTariefUpdate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaBerekeningVenBAppTariefItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            BerekeningVenBApp rp = ws.BerekeningVenBApp;
            rp.UpdateTarief(cedc.Entity, ref ws);
            set.Calculate("BerekeningVenBApp");
            return rp.Data;
        }



        public EY.com.eBook.RuleEngine.AY2014.BerekeningVenBAppTypes.BerekeningVenB BerekeningVenBAppAanslagUpdate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaBerekeningVenBAppAanslagDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            BerekeningVenBApp rp = ws.BerekeningVenBApp;
            rp.UpdateAanslag(cedc.Entity, ref ws);
            set.Calculate("BerekeningVenBApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.BerekeningVenBAppTypes.BerekeningVenB BerekeningVenBAppFairnessTaxUpdate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaBerekeningVenBAppFairnessTaxItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            BerekeningVenBApp rp = ws.BerekeningVenBApp;
            rp.UpdateFairnessTax(cedc.Entity, ref ws);
            set.Calculate("BerekeningVenBApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.BerekeningVenBAppTypes.BerekeningVenB BerekeningVenBAppBerekeningOverzichtUpdate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaBerekeningVenBAppBerekeningOverzichtItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            BerekeningVenBApp rp = ws.BerekeningVenBApp;
            rp.UpdateBerekeningOverzicht(cedc.Entity, ref ws);
            set.Calculate("BerekeningVenBApp");
            return rp.Data;
        }



       

        #endregion

        #endregion



        #region AangifteVenBApp

        public EY.com.eBook.RuleEngine.AY2014.AangifteVenBAppTypes.AangifteVenB AangifteVenBAppGetData(CriteriaFileDataContract cfdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfdc.FileId);

            return set._datacontainer.AangifteVenBApp.Data;
        }


        public EY.com.eBook.RuleEngine.AY2014.AangifteVenBAppTypes.AangifteVenB AangifteVenBAppCalculate(CriteriaFileDataContract cfdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfdc.FileId);
            set.Calculate("AangifteVenBApp");

            return set._datacontainer.AangifteVenBApp.Data;
        }

        #region Entity validation


        public List<FileMessageDataContract> AangifteVenBAppReserveEntityValidate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaAangifteVenBAppReserveEntityDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ws.AangifteVenBApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == AangifteVenBApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> AangifteVenBAppVrijgesteldeReserveEntityValidate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaAangifteVenBAppVrijgesteldeReserveEntityDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ws.AangifteVenBApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == AangifteVenBApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> AangifteVenBAppVerworpenUitgavenEntityValidate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaAangifteVenBAppVerworpenUitgavenEntityDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ws.AangifteVenBApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == AangifteVenBApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> AangifteVenBAppUitgekeerdeDividendenEntityValidate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaAangifteVenBAppUitgekeerdeDividendenEntityDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ws.AangifteVenBApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == AangifteVenBApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> AangifteVenBAppWinstEntityValidate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaAangifteVenBAppWinstEntityDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ws.AangifteVenBApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == AangifteVenBApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> AangifteVenBAppAfzonderlijkeAanslagenEntityValidate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaAangifteVenBAppAfzonderlijkeAanslagenEntityDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ws.AangifteVenBApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == AangifteVenBApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> AangifteVenBAppDiamantEntityValidate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaAangifteVenBAppDiamantEntityDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ws.AangifteVenBApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == AangifteVenBApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> AangifteVenBAppNietBelastbareBestandelenItemValidate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaAangifteVenBAppNietBelastbareBestandelenItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ws.AangifteVenBApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == AangifteVenBApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> AangifteVenBAppDbiEntityValidate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaAangifteVenBAppDbiEntityDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ws.AangifteVenBApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == AangifteVenBApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> AangifteVenBAppOverdrachtDBIAftrekEntityValidate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaAangifteVenBAppOverdrachtDBIAftrekEntityDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ws.AangifteVenBApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == AangifteVenBApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> AangifteVenBAppOverdrachtNIDEntityValidate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaAangifteVenBAppOverdrachtNIDEntityDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ws.AangifteVenBApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == AangifteVenBApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> AangifteVenBAppCompenseerbareVerliezenEntityValidate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaAangifteVenBAppCompenseerbareVerliezenEntityDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ws.AangifteVenBApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == AangifteVenBApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> AangifteVenBAppTariefEntityValidate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaAangifteVenBAppTariefEntityDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ws.AangifteVenBApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == AangifteVenBApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> AangifteVenBAppVoorafBetalingEntityValidate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaAangifteVenBAppVoorafBetalingEntityDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ws.AangifteVenBApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == AangifteVenBApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        



        public List<FileMessageDataContract> AangifteVenBAppTaxShelterEntityValidate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaAangifteVenBAppTaxShelterEntityDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ws.AangifteVenBApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == AangifteVenBApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> AangifteVenBAppGrootteVennootschapeEntityValidate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaAangifteVenBAppGrootteVennootschapeEntityDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ws.AangifteVenBApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == AangifteVenBApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        #endregion



        #region Add/Remove



        public EY.com.eBook.RuleEngine.AY2014.AangifteVenBAppTypes.AangifteVenB AangifteVenBAppReservesUpdate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaAangifteVenBAppReserveEntityDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            AangifteVenBApp rp = ws.AangifteVenBApp;
            rp.UpdateReserves(cedc.Entity, ref ws);
            set.Calculate("AangifteVenBApp");
            return rp.Data;
        }



        public EY.com.eBook.RuleEngine.AY2014.AangifteVenBAppTypes.AangifteVenB AangifteVenBAppVrijgesteldeReserveUpdate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaAangifteVenBAppVrijgesteldeReserveEntityDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            AangifteVenBApp rp = ws.AangifteVenBApp;
            rp.UpdateVrijgesteldeReserve(cedc.Entity, ref ws);
            set.Calculate("AangifteVenBApp");
            return rp.Data;
        }



        public EY.com.eBook.RuleEngine.AY2014.AangifteVenBAppTypes.AangifteVenB AangifteVenBAppVerworpenUitgavenUpdate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaAangifteVenBAppVerworpenUitgavenEntityDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            AangifteVenBApp rp = ws.AangifteVenBApp;
            rp.UpdateVerworpenUitgaven(cedc.Entity, ref ws);
            set.Calculate("AangifteVenBApp");
            return rp.Data;
        }



        public EY.com.eBook.RuleEngine.AY2014.AangifteVenBAppTypes.AangifteVenB AangifteVenBAppUitgekeerdeDividendenUpdate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaAangifteVenBAppUitgekeerdeDividendenEntityDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            AangifteVenBApp rp = ws.AangifteVenBApp;
            rp.UpdateUitgekeerdeDividenden(cedc.Entity, ref ws);
            set.Calculate("AangifteVenBApp");
            return rp.Data;
        }



        public EY.com.eBook.RuleEngine.AY2014.AangifteVenBAppTypes.AangifteVenB AangifteVenBAppUiteenzettingWinstUpdate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaAangifteVenBAppWinstEntityDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            AangifteVenBApp rp = ws.AangifteVenBApp;
            rp.UpdateUiteenzettingWinst(cedc.Entity, ref ws);
            set.Calculate("AangifteVenBApp");
            return rp.Data;
        }



        public EY.com.eBook.RuleEngine.AY2014.AangifteVenBAppTypes.AangifteVenB AangifteVenBAppAfzonderlijkeAanslagenUpdate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaAangifteVenBAppAfzonderlijkeAanslagenEntityDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            AangifteVenBApp rp = ws.AangifteVenBApp;
            rp.UpdateAfzonderlijkeAanslagen(cedc.Entity, ref ws);
            set.Calculate("AangifteVenBApp");
            return rp.Data;
        }



        public EY.com.eBook.RuleEngine.AY2014.AangifteVenBAppTypes.AangifteVenB AangifteVenBAppDiamantUpdate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaAangifteVenBAppDiamantEntityDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            AangifteVenBApp rp = ws.AangifteVenBApp;
            rp.UpdateDiamant(cedc.Entity, ref ws);
            set.Calculate("AangifteVenBApp");
            return rp.Data;
        }



        public EY.com.eBook.RuleEngine.AY2014.AangifteVenBAppTypes.AangifteVenB AangifteVenBAppNietBelastbareBestanddelenUpdate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaAangifteVenBAppNietBelastbareBestandelenItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            AangifteVenBApp rp = ws.AangifteVenBApp;
            rp.UpdateNietBelastbareBestanddelen(cedc.Entity, ref ws);
            set.Calculate("AangifteVenBApp");
            return rp.Data;
        }



        public EY.com.eBook.RuleEngine.AY2014.AangifteVenBAppTypes.AangifteVenB AangifteVenBAppDBIUpdate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaAangifteVenBAppDbiEntityDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            AangifteVenBApp rp = ws.AangifteVenBApp;
            rp.UpdateDBI(cedc.Entity, ref ws);
            set.Calculate("AangifteVenBApp");
            return rp.Data;
        }



        public EY.com.eBook.RuleEngine.AY2014.AangifteVenBAppTypes.AangifteVenB AangifteVenBAppOverdrachtDBIAftrekUpdate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaAangifteVenBAppOverdrachtDBIAftrekEntityDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            AangifteVenBApp rp = ws.AangifteVenBApp;
            rp.UpdateOverdrachtDBIAftrek(cedc.Entity, ref ws);
            set.Calculate("AangifteVenBApp");
            return rp.Data;
        }



        public EY.com.eBook.RuleEngine.AY2014.AangifteVenBAppTypes.AangifteVenB AangifteVenBAppOverdrachtNIDUpdate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaAangifteVenBAppOverdrachtNIDEntityDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            AangifteVenBApp rp = ws.AangifteVenBApp;
            rp.UpdateOverdrachtNID(cedc.Entity, ref ws);
            set.Calculate("AangifteVenBApp");
            return rp.Data;
        }



        public EY.com.eBook.RuleEngine.AY2014.AangifteVenBAppTypes.AangifteVenB AangifteVenBAppCompenseerbareVerliezenUpdate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaAangifteVenBAppCompenseerbareVerliezenEntityDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            AangifteVenBApp rp = ws.AangifteVenBApp;
            rp.UpdateCompenseerbareVerliezen(cedc.Entity, ref ws);
            set.Calculate("AangifteVenBApp");
            return rp.Data;
        }



        public EY.com.eBook.RuleEngine.AY2014.AangifteVenBAppTypes.AangifteVenB AangifteVenBAppTariefBelastingUpdate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaAangifteVenBAppTariefEntityDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            AangifteVenBApp rp = ws.AangifteVenBApp;
            rp.UpdateTariefBelasting(cedc.Entity, ref ws);
            set.Calculate("AangifteVenBApp");
            return rp.Data;
        }



        public EY.com.eBook.RuleEngine.AY2014.AangifteVenBAppTypes.AangifteVenB AangifteVenBAppVoorafBetalingenUpdate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaAangifteVenBAppVoorafBetalingEntityDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            AangifteVenBApp rp = ws.AangifteVenBApp;
            rp.UpdateVoorafBetalingen(cedc.Entity, ref ws);
            set.Calculate("AangifteVenBApp");
            return rp.Data;
        }



       



        public EY.com.eBook.RuleEngine.AY2014.AangifteVenBAppTypes.AangifteVenB AangifteVenBAppTaxShelterUpdate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaAangifteVenBAppTaxShelterEntityDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            AangifteVenBApp rp = ws.AangifteVenBApp;
            rp.UpdateTaxShelter(cedc.Entity, ref ws);
            set.Calculate("AangifteVenBApp");
            return rp.Data;
        }



        public EY.com.eBook.RuleEngine.AY2014.AangifteVenBAppTypes.AangifteVenB AangifteVenBAppGrootteVennootschapUpdate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaAangifteVenBAppGrootteVennootschapeEntityDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            AangifteVenBApp rp = ws.AangifteVenBApp;
            rp.UpdateGrootteVennootschap(cedc.Entity, ref ws);
            set.Calculate("AangifteVenBApp");
            return rp.Data;
        }


        #endregion

        #endregion

        #region DetailRekeningKapitaalApp

        public EY.com.eBook.RuleEngine.AY2014.DetailRekeningKapitaalAppTypes.DetailRekeningKapitaal DetailRekeningKapitaalAppGetData(CriteriaFileDataContract cfdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfdc.FileId);

            return set._datacontainer.DetailRekeningKapitaalApp.Data;
        }


        public EY.com.eBook.RuleEngine.AY2014.DetailRekeningKapitaalAppTypes.DetailRekeningKapitaal DetailRekeningKapitaalAppCalculate(CriteriaFileDataContract cfdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfdc.FileId);
            set.Calculate("DetailRekeningKapitaalApp");

            return set._datacontainer.DetailRekeningKapitaalApp.Data;
        }

        #region Entity validation


        public List<FileMessageDataContract> DetailRekeningKapitaalAppOverzichtValidate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaDetailRekeningKapitaalEntityDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            ws.DetailRekeningKapitaalApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == DetailRekeningKapitaalApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }

        #endregion



        #region Add/Remove


        public EY.com.eBook.RuleEngine.AY2014.DetailRekeningKapitaalAppTypes.DetailRekeningKapitaal DetailRekeningKapitaalAppOverzichtAdd(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaDetailRekeningKapitaalEntityDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            DetailRekeningKapitaalApp rp = ws.DetailRekeningKapitaalApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToOverzicht(cedc.Entity, ref ws);
            set.Calculate("DetailRekeningKapitaalApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.DetailRekeningKapitaalAppTypes.DetailRekeningKapitaal DetailRekeningKapitaalAppOverzichtRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            DetailRekeningKapitaalApp rp = ws.DetailRekeningKapitaalApp;
            rp.DeleteOverzicht(cfrdc.RowId, ref ws);
            set.Calculate("DetailRekeningKapitaalApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2014.DetailRekeningKapitaalAppTypes.DetailRekeningKapitaal DetailRekeningKapitaalAppOverzichtUpdate(EY.com.eBook.RuleEngine.Data.AY2014.CriteriaDetailRekeningKapitaalEntityDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2014.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2014.WorksheetSetData ws = set._datacontainer;
            DetailRekeningKapitaalApp rp = ws.DetailRekeningKapitaalApp;
            rp.UpdateOverzicht(cedc.Entity, ref ws);
            set.Calculate("DetailRekeningKapitaalApp");
            return rp.Data;
        }

        #endregion

        #endregion



        #endregion

        #endregion



        
    }
}
