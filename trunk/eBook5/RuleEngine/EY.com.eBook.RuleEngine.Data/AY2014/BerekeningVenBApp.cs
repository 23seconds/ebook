
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.RuleEngine.Core;
using EY.com.eBook.RuleEngine.Core.Interfaces;
using EY.com.eBook.RuleEngine.Data;
using EY.com.eBook.Core;

namespace EY.com.eBook.RuleEngine.AY2014
{
    
    public class BerekeningVenBApp
    {
        public static WorksheetTypeSettings Settings = new WorksheetTypeSettings
                                                      {
                                                        Clients = true
                                                        ,
                                                        Suppliers = true
                                                        ,
                                                        AllAccounts = true
                                                        , 
                                                        PerformsBookings=true
                                                      };
                                                      
        public static Guid TypeId = new Guid("803d6a06-352e-4e28-ade5-214deaac76ca");

        public List<string> ImportablePreviousTypes = new List<string>() { "EY.com.eBook.RuleEngine.AY2013.BerekeningVenBApp" };

        public Guid GetMyType() { return TypeId; }

        public void SetMinimal()
        {
            if (Data == null)
            {
                Data = new EY.com.eBook.RuleEngine.AY2014.BerekeningVenBAppTypes.BerekeningVenB();
                Data.LastUpdated = DateTime.Now;
            }

           

            if (Data.BelasteReserves == null)
            {
                Data.BelasteReserves = new EY.com.eBook.RuleEngine.AY2014.BerekeningVenBAppTypes.BelasteReservesItem
                {
                    Id = Guid.NewGuid()
                    ,
                    LastUpdated = null
                    ,
                    IsValid = true
                    ,
                    PreviousImported = false

                };
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.VerworpenUitgaven == null)
            {
                Data.VerworpenUitgaven = new EY.com.eBook.RuleEngine.AY2014.BerekeningVenBAppTypes.VerworpenUitgavenItem
                {
                    Id = Guid.NewGuid()
                    ,
                    LastUpdated = null
                    ,
                    IsValid = true
                    ,
                    PreviousImported = false

                };
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.BelastbareBestZonderAftrek == null)
            {
                Data.BelastbareBestZonderAftrek = new EY.com.eBook.RuleEngine.AY2014.BerekeningVenBAppTypes.BelastbareBestZonderAftrekItem
                {
                    Id = Guid.NewGuid()
                    ,
                    LastUpdated = null
                    ,
                    IsValid = true
                    ,
                    PreviousImported = false

                };
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.OpdelingNaarOorsprong == null)
            {
                Data.OpdelingNaarOorsprong = new EY.com.eBook.RuleEngine.AY2014.BerekeningVenBAppTypes.OpdelingNaarOorsprongItem
                {
                    Id = Guid.NewGuid()
                    ,
                    LastUpdated = null
                    ,
                    IsValid = true
                    ,
                    PreviousImported = false

                };
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.FiscaleAftrek == null)
            {
                Data.FiscaleAftrek = new EY.com.eBook.RuleEngine.AY2014.BerekeningVenBAppTypes.FiscaleAftrekItem
                {
                    Id = Guid.NewGuid()
                    ,
                    LastUpdated = null
                    ,
                    IsValid = true
                    ,
                    PreviousImported = false

                };
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.Tarief == null)
            {
                Data.Tarief = new EY.com.eBook.RuleEngine.AY2014.BerekeningVenBAppTypes.TariefItem
                {
                    Id = Guid.NewGuid()
                    ,
                    LastUpdated = null
                    ,
                    IsValid = true
                    ,
                    PreviousImported = false

                };
                // STANDARD ON TRUE.
                Data.Tarief.BezitAandelenDoorVennootschappen = true;
                Data.Tarief.BezitAandelenMeerDan50Percent = true;
                Data.Tarief.ToegekendeBezoldiging = true;
                Data.Tarief.UitgekeerdDividendHogerDan13 = true;

                Data.LastUpdated = DateTime.Now;
            }

            if (Data.Aanslag == null)
            {
                Data.Aanslag = new EY.com.eBook.RuleEngine.AY2014.BerekeningVenBAppTypes.AanslagItem
                {
                    Id = Guid.NewGuid()
                    ,
                    LastUpdated = null
                    ,
                    IsValid = true
                    ,
                    PreviousImported = false
                };
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.FairnessTax == null)
            {
                Data.FairnessTax = new EY.com.eBook.RuleEngine.AY2014.BerekeningVenBAppTypes.FairnessTaxItem
                {
                    Id = Guid.NewGuid()
                    ,
                    LastUpdated = null
                    ,
                    IsValid = true
                    ,
                    PreviousImported = false
                };
                Data.LastUpdated = DateTime.Now;
            }


            if (Data.VerrekenbareVoorheffing == null)
            {
                Data.VerrekenbareVoorheffing = new EY.com.eBook.RuleEngine.AY2014.BerekeningVenBAppTypes.VerrekenbareVoorheffingEntity
                {
                    Id = Guid.NewGuid()
                    ,
                    LastUpdated = null
                    ,
                    IsValid = true
                    ,
                    PreviousImported = false
                };
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.BerekeningOverzicht == null)
            {
                Data.BerekeningOverzicht = new EY.com.eBook.RuleEngine.AY2014.BerekeningVenBAppTypes.BerekeningOverzichtItem
                {
                    Id = Guid.NewGuid()
                    ,
                    LastUpdated = null
                    ,
                    IsValid = true
                    ,
                    PreviousImported = false

                };
                Data.LastUpdated = DateTime.Now;
            }

           

        }
        
        public string ToXml() 
        {
            return CoreHelper.SerializeToString(Data);
        }

        public void ImportPrevious(ref WorksheetSetData wsd)
        {
            if (wsd.DataContract.PreviousFileId.HasValue)
            {
                if (wsd.PreviousSet.AssessmentYear == 2013)
                {
                    EY.com.eBook.RuleEngine.AY2013.WorksheetSetData pset = (EY.com.eBook.RuleEngine.AY2013.WorksheetSetData)wsd.PreviousSet;

                    EY.com.eBook.RuleEngine.AY2013.BerekeningVenBApp previous = pset.BerekeningVenBApp;

                    Data.Aanslag.TotaalBedragVoorzieningen = previous.Data.Aanslag.TotaalBedragVoorzieningen.GetValueOrDefault();
                    Data.Aanslag.AfzonderlijkeAanslagGespreid = previous.Data.Aanslag.AfzonderlijkeAanslagGespreid.GetValueOrDefault();
                    UpdateAanslag(Data.Aanslag, ref wsd);

                }
            }
        }

        public void LoadXml(string xml)
        {
            if (!string.IsNullOrEmpty(xml)) {
                Data = CoreHelper.DeserializeFromString<EY.com.eBook.RuleEngine.AY2014.BerekeningVenBAppTypes.BerekeningVenB>(xml);
            }
            SetMinimal();
            
        }
        
        public EY.com.eBook.RuleEngine.AY2014.BerekeningVenBAppTypes.BerekeningVenB Data { get; set; }

        public void ForceRecalculation(ref WorksheetSetData worksheetset)
        {
            Data.LastUpdated = null;
            Calculate(ref worksheetset);
        }

        private void ImportWorksheets(ref WorksheetSetData worksheetset)
        {
            int assessment = worksheetset.DataContract.AssessmentYear;
            #region Import Belastbare basis
            // BELASTBARE BASIS
            BepalingBelastbareBasisAppTypes.BepalingBasis basis = worksheetset.BepalingBelastbareBasisApp.Data;

            /*
            Data.Overzicht.GereserveerdeWinst = basis.Overzicht.GereserveerdeWinst;
            Data.Overzicht.Dividenden = basis.Overzicht.Dividenden;
            Data.Overzicht.VerkrijgingEigenAandelen = basis.Overzicht.VerkrijgingEigenAandelen;
            Data.Overzicht.OverlijdenUittredingUitsluitingVennoot = basis.Overzicht.OverlijdenUittredingUitsluitingVennoot;
            Data.Overzicht.VerdelingMaatschappelijkVermogen = basis.Overzicht.VerdelingMaatschappelijkVermogen;
            Data.Overzicht.StatAccountingResult = basis.Overzicht.StatAccountingResult;
            Data.Overzicht.TotalBelasteReserves = basis.Overzicht.TotalBelasteReserves;
            Data.Overzicht.TotalAanpassingenBelasteReserves = basis.Overzicht.TotalAanpassingenBelasteReserves;
            Data.Overzicht.NietAftrekbareBelastingen = basis.Overzicht.NietAftrekbareBelastingen;
            Data.Overzicht.TotaalVerworpenUitgaven = basis.Overzicht.TotaalVerworpenUitgaven;
            Data.Overzicht.BelastbaarResultaat = basis.Overzicht.BelastbaarResultaat;
            Data.Overzicht.TotaalBelastbareBestandenZonderAftrekMin = basis.Overzicht.TotaalBelastbareBestandenZonderAftrekMin;
            Data.Overzicht.SubTotaalBelastbaarVoorAftrek = basis.Overzicht.SubTotaalBelastbaarVoorAftrek;
            Data.Overzicht.OpdelingNaarOorsprongBijVerdragVrijgesteldInkomen = basis.Overzicht.OpdelingNaarOorsprongBijVerdragVrijgesteldInkomen;
            */
            Data.BerekeningOverzicht.StatAccountingResult = basis.Overzicht.StatAccountingResult;

            Data.BelasteReserves.OACapitalGainsOnShares = basis.BelasteReserves.OACapitalGainsOnShares;
            Data.BelasteReserves.OACapitalGainsSharesReversal = basis.BelasteReserves.OACapitalGainsSharesReversal;
            Data.BelasteReserves.OADefinitiveExemptionTaxShelter = basis.BelasteReserves.OADefinitiveExemptionTaxShelter;
            Data.BelasteReserves.OAExemptionRegionalPrem = basis.BelasteReserves.OAExemptionRegionalPrem;
            Data.BelasteReserves.OAFinalExemptionProfit = basis.BelasteReserves.OAFinalExemptionProfit;
            Data.BelasteReserves.OAOthers = basis.BelasteReserves.OAOthers;
            Data.BelasteReserves.OAInTheMin = basis.BelasteReserves.OAInTheMin;
            Data.BelasteReserves.OATotal = basis.BelasteReserves.OATotal;
            Data.BelasteReserves.OhterAdjustments = basis.BelasteReserves.OhterAdjustments;
            Data.BelasteReserves.TDExcessDeprications = basis.BelasteReserves.TDExcessDeprications;
            Data.BelasteReserves.TDHerwaardering = basis.BelasteReserves.TDHerwaardering;
            Data.BelasteReserves.TDKapitaalEnUitgifte = basis.BelasteReserves.TDKapitaalEnUitgifte;
            Data.BelasteReserves.TDOthers = basis.BelasteReserves.TDOthers;
            Data.BelasteReserves.TDOtherTaxableReserves = basis.BelasteReserves.TDOtherTaxableReserves;
            Data.BelasteReserves.TDOtherNonBalanceTaxableReserves = basis.BelasteReserves.TDOtherNonBalanceTaxableReserves;
            Data.BelasteReserves.TDOverestimationLiabilities = basis.BelasteReserves.TDOverestimationLiabilities;
            Data.BelasteReserves.TDTaxableDoubtfullDebtors = basis.BelasteReserves.TDTaxableDoubtfullDebtors;
            Data.BelasteReserves.TDTaxableProvisions = basis.BelasteReserves.TDTaxableProvisions;
            Data.BelasteReserves.TDTotal = basis.BelasteReserves.TDTotal;
            Data.BelasteReserves.TDUnderestimationAssetsOverestimationLiabilities = basis.BelasteReserves.TDUnderestimationAssetsOverestimationLiabilities;

            Data.VerworpenUitgaven.DEAbnormalAdvantages = basis.VerworpenUitgaven.DEAbnormalAdvantages;
            Data.VerworpenUitgaven.DEBepaaldeStaten = basis.VerworpenUitgaven.DEBepaaldeStaten;
            Data.VerworpenUitgaven.DECar = basis.VerworpenUitgaven.DECar;
            Data.VerworpenUitgaven.DECarVAA = basis.VerworpenUitgaven.DECarVAA;
            Data.VerworpenUitgaven.DECharity = basis.VerworpenUitgaven.DECharity;
            Data.VerworpenUitgaven.DEClothes = basis.VerworpenUitgaven.DEClothes;
            Data.VerworpenUitgaven.DEDinerSportCultureEco = basis.VerworpenUitgaven.DEDinerSportCultureEco;
            Data.VerworpenUitgaven.DEEmployeeCapital = basis.VerworpenUitgaven.DEEmployeeCapital;
            Data.VerworpenUitgaven.DEExcessInterest = basis.VerworpenUitgaven.DEExcessInterest;
            Data.VerworpenUitgaven.DEExpensesTaxShelter = basis.VerworpenUitgaven.DEExpensesTaxShelter;
            Data.VerworpenUitgaven.DEIndemnityMissingCoupon = basis.VerworpenUitgaven.DEIndemnityMissingCoupon;
            Data.VerworpenUitgaven.DEInterestLoans = basis.VerworpenUitgaven.DEInterestLoans;
            Data.VerworpenUitgaven.DEOther = basis.VerworpenUitgaven.DEOther;
            Data.VerworpenUitgaven.DEPenalties = basis.VerworpenUitgaven.DEPenalties;
            Data.VerworpenUitgaven.DEPensions = basis.VerworpenUitgaven.DEPensions;
            Data.VerworpenUitgaven.DEReception = basis.VerworpenUitgaven.DEReception;
            Data.VerworpenUitgaven.DEReductionValueShares = basis.VerworpenUitgaven.DEReductionValueShares;
            Data.VerworpenUitgaven.DERegionalPremiumsCapital = basis.VerworpenUitgaven.DERegionalPremiumsCapital;
            Data.VerworpenUitgaven.DERegionalTaxes = basis.VerworpenUitgaven.DERegionalTaxes;
            Data.VerworpenUitgaven.DERestaurant = basis.VerworpenUitgaven.DERestaurant;
            Data.VerworpenUitgaven.DEReversalsPreviousTax = basis.VerworpenUitgaven.DEReversalsPreviousTax;
            Data.VerworpenUitgaven.DESocial = basis.VerworpenUitgaven.DESocial;
            Data.VerworpenUitgaven.DEUnjustifiedExpenses = basis.VerworpenUitgaven.DEUnjustifiedExpenses;
            Data.VerworpenUitgaven.DETotal = basis.VerworpenUitgaven.DETotal;
            Data.VerworpenUitgaven.PDDisallowedCurrentIncomeTaxExpenses = basis.VerworpenUitgaven.PDDisallowedCurrentIncomeTaxExpenses;

            Data.BelastbareBestZonderAftrek.TECapitalGains0412 = basis.BelastbareBestZonderAftrek.TECapitalGains0412.GetValueOrDefault();
            Data.BelastbareBestZonderAftrek.TEEmployeeParticipationCapital = basis.BelastbareBestZonderAftrek.TEEmployeeParticipationCapital.GetValueOrDefault();
            Data.BelastbareBestZonderAftrek.TEReceivedAbnormal = basis.BelastbareBestZonderAftrek.TEReceivedAbnormal.GetValueOrDefault();
            Data.BelastbareBestZonderAftrek.TETaxableInvestmentReserve = basis.BelastbareBestZonderAftrek.TETaxableInvestmentReserve.GetValueOrDefault();
            Data.BelastbareBestZonderAftrek.TECapitalAgriCulture = basis.BelastbareBestZonderAftrek.TECapitalAgriCulture.GetValueOrDefault();
            Data.BelastbareBestZonderAftrek.TEVaaCar = basis.BelastbareBestZonderAftrek.TEVaaCar.GetValueOrDefault();
            Data.BelastbareBestZonderAftrek.TETotal = basis.BelastbareBestZonderAftrek.TETotal.GetValueOrDefault();

            Data.OpdelingNaarOorsprong.IRBelgianTaxable = basis.OpdelingNaarOorsprong.IRBelgianTaxable;
            Data.OpdelingNaarOorsprong.IRNonTreatyForeignIncome = basis.OpdelingNaarOorsprong.IRNonTreatyForeignIncome;
            Data.OpdelingNaarOorsprong.IRRemainingResultBelgium = basis.OpdelingNaarOorsprong.IRRemainingResultBelgium;
            Data.OpdelingNaarOorsprong.IRRemainingResultNonTreaty = basis.OpdelingNaarOorsprong.IRRemainingResultNonTreaty;
            Data.OpdelingNaarOorsprong.IRTreatyForeignIncome = basis.OpdelingNaarOorsprong.IRTreatyForeignIncome;

            Data.FiscaleAftrek.NTEBECharity = basis.FiscaleAftrek.NTEBECharity;
            Data.FiscaleAftrek.NTEBEInternalshipPremium = basis.FiscaleAftrek.NTEBEInternalshipPremium;
            Data.FiscaleAftrek.NTEBEOthers = basis.FiscaleAftrek.NTEBEOthers;
            Data.FiscaleAftrek.NTEBEPersonnel = basis.FiscaleAftrek.NTEBEPersonnel;
            Data.FiscaleAftrek.NTEBEPersonnelSME = basis.FiscaleAftrek.NTEBEPersonnelSME;
            Data.FiscaleAftrek.TotalNonTaxableElements = basis.FiscaleAftrek.TotalNonTaxableElements;
            #endregion

            // TRANSFER TAX CREDIT
            TransferTaxCreditsAppTypes.Aanwending aanwendingen = worksheetset.TransferTaxCreditsApp.Data.Aanwendingen;
            Data.FiscaleAftrek.DTLBelgium = aanwendingen.AanwendingVerlies;
            Data.FiscaleAftrek.PEBelgium = aanwendingen.AanwendingDBI;
            Data.FiscaleAftrek.NIDBelgium = aanwendingen.AanwendingNID;
            Data.FiscaleAftrek.IDBelgium = aanwendingen.AanwendingInvest;

            /* new fiche 2014 */
            //Data.FiscaleAftrek.NIDHistoryBelgium = worksheetset.RisicoKapitaalHistoriekApp.Data.Fiche.WerkelijkVorigAJ.GetValueOrDefault();
            Data.FiscaleAftrek.NIDHistoryBelgium = worksheetset.RisicoKapitaalHistoriekApp.Data.Fiche.WerkelijkAfgetrokkenVrijstellingen.GetValueOrDefault();

            


            TransferTaxCreditsAppTypes.BelastingkredietItem bki = worksheetset.TransferTaxCreditsApp.Data.Belastingkrediet.LastOrDefault();

            Data.BerekeningOverzicht.RetributionResearchDevelopmentBase = bki != null ? bki.BedragAftrek : 0;

            #region Fairness Tax
            if (Data.FairnessTax == null)
            {
                Data.FairnessTax = new BerekeningVenBAppTypes.FairnessTaxItem { Id = Guid.NewGuid() };
            }
            else
            {
                // clear calc
                Data.FairnessTax = new BerekeningVenBAppTypes.FairnessTaxItem { Id = Data.FairnessTax.Id, PreviousDividends = Data.FairnessTax.PreviousDividends };
            }
            //else if(Data.FairnessTax!=null && worksheetset.Settings.CurrentPeriodSmall) 

            Data.FairnessTax.Dividenden = basis.Overzicht.Dividenden;
           
            Data.FairnessTax.NidApplied = aanwendingen.AanwendingNID;
            Data.FairnessTax.PreviousLosses = aanwendingen.AanwendingVerlies;
            #endregion

            #region TARIEVEN

            var coefMWAanvullend = worksheetset.GetCoefficient("VENB_MEERWAARDENAANDELEN_AANVULLEND_PERCENT", assessment);
            var coefMWA = worksheetset.GetCoefficient("VENB_MEERWAARDENAANDELEN_PERCENT", assessment);
            var coefExitTarief = worksheetset.GetCoefficient("VENB_EXITTARIEF_PERCENT", assessment);
            var coefFT = worksheetset.GetCoefficient("VENB_FAIRNESSTAX_PERCENT", assessment);

            Data.BerekeningOverzicht.CapitalGainsShares0412Perc = coefMWAanvullend != null ? coefMWAanvullend.Value : 0;
            Data.BerekeningOverzicht.CapitalGainsShares25Perc = coefMWA != null ? coefMWA.Value : 0;
            Data.BerekeningOverzicht.ExitTarifPerc = coefExitTarief != null ? coefExitTarief.Value : 0;

            Data.FairnessTax.FairnessTaxPerc = coefFT != null ? coefFT.Value : 0;

            var coeff = worksheetset.GetCoefficient("VENB_EENDUIDIGTARIEF_PERCENT", assessment);
            Data.BerekeningOverzicht.EenduidigTariefPerc = coeff != null ? coeff.Value : 0;

            coeff = worksheetset.GetCoefficient("VENB_GRENS_STAP1_TOT", assessment);
            Data.BerekeningOverzicht.VerminderdtariefStap1Base = coeff != null ? coeff.Value : 0;

            coeff = worksheetset.GetCoefficient("VENB_GRENS_STAP2_TOT", assessment);
            Data.BerekeningOverzicht.VerminderdtariefStap2Base = coeff != null ? coeff.Value : 0;

            coeff = worksheetset.GetCoefficient("VENB_GRENS_STAP3_TOT", assessment);
            Data.BerekeningOverzicht.VerminderdtariefStap3Base = coeff != null ? coeff.Value : 0;

            coeff = worksheetset.GetCoefficient("VENB_VERMINDERTARIEF_STAP1_PERCENT", assessment);
            Data.BerekeningOverzicht.VerminderdtariefStap1Perc = coeff != null ? coeff.Value : 0;

            coeff = worksheetset.GetCoefficient("VENB_VERMINDERTARIEF_STAP2_PERCENT", assessment);
            Data.BerekeningOverzicht.VerminderdtariefStap2Perc = coeff != null ? coeff.Value : 0;

            coeff = worksheetset.GetCoefficient("VENB_VERMINDERTARIEF_STAP3_PERCENT", assessment);
            Data.BerekeningOverzicht.VerminderdtariefStap3Perc = coeff != null ? coeff.Value : 0;

            coeff = worksheetset.GetCoefficient("VENB_VERMEERDERING_VA1_PERCENT", assessment);
            Data.BerekeningOverzicht.AP1Percentage = coeff != null ? coeff.Value : 0;
            coeff = worksheetset.GetCoefficient("VENB_VERMEERDERING_VA2_PERCENT", assessment);
            Data.BerekeningOverzicht.AP2Percentage = coeff != null ? coeff.Value : 0;
            coeff = worksheetset.GetCoefficient("VENB_VERMEERDERING_VA3_PERCENT", assessment);
            Data.BerekeningOverzicht.AP3Percentage = coeff != null ? coeff.Value : 0;
            coeff = worksheetset.GetCoefficient("VENB_VERMEERDERING_VA4_PERCENT", assessment);
            Data.BerekeningOverzicht.AP4Percentage = coeff != null ? coeff.Value : 0;

            Data.BerekeningOverzicht.BaseForIncreasePerc = (Data.BerekeningOverzicht.AP1Percentage.GetValueOrDefault()
                + Data.BerekeningOverzicht.AP2Percentage.GetValueOrDefault() + Data.BerekeningOverzicht.AP3Percentage.GetValueOrDefault()
                + Data.BerekeningOverzicht.AP4Percentage.GetValueOrDefault()) / 4;


            Data.BerekeningOverzicht.KapitaalEnInterestsubsidiesPerc=(decimal)5;

            Data.BerekeningOverzicht.UnjustifiedExpensesHiddenSurplusPerc=(decimal)309.00;

            Data.BerekeningOverzicht.SeparateAssessmentTaxedReserves3400Perc=(decimal)35.02;

            Data.BerekeningOverzicht.SeparateAssessmentTaxedReserves2800Perc=(decimal)28.84;


            Data.BerekeningOverzicht.SeparateAssessmentDividendsPaidPerc=(decimal)28.84;

            Data.BerekeningOverzicht.SeparateAssessmentTaxPaidTaxedReservesPerc=(decimal)15.00;

            Data.BerekeningOverzicht.DistributionCompanyAssets3300Perc=(decimal)33.00;

            Data.BerekeningOverzicht.DistributionCompanyAssets1650Perc=(decimal)16.50;

            Data.BerekeningOverzicht.BenefitsAllKindCompanyLiquidationPerc=(decimal)33.00;

            Data.BerekeningOverzicht.AdditionalDutiesDiamondTradersPerc = (decimal)10.00;

            Data.BerekeningOverzicht.RetributionResearchDevelopmentPerc = (decimal)100;
          
            #endregion


        }

        public void Calculate(ref WorksheetSetData worksheetset)
        {
            if (!Data.LastUpdated.HasValue) Data.LastUpdated = new DateTime(1980, 1, 1);
            //worksheetset.Messages.RemoveAll(m => m.ConnectionGuid == TypeId);
            worksheetset.ClearMessages(TypeId);
            List<BelastingenAppTypes.Belasting> bels = worksheetset.BelastingenApp
                                                            .Data.Belastingen
                                                            .Where(b => b.Soort != null
                                                                && b.Soort.Id.ToString().ToLower() == "7e073d47-c12f-4e0d-9f38-5005e29afc78"
                                                            ).ToList();

            if (Data.BerekeningOverzicht == null)
            {
                Data.BerekeningOverzicht = new BerekeningVenBAppTypes.BerekeningOverzichtItem { Id = Guid.NewGuid() };
            }

            bool jongVen =  worksheetset.Settings.OneOfFirstThree == true && worksheetset.Settings.CurrentPeriodSmall == true ? true:false;

            ImportWorksheets(ref worksheetset);


           // Data.BelastingsBerekening.JongVennootschap =
            int assessment = worksheetset.DataContract.AssessmentYear;

            /* VerrekenbareVoorheffing
             */

            Data.VerrekenbareVoorheffing.VVNTBelastingKredietOnderzoekOntw = Data.VerrekenbareVoorheffing.VVNTBelastingKredietOnderzoekOntw.GetValueOrDefault();
            Data.VerrekenbareVoorheffing.VVNTFictieveRoerendeVoorheffing = Data.VerrekenbareVoorheffing.VVNTFictieveRoerendeVoorheffing.GetValueOrDefault();
            Data.VerrekenbareVoorheffing.VVNTForfaitairBuitenlands = Data.VerrekenbareVoorheffing.VVNTForfaitairBuitenlands.GetValueOrDefault();
            Data.VerrekenbareVoorheffing.VVNTBelastingKredietOnderzoekOntw = Data.VerrekenbareVoorheffing.VVNTBelastingKredietOnderzoekOntw.GetValueOrDefault();


            Data.VerrekenbareVoorheffing.VVNTTotaal = (Data.VerrekenbareVoorheffing.VVNTBelastingKredietOnderzoekOntw.GetValueOrDefault() + Data.VerrekenbareVoorheffing.VVNTFictieveRoerendeVoorheffing.GetValueOrDefault()) + Data.VerrekenbareVoorheffing.VVNTForfaitairBuitenlands.GetValueOrDefault();
            Data.VerrekenbareVoorheffing.VVTAndere = Math.Abs(Data.VerrekenbareVoorheffing.VVTAndere.GetValueOrDefault());
            Data.VerrekenbareVoorheffing.VVTAndereBuitenlands = Math.Abs(Data.VerrekenbareVoorheffing.VVTAndereBuitenlands.GetValueOrDefault());
            Data.VerrekenbareVoorheffing.VVTAndereEigen = Math.Abs(Data.VerrekenbareVoorheffing.VVTAndereEigen.GetValueOrDefault());
            Data.VerrekenbareVoorheffing.VVTAndereRoerendeVoorheffing = Math.Abs((Data.VerrekenbareVoorheffing.VVTAndereRoerendeVoorheffing.GetValueOrDefault()));
            Data.VerrekenbareVoorheffing.VVTBelgisch = Math.Abs((Data.VerrekenbareVoorheffing.VVTBelgisch.GetValueOrDefault()));
            Data.VerrekenbareVoorheffing.VVTEigenBuitenlands = Math.Abs((Data.VerrekenbareVoorheffing.VVTEigenBuitenlands.GetValueOrDefault()));
            Data.VerrekenbareVoorheffing.VBelastingkredietHuiidTijdperk = Data.VerrekenbareVoorheffing.VBelastingkredietHuiidTijdperk.GetValueOrDefault();
            Data.VerrekenbareVoorheffing.VVTTotaal = ((((Data.VerrekenbareVoorheffing.VVTAndere.GetValueOrDefault() + Data.VerrekenbareVoorheffing.VVTBelgisch.GetValueOrDefault()) + Data.VerrekenbareVoorheffing.VVTAndereBuitenlands.GetValueOrDefault()) + Data.VerrekenbareVoorheffing.VVTAndereEigen.GetValueOrDefault()) + Data.VerrekenbareVoorheffing.VVTAndereRoerendeVoorheffing.GetValueOrDefault()) + Data.VerrekenbareVoorheffing.VVTEigenBuitenlands.GetValueOrDefault();

            Data.BerekeningOverzicht.NonRepayableAdvanceLevies = Data.VerrekenbareVoorheffing.VVNTTotaal.GetValueOrDefault();
            Data.BerekeningOverzicht.RepayableAdvanceLevies = Data.VerrekenbareVoorheffing.VVTTotaal.GetValueOrDefault();
            Data.BerekeningOverzicht.RepayableResearchDevelopment = Data.VerrekenbareVoorheffing.VBelastingkredietHuiidTijdperk.GetValueOrDefault();

            Data.BerekeningOverzicht.AP1Base = 0;
            Data.BerekeningOverzicht.AP2Base = 0;
            Data.BerekeningOverzicht.AP3Base = 0;
            Data.BerekeningOverzicht.AP4Base = 0;

            if (bels.Count > 0)
            {
                Data.BerekeningOverzicht.AP1Base = bels.Sum(b => b.VA1);
                Data.BerekeningOverzicht.AP2Base = bels.Sum(b => b.VA2);
                Data.BerekeningOverzicht.AP3Base = bels.Sum(b => b.VA3);
                Data.BerekeningOverzicht.AP4Base = bels.Sum(b => b.VA4);
            }

            #region Calc IPT

            Data.Aanslag.IPT2013 = 0;
            Data.Aanslag.IPT2014 = 0;
            Data.Aanslag.IPT2015 = 0;
            Data.BerekeningOverzicht.IPTBase = 0;
            Data.BerekeningOverzicht.IPTTotal = 0;
            Data.BerekeningOverzicht.IPTPerc = (decimal)0.6;
            if (Data.Aanslag.TotaalBedragVoorzieningen.GetValueOrDefault() > 0)
            {
                if (Data.Aanslag.AfzonderlijkeAanslagGespreid.GetValueOrDefault())
                {
                    Data.BerekeningOverzicht.IPTBase = Data.Aanslag.TotaalBedragVoorzieningen.GetValueOrDefault();
                    Data.Aanslag.IPT2013 = Math.Round(Data.Aanslag.TotaalBedragVoorzieningen.GetValueOrDefault() * (decimal)0.006, 2, MidpointRounding.AwayFromZero);
                    Data.Aanslag.IPT2014 = Math.Round(Data.Aanslag.TotaalBedragVoorzieningen.GetValueOrDefault() * (decimal)0.006, 2, MidpointRounding.AwayFromZero);
                    Data.Aanslag.IPT2015 = Math.Round(Data.Aanslag.TotaalBedragVoorzieningen.GetValueOrDefault() * (decimal)0.006, 2, MidpointRounding.AwayFromZero);

                    if (!Data.Aanslag.Taxable2015.GetValueOrDefault())
                    {
                        Data.Aanslag.IPT2014 += Data.Aanslag.IPT2015.GetValueOrDefault();
                        Data.Aanslag.IPT2015 = 0;
                        Data.BerekeningOverzicht.IPTPerc = (decimal)1.2;
                    }
                    Data.BerekeningOverzicht.IPTTotal = Data.Aanslag.IPT2014.GetValueOrDefault();
                }
                else
                {
                    Data.Aanslag.IPT2013 = Math.Round(Data.Aanslag.TotaalBedragVoorzieningen.GetValueOrDefault() * (decimal)0.0175, 2, MidpointRounding.AwayFromZero);
                }
            }
            #endregion

            // MEERWAARDEN OP AANDELEN TEGEN VERMINDERD TARIEF
            Guid mwaandelenid = new Guid("071EFA9C-4FA7-47BD-BCAB-DCB98F437651");
            Guid mwaandelenAfzTariefid = new Guid("3410F9C9-ED85-4BB0-87E6-8D91962210DF");

           
            
           
            Data.BerekeningOverzicht.AssessmentYear = worksheetset.DataContract.AssessmentYear;

            
            Data.BerekeningOverzicht.TotalTaxableReserves = Data.BelasteReserves.TDTotal.GetValueOrDefault();
            Data.BerekeningOverzicht.TotalTaxableReservesPlus = Data.BelasteReserves.OACapitalGainsOnShares.GetValueOrDefault()
                + Data.BelasteReserves.OACapitalGainsSharesReversal.GetValueOrDefault() + Data.BelasteReserves.OADefinitiveExemptionTaxShelter.GetValueOrDefault()
                + Data.BelasteReserves.OAExemptionRegionalPrem.GetValueOrDefault() + Data.BelasteReserves.OAFinalExemptionProfit.GetValueOrDefault()
                + Data.BelasteReserves.OAOthers.GetValueOrDefault();
            Data.BerekeningOverzicht.TotalTaxableReservesMinus = Data.BelasteReserves.OAInTheMin.GetValueOrDefault();

            Data.BerekeningOverzicht.TotalNonDeductibleTaxes = Data.VerworpenUitgaven.PDDisallowedCurrentIncomeTaxExpenses.GetValueOrDefault();
            Data.BerekeningOverzicht.TotalDissalowedExpensesWithoutNonDeductibleTax = Data.VerworpenUitgaven.DETotal.GetValueOrDefault();

            Data.BerekeningOverzicht.TaxableResult = Data.BerekeningOverzicht.StatAccountingResult.GetValueOrDefault()
                + Data.BerekeningOverzicht.TotalTaxableReserves.GetValueOrDefault()
                - Data.BerekeningOverzicht.TotalTaxableReservesPlus.GetValueOrDefault()
                + Data.BerekeningOverzicht.TotalTaxableReservesMinus.GetValueOrDefault()
                + Data.BerekeningOverzicht.TotalNonDeductibleTaxes.GetValueOrDefault()
                + Data.BerekeningOverzicht.TotalDissalowedExpensesWithoutNonDeductibleTax.GetValueOrDefault();

            if (Data.BerekeningOverzicht.TaxableResult.GetValueOrDefault() < 0)
                Data.BerekeningOverzicht.TaxableResult = 0;

            Data.BerekeningOverzicht.TotalDeductionLimit = Data.BelastbareBestZonderAftrek.TETotal.GetValueOrDefault();

            Data.BerekeningOverzicht.RemainingTaxableResult = Data.BerekeningOverzicht.TaxableResult.GetValueOrDefault() - Data.BerekeningOverzicht.TotalDeductionLimit.GetValueOrDefault();
            if (Data.BerekeningOverzicht.RemainingTaxableResult.GetValueOrDefault() < 0)
                Data.BerekeningOverzicht.RemainingTaxableResult = 0;



            Data.BerekeningOverzicht.TotalTaxTreaty = Data.OpdelingNaarOorsprong.IRTreatyForeignIncome.GetValueOrDefault(); 

            #region Fiscale aftrek herrekening
           
            decimal resterend = Data.BerekeningOverzicht.RemainingTaxableResult.GetValueOrDefault() - Data.BerekeningOverzicht.TotalTaxTreaty.GetValueOrDefault();

            Data.FiscaleAftrek.TotalNonTaxableElements = Data.FiscaleAftrek.TotalNonTaxableElements.HasValue ? Data.FiscaleAftrek.TotalNonTaxableElements.Value : 0;
            if (resterend < 0) resterend = 0;
            if (Data.FiscaleAftrek.TotalNonTaxableElements.Value > resterend)
            {
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "LARGERTHENGRONDSLAG", "FiscaleAftrek", Data.FiscaleAftrek.Id, "TotalNonTaxableElements");
            }
            else
            {
                resterend -= Data.FiscaleAftrek.TotalNonTaxableElements.Value;
                Data.FiscaleAftrek.PEBelgium = Data.FiscaleAftrek.PEBelgium.HasValue ? Data.FiscaleAftrek.PEBelgium.Value : 0;

                if ((resterend - Data.FiscaleAftrek.PEBelgium.Value) < 0)
                {
                    worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "LARGERTHENGRONDSLAG", "FiscaleAftrek", Data.FiscaleAftrek.Id, "PEBelgium");
                }
                else
                {

                    resterend -= Data.FiscaleAftrek.PEBelgium.Value;
                    Data.FiscaleAftrek.PIDBelgium = Data.FiscaleAftrek.PIDBelgium.HasValue ? Data.FiscaleAftrek.PIDBelgium.Value : 0;
                    if ((resterend - Data.FiscaleAftrek.PIDBelgium.Value) < 0)
                    {
                        worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "LARGERTHENGRONDSLAG", "FiscaleAftrek", Data.FiscaleAftrek.Id, "PIDBelgium");
                    }
                    else
                    {
                        resterend -= Data.FiscaleAftrek.PIDBelgium.Value;
                        Data.FiscaleAftrek.NIDBelgium = Data.FiscaleAftrek.NIDBelgium.HasValue ? Data.FiscaleAftrek.NIDBelgium.Value : 0;
                        if ((resterend - Data.FiscaleAftrek.NIDBelgium.Value) < 0)
                        {
                            worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "LARGERTHENGRONDSLAG", "FiscaleAftrek", Data.FiscaleAftrek.Id, "NIDBelgium");
                        }
                        else
                        {
                            resterend -= Data.FiscaleAftrek.NIDBelgium.Value;
                            Data.FiscaleAftrek.DTLBelgium = Data.FiscaleAftrek.DTLBelgium.HasValue ? Data.FiscaleAftrek.DTLBelgium.Value : 0;
                            if ((resterend - Data.FiscaleAftrek.DTLBelgium.Value) < 0)
                            {
                                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "LARGERTHENGRONDSLAG", "FiscaleAftrek", Data.FiscaleAftrek.Id, "DTLBelgium");
                            }
                            else
                            {
                                resterend -= Data.FiscaleAftrek.DTLBelgium.Value;
                                Data.FiscaleAftrek.IDBelgium = Data.FiscaleAftrek.IDBelgium.HasValue ? Data.FiscaleAftrek.IDBelgium.Value : 0;
                                if ((resterend - Data.FiscaleAftrek.IDBelgium.Value) < 0)
                                {
                                    worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "LARGERTHENGRONDSLAG", "FiscaleAftrek", Data.FiscaleAftrek.Id, "IDBelgium");
                                }
                                else
                                {
                                    resterend -= Data.FiscaleAftrek.IDBelgium.Value;
                                }
                            }
                        }
                    }
                }

            }
            Data.FiscaleAftrek.TotalFiscaleAftrek = ((((Data.FiscaleAftrek.TotalNonTaxableElements + Data.FiscaleAftrek.PEBelgium) + Data.FiscaleAftrek.PIDBelgium) + Data.FiscaleAftrek.NIDBelgium) + Data.FiscaleAftrek.DTLBelgium) + Data.FiscaleAftrek.IDBelgium + Data.FiscaleAftrek.NIDHistoryBelgium.GetValueOrDefault();



            #endregion


            Data.BerekeningOverzicht.TotalFiscalDeductions = Data.FiscaleAftrek.TotalFiscaleAftrek.GetValueOrDefault();
            /* #166
            Data.BerekeningOverzicht.RemainingFiscalProfit = Data.BerekeningOverzicht.RemainingTaxableResult.GetValueOrDefault()
                    - Data.BerekeningOverzicht.TotalTaxTreaty.GetValueOrDefault()
                    - Data.BerekeningOverzicht.TotalFiscalDeductions.GetValueOrDefault();
             */
            if (Data.BerekeningOverzicht.TotalTaxTreaty.GetValueOrDefault() > 0)
            {
                Data.BerekeningOverzicht.RemainingFiscalProfit = Data.BerekeningOverzicht.RemainingTaxableResult.GetValueOrDefault()
                    - Data.BerekeningOverzicht.TotalTaxTreaty.GetValueOrDefault()
                    - Data.BerekeningOverzicht.TotalFiscalDeductions.GetValueOrDefault();
            }
            else
            {
                Data.BerekeningOverzicht.RemainingFiscalProfit = Data.BerekeningOverzicht.RemainingTaxableResult.GetValueOrDefault()
                    - Data.BerekeningOverzicht.TotalFiscalDeductions.GetValueOrDefault();
            }

            if (Data.BerekeningOverzicht.RemainingFiscalProfit.GetValueOrDefault() < 0)
                Data.BerekeningOverzicht.RemainingFiscalProfit = 0;

            Data.BerekeningOverzicht.CommonRateRemainingFiscalProfit = Data.BerekeningOverzicht.RemainingFiscalProfit;
            Data.BerekeningOverzicht.CommonRateDeductionLimit = Data.BerekeningOverzicht.TotalDeductionLimit.GetValueOrDefault()
                - Data.BelastbareBestZonderAftrek.TECapitalGains0412.GetValueOrDefault()
                - Data.BelastbareBestZonderAftrek.TECapitalAgriCulture.GetValueOrDefault();


            decimal? meerwaardenAandelen = worksheetset.MeerwaardeAandelenApp.Data.MeerwaardeAandelenLijst.Where(d => d.Resultaat != null && d.Resultaat.Id == mwaandelenid).Sum(d => d.Saldo).GetValueOrDefault();

            decimal? vaaDeelAuto = Data.BelastbareBestZonderAftrek.TEVaaCar.GetValueOrDefault();
           

            decimal belastbareGrondslagZonderVAA =  Data.BerekeningOverzicht.CommonRateRemainingFiscalProfit.GetValueOrDefault()
                + Data.BerekeningOverzicht.CommonRateDeductionLimit.GetValueOrDefault()
                - vaaDeelAuto.GetValueOrDefault();

            if (belastbareGrondslagZonderVAA <= 0)
            {
                meerwaardenAandelen = 0;
            }
            if (belastbareGrondslagZonderVAA < meerwaardenAandelen.GetValueOrDefault())
            {
                meerwaardenAandelen = belastbareGrondslagZonderVAA;
            }
            Data.BerekeningOverzicht.CommonRateCapitalGains25 = meerwaardenAandelen.GetValueOrDefault();

            //Data.BerekeningOverzicht.CommonRateCapitalGains25 = worksheetset.MeerwaardeAandelenApp.Data.MeerwaardeAandelenLijst.Where(d => d.Resultaat != null && d.Resultaat.Id == mwaandelenid).Sum(d => d.Saldo).GetValueOrDefault();
            Data.BerekeningOverzicht.CommonRateCapitalGains25 = worksheetset.MeerwaardeAandelenApp.Data.MeerwaardeAandelenLijst.Where(d => d.Resultaat != null && d.Resultaat.Id == mwaandelenid).Sum(d => d.Saldo).GetValueOrDefault() > meerwaardenAandelen.GetValueOrDefault() ? meerwaardenAandelen.GetValueOrDefault() : worksheetset.MeerwaardeAandelenApp.Data.MeerwaardeAandelenLijst.Where(d => d.Resultaat != null && d.Resultaat.Id == mwaandelenid).Sum(d => d.Saldo).GetValueOrDefault();

            Data.BerekeningOverzicht.CapitalGainsShares25 = Data.BerekeningOverzicht.CommonRateCapitalGains25.GetValueOrDefault();

            Data.BerekeningOverzicht.CapitalGainsShares25Result = Math.Round((Data.BerekeningOverzicht.CapitalGainsShares25.GetValueOrDefault() / 100) * (Data.BerekeningOverzicht.CapitalGainsShares25Perc.GetValueOrDefault()), 2, MidpointRounding.AwayFromZero);

           

            Data.BerekeningOverzicht.CommonRateResult = Data.BerekeningOverzicht.CommonRateRemainingFiscalProfit.GetValueOrDefault()
                + Data.BerekeningOverzicht.CommonRateDeductionLimit.GetValueOrDefault()
                - Data.BerekeningOverzicht.CommonRateCapitalGains25.GetValueOrDefault();

            if (Data.BerekeningOverzicht.CommonRateResult.GetValueOrDefault() < 0)
                Data.BerekeningOverzicht.CommonRateResult = 0;

            #region Fairness Tax calc

            Data.FairnessTax.TaxableResult = Data.BerekeningOverzicht.CommonRateResult;
            Data.FairnessTax.TotalRestriction = Data.FairnessTax.NidApplied.GetValueOrDefault() + Data.FairnessTax.PreviousLosses.GetValueOrDefault();
            Data.FairnessTax.ResultFirstOperation = Data.BerekeningOverzicht.TaxableResult;
            Data.FairnessTax.FairnessTaxRestrictionPerc = 0;


            
            Data.FairnessTax.DiffDividendTaxable = 0;
            Data.FairnessTax.FairnessTaxBase = 0;
            Data.FairnessTax.FairnessTaxResult = 0;

            if (!worksheetset.Settings.CurrentPeriodSmall)
            {

                decimal taxrestperc = 0;
                if (Data.FairnessTax.ResultFirstOperation.GetValueOrDefault() > 0 && Data.FairnessTax.TotalRestriction.GetValueOrDefault() > 0)
                {
                    taxrestperc = Math.Round((Data.FairnessTax.TotalRestriction.GetValueOrDefault() / Data.FairnessTax.ResultFirstOperation.GetValueOrDefault()), 4, MidpointRounding.AwayFromZero);
                    Data.FairnessTax.FairnessTaxRestrictionPerc = Math.Round(taxrestperc * 100, 2, MidpointRounding.AwayFromZero);
                }

                if (Data.FairnessTax.Dividenden.GetValueOrDefault() > 0)
                {
                    Data.FairnessTax.DiffDividendTaxable = Data.FairnessTax.Dividenden.GetValueOrDefault() - Data.FairnessTax.TaxableResult.GetValueOrDefault();

                }


                if (Data.FairnessTax.DiffDividendTaxable.GetValueOrDefault() > 0 && taxrestperc > 0)
                {
                    Data.FairnessTax.FairnessTaxBase = (Data.FairnessTax.DiffDividendTaxable.GetValueOrDefault() - Data.FairnessTax.PreviousDividends.GetValueOrDefault()) * taxrestperc;

                }


                if (Data.FairnessTax.FairnessTaxBase.GetValueOrDefault() < 0) Data.FairnessTax.FairnessTaxBase = 0;
                


                Data.FairnessTax.FairnessTaxResult = Math.Round(Data.FairnessTax.FairnessTaxBase.GetValueOrDefault() * (Data.FairnessTax.FairnessTaxPerc.GetValueOrDefault() / 100), 2, MidpointRounding.AwayFromZero);
            }

        

            Data.BerekeningOverzicht.FairnessTaxBase = Data.FairnessTax.FairnessTaxBase.GetValueOrDefault();
            Data.BerekeningOverzicht.FairnessTaxPerc = Data.FairnessTax.FairnessTaxPerc.GetValueOrDefault();
            Data.BerekeningOverzicht.FairnessTaxTotal = Data.FairnessTax.FairnessTaxResult.GetValueOrDefault();
            Data.BerekeningOverzicht.BaseForIncreaseFairnessTax = Data.FairnessTax.FairnessTaxResult.GetValueOrDefault();

            #endregion


            Data.BerekeningOverzicht.ExitTarifResult = Math.Round(Data.BerekeningOverzicht.ExitTarif.GetValueOrDefault() * (Data.BerekeningOverzicht.ExitTarifPerc.GetValueOrDefault() / 100), 2, MidpointRounding.AwayFromZero);
            Data.BerekeningOverzicht.CapitalGainsShares0412 = Data.BelastbareBestZonderAftrek.TECapitalGains0412.GetValueOrDefault();
            Data.BerekeningOverzicht.CapitalGainsShares0412Result = Math.Round((Data.BerekeningOverzicht.CapitalGainsShares0412.GetValueOrDefault() / 100) * Data.BerekeningOverzicht.CapitalGainsShares0412Perc.GetValueOrDefault(), 2, MidpointRounding.AwayFromZero);

            decimal verminderdBovenGrens = Data.BerekeningOverzicht.VerminderdtariefStap3Base.GetValueOrDefault();


            Data.BerekeningOverzicht.VerminderdtariefStap1 = 0;

            Data.BerekeningOverzicht.VerminderdtariefStap2 = 0;

            Data.BerekeningOverzicht.VerminderdtariefStap3 = 0;
            Data.BerekeningOverzicht.EenduidigTariefBedrag = 0;
            // Added fbo 25082014
            Data.BerekeningOverzicht.EenduidigTariefTot = 0;

            // added fbo 270821014
            if ((((Data.Tarief.BezitAandelenMeerDan50Percent == true || Data.Tarief.BezitAandelenDoorVennootschappen == true) || Data.Tarief.UitgekeerdDividendHogerDan13 == true) || Data.Tarief.ToegekendeBezoldiging == true) || (Data.BerekeningOverzicht.CommonRateResult.GetValueOrDefault() > verminderdBovenGrens))
            {
                Data.BerekeningOverzicht.Verminderd = false;
            }
            else
            {
                Data.BerekeningOverzicht.Verminderd = true;
            }

            if (Data.BerekeningOverzicht.CommonRateResult.GetValueOrDefault() > 0)
            {

                if ((((Data.Tarief.BezitAandelenMeerDan50Percent == true || Data.Tarief.BezitAandelenDoorVennootschappen == true) || Data.Tarief.UitgekeerdDividendHogerDan13 == true) || Data.Tarief.ToegekendeBezoldiging == true) || (Data.BerekeningOverzicht.CommonRateResult.GetValueOrDefault() > verminderdBovenGrens))
                {
                    Data.BerekeningOverzicht.VerminderdtariefStap1Base = 0;
                    Data.BerekeningOverzicht.VerminderdtariefStap1 = 0;
                    Data.BerekeningOverzicht.VerminderdtariefStap2Base = 0;
                    Data.BerekeningOverzicht.VerminderdtariefStap2 = 0;
                    Data.BerekeningOverzicht.VerminderdtariefStap3Base = 0;
                    Data.BerekeningOverzicht.VerminderdtariefStap3 = 0;

                    Data.BerekeningOverzicht.EenduidigTariefBedrag = Data.BerekeningOverzicht.CommonRateResult.GetValueOrDefault();

                    Data.BerekeningOverzicht.EenduidigTariefTot = Math.Round(Data.BerekeningOverzicht.EenduidigTariefBedrag.Value * (Data.BerekeningOverzicht.EenduidigTariefPerc.Value / 100), 2, MidpointRounding.AwayFromZero);
                    Data.BerekeningOverzicht.Verminderd = false;
                }
                else
                {
                    Data.BerekeningOverzicht.Verminderd = true;
                    Data.BerekeningOverzicht.EenduidigTariefBedrag = 0;
                    Data.BerekeningOverzicht.EenduidigTariefTot = 0;
                    decimal remainingTaxNormalTarif = Data.BerekeningOverzicht.CommonRateResult.GetValueOrDefault();

                    if (remainingTaxNormalTarif < Data.BerekeningOverzicht.VerminderdtariefStap1Base)
                    {
                        Data.BerekeningOverzicht.VerminderdtariefStap1Base = remainingTaxNormalTarif;
                        Data.BerekeningOverzicht.VerminderdtariefStap1 = Math.Round(Data.BerekeningOverzicht.VerminderdtariefStap1Base.Value * (Data.BerekeningOverzicht.VerminderdtariefStap1Perc.Value / 100), 2, MidpointRounding.AwayFromZero);
                        Data.BerekeningOverzicht.VerminderdtariefStap2 = 0;
                        Data.BerekeningOverzicht.VerminderdtariefStap3 = 0;
                        Data.BerekeningOverzicht.VerminderdtariefStap2Base = 0;
                        Data.BerekeningOverzicht.VerminderdtariefStap3Base = 0;
                    }
                    else
                    {
                        Data.BerekeningOverzicht.VerminderdtariefStap1 = Math.Round(Data.BerekeningOverzicht.VerminderdtariefStap1Base.Value * (Data.BerekeningOverzicht.VerminderdtariefStap1Perc.Value / 100), 2, MidpointRounding.AwayFromZero);
                        if (remainingTaxNormalTarif < Data.BerekeningOverzicht.VerminderdtariefStap2Base)
                        {
                            Data.BerekeningOverzicht.VerminderdtariefStap2Base = remainingTaxNormalTarif - Data.BerekeningOverzicht.VerminderdtariefStap1Base;
                            Data.BerekeningOverzicht.VerminderdtariefStap2 = Math.Round(Data.BerekeningOverzicht.VerminderdtariefStap2Base.Value * (Data.BerekeningOverzicht.VerminderdtariefStap2Perc.Value / 100), 2, MidpointRounding.AwayFromZero);

                            Data.BerekeningOverzicht.VerminderdtariefStap3 = 0;
                            Data.BerekeningOverzicht.VerminderdtariefStap3Base = 0;
                        }
                        else
                        {
                            Data.BerekeningOverzicht.VerminderdtariefStap2Base = Data.BerekeningOverzicht.VerminderdtariefStap2Base - Data.BerekeningOverzicht.VerminderdtariefStap1Base;
                            Data.BerekeningOverzicht.VerminderdtariefStap2 = Math.Round(Data.BerekeningOverzicht.VerminderdtariefStap2Base.Value * (Data.BerekeningOverzicht.VerminderdtariefStap2Perc.Value / 100), 2, MidpointRounding.AwayFromZero);
                            if (remainingTaxNormalTarif < Data.BerekeningOverzicht.VerminderdtariefStap3Base)
                            {
                                Data.BerekeningOverzicht.VerminderdtariefStap3Base = (remainingTaxNormalTarif - Data.BerekeningOverzicht.VerminderdtariefStap2Base) - Data.BerekeningOverzicht.VerminderdtariefStap1Base;
                                Data.BerekeningOverzicht.VerminderdtariefStap3 = Math.Round(Data.BerekeningOverzicht.VerminderdtariefStap3Base.Value * (Data.BerekeningOverzicht.VerminderdtariefStap3Perc.Value / 100), 2, MidpointRounding.AwayFromZero);

                            }
                            else
                            {
                                Data.BerekeningOverzicht.EenduidigTariefBedrag = remainingTaxNormalTarif;
                                Data.BerekeningOverzicht.EenduidigTariefTot = Math.Round(Data.BerekeningOverzicht.EenduidigTariefBedrag.Value * (Data.BerekeningOverzicht.EenduidigTariefPerc.Value / 100), 2, MidpointRounding.AwayFromZero);

                            }
                        }
                    }
                }
            }

            Data.BerekeningOverzicht.CorporateTaxBeforeRepayable = Data.BerekeningOverzicht.EenduidigTariefTot.GetValueOrDefault()
                + Data.BerekeningOverzicht.VerminderdtariefStap1.GetValueOrDefault() + Data.BerekeningOverzicht.VerminderdtariefStap2.GetValueOrDefault()
                + Data.BerekeningOverzicht.VerminderdtariefStap3.GetValueOrDefault() + Data.BerekeningOverzicht.ExitTarifResult.GetValueOrDefault()
                + Data.BerekeningOverzicht.CapitalGainsShares25Result.GetValueOrDefault() + Data.BerekeningOverzicht.CapitalGainsShares0412Result.GetValueOrDefault();


            Data.BerekeningOverzicht.RepayableTotal = 0;
            //If the corporate tax is higher than the positive nonrepayableAdvanceLevies 
            if (Data.BerekeningOverzicht.CorporateTaxBeforeRepayable.GetValueOrDefault() >= Math.Abs(Data.BerekeningOverzicht.NonRepayableAdvanceLevies.GetValueOrDefault()))
            {
                //add the non repayable advance levies as being the lower value as you cannot ever return more from the goverment than the set tax. The rest is lost.
                Data.BerekeningOverzicht.RepayableTotal += Math.Abs(Data.BerekeningOverzicht.NonRepayableAdvanceLevies.GetValueOrDefault());
            }
            else
            {
                //add the corporate tax as being the lower value as you cannot ever return more from the goverment than the set tax. The rest is lost.
                Data.BerekeningOverzicht.RepayableTotal += Data.BerekeningOverzicht.CorporateTaxBeforeRepayable.GetValueOrDefault();
            }

            //The following are always repayable and therefore are always added
            Data.BerekeningOverzicht.RepayableTotal += Data.BerekeningOverzicht.RepayableAdvanceLevies.GetValueOrDefault();
            Data.BerekeningOverzicht.RepayableTotal += Data.BerekeningOverzicht.RepayableResearchDevelopment.GetValueOrDefault();

            /*
            Data.BerekeningOverzicht.RepayableTotal = Data.BerekeningOverzicht.NonRepayableAdvanceLevies.GetValueOrDefault() + Data.BerekeningOverzicht.RepayableAdvanceLevies.GetValueOrDefault()
               + Data.BerekeningOverzicht.RepayableResearchDevelopment.GetValueOrDefault();
            if (Data.BerekeningOverzicht.CorporateTaxBeforeRepayable.GetValueOrDefault() - Data.BerekeningOverzicht.NonRepayableAdvanceLevies.GetValueOrDefault() > 0)
            {
                Data.BerekeningOverzicht.RepayableTotal = Data.BerekeningOverzicht.NonRepayableAdvanceLevies.GetValueOrDefault() + Data.BerekeningOverzicht.RepayableAdvanceLevies.GetValueOrDefault()
                + Data.BerekeningOverzicht.RepayableResearchDevelopment.GetValueOrDefault();
            }
            else {
                Data.BerekeningOverzicht.RepayableTotal = Data.BerekeningOverzicht.RepayableAdvanceLevies.GetValueOrDefault() + Data.BerekeningOverzicht.RepayableResearchDevelopment.GetValueOrDefault();
            }*/
            

            Data.BerekeningOverzicht.CorporateTaxAfterRepayable = Data.BerekeningOverzicht.CorporateTaxBeforeRepayable.GetValueOrDefault()
                - Data.BerekeningOverzicht.RepayableTotal.GetValueOrDefault();

            Data.BerekeningOverzicht.RetributionResearchDevelopmentBase = 0;
            Data.BerekeningOverzicht.RetributionResearchDevelopmentTotal = 0;
            if (Data.Aanslag.TerugbetalingBelastingskrediet.GetValueOrDefault() > 0)
            {
                Data.BerekeningOverzicht.RetributionResearchDevelopmentBase = Data.Aanslag.TerugbetalingBelastingskrediet.GetValueOrDefault();
                Data.BerekeningOverzicht.RetributionResearchDevelopmentTotal = Data.BerekeningOverzicht.RetributionResearchDevelopmentBase.GetValueOrDefault()
                    * (Data.BerekeningOverzicht.RetributionResearchDevelopmentPerc.GetValueOrDefault() / 100);
            }

            Data.BerekeningOverzicht.BaseForIncrease = Data.BerekeningOverzicht.CorporateTaxAfterRepayable.GetValueOrDefault()
                + Data.BerekeningOverzicht.RetributionResearchDevelopmentTotal.GetValueOrDefault()
                + Data.BerekeningOverzicht.BaseForIncreaseFairnessTax.GetValueOrDefault();

            if (!Data.BerekeningOverzicht.BaseForIncrease.HasValue) Data.BerekeningOverzicht.BaseForIncrease = 0;

            if (Data.BerekeningOverzicht.BaseForIncrease < 0)
            {
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Warning, "BELOWZERO", "BerekeningOverzicht", Data.BerekeningOverzicht.Id, "BaseForIncrease", new object[] { Data.BerekeningOverzicht.BaseForIncrease.ToString() });
                Data.BerekeningOverzicht.BaseForIncrease = 0;
            }


            Data.BerekeningOverzicht.BaseForIncreaseTotal = Math.Round(Data.BerekeningOverzicht.BaseForIncrease.GetValueOrDefault() * (Data.BerekeningOverzicht.BaseForIncreasePerc.GetValueOrDefault() / 100), 2, MidpointRounding.AwayFromZero);


            Data.BerekeningOverzicht.AP1Total = Math.Round(Data.BerekeningOverzicht.AP1Base.Value * (Data.BerekeningOverzicht.AP1Percentage.Value / 100), 2, MidpointRounding.AwayFromZero);
            Data.BerekeningOverzicht.AP2Total = Math.Round(Data.BerekeningOverzicht.AP2Base.Value * (Data.BerekeningOverzicht.AP2Percentage.Value / 100), 2, MidpointRounding.AwayFromZero);
            Data.BerekeningOverzicht.AP3Total = Math.Round(Data.BerekeningOverzicht.AP3Base.Value * (Data.BerekeningOverzicht.AP3Percentage.Value / 100), 2, MidpointRounding.AwayFromZero);
            Data.BerekeningOverzicht.AP4Total = Math.Round(Data.BerekeningOverzicht.AP4Base.Value * (Data.BerekeningOverzicht.AP4Percentage.Value / 100), 2, MidpointRounding.AwayFromZero);

            Data.BerekeningOverzicht.APBaseTotal = ((Data.BerekeningOverzicht.AP1Base.GetValueOrDefault() + Data.BerekeningOverzicht.AP2Base.GetValueOrDefault()) + Data.BerekeningOverzicht.AP3Base.GetValueOrDefault()) + Data.BerekeningOverzicht.AP4Base.GetValueOrDefault();

            Data.BerekeningOverzicht.APTotal = ((Data.BerekeningOverzicht.AP1Total.GetValueOrDefault() + Data.BerekeningOverzicht.AP2Total.GetValueOrDefault()) + Data.BerekeningOverzicht.AP3Total.GetValueOrDefault()) + Data.BerekeningOverzicht.AP4Total.GetValueOrDefault();

            Data.BerekeningOverzicht.AdvancePayments = Data.BerekeningOverzicht.APBaseTotal;

            Data.BerekeningOverzicht.Test1Percent = Data.BerekeningOverzicht.BaseForIncrease.GetValueOrDefault() * (decimal)0.01;

            if (((Data.BerekeningOverzicht.BaseForIncreaseTotal.GetValueOrDefault() - Data.BerekeningOverzicht.APTotal.GetValueOrDefault()) < Data.BerekeningOverzicht.Test1Percent.GetValueOrDefault())
                || ((Data.BerekeningOverzicht.BaseForIncreaseTotal.GetValueOrDefault() - Data.BerekeningOverzicht.APTotal.GetValueOrDefault()) <= 40))
            {
                Data.BerekeningOverzicht.BaseForIncreaseApplied = 0;
            }
            else
            {
                Data.BerekeningOverzicht.BaseForIncreaseApplied = Data.BerekeningOverzicht.BaseForIncreaseTotal.GetValueOrDefault() - Data.BerekeningOverzicht.APTotal.GetValueOrDefault();
            }
                        
            if (jongVen)
                Data.BerekeningOverzicht.BaseForIncreaseApplied = 0;

            Data.BerekeningOverzicht.TaxResult = Data.BerekeningOverzicht.CorporateTaxBeforeRepayable.GetValueOrDefault()
                + Data.BerekeningOverzicht.BaseForIncreaseApplied.GetValueOrDefault()
                - Data.BerekeningOverzicht.AdvancePayments.GetValueOrDefault();

            if (Data.BerekeningOverzicht.CorporateTaxBeforeRepayable.GetValueOrDefault() - Data.BerekeningOverzicht.NonRepayableAdvanceLevies.GetValueOrDefault() > 0)
            {
                Data.BerekeningOverzicht.TaxResult -= Data.BerekeningOverzicht.NonRepayableAdvanceLevies.GetValueOrDefault();
            }
            
            else if (Data.BerekeningOverzicht.NonRepayableAdvanceLevies.GetValueOrDefault() > 0)
            {
                Data.BerekeningOverzicht.TaxResult -= Data.BerekeningOverzicht.NonRepayableAdvanceLevies.GetValueOrDefault();
                if (Data.BerekeningOverzicht.TaxResult.GetValueOrDefault() < 0) Data.BerekeningOverzicht.TaxResult = 0;
            }
            
            Data.BerekeningOverzicht.TaxResult = Data.BerekeningOverzicht.TaxResult.GetValueOrDefault() - Data.BerekeningOverzicht.RepayableAdvanceLevies.GetValueOrDefault() - Data.BerekeningOverzicht.RepayableResearchDevelopment.GetValueOrDefault();



            Data.BerekeningOverzicht.KapitaalEnInterestsubsidiesResult = 0;
            Data.BerekeningOverzicht.KapitaalEnInterestsubsidies = 0;
            if (Data.Aanslag.KapitaalEnInterestsubsidies.GetValueOrDefault() > 0)
            {
                Data.BerekeningOverzicht.KapitaalEnInterestsubsidies = Data.Aanslag.KapitaalEnInterestsubsidies.GetValueOrDefault();
                Data.BerekeningOverzicht.KapitaalEnInterestsubsidiesResult = Data.BerekeningOverzicht.KapitaalEnInterestsubsidies.GetValueOrDefault() * (Data.BerekeningOverzicht.KapitaalEnInterestsubsidiesPerc.GetValueOrDefault() / 100);
            
            }

            Data.BerekeningOverzicht.TaxResult += Data.BerekeningOverzicht.KapitaalEnInterestsubsidiesResult.GetValueOrDefault();


            // afzonderlijke aanslagen
            //Niet-verantwoorde kosten of voordelen van alle aard, verdoken meerwinsten en financiŽle voordelen of voordelen van alle aard
            Data.BerekeningOverzicht.UnjustifiedExpensesHiddenSurplusBase = 0;
            Data.BerekeningOverzicht.UnjustifiedExpensesHiddenSurplusTotal = 0;
            if (Data.Aanslag.NietVerantwoordeKosten.GetValueOrDefault() > 0) {
                Data.BerekeningOverzicht.UnjustifiedExpensesHiddenSurplusBase = Data.Aanslag.NietVerantwoordeKosten.GetValueOrDefault();
                Data.BerekeningOverzicht.UnjustifiedExpensesHiddenSurplusTotal = Data.BerekeningOverzicht.UnjustifiedExpensesHiddenSurplusBase.GetValueOrDefault() 
                    * (Data.BerekeningOverzicht.UnjustifiedExpensesHiddenSurplusPerc.GetValueOrDefault() / 100);

            }
            Data.BerekeningOverzicht.TaxResult += Data.BerekeningOverzicht.UnjustifiedExpensesHiddenSurplusTotal.GetValueOrDefault();

            //Afzonderlijke aanslag van de belaste reserves ten name van erkende kredietinstellingen, tegen het tarief van 34 %
            Data.BerekeningOverzicht.SeparateAssessmentTaxedReserves3400Base = 0;
            Data.BerekeningOverzicht.SeparateAssessmentTaxedReserves3400Total = 0;
            if (Data.Aanslag.AfzonderlijkeAanslag34.GetValueOrDefault() > 0)
            {
                Data.BerekeningOverzicht.SeparateAssessmentTaxedReserves3400Base = Data.Aanslag.AfzonderlijkeAanslag34.GetValueOrDefault();
                Data.BerekeningOverzicht.SeparateAssessmentTaxedReserves3400Total = Data.BerekeningOverzicht.SeparateAssessmentTaxedReserves3400Base.GetValueOrDefault()
                    * (Data.BerekeningOverzicht.SeparateAssessmentTaxedReserves3400Perc.GetValueOrDefault() / 100);
              
            }
            Data.BerekeningOverzicht.TaxResult += Data.BerekeningOverzicht.SeparateAssessmentTaxedReserves3400Total.GetValueOrDefault();


            //Afzonderlijke aanslag van de belaste reserves ten name van erkende kredietinstellingen, tegen het tarief van 28 %
            Data.BerekeningOverzicht.SeparateAssessmentTaxedReserves2800Base = 0;
            Data.BerekeningOverzicht.SeparateAssessmentTaxedReserves2800Total = 0;
            if (Data.Aanslag.AfzonderlijkeAanslag28.GetValueOrDefault() > 0)
            {
                Data.BerekeningOverzicht.SeparateAssessmentTaxedReserves2800Base = Data.Aanslag.AfzonderlijkeAanslag28.GetValueOrDefault();
                Data.BerekeningOverzicht.SeparateAssessmentTaxedReserves2800Total = Data.BerekeningOverzicht.SeparateAssessmentTaxedReserves2800Base.GetValueOrDefault()
                    * (Data.BerekeningOverzicht.SeparateAssessmentTaxedReserves2800Perc.GetValueOrDefault() / 100);

            }
            Data.BerekeningOverzicht.TaxResult += Data.BerekeningOverzicht.SeparateAssessmentTaxedReserves2800Total.GetValueOrDefault();

            //Afzonderlijke aanslag van de uitgekeerde dividenden ten name van vennootschappen die krediet voor ambachtsoutillage mogen verstrekken en vennootschappen voor huisvesting
            Data.BerekeningOverzicht.SeparateAssessmentDividendsPaidBase = 0;
            Data.BerekeningOverzicht.SeparateAssessmentDividendsPaidTotal = 0;
            if (Data.Aanslag.AfzonderlijkeAanslagDividenden.GetValueOrDefault() > 0)
            {
                Data.BerekeningOverzicht.SeparateAssessmentDividendsPaidBase = Data.Aanslag.AfzonderlijkeAanslagDividenden.GetValueOrDefault();
                Data.BerekeningOverzicht.SeparateAssessmentDividendsPaidTotal = Data.BerekeningOverzicht.SeparateAssessmentDividendsPaidBase.GetValueOrDefault()
                    * (Data.BerekeningOverzicht.SeparateAssessmentDividendsPaidPerc.GetValueOrDefault() / 100);
            }
            Data.BerekeningOverzicht.TaxResult += Data.BerekeningOverzicht.SeparateAssessmentDividendsPaidTotal.GetValueOrDefault();

            // AFZONDERLIJKE AANSLAG FAIRNESS TAX (already calculated, not yet included)
            Data.BerekeningOverzicht.TaxResult += Data.BerekeningOverzicht.FairnessTaxTotal.GetValueOrDefault();

            //Afzonderlijke aanslag in het kader van de overgangsregeling van verlaagde roerende voorheffing op de uitkering van belaste reserves
            Data.BerekeningOverzicht.SeparateAssessmentTaxPaidTaxedReservesBase = 0;
            Data.BerekeningOverzicht.SeparateAssessmentTaxPaidTaxedReservesTotal = 0;
            if (Data.Aanslag.AfzonderlijkeAanslagRV.GetValueOrDefault() > 0)
            {
                Data.BerekeningOverzicht.SeparateAssessmentTaxPaidTaxedReservesBase = Data.Aanslag.AfzonderlijkeAanslagRV.GetValueOrDefault();
                Data.BerekeningOverzicht.SeparateAssessmentTaxPaidTaxedReservesTotal = Data.BerekeningOverzicht.SeparateAssessmentTaxPaidTaxedReservesBase.GetValueOrDefault()
                    * (Data.BerekeningOverzicht.SeparateAssessmentTaxPaidTaxedReservesPerc.GetValueOrDefault() / 100);
            }
            Data.BerekeningOverzicht.TaxResult += Data.BerekeningOverzicht.SeparateAssessmentTaxPaidTaxedReservesTotal.GetValueOrDefault();

            // Bijzondere Aanslagen

            Data.BerekeningOverzicht.DistributionCompanyAssets3300Base = 0;
            Data.BerekeningOverzicht.DistributionCompanyAssets3300Total = 0;
            if (Data.Aanslag.VerdelingVermogen33.GetValueOrDefault() > 0)
            {
                Data.BerekeningOverzicht.DistributionCompanyAssets3300Base = Data.Aanslag.VerdelingVermogen33.GetValueOrDefault();
                Data.BerekeningOverzicht.DistributionCompanyAssets3300Total = Data.BerekeningOverzicht.DistributionCompanyAssets3300Base.GetValueOrDefault()
                    * (Data.BerekeningOverzicht.DistributionCompanyAssets3300Perc.GetValueOrDefault() / 100);

            }
            Data.BerekeningOverzicht.TaxResult += Data.BerekeningOverzicht.DistributionCompanyAssets3300Total.GetValueOrDefault();

            Data.BerekeningOverzicht.DistributionCompanyAssets1650Base = 0;
            Data.BerekeningOverzicht.DistributionCompanyAssets1650Total = 0;
            if (Data.Aanslag.VerdelingVermogen16.GetValueOrDefault() > 0)
            {
                Data.BerekeningOverzicht.DistributionCompanyAssets1650Base = Data.Aanslag.VerdelingVermogen16.GetValueOrDefault();
                Data.BerekeningOverzicht.DistributionCompanyAssets1650Total = Data.BerekeningOverzicht.DistributionCompanyAssets1650Base.GetValueOrDefault()
                    * (Data.BerekeningOverzicht.DistributionCompanyAssets1650Perc.GetValueOrDefault() / 100);
            }
            Data.BerekeningOverzicht.TaxResult += Data.BerekeningOverzicht.DistributionCompanyAssets1650Total.GetValueOrDefault();

            Data.BerekeningOverzicht.BenefitsAllKindCompanyLiquidationBase = 0;
            Data.BerekeningOverzicht.BenefitsAllKindCompanyLiquidationTotal = 0;
            if (Data.Aanslag.VAAVennootschappen.GetValueOrDefault() > 0)
            {
                Data.BerekeningOverzicht.BenefitsAllKindCompanyLiquidationBase = Data.Aanslag.VAAVennootschappen.GetValueOrDefault();
                Data.BerekeningOverzicht.BenefitsAllKindCompanyLiquidationTotal = Data.BerekeningOverzicht.BenefitsAllKindCompanyLiquidationBase.GetValueOrDefault()
                    * (Data.BerekeningOverzicht.BenefitsAllKindCompanyLiquidationPerc.GetValueOrDefault() / 100);
        
            }
            Data.BerekeningOverzicht.TaxResult += Data.BerekeningOverzicht.BenefitsAllKindCompanyLiquidationTotal.GetValueOrDefault();

            // aanvullend diamant
            Data.BerekeningOverzicht.AdditionalDutiesDiamondTradersBase = 0;
            Data.BerekeningOverzicht.AdditionalDutiesDiamondTradersTotal = 0;
            if (Data.Aanslag.AanvullendeHeffingDiamant.GetValueOrDefault() > 0)
            {
                Data.BerekeningOverzicht.AdditionalDutiesDiamondTradersBase = Data.Aanslag.AanvullendeHeffingDiamant.GetValueOrDefault();
                Data.BerekeningOverzicht.AdditionalDutiesDiamondTradersTotal = Data.BerekeningOverzicht.AdditionalDutiesDiamondTradersBase.GetValueOrDefault()
                    * (Data.BerekeningOverzicht.AdditionalDutiesDiamondTradersPerc.GetValueOrDefault() / 100);
            }
            Data.BerekeningOverzicht.TaxResult += Data.BerekeningOverzicht.AdditionalDutiesDiamondTradersTotal.GetValueOrDefault();


            //Terugbetaling van een gedeelte van het voorheen verleende belastingkrediet voor onderzoek en ontwikkeling
  
            Data.BerekeningOverzicht.TaxResult += Data.BerekeningOverzicht.RetributionResearchDevelopmentTotal.GetValueOrDefault();

            
            // IPT ALREADY CALCULATED, JUST ADD TO TAXRESULT
            Data.BerekeningOverzicht.TaxResult += Data.BerekeningOverzicht.IPTTotal.GetValueOrDefault();


            Data.LastUpdated = DateTime.Now;

        }

        #region AddUpdateDelete

        //public void UpdateOverzicht(EY.com.eBook.RuleEngine.AY2014.BerekeningVenBAppTypes.General entity, ref WorksheetSetData worksheetset)
        //{
        //    EY.com.eBook.RuleEngine.AY2014.BerekeningVenBAppTypes.General original = Data.Overzicht;

        //    entity = Validate(entity, "Overzicht", ref worksheetset);
        //    if (entity.IsValid)
        //    {

        //        Data.Overzicht = entity;
        //        Data.LastUpdated = DateTime.Now;
        //        entity.LastUpdated = DateTime.Now;
        //    }
        //}

        public void UpdateBelasteReserves(EY.com.eBook.RuleEngine.AY2014.BerekeningVenBAppTypes.BelasteReservesItem entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2014.BerekeningVenBAppTypes.BelasteReservesItem original = Data.BelasteReserves;

            entity = Validate(entity, "BelasteReserves", ref worksheetset);
            if (entity.IsValid)
            {

                Data.BelasteReserves = entity;
                Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
            }
        }

        public void UpdateVerrekenbareVoorheffing(EY.com.eBook.RuleEngine.AY2014.BerekeningVenBAppTypes.VerrekenbareVoorheffingEntity entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2014.BerekeningVenBAppTypes.VerrekenbareVoorheffingEntity original = Data.VerrekenbareVoorheffing;

            entity = Validate(entity, "VerrekenbareVoorheffing", ref worksheetset);
            if (entity.IsValid)
            {

                Data.VerrekenbareVoorheffing = entity;
                Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
            }
        }

        public void UpdateVerworpenUitgaven(EY.com.eBook.RuleEngine.AY2014.BerekeningVenBAppTypes.VerworpenUitgavenItem entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2014.BerekeningVenBAppTypes.VerworpenUitgavenItem original = Data.VerworpenUitgaven;

            entity = Validate(entity, "VerworpenUitgaven", ref worksheetset);
            if (entity.IsValid)
            {

                Data.VerworpenUitgaven = entity;
                Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
            }
        }

        public void UpdateBelastbareBestZonderAftrek(EY.com.eBook.RuleEngine.AY2014.BerekeningVenBAppTypes.BelastbareBestZonderAftrekItem entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2014.BerekeningVenBAppTypes.BelastbareBestZonderAftrekItem original = Data.BelastbareBestZonderAftrek;

            entity = Validate(entity, "BelastbareBestZonderAftrek", ref worksheetset);
            if (entity.IsValid)
            {

                Data.BelastbareBestZonderAftrek = entity;
                Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
            }
        }

        public void UpdateOpdelingNaarOorsprong(EY.com.eBook.RuleEngine.AY2014.BerekeningVenBAppTypes.OpdelingNaarOorsprongItem entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2014.BerekeningVenBAppTypes.OpdelingNaarOorsprongItem original = Data.OpdelingNaarOorsprong;

            entity = Validate(entity, "OpdelingNaarOorsprong", ref worksheetset);
            if (entity.IsValid)
            {

                Data.OpdelingNaarOorsprong = entity;
                Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
            }
        }

        public void UpdateFiscaleAftrek(EY.com.eBook.RuleEngine.AY2014.BerekeningVenBAppTypes.FiscaleAftrekItem entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2014.BerekeningVenBAppTypes.FiscaleAftrekItem original = Data.FiscaleAftrek;

            entity = Validate(entity, "FiscaleAftrek", ref worksheetset);
            if (entity.IsValid)
            {

                Data.FiscaleAftrek = entity;
                Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
            }
        }

        public void UpdateFairnessTax(EY.com.eBook.RuleEngine.AY2014.BerekeningVenBAppTypes.FairnessTaxItem entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2014.BerekeningVenBAppTypes.FairnessTaxItem original = Data.FairnessTax;

            entity = Validate(entity, "FairnessTax", ref worksheetset);
            if (entity.IsValid)
            {

                Data.FairnessTax = entity;
                Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
            }
        }

        public void UpdateTarief(EY.com.eBook.RuleEngine.AY2014.BerekeningVenBAppTypes.TariefItem entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2014.BerekeningVenBAppTypes.TariefItem original = Data.Tarief;

            entity = Validate(entity, "Tarief", ref worksheetset);
            if (entity.IsValid)
            {

                Data.Tarief = entity;
                Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
            }
        }

        public void UpdateAanslag(EY.com.eBook.RuleEngine.AY2014.BerekeningVenBAppTypes.AanslagItem entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2014.BerekeningVenBAppTypes.AanslagItem original = Data.Aanslag;

            entity = Validate(entity,"Aanslag", ref worksheetset);
            if (entity.IsValid)
            {

                entity.IPT2013 = 0;
                entity.IPT2014 = 0;
                entity.IPT2015 = 0;
                if (entity.TotaalBedragVoorzieningen.GetValueOrDefault() > 0)
                {
                    if (entity.AfzonderlijkeAanslagGespreid.GetValueOrDefault())
                    {
                        entity.IPT2013 = Math.Round(entity.TotaalBedragVoorzieningen.GetValueOrDefault() * (decimal)0.006, 2, MidpointRounding.AwayFromZero);
                        entity.IPT2014 = Math.Round(entity.TotaalBedragVoorzieningen.GetValueOrDefault() * (decimal)0.006, 2, MidpointRounding.AwayFromZero);
                        entity.IPT2015 = Math.Round(entity.TotaalBedragVoorzieningen.GetValueOrDefault() * (decimal)0.006, 2, MidpointRounding.AwayFromZero);
                        if (!entity.Taxable2015.GetValueOrDefault())
                        {
                            entity.IPT2014 += entity.IPT2015.GetValueOrDefault();
                            entity.IPT2015 = 0;
                        }
                    }
                    else
                    {
                        entity.IPT2013 = Math.Round(entity.TotaalBedragVoorzieningen.GetValueOrDefault() * (decimal)0.0175, 2, MidpointRounding.AwayFromZero);
                    }
                }

                Data.Aanslag = entity;
                Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
            }
        }

        public void UpdateBerekeningOverzicht(EY.com.eBook.RuleEngine.AY2014.BerekeningVenBAppTypes.BerekeningOverzichtItem entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2014.BerekeningVenBAppTypes.BerekeningOverzichtItem original = Data.BerekeningOverzicht;

            entity = Validate(entity, "BerekeningOverzicht", ref worksheetset);
            if (entity.IsValid)
            {

                Data.BerekeningOverzicht = entity;
                Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
            }
        }

        #endregion

        #region VALIDATION

        public EY.com.eBook.RuleEngine.AY2014.BerekeningVenBAppTypes.General Validate(EY.com.eBook.RuleEngine.AY2014.BerekeningVenBAppTypes.General entity, string location, ref WorksheetSetData worksheetset)
        {
            entity.IsValid = true;
            worksheetset.ClearMessages(TypeId, entity.Id);
            /*
            if (!entity.Bedrag.HasValue || entity.Bedrag.Value==0)
            {
            worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Grootboek");
            entity.IsValid = false;
            }
            */

            return entity;
        }

        public EY.com.eBook.RuleEngine.AY2014.BerekeningVenBAppTypes.BelasteReservesItem Validate(EY.com.eBook.RuleEngine.AY2014.BerekeningVenBAppTypes.BelasteReservesItem entity, string location, ref WorksheetSetData worksheetset)
        {
            entity.IsValid = true;
            worksheetset.ClearMessages(TypeId, entity.Id);
            /*
            if (!entity.Bedrag.HasValue || entity.Bedrag.Value==0)
            {
            worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Grootboek");
            entity.IsValid = false;
            }
            */

            return entity;
        }

        public EY.com.eBook.RuleEngine.AY2014.BerekeningVenBAppTypes.VerworpenUitgavenItem Validate(EY.com.eBook.RuleEngine.AY2014.BerekeningVenBAppTypes.VerworpenUitgavenItem entity, string location, ref WorksheetSetData worksheetset)
        {
            entity.IsValid = true;
            worksheetset.ClearMessages(TypeId, entity.Id);
            /*
            if (!entity.Bedrag.HasValue || entity.Bedrag.Value==0)
            {
            worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Grootboek");
            entity.IsValid = false;
            }
            */

            return entity;
        }

        public EY.com.eBook.RuleEngine.AY2014.BerekeningVenBAppTypes.BelastbareBestZonderAftrekItem Validate(EY.com.eBook.RuleEngine.AY2014.BerekeningVenBAppTypes.BelastbareBestZonderAftrekItem entity, string location, ref WorksheetSetData worksheetset)
        {
            entity.IsValid = true;
            worksheetset.ClearMessages(TypeId, entity.Id);
            /*
            if (!entity.Bedrag.HasValue || entity.Bedrag.Value==0)
            {
            worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Grootboek");
            entity.IsValid = false;
            }
            */

            return entity;
        }

        public EY.com.eBook.RuleEngine.AY2014.BerekeningVenBAppTypes.OpdelingNaarOorsprongItem Validate(EY.com.eBook.RuleEngine.AY2014.BerekeningVenBAppTypes.OpdelingNaarOorsprongItem entity, string location, ref WorksheetSetData worksheetset)
        {
            entity.IsValid = true;
            worksheetset.ClearMessages(TypeId, entity.Id);
            /*
            if (!entity.Bedrag.HasValue || entity.Bedrag.Value==0)
            {
            worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Grootboek");
            entity.IsValid = false;
            }
            */

            return entity;
        }

        public EY.com.eBook.RuleEngine.AY2014.BerekeningVenBAppTypes.FiscaleAftrekItem Validate(EY.com.eBook.RuleEngine.AY2014.BerekeningVenBAppTypes.FiscaleAftrekItem entity, string location, ref WorksheetSetData worksheetset)
        {
            entity.IsValid = true;
            worksheetset.ClearMessages(TypeId, entity.Id);
            /*
            if (!entity.Bedrag.HasValue || entity.Bedrag.Value==0)
            {
            worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Grootboek");
            entity.IsValid = false;
            }
            */

            return entity;
        }

        public EY.com.eBook.RuleEngine.AY2014.BerekeningVenBAppTypes.TariefItem Validate(EY.com.eBook.RuleEngine.AY2014.BerekeningVenBAppTypes.TariefItem entity, string location, ref WorksheetSetData worksheetset)
        {
            entity.IsValid = true;
            worksheetset.ClearMessages(TypeId, entity.Id);
            /*
            if (!entity.Bedrag.HasValue || entity.Bedrag.Value==0)
            {
            worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Grootboek");
            entity.IsValid = false;
            }
            */

            return entity;
        }

        public EY.com.eBook.RuleEngine.AY2014.BerekeningVenBAppTypes.AanslagItem Validate(EY.com.eBook.RuleEngine.AY2014.BerekeningVenBAppTypes.AanslagItem entity, string location, ref WorksheetSetData worksheetset)
        {
            entity.IsValid = true;
            worksheetset.ClearMessages(TypeId, entity.Id);
            /*
            if (!entity.Bedrag.HasValue || entity.Bedrag.Value==0)
            {
            worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Grootboek");
            entity.IsValid = false;
            }
            */

            return entity;
        }

        public EY.com.eBook.RuleEngine.AY2014.BerekeningVenBAppTypes.FairnessTaxItem Validate(EY.com.eBook.RuleEngine.AY2014.BerekeningVenBAppTypes.FairnessTaxItem entity, string location, ref WorksheetSetData worksheetset)
        {
            entity.IsValid = true;
            worksheetset.ClearMessages(TypeId, entity.Id);
            /*
            if (!entity.Bedrag.HasValue || entity.Bedrag.Value==0)
            {
            worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Grootboek");
            entity.IsValid = false;
            }
            */

            return entity;
        }

        public EY.com.eBook.RuleEngine.AY2014.BerekeningVenBAppTypes.VerrekenbareVoorheffingEntity Validate(EY.com.eBook.RuleEngine.AY2014.BerekeningVenBAppTypes.VerrekenbareVoorheffingEntity entity, string location, ref WorksheetSetData worksheetset)
        {
            entity.IsValid = true;
            worksheetset.ClearMessages(TypeId, entity.Id);
           

            return entity;
        }


        public EY.com.eBook.RuleEngine.AY2014.BerekeningVenBAppTypes.BerekeningOverzichtItem Validate(EY.com.eBook.RuleEngine.AY2014.BerekeningVenBAppTypes.BerekeningOverzichtItem entity, string location, ref WorksheetSetData worksheetset)
        {
            entity.IsValid = true;
            worksheetset.ClearMessages(TypeId, entity.Id);
            /*
            if (!entity.Bedrag.HasValue || entity.Bedrag.Value==0)
            {
            worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Grootboek");
            entity.IsValid = false;
            }
            */

            return entity;
        }

     

        #endregion
        
        // VALIDATION
        
        // CALCULATION
        
        // BOOKINGS
        
        // IMPORT PREVIOUS




       
    }
}
  
