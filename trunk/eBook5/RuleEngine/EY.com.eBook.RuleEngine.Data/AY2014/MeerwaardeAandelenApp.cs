
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.RuleEngine.Core;
using EY.com.eBook.RuleEngine.Core.Interfaces;
using EY.com.eBook.RuleEngine.Data;
using EY.com.eBook.Core;
using EY.com.eBook.API.Contracts.Data.Cache;

namespace EY.com.eBook.RuleEngine.AY2014
{

    public class MeerwaardeAandelenApp
    {
        public static WorksheetTypeSettings Settings = new WorksheetTypeSettings
        {
            Clients = false
          ,
            Suppliers = false
          ,
            AllAccounts = true
          ,
            PerformsBookings = false
        };

        public static Guid TypeId = new Guid("0a277729-8da6-4c69-adcf-9f95047834aa");

        public List<string> ImportablePreviousTypes = new List<string>() { };

        public Guid GetMyType() { return TypeId; }

        public void SetMinimal()
        {
            if (Data == null)
            {
                Data = new EY.com.eBook.RuleEngine.AY2014.MeerwaardeAandelenAppTypes.MeerwaardeAandelen();
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.MeerwaardeAandelenLijst == null)
            {
                Data.MeerwaardeAandelenLijst = new List<EY.com.eBook.RuleEngine.AY2014.MeerwaardeAandelenAppTypes.MeerwaardeAandelenItem>(); ;
                Data.LastUpdated = DateTime.Now;
            }


        }

        public string ToXml()
        {
            return CoreHelper.SerializeToString(Data);
        }

        public void LoadXml(string xml)
        {
            if (!string.IsNullOrEmpty(xml))
            {
                Data = CoreHelper.DeserializeFromString<EY.com.eBook.RuleEngine.AY2014.MeerwaardeAandelenAppTypes.MeerwaardeAandelen>(xml);
            }
            SetMinimal();

        }

        public EY.com.eBook.RuleEngine.AY2014.MeerwaardeAandelenAppTypes.MeerwaardeAandelen Data { get; set; }

        public void Calculate(ref WorksheetSetData worksheetset)
        {
            worksheetset.ClearMessages(TypeId);
            if (!Data.LastUpdated.HasValue) Data.LastUpdated = new DateTime(1980, 1, 1);
            LoadMappings(ref worksheetset);
            ValidateAll(ref worksheetset);
            Data.LastUpdated = DateTime.Now;

        }

        public void ForceRecalculation(ref WorksheetSetData worksheetset)
        {
            Data.LastUpdated = null;
            Calculate(ref worksheetset);
        }

        public void ValidateAll(ref WorksheetSetData worksheetset)
        {
            worksheetset.ClearMessages(TypeId);
            for (int i = 0; i < Data.MeerwaardeAandelenLijst.Count; i++)
            {
                Data.MeerwaardeAandelenLijst[i] = Validate(Data.MeerwaardeAandelenLijst[i], "MeerwaardeAandelenLijst", ref worksheetset);
            }

        }

        private void LoadMappings(ref WorksheetSetData worksheetset)
        {

            List<AccountDataContract> accounts = worksheetset.GetAccountsOfMappingMeta("meerwaardeaandelen", "meerwaardeaandelen", false, true, false, worksheetset.DataContract.Culture);
            accounts.RemoveAll(a => a.Saldo == 0);
            List<string> nrs = accounts.Select(a => a.InternalNr).ToList();

            Data.MeerwaardeAandelenLijst.RemoveAll(d => !nrs.Contains(d.Grootboek.Id));

            foreach (AccountDataContract account in accounts)
            {
                MeerwaardeAandelenAppTypes.MeerwaardeAandelenItem entity = Data.MeerwaardeAandelenLijst.FirstOrDefault(a => a.Grootboek.Id == account.InternalNr);
                if (entity == null)
                {
                    entity = new EY.com.eBook.RuleEngine.AY2014.MeerwaardeAandelenAppTypes.MeerwaardeAandelenItem
                    {
                        Id = Guid.NewGuid()
                        ,
                        Commentaar = string.Empty
                        ,
                        PermanentieVoorwaarde = false
                        ,
                        TaxatieVoorwaarde = false
                        ,
                        TradingVennootschap = false
                        ,
                        Resultaat = null
                        ,
                        PreviousImported = false
                        ,
                        LastUpdated = DateTime.Now
                    };
                    Data.MeerwaardeAandelenLijst.Add(entity);
                }

                entity.Grootboek = worksheetset.GetListAccount(account);
                entity.Saldo = Math.Abs(account.Saldo);
                if (account.InternalNr.StartsWith("65")) {
                    entity.Saldo = entity.Saldo * (-1);
                }
                
                entity = Validate(entity, "MeerwaardeAandelenLijst", ref worksheetset);
            }


        }

        // VALIDATION

        // CALCULATION

        // BOOKINGS

        // IMPORT PREVIOUS


        #region AddUpdateDelete

        public void AddToMeerwaardeAandelenLijst(EY.com.eBook.RuleEngine.AY2014.MeerwaardeAandelenAppTypes.MeerwaardeAandelenItem entity, ref WorksheetSetData worksheetset)
        {
            entity = Validate(entity, "MeerwaardeAandelenLijst", ref worksheetset);
            if (entity.IsValid)
            {
                Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
                Data.MeerwaardeAandelenLijst.Add(entity);
            }
        }

        public void DeleteMeerwaardeAandelenLijst(Guid rowId, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2014.MeerwaardeAandelenAppTypes.MeerwaardeAandelenItem original = Data.MeerwaardeAandelenLijst.FirstOrDefault(e => e.Id == rowId);
            if (original != null)
            {
                /*
                if (original.Boeken.HasValue && original.Boeken.Value)
                {
                    worksheetset.DeleteBooking(FacturatieApp.TypeId, "OpTeMakenCN", original.Id, null);
                    // DELETE BOOKING
                }*/
                Data.MeerwaardeAandelenLijst.Remove(original);
                Data.LastUpdated = DateTime.Now;
            }
        }

        public void UpdateMeerwaardeAandelenLijst(EY.com.eBook.RuleEngine.AY2014.MeerwaardeAandelenAppTypes.MeerwaardeAandelenItem entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2014.MeerwaardeAandelenAppTypes.MeerwaardeAandelenItem original = Data.MeerwaardeAandelenLijst.FirstOrDefault(e => e.Id == entity.Id);
            if (original == null)
            {
                AddToMeerwaardeAandelenLijst(entity, ref worksheetset);
            }
            else
            {
                entity = Validate(entity, "MeerwaardeAandelenLijst", ref worksheetset);
                if (entity.IsValid)
                {
                    int idx = Data.MeerwaardeAandelenLijst.IndexOf(original);
                    Data.MeerwaardeAandelenLijst.RemoveAt(idx);
                    Data.MeerwaardeAandelenLijst.Insert(idx, entity);
                    Data.LastUpdated = DateTime.Now;
                    entity.LastUpdated = DateTime.Now;
                }
            }
        }


        #endregion

        #region VALIDATION

        public EY.com.eBook.RuleEngine.AY2014.MeerwaardeAandelenAppTypes.MeerwaardeAandelenItem Validate(EY.com.eBook.RuleEngine.AY2014.MeerwaardeAandelenAppTypes.MeerwaardeAandelenItem entity, string location, ref WorksheetSetData worksheetset)
        {
            entity.IsValid = true;
            worksheetset.ClearMessages(TypeId, entity.Id);
            entity.Resultaat = worksheetset.GetListItem(new Guid("EE103773-5BC0-46FE-B5C8-08CFFC03B178")); // GEWOON TARIEF

            if (!entity.TradingVennootschap.GetValueOrDefault())
            {
                if (entity.TaxatieVoorwaarde.GetValueOrDefault())
                {
                    if (entity.PermanentieVoorwaarde.GetValueOrDefault())
                    {
                        if (worksheetset.Settings.CurrentPeriodSmall)
                        {
                            entity.Resultaat = worksheetset.GetListItem(new Guid("d9be6d3d-d561-46b4-87fc-9401c9c7ae68")); // VRIJGESTELD
                        }
                        else
                        {
                            entity.Resultaat = worksheetset.GetListItem(new Guid("3410F9C9-ED85-4BB0-87E6-8D91962210DF")); // AANVULLEND TARIEF 0.412%
                        }
                    }
                    else
                    {
                        entity.Resultaat = worksheetset.GetListItem(new Guid("071EFA9C-4FA7-47BD-BCAB-DCB98F437651")); // MEERWAARDE AANDELEN TARIEF
                    }
                }
            }



            return entity;
        }


        #endregion
    }
}
