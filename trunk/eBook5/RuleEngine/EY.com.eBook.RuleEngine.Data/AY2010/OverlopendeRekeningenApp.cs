﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.RuleEngine.Core;
using EY.com.eBook.RuleEngine.Core.Interfaces;
using EY.com.eBook.RuleEngine.Data;
using EY.com.eBook.Core;

namespace EY.com.eBook.RuleEngine.AY2010
{
    
    public class OverlopendeRekeningenApp
    {
        #region standard
        public static WorksheetTypeSettings Settings = new WorksheetTypeSettings
                                                      {
                                                        Clients = true
                                                        ,
                                                        Suppliers = true
                                                        ,
                                                        AllAccounts = true
                                                        , 
                                                        PerformsBookings=true
                                                      };
                                                      
        public static Guid TypeId = new Guid("d145b5f5-b585-4844-a807-b89d7fecb4cf");

        public List<string> ImportablePreviousTypes  = new List<string>() {  "EY.com.eBook.RuleEngine.AY2011.FacturatieApp"  };

        public Guid GetMyType() { return TypeId; }
        
        public void SetMinimal() 
        {
            if(Data==null) {
                Data = new EY.com.eBook.RuleEngine.AY2010.OverlopendeRekeningenAppTypes.OverlopendeRekeningen();
                Data.LastUpdated = DateTime.Now;
            }              
            
              if(Data.OverTeDragenKosten==null) {
                  Data.OverTeDragenKosten = new List<EY.com.eBook.RuleEngine.AY2010.OverlopendeRekeningenAppTypes.OverTeDragenKost>();;
                  Data.LastUpdated = DateTime.Now;
              }
            
              if(Data.VerkregenOpbrensten==null) {
                  Data.VerkregenOpbrensten = new List<EY.com.eBook.RuleEngine.AY2010.OverlopendeRekeningenAppTypes.VerkregenOpbrengst>();;
                  Data.LastUpdated = DateTime.Now;
              }
            
              if(Data.ToeTeRekenenKosten==null) {
                  Data.ToeTeRekenenKosten = new List<EY.com.eBook.RuleEngine.AY2010.OverlopendeRekeningenAppTypes.ToeTeRekenenKost>();;
                  Data.LastUpdated = DateTime.Now;
              }
            
              if(Data.OverTeDragenOpbrengsten==null) {
                  Data.OverTeDragenOpbrengsten = new List<EY.com.eBook.RuleEngine.AY2010.OverlopendeRekeningenAppTypes.OverTeDragenOpbrengst>();;
                  Data.LastUpdated = DateTime.Now;
              }
            
        }
        
        public string ToXml() 
        {
            return CoreHelper.SerializeToString(Data);
        }

        public void LoadXml(string xml)
        {
            if (!string.IsNullOrEmpty(xml)) {
                Data = CoreHelper.DeserializeFromString<EY.com.eBook.RuleEngine.AY2010.OverlopendeRekeningenAppTypes.OverlopendeRekeningen>(xml);
            }
            SetMinimal();
            
        }
        
        public EY.com.eBook.RuleEngine.AY2010.OverlopendeRekeningenAppTypes.OverlopendeRekeningen Data { get; set; }
        
        public void Calculate(ref WorksheetSetData worksheetset)
        {
           // Data.LastCalculated = DateTime.Now;
        }
        #endregion
        
        #region AddUpdateDelete
        
          public void AddToOverTeDragenKosten(EY.com.eBook.RuleEngine.AY2010.OverlopendeRekeningenAppTypes.OverTeDragenKost entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void DeleteOverTeDragenKosten(Guid rowId,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void UpdateOverTeDragenKosten(EY.com.eBook.RuleEngine.AY2010.OverlopendeRekeningenAppTypes.OverTeDragenKost entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void AddToVerkregenOpbrensten(EY.com.eBook.RuleEngine.AY2010.OverlopendeRekeningenAppTypes.VerkregenOpbrengst entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void DeleteVerkregenOpbrensten(Guid rowId,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void UpdateVerkregenOpbrensten(EY.com.eBook.RuleEngine.AY2010.OverlopendeRekeningenAppTypes.VerkregenOpbrengst entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void AddToToeTeRekenenKosten(EY.com.eBook.RuleEngine.AY2010.OverlopendeRekeningenAppTypes.ToeTeRekenenKost entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void DeleteToeTeRekenenKosten(Guid rowId,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void UpdateToeTeRekenenKosten(EY.com.eBook.RuleEngine.AY2010.OverlopendeRekeningenAppTypes.ToeTeRekenenKost entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void AddToOverTeDragenOpbrengsten(EY.com.eBook.RuleEngine.AY2010.OverlopendeRekeningenAppTypes.OverTeDragenOpbrengst entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void DeleteOverTeDragenOpbrengsten(Guid rowId,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void UpdateOverTeDragenOpbrengsten(EY.com.eBook.RuleEngine.AY2010.OverlopendeRekeningenAppTypes.OverTeDragenOpbrengst entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
        #endregion
        
        #region VALIDATION
        
        public EY.com.eBook.RuleEngine.AY2010.OverlopendeRekeningenAppTypes.OverTeDragenKost Validate(EY.com.eBook.RuleEngine.AY2010.OverlopendeRekeningenAppTypes.OverTeDragenKost entity, string location,ref WorksheetSetData worksheetset)
          {
          entity.IsValid = true;
          worksheetset.ClearMessages(TypeId, entity.Id);
          

          return entity;
          }
        
        public EY.com.eBook.RuleEngine.AY2010.OverlopendeRekeningenAppTypes.VerkregenOpbrengst Validate(EY.com.eBook.RuleEngine.AY2010.OverlopendeRekeningenAppTypes.VerkregenOpbrengst entity, string location,ref WorksheetSetData worksheetset)
          {
          entity.IsValid = true;
          worksheetset.ClearMessages(TypeId, entity.Id);
          

          return entity;
          }
        
        public EY.com.eBook.RuleEngine.AY2010.OverlopendeRekeningenAppTypes.ToeTeRekenenKost Validate(EY.com.eBook.RuleEngine.AY2010.OverlopendeRekeningenAppTypes.ToeTeRekenenKost entity, string location,ref WorksheetSetData worksheetset)
          {
          entity.IsValid = true;
          worksheetset.ClearMessages(TypeId, entity.Id);
          

          return entity;
          }
        
        public EY.com.eBook.RuleEngine.AY2010.OverlopendeRekeningenAppTypes.OverTeDragenOpbrengst Validate(EY.com.eBook.RuleEngine.AY2010.OverlopendeRekeningenAppTypes.OverTeDragenOpbrengst entity, string location,ref WorksheetSetData worksheetset)
          {
          entity.IsValid = true;
          worksheetset.ClearMessages(TypeId, entity.Id);
          

          return entity;
          }
        
        #endregion
        // CALCULATION
        
        // BOOKINGS
        
        // IMPORT PREVIOUS
        
    }
}
  