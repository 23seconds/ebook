using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Activation;
using EY.com.eBook.Core.EF;
using System.ServiceModel.Web;
using EY.com.eBook.RuleEngine.Core;
using EY.com.eBook.RuleEngine.Data;
using EY.com.eBook.Core.Data;
using EY.com.eBook.Core;
using EY.com.eBook.Core.EF.Write;
using EY.com.eBook.API.Contracts.Proxies;
using EY.com.eBook.Caching;
using EY.com.eBook.RuleEngine.AY2010;
using EY.com.eBook.API.Contracts.Data;

namespace EY.com.eBook.RuleEngine
{
    [EntityFrameworkServiceBehavior]
    [ServiceBehavior(IncludeExceptionDetailInFaults = true, ConcurrencyMode = ConcurrencyMode.Multiple, InstanceContextMode = InstanceContextMode.PerCall)]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class RuleEngine2010 : IRuleEngine2010
    {

        #region IRuleEngine2010 Members

        public EY.com.eBook.RuleEngine.AY2010.WorksheetSet GetSet(Guid fileId)
        {
           // CacheProxy cp = new CacheProxy();
           // cp.Open();
            CacheRepository cp = new CacheRepository();
            if (!cp.FileInCache(fileId))
            {
                throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { ErrorCode=-99, Message = "CACHE-MISS" , FileId=fileId });
            }
            WorksheetWorkSetDataContract wwsdc = cp.GetWorksheetSet(fileId);

            EY.com.eBook.RuleEngine.AY2010.WorksheetSetData sh = new EY.com.eBook.RuleEngine.AY2010.WorksheetSetData
            { 
               Coeffs = wwsdc.Coeffs
               , DataContract = wwsdc.DataContract
               , AutoBookings = wwsdc.AutoBookings== null ? new List<BookingDataContract>() : wwsdc.AutoBookings
               , Schema = wwsdc.Schema
               , Messages = wwsdc.Messages== null ? new List<FileMessageDataContract>() : wwsdc.Messages
               , IsEmpty = wwsdc.Worksheets == null || wwsdc.Worksheets.Count==0
               , LastSave = wwsdc.Worksheets==null || wwsdc.Worksheets.Count==0 ? new DateTime(1980,1,1) : wwsdc.Worksheets.LastSave
            };
            if (wwsdc.Worksheets != null && wwsdc.Worksheets.Count == 0) wwsdc.Worksheets = null;
            sh.Initialize(wwsdc.Worksheets);
            
            
           // set.DataContainer = sh;
            return new EY.com.eBook.RuleEngine.AY2010.WorksheetSet { DataContainer = sh };
            
        }


        public void CalculateAll(EY.com.eBook.RuleEngine.AY2010.WorksheetSet ws)
        {
            ws.Calculate(null);
        }

        public void Calculate(EY.com.eBook.RuleEngine.AY2010.WorksheetSet ws, string col)
        {
            ws.Calculate(col);
        }

        public void ReCalculate(CriteriaFileDataContract cfdc)
        {
            EY.com.eBook.RuleEngine.AY2010.WorksheetSet set = GetSet(cfdc.FileId);
            set.Calculate(null);
        }

        public void ImportPrevious(CriteriaFileDataContract cfdc)
        {
            throw new NotImplementedException();
        }
        
        public decimal? GetTaxCalculation(CriteriaFileDataContract cfdc)
        {
            EY.com.eBook.RuleEngine.AY2010.WorksheetSet set = GetSet(cfdc.FileId);

            return set.GetTaxCalculation();
        }

       

        #endregion


    }
}
