using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using EY.com.eBook.RuleEngine.Data;
using EY.com.eBook.Core.Data;
using EY.com.eBook.API.Contracts.Data;

namespace EY.com.eBook.RuleEngine
{
    
    [ServiceContract]
    public interface IRuleEngine2010
    {
        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        decimal? GetTaxCalculation(CriteriaFileDataContract cfdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        void ReCalculate(CriteriaFileDataContract cfdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        void ImportPrevious(CriteriaFileDataContract cfdc);

      

    }

    
}
