﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.RuleEngine.Core;
using EY.com.eBook.RuleEngine.Core.Interfaces;
using EY.com.eBook.RuleEngine.Data;
using EY.com.eBook.Core;

namespace EY.com.eBook.RuleEngine.AY2010
{
    
    public class AangifteVenBApp
    {
        #region standard
        public static WorksheetTypeSettings Settings = new WorksheetTypeSettings
                                                      {
                                                        Clients = true
                                                        ,
                                                        Suppliers = true
                                                        ,
                                                        AllAccounts = true
                                                        , 
                                                        PerformsBookings=true
                                                      };
                                                      
        public static Guid TypeId = new Guid("65bf5cbd-187b-4c41-aad9-9793357f222b");

        public List<string> ImportablePreviousTypes  = new List<string>() {  "EY.com.eBook.RuleEngine.AY2011.FacturatieApp"  };

        public Guid GetMyType() { return TypeId; }
        
        public void SetMinimal() 
        {
            if(Data==null) {
                Data = new EY.com.eBook.RuleEngine.AY2010.AangifteVenBAppTypes.AangifteVenB();
                Data.LastUpdated = DateTime.Now;
            }              
            
              if(Data.Bank==null) {
                  Data.Bank = new EY.com.eBook.RuleEngine.AY2010.AangifteVenBAppTypes.BankEntity {
                            Id=Guid.NewGuid()
                            , LastUpdated=null
                            , IsValid=true
                            , PreviousImported=false
                            
                         };
                  Data.LastUpdated = DateTime.Now;
              }
            
              if(Data.Reserves==null) {
                  Data.Reserves = new EY.com.eBook.RuleEngine.AY2010.AangifteVenBAppTypes.ReserveEntity {
                            Id=Guid.NewGuid()
                            , LastUpdated=null
                            , IsValid=true
                            , PreviousImported=false
                            
                         };
                  Data.LastUpdated = DateTime.Now;
              }
            
              if(Data.VrijgesteldeReserve==null) {
                  Data.VrijgesteldeReserve = new EY.com.eBook.RuleEngine.AY2010.AangifteVenBAppTypes.VrijgesteldeReserveEntity {
                            Id=Guid.NewGuid()
                            , LastUpdated=null
                            , IsValid=true
                            , PreviousImported=false
                            
                         };
                  Data.LastUpdated = DateTime.Now;
              }
            
              if(Data.VerworpenUitgaven==null) {
                  Data.VerworpenUitgaven = new EY.com.eBook.RuleEngine.AY2010.AangifteVenBAppTypes.VerworpenUitgavenEntity {
                            Id=Guid.NewGuid()
                            , LastUpdated=null
                            , IsValid=true
                            , PreviousImported=false
                            
                         };
                  Data.LastUpdated = DateTime.Now;
              }
            
              if(Data.UitgekeerdeDividenden==null) {
                  Data.UitgekeerdeDividenden = new EY.com.eBook.RuleEngine.AY2010.AangifteVenBAppTypes.UitgekeerdeDividendenEntity {
                            Id=Guid.NewGuid()
                            , LastUpdated=null
                            , IsValid=true
                            , PreviousImported=false
                            
                         };
                  Data.LastUpdated = DateTime.Now;
              }
            
              if(Data.UiteenzettingWinst==null) {
                  Data.UiteenzettingWinst = new EY.com.eBook.RuleEngine.AY2010.AangifteVenBAppTypes.WinstEntity {
                            Id=Guid.NewGuid()
                            , LastUpdated=null
                            , IsValid=true
                            , PreviousImported=false
                            
                         };
                  Data.LastUpdated = DateTime.Now;
              }
            
              if(Data.AfzonderlijkeAanslagen==null) {
                  Data.AfzonderlijkeAanslagen = new EY.com.eBook.RuleEngine.AY2010.AangifteVenBAppTypes.AfzonderlijkeAanslagenEntity {
                            Id=Guid.NewGuid()
                            , LastUpdated=null
                            , IsValid=true
                            , PreviousImported=false
                            
                         };
                  Data.LastUpdated = DateTime.Now;
              }
            
              if(Data.Diamant==null) {
                  Data.Diamant = new EY.com.eBook.RuleEngine.AY2010.AangifteVenBAppTypes.DiamantEntity {
                            Id=Guid.NewGuid()
                            , LastUpdated=null
                            , IsValid=true
                            , PreviousImported=false
                            
                         };
                  Data.LastUpdated = DateTime.Now;
              }
            
              if(Data.NietBelastbareBestanddelen==null) {
                  Data.NietBelastbareBestanddelen = new EY.com.eBook.RuleEngine.AY2010.AangifteVenBAppTypes.NietBelastbareBestandelenItem {
                            Id=Guid.NewGuid()
                            , LastUpdated=null
                            , IsValid=true
                            , PreviousImported=false
                            
                         };
                  Data.LastUpdated = DateTime.Now;
              }
            
              if(Data.DBI==null) {
                  Data.DBI = new EY.com.eBook.RuleEngine.AY2010.AangifteVenBAppTypes.DbiEntity {
                            Id=Guid.NewGuid()
                            , LastUpdated=null
                            , IsValid=true
                            , PreviousImported=false
                            
                         };
                  Data.LastUpdated = DateTime.Now;
              }
            
              if(Data.OverdrachtDBIAftrek==null) {
                  Data.OverdrachtDBIAftrek = new EY.com.eBook.RuleEngine.AY2010.AangifteVenBAppTypes.OverdrachtDBIAftrekEntity {
                            Id=Guid.NewGuid()
                            , LastUpdated=null
                            , IsValid=true
                            , PreviousImported=false
                            
                         };
                  Data.LastUpdated = DateTime.Now;
              }
            
              if(Data.OverdrachtNID==null) {
                  Data.OverdrachtNID = new EY.com.eBook.RuleEngine.AY2010.AangifteVenBAppTypes.OverdrachtNIDEntity {
                            Id=Guid.NewGuid()
                            , LastUpdated=null
                            , IsValid=true
                            , PreviousImported=false
                            
                         };
                  Data.LastUpdated = DateTime.Now;
              }
            
              if(Data.CompenseerbareVerliezen==null) {
                  Data.CompenseerbareVerliezen = new EY.com.eBook.RuleEngine.AY2010.AangifteVenBAppTypes.CompenseerbareVerliezenEntity {
                            Id=Guid.NewGuid()
                            , LastUpdated=null
                            , IsValid=true
                            , PreviousImported=false
                            
                         };
                  Data.LastUpdated = DateTime.Now;
              }
            
              if(Data.TariefBelasting==null) {
                  Data.TariefBelasting = new EY.com.eBook.RuleEngine.AY2010.AangifteVenBAppTypes.TariefEntity {
                            Id=Guid.NewGuid()
                            , LastUpdated=null
                            , IsValid=true
                            , PreviousImported=false
                            
                         };
                  Data.LastUpdated = DateTime.Now;
              }
            
              if(Data.VoorafBetalingen==null) {
                  Data.VoorafBetalingen = new EY.com.eBook.RuleEngine.AY2010.AangifteVenBAppTypes.VoorafBetalingEntity {
                            Id=Guid.NewGuid()
                            , LastUpdated=null
                            , IsValid=true
                            , PreviousImported=false
                            
                         };
                  Data.LastUpdated = DateTime.Now;
              }
            
              if(Data.VerrekenbareVoorheffing==null) {
                  Data.VerrekenbareVoorheffing = new EY.com.eBook.RuleEngine.AY2010.AangifteVenBAppTypes.VerrekenbareVoorheffingEntity {
                            Id=Guid.NewGuid()
                            , LastUpdated=null
                            , IsValid=true
                            , PreviousImported=false
                            
                         };
                  Data.LastUpdated = DateTime.Now;
              }
            
              if(Data.TaxShelter==null) {
                  Data.TaxShelter = new EY.com.eBook.RuleEngine.AY2010.AangifteVenBAppTypes.TaxShelterEntity {
                            Id=Guid.NewGuid()
                            , LastUpdated=null
                            , IsValid=true
                            , PreviousImported=false
                            
                         };
                  Data.LastUpdated = DateTime.Now;
              }
            
              if(Data.GrootteVennootschap==null) {
                  Data.GrootteVennootschap = new EY.com.eBook.RuleEngine.AY2010.AangifteVenBAppTypes.GrootteVennootschapeEntity {
                            Id=Guid.NewGuid()
                            , LastUpdated=null
                            , IsValid=true
                            , PreviousImported=false
                            
                         };
                  Data.LastUpdated = DateTime.Now;
              }
            
              if(Data.OpgaveBijlagen==null) {
                  Data.OpgaveBijlagen = new EY.com.eBook.RuleEngine.AY2010.AangifteVenBAppTypes.OpgavenEntity {
                            Id=Guid.NewGuid()
                            , LastUpdated=null
                            , IsValid=true
                            , PreviousImported=false
                            
                         };
                  Data.LastUpdated = DateTime.Now;
              }
            
        }
        
        public string ToXml() 
        {
            return CoreHelper.SerializeToString(Data);
        }

        public void LoadXml(string xml)
        {
            if (!string.IsNullOrEmpty(xml)) {
                Data = CoreHelper.DeserializeFromString<EY.com.eBook.RuleEngine.AY2010.AangifteVenBAppTypes.AangifteVenB>(xml);
            }
            SetMinimal();
            
        }
        
        public EY.com.eBook.RuleEngine.AY2010.AangifteVenBAppTypes.AangifteVenB Data { get; set; }
        
        public void Calculate(ref WorksheetSetData worksheetset)
        {
           // Data.LastCalculated = DateTime.Now;
        }
        #endregion
        
        #region AddUpdateDelete
        
          public void UpdateBank(EY.com.eBook.RuleEngine.AY2010.AangifteVenBAppTypes.BankEntity entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void UpdateReserves(EY.com.eBook.RuleEngine.AY2010.AangifteVenBAppTypes.ReserveEntity entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void UpdateVrijgesteldeReserve(EY.com.eBook.RuleEngine.AY2010.AangifteVenBAppTypes.VrijgesteldeReserveEntity entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void UpdateVerworpenUitgaven(EY.com.eBook.RuleEngine.AY2010.AangifteVenBAppTypes.VerworpenUitgavenEntity entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void UpdateUitgekeerdeDividenden(EY.com.eBook.RuleEngine.AY2010.AangifteVenBAppTypes.UitgekeerdeDividendenEntity entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void UpdateUiteenzettingWinst(EY.com.eBook.RuleEngine.AY2010.AangifteVenBAppTypes.WinstEntity entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void UpdateAfzonderlijkeAanslagen(EY.com.eBook.RuleEngine.AY2010.AangifteVenBAppTypes.AfzonderlijkeAanslagenEntity entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void UpdateDiamant(EY.com.eBook.RuleEngine.AY2010.AangifteVenBAppTypes.DiamantEntity entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void UpdateNietBelastbareBestanddelen(EY.com.eBook.RuleEngine.AY2010.AangifteVenBAppTypes.NietBelastbareBestandelenItem entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void UpdateDBI(EY.com.eBook.RuleEngine.AY2010.AangifteVenBAppTypes.DbiEntity entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void UpdateOverdrachtDBIAftrek(EY.com.eBook.RuleEngine.AY2010.AangifteVenBAppTypes.OverdrachtDBIAftrekEntity entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void UpdateOverdrachtNID(EY.com.eBook.RuleEngine.AY2010.AangifteVenBAppTypes.OverdrachtNIDEntity entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void UpdateCompenseerbareVerliezen(EY.com.eBook.RuleEngine.AY2010.AangifteVenBAppTypes.CompenseerbareVerliezenEntity entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void UpdateTariefBelasting(EY.com.eBook.RuleEngine.AY2010.AangifteVenBAppTypes.TariefEntity entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void UpdateVoorafBetalingen(EY.com.eBook.RuleEngine.AY2010.AangifteVenBAppTypes.VoorafBetalingEntity entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void UpdateVerrekenbareVoorheffing(EY.com.eBook.RuleEngine.AY2010.AangifteVenBAppTypes.VerrekenbareVoorheffingEntity entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void UpdateTaxShelter(EY.com.eBook.RuleEngine.AY2010.AangifteVenBAppTypes.TaxShelterEntity entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void UpdateGrootteVennootschap(EY.com.eBook.RuleEngine.AY2010.AangifteVenBAppTypes.GrootteVennootschapeEntity entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void UpdateOpgaveBijlagen(EY.com.eBook.RuleEngine.AY2010.AangifteVenBAppTypes.OpgavenEntity entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
        #endregion
        
        #region VALIDATION
        
        public EY.com.eBook.RuleEngine.AY2010.AangifteVenBAppTypes.BankEntity Validate(EY.com.eBook.RuleEngine.AY2010.AangifteVenBAppTypes.BankEntity entity, string location,ref WorksheetSetData worksheetset)
          {
          entity.IsValid = true;
          worksheetset.ClearMessages(TypeId, entity.Id);
          

          return entity;
          }
        
        public EY.com.eBook.RuleEngine.AY2010.AangifteVenBAppTypes.ReserveEntity Validate(EY.com.eBook.RuleEngine.AY2010.AangifteVenBAppTypes.ReserveEntity entity, string location,ref WorksheetSetData worksheetset)
          {
          entity.IsValid = true;
          worksheetset.ClearMessages(TypeId, entity.Id);
          

          return entity;
          }
        
        public EY.com.eBook.RuleEngine.AY2010.AangifteVenBAppTypes.VrijgesteldeReserveEntity Validate(EY.com.eBook.RuleEngine.AY2010.AangifteVenBAppTypes.VrijgesteldeReserveEntity entity, string location,ref WorksheetSetData worksheetset)
          {
          entity.IsValid = true;
          worksheetset.ClearMessages(TypeId, entity.Id);
          

          return entity;
          }
        
        public EY.com.eBook.RuleEngine.AY2010.AangifteVenBAppTypes.VerworpenUitgavenEntity Validate(EY.com.eBook.RuleEngine.AY2010.AangifteVenBAppTypes.VerworpenUitgavenEntity entity, string location,ref WorksheetSetData worksheetset)
          {
          entity.IsValid = true;
          worksheetset.ClearMessages(TypeId, entity.Id);
          

          return entity;
          }
        
        public EY.com.eBook.RuleEngine.AY2010.AangifteVenBAppTypes.UitgekeerdeDividendenEntity Validate(EY.com.eBook.RuleEngine.AY2010.AangifteVenBAppTypes.UitgekeerdeDividendenEntity entity, string location,ref WorksheetSetData worksheetset)
          {
          entity.IsValid = true;
          worksheetset.ClearMessages(TypeId, entity.Id);
          

          return entity;
          }
        
        public EY.com.eBook.RuleEngine.AY2010.AangifteVenBAppTypes.WinstEntity Validate(EY.com.eBook.RuleEngine.AY2010.AangifteVenBAppTypes.WinstEntity entity, string location,ref WorksheetSetData worksheetset)
          {
          entity.IsValid = true;
          worksheetset.ClearMessages(TypeId, entity.Id);
          

          return entity;
          }
        
        public EY.com.eBook.RuleEngine.AY2010.AangifteVenBAppTypes.AfzonderlijkeAanslagenEntity Validate(EY.com.eBook.RuleEngine.AY2010.AangifteVenBAppTypes.AfzonderlijkeAanslagenEntity entity, string location,ref WorksheetSetData worksheetset)
          {
          entity.IsValid = true;
          worksheetset.ClearMessages(TypeId, entity.Id);
          

          return entity;
          }
        
        public EY.com.eBook.RuleEngine.AY2010.AangifteVenBAppTypes.DiamantEntity Validate(EY.com.eBook.RuleEngine.AY2010.AangifteVenBAppTypes.DiamantEntity entity, string location,ref WorksheetSetData worksheetset)
          {
          entity.IsValid = true;
          worksheetset.ClearMessages(TypeId, entity.Id);
          

          return entity;
          }
        
        public EY.com.eBook.RuleEngine.AY2010.AangifteVenBAppTypes.NietBelastbareBestandelenItem Validate(EY.com.eBook.RuleEngine.AY2010.AangifteVenBAppTypes.NietBelastbareBestandelenItem entity, string location,ref WorksheetSetData worksheetset)
          {
          entity.IsValid = true;
          worksheetset.ClearMessages(TypeId, entity.Id);
          

          return entity;
          }
        
        public EY.com.eBook.RuleEngine.AY2010.AangifteVenBAppTypes.DbiEntity Validate(EY.com.eBook.RuleEngine.AY2010.AangifteVenBAppTypes.DbiEntity entity, string location,ref WorksheetSetData worksheetset)
          {
          entity.IsValid = true;
          worksheetset.ClearMessages(TypeId, entity.Id);
          

          return entity;
          }
        
        public EY.com.eBook.RuleEngine.AY2010.AangifteVenBAppTypes.OverdrachtDBIAftrekEntity Validate(EY.com.eBook.RuleEngine.AY2010.AangifteVenBAppTypes.OverdrachtDBIAftrekEntity entity, string location,ref WorksheetSetData worksheetset)
          {
          entity.IsValid = true;
          worksheetset.ClearMessages(TypeId, entity.Id);
          

          return entity;
          }
        
        public EY.com.eBook.RuleEngine.AY2010.AangifteVenBAppTypes.OverdrachtNIDEntity Validate(EY.com.eBook.RuleEngine.AY2010.AangifteVenBAppTypes.OverdrachtNIDEntity entity, string location,ref WorksheetSetData worksheetset)
          {
          entity.IsValid = true;
          worksheetset.ClearMessages(TypeId, entity.Id);
          

          return entity;
          }
        
        public EY.com.eBook.RuleEngine.AY2010.AangifteVenBAppTypes.CompenseerbareVerliezenEntity Validate(EY.com.eBook.RuleEngine.AY2010.AangifteVenBAppTypes.CompenseerbareVerliezenEntity entity, string location,ref WorksheetSetData worksheetset)
          {
          entity.IsValid = true;
          worksheetset.ClearMessages(TypeId, entity.Id);
          

          return entity;
          }
        
        public EY.com.eBook.RuleEngine.AY2010.AangifteVenBAppTypes.TariefEntity Validate(EY.com.eBook.RuleEngine.AY2010.AangifteVenBAppTypes.TariefEntity entity, string location,ref WorksheetSetData worksheetset)
          {
          entity.IsValid = true;
          worksheetset.ClearMessages(TypeId, entity.Id);
          

          return entity;
          }
        
        public EY.com.eBook.RuleEngine.AY2010.AangifteVenBAppTypes.VoorafBetalingEntity Validate(EY.com.eBook.RuleEngine.AY2010.AangifteVenBAppTypes.VoorafBetalingEntity entity, string location,ref WorksheetSetData worksheetset)
          {
          entity.IsValid = true;
          worksheetset.ClearMessages(TypeId, entity.Id);
          

          return entity;
          }
        
        public EY.com.eBook.RuleEngine.AY2010.AangifteVenBAppTypes.VerrekenbareVoorheffingEntity Validate(EY.com.eBook.RuleEngine.AY2010.AangifteVenBAppTypes.VerrekenbareVoorheffingEntity entity, string location,ref WorksheetSetData worksheetset)
          {
          entity.IsValid = true;
          worksheetset.ClearMessages(TypeId, entity.Id);
          

          return entity;
          }
        
        public EY.com.eBook.RuleEngine.AY2010.AangifteVenBAppTypes.TaxShelterEntity Validate(EY.com.eBook.RuleEngine.AY2010.AangifteVenBAppTypes.TaxShelterEntity entity, string location,ref WorksheetSetData worksheetset)
          {
          entity.IsValid = true;
          worksheetset.ClearMessages(TypeId, entity.Id);
          

          return entity;
          }
        
        public EY.com.eBook.RuleEngine.AY2010.AangifteVenBAppTypes.GrootteVennootschapeEntity Validate(EY.com.eBook.RuleEngine.AY2010.AangifteVenBAppTypes.GrootteVennootschapeEntity entity, string location,ref WorksheetSetData worksheetset)
          {
          entity.IsValid = true;
          worksheetset.ClearMessages(TypeId, entity.Id);
          

          return entity;
          }
        
        public EY.com.eBook.RuleEngine.AY2010.AangifteVenBAppTypes.OpgavenEntity Validate(EY.com.eBook.RuleEngine.AY2010.AangifteVenBAppTypes.OpgavenEntity entity, string location,ref WorksheetSetData worksheetset)
          {
          entity.IsValid = true;
          worksheetset.ClearMessages(TypeId, entity.Id);
          

          return entity;
          }
        
        #endregion
        // CALCULATION
        
        // BOOKINGS
        
        // IMPORT PREVIOUS
        
    }
}
  