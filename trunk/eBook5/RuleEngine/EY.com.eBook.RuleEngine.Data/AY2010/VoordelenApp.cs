﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.RuleEngine.Core;
using EY.com.eBook.RuleEngine.Core.Interfaces;
using EY.com.eBook.RuleEngine.Data;
using EY.com.eBook.Core;

namespace EY.com.eBook.RuleEngine.AY2010
{
    
    public class VoordelenApp
    {
        #region standard
        public static WorksheetTypeSettings Settings = new WorksheetTypeSettings
                                                      {
                                                        Clients = true
                                                        ,
                                                        Suppliers = true
                                                        ,
                                                        AllAccounts = true
                                                        , 
                                                        PerformsBookings=true
                                                      };
                                                      
        public static Guid TypeId = new Guid("67855d3d-4e11-4c7b-96a3-e6602ddaaa6f");

        public List<string> ImportablePreviousTypes  = new List<string>() {  "EY.com.eBook.RuleEngine.AY2011.FacturatieApp"  };

        public Guid GetMyType() { return TypeId; }
        
        public void SetMinimal() 
        {
            if(Data==null) {
                Data = new EY.com.eBook.RuleEngine.AY2010.VoordelenAppTypes.Voordelen();
                Data.LastUpdated = DateTime.Now;
            }              
            
              if(Data.VoordelenVanAlleAardAuto==null) {
                  Data.VoordelenVanAlleAardAuto = new List<EY.com.eBook.RuleEngine.AY2010.VoordelenAppTypes.VAA_Auto>();;
                  Data.LastUpdated = DateTime.Now;
              }
            
              if(Data.VoordelenVanAlleAardAutoCO2==null) {
                  Data.VoordelenVanAlleAardAutoCO2 = new List<EY.com.eBook.RuleEngine.AY2010.VoordelenAppTypes.VAA_AutoCO2>();;
                  Data.LastUpdated = DateTime.Now;
              }
            
              if(Data.VoordelenVanAlleAardNuts==null) {
                  Data.VoordelenVanAlleAardNuts = new List<EY.com.eBook.RuleEngine.AY2010.VoordelenAppTypes.VAA_Nuts>();;
                  Data.LastUpdated = DateTime.Now;
              }
            
              if(Data.VoordelenVanAlleAardHuur==null) {
                  Data.VoordelenVanAlleAardHuur = new List<EY.com.eBook.RuleEngine.AY2010.VoordelenAppTypes.VAA_HUUR>();;
                  Data.LastUpdated = DateTime.Now;
              }
            
              if(Data.VoordelenVanAlleAardHuurHerkwalificatie==null) {
                  Data.VoordelenVanAlleAardHuurHerkwalificatie = new List<EY.com.eBook.RuleEngine.AY2010.VoordelenAppTypes.VAA_HUURHERK>();;
                  Data.LastUpdated = DateTime.Now;
              }
            
              if(Data.VoordelenVanAlleAardPC==null) {
                  Data.VoordelenVanAlleAardPC = new List<EY.com.eBook.RuleEngine.AY2010.VoordelenAppTypes.VAA_PC>();;
                  Data.LastUpdated = DateTime.Now;
              }
            
              if(Data.VoordelenVanAlleAardTelefonie==null) {
                  Data.VoordelenVanAlleAardTelefonie = new List<EY.com.eBook.RuleEngine.AY2010.VoordelenAppTypes.VAA_Telefonie>();;
                  Data.LastUpdated = DateTime.Now;
              }
            
              if(Data.VoordelenVanAlleAardOverige==null) {
                  Data.VoordelenVanAlleAardOverige = new List<EY.com.eBook.RuleEngine.AY2010.VoordelenAppTypes.VAA_Overige>();;
                  Data.LastUpdated = DateTime.Now;
              }
            
              if(Data.VoordelenInterestenRCBedrijfsleider==null) {
                  Data.VoordelenInterestenRCBedrijfsleider = new List<EY.com.eBook.RuleEngine.AY2010.VoordelenAppTypes.VAA_Interesten_RC_Bedrijfsleider>();;
                  Data.LastUpdated = DateTime.Now;
              }
            
              if(Data.VoordelenSocialeBijdragenBedrijfsleider==null) {
                  Data.VoordelenSocialeBijdragenBedrijfsleider = new List<EY.com.eBook.RuleEngine.AY2010.VoordelenAppTypes.VAA_Soc_Bijdr_Bedrijfsleider>();;
                  Data.LastUpdated = DateTime.Now;
              }
            
              if(Data.VoordelenOverigeNuts==null) {
                  Data.VoordelenOverigeNuts = new List<EY.com.eBook.RuleEngine.AY2010.VoordelenAppTypes.VAA_Overige_Nuts>();;
                  Data.LastUpdated = DateTime.Now;
              }
            
        }
        
        public string ToXml() 
        {
            return CoreHelper.SerializeToString(Data);
        }

        public void LoadXml(string xml)
        {
            if (!string.IsNullOrEmpty(xml)) {
                Data = CoreHelper.DeserializeFromString<EY.com.eBook.RuleEngine.AY2010.VoordelenAppTypes.Voordelen>(xml);
            }
            SetMinimal();
            
        }
        
        public EY.com.eBook.RuleEngine.AY2010.VoordelenAppTypes.Voordelen Data { get; set; }
        
        public void Calculate(ref WorksheetSetData worksheetset)
        {
           // Data.LastCalculated = DateTime.Now;
        }
        #endregion
        
        #region AddUpdateDelete
        
          public void AddToVoordelenVanAlleAardAuto(EY.com.eBook.RuleEngine.AY2010.VoordelenAppTypes.VAA_Auto entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void DeleteVoordelenVanAlleAardAuto(Guid rowId,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void UpdateVoordelenVanAlleAardAuto(EY.com.eBook.RuleEngine.AY2010.VoordelenAppTypes.VAA_Auto entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void AddToVoordelenVanAlleAardAutoCO2(EY.com.eBook.RuleEngine.AY2010.VoordelenAppTypes.VAA_AutoCO2 entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void DeleteVoordelenVanAlleAardAutoCO2(Guid rowId,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void UpdateVoordelenVanAlleAardAutoCO2(EY.com.eBook.RuleEngine.AY2010.VoordelenAppTypes.VAA_AutoCO2 entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void AddToVoordelenVanAlleAardNuts(EY.com.eBook.RuleEngine.AY2010.VoordelenAppTypes.VAA_Nuts entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void DeleteVoordelenVanAlleAardNuts(Guid rowId,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void UpdateVoordelenVanAlleAardNuts(EY.com.eBook.RuleEngine.AY2010.VoordelenAppTypes.VAA_Nuts entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void AddToVoordelenVanAlleAardHuur(EY.com.eBook.RuleEngine.AY2010.VoordelenAppTypes.VAA_HUUR entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void DeleteVoordelenVanAlleAardHuur(Guid rowId,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void UpdateVoordelenVanAlleAardHuur(EY.com.eBook.RuleEngine.AY2010.VoordelenAppTypes.VAA_HUUR entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void AddToVoordelenVanAlleAardHuurHerkwalificatie(EY.com.eBook.RuleEngine.AY2010.VoordelenAppTypes.VAA_HUURHERK entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void DeleteVoordelenVanAlleAardHuurHerkwalificatie(Guid rowId,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void UpdateVoordelenVanAlleAardHuurHerkwalificatie(EY.com.eBook.RuleEngine.AY2010.VoordelenAppTypes.VAA_HUURHERK entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void AddToVoordelenVanAlleAardPC(EY.com.eBook.RuleEngine.AY2010.VoordelenAppTypes.VAA_PC entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void DeleteVoordelenVanAlleAardPC(Guid rowId,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void UpdateVoordelenVanAlleAardPC(EY.com.eBook.RuleEngine.AY2010.VoordelenAppTypes.VAA_PC entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void AddToVoordelenVanAlleAardTelefonie(EY.com.eBook.RuleEngine.AY2010.VoordelenAppTypes.VAA_Telefonie entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void DeleteVoordelenVanAlleAardTelefonie(Guid rowId,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void UpdateVoordelenVanAlleAardTelefonie(EY.com.eBook.RuleEngine.AY2010.VoordelenAppTypes.VAA_Telefonie entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void AddToVoordelenVanAlleAardOverige(EY.com.eBook.RuleEngine.AY2010.VoordelenAppTypes.VAA_Overige entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void DeleteVoordelenVanAlleAardOverige(Guid rowId,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void UpdateVoordelenVanAlleAardOverige(EY.com.eBook.RuleEngine.AY2010.VoordelenAppTypes.VAA_Overige entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void AddToVoordelenInterestenRCBedrijfsleider(EY.com.eBook.RuleEngine.AY2010.VoordelenAppTypes.VAA_Interesten_RC_Bedrijfsleider entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void DeleteVoordelenInterestenRCBedrijfsleider(Guid rowId,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void UpdateVoordelenInterestenRCBedrijfsleider(EY.com.eBook.RuleEngine.AY2010.VoordelenAppTypes.VAA_Interesten_RC_Bedrijfsleider entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void AddToVoordelenSocialeBijdragenBedrijfsleider(EY.com.eBook.RuleEngine.AY2010.VoordelenAppTypes.VAA_Soc_Bijdr_Bedrijfsleider entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void DeleteVoordelenSocialeBijdragenBedrijfsleider(Guid rowId,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void UpdateVoordelenSocialeBijdragenBedrijfsleider(EY.com.eBook.RuleEngine.AY2010.VoordelenAppTypes.VAA_Soc_Bijdr_Bedrijfsleider entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void AddToVoordelenOverigeNuts(EY.com.eBook.RuleEngine.AY2010.VoordelenAppTypes.VAA_Overige_Nuts entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void DeleteVoordelenOverigeNuts(Guid rowId,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void UpdateVoordelenOverigeNuts(EY.com.eBook.RuleEngine.AY2010.VoordelenAppTypes.VAA_Overige_Nuts entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
        #endregion
        
        #region VALIDATION
        
        public EY.com.eBook.RuleEngine.AY2010.VoordelenAppTypes.VAA_Auto Validate(EY.com.eBook.RuleEngine.AY2010.VoordelenAppTypes.VAA_Auto entity, string location,ref WorksheetSetData worksheetset)
          {
          entity.IsValid = true;
          worksheetset.ClearMessages(TypeId, entity.Id);
          

          return entity;
          }
        
        public EY.com.eBook.RuleEngine.AY2010.VoordelenAppTypes.VAA_AutoCO2 Validate(EY.com.eBook.RuleEngine.AY2010.VoordelenAppTypes.VAA_AutoCO2 entity, string location,ref WorksheetSetData worksheetset)
          {
          entity.IsValid = true;
          worksheetset.ClearMessages(TypeId, entity.Id);
          

          return entity;
          }
        
        public EY.com.eBook.RuleEngine.AY2010.VoordelenAppTypes.VAA_Nuts Validate(EY.com.eBook.RuleEngine.AY2010.VoordelenAppTypes.VAA_Nuts entity, string location,ref WorksheetSetData worksheetset)
          {
          entity.IsValid = true;
          worksheetset.ClearMessages(TypeId, entity.Id);
          

          return entity;
          }
        
        public EY.com.eBook.RuleEngine.AY2010.VoordelenAppTypes.VAA_HUUR Validate(EY.com.eBook.RuleEngine.AY2010.VoordelenAppTypes.VAA_HUUR entity, string location,ref WorksheetSetData worksheetset)
          {
          entity.IsValid = true;
          worksheetset.ClearMessages(TypeId, entity.Id);
          

          return entity;
          }
        
        public EY.com.eBook.RuleEngine.AY2010.VoordelenAppTypes.VAA_HUURHERK Validate(EY.com.eBook.RuleEngine.AY2010.VoordelenAppTypes.VAA_HUURHERK entity, string location,ref WorksheetSetData worksheetset)
          {
          entity.IsValid = true;
          worksheetset.ClearMessages(TypeId, entity.Id);
          

          return entity;
          }
        
        public EY.com.eBook.RuleEngine.AY2010.VoordelenAppTypes.VAA_PC Validate(EY.com.eBook.RuleEngine.AY2010.VoordelenAppTypes.VAA_PC entity, string location,ref WorksheetSetData worksheetset)
          {
          entity.IsValid = true;
          worksheetset.ClearMessages(TypeId, entity.Id);
          

          return entity;
          }
        
        public EY.com.eBook.RuleEngine.AY2010.VoordelenAppTypes.VAA_Telefonie Validate(EY.com.eBook.RuleEngine.AY2010.VoordelenAppTypes.VAA_Telefonie entity, string location,ref WorksheetSetData worksheetset)
          {
          entity.IsValid = true;
          worksheetset.ClearMessages(TypeId, entity.Id);
          

          return entity;
          }
        
        public EY.com.eBook.RuleEngine.AY2010.VoordelenAppTypes.VAA_Overige Validate(EY.com.eBook.RuleEngine.AY2010.VoordelenAppTypes.VAA_Overige entity, string location,ref WorksheetSetData worksheetset)
          {
          entity.IsValid = true;
          worksheetset.ClearMessages(TypeId, entity.Id);
          

          return entity;
          }
        
        public EY.com.eBook.RuleEngine.AY2010.VoordelenAppTypes.VAA_Interesten_RC_Bedrijfsleider Validate(EY.com.eBook.RuleEngine.AY2010.VoordelenAppTypes.VAA_Interesten_RC_Bedrijfsleider entity, string location,ref WorksheetSetData worksheetset)
          {
          entity.IsValid = true;
          worksheetset.ClearMessages(TypeId, entity.Id);
          

          return entity;
          }
        
        public EY.com.eBook.RuleEngine.AY2010.VoordelenAppTypes.VAA_Soc_Bijdr_Bedrijfsleider Validate(EY.com.eBook.RuleEngine.AY2010.VoordelenAppTypes.VAA_Soc_Bijdr_Bedrijfsleider entity, string location,ref WorksheetSetData worksheetset)
          {
          entity.IsValid = true;
          worksheetset.ClearMessages(TypeId, entity.Id);
          

          return entity;
          }
        
        public EY.com.eBook.RuleEngine.AY2010.VoordelenAppTypes.VAA_Overige_Nuts Validate(EY.com.eBook.RuleEngine.AY2010.VoordelenAppTypes.VAA_Overige_Nuts entity, string location,ref WorksheetSetData worksheetset)
          {
          entity.IsValid = true;
          worksheetset.ClearMessages(TypeId, entity.Id);
          

          return entity;
          }
        
        #endregion
        // CALCULATION
        
        // BOOKINGS
        
        // IMPORT PREVIOUS
        
    }
}
  