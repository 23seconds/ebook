
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.RuleEngine.Core;
using EY.com.eBook.RuleEngine.Core.Interfaces;
using EY.com.eBook.RuleEngine.Data;
using EY.com.eBook.Core;

namespace EY.com.eBook.RuleEngine.AY2016
{

    public class RisicoKapitaalHistoriekApp
    {
        public static WorksheetTypeSettings Settings = new WorksheetTypeSettings
        {
            Clients = true
          ,
            Suppliers = true
          ,
            AllAccounts = true
          ,
            PerformsBookings = true
        };

        public static Guid TypeId = new Guid("E4C15BB2-CDD4-4D9D-A7C9-7D1FD4080776");

        public List<string> ImportablePreviousTypes = new List<string>() { "EY.com.eBook.RuleEngine.AY2016.FacturatieApp" };

        public Guid GetMyType() { return TypeId; }

        public void SetMinimal()
        {
            if (Data == null)
            {
                Data = new EY.com.eBook.RuleEngine.AY2016.RisicoKapitaalHistoriekAppTypes.WPRisicokapitaalHistoriek();
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.Fiche == null)
            {
                Data.Fiche = new EY.com.eBook.RuleEngine.AY2016.RisicoKapitaalHistoriekAppTypes.FicheItem
                {
                    Id = Guid.NewGuid()
                    ,
                    LastUpdated = null
                    ,
                    IsValid = true
                    ,
                    PreviousImported = false

                };
                Data.LastUpdated = DateTime.Now;
            }
            /*
            if (Data.EigenVermogens == null)
            {
                Data.EigenVermogens = new List<EY.com.eBook.RuleEngine.AY2016.RisicoKapitaalHistoriekAppTypes.EigenVermogen>(); ;
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.Bestanddelen == null)
            {
                Data.Bestanddelen = new List<EY.com.eBook.RuleEngine.AY2016.RisicoKapitaalHistoriekAppTypes.Bestanddeel>(); ;
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.Wijzigingen == null)
            {
                Data.Wijzigingen = new List<EY.com.eBook.RuleEngine.AY2016.RisicoKapitaalHistoriekAppTypes.WijzigingItem>(); ;
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.Tarief == null)
            {
                Data.Tarief = new EY.com.eBook.RuleEngine.AY2016.RisicoKapitaalHistoriekAppTypes.TariefItem
                {
                    Id = Guid.NewGuid()
                    ,
                    LastUpdated = null
                    ,
                    IsValid = true
                    ,
                    PreviousImported = false

                };
                Data.LastUpdated = DateTime.Now;
            }
            */
            if (Data.Historiek == null)
            {
                Data.Historiek = new List<EY.com.eBook.RuleEngine.AY2016.RisicoKapitaalHistoriekAppTypes.HistoryItem>(); ;
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.AanpassingenHistoriek == null)
            {
                Data.AanpassingenHistoriek = new List<EY.com.eBook.RuleEngine.AY2016.RisicoKapitaalHistoriekAppTypes.HistoriekAanpassingItem>(); ;
                Data.LastUpdated = DateTime.Now;
            }

        }

        public string ToXml()
        {
            return CoreHelper.SerializeToString(Data);
        }

        public void LoadXml(string xml)
        {
            if (!string.IsNullOrEmpty(xml))
            {
                Data = CoreHelper.DeserializeFromString<EY.com.eBook.RuleEngine.AY2016.RisicoKapitaalHistoriekAppTypes.WPRisicokapitaalHistoriek>(xml);
            }
            SetMinimal();

        }

        public EY.com.eBook.RuleEngine.AY2016.RisicoKapitaalHistoriekAppTypes.WPRisicokapitaalHistoriek Data { get; set; }


        public void ForceRecalculation(ref WorksheetSetData worksheetset)
        {
            Data.LastUpdated = new DateTime(1980, 1, 1);
            // Cleanup old history with invalid data (empty history assessment)
            /*
             * OLD
            Data.Historiek.RemoveAll(h => h.AssessmentYear == null);
             */ 
            


            Calculate(ref worksheetset);
        }

        public void Calculate(ref WorksheetSetData worksheetset)
        {
            

            decimal beschikbaarVoorAanwending = worksheetset.TransferTaxCreditsApp.Data.Aanwendingen.ResterendVoorAanwending.GetValueOrDefault();
            beschikbaarVoorAanwending = beschikbaarVoorAanwending - worksheetset.TransferTaxCreditsApp.Data.Aanwendingen.AanwendingInvest.GetValueOrDefault();
            if (beschikbaarVoorAanwending < 0) beschikbaarVoorAanwending = 0;
            // CLEANUP OLD HISTORY
            int curAss = worksheetset.DataContract.AssessmentYear;
            Data.Historiek.RemoveAll(r => (int.Parse(r.BerekendInAJ.Id) >= curAss)
                                        || (int.Parse(r.Overdraagbaar7Jaar) < curAss && r.OnbeperktOverdraagbaarSaldo.GetValueOrDefault()==0)
                                    );
            Data.Historiek = Data.Historiek.OrderBy(h => int.Parse(h.Overdraagbaar7Jaar)).ToList();

            worksheetset.ClearMessages(TypeId);
            foreach (AY2016.RisicoKapitaalHistoriekAppTypes.HistoryItem h in Data.Historiek.ToList())
            {
                Validate(h, "Historiek", ref worksheetset);
            }

            foreach (AY2016.RisicoKapitaalHistoriekAppTypes.HistoriekAanpassingItem h in Data.AanpassingenHistoriek.ToList())
            {
                Validate(h, "AanpassingenHistoriek", ref worksheetset);
            }
           


            switch (worksheetset.RisicoKapitaalHistoriekApp.Data.Historiek.Count) {
                case 0:
                    worksheetset.SetMessage(TypeId, MessageTypeDataContract.Warning, "COUNT0", "Historiek");
                    break;

                case 1:
                    worksheetset.SetMessage(TypeId, MessageTypeDataContract.Warning, "COUNT1", "Historiek");
                    break;
            }

            // RESET aanwendingen.
            /*
            foreach (RisicoKapitaalHistoriekAppTypes.HistoryItem hi in Data.Historiek.ToList())
            {
                hi.GebruiktHuidigAJ = 0;
                hi.VervallenHuidigAJ = 0;
                /*if (!string.IsNullOrEmpty(hi.Overdraagbaar7Jaar) && int.Parse(hi.Overdraagbaar7Jaar) < curAss) //If the transferable year has passed
                {
                    //if (hi.BeperktOverdraagbaarSaldo.GetValueOrDefault() > 0 && int.Parse(hi.Overdraagbaar7Jaar) == curAss)
                    //{
                    //    hi.VervallenHuidigAJ = hi.BeperktOverdraagbaarSaldo.GetValueOrDefault();
                    //}
                    hi.BeperktOverdraagbaarSaldo = 0; //set the limited transferable amount to zero
                }  *     
                hi.BeperktOverdraagbaarVolgendAJ = hi.BeperktOverdraagbaarSaldo;
                hi.OnbeperktOverdraagbaarVolgendAJ = hi.OnbeperktOverdraagbaarSaldo;
            }*/

            decimal remaining = beschikbaarVoorAanwending;
            decimal onbepRemaing = remaining > 1000000 ? (remaining - 1000000) * (decimal)0.4 : 0;
            remaining = remaining - onbepRemaing;

                
            // AANWENDING BEPERKTE SALDI
            foreach (RisicoKapitaalHistoriekAppTypes.HistoryItem hi in Data.Historiek.Where(h=>int.Parse(h.BerekendInAJ.Id) < curAss).OrderBy(h => int.Parse(h.Overdraagbaar7Jaar)).ToList())
            {

                hi.GebruiktHuidigAJ = 0;
                hi.VervallenHuidigAJ = 0;

                if (!string.IsNullOrEmpty(hi.Overdraagbaar7Jaar) && int.Parse(hi.Overdraagbaar7Jaar) < curAss) //Before current year
                {
                    hi.BeperktOverdraagbaarSaldo = 0;
                }

                //Set values for next year
                hi.BeperktOverdraagbaarVolgendAJ = hi.BeperktOverdraagbaarSaldo;
                hi.OnbeperktOverdraagbaarVolgendAJ = hi.OnbeperktOverdraagbaarSaldo;
                if (!string.IsNullOrEmpty(hi.Overdraagbaar7Jaar) && int.Parse(hi.Overdraagbaar7Jaar) == curAss && remaining == 0) //Current year, nothing remains
                {
                    hi.VervallenHuidigAJ = hi.BeperktOverdraagbaarSaldo.GetValueOrDefault();
                    hi.BeperktOverdraagbaarVolgendAJ = 0;
                }

                if (hi.BeperktOverdraagbaarSaldo.GetValueOrDefault() > 0 && remaining > 0)
                {
                    if (hi.BeperktOverdraagbaarSaldo.GetValueOrDefault() > remaining)
                    {
                        hi.GebruiktHuidigAJ = remaining;

                        if (!string.IsNullOrEmpty(hi.Overdraagbaar7Jaar) && int.Parse(hi.Overdraagbaar7Jaar) == curAss) //Current year
                        {
                            hi.VervallenHuidigAJ = hi.BeperktOverdraagbaarSaldo.GetValueOrDefault() - hi.GebruiktHuidigAJ.GetValueOrDefault();
                            hi.BeperktOverdraagbaarVolgendAJ = 0;
                        }
                        if (!string.IsNullOrEmpty(hi.Overdraagbaar7Jaar) && int.Parse(hi.Overdraagbaar7Jaar) > curAss) //Beyond current year
                        {
                            hi.BeperktOverdraagbaarVolgendAJ = hi.BeperktOverdraagbaarSaldo.GetValueOrDefault() - hi.GebruiktHuidigAJ.GetValueOrDefault();
                        }
                        /*else  //Before current year (handle if no remaining too
                        {
                            hi.BeperktOverdraagbaarVolgendAJ = 0;
                        }*/
                        remaining = 0;

                    }
                    else
                    {
                        hi.GebruiktHuidigAJ = hi.BeperktOverdraagbaarSaldo.GetValueOrDefault();
                        hi.BeperktOverdraagbaarVolgendAJ = 0;
                        hi.VervallenHuidigAJ = 0;
                        remaining -= hi.GebruiktHuidigAJ.GetValueOrDefault();
                    }

                }
    }

           // if (beschikbaarVoorAanwending > 0)
           // {

                if (remaining > 0)
                {
                    // AANWENDING ONBEPERKTE SALDI
                    foreach (RisicoKapitaalHistoriekAppTypes.HistoryItem hi in Data.Historiek.Where(h => int.Parse(h.BerekendInAJ.Id) < curAss && h.OnbeperktOverdraagbaarSaldo.GetValueOrDefault()>0).OrderBy(h => int.Parse(h.Overdraagbaar7Jaar)).ToList())
                    {
                        if (hi.OnbeperktOverdraagbaarSaldo.GetValueOrDefault() > 0 && remaining > 0)
                        {
                            decimal used = 0;
                            if (hi.OnbeperktOverdraagbaarSaldo.GetValueOrDefault() > remaining)
                            {
                                used = remaining;
                               
                                remaining = 0;
                               
                            }
                            else
                            {
                                used = hi.OnbeperktOverdraagbaarSaldo.GetValueOrDefault();
                                remaining -= used;
                            }
                            hi.GebruiktHuidigAJ += used;
                            hi.OnbeperktOverdraagbaarVolgendAJ = hi.OnbeperktOverdraagbaarSaldo.GetValueOrDefault() - used;
                            if (remaining == 0) break;
                        }
                    }

                }

                // VERDELING REMAINING 40% (IF > 1.000.000)
               //  Als > 1.000.000 dan mag je 60% aanwenden en 40% onbeperkt overdragen
            // hier gaan we dan ook de overige 40% die onbeperkt overdraagbaar BIJKOMT verdelen uit wat anders beperkt zou zijn.
                if(onbepRemaing>0) {
                    foreach (RisicoKapitaalHistoriekAppTypes.HistoryItem hi in Data.Historiek.Where(h => int.Parse(h.BerekendInAJ.Id) < curAss && h.BeperktOverdraagbaarVolgendAJ.GetValueOrDefault() > 0).OrderBy(h => int.Parse(h.Overdraagbaar7Jaar)).ToList())
                    {
                        if (hi.BeperktOverdraagbaarVolgendAJ.GetValueOrDefault() > onbepRemaing)
                        {
                            hi.OnbeperktOverdraagbaarVolgendAJ = onbepRemaing;
                            hi.BeperktOverdraagbaarVolgendAJ = hi.BeperktOverdraagbaarVolgendAJ.GetValueOrDefault() - onbepRemaing;
                            onbepRemaing = 0;
                            break;
                        }
                        else
                        {
                            hi.OnbeperktOverdraagbaarVolgendAJ = hi.BeperktOverdraagbaarVolgendAJ;
                            hi.BeperktOverdraagbaarVolgendAJ = 0;
                            onbepRemaing -= hi.OnbeperktOverdraagbaarVolgendAJ.GetValueOrDefault();
                        }
                    }
                }

           // }

            // copy fiche from risicoKapitaal (except 4 last fields)
            Data.Fiche.SaldoBelgischResterend = worksheetset.RisicoKapitaalApp.Data.Fiche.SaldoBelgischResterend;
            Data.Fiche.EigenVermogen = worksheetset.RisicoKapitaalApp.Data.Fiche.EigenVermogen;
            Data.Fiche.EigenAandelen = worksheetset.RisicoKapitaalApp.Data.Fiche.EigenAandelen;
            Data.Fiche.FinancVasteActiva = worksheetset.RisicoKapitaalApp.Data.Fiche.FinancVasteActiva;

            Data.Fiche.AandelenDBI = worksheetset.RisicoKapitaalApp.Data.Fiche.AandelenDBI;
            
            Data.Fiche.MvaOnredelijk = worksheetset.RisicoKapitaalApp.Data.Fiche.MvaOnredelijk;
            Data.Fiche.BestanddelenBelegging = worksheetset.RisicoKapitaalApp.Data.Fiche.BestanddelenBelegging;
            Data.Fiche.OnroerendeBedrijfsleiders = worksheetset.RisicoKapitaalApp.Data.Fiche.OnroerendeBedrijfsleiders;
            Data.Fiche.UitgedrukteMeerwaarden = worksheetset.RisicoKapitaalApp.Data.Fiche.UitgedrukteMeerwaarden;
            Data.Fiche.Belastingkrediet = worksheetset.RisicoKapitaalApp.Data.Fiche.Belastingkrediet;
            Data.Fiche.KapitaalSubsidies = worksheetset.RisicoKapitaalApp.Data.Fiche.KapitaalSubsidies;
            Data.Fiche.Voorraadactualisering = worksheetset.RisicoKapitaalApp.Data.Fiche.Voorraadactualisering;
            Data.Fiche.Hoofdzetel = worksheetset.RisicoKapitaalApp.Data.Fiche.Hoofdzetel;
            Data.Fiche.BelgischeInrichting = worksheetset.RisicoKapitaalApp.Data.Fiche.BelgischeInrichting;
            Data.Fiche.Totaal = worksheetset.RisicoKapitaalApp.Data.Fiche.Totaal;
            Data.Fiche.PositieveWijzigingen = worksheetset.RisicoKapitaalApp.Data.Fiche.PositieveWijzigingen;
            Data.Fiche.NegatieveWijzigingen = worksheetset.RisicoKapitaalApp.Data.Fiche.NegatieveWijzigingen;
            Data.Fiche.RisicoKapitaalBelastbaar = worksheetset.RisicoKapitaalApp.Data.Fiche.RisicoKapitaalBelastbaar;
            Data.Fiche.RisicoKapitaalBelastbaarOverrule = worksheetset.RisicoKapitaalApp.Data.Fiche.RisicoKapitaalBelastbaarOverrule;
            Data.Fiche.TariefKleineVennootschap = worksheetset.RisicoKapitaalApp.Data.Fiche.TariefKleineVennootschap;
            Data.Fiche.NotioneelPercentage = worksheetset.RisicoKapitaalApp.Data.Fiche.NotioneelPercentage;
            Data.Fiche.Aftrekbaar = worksheetset.RisicoKapitaalApp.Data.Fiche.Aftrekbaar;
            Data.Fiche.AftrekbaarOverrule = worksheetset.RisicoKapitaalApp.Data.Fiche.AftrekbaarOverrule;

            Data.Fiche.VerminderingInEER = worksheetset.RisicoKapitaalApp.Data.Fiche.VerminderingInEER;
            Data.Fiche.VerminderingBuitenEER = worksheetset.RisicoKapitaalApp.Data.Fiche.VerminderingBuitenEER;
            Data.Fiche.AftrekbaarNaAftrek = worksheetset.RisicoKapitaalApp.Data.Fiche.AftrekbaarNaAftrek;

            Data.Fiche.WerkelijkAfgetrokken = worksheetset.RisicoKapitaalApp.Data.Fiche.WerkelijkAfgetrokken;
            /* new fiche AY14 
            Data.Fiche.OverdraagbaarSaldo = worksheetset.RisicoKapitaalApp.Data.Fiche.OverdraagbaarSaldo;
            */
            Data.Fiche.GevormdeVrijstellingen = Data.Historiek.Sum(h => h.BeperktOverdraagbaarSaldo.GetValueOrDefault())
                                                + Data.Historiek.Sum(h => h.OnbeperktOverdraagbaarSaldo.GetValueOrDefault());
                                                 //+ Data.Historiek.Sum(h => h.VervallenHuidigAJ.GetValueOrDefault());

            /* new fiche AY14
            Data.Fiche.SaldoVorigeAJGevormd = Data.Historiek.Sum(h => h.BeperktOverdraagbaarSaldo.GetValueOrDefault())
                                                + Data.Historiek.Sum(h => h.OnbeperktOverdraagbaarSaldo.GetValueOrDefault());
            */
            Data.Fiche.WerkelijkAfgetrokkenVrijstellingen = Data.Historiek.Sum(h => h.GebruiktHuidigAJ.GetValueOrDefault());
            Data.Fiche.SaldoVervallendHuidigAJ = Data.Historiek.Sum(h => h.VervallenHuidigAJ.GetValueOrDefault());
            Data.Fiche.SaldoOvergedragenVrijstellingen = Data.Fiche.GevormdeVrijstellingen - Data.Fiche.WerkelijkAfgetrokkenVrijstellingen;

            /* new fiche AY14 (valt weg)
            Data.Fiche.WerkelijkVorigAJ = Data.Historiek.Sum(h => h.GebruiktHuidigAJ.GetValueOrDefault());
            /*if (Data.Fiche.SaldoVorigeAJGevormd.GetValueOrDefault() > 0)
            {
                Data.Fiche.WerkelijkVorigAJ = beschikbaarVoorAanwending - remaining - onbepRemaing;
            }
            else
            {
                Data.Fiche.WerkelijkVorigAJ = 0;
            }*/
            /*
            Data.Fiche.TotaalAftrekAJ = Data.Fiche.WerkelijkAfgetrokken.GetValueOrDefault() + Data.Fiche.WerkelijkVorigAJ.GetValueOrDefault();
            Data.Fiche.SaldoRisicoOverdraagbaarForm = Data.Fiche.OverdraagbaarSaldo + Data.Fiche.SaldoVorigeAJGevormd - Data.Fiche.WerkelijkVorigAJ;
            Data.Fiche.SaldoRisicoOverdraagbaar = Data.Fiche.SaldoRisicoOverdraagbaarForm;
            //worksheetset.Messages.RemoveAll(m => m.ConnectionGuid == TypeId);
            */
            /*
            // copy eigen vermogen
            foreach (RisicoKapitaalAppTypes.EigenVermogen orig in worksheetset.RisicoKapitaalApp.Data.EigenVermogens.Where(w => w.LastUpdated.HasValue || w.LastUpdated > Data.LastUpdated))
            {
                RisicoKapitaalHistoriekAppTypes.EigenVermogen cur = Data.EigenVermogens.FirstOrDefault(e => e.Id == orig.Id);
                if (cur == null) {
                    cur = new EY.com.eBook.RuleEngine.AY2016.RisicoKapitaalHistoriekAppTypes.EigenVermogen { 
                        Id = orig.Id
                    };
                    Data.EigenVermogens.Add(cur);
                }
                cur.Auto = orig.Auto;
                cur.Evolutie = orig.Evolutie;
                cur.Grootboek = orig.Grootboek;
                cur.HuidigBj = orig.HuidigBj;
                cur.HuidigBjImported = orig.HuidigBjImported;
                cur.IsValid = orig.IsValid;
                cur.LastUpdated = orig.LastUpdated;
                cur.Omschrijving = orig.Omschrijving;
                cur.Overdracht = orig.Overdracht;
                cur.PreviousImported = orig.PreviousImported;
                cur.Type = orig.Type;
                cur.VorigBj = orig.VorigBj;
                cur.VorigBjImported = orig.VorigBjImported;
                cur.WerkelijkeWijziging = orig.WerkelijkeWijziging;
            }
            List<RisicoKapitaalAppTypes.EigenVermogen> lstEigenVermogen = worksheetset.RisicoKapitaalApp.Data.EigenVermogens;
            Data.EigenVermogens.RemoveAll(e => !lstEigenVermogen.Select(r => r.Id).Contains(e.Id));

            // copy correctie eigen vermogen
            foreach (RisicoKapitaalAppTypes.Bestanddeel orig in worksheetset.RisicoKapitaalApp.Data.Bestanddelen.Where(w => !w.LastUpdated.HasValue || w.LastUpdated > Data.LastUpdated))
            {
                RisicoKapitaalHistoriekAppTypes.Bestanddeel cur = Data.Bestanddelen.FirstOrDefault(b => b.Id == orig.Id);
                if (cur == null)
                {
                    cur = new EY.com.eBook.RuleEngine.AY2016.RisicoKapitaalHistoriekAppTypes.Bestanddeel
                    {
                        Id = orig.Id
                    };
                    Data.Bestanddelen.Add(cur);
                }
                cur.Auto = orig.Auto;
                cur.Evolutie = orig.Evolutie;
                cur.Grootboek = orig.Grootboek;
                cur.HuidigBj = orig.HuidigBj;
                cur.HuidigBjImported = orig.HuidigBjImported;
                cur.IsValid = orig.IsValid;
                cur.LastUpdated = orig.LastUpdated;
                cur.Omschrijving = orig.Omschrijving;
                cur.Overdracht = orig.Overdracht;
                cur.PreviousImported = orig.PreviousImported;
                cur.Type = orig.Type;
                cur.VorigBj = orig.VorigBj;
                cur.VorigBjImported = orig.VorigBjImported;
                cur.WerkelijkeWijziging = orig.WerkelijkeWijziging;

            }
            List<RisicoKapitaalAppTypes.Bestanddeel> lstBestanddelen = worksheetset.RisicoKapitaalApp.Data.Bestanddelen;
            Data.Bestanddelen.RemoveAll(r => !lstBestanddelen.Select(b => b.Id).Contains(r.Id));


            // copy wijzingen
            foreach (RisicoKapitaalAppTypes.WijzigingItem orig in worksheetset.RisicoKapitaalApp.Data.Wijzigingen) {

                RisicoKapitaalHistoriekAppTypes.WijzigingItem cur = Data.Wijzigingen.FirstOrDefault(w=>w.Id == orig.Id);
                if (cur == null)
                {
                    cur = new EY.com.eBook.RuleEngine.AY2016.RisicoKapitaalHistoriekAppTypes.WijzigingItem
                    {
                        Id = orig.Id
                    };
                    Data.Wijzigingen.Add(cur);
                }
                    cur.AardToename = orig.AardToename;
                    cur.AfOfToename = orig.AfOfToename;
                    cur.Bestaand = orig.Bestaand;
                    cur.BestaandTeWijzigen = orig.BestaandTeWijzigen;
                    cur.DatumTransactie = orig.DatumTransactie;
                    cur.Gewijzigde = orig.Gewijzigde;
                    cur.Grootboek = orig.Grootboek;
                    cur.IsValid = orig.IsValid;
                    cur.LastUpdated = orig.LastUpdated;
                    cur.Notitie = orig.Notitie;
                    cur.PosNeg = orig.PosNeg;
                    cur.PreviousImported = orig.PreviousImported;
                    cur.TeWijzigen = orig.TeWijzigen;
                    cur.TypeAftrekNID = orig.TypeAftrekNID;
                    cur.TypeNID = orig.TypeNID;
                    cur.VorigBj = orig.VorigBj;
                    cur.Wijziging = orig.Wijziging;
                    cur.Wijzigtype = orig.Wijzigtype;
                    cur.XbrlValue = orig.XbrlValue;
            }
                List<RisicoKapitaalAppTypes.WijzigingItem> lstWijzigingen = worksheetset.RisicoKapitaalApp.Data.Wijzigingen;
                Data.Wijzigingen.RemoveAll(r => !lstWijzigingen.Select(b => b.Id).Contains(r.Id));
            

            // copy form Tarief
            RisicoKapitaalAppTypes.TariefItem ti = worksheetset.RisicoKapitaalApp.Data.Tarief;
            Data.Tarief.Default = ti.Default;
            Data.Tarief.eindDatum = ti.eindDatum;
            Data.Tarief.IsValid = ti.IsValid;
            Data.Tarief.laatstebeltijdperk = ti.laatstebeltijdperk;
            Data.Tarief.LastUpdated = ti.LastUpdated;
            Data.Tarief.mainTitle = ti.mainTitle;
            Data.Tarief.PreviousImported = ti.PreviousImported;
            Data.Tarief.startDatum = ti.startDatum;
            Data.Tarief.tarief = ti.tarief;
            Data.Tarief.voorlaatstebeltijdperk = ti.voorlaatstebeltijdperk;
            */

            Data.LastUpdated = DateTime.Now;
        }
        /*
        private void CalculateTarif(ref WorksheetSetData worksheetset)
        {
            Data.Tarief.startDatum = worksheetset.DataContract.StartDate;
            Data.Tarief.eindDatum = worksheetset.DataContract.EndDate;
            Data.Tarief.laatstebeltijdperk = worksheetset.Settings.PreviousPeriodSmall;
            Data.Tarief.voorlaatstebeltijdperk = worksheetset.Settings.BeforePreviousPeriodSmall;


            CoefficientDataContract cdc = null;

            if ((worksheetset.Settings.PreviousPeriodSmall && worksheetset.Settings.BeforePreviousPeriodSmall) || (worksheetset.Settings.CurrentPeriodSmall && worksheetset.Settings.FirstBookyear))
            {

                cdc = worksheetset.GetCoefficient("NID_VERHOOGD", worksheetset.DataContract.AssessmentYear);
                Data.Fiche.TariefKleineVennootschap = true;
            }
            else
            {
                cdc = worksheetset.GetCoefficient("NID", worksheetset.DataContract.AssessmentYear);
                Data.Fiche.TariefKleineVennootschap = false;
            }

            DateTime enddate = Data.Tarief.eindDatum.Value;
            DateTime calcEnd = Data.Tarief.startDatum.Value.AddDays(-1).AddYears(1);
            if (calcEnd.Year == enddate.Year && calcEnd.Month == enddate.Month && calcEnd.Day == enddate.Day)
            {
                Data.Tarief.Default = true;
                Data.Tarief.tarief = cdc.Value;
            }
            else
            {
                Data.Tarief.tarief = ((cdc.Value / 365) * (decimal)(enddate.AddDays(1) - Data.Tarief.startDatum.Value).TotalDays);
                Data.Tarief.Default = false;
            }
            Data.Fiche.NotioneelPercentage = Data.Tarief.tarief;


        }
        */
        /*
             * OLD
         * private void CalculateFiche(ref WorksheetSetData worksheetset)
        {
            Data.Fiche.EigenVermogen = worksheetset.RisicoKapitaalApp.Data.Fiche.EigenVermogen;
            //Data.Fiche.EigenVermogen = Data.EigenVermogens.Sum(d => d.VorigBj);
            Data.Fiche.EigenAandelen = Data.Bestanddelen.Where(e => e.Type != null && e.Type.Id.ToString().ToLower() == "96f3de0b-8a57-4d47-a4ef-0a0aa4c1bea8").Sum(d => d.VorigBj);

            Data.Fiche.FinancVasteActiva = Data.Bestanddelen.Where(e => e.Type != null && e.Type.Id.ToString().ToLower() == "a1219a9b-6355-4c5b-a43d-dc1931ce1a9a").Sum(d => d.VorigBj);
            Data.Fiche.AandelenBelegging = Data.Bestanddelen.Where(e => e.Type != null && e.Type.Id.ToString().ToLower() == "86169623-3b27-435c-bf65-7932204b31bc").Sum(d => d.VorigBj);
            Data.Fiche.InrichtingVerdrag = Data.Bestanddelen.Where(e => e.Type != null && e.Type.Id.ToString().ToLower() == "b5c8aa77-a3d0-43a9-9798-33cd865f9340").Sum(d => d.VorigBj);
            Data.Fiche.OnroerendVerdrag = Data.Bestanddelen.Where(e => e.Type != null && e.Type.Id.ToString().ToLower() == "803b2f0f-7ce2-4d62-9675-bb7eecf04598").Sum(d => d.VorigBj);
            Data.Fiche.MvaOnredelijk = Data.Bestanddelen.Where(e => e.Type != null && e.Type.Id.ToString().ToLower() == "97f9d1aa-364e-493b-940f-769cee502bf2").Sum(d => d.VorigBj);
            Data.Fiche.BestanddelenBelegging = Data.Bestanddelen.Where(e => e.Type != null && e.Type.Id.ToString().ToLower() == "a4982a60-b8f5-40e9-af6f-659ebf27d8f6").Sum(d => d.VorigBj);
            Data.Fiche.OnroerendeBedrijfsleiders = Data.Bestanddelen.Where(e => e.Type != null && e.Type.Id.ToString().ToLower() == "5223b43c-f421-4222-9480-a5750e736835").Sum(d => d.VorigBj);
            Data.Fiche.UitgedrukteMeerwaarden = Data.Bestanddelen.Where(e => e.Type != null && e.Type.Id.ToString().ToLower() == "748adf93-8ac7-42de-baf2-e9b3b59868dc").Sum(d => d.VorigBj);
            Data.Fiche.Belastingkrediet = Data.Bestanddelen.Where(e => e.Type != null && e.Type.Id.ToString().ToLower() == "6c5f1e7f-d8a8-4ee1-9422-32a4b9ef1a1e").Sum(d => d.VorigBj);
            Data.Fiche.KapitaalSubsidies = Data.Bestanddelen.Where(e => e.Type != null && e.Type.Id.ToString().ToLower() == "45270189-35ca-4d6e-8b3d-0e27fc06b314").Sum(d => d.VorigBj);
            Data.Fiche.Voorraadactualisering = Data.Bestanddelen.Where(e => e.Type != null && e.Type.Id.ToString().ToLower() == "fd102075-f991-4aea-97a2-700d1b84ea6a").Sum(d => d.VorigBj);
            Data.Fiche.Hoofdzetel = Data.Bestanddelen.Where(e => e.Type != null && e.Type.Id.ToString().ToLower() == "6a1c6084-6d02-4ee7-aac9-011835593c0f").Sum(d => d.VorigBj);
            Data.Fiche.BelgischeInrichting = Data.Bestanddelen.Where(e => e.Type != null && e.Type.Id.ToString().ToLower() == "dcf6e302-459c-46ff-8b83-6a865d7d2212").Sum(d => d.VorigBj);

            Data.Fiche.Totaal = Data.Bestanddelen.Sum(d => d.VorigBj);

            Data.Fiche.PositieveWijzigingen = Data.Wijzigingen.Where(e => e.PosNeg == "+").Sum(e => Math.Abs(e.TeWijzigen.GetValueOrDefault()));
            Data.Fiche.NegatieveWijzigingen = Data.Wijzigingen.Where(e => e.PosNeg == "-").Sum(e => Math.Abs(e.TeWijzigen.GetValueOrDefault()));

            Data.Fiche.WijzigingenCumul = Data.Fiche.PositieveWijzigingen.GetValueOrDefault() - Data.Fiche.NegatieveWijzigingen.GetValueOrDefault();


            Data.Fiche.RisicoKapitaalBelastbaar = Data.Fiche.EigenVermogen.GetValueOrDefault() - Data.Fiche.Totaal.GetValueOrDefault() + Data.Fiche.PositieveWijzigingen.GetValueOrDefault() - Data.Fiche.NegatieveWijzigingen.GetValueOrDefault();

            if (Data.Fiche.RisicoKapitaalBelastbaar < 0) Data.Fiche.RisicoKapitaalBelastbaar = 0;


            Data.Fiche.Aftrekbaar = Math.Round(Data.Fiche.RisicoKapitaalBelastbaar.GetValueOrDefault() * Data.Fiche.NotioneelPercentage.GetValueOrDefault() / 100, 2, MidpointRounding.AwayFromZero);

            int assess = worksheetset.DataContract.AssessmentYear;

            
            RisicoKapitaalHistoriekAppTypes.HistoryItem huidig = Data.Historiek.FirstOrDefault(e => e.AssessmentYear != null && e.AssessmentYear.Id == assess.ToString());
            if (huidig == null)
            {
                huidig = new EY.com.eBook.RuleEngine.AY2016.RisicoKapitaalHistoriekAppTypes.HistoryItem
                {
                    AssessmentYear = new StringListItemDataContract
                    {
                        Id = assess.ToString()
                        ,
                        NL = assess.ToString()
                        ,
                        FR = assess.ToString()
                        ,
                        EN = assess.ToString()
                    }
                    ,
                    Id = Guid.NewGuid()
                    ,
                    IsValid = true
                    ,
                    LastUpdated = DateTime.Now
                    ,
                    PreviousImported = false
                    ,
                    GebruiktHuidigBJ = 0
                    ,
                    BedragAftrek = 0
                    ,
                    BedragAftrekImport = 0
                    ,
                    Saldo = 0
                };
                Data.Historiek.Add(huidig);

            }
            huidig.BedragAftrek = Data.Fiche.Aftrekbaar;
            huidig.BedragAftrekImport = Data.Fiche.Aftrekbaar;
           
            Data.Fiche.SaldoBelgischResterend = worksheetset.DBIApp.Data.DBI.BelastbareBasisResterend;

            decimal toDistribute = Data.Fiche.SaldoBelgischResterend.HasValue ? Data.Fiche.SaldoBelgischResterend.Value : 0;
            /*
             * OLD
            if (Data.Historiek.Count(h => !h.IsValid) > 0)
            {
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "INVALID", "Historiek");
                toDistribute = 0;
                Data.Fiche.BelastbareBasisResterend = toDistribute;
            }
            else
            {

                Data.Historiek = Data.Historiek.OrderBy(h => h.AssessmentYear.Id).ToList();

                foreach (RisicoKapitaalHistoriekAppTypes.HistoryItem hist in Data.Historiek)
                {
                    hist.GebruiktHuidigBJ = 0;
                    if (!hist.BedragAftrek.HasValue) hist.BedragAftrek = hist.BedragAftrekImport.GetValueOrDefault();
                    if (toDistribute > 0)
                    {
                        if (hist.BedragAftrek.GetValueOrDefault() > toDistribute)
                        {
                            hist.GebruiktHuidigBJ = toDistribute;
                            toDistribute = 0;
                        }
                        else
                        {
                            hist.GebruiktHuidigBJ = hist.BedragAftrek.GetValueOrDefault();
                            toDistribute = toDistribute - hist.GebruiktHuidigBJ.GetValueOrDefault();
                        }
                    }
                    hist.Saldo = hist.BedragAftrek.GetValueOrDefault() - hist.GebruiktHuidigBJ.GetValueOrDefault();
                }
                
                Data.Fiche.BelastbareBasisResterend = toDistribute;
                Data.Fiche.Aanwending = Data.Fiche.SaldoBelgischResterend.GetValueOrDefault() - Data.Fiche.BelastbareBasisResterend.GetValueOrDefault();

                Data.Fiche.SaldoVorigeAJGevormd = Data.Historiek.Where(h => h.AssessmentYear.Id != assess.ToString()).Sum(h => h.BedragAftrek);
                Data.Fiche.WerkelijkVorigAJ = Data.Historiek.Where(h => h.AssessmentYear.Id != assess.ToString()).Sum(h => h.GebruiktHuidigBJ);
                Data.Fiche.WerkelijkAfgetrokken = huidig.GebruiktHuidigBJ.GetValueOrDefault();

                Data.Fiche.OverdraagbaarSaldoOverrule = Data.Historiek.Where(h => h.AssessmentYear.Id != assess.ToString()).Sum(h => h.Saldo);

                Data.Fiche.OverdraagbaarSaldo = Data.Fiche.Aftrekbaar - Data.Fiche.WerkelijkAfgetrokken;
                Data.Fiche.TotaalAftrekAJ = Data.Fiche.WerkelijkAfgetrokken + Data.Fiche.WerkelijkVorigAJ;

                Data.Fiche.SaldoRisicoOverdraagbaarForm = Data.Fiche.OverdraagbaarSaldo + Data.Fiche.SaldoVorigeAJGevormd - Data.Fiche.WerkelijkVorigAJ;
                Data.Fiche.SaldoRisicoOverdraagbaar = Data.Fiche.SaldoRisicoOverdraagbaarForm;
            }
             

        }
     */
        #region AddUpdateDelete

        public void UpdateFiche(EY.com.eBook.RuleEngine.AY2016.RisicoKapitaalHistoriekAppTypes.FicheItem entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2016.RisicoKapitaalHistoriekAppTypes.FicheItem original = Data.Fiche;

            entity = Validate(entity, "Fiche", ref worksheetset);
            if (entity.IsValid)
            {

                Data.Fiche = entity;
                Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
            }
        }
        /*
        public void AddToEigenVermogens(EY.com.eBook.RuleEngine.AY2016.RisicoKapitaalHistoriekAppTypes.EigenVermogen entity, ref WorksheetSetData worksheetset)
        {
            entity = Validate(entity, "EigenVermogens", ref worksheetset);
            if (entity.IsValid)
            {
                entity.VorigBjImported = entity.VorigBj;
                entity.HuidigBjImported = entity.HuidigBj;
                entity.Evolutie = entity.HuidigBj - entity.VorigBj;
                Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
                Data.EigenVermogens.Add(entity);
            }
        }

        public void DeleteEigenVermogens(Guid rowId, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2016.RisicoKapitaalHistoriekAppTypes.EigenVermogen original = Data.EigenVermogens.FirstOrDefault(e => e.Id == rowId);
            if (original != null)
            {

                
                Data.EigenVermogens.Remove(original);
                Data.LastUpdated = DateTime.Now;
            }
        }

        public void UpdateEigenVermogens(EY.com.eBook.RuleEngine.AY2016.RisicoKapitaalHistoriekAppTypes.EigenVermogen entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2016.RisicoKapitaalHistoriekAppTypes.EigenVermogen original = Data.EigenVermogens.FirstOrDefault(e => e.Id == entity.Id);
            if (original == null)
            {
                AddToEigenVermogens(entity, ref worksheetset);
            }
            else
            {
                entity = Validate(entity, "EigenVermogens", ref worksheetset);
                if (entity.IsValid)
                {
                    RisicoKapitaalHistoriekAppTypes.WijzigingItem witem = Data.Wijzigingen.FirstOrDefault(w => w.Bestaand == true && w.BestaandTeWijzigen.Id == entity.Id.ToString());
                    if (witem == null)
                    {
                        entity.VorigBj = entity.VorigBjImported;
                        entity.HuidigBj = entity.HuidigBjImported;
                    }
                    entity.Evolutie = entity.HuidigBj - entity.VorigBj;
                    int idx = Data.EigenVermogens.IndexOf(original);
                    Data.EigenVermogens.RemoveAt(idx);
                    Data.EigenVermogens.Insert(idx, entity);
                    Data.LastUpdated = DateTime.Now;
                    entity.LastUpdated = DateTime.Now;
                }
            }
        }

        public void AddToBestanddelen(EY.com.eBook.RuleEngine.AY2016.RisicoKapitaalHistoriekAppTypes.Bestanddeel entity, ref WorksheetSetData worksheetset)
        {
            entity = Validate(entity, "Bestanddelen", ref worksheetset);
            if (entity.IsValid)
            {
                entity.VorigBjImported = entity.VorigBj;
                entity.HuidigBjImported = entity.HuidigBj;
                entity.Evolutie = entity.HuidigBj - entity.VorigBj;
                Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
                Data.Bestanddelen.Add(entity);
            }
        }

        public void DeleteBestanddelen(Guid rowId, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2016.RisicoKapitaalHistoriekAppTypes.Bestanddeel original = Data.Bestanddelen.FirstOrDefault(e => e.Id == rowId);
            if (original != null)
            {
                
                Data.Bestanddelen.Remove(original);
                Data.LastUpdated = DateTime.Now;
            }
        }

        public void UpdateBestanddelen(EY.com.eBook.RuleEngine.AY2016.RisicoKapitaalHistoriekAppTypes.Bestanddeel entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2016.RisicoKapitaalHistoriekAppTypes.Bestanddeel original = Data.Bestanddelen.FirstOrDefault(e => e.Id == entity.Id);
            if (original == null)
            {
                AddToBestanddelen(entity, ref worksheetset);
            }
            else
            {
                entity = Validate(entity, "Bestanddelen", ref worksheetset);
                if (entity.IsValid)
                {
                    RisicoKapitaalHistoriekAppTypes.WijzigingItem witem = Data.Wijzigingen.FirstOrDefault(w => w.Bestaand == true && w.BestaandTeWijzigen.Id == entity.Id.ToString());
                    if (witem == null)
                    {
                        entity.VorigBj = entity.VorigBjImported;
                        entity.HuidigBj = entity.HuidigBjImported;
                    }
                    entity.Evolutie = entity.HuidigBj - entity.VorigBj;
                    int idx = Data.Bestanddelen.IndexOf(original);
                    Data.Bestanddelen.RemoveAt(idx);
                    Data.Bestanddelen.Insert(idx, entity);
                    Data.LastUpdated = DateTime.Now;
                    entity.LastUpdated = DateTime.Now;
                }
            }
        }

        private void ResetEigenVermogen(ref RisicoKapitaalHistoriekAppTypes.EigenVermogen ev)
        {
            ev.VorigBj = ev.VorigBjImported;
            ev.HuidigBj = ev.HuidigBjImported;
            ev.Evolutie = ev.HuidigBj - ev.VorigBj;
            ev.WerkelijkeWijziging = null;
        }

        private void ResetBestanddeel(ref RisicoKapitaalHistoriekAppTypes.Bestanddeel bd)
        {
            bd.VorigBj = bd.VorigBjImported;
            bd.HuidigBj = bd.HuidigBjImported;
            bd.Evolutie = bd.HuidigBj - bd.VorigBj;
            bd.WerkelijkeWijziging = null;
        }


        private RisicoKapitaalHistoriekAppTypes.WijzigingItem Calculatewijziging(RisicoKapitaalHistoriekAppTypes.WijzigingItem entity, ref WorksheetSetData worksheetset)
        {
            if (entity.Bestaand == true)
            {
                RisicoKapitaalHistoriekAppTypes.EigenVermogen ev = Data.EigenVermogens.FirstOrDefault(e => e.Id.ToString().ToLower() == entity.BestaandTeWijzigen.Id.ToLower());
                RisicoKapitaalHistoriekAppTypes.Bestanddeel bd = Data.Bestanddelen.FirstOrDefault(e => e.Id.ToString().ToLower() == entity.BestaandTeWijzigen.Id.ToLower());
                if (ev != null)
                {
                    entity.Gewijzigde = ev.Omschrijving;
                }
                else if (bd != null)
                {
                    entity.Gewijzigde = bd.Omschrijving;
                }
            }
            else
            {
                switch (worksheetset.DataContract.Culture)
                {
                    case "nl-BE":
                        entity.Gewijzigde = entity.Grootboek.NL;
                        break;
                    case "fr-FR":
                        entity.Gewijzigde = entity.Grootboek.FR;
                        break;
                    case "en-US":
                        entity.Gewijzigde = entity.Grootboek.EN;
                        break;
                }
            }

            if (entity.Wijzigtype.Id.ToString().ToLower() == "310ac564-d0c0-4909-ab68-526e47d7e7bd") //1 Wijziging in de loop van het belastbare tijdperk
            {
                if (worksheetset.Settings.FirstBookyear)
                {
                    entity.TeWijzigen = entity.Wijziging;
                }
                else
                {
                    DateTime yearend = worksheetset.DataContract.StartDate.AddDays(-1).AddYears(1);
                    decimal quot = 12;
                    if (yearend > worksheetset.DataContract.EndDate)
                    {
                        quot = worksheetset.DataContract.EndDate.MonthsBetween(yearend);
                    }
                    else if (yearend < worksheetset.DataContract.EndDate)
                    {
                        quot = yearend.MonthsBetween(worksheetset.DataContract.EndDate);

                        yearend = yearend.AddMonths((int)quot);
                        decimal days = 0;
                        if (yearend > worksheetset.DataContract.EndDate)
                        {
                            days = (decimal)Math.Abs((yearend - worksheetset.DataContract.EndDate).TotalDays);
                            quot += Math.Round(days / DateTime.DaysInMonth(yearend.Year, yearend.Month), 3, MidpointRounding.AwayFromZero);
                        }
                        else if (yearend < worksheetset.DataContract.EndDate)
                        {
                            days = (decimal)Math.Abs((worksheetset.DataContract.EndDate - yearend).TotalDays);
                            quot -= Math.Round(days / DateTime.DaysInMonth(yearend.Year, yearend.Month), 3, MidpointRounding.AwayFromZero);
                        }
                        quot += 12;
                    }
                    quot = entity.DatumTransactie.Value.FullMonthsBetween(worksheetset.DataContract.EndDate) / quot;
                    entity.TeWijzigen = Math.Round(entity.Wijziging.Value * quot, 2, MidpointRounding.AwayFromZero);
                }

            }

            return entity;
        }

        private RisicoKapitaalHistoriekAppTypes.WijzigingItem PerformWijziging(RisicoKapitaalHistoriekAppTypes.WijzigingItem entity, ref WorksheetSetData worksheetset, bool delete)
        {
            RisicoKapitaalHistoriekAppTypes.EigenVermogen ev = null;
            RisicoKapitaalHistoriekAppTypes.Bestanddeel bd = null;
            if (entity.BestaandTeWijzigen != null)
            {
                ev = Data.EigenVermogens.FirstOrDefault(e => e.Id.ToString() == entity.BestaandTeWijzigen.Id);
                bd = Data.Bestanddelen.FirstOrDefault(e => e.Id.ToString() == entity.BestaandTeWijzigen.Id);
                if (delete)
                {
                    if (entity.Bestaand == true)
                    {
                        if (ev != null)
                        {
                            ResetEigenVermogen(ref ev);
                        }
                        if (bd != null)
                        {
                            ResetBestanddeel(ref bd);
                        }
                    }
                    else
                    {
                        if (ev != null) Data.EigenVermogens.Remove(ev);
                        if (bd != null) Data.Bestanddelen.Remove(bd);
                    }
                    return null;
                }
            }

            decimal werkwijziging = entity.Wijziging.Value;
            //decimal wijziging = entity.TeWijzigen.Value;
            if (entity.Bestaand == true)
            {

                switch (entity.Wijzigtype.Id.ToString().ToLower())
                {
                    case "310ac564-d0c0-4909-ab68-526e47d7e7bd": //1 Wijziging in de loop van het belastbare tijdperk
                        decimal wijziging = entity.TeWijzigen.Value;
                        switch (entity.AfOfToename.Id.ToString().ToLower())
                        {
                            case "e5c1abfa-47bc-4e0d-b287-3f10e5a752ed": // + toename
                                wijziging = Math.Abs(wijziging);
                                werkwijziging = Math.Abs(werkwijziging);
                                break;
                            case "396aee1e-878d-4ffe-8524-1b22a49607b3": // - afname
                                wijziging = 0 - Math.Abs(wijziging);
                                werkwijziging = 0 - Math.Abs(werkwijziging);
                                break;
                        }
                        if (ev != null)
                        {
                            if (!ev.WerkelijkeWijziging.HasValue || ev.WerkelijkeWijziging == 0)
                            {
                                ev.HuidigBj = ev.HuidigBj + wijziging;
                                ev.Evolutie = ev.Evolutie + wijziging;
                                ev.WerkelijkeWijziging = werkwijziging;
                            }
                            else
                            {
                                ev.HuidigBj = wijziging;
                                ev.Evolutie = wijziging;
                                ev.WerkelijkeWijziging = werkwijziging;
                            }
                            entity.PosNeg = wijziging > 0 ? "+" : "-";
                        }
                        else if (bd != null)
                        {
                            if (!bd.WerkelijkeWijziging.HasValue || bd.WerkelijkeWijziging == 0)
                            {
                                bd.HuidigBj = bd.HuidigBj + wijziging;
                                bd.Evolutie = bd.Evolutie + wijziging;
                                bd.WerkelijkeWijziging = werkwijziging;
                            }
                            else
                            {
                                bd.HuidigBj = wijziging;
                                bd.Evolutie = wijziging;
                                bd.WerkelijkeWijziging = werkwijziging;
                            }
                            entity.PosNeg = wijziging > 0 ? "-" : "+";
                        }
                        Data.Fiche.PositieveWijzigingen = Data.Wijzigingen.Where(e => e.PosNeg == "+").Sum(e => Math.Abs(e.Wijziging.Value));
                        Data.Fiche.NegatieveWijzigingen = Data.Wijzigingen.Where(e => e.PosNeg == "-").Sum(e => Math.Abs(e.Wijziging.Value));
                        break;
                    case "7edb61e5-fa4f-4115-887b-d644c3b36a6a": //2 Aanpassing van de begintoestand
                        if (ev != null)
                        {
                            ev.VorigBj = entity.VorigBj;
                            ev.Evolutie = ev.HuidigBj - ev.VorigBj;
                        }
                        else if (bd != null)
                        {
                            bd.VorigBj = entity.VorigBj;
                            bd.Evolutie = bd.HuidigBj - bd.VorigBj;
                        }
                        entity.PosNeg = string.Empty;
                        break;
                }
            }
            else
            {
                switch (entity.Wijzigtype.Id.ToString().ToLower())
                {
                    case "310ac564-d0c0-4909-ab68-526e47d7e7bd": //1 Wijziging in de loop van het belastbare tijdperk
                        decimal wijziging = entity.TeWijzigen.Value;


                        if (entity.AfOfToename.Id.ToString().ToLower() == "396aee1e-878d-4ffe-8524-1b22a49607b3"
                            || (entity.AardToename != null && entity.AardToename.Id.ToString().ToLower() == "c5a3de38-65cb-4800-9742-a2b12954d92f"))
                        {
                            wijziging = 0 - Math.Abs(wijziging);
                            werkwijziging = 0 - Math.Abs(werkwijziging);
                        }
                        else
                        {
                            wijziging = Math.Abs(wijziging);
                            werkwijziging = Math.Abs(werkwijziging);
                        }
                        if (entity.TypeNID != null)
                        {
                            if (bd != null) ResetBestanddeel(ref bd);
                            if (ev == null)
                            {
                                ev = new EY.com.eBook.RuleEngine.AY2016.RisicoKapitaalHistoriekAppTypes.EigenVermogen
                                {
                                    Id = Guid.NewGuid()
                                    ,
                                    Omschrijving = entity.Gewijzigde
                                    ,
                                    Type = entity.TypeNID
                                    ,
                                    Grootboek = entity.Grootboek
                                    ,
                                    HuidigBjImported = wijziging
                                    ,
                                    VorigBj = 0
                                    ,
                                    VorigBjImported = 0
                                };
                                entity.BestaandTeWijzigen = new StringListItemDataContract
                                {
                                    Id = ev.Id.ToString()
                                    ,
                                    NL = ev.Omschrijving
                                    ,
                                    EN = ev.Omschrijving
                                    ,
                                    FR = ev.Omschrijving
                                };
                                Data.EigenVermogens.Add(ev);
                            }
                            ev.Type = entity.TypeNID;
                            ev.HuidigBj = wijziging;
                            ev.Evolutie = wijziging;
                            ev.WerkelijkeWijziging = werkwijziging;
                            entity.PosNeg = wijziging > 0 ? "+" : "-";

                        }
                        else if (entity.TypeAftrekNID != null)
                        {
                            if (ev != null) ResetEigenVermogen(ref ev);
                            if (bd == null)
                            {
                                bd = new EY.com.eBook.RuleEngine.AY2016.RisicoKapitaalHistoriekAppTypes.Bestanddeel
                                {
                                    Id = Guid.NewGuid()
                                    ,
                                    Omschrijving = entity.Gewijzigde
                                    ,
                                    Type = entity.TypeAftrekNID
                                    ,
                                    Grootboek = entity.Grootboek
                                    ,
                                    HuidigBjImported = wijziging
                                    ,
                                    VorigBj = 0
                                    ,
                                    VorigBjImported = 0
                                };
                                entity.BestaandTeWijzigen = new StringListItemDataContract
                                {
                                    Id = bd.Id.ToString()
                                    ,
                                    NL = bd.Omschrijving
                                    ,
                                    EN = bd.Omschrijving
                                    ,
                                    FR = bd.Omschrijving
                                };
                                Data.Bestanddelen.Add(bd);
                            }
                            bd.Type = entity.TypeAftrekNID;
                            bd.HuidigBj = wijziging;
                            bd.Evolutie = wijziging;
                            bd.Overdracht = werkwijziging;
                            entity.PosNeg = wijziging > 0 ? "-" : "+";
                        }
                        Data.Fiche.PositieveWijzigingen = Data.Wijzigingen.Where(e => e.PosNeg == "+").Sum(e => Math.Abs(e.Wijziging.Value));
                        Data.Fiche.NegatieveWijzigingen = Data.Wijzigingen.Where(e => e.PosNeg == "-").Sum(e => Math.Abs(e.Wijziging.Value));
                        break;
                    case "7edb61e5-fa4f-4115-887b-d644c3b36a6a": //2 Aanpassing van de begintoestand
                        if (entity.TypeNID != null)
                        {
                            if (bd != null) ResetBestanddeel(ref bd);
                            if (ev == null)
                            {
                                ev = new EY.com.eBook.RuleEngine.AY2016.RisicoKapitaalHistoriekAppTypes.EigenVermogen
                                {
                                    Id = Guid.NewGuid()
                                    ,
                                    Omschrijving = entity.Gewijzigde
                                    ,
                                    Type = entity.TypeNID
                                    ,
                                    Grootboek = entity.Grootboek
                                    ,
                                    HuidigBjImported = 0
                                    ,
                                    VorigBj = 0
                                    ,
                                    VorigBjImported = 0
                                };
                                entity.BestaandTeWijzigen = new StringListItemDataContract
                                {
                                    Id = ev.Id.ToString()
                                    ,
                                    NL = ev.Omschrijving
                                    ,
                                    EN = ev.Omschrijving
                                    ,
                                    FR = ev.Omschrijving
                                };
                                Data.EigenVermogens.Add(ev);
                            }
                            ev.Type = entity.TypeNID;
                            ev.VorigBj = entity.VorigBj;
                            ev.Evolutie = (ev.HuidigBj.HasValue ? ev.HuidigBj.Value : 0) - ev.VorigBj;
                            ev.WerkelijkeWijziging = entity.Wijziging;
                        }
                        else if (entity.TypeAftrekNID != null)
                        {
                            if (bd == null)
                            {
                                bd = new EY.com.eBook.RuleEngine.AY2016.RisicoKapitaalHistoriekAppTypes.Bestanddeel
                                {
                                    Id = Guid.NewGuid()
                                    ,
                                    Omschrijving = entity.Gewijzigde
                                    ,
                                    Type = entity.TypeAftrekNID
                                    ,
                                    Grootboek = entity.Grootboek
                                    ,
                                    HuidigBjImported = 0
                                    ,
                                    VorigBj = 0
                                    ,
                                    VorigBjImported = 0
                                };
                                entity.BestaandTeWijzigen = new StringListItemDataContract
                                {
                                    Id = bd.Id.ToString()
                                    ,
                                    NL = bd.Omschrijving
                                    ,
                                    EN = bd.Omschrijving
                                    ,
                                    FR = bd.Omschrijving
                                };
                                Data.Bestanddelen.Add(bd);
                            }
                            bd.Type = entity.TypeAftrekNID;
                            bd.VorigBj = entity.VorigBj;
                            bd.Evolutie = (bd.HuidigBj.HasValue ? bd.HuidigBj.Value : 0) - bd.VorigBj;
                            bd.WerkelijkeWijziging = entity.Wijziging;
                        }
                        entity.PosNeg = string.Empty;
                        break;
                }
            }
            entity.XbrlValue = entity.PosNeg == "-" ? 0 - Math.Abs(entity.TeWijzigen.GetValueOrDefault()) : Math.Abs(entity.TeWijzigen.GetValueOrDefault());
            return entity;
        }

        public void AddToWijzigingen(EY.com.eBook.RuleEngine.AY2016.RisicoKapitaalHistoriekAppTypes.WijzigingItem entity, ref WorksheetSetData worksheetset)
        {
            entity = Validate(entity, "Wijzigingen", ref worksheetset);
            if (entity.IsValid)
            {
                entity = Calculatewijziging(entity, ref worksheetset);
                entity = PerformWijziging(entity, ref worksheetset, false);
                Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
                Data.Wijzigingen.Add(entity);
            }
        }

        public void DeleteWijzigingen(Guid rowId, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2016.RisicoKapitaalHistoriekAppTypes.WijzigingItem original = Data.Wijzigingen.FirstOrDefault(e => e.Id == rowId);
            if (original != null)
            {
                PerformWijziging(original, ref worksheetset, true);

                
                Data.Wijzigingen.Remove(original);
                Data.LastUpdated = DateTime.Now;
            }
        }

        public void UpdateWijzigingen(EY.com.eBook.RuleEngine.AY2016.RisicoKapitaalHistoriekAppTypes.WijzigingItem entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2016.RisicoKapitaalHistoriekAppTypes.WijzigingItem original = Data.Wijzigingen.FirstOrDefault(e => e.Id == entity.Id);
            if (original == null)
            {
                AddToWijzigingen(entity, ref worksheetset);
            }
            else
            {
                entity = Validate(entity, "Wijzigingen", ref worksheetset);
                if (entity.IsValid)
                {
                    entity = Calculatewijziging(entity, ref worksheetset);
                    PerformWijziging(entity, ref worksheetset, true);
                    entity = PerformWijziging(entity, ref worksheetset, false);
                    int idx = Data.Wijzigingen.IndexOf(original);
                    Data.Wijzigingen.RemoveAt(idx);
                    Data.Wijzigingen.Insert(idx, entity);
                    Data.LastUpdated = DateTime.Now;
                    entity.LastUpdated = DateTime.Now;
                }
            }
        }

        public void UpdateTarief(EY.com.eBook.RuleEngine.AY2016.RisicoKapitaalHistoriekAppTypes.TariefItem entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2016.RisicoKapitaalHistoriekAppTypes.TariefItem original = Data.Tarief;

            entity = Validate(entity, "Tarief", ref worksheetset);
            if (entity.IsValid)
            {

                Data.Tarief = entity;
                Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
            }
        }
        */
        public void AddToHistoriek(EY.com.eBook.RuleEngine.AY2016.RisicoKapitaalHistoriekAppTypes.HistoryItem entity, ref WorksheetSetData worksheetset)
        {
            entity = Validate(entity, "Historiek", ref worksheetset);
            if (entity.IsValid)
            {

                Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
                if (entity.BeperktOverdraagbaarSaldo == null){entity.BeperktOverdraagbaarSaldo = 0;}
                if (entity.BeperktOverdraagbaarVolgendAJ == null) { entity.BeperktOverdraagbaarVolgendAJ = 0; }
                if (entity.OnbeperktOverdraagbaarSaldo == null) { entity.OnbeperktOverdraagbaarSaldo = 0; }
                if (entity.OnbeperktOverdraagbaarVolgendAJ == null) { entity.OnbeperktOverdraagbaarVolgendAJ = 0; }
                entity.BerekendInAJString = entity.BerekendInAJ.Id;
                entity.Overdraagbaar7Jaar = (Convert.ToInt16(entity.BerekendInAJ.Id) + 7).ToString();
                Data.Historiek.Add(entity);
            }
        }

        public void DeleteHistoriek(Guid rowId, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2016.RisicoKapitaalHistoriekAppTypes.HistoryItem original = Data.Historiek.FirstOrDefault(e => e.Id == rowId);
            if (original != null)
            {
                /*
                if (original.Boeken.HasValue && original.Boeken.Value)
                {
                    worksheetset.DeleteBooking(FacturatieApp.TypeId, "OpTeMakenCN", original.Id, null);
                    // DELETE BOOKING
                }*/
                Data.Historiek.Remove(original);
                Data.LastUpdated = DateTime.Now;
            }
        }

        public void UpdateHistoriek(EY.com.eBook.RuleEngine.AY2016.RisicoKapitaalHistoriekAppTypes.HistoryItem entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2016.RisicoKapitaalHistoriekAppTypes.HistoryItem original = Data.Historiek.FirstOrDefault(e => e.Id == entity.Id);
            if (original == null)
            {
                AddToHistoriek(entity, ref worksheetset);
            }
            else
            {
                entity = Validate(entity, "Historiek", ref worksheetset);
                if (entity.IsValid)
                {
                    int idx = Data.Historiek.IndexOf(original);
                    Data.Historiek.RemoveAt(idx);
                    Data.Historiek.Insert(idx, entity);
                    Data.LastUpdated = DateTime.Now;
                    entity.LastUpdated = DateTime.Now;
                    if (entity.BeperktOverdraagbaarSaldo == null) { entity.BeperktOverdraagbaarSaldo = 0; }
                    if (entity.BeperktOverdraagbaarVolgendAJ == null) { entity.BeperktOverdraagbaarVolgendAJ = 0; }
                    if (entity.OnbeperktOverdraagbaarSaldo == null) { entity.OnbeperktOverdraagbaarSaldo = 0; }
                    if (entity.OnbeperktOverdraagbaarVolgendAJ == null) { entity.OnbeperktOverdraagbaarVolgendAJ = 0; }
                    entity.BerekendInAJString = entity.BerekendInAJ.Id;
                    entity.Overdraagbaar7Jaar = (Convert.ToInt16(entity.BerekendInAJ.Id) + 7).ToString();
                }
            }
        }

        public void AddToAanpassingenHistoriek(EY.com.eBook.RuleEngine.AY2016.RisicoKapitaalHistoriekAppTypes.HistoriekAanpassingItem entity, ref WorksheetSetData worksheetset)
        {
            entity = Validate(entity, "AanpassingenHistoriek", ref worksheetset);
            if (entity.IsValid)
            {
                //PerformAdjustHistory(entity, false);
                Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
                if (entity.BedragAftrekBeperkt == null) { entity.BedragAftrekBeperkt = 0; }
                if (entity.BedragAftrekOnbeperkt == null) { entity.BedragAftrekOnbeperkt = 0; }

                EY.com.eBook.RuleEngine.AY2016.RisicoKapitaalHistoriekAppTypes.HistoryItem h = worksheetset.RisicoKapitaalHistoriekApp.Data.Historiek.FirstOrDefault(hi=>hi.BerekendInAJString == entity.AssessmentYear.Id);

                if (h == null)
                { // -> create new history item
                    EY.com.eBook.RuleEngine.AY2016.RisicoKapitaalHistoriekAppTypes.HistoryItem nh = new EY.com.eBook.RuleEngine.AY2016.RisicoKapitaalHistoriekAppTypes.HistoryItem
                    {
                        BerekendInAJ = entity.AssessmentYear,
                        BeperktOverdraagbaarSaldo = entity.BedragAftrekBeperkt,
                        OnbeperktOverdraagbaarSaldo = entity.BedragAftrekOnbeperkt,
                        Id = System.Guid.Empty
                    };
                    AddToHistoriek(nh, ref worksheetset);
                }
                else 
                { // -> update history item
                    h.BeperktOverdraagbaarSaldo += entity.BedragAftrekBeperkt;
                    h.OnbeperktOverdraagbaarSaldo += entity.BedragAftrekOnbeperkt;
                    UpdateHistoriek(h, ref worksheetset);
                }
                /*
                if (entity.BedragAftrekBeperkt != null){
                    h.BeperktOverdraagbaarSaldo+=entity.BedragAftrekBeperkt;
                }
                if (entity.BedragAftrekOnbeperkt!=null){
                    h.OnbeperktOverdraagbaarSaldo = h.OnbeperktOverdraagbaarSaldo == null ? entity.BedragAftrekOnbeperkt : h.OnbeperktOverdraagbaarSaldo + entity.BedragAftrekOnbeperkt;
                    //h.OnbeperktOverdraagbaarSaldo+=entity.BedragAftrekOnbeperkt;
                }
                */


                //worksheetset.RisicoKapitaalHistoriekApp.UpdateHistoriek(h, ref worksheetset);
                Data.AanpassingenHistoriek.Add(entity);
            }
        }

        private void PerformAdjustHistory(RisicoKapitaalHistoriekAppTypes.HistoriekAanpassingItem entity, bool delete)
        {
            /*
             * OLD
            RisicoKapitaalHistoriekAppTypes.HistoryItem item = Data.Historiek.First(h => h.AssessmentYear.Id == entity.AssessmentYear.Id);
            if (item != null)
            {
                item.BedragAftrek = item.BedragAftrekImport;
                item.Saldo = item.BedragAftrek - item.GebruiktHuidigBJ;
            }
            if (!delete)
            {
                if (item == null)
                {
                    item = new EY.com.eBook.RuleEngine.AY2016.RisicoKapitaalHistoriekAppTypes.HistoryItem
                    {
                        Id = Guid.NewGuid()
                        ,
                        AssessmentYear = entity.AssessmentYear
                        ,
                        BedragAftrekImport = 0
                        ,
                        GebruiktHuidigBJ = 0
                        ,
                        LastUpdated = DateTime.Now
                    };
                    Data.Historiek.Add(item);
                }
                item.BedragAftrek = entity.BedragAftrek;
                item.Saldo = item.BedragAftrek - item.GebruiktHuidigBJ;
            }
             */ 
        }

        public void DeleteAanpassingenHistoriek(Guid rowId, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2016.RisicoKapitaalHistoriekAppTypes.HistoriekAanpassingItem original = Data.AanpassingenHistoriek.FirstOrDefault(e => e.Id == rowId);
            if (original != null)
            {
                PerformAdjustHistory(original, true);
                /*
                if (original.Boeken.HasValue && original.Boeken.Value)
                {
                    worksheetset.DeleteBooking(FacturatieApp.TypeId, "OpTeMakenCN", original.Id, null);
                    // DELETE BOOKING
                }*/
                Data.AanpassingenHistoriek.Remove(original);
                Data.LastUpdated = DateTime.Now;
            }
        }

        public void UpdateAanpassingenHistoriek(EY.com.eBook.RuleEngine.AY2016.RisicoKapitaalHistoriekAppTypes.HistoriekAanpassingItem entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2016.RisicoKapitaalHistoriekAppTypes.HistoriekAanpassingItem original = Data.AanpassingenHistoriek.FirstOrDefault(e => e.Id == entity.Id);
            if (original == null)
            {
                AddToAanpassingenHistoriek(entity, ref worksheetset);
            }
            else
            {
                entity = Validate(entity, "AanpassingenHistoriek", ref worksheetset);
                if (entity.IsValid)
                {
                    PerformAdjustHistory(entity, false);
                    int idx = Data.AanpassingenHistoriek.IndexOf(original);
                    Data.AanpassingenHistoriek.RemoveAt(idx);
                    Data.AanpassingenHistoriek.Insert(idx, entity);
                    Data.LastUpdated = DateTime.Now;
                    entity.LastUpdated = DateTime.Now;
                }
            }
        }

        #endregion

        #region VALIDATION

        public EY.com.eBook.RuleEngine.AY2016.RisicoKapitaalHistoriekAppTypes.FicheItem Validate(EY.com.eBook.RuleEngine.AY2016.RisicoKapitaalHistoriekAppTypes.FicheItem entity, string location, ref WorksheetSetData worksheetset)
        {
            entity.IsValid = true;
            worksheetset.ClearMessages(TypeId, entity.Id);
            /*
            if (!entity.Bedrag.HasValue || entity.Bedrag.Value==0)
            {
            worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Grootboek");
            entity.IsValid = false;
            }
            */

            return entity;
        }
        /*
        public EY.com.eBook.RuleEngine.AY2016.RisicoKapitaalHistoriekAppTypes.EigenVermogen Validate(EY.com.eBook.RuleEngine.AY2016.RisicoKapitaalHistoriekAppTypes.EigenVermogen entity, string location, ref WorksheetSetData worksheetset)
        {
            entity.IsValid = true;
            worksheetset.ClearMessages(TypeId, entity.Id);


            if (!entity.HuidigBjImported.HasValue)
            {
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "HuidigBjImported");
                entity.IsValid = false;
            }
            if (!entity.VorigBjImported.HasValue)
            {
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "VorigBjImported");
                entity.IsValid = false;
            }
            if (entity.Type == null)
            {
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Type");
                entity.IsValid = false;
            }
            if (entity.IsValid)
            {
                entity.HuidigBj = entity.HuidigBjImported;
                entity.VorigBj = entity.VorigBjImported;
                entity.Evolutie = entity.HuidigBj - entity.VorigBj;
            }


            return entity;
        }

        public EY.com.eBook.RuleEngine.AY2016.RisicoKapitaalHistoriekAppTypes.Bestanddeel Validate(EY.com.eBook.RuleEngine.AY2016.RisicoKapitaalHistoriekAppTypes.Bestanddeel entity, string location, ref WorksheetSetData worksheetset)
        {
            entity.IsValid = true;
            worksheetset.ClearMessages(TypeId, entity.Id);

            if (!entity.HuidigBj.HasValue && !entity.HuidigBjImported.HasValue)
            {
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "HuidigBjImported");
                entity.IsValid = false;
            }
            if (!entity.VorigBj.HasValue && !entity.VorigBjImported.HasValue)
            {
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "VorigBjImported");
                entity.IsValid = false;
            }
            if (entity.Type == null)
            {
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Type");
                entity.IsValid = false;
            }

            if (entity.IsValid)
            {
                entity.HuidigBj = entity.HuidigBjImported;
                entity.VorigBj = entity.VorigBjImported;
                entity.Evolutie = entity.HuidigBj - entity.VorigBj;
            }

            return entity;
        }

        public EY.com.eBook.RuleEngine.AY2016.RisicoKapitaalHistoriekAppTypes.WijzigingItem Validate(EY.com.eBook.RuleEngine.AY2016.RisicoKapitaalHistoriekAppTypes.WijzigingItem entity, string location, ref WorksheetSetData worksheetset)
        {
            entity.IsValid = true;
            worksheetset.ClearMessages(TypeId, entity.Id);

            if (entity.Bestaand.HasValue && entity.Bestaand.Value)
            {
                if (entity.BestaandTeWijzigen == null)
                {
                    worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "BestaandTeWijzigen");
                    entity.IsValid = false;
                }
            }
            else
            {
                if (entity.Grootboek == null)
                {
                    worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Grootboek");
                    entity.IsValid = false;
                }
            }

            if (entity.Wijzigtype == null)
            {
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Wijzigtype");
                entity.IsValid = false;
            }
            else
            {
                switch (entity.Wijzigtype.Id.ToString().ToLower())
                {
                    case "310ac564-d0c0-4909-ab68-526e47d7e7bd": //1 Wijziging in de loop van het belastbare tijdperk
                        if (entity.AfOfToename == null)
                        {
                            worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "AfOfToename");
                            entity.IsValid = false;
                        }
                        if ((!entity.Bestaand.HasValue || entity.Bestaand == false))
                        {
                            if (entity.TypeAftrekNID == null && entity.TypeNID == null)
                            {
                                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "ONEOREMPTY");
                                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "ONEOREMPTY");
                                entity.IsValid = false;
                            }
                            if (entity.TypeAftrekNID != null && entity.TypeNID != null)
                            {
                                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "ONEOREMPTY");
                                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "ONEOREMPTY");
                                entity.IsValid = false;
                            }
                        }
                        if (!entity.Wijziging.HasValue || entity.Wijziging == 0)
                        {
                            worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Wijziging");
                            entity.IsValid = false;
                        }
                        if (!entity.DatumTransactie.HasValue)
                        {
                            worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "DatumTransactie");
                            entity.IsValid = false;
                        }
                        else
                        {
                            if (entity.DatumTransactie.Value > worksheetset.DataContract.EndDate)
                            {
                                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "GREATERTHENENDDATE", location, entity.Id, "DatumTransactie");
                                entity.IsValid = false;
                            }
                        }

                        break;
                    case "7edb61e5-fa4f-4115-887b-d644c3b36a6a": //2 Aanpassing van de begintoestand
                        if (!entity.VorigBj.HasValue)
                        {
                            worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "VorigBj");
                            entity.IsValid = false;
                        }
                        break;
                }
            }
            if (entity.AardToename == null && entity.Bestaand == true)
            {
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "AardToename");
                entity.IsValid = false;
            }
            if (entity.Notitie == null)
            {
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Notitie");
                entity.IsValid = false;
            }

            return entity;
        }

        public EY.com.eBook.RuleEngine.AY2016.RisicoKapitaalHistoriekAppTypes.TariefItem Validate(EY.com.eBook.RuleEngine.AY2016.RisicoKapitaalHistoriekAppTypes.TariefItem entity, string location, ref WorksheetSetData worksheetset)
        {
            entity.IsValid = true;
            worksheetset.ClearMessages(TypeId, entity.Id);
            

            return entity;
        }
        */
        public EY.com.eBook.RuleEngine.AY2016.RisicoKapitaalHistoriekAppTypes.HistoryItem Validate(EY.com.eBook.RuleEngine.AY2016.RisicoKapitaalHistoriekAppTypes.HistoryItem entity, string location, ref WorksheetSetData worksheetset)
        {
            entity.IsValid = true;
            worksheetset.ClearMessages(TypeId, entity.Id);

            if (entity.BerekendInAJ == null)
            {
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "BerekendInAJ");
                entity.IsValid = false;
            }


            if ((!entity.BeperktOverdraagbaarSaldo.HasValue || entity.BeperktOverdraagbaarSaldo.Value == 0) && (!entity.OnbeperktOverdraagbaarSaldo.HasValue || entity.OnbeperktOverdraagbaarSaldo.Value == 0) && (!entity.VervallenHuidigAJ.HasValue || entity.VervallenHuidigAJ.Value == 0))
            {
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "BeperktOverdraagbaarSaldo");
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "OnbeperktOverdraagbaarSaldo");
                entity.IsValid = false;
            }

            if ((entity.OorspronkelijkSaldoBeperkt.HasValue && entity.OorspronkelijkSaldoBeperkt > 0) && (entity.OorspronkelijkSaldoOnbeperkt.HasValue && entity.OorspronkelijkSaldoOnbeperkt > 0)) {
                if (entity.OorspronkelijkSaldoOnbeperkt != entity.OnbeperktOverdraagbaarSaldo && entity.OorspronkelijkSaldoBeperkt != entity.BeperktOverdraagbaarSaldo)
                {
                    worksheetset.SetMessage(TypeId, MessageTypeDataContract.Information, "NOTEQUAL", location, entity.Id, "BeperktOverdraagbaarSaldo", new object[] {entity.BeperktOverdraagbaarSaldo, entity.OorspronkelijkSaldoBeperkt});
                    worksheetset.SetMessage(TypeId, MessageTypeDataContract.Information, "NOTEQUAL", location, entity.Id, "OnbeperktOverdraagbaarSaldo", new object[] { entity.BeperktOverdraagbaarSaldo, entity.OorspronkelijkSaldoBeperkt });
                }   
            }
            else if (entity.OorspronkelijkSaldoOnbeperkt.HasValue && entity.OorspronkelijkSaldoOnbeperkt > 0)
            {
                if (entity.OorspronkelijkSaldoOnbeperkt != entity.OnbeperktOverdraagbaarSaldo) {
                    worksheetset.SetMessage(TypeId, MessageTypeDataContract.Information, "NOTEQUAL", location, entity.Id, "OnbeperktOverdraagbaarSaldo", new object[] { entity.BeperktOverdraagbaarSaldo, entity.OorspronkelijkSaldoBeperkt });
                }                
            }
            else if (entity.OorspronkelijkSaldoBeperkt.HasValue && entity.OorspronkelijkSaldoBeperkt > 0)
            {
                if (entity.OorspronkelijkSaldoBeperkt != entity.BeperktOverdraagbaarSaldo)
                {
                    worksheetset.SetMessage(TypeId, MessageTypeDataContract.Information, "NOTEQUAL", location, entity.Id, "BeperktOverdraagbaarSaldo", new object[] { entity.BeperktOverdraagbaarSaldo, entity.OorspronkelijkSaldoBeperkt });
                }   
            }
            /*
             * OLD
            if (entity.AssessmentYear == null)
            {
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "AssessmentYear");
                entity.IsValid = false;
            }


            if (!entity.BedragAftrekImport.HasValue && !entity.BedragAftrek.HasValue)
            {
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "BedragAftrekImport");
                entity.IsValid = false;
            }
            else
            {
                entity.BedragAftrek = entity.BedragAftrekImport.GetValueOrDefault();
            }

            // if (!entity.BedragAftrek.HasValue) entity.BedragAftrek = entity.BedragAftrekImport.GetValueOrDefault();
            */
            return entity;
        }

        public EY.com.eBook.RuleEngine.AY2016.RisicoKapitaalHistoriekAppTypes.HistoriekAanpassingItem Validate(EY.com.eBook.RuleEngine.AY2016.RisicoKapitaalHistoriekAppTypes.HistoriekAanpassingItem entity, string location, ref WorksheetSetData worksheetset)
        {
            entity.IsValid = true;
            worksheetset.ClearMessages(TypeId, entity.Id);

            if (entity.AssessmentYear == null)
            {
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "AssessmentYear");
                entity.IsValid = false;
            }


            if ((!entity.BedragAftrekBeperkt.HasValue || entity.BedragAftrekBeperkt.Value == 0) && (!entity.BedragAftrekOnbeperkt.HasValue || entity.BedragAftrekOnbeperkt.Value == 0))
            {
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "BedragAftrekBeperkt");
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "BedragAftrekOnbeperkt");
                entity.IsValid = false;
            }

            if (entity.Notitie == null)
            {
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Notitie");
                entity.IsValid = false;
            }
            

            return entity;
        }

        #endregion
        // VALIDATION

        public void ImportPrevious(ref WorksheetSetData wsd)
        {
            if (wsd.DataContract.PreviousFileId.HasValue)
            {
                if (wsd.PreviousSet.AssessmentYear == 2015)
                {
                    EY.com.eBook.RuleEngine.AY2015.WorksheetSetData pset = (EY.com.eBook.RuleEngine.AY2015.WorksheetSetData)wsd.PreviousSet;


                    EY.com.eBook.RuleEngine.AY2015.RisicoKapitaalHistoriekApp previous = pset.RisicoKapitaalHistoriekApp;
                    Data.Historiek.RemoveAll(h => h.PreviousImported == true);



                    foreach (AY2015.RisicoKapitaalHistoriekAppTypes.HistoryItem hist in previous.Data.Historiek.Where(h => (h.BeperktOverdraagbaarVolgendAJ.HasValue && h.BeperktOverdraagbaarVolgendAJ.Value > 0) || (h.OnbeperktOverdraagbaarVolgendAJ.HasValue && h.OnbeperktOverdraagbaarVolgendAJ.Value > 0)))
                    {
                        Data.Historiek.Add(new EY.com.eBook.RuleEngine.AY2016.RisicoKapitaalHistoriekAppTypes.HistoryItem
                        {
                            PreviousImported = true
                            ,
                            OnbeperktOverdraagbaarSaldo = 0
                            ,
                            BeperktOverdraagbaarSaldo = hist.BeperktOverdraagbaarVolgendAJ
                            ,
                            BerekendInAJ = hist.BerekendInAJ
                            ,
                            BerekendInAJString = hist.BerekendInAJString
                            ,
                            Overdraagbaar7Jaar = hist.Overdraagbaar7Jaar
                            ,
                            GebruiktHuidigAJ = 0
                            ,
                            BeperktOverdraagbaarVolgendAJ = 0
                            ,
                            OnbeperktOverdraagbaarVolgendAJ = 0
                            ,
                            OorspronkelijkSaldoBeperkt = hist.OorspronkelijkSaldoBeperkt
                            ,
                            OorspronkelijkSaldoOnbeperkt = hist.OorspronkelijkSaldoOnbeperkt// TODO: opvangen voor AY2016,
                            ,
                            Id = hist.Id
                            ,
                            LastUpdated = hist.LastUpdated
                        });

                    }

                    Calculate(ref wsd); // ?
                }

                if (wsd.PreviousSet.AssessmentYear == 2015)
                {
                    EY.com.eBook.RuleEngine.AY2015.WorksheetSetData pset = (EY.com.eBook.RuleEngine.AY2015.WorksheetSetData)wsd.PreviousSet;


                    EY.com.eBook.RuleEngine.AY2015.RisicoKapitaalHistoriekApp previous = pset.RisicoKapitaalHistoriekApp;
                    Data.Historiek.RemoveAll(h => h.PreviousImported == true);



                    foreach (AY2015.RisicoKapitaalHistoriekAppTypes.HistoryItem hist in previous.Data.Historiek.Where(h => (h.BeperktOverdraagbaarVolgendAJ.HasValue && h.BeperktOverdraagbaarVolgendAJ.Value > 0) || (h.OnbeperktOverdraagbaarVolgendAJ.HasValue && h.OnbeperktOverdraagbaarVolgendAJ.Value > 0)))
                    {
                        Data.Historiek.Add(new EY.com.eBook.RuleEngine.AY2016.RisicoKapitaalHistoriekAppTypes.HistoryItem
                        {
                            PreviousImported = true
                            ,
                            OnbeperktOverdraagbaarSaldo = 0
                            ,
                            BeperktOverdraagbaarSaldo = hist.BeperktOverdraagbaarVolgendAJ
                            ,
                            BerekendInAJ = hist.BerekendInAJ
                            ,
                            BerekendInAJString = hist.BerekendInAJString
                            ,
                            Overdraagbaar7Jaar = hist.Overdraagbaar7Jaar
                            ,
                            GebruiktHuidigAJ = 0
                            ,
                            BeperktOverdraagbaarVolgendAJ = 0
                            ,
                            OnbeperktOverdraagbaarVolgendAJ = 0
                            ,
                            OorspronkelijkSaldoBeperkt = hist.OorspronkelijkSaldoBeperkt
                            ,
                            OorspronkelijkSaldoOnbeperkt = hist.OorspronkelijkSaldoOnbeperkt// TODO: opvangen voor AY2016,
                            ,
                            Id = hist.Id
                            ,
                            LastUpdated = hist.LastUpdated
                        });

                    }

                    Calculate(ref wsd); // ?
                }
            }
        }

        // CALCULATION

        // BOOKINGS

        // IMPORT PREVIOUS


    }
}
