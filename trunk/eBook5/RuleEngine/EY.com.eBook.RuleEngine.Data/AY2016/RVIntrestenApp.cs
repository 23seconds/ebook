
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.RuleEngine.Core;
using EY.com.eBook.RuleEngine.Core.Interfaces;
using EY.com.eBook.RuleEngine.Data;
using EY.com.eBook.Core;
using EY.com.eBook.API.Contracts.Data.Cache;

namespace EY.com.eBook.RuleEngine.AY2016
{
    
    public class RVIntrestenApp
    {
        public static WorksheetTypeSettings Settings = new WorksheetTypeSettings
                                                      {
                                                        Clients = true
                                                        ,
                                                        Suppliers = true
                                                        ,
                                                        AllAccounts = true
                                                        , 
                                                        PerformsBookings=true
                                                      };
                                                      
        public static Guid TypeId = new Guid("3b260f00-e6e8-4c6b-83ea-69457799a068");

        public List<string> ImportablePreviousTypes  = new List<string>() {  "EY.com.eBook.RuleEngine.AY2015.FacturatieApp"  };

        public Guid GetMyType() { return TypeId; }

        public void SetMinimal()
        {
            if (Data == null)
            {
                Data = new EY.com.eBook.RuleEngine.AY2016.RVIntrestenAppTypes.WPRVIntresten();
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.Intresten == null)
            {
                Data.Intresten = new List<EY.com.eBook.RuleEngine.AY2016.RVIntrestenAppTypes.IntrestItem>(); ;
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.Adjustments == null)
            {
                Data.Adjustments = new List<EY.com.eBook.RuleEngine.AY2016.RVIntrestenAppTypes.Adjustment>(); ;
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.Verschillen == null)
            {
                Data.Verschillen = new List<EY.com.eBook.RuleEngine.AY2016.RVIntrestenAppTypes.VerchilItem>(); ;
                Data.LastUpdated = DateTime.Now;
            }

        }
        
        public string ToXml() 
        {
            return CoreHelper.SerializeToString(Data);
        }

        public void LoadXml(string xml)
        {
            if (!string.IsNullOrEmpty(xml)) {
                Data = CoreHelper.DeserializeFromString<EY.com.eBook.RuleEngine.AY2016.RVIntrestenAppTypes.WPRVIntresten>(xml);
            }
            SetMinimal();
            
        }
        
        public EY.com.eBook.RuleEngine.AY2016.RVIntrestenAppTypes.WPRVIntresten Data { get; set; }
        
        

        #region worksheet type validation

        public EY.com.eBook.RuleEngine.AY2016.RVIntrestenAppTypes.IntrestItem Validate(EY.com.eBook.RuleEngine.AY2016.RVIntrestenAppTypes.IntrestItem entity, string location, ref WorksheetSetData worksheetset)
        {
            worksheetset.ClearMessages(TypeId, entity.Id);
            entity.IsValid = true;
           
            
            entity.Percent0 = 0;
            entity.Percent15 = Math.Round(entity.Saldo15Percent.GetValueOrDefault() * (decimal)0.15, 2, MidpointRounding.AwayFromZero);
            entity.Percent21 = Math.Round(entity.Saldo21Percent.GetValueOrDefault() * (decimal)0.21, 2, MidpointRounding.AwayFromZero);
            entity.Percent25 = Math.Round(entity.Saldo25Percent.GetValueOrDefault() * (decimal)0.25, 2, MidpointRounding.AwayFromZero);

            decimal ttl = entity.Saldo0Percent.GetValueOrDefault() + entity.Saldo15Percent.GetValueOrDefault() + entity.Saldo21Percent.GetValueOrDefault() + entity.Saldo25Percent.GetValueOrDefault();
            entity.ResterendSaldo = Math.Abs(entity.Saldo.GetValueOrDefault()) - Math.Abs(ttl);

            if (entity.ResterendSaldo!=0)
            {

                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "PERCENTAGESTOLOW", location, entity.Id, "Saldo0Percent");
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "PERCENTAGESTOLOW", location, entity.Id, "Saldo15Percent");
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "PERCENTAGESTOLOW", location, entity.Id, "Saldo21Percent");
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "PERCENTAGESTOLOW", location, entity.Id, "Saldo25Percent");
                entity.IsValid = false;
            }

            

            

            return entity;
        }

        public EY.com.eBook.RuleEngine.AY2016.RVIntrestenAppTypes.VerchilItem Validate(EY.com.eBook.RuleEngine.AY2016.RVIntrestenAppTypes.VerchilItem entity, string location, ref WorksheetSetData worksheetset)
        {
            worksheetset.ClearMessages(TypeId, entity.Id);
            entity.IsValid = true;

            if (!entity.Bedrag.HasValue)
            {
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Bedrag");
                entity.IsValid = false;
            }

            if (string.IsNullOrEmpty(entity.Omschrijving))
            {
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Omschrijving");
                entity.IsValid = false;
            }

            /*
            if (!entity.Bedrag.HasValue || entity.Bedrag.Value==0)
            {
                //add message collection, field:"Grootboek", required
                invalids.Add(new InvalidField
                {
                    WorksheetLocation = location
                    ,
                    FieldName = "Bedrag"
                    ,
                    MessageCode = "REQUIRED"
                });
                entity.IsValid = false;
            }
            */
            entity.Order = 10;
            return entity;
        }

        public EY.com.eBook.RuleEngine.AY2016.RVIntrestenAppTypes.Adjustment Validate(EY.com.eBook.RuleEngine.AY2016.RVIntrestenAppTypes.Adjustment entity, string location, ref WorksheetSetData worksheetset)
        {
            worksheetset.ClearMessages(TypeId, entity.Id);
            entity.IsValid = true;
           
             if (!entity.HerzienSaldo.HasValue)
            {
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "HerzienSaldo");
                entity.IsValid = false;
            }
             if (entity.Rekening==null)
             {
                 worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Rekening");
                 entity.IsValid = false;
             }
            

            /*
            if (!entity.Bedrag.HasValue || entity.Bedrag.Value==0)
            {
                //add message collection, field:"Grootboek", required
                invalids.Add(new InvalidField
                {
                    WorksheetLocation = location
                    ,
                    FieldName = "Bedrag"
                    ,
                    MessageCode = "REQUIRED"
                });
                entity.IsValid = false;
            }
            */

            return entity;
        }


        #endregion

        #region AddUpdateDelete

        public void AddToIntresten(EY.com.eBook.RuleEngine.AY2016.RVIntrestenAppTypes.IntrestItem entity, ref WorksheetSetData worksheetset)
        {
            entity = Validate(entity, "Intresten", ref worksheetset);
            if (entity.IsValid)
            {
                Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
                Data.Intresten.Add(entity);
            }
        }

        public void UpdateIntresten(EY.com.eBook.RuleEngine.AY2016.RVIntrestenAppTypes.IntrestItem entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2016.RVIntrestenAppTypes.IntrestItem original = Data.Intresten.FirstOrDefault(e => e.Id == entity.Id);
            if (original == null)
            {
                AddToIntresten(entity, ref worksheetset);
            }
            else
            {
                entity = Validate(entity, "Intresten", ref worksheetset);
                if (entity.IsValid)
                {
                    int idx = Data.Intresten.IndexOf(original);
                    Data.Intresten.RemoveAt(idx);
                    Data.Intresten.Insert(idx, entity);
                    Data.LastUpdated = DateTime.Now;
                    entity.LastUpdated = DateTime.Now;
                }
            }
        }

        public void AddToAdjustments(EY.com.eBook.RuleEngine.AY2016.RVIntrestenAppTypes.Adjustment entity, ref WorksheetSetData worksheetset)
        {
            entity = Validate(entity, "Adjustments", ref worksheetset);
            if (entity.IsValid)
            {
                Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
                Data.Adjustments.Add(entity);
            }
        }

        public void DeleteAdjustments(Guid rowId, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2016.RVIntrestenAppTypes.Adjustment original = Data.Adjustments.FirstOrDefault(e => e.Id == rowId);
            if (original != null)
            {
                /*
                if (original.Boeken.HasValue && original.Boeken.Value)
                {
                    worksheetset.DeleteBooking(FacturatieApp.TypeId, "OpTeMakenCN", original.Id, null);
                    // DELETE BOOKING
                }*/
                Data.Adjustments.Remove(original);
                Data.LastUpdated = DateTime.Now;
            }
        }

        public void UpdateAdjustments(EY.com.eBook.RuleEngine.AY2016.RVIntrestenAppTypes.Adjustment entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2016.RVIntrestenAppTypes.Adjustment original = Data.Adjustments.FirstOrDefault(e => e.Id == entity.Id);
            if (original == null)
            {
                AddToAdjustments(entity, ref worksheetset);
            }
            else
            {
                entity = Validate(entity, "Adjustments", ref worksheetset);
                if (entity.IsValid)
                {
                    int idx = Data.Adjustments.IndexOf(original);
                    Data.Adjustments.RemoveAt(idx);
                    Data.Adjustments.Insert(idx, entity);
                    Data.LastUpdated = DateTime.Now;
                    entity.LastUpdated = DateTime.Now;
                }
            }
        }

        public void AddToVerschillen(EY.com.eBook.RuleEngine.AY2016.RVIntrestenAppTypes.VerchilItem entity, ref WorksheetSetData worksheetset)
        {
            entity = Validate(entity, "Verschillen", ref worksheetset);
            if (entity.IsValid)
            {
                Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
                Data.Verschillen.Add(entity);
            }
        }

        public void DeleteVerschillen(Guid rowId, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2016.RVIntrestenAppTypes.VerchilItem original = Data.Verschillen.FirstOrDefault(e => e.Id == rowId);
            if (original != null)
            {
                /*
                if (original.Boeken.HasValue && original.Boeken.Value)
                {
                    worksheetset.DeleteBooking(FacturatieApp.TypeId, "OpTeMakenCN", original.Id, null);
                    // DELETE BOOKING
                }*/
                Data.Verschillen.Remove(original);
                Data.LastUpdated = DateTime.Now;
            }
        }

        public void UpdateVerschillen(EY.com.eBook.RuleEngine.AY2016.RVIntrestenAppTypes.VerchilItem entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2016.RVIntrestenAppTypes.VerchilItem original = Data.Verschillen.FirstOrDefault(e => e.Id == entity.Id);
            if (original == null)
            {
                AddToVerschillen(entity, ref worksheetset);
            }
            else
            {
                entity = Validate(entity, "Verschillen", ref worksheetset);
                if (entity.IsValid)
                {
                    
                    int idx = Data.Verschillen.IndexOf(original);
                    Data.Verschillen.RemoveAt(idx);
                    Data.Verschillen.Insert(idx, entity);
                    Data.LastUpdated = DateTime.Now;
                    entity.LastUpdated = DateTime.Now;
                }
            }
        }

        #endregion

        public void ForceRecalculation(ref WorksheetSetData worksheetset)
        {
            Data.LastUpdated = new DateTime(1980, 1, 1);
            Data.Verschillen.RemoveAll(e => e.AutoLine.HasValue && e.AutoLine.Value);
            Calculate(ref worksheetset);
        }

        #region worksheet type rules
        // Only if worksheet does any calculation.
        public void Calculate(ref WorksheetSetData wsd)
        {
            wsd.ClearMessages(TypeId);
           
            ImportData(ref wsd);
            foreach (EY.com.eBook.RuleEngine.AY2016.RVIntrestenAppTypes.IntrestItem item in Data.Intresten)
            {
                item.Saldo = item.SaldoAccount;
                RVIntrestenAppTypes.Adjustment adj = Data.Adjustments.FirstOrDefault(a => a.Rekening!=null && a.Rekening.Id == item.Rekening);
                if (adj != null)
                {
                    item.Saldo = adj.HerzienSaldo;
                }
                Validate(item, "Intresten", ref wsd);
            }


            CreateDifference(ref wsd);
            Data.LastUpdated = DateTime.Now;

        }

        public void ImportData(ref WorksheetSetData wsd)
        {
            FileDataContract fdc = wsd.DataContract;
            List<AccountDataContract> accounts = wsd.GetAccountsOfMappingMeta("intrest", "RVIntresten", false, false, true, false, false, fdc.Culture);
            List<string> nrs = accounts.Select(a => a.InternalNr).ToList();

            List<string> existing = Data.Intresten.Select(a => a.Rekening).ToList();

            // delete removed mappings
            Data.Intresten.RemoveAll(a => !nrs.Contains(a.Rekening));

            // update
            foreach (EY.com.eBook.RuleEngine.AY2016.RVIntrestenAppTypes.IntrestItem item in Data.Intresten.Where(a => nrs.Contains(a.Rekening)))
            {
                AccountDataContract adc = accounts.First(a => a.InternalNr == item.Rekening);
                if (item.SaldoAccount == item.Saldo)
                {
                    item.Saldo = Math.Abs(adc.Saldo); // negate (credit)
                }
                item.SaldoAccount = Math.Abs(adc.Saldo); // negate (credit)
                item.RekeningOms = adc.DefaultDescription;
                item.LastUpdated = DateTime.Now;
                Validate(item, "Intresten", ref wsd);
            }

            //insert 
            foreach (AccountDataContract adc in accounts.Where(a => !existing.Contains(a.InternalNr)))
            {
                EY.com.eBook.RuleEngine.AY2016.RVIntrestenAppTypes.IntrestItem item = new EY.com.eBook.RuleEngine.AY2016.RVIntrestenAppTypes.IntrestItem
                {
                    Id = Guid.NewGuid()
                    ,
                    Percent0 = 0
                    ,
                    Percent15 = 0
                    ,
                    Percent25 = 0
                    ,
                    Saldo = Math.Abs(adc.Saldo) // negate (credit)
                    ,
                    LastUpdated = DateTime.Now
                    ,
                    Rekening = adc.InternalNr
                    ,
                    RekeningOms = adc.DefaultDescription
                    ,
                    ResterendSaldo = Math.Abs(adc.Saldo) // negate (credit)
                    ,
                    Saldo0Percent = 0
                    ,
                    Saldo15Percent = 0
                    ,
                    Saldo25Percent = 0
                    ,
                    SaldoAccount = Math.Abs(adc.Saldo) // negate (credit)
                    ,
                    IsValid = true
                };
                Data.Intresten.Add(item);

            }

            Data.Adjustments.RemoveAll(r => !existing.Contains(r.Rekening.Id));

            Data.Adjustments.ForEach(r =>
            {
                EY.com.eBook.RuleEngine.AY2016.RVIntrestenAppTypes.IntrestItem ii = Data.Intresten.FirstOrDefault(a => a.Rekening == r.Rekening.Id);
                if (ii != null)
                {
                    ii.Saldo = r.HerzienSaldo;
                    ii.LastUpdated = DateTime.Now;
                }
            });


        }

        public void CreateDifference(ref WorksheetSetData wsd)
        {
            //TRANSLATIONS

            string toexplain = wsd.GetTranslation(TypeId, "Verschillen", "ToExplain");
            string difference = wsd.GetTranslation(TypeId, "Verschillen", "Difference");



            decimal percent0 = Data.Intresten.Where(r => r.Percent0.HasValue).Count() > 0 ? Data.Intresten.Sum(r => r.Percent0.GetValueOrDefault()) : 0;
            decimal percent15 = Data.Intresten.Where(r => r.Percent15.HasValue).Count() > 0 ? Data.Intresten.Sum(r => r.Percent15.GetValueOrDefault()) : 0;
            decimal percent21 = Data.Intresten.Where(r => r.Percent21.HasValue).Count() > 0 ? Data.Intresten.Sum(r => r.Percent21.GetValueOrDefault()) : 0;
            decimal percent25 = Data.Intresten.Where(r => r.Percent25.HasValue).Count() > 0 ? Data.Intresten.Sum(r => r.Percent25.GetValueOrDefault()) : 0;

            decimal diff = percent0 + percent15 + percent21 + percent25;

            EY.com.eBook.RuleEngine.AY2016.RVIntrestenAppTypes.VerchilItem vi0 = Data.Verschillen.FirstOrDefault(v => v.Type == "INTREST" && v.Omschrijving == "0%");
            EY.com.eBook.RuleEngine.AY2016.RVIntrestenAppTypes.VerchilItem vi15 = Data.Verschillen.FirstOrDefault(v => v.Type == "INTREST" && v.Omschrijving == "15%");
            EY.com.eBook.RuleEngine.AY2016.RVIntrestenAppTypes.VerchilItem vi21 = Data.Verschillen.FirstOrDefault(v => v.Type == "INTREST" && v.Omschrijving == "21%");
            EY.com.eBook.RuleEngine.AY2016.RVIntrestenAppTypes.VerchilItem vi25 = Data.Verschillen.FirstOrDefault(v => v.Type == "INTREST" && v.Omschrijving == "25%");

            if (vi0 == null)
            {
                vi0 = new EY.com.eBook.RuleEngine.AY2016.RVIntrestenAppTypes.VerchilItem
                {
                    Id = Guid.NewGuid()
                    ,
                    AutoLine = true
                    ,
                    Bedrag = 0
                    ,
                    LastUpdated = DateTime.Now
                    ,
                    Omschrijving = "0%"
                    ,
                    Order = 0
                    ,
                    Type = "INTREST"
                };
                Data.Verschillen.Add(vi0);
            }
            vi0.Bedrag = percent0;

            if (vi15 == null)
            {
                vi15 = new EY.com.eBook.RuleEngine.AY2016.RVIntrestenAppTypes.VerchilItem
                {
                    Id = Guid.NewGuid()
                    ,
                    AutoLine = true
                    ,
                    Bedrag = 0
                    ,
                    LastUpdated = DateTime.Now
                    ,
                    Omschrijving = "15%"
                    ,
                    Order = 1
                    ,
                    Type = "INTREST"
                };
                Data.Verschillen.Add(vi15);
            }
            vi15.Bedrag = percent15;

            if (vi21 == null)
            {
                vi21 = new EY.com.eBook.RuleEngine.AY2016.RVIntrestenAppTypes.VerchilItem
                {
                    Id = Guid.NewGuid()
                    ,
                    AutoLine = true
                    ,
                    Bedrag = 0
                    ,
                    LastUpdated = DateTime.Now
                    ,
                    Omschrijving = "21%"
                    ,
                    Order = 1
                    ,
                    Type = "INTREST"
                };
                Data.Verschillen.Add(vi21);
            }
            vi21.Bedrag = percent21;

            if (vi25 == null)
            {
                vi25 = new EY.com.eBook.RuleEngine.AY2016.RVIntrestenAppTypes.VerchilItem
                {
                    Id = Guid.NewGuid()
                    ,
                    AutoLine = true
                    ,
                    Bedrag = 0
                    ,
                    LastUpdated = DateTime.Now
                    ,
                    Omschrijving = "25%"
                    ,
                    Order = 2
                    ,
                    Type = "INTREST"
                };
                Data.Verschillen.Add(vi25);
            }
            vi25.Bedrag = percent25;

            FileDataContract fdc = wsd.DataContract;
            List<AccountDataContract> accounts = wsd.GetAccountsOfMappingMeta("rv", "RVIntresten", false, false, true, false, false, fdc.Culture);

            List<string> nrs = accounts.Select(a => a.InternalNr).ToList();
            List<string> existing = Data.Verschillen.Where(v => !string.IsNullOrEmpty(v.AutoKey) && v.AutoLine == true).Select(v => v.AutoKey).ToList();

            Data.Verschillen.RemoveAll(v => !string.IsNullOrEmpty(v.AutoKey) && v.Type == "RV" && !nrs.Contains(v.AutoKey));

            // updates 
            foreach (EY.com.eBook.RuleEngine.AY2016.RVIntrestenAppTypes.VerchilItem vi in Data.Verschillen.Where(a => a.Type == "RV" && nrs.Contains(a.AutoKey)))
            {
                AccountDataContract adc = accounts.First(a => a.InternalNr == vi.AutoKey);
                vi.Omschrijving = adc.DefaultDescription;
                vi.Bedrag = Math.Abs(adc.Saldo);
                vi.LastUpdated = DateTime.Now;
            }

            //inserts
            foreach (AccountDataContract adc in accounts.Where(a => !existing.Contains(a.InternalNr)))
            {
                EY.com.eBook.RuleEngine.AY2016.RVIntrestenAppTypes.VerchilItem vi = new EY.com.eBook.RuleEngine.AY2016.RVIntrestenAppTypes.VerchilItem
                {
                    Id = Guid.NewGuid()
                    ,
                    AutoKey = adc.InternalNr
                    ,
                    AutoLine = true
                    ,
                    LastUpdated = DateTime.Now
                    ,
                    Bedrag = Math.Abs(adc.Saldo)
                    ,
                    Omschrijving = adc.DefaultDescription
                    ,
                    Order = 3
                    ,
                    Type = "RV"

                };
                Data.Verschillen.Add(vi);
            }

            diff = diff - Data.Verschillen.Where(a => !string.IsNullOrEmpty(a.AutoKey) && a.Type == "RV").Sum(v => v.Bedrag.Value);

            EY.com.eBook.RuleEngine.AY2016.RVIntrestenAppTypes.VerchilItem viExpl = Data.Verschillen.FirstOrDefault(v => v.Type == "TOEXPLAIN");
            EY.com.eBook.RuleEngine.AY2016.RVIntrestenAppTypes.VerchilItem viDiff = Data.Verschillen.FirstOrDefault(v => v.Type == "DIFFERENCE");


            if (viExpl == null)
            {
                viExpl = new EY.com.eBook.RuleEngine.AY2016.RVIntrestenAppTypes.VerchilItem
                {
                    Id = Guid.NewGuid()
                    ,
                    AutoKey = null
                    ,
                    AutoLine = true
                    ,
                    Type = "TOEXPLAIN"
                    ,
                    LastUpdated = DateTime.Now
                    ,
                    Omschrijving = toexplain
                    ,
                    Order = 4
                    ,
                    Bedrag = 0
                };
                Data.Verschillen.Add(viExpl);
            }
            viExpl.Bedrag = diff;
            viExpl.LastUpdated = DateTime.Now;


            if (viDiff == null)
            {
                viDiff = new EY.com.eBook.RuleEngine.AY2016.RVIntrestenAppTypes.VerchilItem
                {
                    Id = Guid.NewGuid()
                    ,
                    AutoKey = null
                    ,
                    AutoLine = true
                    ,
                    Type = "DIFFERENCE"
                    ,
                    LastUpdated = DateTime.Now
                    ,
                    Omschrijving = difference
                    ,
                    Order = 10001
                    ,
                    Bedrag = 0
                };
                Data.Verschillen.Add(viDiff);
            }
            viDiff.Bedrag = diff + Data.Verschillen.Where(v => !v.AutoLine.HasValue || v.AutoLine == false).Sum(v => v.Bedrag.GetValueOrDefault());
            viDiff.LastUpdated = DateTime.Now;


            Data.Verschillen = Data.Verschillen.OrderBy(v => v.Order).ToList();


        }

        #endregion
        
        // BOOKINGS
        
        // IMPORT PREVIOUS
        
    }
}
  