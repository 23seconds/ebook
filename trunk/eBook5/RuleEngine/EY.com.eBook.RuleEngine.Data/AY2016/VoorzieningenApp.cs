
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.RuleEngine.Core;
using EY.com.eBook.RuleEngine.Core.Interfaces;
using EY.com.eBook.RuleEngine.Data;
using EY.com.eBook.Core;

namespace EY.com.eBook.RuleEngine.AY2016
{

    public class VoorzieningenApp
    {
        public static WorksheetTypeSettings Settings = new WorksheetTypeSettings
        {
            Clients = true
          ,
            Suppliers = true
          ,
            AllAccounts = true
          ,
            PerformsBookings = true
        };

        public static Guid TypeId = new Guid("eda9a421-b457-4ca3-a9ac-f3c6bd1f9fff");

        public List<string> ImportablePreviousTypes = new List<string>() { "EY.com.eBook.RuleEngine.AY2016.VoorzieningenApp" };

        public Guid GetMyType() { return TypeId; }

        public void SetMinimal()
        {
            if (Data == null)
            {
                Data = new EY.com.eBook.RuleEngine.AY2016.VoorzieningenAppTypes.Voorzieningen();
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.ToestandEindVorigBoekjaar == null)
            {
                Data.ToestandEindVorigBoekjaar = new List<EY.com.eBook.RuleEngine.AY2016.VoorzieningenAppTypes.VoorzieningenT>(); ;
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.WijzigingBestaandeTijdensBoekjaar == null)
            {
                Data.WijzigingBestaandeTijdensBoekjaar = new List<EY.com.eBook.RuleEngine.AY2016.VoorzieningenAppTypes.VoorzieningenW>(); ;
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.NieuweTijdensBoekjaar == null)
            {
                Data.NieuweTijdensBoekjaar = new List<EY.com.eBook.RuleEngine.AY2016.VoorzieningenAppTypes.VoorzieningenN>(); ;
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.ToestandEindHuidigBoekjaar == null)
            {
                Data.ToestandEindHuidigBoekjaar = new List<EY.com.eBook.RuleEngine.AY2016.VoorzieningenAppTypes.VoorzieningenH>(); ;
                Data.LastUpdated = DateTime.Now;
            }

        }

        public string ToXml()
        {
            return CoreHelper.SerializeToString(Data);
        }

        public void LoadXml(string xml)
        {
            if (!string.IsNullOrEmpty(xml))
            {
                Data = CoreHelper.DeserializeFromString<EY.com.eBook.RuleEngine.AY2016.VoorzieningenAppTypes.Voorzieningen>(xml);
            }
            SetMinimal();

        }

        public EY.com.eBook.RuleEngine.AY2016.VoorzieningenAppTypes.Voorzieningen Data { get; set; }


        public void ForceRecalculation(ref WorksheetSetData worksheetset)
        {
            Data.LastUpdated = new DateTime(1980, 1, 1);
            Data.ToestandEindVorigBoekjaar.ForEach(e => e.LastUpdated = null);
            Data.WijzigingBestaandeTijdensBoekjaar.ForEach(e => e.LastUpdated = null);
            Data.NieuweTijdensBoekjaar.ForEach(e => e.LastUpdated = null);
            Data.ToestandEindHuidigBoekjaar.Clear();
            if(Data.List2043!=null ) Data.List2043.Clear();
            Calculate(ref worksheetset);
        }

        public void Calculate(ref WorksheetSetData worksheetset)
        {
            worksheetset.ClearMessages(TypeId);
            foreach (VoorzieningenAppTypes.VoorzieningenT entity in Data.ToestandEindVorigBoekjaar.Where(d => (!d.LastUpdated.HasValue || d.LastUpdated.Value > Data.LastUpdated) || !d.Nr.HasValue).ToList())
            {
                UpdateToestandEindVorigBoekjaar(entity, ref worksheetset);
            }
            foreach (VoorzieningenAppTypes.VoorzieningenW entity in Data.WijzigingBestaandeTijdensBoekjaar.Where(d => !d.LastUpdated.HasValue || d.LastUpdated.Value > Data.LastUpdated).ToList())
            {
                UpdateWijzigingBestaandeTijdensBoekjaar(entity, ref worksheetset);
            }
            foreach (VoorzieningenAppTypes.VoorzieningenN entity in Data.NieuweTijdensBoekjaar.Where(d => !d.LastUpdated.HasValue || d.LastUpdated.Value > Data.LastUpdated).ToList())
            {
                UpdateNieuweTijdensBoekjaar(entity, ref worksheetset);
            }
            Render2043(ref worksheetset);
            Data.LastUpdated = DateTime.Now;
        }

        public void Render2043(ref WorksheetSetData worksheetset)
        {
            if (Data.List2043 == null)
            {
                Data.List2043 = new List<EY.com.eBook.RuleEngine.AY2016.VoorzieningenAppTypes.Voorzieningen2043>();
            }
            else
            {
                Data.List2043.Clear();
            }

            if (Data.ToestandEindHuidigBoekjaar.Count(d => !d.IsValid) > 0)
            {
                foreach (VoorzieningenAppTypes.VoorzieningenH dd in Data.ToestandEindHuidigBoekjaar.Where(d => !d.IsValid).ToList())
                {
                    worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", "ToestandEindHuidigBoekjaar", dd.Id, "INVALID_REVIEW");
                }
            }


            Guid vrijgesteld = new Guid("9AFE7536-2A45-4146-AD96-1F7839FDA38C");
            foreach (VoorzieningenAppTypes.VoorzieningenH dd in Data.ToestandEindHuidigBoekjaar.Where(d => d.IsValid && d.Belastbaar != null && d.Belastbaar.Id == vrijgesteld && d.IsValid && d.AardVanDeKosten != null))
            {
                if (dd.AardVanDeKosten != null && dd.AardVanDeKosten.Id == "ExpensesOverhaulImportantRepairsMaximum10YearsMembe")
                {
                    dd.AardVanDeKosten.Id = "ExpensesOverhaulImportantRepairsMaximum10YearsMember";
                }
                VoorzieningenAppTypes.Voorzieningen2043 entity = new EY.com.eBook.RuleEngine.AY2016.VoorzieningenAppTypes.Voorzieningen2043
                {
                    Id = dd.Id
                    ,
                    IsValid = true
                    ,
                    LastUpdated = DateTime.Now
                    ,
                    VoorzieningBeginBj = dd.VoorzieningBeginBj
                    ,
                    VoorzieningEindeBJ = dd.VoorzieningEindeBJ
                    ,
                    WaarschijnlijkeKosten = dd.WaarschijnlijkeKosten
                    ,
                    VerminderingTijdensBJAanwending = dd.VerminderingTijdensBJAanwending
                    ,
                    VerminderingTijdensBJNieuweInschatting = dd.VerminderingTijdensBJNieuweInschatting
                    ,
                    VerhogingTijdensBJ = dd.VerhogingTijdensBJ
                    ,
                    PreviousImported = false
                    ,
                    Commentaar = dd.OmschrijvingProvisie.GetValueOrDefault("-")
                };
                entity.TypedMember = string.Format("{0}-{1}", dd.Assesmentyear.Value.ToString(), dd.Nr.Value.ToString()); //.PadLeft(2, '0')
                if (dd.isNew == true)
                {
                    entity.ExplicitMemberCategory = "d-ec:NewProvisionRisksExpensesMember";
                }
                else
                {
                    if (Data.WijzigingBestaandeTijdensBoekjaar.Count(d => d.Id == entity.Id) > 0)
                    {
                        if (dd.VerhogingTijdensBJ.GetValueOrDefault() > 0)
                        {
                            entity.ExplicitMemberCategory = "d-ec:RemainingBalanceAfterIncreaseProvisionRisksExpensesMember";
                        }
                        else
                        {
                            entity.ExplicitMemberCategory = "d-ec:RemainingBalanceAfterDecreaseProvisionRisksExpensesMember";
                        }
                    }
                    else
                    {
                        entity.ExplicitMemberCategory = "d-ec:UnalteredBalanceProvisionRisksExpensesMember";
                    }
                }
                
                entity.ExplicitMemberKind = "d-expt:" + dd.AardVanDeKosten.Id;
                Data.List2043.Add(entity);
            }
        }


        #region AddUpdateDelete

        public void AddToToestandEindVorigBoekjaar(EY.com.eBook.RuleEngine.AY2016.VoorzieningenAppTypes.VoorzieningenT entity, ref WorksheetSetData worksheetset)
        {
            entity = Validate(entity, "ToestandEindVorigBoekjaar", ref worksheetset);
            if (entity.IsValid)
            {
                Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
                UpdateAtEnd(entity, ref worksheetset);
                Data.ToestandEindVorigBoekjaar.Add(entity);
            }
        }

        public void DeleteToestandEindVorigBoekjaar(Guid rowId, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2016.VoorzieningenAppTypes.VoorzieningenT original = Data.ToestandEindVorigBoekjaar.FirstOrDefault(e => e.Id == rowId);
            if (original != null)
            {
                /*
                if (original.Boeken.HasValue && original.Boeken.Value)
                {
                    worksheetset.DeleteBooking(FacturatieApp.TypeId, "OpTeMakenCN", original.Id, null);
                    // DELETE BOOKING
                }*/
                Data.ToestandEindVorigBoekjaar.Remove(original);

                List<EY.com.eBook.RuleEngine.AY2016.VoorzieningenAppTypes.VoorzieningenW> wijzigingen = Data.WijzigingBestaandeTijdensBoekjaar.Where(e => e.Bestaande.Id == rowId.ToString()).ToList();

                if (wijzigingen.Count > 0)
                {
                    foreach (EY.com.eBook.RuleEngine.AY2016.VoorzieningenAppTypes.VoorzieningenW wijziging in wijzigingen)
                    {
                        DeleteWijzigingBestaandeTijdensBoekjaar(wijziging.Id, ref worksheetset);
                    }
                }

                EY.com.eBook.RuleEngine.AY2016.VoorzieningenAppTypes.VoorzieningenH huidige = Data.ToestandEindHuidigBoekjaar.FirstOrDefault(e => e.Id == rowId);
                if (huidige != null)
                {
                    Data.ToestandEindHuidigBoekjaar.Remove(huidige);
                }

                Data.LastUpdated = DateTime.Now;
            }
        }

        public void UpdateToestandEindVorigBoekjaar(EY.com.eBook.RuleEngine.AY2016.VoorzieningenAppTypes.VoorzieningenT entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2016.VoorzieningenAppTypes.VoorzieningenT original = Data.ToestandEindVorigBoekjaar.FirstOrDefault(e => e.Id == entity.Id);
            if (original == null)
            {
                AddToToestandEindVorigBoekjaar(entity, ref worksheetset);
            }
            else
            {
                entity = Validate(entity, "ToestandEindVorigBoekjaar", ref worksheetset);
                if (entity.IsValid)
                {
                    if (entity.AardVanDeKosten != null && entity.AardVanDeKosten.Id == "ExpensesOverhaulImportantRepairsMaximum10YearsMembe")
                    {
                        entity.AardVanDeKosten.Id = "ExpensesOverhaulImportantRepairsMaximum10YearsMember";
                    }
                    // Get All wijzigingen
                    foreach (EY.com.eBook.RuleEngine.AY2016.VoorzieningenAppTypes.VoorzieningenW wijziging in Data.WijzigingBestaandeTijdensBoekjaar.Where(e => e.Bestaande.Id == e.Id.ToString()))
                    {
                        UpdateWijzigingBestaandeTijdensBoekjaar(wijziging, ref worksheetset);
                    }


                    int idx = Data.ToestandEindVorigBoekjaar.IndexOf(original);
                    Data.ToestandEindVorigBoekjaar.RemoveAt(idx);
                    Data.ToestandEindVorigBoekjaar.Insert(idx, entity);
                    UpdateAtEnd(entity, ref worksheetset);
                    Data.LastUpdated = DateTime.Now;
                    entity.LastUpdated = DateTime.Now;
                }
            }
        }

        public void AddToWijzigingBestaandeTijdensBoekjaar(EY.com.eBook.RuleEngine.AY2016.VoorzieningenAppTypes.VoorzieningenW entity, ref WorksheetSetData worksheetset)
        {
            entity = Validate(entity, "WijzigingBestaandeTijdensBoekjaar", ref worksheetset);
            if (entity.IsValid)
            {
                EY.com.eBook.RuleEngine.AY2016.VoorzieningenAppTypes.VoorzieningenT source = Data.ToestandEindVorigBoekjaar.FirstOrDefault(e => e.Id.ToString() == entity.Bestaande.Id);

                if (source != null)
                {
                    entity.Assesmentyear = source.Assesmentyear;
                    entity.Belastbaar = source.Belastbaar;
                    entity.Nr = source.Nr;
                    entity.WaarschijnlijkeKostenBeginBJ = source.WaarschijnlijkeKosten;
                    entity.OmschrijvingProvisie = source.OmschrijvingProvisie;
                    entity.VoorzieningBeginBj = source.Voorziening;
                    entity.AardVanDeKosten = source.AardVanDeKosten;
                    if (!entity.WaarschijnlijkeKostenEindeBJ.HasValue) entity.WaarschijnlijkeKostenEindeBJ = entity.WaarschijnlijkeKostenBeginBJ;
                    decimal change = 0;
                    if (entity.VerhogingTijdensBJ.HasValue) change += entity.VerhogingTijdensBJ.Value;
                    if (entity.VerminderingTijdensBJAanwending.HasValue) change -= entity.VerminderingTijdensBJAanwending.Value;
                    if (entity.VerminderingTijdensBJNieuweInschatting.HasValue) change -= entity.VerminderingTijdensBJNieuweInschatting.Value;


                    entity.VoorzieningEindeBJ = entity.VoorzieningBeginBj + change;

                    if (entity.Book.HasValue && entity.Book.Value)
                    {
                        List<BookingLineDataContract> lines = new List<BookingLineDataContract>() {
                             new BookingLineDataContract { AccountNr =worksheetset.GetListAccount("407"), Amount=entity.VoorzieningEindeBJ.GetValueOrDefault() }
                            , new BookingLineDataContract { AccountNr = worksheetset.GetListAccount("400"), Amount=0-(entity.VoorzieningEindeBJ.GetValueOrDefault()) }
                        };
                        if (entity.WaarschijnlijkeKostenEindeBJ.GetValueOrDefault() != 0)
                        {
                            lines.Add(new BookingLineDataContract { AccountNr = worksheetset.GetListAccount("634"), Amount = entity.WaarschijnlijkeKostenEindeBJ.GetValueOrDefault() });
                            lines.Add(new BookingLineDataContract { AccountNr = worksheetset.GetListAccount("409"), Amount = 0-entity.WaarschijnlijkeKostenEindeBJ.GetValueOrDefault() });
                        }
                        worksheetset.AddBooking(TypeId, "WijzigingBestaandeTijdensBoekjaar", entity.Id, null, lines);
                    }
                    UpdateAtEnd(entity, ref worksheetset);
                }

                Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
                Data.WijzigingBestaandeTijdensBoekjaar.Add(entity);
            }
        }

        public void DeleteWijzigingBestaandeTijdensBoekjaar(Guid rowId, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2016.VoorzieningenAppTypes.VoorzieningenW original = Data.WijzigingBestaandeTijdensBoekjaar.FirstOrDefault(e => e.Id == rowId);
            if (original != null)
            {

                if (original.Book.HasValue && original.Book.Value)
                {
                    worksheetset.DeleteBooking(TypeId, "WijzigingBestaandeTijdensBoekjaar", original.Id, null);
                    // DELETE BOOKING
                }
                Data.WijzigingBestaandeTijdensBoekjaar.Remove(original);
                EY.com.eBook.RuleEngine.AY2016.VoorzieningenAppTypes.VoorzieningenH huidige = Data.ToestandEindHuidigBoekjaar.FirstOrDefault(e => e.Id == rowId);
                if (huidige != null)
                {
                    Data.ToestandEindHuidigBoekjaar.Remove(huidige);
                    if (original.Bestaande != null)
                    {
                        EY.com.eBook.RuleEngine.AY2016.VoorzieningenAppTypes.VoorzieningenT prev = Data.ToestandEindVorigBoekjaar.FirstOrDefault(t => t.Id.ToString() == original.Bestaande.Id);
                        if (prev != null) UpdateAtEnd(prev, ref worksheetset);
                    }
                }

                Data.LastUpdated = DateTime.Now;
            }
        }

        public void UpdateWijzigingBestaandeTijdensBoekjaar(EY.com.eBook.RuleEngine.AY2016.VoorzieningenAppTypes.VoorzieningenW entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2016.VoorzieningenAppTypes.VoorzieningenW original = Data.WijzigingBestaandeTijdensBoekjaar.FirstOrDefault(e => e.Id == entity.Id);
            if (original == null)
            {
                AddToWijzigingBestaandeTijdensBoekjaar(entity, ref worksheetset);
            }
            else
            {
                entity = Validate(entity, "WijzigingBestaandeTijdensBoekjaar", ref worksheetset);
                EY.com.eBook.RuleEngine.AY2016.VoorzieningenAppTypes.VoorzieningenT source = Data.ToestandEindVorigBoekjaar.FirstOrDefault(e => e.Id.ToString() == entity.Bestaande.Id);

                if (source != null)
                {
                    entity.Assesmentyear = source.Assesmentyear;
                    entity.Belastbaar = source.Belastbaar;
                    entity.Nr = source.Nr;
                    entity.WaarschijnlijkeKostenBeginBJ = source.WaarschijnlijkeKosten;
                    entity.OmschrijvingProvisie = source.OmschrijvingProvisie;
                    entity.VoorzieningBeginBj = source.Voorziening;
                    entity.AardVanDeKosten = source.AardVanDeKosten;
                    if (!entity.WaarschijnlijkeKostenEindeBJ.HasValue) entity.WaarschijnlijkeKostenEindeBJ = entity.WaarschijnlijkeKostenBeginBJ;
                    decimal change = 0;
                    if (entity.VerhogingTijdensBJ.HasValue) change += entity.VerhogingTijdensBJ.Value;
                    if (entity.VerminderingTijdensBJAanwending.HasValue) change -= entity.VerminderingTijdensBJAanwending.Value;
                    if (entity.VerminderingTijdensBJNieuweInschatting.HasValue) change -= entity.VerminderingTijdensBJNieuweInschatting.Value;

                    entity.VoorzieningEindeBJ = entity.VoorzieningBeginBj + change;
                }
                if (entity.IsValid)
                {
                    if (original.Book.HasValue && original.Book.Value && (!entity.Book.HasValue || !entity.Book.Value))
                    {
                        worksheetset.DeleteBooking(TypeId, "WijzigingBestaandeTijdensBoekjaar", original.Id, null);
                        // DELETE BOOKING
                    }
                    if (entity.Book.HasValue && entity.Book.Value)
                    {
                        List<BookingLineDataContract> lines = new List<BookingLineDataContract>() {
                             new BookingLineDataContract { AccountNr =worksheetset.GetListAccount("407"), Amount=entity.VoorzieningEindeBJ.GetValueOrDefault() }
                            , new BookingLineDataContract { AccountNr = worksheetset.GetListAccount("400"), Amount=0-(entity.VoorzieningEindeBJ.GetValueOrDefault()) }
                        };
                        if (entity.WaarschijnlijkeKostenEindeBJ.GetValueOrDefault() != 0)
                        {
                            lines.Add(new BookingLineDataContract { AccountNr = worksheetset.GetListAccount("634"), Amount = entity.WaarschijnlijkeKostenEindeBJ.GetValueOrDefault() });
                            lines.Add(new BookingLineDataContract { AccountNr = worksheetset.GetListAccount("409"), Amount = 0 - entity.WaarschijnlijkeKostenEindeBJ.GetValueOrDefault() });
                        }
                        if (original.Book.HasValue && original.Book.Value)
                        {

                            worksheetset.AddBooking(TypeId, "WijzigingBestaandeTijdensBoekjaar", entity.Id, null, lines);
                        }
                        else
                        {
                            worksheetset.UpdateBooking(TypeId, "WijzigingBestaandeTijdensBoekjaar", entity.Id, null, lines);
                        }
                    }
                    int idx = Data.WijzigingBestaandeTijdensBoekjaar.IndexOf(original);
                    Data.WijzigingBestaandeTijdensBoekjaar.RemoveAt(idx);
                    Data.WijzigingBestaandeTijdensBoekjaar.Insert(idx, entity);
                    UpdateAtEnd(entity, ref worksheetset);
                    Data.LastUpdated = DateTime.Now;
                    entity.LastUpdated = DateTime.Now;
                }
            }
        }

        public void AddToNieuweTijdensBoekjaar(EY.com.eBook.RuleEngine.AY2016.VoorzieningenAppTypes.VoorzieningenN entity, ref WorksheetSetData worksheetset)
        {
            entity = Validate(entity, "NieuweTijdensBoekjaar", ref worksheetset);
            if (entity.IsValid)
            {
                entity.Assesmentyear = worksheetset.DataContract.AssessmentYear;
                if (!entity.Nr.HasValue)
                {
                    entity.Nr = Data.NieuweTijdensBoekjaar.Max(e => e.Nr) + 1;
                }

                if (entity.Book.HasValue && entity.Book.Value)
                {
                    List<BookingLineDataContract> lines = new List<BookingLineDataContract>() {
                             new BookingLineDataContract { AccountNr =worksheetset.GetListAccount("407"), Amount=entity.Voorziening.GetValueOrDefault() }
                            , new BookingLineDataContract { AccountNr = worksheetset.GetListAccount("400"), Amount=0-(entity.Voorziening.GetValueOrDefault()) }
                        };
                    if (entity.WaarschijnlijkeKosten.GetValueOrDefault() != 0)
                    {
                        lines.Add(new BookingLineDataContract { AccountNr = worksheetset.GetListAccount("634"), Amount = entity.WaarschijnlijkeKosten.GetValueOrDefault() });
                        lines.Add(new BookingLineDataContract { AccountNr = worksheetset.GetListAccount("409"), Amount = 0 - entity.WaarschijnlijkeKosten.GetValueOrDefault() });
                    }
                    worksheetset.AddBooking(TypeId, "NieuweTijdensBoekjaar", entity.Id, null, lines);
                }

                Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
                Data.NieuweTijdensBoekjaar.Add(entity);
                UpdateAtEnd(entity, ref worksheetset);
            }
        }

        public void DeleteNieuweTijdensBoekjaar(Guid rowId, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2016.VoorzieningenAppTypes.VoorzieningenN original = Data.NieuweTijdensBoekjaar.FirstOrDefault(e => e.Id == rowId);
            if (original != null)
            {

                if (original.Book.HasValue && original.Book.Value)
                {
                    worksheetset.DeleteBooking(TypeId, "NieuweTijdensBoekjaar", original.Id, null);
                    // DELETE BOOKING
                }
                Data.NieuweTijdensBoekjaar.Remove(original);
                EY.com.eBook.RuleEngine.AY2016.VoorzieningenAppTypes.VoorzieningenH huidige = Data.ToestandEindHuidigBoekjaar.FirstOrDefault(e => e.Id == rowId);
                if (huidige != null)
                {
                    Data.ToestandEindHuidigBoekjaar.Remove(huidige);
                }
                Data.LastUpdated = DateTime.Now;
            }
        }

        public void UpdateNieuweTijdensBoekjaar(EY.com.eBook.RuleEngine.AY2016.VoorzieningenAppTypes.VoorzieningenN entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2016.VoorzieningenAppTypes.VoorzieningenN original = Data.NieuweTijdensBoekjaar.FirstOrDefault(e => e.Id == entity.Id);
            if (original == null)
            {
                AddToNieuweTijdensBoekjaar(entity, ref worksheetset);
            }
            else
            {
                entity = Validate(entity, "NieuweTijdensBoekjaar", ref worksheetset);
                if (entity.IsValid)
                {
                    if (original.Book.HasValue && original.Book.Value && (!entity.Book.HasValue || !entity.Book.Value))
                    {
                        worksheetset.DeleteBooking(TypeId, "NieuweTijdensBoekjaar", original.Id, null);
                        // DELETE BOOKING
                    }
                    if (entity.Book.HasValue && entity.Book.Value)
                    {
                        List<BookingLineDataContract> lines = new List<BookingLineDataContract>() {
                             new BookingLineDataContract { AccountNr =worksheetset.GetListAccount("407"), Amount=entity.Voorziening.GetValueOrDefault() }
                            , new BookingLineDataContract { AccountNr = worksheetset.GetListAccount("400"), Amount=0-(entity.Voorziening.GetValueOrDefault()) }
                        };
                        if (entity.WaarschijnlijkeKosten.GetValueOrDefault() != 0)
                        {
                            lines.Add(new BookingLineDataContract { AccountNr = worksheetset.GetListAccount("634"), Amount = entity.WaarschijnlijkeKosten.GetValueOrDefault() });
                            lines.Add(new BookingLineDataContract { AccountNr = worksheetset.GetListAccount("409"), Amount = 0 - entity.WaarschijnlijkeKosten.GetValueOrDefault() });
                        }
                        if (original.Book.HasValue && original.Book.Value)
                        {
                            worksheetset.UpdateBooking(TypeId, "NieuweTijdensBoekjaar", entity.Id, null, lines);
                        }
                        else
                        {
                            worksheetset.AddBooking(TypeId, "NieuweTijdensBoekjaar", entity.Id, null, lines);
                        }
                    }
                    int idx = Data.NieuweTijdensBoekjaar.IndexOf(original);
                    Data.NieuweTijdensBoekjaar.RemoveAt(idx);
                    Data.NieuweTijdensBoekjaar.Insert(idx, entity);
                    UpdateAtEnd(entity, ref worksheetset);
                    Data.LastUpdated = DateTime.Now;
                    entity.LastUpdated = DateTime.Now;
                }
            }
        }



        public void UpdateAtEnd(EY.com.eBook.RuleEngine.AY2016.VoorzieningenAppTypes.VoorzieningenN sourceEntity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2016.VoorzieningenAppTypes.VoorzieningenH huidig = Data.ToestandEindHuidigBoekjaar.FirstOrDefault(e => e.Id == sourceEntity.Id);
            if (huidig == null)
            {
                huidig = new EY.com.eBook.RuleEngine.AY2016.VoorzieningenAppTypes.VoorzieningenH
                {
                    Id = sourceEntity.Id
                };
                Data.ToestandEindHuidigBoekjaar.Add(huidig);
            }
            
            huidig.Assesmentyear = sourceEntity.Assesmentyear;
            huidig.Nr = sourceEntity.Nr;
            huidig.OmschrijvingProvisie = sourceEntity.OmschrijvingProvisie;
            huidig.VoorzieningBeginBj = 0;
            huidig.VoorzieningEindeBJ = sourceEntity.Voorziening;
            huidig.WaarschijnlijkeKosten = sourceEntity.WaarschijnlijkeKosten;

            
            huidig.AardVanDeKosten = sourceEntity.AardVanDeKosten;
            huidig.Belastbaar = sourceEntity.Belastbaar;
            huidig.isNew = true;
            huidig.IsValid = true;
            huidig.LastUpdated = DateTime.Now;
            
          
            huidig.PreviousImported = false;
          
            huidig.VerhogingTijdensBJ = 0;
            huidig.VerminderingTijdensBJAanwending = 0;
            huidig.VerminderingTijdensBJNieuweInschatting = 0;
            
        }

        public void UpdateAtEnd(EY.com.eBook.RuleEngine.AY2016.VoorzieningenAppTypes.VoorzieningenW sourceEntity, ref WorksheetSetData worksheetset)
        {

            EY.com.eBook.RuleEngine.AY2016.VoorzieningenAppTypes.VoorzieningenH huidigVorige = Data.ToestandEindHuidigBoekjaar.FirstOrDefault(e => e.Id.ToString() == sourceEntity.Bestaande.Id);
            if (huidigVorige != null)
            {
                Data.ToestandEindHuidigBoekjaar.Remove(huidigVorige);
            }

            EY.com.eBook.RuleEngine.AY2016.VoorzieningenAppTypes.VoorzieningenH huidig = Data.ToestandEindHuidigBoekjaar.FirstOrDefault(e => e.Id == sourceEntity.Id);
            if (huidig == null)
            {
                huidig = new EY.com.eBook.RuleEngine.AY2016.VoorzieningenAppTypes.VoorzieningenH
                {
                    Id = sourceEntity.Id
                };
                Data.ToestandEindHuidigBoekjaar.Add(huidig);
            }
            huidig.Assesmentyear = sourceEntity.Assesmentyear;
            huidig.Nr = sourceEntity.Nr;
            huidig.OmschrijvingProvisie = sourceEntity.OmschrijvingProvisie;
            huidig.VoorzieningBeginBj = sourceEntity.VoorzieningBeginBj;
            huidig.VoorzieningEindeBJ = sourceEntity.VoorzieningEindeBJ;
            huidig.WaarschijnlijkeKosten = sourceEntity.WaarschijnlijkeKostenEindeBJ;


            huidig.AardVanDeKosten = sourceEntity.AardVanDeKosten;
            huidig.Belastbaar = sourceEntity.Belastbaar;
            huidig.isNew = false;
            huidig.IsValid = true;
            huidig.LastUpdated = DateTime.Now;


            huidig.PreviousImported = false;

            huidig.VerhogingTijdensBJ = sourceEntity.VerhogingTijdensBJ;
            huidig.VerminderingTijdensBJAanwending = sourceEntity.VerminderingTijdensBJAanwending;
            huidig.VerminderingTijdensBJNieuweInschatting = sourceEntity.VerminderingTijdensBJNieuweInschatting;

        }

        public void UpdateAtEnd(EY.com.eBook.RuleEngine.AY2016.VoorzieningenAppTypes.VoorzieningenT sourceEntity, ref WorksheetSetData worksheetset)
        {
            List<EY.com.eBook.RuleEngine.AY2016.VoorzieningenAppTypes.VoorzieningenW> wijzigingen = Data.WijzigingBestaandeTijdensBoekjaar.Where(e => e.Bestaande.Id == sourceEntity.Id.ToString()).ToList();

            if (wijzigingen.Count > 0)
            {
                foreach (EY.com.eBook.RuleEngine.AY2016.VoorzieningenAppTypes.VoorzieningenW wijziging in wijzigingen)
                {
                    UpdateAtEnd(wijziging, ref worksheetset);
                }
            }
            else
            {
                EY.com.eBook.RuleEngine.AY2016.VoorzieningenAppTypes.VoorzieningenH huidig = Data.ToestandEindHuidigBoekjaar.FirstOrDefault(e => e.Id == sourceEntity.Id);
                if (huidig == null)
                {
                    huidig = new EY.com.eBook.RuleEngine.AY2016.VoorzieningenAppTypes.VoorzieningenH
                    {
                        Id = sourceEntity.Id
                    };
                    Data.ToestandEindHuidigBoekjaar.Add(huidig);
                }
                huidig.Assesmentyear = sourceEntity.Assesmentyear;
                huidig.Nr = sourceEntity.Nr;
                huidig.OmschrijvingProvisie = sourceEntity.OmschrijvingProvisie;
                huidig.VoorzieningBeginBj = sourceEntity.Voorziening;
                huidig.VoorzieningEindeBJ = sourceEntity.Voorziening;
                huidig.WaarschijnlijkeKosten = sourceEntity.WaarschijnlijkeKosten;


                huidig.AardVanDeKosten = sourceEntity.AardVanDeKosten;
                huidig.Belastbaar = sourceEntity.Belastbaar;
                huidig.isNew = false;
                huidig.IsValid = true;
                huidig.LastUpdated = DateTime.Now;


                huidig.PreviousImported = false;

                huidig.VerhogingTijdensBJ = 0;
                huidig.VerminderingTijdensBJAanwending = 0;
                huidig.VerminderingTijdensBJNieuweInschatting = 0;


            }

        }

        #endregion

        // CALCULATION

        // BOOKINGS

        // IMPORT PREVIOUS

        #region VALIDATION

        public EY.com.eBook.RuleEngine.AY2016.VoorzieningenAppTypes.VoorzieningenT Validate(EY.com.eBook.RuleEngine.AY2016.VoorzieningenAppTypes.VoorzieningenT entity, string location, ref WorksheetSetData worksheetset)
        {
            entity.IsValid = true;
            worksheetset.ClearMessages(TypeId, entity.Id);
            
            if (string.IsNullOrEmpty(entity.OmschrijvingProvisie))
            {
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "OmschrijvingProvisie");
                entity.IsValid = false;
            }
            if (entity.AardVanDeKosten ==null)
            {
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "AardVanDeKosten");
                entity.IsValid = false;
            }
            if (entity.Belastbaar == null)
            {
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Belastbaar");
                entity.IsValid = false;
            }
            if (entity.AardVanDeKosten == null)
            {
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "AardVanDeKosten");
                entity.IsValid = false;
            }
            if (!entity.Voorziening.HasValue)
            {
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Voorziening");
                entity.IsValid = false;
            }
            if (!entity.WaarschijnlijkeKosten.HasValue)
            {
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "WaarschijnlijkeKosten");
                entity.IsValid = false;
            }

            /* eBook 5.2 #52 */
            if (entity.Voorziening.GetValueOrDefault() > entity.WaarschijnlijkeKosten.GetValueOrDefault())
            {
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "SMALLERTHEN", location, entity.Id, "Voorziening");
                entity.IsValid = false;
            }

            if (!entity.Assesmentyear.HasValue)
            {
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Assesmentyear");
                entity.IsValid = false;
            }

            /* eBook 5.2 #52 */
            if (entity.Assesmentyear.GetValueOrDefault() >= worksheetset.DataContract.AssessmentYear)
            {
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "SMALLERTHEN", location, entity.Id, "Assesmentyear");
                entity.IsValid = false;
            }

            if (!entity.Nr.HasValue)
            {
                if (Data.ToestandEindVorigBoekjaar.Count(e => e.Assesmentyear.HasValue && e.Assesmentyear == entity.Assesmentyear && e.Nr.HasValue) > 0)
                {
                    entity.Nr = Data.ToestandEindVorigBoekjaar.Where(e => e.Assesmentyear.HasValue && e.Assesmentyear == entity.Assesmentyear && e.Nr.HasValue).Max(e => e.Nr.Value) + 1;
                    entity.LastUpdated = DateTime.Now.AddSeconds(-1);
                }
                else
                {
                    entity.Nr = 1;
                }
            }

            return entity;
        }

        public EY.com.eBook.RuleEngine.AY2016.VoorzieningenAppTypes.VoorzieningenW Validate(EY.com.eBook.RuleEngine.AY2016.VoorzieningenAppTypes.VoorzieningenW entity, string location, ref WorksheetSetData worksheetset)
        {
            entity.IsValid = true;
            worksheetset.ClearMessages(TypeId, entity.Id);

            if (entity.Bestaande == null)
            {
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Bestaande");
                entity.IsValid = false;
            }
            else
            {
                if (string.IsNullOrEmpty(entity.Bestaande.Id))
                {
                    worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Bestaande");
                    entity.IsValid = false;
                }
                else
                {
                    if (Data.ToestandEindVorigBoekjaar.Count(c => c.Id.ToString().ToLower() == entity.Bestaande.Id.ToLower()) == 0)
                    {
                        worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Bestaande");
                        entity.IsValid = false;
                    }

                    /* eBook 5.2 #52 */
                    if (entity.VerminderingTijdensBJAanwending > 0 && entity.VerminderingTijdensBJNieuweInschatting > 0 && entity.VerhogingTijdensBJ > 0)
                    {
                        worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "GREATERTHEN", location, entity.Id, "VerminderingTijdensBJAanwending");
                        worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "GREATERTHEN", location, entity.Id, "VerminderingTijdensBJNieuweInschatting");
                        worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "GREATERTHEN", location, entity.Id, "VerhogingTijdensBJ");
                        entity.IsValid = false;
                    }
                    else if (entity.VerminderingTijdensBJAanwending > 0 && entity.VerminderingTijdensBJNieuweInschatting > 0)
                    {
                        worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "GREATERTHEN", location, entity.Id, "VerminderingTijdensBJAanwending");
                        worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "GREATERTHEN", location, entity.Id, "VerminderingTijdensBJNieuweInschatting");
                        entity.IsValid = false;
                    }
                    else if (entity.VerminderingTijdensBJAanwending > 0 && entity.VerhogingTijdensBJ > 0)
                    {
                        worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "GREATERTHEN", location, entity.Id, "VerminderingTijdensBJAanwending");
                        worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "GREATERTHEN", location, entity.Id, "VerhogingTijdensBJ");
                        entity.IsValid = false;
                    }
                    else if (entity.VerminderingTijdensBJNieuweInschatting > 0 && entity.VerhogingTijdensBJ > 0)
                    {
                        worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "GREATERTHEN", location, entity.Id, "VerminderingTijdensBJNieuweInschatting");
                        worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "GREATERTHEN", location, entity.Id, "VerhogingTijdensBJ");
                        entity.IsValid = false;
                    }
                    /* */
                }
            }
            //if (entity.AardVanDeKosten == null)
            //{
            //    worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "AardVanDeKosten");
            //    entity.IsValid = false;
            //}
            /*
            if (!entity.Bedrag.HasValue || entity.Bedrag.Value==0)
            {
            worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Grootboek");
            entity.IsValid = false;
            }
            */

            return entity;
        }

        public EY.com.eBook.RuleEngine.AY2016.VoorzieningenAppTypes.VoorzieningenN Validate(EY.com.eBook.RuleEngine.AY2016.VoorzieningenAppTypes.VoorzieningenN entity, string location, ref WorksheetSetData worksheetset)
        {
            entity.IsValid = true;
            worksheetset.ClearMessages(TypeId, entity.Id);
            if (!entity.Nr.HasValue)
            {
                if (Data.ToestandEindVorigBoekjaar.Count(e => e.Assesmentyear.HasValue && e.Assesmentyear == entity.Assesmentyear && e.Nr.HasValue) > 0)
                {
                    entity.Nr = Data.ToestandEindVorigBoekjaar.Where(e => e.Assesmentyear.HasValue && e.Assesmentyear == entity.Assesmentyear && e.Nr.HasValue).Max(e => e.Nr.Value) + 1;
                }
                else
                {
                    /* Ticket #72 */
                    if (Data.NieuweTijdensBoekjaar.Count > 0)
                    {
                        entity.Nr = Data.NieuweTijdensBoekjaar.Max(e => e.Nr) + 1;
                    }
                    else
                    {
                        entity.Nr = 1;
                    }
                }

                
            }
            
            if (entity.AardVanDeKosten==null)
            {
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "AardVanDeKosten");
                entity.IsValid = false;
            }

            /* eBook 5.2 #52 */
            if (entity.Voorziening.GetValueOrDefault() > entity.WaarschijnlijkeKosten.GetValueOrDefault())
            {
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "SMALLERTHEN", location, entity.Id, "Voorziening");
                entity.IsValid = false;
            }
            /* */

            return entity;
        }

        public EY.com.eBook.RuleEngine.AY2016.VoorzieningenAppTypes.VoorzieningenH Validate(EY.com.eBook.RuleEngine.AY2016.VoorzieningenAppTypes.VoorzieningenH entity, string location, ref WorksheetSetData worksheetset)
        {
            entity.IsValid = true;
            worksheetset.ClearMessages(TypeId, entity.Id);
            /*
            if (!entity.Bedrag.HasValue || entity.Bedrag.Value==0)
            {
            worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Grootboek");
            entity.IsValid = false;
            }
            */

            return entity;
        }

        #endregion


        public void ImportPrevious(ref WorksheetSetData wsd)
        {
            if (wsd.DataContract.PreviousFileId.HasValue)
            {
                if (wsd.PreviousSet.AssessmentYear == 2015)
                {
                    EY.com.eBook.RuleEngine.AY2015.WorksheetSetData pset = (EY.com.eBook.RuleEngine.AY2015.WorksheetSetData)wsd.PreviousSet;
                    EY.com.eBook.RuleEngine.AY2015.VoorzieningenApp previous = pset.VoorzieningenApp;

                    int nr = 1;
                    foreach (AY2015.VoorzieningenAppTypes.VoorzieningenH import in previous.Data.ToestandEindHuidigBoekjaar)
                    {
                        AY2016.VoorzieningenAppTypes.VoorzieningenT item = Data.ToestandEindVorigBoekjaar.FirstOrDefault(t => t.Id == import.Id);
                        if (item == null)
                        {
                            item = new EY.com.eBook.RuleEngine.AY2016.VoorzieningenAppTypes.VoorzieningenT
                            {
                                Id = import.Id
                            };
                            Data.ToestandEindVorigBoekjaar.Add(item);
                        }
                        item.AardVanDeKosten = import.AardVanDeKosten;
                        if (!import.Assesmentyear.HasValue)
                        {
                            item.Assesmentyear = wsd.DataContract.EndDate.AddDays(1).Year - 1;
                        }
                        else
                        {
                            item.Assesmentyear = import.Assesmentyear; // import.isNew == true ? wsd.DataContract.StartDate.Year : wsd.DataContract.StartDate.Year - 1;
                        }

                        item.OmschrijvingProvisie = import.OmschrijvingProvisie;
                        item.PreviousImported = true;
                        item.Voorziening = import.VoorzieningEindeBJ;
                        item.WaarschijnlijkeKosten = import.WaarschijnlijkeKosten;
                        item.Belastbaar = import.Belastbaar;

                        if (Data.ToestandEindVorigBoekjaar.Count(t => t.Id != item.Id && t.Assesmentyear == import.Assesmentyear && t.Nr == import.Nr) > 0)
                        {
                            item.Nr = null;
                        }
                        else
                        {
                            item.Nr = import.Nr;
                        }
                    }


                }

                if (wsd.PreviousSet.AssessmentYear == 2015)
                {
                    EY.com.eBook.RuleEngine.AY2015.WorksheetSetData pset = (EY.com.eBook.RuleEngine.AY2015.WorksheetSetData)wsd.PreviousSet;
                    EY.com.eBook.RuleEngine.AY2015.VoorzieningenApp previous = pset.VoorzieningenApp;

                    int nr = 1;
                    foreach (AY2015.VoorzieningenAppTypes.VoorzieningenH import in previous.Data.ToestandEindHuidigBoekjaar)
                    {
                        AY2016.VoorzieningenAppTypes.VoorzieningenT item = Data.ToestandEindVorigBoekjaar.FirstOrDefault(t => t.Id == import.Id);
                        if (item == null)
                        {
                            item = new EY.com.eBook.RuleEngine.AY2016.VoorzieningenAppTypes.VoorzieningenT
                            {
                                Id = import.Id
                            };
                            Data.ToestandEindVorigBoekjaar.Add(item);
                        }
                        item.AardVanDeKosten = import.AardVanDeKosten;
                        if (!import.Assesmentyear.HasValue)
                        {
                            item.Assesmentyear = wsd.DataContract.EndDate.AddDays(1).Year - 1;
                        }
                        else
                        {
                            item.Assesmentyear = import.Assesmentyear; // import.isNew == true ? wsd.DataContract.StartDate.Year : wsd.DataContract.StartDate.Year - 1;
                        }

                        item.OmschrijvingProvisie = import.OmschrijvingProvisie;
                        item.PreviousImported = true;
                        item.Voorziening = import.VoorzieningEindeBJ;
                        item.WaarschijnlijkeKosten = import.WaarschijnlijkeKosten;
                        item.Belastbaar = import.Belastbaar;

                        if (Data.ToestandEindVorigBoekjaar.Count(t => t.Id != item.Id && t.Assesmentyear == import.Assesmentyear && t.Nr == import.Nr) > 0)
                        {
                            item.Nr = null;
                        }
                        else
                        {
                            item.Nr = import.Nr;
                        }
                    }


                }
            }
        }
    }
}
  

/*
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.RuleEngine.Core;
using EY.com.eBook.RuleEngine.Core.Interfaces;
using EY.com.eBook.RuleEngine.Data;
using EY.com.eBook.Core;

namespace EY.com.eBook.RuleEngine.AY2016
{
    
    public class VoorzieningenApp
    {
        public static WorksheetTypeSettings Settings = new WorksheetTypeSettings
                                                      {
                                                        Clients = true
                                                        ,
                                                        Suppliers = true
                                                        ,
                                                        AllAccounts = true
                                                        , 
                                                        PerformsBookings=true
                                                      };
                                                      
        public static Guid TypeId = new Guid("eda9a421-b457-4ca3-a9ac-f3c6bd1f9fff");

        public List<string> ImportablePreviousTypes  = new List<string>() {  "EY.com.eBook.RuleEngine.AY2012.FacturatieApp"  };

        public Guid GetMyType() { return TypeId; }

        public void SetMinimal()
        {
            if (Data == null)
            {
                Data = new EY.com.eBook.RuleEngine.AY2016.VoorzieningenAppTypes.Voorzieningen();
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.ToestandEindVorigBoekjaar == null)
            {
                Data.ToestandEindVorigBoekjaar = new List<EY.com.eBook.RuleEngine.AY2016.VoorzieningenAppTypes.VoorzieningenT>(); ;
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.WijzigingBestaandeTijdensBoekjaar == null)
            {
                Data.WijzigingBestaandeTijdensBoekjaar = new List<EY.com.eBook.RuleEngine.AY2016.VoorzieningenAppTypes.VoorzieningenW>(); ;
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.NieuweTijdensBoekjaar == null)
            {
                Data.NieuweTijdensBoekjaar = new List<EY.com.eBook.RuleEngine.AY2016.VoorzieningenAppTypes.VoorzieningenN>(); ;
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.ToestandEindHuidigBoekjaar == null)
            {
                Data.ToestandEindHuidigBoekjaar = new List<EY.com.eBook.RuleEngine.AY2016.VoorzieningenAppTypes.VoorzieningenH>(); ;
                Data.LastUpdated = DateTime.Now;
            }

        }
        
        public string ToXml() 
        {
            return CoreHelper.SerializeToString(Data);
        }

        public void LoadXml(string xml)
        {
            if (!string.IsNullOrEmpty(xml)) {
                Data = CoreHelper.DeserializeFromString<EY.com.eBook.RuleEngine.AY2016.VoorzieningenAppTypes.Voorzieningen>(xml);
            }
            SetMinimal();
            
        }
        
        public EY.com.eBook.RuleEngine.AY2016.VoorzieningenAppTypes.Voorzieningen Data { get; set; }
        
        public void Calculate(ref WorksheetSetData worksheetset)
        {
            worksheetset.ClearMessages(TypeId);
           // Data.LastCalculated = DateTime.Now;
        }

        #region AddUpdateDelete

        public void AddToToestandEindVorigBoekjaar(EY.com.eBook.RuleEngine.AY2016.VoorzieningenAppTypes.VoorzieningenT entity, ref WorksheetSetData worksheetset)
        {
            entity = Validate(entity, "ToestandEindVorigBoekjaar", ref worksheetset);
            if (entity.IsValid)
            {
                Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
                Data.ToestandEindVorigBoekjaar.Add(entity);
            }
        }

        public void DeleteToestandEindVorigBoekjaar(Guid rowId, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2016.VoorzieningenAppTypes.VoorzieningenT original = Data.ToestandEindVorigBoekjaar.FirstOrDefault(e => e.Id == rowId);
            if (original != null)
            {
           
                if (original.Boeken.HasValue && original.Boeken.Value)
                {
                    worksheetset.DeleteBooking(FacturatieApp.TypeId, "OpTeMakenCN", original.Id, null);
                    // DELETE BOOKING
                }
                Data.ToestandEindVorigBoekjaar.Remove(original);
                Data.LastUpdated = DateTime.Now;
            }
        }

        public void UpdateToestandEindVorigBoekjaar(EY.com.eBook.RuleEngine.AY2016.VoorzieningenAppTypes.VoorzieningenT entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2016.VoorzieningenAppTypes.VoorzieningenT original = Data.ToestandEindVorigBoekjaar.FirstOrDefault(e => e.Id == entity.Id);
            if (original == null)
            {
                AddToToestandEindVorigBoekjaar(entity, ref worksheetset);
            }
            else
            {
                entity = Validate(entity, "ToestandEindVorigBoekjaar", ref worksheetset);
                if (entity.IsValid)
                {
                    int idx = Data.ToestandEindVorigBoekjaar.IndexOf(original);
                    Data.ToestandEindVorigBoekjaar.RemoveAt(idx);
                    Data.ToestandEindVorigBoekjaar.Insert(idx, entity);
                    Data.LastUpdated = DateTime.Now;
                    entity.LastUpdated = DateTime.Now;
                }
            }
        }

        public void AddToWijzigingBestaandeTijdensBoekjaar(EY.com.eBook.RuleEngine.AY2016.VoorzieningenAppTypes.VoorzieningenW entity, ref WorksheetSetData worksheetset)
        {
            entity = Validate(entity, "WijzigingBestaandeTijdensBoekjaar", ref worksheetset);
            if (entity.IsValid)
            {
                Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
                Data.WijzigingBestaandeTijdensBoekjaar.Add(entity);
            }
        }

        public void DeleteWijzigingBestaandeTijdensBoekjaar(Guid rowId, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2016.VoorzieningenAppTypes.VoorzieningenW original = Data.WijzigingBestaandeTijdensBoekjaar.FirstOrDefault(e => e.Id == rowId);
            if (original != null)
            {
           
                if (original.Boeken.HasValue && original.Boeken.Value)
                {
                    worksheetset.DeleteBooking(FacturatieApp.TypeId, "OpTeMakenCN", original.Id, null);
                    // DELETE BOOKING
                }
                Data.WijzigingBestaandeTijdensBoekjaar.Remove(original);
                Data.LastUpdated = DateTime.Now;
            }
        }

        public void UpdateWijzigingBestaandeTijdensBoekjaar(EY.com.eBook.RuleEngine.AY2016.VoorzieningenAppTypes.VoorzieningenW entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2016.VoorzieningenAppTypes.VoorzieningenW original = Data.WijzigingBestaandeTijdensBoekjaar.FirstOrDefault(e => e.Id == entity.Id);
            if (original == null)
            {
                AddToWijzigingBestaandeTijdensBoekjaar(entity, ref worksheetset);
            }
            else
            {
                entity = Validate(entity, "WijzigingBestaandeTijdensBoekjaar", ref worksheetset);
                if (entity.IsValid)
                {
                    int idx = Data.WijzigingBestaandeTijdensBoekjaar.IndexOf(original);
                    Data.WijzigingBestaandeTijdensBoekjaar.RemoveAt(idx);
                    Data.WijzigingBestaandeTijdensBoekjaar.Insert(idx, entity);
                    Data.LastUpdated = DateTime.Now;
                    entity.LastUpdated = DateTime.Now;
                }
            }
        }

        public void AddToNieuweTijdensBoekjaar(EY.com.eBook.RuleEngine.AY2016.VoorzieningenAppTypes.VoorzieningenN entity, ref WorksheetSetData worksheetset)
        {
            entity = Validate(entity, "NieuweTijdensBoekjaar", ref worksheetset);
            if (entity.IsValid)
            {
                Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
                Data.NieuweTijdensBoekjaar.Add(entity);
            }
        }

        public void DeleteNieuweTijdensBoekjaar(Guid rowId, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2016.VoorzieningenAppTypes.VoorzieningenN original = Data.NieuweTijdensBoekjaar.FirstOrDefault(e => e.Id == rowId);
            if (original != null)
            {
             
                if (original.Boeken.HasValue && original.Boeken.Value)
                {
                    worksheetset.DeleteBooking(FacturatieApp.TypeId, "OpTeMakenCN", original.Id, null);
                    // DELETE BOOKING
                }
                Data.NieuweTijdensBoekjaar.Remove(original);
                Data.LastUpdated = DateTime.Now;
            }
        }

        public void UpdateNieuweTijdensBoekjaar(EY.com.eBook.RuleEngine.AY2016.VoorzieningenAppTypes.VoorzieningenN entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2016.VoorzieningenAppTypes.VoorzieningenN original = Data.NieuweTijdensBoekjaar.FirstOrDefault(e => e.Id == entity.Id);
            if (original == null)
            {
                AddToNieuweTijdensBoekjaar(entity, ref worksheetset);
            }
            else
            {
                entity = Validate(entity, "NieuweTijdensBoekjaar", ref worksheetset);
                if (entity.IsValid)
                {
                    int idx = Data.NieuweTijdensBoekjaar.IndexOf(original);
                    Data.NieuweTijdensBoekjaar.RemoveAt(idx);
                    Data.NieuweTijdensBoekjaar.Insert(idx, entity);
                    Data.LastUpdated = DateTime.Now;
                    entity.LastUpdated = DateTime.Now;
                }
            }
        }

        public void AddToToestandEindHuidigBoekjaar(EY.com.eBook.RuleEngine.AY2016.VoorzieningenAppTypes.VoorzieningenH entity, ref WorksheetSetData worksheetset)
        {
            entity = Validate(entity, "ToestandEindHuidigBoekjaar", ref worksheetset);
            if (entity.IsValid)
            {
                Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
                Data.ToestandEindHuidigBoekjaar.Add(entity);
            }
        }

        public void DeleteToestandEindHuidigBoekjaar(Guid rowId, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2016.VoorzieningenAppTypes.VoorzieningenH original = Data.ToestandEindHuidigBoekjaar.FirstOrDefault(e => e.Id == rowId);
            if (original != null)
            {
            
                if (original.Boeken.HasValue && original.Boeken.Value)
                {
                    worksheetset.DeleteBooking(FacturatieApp.TypeId, "OpTeMakenCN", original.Id, null);
                    // DELETE BOOKING
                }
                Data.ToestandEindHuidigBoekjaar.Remove(original);
                Data.LastUpdated = DateTime.Now;
            }
        }

        public void UpdateToestandEindHuidigBoekjaar(EY.com.eBook.RuleEngine.AY2016.VoorzieningenAppTypes.VoorzieningenH entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2016.VoorzieningenAppTypes.VoorzieningenH original = Data.ToestandEindHuidigBoekjaar.FirstOrDefault(e => e.Id == entity.Id);
            if (original == null)
            {
                AddToToestandEindHuidigBoekjaar(entity, ref worksheetset);
            }
            else
            {
                entity = Validate(entity, "ToestandEindHuidigBoekjaar", ref worksheetset);
                if (entity.IsValid)
                {
                    int idx = Data.ToestandEindHuidigBoekjaar.IndexOf(original);
                    Data.ToestandEindHuidigBoekjaar.RemoveAt(idx);
                    Data.ToestandEindHuidigBoekjaar.Insert(idx, entity);
                    Data.LastUpdated = DateTime.Now;
                    entity.LastUpdated = DateTime.Now;
                }
            }
        }

        #endregion

        #region VALIDATION

        public EY.com.eBook.RuleEngine.AY2016.VoorzieningenAppTypes.VoorzieningenT Validate(EY.com.eBook.RuleEngine.AY2016.VoorzieningenAppTypes.VoorzieningenT entity, string location, ref WorksheetSetData worksheetset)
        {
            entity.IsValid = true;
      
            if (!entity.Bedrag.HasValue || entity.Bedrag.Value==0)
            {
            worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Grootboek");
            entity.IsValid = false;
            }
            

            return entity;
        }

        public EY.com.eBook.RuleEngine.AY2016.VoorzieningenAppTypes.VoorzieningenW Validate(EY.com.eBook.RuleEngine.AY2016.VoorzieningenAppTypes.VoorzieningenW entity, string location, ref WorksheetSetData worksheetset)
        {
            entity.IsValid = true;
            worksheetset.ClearMessages(TypeId, entity.Id);
   
            if (!entity.Bedrag.HasValue || entity.Bedrag.Value==0)
            {
            worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Grootboek");
            entity.IsValid = false;
            }
            

            return entity;
        }

        public EY.com.eBook.RuleEngine.AY2016.VoorzieningenAppTypes.VoorzieningenN Validate(EY.com.eBook.RuleEngine.AY2016.VoorzieningenAppTypes.VoorzieningenN entity, string location, ref WorksheetSetData worksheetset)
        {
            entity.IsValid = true;
            worksheetset.ClearMessages(TypeId, entity.Id);


            return entity;
        }

        public EY.com.eBook.RuleEngine.AY2016.VoorzieningenAppTypes.VoorzieningenH Validate(EY.com.eBook.RuleEngine.AY2016.VoorzieningenAppTypes.VoorzieningenH entity, string location, ref WorksheetSetData worksheetset)
        {
            entity.IsValid = true;
            worksheetset.ClearMessages(TypeId, entity.Id);
            

            return entity;
        }

        #endregion
        
        // VALIDATION
        
        // CALCULATION
        
        // BOOKINGS
        
        // IMPORT PREVIOUS


    }
}
  */