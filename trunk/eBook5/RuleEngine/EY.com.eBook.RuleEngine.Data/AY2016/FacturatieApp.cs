
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.RuleEngine.Core;
using EY.com.eBook.RuleEngine.Core.Interfaces;
using EY.com.eBook.RuleEngine.Data;
using EY.com.eBook.Core;
using System.Xml.Linq;
using System.Xml.Xsl;
using System.Globalization;
using System.IO;
using System.Xml.XPath;
using System.Xml;

namespace EY.com.eBook.RuleEngine.AY2016
{
    
    public class FacturatieApp
    {
        public static WorksheetTypeSettings Settings = new WorksheetTypeSettings
                                                      {
                                                        Clients = true
                                                        ,
                                                        Suppliers = true
                                                        ,
                                                        AllAccounts = true
                                                        , 
                                                        PerformsBookings=true
                                                      };
                                                      
        public static Guid TypeId = new Guid("699d94dc-6739-4fda-8941-5926521761b9");

        public List<string> ImportablePreviousTypes  = new List<string>() {  "EY.com.eBook.RuleEngine.AY2015.FacturatieApp"  };

        public Guid GetMyType() { return TypeId; }

        public void SetMinimal()
        {
            if (Data == null)
            {
                Data = new EY.com.eBook.RuleEngine.AY2016.FacturatieAppTypes.Facturatie();
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.OpTeMakenFacturen == null)
            {
                Data.OpTeMakenFacturen = new List<EY.com.eBook.RuleEngine.AY2016.FacturatieAppTypes.FacturatieKlant>(); ;
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.TeOntvangenCN == null)
            {
                Data.TeOntvangenCN = new List<EY.com.eBook.RuleEngine.AY2016.FacturatieAppTypes.FacturatieLeverancierCN>(); ;
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.TeOntvangenFacturen == null)
            {
                Data.TeOntvangenFacturen = new List<EY.com.eBook.RuleEngine.AY2016.FacturatieAppTypes.FacturatieLeverancier>(); ;
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.OpTeMakenCN == null)
            {
                Data.OpTeMakenCN = new List<EY.com.eBook.RuleEngine.AY2016.FacturatieAppTypes.FacturatieKlantCN>(); ;
                Data.LastUpdated = DateTime.Now;
            }

        }
        
        public string ToXml() 
        {
            return CoreHelper.SerializeToString(Data);
        }

        public void LoadXml(string xml)
        {
            if (!string.IsNullOrEmpty(xml)) {
                Data = CoreHelper.DeserializeFromString<EY.com.eBook.RuleEngine.AY2016.FacturatieAppTypes.Facturatie>(xml);
            }
            SetMinimal();
            
        }
        
        public EY.com.eBook.RuleEngine.AY2016.FacturatieAppTypes.Facturatie Data { get; set; }


        public void ForceRecalculation(ref WorksheetSetData worksheetset)
        {
            Data.LastUpdated = new DateTime(1980, 1, 1);
            Data.OpTeMakenFacturen.ForEach(e => e.LastUpdated = null);
            Data.OpTeMakenCN.ForEach(e => e.LastUpdated = null);
            Data.TeOntvangenFacturen.ForEach(e => e.LastUpdated = null);
            Data.TeOntvangenCN.ForEach(e => e.LastUpdated = null);

            Calculate(ref worksheetset);
        }

        public void Calculate(ref WorksheetSetData worksheetset)
        {
            foreach (FacturatieAppTypes.FacturatieKlant entity in Data.OpTeMakenFacturen.Where(e => !e.LastUpdated.HasValue).ToList())
            {
                UpdateOpTeMakenFacturen(entity, ref worksheetset);
            }
            foreach (FacturatieAppTypes.FacturatieKlantCN entity in Data.OpTeMakenCN.Where(e => !e.LastUpdated.HasValue).ToList())
            {
                UpdateOpTeMakenCN(entity, ref worksheetset);
            }

            foreach (FacturatieAppTypes.FacturatieLeverancier entity in Data.TeOntvangenFacturen.Where(e => !e.LastUpdated.HasValue).ToList())
            {
                UpdateTeOntvangenFacturen(entity, ref worksheetset);
            }

            foreach (FacturatieAppTypes.FacturatieLeverancierCN entity in Data.TeOntvangenCN.Where(e => !e.LastUpdated.HasValue).ToList())
            {
                UpdateTeOntvangenCN(entity, ref worksheetset);
            }


            DateTime lastCalc = DateTime.Now;


            Data.LastUpdated = lastCalc;
        }

        public void AddToOpTeMakenCN(EY.com.eBook.RuleEngine.AY2016.FacturatieAppTypes.FacturatieKlantCN entity,ref WorksheetSetData worksheetset)
        {
            entity = Validate(entity, "OpTeMakenCN", ref worksheetset);
            if (entity.IsValid)
            {
                Data.OpTeMakenCN.Add(entity);
                if (string.IsNullOrEmpty(entity.Omschrijving))
                {
                    entity.Omschrijving = string.Format("{0} {1}", entity.Klant.NL, entity.FactuurNr);
                }
                if (entity.Boeken.HasValue && entity.Boeken.Value)
                {
                    worksheetset.AddBooking(FacturatieApp.TypeId, "OpTeMakenCN",entity.Id,null, new List<BookingLineDataContract>() {
                        new BookingLineDataContract { AccountNr = entity.Grootboek, Amount=entity.Bedrag.Value }
                            , new BookingLineDataContract { AccountNr = worksheetset.GetListAccount("4441"), Amount=0-entity.Bedrag.Value}
                    });
                }
                Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
            }
        }

        public void UpdateOpTeMakenCN(EY.com.eBook.RuleEngine.AY2016.FacturatieAppTypes.FacturatieKlantCN entity, ref WorksheetSetData worksheetset)
        {
            entity = Validate(entity, "OpTeMakenCN", ref worksheetset);
            EY.com.eBook.RuleEngine.AY2016.FacturatieAppTypes.FacturatieKlantCN original = Data.OpTeMakenCN.FirstOrDefault(e => e.Id == entity.Id);
            if (original == null)
            {
                AddToOpTeMakenCN(entity, ref worksheetset);
            }
            else
            {
                if ((entity != original || !entity.LastUpdated.HasValue) && entity.IsValid)
                {
                    if (string.IsNullOrEmpty(entity.Omschrijving))
                    {
                        entity.Omschrijving = string.Format("{0} {1}", entity.Klant.NL, entity.FactuurNr);
                    }
                    entity.Boeken = entity.Boeken.HasValue ? entity.Boeken.Value : false;
                    original.Boeken = original.Boeken.HasValue ? original.Boeken.Value : false;

                    if (original.Boeken.Value && !entity.Boeken.Value)
                    {
                        worksheetset.DeleteBooking(FacturatieApp.TypeId, "OpTeMakenCN", original.Id, null);
                    }
                    else if (!original.Boeken.Value && entity.Boeken.Value)
                    {
                        worksheetset.AddBooking(FacturatieApp.TypeId, "OpTeMakenCN", entity.Id, null, new List<BookingLineDataContract>() {
                            new BookingLineDataContract { AccountNr = entity.Grootboek, Amount=entity.Bedrag.Value }
                            , new BookingLineDataContract { AccountNr = worksheetset.GetListAccount("4441"), Amount=0-entity.Bedrag.Value}
                        });
                    }
                    else if (original.Boeken.Value && entity.Boeken.Value && (original.Bedrag != entity.Bedrag || original.Grootboek != entity.Grootboek))
                    {
                        worksheetset.UpdateBooking(FacturatieApp.TypeId, "OpTeMakenCN", entity.Id, null, new List<BookingLineDataContract>() {
                            new BookingLineDataContract { AccountNr = entity.Grootboek, Amount=entity.Bedrag.Value }
                            , new BookingLineDataContract { AccountNr = worksheetset.GetListAccount("4441"), Amount=0-entity.Bedrag.Value}
                        });
                    }
                    else if (!entity.LastUpdated.HasValue && entity.Boeken.Value)
                    {
                        worksheetset.UpdateBooking(FacturatieApp.TypeId, "OpTeMakenCN", entity.Id, null, new List<BookingLineDataContract>() {
                            new BookingLineDataContract { AccountNr = entity.Grootboek, Amount=entity.Bedrag.Value }
                            , new BookingLineDataContract { AccountNr = worksheetset.GetListAccount("4441"), Amount=0-entity.Bedrag.Value}
                        });
                    }
                    int idx = Data.OpTeMakenCN.IndexOf(original);
                    Data.OpTeMakenCN.RemoveAt(idx);
                    Data.OpTeMakenCN.Insert(idx, entity);
                    Data.LastUpdated = DateTime.Now;
                    entity.LastUpdated = DateTime.Now;
                }

            }
           
        }

        public void DeleteOpTeMakenCN(Guid rowId, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2016.FacturatieAppTypes.FacturatieKlantCN original = Data.OpTeMakenCN.FirstOrDefault(e => e.Id == rowId);
            if (original != null)
            {
                if (original.Boeken.HasValue && original.Boeken.Value)
                {
                    worksheetset.DeleteBooking(FacturatieApp.TypeId, "OpTeMakenCN", original.Id, null);
                    // DELETE BOOKING
                }
                Data.OpTeMakenCN.Remove(original);
                Data.LastUpdated = DateTime.Now;
            }

        }

        public void AddToOpTeMakenFacturen(EY.com.eBook.RuleEngine.AY2016.FacturatieAppTypes.FacturatieKlant entity, ref WorksheetSetData worksheetset)
        {
            entity=Validate(entity, "OpTeMakenFacturen", ref worksheetset);
            if (entity.IsValid)
            {
                if (string.IsNullOrEmpty(entity.Omschrijving))
                {
                    entity.Omschrijving = string.Format("{0} {1}", entity.Klant.NL, entity.FactuurNr);
                }
                Data.OpTeMakenFacturen.Add(entity);
                if (entity.Boeken.HasValue && entity.Boeken.Value)
                {
                    worksheetset.AddBooking(FacturatieApp.TypeId, "OpTeMakenFacturen", entity.Id, null, new List<BookingLineDataContract>() {
                        new BookingLineDataContract { AccountNr = entity.Grootboek, Amount=0-entity.Bedrag.Value }
                            , new BookingLineDataContract { AccountNr = worksheetset.GetListAccount("404"), Amount=entity.Bedrag.Value}
                    });
                }
                Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
            }
        }

        public void UpdateOpTeMakenFacturen(EY.com.eBook.RuleEngine.AY2016.FacturatieAppTypes.FacturatieKlant entity, ref WorksheetSetData worksheetset)
        {
            entity = Validate(entity, "OpTeMakenFacturen", ref worksheetset);
            EY.com.eBook.RuleEngine.AY2016.FacturatieAppTypes.FacturatieKlant original = Data.OpTeMakenFacturen.FirstOrDefault(e => e.Id == entity.Id);
            if (original == null)
            {
                AddToOpTeMakenFacturen(entity, ref worksheetset);
            }
            else
            {
                if ((entity != original || !entity.LastUpdated.HasValue) && entity.IsValid)
                {
                    if (string.IsNullOrEmpty(entity.Omschrijving))
                    {
                        entity.Omschrijving = string.Format("{0} {1}", entity.Klant.NL, entity.FactuurNr);
                    }
                    entity.Boeken = entity.Boeken.HasValue ? entity.Boeken.Value : false;
                    original.Boeken = original.Boeken.HasValue ? original.Boeken.Value : false;

                    if (original.Boeken.Value && !entity.Boeken.Value)
                    {
                        worksheetset.DeleteBooking(FacturatieApp.TypeId, "OpTeMakenFacturen", original.Id, null);
                    }
                    else if (!original.Boeken.Value && entity.Boeken.Value)
                    {
                        worksheetset.AddBooking(FacturatieApp.TypeId, "OpTeMakenFacturen", entity.Id, null, new List<BookingLineDataContract>() {
                            new BookingLineDataContract { AccountNr = entity.Grootboek, Amount=0-entity.Bedrag.Value }
                            , new BookingLineDataContract { AccountNr = worksheetset.GetListAccount("404"), Amount=entity.Bedrag.Value}
                        });
                    }
                    else if (original.Boeken.Value && entity.Boeken.Value && (original.Bedrag != entity.Bedrag || original.Grootboek != entity.Grootboek))
                    {
                        worksheetset.UpdateBooking(FacturatieApp.TypeId, "OpTeMakenFacturen", entity.Id, null, new List<BookingLineDataContract>() {
                            new BookingLineDataContract { AccountNr = entity.Grootboek, Amount=0-entity.Bedrag.Value }
                            , new BookingLineDataContract { AccountNr = worksheetset.GetListAccount("404"), Amount=entity.Bedrag.Value}
                        });
                    }
                    else if (!entity.LastUpdated.HasValue && entity.Boeken.Value)
                    {
                        worksheetset.UpdateBooking(FacturatieApp.TypeId, "OpTeMakenFacturen", entity.Id, null, new List<BookingLineDataContract>() {
                            new BookingLineDataContract { AccountNr = entity.Grootboek, Amount=0-entity.Bedrag.Value }
                            , new BookingLineDataContract { AccountNr = worksheetset.GetListAccount("404"), Amount=entity.Bedrag.Value}
                        });
                    }
                    int idx = Data.OpTeMakenFacturen.IndexOf(original);
                    Data.OpTeMakenFacturen.RemoveAt(idx);
                    Data.OpTeMakenFacturen.Insert(idx, entity);

                    Data.LastUpdated = DateTime.Now;
                    entity.LastUpdated = DateTime.Now;
                }
            }

        }

        public void DeleteOpTeMakenFacturen(Guid rowId, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2016.FacturatieAppTypes.FacturatieKlant original = Data.OpTeMakenFacturen.FirstOrDefault(e => e.Id == rowId);
            if (original != null)
            {
                if (original.Boeken.HasValue && original.Boeken.Value)
                {
                    worksheetset.DeleteBooking(FacturatieApp.TypeId, "OpTeMakenFacturen", original.Id, null);
                    // DELETE BOOKING
                }
                Data.OpTeMakenFacturen.Remove(original);
                Data.LastUpdated = DateTime.Now;
                
            }

        }

        public void AddToTeOntvangenCN(EY.com.eBook.RuleEngine.AY2016.FacturatieAppTypes.FacturatieLeverancierCN entity, ref WorksheetSetData worksheetset)
        {
            entity=Validate(entity, "TeOntvangenCN", ref worksheetset);
            if (entity.IsValid)
            {
                if (string.IsNullOrEmpty(entity.Omschrijving))
                {
                    entity.Omschrijving = string.Format("{0} {1}", entity.Leverancier.NL, entity.FactuurNr);
                }
                Data.TeOntvangenCN.Add(entity);
                if (entity.Boeken.HasValue && entity.Boeken.Value)
                {
                    worksheetset.AddBooking(FacturatieApp.TypeId, "TeOntvangenCN", entity.Id, null, new List<BookingLineDataContract>() {
                        new BookingLineDataContract { AccountNr = entity.Grootboek, Amount=0-entity.Bedrag.Value }
                            , new BookingLineDataContract { AccountNr = worksheetset.GetListAccount("4041"), Amount=entity.Bedrag.Value}
                    });
                }
                Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
            }
        }

        public void UpdateTeOntvangenCN(EY.com.eBook.RuleEngine.AY2016.FacturatieAppTypes.FacturatieLeverancierCN entity, ref WorksheetSetData worksheetset)
        {
            entity = Validate(entity, "TeOntvangenCN", ref worksheetset);
            EY.com.eBook.RuleEngine.AY2016.FacturatieAppTypes.FacturatieLeverancierCN original = Data.TeOntvangenCN.FirstOrDefault(e => e.Id == entity.Id);
            if (original == null)
            {
                AddToTeOntvangenCN(entity, ref worksheetset);
            }
            else
            {
                if ((entity != original|| !entity.LastUpdated.HasValue) && entity.IsValid)
                {
                    if (string.IsNullOrEmpty(entity.Omschrijving))
                    {
                        entity.Omschrijving = string.Format("{0} {1}", entity.Leverancier.NL, entity.FactuurNr);
                    }
                    entity.Boeken = entity.Boeken.HasValue ? entity.Boeken.Value : false;
                    original.Boeken = original.Boeken.HasValue ? original.Boeken.Value : false;

                    if (original.Boeken.Value && !entity.Boeken.Value)
                    {
                        worksheetset.DeleteBooking(FacturatieApp.TypeId, "TeOntvangenCN", original.Id, null);
                    }
                    else if (!original.Boeken.Value && entity.Boeken.Value)
                    {
                        worksheetset.AddBooking(FacturatieApp.TypeId, "TeOntvangenCN", entity.Id, null, new List<BookingLineDataContract>() {
                            new BookingLineDataContract { AccountNr = entity.Grootboek, Amount=0-entity.Bedrag.Value }
                            , new BookingLineDataContract { AccountNr = worksheetset.GetListAccount("4041"), Amount=entity.Bedrag.Value}
                        });
                    }
                    else if (original.Boeken.Value && entity.Boeken.Value && (original.Bedrag != entity.Bedrag || original.Grootboek != entity.Grootboek))
                    {
                        worksheetset.UpdateBooking(FacturatieApp.TypeId, "TeOntvangenCN", entity.Id, null, new List<BookingLineDataContract>() {
                            new BookingLineDataContract { AccountNr = entity.Grootboek, Amount=0-entity.Bedrag.Value }
                            , new BookingLineDataContract { AccountNr = worksheetset.GetListAccount("4041"), Amount=entity.Bedrag.Value}
                        });
                    }
                    else if (!entity.LastUpdated.HasValue && entity.Boeken.Value)
                    {
                        worksheetset.UpdateBooking(FacturatieApp.TypeId, "TeOntvangenCN", entity.Id, null, new List<BookingLineDataContract>() {
                            new BookingLineDataContract { AccountNr = entity.Grootboek, Amount=0-entity.Bedrag.Value }
                            , new BookingLineDataContract { AccountNr = worksheetset.GetListAccount("4041"), Amount=entity.Bedrag.Value}
                        });
                    }
                    int idx = Data.TeOntvangenCN.IndexOf(original);
                    Data.TeOntvangenCN.RemoveAt(idx);
                    Data.TeOntvangenCN.Insert(idx, entity);
                    Data.LastUpdated = DateTime.Now;
                    entity.LastUpdated = DateTime.Now;
                }
            }

        }

        public void DeleteTeOntvangenCN(Guid rowId, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2016.FacturatieAppTypes.FacturatieLeverancierCN original = Data.TeOntvangenCN.FirstOrDefault(e => e.Id == rowId);
            if (original != null)
            {
                if (original.Boeken.HasValue && original.Boeken.Value)
                {
                    worksheetset.DeleteBooking(FacturatieApp.TypeId, "TeOntvangenCN", original.Id, null);
                    // DELETE BOOKING
                }
                Data.TeOntvangenCN.Remove(original);
                Data.LastUpdated = DateTime.Now;
            }
        }


        public void AddToTeOntvangenFacturen(EY.com.eBook.RuleEngine.AY2016.FacturatieAppTypes.FacturatieLeverancier entity, ref WorksheetSetData worksheetset)
        {
            entity=Validate(entity, "TeOntvangenFacturen", ref worksheetset);
            if (entity.IsValid)
            {
                if (string.IsNullOrEmpty(entity.Omschrijving))
                {
                    entity.Omschrijving = string.Format("{0} {1}", entity.Leverancier.NL, entity.FactuurNr);
                }
                Data.TeOntvangenFacturen.Add(entity);
                if (entity.Boeken.HasValue && entity.Boeken.Value)
                {
                    worksheetset.AddBooking(FacturatieApp.TypeId, "TeOntvangenFacturen", entity.Id, null, new List<BookingLineDataContract>() {
                        new BookingLineDataContract { AccountNr = entity.Grootboek, Amount=entity.Bedrag.Value }
                            , new BookingLineDataContract { AccountNr = worksheetset.GetListAccount("444"), Amount=0-entity.Bedrag.Value}
                    });
                }
                Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
            }
        }

        public void UpdateTeOntvangenFacturen(EY.com.eBook.RuleEngine.AY2016.FacturatieAppTypes.FacturatieLeverancier entity, ref WorksheetSetData worksheetset)
        {
            entity = Validate(entity, "TeOntvangenFacturen", ref worksheetset);
            EY.com.eBook.RuleEngine.AY2016.FacturatieAppTypes.FacturatieLeverancier original = Data.TeOntvangenFacturen.FirstOrDefault(e => e.Id == entity.Id);
            if (original == null)
            {
                AddToTeOntvangenFacturen(entity, ref worksheetset);
            }
            else
            {
                if ((entity != original || !entity.LastUpdated.HasValue) && entity.IsValid)
                {
                    if (string.IsNullOrEmpty(entity.Omschrijving))
                    {
                        entity.Omschrijving = string.Format("{0} {1}", entity.Leverancier.NL, entity.FactuurNr);
                    }
                    entity.Boeken = entity.Boeken.HasValue ? entity.Boeken.Value : false;
                    original.Boeken = original.Boeken.HasValue ? original.Boeken.Value : false;

                    if (original.Boeken.Value && !entity.Boeken.Value)
                    {
                        worksheetset.DeleteBooking(FacturatieApp.TypeId, "TeOntvangenFacturen", original.Id, null);
                    }
                    else if (!original.Boeken.Value && entity.Boeken.Value)
                    {
                        worksheetset.AddBooking(FacturatieApp.TypeId, "TeOntvangenFacturen", entity.Id, null, new List<BookingLineDataContract>() {
                            new BookingLineDataContract { AccountNr = entity.Grootboek, Amount=entity.Bedrag.Value }
                            , new BookingLineDataContract { AccountNr = worksheetset.GetListAccount("444"), Amount=0-entity.Bedrag.Value}
                        });
                    }
                    else if (original.Boeken.Value && entity.Boeken.Value && (original.Bedrag != entity.Bedrag || original.Grootboek != entity.Grootboek))
                    {
                        worksheetset.UpdateBooking(FacturatieApp.TypeId, "TeOntvangenFacturen", entity.Id, null, new List<BookingLineDataContract>() {
                            new BookingLineDataContract { AccountNr = entity.Grootboek, Amount=entity.Bedrag.Value }
                            , new BookingLineDataContract { AccountNr = worksheetset.GetListAccount("444"), Amount=0-entity.Bedrag.Value}
                        });
                    }
                    else if (!entity.LastUpdated.HasValue && entity.Boeken.Value)
                    {
                        worksheetset.UpdateBooking(FacturatieApp.TypeId, "TeOntvangenFacturen", entity.Id, null, new List<BookingLineDataContract>() {
                            new BookingLineDataContract { AccountNr = entity.Grootboek, Amount=entity.Bedrag.Value }
                            , new BookingLineDataContract { AccountNr = worksheetset.GetListAccount("444"), Amount=0-entity.Bedrag.Value}
                        });
                    }
                }
                int idx = Data.TeOntvangenFacturen.IndexOf(original);
                Data.TeOntvangenFacturen.RemoveAt(idx);
                Data.TeOntvangenFacturen.Insert(idx, entity);

                Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
            }

        }

        public void DeleteTeOntvangenFacturen(Guid rowId, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2016.FacturatieAppTypes.FacturatieLeverancier original = Data.TeOntvangenFacturen.FirstOrDefault(e => e.Id == rowId);
            if (original != null)
            {
                if (original.Boeken.HasValue && original.Boeken.Value)
                {
                    worksheetset.DeleteBooking(FacturatieApp.TypeId, "TeOntvangenFacturen", original.Id, null);
                    // DELETE BOOKING
                }
                Data.TeOntvangenFacturen.Remove(original);
                Data.LastUpdated = DateTime.Now;
            }
        }

        #region worksheet type validation


        public EY.com.eBook.RuleEngine.AY2016.FacturatieAppTypes.FacturatieKlant Validate(EY.com.eBook.RuleEngine.AY2016.FacturatieAppTypes.FacturatieKlant entity, string location, ref WorksheetSetData worksheetset)
        {
            worksheetset.ClearMessages(TypeId, entity.Id);
            entity.IsValid = true;
            if (entity.Grootboek == null)
            {
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Grootboek");
                entity.IsValid = false;
            }

            if (entity.Klant == null)
            {
                //add message collection, field:"Grootboek", required
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Klant");
                entity.IsValid = false;
            }

            if (entity.FacJaar == null)
            {
                //add message collection, field:"Grootboek", required
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "FacJaar");
                entity.IsValid = false;
            }

            if (!entity.Bedrag.HasValue || entity.Bedrag.Value == 0)
            {
                //add message collection, field:"Grootboek", required
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Bedrag");
                entity.IsValid = false;
            }
            return entity;
        }

        public EY.com.eBook.RuleEngine.AY2016.FacturatieAppTypes.FacturatieKlantCN Validate(EY.com.eBook.RuleEngine.AY2016.FacturatieAppTypes.FacturatieKlantCN entity, string location, ref WorksheetSetData worksheetset)
        {
            entity.IsValid = true;
            worksheetset.ClearMessages(TypeId, entity.Id);
            if (entity.Grootboek == null)
            {
                //add message collection, field:"Grootboek", required
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Grootboek");
                entity.IsValid = false;
            }

            if (entity.Klant == null)
            {
                //add message collection, field:"Grootboek", required
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Klant");
                entity.IsValid = false;
            }

            if (entity.FacJaar == null)
            {
                //add message collection, field:"Grootboek", required
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "FacJaar");
                entity.IsValid = false;
            }

            if (!entity.Bedrag.HasValue || entity.Bedrag.Value == 0)
            {
                //add message collection, field:"Grootboek", required
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Bedrag");
                entity.IsValid = false;
            }
            return entity;
        }


        public EY.com.eBook.RuleEngine.AY2016.FacturatieAppTypes.FacturatieLeverancier Validate(EY.com.eBook.RuleEngine.AY2016.FacturatieAppTypes.FacturatieLeverancier entity, string location, ref WorksheetSetData worksheetset)
        {
            entity.IsValid = true;
            worksheetset.ClearMessages(TypeId, entity.Id);
            if (entity.Grootboek == null)
            {
                //add message collection, field:"Grootboek", required
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Grootboek");
                entity.IsValid = false;
            }

            if (entity.Leverancier == null)
            {
                //add message collection, field:"Grootboek", required
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Leverancier");
                entity.IsValid = false;
            }

            if (entity.FacJaar == null)
            {
                //add message collection, field:"Grootboek", required
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "FacJaar");
                entity.IsValid = false;
            }

            if (!entity.Bedrag.HasValue || entity.Bedrag.Value == 0)
            {
                //add message collection, field:"Grootboek", required
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Bedrag");
                entity.IsValid = false;
            }
            return entity;
        }

        public EY.com.eBook.RuleEngine.AY2016.FacturatieAppTypes.FacturatieLeverancierCN Validate(EY.com.eBook.RuleEngine.AY2016.FacturatieAppTypes.FacturatieLeverancierCN entity, string location, ref WorksheetSetData worksheetset)
        {
            entity.IsValid = true;
            worksheetset.ClearMessages(TypeId, entity.Id);
            if (entity.Grootboek == null)
            {
                //add message collection, field:"Grootboek", required
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Grootboek");
                entity.IsValid = false;
            }

            if (entity.Leverancier == null)
            {
                //add message collection, field:"Grootboek", required
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Leverancier");
                entity.IsValid = false;
            }

            if (entity.FacJaar == null)
            {
                //add message collection, field:"Grootboek", required
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "FacJaar");
                entity.IsValid = false;
            }

            if (!entity.Bedrag.HasValue || entity.Bedrag.Value == 0)
            {
                //add message collection, field:"Grootboek", required
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Bedrag");
                entity.IsValid = false;
            }
            return entity;

        }

        #endregion


        // VALIDATION
        
        // CALCULATION
        
        // BOOKINGS
        public void UpdateBookings()
        {
        }


        #region worksheet type import previous

        public void ImportPrevious(ref WorksheetSetData wsd)
        {
            if (wsd.PreviousSet.AssessmentYear == 2015)
            {
                EY.com.eBook.RuleEngine.AY2015.WorksheetSetData pset = (EY.com.eBook.RuleEngine.AY2015.WorksheetSetData)wsd.PreviousSet;

                EY.com.eBook.RuleEngine.AY2015.FacturatieApp previous = pset.FacturatieApp;
                foreach (EY.com.eBook.RuleEngine.AY2015.FacturatieAppTypes.FacturatieKlantCN klant
                            in previous.Data.OpTeMakenCN.Where(e => e.Boeken.HasValue && e.Boeken.Value))
                {
                    EY.com.eBook.RuleEngine.AY2016.FacturatieAppTypes.FacturatieKlantCN newEnt = new EY.com.eBook.RuleEngine.AY2016.FacturatieAppTypes.FacturatieKlantCN
                    {
                        Id = klant.Id
                        ,
                        Bedrag = 0 - klant.Bedrag
                        ,
                        Boeken = true
                        ,
                        FacJaar = klant.FacJaar
                        ,
                        FactuurNr = klant.FactuurNr
                        ,
                        Klant = klant.Klant
                        ,
                        Grootboek = klant.Grootboek
                        ,
                        PreviousImported = true
                        ,
                        LastUpdated = new DateTime(1980, 1, 1)
                    };
                    Data.OpTeMakenCN.Add(newEnt);
                }

                foreach (EY.com.eBook.RuleEngine.AY2015.FacturatieAppTypes.FacturatieKlant klant
                            in previous.Data.OpTeMakenFacturen.Where(e => e.Boeken.HasValue && e.Boeken.Value))
                {
                    EY.com.eBook.RuleEngine.AY2016.FacturatieAppTypes.FacturatieKlant newEnt = new EY.com.eBook.RuleEngine.AY2016.FacturatieAppTypes.FacturatieKlant
                    {
                        Id = klant.Id
                        ,
                        Bedrag = 0 - klant.Bedrag
                        ,
                        Boeken = true
                        ,
                        FacJaar = klant.FacJaar
                        ,
                        FactuurNr = klant.FactuurNr
                        ,
                        Klant = klant.Klant
                        ,
                        Grootboek = klant.Grootboek
                        ,
                        PreviousImported = true
                        ,
                        LastUpdated = new DateTime(1980, 1, 1)
                    };
                    Data.OpTeMakenFacturen.Add(newEnt);
                }

                foreach (EY.com.eBook.RuleEngine.AY2015.FacturatieAppTypes.FacturatieLeverancierCN klant
                            in previous.Data.TeOntvangenCN.Where(e => e.Boeken.HasValue && e.Boeken.Value))
                {
                    EY.com.eBook.RuleEngine.AY2016.FacturatieAppTypes.FacturatieLeverancierCN newEnt = new EY.com.eBook.RuleEngine.AY2016.FacturatieAppTypes.FacturatieLeverancierCN
                    {
                        Id = klant.Id
                        ,
                        Bedrag = 0 - klant.Bedrag
                        ,
                        Boeken = true
                        ,
                        FacJaar = klant.FacJaar
                        ,
                        FactuurNr = klant.FactuurNr
                        ,
                        Leverancier = klant.Leverancier
                        ,
                        Grootboek = klant.Grootboek
                        ,
                        PreviousImported = true
                        ,
                        LastUpdated = new DateTime(1980, 1, 1)
                    };
                    Data.TeOntvangenCN.Add(newEnt);
                }

                foreach (EY.com.eBook.RuleEngine.AY2015.FacturatieAppTypes.FacturatieLeverancier klant
                            in previous.Data.TeOntvangenFacturen.Where(e => e.Boeken.HasValue && e.Boeken.Value))
                {
                    EY.com.eBook.RuleEngine.AY2016.FacturatieAppTypes.FacturatieLeverancier newEnt = new EY.com.eBook.RuleEngine.AY2016.FacturatieAppTypes.FacturatieLeverancier
                    {
                        Id = klant.Id
                        ,
                        Bedrag = 0 - klant.Bedrag
                        ,
                        Boeken = true
                        ,
                        FacJaar = klant.FacJaar
                        ,
                        FactuurNr = klant.FactuurNr
                        ,
                        Leverancier = klant.Leverancier
                        ,
                        Grootboek = klant.Grootboek
                        ,
                        PreviousImported = true
                        ,
                        LastUpdated = new DateTime(1980, 1, 1)
                    };
                    Data.TeOntvangenFacturen.Add(newEnt);
                }
            }

            if (wsd.PreviousSet.AssessmentYear == 2015)
            {
                EY.com.eBook.RuleEngine.AY2015.WorksheetSetData pset = (EY.com.eBook.RuleEngine.AY2015.WorksheetSetData)wsd.PreviousSet;

                EY.com.eBook.RuleEngine.AY2015.FacturatieApp previous = pset.FacturatieApp;
                foreach (EY.com.eBook.RuleEngine.AY2015.FacturatieAppTypes.FacturatieKlantCN klant
                            in previous.Data.OpTeMakenCN.Where(e => e.Boeken.HasValue && e.Boeken.Value))
                {
                    EY.com.eBook.RuleEngine.AY2016.FacturatieAppTypes.FacturatieKlantCN newEnt = new EY.com.eBook.RuleEngine.AY2016.FacturatieAppTypes.FacturatieKlantCN
                    {
                        Id = klant.Id
                        ,
                        Bedrag = 0 - klant.Bedrag
                        ,
                        Boeken = true
                        ,
                        FacJaar = klant.FacJaar
                        ,
                        FactuurNr = klant.FactuurNr
                        ,
                        Klant = klant.Klant
                        ,
                        Grootboek = klant.Grootboek
                        ,
                        PreviousImported = true
                        ,
                        LastUpdated = new DateTime(1980, 1, 1)
                    };
                    Data.OpTeMakenCN.Add(newEnt);
                }

                foreach (EY.com.eBook.RuleEngine.AY2015.FacturatieAppTypes.FacturatieKlant klant
                            in previous.Data.OpTeMakenFacturen.Where(e => e.Boeken.HasValue && e.Boeken.Value))
                {
                    EY.com.eBook.RuleEngine.AY2016.FacturatieAppTypes.FacturatieKlant newEnt = new EY.com.eBook.RuleEngine.AY2016.FacturatieAppTypes.FacturatieKlant
                    {
                        Id = klant.Id
                        ,
                        Bedrag = 0 - klant.Bedrag
                        ,
                        Boeken = true
                        ,
                        FacJaar = klant.FacJaar
                        ,
                        FactuurNr = klant.FactuurNr
                        ,
                        Klant = klant.Klant
                        ,
                        Grootboek = klant.Grootboek
                        ,
                        PreviousImported = true
                        ,
                        LastUpdated = new DateTime(1980, 1, 1)
                    };
                    Data.OpTeMakenFacturen.Add(newEnt);
                }

                foreach (EY.com.eBook.RuleEngine.AY2015.FacturatieAppTypes.FacturatieLeverancierCN klant
                            in previous.Data.TeOntvangenCN.Where(e => e.Boeken.HasValue && e.Boeken.Value))
                {
                    EY.com.eBook.RuleEngine.AY2016.FacturatieAppTypes.FacturatieLeverancierCN newEnt = new EY.com.eBook.RuleEngine.AY2016.FacturatieAppTypes.FacturatieLeverancierCN
                    {
                        Id = klant.Id
                        ,
                        Bedrag = 0 - klant.Bedrag
                        ,
                        Boeken = true
                        ,
                        FacJaar = klant.FacJaar
                        ,
                        FactuurNr = klant.FactuurNr
                        ,
                        Leverancier = klant.Leverancier
                        ,
                        Grootboek = klant.Grootboek
                        ,
                        PreviousImported = true
                        ,
                        LastUpdated = new DateTime(1980, 1, 1)
                    };
                    Data.TeOntvangenCN.Add(newEnt);
                }

                foreach (EY.com.eBook.RuleEngine.AY2015.FacturatieAppTypes.FacturatieLeverancier klant
                            in previous.Data.TeOntvangenFacturen.Where(e => e.Boeken.HasValue && e.Boeken.Value))
                {
                    EY.com.eBook.RuleEngine.AY2016.FacturatieAppTypes.FacturatieLeverancier newEnt = new EY.com.eBook.RuleEngine.AY2016.FacturatieAppTypes.FacturatieLeverancier
                    {
                        Id = klant.Id
                        ,
                        Bedrag = 0 - klant.Bedrag
                        ,
                        Boeken = true
                        ,
                        FacJaar = klant.FacJaar
                        ,
                        FactuurNr = klant.FactuurNr
                        ,
                        Leverancier = klant.Leverancier
                        ,
                        Grootboek = klant.Grootboek
                        ,
                        PreviousImported = true
                        ,
                        LastUpdated = new DateTime(1980, 1, 1)
                    };
                    Data.TeOntvangenFacturen.Add(newEnt);
                }
            }
        }

        /*
        public void ImportPrevious(EY.com.eBook.RuleEngine.AY2010.FacturatieApp previous)
        {
            foreach (EY.com.eBook.RuleEngine.AY2010.FacturatieAppTypes.FacturatieKlant klant 
                        in previous.Data.OpTeMakenCN.Where(e=>e.BookReversed.HasValue && e.BookReversed.Value))
            {
                EY.com.eBook.RuleEngine.AY2016.FacturatieAppTypes.FacturatieKlant newEnt = new EY.com.eBook.RuleEngine.AY2016.FacturatieAppTypes.FacturatieKlant
                {
                    Id = klant.Id
                    , Bedrag = 0-klant.Bedrag
                    , Boeken =true
                    , FacJaar = klant.FacJaar
                    , FactuurNr = klant.FactuurNr
                    , Klant = klant.Klant
                    , Grootboek = klant.Grootboek
                    , PreviousImported=true
                    ,LastUpdated = DateTime.Now
                };
                Data.OpTeMakenCN.Add(newEnt);
            }

            foreach (EY.com.eBook.RuleEngine.AY2010.FacturatieApp.FacturatieKlant klant 
                        in previous.Data.OpTeMakenFacturen.Where(e=>e.BookReversed.HasValue && e.BookReversed.Value))
            {
                EY.com.eBook.RuleEngine.AY2016.FacturatieAppTypes.FacturatieKlant newEnt = new EY.com.eBook.RuleEngine.AY2016.FacturatieAppTypes.FacturatieKlant
                {
                    Id = klant.Id
                    , Bedrag = 0-klant.Bedrag
                    , Boeken =true
                    , FacJaar = klant.FacJaar
                    , FactuurNr = klant.FactuurNr
                    , Klant = klant.Klant
                    , Grootboek = klant.Grootboek
                    , PreviousImported=true
                    , BookReversed =false
                    ,LastUpdated = DateTime.Now
                };
                Data.OpTeMakenFacturen.Add(newEnt);
            }

            foreach (EY.com.eBook.RuleEngine.AY2010.FacturatieApp.FacturatieLeverancierCN klant 
                        in previous.Data.TeOntvangenCN.Where(e=>e.BookReversed.HasValue && e.BookReversed.Value))
            {
                EY.com.eBook.RuleEngine.AY2016.FacturatieAppTypes.FacturatieLeverancierCN newEnt = new EY.com.eBook.RuleEngine.AY2016.FacturatieAppTypes.FacturatieLeverancierCN
                {
                    Id = klant.Id
                    , Bedrag = 0-klant.Bedrag
                    , Boeken =true
                    , FacJaar = klant.FacJaar
                    , FactuurNr = klant.FactuurNr
                    , Leverancier = klant.Leverancier
                    , Grootboek = klant.Grootboek
                    , PreviousImported=true
                    , BookReversed =false
                    ,LastUpdated = DateTime.Now
                };
                Data.TeOntvangenCN.Add(newEnt);
            }

            foreach (EY.com.eBook.RuleEngine.AY2010.FacturatieApp.FacturatieLeverancier klant 
                        in previous.Data.TeOntvangenFacturen.Where(e=>e.BookReversed.HasValue && e.BookReversed.Value))
            {
                EY.com.eBook.RuleEngine.AY2016.FacturatieAppTypes.FacturatieLeverancier newEnt = new EY.com.eBook.RuleEngine.AY2016.FacturatieAppTypes.FacturatieLeverancier
                {
                    Id = klant.Id
                    , Bedrag = 0-klant.Bedrag
                    , Boeken =true
                    , FacJaar = klant.FacJaar
                    , FactuurNr = klant.FactuurNr
                    , Leverancier = klant.Leverancier
                    , Grootboek = klant.Grootboek
                    , PreviousImported=true
                    , BookReversed =false
                    ,LastUpdated = DateTime.Now
                };
                Data.TeOntvangenFacturen.Add(newEnt);
            }
        }
        */
        #endregion

        public XElement GetPdfTemplate(string printLayout, string culture, ref WorksheetSetData worksheetset)
        {

            XElement source = XElement.Parse(this.ToXml()).StripNamespaces();
           
            string xsltPath = Config.GetWorksheetTemplate("FacturatieApp",worksheetset.DataContract.AssessmentYear,printLayout);

            if (string.IsNullOrEmpty(xsltPath))
            {
                return new XElement("Root", new XElement("Paragraph", string.Format("XSLT - PRINTLAYOUT NOT FOUND, CONTACT SUPPORT WITH CLIENT AND EBOOK FILE NAME AS WELL AS 'WORKSHEET:{0} - LAYOUT:{1} - ASSESSMENTYEAR:{2}'", "FacturatieApp", printLayout, worksheetset.DataContract.AssessmentYear.ToString())));
            }

            XslCompiledTransform xslTransformer = new XslCompiledTransform(Config.DebugXslt());

            XsltSettings settings = new XsltSettings
            {
                EnableDocumentFunction = true
                ,
                EnableScript = false
            };

            xslTransformer.Load(xsltPath, settings, null);

            CultureInfo cObj = CultureInfo.CreateSpecificCulture(culture);

            XsltHelperClass xhc = new XsltHelperClass { Culture = cObj };

          
            XsltArgumentList arguments = new XsltArgumentList();
            arguments.AddExtensionObject("urn:Helper", xhc);
            arguments.AddExtensionObject("urn:Set", worksheetset);
            arguments.AddExtensionObject("urn:File", worksheetset.DataContract);
            arguments.AddExtensionObject("urn:Client", worksheetset.GetClient());
            //arguments.AddParam("RuleAppDef", "", xdoc.CreateNavigator());

            
            StringWriter stringWriter = new StringWriter();
            XmlWriterSettings wSettings = new XmlWriterSettings();
            wSettings.NewLineHandling = NewLineHandling.None;
            wSettings.NewLineOnAttributes = false;
            XmlWriter xmlTextWriter = XmlTextWriter.Create(stringWriter, wSettings);

            xslTransformer.Transform(source.CreateNavigator(), arguments, xmlTextWriter);
            XElement el = XElement.Parse(stringWriter.GetStringBuilder().ToString(), LoadOptions.None);

            // CLEANUP
            xslTransformer = null;
            cObj = null;
            xhc = null;
            stringWriter.Close();
            stringWriter.Dispose();
            stringWriter = null;
            wSettings = null;
            xmlTextWriter = null;

            return el;

        }
       
        
    }
}
  