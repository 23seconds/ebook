
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.RuleEngine.Core;
using EY.com.eBook.RuleEngine.Core.Interfaces;
using EY.com.eBook.RuleEngine.Data;
using EY.com.eBook.Core;
using EY.com.eBook.API.Contracts.Data.Cache;

namespace EY.com.eBook.RuleEngine.AY2016
{
    
    public class VergelijkendeCijfersApp
    {
        public static WorksheetTypeSettings Settings = new WorksheetTypeSettings
                                                      {
                                                        Clients = true
                                                        ,
                                                        Suppliers = true
                                                        ,
                                                        AllAccounts = true
                                                        , 
                                                        PerformsBookings=true
                                                      };
                                                      
        public static Guid TypeId = new Guid("d6b8e81c-670f-404a-bcdc-0c6deb0fa7d4");

        public List<string> ImportablePreviousTypes  = new List<string>() {  "EY.com.eBook.RuleEngine.AY2015.FacturatieApp"  };

        public Guid GetMyType() { return TypeId; }

        public void SetMinimal()
        {
            if (Data == null)
            {
                Data = new EY.com.eBook.RuleEngine.AY2016.VergelijkendeCijfersAppTypes.WPVergelijkendeCijfers();
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.Kosten == null)
            {
                Data.Kosten = new List<EY.com.eBook.RuleEngine.AY2016.VergelijkendeCijfersAppTypes.CijferBlok>(); ;
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.Opbrengsten == null)
            {
                Data.Opbrengsten = new List<EY.com.eBook.RuleEngine.AY2016.VergelijkendeCijfersAppTypes.CijferBlok>(); ;
                Data.LastUpdated = DateTime.Now;
            }

        }
        
        public string ToXml() 
        {
            return CoreHelper.SerializeToString(Data);
        }

        public void LoadXml(string xml)
        {
            if (!string.IsNullOrEmpty(xml)) {
                Data = CoreHelper.DeserializeFromString<EY.com.eBook.RuleEngine.AY2016.VergelijkendeCijfersAppTypes.WPVergelijkendeCijfers>(xml);
            }
            SetMinimal();
            
        }
        
        public EY.com.eBook.RuleEngine.AY2016.VergelijkendeCijfersAppTypes.WPVergelijkendeCijfers Data { get; set; }

        public void ForceRecalculation(ref WorksheetSetData worksheetset)
        {
            Data.LastUpdated = new DateTime(1980, 1, 1);
            Calculate(ref worksheetset);
        }

        #region worksheet type rules
        // Only if worksheet does any calculation.
        public void Calculate(ref WorksheetSetData worksheetset)
        {
            worksheetset.ClearMessages(TypeId);
            DateTime fileEnd = worksheetset.DataContract.EndDate;
            DateTime? prevFileEnd = worksheetset.DataContract.PreviousEndDate;
            if (!prevFileEnd.HasValue)
            {
                Data.LastUpdated = DateTime.Now;
                return;
            }
            if (!Data.LastUpdated.HasValue) Data.LastUpdated = new DateTime(1980,1,1);
            List<string> ranges = new List<string>();
            ranges.Add("6");

            List<AccountDataContract> accounts = worksheetset.GetAccountsInRanges(ranges, false, true, true, false, worksheetset.DataContract.Culture);

            List<string> accountnrs = accounts.Select(k => k.InternalNr).ToList();
            List<string> kostAccounts = Data.Kosten.Select(k => k.accountNr).ToList();

            List<AccountDataContract> missingAccounts = accounts.Where(a => !kostAccounts.Contains(a.InternalNr)).ToList();
            List<AccountDataContract> updateAccounts = accounts
                                            .Where(a => kostAccounts.Contains(a.InternalNr)
                                                        && (a.LastChanged > Data.LastUpdated.Value)
                                            ).ToList();

            List<EY.com.eBook.RuleEngine.AY2016.VergelijkendeCijfersAppTypes.CijferBlok> deletedItems = Data.Kosten.Where(k => !accountnrs.Contains(k.accountNr)).ToList();
            deletedItems.ForEach(a => Data.Kosten.Remove(a));

            foreach (AccountDataContract account in updateAccounts)
            {
                EY.com.eBook.RuleEngine.AY2016.VergelijkendeCijfersAppTypes.CijferBlok cb = Data.Kosten.FirstOrDefault(k => k.accountNr == account.InternalNr);
                if (cb != null)
                {
                    cb.SaldoVorigBJ = account.PreviousSaldo;
                    cb.Saldo = account.Saldo;
                    cb.accountDescription = string.Format("{0} {1}", account.VisualNr, account.DefaultDescription);
                    cb.Beweging = cb.Saldo - cb.SaldoVorigBJ;
                    cb.PercentBeweging = Math.Abs(cb.Saldo.Value) > Math.Abs(cb.SaldoVorigBJ.Value)
                                            ? (((cb.Saldo.Value - cb.SaldoVorigBJ.Value) / cb.Saldo.Value) * 100)
                                            : (((cb.SaldoVorigBJ.Value - cb.Saldo.Value) / cb.SaldoVorigBJ.Value) * 100);
                    cb.PercentBeweging = Math.Round(cb.PercentBeweging.Value, 2, MidpointRounding.AwayFromZero);
                    cb.LastUpdated = DateTime.Now;
                    Validate(cb, "Kosten", ref worksheetset);

                }
            }

            foreach (AccountDataContract account in missingAccounts)
            {
                EY.com.eBook.RuleEngine.AY2016.VergelijkendeCijfersAppTypes.CijferBlok cb =
                   new EY.com.eBook.RuleEngine.AY2016.VergelijkendeCijfersAppTypes.CijferBlok
                   {
                       Id = Guid.NewGuid()
                       ,
                       accountDescription = string.Format("{0} {1}", account.VisualNr, account.DefaultDescription)
                       ,
                       accountNr = account.InternalNr
                       ,
                       eindeBoekjaar = fileEnd
                       ,
                       eindeVorigBJ = prevFileEnd
                       ,
                       Coll = "Kosten"
                       ,
                       LastUpdated = DateTime.Now
                       ,
                       Notitie = null
                       ,
                       Saldo = account.Saldo
                       ,
                       SaldoVorigBJ = account.PreviousSaldo
                       ,
                       IsValid = true
                       ,
                       Beweging = account.Saldo - account.PreviousSaldo
                   };
                cb.PercentBeweging = Math.Abs(cb.Saldo.Value) > Math.Abs(cb.SaldoVorigBJ.Value)
                                            ? (((cb.Saldo.Value - cb.SaldoVorigBJ.Value) / cb.Saldo.Value) * 100)
                                            : (((cb.SaldoVorigBJ.Value - cb.Saldo.Value) / cb.SaldoVorigBJ.Value) * 100);
                cb.PercentBeweging = Math.Round(cb.PercentBeweging.Value, 2, MidpointRounding.AwayFromZero);
                Validate(cb, "Kosten", ref worksheetset);
                Data.Kosten.Add(cb);
            }
            Data.Kosten = Data.Kosten.OrderBy(k => k.accountNr).ToList();


            ranges.Clear();
            ranges.Add("7");

            accounts = worksheetset.GetAccountsInRanges(ranges, false, true, true, false, worksheetset.DataContract.Culture);

            accountnrs = accounts.Select(k => k.InternalNr).ToList();
            List<string> opbrengstAccounts = Data.Opbrengsten.Select(k => k.accountNr).ToList();

            missingAccounts = accounts.Where(a => !opbrengstAccounts.Contains(a.InternalNr)).ToList();
            updateAccounts = accounts
                                            .Where(a => opbrengstAccounts.Contains(a.InternalNr)
                                                        && (a.LastChanged > Data.LastUpdated.Value)
                                            ).ToList();

            deletedItems = Data.Opbrengsten.Where(k => !accountnrs.Contains(k.accountNr)).ToList();
            deletedItems.ForEach(a => Data.Opbrengsten.Remove(a));

            foreach (AccountDataContract account in updateAccounts)
            {
                EY.com.eBook.RuleEngine.AY2016.VergelijkendeCijfersAppTypes.CijferBlok cb = Data.Opbrengsten.FirstOrDefault(k => k.accountNr == account.InternalNr);
                if (cb != null)
                {
                    cb.SaldoVorigBJ = account.PreviousSaldo;
                    cb.Saldo = account.Saldo;
                    cb.accountDescription = string.Format("{0} {1}", account.VisualNr, account.DefaultDescription);
                    cb.Beweging = cb.Saldo - cb.SaldoVorigBJ;
                    cb.PercentBeweging = Math.Abs(cb.Saldo.Value) > Math.Abs(cb.SaldoVorigBJ.Value)
                                            ? (((cb.Saldo.Value - cb.SaldoVorigBJ.Value) / cb.Saldo.Value) * 100)
                                            : (((cb.SaldoVorigBJ.Value - cb.Saldo.Value) / cb.SaldoVorigBJ.Value) * 100);
                    cb.PercentBeweging = Math.Round(cb.PercentBeweging.Value, 2, MidpointRounding.AwayFromZero);
                    cb.LastUpdated = DateTime.Now;
                    Validate(cb, "Opbrengsten", ref worksheetset);
                }
            }

            foreach (AccountDataContract account in missingAccounts)
            {
                EY.com.eBook.RuleEngine.AY2016.VergelijkendeCijfersAppTypes.CijferBlok cb =
                   new EY.com.eBook.RuleEngine.AY2016.VergelijkendeCijfersAppTypes.CijferBlok
                   {
                       Id = Guid.NewGuid()
                       ,
                       accountDescription = string.Format("{0} {1}", account.VisualNr, account.DefaultDescription)
                       ,
                       accountNr = account.InternalNr
                       ,
                       eindeBoekjaar = fileEnd
                       ,
                       eindeVorigBJ = prevFileEnd
                       ,
                       Coll = "Opbrengsten"
                       ,
                       LastUpdated = DateTime.Now
                       ,
                       Notitie = null
                       ,
                       Saldo = account.Saldo
                       ,
                       SaldoVorigBJ = account.PreviousSaldo
                       ,
                       IsValid = true
                       ,
                       Beweging = account.Saldo - account.PreviousSaldo
                   };
                cb.PercentBeweging = Math.Abs(cb.Saldo.Value) > Math.Abs(cb.SaldoVorigBJ.Value)
                                            ? (((cb.Saldo.Value - cb.SaldoVorigBJ.Value) / cb.Saldo.Value) * 100)
                                            : (((cb.SaldoVorigBJ.Value - cb.Saldo.Value) / cb.SaldoVorigBJ.Value) * 100);
                cb.PercentBeweging = Math.Round(cb.PercentBeweging.Value, 2, MidpointRounding.AwayFromZero);
                Validate(cb, "Opbrengsten", ref worksheetset);
                Data.Opbrengsten.Add(cb);
            }
            Data.Opbrengsten = Data.Opbrengsten.OrderBy(k => k.accountNr).ToList();

            Data.LastUpdated = DateTime.Now;

        }

        #endregion


        #region AddUpdateDelete

        public void UpdateKosten(EY.com.eBook.RuleEngine.AY2016.VergelijkendeCijfersAppTypes.CijferBlok entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2016.VergelijkendeCijfersAppTypes.CijferBlok original = Data.Kosten.FirstOrDefault(e => e.Id == entity.Id);
            if (original == null)
            {
                
            }
            else
            {
                entity = Validate(entity, "Kosten", ref worksheetset);
                if (entity.IsValid)
                {
                    int idx = Data.Kosten.IndexOf(original);
                    Data.Kosten.RemoveAt(idx);
                    Data.Kosten.Insert(idx, entity);
                    Data.LastUpdated = DateTime.Now;
                    entity.LastUpdated = DateTime.Now;
                }
            }
        }

       

        public void UpdateOpbrengsten(EY.com.eBook.RuleEngine.AY2016.VergelijkendeCijfersAppTypes.CijferBlok entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2016.VergelijkendeCijfersAppTypes.CijferBlok original = Data.Opbrengsten.FirstOrDefault(e => e.Id == entity.Id);
            if (original == null)
            {
                
            }
            else
            {
                entity = Validate(entity, "Opbrengsten", ref worksheetset);
                if (entity.IsValid)
                {
                    int idx = Data.Opbrengsten.IndexOf(original);
                    Data.Opbrengsten.RemoveAt(idx);
                    Data.Opbrengsten.Insert(idx, entity);
                    Data.LastUpdated = DateTime.Now;
                    entity.LastUpdated = DateTime.Now;
                }
            }
        }

        #endregion

        #region worksheet type validation

        public EY.com.eBook.RuleEngine.AY2016.VergelijkendeCijfersAppTypes.CijferBlok Validate(EY.com.eBook.RuleEngine.AY2016.VergelijkendeCijfersAppTypes.CijferBlok entity, string location, ref WorksheetSetData worksheetset)
        {
            entity.IsValid = true;
            worksheetset.ClearMessages(TypeId, entity.Id);
            if (entity.PercentBeweging.Value > 25 || entity.PercentBeweging.Value < -25)
            {
                if (Math.Abs(entity.Beweging.Value) > Data.Treshold)
                {
                    if (!string.IsNullOrEmpty(entity.Notitie))
                    {
                        entity.Notitie = entity.Notitie.Trim();
                    }
                    if (string.IsNullOrEmpty(entity.Notitie))
                    {
                        //add message collection, field:"Grootboek", required
                        worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "NOTE_EMPTY", location, entity.Id, "Notitie");
                        entity.IsValid = false;
                    }
                }
            }

            return entity;
        }


        #endregion

        
        // CALCULATION
        
        // BOOKINGS
        
        // IMPORT PREVIOUS
        
    }
}
  