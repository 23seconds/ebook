﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel.Activation;
using ProtoBuf.Meta;
using System.Reflection;
using EY.com.eBook.RuleEngine.Core.Interfaces;
using System.ServiceModel;
using EY.com.eBook.Utils.Services;

namespace EY.com.eBook.RuleEngine
{
    public class WebServiceProxyFactory : ServiceHostFactoryBase
    {
        static WebServiceProxyFactory()
        {
            RuntimeTypeModel.Default.AutoAddMissingTypes = true;
            RuntimeTypeModel.Default.AutoCompile = true;
            new EY.com.eBook.RuleEngine.Data.Prepper().PrepareProtobuf();
        }

        public override ServiceHostBase CreateServiceHost(string constructorString, Uri[] baseAddresses)
        {
            var interfaceType = Type.GetType(constructorString, true);
            return
                new ServiceHost(
                    ProxyFactory.CreateInstance(interfaceType.AssemblyQualifiedName,
                                                "WebServiceProxyFactory", string.Empty, true), baseAddresses);
        }
    }
}
