﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.RuleEngine.Core;
using EY.com.eBook.RuleEngine.Core.Interfaces;
using EY.com.eBook.RuleEngine.Data;
using EY.com.eBook.Core;

namespace EY.com.eBook.RuleEngine.AY2008
{
    
    public class ErelonenHuurApp
    {
        #region standard
        public static WorksheetTypeSettings Settings = new WorksheetTypeSettings
                                                      {
                                                        Clients = true
                                                        ,
                                                        Suppliers = true
                                                        ,
                                                        AllAccounts = true
                                                        , 
                                                        PerformsBookings=true
                                                      };
                                                      
        public static Guid TypeId = new Guid("3e9bb601-ea23-4072-bd49-e5dc61a6b5a8");

        public List<string> ImportablePreviousTypes  = new List<string>() {  "EY.com.eBook.RuleEngine.AY2011.FacturatieApp"  };

        public Guid GetMyType() { return TypeId; }
        
        public void SetMinimal() 
        {
            if(Data==null) {
                Data = new EY.com.eBook.RuleEngine.AY2008.ErelonenHuurAppTypes.ErelonenHuur();
                Data.LastUpdated = DateTime.Now;
            }              
            
              if(Data.Import==null) {
                  Data.Import = new List<EY.com.eBook.RuleEngine.AY2008.ErelonenHuurAppTypes.ImportItem>();;
                  Data.LastUpdated = DateTime.Now;
              }
            
              if(Data.ImportedHistory==null) {
                  Data.ImportedHistory = new List<EY.com.eBook.RuleEngine.AY2008.ErelonenHuurAppTypes.ImportedHistory>();;
                  Data.LastUpdated = DateTime.Now;
              }
            
              if(Data.Historiek==null) {
                  Data.Historiek = new List<EY.com.eBook.RuleEngine.AY2008.ErelonenHuurAppTypes.VerdelingHistoriek>();;
                  Data.LastUpdated = DateTime.Now;
              }
            
              if(Data.Fiches281_50==null) {
                  Data.Fiches281_50 = new List<EY.com.eBook.RuleEngine.AY2008.ErelonenHuurAppTypes.Fiche281_50>();;
                  Data.LastUpdated = DateTime.Now;
              }
            
              if(Data.Fiche281_50Col==null) {
                  Data.Fiche281_50Col = new List<EY.com.eBook.RuleEngine.AY2008.ErelonenHuurAppTypes.Fiche281_50>();;
                  Data.LastUpdated = DateTime.Now;
              }
            
        }
        
        public string ToXml() 
        {
            return CoreHelper.SerializeToString(Data);
        }

        public void LoadXml(string xml)
        {
            if (!string.IsNullOrEmpty(xml)) {
                Data = CoreHelper.DeserializeFromString<EY.com.eBook.RuleEngine.AY2008.ErelonenHuurAppTypes.ErelonenHuur>(xml);
            }
            SetMinimal();
            
        }
        
        public EY.com.eBook.RuleEngine.AY2008.ErelonenHuurAppTypes.ErelonenHuur Data { get; set; }
        
        public void Calculate(ref WorksheetSetData worksheetset)
        {
           // Data.LastCalculated = DateTime.Now;
        }
        #endregion
        
        #region AddUpdateDelete
        
          public void AddToHistoriek(EY.com.eBook.RuleEngine.AY2008.ErelonenHuurAppTypes.VerdelingHistoriek entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void DeleteHistoriek(Guid rowId,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void UpdateHistoriek(EY.com.eBook.RuleEngine.AY2008.ErelonenHuurAppTypes.VerdelingHistoriek entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void AddToFiches281_50(EY.com.eBook.RuleEngine.AY2008.ErelonenHuurAppTypes.Fiche281_50 entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void UpdateFiches281_50(EY.com.eBook.RuleEngine.AY2008.ErelonenHuurAppTypes.Fiche281_50 entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void AddToFiche281_50Col(EY.com.eBook.RuleEngine.AY2008.ErelonenHuurAppTypes.Fiche281_50 entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void DeleteFiche281_50Col(Guid rowId,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void UpdateFiche281_50Col(EY.com.eBook.RuleEngine.AY2008.ErelonenHuurAppTypes.Fiche281_50 entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
        #endregion
        
        #region VALIDATION
        
        public EY.com.eBook.RuleEngine.AY2008.ErelonenHuurAppTypes.ImportItem Validate(EY.com.eBook.RuleEngine.AY2008.ErelonenHuurAppTypes.ImportItem entity, string location,ref WorksheetSetData worksheetset)
          {
          entity.IsValid = true;
          worksheetset.ClearMessages(TypeId, entity.Id);
          

          return entity;
          }
        
        public EY.com.eBook.RuleEngine.AY2008.ErelonenHuurAppTypes.ImportedHistory Validate(EY.com.eBook.RuleEngine.AY2008.ErelonenHuurAppTypes.ImportedHistory entity, string location,ref WorksheetSetData worksheetset)
          {
          entity.IsValid = true;
          worksheetset.ClearMessages(TypeId, entity.Id);
          

          return entity;
          }
        
        public EY.com.eBook.RuleEngine.AY2008.ErelonenHuurAppTypes.VerdelingHistoriek Validate(EY.com.eBook.RuleEngine.AY2008.ErelonenHuurAppTypes.VerdelingHistoriek entity, string location,ref WorksheetSetData worksheetset)
          {
          entity.IsValid = true;
          worksheetset.ClearMessages(TypeId, entity.Id);
          

          return entity;
          }
        
        public EY.com.eBook.RuleEngine.AY2008.ErelonenHuurAppTypes.Fiche281_50 Validate(EY.com.eBook.RuleEngine.AY2008.ErelonenHuurAppTypes.Fiche281_50 entity, string location,ref WorksheetSetData worksheetset)
          {
          entity.IsValid = true;
          worksheetset.ClearMessages(TypeId, entity.Id);
          

          return entity;
          }
        
        #endregion
        // CALCULATION
        
        // BOOKINGS
        
        // IMPORT PREVIOUS
        
    }
}
  