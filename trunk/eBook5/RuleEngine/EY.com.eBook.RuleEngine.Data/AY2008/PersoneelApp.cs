﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.RuleEngine.Core;
using EY.com.eBook.RuleEngine.Core.Interfaces;
using EY.com.eBook.RuleEngine.Data;
using EY.com.eBook.Core;

namespace EY.com.eBook.RuleEngine.AY2008
{
    
    public class PersoneelApp
    {
        #region standard
        public static WorksheetTypeSettings Settings = new WorksheetTypeSettings
                                                      {
                                                        Clients = true
                                                        ,
                                                        Suppliers = true
                                                        ,
                                                        AllAccounts = true
                                                        , 
                                                        PerformsBookings=true
                                                      };
                                                      
        public static Guid TypeId = new Guid("bd2374b4-42c3-4c09-96b2-d8debe3fa1b1");

        public List<string> ImportablePreviousTypes  = new List<string>() {  "EY.com.eBook.RuleEngine.AY2011.FacturatieApp"  };

        public Guid GetMyType() { return TypeId; }
        
        public void SetMinimal() 
        {
            if(Data==null) {
                Data = new EY.com.eBook.RuleEngine.AY2008.PersoneelAppTypes.WPPersoneel();
                Data.LastUpdated = DateTime.Now;
            }              
            
              if(Data._325_20Coll==null) {
                  Data._325_20Coll = new List<EY.com.eBook.RuleEngine.AY2008.PersoneelAppTypes.Item325>();;
                  Data.LastUpdated = DateTime.Now;
              }
            
              if(Data.TeVerklarenVerschil325_20==null) {
                  Data.TeVerklarenVerschil325_20 = new List<EY.com.eBook.RuleEngine.AY2008.PersoneelAppTypes.VerschilItem>();;
                  Data.LastUpdated = DateTime.Now;
              }
            
              if(Data._325_10Coll==null) {
                  Data._325_10Coll = new List<EY.com.eBook.RuleEngine.AY2008.PersoneelAppTypes.Item325>();;
                  Data.LastUpdated = DateTime.Now;
              }
            
              if(Data.TeVerklarenVerschil325_10==null) {
                  Data.TeVerklarenVerschil325_10 = new List<EY.com.eBook.RuleEngine.AY2008.PersoneelAppTypes.VerschilItem>();;
                  Data.LastUpdated = DateTime.Now;
              }
            
              if(Data.ArbeidersForm==null) {
                  Data.ArbeidersForm = new EY.com.eBook.RuleEngine.AY2008.PersoneelAppTypes.Werknemer {
                            Id=Guid.NewGuid()
                            , LastUpdated=null
                            , IsValid=true
                            , PreviousImported=false
                            
                         };
                  Data.LastUpdated = DateTime.Now;
              }
            
              if(Data.BediendeForm==null) {
                  Data.BediendeForm = new EY.com.eBook.RuleEngine.AY2008.PersoneelAppTypes.Werknemer {
                            Id=Guid.NewGuid()
                            , LastUpdated=null
                            , IsValid=true
                            , PreviousImported=false
                            
                         };
                  Data.LastUpdated = DateTime.Now;
              }
            
              if(Data.AansluitingArbeiders==null) {
                  Data.AansluitingArbeiders = new List<EY.com.eBook.RuleEngine.AY2008.PersoneelAppTypes.VerschilItem>();;
                  Data.LastUpdated = DateTime.Now;
              }
            
              if(Data.AansluitingBedienden==null) {
                  Data.AansluitingBedienden = new List<EY.com.eBook.RuleEngine.AY2008.PersoneelAppTypes.VerschilItem>();;
                  Data.LastUpdated = DateTime.Now;
              }
            
              if(Data.RSZColl==null) {
                  Data.RSZColl = new List<EY.com.eBook.RuleEngine.AY2008.PersoneelAppTypes.Algemeen>();;
                  Data.LastUpdated = DateTime.Now;
              }
            
              if(Data.BVsColl==null) {
                  Data.BVsColl = new List<EY.com.eBook.RuleEngine.AY2008.PersoneelAppTypes.Algemeen>();;
                  Data.LastUpdated = DateTime.Now;
              }
            
              if(Data.NettoColl==null) {
                  Data.NettoColl = new List<EY.com.eBook.RuleEngine.AY2008.PersoneelAppTypes.Algemeen>();;
                  Data.LastUpdated = DateTime.Now;
              }
            
              if(Data.VakantieGeldColl==null) {
                  Data.VakantieGeldColl = new EY.com.eBook.RuleEngine.AY2008.PersoneelAppTypes.Vakantiegeld {
                            Id=Guid.NewGuid()
                            , LastUpdated=null
                            , IsValid=true
                            , PreviousImported=false
                            
                         };
                  Data.LastUpdated = DateTime.Now;
              }
            
        }
        
        public string ToXml() 
        {
            return CoreHelper.SerializeToString(Data);
        }

        public void LoadXml(string xml)
        {
            if (!string.IsNullOrEmpty(xml)) {
                Data = CoreHelper.DeserializeFromString<EY.com.eBook.RuleEngine.AY2008.PersoneelAppTypes.WPPersoneel>(xml);
            }
            SetMinimal();
            
        }
        
        public EY.com.eBook.RuleEngine.AY2008.PersoneelAppTypes.WPPersoneel Data { get; set; }
        
        public void Calculate(ref WorksheetSetData worksheetset)
        {
           // Data.LastCalculated = DateTime.Now;
        }
        #endregion
        
        #region AddUpdateDelete
        
          public void AddTo_325_20Coll(EY.com.eBook.RuleEngine.AY2008.PersoneelAppTypes.Item325 entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void Delete_325_20Coll(Guid rowId,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void Update_325_20Coll(EY.com.eBook.RuleEngine.AY2008.PersoneelAppTypes.Item325 entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void AddToTeVerklarenVerschil325_20(EY.com.eBook.RuleEngine.AY2008.PersoneelAppTypes.VerschilItem entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void DeleteTeVerklarenVerschil325_20(Guid rowId,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void UpdateTeVerklarenVerschil325_20(EY.com.eBook.RuleEngine.AY2008.PersoneelAppTypes.VerschilItem entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void AddTo_325_10Coll(EY.com.eBook.RuleEngine.AY2008.PersoneelAppTypes.Item325 entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void Delete_325_10Coll(Guid rowId,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void Update_325_10Coll(EY.com.eBook.RuleEngine.AY2008.PersoneelAppTypes.Item325 entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void AddToTeVerklarenVerschil325_10(EY.com.eBook.RuleEngine.AY2008.PersoneelAppTypes.VerschilItem entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void DeleteTeVerklarenVerschil325_10(Guid rowId,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void UpdateTeVerklarenVerschil325_10(EY.com.eBook.RuleEngine.AY2008.PersoneelAppTypes.VerschilItem entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void UpdateArbeidersForm(EY.com.eBook.RuleEngine.AY2008.PersoneelAppTypes.Werknemer entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void UpdateBediendeForm(EY.com.eBook.RuleEngine.AY2008.PersoneelAppTypes.Werknemer entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void AddToAansluitingArbeiders(EY.com.eBook.RuleEngine.AY2008.PersoneelAppTypes.VerschilItem entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void DeleteAansluitingArbeiders(Guid rowId,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void UpdateAansluitingArbeiders(EY.com.eBook.RuleEngine.AY2008.PersoneelAppTypes.VerschilItem entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void AddToAansluitingBedienden(EY.com.eBook.RuleEngine.AY2008.PersoneelAppTypes.VerschilItem entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void DeleteAansluitingBedienden(Guid rowId,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void UpdateAansluitingBedienden(EY.com.eBook.RuleEngine.AY2008.PersoneelAppTypes.VerschilItem entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void AddToRSZColl(EY.com.eBook.RuleEngine.AY2008.PersoneelAppTypes.Algemeen entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void DeleteRSZColl(Guid rowId,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void UpdateRSZColl(EY.com.eBook.RuleEngine.AY2008.PersoneelAppTypes.Algemeen entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void AddToBVsColl(EY.com.eBook.RuleEngine.AY2008.PersoneelAppTypes.Algemeen entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void DeleteBVsColl(Guid rowId,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void UpdateBVsColl(EY.com.eBook.RuleEngine.AY2008.PersoneelAppTypes.Algemeen entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void AddToNettoColl(EY.com.eBook.RuleEngine.AY2008.PersoneelAppTypes.Algemeen entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void DeleteNettoColl(Guid rowId,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void UpdateNettoColl(EY.com.eBook.RuleEngine.AY2008.PersoneelAppTypes.Algemeen entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void UpdateVakantieGeldColl(EY.com.eBook.RuleEngine.AY2008.PersoneelAppTypes.Vakantiegeld entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
        #endregion
        
        #region VALIDATION
        
        public EY.com.eBook.RuleEngine.AY2008.PersoneelAppTypes.Item325 Validate(EY.com.eBook.RuleEngine.AY2008.PersoneelAppTypes.Item325 entity, string location,ref WorksheetSetData worksheetset)
          {
          entity.IsValid = true;
          worksheetset.ClearMessages(TypeId, entity.Id);
          

          return entity;
          }
        
        public EY.com.eBook.RuleEngine.AY2008.PersoneelAppTypes.VerschilItem Validate(EY.com.eBook.RuleEngine.AY2008.PersoneelAppTypes.VerschilItem entity, string location,ref WorksheetSetData worksheetset)
          {
          entity.IsValid = true;
          worksheetset.ClearMessages(TypeId, entity.Id);
          

          return entity;
          }
        
        public EY.com.eBook.RuleEngine.AY2008.PersoneelAppTypes.Werknemer Validate(EY.com.eBook.RuleEngine.AY2008.PersoneelAppTypes.Werknemer entity, string location,ref WorksheetSetData worksheetset)
          {
          entity.IsValid = true;
          worksheetset.ClearMessages(TypeId, entity.Id);
          

          return entity;
          }
        
        public EY.com.eBook.RuleEngine.AY2008.PersoneelAppTypes.Algemeen Validate(EY.com.eBook.RuleEngine.AY2008.PersoneelAppTypes.Algemeen entity, string location,ref WorksheetSetData worksheetset)
          {
          entity.IsValid = true;
          worksheetset.ClearMessages(TypeId, entity.Id);
          

          return entity;
          }
        
        public EY.com.eBook.RuleEngine.AY2008.PersoneelAppTypes.Vakantiegeld Validate(EY.com.eBook.RuleEngine.AY2008.PersoneelAppTypes.Vakantiegeld entity, string location,ref WorksheetSetData worksheetset)
          {
          entity.IsValid = true;
          worksheetset.ClearMessages(TypeId, entity.Id);
          

          return entity;
          }
        
        #endregion
        // CALCULATION
        
        // BOOKINGS
        
        // IMPORT PREVIOUS
        
    }
}
  