﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.RuleEngine.Core;
using EY.com.eBook.RuleEngine.Core.Interfaces;
using EY.com.eBook.RuleEngine.Data;
using EY.com.eBook.Core;

namespace EY.com.eBook.RuleEngine.AY2008
{
    
    public class DubieuzeDebiteurenApp
    {
        #region standard
        public static WorksheetTypeSettings Settings = new WorksheetTypeSettings
                                                      {
                                                        Clients = true
                                                        ,
                                                        Suppliers = true
                                                        ,
                                                        AllAccounts = true
                                                        , 
                                                        PerformsBookings=true
                                                      };
                                                      
        public static Guid TypeId = new Guid("a8040411-be4e-4bc7-a463-04d4f52ded46");

        public List<string> ImportablePreviousTypes  = new List<string>() {  "EY.com.eBook.RuleEngine.AY2011.FacturatieApp"  };

        public Guid GetMyType() { return TypeId; }
        
        public void SetMinimal() 
        {
            if(Data==null) {
                Data = new EY.com.eBook.RuleEngine.AY2008.DubieuzeDebiteurenAppTypes.DubieuzeDebiteuren();
                Data.LastUpdated = DateTime.Now;
            }              
            
              if(Data.ToestandEindVorigBoekjaar==null) {
                  Data.ToestandEindVorigBoekjaar = new List<EY.com.eBook.RuleEngine.AY2008.DubieuzeDebiteurenAppTypes.DubieuzeDebiteurT>();;
                  Data.LastUpdated = DateTime.Now;
              }
            
              if(Data.WijzigingBestaandeTijdensBoekjaar==null) {
                  Data.WijzigingBestaandeTijdensBoekjaar = new List<EY.com.eBook.RuleEngine.AY2008.DubieuzeDebiteurenAppTypes.DubieuzeDebiteurW>();;
                  Data.LastUpdated = DateTime.Now;
              }
            
              if(Data.NieuweTijdensBoekjaar==null) {
                  Data.NieuweTijdensBoekjaar = new List<EY.com.eBook.RuleEngine.AY2008.DubieuzeDebiteurenAppTypes.DubieuzeDebiteurN>();;
                  Data.LastUpdated = DateTime.Now;
              }
            
              if(Data.ToestandEindHuidigBoekjaar==null) {
                  Data.ToestandEindHuidigBoekjaar = new List<EY.com.eBook.RuleEngine.AY2008.DubieuzeDebiteurenAppTypes.DubieuzeDebiteurH>();;
                  Data.LastUpdated = DateTime.Now;
              }
            
        }
        
        public string ToXml() 
        {
            return CoreHelper.SerializeToString(Data);
        }

        public void LoadXml(string xml)
        {
            if (!string.IsNullOrEmpty(xml)) {
                Data = CoreHelper.DeserializeFromString<EY.com.eBook.RuleEngine.AY2008.DubieuzeDebiteurenAppTypes.DubieuzeDebiteuren>(xml);
            }
            SetMinimal();
            
        }
        
        public EY.com.eBook.RuleEngine.AY2008.DubieuzeDebiteurenAppTypes.DubieuzeDebiteuren Data { get; set; }
        
        public void Calculate(ref WorksheetSetData worksheetset)
        {
           // Data.LastCalculated = DateTime.Now;
        }
        #endregion
        
        #region AddUpdateDelete
        
          public void AddToToestandEindVorigBoekjaar(EY.com.eBook.RuleEngine.AY2008.DubieuzeDebiteurenAppTypes.DubieuzeDebiteurT entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void DeleteToestandEindVorigBoekjaar(Guid rowId,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void UpdateToestandEindVorigBoekjaar(EY.com.eBook.RuleEngine.AY2008.DubieuzeDebiteurenAppTypes.DubieuzeDebiteurT entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void AddToWijzigingBestaandeTijdensBoekjaar(EY.com.eBook.RuleEngine.AY2008.DubieuzeDebiteurenAppTypes.DubieuzeDebiteurW entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void DeleteWijzigingBestaandeTijdensBoekjaar(Guid rowId,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void UpdateWijzigingBestaandeTijdensBoekjaar(EY.com.eBook.RuleEngine.AY2008.DubieuzeDebiteurenAppTypes.DubieuzeDebiteurW entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void AddToNieuweTijdensBoekjaar(EY.com.eBook.RuleEngine.AY2008.DubieuzeDebiteurenAppTypes.DubieuzeDebiteurN entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void DeleteNieuweTijdensBoekjaar(Guid rowId,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void UpdateNieuweTijdensBoekjaar(EY.com.eBook.RuleEngine.AY2008.DubieuzeDebiteurenAppTypes.DubieuzeDebiteurN entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
        #endregion
        
        #region VALIDATION
        
        public EY.com.eBook.RuleEngine.AY2008.DubieuzeDebiteurenAppTypes.DubieuzeDebiteurT Validate(EY.com.eBook.RuleEngine.AY2008.DubieuzeDebiteurenAppTypes.DubieuzeDebiteurT entity, string location,ref WorksheetSetData worksheetset)
          {
          entity.IsValid = true;
          worksheetset.ClearMessages(TypeId, entity.Id);
          

          return entity;
          }
        
        public EY.com.eBook.RuleEngine.AY2008.DubieuzeDebiteurenAppTypes.DubieuzeDebiteurW Validate(EY.com.eBook.RuleEngine.AY2008.DubieuzeDebiteurenAppTypes.DubieuzeDebiteurW entity, string location,ref WorksheetSetData worksheetset)
          {
          entity.IsValid = true;
          worksheetset.ClearMessages(TypeId, entity.Id);
          

          return entity;
          }
        
        public EY.com.eBook.RuleEngine.AY2008.DubieuzeDebiteurenAppTypes.DubieuzeDebiteurN Validate(EY.com.eBook.RuleEngine.AY2008.DubieuzeDebiteurenAppTypes.DubieuzeDebiteurN entity, string location,ref WorksheetSetData worksheetset)
          {
          entity.IsValid = true;
          worksheetset.ClearMessages(TypeId, entity.Id);
          

          return entity;
          }
        
        public EY.com.eBook.RuleEngine.AY2008.DubieuzeDebiteurenAppTypes.DubieuzeDebiteurH Validate(EY.com.eBook.RuleEngine.AY2008.DubieuzeDebiteurenAppTypes.DubieuzeDebiteurH entity, string location,ref WorksheetSetData worksheetset)
          {
          entity.IsValid = true;
          worksheetset.ClearMessages(TypeId, entity.Id);
          

          return entity;
          }
        
        #endregion
        // CALCULATION
        
        // BOOKINGS
        
        // IMPORT PREVIOUS
        
    }
}
  