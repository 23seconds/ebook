﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.RuleEngine.Core;
using EY.com.eBook.RuleEngine.Core.Interfaces;
using EY.com.eBook.RuleEngine.Data;
using EY.com.eBook.Core;

namespace EY.com.eBook.RuleEngine.AY2008
{
    
    public class VoorschottenApp
    {
        #region standard
        public static WorksheetTypeSettings Settings = new WorksheetTypeSettings
                                                      {
                                                        Clients = true
                                                        ,
                                                        Suppliers = true
                                                        ,
                                                        AllAccounts = true
                                                        , 
                                                        PerformsBookings=true
                                                      };
                                                      
        public static Guid TypeId = new Guid("997a80e4-ea39-42c7-95d5-cdf8322c4c11");

        public List<string> ImportablePreviousTypes  = new List<string>() {  "EY.com.eBook.RuleEngine.AY2011.FacturatieApp"  };

        public Guid GetMyType() { return TypeId; }
        
        public void SetMinimal() 
        {
            if(Data==null) {
                Data = new EY.com.eBook.RuleEngine.AY2008.VoorschottenAppTypes.Voorschotten();
                Data.LastUpdated = DateTime.Now;
            }              
            
              if(Data.Gegevens==null) {
                  Data.Gegevens = new List<EY.com.eBook.RuleEngine.AY2008.VoorschottenAppTypes.Gegeven>();;
                  Data.LastUpdated = DateTime.Now;
              }
            
              if(Data.InterestVoeten==null) {
                  Data.InterestVoeten = new List<EY.com.eBook.RuleEngine.AY2008.VoorschottenAppTypes.IntrestVoet>();;
                  Data.LastUpdated = DateTime.Now;
              }
            
              if(Data.RentegevendeVoorschotten==null) {
                  Data.RentegevendeVoorschotten = new List<EY.com.eBook.RuleEngine.AY2008.VoorschottenAppTypes.RentegevendVoorschot>();;
                  Data.LastUpdated = DateTime.Now;
              }
            
        }
        
        public string ToXml() 
        {
            return CoreHelper.SerializeToString(Data);
        }

        public void LoadXml(string xml)
        {
            if (!string.IsNullOrEmpty(xml)) {
                Data = CoreHelper.DeserializeFromString<EY.com.eBook.RuleEngine.AY2008.VoorschottenAppTypes.Voorschotten>(xml);
            }
            SetMinimal();
            
        }
        
        public EY.com.eBook.RuleEngine.AY2008.VoorschottenAppTypes.Voorschotten Data { get; set; }
        
        public void Calculate(ref WorksheetSetData worksheetset)
        {
           // Data.LastCalculated = DateTime.Now;
        }
        #endregion
        
        #region AddUpdateDelete
        
          public void AddToGegevens(EY.com.eBook.RuleEngine.AY2008.VoorschottenAppTypes.Gegeven entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void DeleteGegevens(Guid rowId,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void UpdateGegevens(EY.com.eBook.RuleEngine.AY2008.VoorschottenAppTypes.Gegeven entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void AddToInterestVoeten(EY.com.eBook.RuleEngine.AY2008.VoorschottenAppTypes.IntrestVoet entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void DeleteInterestVoeten(Guid rowId,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void UpdateInterestVoeten(EY.com.eBook.RuleEngine.AY2008.VoorschottenAppTypes.IntrestVoet entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void AddToRentegevendeVoorschotten(EY.com.eBook.RuleEngine.AY2008.VoorschottenAppTypes.RentegevendVoorschot entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void DeleteRentegevendeVoorschotten(Guid rowId,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void UpdateRentegevendeVoorschotten(EY.com.eBook.RuleEngine.AY2008.VoorschottenAppTypes.RentegevendVoorschot entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
        #endregion
        
        #region VALIDATION
        
        public EY.com.eBook.RuleEngine.AY2008.VoorschottenAppTypes.Gegeven Validate(EY.com.eBook.RuleEngine.AY2008.VoorschottenAppTypes.Gegeven entity, string location,ref WorksheetSetData worksheetset)
          {
          entity.IsValid = true;
          worksheetset.ClearMessages(TypeId, entity.Id);
          

          return entity;
          }
        
        public EY.com.eBook.RuleEngine.AY2008.VoorschottenAppTypes.IntrestVoet Validate(EY.com.eBook.RuleEngine.AY2008.VoorschottenAppTypes.IntrestVoet entity, string location,ref WorksheetSetData worksheetset)
          {
          entity.IsValid = true;
          worksheetset.ClearMessages(TypeId, entity.Id);
          

          return entity;
          }
        
        public EY.com.eBook.RuleEngine.AY2008.VoorschottenAppTypes.RentegevendVoorschot Validate(EY.com.eBook.RuleEngine.AY2008.VoorschottenAppTypes.RentegevendVoorschot entity, string location,ref WorksheetSetData worksheetset)
          {
          entity.IsValid = true;
          worksheetset.ClearMessages(TypeId, entity.Id);
          

          return entity;
          }
        
        #endregion
        // CALCULATION
        
        // BOOKINGS
        
        // IMPORT PREVIOUS
        
    }
}
  