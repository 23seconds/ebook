using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using EY.com.eBook.API.Contracts.Services;
using EY.com.eBook.Caching;
using EY.com.eBook.RuleEngine;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.RuleEngine.Data;

namespace EY.com.eBook.API.Contracts.Proxies
{
    public class RuleEngine2008Proxy : ClientBase<IRuleEngine2008>, IRuleEngine2008
    {

        #region IRuleEngine2008 Members


        public decimal? GetTaxCalculation(CriteriaFileDataContract cfdc)
        {
            return Channel.GetTaxCalculation(cfdc);
        }

        public void ReCalculate(CriteriaFileDataContract cfdc)
        {
            Channel.ReCalculate(cfdc);
        }

        public void ImportPrevious(CriteriaFileDataContract cfdc)
        {
            Channel.ImportPrevious(cfdc);
        }

      
        #endregion
    }
}

