using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Activation;
using EY.com.eBook.Core.EF;
using System.ServiceModel.Web;
using EY.com.eBook.RuleEngine.Core;
using EY.com.eBook.RuleEngine.Data;
using EY.com.eBook.Core.Data;
using EY.com.eBook.Core;
using EY.com.eBook.Core.EF.Write;
using EY.com.eBook.API.Contracts.Proxies;
using EY.com.eBook.Caching;
using EY.com.eBook.RuleEngine.AY2008;
using EY.com.eBook.API.Contracts.Data;
using System.Reflection;
using EY.com.eBook.RuleEngine.Core.Interfaces;
using ProtoBuf.Meta;

namespace EY.com.eBook.RuleEngine
{
    [EntityFrameworkServiceBehavior]
    [ServiceBehavior(IncludeExceptionDetailInFaults = true, ConcurrencyMode = ConcurrencyMode.Multiple, InstanceContextMode = InstanceContextMode.PerCall)]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class WebProxyRuleEngine2008 : IRuleEngine2008
    {


        public WebProxyRuleEngine2008()
        {
            RuntimeTypeModel.Default.AutoAddMissingTypes = true;
            RuntimeTypeModel.Default.AutoCompile = true;
            new EY.com.eBook.RuleEngine.Data.Prepper().PrepareProtobuf();
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
       RequestFormat = WebMessageFormat.Json,
       ResponseFormat = WebMessageFormat.Json,
       UriTemplate = "/ReCalculate")]
        public void ReCalculate(CriteriaFileDataContract cfdc)
        {
            RuleEngine2008Proxy rp = new RuleEngine2008Proxy();
            rp.Open();
            rp.ReCalculate(cfdc);
            rp.Close();
            
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
       RequestFormat = WebMessageFormat.Json,
       ResponseFormat = WebMessageFormat.Json,
       UriTemplate = "/ImportPrevious")]
        public void ImportPrevious(CriteriaFileDataContract cfdc)
        {
            RuleEngine2008Proxy rp = new RuleEngine2008Proxy();
            rp.Open();
            rp.ImportPrevious(cfdc);
            rp.Close();
            
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
       RequestFormat = WebMessageFormat.Json,
       ResponseFormat = WebMessageFormat.Json,
       UriTemplate = "/GetTaxCalculation")]
        public decimal? GetTaxCalculation(CriteriaFileDataContract cfdc)
        {
            RuleEngine2008Proxy rp = new RuleEngine2008Proxy();
            rp.Open();
            decimal? result = rp.GetTaxCalculation(cfdc);
            rp.Close();
            return result;
        }


        
    }
}


/*
[WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
       RequestFormat = WebMessageFormat.Json,
       ResponseFormat = WebMessageFormat.Json,
       UriTemplate = "/FacturatieAppGetData")]
        public EY.com.eBook.RuleEngine.AY2011.FacturatieAppTypes.Facturatie FacturatieAppGetData(CriteriaFileDataContract cfdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.FacturatieAppTypes.Facturatie result = rp.FacturatieAppGetData(cfdc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
       RequestFormat = WebMessageFormat.Json,
       ResponseFormat = WebMessageFormat.Json,
       UriTemplate = "/FacturatieAppCalculate")]
        public EY.com.eBook.RuleEngine.AY2011.FacturatieAppTypes.Facturatie FacturatieAppCalculate(CriteriaFileDataContract cfdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.FacturatieAppTypes.Facturatie result = rp.FacturatieAppCalculate(cfdc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
       RequestFormat = WebMessageFormat.Json,
       ResponseFormat = WebMessageFormat.Json,
       UriTemplate = "/FacturatieAppFacturatieKlantValidate")]
        public List<InvalidField> FacturatieAppFacturatieKlantValidate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaFacturatieAppFacturatieKlantDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            List<InvalidField> result = rp.FacturatieAppFacturatieKlantValidate(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
       RequestFormat = WebMessageFormat.Json,
       ResponseFormat = WebMessageFormat.Json,
       UriTemplate = "/FacturatieAppFacturatieLeverancierValidate")]
        public List<InvalidField> FacturatieAppFacturatieLeverancierValidate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaFacturatieAppFacturatieLeverancierDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            List<InvalidField> result = rp.FacturatieAppFacturatieLeverancierValidate(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
       RequestFormat = WebMessageFormat.Json,
       ResponseFormat = WebMessageFormat.Json,
       UriTemplate = "/FacturatieAppFacturatieLeverancierCNValidate")]
        public List<InvalidField> FacturatieAppFacturatieLeverancierCNValidate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaFacturatieAppFacturatieLeverancierCNDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            List<InvalidField> result = rp.FacturatieAppFacturatieLeverancierCNValidate(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
       RequestFormat = WebMessageFormat.Json,
       ResponseFormat = WebMessageFormat.Json,
       UriTemplate = "/FacturatieAppFacturatieKlantCNValidate")]
        public List<InvalidField> FacturatieAppFacturatieKlantCNValidate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaFacturatieAppFacturatieKlantCNDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            List<InvalidField> result = rp.FacturatieAppFacturatieKlantCNValidate(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
       RequestFormat = WebMessageFormat.Json,
       ResponseFormat = WebMessageFormat.Json,
       UriTemplate = "/FacturatieAppOpTeMakenFacturenAdd")]
        public EY.com.eBook.RuleEngine.AY2011.FacturatieAppTypes.Facturatie FacturatieAppOpTeMakenFacturenAdd(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaFacturatieAppFacturatieKlantDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.FacturatieAppTypes.Facturatie result = rp.FacturatieAppOpTeMakenFacturenAdd(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
       RequestFormat = WebMessageFormat.Json,
       ResponseFormat = WebMessageFormat.Json,
       UriTemplate = "/FacturatieAppOpTeMakenFacturenUpdate")]
        public EY.com.eBook.RuleEngine.AY2011.FacturatieAppTypes.Facturatie FacturatieAppOpTeMakenFacturenUpdate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaFacturatieAppFacturatieKlantDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.FacturatieAppTypes.Facturatie result = rp.FacturatieAppOpTeMakenFacturenUpdate(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
       RequestFormat = WebMessageFormat.Json,
       ResponseFormat = WebMessageFormat.Json,
       UriTemplate = "/FacturatieAppOpTeMakenFacturenRemove")]
        public EY.com.eBook.RuleEngine.AY2011.FacturatieAppTypes.Facturatie FacturatieAppOpTeMakenFacturenRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.FacturatieAppTypes.Facturatie result = rp.FacturatieAppOpTeMakenFacturenRemove(cfrdc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
       RequestFormat = WebMessageFormat.Json,
       ResponseFormat = WebMessageFormat.Json,
       UriTemplate = "/FacturatieAppTeOntvangenCNAdd")]
        public EY.com.eBook.RuleEngine.AY2011.FacturatieAppTypes.Facturatie FacturatieAppTeOntvangenCNAdd(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaFacturatieAppFacturatieLeverancierCNDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.FacturatieAppTypes.Facturatie result = rp.FacturatieAppTeOntvangenCNAdd(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
       RequestFormat = WebMessageFormat.Json,
       ResponseFormat = WebMessageFormat.Json,
       UriTemplate = "/FacturatieAppTeOntvangenCNUpdate")]
        public EY.com.eBook.RuleEngine.AY2011.FacturatieAppTypes.Facturatie FacturatieAppTeOntvangenCNUpdate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaFacturatieAppFacturatieLeverancierCNDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.FacturatieAppTypes.Facturatie result = rp.FacturatieAppTeOntvangenCNUpdate(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
       RequestFormat = WebMessageFormat.Json,
       ResponseFormat = WebMessageFormat.Json,
       UriTemplate = "/FacturatieAppTeOntvangenCNRemove")]
        public EY.com.eBook.RuleEngine.AY2011.FacturatieAppTypes.Facturatie FacturatieAppTeOntvangenCNRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.FacturatieAppTypes.Facturatie result = rp.FacturatieAppTeOntvangenCNRemove(cfrdc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
       RequestFormat = WebMessageFormat.Json,
       ResponseFormat = WebMessageFormat.Json,
       UriTemplate = "/FacturatieAppTeOntvangenFacturenAdd")]
        public EY.com.eBook.RuleEngine.AY2011.FacturatieAppTypes.Facturatie FacturatieAppTeOntvangenFacturenAdd(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaFacturatieAppFacturatieLeverancierDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.FacturatieAppTypes.Facturatie result = rp.FacturatieAppTeOntvangenFacturenAdd(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
       RequestFormat = WebMessageFormat.Json,
       ResponseFormat = WebMessageFormat.Json,
       UriTemplate = "/FacturatieAppTeOntvangenFacturenUpdate")]
        public EY.com.eBook.RuleEngine.AY2011.FacturatieAppTypes.Facturatie FacturatieAppTeOntvangenFacturenUpdate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaFacturatieAppFacturatieLeverancierDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.FacturatieAppTypes.Facturatie result = rp.FacturatieAppTeOntvangenFacturenUpdate(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
       RequestFormat = WebMessageFormat.Json,
       ResponseFormat = WebMessageFormat.Json,
       UriTemplate = "/FacturatieAppTeOntvangenFacturenRemove")]
        public EY.com.eBook.RuleEngine.AY2011.FacturatieAppTypes.Facturatie FacturatieAppTeOntvangenFacturenRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.FacturatieAppTypes.Facturatie result = rp.FacturatieAppTeOntvangenFacturenRemove(cfrdc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
       RequestFormat = WebMessageFormat.Json,
       ResponseFormat = WebMessageFormat.Json,
       UriTemplate = "/FacturatieAppOpTeMakenCNAdd")]
        public EY.com.eBook.RuleEngine.AY2011.FacturatieAppTypes.Facturatie FacturatieAppOpTeMakenCNAdd(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaFacturatieAppFacturatieKlantCNDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.FacturatieAppTypes.Facturatie result = rp.FacturatieAppOpTeMakenCNAdd(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
       RequestFormat = WebMessageFormat.Json,
       ResponseFormat = WebMessageFormat.Json,
       UriTemplate = "/FacturatieAppOpTeMakenCNUpdate")]
        public EY.com.eBook.RuleEngine.AY2011.FacturatieAppTypes.Facturatie FacturatieAppOpTeMakenCNUpdate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaFacturatieAppFacturatieKlantCNDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.FacturatieAppTypes.Facturatie result = rp.FacturatieAppOpTeMakenCNUpdate(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
       RequestFormat = WebMessageFormat.Json,
       ResponseFormat = WebMessageFormat.Json,
       UriTemplate = "/FacturatieAppOpTeMakenCNRemove")]
        public EY.com.eBook.RuleEngine.AY2011.FacturatieAppTypes.Facturatie FacturatieAppOpTeMakenCNRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.FacturatieAppTypes.Facturatie result = rp.FacturatieAppOpTeMakenCNRemove(cfrdc);
            rp.Close();
            return result;
        }

       // #endregion
*/