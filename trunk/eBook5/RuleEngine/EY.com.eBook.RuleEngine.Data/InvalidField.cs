﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.RuleEngine.Data
{
    [DataContract]
    public class InvalidField
    {
        // for example /OpTeMakenFacturen aka collection
        [DataMember(Name="l")]
        public string WorksheetTab  { get; set; }

        [DataMember(Name = "fn")]
        public string FieldName { get; set; }

        [DataMember(Name = "mc")]
        public string MessageCode { get; set; }

        [DataMember(Name = "fv")]
        public List<string> FormattingValues { get; set; }
    }
}
