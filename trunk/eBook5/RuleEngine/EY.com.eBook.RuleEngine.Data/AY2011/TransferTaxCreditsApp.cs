﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.RuleEngine.Core;
using EY.com.eBook.RuleEngine.Core.Interfaces;
using EY.com.eBook.RuleEngine.Data;
using EY.com.eBook.Core;

namespace EY.com.eBook.RuleEngine.AY2011
{
    
    public class TransferTaxCreditsApp
    {
        #region standard
        public static WorksheetTypeSettings Settings = new WorksheetTypeSettings
                                                      {
                                                        Clients = true
                                                        ,
                                                        Suppliers = true
                                                        ,
                                                        AllAccounts = true
                                                        , 
                                                        PerformsBookings=true
                                                      };
                                                      
        public static Guid TypeId = new Guid("34b1fab3-1d1a-44d0-87e7-93fb703ef3a2");

        public List<string> ImportablePreviousTypes  = new List<string>() {  "EY.com.eBook.RuleEngine.AY2011.FacturatieApp"  };

        public Guid GetMyType() { return TypeId; }
        
        public void SetMinimal() 
        {
            if(Data==null) {
                Data = new EY.com.eBook.RuleEngine.AY2011.TransferTaxCreditsAppTypes.TransferTaxCredits();
                Data.LastUpdated = DateTime.Now;
            }              
            
              if(Data.FiscaleVerliezen==null) {
                  Data.FiscaleVerliezen = new List<EY.com.eBook.RuleEngine.AY2011.TransferTaxCreditsAppTypes.FiscaalVerlies>();;
                  Data.LastUpdated = DateTime.Now;
              }
            
              if(Data.NID==null) {
                  Data.NID = new List<EY.com.eBook.RuleEngine.AY2011.TransferTaxCreditsAppTypes.NIDItem>();;
                  Data.LastUpdated = DateTime.Now;
              }
            
              if(Data.DBI==null) {
                  Data.DBI = new EY.com.eBook.RuleEngine.AY2011.TransferTaxCreditsAppTypes.DBIItem {
                            Id=Guid.NewGuid()
                            , LastUpdated=null
                            , IsValid=true
                            , PreviousImported=false
                            
                         };
                  Data.LastUpdated = DateTime.Now;
              }
            
              if(Data.Invest==null) {
                  Data.Invest = new List<EY.com.eBook.RuleEngine.AY2011.TransferTaxCreditsAppTypes.InvestItem>();;
                  Data.LastUpdated = DateTime.Now;
              }
            
              if(Data.Belastingkrediet==null) {
                  Data.Belastingkrediet = new List<EY.com.eBook.RuleEngine.AY2011.TransferTaxCreditsAppTypes.BelastingkredietItem>();;
                  Data.LastUpdated = DateTime.Now;
              }
            
              if(Data.Aanpassingen==null) {
                  Data.Aanpassingen = new List<EY.com.eBook.RuleEngine.AY2011.TransferTaxCreditsAppTypes.AanpassingItem>();;
                  Data.LastUpdated = DateTime.Now;
              }
            
              if(Data.Aanwendingen==null) {
                  Data.Aanwendingen = new EY.com.eBook.RuleEngine.AY2011.TransferTaxCreditsAppTypes.Aanwending {
                            Id=Guid.NewGuid()
                            , LastUpdated=null
                            , IsValid=true
                            , PreviousImported=false
                            
                         };
                  Data.LastUpdated = DateTime.Now;
              }
            
        }
        
        public string ToXml() 
        {
            return CoreHelper.SerializeToString(Data);
        }

        public void LoadXml(string xml)
        {
            if (!string.IsNullOrEmpty(xml)) {
                Data = CoreHelper.DeserializeFromString<EY.com.eBook.RuleEngine.AY2011.TransferTaxCreditsAppTypes.TransferTaxCredits>(xml);
            }
            SetMinimal();
            
        }
        
        public EY.com.eBook.RuleEngine.AY2011.TransferTaxCreditsAppTypes.TransferTaxCredits Data { get; set; }
        
        public void Calculate(ref WorksheetSetData worksheetset)
        {
           // Data.LastCalculated = DateTime.Now;
        }
        #endregion
        
        #region AddUpdateDelete
        
          public void AddToFiscaleVerliezen(EY.com.eBook.RuleEngine.AY2011.TransferTaxCreditsAppTypes.FiscaalVerlies entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void DeleteFiscaleVerliezen(Guid rowId,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void UpdateFiscaleVerliezen(EY.com.eBook.RuleEngine.AY2011.TransferTaxCreditsAppTypes.FiscaalVerlies entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void UpdateDBI(EY.com.eBook.RuleEngine.AY2011.TransferTaxCreditsAppTypes.DBIItem entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void AddToInvest(EY.com.eBook.RuleEngine.AY2011.TransferTaxCreditsAppTypes.InvestItem entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void DeleteInvest(Guid rowId,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void UpdateInvest(EY.com.eBook.RuleEngine.AY2011.TransferTaxCreditsAppTypes.InvestItem entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void AddToBelastingkrediet(EY.com.eBook.RuleEngine.AY2011.TransferTaxCreditsAppTypes.BelastingkredietItem entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void DeleteBelastingkrediet(Guid rowId,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void UpdateBelastingkrediet(EY.com.eBook.RuleEngine.AY2011.TransferTaxCreditsAppTypes.BelastingkredietItem entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void AddToAanpassingen(EY.com.eBook.RuleEngine.AY2011.TransferTaxCreditsAppTypes.AanpassingItem entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void DeleteAanpassingen(Guid rowId,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void UpdateAanpassingen(EY.com.eBook.RuleEngine.AY2011.TransferTaxCreditsAppTypes.AanpassingItem entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void UpdateAanwendingen(EY.com.eBook.RuleEngine.AY2011.TransferTaxCreditsAppTypes.Aanwending entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
        #endregion
        
        #region VALIDATION
        
        public EY.com.eBook.RuleEngine.AY2011.TransferTaxCreditsAppTypes.FiscaalVerlies Validate(EY.com.eBook.RuleEngine.AY2011.TransferTaxCreditsAppTypes.FiscaalVerlies entity, string location,ref WorksheetSetData worksheetset)
          {
          entity.IsValid = true;
          worksheetset.ClearMessages(TypeId, entity.Id);
          

          return entity;
          }
        
        public EY.com.eBook.RuleEngine.AY2011.TransferTaxCreditsAppTypes.NIDItem Validate(EY.com.eBook.RuleEngine.AY2011.TransferTaxCreditsAppTypes.NIDItem entity, string location,ref WorksheetSetData worksheetset)
          {
          entity.IsValid = true;
          worksheetset.ClearMessages(TypeId, entity.Id);
          

          return entity;
          }
        
        public EY.com.eBook.RuleEngine.AY2011.TransferTaxCreditsAppTypes.DBIItem Validate(EY.com.eBook.RuleEngine.AY2011.TransferTaxCreditsAppTypes.DBIItem entity, string location,ref WorksheetSetData worksheetset)
          {
          entity.IsValid = true;
          worksheetset.ClearMessages(TypeId, entity.Id);
          

          return entity;
          }
        
        public EY.com.eBook.RuleEngine.AY2011.TransferTaxCreditsAppTypes.InvestItem Validate(EY.com.eBook.RuleEngine.AY2011.TransferTaxCreditsAppTypes.InvestItem entity, string location,ref WorksheetSetData worksheetset)
          {
          entity.IsValid = true;
          worksheetset.ClearMessages(TypeId, entity.Id);
          

          return entity;
          }
        
        public EY.com.eBook.RuleEngine.AY2011.TransferTaxCreditsAppTypes.BelastingkredietItem Validate(EY.com.eBook.RuleEngine.AY2011.TransferTaxCreditsAppTypes.BelastingkredietItem entity, string location,ref WorksheetSetData worksheetset)
          {
          entity.IsValid = true;
          worksheetset.ClearMessages(TypeId, entity.Id);
          

          return entity;
          }
        
        public EY.com.eBook.RuleEngine.AY2011.TransferTaxCreditsAppTypes.AanpassingItem Validate(EY.com.eBook.RuleEngine.AY2011.TransferTaxCreditsAppTypes.AanpassingItem entity, string location,ref WorksheetSetData worksheetset)
          {
          entity.IsValid = true;
          worksheetset.ClearMessages(TypeId, entity.Id);
          

          return entity;
          }
        
        public EY.com.eBook.RuleEngine.AY2011.TransferTaxCreditsAppTypes.Aanwending Validate(EY.com.eBook.RuleEngine.AY2011.TransferTaxCreditsAppTypes.Aanwending entity, string location,ref WorksheetSetData worksheetset)
          {
          entity.IsValid = true;
          worksheetset.ClearMessages(TypeId, entity.Id);
          

          return entity;
          }
        
        #endregion
        // CALCULATION
        
        // BOOKINGS
        
        // IMPORT PREVIOUS
        
    }
}
  