﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.RuleEngine.Core;
using EY.com.eBook.RuleEngine.Core.Interfaces;
using EY.com.eBook.RuleEngine.Data;
using EY.com.eBook.Core;

namespace EY.com.eBook.RuleEngine.AY2011
{
    
    public class BepalingBelastbareBasisApp
    {
        #region standard
        public static WorksheetTypeSettings Settings = new WorksheetTypeSettings
                                                      {
                                                        Clients = true
                                                        ,
                                                        Suppliers = true
                                                        ,
                                                        AllAccounts = true
                                                        , 
                                                        PerformsBookings=true
                                                      };
                                                      
        public static Guid TypeId = new Guid("ceae70d1-20ef-4ab9-91e6-64b3f0f5eb98");

        public List<string> ImportablePreviousTypes  = new List<string>() {  "EY.com.eBook.RuleEngine.AY2011.FacturatieApp"  };

        public Guid GetMyType() { return TypeId; }
        
        public void SetMinimal() 
        {
            if(Data==null) {
                Data = new EY.com.eBook.RuleEngine.AY2011.BepalingBelastbareBasisAppTypes.BepalingBasis();
                Data.LastUpdated = DateTime.Now;
            }              
            
              if(Data.Overzicht==null) {
                  Data.Overzicht = new EY.com.eBook.RuleEngine.AY2011.BepalingBelastbareBasisAppTypes.General {
                            Id=Guid.NewGuid()
                            , LastUpdated=null
                            , IsValid=true
                            , PreviousImported=false
                            
                         };
                  Data.LastUpdated = DateTime.Now;
              }
            
              if(Data.BelasteReserves==null) {
                  Data.BelasteReserves = new EY.com.eBook.RuleEngine.AY2011.BepalingBelastbareBasisAppTypes.BelasteReservesItem {
                            Id=Guid.NewGuid()
                            , LastUpdated=null
                            , IsValid=true
                            , PreviousImported=false
                            
                         };
                  Data.LastUpdated = DateTime.Now;
              }
            
              if(Data.VerworpenUitgaven==null) {
                  Data.VerworpenUitgaven = new EY.com.eBook.RuleEngine.AY2011.BepalingBelastbareBasisAppTypes.VerworpenUitgavenItem {
                            Id=Guid.NewGuid()
                            , LastUpdated=null
                            , IsValid=true
                            , PreviousImported=false
                            
                         };
                  Data.LastUpdated = DateTime.Now;
              }
            
              if(Data.BelastbareBestZonderAftrek==null) {
                  Data.BelastbareBestZonderAftrek = new EY.com.eBook.RuleEngine.AY2011.BepalingBelastbareBasisAppTypes.BelastbareBestZonderAftrekItem {
                            Id=Guid.NewGuid()
                            , LastUpdated=null
                            , IsValid=true
                            , PreviousImported=false
                            
                         };
                  Data.LastUpdated = DateTime.Now;
              }
            
              if(Data.OpdelingNaarOorsprong==null) {
                  Data.OpdelingNaarOorsprong = new EY.com.eBook.RuleEngine.AY2011.BepalingBelastbareBasisAppTypes.OpdelingNaarOorsprongItem {
                            Id=Guid.NewGuid()
                            , LastUpdated=null
                            , IsValid=true
                            , PreviousImported=false
                            
                         };
                  Data.LastUpdated = DateTime.Now;
              }
            
              if(Data.FiscaleAftrek==null) {
                  Data.FiscaleAftrek = new EY.com.eBook.RuleEngine.AY2011.BepalingBelastbareBasisAppTypes.FiscaleAftrekItem {
                            Id=Guid.NewGuid()
                            , LastUpdated=null
                            , IsValid=true
                            , PreviousImported=false
                            
                         };
                  Data.LastUpdated = DateTime.Now;
              }
            
        }
        
        public string ToXml() 
        {
            return CoreHelper.SerializeToString(Data);
        }

        public void LoadXml(string xml)
        {
            if (!string.IsNullOrEmpty(xml)) {
                Data = CoreHelper.DeserializeFromString<EY.com.eBook.RuleEngine.AY2011.BepalingBelastbareBasisAppTypes.BepalingBasis>(xml);
            }
            SetMinimal();
            
        }
        
        public EY.com.eBook.RuleEngine.AY2011.BepalingBelastbareBasisAppTypes.BepalingBasis Data { get; set; }
        
        public void Calculate(ref WorksheetSetData worksheetset)
        {
           // Data.LastCalculated = DateTime.Now;
        }
        #endregion
        
        #region AddUpdateDelete
        
          public void UpdateOverzicht(EY.com.eBook.RuleEngine.AY2011.BepalingBelastbareBasisAppTypes.General entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void UpdateBelasteReserves(EY.com.eBook.RuleEngine.AY2011.BepalingBelastbareBasisAppTypes.BelasteReservesItem entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void UpdateVerworpenUitgaven(EY.com.eBook.RuleEngine.AY2011.BepalingBelastbareBasisAppTypes.VerworpenUitgavenItem entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void UpdateBelastbareBestZonderAftrek(EY.com.eBook.RuleEngine.AY2011.BepalingBelastbareBasisAppTypes.BelastbareBestZonderAftrekItem entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void UpdateOpdelingNaarOorsprong(EY.com.eBook.RuleEngine.AY2011.BepalingBelastbareBasisAppTypes.OpdelingNaarOorsprongItem entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void UpdateFiscaleAftrek(EY.com.eBook.RuleEngine.AY2011.BepalingBelastbareBasisAppTypes.FiscaleAftrekItem entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
        #endregion
        
        #region VALIDATION
        
        public EY.com.eBook.RuleEngine.AY2011.BepalingBelastbareBasisAppTypes.General Validate(EY.com.eBook.RuleEngine.AY2011.BepalingBelastbareBasisAppTypes.General entity, string location,ref WorksheetSetData worksheetset)
          {
          entity.IsValid = true;
          worksheetset.ClearMessages(TypeId, entity.Id);
          

          return entity;
          }
        
        public EY.com.eBook.RuleEngine.AY2011.BepalingBelastbareBasisAppTypes.BelasteReservesItem Validate(EY.com.eBook.RuleEngine.AY2011.BepalingBelastbareBasisAppTypes.BelasteReservesItem entity, string location,ref WorksheetSetData worksheetset)
          {
          entity.IsValid = true;
          worksheetset.ClearMessages(TypeId, entity.Id);
          

          return entity;
          }
        
        public EY.com.eBook.RuleEngine.AY2011.BepalingBelastbareBasisAppTypes.VerworpenUitgavenItem Validate(EY.com.eBook.RuleEngine.AY2011.BepalingBelastbareBasisAppTypes.VerworpenUitgavenItem entity, string location,ref WorksheetSetData worksheetset)
          {
          entity.IsValid = true;
          worksheetset.ClearMessages(TypeId, entity.Id);
          

          return entity;
          }
        
        public EY.com.eBook.RuleEngine.AY2011.BepalingBelastbareBasisAppTypes.BelastbareBestZonderAftrekItem Validate(EY.com.eBook.RuleEngine.AY2011.BepalingBelastbareBasisAppTypes.BelastbareBestZonderAftrekItem entity, string location,ref WorksheetSetData worksheetset)
          {
          entity.IsValid = true;
          worksheetset.ClearMessages(TypeId, entity.Id);
          

          return entity;
          }
        
        public EY.com.eBook.RuleEngine.AY2011.BepalingBelastbareBasisAppTypes.OpdelingNaarOorsprongItem Validate(EY.com.eBook.RuleEngine.AY2011.BepalingBelastbareBasisAppTypes.OpdelingNaarOorsprongItem entity, string location,ref WorksheetSetData worksheetset)
          {
          entity.IsValid = true;
          worksheetset.ClearMessages(TypeId, entity.Id);
          

          return entity;
          }
        
        public EY.com.eBook.RuleEngine.AY2011.BepalingBelastbareBasisAppTypes.FiscaleAftrekItem Validate(EY.com.eBook.RuleEngine.AY2011.BepalingBelastbareBasisAppTypes.FiscaleAftrekItem entity, string location,ref WorksheetSetData worksheetset)
          {
          entity.IsValid = true;
          worksheetset.ClearMessages(TypeId, entity.Id);
          

          return entity;
          }
        
        #endregion
        // CALCULATION
        
        // BOOKINGS
        
        // IMPORT PREVIOUS
        
    }
}
  