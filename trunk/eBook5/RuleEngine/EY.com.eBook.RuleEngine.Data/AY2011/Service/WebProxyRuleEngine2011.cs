using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Activation;
using EY.com.eBook.Core.EF;
using System.ServiceModel.Web;
using EY.com.eBook.RuleEngine.Core;
using EY.com.eBook.RuleEngine.Data;
using EY.com.eBook.Core.Data;
using EY.com.eBook.Core;
using EY.com.eBook.Core.EF.Write;
using EY.com.eBook.API.Contracts.Proxies;
using EY.com.eBook.Caching;
using EY.com.eBook.RuleEngine.AY2011;
using EY.com.eBook.API.Contracts.Data;
using System.Reflection;
using EY.com.eBook.RuleEngine.Core.Interfaces;
using ProtoBuf.Meta;

namespace EY.com.eBook.RuleEngine
{
    [EntityFrameworkServiceBehavior]
    [ServiceBehavior(IncludeExceptionDetailInFaults = true, ConcurrencyMode = ConcurrencyMode.Multiple, InstanceContextMode = InstanceContextMode.PerCall)]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class WebProxyRuleEngine2011 : IRuleEngine2011
    {


        public WebProxyRuleEngine2011()
        {
            RuntimeTypeModel.Default.AutoAddMissingTypes = true;
            RuntimeTypeModel.Default.AutoCompile = true;
            new EY.com.eBook.RuleEngine.Data.Prepper().PrepareProtobuf();
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
       RequestFormat = WebMessageFormat.Json,
       ResponseFormat = WebMessageFormat.Json,
       UriTemplate = "/ReCalculate")]
        public void ReCalculate(CriteriaFileDataContract cfdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            rp.ReCalculate(cfdc);
            rp.Close();
            
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
       RequestFormat = WebMessageFormat.Json,
       ResponseFormat = WebMessageFormat.Json,
       UriTemplate = "/ImportPrevious")]
        public void ImportPrevious(CriteriaFileDataContract cfdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            rp.ImportPrevious(cfdc);
            rp.Close();
            
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
       RequestFormat = WebMessageFormat.Json,
       ResponseFormat = WebMessageFormat.Json,
       UriTemplate = "/GetTaxCalculation")]
        public decimal? GetTaxCalculation(CriteriaFileDataContract cfdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            decimal? result = rp.GetTaxCalculation(cfdc);
            rp.Close();
            return result;
        }

        #region Web proxy



        #region FacturatieApp
        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/FacturatieAppGetData")]
        public EY.com.eBook.RuleEngine.AY2011.FacturatieAppTypes.Facturatie FacturatieAppGetData(CriteriaFileDataContract cfdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.FacturatieAppTypes.Facturatie result = rp.FacturatieAppGetData(cfdc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/FacturatieAppCalculate")]
        public EY.com.eBook.RuleEngine.AY2011.FacturatieAppTypes.Facturatie FacturatieAppCalculate(CriteriaFileDataContract cfdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.FacturatieAppTypes.Facturatie result = rp.FacturatieAppCalculate(cfdc);
            rp.Close();
            return result;
        }

        #region Entity validation


        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/FacturatieAppFacturatieKlantValidate")]
        public List<FileMessageDataContract> FacturatieAppFacturatieKlantValidate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaFacturatieAppFacturatieKlantDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            List<FileMessageDataContract> result = rp.FacturatieAppFacturatieKlantValidate(cedc);
            rp.Close();
            return result;
        }



        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/FacturatieAppFacturatieLeverancierCNValidate")]
        public List<FileMessageDataContract> FacturatieAppFacturatieLeverancierCNValidate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaFacturatieAppFacturatieLeverancierCNDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            List<FileMessageDataContract> result = rp.FacturatieAppFacturatieLeverancierCNValidate(cedc);
            rp.Close();
            return result;
        }



        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/FacturatieAppFacturatieLeverancierValidate")]
        public List<FileMessageDataContract> FacturatieAppFacturatieLeverancierValidate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaFacturatieAppFacturatieLeverancierDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            List<FileMessageDataContract> result = rp.FacturatieAppFacturatieLeverancierValidate(cedc);
            rp.Close();
            return result;
        }



        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/FacturatieAppFacturatieKlantCNValidate")]
        public List<FileMessageDataContract> FacturatieAppFacturatieKlantCNValidate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaFacturatieAppFacturatieKlantCNDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            List<FileMessageDataContract> result = rp.FacturatieAppFacturatieKlantCNValidate(cedc);
            rp.Close();
            return result;
        }



        #endregion



        #region Add/Remove


        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/FacturatieAppOpTeMakenFacturenAdd")]
        public EY.com.eBook.RuleEngine.AY2011.FacturatieAppTypes.Facturatie FacturatieAppOpTeMakenFacturenAdd(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaFacturatieAppFacturatieKlantDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.FacturatieAppTypes.Facturatie result = rp.FacturatieAppOpTeMakenFacturenAdd(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/FacturatieAppOpTeMakenFacturenRemove")]
        public EY.com.eBook.RuleEngine.AY2011.FacturatieAppTypes.Facturatie FacturatieAppOpTeMakenFacturenRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.FacturatieAppTypes.Facturatie result = rp.FacturatieAppOpTeMakenFacturenRemove(cfrdc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/FacturatieAppOpTeMakenFacturenUpdate")]
        public EY.com.eBook.RuleEngine.AY2011.FacturatieAppTypes.Facturatie FacturatieAppOpTeMakenFacturenUpdate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaFacturatieAppFacturatieKlantDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.FacturatieAppTypes.Facturatie result = rp.FacturatieAppOpTeMakenFacturenUpdate(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/FacturatieAppTeOntvangenCNAdd")]
        public EY.com.eBook.RuleEngine.AY2011.FacturatieAppTypes.Facturatie FacturatieAppTeOntvangenCNAdd(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaFacturatieAppFacturatieLeverancierCNDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.FacturatieAppTypes.Facturatie result = rp.FacturatieAppTeOntvangenCNAdd(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/FacturatieAppTeOntvangenCNRemove")]
        public EY.com.eBook.RuleEngine.AY2011.FacturatieAppTypes.Facturatie FacturatieAppTeOntvangenCNRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.FacturatieAppTypes.Facturatie result = rp.FacturatieAppTeOntvangenCNRemove(cfrdc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/FacturatieAppTeOntvangenCNUpdate")]
        public EY.com.eBook.RuleEngine.AY2011.FacturatieAppTypes.Facturatie FacturatieAppTeOntvangenCNUpdate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaFacturatieAppFacturatieLeverancierCNDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.FacturatieAppTypes.Facturatie result = rp.FacturatieAppTeOntvangenCNUpdate(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/FacturatieAppTeOntvangenFacturenAdd")]
        public EY.com.eBook.RuleEngine.AY2011.FacturatieAppTypes.Facturatie FacturatieAppTeOntvangenFacturenAdd(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaFacturatieAppFacturatieLeverancierDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.FacturatieAppTypes.Facturatie result = rp.FacturatieAppTeOntvangenFacturenAdd(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/FacturatieAppTeOntvangenFacturenRemove")]
        public EY.com.eBook.RuleEngine.AY2011.FacturatieAppTypes.Facturatie FacturatieAppTeOntvangenFacturenRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.FacturatieAppTypes.Facturatie result = rp.FacturatieAppTeOntvangenFacturenRemove(cfrdc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/FacturatieAppTeOntvangenFacturenUpdate")]
        public EY.com.eBook.RuleEngine.AY2011.FacturatieAppTypes.Facturatie FacturatieAppTeOntvangenFacturenUpdate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaFacturatieAppFacturatieLeverancierDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.FacturatieAppTypes.Facturatie result = rp.FacturatieAppTeOntvangenFacturenUpdate(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/FacturatieAppOpTeMakenCNAdd")]
        public EY.com.eBook.RuleEngine.AY2011.FacturatieAppTypes.Facturatie FacturatieAppOpTeMakenCNAdd(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaFacturatieAppFacturatieKlantCNDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.FacturatieAppTypes.Facturatie result = rp.FacturatieAppOpTeMakenCNAdd(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/FacturatieAppOpTeMakenCNRemove")]
        public EY.com.eBook.RuleEngine.AY2011.FacturatieAppTypes.Facturatie FacturatieAppOpTeMakenCNRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.FacturatieAppTypes.Facturatie result = rp.FacturatieAppOpTeMakenCNRemove(cfrdc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/FacturatieAppOpTeMakenCNUpdate")]
        public EY.com.eBook.RuleEngine.AY2011.FacturatieAppTypes.Facturatie FacturatieAppOpTeMakenCNUpdate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaFacturatieAppFacturatieKlantCNDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.FacturatieAppTypes.Facturatie result = rp.FacturatieAppOpTeMakenCNUpdate(cedc);
            rp.Close();
            return result;
        }

        #endregion

        #endregion



        #region OverlopendeRekeningenApp
        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/OverlopendeRekeningenAppGetData")]
        public EY.com.eBook.RuleEngine.AY2011.OverlopendeRekeningenAppTypes.OverlopendeRekeningen OverlopendeRekeningenAppGetData(CriteriaFileDataContract cfdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.OverlopendeRekeningenAppTypes.OverlopendeRekeningen result = rp.OverlopendeRekeningenAppGetData(cfdc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/OverlopendeRekeningenAppCalculate")]
        public EY.com.eBook.RuleEngine.AY2011.OverlopendeRekeningenAppTypes.OverlopendeRekeningen OverlopendeRekeningenAppCalculate(CriteriaFileDataContract cfdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.OverlopendeRekeningenAppTypes.OverlopendeRekeningen result = rp.OverlopendeRekeningenAppCalculate(cfdc);
            rp.Close();
            return result;
        }

        #region Entity validation


        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/OverlopendeRekeningenAppOverTeDragenKostValidate")]
        public List<FileMessageDataContract> OverlopendeRekeningenAppOverTeDragenKostValidate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaOverlopendeRekeningenAppOverTeDragenKostDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            List<FileMessageDataContract> result = rp.OverlopendeRekeningenAppOverTeDragenKostValidate(cedc);
            rp.Close();
            return result;
        }



        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/OverlopendeRekeningenAppVerkregenOpbrengstValidate")]
        public List<FileMessageDataContract> OverlopendeRekeningenAppVerkregenOpbrengstValidate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaOverlopendeRekeningenAppVerkregenOpbrengstDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            List<FileMessageDataContract> result = rp.OverlopendeRekeningenAppVerkregenOpbrengstValidate(cedc);
            rp.Close();
            return result;
        }



        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/OverlopendeRekeningenAppToeTeRekenenKostValidate")]
        public List<FileMessageDataContract> OverlopendeRekeningenAppToeTeRekenenKostValidate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaOverlopendeRekeningenAppToeTeRekenenKostDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            List<FileMessageDataContract> result = rp.OverlopendeRekeningenAppToeTeRekenenKostValidate(cedc);
            rp.Close();
            return result;
        }



        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/OverlopendeRekeningenAppOverTeDragenOpbrengstValidate")]
        public List<FileMessageDataContract> OverlopendeRekeningenAppOverTeDragenOpbrengstValidate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaOverlopendeRekeningenAppOverTeDragenOpbrengstDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            List<FileMessageDataContract> result = rp.OverlopendeRekeningenAppOverTeDragenOpbrengstValidate(cedc);
            rp.Close();
            return result;
        }



        #endregion



        #region Add/Remove


        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/OverlopendeRekeningenAppOverTeDragenKostenAdd")]
        public EY.com.eBook.RuleEngine.AY2011.OverlopendeRekeningenAppTypes.OverlopendeRekeningen OverlopendeRekeningenAppOverTeDragenKostenAdd(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaOverlopendeRekeningenAppOverTeDragenKostDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.OverlopendeRekeningenAppTypes.OverlopendeRekeningen result = rp.OverlopendeRekeningenAppOverTeDragenKostenAdd(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/OverlopendeRekeningenAppOverTeDragenKostenRemove")]
        public EY.com.eBook.RuleEngine.AY2011.OverlopendeRekeningenAppTypes.OverlopendeRekeningen OverlopendeRekeningenAppOverTeDragenKostenRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.OverlopendeRekeningenAppTypes.OverlopendeRekeningen result = rp.OverlopendeRekeningenAppOverTeDragenKostenRemove(cfrdc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/OverlopendeRekeningenAppOverTeDragenKostenUpdate")]
        public EY.com.eBook.RuleEngine.AY2011.OverlopendeRekeningenAppTypes.OverlopendeRekeningen OverlopendeRekeningenAppOverTeDragenKostenUpdate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaOverlopendeRekeningenAppOverTeDragenKostDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.OverlopendeRekeningenAppTypes.OverlopendeRekeningen result = rp.OverlopendeRekeningenAppOverTeDragenKostenUpdate(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/OverlopendeRekeningenAppVerkregenOpbrenstenAdd")]
        public EY.com.eBook.RuleEngine.AY2011.OverlopendeRekeningenAppTypes.OverlopendeRekeningen OverlopendeRekeningenAppVerkregenOpbrenstenAdd(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaOverlopendeRekeningenAppVerkregenOpbrengstDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.OverlopendeRekeningenAppTypes.OverlopendeRekeningen result = rp.OverlopendeRekeningenAppVerkregenOpbrenstenAdd(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/OverlopendeRekeningenAppVerkregenOpbrenstenRemove")]
        public EY.com.eBook.RuleEngine.AY2011.OverlopendeRekeningenAppTypes.OverlopendeRekeningen OverlopendeRekeningenAppVerkregenOpbrenstenRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.OverlopendeRekeningenAppTypes.OverlopendeRekeningen result = rp.OverlopendeRekeningenAppVerkregenOpbrenstenRemove(cfrdc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/OverlopendeRekeningenAppVerkregenOpbrenstenUpdate")]
        public EY.com.eBook.RuleEngine.AY2011.OverlopendeRekeningenAppTypes.OverlopendeRekeningen OverlopendeRekeningenAppVerkregenOpbrenstenUpdate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaOverlopendeRekeningenAppVerkregenOpbrengstDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.OverlopendeRekeningenAppTypes.OverlopendeRekeningen result = rp.OverlopendeRekeningenAppVerkregenOpbrenstenUpdate(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/OverlopendeRekeningenAppToeTeRekenenKostenAdd")]
        public EY.com.eBook.RuleEngine.AY2011.OverlopendeRekeningenAppTypes.OverlopendeRekeningen OverlopendeRekeningenAppToeTeRekenenKostenAdd(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaOverlopendeRekeningenAppToeTeRekenenKostDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.OverlopendeRekeningenAppTypes.OverlopendeRekeningen result = rp.OverlopendeRekeningenAppToeTeRekenenKostenAdd(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/OverlopendeRekeningenAppToeTeRekenenKostenRemove")]
        public EY.com.eBook.RuleEngine.AY2011.OverlopendeRekeningenAppTypes.OverlopendeRekeningen OverlopendeRekeningenAppToeTeRekenenKostenRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.OverlopendeRekeningenAppTypes.OverlopendeRekeningen result = rp.OverlopendeRekeningenAppToeTeRekenenKostenRemove(cfrdc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/OverlopendeRekeningenAppToeTeRekenenKostenUpdate")]
        public EY.com.eBook.RuleEngine.AY2011.OverlopendeRekeningenAppTypes.OverlopendeRekeningen OverlopendeRekeningenAppToeTeRekenenKostenUpdate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaOverlopendeRekeningenAppToeTeRekenenKostDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.OverlopendeRekeningenAppTypes.OverlopendeRekeningen result = rp.OverlopendeRekeningenAppToeTeRekenenKostenUpdate(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/OverlopendeRekeningenAppOverTeDragenOpbrengstenAdd")]
        public EY.com.eBook.RuleEngine.AY2011.OverlopendeRekeningenAppTypes.OverlopendeRekeningen OverlopendeRekeningenAppOverTeDragenOpbrengstenAdd(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaOverlopendeRekeningenAppOverTeDragenOpbrengstDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.OverlopendeRekeningenAppTypes.OverlopendeRekeningen result = rp.OverlopendeRekeningenAppOverTeDragenOpbrengstenAdd(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/OverlopendeRekeningenAppOverTeDragenOpbrengstenRemove")]
        public EY.com.eBook.RuleEngine.AY2011.OverlopendeRekeningenAppTypes.OverlopendeRekeningen OverlopendeRekeningenAppOverTeDragenOpbrengstenRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.OverlopendeRekeningenAppTypes.OverlopendeRekeningen result = rp.OverlopendeRekeningenAppOverTeDragenOpbrengstenRemove(cfrdc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/OverlopendeRekeningenAppOverTeDragenOpbrengstenUpdate")]
        public EY.com.eBook.RuleEngine.AY2011.OverlopendeRekeningenAppTypes.OverlopendeRekeningen OverlopendeRekeningenAppOverTeDragenOpbrengstenUpdate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaOverlopendeRekeningenAppOverTeDragenOpbrengstDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.OverlopendeRekeningenAppTypes.OverlopendeRekeningen result = rp.OverlopendeRekeningenAppOverTeDragenOpbrengstenUpdate(cedc);
            rp.Close();
            return result;
        }

        #endregion

        #endregion



        #region VergelijkendeCijfersApp
        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/VergelijkendeCijfersAppGetData")]
        public EY.com.eBook.RuleEngine.AY2011.VergelijkendeCijfersAppTypes.WPVergelijkendeCijfers VergelijkendeCijfersAppGetData(CriteriaFileDataContract cfdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.VergelijkendeCijfersAppTypes.WPVergelijkendeCijfers result = rp.VergelijkendeCijfersAppGetData(cfdc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/VergelijkendeCijfersAppCalculate")]
        public EY.com.eBook.RuleEngine.AY2011.VergelijkendeCijfersAppTypes.WPVergelijkendeCijfers VergelijkendeCijfersAppCalculate(CriteriaFileDataContract cfdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.VergelijkendeCijfersAppTypes.WPVergelijkendeCijfers result = rp.VergelijkendeCijfersAppCalculate(cfdc);
            rp.Close();
            return result;
        }

        #region Entity validation


        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/VergelijkendeCijfersAppCijferBlokValidate")]
        public List<FileMessageDataContract> VergelijkendeCijfersAppCijferBlokValidate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaVergelijkendeCijfersAppCijferBlokDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            List<FileMessageDataContract> result = rp.VergelijkendeCijfersAppCijferBlokValidate(cedc);
            rp.Close();
            return result;
        }



        #endregion



        #region Add/Remove


        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/VergelijkendeCijfersAppKostenAdd")]
        public EY.com.eBook.RuleEngine.AY2011.VergelijkendeCijfersAppTypes.WPVergelijkendeCijfers VergelijkendeCijfersAppKostenAdd(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaVergelijkendeCijfersAppCijferBlokDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.VergelijkendeCijfersAppTypes.WPVergelijkendeCijfers result = rp.VergelijkendeCijfersAppKostenAdd(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/VergelijkendeCijfersAppKostenRemove")]
        public EY.com.eBook.RuleEngine.AY2011.VergelijkendeCijfersAppTypes.WPVergelijkendeCijfers VergelijkendeCijfersAppKostenRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.VergelijkendeCijfersAppTypes.WPVergelijkendeCijfers result = rp.VergelijkendeCijfersAppKostenRemove(cfrdc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/VergelijkendeCijfersAppKostenUpdate")]
        public EY.com.eBook.RuleEngine.AY2011.VergelijkendeCijfersAppTypes.WPVergelijkendeCijfers VergelijkendeCijfersAppKostenUpdate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaVergelijkendeCijfersAppCijferBlokDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.VergelijkendeCijfersAppTypes.WPVergelijkendeCijfers result = rp.VergelijkendeCijfersAppKostenUpdate(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/VergelijkendeCijfersAppOpbrengstenAdd")]
        public EY.com.eBook.RuleEngine.AY2011.VergelijkendeCijfersAppTypes.WPVergelijkendeCijfers VergelijkendeCijfersAppOpbrengstenAdd(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaVergelijkendeCijfersAppCijferBlokDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.VergelijkendeCijfersAppTypes.WPVergelijkendeCijfers result = rp.VergelijkendeCijfersAppOpbrengstenAdd(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/VergelijkendeCijfersAppOpbrengstenRemove")]
        public EY.com.eBook.RuleEngine.AY2011.VergelijkendeCijfersAppTypes.WPVergelijkendeCijfers VergelijkendeCijfersAppOpbrengstenRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.VergelijkendeCijfersAppTypes.WPVergelijkendeCijfers result = rp.VergelijkendeCijfersAppOpbrengstenRemove(cfrdc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/VergelijkendeCijfersAppOpbrengstenUpdate")]
        public EY.com.eBook.RuleEngine.AY2011.VergelijkendeCijfersAppTypes.WPVergelijkendeCijfers VergelijkendeCijfersAppOpbrengstenUpdate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaVergelijkendeCijfersAppCijferBlokDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.VergelijkendeCijfersAppTypes.WPVergelijkendeCijfers result = rp.VergelijkendeCijfersAppOpbrengstenUpdate(cedc);
            rp.Close();
            return result;
        }

        #endregion

        #endregion



        #region VoorschottenApp
        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/VoorschottenAppGetData")]
        public EY.com.eBook.RuleEngine.AY2011.VoorschottenAppTypes.Voorschotten VoorschottenAppGetData(CriteriaFileDataContract cfdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.VoorschottenAppTypes.Voorschotten result = rp.VoorschottenAppGetData(cfdc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/VoorschottenAppCalculate")]
        public EY.com.eBook.RuleEngine.AY2011.VoorschottenAppTypes.Voorschotten VoorschottenAppCalculate(CriteriaFileDataContract cfdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.VoorschottenAppTypes.Voorschotten result = rp.VoorschottenAppCalculate(cfdc);
            rp.Close();
            return result;
        }

        #region Entity validation


        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/VoorschottenAppGegevenValidate")]
        public List<FileMessageDataContract> VoorschottenAppGegevenValidate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaVoorschottenAppGegevenDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            List<FileMessageDataContract> result = rp.VoorschottenAppGegevenValidate(cedc);
            rp.Close();
            return result;
        }



        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/VoorschottenAppIntrestVoetValidate")]
        public List<FileMessageDataContract> VoorschottenAppIntrestVoetValidate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaVoorschottenAppIntrestVoetDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            List<FileMessageDataContract> result = rp.VoorschottenAppIntrestVoetValidate(cedc);
            rp.Close();
            return result;
        }



        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/VoorschottenAppRentegevendVoorschotValidate")]
        public List<FileMessageDataContract> VoorschottenAppRentegevendVoorschotValidate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaVoorschottenAppRentegevendVoorschotDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            List<FileMessageDataContract> result = rp.VoorschottenAppRentegevendVoorschotValidate(cedc);
            rp.Close();
            return result;
        }



        #endregion



        #region Add/Remove


        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/VoorschottenAppGegevensAdd")]
        public EY.com.eBook.RuleEngine.AY2011.VoorschottenAppTypes.Voorschotten VoorschottenAppGegevensAdd(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaVoorschottenAppGegevenDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.VoorschottenAppTypes.Voorschotten result = rp.VoorschottenAppGegevensAdd(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/VoorschottenAppGegevensRemove")]
        public EY.com.eBook.RuleEngine.AY2011.VoorschottenAppTypes.Voorschotten VoorschottenAppGegevensRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.VoorschottenAppTypes.Voorschotten result = rp.VoorschottenAppGegevensRemove(cfrdc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/VoorschottenAppGegevensUpdate")]
        public EY.com.eBook.RuleEngine.AY2011.VoorschottenAppTypes.Voorschotten VoorschottenAppGegevensUpdate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaVoorschottenAppGegevenDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.VoorschottenAppTypes.Voorschotten result = rp.VoorschottenAppGegevensUpdate(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/VoorschottenAppInterestVoetenAdd")]
        public EY.com.eBook.RuleEngine.AY2011.VoorschottenAppTypes.Voorschotten VoorschottenAppInterestVoetenAdd(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaVoorschottenAppIntrestVoetDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.VoorschottenAppTypes.Voorschotten result = rp.VoorschottenAppInterestVoetenAdd(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/VoorschottenAppInterestVoetenRemove")]
        public EY.com.eBook.RuleEngine.AY2011.VoorschottenAppTypes.Voorschotten VoorschottenAppInterestVoetenRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.VoorschottenAppTypes.Voorschotten result = rp.VoorschottenAppInterestVoetenRemove(cfrdc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/VoorschottenAppInterestVoetenUpdate")]
        public EY.com.eBook.RuleEngine.AY2011.VoorschottenAppTypes.Voorschotten VoorschottenAppInterestVoetenUpdate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaVoorschottenAppIntrestVoetDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.VoorschottenAppTypes.Voorschotten result = rp.VoorschottenAppInterestVoetenUpdate(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/VoorschottenAppRentegevendeVoorschottenAdd")]
        public EY.com.eBook.RuleEngine.AY2011.VoorschottenAppTypes.Voorschotten VoorschottenAppRentegevendeVoorschottenAdd(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaVoorschottenAppRentegevendVoorschotDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.VoorschottenAppTypes.Voorschotten result = rp.VoorschottenAppRentegevendeVoorschottenAdd(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/VoorschottenAppRentegevendeVoorschottenRemove")]
        public EY.com.eBook.RuleEngine.AY2011.VoorschottenAppTypes.Voorschotten VoorschottenAppRentegevendeVoorschottenRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.VoorschottenAppTypes.Voorschotten result = rp.VoorschottenAppRentegevendeVoorschottenRemove(cfrdc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/VoorschottenAppRentegevendeVoorschottenUpdate")]
        public EY.com.eBook.RuleEngine.AY2011.VoorschottenAppTypes.Voorschotten VoorschottenAppRentegevendeVoorschottenUpdate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaVoorschottenAppRentegevendVoorschotDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.VoorschottenAppTypes.Voorschotten result = rp.VoorschottenAppRentegevendeVoorschottenUpdate(cedc);
            rp.Close();
            return result;
        }

        #endregion

        #endregion



        #region ErelonenHuurApp
        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/ErelonenHuurAppGetData")]
        public EY.com.eBook.RuleEngine.AY2011.ErelonenHuurAppTypes.ErelonenHuur ErelonenHuurAppGetData(CriteriaFileDataContract cfdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.ErelonenHuurAppTypes.ErelonenHuur result = rp.ErelonenHuurAppGetData(cfdc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/ErelonenHuurAppCalculate")]
        public EY.com.eBook.RuleEngine.AY2011.ErelonenHuurAppTypes.ErelonenHuur ErelonenHuurAppCalculate(CriteriaFileDataContract cfdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.ErelonenHuurAppTypes.ErelonenHuur result = rp.ErelonenHuurAppCalculate(cfdc);
            rp.Close();
            return result;
        }

        #region Entity validation


        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/ErelonenHuurAppImportItemValidate")]
        public List<FileMessageDataContract> ErelonenHuurAppImportItemValidate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaErelonenHuurAppImportItemDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            List<FileMessageDataContract> result = rp.ErelonenHuurAppImportItemValidate(cedc);
            rp.Close();
            return result;
        }



        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/ErelonenHuurAppImportedHistoryValidate")]
        public List<FileMessageDataContract> ErelonenHuurAppImportedHistoryValidate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaErelonenHuurAppImportedHistoryDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            List<FileMessageDataContract> result = rp.ErelonenHuurAppImportedHistoryValidate(cedc);
            rp.Close();
            return result;
        }



        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/ErelonenHuurAppVerdelingHistoriekValidate")]
        public List<FileMessageDataContract> ErelonenHuurAppVerdelingHistoriekValidate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaErelonenHuurAppVerdelingHistoriekDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            List<FileMessageDataContract> result = rp.ErelonenHuurAppVerdelingHistoriekValidate(cedc);
            rp.Close();
            return result;
        }



        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/ErelonenHuurAppFiche281_50Validate")]
        public List<FileMessageDataContract> ErelonenHuurAppFiche281_50Validate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaErelonenHuurAppFiche281_50DataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            List<FileMessageDataContract> result = rp.ErelonenHuurAppFiche281_50Validate(cedc);
            rp.Close();
            return result;
        }



        #endregion



        #region Add/Remove


        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/ErelonenHuurAppHistoriekAdd")]
        public EY.com.eBook.RuleEngine.AY2011.ErelonenHuurAppTypes.ErelonenHuur ErelonenHuurAppHistoriekAdd(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaErelonenHuurAppVerdelingHistoriekDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.ErelonenHuurAppTypes.ErelonenHuur result = rp.ErelonenHuurAppHistoriekAdd(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/ErelonenHuurAppHistoriekRemove")]
        public EY.com.eBook.RuleEngine.AY2011.ErelonenHuurAppTypes.ErelonenHuur ErelonenHuurAppHistoriekRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.ErelonenHuurAppTypes.ErelonenHuur result = rp.ErelonenHuurAppHistoriekRemove(cfrdc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/ErelonenHuurAppHistoriekUpdate")]
        public EY.com.eBook.RuleEngine.AY2011.ErelonenHuurAppTypes.ErelonenHuur ErelonenHuurAppHistoriekUpdate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaErelonenHuurAppVerdelingHistoriekDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.ErelonenHuurAppTypes.ErelonenHuur result = rp.ErelonenHuurAppHistoriekUpdate(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/ErelonenHuurAppFiches281_50Update")]
        public EY.com.eBook.RuleEngine.AY2011.ErelonenHuurAppTypes.ErelonenHuur ErelonenHuurAppFiches281_50Update(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaErelonenHuurAppFiche281_50DataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.ErelonenHuurAppTypes.ErelonenHuur result = rp.ErelonenHuurAppFiches281_50Update(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/ErelonenHuurAppFiche281_50ColAdd")]
        public EY.com.eBook.RuleEngine.AY2011.ErelonenHuurAppTypes.ErelonenHuur ErelonenHuurAppFiche281_50ColAdd(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaErelonenHuurAppFiche281_50DataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.ErelonenHuurAppTypes.ErelonenHuur result = rp.ErelonenHuurAppFiche281_50ColAdd(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/ErelonenHuurAppFiche281_50ColRemove")]
        public EY.com.eBook.RuleEngine.AY2011.ErelonenHuurAppTypes.ErelonenHuur ErelonenHuurAppFiche281_50ColRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.ErelonenHuurAppTypes.ErelonenHuur result = rp.ErelonenHuurAppFiche281_50ColRemove(cfrdc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/ErelonenHuurAppFiche281_50ColUpdate")]
        public EY.com.eBook.RuleEngine.AY2011.ErelonenHuurAppTypes.ErelonenHuur ErelonenHuurAppFiche281_50ColUpdate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaErelonenHuurAppFiche281_50DataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.ErelonenHuurAppTypes.ErelonenHuur result = rp.ErelonenHuurAppFiche281_50ColUpdate(cedc);
            rp.Close();
            return result;
        }

        #endregion

        #endregion



        #region VoordelenApp
        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/VoordelenAppGetData")]
        public EY.com.eBook.RuleEngine.AY2011.VoordelenAppTypes.Voordelen VoordelenAppGetData(CriteriaFileDataContract cfdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.VoordelenAppTypes.Voordelen result = rp.VoordelenAppGetData(cfdc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/VoordelenAppCalculate")]
        public EY.com.eBook.RuleEngine.AY2011.VoordelenAppTypes.Voordelen VoordelenAppCalculate(CriteriaFileDataContract cfdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.VoordelenAppTypes.Voordelen result = rp.VoordelenAppCalculate(cfdc);
            rp.Close();
            return result;
        }

        #region Entity validation


        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/VoordelenAppVAA_AutoValidate")]
        public List<FileMessageDataContract> VoordelenAppVAA_AutoValidate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaVoordelenAppVAA_AutoDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            List<FileMessageDataContract> result = rp.VoordelenAppVAA_AutoValidate(cedc);
            rp.Close();
            return result;
        }



        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/VoordelenAppVAA_AutoCO2Validate")]
        public List<FileMessageDataContract> VoordelenAppVAA_AutoCO2Validate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaVoordelenAppVAA_AutoCO2DataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            List<FileMessageDataContract> result = rp.VoordelenAppVAA_AutoCO2Validate(cedc);
            rp.Close();
            return result;
        }



        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/VoordelenAppVAA_NutsValidate")]
        public List<FileMessageDataContract> VoordelenAppVAA_NutsValidate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaVoordelenAppVAA_NutsDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            List<FileMessageDataContract> result = rp.VoordelenAppVAA_NutsValidate(cedc);
            rp.Close();
            return result;
        }



        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/VoordelenAppVAA_HUURValidate")]
        public List<FileMessageDataContract> VoordelenAppVAA_HUURValidate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaVoordelenAppVAA_HUURDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            List<FileMessageDataContract> result = rp.VoordelenAppVAA_HUURValidate(cedc);
            rp.Close();
            return result;
        }



        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/VoordelenAppVAA_HUURHERKValidate")]
        public List<FileMessageDataContract> VoordelenAppVAA_HUURHERKValidate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaVoordelenAppVAA_HUURHERKDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            List<FileMessageDataContract> result = rp.VoordelenAppVAA_HUURHERKValidate(cedc);
            rp.Close();
            return result;
        }



        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/VoordelenAppVAA_PCValidate")]
        public List<FileMessageDataContract> VoordelenAppVAA_PCValidate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaVoordelenAppVAA_PCDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            List<FileMessageDataContract> result = rp.VoordelenAppVAA_PCValidate(cedc);
            rp.Close();
            return result;
        }



        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/VoordelenAppVAA_TelefonieValidate")]
        public List<FileMessageDataContract> VoordelenAppVAA_TelefonieValidate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaVoordelenAppVAA_TelefonieDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            List<FileMessageDataContract> result = rp.VoordelenAppVAA_TelefonieValidate(cedc);
            rp.Close();
            return result;
        }



        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/VoordelenAppVAA_OverigeValidate")]
        public List<FileMessageDataContract> VoordelenAppVAA_OverigeValidate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaVoordelenAppVAA_OverigeDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            List<FileMessageDataContract> result = rp.VoordelenAppVAA_OverigeValidate(cedc);
            rp.Close();
            return result;
        }



        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/VoordelenAppVAA_Interesten_RC_BedrijfsleiderValidate")]
        public List<FileMessageDataContract> VoordelenAppVAA_Interesten_RC_BedrijfsleiderValidate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaVoordelenAppVAA_Interesten_RC_BedrijfsleiderDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            List<FileMessageDataContract> result = rp.VoordelenAppVAA_Interesten_RC_BedrijfsleiderValidate(cedc);
            rp.Close();
            return result;
        }



        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/VoordelenAppVAA_Soc_Bijdr_BedrijfsleiderValidate")]
        public List<FileMessageDataContract> VoordelenAppVAA_Soc_Bijdr_BedrijfsleiderValidate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaVoordelenAppVAA_Soc_Bijdr_BedrijfsleiderDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            List<FileMessageDataContract> result = rp.VoordelenAppVAA_Soc_Bijdr_BedrijfsleiderValidate(cedc);
            rp.Close();
            return result;
        }



        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/VoordelenAppVAA_Overige_NutsValidate")]
        public List<FileMessageDataContract> VoordelenAppVAA_Overige_NutsValidate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaVoordelenAppVAA_Overige_NutsDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            List<FileMessageDataContract> result = rp.VoordelenAppVAA_Overige_NutsValidate(cedc);
            rp.Close();
            return result;
        }



        #endregion



        #region Add/Remove


        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/VoordelenAppVoordelenVanAlleAardAutoAdd")]
        public EY.com.eBook.RuleEngine.AY2011.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardAutoAdd(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaVoordelenAppVAA_AutoDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.VoordelenAppTypes.Voordelen result = rp.VoordelenAppVoordelenVanAlleAardAutoAdd(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/VoordelenAppVoordelenVanAlleAardAutoRemove")]
        public EY.com.eBook.RuleEngine.AY2011.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardAutoRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.VoordelenAppTypes.Voordelen result = rp.VoordelenAppVoordelenVanAlleAardAutoRemove(cfrdc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/VoordelenAppVoordelenVanAlleAardAutoUpdate")]
        public EY.com.eBook.RuleEngine.AY2011.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardAutoUpdate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaVoordelenAppVAA_AutoDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.VoordelenAppTypes.Voordelen result = rp.VoordelenAppVoordelenVanAlleAardAutoUpdate(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/VoordelenAppVoordelenVanAlleAardAutoCO2Add")]
        public EY.com.eBook.RuleEngine.AY2011.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardAutoCO2Add(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaVoordelenAppVAA_AutoCO2DataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.VoordelenAppTypes.Voordelen result = rp.VoordelenAppVoordelenVanAlleAardAutoCO2Add(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/VoordelenAppVoordelenVanAlleAardAutoCO2Remove")]
        public EY.com.eBook.RuleEngine.AY2011.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardAutoCO2Remove(CriteriaFileAndRowDataContract cfrdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.VoordelenAppTypes.Voordelen result = rp.VoordelenAppVoordelenVanAlleAardAutoCO2Remove(cfrdc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/VoordelenAppVoordelenVanAlleAardAutoCO2Update")]
        public EY.com.eBook.RuleEngine.AY2011.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardAutoCO2Update(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaVoordelenAppVAA_AutoCO2DataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.VoordelenAppTypes.Voordelen result = rp.VoordelenAppVoordelenVanAlleAardAutoCO2Update(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/VoordelenAppVoordelenVanAlleAardNutsAdd")]
        public EY.com.eBook.RuleEngine.AY2011.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardNutsAdd(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaVoordelenAppVAA_NutsDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.VoordelenAppTypes.Voordelen result = rp.VoordelenAppVoordelenVanAlleAardNutsAdd(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/VoordelenAppVoordelenVanAlleAardNutsRemove")]
        public EY.com.eBook.RuleEngine.AY2011.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardNutsRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.VoordelenAppTypes.Voordelen result = rp.VoordelenAppVoordelenVanAlleAardNutsRemove(cfrdc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/VoordelenAppVoordelenVanAlleAardNutsUpdate")]
        public EY.com.eBook.RuleEngine.AY2011.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardNutsUpdate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaVoordelenAppVAA_NutsDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.VoordelenAppTypes.Voordelen result = rp.VoordelenAppVoordelenVanAlleAardNutsUpdate(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/VoordelenAppVoordelenVanAlleAardHuurAdd")]
        public EY.com.eBook.RuleEngine.AY2011.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardHuurAdd(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaVoordelenAppVAA_HUURDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.VoordelenAppTypes.Voordelen result = rp.VoordelenAppVoordelenVanAlleAardHuurAdd(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/VoordelenAppVoordelenVanAlleAardHuurRemove")]
        public EY.com.eBook.RuleEngine.AY2011.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardHuurRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.VoordelenAppTypes.Voordelen result = rp.VoordelenAppVoordelenVanAlleAardHuurRemove(cfrdc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/VoordelenAppVoordelenVanAlleAardHuurUpdate")]
        public EY.com.eBook.RuleEngine.AY2011.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardHuurUpdate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaVoordelenAppVAA_HUURDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.VoordelenAppTypes.Voordelen result = rp.VoordelenAppVoordelenVanAlleAardHuurUpdate(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/VoordelenAppVoordelenVanAlleAardHuurHerkwalificatieAdd")]
        public EY.com.eBook.RuleEngine.AY2011.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardHuurHerkwalificatieAdd(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaVoordelenAppVAA_HUURHERKDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.VoordelenAppTypes.Voordelen result = rp.VoordelenAppVoordelenVanAlleAardHuurHerkwalificatieAdd(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/VoordelenAppVoordelenVanAlleAardHuurHerkwalificatieRemove")]
        public EY.com.eBook.RuleEngine.AY2011.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardHuurHerkwalificatieRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.VoordelenAppTypes.Voordelen result = rp.VoordelenAppVoordelenVanAlleAardHuurHerkwalificatieRemove(cfrdc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/VoordelenAppVoordelenVanAlleAardHuurHerkwalificatieUpdate")]
        public EY.com.eBook.RuleEngine.AY2011.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardHuurHerkwalificatieUpdate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaVoordelenAppVAA_HUURHERKDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.VoordelenAppTypes.Voordelen result = rp.VoordelenAppVoordelenVanAlleAardHuurHerkwalificatieUpdate(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/VoordelenAppVoordelenVanAlleAardPCAdd")]
        public EY.com.eBook.RuleEngine.AY2011.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardPCAdd(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaVoordelenAppVAA_PCDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.VoordelenAppTypes.Voordelen result = rp.VoordelenAppVoordelenVanAlleAardPCAdd(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/VoordelenAppVoordelenVanAlleAardPCRemove")]
        public EY.com.eBook.RuleEngine.AY2011.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardPCRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.VoordelenAppTypes.Voordelen result = rp.VoordelenAppVoordelenVanAlleAardPCRemove(cfrdc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/VoordelenAppVoordelenVanAlleAardPCUpdate")]
        public EY.com.eBook.RuleEngine.AY2011.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardPCUpdate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaVoordelenAppVAA_PCDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.VoordelenAppTypes.Voordelen result = rp.VoordelenAppVoordelenVanAlleAardPCUpdate(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/VoordelenAppVoordelenVanAlleAardTelefonieAdd")]
        public EY.com.eBook.RuleEngine.AY2011.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardTelefonieAdd(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaVoordelenAppVAA_TelefonieDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.VoordelenAppTypes.Voordelen result = rp.VoordelenAppVoordelenVanAlleAardTelefonieAdd(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/VoordelenAppVoordelenVanAlleAardTelefonieRemove")]
        public EY.com.eBook.RuleEngine.AY2011.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardTelefonieRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.VoordelenAppTypes.Voordelen result = rp.VoordelenAppVoordelenVanAlleAardTelefonieRemove(cfrdc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/VoordelenAppVoordelenVanAlleAardTelefonieUpdate")]
        public EY.com.eBook.RuleEngine.AY2011.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardTelefonieUpdate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaVoordelenAppVAA_TelefonieDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.VoordelenAppTypes.Voordelen result = rp.VoordelenAppVoordelenVanAlleAardTelefonieUpdate(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/VoordelenAppVoordelenVanAlleAardOverigeAdd")]
        public EY.com.eBook.RuleEngine.AY2011.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardOverigeAdd(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaVoordelenAppVAA_OverigeDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.VoordelenAppTypes.Voordelen result = rp.VoordelenAppVoordelenVanAlleAardOverigeAdd(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/VoordelenAppVoordelenVanAlleAardOverigeRemove")]
        public EY.com.eBook.RuleEngine.AY2011.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardOverigeRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.VoordelenAppTypes.Voordelen result = rp.VoordelenAppVoordelenVanAlleAardOverigeRemove(cfrdc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/VoordelenAppVoordelenVanAlleAardOverigeUpdate")]
        public EY.com.eBook.RuleEngine.AY2011.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardOverigeUpdate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaVoordelenAppVAA_OverigeDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.VoordelenAppTypes.Voordelen result = rp.VoordelenAppVoordelenVanAlleAardOverigeUpdate(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/VoordelenAppVoordelenInterestenRCBedrijfsleiderAdd")]
        public EY.com.eBook.RuleEngine.AY2011.VoordelenAppTypes.Voordelen VoordelenAppVoordelenInterestenRCBedrijfsleiderAdd(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaVoordelenAppVAA_Interesten_RC_BedrijfsleiderDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.VoordelenAppTypes.Voordelen result = rp.VoordelenAppVoordelenInterestenRCBedrijfsleiderAdd(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/VoordelenAppVoordelenInterestenRCBedrijfsleiderRemove")]
        public EY.com.eBook.RuleEngine.AY2011.VoordelenAppTypes.Voordelen VoordelenAppVoordelenInterestenRCBedrijfsleiderRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.VoordelenAppTypes.Voordelen result = rp.VoordelenAppVoordelenInterestenRCBedrijfsleiderRemove(cfrdc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/VoordelenAppVoordelenInterestenRCBedrijfsleiderUpdate")]
        public EY.com.eBook.RuleEngine.AY2011.VoordelenAppTypes.Voordelen VoordelenAppVoordelenInterestenRCBedrijfsleiderUpdate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaVoordelenAppVAA_Interesten_RC_BedrijfsleiderDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.VoordelenAppTypes.Voordelen result = rp.VoordelenAppVoordelenInterestenRCBedrijfsleiderUpdate(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/VoordelenAppVoordelenSocialeBijdragenBedrijfsleiderAdd")]
        public EY.com.eBook.RuleEngine.AY2011.VoordelenAppTypes.Voordelen VoordelenAppVoordelenSocialeBijdragenBedrijfsleiderAdd(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaVoordelenAppVAA_Soc_Bijdr_BedrijfsleiderDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.VoordelenAppTypes.Voordelen result = rp.VoordelenAppVoordelenSocialeBijdragenBedrijfsleiderAdd(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/VoordelenAppVoordelenSocialeBijdragenBedrijfsleiderRemove")]
        public EY.com.eBook.RuleEngine.AY2011.VoordelenAppTypes.Voordelen VoordelenAppVoordelenSocialeBijdragenBedrijfsleiderRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.VoordelenAppTypes.Voordelen result = rp.VoordelenAppVoordelenSocialeBijdragenBedrijfsleiderRemove(cfrdc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/VoordelenAppVoordelenSocialeBijdragenBedrijfsleiderUpdate")]
        public EY.com.eBook.RuleEngine.AY2011.VoordelenAppTypes.Voordelen VoordelenAppVoordelenSocialeBijdragenBedrijfsleiderUpdate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaVoordelenAppVAA_Soc_Bijdr_BedrijfsleiderDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.VoordelenAppTypes.Voordelen result = rp.VoordelenAppVoordelenSocialeBijdragenBedrijfsleiderUpdate(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/VoordelenAppVoordelenOverigeNutsAdd")]
        public EY.com.eBook.RuleEngine.AY2011.VoordelenAppTypes.Voordelen VoordelenAppVoordelenOverigeNutsAdd(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaVoordelenAppVAA_Overige_NutsDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.VoordelenAppTypes.Voordelen result = rp.VoordelenAppVoordelenOverigeNutsAdd(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/VoordelenAppVoordelenOverigeNutsRemove")]
        public EY.com.eBook.RuleEngine.AY2011.VoordelenAppTypes.Voordelen VoordelenAppVoordelenOverigeNutsRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.VoordelenAppTypes.Voordelen result = rp.VoordelenAppVoordelenOverigeNutsRemove(cfrdc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/VoordelenAppVoordelenOverigeNutsUpdate")]
        public EY.com.eBook.RuleEngine.AY2011.VoordelenAppTypes.Voordelen VoordelenAppVoordelenOverigeNutsUpdate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaVoordelenAppVAA_Overige_NutsDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.VoordelenAppTypes.Voordelen result = rp.VoordelenAppVoordelenOverigeNutsUpdate(cedc);
            rp.Close();
            return result;
        }

        #endregion

        #endregion



        #region PersoneelApp
        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/PersoneelAppGetData")]
        public EY.com.eBook.RuleEngine.AY2011.PersoneelAppTypes.WPPersoneel PersoneelAppGetData(CriteriaFileDataContract cfdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.PersoneelAppTypes.WPPersoneel result = rp.PersoneelAppGetData(cfdc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/PersoneelAppCalculate")]
        public EY.com.eBook.RuleEngine.AY2011.PersoneelAppTypes.WPPersoneel PersoneelAppCalculate(CriteriaFileDataContract cfdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.PersoneelAppTypes.WPPersoneel result = rp.PersoneelAppCalculate(cfdc);
            rp.Close();
            return result;
        }

        #region Entity validation


        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/PersoneelAppItem325Validate")]
        public List<FileMessageDataContract> PersoneelAppItem325Validate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaPersoneelAppItem325DataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            List<FileMessageDataContract> result = rp.PersoneelAppItem325Validate(cedc);
            rp.Close();
            return result;
        }



        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/PersoneelAppVerschilItemValidate")]
        public List<FileMessageDataContract> PersoneelAppVerschilItemValidate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaPersoneelAppVerschilItemDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            List<FileMessageDataContract> result = rp.PersoneelAppVerschilItemValidate(cedc);
            rp.Close();
            return result;
        }



        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/PersoneelAppWerknemerValidate")]
        public List<FileMessageDataContract> PersoneelAppWerknemerValidate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaPersoneelAppWerknemerDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            List<FileMessageDataContract> result = rp.PersoneelAppWerknemerValidate(cedc);
            rp.Close();
            return result;
        }



        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/PersoneelAppAlgemeenValidate")]
        public List<FileMessageDataContract> PersoneelAppAlgemeenValidate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaPersoneelAppAlgemeenDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            List<FileMessageDataContract> result = rp.PersoneelAppAlgemeenValidate(cedc);
            rp.Close();
            return result;
        }



        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/PersoneelAppVakantiegeldValidate")]
        public List<FileMessageDataContract> PersoneelAppVakantiegeldValidate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaPersoneelAppVakantiegeldDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            List<FileMessageDataContract> result = rp.PersoneelAppVakantiegeldValidate(cedc);
            rp.Close();
            return result;
        }



        #endregion



        #region Add/Remove


        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/PersoneelApp_325_20CollAdd")]
        public EY.com.eBook.RuleEngine.AY2011.PersoneelAppTypes.WPPersoneel PersoneelApp_325_20CollAdd(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaPersoneelAppItem325DataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.PersoneelAppTypes.WPPersoneel result = rp.PersoneelApp_325_20CollAdd(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/PersoneelApp_325_20CollRemove")]
        public EY.com.eBook.RuleEngine.AY2011.PersoneelAppTypes.WPPersoneel PersoneelApp_325_20CollRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.PersoneelAppTypes.WPPersoneel result = rp.PersoneelApp_325_20CollRemove(cfrdc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/PersoneelApp_325_20CollUpdate")]
        public EY.com.eBook.RuleEngine.AY2011.PersoneelAppTypes.WPPersoneel PersoneelApp_325_20CollUpdate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaPersoneelAppItem325DataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.PersoneelAppTypes.WPPersoneel result = rp.PersoneelApp_325_20CollUpdate(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/PersoneelAppTeVerklarenVerschil325_20Add")]
        public EY.com.eBook.RuleEngine.AY2011.PersoneelAppTypes.WPPersoneel PersoneelAppTeVerklarenVerschil325_20Add(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaPersoneelAppVerschilItemDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.PersoneelAppTypes.WPPersoneel result = rp.PersoneelAppTeVerklarenVerschil325_20Add(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/PersoneelAppTeVerklarenVerschil325_20Remove")]
        public EY.com.eBook.RuleEngine.AY2011.PersoneelAppTypes.WPPersoneel PersoneelAppTeVerklarenVerschil325_20Remove(CriteriaFileAndRowDataContract cfrdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.PersoneelAppTypes.WPPersoneel result = rp.PersoneelAppTeVerklarenVerschil325_20Remove(cfrdc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/PersoneelAppTeVerklarenVerschil325_20Update")]
        public EY.com.eBook.RuleEngine.AY2011.PersoneelAppTypes.WPPersoneel PersoneelAppTeVerklarenVerschil325_20Update(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaPersoneelAppVerschilItemDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.PersoneelAppTypes.WPPersoneel result = rp.PersoneelAppTeVerklarenVerschil325_20Update(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/PersoneelApp_325_10CollAdd")]
        public EY.com.eBook.RuleEngine.AY2011.PersoneelAppTypes.WPPersoneel PersoneelApp_325_10CollAdd(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaPersoneelAppItem325DataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.PersoneelAppTypes.WPPersoneel result = rp.PersoneelApp_325_10CollAdd(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/PersoneelApp_325_10CollRemove")]
        public EY.com.eBook.RuleEngine.AY2011.PersoneelAppTypes.WPPersoneel PersoneelApp_325_10CollRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.PersoneelAppTypes.WPPersoneel result = rp.PersoneelApp_325_10CollRemove(cfrdc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/PersoneelApp_325_10CollUpdate")]
        public EY.com.eBook.RuleEngine.AY2011.PersoneelAppTypes.WPPersoneel PersoneelApp_325_10CollUpdate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaPersoneelAppItem325DataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.PersoneelAppTypes.WPPersoneel result = rp.PersoneelApp_325_10CollUpdate(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/PersoneelAppTeVerklarenVerschil325_10Add")]
        public EY.com.eBook.RuleEngine.AY2011.PersoneelAppTypes.WPPersoneel PersoneelAppTeVerklarenVerschil325_10Add(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaPersoneelAppVerschilItemDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.PersoneelAppTypes.WPPersoneel result = rp.PersoneelAppTeVerklarenVerschil325_10Add(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/PersoneelAppTeVerklarenVerschil325_10Remove")]
        public EY.com.eBook.RuleEngine.AY2011.PersoneelAppTypes.WPPersoneel PersoneelAppTeVerklarenVerschil325_10Remove(CriteriaFileAndRowDataContract cfrdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.PersoneelAppTypes.WPPersoneel result = rp.PersoneelAppTeVerklarenVerschil325_10Remove(cfrdc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/PersoneelAppTeVerklarenVerschil325_10Update")]
        public EY.com.eBook.RuleEngine.AY2011.PersoneelAppTypes.WPPersoneel PersoneelAppTeVerklarenVerschil325_10Update(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaPersoneelAppVerschilItemDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.PersoneelAppTypes.WPPersoneel result = rp.PersoneelAppTeVerklarenVerschil325_10Update(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/PersoneelAppArbeidersFormUpdate")]
        public EY.com.eBook.RuleEngine.AY2011.PersoneelAppTypes.WPPersoneel PersoneelAppArbeidersFormUpdate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaPersoneelAppWerknemerDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.PersoneelAppTypes.WPPersoneel result = rp.PersoneelAppArbeidersFormUpdate(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/PersoneelAppBediendeFormUpdate")]
        public EY.com.eBook.RuleEngine.AY2011.PersoneelAppTypes.WPPersoneel PersoneelAppBediendeFormUpdate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaPersoneelAppWerknemerDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.PersoneelAppTypes.WPPersoneel result = rp.PersoneelAppBediendeFormUpdate(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/PersoneelAppAansluitingArbeidersAdd")]
        public EY.com.eBook.RuleEngine.AY2011.PersoneelAppTypes.WPPersoneel PersoneelAppAansluitingArbeidersAdd(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaPersoneelAppVerschilItemDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.PersoneelAppTypes.WPPersoneel result = rp.PersoneelAppAansluitingArbeidersAdd(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/PersoneelAppAansluitingArbeidersRemove")]
        public EY.com.eBook.RuleEngine.AY2011.PersoneelAppTypes.WPPersoneel PersoneelAppAansluitingArbeidersRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.PersoneelAppTypes.WPPersoneel result = rp.PersoneelAppAansluitingArbeidersRemove(cfrdc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/PersoneelAppAansluitingArbeidersUpdate")]
        public EY.com.eBook.RuleEngine.AY2011.PersoneelAppTypes.WPPersoneel PersoneelAppAansluitingArbeidersUpdate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaPersoneelAppVerschilItemDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.PersoneelAppTypes.WPPersoneel result = rp.PersoneelAppAansluitingArbeidersUpdate(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/PersoneelAppAansluitingBediendenAdd")]
        public EY.com.eBook.RuleEngine.AY2011.PersoneelAppTypes.WPPersoneel PersoneelAppAansluitingBediendenAdd(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaPersoneelAppVerschilItemDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.PersoneelAppTypes.WPPersoneel result = rp.PersoneelAppAansluitingBediendenAdd(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/PersoneelAppAansluitingBediendenRemove")]
        public EY.com.eBook.RuleEngine.AY2011.PersoneelAppTypes.WPPersoneel PersoneelAppAansluitingBediendenRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.PersoneelAppTypes.WPPersoneel result = rp.PersoneelAppAansluitingBediendenRemove(cfrdc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/PersoneelAppAansluitingBediendenUpdate")]
        public EY.com.eBook.RuleEngine.AY2011.PersoneelAppTypes.WPPersoneel PersoneelAppAansluitingBediendenUpdate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaPersoneelAppVerschilItemDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.PersoneelAppTypes.WPPersoneel result = rp.PersoneelAppAansluitingBediendenUpdate(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/PersoneelAppRSZCollAdd")]
        public EY.com.eBook.RuleEngine.AY2011.PersoneelAppTypes.WPPersoneel PersoneelAppRSZCollAdd(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaPersoneelAppAlgemeenDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.PersoneelAppTypes.WPPersoneel result = rp.PersoneelAppRSZCollAdd(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/PersoneelAppRSZCollRemove")]
        public EY.com.eBook.RuleEngine.AY2011.PersoneelAppTypes.WPPersoneel PersoneelAppRSZCollRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.PersoneelAppTypes.WPPersoneel result = rp.PersoneelAppRSZCollRemove(cfrdc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/PersoneelAppRSZCollUpdate")]
        public EY.com.eBook.RuleEngine.AY2011.PersoneelAppTypes.WPPersoneel PersoneelAppRSZCollUpdate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaPersoneelAppAlgemeenDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.PersoneelAppTypes.WPPersoneel result = rp.PersoneelAppRSZCollUpdate(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/PersoneelAppBVsCollAdd")]
        public EY.com.eBook.RuleEngine.AY2011.PersoneelAppTypes.WPPersoneel PersoneelAppBVsCollAdd(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaPersoneelAppAlgemeenDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.PersoneelAppTypes.WPPersoneel result = rp.PersoneelAppBVsCollAdd(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/PersoneelAppBVsCollRemove")]
        public EY.com.eBook.RuleEngine.AY2011.PersoneelAppTypes.WPPersoneel PersoneelAppBVsCollRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.PersoneelAppTypes.WPPersoneel result = rp.PersoneelAppBVsCollRemove(cfrdc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/PersoneelAppBVsCollUpdate")]
        public EY.com.eBook.RuleEngine.AY2011.PersoneelAppTypes.WPPersoneel PersoneelAppBVsCollUpdate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaPersoneelAppAlgemeenDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.PersoneelAppTypes.WPPersoneel result = rp.PersoneelAppBVsCollUpdate(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/PersoneelAppNettoCollAdd")]
        public EY.com.eBook.RuleEngine.AY2011.PersoneelAppTypes.WPPersoneel PersoneelAppNettoCollAdd(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaPersoneelAppAlgemeenDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.PersoneelAppTypes.WPPersoneel result = rp.PersoneelAppNettoCollAdd(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/PersoneelAppNettoCollRemove")]
        public EY.com.eBook.RuleEngine.AY2011.PersoneelAppTypes.WPPersoneel PersoneelAppNettoCollRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.PersoneelAppTypes.WPPersoneel result = rp.PersoneelAppNettoCollRemove(cfrdc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/PersoneelAppNettoCollUpdate")]
        public EY.com.eBook.RuleEngine.AY2011.PersoneelAppTypes.WPPersoneel PersoneelAppNettoCollUpdate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaPersoneelAppAlgemeenDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.PersoneelAppTypes.WPPersoneel result = rp.PersoneelAppNettoCollUpdate(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/PersoneelAppVakantieGeldCollUpdate")]
        public EY.com.eBook.RuleEngine.AY2011.PersoneelAppTypes.WPPersoneel PersoneelAppVakantieGeldCollUpdate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaPersoneelAppVakantiegeldDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.PersoneelAppTypes.WPPersoneel result = rp.PersoneelAppVakantieGeldCollUpdate(cedc);
            rp.Close();
            return result;
        }

        #endregion

        #endregion



        #region BTWApp
        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/BTWAppGetData")]
        public EY.com.eBook.RuleEngine.AY2011.BTWAppTypes.WPBTW BTWAppGetData(CriteriaFileDataContract cfdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.BTWAppTypes.WPBTW result = rp.BTWAppGetData(cfdc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/BTWAppCalculate")]
        public EY.com.eBook.RuleEngine.AY2011.BTWAppTypes.WPBTW BTWAppCalculate(CriteriaFileDataContract cfdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.BTWAppTypes.WPBTW result = rp.BTWAppCalculate(cfdc);
            rp.Close();
            return result;
        }

        #region Entity validation


        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/BTWAppOmzetAangifteValidate")]
        public List<FileMessageDataContract> BTWAppOmzetAangifteValidate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaBTWAppOmzetAangifteDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            List<FileMessageDataContract> result = rp.BTWAppOmzetAangifteValidate(cedc);
            rp.Close();
            return result;
        }



        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/BTWAppOmzetGeboektValidate")]
        public List<FileMessageDataContract> BTWAppOmzetGeboektValidate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaBTWAppOmzetGeboektDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            List<FileMessageDataContract> result = rp.BTWAppOmzetGeboektValidate(cedc);
            rp.Close();
            return result;
        }



        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/BTWAppOmzetVerklaringValidate")]
        public List<FileMessageDataContract> BTWAppOmzetVerklaringValidate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaBTWAppOmzetVerklaringDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            List<FileMessageDataContract> result = rp.BTWAppOmzetVerklaringValidate(cedc);
            rp.Close();
            return result;
        }



        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/BTWAppRCAangifteValidate")]
        public List<FileMessageDataContract> BTWAppRCAangifteValidate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaBTWAppRCAangifteDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            List<FileMessageDataContract> result = rp.BTWAppRCAangifteValidate(cedc);
            rp.Close();
            return result;
        }



        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/BTWAppRCGeboektValidate")]
        public List<FileMessageDataContract> BTWAppRCGeboektValidate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaBTWAppRCGeboektDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            List<FileMessageDataContract> result = rp.BTWAppRCGeboektValidate(cedc);
            rp.Close();
            return result;
        }



        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/BTWAppRCVerklaringValidate")]
        public List<FileMessageDataContract> BTWAppRCVerklaringValidate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaBTWAppRCVerklaringDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            List<FileMessageDataContract> result = rp.BTWAppRCVerklaringValidate(cedc);
            rp.Close();
            return result;
        }



        #endregion



        #region Add/Remove


        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/BTWAppOmzetAangiftesAdd")]
        public EY.com.eBook.RuleEngine.AY2011.BTWAppTypes.WPBTW BTWAppOmzetAangiftesAdd(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaBTWAppOmzetAangifteDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.BTWAppTypes.WPBTW result = rp.BTWAppOmzetAangiftesAdd(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/BTWAppOmzetAangiftesRemove")]
        public EY.com.eBook.RuleEngine.AY2011.BTWAppTypes.WPBTW BTWAppOmzetAangiftesRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.BTWAppTypes.WPBTW result = rp.BTWAppOmzetAangiftesRemove(cfrdc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/BTWAppOmzetAangiftesUpdate")]
        public EY.com.eBook.RuleEngine.AY2011.BTWAppTypes.WPBTW BTWAppOmzetAangiftesUpdate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaBTWAppOmzetAangifteDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.BTWAppTypes.WPBTW result = rp.BTWAppOmzetAangiftesUpdate(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/BTWAppOmzettenGeboektAdd")]
        public EY.com.eBook.RuleEngine.AY2011.BTWAppTypes.WPBTW BTWAppOmzettenGeboektAdd(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaBTWAppOmzetGeboektDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.BTWAppTypes.WPBTW result = rp.BTWAppOmzettenGeboektAdd(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/BTWAppOmzettenGeboektRemove")]
        public EY.com.eBook.RuleEngine.AY2011.BTWAppTypes.WPBTW BTWAppOmzettenGeboektRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.BTWAppTypes.WPBTW result = rp.BTWAppOmzettenGeboektRemove(cfrdc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/BTWAppOmzettenGeboektUpdate")]
        public EY.com.eBook.RuleEngine.AY2011.BTWAppTypes.WPBTW BTWAppOmzettenGeboektUpdate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaBTWAppOmzetGeboektDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.BTWAppTypes.WPBTW result = rp.BTWAppOmzettenGeboektUpdate(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/BTWAppOmzetVerklaringenAdd")]
        public EY.com.eBook.RuleEngine.AY2011.BTWAppTypes.WPBTW BTWAppOmzetVerklaringenAdd(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaBTWAppOmzetVerklaringDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.BTWAppTypes.WPBTW result = rp.BTWAppOmzetVerklaringenAdd(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/BTWAppOmzetVerklaringenRemove")]
        public EY.com.eBook.RuleEngine.AY2011.BTWAppTypes.WPBTW BTWAppOmzetVerklaringenRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.BTWAppTypes.WPBTW result = rp.BTWAppOmzetVerklaringenRemove(cfrdc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/BTWAppOmzetVerklaringenUpdate")]
        public EY.com.eBook.RuleEngine.AY2011.BTWAppTypes.WPBTW BTWAppOmzetVerklaringenUpdate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaBTWAppOmzetVerklaringDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.BTWAppTypes.WPBTW result = rp.BTWAppOmzetVerklaringenUpdate(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/BTWAppRCAangiftesAdd")]
        public EY.com.eBook.RuleEngine.AY2011.BTWAppTypes.WPBTW BTWAppRCAangiftesAdd(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaBTWAppRCAangifteDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.BTWAppTypes.WPBTW result = rp.BTWAppRCAangiftesAdd(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/BTWAppRCAangiftesRemove")]
        public EY.com.eBook.RuleEngine.AY2011.BTWAppTypes.WPBTW BTWAppRCAangiftesRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.BTWAppTypes.WPBTW result = rp.BTWAppRCAangiftesRemove(cfrdc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/BTWAppRCAangiftesUpdate")]
        public EY.com.eBook.RuleEngine.AY2011.BTWAppTypes.WPBTW BTWAppRCAangiftesUpdate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaBTWAppRCAangifteDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.BTWAppTypes.WPBTW result = rp.BTWAppRCAangiftesUpdate(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/BTWAppRCenGeboektAdd")]
        public EY.com.eBook.RuleEngine.AY2011.BTWAppTypes.WPBTW BTWAppRCenGeboektAdd(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaBTWAppRCGeboektDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.BTWAppTypes.WPBTW result = rp.BTWAppRCenGeboektAdd(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/BTWAppRCenGeboektRemove")]
        public EY.com.eBook.RuleEngine.AY2011.BTWAppTypes.WPBTW BTWAppRCenGeboektRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.BTWAppTypes.WPBTW result = rp.BTWAppRCenGeboektRemove(cfrdc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/BTWAppRCenGeboektUpdate")]
        public EY.com.eBook.RuleEngine.AY2011.BTWAppTypes.WPBTW BTWAppRCenGeboektUpdate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaBTWAppRCGeboektDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.BTWAppTypes.WPBTW result = rp.BTWAppRCenGeboektUpdate(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/BTWAppRCVerklaringenAdd")]
        public EY.com.eBook.RuleEngine.AY2011.BTWAppTypes.WPBTW BTWAppRCVerklaringenAdd(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaBTWAppRCVerklaringDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.BTWAppTypes.WPBTW result = rp.BTWAppRCVerklaringenAdd(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/BTWAppRCVerklaringenRemove")]
        public EY.com.eBook.RuleEngine.AY2011.BTWAppTypes.WPBTW BTWAppRCVerklaringenRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.BTWAppTypes.WPBTW result = rp.BTWAppRCVerklaringenRemove(cfrdc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/BTWAppRCVerklaringenUpdate")]
        public EY.com.eBook.RuleEngine.AY2011.BTWAppTypes.WPBTW BTWAppRCVerklaringenUpdate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaBTWAppRCVerklaringDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.BTWAppTypes.WPBTW result = rp.BTWAppRCVerklaringenUpdate(cedc);
            rp.Close();
            return result;
        }

        #endregion

        #endregion



        #region RVIntrestenApp
        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/RVIntrestenAppGetData")]
        public EY.com.eBook.RuleEngine.AY2011.RVIntrestenAppTypes.WPRVIntresten RVIntrestenAppGetData(CriteriaFileDataContract cfdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.RVIntrestenAppTypes.WPRVIntresten result = rp.RVIntrestenAppGetData(cfdc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/RVIntrestenAppCalculate")]
        public EY.com.eBook.RuleEngine.AY2011.RVIntrestenAppTypes.WPRVIntresten RVIntrestenAppCalculate(CriteriaFileDataContract cfdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.RVIntrestenAppTypes.WPRVIntresten result = rp.RVIntrestenAppCalculate(cfdc);
            rp.Close();
            return result;
        }

        #region Entity validation


        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/RVIntrestenAppIntrestItemValidate")]
        public List<FileMessageDataContract> RVIntrestenAppIntrestItemValidate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaRVIntrestenAppIntrestItemDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            List<FileMessageDataContract> result = rp.RVIntrestenAppIntrestItemValidate(cedc);
            rp.Close();
            return result;
        }



        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/RVIntrestenAppAdjustmentValidate")]
        public List<FileMessageDataContract> RVIntrestenAppAdjustmentValidate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaRVIntrestenAppAdjustmentDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            List<FileMessageDataContract> result = rp.RVIntrestenAppAdjustmentValidate(cedc);
            rp.Close();
            return result;
        }



        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/RVIntrestenAppVerchilItemValidate")]
        public List<FileMessageDataContract> RVIntrestenAppVerchilItemValidate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaRVIntrestenAppVerchilItemDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            List<FileMessageDataContract> result = rp.RVIntrestenAppVerchilItemValidate(cedc);
            rp.Close();
            return result;
        }



        #endregion



        #region Add/Remove


        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/RVIntrestenAppIntrestenUpdate")]
        public EY.com.eBook.RuleEngine.AY2011.RVIntrestenAppTypes.WPRVIntresten RVIntrestenAppIntrestenUpdate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaRVIntrestenAppIntrestItemDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.RVIntrestenAppTypes.WPRVIntresten result = rp.RVIntrestenAppIntrestenUpdate(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/RVIntrestenAppAdjustmentsAdd")]
        public EY.com.eBook.RuleEngine.AY2011.RVIntrestenAppTypes.WPRVIntresten RVIntrestenAppAdjustmentsAdd(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaRVIntrestenAppAdjustmentDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.RVIntrestenAppTypes.WPRVIntresten result = rp.RVIntrestenAppAdjustmentsAdd(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/RVIntrestenAppAdjustmentsRemove")]
        public EY.com.eBook.RuleEngine.AY2011.RVIntrestenAppTypes.WPRVIntresten RVIntrestenAppAdjustmentsRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.RVIntrestenAppTypes.WPRVIntresten result = rp.RVIntrestenAppAdjustmentsRemove(cfrdc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/RVIntrestenAppAdjustmentsUpdate")]
        public EY.com.eBook.RuleEngine.AY2011.RVIntrestenAppTypes.WPRVIntresten RVIntrestenAppAdjustmentsUpdate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaRVIntrestenAppAdjustmentDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.RVIntrestenAppTypes.WPRVIntresten result = rp.RVIntrestenAppAdjustmentsUpdate(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/RVIntrestenAppVerschillenAdd")]
        public EY.com.eBook.RuleEngine.AY2011.RVIntrestenAppTypes.WPRVIntresten RVIntrestenAppVerschillenAdd(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaRVIntrestenAppVerchilItemDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.RVIntrestenAppTypes.WPRVIntresten result = rp.RVIntrestenAppVerschillenAdd(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/RVIntrestenAppVerschillenRemove")]
        public EY.com.eBook.RuleEngine.AY2011.RVIntrestenAppTypes.WPRVIntresten RVIntrestenAppVerschillenRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.RVIntrestenAppTypes.WPRVIntresten result = rp.RVIntrestenAppVerschillenRemove(cfrdc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/RVIntrestenAppVerschillenUpdate")]
        public EY.com.eBook.RuleEngine.AY2011.RVIntrestenAppTypes.WPRVIntresten RVIntrestenAppVerschillenUpdate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaRVIntrestenAppVerchilItemDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.RVIntrestenAppTypes.WPRVIntresten result = rp.RVIntrestenAppVerschillenUpdate(cedc);
            rp.Close();
            return result;
        }

        #endregion

        #endregion



        #region DubieuzeDebiteurenApp
        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/DubieuzeDebiteurenAppGetData")]
        public EY.com.eBook.RuleEngine.AY2011.DubieuzeDebiteurenAppTypes.DubieuzeDebiteuren DubieuzeDebiteurenAppGetData(CriteriaFileDataContract cfdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.DubieuzeDebiteurenAppTypes.DubieuzeDebiteuren result = rp.DubieuzeDebiteurenAppGetData(cfdc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/DubieuzeDebiteurenAppCalculate")]
        public EY.com.eBook.RuleEngine.AY2011.DubieuzeDebiteurenAppTypes.DubieuzeDebiteuren DubieuzeDebiteurenAppCalculate(CriteriaFileDataContract cfdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.DubieuzeDebiteurenAppTypes.DubieuzeDebiteuren result = rp.DubieuzeDebiteurenAppCalculate(cfdc);
            rp.Close();
            return result;
        }

        #region Entity validation


        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/DubieuzeDebiteurenAppDubieuzeDebiteurTValidate")]
        public List<FileMessageDataContract> DubieuzeDebiteurenAppDubieuzeDebiteurTValidate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaDubieuzeDebiteurenAppDubieuzeDebiteurTDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            List<FileMessageDataContract> result = rp.DubieuzeDebiteurenAppDubieuzeDebiteurTValidate(cedc);
            rp.Close();
            return result;
        }



        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/DubieuzeDebiteurenAppDubieuzeDebiteurWValidate")]
        public List<FileMessageDataContract> DubieuzeDebiteurenAppDubieuzeDebiteurWValidate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaDubieuzeDebiteurenAppDubieuzeDebiteurWDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            List<FileMessageDataContract> result = rp.DubieuzeDebiteurenAppDubieuzeDebiteurWValidate(cedc);
            rp.Close();
            return result;
        }



        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/DubieuzeDebiteurenAppDubieuzeDebiteurNValidate")]
        public List<FileMessageDataContract> DubieuzeDebiteurenAppDubieuzeDebiteurNValidate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaDubieuzeDebiteurenAppDubieuzeDebiteurNDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            List<FileMessageDataContract> result = rp.DubieuzeDebiteurenAppDubieuzeDebiteurNValidate(cedc);
            rp.Close();
            return result;
        }



        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/DubieuzeDebiteurenAppDubieuzeDebiteurHValidate")]
        public List<FileMessageDataContract> DubieuzeDebiteurenAppDubieuzeDebiteurHValidate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaDubieuzeDebiteurenAppDubieuzeDebiteurHDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            List<FileMessageDataContract> result = rp.DubieuzeDebiteurenAppDubieuzeDebiteurHValidate(cedc);
            rp.Close();
            return result;
        }



        #endregion



        #region Add/Remove


        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/DubieuzeDebiteurenAppToestandEindVorigBoekjaarAdd")]
        public EY.com.eBook.RuleEngine.AY2011.DubieuzeDebiteurenAppTypes.DubieuzeDebiteuren DubieuzeDebiteurenAppToestandEindVorigBoekjaarAdd(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaDubieuzeDebiteurenAppDubieuzeDebiteurTDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.DubieuzeDebiteurenAppTypes.DubieuzeDebiteuren result = rp.DubieuzeDebiteurenAppToestandEindVorigBoekjaarAdd(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/DubieuzeDebiteurenAppToestandEindVorigBoekjaarRemove")]
        public EY.com.eBook.RuleEngine.AY2011.DubieuzeDebiteurenAppTypes.DubieuzeDebiteuren DubieuzeDebiteurenAppToestandEindVorigBoekjaarRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.DubieuzeDebiteurenAppTypes.DubieuzeDebiteuren result = rp.DubieuzeDebiteurenAppToestandEindVorigBoekjaarRemove(cfrdc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/DubieuzeDebiteurenAppToestandEindVorigBoekjaarUpdate")]
        public EY.com.eBook.RuleEngine.AY2011.DubieuzeDebiteurenAppTypes.DubieuzeDebiteuren DubieuzeDebiteurenAppToestandEindVorigBoekjaarUpdate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaDubieuzeDebiteurenAppDubieuzeDebiteurTDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.DubieuzeDebiteurenAppTypes.DubieuzeDebiteuren result = rp.DubieuzeDebiteurenAppToestandEindVorigBoekjaarUpdate(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/DubieuzeDebiteurenAppWijzigingBestaandeTijdensBoekjaarAdd")]
        public EY.com.eBook.RuleEngine.AY2011.DubieuzeDebiteurenAppTypes.DubieuzeDebiteuren DubieuzeDebiteurenAppWijzigingBestaandeTijdensBoekjaarAdd(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaDubieuzeDebiteurenAppDubieuzeDebiteurWDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.DubieuzeDebiteurenAppTypes.DubieuzeDebiteuren result = rp.DubieuzeDebiteurenAppWijzigingBestaandeTijdensBoekjaarAdd(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/DubieuzeDebiteurenAppWijzigingBestaandeTijdensBoekjaarRemove")]
        public EY.com.eBook.RuleEngine.AY2011.DubieuzeDebiteurenAppTypes.DubieuzeDebiteuren DubieuzeDebiteurenAppWijzigingBestaandeTijdensBoekjaarRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.DubieuzeDebiteurenAppTypes.DubieuzeDebiteuren result = rp.DubieuzeDebiteurenAppWijzigingBestaandeTijdensBoekjaarRemove(cfrdc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/DubieuzeDebiteurenAppWijzigingBestaandeTijdensBoekjaarUpdate")]
        public EY.com.eBook.RuleEngine.AY2011.DubieuzeDebiteurenAppTypes.DubieuzeDebiteuren DubieuzeDebiteurenAppWijzigingBestaandeTijdensBoekjaarUpdate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaDubieuzeDebiteurenAppDubieuzeDebiteurWDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.DubieuzeDebiteurenAppTypes.DubieuzeDebiteuren result = rp.DubieuzeDebiteurenAppWijzigingBestaandeTijdensBoekjaarUpdate(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/DubieuzeDebiteurenAppNieuweTijdensBoekjaarAdd")]
        public EY.com.eBook.RuleEngine.AY2011.DubieuzeDebiteurenAppTypes.DubieuzeDebiteuren DubieuzeDebiteurenAppNieuweTijdensBoekjaarAdd(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaDubieuzeDebiteurenAppDubieuzeDebiteurNDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.DubieuzeDebiteurenAppTypes.DubieuzeDebiteuren result = rp.DubieuzeDebiteurenAppNieuweTijdensBoekjaarAdd(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/DubieuzeDebiteurenAppNieuweTijdensBoekjaarRemove")]
        public EY.com.eBook.RuleEngine.AY2011.DubieuzeDebiteurenAppTypes.DubieuzeDebiteuren DubieuzeDebiteurenAppNieuweTijdensBoekjaarRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.DubieuzeDebiteurenAppTypes.DubieuzeDebiteuren result = rp.DubieuzeDebiteurenAppNieuweTijdensBoekjaarRemove(cfrdc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/DubieuzeDebiteurenAppNieuweTijdensBoekjaarUpdate")]
        public EY.com.eBook.RuleEngine.AY2011.DubieuzeDebiteurenAppTypes.DubieuzeDebiteuren DubieuzeDebiteurenAppNieuweTijdensBoekjaarUpdate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaDubieuzeDebiteurenAppDubieuzeDebiteurNDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.DubieuzeDebiteurenAppTypes.DubieuzeDebiteuren result = rp.DubieuzeDebiteurenAppNieuweTijdensBoekjaarUpdate(cedc);
            rp.Close();
            return result;
        }

        #endregion

        #endregion



        #region VerliesHandelsdebiteurenApp
        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/VerliesHandelsdebiteurenAppGetData")]
        public EY.com.eBook.RuleEngine.AY2011.VerliesHandelsdebiteurenAppTypes.VerliesHandelsDebiteuren VerliesHandelsdebiteurenAppGetData(CriteriaFileDataContract cfdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.VerliesHandelsdebiteurenAppTypes.VerliesHandelsDebiteuren result = rp.VerliesHandelsdebiteurenAppGetData(cfdc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/VerliesHandelsdebiteurenAppCalculate")]
        public EY.com.eBook.RuleEngine.AY2011.VerliesHandelsdebiteurenAppTypes.VerliesHandelsDebiteuren VerliesHandelsdebiteurenAppCalculate(CriteriaFileDataContract cfdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.VerliesHandelsdebiteurenAppTypes.VerliesHandelsDebiteuren result = rp.VerliesHandelsdebiteurenAppCalculate(cfdc);
            rp.Close();
            return result;
        }

        #region Entity validation


        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/VerliesHandelsdebiteurenAppVerliesHandelsDebiteurValidate")]
        public List<FileMessageDataContract> VerliesHandelsdebiteurenAppVerliesHandelsDebiteurValidate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaVerliesHandelsdebiteurenAppVerliesHandelsDebiteurDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            List<FileMessageDataContract> result = rp.VerliesHandelsdebiteurenAppVerliesHandelsDebiteurValidate(cedc);
            rp.Close();
            return result;
        }



        #endregion



        #region Add/Remove


        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/VerliesHandelsdebiteurenAppVerliesHDAdd")]
        public EY.com.eBook.RuleEngine.AY2011.VerliesHandelsdebiteurenAppTypes.VerliesHandelsDebiteuren VerliesHandelsdebiteurenAppVerliesHDAdd(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaVerliesHandelsdebiteurenAppVerliesHandelsDebiteurDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.VerliesHandelsdebiteurenAppTypes.VerliesHandelsDebiteuren result = rp.VerliesHandelsdebiteurenAppVerliesHDAdd(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/VerliesHandelsdebiteurenAppVerliesHDRemove")]
        public EY.com.eBook.RuleEngine.AY2011.VerliesHandelsdebiteurenAppTypes.VerliesHandelsDebiteuren VerliesHandelsdebiteurenAppVerliesHDRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.VerliesHandelsdebiteurenAppTypes.VerliesHandelsDebiteuren result = rp.VerliesHandelsdebiteurenAppVerliesHDRemove(cfrdc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/VerliesHandelsdebiteurenAppVerliesHDUpdate")]
        public EY.com.eBook.RuleEngine.AY2011.VerliesHandelsdebiteurenAppTypes.VerliesHandelsDebiteuren VerliesHandelsdebiteurenAppVerliesHDUpdate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaVerliesHandelsdebiteurenAppVerliesHandelsDebiteurDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.VerliesHandelsdebiteurenAppTypes.VerliesHandelsDebiteuren result = rp.VerliesHandelsdebiteurenAppVerliesHDUpdate(cedc);
            rp.Close();
            return result;
        }

        #endregion

        #endregion



        #region VoorzieningenApp
        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/VoorzieningenAppGetData")]
        public EY.com.eBook.RuleEngine.AY2011.VoorzieningenAppTypes.Voorzieningen VoorzieningenAppGetData(CriteriaFileDataContract cfdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.VoorzieningenAppTypes.Voorzieningen result = rp.VoorzieningenAppGetData(cfdc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/VoorzieningenAppCalculate")]
        public EY.com.eBook.RuleEngine.AY2011.VoorzieningenAppTypes.Voorzieningen VoorzieningenAppCalculate(CriteriaFileDataContract cfdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.VoorzieningenAppTypes.Voorzieningen result = rp.VoorzieningenAppCalculate(cfdc);
            rp.Close();
            return result;
        }

        #region Entity validation


        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/VoorzieningenAppVoorzieningenTValidate")]
        public List<FileMessageDataContract> VoorzieningenAppVoorzieningenTValidate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaVoorzieningenAppVoorzieningenTDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            List<FileMessageDataContract> result = rp.VoorzieningenAppVoorzieningenTValidate(cedc);
            rp.Close();
            return result;
        }



        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/VoorzieningenAppVoorzieningenWValidate")]
        public List<FileMessageDataContract> VoorzieningenAppVoorzieningenWValidate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaVoorzieningenAppVoorzieningenWDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            List<FileMessageDataContract> result = rp.VoorzieningenAppVoorzieningenWValidate(cedc);
            rp.Close();
            return result;
        }



        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/VoorzieningenAppVoorzieningenNValidate")]
        public List<FileMessageDataContract> VoorzieningenAppVoorzieningenNValidate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaVoorzieningenAppVoorzieningenNDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            List<FileMessageDataContract> result = rp.VoorzieningenAppVoorzieningenNValidate(cedc);
            rp.Close();
            return result;
        }



        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/VoorzieningenAppVoorzieningenHValidate")]
        public List<FileMessageDataContract> VoorzieningenAppVoorzieningenHValidate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaVoorzieningenAppVoorzieningenHDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            List<FileMessageDataContract> result = rp.VoorzieningenAppVoorzieningenHValidate(cedc);
            rp.Close();
            return result;
        }



        #endregion



        #region Add/Remove


        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/VoorzieningenAppToestandEindVorigBoekjaarAdd")]
        public EY.com.eBook.RuleEngine.AY2011.VoorzieningenAppTypes.Voorzieningen VoorzieningenAppToestandEindVorigBoekjaarAdd(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaVoorzieningenAppVoorzieningenTDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.VoorzieningenAppTypes.Voorzieningen result = rp.VoorzieningenAppToestandEindVorigBoekjaarAdd(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/VoorzieningenAppToestandEindVorigBoekjaarRemove")]
        public EY.com.eBook.RuleEngine.AY2011.VoorzieningenAppTypes.Voorzieningen VoorzieningenAppToestandEindVorigBoekjaarRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.VoorzieningenAppTypes.Voorzieningen result = rp.VoorzieningenAppToestandEindVorigBoekjaarRemove(cfrdc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/VoorzieningenAppToestandEindVorigBoekjaarUpdate")]
        public EY.com.eBook.RuleEngine.AY2011.VoorzieningenAppTypes.Voorzieningen VoorzieningenAppToestandEindVorigBoekjaarUpdate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaVoorzieningenAppVoorzieningenTDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.VoorzieningenAppTypes.Voorzieningen result = rp.VoorzieningenAppToestandEindVorigBoekjaarUpdate(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/VoorzieningenAppWijzigingBestaandeTijdensBoekjaarAdd")]
        public EY.com.eBook.RuleEngine.AY2011.VoorzieningenAppTypes.Voorzieningen VoorzieningenAppWijzigingBestaandeTijdensBoekjaarAdd(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaVoorzieningenAppVoorzieningenWDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.VoorzieningenAppTypes.Voorzieningen result = rp.VoorzieningenAppWijzigingBestaandeTijdensBoekjaarAdd(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/VoorzieningenAppWijzigingBestaandeTijdensBoekjaarRemove")]
        public EY.com.eBook.RuleEngine.AY2011.VoorzieningenAppTypes.Voorzieningen VoorzieningenAppWijzigingBestaandeTijdensBoekjaarRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.VoorzieningenAppTypes.Voorzieningen result = rp.VoorzieningenAppWijzigingBestaandeTijdensBoekjaarRemove(cfrdc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/VoorzieningenAppWijzigingBestaandeTijdensBoekjaarUpdate")]
        public EY.com.eBook.RuleEngine.AY2011.VoorzieningenAppTypes.Voorzieningen VoorzieningenAppWijzigingBestaandeTijdensBoekjaarUpdate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaVoorzieningenAppVoorzieningenWDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.VoorzieningenAppTypes.Voorzieningen result = rp.VoorzieningenAppWijzigingBestaandeTijdensBoekjaarUpdate(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/VoorzieningenAppNieuweTijdensBoekjaarAdd")]
        public EY.com.eBook.RuleEngine.AY2011.VoorzieningenAppTypes.Voorzieningen VoorzieningenAppNieuweTijdensBoekjaarAdd(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaVoorzieningenAppVoorzieningenNDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.VoorzieningenAppTypes.Voorzieningen result = rp.VoorzieningenAppNieuweTijdensBoekjaarAdd(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/VoorzieningenAppNieuweTijdensBoekjaarRemove")]
        public EY.com.eBook.RuleEngine.AY2011.VoorzieningenAppTypes.Voorzieningen VoorzieningenAppNieuweTijdensBoekjaarRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.VoorzieningenAppTypes.Voorzieningen result = rp.VoorzieningenAppNieuweTijdensBoekjaarRemove(cfrdc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/VoorzieningenAppNieuweTijdensBoekjaarUpdate")]
        public EY.com.eBook.RuleEngine.AY2011.VoorzieningenAppTypes.Voorzieningen VoorzieningenAppNieuweTijdensBoekjaarUpdate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaVoorzieningenAppVoorzieningenNDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.VoorzieningenAppTypes.Voorzieningen result = rp.VoorzieningenAppNieuweTijdensBoekjaarUpdate(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/VoorzieningenAppToestandEindHuidigBoekjaarAdd")]
        public EY.com.eBook.RuleEngine.AY2011.VoorzieningenAppTypes.Voorzieningen VoorzieningenAppToestandEindHuidigBoekjaarAdd(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaVoorzieningenAppVoorzieningenHDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.VoorzieningenAppTypes.Voorzieningen result = rp.VoorzieningenAppToestandEindHuidigBoekjaarAdd(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/VoorzieningenAppToestandEindHuidigBoekjaarRemove")]
        public EY.com.eBook.RuleEngine.AY2011.VoorzieningenAppTypes.Voorzieningen VoorzieningenAppToestandEindHuidigBoekjaarRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.VoorzieningenAppTypes.Voorzieningen result = rp.VoorzieningenAppToestandEindHuidigBoekjaarRemove(cfrdc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/VoorzieningenAppToestandEindHuidigBoekjaarUpdate")]
        public EY.com.eBook.RuleEngine.AY2011.VoorzieningenAppTypes.Voorzieningen VoorzieningenAppToestandEindHuidigBoekjaarUpdate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaVoorzieningenAppVoorzieningenHDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.VoorzieningenAppTypes.Voorzieningen result = rp.VoorzieningenAppToestandEindHuidigBoekjaarUpdate(cedc);
            rp.Close();
            return result;
        }

        #endregion

        #endregion



        #region BelastingenApp
        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/BelastingenAppGetData")]
        public EY.com.eBook.RuleEngine.AY2011.BelastingenAppTypes.WPBelastingen BelastingenAppGetData(CriteriaFileDataContract cfdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.BelastingenAppTypes.WPBelastingen result = rp.BelastingenAppGetData(cfdc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/BelastingenAppCalculate")]
        public EY.com.eBook.RuleEngine.AY2011.BelastingenAppTypes.WPBelastingen BelastingenAppCalculate(CriteriaFileDataContract cfdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.BelastingenAppTypes.WPBelastingen result = rp.BelastingenAppCalculate(cfdc);
            rp.Close();
            return result;
        }

        #region Entity validation


        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/BelastingenAppAdjustmentValidate")]
        public List<FileMessageDataContract> BelastingenAppAdjustmentValidate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaBelastingenAppAdjustmentDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            List<FileMessageDataContract> result = rp.BelastingenAppAdjustmentValidate(cedc);
            rp.Close();
            return result;
        }



        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/BelastingenAppBelastingValidate")]
        public List<FileMessageDataContract> BelastingenAppBelastingValidate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaBelastingenAppBelastingDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            List<FileMessageDataContract> result = rp.BelastingenAppBelastingValidate(cedc);
            rp.Close();
            return result;
        }



        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/BelastingenAppKostItemValidate")]
        public List<FileMessageDataContract> BelastingenAppKostItemValidate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaBelastingenAppKostItemDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            List<FileMessageDataContract> result = rp.BelastingenAppKostItemValidate(cedc);
            rp.Close();
            return result;
        }



        #endregion



        #region Add/Remove


        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/BelastingenAppAdjustmentsAdd")]
        public EY.com.eBook.RuleEngine.AY2011.BelastingenAppTypes.WPBelastingen BelastingenAppAdjustmentsAdd(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaBelastingenAppAdjustmentDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.BelastingenAppTypes.WPBelastingen result = rp.BelastingenAppAdjustmentsAdd(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/BelastingenAppAdjustmentsRemove")]
        public EY.com.eBook.RuleEngine.AY2011.BelastingenAppTypes.WPBelastingen BelastingenAppAdjustmentsRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.BelastingenAppTypes.WPBelastingen result = rp.BelastingenAppAdjustmentsRemove(cfrdc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/BelastingenAppAdjustmentsUpdate")]
        public EY.com.eBook.RuleEngine.AY2011.BelastingenAppTypes.WPBelastingen BelastingenAppAdjustmentsUpdate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaBelastingenAppAdjustmentDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.BelastingenAppTypes.WPBelastingen result = rp.BelastingenAppAdjustmentsUpdate(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/BelastingenAppBelastingenUpdate")]
        public EY.com.eBook.RuleEngine.AY2011.BelastingenAppTypes.WPBelastingen BelastingenAppBelastingenUpdate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaBelastingenAppBelastingDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.BelastingenAppTypes.WPBelastingen result = rp.BelastingenAppBelastingenUpdate(cedc);
            rp.Close();
            return result;
        }

        #endregion

        #endregion



        #region BelasteReservesApp
        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/BelasteReservesAppGetData")]
        public EY.com.eBook.RuleEngine.AY2011.BelasteReservesAppTypes.BelasteReserves BelasteReservesAppGetData(CriteriaFileDataContract cfdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.BelasteReservesAppTypes.BelasteReserves result = rp.BelasteReservesAppGetData(cfdc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/BelasteReservesAppCalculate")]
        public EY.com.eBook.RuleEngine.AY2011.BelasteReservesAppTypes.BelasteReserves BelasteReservesAppCalculate(CriteriaFileDataContract cfdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.BelasteReservesAppTypes.BelasteReserves result = rp.BelasteReservesAppCalculate(cfdc);
            rp.Close();
            return result;
        }

        #region Entity validation


        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/BelasteReservesAppBelasteReservesItemValidate")]
        public List<FileMessageDataContract> BelasteReservesAppBelasteReservesItemValidate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaBelasteReservesAppBelasteReservesItemDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            List<FileMessageDataContract> result = rp.BelasteReservesAppBelasteReservesItemValidate(cedc);
            rp.Close();
            return result;
        }



        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/BelasteReservesAppAanpassingBTReservesItemValidate")]
        public List<FileMessageDataContract> BelasteReservesAppAanpassingBTReservesItemValidate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaBelasteReservesAppAanpassingBTReservesItemDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            List<FileMessageDataContract> result = rp.BelasteReservesAppAanpassingBTReservesItemValidate(cedc);
            rp.Close();
            return result;
        }



        #endregion



        #region Add/Remove


        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/BelasteReservesAppAllBelasteReservesAdd")]
        public EY.com.eBook.RuleEngine.AY2011.BelasteReservesAppTypes.BelasteReserves BelasteReservesAppAllBelasteReservesAdd(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaBelasteReservesAppBelasteReservesItemDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.BelasteReservesAppTypes.BelasteReserves result = rp.BelasteReservesAppAllBelasteReservesAdd(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/BelasteReservesAppAllBelasteReservesRemove")]
        public EY.com.eBook.RuleEngine.AY2011.BelasteReservesAppTypes.BelasteReserves BelasteReservesAppAllBelasteReservesRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.BelasteReservesAppTypes.BelasteReserves result = rp.BelasteReservesAppAllBelasteReservesRemove(cfrdc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/BelasteReservesAppAllBelasteReservesUpdate")]
        public EY.com.eBook.RuleEngine.AY2011.BelasteReservesAppTypes.BelasteReserves BelasteReservesAppAllBelasteReservesUpdate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaBelasteReservesAppBelasteReservesItemDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.BelasteReservesAppTypes.BelasteReserves result = rp.BelasteReservesAppAllBelasteReservesUpdate(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/BelasteReservesAppAanpassingBTReservesAdd")]
        public EY.com.eBook.RuleEngine.AY2011.BelasteReservesAppTypes.BelasteReserves BelasteReservesAppAanpassingBTReservesAdd(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaBelasteReservesAppAanpassingBTReservesItemDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.BelasteReservesAppTypes.BelasteReserves result = rp.BelasteReservesAppAanpassingBTReservesAdd(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/BelasteReservesAppAanpassingBTReservesRemove")]
        public EY.com.eBook.RuleEngine.AY2011.BelasteReservesAppTypes.BelasteReserves BelasteReservesAppAanpassingBTReservesRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.BelasteReservesAppTypes.BelasteReserves result = rp.BelasteReservesAppAanpassingBTReservesRemove(cfrdc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/BelasteReservesAppAanpassingBTReservesUpdate")]
        public EY.com.eBook.RuleEngine.AY2011.BelasteReservesAppTypes.BelasteReserves BelasteReservesAppAanpassingBTReservesUpdate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaBelasteReservesAppAanpassingBTReservesItemDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.BelasteReservesAppTypes.BelasteReserves result = rp.BelasteReservesAppAanpassingBTReservesUpdate(cedc);
            rp.Close();
            return result;
        }

        #endregion

        #endregion



        #region BelastingVrijeReservesApp
        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/BelastingVrijeReservesAppGetData")]
        public EY.com.eBook.RuleEngine.AY2011.BelastingVrijeReservesAppTypes.BelastingVrijeReserves BelastingVrijeReservesAppGetData(CriteriaFileDataContract cfdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.BelastingVrijeReservesAppTypes.BelastingVrijeReserves result = rp.BelastingVrijeReservesAppGetData(cfdc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/BelastingVrijeReservesAppCalculate")]
        public EY.com.eBook.RuleEngine.AY2011.BelastingVrijeReservesAppTypes.BelastingVrijeReserves BelastingVrijeReservesAppCalculate(CriteriaFileDataContract cfdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.BelastingVrijeReservesAppTypes.BelastingVrijeReserves result = rp.BelastingVrijeReservesAppCalculate(cfdc);
            rp.Close();
            return result;
        }

        #region Entity validation


        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/BelastingVrijeReservesAppDetailValidate")]
        public List<FileMessageDataContract> BelastingVrijeReservesAppDetailValidate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaBelastingVrijeReservesAppDetailDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            List<FileMessageDataContract> result = rp.BelastingVrijeReservesAppDetailValidate(cedc);
            rp.Close();
            return result;
        }



        #endregion



        #region Add/Remove


        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/BelastingVrijeReservesAppVrijgesteldeReservesUpdate")]
        public EY.com.eBook.RuleEngine.AY2011.BelastingVrijeReservesAppTypes.BelastingVrijeReserves BelastingVrijeReservesAppVrijgesteldeReservesUpdate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaBelastingVrijeReservesAppDetailDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.BelastingVrijeReservesAppTypes.BelastingVrijeReserves result = rp.BelastingVrijeReservesAppVrijgesteldeReservesUpdate(cedc);
            rp.Close();
            return result;
        }

        #endregion

        #endregion



        #region BepalingBelastbareBasisApp
        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/BepalingBelastbareBasisAppGetData")]
        public EY.com.eBook.RuleEngine.AY2011.BepalingBelastbareBasisAppTypes.BepalingBasis BepalingBelastbareBasisAppGetData(CriteriaFileDataContract cfdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.BepalingBelastbareBasisAppTypes.BepalingBasis result = rp.BepalingBelastbareBasisAppGetData(cfdc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/BepalingBelastbareBasisAppCalculate")]
        public EY.com.eBook.RuleEngine.AY2011.BepalingBelastbareBasisAppTypes.BepalingBasis BepalingBelastbareBasisAppCalculate(CriteriaFileDataContract cfdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.BepalingBelastbareBasisAppTypes.BepalingBasis result = rp.BepalingBelastbareBasisAppCalculate(cfdc);
            rp.Close();
            return result;
        }

        #region Entity validation


        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/BepalingBelastbareBasisAppGeneralValidate")]
        public List<FileMessageDataContract> BepalingBelastbareBasisAppGeneralValidate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaBepalingBelastbareBasisAppGeneralDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            List<FileMessageDataContract> result = rp.BepalingBelastbareBasisAppGeneralValidate(cedc);
            rp.Close();
            return result;
        }



        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/BepalingBelastbareBasisAppBelasteReservesItemValidate")]
        public List<FileMessageDataContract> BepalingBelastbareBasisAppBelasteReservesItemValidate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaBepalingBelastbareBasisAppBelasteReservesItemDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            List<FileMessageDataContract> result = rp.BepalingBelastbareBasisAppBelasteReservesItemValidate(cedc);
            rp.Close();
            return result;
        }



        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/BepalingBelastbareBasisAppVerworpenUitgavenItemValidate")]
        public List<FileMessageDataContract> BepalingBelastbareBasisAppVerworpenUitgavenItemValidate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaBepalingBelastbareBasisAppVerworpenUitgavenItemDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            List<FileMessageDataContract> result = rp.BepalingBelastbareBasisAppVerworpenUitgavenItemValidate(cedc);
            rp.Close();
            return result;
        }



        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/BepalingBelastbareBasisAppBelastbareBestZonderAftrekItemValidate")]
        public List<FileMessageDataContract> BepalingBelastbareBasisAppBelastbareBestZonderAftrekItemValidate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaBepalingBelastbareBasisAppBelastbareBestZonderAftrekItemDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            List<FileMessageDataContract> result = rp.BepalingBelastbareBasisAppBelastbareBestZonderAftrekItemValidate(cedc);
            rp.Close();
            return result;
        }



        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/BepalingBelastbareBasisAppOpdelingNaarOorsprongItemValidate")]
        public List<FileMessageDataContract> BepalingBelastbareBasisAppOpdelingNaarOorsprongItemValidate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaBepalingBelastbareBasisAppOpdelingNaarOorsprongItemDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            List<FileMessageDataContract> result = rp.BepalingBelastbareBasisAppOpdelingNaarOorsprongItemValidate(cedc);
            rp.Close();
            return result;
        }



        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/BepalingBelastbareBasisAppFiscaleAftrekItemValidate")]
        public List<FileMessageDataContract> BepalingBelastbareBasisAppFiscaleAftrekItemValidate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaBepalingBelastbareBasisAppFiscaleAftrekItemDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            List<FileMessageDataContract> result = rp.BepalingBelastbareBasisAppFiscaleAftrekItemValidate(cedc);
            rp.Close();
            return result;
        }



        #endregion



        #region Add/Remove


        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/BepalingBelastbareBasisAppOverzichtUpdate")]
        public EY.com.eBook.RuleEngine.AY2011.BepalingBelastbareBasisAppTypes.BepalingBasis BepalingBelastbareBasisAppOverzichtUpdate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaBepalingBelastbareBasisAppGeneralDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.BepalingBelastbareBasisAppTypes.BepalingBasis result = rp.BepalingBelastbareBasisAppOverzichtUpdate(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/BepalingBelastbareBasisAppBelasteReservesUpdate")]
        public EY.com.eBook.RuleEngine.AY2011.BepalingBelastbareBasisAppTypes.BepalingBasis BepalingBelastbareBasisAppBelasteReservesUpdate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaBepalingBelastbareBasisAppBelasteReservesItemDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.BepalingBelastbareBasisAppTypes.BepalingBasis result = rp.BepalingBelastbareBasisAppBelasteReservesUpdate(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/BepalingBelastbareBasisAppVerworpenUitgavenUpdate")]
        public EY.com.eBook.RuleEngine.AY2011.BepalingBelastbareBasisAppTypes.BepalingBasis BepalingBelastbareBasisAppVerworpenUitgavenUpdate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaBepalingBelastbareBasisAppVerworpenUitgavenItemDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.BepalingBelastbareBasisAppTypes.BepalingBasis result = rp.BepalingBelastbareBasisAppVerworpenUitgavenUpdate(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/BepalingBelastbareBasisAppBelastbareBestZonderAftrekUpdate")]
        public EY.com.eBook.RuleEngine.AY2011.BepalingBelastbareBasisAppTypes.BepalingBasis BepalingBelastbareBasisAppBelastbareBestZonderAftrekUpdate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaBepalingBelastbareBasisAppBelastbareBestZonderAftrekItemDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.BepalingBelastbareBasisAppTypes.BepalingBasis result = rp.BepalingBelastbareBasisAppBelastbareBestZonderAftrekUpdate(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/BepalingBelastbareBasisAppOpdelingNaarOorsprongUpdate")]
        public EY.com.eBook.RuleEngine.AY2011.BepalingBelastbareBasisAppTypes.BepalingBasis BepalingBelastbareBasisAppOpdelingNaarOorsprongUpdate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaBepalingBelastbareBasisAppOpdelingNaarOorsprongItemDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.BepalingBelastbareBasisAppTypes.BepalingBasis result = rp.BepalingBelastbareBasisAppOpdelingNaarOorsprongUpdate(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/BepalingBelastbareBasisAppFiscaleAftrekUpdate")]
        public EY.com.eBook.RuleEngine.AY2011.BepalingBelastbareBasisAppTypes.BepalingBasis BepalingBelastbareBasisAppFiscaleAftrekUpdate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaBepalingBelastbareBasisAppFiscaleAftrekItemDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.BepalingBelastbareBasisAppTypes.BepalingBasis result = rp.BepalingBelastbareBasisAppFiscaleAftrekUpdate(cedc);
            rp.Close();
            return result;
        }

        #endregion

        #endregion



        #region DBIApp
        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/DBIAppGetData")]
        public EY.com.eBook.RuleEngine.AY2011.DBIAppTypes.DBIWorksheet DBIAppGetData(CriteriaFileDataContract cfdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.DBIAppTypes.DBIWorksheet result = rp.DBIAppGetData(cfdc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/DBIAppCalculate")]
        public EY.com.eBook.RuleEngine.AY2011.DBIAppTypes.DBIWorksheet DBIAppCalculate(CriteriaFileDataContract cfdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.DBIAppTypes.DBIWorksheet result = rp.DBIAppCalculate(cfdc);
            rp.Close();
            return result;
        }

        #region Entity validation


        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/DBIAppParticipatieItemValidate")]
        public List<FileMessageDataContract> DBIAppParticipatieItemValidate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaDBIAppParticipatieItemDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            List<FileMessageDataContract> result = rp.DBIAppParticipatieItemValidate(cedc);
            rp.Close();
            return result;
        }



        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/DBIAppDividendItemValidate")]
        public List<FileMessageDataContract> DBIAppDividendItemValidate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaDBIAppDividendItemDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            List<FileMessageDataContract> result = rp.DBIAppDividendItemValidate(cedc);
            rp.Close();
            return result;
        }



        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/DBIAppDbiEntityValidate")]
        public List<FileMessageDataContract> DBIAppDbiEntityValidate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaDBIAppDbiEntityDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            List<FileMessageDataContract> result = rp.DBIAppDbiEntityValidate(cedc);
            rp.Close();
            return result;
        }



        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/DBIAppHistoryItemValidate")]
        public List<FileMessageDataContract> DBIAppHistoryItemValidate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaDBIAppHistoryItemDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            List<FileMessageDataContract> result = rp.DBIAppHistoryItemValidate(cedc);
            rp.Close();
            return result;
        }



        #endregion



        #region Add/Remove


        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/DBIAppParticipationsAdd")]
        public EY.com.eBook.RuleEngine.AY2011.DBIAppTypes.DBIWorksheet DBIAppParticipationsAdd(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaDBIAppParticipatieItemDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.DBIAppTypes.DBIWorksheet result = rp.DBIAppParticipationsAdd(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/DBIAppParticipationsRemove")]
        public EY.com.eBook.RuleEngine.AY2011.DBIAppTypes.DBIWorksheet DBIAppParticipationsRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.DBIAppTypes.DBIWorksheet result = rp.DBIAppParticipationsRemove(cfrdc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/DBIAppParticipationsUpdate")]
        public EY.com.eBook.RuleEngine.AY2011.DBIAppTypes.DBIWorksheet DBIAppParticipationsUpdate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaDBIAppParticipatieItemDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.DBIAppTypes.DBIWorksheet result = rp.DBIAppParticipationsUpdate(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/DBIAppDividendsAdd")]
        public EY.com.eBook.RuleEngine.AY2011.DBIAppTypes.DBIWorksheet DBIAppDividendsAdd(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaDBIAppDividendItemDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.DBIAppTypes.DBIWorksheet result = rp.DBIAppDividendsAdd(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/DBIAppDividendsRemove")]
        public EY.com.eBook.RuleEngine.AY2011.DBIAppTypes.DBIWorksheet DBIAppDividendsRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.DBIAppTypes.DBIWorksheet result = rp.DBIAppDividendsRemove(cfrdc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/DBIAppDividendsUpdate")]
        public EY.com.eBook.RuleEngine.AY2011.DBIAppTypes.DBIWorksheet DBIAppDividendsUpdate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaDBIAppDividendItemDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.DBIAppTypes.DBIWorksheet result = rp.DBIAppDividendsUpdate(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/DBIAppDBIUpdate")]
        public EY.com.eBook.RuleEngine.AY2011.DBIAppTypes.DBIWorksheet DBIAppDBIUpdate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaDBIAppDbiEntityDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.DBIAppTypes.DBIWorksheet result = rp.DBIAppDBIUpdate(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/DBIAppHistoriekAdd")]
        public EY.com.eBook.RuleEngine.AY2011.DBIAppTypes.DBIWorksheet DBIAppHistoriekAdd(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaDBIAppHistoryItemDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.DBIAppTypes.DBIWorksheet result = rp.DBIAppHistoriekAdd(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/DBIAppHistoriekRemove")]
        public EY.com.eBook.RuleEngine.AY2011.DBIAppTypes.DBIWorksheet DBIAppHistoriekRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.DBIAppTypes.DBIWorksheet result = rp.DBIAppHistoriekRemove(cfrdc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/DBIAppHistoriekUpdate")]
        public EY.com.eBook.RuleEngine.AY2011.DBIAppTypes.DBIWorksheet DBIAppHistoriekUpdate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaDBIAppHistoryItemDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.DBIAppTypes.DBIWorksheet result = rp.DBIAppHistoriekUpdate(cedc);
            rp.Close();
            return result;
        }

        #endregion

        #endregion



        #region RisicoKapitaalApp
        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/RisicoKapitaalAppGetData")]
        public EY.com.eBook.RuleEngine.AY2011.RisicoKapitaalAppTypes.WPRisicokapitaal RisicoKapitaalAppGetData(CriteriaFileDataContract cfdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.RisicoKapitaalAppTypes.WPRisicokapitaal result = rp.RisicoKapitaalAppGetData(cfdc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/RisicoKapitaalAppCalculate")]
        public EY.com.eBook.RuleEngine.AY2011.RisicoKapitaalAppTypes.WPRisicokapitaal RisicoKapitaalAppCalculate(CriteriaFileDataContract cfdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.RisicoKapitaalAppTypes.WPRisicokapitaal result = rp.RisicoKapitaalAppCalculate(cfdc);
            rp.Close();
            return result;
        }

        #region Entity validation


        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/RisicoKapitaalAppFicheItemValidate")]
        public List<FileMessageDataContract> RisicoKapitaalAppFicheItemValidate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaRisicoKapitaalAppFicheItemDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            List<FileMessageDataContract> result = rp.RisicoKapitaalAppFicheItemValidate(cedc);
            rp.Close();
            return result;
        }



        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/RisicoKapitaalAppEigenVermogenValidate")]
        public List<FileMessageDataContract> RisicoKapitaalAppEigenVermogenValidate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaRisicoKapitaalAppEigenVermogenDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            List<FileMessageDataContract> result = rp.RisicoKapitaalAppEigenVermogenValidate(cedc);
            rp.Close();
            return result;
        }



        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/RisicoKapitaalAppBestanddeelValidate")]
        public List<FileMessageDataContract> RisicoKapitaalAppBestanddeelValidate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaRisicoKapitaalAppBestanddeelDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            List<FileMessageDataContract> result = rp.RisicoKapitaalAppBestanddeelValidate(cedc);
            rp.Close();
            return result;
        }



        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/RisicoKapitaalAppWijzigingItemValidate")]
        public List<FileMessageDataContract> RisicoKapitaalAppWijzigingItemValidate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaRisicoKapitaalAppWijzigingItemDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            List<FileMessageDataContract> result = rp.RisicoKapitaalAppWijzigingItemValidate(cedc);
            rp.Close();
            return result;
        }



        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/RisicoKapitaalAppTariefItemValidate")]
        public List<FileMessageDataContract> RisicoKapitaalAppTariefItemValidate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaRisicoKapitaalAppTariefItemDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            List<FileMessageDataContract> result = rp.RisicoKapitaalAppTariefItemValidate(cedc);
            rp.Close();
            return result;
        }



        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/RisicoKapitaalAppHistoryItemValidate")]
        public List<FileMessageDataContract> RisicoKapitaalAppHistoryItemValidate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaRisicoKapitaalAppHistoryItemDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            List<FileMessageDataContract> result = rp.RisicoKapitaalAppHistoryItemValidate(cedc);
            rp.Close();
            return result;
        }



        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/RisicoKapitaalAppHistoriekAanpassingItemValidate")]
        public List<FileMessageDataContract> RisicoKapitaalAppHistoriekAanpassingItemValidate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaRisicoKapitaalAppHistoriekAanpassingItemDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            List<FileMessageDataContract> result = rp.RisicoKapitaalAppHistoriekAanpassingItemValidate(cedc);
            rp.Close();
            return result;
        }



        #endregion



        #region Add/Remove


        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/RisicoKapitaalAppFicheUpdate")]
        public EY.com.eBook.RuleEngine.AY2011.RisicoKapitaalAppTypes.WPRisicokapitaal RisicoKapitaalAppFicheUpdate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaRisicoKapitaalAppFicheItemDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.RisicoKapitaalAppTypes.WPRisicokapitaal result = rp.RisicoKapitaalAppFicheUpdate(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/RisicoKapitaalAppEigenVermogensAdd")]
        public EY.com.eBook.RuleEngine.AY2011.RisicoKapitaalAppTypes.WPRisicokapitaal RisicoKapitaalAppEigenVermogensAdd(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaRisicoKapitaalAppEigenVermogenDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.RisicoKapitaalAppTypes.WPRisicokapitaal result = rp.RisicoKapitaalAppEigenVermogensAdd(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/RisicoKapitaalAppEigenVermogensRemove")]
        public EY.com.eBook.RuleEngine.AY2011.RisicoKapitaalAppTypes.WPRisicokapitaal RisicoKapitaalAppEigenVermogensRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.RisicoKapitaalAppTypes.WPRisicokapitaal result = rp.RisicoKapitaalAppEigenVermogensRemove(cfrdc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/RisicoKapitaalAppEigenVermogensUpdate")]
        public EY.com.eBook.RuleEngine.AY2011.RisicoKapitaalAppTypes.WPRisicokapitaal RisicoKapitaalAppEigenVermogensUpdate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaRisicoKapitaalAppEigenVermogenDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.RisicoKapitaalAppTypes.WPRisicokapitaal result = rp.RisicoKapitaalAppEigenVermogensUpdate(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/RisicoKapitaalAppBestanddelenAdd")]
        public EY.com.eBook.RuleEngine.AY2011.RisicoKapitaalAppTypes.WPRisicokapitaal RisicoKapitaalAppBestanddelenAdd(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaRisicoKapitaalAppBestanddeelDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.RisicoKapitaalAppTypes.WPRisicokapitaal result = rp.RisicoKapitaalAppBestanddelenAdd(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/RisicoKapitaalAppBestanddelenRemove")]
        public EY.com.eBook.RuleEngine.AY2011.RisicoKapitaalAppTypes.WPRisicokapitaal RisicoKapitaalAppBestanddelenRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.RisicoKapitaalAppTypes.WPRisicokapitaal result = rp.RisicoKapitaalAppBestanddelenRemove(cfrdc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/RisicoKapitaalAppBestanddelenUpdate")]
        public EY.com.eBook.RuleEngine.AY2011.RisicoKapitaalAppTypes.WPRisicokapitaal RisicoKapitaalAppBestanddelenUpdate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaRisicoKapitaalAppBestanddeelDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.RisicoKapitaalAppTypes.WPRisicokapitaal result = rp.RisicoKapitaalAppBestanddelenUpdate(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/RisicoKapitaalAppWijzigingenAdd")]
        public EY.com.eBook.RuleEngine.AY2011.RisicoKapitaalAppTypes.WPRisicokapitaal RisicoKapitaalAppWijzigingenAdd(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaRisicoKapitaalAppWijzigingItemDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.RisicoKapitaalAppTypes.WPRisicokapitaal result = rp.RisicoKapitaalAppWijzigingenAdd(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/RisicoKapitaalAppWijzigingenRemove")]
        public EY.com.eBook.RuleEngine.AY2011.RisicoKapitaalAppTypes.WPRisicokapitaal RisicoKapitaalAppWijzigingenRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.RisicoKapitaalAppTypes.WPRisicokapitaal result = rp.RisicoKapitaalAppWijzigingenRemove(cfrdc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/RisicoKapitaalAppWijzigingenUpdate")]
        public EY.com.eBook.RuleEngine.AY2011.RisicoKapitaalAppTypes.WPRisicokapitaal RisicoKapitaalAppWijzigingenUpdate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaRisicoKapitaalAppWijzigingItemDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.RisicoKapitaalAppTypes.WPRisicokapitaal result = rp.RisicoKapitaalAppWijzigingenUpdate(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/RisicoKapitaalAppTariefUpdate")]
        public EY.com.eBook.RuleEngine.AY2011.RisicoKapitaalAppTypes.WPRisicokapitaal RisicoKapitaalAppTariefUpdate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaRisicoKapitaalAppTariefItemDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.RisicoKapitaalAppTypes.WPRisicokapitaal result = rp.RisicoKapitaalAppTariefUpdate(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/RisicoKapitaalAppHistoriekAdd")]
        public EY.com.eBook.RuleEngine.AY2011.RisicoKapitaalAppTypes.WPRisicokapitaal RisicoKapitaalAppHistoriekAdd(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaRisicoKapitaalAppHistoryItemDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.RisicoKapitaalAppTypes.WPRisicokapitaal result = rp.RisicoKapitaalAppHistoriekAdd(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/RisicoKapitaalAppHistoriekRemove")]
        public EY.com.eBook.RuleEngine.AY2011.RisicoKapitaalAppTypes.WPRisicokapitaal RisicoKapitaalAppHistoriekRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.RisicoKapitaalAppTypes.WPRisicokapitaal result = rp.RisicoKapitaalAppHistoriekRemove(cfrdc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/RisicoKapitaalAppHistoriekUpdate")]
        public EY.com.eBook.RuleEngine.AY2011.RisicoKapitaalAppTypes.WPRisicokapitaal RisicoKapitaalAppHistoriekUpdate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaRisicoKapitaalAppHistoryItemDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.RisicoKapitaalAppTypes.WPRisicokapitaal result = rp.RisicoKapitaalAppHistoriekUpdate(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/RisicoKapitaalAppAanpassingenHistoriekAdd")]
        public EY.com.eBook.RuleEngine.AY2011.RisicoKapitaalAppTypes.WPRisicokapitaal RisicoKapitaalAppAanpassingenHistoriekAdd(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaRisicoKapitaalAppHistoriekAanpassingItemDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.RisicoKapitaalAppTypes.WPRisicokapitaal result = rp.RisicoKapitaalAppAanpassingenHistoriekAdd(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/RisicoKapitaalAppAanpassingenHistoriekRemove")]
        public EY.com.eBook.RuleEngine.AY2011.RisicoKapitaalAppTypes.WPRisicokapitaal RisicoKapitaalAppAanpassingenHistoriekRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.RisicoKapitaalAppTypes.WPRisicokapitaal result = rp.RisicoKapitaalAppAanpassingenHistoriekRemove(cfrdc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/RisicoKapitaalAppAanpassingenHistoriekUpdate")]
        public EY.com.eBook.RuleEngine.AY2011.RisicoKapitaalAppTypes.WPRisicokapitaal RisicoKapitaalAppAanpassingenHistoriekUpdate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaRisicoKapitaalAppHistoriekAanpassingItemDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.RisicoKapitaalAppTypes.WPRisicokapitaal result = rp.RisicoKapitaalAppAanpassingenHistoriekUpdate(cedc);
            rp.Close();
            return result;
        }

        #endregion

        #endregion



        #region TransferTaxCreditsApp
        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/TransferTaxCreditsAppGetData")]
        public EY.com.eBook.RuleEngine.AY2011.TransferTaxCreditsAppTypes.TransferTaxCredits TransferTaxCreditsAppGetData(CriteriaFileDataContract cfdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.TransferTaxCreditsAppTypes.TransferTaxCredits result = rp.TransferTaxCreditsAppGetData(cfdc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/TransferTaxCreditsAppCalculate")]
        public EY.com.eBook.RuleEngine.AY2011.TransferTaxCreditsAppTypes.TransferTaxCredits TransferTaxCreditsAppCalculate(CriteriaFileDataContract cfdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.TransferTaxCreditsAppTypes.TransferTaxCredits result = rp.TransferTaxCreditsAppCalculate(cfdc);
            rp.Close();
            return result;
        }

        #region Entity validation


        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/TransferTaxCreditsAppFiscaalVerliesValidate")]
        public List<FileMessageDataContract> TransferTaxCreditsAppFiscaalVerliesValidate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaTransferTaxCreditsAppFiscaalVerliesDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            List<FileMessageDataContract> result = rp.TransferTaxCreditsAppFiscaalVerliesValidate(cedc);
            rp.Close();
            return result;
        }



        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/TransferTaxCreditsAppNIDItemValidate")]
        public List<FileMessageDataContract> TransferTaxCreditsAppNIDItemValidate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaTransferTaxCreditsAppNIDItemDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            List<FileMessageDataContract> result = rp.TransferTaxCreditsAppNIDItemValidate(cedc);
            rp.Close();
            return result;
        }



        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/TransferTaxCreditsAppDBIItemValidate")]
        public List<FileMessageDataContract> TransferTaxCreditsAppDBIItemValidate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaTransferTaxCreditsAppDBIItemDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            List<FileMessageDataContract> result = rp.TransferTaxCreditsAppDBIItemValidate(cedc);
            rp.Close();
            return result;
        }



        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/TransferTaxCreditsAppInvestItemValidate")]
        public List<FileMessageDataContract> TransferTaxCreditsAppInvestItemValidate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaTransferTaxCreditsAppInvestItemDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            List<FileMessageDataContract> result = rp.TransferTaxCreditsAppInvestItemValidate(cedc);
            rp.Close();
            return result;
        }



        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/TransferTaxCreditsAppBelastingkredietItemValidate")]
        public List<FileMessageDataContract> TransferTaxCreditsAppBelastingkredietItemValidate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaTransferTaxCreditsAppBelastingkredietItemDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            List<FileMessageDataContract> result = rp.TransferTaxCreditsAppBelastingkredietItemValidate(cedc);
            rp.Close();
            return result;
        }



        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/TransferTaxCreditsAppAanpassingItemValidate")]
        public List<FileMessageDataContract> TransferTaxCreditsAppAanpassingItemValidate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaTransferTaxCreditsAppAanpassingItemDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            List<FileMessageDataContract> result = rp.TransferTaxCreditsAppAanpassingItemValidate(cedc);
            rp.Close();
            return result;
        }



        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/TransferTaxCreditsAppAanwendingValidate")]
        public List<FileMessageDataContract> TransferTaxCreditsAppAanwendingValidate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaTransferTaxCreditsAppAanwendingDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            List<FileMessageDataContract> result = rp.TransferTaxCreditsAppAanwendingValidate(cedc);
            rp.Close();
            return result;
        }



        #endregion



        #region Add/Remove


        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/TransferTaxCreditsAppFiscaleVerliezenAdd")]
        public EY.com.eBook.RuleEngine.AY2011.TransferTaxCreditsAppTypes.TransferTaxCredits TransferTaxCreditsAppFiscaleVerliezenAdd(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaTransferTaxCreditsAppFiscaalVerliesDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.TransferTaxCreditsAppTypes.TransferTaxCredits result = rp.TransferTaxCreditsAppFiscaleVerliezenAdd(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/TransferTaxCreditsAppFiscaleVerliezenRemove")]
        public EY.com.eBook.RuleEngine.AY2011.TransferTaxCreditsAppTypes.TransferTaxCredits TransferTaxCreditsAppFiscaleVerliezenRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.TransferTaxCreditsAppTypes.TransferTaxCredits result = rp.TransferTaxCreditsAppFiscaleVerliezenRemove(cfrdc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/TransferTaxCreditsAppFiscaleVerliezenUpdate")]
        public EY.com.eBook.RuleEngine.AY2011.TransferTaxCreditsAppTypes.TransferTaxCredits TransferTaxCreditsAppFiscaleVerliezenUpdate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaTransferTaxCreditsAppFiscaalVerliesDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.TransferTaxCreditsAppTypes.TransferTaxCredits result = rp.TransferTaxCreditsAppFiscaleVerliezenUpdate(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/TransferTaxCreditsAppDBIUpdate")]
        public EY.com.eBook.RuleEngine.AY2011.TransferTaxCreditsAppTypes.TransferTaxCredits TransferTaxCreditsAppDBIUpdate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaTransferTaxCreditsAppDBIItemDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.TransferTaxCreditsAppTypes.TransferTaxCredits result = rp.TransferTaxCreditsAppDBIUpdate(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/TransferTaxCreditsAppInvestAdd")]
        public EY.com.eBook.RuleEngine.AY2011.TransferTaxCreditsAppTypes.TransferTaxCredits TransferTaxCreditsAppInvestAdd(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaTransferTaxCreditsAppInvestItemDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.TransferTaxCreditsAppTypes.TransferTaxCredits result = rp.TransferTaxCreditsAppInvestAdd(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/TransferTaxCreditsAppInvestRemove")]
        public EY.com.eBook.RuleEngine.AY2011.TransferTaxCreditsAppTypes.TransferTaxCredits TransferTaxCreditsAppInvestRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.TransferTaxCreditsAppTypes.TransferTaxCredits result = rp.TransferTaxCreditsAppInvestRemove(cfrdc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/TransferTaxCreditsAppInvestUpdate")]
        public EY.com.eBook.RuleEngine.AY2011.TransferTaxCreditsAppTypes.TransferTaxCredits TransferTaxCreditsAppInvestUpdate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaTransferTaxCreditsAppInvestItemDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.TransferTaxCreditsAppTypes.TransferTaxCredits result = rp.TransferTaxCreditsAppInvestUpdate(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/TransferTaxCreditsAppBelastingkredietAdd")]
        public EY.com.eBook.RuleEngine.AY2011.TransferTaxCreditsAppTypes.TransferTaxCredits TransferTaxCreditsAppBelastingkredietAdd(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaTransferTaxCreditsAppBelastingkredietItemDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.TransferTaxCreditsAppTypes.TransferTaxCredits result = rp.TransferTaxCreditsAppBelastingkredietAdd(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/TransferTaxCreditsAppBelastingkredietRemove")]
        public EY.com.eBook.RuleEngine.AY2011.TransferTaxCreditsAppTypes.TransferTaxCredits TransferTaxCreditsAppBelastingkredietRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.TransferTaxCreditsAppTypes.TransferTaxCredits result = rp.TransferTaxCreditsAppBelastingkredietRemove(cfrdc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/TransferTaxCreditsAppBelastingkredietUpdate")]
        public EY.com.eBook.RuleEngine.AY2011.TransferTaxCreditsAppTypes.TransferTaxCredits TransferTaxCreditsAppBelastingkredietUpdate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaTransferTaxCreditsAppBelastingkredietItemDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.TransferTaxCreditsAppTypes.TransferTaxCredits result = rp.TransferTaxCreditsAppBelastingkredietUpdate(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/TransferTaxCreditsAppAanpassingenAdd")]
        public EY.com.eBook.RuleEngine.AY2011.TransferTaxCreditsAppTypes.TransferTaxCredits TransferTaxCreditsAppAanpassingenAdd(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaTransferTaxCreditsAppAanpassingItemDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.TransferTaxCreditsAppTypes.TransferTaxCredits result = rp.TransferTaxCreditsAppAanpassingenAdd(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/TransferTaxCreditsAppAanpassingenRemove")]
        public EY.com.eBook.RuleEngine.AY2011.TransferTaxCreditsAppTypes.TransferTaxCredits TransferTaxCreditsAppAanpassingenRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.TransferTaxCreditsAppTypes.TransferTaxCredits result = rp.TransferTaxCreditsAppAanpassingenRemove(cfrdc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/TransferTaxCreditsAppAanpassingenUpdate")]
        public EY.com.eBook.RuleEngine.AY2011.TransferTaxCreditsAppTypes.TransferTaxCredits TransferTaxCreditsAppAanpassingenUpdate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaTransferTaxCreditsAppAanpassingItemDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.TransferTaxCreditsAppTypes.TransferTaxCredits result = rp.TransferTaxCreditsAppAanpassingenUpdate(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/TransferTaxCreditsAppAanwendingenUpdate")]
        public EY.com.eBook.RuleEngine.AY2011.TransferTaxCreditsAppTypes.TransferTaxCredits TransferTaxCreditsAppAanwendingenUpdate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaTransferTaxCreditsAppAanwendingDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.TransferTaxCreditsAppTypes.TransferTaxCredits result = rp.TransferTaxCreditsAppAanwendingenUpdate(cedc);
            rp.Close();
            return result;
        }

        #endregion

        #endregion



        #region BerekeningVenBApp
        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/BerekeningVenBAppGetData")]
        public EY.com.eBook.RuleEngine.AY2011.BerekeningVenBAppTypes.BerekeningVenB BerekeningVenBAppGetData(CriteriaFileDataContract cfdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.BerekeningVenBAppTypes.BerekeningVenB result = rp.BerekeningVenBAppGetData(cfdc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/BerekeningVenBAppCalculate")]
        public EY.com.eBook.RuleEngine.AY2011.BerekeningVenBAppTypes.BerekeningVenB BerekeningVenBAppCalculate(CriteriaFileDataContract cfdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.BerekeningVenBAppTypes.BerekeningVenB result = rp.BerekeningVenBAppCalculate(cfdc);
            rp.Close();
            return result;
        }

        #region Entity validation


        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/BerekeningVenBAppGeneralValidate")]
        public List<FileMessageDataContract> BerekeningVenBAppGeneralValidate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaBerekeningVenBAppGeneralDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            List<FileMessageDataContract> result = rp.BerekeningVenBAppGeneralValidate(cedc);
            rp.Close();
            return result;
        }



        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/BerekeningVenBAppBelasteReservesItemValidate")]
        public List<FileMessageDataContract> BerekeningVenBAppBelasteReservesItemValidate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaBerekeningVenBAppBelasteReservesItemDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            List<FileMessageDataContract> result = rp.BerekeningVenBAppBelasteReservesItemValidate(cedc);
            rp.Close();
            return result;
        }



        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/BerekeningVenBAppVerworpenUitgavenItemValidate")]
        public List<FileMessageDataContract> BerekeningVenBAppVerworpenUitgavenItemValidate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaBerekeningVenBAppVerworpenUitgavenItemDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            List<FileMessageDataContract> result = rp.BerekeningVenBAppVerworpenUitgavenItemValidate(cedc);
            rp.Close();
            return result;
        }



        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/BerekeningVenBAppBelastbareBestZonderAftrekItemValidate")]
        public List<FileMessageDataContract> BerekeningVenBAppBelastbareBestZonderAftrekItemValidate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaBerekeningVenBAppBelastbareBestZonderAftrekItemDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            List<FileMessageDataContract> result = rp.BerekeningVenBAppBelastbareBestZonderAftrekItemValidate(cedc);
            rp.Close();
            return result;
        }



        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/BerekeningVenBAppOpdelingNaarOorsprongItemValidate")]
        public List<FileMessageDataContract> BerekeningVenBAppOpdelingNaarOorsprongItemValidate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaBerekeningVenBAppOpdelingNaarOorsprongItemDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            List<FileMessageDataContract> result = rp.BerekeningVenBAppOpdelingNaarOorsprongItemValidate(cedc);
            rp.Close();
            return result;
        }



        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/BerekeningVenBAppFiscaleAftrekItemValidate")]
        public List<FileMessageDataContract> BerekeningVenBAppFiscaleAftrekItemValidate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaBerekeningVenBAppFiscaleAftrekItemDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            List<FileMessageDataContract> result = rp.BerekeningVenBAppFiscaleAftrekItemValidate(cedc);
            rp.Close();
            return result;
        }



        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/BerekeningVenBAppTariefItemValidate")]
        public List<FileMessageDataContract> BerekeningVenBAppTariefItemValidate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaBerekeningVenBAppTariefItemDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            List<FileMessageDataContract> result = rp.BerekeningVenBAppTariefItemValidate(cedc);
            rp.Close();
            return result;
        }



        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/BerekeningVenBAppBelastingsBerekeningItemValidate")]
        public List<FileMessageDataContract> BerekeningVenBAppBelastingsBerekeningItemValidate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaBerekeningVenBAppBelastingsBerekeningItemDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            List<FileMessageDataContract> result = rp.BerekeningVenBAppBelastingsBerekeningItemValidate(cedc);
            rp.Close();
            return result;
        }



        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/BerekeningVenBAppBelastingsBerekeningItem2Validate")]
        public List<FileMessageDataContract> BerekeningVenBAppBelastingsBerekeningItem2Validate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaBerekeningVenBAppBelastingsBerekeningItem2DataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            List<FileMessageDataContract> result = rp.BerekeningVenBAppBelastingsBerekeningItem2Validate(cedc);
            rp.Close();
            return result;
        }



        #endregion



        #region Add/Remove


        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/BerekeningVenBAppOverzichtUpdate")]
        public EY.com.eBook.RuleEngine.AY2011.BerekeningVenBAppTypes.BerekeningVenB BerekeningVenBAppOverzichtUpdate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaBerekeningVenBAppGeneralDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.BerekeningVenBAppTypes.BerekeningVenB result = rp.BerekeningVenBAppOverzichtUpdate(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/BerekeningVenBAppBelasteReservesUpdate")]
        public EY.com.eBook.RuleEngine.AY2011.BerekeningVenBAppTypes.BerekeningVenB BerekeningVenBAppBelasteReservesUpdate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaBerekeningVenBAppBelasteReservesItemDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.BerekeningVenBAppTypes.BerekeningVenB result = rp.BerekeningVenBAppBelasteReservesUpdate(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/BerekeningVenBAppVerworpenUitgavenUpdate")]
        public EY.com.eBook.RuleEngine.AY2011.BerekeningVenBAppTypes.BerekeningVenB BerekeningVenBAppVerworpenUitgavenUpdate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaBerekeningVenBAppVerworpenUitgavenItemDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.BerekeningVenBAppTypes.BerekeningVenB result = rp.BerekeningVenBAppVerworpenUitgavenUpdate(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/BerekeningVenBAppBelastbareBestZonderAftrekUpdate")]
        public EY.com.eBook.RuleEngine.AY2011.BerekeningVenBAppTypes.BerekeningVenB BerekeningVenBAppBelastbareBestZonderAftrekUpdate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaBerekeningVenBAppBelastbareBestZonderAftrekItemDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.BerekeningVenBAppTypes.BerekeningVenB result = rp.BerekeningVenBAppBelastbareBestZonderAftrekUpdate(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/BerekeningVenBAppOpdelingNaarOorsprongUpdate")]
        public EY.com.eBook.RuleEngine.AY2011.BerekeningVenBAppTypes.BerekeningVenB BerekeningVenBAppOpdelingNaarOorsprongUpdate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaBerekeningVenBAppOpdelingNaarOorsprongItemDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.BerekeningVenBAppTypes.BerekeningVenB result = rp.BerekeningVenBAppOpdelingNaarOorsprongUpdate(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/BerekeningVenBAppFiscaleAftrekUpdate")]
        public EY.com.eBook.RuleEngine.AY2011.BerekeningVenBAppTypes.BerekeningVenB BerekeningVenBAppFiscaleAftrekUpdate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaBerekeningVenBAppFiscaleAftrekItemDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.BerekeningVenBAppTypes.BerekeningVenB result = rp.BerekeningVenBAppFiscaleAftrekUpdate(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/BerekeningVenBAppTariefUpdate")]
        public EY.com.eBook.RuleEngine.AY2011.BerekeningVenBAppTypes.BerekeningVenB BerekeningVenBAppTariefUpdate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaBerekeningVenBAppTariefItemDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.BerekeningVenBAppTypes.BerekeningVenB result = rp.BerekeningVenBAppTariefUpdate(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/BerekeningVenBAppBelastingsBerekeningUpdate")]
        public EY.com.eBook.RuleEngine.AY2011.BerekeningVenBAppTypes.BerekeningVenB BerekeningVenBAppBelastingsBerekeningUpdate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaBerekeningVenBAppBelastingsBerekeningItemDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.BerekeningVenBAppTypes.BerekeningVenB result = rp.BerekeningVenBAppBelastingsBerekeningUpdate(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/BerekeningVenBAppBelastingsBerekening2Update")]
        public EY.com.eBook.RuleEngine.AY2011.BerekeningVenBAppTypes.BerekeningVenB BerekeningVenBAppBelastingsBerekening2Update(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaBerekeningVenBAppBelastingsBerekeningItem2DataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.BerekeningVenBAppTypes.BerekeningVenB result = rp.BerekeningVenBAppBelastingsBerekening2Update(cedc);
            rp.Close();
            return result;
        }

        #endregion

        #endregion



        #region AangifteVenBApp
        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/AangifteVenBAppGetData")]
        public EY.com.eBook.RuleEngine.AY2011.AangifteVenBAppTypes.AangifteVenB AangifteVenBAppGetData(CriteriaFileDataContract cfdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.AangifteVenBAppTypes.AangifteVenB result = rp.AangifteVenBAppGetData(cfdc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/AangifteVenBAppCalculate")]
        public EY.com.eBook.RuleEngine.AY2011.AangifteVenBAppTypes.AangifteVenB AangifteVenBAppCalculate(CriteriaFileDataContract cfdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.AangifteVenBAppTypes.AangifteVenB result = rp.AangifteVenBAppCalculate(cfdc);
            rp.Close();
            return result;
        }

        #region Entity validation


        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/AangifteVenBAppReserveEntityValidate")]
        public List<FileMessageDataContract> AangifteVenBAppReserveEntityValidate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaAangifteVenBAppReserveEntityDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            List<FileMessageDataContract> result = rp.AangifteVenBAppReserveEntityValidate(cedc);
            rp.Close();
            return result;
        }



        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/AangifteVenBAppVrijgesteldeReserveEntityValidate")]
        public List<FileMessageDataContract> AangifteVenBAppVrijgesteldeReserveEntityValidate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaAangifteVenBAppVrijgesteldeReserveEntityDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            List<FileMessageDataContract> result = rp.AangifteVenBAppVrijgesteldeReserveEntityValidate(cedc);
            rp.Close();
            return result;
        }



        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/AangifteVenBAppVerworpenUitgavenEntityValidate")]
        public List<FileMessageDataContract> AangifteVenBAppVerworpenUitgavenEntityValidate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaAangifteVenBAppVerworpenUitgavenEntityDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            List<FileMessageDataContract> result = rp.AangifteVenBAppVerworpenUitgavenEntityValidate(cedc);
            rp.Close();
            return result;
        }



        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/AangifteVenBAppUitgekeerdeDividendenEntityValidate")]
        public List<FileMessageDataContract> AangifteVenBAppUitgekeerdeDividendenEntityValidate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaAangifteVenBAppUitgekeerdeDividendenEntityDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            List<FileMessageDataContract> result = rp.AangifteVenBAppUitgekeerdeDividendenEntityValidate(cedc);
            rp.Close();
            return result;
        }



        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/AangifteVenBAppWinstEntityValidate")]
        public List<FileMessageDataContract> AangifteVenBAppWinstEntityValidate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaAangifteVenBAppWinstEntityDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            List<FileMessageDataContract> result = rp.AangifteVenBAppWinstEntityValidate(cedc);
            rp.Close();
            return result;
        }



        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/AangifteVenBAppAfzonderlijkeAanslagenEntityValidate")]
        public List<FileMessageDataContract> AangifteVenBAppAfzonderlijkeAanslagenEntityValidate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaAangifteVenBAppAfzonderlijkeAanslagenEntityDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            List<FileMessageDataContract> result = rp.AangifteVenBAppAfzonderlijkeAanslagenEntityValidate(cedc);
            rp.Close();
            return result;
        }



        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/AangifteVenBAppDiamantEntityValidate")]
        public List<FileMessageDataContract> AangifteVenBAppDiamantEntityValidate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaAangifteVenBAppDiamantEntityDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            List<FileMessageDataContract> result = rp.AangifteVenBAppDiamantEntityValidate(cedc);
            rp.Close();
            return result;
        }



        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/AangifteVenBAppNietBelastbareBestandelenItemValidate")]
        public List<FileMessageDataContract> AangifteVenBAppNietBelastbareBestandelenItemValidate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaAangifteVenBAppNietBelastbareBestandelenItemDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            List<FileMessageDataContract> result = rp.AangifteVenBAppNietBelastbareBestandelenItemValidate(cedc);
            rp.Close();
            return result;
        }



        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/AangifteVenBAppDbiEntityValidate")]
        public List<FileMessageDataContract> AangifteVenBAppDbiEntityValidate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaAangifteVenBAppDbiEntityDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            List<FileMessageDataContract> result = rp.AangifteVenBAppDbiEntityValidate(cedc);
            rp.Close();
            return result;
        }



        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/AangifteVenBAppOverdrachtDBIAftrekEntityValidate")]
        public List<FileMessageDataContract> AangifteVenBAppOverdrachtDBIAftrekEntityValidate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaAangifteVenBAppOverdrachtDBIAftrekEntityDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            List<FileMessageDataContract> result = rp.AangifteVenBAppOverdrachtDBIAftrekEntityValidate(cedc);
            rp.Close();
            return result;
        }



        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/AangifteVenBAppOverdrachtNIDEntityValidate")]
        public List<FileMessageDataContract> AangifteVenBAppOverdrachtNIDEntityValidate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaAangifteVenBAppOverdrachtNIDEntityDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            List<FileMessageDataContract> result = rp.AangifteVenBAppOverdrachtNIDEntityValidate(cedc);
            rp.Close();
            return result;
        }



        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/AangifteVenBAppCompenseerbareVerliezenEntityValidate")]
        public List<FileMessageDataContract> AangifteVenBAppCompenseerbareVerliezenEntityValidate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaAangifteVenBAppCompenseerbareVerliezenEntityDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            List<FileMessageDataContract> result = rp.AangifteVenBAppCompenseerbareVerliezenEntityValidate(cedc);
            rp.Close();
            return result;
        }



        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/AangifteVenBAppTariefEntityValidate")]
        public List<FileMessageDataContract> AangifteVenBAppTariefEntityValidate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaAangifteVenBAppTariefEntityDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            List<FileMessageDataContract> result = rp.AangifteVenBAppTariefEntityValidate(cedc);
            rp.Close();
            return result;
        }



        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/AangifteVenBAppVoorafBetalingEntityValidate")]
        public List<FileMessageDataContract> AangifteVenBAppVoorafBetalingEntityValidate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaAangifteVenBAppVoorafBetalingEntityDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            List<FileMessageDataContract> result = rp.AangifteVenBAppVoorafBetalingEntityValidate(cedc);
            rp.Close();
            return result;
        }



        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/AangifteVenBAppVerrekenbareVoorheffingEntityValidate")]
        public List<FileMessageDataContract> AangifteVenBAppVerrekenbareVoorheffingEntityValidate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaAangifteVenBAppVerrekenbareVoorheffingEntityDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            List<FileMessageDataContract> result = rp.AangifteVenBAppVerrekenbareVoorheffingEntityValidate(cedc);
            rp.Close();
            return result;
        }



        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/AangifteVenBAppTaxShelterEntityValidate")]
        public List<FileMessageDataContract> AangifteVenBAppTaxShelterEntityValidate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaAangifteVenBAppTaxShelterEntityDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            List<FileMessageDataContract> result = rp.AangifteVenBAppTaxShelterEntityValidate(cedc);
            rp.Close();
            return result;
        }



        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/AangifteVenBAppGrootteVennootschapeEntityValidate")]
        public List<FileMessageDataContract> AangifteVenBAppGrootteVennootschapeEntityValidate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaAangifteVenBAppGrootteVennootschapeEntityDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            List<FileMessageDataContract> result = rp.AangifteVenBAppGrootteVennootschapeEntityValidate(cedc);
            rp.Close();
            return result;
        }



        #endregion



        #region Add/Remove


        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/AangifteVenBAppReservesUpdate")]
        public EY.com.eBook.RuleEngine.AY2011.AangifteVenBAppTypes.AangifteVenB AangifteVenBAppReservesUpdate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaAangifteVenBAppReserveEntityDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.AangifteVenBAppTypes.AangifteVenB result = rp.AangifteVenBAppReservesUpdate(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/AangifteVenBAppVrijgesteldeReserveUpdate")]
        public EY.com.eBook.RuleEngine.AY2011.AangifteVenBAppTypes.AangifteVenB AangifteVenBAppVrijgesteldeReserveUpdate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaAangifteVenBAppVrijgesteldeReserveEntityDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.AangifteVenBAppTypes.AangifteVenB result = rp.AangifteVenBAppVrijgesteldeReserveUpdate(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/AangifteVenBAppVerworpenUitgavenUpdate")]
        public EY.com.eBook.RuleEngine.AY2011.AangifteVenBAppTypes.AangifteVenB AangifteVenBAppVerworpenUitgavenUpdate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaAangifteVenBAppVerworpenUitgavenEntityDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.AangifteVenBAppTypes.AangifteVenB result = rp.AangifteVenBAppVerworpenUitgavenUpdate(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/AangifteVenBAppUitgekeerdeDividendenUpdate")]
        public EY.com.eBook.RuleEngine.AY2011.AangifteVenBAppTypes.AangifteVenB AangifteVenBAppUitgekeerdeDividendenUpdate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaAangifteVenBAppUitgekeerdeDividendenEntityDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.AangifteVenBAppTypes.AangifteVenB result = rp.AangifteVenBAppUitgekeerdeDividendenUpdate(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/AangifteVenBAppUiteenzettingWinstUpdate")]
        public EY.com.eBook.RuleEngine.AY2011.AangifteVenBAppTypes.AangifteVenB AangifteVenBAppUiteenzettingWinstUpdate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaAangifteVenBAppWinstEntityDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.AangifteVenBAppTypes.AangifteVenB result = rp.AangifteVenBAppUiteenzettingWinstUpdate(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/AangifteVenBAppAfzonderlijkeAanslagenUpdate")]
        public EY.com.eBook.RuleEngine.AY2011.AangifteVenBAppTypes.AangifteVenB AangifteVenBAppAfzonderlijkeAanslagenUpdate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaAangifteVenBAppAfzonderlijkeAanslagenEntityDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.AangifteVenBAppTypes.AangifteVenB result = rp.AangifteVenBAppAfzonderlijkeAanslagenUpdate(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/AangifteVenBAppDiamantUpdate")]
        public EY.com.eBook.RuleEngine.AY2011.AangifteVenBAppTypes.AangifteVenB AangifteVenBAppDiamantUpdate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaAangifteVenBAppDiamantEntityDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.AangifteVenBAppTypes.AangifteVenB result = rp.AangifteVenBAppDiamantUpdate(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/AangifteVenBAppNietBelastbareBestanddelenUpdate")]
        public EY.com.eBook.RuleEngine.AY2011.AangifteVenBAppTypes.AangifteVenB AangifteVenBAppNietBelastbareBestanddelenUpdate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaAangifteVenBAppNietBelastbareBestandelenItemDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.AangifteVenBAppTypes.AangifteVenB result = rp.AangifteVenBAppNietBelastbareBestanddelenUpdate(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/AangifteVenBAppDBIUpdate")]
        public EY.com.eBook.RuleEngine.AY2011.AangifteVenBAppTypes.AangifteVenB AangifteVenBAppDBIUpdate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaAangifteVenBAppDbiEntityDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.AangifteVenBAppTypes.AangifteVenB result = rp.AangifteVenBAppDBIUpdate(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/AangifteVenBAppOverdrachtDBIAftrekUpdate")]
        public EY.com.eBook.RuleEngine.AY2011.AangifteVenBAppTypes.AangifteVenB AangifteVenBAppOverdrachtDBIAftrekUpdate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaAangifteVenBAppOverdrachtDBIAftrekEntityDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.AangifteVenBAppTypes.AangifteVenB result = rp.AangifteVenBAppOverdrachtDBIAftrekUpdate(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/AangifteVenBAppOverdrachtNIDUpdate")]
        public EY.com.eBook.RuleEngine.AY2011.AangifteVenBAppTypes.AangifteVenB AangifteVenBAppOverdrachtNIDUpdate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaAangifteVenBAppOverdrachtNIDEntityDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.AangifteVenBAppTypes.AangifteVenB result = rp.AangifteVenBAppOverdrachtNIDUpdate(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/AangifteVenBAppCompenseerbareVerliezenUpdate")]
        public EY.com.eBook.RuleEngine.AY2011.AangifteVenBAppTypes.AangifteVenB AangifteVenBAppCompenseerbareVerliezenUpdate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaAangifteVenBAppCompenseerbareVerliezenEntityDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.AangifteVenBAppTypes.AangifteVenB result = rp.AangifteVenBAppCompenseerbareVerliezenUpdate(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/AangifteVenBAppTariefBelastingUpdate")]
        public EY.com.eBook.RuleEngine.AY2011.AangifteVenBAppTypes.AangifteVenB AangifteVenBAppTariefBelastingUpdate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaAangifteVenBAppTariefEntityDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.AangifteVenBAppTypes.AangifteVenB result = rp.AangifteVenBAppTariefBelastingUpdate(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/AangifteVenBAppVoorafBetalingenUpdate")]
        public EY.com.eBook.RuleEngine.AY2011.AangifteVenBAppTypes.AangifteVenB AangifteVenBAppVoorafBetalingenUpdate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaAangifteVenBAppVoorafBetalingEntityDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.AangifteVenBAppTypes.AangifteVenB result = rp.AangifteVenBAppVoorafBetalingenUpdate(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/AangifteVenBAppVerrekenbareVoorheffingUpdate")]
        public EY.com.eBook.RuleEngine.AY2011.AangifteVenBAppTypes.AangifteVenB AangifteVenBAppVerrekenbareVoorheffingUpdate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaAangifteVenBAppVerrekenbareVoorheffingEntityDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.AangifteVenBAppTypes.AangifteVenB result = rp.AangifteVenBAppVerrekenbareVoorheffingUpdate(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/AangifteVenBAppTaxShelterUpdate")]
        public EY.com.eBook.RuleEngine.AY2011.AangifteVenBAppTypes.AangifteVenB AangifteVenBAppTaxShelterUpdate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaAangifteVenBAppTaxShelterEntityDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.AangifteVenBAppTypes.AangifteVenB result = rp.AangifteVenBAppTaxShelterUpdate(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/AangifteVenBAppGrootteVennootschapUpdate")]
        public EY.com.eBook.RuleEngine.AY2011.AangifteVenBAppTypes.AangifteVenB AangifteVenBAppGrootteVennootschapUpdate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaAangifteVenBAppGrootteVennootschapeEntityDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.AangifteVenBAppTypes.AangifteVenB result = rp.AangifteVenBAppGrootteVennootschapUpdate(cedc);
            rp.Close();
            return result;
        }

        #endregion

        #endregion



        #endregion

        
    }
}


/*
[WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
       RequestFormat = WebMessageFormat.Json,
       ResponseFormat = WebMessageFormat.Json,
       UriTemplate = "/FacturatieAppGetData")]
        public EY.com.eBook.RuleEngine.AY2011.FacturatieAppTypes.Facturatie FacturatieAppGetData(CriteriaFileDataContract cfdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.FacturatieAppTypes.Facturatie result = rp.FacturatieAppGetData(cfdc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
       RequestFormat = WebMessageFormat.Json,
       ResponseFormat = WebMessageFormat.Json,
       UriTemplate = "/FacturatieAppCalculate")]
        public EY.com.eBook.RuleEngine.AY2011.FacturatieAppTypes.Facturatie FacturatieAppCalculate(CriteriaFileDataContract cfdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.FacturatieAppTypes.Facturatie result = rp.FacturatieAppCalculate(cfdc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
       RequestFormat = WebMessageFormat.Json,
       ResponseFormat = WebMessageFormat.Json,
       UriTemplate = "/FacturatieAppFacturatieKlantValidate")]
        public List<InvalidField> FacturatieAppFacturatieKlantValidate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaFacturatieAppFacturatieKlantDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            List<InvalidField> result = rp.FacturatieAppFacturatieKlantValidate(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
       RequestFormat = WebMessageFormat.Json,
       ResponseFormat = WebMessageFormat.Json,
       UriTemplate = "/FacturatieAppFacturatieLeverancierValidate")]
        public List<InvalidField> FacturatieAppFacturatieLeverancierValidate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaFacturatieAppFacturatieLeverancierDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            List<InvalidField> result = rp.FacturatieAppFacturatieLeverancierValidate(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
       RequestFormat = WebMessageFormat.Json,
       ResponseFormat = WebMessageFormat.Json,
       UriTemplate = "/FacturatieAppFacturatieLeverancierCNValidate")]
        public List<InvalidField> FacturatieAppFacturatieLeverancierCNValidate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaFacturatieAppFacturatieLeverancierCNDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            List<InvalidField> result = rp.FacturatieAppFacturatieLeverancierCNValidate(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
       RequestFormat = WebMessageFormat.Json,
       ResponseFormat = WebMessageFormat.Json,
       UriTemplate = "/FacturatieAppFacturatieKlantCNValidate")]
        public List<InvalidField> FacturatieAppFacturatieKlantCNValidate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaFacturatieAppFacturatieKlantCNDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            List<InvalidField> result = rp.FacturatieAppFacturatieKlantCNValidate(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
       RequestFormat = WebMessageFormat.Json,
       ResponseFormat = WebMessageFormat.Json,
       UriTemplate = "/FacturatieAppOpTeMakenFacturenAdd")]
        public EY.com.eBook.RuleEngine.AY2011.FacturatieAppTypes.Facturatie FacturatieAppOpTeMakenFacturenAdd(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaFacturatieAppFacturatieKlantDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.FacturatieAppTypes.Facturatie result = rp.FacturatieAppOpTeMakenFacturenAdd(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
       RequestFormat = WebMessageFormat.Json,
       ResponseFormat = WebMessageFormat.Json,
       UriTemplate = "/FacturatieAppOpTeMakenFacturenUpdate")]
        public EY.com.eBook.RuleEngine.AY2011.FacturatieAppTypes.Facturatie FacturatieAppOpTeMakenFacturenUpdate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaFacturatieAppFacturatieKlantDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.FacturatieAppTypes.Facturatie result = rp.FacturatieAppOpTeMakenFacturenUpdate(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
       RequestFormat = WebMessageFormat.Json,
       ResponseFormat = WebMessageFormat.Json,
       UriTemplate = "/FacturatieAppOpTeMakenFacturenRemove")]
        public EY.com.eBook.RuleEngine.AY2011.FacturatieAppTypes.Facturatie FacturatieAppOpTeMakenFacturenRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.FacturatieAppTypes.Facturatie result = rp.FacturatieAppOpTeMakenFacturenRemove(cfrdc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
       RequestFormat = WebMessageFormat.Json,
       ResponseFormat = WebMessageFormat.Json,
       UriTemplate = "/FacturatieAppTeOntvangenCNAdd")]
        public EY.com.eBook.RuleEngine.AY2011.FacturatieAppTypes.Facturatie FacturatieAppTeOntvangenCNAdd(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaFacturatieAppFacturatieLeverancierCNDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.FacturatieAppTypes.Facturatie result = rp.FacturatieAppTeOntvangenCNAdd(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
       RequestFormat = WebMessageFormat.Json,
       ResponseFormat = WebMessageFormat.Json,
       UriTemplate = "/FacturatieAppTeOntvangenCNUpdate")]
        public EY.com.eBook.RuleEngine.AY2011.FacturatieAppTypes.Facturatie FacturatieAppTeOntvangenCNUpdate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaFacturatieAppFacturatieLeverancierCNDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.FacturatieAppTypes.Facturatie result = rp.FacturatieAppTeOntvangenCNUpdate(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
       RequestFormat = WebMessageFormat.Json,
       ResponseFormat = WebMessageFormat.Json,
       UriTemplate = "/FacturatieAppTeOntvangenCNRemove")]
        public EY.com.eBook.RuleEngine.AY2011.FacturatieAppTypes.Facturatie FacturatieAppTeOntvangenCNRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.FacturatieAppTypes.Facturatie result = rp.FacturatieAppTeOntvangenCNRemove(cfrdc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
       RequestFormat = WebMessageFormat.Json,
       ResponseFormat = WebMessageFormat.Json,
       UriTemplate = "/FacturatieAppTeOntvangenFacturenAdd")]
        public EY.com.eBook.RuleEngine.AY2011.FacturatieAppTypes.Facturatie FacturatieAppTeOntvangenFacturenAdd(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaFacturatieAppFacturatieLeverancierDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.FacturatieAppTypes.Facturatie result = rp.FacturatieAppTeOntvangenFacturenAdd(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
       RequestFormat = WebMessageFormat.Json,
       ResponseFormat = WebMessageFormat.Json,
       UriTemplate = "/FacturatieAppTeOntvangenFacturenUpdate")]
        public EY.com.eBook.RuleEngine.AY2011.FacturatieAppTypes.Facturatie FacturatieAppTeOntvangenFacturenUpdate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaFacturatieAppFacturatieLeverancierDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.FacturatieAppTypes.Facturatie result = rp.FacturatieAppTeOntvangenFacturenUpdate(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
       RequestFormat = WebMessageFormat.Json,
       ResponseFormat = WebMessageFormat.Json,
       UriTemplate = "/FacturatieAppTeOntvangenFacturenRemove")]
        public EY.com.eBook.RuleEngine.AY2011.FacturatieAppTypes.Facturatie FacturatieAppTeOntvangenFacturenRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.FacturatieAppTypes.Facturatie result = rp.FacturatieAppTeOntvangenFacturenRemove(cfrdc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
       RequestFormat = WebMessageFormat.Json,
       ResponseFormat = WebMessageFormat.Json,
       UriTemplate = "/FacturatieAppOpTeMakenCNAdd")]
        public EY.com.eBook.RuleEngine.AY2011.FacturatieAppTypes.Facturatie FacturatieAppOpTeMakenCNAdd(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaFacturatieAppFacturatieKlantCNDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.FacturatieAppTypes.Facturatie result = rp.FacturatieAppOpTeMakenCNAdd(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
       RequestFormat = WebMessageFormat.Json,
       ResponseFormat = WebMessageFormat.Json,
       UriTemplate = "/FacturatieAppOpTeMakenCNUpdate")]
        public EY.com.eBook.RuleEngine.AY2011.FacturatieAppTypes.Facturatie FacturatieAppOpTeMakenCNUpdate(EY.com.eBook.RuleEngine.Data.AY2011.CriteriaFacturatieAppFacturatieKlantCNDataContract cedc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.FacturatieAppTypes.Facturatie result = rp.FacturatieAppOpTeMakenCNUpdate(cedc);
            rp.Close();
            return result;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
       RequestFormat = WebMessageFormat.Json,
       ResponseFormat = WebMessageFormat.Json,
       UriTemplate = "/FacturatieAppOpTeMakenCNRemove")]
        public EY.com.eBook.RuleEngine.AY2011.FacturatieAppTypes.Facturatie FacturatieAppOpTeMakenCNRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            RuleEngine2011Proxy rp = new RuleEngine2011Proxy();
            rp.Open();
            EY.com.eBook.RuleEngine.AY2011.FacturatieAppTypes.Facturatie result = rp.FacturatieAppOpTeMakenCNRemove(cfrdc);
            rp.Close();
            return result;
        }

       // #endregion
*/