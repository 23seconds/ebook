﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.RuleEngine.Core;
using EY.com.eBook.RuleEngine.Core.Interfaces;
using EY.com.eBook.RuleEngine.Data;
using EY.com.eBook.Core;

namespace EY.com.eBook.RuleEngine.AY2011
{
    
    public class BelastingenApp
    {
        #region standard
        public static WorksheetTypeSettings Settings = new WorksheetTypeSettings
                                                      {
                                                        Clients = true
                                                        ,
                                                        Suppliers = true
                                                        ,
                                                        AllAccounts = true
                                                        , 
                                                        PerformsBookings=true
                                                      };
                                                      
        public static Guid TypeId = new Guid("10eea163-5e43-4501-9f1c-35ae838be615");

        public List<string> ImportablePreviousTypes  = new List<string>() {  "EY.com.eBook.RuleEngine.AY2011.FacturatieApp"  };

        public Guid GetMyType() { return TypeId; }
        
        public void SetMinimal() 
        {
            if(Data==null) {
                Data = new EY.com.eBook.RuleEngine.AY2011.BelastingenAppTypes.WPBelastingen();
                Data.LastUpdated = DateTime.Now;
            }              
            
              if(Data.Adjustments==null) {
                  Data.Adjustments = new List<EY.com.eBook.RuleEngine.AY2011.BelastingenAppTypes.Adjustment>();;
                  Data.LastUpdated = DateTime.Now;
              }
            
              if(Data.Belastingen==null) {
                  Data.Belastingen = new List<EY.com.eBook.RuleEngine.AY2011.BelastingenAppTypes.Belasting>();;
                  Data.LastUpdated = DateTime.Now;
              }
            
              if(Data.GewestBelasting==null) {
                  Data.GewestBelasting = new List<EY.com.eBook.RuleEngine.AY2011.BelastingenAppTypes.KostItem>();;
                  Data.LastUpdated = DateTime.Now;
              }
            
              if(Data.Boetes==null) {
                  Data.Boetes = new List<EY.com.eBook.RuleEngine.AY2011.BelastingenAppTypes.KostItem>();;
                  Data.LastUpdated = DateTime.Now;
              }
            
              if(Data.Pensioenen==null) {
                  Data.Pensioenen = new List<EY.com.eBook.RuleEngine.AY2011.BelastingenAppTypes.KostItem>();;
                  Data.LastUpdated = DateTime.Now;
              }
            
              if(Data.Auto==null) {
                  Data.Auto = new List<EY.com.eBook.RuleEngine.AY2011.BelastingenAppTypes.KostItem>();;
                  Data.LastUpdated = DateTime.Now;
              }
            
              if(Data.Receptie==null) {
                  Data.Receptie = new List<EY.com.eBook.RuleEngine.AY2011.BelastingenAppTypes.KostItem>();;
                  Data.LastUpdated = DateTime.Now;
              }
            
              if(Data.Restaurant==null) {
                  Data.Restaurant = new List<EY.com.eBook.RuleEngine.AY2011.BelastingenAppTypes.KostItem>();;
                  Data.LastUpdated = DateTime.Now;
              }
            
              if(Data.Kledij==null) {
                  Data.Kledij = new List<EY.com.eBook.RuleEngine.AY2011.BelastingenAppTypes.KostItem>();;
                  Data.LastUpdated = DateTime.Now;
              }
            
              if(Data.OverdrevenIntresten==null) {
                  Data.OverdrevenIntresten = new List<EY.com.eBook.RuleEngine.AY2011.BelastingenAppTypes.KostItem>();;
                  Data.LastUpdated = DateTime.Now;
              }
            
              if(Data.IntrestenLeningen==null) {
                  Data.IntrestenLeningen = new List<EY.com.eBook.RuleEngine.AY2011.BelastingenAppTypes.KostItem>();;
                  Data.LastUpdated = DateTime.Now;
              }
            
              if(Data.AbnormaleGoedgunstigeVoordelen==null) {
                  Data.AbnormaleGoedgunstigeVoordelen = new List<EY.com.eBook.RuleEngine.AY2011.BelastingenAppTypes.KostItem>();;
                  Data.LastUpdated = DateTime.Now;
              }
            
              if(Data.SocialeVoordelen==null) {
                  Data.SocialeVoordelen = new List<EY.com.eBook.RuleEngine.AY2011.BelastingenAppTypes.KostItem>();;
                  Data.LastUpdated = DateTime.Now;
              }
            
              if(Data.MaaltijdSportCultuurEco==null) {
                  Data.MaaltijdSportCultuurEco = new List<EY.com.eBook.RuleEngine.AY2011.BelastingenAppTypes.KostItem>();;
                  Data.LastUpdated = DateTime.Now;
              }
            
              if(Data.Liberaliteiten==null) {
                  Data.Liberaliteiten = new List<EY.com.eBook.RuleEngine.AY2011.BelastingenAppTypes.KostItem>();;
                  Data.LastUpdated = DateTime.Now;
              }
            
              if(Data.WaardeverminderingAandelen==null) {
                  Data.WaardeverminderingAandelen = new List<EY.com.eBook.RuleEngine.AY2011.BelastingenAppTypes.KostItem>();;
                  Data.LastUpdated = DateTime.Now;
              }
            
              if(Data.TerugnemingVroegereVrijstellingen==null) {
                  Data.TerugnemingVroegereVrijstellingen = new List<EY.com.eBook.RuleEngine.AY2011.BelastingenAppTypes.KostItem>();;
                  Data.LastUpdated = DateTime.Now;
              }
            
              if(Data.Werknemersparticipatie==null) {
                  Data.Werknemersparticipatie = new List<EY.com.eBook.RuleEngine.AY2011.BelastingenAppTypes.KostItem>();;
                  Data.LastUpdated = DateTime.Now;
              }
            
              if(Data.VergoedingCoupon==null) {
                  Data.VergoedingCoupon = new List<EY.com.eBook.RuleEngine.AY2011.BelastingenAppTypes.KostItem>();;
                  Data.LastUpdated = DateTime.Now;
              }
            
              if(Data.KostenTaxShelter==null) {
                  Data.KostenTaxShelter = new List<EY.com.eBook.RuleEngine.AY2011.BelastingenAppTypes.KostItem>();;
                  Data.LastUpdated = DateTime.Now;
              }
            
              if(Data.GewestPremies==null) {
                  Data.GewestPremies = new List<EY.com.eBook.RuleEngine.AY2011.BelastingenAppTypes.KostItem>();;
                  Data.LastUpdated = DateTime.Now;
              }
            
              if(Data.BepaaldeStaten==null) {
                  Data.BepaaldeStaten = new List<EY.com.eBook.RuleEngine.AY2011.BelastingenAppTypes.KostItem>();;
                  Data.LastUpdated = DateTime.Now;
              }
            
              if(Data.Andere==null) {
                  Data.Andere = new List<EY.com.eBook.RuleEngine.AY2011.BelastingenAppTypes.KostItem>();;
                  Data.LastUpdated = DateTime.Now;
              }
            
        }
        
        public string ToXml() 
        {
            return CoreHelper.SerializeToString(Data);
        }

        public void LoadXml(string xml)
        {
            if (!string.IsNullOrEmpty(xml)) {
                Data = CoreHelper.DeserializeFromString<EY.com.eBook.RuleEngine.AY2011.BelastingenAppTypes.WPBelastingen>(xml);
            }
            SetMinimal();
            
        }
        
        public EY.com.eBook.RuleEngine.AY2011.BelastingenAppTypes.WPBelastingen Data { get; set; }
        
        public void Calculate(ref WorksheetSetData worksheetset)
        {
           // Data.LastCalculated = DateTime.Now;
        }
        #endregion
        
        #region AddUpdateDelete
        
          public void AddToAdjustments(EY.com.eBook.RuleEngine.AY2011.BelastingenAppTypes.Adjustment entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void DeleteAdjustments(Guid rowId,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void UpdateAdjustments(EY.com.eBook.RuleEngine.AY2011.BelastingenAppTypes.Adjustment entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void AddToBelastingen(EY.com.eBook.RuleEngine.AY2011.BelastingenAppTypes.Belasting entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void UpdateBelastingen(EY.com.eBook.RuleEngine.AY2011.BelastingenAppTypes.Belasting entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
        #endregion
        
        #region VALIDATION
        
        public EY.com.eBook.RuleEngine.AY2011.BelastingenAppTypes.Adjustment Validate(EY.com.eBook.RuleEngine.AY2011.BelastingenAppTypes.Adjustment entity, string location,ref WorksheetSetData worksheetset)
          {
          entity.IsValid = true;
          worksheetset.ClearMessages(TypeId, entity.Id);
          

          return entity;
          }
        
        public EY.com.eBook.RuleEngine.AY2011.BelastingenAppTypes.Belasting Validate(EY.com.eBook.RuleEngine.AY2011.BelastingenAppTypes.Belasting entity, string location,ref WorksheetSetData worksheetset)
          {
          entity.IsValid = true;
          worksheetset.ClearMessages(TypeId, entity.Id);
          

          return entity;
          }
        
        public EY.com.eBook.RuleEngine.AY2011.BelastingenAppTypes.KostItem Validate(EY.com.eBook.RuleEngine.AY2011.BelastingenAppTypes.KostItem entity, string location,ref WorksheetSetData worksheetset)
          {
          entity.IsValid = true;
          worksheetset.ClearMessages(TypeId, entity.Id);
          

          return entity;
          }
        
        #endregion
        // CALCULATION
        
        // BOOKINGS
        
        // IMPORT PREVIOUS
        
    }
}
  