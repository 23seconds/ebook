﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.RuleEngine.Core;
using EY.com.eBook.RuleEngine.Core.Interfaces;
using EY.com.eBook.RuleEngine.Data;
using EY.com.eBook.Core;

namespace EY.com.eBook.RuleEngine.AY2011
{
    
    public class RVIntrestenApp
    {
        #region standard
        public static WorksheetTypeSettings Settings = new WorksheetTypeSettings
                                                      {
                                                        Clients = true
                                                        ,
                                                        Suppliers = true
                                                        ,
                                                        AllAccounts = true
                                                        , 
                                                        PerformsBookings=true
                                                      };
                                                      
        public static Guid TypeId = new Guid("3b260f00-e6e8-4c6b-83ea-69457799a068");

        public List<string> ImportablePreviousTypes  = new List<string>() {  "EY.com.eBook.RuleEngine.AY2011.FacturatieApp"  };

        public Guid GetMyType() { return TypeId; }
        
        public void SetMinimal() 
        {
            if(Data==null) {
                Data = new EY.com.eBook.RuleEngine.AY2011.RVIntrestenAppTypes.WPRVIntresten();
                Data.LastUpdated = DateTime.Now;
            }              
            
              if(Data.Intresten==null) {
                  Data.Intresten = new List<EY.com.eBook.RuleEngine.AY2011.RVIntrestenAppTypes.IntrestItem>();;
                  Data.LastUpdated = DateTime.Now;
              }
            
              if(Data.Adjustments==null) {
                  Data.Adjustments = new List<EY.com.eBook.RuleEngine.AY2011.RVIntrestenAppTypes.Adjustment>();;
                  Data.LastUpdated = DateTime.Now;
              }
            
              if(Data.Verschillen==null) {
                  Data.Verschillen = new List<EY.com.eBook.RuleEngine.AY2011.RVIntrestenAppTypes.VerchilItem>();;
                  Data.LastUpdated = DateTime.Now;
              }
            
        }
        
        public string ToXml() 
        {
            return CoreHelper.SerializeToString(Data);
        }

        public void LoadXml(string xml)
        {
            if (!string.IsNullOrEmpty(xml)) {
                Data = CoreHelper.DeserializeFromString<EY.com.eBook.RuleEngine.AY2011.RVIntrestenAppTypes.WPRVIntresten>(xml);
            }
            SetMinimal();
            
        }
        
        public EY.com.eBook.RuleEngine.AY2011.RVIntrestenAppTypes.WPRVIntresten Data { get; set; }
        
        public void Calculate(ref WorksheetSetData worksheetset)
        {
           // Data.LastCalculated = DateTime.Now;
        }
        #endregion
        
        #region AddUpdateDelete
        
          public void AddToIntresten(EY.com.eBook.RuleEngine.AY2011.RVIntrestenAppTypes.IntrestItem entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void UpdateIntresten(EY.com.eBook.RuleEngine.AY2011.RVIntrestenAppTypes.IntrestItem entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void AddToAdjustments(EY.com.eBook.RuleEngine.AY2011.RVIntrestenAppTypes.Adjustment entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void DeleteAdjustments(Guid rowId,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void UpdateAdjustments(EY.com.eBook.RuleEngine.AY2011.RVIntrestenAppTypes.Adjustment entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void AddToVerschillen(EY.com.eBook.RuleEngine.AY2011.RVIntrestenAppTypes.VerchilItem entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void DeleteVerschillen(Guid rowId,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void UpdateVerschillen(EY.com.eBook.RuleEngine.AY2011.RVIntrestenAppTypes.VerchilItem entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
        #endregion
        
        #region VALIDATION
        
        public EY.com.eBook.RuleEngine.AY2011.RVIntrestenAppTypes.IntrestItem Validate(EY.com.eBook.RuleEngine.AY2011.RVIntrestenAppTypes.IntrestItem entity, string location,ref WorksheetSetData worksheetset)
          {
          entity.IsValid = true;
          worksheetset.ClearMessages(TypeId, entity.Id);
          

          return entity;
          }
        
        public EY.com.eBook.RuleEngine.AY2011.RVIntrestenAppTypes.Adjustment Validate(EY.com.eBook.RuleEngine.AY2011.RVIntrestenAppTypes.Adjustment entity, string location,ref WorksheetSetData worksheetset)
          {
          entity.IsValid = true;
          worksheetset.ClearMessages(TypeId, entity.Id);
          

          return entity;
          }
        
        public EY.com.eBook.RuleEngine.AY2011.RVIntrestenAppTypes.VerchilItem Validate(EY.com.eBook.RuleEngine.AY2011.RVIntrestenAppTypes.VerchilItem entity, string location,ref WorksheetSetData worksheetset)
          {
          entity.IsValid = true;
          worksheetset.ClearMessages(TypeId, entity.Id);
          

          return entity;
          }
        
        #endregion
        // CALCULATION
        
        // BOOKINGS
        
        // IMPORT PREVIOUS
        
    }
}
  