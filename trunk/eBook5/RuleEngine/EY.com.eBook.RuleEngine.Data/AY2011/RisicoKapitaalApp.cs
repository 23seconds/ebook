﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.RuleEngine.Core;
using EY.com.eBook.RuleEngine.Core.Interfaces;
using EY.com.eBook.RuleEngine.Data;
using EY.com.eBook.Core;

namespace EY.com.eBook.RuleEngine.AY2011
{
    
    public class RisicoKapitaalApp
    {
        #region standard
        public static WorksheetTypeSettings Settings = new WorksheetTypeSettings
                                                      {
                                                        Clients = true
                                                        ,
                                                        Suppliers = true
                                                        ,
                                                        AllAccounts = true
                                                        , 
                                                        PerformsBookings=true
                                                      };
                                                      
        public static Guid TypeId = new Guid("a733b82a-e152-4916-9d77-2ed1b39e2d36");

        public List<string> ImportablePreviousTypes  = new List<string>() {  "EY.com.eBook.RuleEngine.AY2011.FacturatieApp"  };

        public Guid GetMyType() { return TypeId; }
        
        public void SetMinimal() 
        {
            if(Data==null) {
                Data = new EY.com.eBook.RuleEngine.AY2011.RisicoKapitaalAppTypes.WPRisicokapitaal();
                Data.LastUpdated = DateTime.Now;
            }              
            
              if(Data.Fiche==null) {
                  Data.Fiche = new EY.com.eBook.RuleEngine.AY2011.RisicoKapitaalAppTypes.FicheItem {
                            Id=Guid.NewGuid()
                            , LastUpdated=null
                            , IsValid=true
                            , PreviousImported=false
                            
                         };
                  Data.LastUpdated = DateTime.Now;
              }
            
              if(Data.EigenVermogens==null) {
                  Data.EigenVermogens = new List<EY.com.eBook.RuleEngine.AY2011.RisicoKapitaalAppTypes.EigenVermogen>();;
                  Data.LastUpdated = DateTime.Now;
              }
            
              if(Data.Bestanddelen==null) {
                  Data.Bestanddelen = new List<EY.com.eBook.RuleEngine.AY2011.RisicoKapitaalAppTypes.Bestanddeel>();;
                  Data.LastUpdated = DateTime.Now;
              }
            
              if(Data.Wijzigingen==null) {
                  Data.Wijzigingen = new List<EY.com.eBook.RuleEngine.AY2011.RisicoKapitaalAppTypes.WijzigingItem>();;
                  Data.LastUpdated = DateTime.Now;
              }
            
              if(Data.Tarief==null) {
                  Data.Tarief = new EY.com.eBook.RuleEngine.AY2011.RisicoKapitaalAppTypes.TariefItem {
                            Id=Guid.NewGuid()
                            , LastUpdated=null
                            , IsValid=true
                            , PreviousImported=false
                            
                         };
                  Data.LastUpdated = DateTime.Now;
              }
            
              if(Data.Historiek==null) {
                  Data.Historiek = new List<EY.com.eBook.RuleEngine.AY2011.RisicoKapitaalAppTypes.HistoryItem>();;
                  Data.LastUpdated = DateTime.Now;
              }
            
              if(Data.AanpassingenHistoriek==null) {
                  Data.AanpassingenHistoriek = new List<EY.com.eBook.RuleEngine.AY2011.RisicoKapitaalAppTypes.HistoriekAanpassingItem>();;
                  Data.LastUpdated = DateTime.Now;
              }
            
        }
        
        public string ToXml() 
        {
            return CoreHelper.SerializeToString(Data);
        }

        public void LoadXml(string xml)
        {
            if (!string.IsNullOrEmpty(xml)) {
                Data = CoreHelper.DeserializeFromString<EY.com.eBook.RuleEngine.AY2011.RisicoKapitaalAppTypes.WPRisicokapitaal>(xml);
            }
            SetMinimal();
            
        }
        
        public EY.com.eBook.RuleEngine.AY2011.RisicoKapitaalAppTypes.WPRisicokapitaal Data { get; set; }
        
        public void Calculate(ref WorksheetSetData worksheetset)
        {
           // Data.LastCalculated = DateTime.Now;
        }
        #endregion
        
        #region AddUpdateDelete
        
          public void UpdateFiche(EY.com.eBook.RuleEngine.AY2011.RisicoKapitaalAppTypes.FicheItem entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void AddToEigenVermogens(EY.com.eBook.RuleEngine.AY2011.RisicoKapitaalAppTypes.EigenVermogen entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void DeleteEigenVermogens(Guid rowId,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void UpdateEigenVermogens(EY.com.eBook.RuleEngine.AY2011.RisicoKapitaalAppTypes.EigenVermogen entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void AddToBestanddelen(EY.com.eBook.RuleEngine.AY2011.RisicoKapitaalAppTypes.Bestanddeel entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void DeleteBestanddelen(Guid rowId,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void UpdateBestanddelen(EY.com.eBook.RuleEngine.AY2011.RisicoKapitaalAppTypes.Bestanddeel entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void AddToWijzigingen(EY.com.eBook.RuleEngine.AY2011.RisicoKapitaalAppTypes.WijzigingItem entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void DeleteWijzigingen(Guid rowId,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void UpdateWijzigingen(EY.com.eBook.RuleEngine.AY2011.RisicoKapitaalAppTypes.WijzigingItem entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void UpdateTarief(EY.com.eBook.RuleEngine.AY2011.RisicoKapitaalAppTypes.TariefItem entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void AddToHistoriek(EY.com.eBook.RuleEngine.AY2011.RisicoKapitaalAppTypes.HistoryItem entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void DeleteHistoriek(Guid rowId,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void UpdateHistoriek(EY.com.eBook.RuleEngine.AY2011.RisicoKapitaalAppTypes.HistoryItem entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void AddToAanpassingenHistoriek(EY.com.eBook.RuleEngine.AY2011.RisicoKapitaalAppTypes.HistoriekAanpassingItem entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void DeleteAanpassingenHistoriek(Guid rowId,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void UpdateAanpassingenHistoriek(EY.com.eBook.RuleEngine.AY2011.RisicoKapitaalAppTypes.HistoriekAanpassingItem entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
        #endregion
        
        #region VALIDATION
        
        public EY.com.eBook.RuleEngine.AY2011.RisicoKapitaalAppTypes.FicheItem Validate(EY.com.eBook.RuleEngine.AY2011.RisicoKapitaalAppTypes.FicheItem entity, string location,ref WorksheetSetData worksheetset)
          {
          entity.IsValid = true;
          worksheetset.ClearMessages(TypeId, entity.Id);
          

          return entity;
          }
        
        public EY.com.eBook.RuleEngine.AY2011.RisicoKapitaalAppTypes.EigenVermogen Validate(EY.com.eBook.RuleEngine.AY2011.RisicoKapitaalAppTypes.EigenVermogen entity, string location,ref WorksheetSetData worksheetset)
          {
          entity.IsValid = true;
          worksheetset.ClearMessages(TypeId, entity.Id);
          

          return entity;
          }
        
        public EY.com.eBook.RuleEngine.AY2011.RisicoKapitaalAppTypes.Bestanddeel Validate(EY.com.eBook.RuleEngine.AY2011.RisicoKapitaalAppTypes.Bestanddeel entity, string location,ref WorksheetSetData worksheetset)
          {
          entity.IsValid = true;
          worksheetset.ClearMessages(TypeId, entity.Id);
          

          return entity;
          }
        
        public EY.com.eBook.RuleEngine.AY2011.RisicoKapitaalAppTypes.WijzigingItem Validate(EY.com.eBook.RuleEngine.AY2011.RisicoKapitaalAppTypes.WijzigingItem entity, string location,ref WorksheetSetData worksheetset)
          {
          entity.IsValid = true;
          worksheetset.ClearMessages(TypeId, entity.Id);
          

          return entity;
          }
        
        public EY.com.eBook.RuleEngine.AY2011.RisicoKapitaalAppTypes.TariefItem Validate(EY.com.eBook.RuleEngine.AY2011.RisicoKapitaalAppTypes.TariefItem entity, string location,ref WorksheetSetData worksheetset)
          {
          entity.IsValid = true;
          worksheetset.ClearMessages(TypeId, entity.Id);
          

          return entity;
          }
        
        public EY.com.eBook.RuleEngine.AY2011.RisicoKapitaalAppTypes.HistoryItem Validate(EY.com.eBook.RuleEngine.AY2011.RisicoKapitaalAppTypes.HistoryItem entity, string location,ref WorksheetSetData worksheetset)
          {
          entity.IsValid = true;
          worksheetset.ClearMessages(TypeId, entity.Id);
          

          return entity;
          }
        
        public EY.com.eBook.RuleEngine.AY2011.RisicoKapitaalAppTypes.HistoriekAanpassingItem Validate(EY.com.eBook.RuleEngine.AY2011.RisicoKapitaalAppTypes.HistoriekAanpassingItem entity, string location,ref WorksheetSetData worksheetset)
          {
          entity.IsValid = true;
          worksheetset.ClearMessages(TypeId, entity.Id);
          

          return entity;
          }
        
        #endregion
        // CALCULATION
        
        // BOOKINGS
        
        // IMPORT PREVIOUS
        
    }
}
  