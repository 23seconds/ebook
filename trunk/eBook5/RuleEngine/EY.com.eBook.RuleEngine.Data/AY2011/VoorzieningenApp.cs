﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.RuleEngine.Core;
using EY.com.eBook.RuleEngine.Core.Interfaces;
using EY.com.eBook.RuleEngine.Data;
using EY.com.eBook.Core;

namespace EY.com.eBook.RuleEngine.AY2011
{
    
    public class VoorzieningenApp
    {
        #region standard
        public static WorksheetTypeSettings Settings = new WorksheetTypeSettings
                                                      {
                                                        Clients = true
                                                        ,
                                                        Suppliers = true
                                                        ,
                                                        AllAccounts = true
                                                        , 
                                                        PerformsBookings=true
                                                      };
                                                      
        public static Guid TypeId = new Guid("eda9a421-b457-4ca3-a9ac-f3c6bd1f9fff");

        public List<string> ImportablePreviousTypes  = new List<string>() {  "EY.com.eBook.RuleEngine.AY2011.FacturatieApp"  };

        public Guid GetMyType() { return TypeId; }
        
        public void SetMinimal() 
        {
            if(Data==null) {
                Data = new EY.com.eBook.RuleEngine.AY2011.VoorzieningenAppTypes.Voorzieningen();
                Data.LastUpdated = DateTime.Now;
            }              
            
              if(Data.ToestandEindVorigBoekjaar==null) {
                  Data.ToestandEindVorigBoekjaar = new List<EY.com.eBook.RuleEngine.AY2011.VoorzieningenAppTypes.VoorzieningenT>();;
                  Data.LastUpdated = DateTime.Now;
              }
            
              if(Data.WijzigingBestaandeTijdensBoekjaar==null) {
                  Data.WijzigingBestaandeTijdensBoekjaar = new List<EY.com.eBook.RuleEngine.AY2011.VoorzieningenAppTypes.VoorzieningenW>();;
                  Data.LastUpdated = DateTime.Now;
              }
            
              if(Data.NieuweTijdensBoekjaar==null) {
                  Data.NieuweTijdensBoekjaar = new List<EY.com.eBook.RuleEngine.AY2011.VoorzieningenAppTypes.VoorzieningenN>();;
                  Data.LastUpdated = DateTime.Now;
              }
            
              if(Data.ToestandEindHuidigBoekjaar==null) {
                  Data.ToestandEindHuidigBoekjaar = new List<EY.com.eBook.RuleEngine.AY2011.VoorzieningenAppTypes.VoorzieningenH>();;
                  Data.LastUpdated = DateTime.Now;
              }
            
        }
        
        public string ToXml() 
        {
            return CoreHelper.SerializeToString(Data);
        }

        public void LoadXml(string xml)
        {
            if (!string.IsNullOrEmpty(xml)) {
                Data = CoreHelper.DeserializeFromString<EY.com.eBook.RuleEngine.AY2011.VoorzieningenAppTypes.Voorzieningen>(xml);
            }
            SetMinimal();
            
        }
        
        public EY.com.eBook.RuleEngine.AY2011.VoorzieningenAppTypes.Voorzieningen Data { get; set; }
        
        public void Calculate(ref WorksheetSetData worksheetset)
        {
           // Data.LastCalculated = DateTime.Now;
        }
        #endregion
        
        #region AddUpdateDelete
        
          public void AddToToestandEindVorigBoekjaar(EY.com.eBook.RuleEngine.AY2011.VoorzieningenAppTypes.VoorzieningenT entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void DeleteToestandEindVorigBoekjaar(Guid rowId,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void UpdateToestandEindVorigBoekjaar(EY.com.eBook.RuleEngine.AY2011.VoorzieningenAppTypes.VoorzieningenT entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void AddToWijzigingBestaandeTijdensBoekjaar(EY.com.eBook.RuleEngine.AY2011.VoorzieningenAppTypes.VoorzieningenW entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void DeleteWijzigingBestaandeTijdensBoekjaar(Guid rowId,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void UpdateWijzigingBestaandeTijdensBoekjaar(EY.com.eBook.RuleEngine.AY2011.VoorzieningenAppTypes.VoorzieningenW entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void AddToNieuweTijdensBoekjaar(EY.com.eBook.RuleEngine.AY2011.VoorzieningenAppTypes.VoorzieningenN entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void DeleteNieuweTijdensBoekjaar(Guid rowId,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void UpdateNieuweTijdensBoekjaar(EY.com.eBook.RuleEngine.AY2011.VoorzieningenAppTypes.VoorzieningenN entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void AddToToestandEindHuidigBoekjaar(EY.com.eBook.RuleEngine.AY2011.VoorzieningenAppTypes.VoorzieningenH entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void DeleteToestandEindHuidigBoekjaar(Guid rowId,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void UpdateToestandEindHuidigBoekjaar(EY.com.eBook.RuleEngine.AY2011.VoorzieningenAppTypes.VoorzieningenH entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
        #endregion
        
        #region VALIDATION
        
        public EY.com.eBook.RuleEngine.AY2011.VoorzieningenAppTypes.VoorzieningenT Validate(EY.com.eBook.RuleEngine.AY2011.VoorzieningenAppTypes.VoorzieningenT entity, string location,ref WorksheetSetData worksheetset)
          {
          entity.IsValid = true;
          worksheetset.ClearMessages(TypeId, entity.Id);
          

          return entity;
          }
        
        public EY.com.eBook.RuleEngine.AY2011.VoorzieningenAppTypes.VoorzieningenW Validate(EY.com.eBook.RuleEngine.AY2011.VoorzieningenAppTypes.VoorzieningenW entity, string location,ref WorksheetSetData worksheetset)
          {
          entity.IsValid = true;
          worksheetset.ClearMessages(TypeId, entity.Id);
          

          return entity;
          }
        
        public EY.com.eBook.RuleEngine.AY2011.VoorzieningenAppTypes.VoorzieningenN Validate(EY.com.eBook.RuleEngine.AY2011.VoorzieningenAppTypes.VoorzieningenN entity, string location,ref WorksheetSetData worksheetset)
          {
          entity.IsValid = true;
          worksheetset.ClearMessages(TypeId, entity.Id);
          

          return entity;
          }
        
        public EY.com.eBook.RuleEngine.AY2011.VoorzieningenAppTypes.VoorzieningenH Validate(EY.com.eBook.RuleEngine.AY2011.VoorzieningenAppTypes.VoorzieningenH entity, string location,ref WorksheetSetData worksheetset)
          {
          entity.IsValid = true;
          worksheetset.ClearMessages(TypeId, entity.Id);
          

          return entity;
          }
        
        #endregion
        // CALCULATION
        
        // BOOKINGS
        
        // IMPORT PREVIOUS
        
    }
}
  