﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.RuleEngine.Core;
using EY.com.eBook.RuleEngine.Core.Interfaces;
using EY.com.eBook.RuleEngine.Data;
using EY.com.eBook.Core;

namespace EY.com.eBook.RuleEngine.AY2011
{
    
    public class BelastingVrijeReservesApp
    {
        #region standard
        public static WorksheetTypeSettings Settings = new WorksheetTypeSettings
                                                      {
                                                        Clients = true
                                                        ,
                                                        Suppliers = true
                                                        ,
                                                        AllAccounts = true
                                                        , 
                                                        PerformsBookings=true
                                                      };
                                                      
        public static Guid TypeId = new Guid("a1b361d5-4170-4241-9dc4-e204bd8f677c");

        public List<string> ImportablePreviousTypes  = new List<string>() {  "EY.com.eBook.RuleEngine.AY2011.FacturatieApp"  };

        public Guid GetMyType() { return TypeId; }
        
        public void SetMinimal() 
        {
            if(Data==null) {
                Data = new EY.com.eBook.RuleEngine.AY2011.BelastingVrijeReservesAppTypes.BelastingVrijeReserves();
                Data.LastUpdated = DateTime.Now;
            }              
            
              if(Data.VrijgesteldeReserves==null) {
                  Data.VrijgesteldeReserves = new EY.com.eBook.RuleEngine.AY2011.BelastingVrijeReservesAppTypes.Detail {
                            Id=Guid.NewGuid()
                            , LastUpdated=null
                            , IsValid=true
                            , PreviousImported=false
                            
                         };
                  Data.LastUpdated = DateTime.Now;
              }
            
        }
        
        public string ToXml() 
        {
            return CoreHelper.SerializeToString(Data);
        }

        public void LoadXml(string xml)
        {
            if (!string.IsNullOrEmpty(xml)) {
                Data = CoreHelper.DeserializeFromString<EY.com.eBook.RuleEngine.AY2011.BelastingVrijeReservesAppTypes.BelastingVrijeReserves>(xml);
            }
            SetMinimal();
            
        }
        
        public EY.com.eBook.RuleEngine.AY2011.BelastingVrijeReservesAppTypes.BelastingVrijeReserves Data { get; set; }
        
        public void Calculate(ref WorksheetSetData worksheetset)
        {
           // Data.LastCalculated = DateTime.Now;
        }
        #endregion
        
        #region AddUpdateDelete
        
          public void UpdateVrijgesteldeReserves(EY.com.eBook.RuleEngine.AY2011.BelastingVrijeReservesAppTypes.Detail entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
        #endregion
        
        #region VALIDATION
        
        public EY.com.eBook.RuleEngine.AY2011.BelastingVrijeReservesAppTypes.Detail Validate(EY.com.eBook.RuleEngine.AY2011.BelastingVrijeReservesAppTypes.Detail entity, string location,ref WorksheetSetData worksheetset)
          {
          entity.IsValid = true;
          worksheetset.ClearMessages(TypeId, entity.Id);
          

          return entity;
          }
        
        #endregion
        // CALCULATION
        
        // BOOKINGS
        
        // IMPORT PREVIOUS
        
    }
}
  