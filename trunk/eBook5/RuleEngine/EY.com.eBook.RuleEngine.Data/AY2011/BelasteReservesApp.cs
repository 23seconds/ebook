﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.RuleEngine.Core;
using EY.com.eBook.RuleEngine.Core.Interfaces;
using EY.com.eBook.RuleEngine.Data;
using EY.com.eBook.Core;

namespace EY.com.eBook.RuleEngine.AY2011
{
    
    public class BelasteReservesApp
    {
        #region standard
        public static WorksheetTypeSettings Settings = new WorksheetTypeSettings
                                                      {
                                                        Clients = true
                                                        ,
                                                        Suppliers = true
                                                        ,
                                                        AllAccounts = true
                                                        , 
                                                        PerformsBookings=true
                                                      };
                                                      
        public static Guid TypeId = new Guid("7df44f01-2f13-4cd5-ada1-7ad8003a85c1");

        public List<string> ImportablePreviousTypes  = new List<string>() {  "EY.com.eBook.RuleEngine.AY2011.FacturatieApp"  };

        public Guid GetMyType() { return TypeId; }
        
        public void SetMinimal() 
        {
            if(Data==null) {
                Data = new EY.com.eBook.RuleEngine.AY2011.BelasteReservesAppTypes.BelasteReserves();
                Data.LastUpdated = DateTime.Now;
            }              
            
              if(Data.AllBelasteReserves==null) {
                  Data.AllBelasteReserves = new List<EY.com.eBook.RuleEngine.AY2011.BelasteReservesAppTypes.BelasteReservesItem>();;
                  Data.LastUpdated = DateTime.Now;
              }
            
              if(Data.AanpassingBTReserves==null) {
                  Data.AanpassingBTReserves = new List<EY.com.eBook.RuleEngine.AY2011.BelasteReservesAppTypes.AanpassingBTReservesItem>();;
                  Data.LastUpdated = DateTime.Now;
              }
            
        }
        
        public string ToXml() 
        {
            return CoreHelper.SerializeToString(Data);
        }

        public void LoadXml(string xml)
        {
            if (!string.IsNullOrEmpty(xml)) {
                Data = CoreHelper.DeserializeFromString<EY.com.eBook.RuleEngine.AY2011.BelasteReservesAppTypes.BelasteReserves>(xml);
            }
            SetMinimal();
            
        }
        
        public EY.com.eBook.RuleEngine.AY2011.BelasteReservesAppTypes.BelasteReserves Data { get; set; }
        
        public void Calculate(ref WorksheetSetData worksheetset)
        {
           // Data.LastCalculated = DateTime.Now;
        }
        #endregion
        
        #region AddUpdateDelete
        
          public void AddToAllBelasteReserves(EY.com.eBook.RuleEngine.AY2011.BelasteReservesAppTypes.BelasteReservesItem entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void DeleteAllBelasteReserves(Guid rowId,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void UpdateAllBelasteReserves(EY.com.eBook.RuleEngine.AY2011.BelasteReservesAppTypes.BelasteReservesItem entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void AddToAanpassingBTReserves(EY.com.eBook.RuleEngine.AY2011.BelasteReservesAppTypes.AanpassingBTReservesItem entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void DeleteAanpassingBTReserves(Guid rowId,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void UpdateAanpassingBTReserves(EY.com.eBook.RuleEngine.AY2011.BelasteReservesAppTypes.AanpassingBTReservesItem entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
        #endregion
        
        #region VALIDATION
        
        public EY.com.eBook.RuleEngine.AY2011.BelasteReservesAppTypes.BelasteReservesItem Validate(EY.com.eBook.RuleEngine.AY2011.BelasteReservesAppTypes.BelasteReservesItem entity, string location,ref WorksheetSetData worksheetset)
          {
          entity.IsValid = true;
          worksheetset.ClearMessages(TypeId, entity.Id);
          

          return entity;
          }
        
        public EY.com.eBook.RuleEngine.AY2011.BelasteReservesAppTypes.AanpassingBTReservesItem Validate(EY.com.eBook.RuleEngine.AY2011.BelasteReservesAppTypes.AanpassingBTReservesItem entity, string location,ref WorksheetSetData worksheetset)
          {
          entity.IsValid = true;
          worksheetset.ClearMessages(TypeId, entity.Id);
          

          return entity;
          }
        
        #endregion
        // CALCULATION
        
        // BOOKINGS
        
        // IMPORT PREVIOUS
        
    }
}
  