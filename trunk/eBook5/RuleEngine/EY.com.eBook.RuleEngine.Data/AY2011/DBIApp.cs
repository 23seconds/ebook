﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.RuleEngine.Core;
using EY.com.eBook.RuleEngine.Core.Interfaces;
using EY.com.eBook.RuleEngine.Data;
using EY.com.eBook.Core;

namespace EY.com.eBook.RuleEngine.AY2011
{
    
    public class DBIApp
    {
        #region standard
        public static WorksheetTypeSettings Settings = new WorksheetTypeSettings
                                                      {
                                                        Clients = true
                                                        ,
                                                        Suppliers = true
                                                        ,
                                                        AllAccounts = true
                                                        , 
                                                        PerformsBookings=true
                                                      };
                                                      
        public static Guid TypeId = new Guid("77d8dbbc-feec-478e-9e11-83838e346caf");

        public List<string> ImportablePreviousTypes  = new List<string>() {  "EY.com.eBook.RuleEngine.AY2011.FacturatieApp"  };

        public Guid GetMyType() { return TypeId; }
        
        public void SetMinimal() 
        {
            if(Data==null) {
                Data = new EY.com.eBook.RuleEngine.AY2011.DBIAppTypes.DBIWorksheet();
                Data.LastUpdated = DateTime.Now;
            }              
            
              if(Data.Participations==null) {
                  Data.Participations = new List<EY.com.eBook.RuleEngine.AY2011.DBIAppTypes.ParticipatieItem>();;
                  Data.LastUpdated = DateTime.Now;
              }
            
              if(Data.Dividends==null) {
                  Data.Dividends = new List<EY.com.eBook.RuleEngine.AY2011.DBIAppTypes.DividendItem>();;
                  Data.LastUpdated = DateTime.Now;
              }
            
              if(Data.DBI==null) {
                  Data.DBI = new EY.com.eBook.RuleEngine.AY2011.DBIAppTypes.DbiEntity {
                            Id=Guid.NewGuid()
                            , LastUpdated=null
                            , IsValid=true
                            , PreviousImported=false
                            
                         };
                  Data.LastUpdated = DateTime.Now;
              }
            
              if(Data.Historiek==null) {
                  Data.Historiek = new List<EY.com.eBook.RuleEngine.AY2011.DBIAppTypes.HistoryItem>();;
                  Data.LastUpdated = DateTime.Now;
              }
            
        }
        
        public string ToXml() 
        {
            return CoreHelper.SerializeToString(Data);
        }

        public void LoadXml(string xml)
        {
            if (!string.IsNullOrEmpty(xml)) {
                Data = CoreHelper.DeserializeFromString<EY.com.eBook.RuleEngine.AY2011.DBIAppTypes.DBIWorksheet>(xml);
            }
            SetMinimal();
            
        }
        
        public EY.com.eBook.RuleEngine.AY2011.DBIAppTypes.DBIWorksheet Data { get; set; }
        
        public void Calculate(ref WorksheetSetData worksheetset)
        {
           // Data.LastCalculated = DateTime.Now;
        }
        #endregion
        
        #region AddUpdateDelete
        
          public void AddToParticipations(EY.com.eBook.RuleEngine.AY2011.DBIAppTypes.ParticipatieItem entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void DeleteParticipations(Guid rowId,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void UpdateParticipations(EY.com.eBook.RuleEngine.AY2011.DBIAppTypes.ParticipatieItem entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void AddToDividends(EY.com.eBook.RuleEngine.AY2011.DBIAppTypes.DividendItem entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void DeleteDividends(Guid rowId,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void UpdateDividends(EY.com.eBook.RuleEngine.AY2011.DBIAppTypes.DividendItem entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void UpdateDBI(EY.com.eBook.RuleEngine.AY2011.DBIAppTypes.DbiEntity entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void AddToHistoriek(EY.com.eBook.RuleEngine.AY2011.DBIAppTypes.HistoryItem entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void DeleteHistoriek(Guid rowId,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void UpdateHistoriek(EY.com.eBook.RuleEngine.AY2011.DBIAppTypes.HistoryItem entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
        #endregion
        
        #region VALIDATION
        
        public EY.com.eBook.RuleEngine.AY2011.DBIAppTypes.ParticipatieItem Validate(EY.com.eBook.RuleEngine.AY2011.DBIAppTypes.ParticipatieItem entity, string location,ref WorksheetSetData worksheetset)
          {
          entity.IsValid = true;
          worksheetset.ClearMessages(TypeId, entity.Id);
          

          return entity;
          }
        
        public EY.com.eBook.RuleEngine.AY2011.DBIAppTypes.DividendItem Validate(EY.com.eBook.RuleEngine.AY2011.DBIAppTypes.DividendItem entity, string location,ref WorksheetSetData worksheetset)
          {
          entity.IsValid = true;
          worksheetset.ClearMessages(TypeId, entity.Id);
          

          return entity;
          }
        
        public EY.com.eBook.RuleEngine.AY2011.DBIAppTypes.DbiEntity Validate(EY.com.eBook.RuleEngine.AY2011.DBIAppTypes.DbiEntity entity, string location,ref WorksheetSetData worksheetset)
          {
          entity.IsValid = true;
          worksheetset.ClearMessages(TypeId, entity.Id);
          

          return entity;
          }
        
        public EY.com.eBook.RuleEngine.AY2011.DBIAppTypes.HistoryItem Validate(EY.com.eBook.RuleEngine.AY2011.DBIAppTypes.HistoryItem entity, string location,ref WorksheetSetData worksheetset)
          {
          entity.IsValid = true;
          worksheetset.ClearMessages(TypeId, entity.Id);
          

          return entity;
          }
        
        #endregion
        // CALCULATION
        
        // BOOKINGS
        
        // IMPORT PREVIOUS
        
    }
}
  