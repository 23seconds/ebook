﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.RuleEngine.Core;
using EY.com.eBook.RuleEngine.Core.Interfaces;
using EY.com.eBook.RuleEngine.Data;
using EY.com.eBook.Core;

namespace EY.com.eBook.RuleEngine.AY2011
{
    
    public class VerliesHandelsdebiteurenApp
    {
        #region standard
        public static WorksheetTypeSettings Settings = new WorksheetTypeSettings
                                                      {
                                                        Clients = true
                                                        ,
                                                        Suppliers = true
                                                        ,
                                                        AllAccounts = true
                                                        , 
                                                        PerformsBookings=true
                                                      };
                                                      
        public static Guid TypeId = new Guid("41def4da-0f30-4d13-ae80-124f4c51a966");

        public List<string> ImportablePreviousTypes  = new List<string>() {  "EY.com.eBook.RuleEngine.AY2011.FacturatieApp"  };

        public Guid GetMyType() { return TypeId; }
        
        public void SetMinimal() 
        {
            if(Data==null) {
                Data = new EY.com.eBook.RuleEngine.AY2011.VerliesHandelsdebiteurenAppTypes.VerliesHandelsDebiteuren();
                Data.LastUpdated = DateTime.Now;
            }              
            
              if(Data.VerliesHD==null) {
                  Data.VerliesHD = new List<EY.com.eBook.RuleEngine.AY2011.VerliesHandelsdebiteurenAppTypes.VerliesHandelsDebiteur>();;
                  Data.LastUpdated = DateTime.Now;
              }
            
        }
        
        public string ToXml() 
        {
            return CoreHelper.SerializeToString(Data);
        }

        public void LoadXml(string xml)
        {
            if (!string.IsNullOrEmpty(xml)) {
                Data = CoreHelper.DeserializeFromString<EY.com.eBook.RuleEngine.AY2011.VerliesHandelsdebiteurenAppTypes.VerliesHandelsDebiteuren>(xml);
            }
            SetMinimal();
            
        }
        
        public EY.com.eBook.RuleEngine.AY2011.VerliesHandelsdebiteurenAppTypes.VerliesHandelsDebiteuren Data { get; set; }
        
        public void Calculate(ref WorksheetSetData worksheetset)
        {
           // Data.LastCalculated = DateTime.Now;
        }
        #endregion
        
        #region AddUpdateDelete
        
          public void AddToVerliesHD(EY.com.eBook.RuleEngine.AY2011.VerliesHandelsdebiteurenAppTypes.VerliesHandelsDebiteur entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void DeleteVerliesHD(Guid rowId,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void UpdateVerliesHD(EY.com.eBook.RuleEngine.AY2011.VerliesHandelsdebiteurenAppTypes.VerliesHandelsDebiteur entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
        #endregion
        
        #region VALIDATION
        
        public EY.com.eBook.RuleEngine.AY2011.VerliesHandelsdebiteurenAppTypes.VerliesHandelsDebiteur Validate(EY.com.eBook.RuleEngine.AY2011.VerliesHandelsdebiteurenAppTypes.VerliesHandelsDebiteur entity, string location,ref WorksheetSetData worksheetset)
          {
          entity.IsValid = true;
          worksheetset.ClearMessages(TypeId, entity.Id);
          

          return entity;
          }
        
        #endregion
        // CALCULATION
        
        // BOOKINGS
        
        // IMPORT PREVIOUS
        
    }
}
  