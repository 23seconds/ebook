﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ProtoBuf.Meta;
using EY.com.eBook.RuleEngine.Core.Interfaces;
using System.Reflection;
using System.Text.RegularExpressions;

namespace EY.com.eBook.RuleEngine.Data
{
    public class Prepper
    {
        

        public Dictionary<string, List<Type>> PrepareProtobuf()
        {
            Dictionary<string, List<Type>> _iWorksheetClassTypes = new Dictionary<string, List<Type>>();
            MetaType mt = RuntimeTypeModel.Default[typeof(IWorksheet)];
            Assembly ass = Assembly.Load("EY.com.eBook.RuleEngine.Contracts");
            Regex re = new Regex(@"EY\.com\.eBook\.RuleEngine\.AY[0-9]{4}\.(.{1,})Types");
            int nr = 1000;
            bool addsubs = !mt.HasSubtypes;
            foreach (Type t in ass.GetTypes().OrderBy(t=>t.FullName))
            {
                if (t.GetInterface("IWorksheet") != null)
                {
                    // INDEX ALL WORKSHEET TYPE - IWORKSHEET CLASSES.
                    Match ma = re.Match(t.Namespace);
                    if (ma != null && ma.Groups.Count > 1)
                    {
                        string ruleAppName = ma.Groups[1].Value;
                        if (!_iWorksheetClassTypes.ContainsKey(ruleAppName)) _iWorksheetClassTypes.Add(ruleAppName, new List<Type>());
                        _iWorksheetClassTypes[ruleAppName].Add(t);
                        nr++;
                        try {
                            
                            if(addsubs) mt.AddSubType(nr, t);
                        } catch {}
                        //t.GetProperties().First().Name //.GetType().GetGenericTypeDefinition().Name
                        
                    }


                    
                }
            }
            return _iWorksheetClassTypes;
        }
    }
}
