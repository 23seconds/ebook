using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using EY.com.eBook.API.Contracts.Services;
using EY.com.eBook.Caching;
using EY.com.eBook.RuleEngine;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.RuleEngine.Data;

namespace EY.com.eBook.API.Contracts.Proxies
{
    public class RuleEngine2009Proxy : ClientBase<IRuleEngine2009>, IRuleEngine2009
    {

        #region IRuleEngine2009 Members


        public decimal? GetTaxCalculation(CriteriaFileDataContract cfdc)
        {
            return Channel.GetTaxCalculation(cfdc);
        }

        public void ReCalculate(CriteriaFileDataContract cfdc)
        {
            Channel.ReCalculate(cfdc);
        }

        public void ImportPrevious(CriteriaFileDataContract cfdc)
        {
            Channel.ImportPrevious(cfdc);
        }

        #region Proxy



        #region FacturatieApp

        public EY.com.eBook.RuleEngine.AY2009.FacturatieAppTypes.Facturatie FacturatieAppGetData(CriteriaFileDataContract cfdc)
        {
            return Channel.FacturatieAppGetData(cfdc);
        }


        public EY.com.eBook.RuleEngine.AY2009.FacturatieAppTypes.Facturatie FacturatieAppCalculate(CriteriaFileDataContract cfdc)
        {
            return Channel.FacturatieAppCalculate(cfdc);
        }

        #region Entity validation


        public List<FileMessageDataContract> FacturatieAppFacturatieKlantValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaFacturatieAppFacturatieKlantDataContract cedc)
        {
            return Channel.FacturatieAppFacturatieKlantValidate(cedc);
        }



        public List<FileMessageDataContract> FacturatieAppFacturatieLeverancierCNValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaFacturatieAppFacturatieLeverancierCNDataContract cedc)
        {
            return Channel.FacturatieAppFacturatieLeverancierCNValidate(cedc);
        }



        public List<FileMessageDataContract> FacturatieAppFacturatieLeverancierValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaFacturatieAppFacturatieLeverancierDataContract cedc)
        {
            return Channel.FacturatieAppFacturatieLeverancierValidate(cedc);
        }



        #endregion



        #region Add/Remove


        public EY.com.eBook.RuleEngine.AY2009.FacturatieAppTypes.Facturatie FacturatieAppOpTeMakenFacturenAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaFacturatieAppFacturatieKlantDataContract cedc)
        {
            return Channel.FacturatieAppOpTeMakenFacturenAdd(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.FacturatieAppTypes.Facturatie FacturatieAppOpTeMakenFacturenRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            return Channel.FacturatieAppOpTeMakenFacturenRemove(cfrdc);
        }

        public EY.com.eBook.RuleEngine.AY2009.FacturatieAppTypes.Facturatie FacturatieAppOpTeMakenFacturenUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaFacturatieAppFacturatieKlantDataContract cedc)
        {
            return Channel.FacturatieAppOpTeMakenFacturenUpdate(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.FacturatieAppTypes.Facturatie FacturatieAppTeOntvangenCNAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaFacturatieAppFacturatieLeverancierCNDataContract cedc)
        {
            return Channel.FacturatieAppTeOntvangenCNAdd(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.FacturatieAppTypes.Facturatie FacturatieAppTeOntvangenCNRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            return Channel.FacturatieAppTeOntvangenCNRemove(cfrdc);
        }

        public EY.com.eBook.RuleEngine.AY2009.FacturatieAppTypes.Facturatie FacturatieAppTeOntvangenCNUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaFacturatieAppFacturatieLeverancierCNDataContract cedc)
        {
            return Channel.FacturatieAppTeOntvangenCNUpdate(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.FacturatieAppTypes.Facturatie FacturatieAppTeOntvangenFacturenAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaFacturatieAppFacturatieLeverancierDataContract cedc)
        {
            return Channel.FacturatieAppTeOntvangenFacturenAdd(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.FacturatieAppTypes.Facturatie FacturatieAppTeOntvangenFacturenRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            return Channel.FacturatieAppTeOntvangenFacturenRemove(cfrdc);
        }

        public EY.com.eBook.RuleEngine.AY2009.FacturatieAppTypes.Facturatie FacturatieAppTeOntvangenFacturenUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaFacturatieAppFacturatieLeverancierDataContract cedc)
        {
            return Channel.FacturatieAppTeOntvangenFacturenUpdate(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.FacturatieAppTypes.Facturatie FacturatieAppOpTeMakenCNAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaFacturatieAppFacturatieKlantDataContract cedc)
        {
            return Channel.FacturatieAppOpTeMakenCNAdd(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.FacturatieAppTypes.Facturatie FacturatieAppOpTeMakenCNRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            return Channel.FacturatieAppOpTeMakenCNRemove(cfrdc);
        }

        public EY.com.eBook.RuleEngine.AY2009.FacturatieAppTypes.Facturatie FacturatieAppOpTeMakenCNUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaFacturatieAppFacturatieKlantDataContract cedc)
        {
            return Channel.FacturatieAppOpTeMakenCNUpdate(cedc);
        }

        #endregion

        #endregion



        #region OverlopendeRekeningenApp

        public EY.com.eBook.RuleEngine.AY2009.OverlopendeRekeningenAppTypes.OverlopendeRekeningen OverlopendeRekeningenAppGetData(CriteriaFileDataContract cfdc)
        {
            return Channel.OverlopendeRekeningenAppGetData(cfdc);
        }


        public EY.com.eBook.RuleEngine.AY2009.OverlopendeRekeningenAppTypes.OverlopendeRekeningen OverlopendeRekeningenAppCalculate(CriteriaFileDataContract cfdc)
        {
            return Channel.OverlopendeRekeningenAppCalculate(cfdc);
        }

        #region Entity validation


        public List<FileMessageDataContract> OverlopendeRekeningenAppOverTeDragenKostValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaOverlopendeRekeningenAppOverTeDragenKostDataContract cedc)
        {
            return Channel.OverlopendeRekeningenAppOverTeDragenKostValidate(cedc);
        }



        public List<FileMessageDataContract> OverlopendeRekeningenAppVerkregenOpbrengstValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaOverlopendeRekeningenAppVerkregenOpbrengstDataContract cedc)
        {
            return Channel.OverlopendeRekeningenAppVerkregenOpbrengstValidate(cedc);
        }



        public List<FileMessageDataContract> OverlopendeRekeningenAppToeTeRekenenKostValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaOverlopendeRekeningenAppToeTeRekenenKostDataContract cedc)
        {
            return Channel.OverlopendeRekeningenAppToeTeRekenenKostValidate(cedc);
        }



        public List<FileMessageDataContract> OverlopendeRekeningenAppOverTeDragenOpbrengstValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaOverlopendeRekeningenAppOverTeDragenOpbrengstDataContract cedc)
        {
            return Channel.OverlopendeRekeningenAppOverTeDragenOpbrengstValidate(cedc);
        }



        #endregion



        #region Add/Remove


        public EY.com.eBook.RuleEngine.AY2009.OverlopendeRekeningenAppTypes.OverlopendeRekeningen OverlopendeRekeningenAppOverTeDragenKostenAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaOverlopendeRekeningenAppOverTeDragenKostDataContract cedc)
        {
            return Channel.OverlopendeRekeningenAppOverTeDragenKostenAdd(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.OverlopendeRekeningenAppTypes.OverlopendeRekeningen OverlopendeRekeningenAppOverTeDragenKostenRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            return Channel.OverlopendeRekeningenAppOverTeDragenKostenRemove(cfrdc);
        }

        public EY.com.eBook.RuleEngine.AY2009.OverlopendeRekeningenAppTypes.OverlopendeRekeningen OverlopendeRekeningenAppOverTeDragenKostenUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaOverlopendeRekeningenAppOverTeDragenKostDataContract cedc)
        {
            return Channel.OverlopendeRekeningenAppOverTeDragenKostenUpdate(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.OverlopendeRekeningenAppTypes.OverlopendeRekeningen OverlopendeRekeningenAppVerkregenOpbrenstenAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaOverlopendeRekeningenAppVerkregenOpbrengstDataContract cedc)
        {
            return Channel.OverlopendeRekeningenAppVerkregenOpbrenstenAdd(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.OverlopendeRekeningenAppTypes.OverlopendeRekeningen OverlopendeRekeningenAppVerkregenOpbrenstenRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            return Channel.OverlopendeRekeningenAppVerkregenOpbrenstenRemove(cfrdc);
        }

        public EY.com.eBook.RuleEngine.AY2009.OverlopendeRekeningenAppTypes.OverlopendeRekeningen OverlopendeRekeningenAppVerkregenOpbrenstenUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaOverlopendeRekeningenAppVerkregenOpbrengstDataContract cedc)
        {
            return Channel.OverlopendeRekeningenAppVerkregenOpbrenstenUpdate(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.OverlopendeRekeningenAppTypes.OverlopendeRekeningen OverlopendeRekeningenAppToeTeRekenenKostenAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaOverlopendeRekeningenAppToeTeRekenenKostDataContract cedc)
        {
            return Channel.OverlopendeRekeningenAppToeTeRekenenKostenAdd(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.OverlopendeRekeningenAppTypes.OverlopendeRekeningen OverlopendeRekeningenAppToeTeRekenenKostenRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            return Channel.OverlopendeRekeningenAppToeTeRekenenKostenRemove(cfrdc);
        }

        public EY.com.eBook.RuleEngine.AY2009.OverlopendeRekeningenAppTypes.OverlopendeRekeningen OverlopendeRekeningenAppToeTeRekenenKostenUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaOverlopendeRekeningenAppToeTeRekenenKostDataContract cedc)
        {
            return Channel.OverlopendeRekeningenAppToeTeRekenenKostenUpdate(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.OverlopendeRekeningenAppTypes.OverlopendeRekeningen OverlopendeRekeningenAppOverTeDragenOpbrengstenAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaOverlopendeRekeningenAppOverTeDragenOpbrengstDataContract cedc)
        {
            return Channel.OverlopendeRekeningenAppOverTeDragenOpbrengstenAdd(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.OverlopendeRekeningenAppTypes.OverlopendeRekeningen OverlopendeRekeningenAppOverTeDragenOpbrengstenRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            return Channel.OverlopendeRekeningenAppOverTeDragenOpbrengstenRemove(cfrdc);
        }

        public EY.com.eBook.RuleEngine.AY2009.OverlopendeRekeningenAppTypes.OverlopendeRekeningen OverlopendeRekeningenAppOverTeDragenOpbrengstenUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaOverlopendeRekeningenAppOverTeDragenOpbrengstDataContract cedc)
        {
            return Channel.OverlopendeRekeningenAppOverTeDragenOpbrengstenUpdate(cedc);
        }

        #endregion

        #endregion



        #region VergelijkendeCijfersApp

        public EY.com.eBook.RuleEngine.AY2009.VergelijkendeCijfersAppTypes.WPVergelijkendeCijfers VergelijkendeCijfersAppGetData(CriteriaFileDataContract cfdc)
        {
            return Channel.VergelijkendeCijfersAppGetData(cfdc);
        }


        public EY.com.eBook.RuleEngine.AY2009.VergelijkendeCijfersAppTypes.WPVergelijkendeCijfers VergelijkendeCijfersAppCalculate(CriteriaFileDataContract cfdc)
        {
            return Channel.VergelijkendeCijfersAppCalculate(cfdc);
        }

        #region Entity validation


        public List<FileMessageDataContract> VergelijkendeCijfersAppCijferBlokValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVergelijkendeCijfersAppCijferBlokDataContract cedc)
        {
            return Channel.VergelijkendeCijfersAppCijferBlokValidate(cedc);
        }



        #endregion



        #region Add/Remove


        public EY.com.eBook.RuleEngine.AY2009.VergelijkendeCijfersAppTypes.WPVergelijkendeCijfers VergelijkendeCijfersAppKostenAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVergelijkendeCijfersAppCijferBlokDataContract cedc)
        {
            return Channel.VergelijkendeCijfersAppKostenAdd(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.VergelijkendeCijfersAppTypes.WPVergelijkendeCijfers VergelijkendeCijfersAppKostenRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            return Channel.VergelijkendeCijfersAppKostenRemove(cfrdc);
        }

        public EY.com.eBook.RuleEngine.AY2009.VergelijkendeCijfersAppTypes.WPVergelijkendeCijfers VergelijkendeCijfersAppKostenUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVergelijkendeCijfersAppCijferBlokDataContract cedc)
        {
            return Channel.VergelijkendeCijfersAppKostenUpdate(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.VergelijkendeCijfersAppTypes.WPVergelijkendeCijfers VergelijkendeCijfersAppOpbrengstenAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVergelijkendeCijfersAppCijferBlokDataContract cedc)
        {
            return Channel.VergelijkendeCijfersAppOpbrengstenAdd(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.VergelijkendeCijfersAppTypes.WPVergelijkendeCijfers VergelijkendeCijfersAppOpbrengstenRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            return Channel.VergelijkendeCijfersAppOpbrengstenRemove(cfrdc);
        }

        public EY.com.eBook.RuleEngine.AY2009.VergelijkendeCijfersAppTypes.WPVergelijkendeCijfers VergelijkendeCijfersAppOpbrengstenUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVergelijkendeCijfersAppCijferBlokDataContract cedc)
        {
            return Channel.VergelijkendeCijfersAppOpbrengstenUpdate(cedc);
        }

        #endregion

        #endregion



        #region VoorschottenApp

        public EY.com.eBook.RuleEngine.AY2009.VoorschottenAppTypes.Voorschotten VoorschottenAppGetData(CriteriaFileDataContract cfdc)
        {
            return Channel.VoorschottenAppGetData(cfdc);
        }


        public EY.com.eBook.RuleEngine.AY2009.VoorschottenAppTypes.Voorschotten VoorschottenAppCalculate(CriteriaFileDataContract cfdc)
        {
            return Channel.VoorschottenAppCalculate(cfdc);
        }

        #region Entity validation


        public List<FileMessageDataContract> VoorschottenAppGegevenValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoorschottenAppGegevenDataContract cedc)
        {
            return Channel.VoorschottenAppGegevenValidate(cedc);
        }



        public List<FileMessageDataContract> VoorschottenAppIntrestVoetValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoorschottenAppIntrestVoetDataContract cedc)
        {
            return Channel.VoorschottenAppIntrestVoetValidate(cedc);
        }



        public List<FileMessageDataContract> VoorschottenAppRentegevendVoorschotValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoorschottenAppRentegevendVoorschotDataContract cedc)
        {
            return Channel.VoorschottenAppRentegevendVoorschotValidate(cedc);
        }



        #endregion



        #region Add/Remove


        public EY.com.eBook.RuleEngine.AY2009.VoorschottenAppTypes.Voorschotten VoorschottenAppGegevensAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoorschottenAppGegevenDataContract cedc)
        {
            return Channel.VoorschottenAppGegevensAdd(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.VoorschottenAppTypes.Voorschotten VoorschottenAppGegevensRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            return Channel.VoorschottenAppGegevensRemove(cfrdc);
        }

        public EY.com.eBook.RuleEngine.AY2009.VoorschottenAppTypes.Voorschotten VoorschottenAppGegevensUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoorschottenAppGegevenDataContract cedc)
        {
            return Channel.VoorschottenAppGegevensUpdate(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.VoorschottenAppTypes.Voorschotten VoorschottenAppInterestVoetenAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoorschottenAppIntrestVoetDataContract cedc)
        {
            return Channel.VoorschottenAppInterestVoetenAdd(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.VoorschottenAppTypes.Voorschotten VoorschottenAppInterestVoetenRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            return Channel.VoorschottenAppInterestVoetenRemove(cfrdc);
        }

        public EY.com.eBook.RuleEngine.AY2009.VoorschottenAppTypes.Voorschotten VoorschottenAppInterestVoetenUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoorschottenAppIntrestVoetDataContract cedc)
        {
            return Channel.VoorschottenAppInterestVoetenUpdate(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.VoorschottenAppTypes.Voorschotten VoorschottenAppRentegevendeVoorschottenAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoorschottenAppRentegevendVoorschotDataContract cedc)
        {
            return Channel.VoorschottenAppRentegevendeVoorschottenAdd(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.VoorschottenAppTypes.Voorschotten VoorschottenAppRentegevendeVoorschottenRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            return Channel.VoorschottenAppRentegevendeVoorschottenRemove(cfrdc);
        }

        public EY.com.eBook.RuleEngine.AY2009.VoorschottenAppTypes.Voorschotten VoorschottenAppRentegevendeVoorschottenUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoorschottenAppRentegevendVoorschotDataContract cedc)
        {
            return Channel.VoorschottenAppRentegevendeVoorschottenUpdate(cedc);
        }

        #endregion

        #endregion



        #region ErelonenHuurApp

        public EY.com.eBook.RuleEngine.AY2009.ErelonenHuurAppTypes.ErelonenHuur ErelonenHuurAppGetData(CriteriaFileDataContract cfdc)
        {
            return Channel.ErelonenHuurAppGetData(cfdc);
        }


        public EY.com.eBook.RuleEngine.AY2009.ErelonenHuurAppTypes.ErelonenHuur ErelonenHuurAppCalculate(CriteriaFileDataContract cfdc)
        {
            return Channel.ErelonenHuurAppCalculate(cfdc);
        }

        #region Entity validation


        public List<FileMessageDataContract> ErelonenHuurAppImportItemValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaErelonenHuurAppImportItemDataContract cedc)
        {
            return Channel.ErelonenHuurAppImportItemValidate(cedc);
        }



        public List<FileMessageDataContract> ErelonenHuurAppImportedHistoryValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaErelonenHuurAppImportedHistoryDataContract cedc)
        {
            return Channel.ErelonenHuurAppImportedHistoryValidate(cedc);
        }



        public List<FileMessageDataContract> ErelonenHuurAppVerdelingHistoriekValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaErelonenHuurAppVerdelingHistoriekDataContract cedc)
        {
            return Channel.ErelonenHuurAppVerdelingHistoriekValidate(cedc);
        }



        public List<FileMessageDataContract> ErelonenHuurAppFiche281_50Validate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaErelonenHuurAppFiche281_50DataContract cedc)
        {
            return Channel.ErelonenHuurAppFiche281_50Validate(cedc);
        }



        #endregion



        #region Add/Remove


        public EY.com.eBook.RuleEngine.AY2009.ErelonenHuurAppTypes.ErelonenHuur ErelonenHuurAppHistoriekAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaErelonenHuurAppVerdelingHistoriekDataContract cedc)
        {
            return Channel.ErelonenHuurAppHistoriekAdd(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.ErelonenHuurAppTypes.ErelonenHuur ErelonenHuurAppHistoriekRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            return Channel.ErelonenHuurAppHistoriekRemove(cfrdc);
        }

        public EY.com.eBook.RuleEngine.AY2009.ErelonenHuurAppTypes.ErelonenHuur ErelonenHuurAppHistoriekUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaErelonenHuurAppVerdelingHistoriekDataContract cedc)
        {
            return Channel.ErelonenHuurAppHistoriekUpdate(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.ErelonenHuurAppTypes.ErelonenHuur ErelonenHuurAppFiches281_50Update(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaErelonenHuurAppFiche281_50DataContract cedc)
        {
            return Channel.ErelonenHuurAppFiches281_50Update(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.ErelonenHuurAppTypes.ErelonenHuur ErelonenHuurAppFiche281_50ColAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaErelonenHuurAppFiche281_50DataContract cedc)
        {
            return Channel.ErelonenHuurAppFiche281_50ColAdd(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.ErelonenHuurAppTypes.ErelonenHuur ErelonenHuurAppFiche281_50ColRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            return Channel.ErelonenHuurAppFiche281_50ColRemove(cfrdc);
        }

        public EY.com.eBook.RuleEngine.AY2009.ErelonenHuurAppTypes.ErelonenHuur ErelonenHuurAppFiche281_50ColUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaErelonenHuurAppFiche281_50DataContract cedc)
        {
            return Channel.ErelonenHuurAppFiche281_50ColUpdate(cedc);
        }

        #endregion

        #endregion



        #region VoordelenApp

        public EY.com.eBook.RuleEngine.AY2009.VoordelenAppTypes.Voordelen VoordelenAppGetData(CriteriaFileDataContract cfdc)
        {
            return Channel.VoordelenAppGetData(cfdc);
        }


        public EY.com.eBook.RuleEngine.AY2009.VoordelenAppTypes.Voordelen VoordelenAppCalculate(CriteriaFileDataContract cfdc)
        {
            return Channel.VoordelenAppCalculate(cfdc);
        }

        #region Entity validation


        public List<FileMessageDataContract> VoordelenAppVAA_AutoValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoordelenAppVAA_AutoDataContract cedc)
        {
            return Channel.VoordelenAppVAA_AutoValidate(cedc);
        }



        public List<FileMessageDataContract> VoordelenAppVAA_AutoCO2Validate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoordelenAppVAA_AutoCO2DataContract cedc)
        {
            return Channel.VoordelenAppVAA_AutoCO2Validate(cedc);
        }



        public List<FileMessageDataContract> VoordelenAppVAA_NutsValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoordelenAppVAA_NutsDataContract cedc)
        {
            return Channel.VoordelenAppVAA_NutsValidate(cedc);
        }



        public List<FileMessageDataContract> VoordelenAppVAA_HUURValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoordelenAppVAA_HUURDataContract cedc)
        {
            return Channel.VoordelenAppVAA_HUURValidate(cedc);
        }



        public List<FileMessageDataContract> VoordelenAppVAA_HUURHERKValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoordelenAppVAA_HUURHERKDataContract cedc)
        {
            return Channel.VoordelenAppVAA_HUURHERKValidate(cedc);
        }



        public List<FileMessageDataContract> VoordelenAppVAA_PCValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoordelenAppVAA_PCDataContract cedc)
        {
            return Channel.VoordelenAppVAA_PCValidate(cedc);
        }



        public List<FileMessageDataContract> VoordelenAppVAA_TelefonieValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoordelenAppVAA_TelefonieDataContract cedc)
        {
            return Channel.VoordelenAppVAA_TelefonieValidate(cedc);
        }



        public List<FileMessageDataContract> VoordelenAppVAA_OverigeValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoordelenAppVAA_OverigeDataContract cedc)
        {
            return Channel.VoordelenAppVAA_OverigeValidate(cedc);
        }



        public List<FileMessageDataContract> VoordelenAppVAA_Interesten_RC_BedrijfsleiderValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoordelenAppVAA_Interesten_RC_BedrijfsleiderDataContract cedc)
        {
            return Channel.VoordelenAppVAA_Interesten_RC_BedrijfsleiderValidate(cedc);
        }



        public List<FileMessageDataContract> VoordelenAppVAA_Soc_Bijdr_BedrijfsleiderValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoordelenAppVAA_Soc_Bijdr_BedrijfsleiderDataContract cedc)
        {
            return Channel.VoordelenAppVAA_Soc_Bijdr_BedrijfsleiderValidate(cedc);
        }



        public List<FileMessageDataContract> VoordelenAppVAA_Overige_NutsValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoordelenAppVAA_Overige_NutsDataContract cedc)
        {
            return Channel.VoordelenAppVAA_Overige_NutsValidate(cedc);
        }



        #endregion



        #region Add/Remove


        public EY.com.eBook.RuleEngine.AY2009.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardAutoAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoordelenAppVAA_AutoDataContract cedc)
        {
            return Channel.VoordelenAppVoordelenVanAlleAardAutoAdd(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardAutoRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            return Channel.VoordelenAppVoordelenVanAlleAardAutoRemove(cfrdc);
        }

        public EY.com.eBook.RuleEngine.AY2009.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardAutoUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoordelenAppVAA_AutoDataContract cedc)
        {
            return Channel.VoordelenAppVoordelenVanAlleAardAutoUpdate(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardAutoCO2Add(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoordelenAppVAA_AutoCO2DataContract cedc)
        {
            return Channel.VoordelenAppVoordelenVanAlleAardAutoCO2Add(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardAutoCO2Remove(CriteriaFileAndRowDataContract cfrdc)
        {
            return Channel.VoordelenAppVoordelenVanAlleAardAutoCO2Remove(cfrdc);
        }

        public EY.com.eBook.RuleEngine.AY2009.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardAutoCO2Update(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoordelenAppVAA_AutoCO2DataContract cedc)
        {
            return Channel.VoordelenAppVoordelenVanAlleAardAutoCO2Update(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardNutsAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoordelenAppVAA_NutsDataContract cedc)
        {
            return Channel.VoordelenAppVoordelenVanAlleAardNutsAdd(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardNutsRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            return Channel.VoordelenAppVoordelenVanAlleAardNutsRemove(cfrdc);
        }

        public EY.com.eBook.RuleEngine.AY2009.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardNutsUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoordelenAppVAA_NutsDataContract cedc)
        {
            return Channel.VoordelenAppVoordelenVanAlleAardNutsUpdate(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardHuurAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoordelenAppVAA_HUURDataContract cedc)
        {
            return Channel.VoordelenAppVoordelenVanAlleAardHuurAdd(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardHuurRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            return Channel.VoordelenAppVoordelenVanAlleAardHuurRemove(cfrdc);
        }

        public EY.com.eBook.RuleEngine.AY2009.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardHuurUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoordelenAppVAA_HUURDataContract cedc)
        {
            return Channel.VoordelenAppVoordelenVanAlleAardHuurUpdate(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardHuurHerkwalificatieAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoordelenAppVAA_HUURHERKDataContract cedc)
        {
            return Channel.VoordelenAppVoordelenVanAlleAardHuurHerkwalificatieAdd(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardHuurHerkwalificatieRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            return Channel.VoordelenAppVoordelenVanAlleAardHuurHerkwalificatieRemove(cfrdc);
        }

        public EY.com.eBook.RuleEngine.AY2009.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardHuurHerkwalificatieUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoordelenAppVAA_HUURHERKDataContract cedc)
        {
            return Channel.VoordelenAppVoordelenVanAlleAardHuurHerkwalificatieUpdate(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardPCAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoordelenAppVAA_PCDataContract cedc)
        {
            return Channel.VoordelenAppVoordelenVanAlleAardPCAdd(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardPCRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            return Channel.VoordelenAppVoordelenVanAlleAardPCRemove(cfrdc);
        }

        public EY.com.eBook.RuleEngine.AY2009.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardPCUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoordelenAppVAA_PCDataContract cedc)
        {
            return Channel.VoordelenAppVoordelenVanAlleAardPCUpdate(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardTelefonieAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoordelenAppVAA_TelefonieDataContract cedc)
        {
            return Channel.VoordelenAppVoordelenVanAlleAardTelefonieAdd(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardTelefonieRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            return Channel.VoordelenAppVoordelenVanAlleAardTelefonieRemove(cfrdc);
        }

        public EY.com.eBook.RuleEngine.AY2009.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardTelefonieUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoordelenAppVAA_TelefonieDataContract cedc)
        {
            return Channel.VoordelenAppVoordelenVanAlleAardTelefonieUpdate(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardOverigeAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoordelenAppVAA_OverigeDataContract cedc)
        {
            return Channel.VoordelenAppVoordelenVanAlleAardOverigeAdd(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardOverigeRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            return Channel.VoordelenAppVoordelenVanAlleAardOverigeRemove(cfrdc);
        }

        public EY.com.eBook.RuleEngine.AY2009.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardOverigeUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoordelenAppVAA_OverigeDataContract cedc)
        {
            return Channel.VoordelenAppVoordelenVanAlleAardOverigeUpdate(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.VoordelenAppTypes.Voordelen VoordelenAppVoordelenInterestenRCBedrijfsleiderAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoordelenAppVAA_Interesten_RC_BedrijfsleiderDataContract cedc)
        {
            return Channel.VoordelenAppVoordelenInterestenRCBedrijfsleiderAdd(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.VoordelenAppTypes.Voordelen VoordelenAppVoordelenInterestenRCBedrijfsleiderRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            return Channel.VoordelenAppVoordelenInterestenRCBedrijfsleiderRemove(cfrdc);
        }

        public EY.com.eBook.RuleEngine.AY2009.VoordelenAppTypes.Voordelen VoordelenAppVoordelenInterestenRCBedrijfsleiderUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoordelenAppVAA_Interesten_RC_BedrijfsleiderDataContract cedc)
        {
            return Channel.VoordelenAppVoordelenInterestenRCBedrijfsleiderUpdate(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.VoordelenAppTypes.Voordelen VoordelenAppVoordelenSocialeBijdragenBedrijfsleiderAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoordelenAppVAA_Soc_Bijdr_BedrijfsleiderDataContract cedc)
        {
            return Channel.VoordelenAppVoordelenSocialeBijdragenBedrijfsleiderAdd(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.VoordelenAppTypes.Voordelen VoordelenAppVoordelenSocialeBijdragenBedrijfsleiderRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            return Channel.VoordelenAppVoordelenSocialeBijdragenBedrijfsleiderRemove(cfrdc);
        }

        public EY.com.eBook.RuleEngine.AY2009.VoordelenAppTypes.Voordelen VoordelenAppVoordelenSocialeBijdragenBedrijfsleiderUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoordelenAppVAA_Soc_Bijdr_BedrijfsleiderDataContract cedc)
        {
            return Channel.VoordelenAppVoordelenSocialeBijdragenBedrijfsleiderUpdate(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.VoordelenAppTypes.Voordelen VoordelenAppVoordelenOverigeNutsAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoordelenAppVAA_Overige_NutsDataContract cedc)
        {
            return Channel.VoordelenAppVoordelenOverigeNutsAdd(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.VoordelenAppTypes.Voordelen VoordelenAppVoordelenOverigeNutsRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            return Channel.VoordelenAppVoordelenOverigeNutsRemove(cfrdc);
        }

        public EY.com.eBook.RuleEngine.AY2009.VoordelenAppTypes.Voordelen VoordelenAppVoordelenOverigeNutsUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoordelenAppVAA_Overige_NutsDataContract cedc)
        {
            return Channel.VoordelenAppVoordelenOverigeNutsUpdate(cedc);
        }

        #endregion

        #endregion



        #region PersoneelApp

        public EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.WPPersoneel PersoneelAppGetData(CriteriaFileDataContract cfdc)
        {
            return Channel.PersoneelAppGetData(cfdc);
        }


        public EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.WPPersoneel PersoneelAppCalculate(CriteriaFileDataContract cfdc)
        {
            return Channel.PersoneelAppCalculate(cfdc);
        }

        #region Entity validation


        public List<FileMessageDataContract> PersoneelAppItem325Validate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaPersoneelAppItem325DataContract cedc)
        {
            return Channel.PersoneelAppItem325Validate(cedc);
        }



        public List<FileMessageDataContract> PersoneelAppVerschilItemValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaPersoneelAppVerschilItemDataContract cedc)
        {
            return Channel.PersoneelAppVerschilItemValidate(cedc);
        }



        public List<FileMessageDataContract> PersoneelAppWerknemerValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaPersoneelAppWerknemerDataContract cedc)
        {
            return Channel.PersoneelAppWerknemerValidate(cedc);
        }



        public List<FileMessageDataContract> PersoneelAppAlgemeenValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaPersoneelAppAlgemeenDataContract cedc)
        {
            return Channel.PersoneelAppAlgemeenValidate(cedc);
        }



        public List<FileMessageDataContract> PersoneelAppVakantiegeldValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaPersoneelAppVakantiegeldDataContract cedc)
        {
            return Channel.PersoneelAppVakantiegeldValidate(cedc);
        }



        #endregion



        #region Add/Remove


        public EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.WPPersoneel PersoneelApp_325_20CollAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaPersoneelAppItem325DataContract cedc)
        {
            return Channel.PersoneelApp_325_20CollAdd(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.WPPersoneel PersoneelApp_325_20CollRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            return Channel.PersoneelApp_325_20CollRemove(cfrdc);
        }

        public EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.WPPersoneel PersoneelApp_325_20CollUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaPersoneelAppItem325DataContract cedc)
        {
            return Channel.PersoneelApp_325_20CollUpdate(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.WPPersoneel PersoneelAppTeVerklarenVerschil325_20Add(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaPersoneelAppVerschilItemDataContract cedc)
        {
            return Channel.PersoneelAppTeVerklarenVerschil325_20Add(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.WPPersoneel PersoneelAppTeVerklarenVerschil325_20Remove(CriteriaFileAndRowDataContract cfrdc)
        {
            return Channel.PersoneelAppTeVerklarenVerschil325_20Remove(cfrdc);
        }

        public EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.WPPersoneel PersoneelAppTeVerklarenVerschil325_20Update(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaPersoneelAppVerschilItemDataContract cedc)
        {
            return Channel.PersoneelAppTeVerklarenVerschil325_20Update(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.WPPersoneel PersoneelApp_325_10CollAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaPersoneelAppItem325DataContract cedc)
        {
            return Channel.PersoneelApp_325_10CollAdd(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.WPPersoneel PersoneelApp_325_10CollRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            return Channel.PersoneelApp_325_10CollRemove(cfrdc);
        }

        public EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.WPPersoneel PersoneelApp_325_10CollUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaPersoneelAppItem325DataContract cedc)
        {
            return Channel.PersoneelApp_325_10CollUpdate(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.WPPersoneel PersoneelAppTeVerklarenVerschil325_10Add(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaPersoneelAppVerschilItemDataContract cedc)
        {
            return Channel.PersoneelAppTeVerklarenVerschil325_10Add(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.WPPersoneel PersoneelAppTeVerklarenVerschil325_10Remove(CriteriaFileAndRowDataContract cfrdc)
        {
            return Channel.PersoneelAppTeVerklarenVerschil325_10Remove(cfrdc);
        }

        public EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.WPPersoneel PersoneelAppTeVerklarenVerschil325_10Update(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaPersoneelAppVerschilItemDataContract cedc)
        {
            return Channel.PersoneelAppTeVerklarenVerschil325_10Update(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.WPPersoneel PersoneelAppArbeidersFormUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaPersoneelAppWerknemerDataContract cedc)
        {
            return Channel.PersoneelAppArbeidersFormUpdate(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.WPPersoneel PersoneelAppBediendeFormUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaPersoneelAppWerknemerDataContract cedc)
        {
            return Channel.PersoneelAppBediendeFormUpdate(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.WPPersoneel PersoneelAppAansluitingArbeidersAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaPersoneelAppVerschilItemDataContract cedc)
        {
            return Channel.PersoneelAppAansluitingArbeidersAdd(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.WPPersoneel PersoneelAppAansluitingArbeidersRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            return Channel.PersoneelAppAansluitingArbeidersRemove(cfrdc);
        }

        public EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.WPPersoneel PersoneelAppAansluitingArbeidersUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaPersoneelAppVerschilItemDataContract cedc)
        {
            return Channel.PersoneelAppAansluitingArbeidersUpdate(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.WPPersoneel PersoneelAppAansluitingBediendenAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaPersoneelAppVerschilItemDataContract cedc)
        {
            return Channel.PersoneelAppAansluitingBediendenAdd(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.WPPersoneel PersoneelAppAansluitingBediendenRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            return Channel.PersoneelAppAansluitingBediendenRemove(cfrdc);
        }

        public EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.WPPersoneel PersoneelAppAansluitingBediendenUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaPersoneelAppVerschilItemDataContract cedc)
        {
            return Channel.PersoneelAppAansluitingBediendenUpdate(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.WPPersoneel PersoneelAppRSZCollAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaPersoneelAppAlgemeenDataContract cedc)
        {
            return Channel.PersoneelAppRSZCollAdd(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.WPPersoneel PersoneelAppRSZCollRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            return Channel.PersoneelAppRSZCollRemove(cfrdc);
        }

        public EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.WPPersoneel PersoneelAppRSZCollUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaPersoneelAppAlgemeenDataContract cedc)
        {
            return Channel.PersoneelAppRSZCollUpdate(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.WPPersoneel PersoneelAppBVsCollAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaPersoneelAppAlgemeenDataContract cedc)
        {
            return Channel.PersoneelAppBVsCollAdd(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.WPPersoneel PersoneelAppBVsCollRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            return Channel.PersoneelAppBVsCollRemove(cfrdc);
        }

        public EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.WPPersoneel PersoneelAppBVsCollUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaPersoneelAppAlgemeenDataContract cedc)
        {
            return Channel.PersoneelAppBVsCollUpdate(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.WPPersoneel PersoneelAppNettoCollAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaPersoneelAppAlgemeenDataContract cedc)
        {
            return Channel.PersoneelAppNettoCollAdd(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.WPPersoneel PersoneelAppNettoCollRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            return Channel.PersoneelAppNettoCollRemove(cfrdc);
        }

        public EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.WPPersoneel PersoneelAppNettoCollUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaPersoneelAppAlgemeenDataContract cedc)
        {
            return Channel.PersoneelAppNettoCollUpdate(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.WPPersoneel PersoneelAppVakantieGeldCollUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaPersoneelAppVakantiegeldDataContract cedc)
        {
            return Channel.PersoneelAppVakantieGeldCollUpdate(cedc);
        }

        #endregion

        #endregion



        #region BTWApp

        public EY.com.eBook.RuleEngine.AY2009.BTWAppTypes.WPBTW BTWAppGetData(CriteriaFileDataContract cfdc)
        {
            return Channel.BTWAppGetData(cfdc);
        }


        public EY.com.eBook.RuleEngine.AY2009.BTWAppTypes.WPBTW BTWAppCalculate(CriteriaFileDataContract cfdc)
        {
            return Channel.BTWAppCalculate(cfdc);
        }

        #region Entity validation


        public List<FileMessageDataContract> BTWAppOmzetAangifteValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBTWAppOmzetAangifteDataContract cedc)
        {
            return Channel.BTWAppOmzetAangifteValidate(cedc);
        }



        public List<FileMessageDataContract> BTWAppOmzetGeboektValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBTWAppOmzetGeboektDataContract cedc)
        {
            return Channel.BTWAppOmzetGeboektValidate(cedc);
        }



        public List<FileMessageDataContract> BTWAppOmzetVerklaringValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBTWAppOmzetVerklaringDataContract cedc)
        {
            return Channel.BTWAppOmzetVerklaringValidate(cedc);
        }



        public List<FileMessageDataContract> BTWAppRCAangifteValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBTWAppRCAangifteDataContract cedc)
        {
            return Channel.BTWAppRCAangifteValidate(cedc);
        }



        public List<FileMessageDataContract> BTWAppRCGeboektValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBTWAppRCGeboektDataContract cedc)
        {
            return Channel.BTWAppRCGeboektValidate(cedc);
        }



        public List<FileMessageDataContract> BTWAppRCVerklaringValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBTWAppRCVerklaringDataContract cedc)
        {
            return Channel.BTWAppRCVerklaringValidate(cedc);
        }



        #endregion



        #region Add/Remove


        public EY.com.eBook.RuleEngine.AY2009.BTWAppTypes.WPBTW BTWAppOmzetAangiftesAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBTWAppOmzetAangifteDataContract cedc)
        {
            return Channel.BTWAppOmzetAangiftesAdd(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.BTWAppTypes.WPBTW BTWAppOmzetAangiftesRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            return Channel.BTWAppOmzetAangiftesRemove(cfrdc);
        }

        public EY.com.eBook.RuleEngine.AY2009.BTWAppTypes.WPBTW BTWAppOmzetAangiftesUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBTWAppOmzetAangifteDataContract cedc)
        {
            return Channel.BTWAppOmzetAangiftesUpdate(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.BTWAppTypes.WPBTW BTWAppOmzettenGeboektAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBTWAppOmzetGeboektDataContract cedc)
        {
            return Channel.BTWAppOmzettenGeboektAdd(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.BTWAppTypes.WPBTW BTWAppOmzettenGeboektRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            return Channel.BTWAppOmzettenGeboektRemove(cfrdc);
        }

        public EY.com.eBook.RuleEngine.AY2009.BTWAppTypes.WPBTW BTWAppOmzettenGeboektUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBTWAppOmzetGeboektDataContract cedc)
        {
            return Channel.BTWAppOmzettenGeboektUpdate(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.BTWAppTypes.WPBTW BTWAppOmzetVerklaringenAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBTWAppOmzetVerklaringDataContract cedc)
        {
            return Channel.BTWAppOmzetVerklaringenAdd(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.BTWAppTypes.WPBTW BTWAppOmzetVerklaringenRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            return Channel.BTWAppOmzetVerklaringenRemove(cfrdc);
        }

        public EY.com.eBook.RuleEngine.AY2009.BTWAppTypes.WPBTW BTWAppOmzetVerklaringenUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBTWAppOmzetVerklaringDataContract cedc)
        {
            return Channel.BTWAppOmzetVerklaringenUpdate(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.BTWAppTypes.WPBTW BTWAppRCAangiftesAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBTWAppRCAangifteDataContract cedc)
        {
            return Channel.BTWAppRCAangiftesAdd(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.BTWAppTypes.WPBTW BTWAppRCAangiftesRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            return Channel.BTWAppRCAangiftesRemove(cfrdc);
        }

        public EY.com.eBook.RuleEngine.AY2009.BTWAppTypes.WPBTW BTWAppRCAangiftesUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBTWAppRCAangifteDataContract cedc)
        {
            return Channel.BTWAppRCAangiftesUpdate(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.BTWAppTypes.WPBTW BTWAppRCenGeboektAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBTWAppRCGeboektDataContract cedc)
        {
            return Channel.BTWAppRCenGeboektAdd(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.BTWAppTypes.WPBTW BTWAppRCenGeboektRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            return Channel.BTWAppRCenGeboektRemove(cfrdc);
        }

        public EY.com.eBook.RuleEngine.AY2009.BTWAppTypes.WPBTW BTWAppRCenGeboektUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBTWAppRCGeboektDataContract cedc)
        {
            return Channel.BTWAppRCenGeboektUpdate(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.BTWAppTypes.WPBTW BTWAppRCVerklaringenAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBTWAppRCVerklaringDataContract cedc)
        {
            return Channel.BTWAppRCVerklaringenAdd(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.BTWAppTypes.WPBTW BTWAppRCVerklaringenRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            return Channel.BTWAppRCVerklaringenRemove(cfrdc);
        }

        public EY.com.eBook.RuleEngine.AY2009.BTWAppTypes.WPBTW BTWAppRCVerklaringenUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBTWAppRCVerklaringDataContract cedc)
        {
            return Channel.BTWAppRCVerklaringenUpdate(cedc);
        }

        #endregion

        #endregion



        #region RVIntrestenApp

        public EY.com.eBook.RuleEngine.AY2009.RVIntrestenAppTypes.WPRVIntresten RVIntrestenAppGetData(CriteriaFileDataContract cfdc)
        {
            return Channel.RVIntrestenAppGetData(cfdc);
        }


        public EY.com.eBook.RuleEngine.AY2009.RVIntrestenAppTypes.WPRVIntresten RVIntrestenAppCalculate(CriteriaFileDataContract cfdc)
        {
            return Channel.RVIntrestenAppCalculate(cfdc);
        }

        #region Entity validation


        public List<FileMessageDataContract> RVIntrestenAppIntrestItemValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaRVIntrestenAppIntrestItemDataContract cedc)
        {
            return Channel.RVIntrestenAppIntrestItemValidate(cedc);
        }



        public List<FileMessageDataContract> RVIntrestenAppAdjustmentValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaRVIntrestenAppAdjustmentDataContract cedc)
        {
            return Channel.RVIntrestenAppAdjustmentValidate(cedc);
        }



        public List<FileMessageDataContract> RVIntrestenAppVerchilItemValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaRVIntrestenAppVerchilItemDataContract cedc)
        {
            return Channel.RVIntrestenAppVerchilItemValidate(cedc);
        }



        #endregion



        #region Add/Remove


        public EY.com.eBook.RuleEngine.AY2009.RVIntrestenAppTypes.WPRVIntresten RVIntrestenAppIntrestenUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaRVIntrestenAppIntrestItemDataContract cedc)
        {
            return Channel.RVIntrestenAppIntrestenUpdate(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.RVIntrestenAppTypes.WPRVIntresten RVIntrestenAppAdjustmentsAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaRVIntrestenAppAdjustmentDataContract cedc)
        {
            return Channel.RVIntrestenAppAdjustmentsAdd(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.RVIntrestenAppTypes.WPRVIntresten RVIntrestenAppAdjustmentsRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            return Channel.RVIntrestenAppAdjustmentsRemove(cfrdc);
        }

        public EY.com.eBook.RuleEngine.AY2009.RVIntrestenAppTypes.WPRVIntresten RVIntrestenAppAdjustmentsUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaRVIntrestenAppAdjustmentDataContract cedc)
        {
            return Channel.RVIntrestenAppAdjustmentsUpdate(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.RVIntrestenAppTypes.WPRVIntresten RVIntrestenAppVerschillenAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaRVIntrestenAppVerchilItemDataContract cedc)
        {
            return Channel.RVIntrestenAppVerschillenAdd(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.RVIntrestenAppTypes.WPRVIntresten RVIntrestenAppVerschillenRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            return Channel.RVIntrestenAppVerschillenRemove(cfrdc);
        }

        public EY.com.eBook.RuleEngine.AY2009.RVIntrestenAppTypes.WPRVIntresten RVIntrestenAppVerschillenUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaRVIntrestenAppVerchilItemDataContract cedc)
        {
            return Channel.RVIntrestenAppVerschillenUpdate(cedc);
        }

        #endregion

        #endregion



        #region DubieuzeDebiteurenApp

        public EY.com.eBook.RuleEngine.AY2009.DubieuzeDebiteurenAppTypes.DubieuzeDebiteuren DubieuzeDebiteurenAppGetData(CriteriaFileDataContract cfdc)
        {
            return Channel.DubieuzeDebiteurenAppGetData(cfdc);
        }


        public EY.com.eBook.RuleEngine.AY2009.DubieuzeDebiteurenAppTypes.DubieuzeDebiteuren DubieuzeDebiteurenAppCalculate(CriteriaFileDataContract cfdc)
        {
            return Channel.DubieuzeDebiteurenAppCalculate(cfdc);
        }

        #region Entity validation


        public List<FileMessageDataContract> DubieuzeDebiteurenAppDubieuzeDebiteurTValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaDubieuzeDebiteurenAppDubieuzeDebiteurTDataContract cedc)
        {
            return Channel.DubieuzeDebiteurenAppDubieuzeDebiteurTValidate(cedc);
        }



        public List<FileMessageDataContract> DubieuzeDebiteurenAppDubieuzeDebiteurWValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaDubieuzeDebiteurenAppDubieuzeDebiteurWDataContract cedc)
        {
            return Channel.DubieuzeDebiteurenAppDubieuzeDebiteurWValidate(cedc);
        }



        public List<FileMessageDataContract> DubieuzeDebiteurenAppDubieuzeDebiteurNValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaDubieuzeDebiteurenAppDubieuzeDebiteurNDataContract cedc)
        {
            return Channel.DubieuzeDebiteurenAppDubieuzeDebiteurNValidate(cedc);
        }



        public List<FileMessageDataContract> DubieuzeDebiteurenAppDubieuzeDebiteurHValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaDubieuzeDebiteurenAppDubieuzeDebiteurHDataContract cedc)
        {
            return Channel.DubieuzeDebiteurenAppDubieuzeDebiteurHValidate(cedc);
        }



        #endregion



        #region Add/Remove


        public EY.com.eBook.RuleEngine.AY2009.DubieuzeDebiteurenAppTypes.DubieuzeDebiteuren DubieuzeDebiteurenAppToestandEindVorigBoekjaarAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaDubieuzeDebiteurenAppDubieuzeDebiteurTDataContract cedc)
        {
            return Channel.DubieuzeDebiteurenAppToestandEindVorigBoekjaarAdd(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.DubieuzeDebiteurenAppTypes.DubieuzeDebiteuren DubieuzeDebiteurenAppToestandEindVorigBoekjaarRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            return Channel.DubieuzeDebiteurenAppToestandEindVorigBoekjaarRemove(cfrdc);
        }

        public EY.com.eBook.RuleEngine.AY2009.DubieuzeDebiteurenAppTypes.DubieuzeDebiteuren DubieuzeDebiteurenAppToestandEindVorigBoekjaarUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaDubieuzeDebiteurenAppDubieuzeDebiteurTDataContract cedc)
        {
            return Channel.DubieuzeDebiteurenAppToestandEindVorigBoekjaarUpdate(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.DubieuzeDebiteurenAppTypes.DubieuzeDebiteuren DubieuzeDebiteurenAppWijzigingBestaandeTijdensBoekjaarAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaDubieuzeDebiteurenAppDubieuzeDebiteurWDataContract cedc)
        {
            return Channel.DubieuzeDebiteurenAppWijzigingBestaandeTijdensBoekjaarAdd(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.DubieuzeDebiteurenAppTypes.DubieuzeDebiteuren DubieuzeDebiteurenAppWijzigingBestaandeTijdensBoekjaarRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            return Channel.DubieuzeDebiteurenAppWijzigingBestaandeTijdensBoekjaarRemove(cfrdc);
        }

        public EY.com.eBook.RuleEngine.AY2009.DubieuzeDebiteurenAppTypes.DubieuzeDebiteuren DubieuzeDebiteurenAppWijzigingBestaandeTijdensBoekjaarUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaDubieuzeDebiteurenAppDubieuzeDebiteurWDataContract cedc)
        {
            return Channel.DubieuzeDebiteurenAppWijzigingBestaandeTijdensBoekjaarUpdate(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.DubieuzeDebiteurenAppTypes.DubieuzeDebiteuren DubieuzeDebiteurenAppNieuweTijdensBoekjaarAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaDubieuzeDebiteurenAppDubieuzeDebiteurNDataContract cedc)
        {
            return Channel.DubieuzeDebiteurenAppNieuweTijdensBoekjaarAdd(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.DubieuzeDebiteurenAppTypes.DubieuzeDebiteuren DubieuzeDebiteurenAppNieuweTijdensBoekjaarRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            return Channel.DubieuzeDebiteurenAppNieuweTijdensBoekjaarRemove(cfrdc);
        }

        public EY.com.eBook.RuleEngine.AY2009.DubieuzeDebiteurenAppTypes.DubieuzeDebiteuren DubieuzeDebiteurenAppNieuweTijdensBoekjaarUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaDubieuzeDebiteurenAppDubieuzeDebiteurNDataContract cedc)
        {
            return Channel.DubieuzeDebiteurenAppNieuweTijdensBoekjaarUpdate(cedc);
        }

        #endregion

        #endregion



        #region VerliesHandelsdebiteurenApp

        public EY.com.eBook.RuleEngine.AY2009.VerliesHandelsdebiteurenAppTypes.VerliesHandelsDebiteuren VerliesHandelsdebiteurenAppGetData(CriteriaFileDataContract cfdc)
        {
            return Channel.VerliesHandelsdebiteurenAppGetData(cfdc);
        }


        public EY.com.eBook.RuleEngine.AY2009.VerliesHandelsdebiteurenAppTypes.VerliesHandelsDebiteuren VerliesHandelsdebiteurenAppCalculate(CriteriaFileDataContract cfdc)
        {
            return Channel.VerliesHandelsdebiteurenAppCalculate(cfdc);
        }

        #region Entity validation


        public List<FileMessageDataContract> VerliesHandelsdebiteurenAppVerliesHandelsDebiteurValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVerliesHandelsdebiteurenAppVerliesHandelsDebiteurDataContract cedc)
        {
            return Channel.VerliesHandelsdebiteurenAppVerliesHandelsDebiteurValidate(cedc);
        }



        #endregion



        #region Add/Remove


        public EY.com.eBook.RuleEngine.AY2009.VerliesHandelsdebiteurenAppTypes.VerliesHandelsDebiteuren VerliesHandelsdebiteurenAppVerliesHDAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVerliesHandelsdebiteurenAppVerliesHandelsDebiteurDataContract cedc)
        {
            return Channel.VerliesHandelsdebiteurenAppVerliesHDAdd(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.VerliesHandelsdebiteurenAppTypes.VerliesHandelsDebiteuren VerliesHandelsdebiteurenAppVerliesHDRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            return Channel.VerliesHandelsdebiteurenAppVerliesHDRemove(cfrdc);
        }

        public EY.com.eBook.RuleEngine.AY2009.VerliesHandelsdebiteurenAppTypes.VerliesHandelsDebiteuren VerliesHandelsdebiteurenAppVerliesHDUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVerliesHandelsdebiteurenAppVerliesHandelsDebiteurDataContract cedc)
        {
            return Channel.VerliesHandelsdebiteurenAppVerliesHDUpdate(cedc);
        }

        #endregion

        #endregion



        #region VoorzieningenApp

        public EY.com.eBook.RuleEngine.AY2009.VoorzieningenAppTypes.Voorzieningen VoorzieningenAppGetData(CriteriaFileDataContract cfdc)
        {
            return Channel.VoorzieningenAppGetData(cfdc);
        }


        public EY.com.eBook.RuleEngine.AY2009.VoorzieningenAppTypes.Voorzieningen VoorzieningenAppCalculate(CriteriaFileDataContract cfdc)
        {
            return Channel.VoorzieningenAppCalculate(cfdc);
        }

        #region Entity validation


        public List<FileMessageDataContract> VoorzieningenAppVoorzieningenTValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoorzieningenAppVoorzieningenTDataContract cedc)
        {
            return Channel.VoorzieningenAppVoorzieningenTValidate(cedc);
        }



        public List<FileMessageDataContract> VoorzieningenAppVoorzieningenWValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoorzieningenAppVoorzieningenWDataContract cedc)
        {
            return Channel.VoorzieningenAppVoorzieningenWValidate(cedc);
        }



        public List<FileMessageDataContract> VoorzieningenAppVoorzieningenNValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoorzieningenAppVoorzieningenNDataContract cedc)
        {
            return Channel.VoorzieningenAppVoorzieningenNValidate(cedc);
        }



        public List<FileMessageDataContract> VoorzieningenAppVoorzieningenHValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoorzieningenAppVoorzieningenHDataContract cedc)
        {
            return Channel.VoorzieningenAppVoorzieningenHValidate(cedc);
        }



        #endregion



        #region Add/Remove


        public EY.com.eBook.RuleEngine.AY2009.VoorzieningenAppTypes.Voorzieningen VoorzieningenAppToestandEindVorigBoekjaarAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoorzieningenAppVoorzieningenTDataContract cedc)
        {
            return Channel.VoorzieningenAppToestandEindVorigBoekjaarAdd(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.VoorzieningenAppTypes.Voorzieningen VoorzieningenAppToestandEindVorigBoekjaarRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            return Channel.VoorzieningenAppToestandEindVorigBoekjaarRemove(cfrdc);
        }

        public EY.com.eBook.RuleEngine.AY2009.VoorzieningenAppTypes.Voorzieningen VoorzieningenAppToestandEindVorigBoekjaarUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoorzieningenAppVoorzieningenTDataContract cedc)
        {
            return Channel.VoorzieningenAppToestandEindVorigBoekjaarUpdate(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.VoorzieningenAppTypes.Voorzieningen VoorzieningenAppWijzigingBestaandeTijdensBoekjaarAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoorzieningenAppVoorzieningenWDataContract cedc)
        {
            return Channel.VoorzieningenAppWijzigingBestaandeTijdensBoekjaarAdd(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.VoorzieningenAppTypes.Voorzieningen VoorzieningenAppWijzigingBestaandeTijdensBoekjaarRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            return Channel.VoorzieningenAppWijzigingBestaandeTijdensBoekjaarRemove(cfrdc);
        }

        public EY.com.eBook.RuleEngine.AY2009.VoorzieningenAppTypes.Voorzieningen VoorzieningenAppWijzigingBestaandeTijdensBoekjaarUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoorzieningenAppVoorzieningenWDataContract cedc)
        {
            return Channel.VoorzieningenAppWijzigingBestaandeTijdensBoekjaarUpdate(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.VoorzieningenAppTypes.Voorzieningen VoorzieningenAppNieuweTijdensBoekjaarAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoorzieningenAppVoorzieningenNDataContract cedc)
        {
            return Channel.VoorzieningenAppNieuweTijdensBoekjaarAdd(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.VoorzieningenAppTypes.Voorzieningen VoorzieningenAppNieuweTijdensBoekjaarRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            return Channel.VoorzieningenAppNieuweTijdensBoekjaarRemove(cfrdc);
        }

        public EY.com.eBook.RuleEngine.AY2009.VoorzieningenAppTypes.Voorzieningen VoorzieningenAppNieuweTijdensBoekjaarUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoorzieningenAppVoorzieningenNDataContract cedc)
        {
            return Channel.VoorzieningenAppNieuweTijdensBoekjaarUpdate(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.VoorzieningenAppTypes.Voorzieningen VoorzieningenAppToestandEindHuidigBoekjaarAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoorzieningenAppVoorzieningenHDataContract cedc)
        {
            return Channel.VoorzieningenAppToestandEindHuidigBoekjaarAdd(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.VoorzieningenAppTypes.Voorzieningen VoorzieningenAppToestandEindHuidigBoekjaarRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            return Channel.VoorzieningenAppToestandEindHuidigBoekjaarRemove(cfrdc);
        }

        public EY.com.eBook.RuleEngine.AY2009.VoorzieningenAppTypes.Voorzieningen VoorzieningenAppToestandEindHuidigBoekjaarUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoorzieningenAppVoorzieningenHDataContract cedc)
        {
            return Channel.VoorzieningenAppToestandEindHuidigBoekjaarUpdate(cedc);
        }

        #endregion

        #endregion



        #region BelastingenApp

        public EY.com.eBook.RuleEngine.AY2009.BelastingenAppTypes.WPBelastingen BelastingenAppGetData(CriteriaFileDataContract cfdc)
        {
            return Channel.BelastingenAppGetData(cfdc);
        }


        public EY.com.eBook.RuleEngine.AY2009.BelastingenAppTypes.WPBelastingen BelastingenAppCalculate(CriteriaFileDataContract cfdc)
        {
            return Channel.BelastingenAppCalculate(cfdc);
        }

        #region Entity validation


        public List<FileMessageDataContract> BelastingenAppAdjustmentValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBelastingenAppAdjustmentDataContract cedc)
        {
            return Channel.BelastingenAppAdjustmentValidate(cedc);
        }



        public List<FileMessageDataContract> BelastingenAppBelastingValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBelastingenAppBelastingDataContract cedc)
        {
            return Channel.BelastingenAppBelastingValidate(cedc);
        }



        public List<FileMessageDataContract> BelastingenAppKostItemValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBelastingenAppKostItemDataContract cedc)
        {
            return Channel.BelastingenAppKostItemValidate(cedc);
        }



        #endregion



        #region Add/Remove


        public EY.com.eBook.RuleEngine.AY2009.BelastingenAppTypes.WPBelastingen BelastingenAppAdjustmentsAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBelastingenAppAdjustmentDataContract cedc)
        {
            return Channel.BelastingenAppAdjustmentsAdd(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.BelastingenAppTypes.WPBelastingen BelastingenAppAdjustmentsRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            return Channel.BelastingenAppAdjustmentsRemove(cfrdc);
        }

        public EY.com.eBook.RuleEngine.AY2009.BelastingenAppTypes.WPBelastingen BelastingenAppAdjustmentsUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBelastingenAppAdjustmentDataContract cedc)
        {
            return Channel.BelastingenAppAdjustmentsUpdate(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.BelastingenAppTypes.WPBelastingen BelastingenAppBelastingenUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBelastingenAppBelastingDataContract cedc)
        {
            return Channel.BelastingenAppBelastingenUpdate(cedc);
        }

        #endregion

        #endregion



        #region BelasteReservesApp

        public EY.com.eBook.RuleEngine.AY2009.BelasteReservesAppTypes.BelasteReserves BelasteReservesAppGetData(CriteriaFileDataContract cfdc)
        {
            return Channel.BelasteReservesAppGetData(cfdc);
        }


        public EY.com.eBook.RuleEngine.AY2009.BelasteReservesAppTypes.BelasteReserves BelasteReservesAppCalculate(CriteriaFileDataContract cfdc)
        {
            return Channel.BelasteReservesAppCalculate(cfdc);
        }

        #region Entity validation


        public List<FileMessageDataContract> BelasteReservesAppBelasteReservesItemValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBelasteReservesAppBelasteReservesItemDataContract cedc)
        {
            return Channel.BelasteReservesAppBelasteReservesItemValidate(cedc);
        }



        public List<FileMessageDataContract> BelasteReservesAppAanpassingBTReservesItemValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBelasteReservesAppAanpassingBTReservesItemDataContract cedc)
        {
            return Channel.BelasteReservesAppAanpassingBTReservesItemValidate(cedc);
        }



        #endregion



        #region Add/Remove


        public EY.com.eBook.RuleEngine.AY2009.BelasteReservesAppTypes.BelasteReserves BelasteReservesAppAllBelasteReservesAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBelasteReservesAppBelasteReservesItemDataContract cedc)
        {
            return Channel.BelasteReservesAppAllBelasteReservesAdd(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.BelasteReservesAppTypes.BelasteReserves BelasteReservesAppAllBelasteReservesRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            return Channel.BelasteReservesAppAllBelasteReservesRemove(cfrdc);
        }

        public EY.com.eBook.RuleEngine.AY2009.BelasteReservesAppTypes.BelasteReserves BelasteReservesAppAllBelasteReservesUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBelasteReservesAppBelasteReservesItemDataContract cedc)
        {
            return Channel.BelasteReservesAppAllBelasteReservesUpdate(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.BelasteReservesAppTypes.BelasteReserves BelasteReservesAppAanpassingBTReservesAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBelasteReservesAppAanpassingBTReservesItemDataContract cedc)
        {
            return Channel.BelasteReservesAppAanpassingBTReservesAdd(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.BelasteReservesAppTypes.BelasteReserves BelasteReservesAppAanpassingBTReservesRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            return Channel.BelasteReservesAppAanpassingBTReservesRemove(cfrdc);
        }

        public EY.com.eBook.RuleEngine.AY2009.BelasteReservesAppTypes.BelasteReserves BelasteReservesAppAanpassingBTReservesUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBelasteReservesAppAanpassingBTReservesItemDataContract cedc)
        {
            return Channel.BelasteReservesAppAanpassingBTReservesUpdate(cedc);
        }

        #endregion

        #endregion



        #region BelastingVrijeReservesApp

        public EY.com.eBook.RuleEngine.AY2009.BelastingVrijeReservesAppTypes.BelastingVrijeReserves BelastingVrijeReservesAppGetData(CriteriaFileDataContract cfdc)
        {
            return Channel.BelastingVrijeReservesAppGetData(cfdc);
        }


        public EY.com.eBook.RuleEngine.AY2009.BelastingVrijeReservesAppTypes.BelastingVrijeReserves BelastingVrijeReservesAppCalculate(CriteriaFileDataContract cfdc)
        {
            return Channel.BelastingVrijeReservesAppCalculate(cfdc);
        }

        #region Entity validation


        public List<FileMessageDataContract> BelastingVrijeReservesAppDetailValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBelastingVrijeReservesAppDetailDataContract cedc)
        {
            return Channel.BelastingVrijeReservesAppDetailValidate(cedc);
        }



        #endregion



        #region Add/Remove


        public EY.com.eBook.RuleEngine.AY2009.BelastingVrijeReservesAppTypes.BelastingVrijeReserves BelastingVrijeReservesAppVrijgesteldeReservesUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBelastingVrijeReservesAppDetailDataContract cedc)
        {
            return Channel.BelastingVrijeReservesAppVrijgesteldeReservesUpdate(cedc);
        }

        #endregion

        #endregion



        #region BepalingBelastbareBasisApp

        public EY.com.eBook.RuleEngine.AY2009.BepalingBelastbareBasisAppTypes.BepalingBasis BepalingBelastbareBasisAppGetData(CriteriaFileDataContract cfdc)
        {
            return Channel.BepalingBelastbareBasisAppGetData(cfdc);
        }


        public EY.com.eBook.RuleEngine.AY2009.BepalingBelastbareBasisAppTypes.BepalingBasis BepalingBelastbareBasisAppCalculate(CriteriaFileDataContract cfdc)
        {
            return Channel.BepalingBelastbareBasisAppCalculate(cfdc);
        }

        #region Entity validation


        public List<FileMessageDataContract> BepalingBelastbareBasisAppGeneralValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBepalingBelastbareBasisAppGeneralDataContract cedc)
        {
            return Channel.BepalingBelastbareBasisAppGeneralValidate(cedc);
        }



        public List<FileMessageDataContract> BepalingBelastbareBasisAppBelasteReservesItemValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBepalingBelastbareBasisAppBelasteReservesItemDataContract cedc)
        {
            return Channel.BepalingBelastbareBasisAppBelasteReservesItemValidate(cedc);
        }



        public List<FileMessageDataContract> BepalingBelastbareBasisAppVerworpenUitgavenItemValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBepalingBelastbareBasisAppVerworpenUitgavenItemDataContract cedc)
        {
            return Channel.BepalingBelastbareBasisAppVerworpenUitgavenItemValidate(cedc);
        }



        public List<FileMessageDataContract> BepalingBelastbareBasisAppBelastbareBestZonderAftrekItemValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBepalingBelastbareBasisAppBelastbareBestZonderAftrekItemDataContract cedc)
        {
            return Channel.BepalingBelastbareBasisAppBelastbareBestZonderAftrekItemValidate(cedc);
        }



        public List<FileMessageDataContract> BepalingBelastbareBasisAppOpdelingNaarOorsprongItemValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBepalingBelastbareBasisAppOpdelingNaarOorsprongItemDataContract cedc)
        {
            return Channel.BepalingBelastbareBasisAppOpdelingNaarOorsprongItemValidate(cedc);
        }



        public List<FileMessageDataContract> BepalingBelastbareBasisAppFiscaleAftrekItemValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBepalingBelastbareBasisAppFiscaleAftrekItemDataContract cedc)
        {
            return Channel.BepalingBelastbareBasisAppFiscaleAftrekItemValidate(cedc);
        }



        #endregion



        #region Add/Remove


        public EY.com.eBook.RuleEngine.AY2009.BepalingBelastbareBasisAppTypes.BepalingBasis BepalingBelastbareBasisAppOverzichtUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBepalingBelastbareBasisAppGeneralDataContract cedc)
        {
            return Channel.BepalingBelastbareBasisAppOverzichtUpdate(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.BepalingBelastbareBasisAppTypes.BepalingBasis BepalingBelastbareBasisAppBelasteReservesUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBepalingBelastbareBasisAppBelasteReservesItemDataContract cedc)
        {
            return Channel.BepalingBelastbareBasisAppBelasteReservesUpdate(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.BepalingBelastbareBasisAppTypes.BepalingBasis BepalingBelastbareBasisAppVerworpenUitgavenUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBepalingBelastbareBasisAppVerworpenUitgavenItemDataContract cedc)
        {
            return Channel.BepalingBelastbareBasisAppVerworpenUitgavenUpdate(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.BepalingBelastbareBasisAppTypes.BepalingBasis BepalingBelastbareBasisAppBelastbareBestZonderAftrekUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBepalingBelastbareBasisAppBelastbareBestZonderAftrekItemDataContract cedc)
        {
            return Channel.BepalingBelastbareBasisAppBelastbareBestZonderAftrekUpdate(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.BepalingBelastbareBasisAppTypes.BepalingBasis BepalingBelastbareBasisAppOpdelingNaarOorsprongUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBepalingBelastbareBasisAppOpdelingNaarOorsprongItemDataContract cedc)
        {
            return Channel.BepalingBelastbareBasisAppOpdelingNaarOorsprongUpdate(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.BepalingBelastbareBasisAppTypes.BepalingBasis BepalingBelastbareBasisAppFiscaleAftrekUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBepalingBelastbareBasisAppFiscaleAftrekItemDataContract cedc)
        {
            return Channel.BepalingBelastbareBasisAppFiscaleAftrekUpdate(cedc);
        }

        #endregion

        #endregion



        #region DBIApp

        public EY.com.eBook.RuleEngine.AY2009.DBIAppTypes.DBIWorksheet DBIAppGetData(CriteriaFileDataContract cfdc)
        {
            return Channel.DBIAppGetData(cfdc);
        }


        public EY.com.eBook.RuleEngine.AY2009.DBIAppTypes.DBIWorksheet DBIAppCalculate(CriteriaFileDataContract cfdc)
        {
            return Channel.DBIAppCalculate(cfdc);
        }

        #region Entity validation


        public List<FileMessageDataContract> DBIAppParticipatieItemValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaDBIAppParticipatieItemDataContract cedc)
        {
            return Channel.DBIAppParticipatieItemValidate(cedc);
        }



        public List<FileMessageDataContract> DBIAppDividendItemValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaDBIAppDividendItemDataContract cedc)
        {
            return Channel.DBIAppDividendItemValidate(cedc);
        }



        #endregion



        #region Add/Remove


        public EY.com.eBook.RuleEngine.AY2009.DBIAppTypes.DBIWorksheet DBIAppParticipationsAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaDBIAppParticipatieItemDataContract cedc)
        {
            return Channel.DBIAppParticipationsAdd(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.DBIAppTypes.DBIWorksheet DBIAppParticipationsRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            return Channel.DBIAppParticipationsRemove(cfrdc);
        }

        public EY.com.eBook.RuleEngine.AY2009.DBIAppTypes.DBIWorksheet DBIAppParticipationsUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaDBIAppParticipatieItemDataContract cedc)
        {
            return Channel.DBIAppParticipationsUpdate(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.DBIAppTypes.DBIWorksheet DBIAppDividendsAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaDBIAppDividendItemDataContract cedc)
        {
            return Channel.DBIAppDividendsAdd(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.DBIAppTypes.DBIWorksheet DBIAppDividendsRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            return Channel.DBIAppDividendsRemove(cfrdc);
        }

        public EY.com.eBook.RuleEngine.AY2009.DBIAppTypes.DBIWorksheet DBIAppDividendsUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaDBIAppDividendItemDataContract cedc)
        {
            return Channel.DBIAppDividendsUpdate(cedc);
        }

        #endregion

        #endregion



        #region RisicoKapitaalApp

        public EY.com.eBook.RuleEngine.AY2009.RisicoKapitaalAppTypes.WPRisicokapitaal RisicoKapitaalAppGetData(CriteriaFileDataContract cfdc)
        {
            return Channel.RisicoKapitaalAppGetData(cfdc);
        }


        public EY.com.eBook.RuleEngine.AY2009.RisicoKapitaalAppTypes.WPRisicokapitaal RisicoKapitaalAppCalculate(CriteriaFileDataContract cfdc)
        {
            return Channel.RisicoKapitaalAppCalculate(cfdc);
        }

        #region Entity validation


        public List<FileMessageDataContract> RisicoKapitaalAppFicheItemValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaRisicoKapitaalAppFicheItemDataContract cedc)
        {
            return Channel.RisicoKapitaalAppFicheItemValidate(cedc);
        }



        #endregion



        #region Add/Remove


        public EY.com.eBook.RuleEngine.AY2009.RisicoKapitaalAppTypes.WPRisicokapitaal RisicoKapitaalAppFicheUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaRisicoKapitaalAppFicheItemDataContract cedc)
        {
            return Channel.RisicoKapitaalAppFicheUpdate(cedc);
        }

        #endregion

        #endregion



        #region TransferTaxCreditsApp

        public EY.com.eBook.RuleEngine.AY2009.TransferTaxCreditsAppTypes.TransferTaxCredits TransferTaxCreditsAppGetData(CriteriaFileDataContract cfdc)
        {
            return Channel.TransferTaxCreditsAppGetData(cfdc);
        }


        public EY.com.eBook.RuleEngine.AY2009.TransferTaxCreditsAppTypes.TransferTaxCredits TransferTaxCreditsAppCalculate(CriteriaFileDataContract cfdc)
        {
            return Channel.TransferTaxCreditsAppCalculate(cfdc);
        }

        #region Entity validation


        public List<FileMessageDataContract> TransferTaxCreditsAppFiscaalVerliesValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaTransferTaxCreditsAppFiscaalVerliesDataContract cedc)
        {
            return Channel.TransferTaxCreditsAppFiscaalVerliesValidate(cedc);
        }



        public List<FileMessageDataContract> TransferTaxCreditsAppNIDItemValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaTransferTaxCreditsAppNIDItemDataContract cedc)
        {
            return Channel.TransferTaxCreditsAppNIDItemValidate(cedc);
        }



        public List<FileMessageDataContract> TransferTaxCreditsAppDBIItemValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaTransferTaxCreditsAppDBIItemDataContract cedc)
        {
            return Channel.TransferTaxCreditsAppDBIItemValidate(cedc);
        }



        public List<FileMessageDataContract> TransferTaxCreditsAppInvestItemValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaTransferTaxCreditsAppInvestItemDataContract cedc)
        {
            return Channel.TransferTaxCreditsAppInvestItemValidate(cedc);
        }



        public List<FileMessageDataContract> TransferTaxCreditsAppBelastingkredietItemValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaTransferTaxCreditsAppBelastingkredietItemDataContract cedc)
        {
            return Channel.TransferTaxCreditsAppBelastingkredietItemValidate(cedc);
        }



        public List<FileMessageDataContract> TransferTaxCreditsAppAanpassingItemValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaTransferTaxCreditsAppAanpassingItemDataContract cedc)
        {
            return Channel.TransferTaxCreditsAppAanpassingItemValidate(cedc);
        }



        #endregion



        #region Add/Remove


        public EY.com.eBook.RuleEngine.AY2009.TransferTaxCreditsAppTypes.TransferTaxCredits TransferTaxCreditsAppFiscaleVerliezenAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaTransferTaxCreditsAppFiscaalVerliesDataContract cedc)
        {
            return Channel.TransferTaxCreditsAppFiscaleVerliezenAdd(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.TransferTaxCreditsAppTypes.TransferTaxCredits TransferTaxCreditsAppFiscaleVerliezenRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            return Channel.TransferTaxCreditsAppFiscaleVerliezenRemove(cfrdc);
        }

        public EY.com.eBook.RuleEngine.AY2009.TransferTaxCreditsAppTypes.TransferTaxCredits TransferTaxCreditsAppFiscaleVerliezenUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaTransferTaxCreditsAppFiscaalVerliesDataContract cedc)
        {
            return Channel.TransferTaxCreditsAppFiscaleVerliezenUpdate(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.TransferTaxCreditsAppTypes.TransferTaxCredits TransferTaxCreditsAppInvestAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaTransferTaxCreditsAppInvestItemDataContract cedc)
        {
            return Channel.TransferTaxCreditsAppInvestAdd(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.TransferTaxCreditsAppTypes.TransferTaxCredits TransferTaxCreditsAppInvestRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            return Channel.TransferTaxCreditsAppInvestRemove(cfrdc);
        }

        public EY.com.eBook.RuleEngine.AY2009.TransferTaxCreditsAppTypes.TransferTaxCredits TransferTaxCreditsAppInvestUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaTransferTaxCreditsAppInvestItemDataContract cedc)
        {
            return Channel.TransferTaxCreditsAppInvestUpdate(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.TransferTaxCreditsAppTypes.TransferTaxCredits TransferTaxCreditsAppBelastingkredietAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaTransferTaxCreditsAppBelastingkredietItemDataContract cedc)
        {
            return Channel.TransferTaxCreditsAppBelastingkredietAdd(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.TransferTaxCreditsAppTypes.TransferTaxCredits TransferTaxCreditsAppBelastingkredietRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            return Channel.TransferTaxCreditsAppBelastingkredietRemove(cfrdc);
        }

        public EY.com.eBook.RuleEngine.AY2009.TransferTaxCreditsAppTypes.TransferTaxCredits TransferTaxCreditsAppBelastingkredietUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaTransferTaxCreditsAppBelastingkredietItemDataContract cedc)
        {
            return Channel.TransferTaxCreditsAppBelastingkredietUpdate(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.TransferTaxCreditsAppTypes.TransferTaxCredits TransferTaxCreditsAppAanpassingenAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaTransferTaxCreditsAppAanpassingItemDataContract cedc)
        {
            return Channel.TransferTaxCreditsAppAanpassingenAdd(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.TransferTaxCreditsAppTypes.TransferTaxCredits TransferTaxCreditsAppAanpassingenRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            return Channel.TransferTaxCreditsAppAanpassingenRemove(cfrdc);
        }

        public EY.com.eBook.RuleEngine.AY2009.TransferTaxCreditsAppTypes.TransferTaxCredits TransferTaxCreditsAppAanpassingenUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaTransferTaxCreditsAppAanpassingItemDataContract cedc)
        {
            return Channel.TransferTaxCreditsAppAanpassingenUpdate(cedc);
        }

        #endregion

        #endregion



        #region BerekeningVenBApp

        public EY.com.eBook.RuleEngine.AY2009.BerekeningVenBAppTypes.BerekeningVenB BerekeningVenBAppGetData(CriteriaFileDataContract cfdc)
        {
            return Channel.BerekeningVenBAppGetData(cfdc);
        }


        public EY.com.eBook.RuleEngine.AY2009.BerekeningVenBAppTypes.BerekeningVenB BerekeningVenBAppCalculate(CriteriaFileDataContract cfdc)
        {
            return Channel.BerekeningVenBAppCalculate(cfdc);
        }

        #region Entity validation


        public List<FileMessageDataContract> BerekeningVenBAppGeneralValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBerekeningVenBAppGeneralDataContract cedc)
        {
            return Channel.BerekeningVenBAppGeneralValidate(cedc);
        }



        public List<FileMessageDataContract> BerekeningVenBAppBelasteReservesItemValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBerekeningVenBAppBelasteReservesItemDataContract cedc)
        {
            return Channel.BerekeningVenBAppBelasteReservesItemValidate(cedc);
        }



        public List<FileMessageDataContract> BerekeningVenBAppVerworpenUitgavenItemValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBerekeningVenBAppVerworpenUitgavenItemDataContract cedc)
        {
            return Channel.BerekeningVenBAppVerworpenUitgavenItemValidate(cedc);
        }



        public List<FileMessageDataContract> BerekeningVenBAppBelastbareBestZonderAftrekItemValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBerekeningVenBAppBelastbareBestZonderAftrekItemDataContract cedc)
        {
            return Channel.BerekeningVenBAppBelastbareBestZonderAftrekItemValidate(cedc);
        }



        public List<FileMessageDataContract> BerekeningVenBAppOpdelingNaarOorsprongItemValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBerekeningVenBAppOpdelingNaarOorsprongItemDataContract cedc)
        {
            return Channel.BerekeningVenBAppOpdelingNaarOorsprongItemValidate(cedc);
        }



        public List<FileMessageDataContract> BerekeningVenBAppFiscaleAftrekItemValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBerekeningVenBAppFiscaleAftrekItemDataContract cedc)
        {
            return Channel.BerekeningVenBAppFiscaleAftrekItemValidate(cedc);
        }



        public List<FileMessageDataContract> BerekeningVenBAppTariefItemValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBerekeningVenBAppTariefItemDataContract cedc)
        {
            return Channel.BerekeningVenBAppTariefItemValidate(cedc);
        }



        public List<FileMessageDataContract> BerekeningVenBAppBelastingsBerekeningItemValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBerekeningVenBAppBelastingsBerekeningItemDataContract cedc)
        {
            return Channel.BerekeningVenBAppBelastingsBerekeningItemValidate(cedc);
        }



        public List<FileMessageDataContract> BerekeningVenBAppBelastingsBerekeningItem2Validate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBerekeningVenBAppBelastingsBerekeningItem2DataContract cedc)
        {
            return Channel.BerekeningVenBAppBelastingsBerekeningItem2Validate(cedc);
        }



        #endregion



        #region Add/Remove


        public EY.com.eBook.RuleEngine.AY2009.BerekeningVenBAppTypes.BerekeningVenB BerekeningVenBAppOverzichtUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBerekeningVenBAppGeneralDataContract cedc)
        {
            return Channel.BerekeningVenBAppOverzichtUpdate(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.BerekeningVenBAppTypes.BerekeningVenB BerekeningVenBAppBelasteReservesUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBerekeningVenBAppBelasteReservesItemDataContract cedc)
        {
            return Channel.BerekeningVenBAppBelasteReservesUpdate(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.BerekeningVenBAppTypes.BerekeningVenB BerekeningVenBAppVerworpenUitgavenUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBerekeningVenBAppVerworpenUitgavenItemDataContract cedc)
        {
            return Channel.BerekeningVenBAppVerworpenUitgavenUpdate(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.BerekeningVenBAppTypes.BerekeningVenB BerekeningVenBAppBelastbareBestZonderAftrekUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBerekeningVenBAppBelastbareBestZonderAftrekItemDataContract cedc)
        {
            return Channel.BerekeningVenBAppBelastbareBestZonderAftrekUpdate(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.BerekeningVenBAppTypes.BerekeningVenB BerekeningVenBAppOpdelingNaarOorsprongUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBerekeningVenBAppOpdelingNaarOorsprongItemDataContract cedc)
        {
            return Channel.BerekeningVenBAppOpdelingNaarOorsprongUpdate(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.BerekeningVenBAppTypes.BerekeningVenB BerekeningVenBAppFiscaleAftrekUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBerekeningVenBAppFiscaleAftrekItemDataContract cedc)
        {
            return Channel.BerekeningVenBAppFiscaleAftrekUpdate(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.BerekeningVenBAppTypes.BerekeningVenB BerekeningVenBAppTariefUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBerekeningVenBAppTariefItemDataContract cedc)
        {
            return Channel.BerekeningVenBAppTariefUpdate(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.BerekeningVenBAppTypes.BerekeningVenB BerekeningVenBAppBelastingsBerekeningUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBerekeningVenBAppBelastingsBerekeningItemDataContract cedc)
        {
            return Channel.BerekeningVenBAppBelastingsBerekeningUpdate(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.BerekeningVenBAppTypes.BerekeningVenB BerekeningVenBAppBelastingsBerekening2Update(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBerekeningVenBAppBelastingsBerekeningItem2DataContract cedc)
        {
            return Channel.BerekeningVenBAppBelastingsBerekening2Update(cedc);
        }

        #endregion

        #endregion



        #region AangifteVenBApp

        public EY.com.eBook.RuleEngine.AY2009.AangifteVenBAppTypes.AangifteVenB AangifteVenBAppGetData(CriteriaFileDataContract cfdc)
        {
            return Channel.AangifteVenBAppGetData(cfdc);
        }


        public EY.com.eBook.RuleEngine.AY2009.AangifteVenBAppTypes.AangifteVenB AangifteVenBAppCalculate(CriteriaFileDataContract cfdc)
        {
            return Channel.AangifteVenBAppCalculate(cfdc);
        }

        #region Entity validation


        public List<FileMessageDataContract> AangifteVenBAppReserveEntityValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaAangifteVenBAppReserveEntityDataContract cedc)
        {
            return Channel.AangifteVenBAppReserveEntityValidate(cedc);
        }



        public List<FileMessageDataContract> AangifteVenBAppVrijgesteldeReserveEntityValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaAangifteVenBAppVrijgesteldeReserveEntityDataContract cedc)
        {
            return Channel.AangifteVenBAppVrijgesteldeReserveEntityValidate(cedc);
        }



        public List<FileMessageDataContract> AangifteVenBAppVerworpenUitgavenEntityValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaAangifteVenBAppVerworpenUitgavenEntityDataContract cedc)
        {
            return Channel.AangifteVenBAppVerworpenUitgavenEntityValidate(cedc);
        }



        public List<FileMessageDataContract> AangifteVenBAppUitgekeerdeDividendenEntityValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaAangifteVenBAppUitgekeerdeDividendenEntityDataContract cedc)
        {
            return Channel.AangifteVenBAppUitgekeerdeDividendenEntityValidate(cedc);
        }



        public List<FileMessageDataContract> AangifteVenBAppWinstEntityValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaAangifteVenBAppWinstEntityDataContract cedc)
        {
            return Channel.AangifteVenBAppWinstEntityValidate(cedc);
        }



        public List<FileMessageDataContract> AangifteVenBAppAfzonderlijkeAanslagenEntityValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaAangifteVenBAppAfzonderlijkeAanslagenEntityDataContract cedc)
        {
            return Channel.AangifteVenBAppAfzonderlijkeAanslagenEntityValidate(cedc);
        }



        public List<FileMessageDataContract> AangifteVenBAppDiamantEntityValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaAangifteVenBAppDiamantEntityDataContract cedc)
        {
            return Channel.AangifteVenBAppDiamantEntityValidate(cedc);
        }



        public List<FileMessageDataContract> AangifteVenBAppDbiEntityValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaAangifteVenBAppDbiEntityDataContract cedc)
        {
            return Channel.AangifteVenBAppDbiEntityValidate(cedc);
        }



        public List<FileMessageDataContract> AangifteVenBAppOverdrachtDBIAftrekEntityValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaAangifteVenBAppOverdrachtDBIAftrekEntityDataContract cedc)
        {
            return Channel.AangifteVenBAppOverdrachtDBIAftrekEntityValidate(cedc);
        }



        public List<FileMessageDataContract> AangifteVenBAppOverdrachtNIDEntityValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaAangifteVenBAppOverdrachtNIDEntityDataContract cedc)
        {
            return Channel.AangifteVenBAppOverdrachtNIDEntityValidate(cedc);
        }



        public List<FileMessageDataContract> AangifteVenBAppCompenseerbareVerliezenEntityValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaAangifteVenBAppCompenseerbareVerliezenEntityDataContract cedc)
        {
            return Channel.AangifteVenBAppCompenseerbareVerliezenEntityValidate(cedc);
        }



        public List<FileMessageDataContract> AangifteVenBAppTariefEntityValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaAangifteVenBAppTariefEntityDataContract cedc)
        {
            return Channel.AangifteVenBAppTariefEntityValidate(cedc);
        }



        public List<FileMessageDataContract> AangifteVenBAppVoorafBetalingEntityValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaAangifteVenBAppVoorafBetalingEntityDataContract cedc)
        {
            return Channel.AangifteVenBAppVoorafBetalingEntityValidate(cedc);
        }



        public List<FileMessageDataContract> AangifteVenBAppVerrekenbareVoorheffingEntityValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaAangifteVenBAppVerrekenbareVoorheffingEntityDataContract cedc)
        {
            return Channel.AangifteVenBAppVerrekenbareVoorheffingEntityValidate(cedc);
        }



        public List<FileMessageDataContract> AangifteVenBAppTaxShelterEntityValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaAangifteVenBAppTaxShelterEntityDataContract cedc)
        {
            return Channel.AangifteVenBAppTaxShelterEntityValidate(cedc);
        }



        public List<FileMessageDataContract> AangifteVenBAppGrootteVennootschapeEntityValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaAangifteVenBAppGrootteVennootschapeEntityDataContract cedc)
        {
            return Channel.AangifteVenBAppGrootteVennootschapeEntityValidate(cedc);
        }



        public List<FileMessageDataContract> AangifteVenBAppOpgavenEntityValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaAangifteVenBAppOpgavenEntityDataContract cedc)
        {
            return Channel.AangifteVenBAppOpgavenEntityValidate(cedc);
        }



        public List<FileMessageDataContract> AangifteVenBAppBankEntityValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaAangifteVenBAppBankEntityDataContract cedc)
        {
            return Channel.AangifteVenBAppBankEntityValidate(cedc);
        }



        #endregion



        #region Add/Remove


        public EY.com.eBook.RuleEngine.AY2009.AangifteVenBAppTypes.AangifteVenB AangifteVenBAppReservesUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaAangifteVenBAppReserveEntityDataContract cedc)
        {
            return Channel.AangifteVenBAppReservesUpdate(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.AangifteVenBAppTypes.AangifteVenB AangifteVenBAppVrijgesteldeReserveUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaAangifteVenBAppVrijgesteldeReserveEntityDataContract cedc)
        {
            return Channel.AangifteVenBAppVrijgesteldeReserveUpdate(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.AangifteVenBAppTypes.AangifteVenB AangifteVenBAppVerworpenUitgavenUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaAangifteVenBAppVerworpenUitgavenEntityDataContract cedc)
        {
            return Channel.AangifteVenBAppVerworpenUitgavenUpdate(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.AangifteVenBAppTypes.AangifteVenB AangifteVenBAppUitgekeerdeDividendenUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaAangifteVenBAppUitgekeerdeDividendenEntityDataContract cedc)
        {
            return Channel.AangifteVenBAppUitgekeerdeDividendenUpdate(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.AangifteVenBAppTypes.AangifteVenB AangifteVenBAppUiteenzettingWinstUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaAangifteVenBAppWinstEntityDataContract cedc)
        {
            return Channel.AangifteVenBAppUiteenzettingWinstUpdate(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.AangifteVenBAppTypes.AangifteVenB AangifteVenBAppAfzonderlijkeAanslagenUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaAangifteVenBAppAfzonderlijkeAanslagenEntityDataContract cedc)
        {
            return Channel.AangifteVenBAppAfzonderlijkeAanslagenUpdate(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.AangifteVenBAppTypes.AangifteVenB AangifteVenBAppDiamantUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaAangifteVenBAppDiamantEntityDataContract cedc)
        {
            return Channel.AangifteVenBAppDiamantUpdate(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.AangifteVenBAppTypes.AangifteVenB AangifteVenBAppDBIUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaAangifteVenBAppDbiEntityDataContract cedc)
        {
            return Channel.AangifteVenBAppDBIUpdate(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.AangifteVenBAppTypes.AangifteVenB AangifteVenBAppOverdrachtDBIAftrekUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaAangifteVenBAppOverdrachtDBIAftrekEntityDataContract cedc)
        {
            return Channel.AangifteVenBAppOverdrachtDBIAftrekUpdate(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.AangifteVenBAppTypes.AangifteVenB AangifteVenBAppOverdrachtNIDUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaAangifteVenBAppOverdrachtNIDEntityDataContract cedc)
        {
            return Channel.AangifteVenBAppOverdrachtNIDUpdate(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.AangifteVenBAppTypes.AangifteVenB AangifteVenBAppCompenseerbareVerliezenUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaAangifteVenBAppCompenseerbareVerliezenEntityDataContract cedc)
        {
            return Channel.AangifteVenBAppCompenseerbareVerliezenUpdate(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.AangifteVenBAppTypes.AangifteVenB AangifteVenBAppTariefBelastingUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaAangifteVenBAppTariefEntityDataContract cedc)
        {
            return Channel.AangifteVenBAppTariefBelastingUpdate(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.AangifteVenBAppTypes.AangifteVenB AangifteVenBAppVoorafBetalingenUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaAangifteVenBAppVoorafBetalingEntityDataContract cedc)
        {
            return Channel.AangifteVenBAppVoorafBetalingenUpdate(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.AangifteVenBAppTypes.AangifteVenB AangifteVenBAppVerrekenbareVoorheffingUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaAangifteVenBAppVerrekenbareVoorheffingEntityDataContract cedc)
        {
            return Channel.AangifteVenBAppVerrekenbareVoorheffingUpdate(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.AangifteVenBAppTypes.AangifteVenB AangifteVenBAppTaxShelterUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaAangifteVenBAppTaxShelterEntityDataContract cedc)
        {
            return Channel.AangifteVenBAppTaxShelterUpdate(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.AangifteVenBAppTypes.AangifteVenB AangifteVenBAppGrootteVennootschapUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaAangifteVenBAppGrootteVennootschapeEntityDataContract cedc)
        {
            return Channel.AangifteVenBAppGrootteVennootschapUpdate(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.AangifteVenBAppTypes.AangifteVenB AangifteVenBAppOpgaveBijlagenUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaAangifteVenBAppOpgavenEntityDataContract cedc)
        {
            return Channel.AangifteVenBAppOpgaveBijlagenUpdate(cedc);
        }

        public EY.com.eBook.RuleEngine.AY2009.AangifteVenBAppTypes.AangifteVenB AangifteVenBAppBankUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaAangifteVenBAppBankEntityDataContract cedc)
        {
            return Channel.AangifteVenBAppBankUpdate(cedc);
        }

        #endregion

        #endregion



        #endregion
      
        #endregion
    }
}

