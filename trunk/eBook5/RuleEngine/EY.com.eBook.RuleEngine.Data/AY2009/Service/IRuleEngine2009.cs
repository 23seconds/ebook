using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using EY.com.eBook.RuleEngine.Data;
using EY.com.eBook.Core.Data;
using EY.com.eBook.API.Contracts.Data;

namespace EY.com.eBook.RuleEngine
{
    
    [ServiceContract]
    public interface IRuleEngine2009
    {
        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        decimal? GetTaxCalculation(CriteriaFileDataContract cfdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        void ReCalculate(CriteriaFileDataContract cfdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        void ImportPrevious(CriteriaFileDataContract cfdc);
        #region Interface



        #region FacturatieApp
        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.FacturatieAppTypes.Facturatie FacturatieAppGetData(CriteriaFileDataContract cfdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.FacturatieAppTypes.Facturatie FacturatieAppCalculate(CriteriaFileDataContract cfdc);

        #region Entity validation

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> FacturatieAppFacturatieKlantValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaFacturatieAppFacturatieKlantDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> FacturatieAppFacturatieLeverancierCNValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaFacturatieAppFacturatieLeverancierCNDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> FacturatieAppFacturatieLeverancierValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaFacturatieAppFacturatieLeverancierDataContract cedc);



        #endregion



        #region Add/Remove


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.FacturatieAppTypes.Facturatie FacturatieAppOpTeMakenFacturenAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaFacturatieAppFacturatieKlantDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.FacturatieAppTypes.Facturatie FacturatieAppOpTeMakenFacturenRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.FacturatieAppTypes.Facturatie FacturatieAppOpTeMakenFacturenUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaFacturatieAppFacturatieKlantDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.FacturatieAppTypes.Facturatie FacturatieAppTeOntvangenCNAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaFacturatieAppFacturatieLeverancierCNDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.FacturatieAppTypes.Facturatie FacturatieAppTeOntvangenCNRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.FacturatieAppTypes.Facturatie FacturatieAppTeOntvangenCNUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaFacturatieAppFacturatieLeverancierCNDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.FacturatieAppTypes.Facturatie FacturatieAppTeOntvangenFacturenAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaFacturatieAppFacturatieLeverancierDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.FacturatieAppTypes.Facturatie FacturatieAppTeOntvangenFacturenRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.FacturatieAppTypes.Facturatie FacturatieAppTeOntvangenFacturenUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaFacturatieAppFacturatieLeverancierDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.FacturatieAppTypes.Facturatie FacturatieAppOpTeMakenCNAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaFacturatieAppFacturatieKlantDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.FacturatieAppTypes.Facturatie FacturatieAppOpTeMakenCNRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.FacturatieAppTypes.Facturatie FacturatieAppOpTeMakenCNUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaFacturatieAppFacturatieKlantDataContract cedc);

        #endregion

        #endregion



        #region OverlopendeRekeningenApp
        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.OverlopendeRekeningenAppTypes.OverlopendeRekeningen OverlopendeRekeningenAppGetData(CriteriaFileDataContract cfdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.OverlopendeRekeningenAppTypes.OverlopendeRekeningen OverlopendeRekeningenAppCalculate(CriteriaFileDataContract cfdc);

        #region Entity validation

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> OverlopendeRekeningenAppOverTeDragenKostValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaOverlopendeRekeningenAppOverTeDragenKostDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> OverlopendeRekeningenAppVerkregenOpbrengstValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaOverlopendeRekeningenAppVerkregenOpbrengstDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> OverlopendeRekeningenAppToeTeRekenenKostValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaOverlopendeRekeningenAppToeTeRekenenKostDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> OverlopendeRekeningenAppOverTeDragenOpbrengstValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaOverlopendeRekeningenAppOverTeDragenOpbrengstDataContract cedc);



        #endregion



        #region Add/Remove


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.OverlopendeRekeningenAppTypes.OverlopendeRekeningen OverlopendeRekeningenAppOverTeDragenKostenAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaOverlopendeRekeningenAppOverTeDragenKostDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.OverlopendeRekeningenAppTypes.OverlopendeRekeningen OverlopendeRekeningenAppOverTeDragenKostenRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.OverlopendeRekeningenAppTypes.OverlopendeRekeningen OverlopendeRekeningenAppOverTeDragenKostenUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaOverlopendeRekeningenAppOverTeDragenKostDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.OverlopendeRekeningenAppTypes.OverlopendeRekeningen OverlopendeRekeningenAppVerkregenOpbrenstenAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaOverlopendeRekeningenAppVerkregenOpbrengstDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.OverlopendeRekeningenAppTypes.OverlopendeRekeningen OverlopendeRekeningenAppVerkregenOpbrenstenRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.OverlopendeRekeningenAppTypes.OverlopendeRekeningen OverlopendeRekeningenAppVerkregenOpbrenstenUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaOverlopendeRekeningenAppVerkregenOpbrengstDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.OverlopendeRekeningenAppTypes.OverlopendeRekeningen OverlopendeRekeningenAppToeTeRekenenKostenAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaOverlopendeRekeningenAppToeTeRekenenKostDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.OverlopendeRekeningenAppTypes.OverlopendeRekeningen OverlopendeRekeningenAppToeTeRekenenKostenRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.OverlopendeRekeningenAppTypes.OverlopendeRekeningen OverlopendeRekeningenAppToeTeRekenenKostenUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaOverlopendeRekeningenAppToeTeRekenenKostDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.OverlopendeRekeningenAppTypes.OverlopendeRekeningen OverlopendeRekeningenAppOverTeDragenOpbrengstenAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaOverlopendeRekeningenAppOverTeDragenOpbrengstDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.OverlopendeRekeningenAppTypes.OverlopendeRekeningen OverlopendeRekeningenAppOverTeDragenOpbrengstenRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.OverlopendeRekeningenAppTypes.OverlopendeRekeningen OverlopendeRekeningenAppOverTeDragenOpbrengstenUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaOverlopendeRekeningenAppOverTeDragenOpbrengstDataContract cedc);

        #endregion

        #endregion



        #region VergelijkendeCijfersApp
        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.VergelijkendeCijfersAppTypes.WPVergelijkendeCijfers VergelijkendeCijfersAppGetData(CriteriaFileDataContract cfdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.VergelijkendeCijfersAppTypes.WPVergelijkendeCijfers VergelijkendeCijfersAppCalculate(CriteriaFileDataContract cfdc);

        #region Entity validation

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> VergelijkendeCijfersAppCijferBlokValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVergelijkendeCijfersAppCijferBlokDataContract cedc);



        #endregion



        #region Add/Remove


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.VergelijkendeCijfersAppTypes.WPVergelijkendeCijfers VergelijkendeCijfersAppKostenAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVergelijkendeCijfersAppCijferBlokDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.VergelijkendeCijfersAppTypes.WPVergelijkendeCijfers VergelijkendeCijfersAppKostenRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.VergelijkendeCijfersAppTypes.WPVergelijkendeCijfers VergelijkendeCijfersAppKostenUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVergelijkendeCijfersAppCijferBlokDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.VergelijkendeCijfersAppTypes.WPVergelijkendeCijfers VergelijkendeCijfersAppOpbrengstenAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVergelijkendeCijfersAppCijferBlokDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.VergelijkendeCijfersAppTypes.WPVergelijkendeCijfers VergelijkendeCijfersAppOpbrengstenRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.VergelijkendeCijfersAppTypes.WPVergelijkendeCijfers VergelijkendeCijfersAppOpbrengstenUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVergelijkendeCijfersAppCijferBlokDataContract cedc);

        #endregion

        #endregion



        #region VoorschottenApp
        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.VoorschottenAppTypes.Voorschotten VoorschottenAppGetData(CriteriaFileDataContract cfdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.VoorschottenAppTypes.Voorschotten VoorschottenAppCalculate(CriteriaFileDataContract cfdc);

        #region Entity validation

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> VoorschottenAppGegevenValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoorschottenAppGegevenDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> VoorschottenAppIntrestVoetValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoorschottenAppIntrestVoetDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> VoorschottenAppRentegevendVoorschotValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoorschottenAppRentegevendVoorschotDataContract cedc);



        #endregion



        #region Add/Remove


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.VoorschottenAppTypes.Voorschotten VoorschottenAppGegevensAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoorschottenAppGegevenDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.VoorschottenAppTypes.Voorschotten VoorschottenAppGegevensRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.VoorschottenAppTypes.Voorschotten VoorschottenAppGegevensUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoorschottenAppGegevenDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.VoorschottenAppTypes.Voorschotten VoorschottenAppInterestVoetenAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoorschottenAppIntrestVoetDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.VoorschottenAppTypes.Voorschotten VoorschottenAppInterestVoetenRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.VoorschottenAppTypes.Voorschotten VoorschottenAppInterestVoetenUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoorschottenAppIntrestVoetDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.VoorschottenAppTypes.Voorschotten VoorschottenAppRentegevendeVoorschottenAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoorschottenAppRentegevendVoorschotDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.VoorschottenAppTypes.Voorschotten VoorschottenAppRentegevendeVoorschottenRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.VoorschottenAppTypes.Voorschotten VoorschottenAppRentegevendeVoorschottenUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoorschottenAppRentegevendVoorschotDataContract cedc);

        #endregion

        #endregion



        #region ErelonenHuurApp
        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.ErelonenHuurAppTypes.ErelonenHuur ErelonenHuurAppGetData(CriteriaFileDataContract cfdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.ErelonenHuurAppTypes.ErelonenHuur ErelonenHuurAppCalculate(CriteriaFileDataContract cfdc);

        #region Entity validation

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> ErelonenHuurAppImportItemValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaErelonenHuurAppImportItemDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> ErelonenHuurAppImportedHistoryValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaErelonenHuurAppImportedHistoryDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> ErelonenHuurAppVerdelingHistoriekValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaErelonenHuurAppVerdelingHistoriekDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> ErelonenHuurAppFiche281_50Validate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaErelonenHuurAppFiche281_50DataContract cedc);



        #endregion



        #region Add/Remove


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.ErelonenHuurAppTypes.ErelonenHuur ErelonenHuurAppHistoriekAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaErelonenHuurAppVerdelingHistoriekDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.ErelonenHuurAppTypes.ErelonenHuur ErelonenHuurAppHistoriekRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.ErelonenHuurAppTypes.ErelonenHuur ErelonenHuurAppHistoriekUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaErelonenHuurAppVerdelingHistoriekDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.ErelonenHuurAppTypes.ErelonenHuur ErelonenHuurAppFiches281_50Update(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaErelonenHuurAppFiche281_50DataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.ErelonenHuurAppTypes.ErelonenHuur ErelonenHuurAppFiche281_50ColAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaErelonenHuurAppFiche281_50DataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.ErelonenHuurAppTypes.ErelonenHuur ErelonenHuurAppFiche281_50ColRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.ErelonenHuurAppTypes.ErelonenHuur ErelonenHuurAppFiche281_50ColUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaErelonenHuurAppFiche281_50DataContract cedc);

        #endregion

        #endregion



        #region VoordelenApp
        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.VoordelenAppTypes.Voordelen VoordelenAppGetData(CriteriaFileDataContract cfdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.VoordelenAppTypes.Voordelen VoordelenAppCalculate(CriteriaFileDataContract cfdc);

        #region Entity validation

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> VoordelenAppVAA_AutoValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoordelenAppVAA_AutoDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> VoordelenAppVAA_AutoCO2Validate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoordelenAppVAA_AutoCO2DataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> VoordelenAppVAA_NutsValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoordelenAppVAA_NutsDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> VoordelenAppVAA_HUURValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoordelenAppVAA_HUURDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> VoordelenAppVAA_HUURHERKValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoordelenAppVAA_HUURHERKDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> VoordelenAppVAA_PCValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoordelenAppVAA_PCDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> VoordelenAppVAA_TelefonieValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoordelenAppVAA_TelefonieDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> VoordelenAppVAA_OverigeValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoordelenAppVAA_OverigeDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> VoordelenAppVAA_Interesten_RC_BedrijfsleiderValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoordelenAppVAA_Interesten_RC_BedrijfsleiderDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> VoordelenAppVAA_Soc_Bijdr_BedrijfsleiderValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoordelenAppVAA_Soc_Bijdr_BedrijfsleiderDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> VoordelenAppVAA_Overige_NutsValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoordelenAppVAA_Overige_NutsDataContract cedc);



        #endregion



        #region Add/Remove


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardAutoAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoordelenAppVAA_AutoDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardAutoRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardAutoUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoordelenAppVAA_AutoDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardAutoCO2Add(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoordelenAppVAA_AutoCO2DataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardAutoCO2Remove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardAutoCO2Update(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoordelenAppVAA_AutoCO2DataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardNutsAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoordelenAppVAA_NutsDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardNutsRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardNutsUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoordelenAppVAA_NutsDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardHuurAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoordelenAppVAA_HUURDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardHuurRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardHuurUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoordelenAppVAA_HUURDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardHuurHerkwalificatieAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoordelenAppVAA_HUURHERKDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardHuurHerkwalificatieRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardHuurHerkwalificatieUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoordelenAppVAA_HUURHERKDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardPCAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoordelenAppVAA_PCDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardPCRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardPCUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoordelenAppVAA_PCDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardTelefonieAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoordelenAppVAA_TelefonieDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardTelefonieRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardTelefonieUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoordelenAppVAA_TelefonieDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardOverigeAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoordelenAppVAA_OverigeDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardOverigeRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardOverigeUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoordelenAppVAA_OverigeDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.VoordelenAppTypes.Voordelen VoordelenAppVoordelenInterestenRCBedrijfsleiderAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoordelenAppVAA_Interesten_RC_BedrijfsleiderDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.VoordelenAppTypes.Voordelen VoordelenAppVoordelenInterestenRCBedrijfsleiderRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.VoordelenAppTypes.Voordelen VoordelenAppVoordelenInterestenRCBedrijfsleiderUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoordelenAppVAA_Interesten_RC_BedrijfsleiderDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.VoordelenAppTypes.Voordelen VoordelenAppVoordelenSocialeBijdragenBedrijfsleiderAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoordelenAppVAA_Soc_Bijdr_BedrijfsleiderDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.VoordelenAppTypes.Voordelen VoordelenAppVoordelenSocialeBijdragenBedrijfsleiderRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.VoordelenAppTypes.Voordelen VoordelenAppVoordelenSocialeBijdragenBedrijfsleiderUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoordelenAppVAA_Soc_Bijdr_BedrijfsleiderDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.VoordelenAppTypes.Voordelen VoordelenAppVoordelenOverigeNutsAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoordelenAppVAA_Overige_NutsDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.VoordelenAppTypes.Voordelen VoordelenAppVoordelenOverigeNutsRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.VoordelenAppTypes.Voordelen VoordelenAppVoordelenOverigeNutsUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoordelenAppVAA_Overige_NutsDataContract cedc);

        #endregion

        #endregion



        #region PersoneelApp
        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.WPPersoneel PersoneelAppGetData(CriteriaFileDataContract cfdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.WPPersoneel PersoneelAppCalculate(CriteriaFileDataContract cfdc);

        #region Entity validation

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> PersoneelAppItem325Validate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaPersoneelAppItem325DataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> PersoneelAppVerschilItemValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaPersoneelAppVerschilItemDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> PersoneelAppWerknemerValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaPersoneelAppWerknemerDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> PersoneelAppAlgemeenValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaPersoneelAppAlgemeenDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> PersoneelAppVakantiegeldValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaPersoneelAppVakantiegeldDataContract cedc);



        #endregion



        #region Add/Remove


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.WPPersoneel PersoneelApp_325_20CollAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaPersoneelAppItem325DataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.WPPersoneel PersoneelApp_325_20CollRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.WPPersoneel PersoneelApp_325_20CollUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaPersoneelAppItem325DataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.WPPersoneel PersoneelAppTeVerklarenVerschil325_20Add(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaPersoneelAppVerschilItemDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.WPPersoneel PersoneelAppTeVerklarenVerschil325_20Remove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.WPPersoneel PersoneelAppTeVerklarenVerschil325_20Update(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaPersoneelAppVerschilItemDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.WPPersoneel PersoneelApp_325_10CollAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaPersoneelAppItem325DataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.WPPersoneel PersoneelApp_325_10CollRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.WPPersoneel PersoneelApp_325_10CollUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaPersoneelAppItem325DataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.WPPersoneel PersoneelAppTeVerklarenVerschil325_10Add(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaPersoneelAppVerschilItemDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.WPPersoneel PersoneelAppTeVerklarenVerschil325_10Remove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.WPPersoneel PersoneelAppTeVerklarenVerschil325_10Update(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaPersoneelAppVerschilItemDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.WPPersoneel PersoneelAppArbeidersFormUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaPersoneelAppWerknemerDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.WPPersoneel PersoneelAppBediendeFormUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaPersoneelAppWerknemerDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.WPPersoneel PersoneelAppAansluitingArbeidersAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaPersoneelAppVerschilItemDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.WPPersoneel PersoneelAppAansluitingArbeidersRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.WPPersoneel PersoneelAppAansluitingArbeidersUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaPersoneelAppVerschilItemDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.WPPersoneel PersoneelAppAansluitingBediendenAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaPersoneelAppVerschilItemDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.WPPersoneel PersoneelAppAansluitingBediendenRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.WPPersoneel PersoneelAppAansluitingBediendenUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaPersoneelAppVerschilItemDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.WPPersoneel PersoneelAppRSZCollAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaPersoneelAppAlgemeenDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.WPPersoneel PersoneelAppRSZCollRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.WPPersoneel PersoneelAppRSZCollUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaPersoneelAppAlgemeenDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.WPPersoneel PersoneelAppBVsCollAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaPersoneelAppAlgemeenDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.WPPersoneel PersoneelAppBVsCollRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.WPPersoneel PersoneelAppBVsCollUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaPersoneelAppAlgemeenDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.WPPersoneel PersoneelAppNettoCollAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaPersoneelAppAlgemeenDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.WPPersoneel PersoneelAppNettoCollRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.WPPersoneel PersoneelAppNettoCollUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaPersoneelAppAlgemeenDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.WPPersoneel PersoneelAppVakantieGeldCollUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaPersoneelAppVakantiegeldDataContract cedc);

        #endregion

        #endregion



        #region BTWApp
        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.BTWAppTypes.WPBTW BTWAppGetData(CriteriaFileDataContract cfdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.BTWAppTypes.WPBTW BTWAppCalculate(CriteriaFileDataContract cfdc);

        #region Entity validation

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> BTWAppOmzetAangifteValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBTWAppOmzetAangifteDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> BTWAppOmzetGeboektValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBTWAppOmzetGeboektDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> BTWAppOmzetVerklaringValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBTWAppOmzetVerklaringDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> BTWAppRCAangifteValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBTWAppRCAangifteDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> BTWAppRCGeboektValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBTWAppRCGeboektDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> BTWAppRCVerklaringValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBTWAppRCVerklaringDataContract cedc);



        #endregion



        #region Add/Remove


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.BTWAppTypes.WPBTW BTWAppOmzetAangiftesAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBTWAppOmzetAangifteDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.BTWAppTypes.WPBTW BTWAppOmzetAangiftesRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.BTWAppTypes.WPBTW BTWAppOmzetAangiftesUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBTWAppOmzetAangifteDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.BTWAppTypes.WPBTW BTWAppOmzettenGeboektAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBTWAppOmzetGeboektDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.BTWAppTypes.WPBTW BTWAppOmzettenGeboektRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.BTWAppTypes.WPBTW BTWAppOmzettenGeboektUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBTWAppOmzetGeboektDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.BTWAppTypes.WPBTW BTWAppOmzetVerklaringenAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBTWAppOmzetVerklaringDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.BTWAppTypes.WPBTW BTWAppOmzetVerklaringenRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.BTWAppTypes.WPBTW BTWAppOmzetVerklaringenUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBTWAppOmzetVerklaringDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.BTWAppTypes.WPBTW BTWAppRCAangiftesAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBTWAppRCAangifteDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.BTWAppTypes.WPBTW BTWAppRCAangiftesRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.BTWAppTypes.WPBTW BTWAppRCAangiftesUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBTWAppRCAangifteDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.BTWAppTypes.WPBTW BTWAppRCenGeboektAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBTWAppRCGeboektDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.BTWAppTypes.WPBTW BTWAppRCenGeboektRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.BTWAppTypes.WPBTW BTWAppRCenGeboektUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBTWAppRCGeboektDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.BTWAppTypes.WPBTW BTWAppRCVerklaringenAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBTWAppRCVerklaringDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.BTWAppTypes.WPBTW BTWAppRCVerklaringenRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.BTWAppTypes.WPBTW BTWAppRCVerklaringenUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBTWAppRCVerklaringDataContract cedc);

        #endregion

        #endregion



        #region RVIntrestenApp
        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.RVIntrestenAppTypes.WPRVIntresten RVIntrestenAppGetData(CriteriaFileDataContract cfdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.RVIntrestenAppTypes.WPRVIntresten RVIntrestenAppCalculate(CriteriaFileDataContract cfdc);

        #region Entity validation

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> RVIntrestenAppIntrestItemValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaRVIntrestenAppIntrestItemDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> RVIntrestenAppAdjustmentValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaRVIntrestenAppAdjustmentDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> RVIntrestenAppVerchilItemValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaRVIntrestenAppVerchilItemDataContract cedc);



        #endregion



        #region Add/Remove


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.RVIntrestenAppTypes.WPRVIntresten RVIntrestenAppIntrestenUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaRVIntrestenAppIntrestItemDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.RVIntrestenAppTypes.WPRVIntresten RVIntrestenAppAdjustmentsAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaRVIntrestenAppAdjustmentDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.RVIntrestenAppTypes.WPRVIntresten RVIntrestenAppAdjustmentsRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.RVIntrestenAppTypes.WPRVIntresten RVIntrestenAppAdjustmentsUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaRVIntrestenAppAdjustmentDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.RVIntrestenAppTypes.WPRVIntresten RVIntrestenAppVerschillenAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaRVIntrestenAppVerchilItemDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.RVIntrestenAppTypes.WPRVIntresten RVIntrestenAppVerschillenRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.RVIntrestenAppTypes.WPRVIntresten RVIntrestenAppVerschillenUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaRVIntrestenAppVerchilItemDataContract cedc);

        #endregion

        #endregion



        #region DubieuzeDebiteurenApp
        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.DubieuzeDebiteurenAppTypes.DubieuzeDebiteuren DubieuzeDebiteurenAppGetData(CriteriaFileDataContract cfdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.DubieuzeDebiteurenAppTypes.DubieuzeDebiteuren DubieuzeDebiteurenAppCalculate(CriteriaFileDataContract cfdc);

        #region Entity validation

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> DubieuzeDebiteurenAppDubieuzeDebiteurTValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaDubieuzeDebiteurenAppDubieuzeDebiteurTDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> DubieuzeDebiteurenAppDubieuzeDebiteurWValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaDubieuzeDebiteurenAppDubieuzeDebiteurWDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> DubieuzeDebiteurenAppDubieuzeDebiteurNValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaDubieuzeDebiteurenAppDubieuzeDebiteurNDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> DubieuzeDebiteurenAppDubieuzeDebiteurHValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaDubieuzeDebiteurenAppDubieuzeDebiteurHDataContract cedc);



        #endregion



        #region Add/Remove


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.DubieuzeDebiteurenAppTypes.DubieuzeDebiteuren DubieuzeDebiteurenAppToestandEindVorigBoekjaarAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaDubieuzeDebiteurenAppDubieuzeDebiteurTDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.DubieuzeDebiteurenAppTypes.DubieuzeDebiteuren DubieuzeDebiteurenAppToestandEindVorigBoekjaarRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.DubieuzeDebiteurenAppTypes.DubieuzeDebiteuren DubieuzeDebiteurenAppToestandEindVorigBoekjaarUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaDubieuzeDebiteurenAppDubieuzeDebiteurTDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.DubieuzeDebiteurenAppTypes.DubieuzeDebiteuren DubieuzeDebiteurenAppWijzigingBestaandeTijdensBoekjaarAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaDubieuzeDebiteurenAppDubieuzeDebiteurWDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.DubieuzeDebiteurenAppTypes.DubieuzeDebiteuren DubieuzeDebiteurenAppWijzigingBestaandeTijdensBoekjaarRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.DubieuzeDebiteurenAppTypes.DubieuzeDebiteuren DubieuzeDebiteurenAppWijzigingBestaandeTijdensBoekjaarUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaDubieuzeDebiteurenAppDubieuzeDebiteurWDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.DubieuzeDebiteurenAppTypes.DubieuzeDebiteuren DubieuzeDebiteurenAppNieuweTijdensBoekjaarAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaDubieuzeDebiteurenAppDubieuzeDebiteurNDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.DubieuzeDebiteurenAppTypes.DubieuzeDebiteuren DubieuzeDebiteurenAppNieuweTijdensBoekjaarRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.DubieuzeDebiteurenAppTypes.DubieuzeDebiteuren DubieuzeDebiteurenAppNieuweTijdensBoekjaarUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaDubieuzeDebiteurenAppDubieuzeDebiteurNDataContract cedc);

        #endregion

        #endregion



        #region VerliesHandelsdebiteurenApp
        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.VerliesHandelsdebiteurenAppTypes.VerliesHandelsDebiteuren VerliesHandelsdebiteurenAppGetData(CriteriaFileDataContract cfdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.VerliesHandelsdebiteurenAppTypes.VerliesHandelsDebiteuren VerliesHandelsdebiteurenAppCalculate(CriteriaFileDataContract cfdc);

        #region Entity validation

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> VerliesHandelsdebiteurenAppVerliesHandelsDebiteurValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVerliesHandelsdebiteurenAppVerliesHandelsDebiteurDataContract cedc);



        #endregion



        #region Add/Remove


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.VerliesHandelsdebiteurenAppTypes.VerliesHandelsDebiteuren VerliesHandelsdebiteurenAppVerliesHDAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVerliesHandelsdebiteurenAppVerliesHandelsDebiteurDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.VerliesHandelsdebiteurenAppTypes.VerliesHandelsDebiteuren VerliesHandelsdebiteurenAppVerliesHDRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.VerliesHandelsdebiteurenAppTypes.VerliesHandelsDebiteuren VerliesHandelsdebiteurenAppVerliesHDUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVerliesHandelsdebiteurenAppVerliesHandelsDebiteurDataContract cedc);

        #endregion

        #endregion



        #region VoorzieningenApp
        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.VoorzieningenAppTypes.Voorzieningen VoorzieningenAppGetData(CriteriaFileDataContract cfdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.VoorzieningenAppTypes.Voorzieningen VoorzieningenAppCalculate(CriteriaFileDataContract cfdc);

        #region Entity validation

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> VoorzieningenAppVoorzieningenTValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoorzieningenAppVoorzieningenTDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> VoorzieningenAppVoorzieningenWValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoorzieningenAppVoorzieningenWDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> VoorzieningenAppVoorzieningenNValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoorzieningenAppVoorzieningenNDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> VoorzieningenAppVoorzieningenHValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoorzieningenAppVoorzieningenHDataContract cedc);



        #endregion



        #region Add/Remove


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.VoorzieningenAppTypes.Voorzieningen VoorzieningenAppToestandEindVorigBoekjaarAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoorzieningenAppVoorzieningenTDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.VoorzieningenAppTypes.Voorzieningen VoorzieningenAppToestandEindVorigBoekjaarRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.VoorzieningenAppTypes.Voorzieningen VoorzieningenAppToestandEindVorigBoekjaarUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoorzieningenAppVoorzieningenTDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.VoorzieningenAppTypes.Voorzieningen VoorzieningenAppWijzigingBestaandeTijdensBoekjaarAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoorzieningenAppVoorzieningenWDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.VoorzieningenAppTypes.Voorzieningen VoorzieningenAppWijzigingBestaandeTijdensBoekjaarRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.VoorzieningenAppTypes.Voorzieningen VoorzieningenAppWijzigingBestaandeTijdensBoekjaarUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoorzieningenAppVoorzieningenWDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.VoorzieningenAppTypes.Voorzieningen VoorzieningenAppNieuweTijdensBoekjaarAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoorzieningenAppVoorzieningenNDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.VoorzieningenAppTypes.Voorzieningen VoorzieningenAppNieuweTijdensBoekjaarRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.VoorzieningenAppTypes.Voorzieningen VoorzieningenAppNieuweTijdensBoekjaarUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoorzieningenAppVoorzieningenNDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.VoorzieningenAppTypes.Voorzieningen VoorzieningenAppToestandEindHuidigBoekjaarAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoorzieningenAppVoorzieningenHDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.VoorzieningenAppTypes.Voorzieningen VoorzieningenAppToestandEindHuidigBoekjaarRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.VoorzieningenAppTypes.Voorzieningen VoorzieningenAppToestandEindHuidigBoekjaarUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoorzieningenAppVoorzieningenHDataContract cedc);

        #endregion

        #endregion



        #region BelastingenApp
        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.BelastingenAppTypes.WPBelastingen BelastingenAppGetData(CriteriaFileDataContract cfdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.BelastingenAppTypes.WPBelastingen BelastingenAppCalculate(CriteriaFileDataContract cfdc);

        #region Entity validation

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> BelastingenAppAdjustmentValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBelastingenAppAdjustmentDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> BelastingenAppBelastingValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBelastingenAppBelastingDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> BelastingenAppKostItemValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBelastingenAppKostItemDataContract cedc);



        #endregion



        #region Add/Remove


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.BelastingenAppTypes.WPBelastingen BelastingenAppAdjustmentsAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBelastingenAppAdjustmentDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.BelastingenAppTypes.WPBelastingen BelastingenAppAdjustmentsRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.BelastingenAppTypes.WPBelastingen BelastingenAppAdjustmentsUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBelastingenAppAdjustmentDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.BelastingenAppTypes.WPBelastingen BelastingenAppBelastingenUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBelastingenAppBelastingDataContract cedc);

        #endregion

        #endregion



        #region BelasteReservesApp
        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.BelasteReservesAppTypes.BelasteReserves BelasteReservesAppGetData(CriteriaFileDataContract cfdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.BelasteReservesAppTypes.BelasteReserves BelasteReservesAppCalculate(CriteriaFileDataContract cfdc);

        #region Entity validation

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> BelasteReservesAppBelasteReservesItemValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBelasteReservesAppBelasteReservesItemDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> BelasteReservesAppAanpassingBTReservesItemValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBelasteReservesAppAanpassingBTReservesItemDataContract cedc);



        #endregion



        #region Add/Remove


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.BelasteReservesAppTypes.BelasteReserves BelasteReservesAppAllBelasteReservesAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBelasteReservesAppBelasteReservesItemDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.BelasteReservesAppTypes.BelasteReserves BelasteReservesAppAllBelasteReservesRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.BelasteReservesAppTypes.BelasteReserves BelasteReservesAppAllBelasteReservesUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBelasteReservesAppBelasteReservesItemDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.BelasteReservesAppTypes.BelasteReserves BelasteReservesAppAanpassingBTReservesAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBelasteReservesAppAanpassingBTReservesItemDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.BelasteReservesAppTypes.BelasteReserves BelasteReservesAppAanpassingBTReservesRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.BelasteReservesAppTypes.BelasteReserves BelasteReservesAppAanpassingBTReservesUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBelasteReservesAppAanpassingBTReservesItemDataContract cedc);

        #endregion

        #endregion



        #region BelastingVrijeReservesApp
        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.BelastingVrijeReservesAppTypes.BelastingVrijeReserves BelastingVrijeReservesAppGetData(CriteriaFileDataContract cfdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.BelastingVrijeReservesAppTypes.BelastingVrijeReserves BelastingVrijeReservesAppCalculate(CriteriaFileDataContract cfdc);

        #region Entity validation

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> BelastingVrijeReservesAppDetailValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBelastingVrijeReservesAppDetailDataContract cedc);



        #endregion



        #region Add/Remove


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.BelastingVrijeReservesAppTypes.BelastingVrijeReserves BelastingVrijeReservesAppVrijgesteldeReservesUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBelastingVrijeReservesAppDetailDataContract cedc);

        #endregion

        #endregion



        #region BepalingBelastbareBasisApp
        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.BepalingBelastbareBasisAppTypes.BepalingBasis BepalingBelastbareBasisAppGetData(CriteriaFileDataContract cfdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.BepalingBelastbareBasisAppTypes.BepalingBasis BepalingBelastbareBasisAppCalculate(CriteriaFileDataContract cfdc);

        #region Entity validation

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> BepalingBelastbareBasisAppGeneralValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBepalingBelastbareBasisAppGeneralDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> BepalingBelastbareBasisAppBelasteReservesItemValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBepalingBelastbareBasisAppBelasteReservesItemDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> BepalingBelastbareBasisAppVerworpenUitgavenItemValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBepalingBelastbareBasisAppVerworpenUitgavenItemDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> BepalingBelastbareBasisAppBelastbareBestZonderAftrekItemValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBepalingBelastbareBasisAppBelastbareBestZonderAftrekItemDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> BepalingBelastbareBasisAppOpdelingNaarOorsprongItemValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBepalingBelastbareBasisAppOpdelingNaarOorsprongItemDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> BepalingBelastbareBasisAppFiscaleAftrekItemValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBepalingBelastbareBasisAppFiscaleAftrekItemDataContract cedc);



        #endregion



        #region Add/Remove


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.BepalingBelastbareBasisAppTypes.BepalingBasis BepalingBelastbareBasisAppOverzichtUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBepalingBelastbareBasisAppGeneralDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.BepalingBelastbareBasisAppTypes.BepalingBasis BepalingBelastbareBasisAppBelasteReservesUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBepalingBelastbareBasisAppBelasteReservesItemDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.BepalingBelastbareBasisAppTypes.BepalingBasis BepalingBelastbareBasisAppVerworpenUitgavenUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBepalingBelastbareBasisAppVerworpenUitgavenItemDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.BepalingBelastbareBasisAppTypes.BepalingBasis BepalingBelastbareBasisAppBelastbareBestZonderAftrekUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBepalingBelastbareBasisAppBelastbareBestZonderAftrekItemDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.BepalingBelastbareBasisAppTypes.BepalingBasis BepalingBelastbareBasisAppOpdelingNaarOorsprongUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBepalingBelastbareBasisAppOpdelingNaarOorsprongItemDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.BepalingBelastbareBasisAppTypes.BepalingBasis BepalingBelastbareBasisAppFiscaleAftrekUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBepalingBelastbareBasisAppFiscaleAftrekItemDataContract cedc);

        #endregion

        #endregion



        #region DBIApp
        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.DBIAppTypes.DBIWorksheet DBIAppGetData(CriteriaFileDataContract cfdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.DBIAppTypes.DBIWorksheet DBIAppCalculate(CriteriaFileDataContract cfdc);

        #region Entity validation

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> DBIAppParticipatieItemValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaDBIAppParticipatieItemDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> DBIAppDividendItemValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaDBIAppDividendItemDataContract cedc);



        #endregion



        #region Add/Remove


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.DBIAppTypes.DBIWorksheet DBIAppParticipationsAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaDBIAppParticipatieItemDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.DBIAppTypes.DBIWorksheet DBIAppParticipationsRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.DBIAppTypes.DBIWorksheet DBIAppParticipationsUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaDBIAppParticipatieItemDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.DBIAppTypes.DBIWorksheet DBIAppDividendsAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaDBIAppDividendItemDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.DBIAppTypes.DBIWorksheet DBIAppDividendsRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.DBIAppTypes.DBIWorksheet DBIAppDividendsUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaDBIAppDividendItemDataContract cedc);

        #endregion

        #endregion



        #region RisicoKapitaalApp
        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.RisicoKapitaalAppTypes.WPRisicokapitaal RisicoKapitaalAppGetData(CriteriaFileDataContract cfdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.RisicoKapitaalAppTypes.WPRisicokapitaal RisicoKapitaalAppCalculate(CriteriaFileDataContract cfdc);

        #region Entity validation

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> RisicoKapitaalAppFicheItemValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaRisicoKapitaalAppFicheItemDataContract cedc);



        #endregion



        #region Add/Remove


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.RisicoKapitaalAppTypes.WPRisicokapitaal RisicoKapitaalAppFicheUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaRisicoKapitaalAppFicheItemDataContract cedc);

        #endregion

        #endregion



        #region TransferTaxCreditsApp
        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.TransferTaxCreditsAppTypes.TransferTaxCredits TransferTaxCreditsAppGetData(CriteriaFileDataContract cfdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.TransferTaxCreditsAppTypes.TransferTaxCredits TransferTaxCreditsAppCalculate(CriteriaFileDataContract cfdc);

        #region Entity validation

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> TransferTaxCreditsAppFiscaalVerliesValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaTransferTaxCreditsAppFiscaalVerliesDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> TransferTaxCreditsAppNIDItemValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaTransferTaxCreditsAppNIDItemDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> TransferTaxCreditsAppDBIItemValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaTransferTaxCreditsAppDBIItemDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> TransferTaxCreditsAppInvestItemValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaTransferTaxCreditsAppInvestItemDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> TransferTaxCreditsAppBelastingkredietItemValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaTransferTaxCreditsAppBelastingkredietItemDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> TransferTaxCreditsAppAanpassingItemValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaTransferTaxCreditsAppAanpassingItemDataContract cedc);



        #endregion



        #region Add/Remove


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.TransferTaxCreditsAppTypes.TransferTaxCredits TransferTaxCreditsAppFiscaleVerliezenAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaTransferTaxCreditsAppFiscaalVerliesDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.TransferTaxCreditsAppTypes.TransferTaxCredits TransferTaxCreditsAppFiscaleVerliezenRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.TransferTaxCreditsAppTypes.TransferTaxCredits TransferTaxCreditsAppFiscaleVerliezenUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaTransferTaxCreditsAppFiscaalVerliesDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.TransferTaxCreditsAppTypes.TransferTaxCredits TransferTaxCreditsAppInvestAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaTransferTaxCreditsAppInvestItemDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.TransferTaxCreditsAppTypes.TransferTaxCredits TransferTaxCreditsAppInvestRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.TransferTaxCreditsAppTypes.TransferTaxCredits TransferTaxCreditsAppInvestUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaTransferTaxCreditsAppInvestItemDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.TransferTaxCreditsAppTypes.TransferTaxCredits TransferTaxCreditsAppBelastingkredietAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaTransferTaxCreditsAppBelastingkredietItemDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.TransferTaxCreditsAppTypes.TransferTaxCredits TransferTaxCreditsAppBelastingkredietRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.TransferTaxCreditsAppTypes.TransferTaxCredits TransferTaxCreditsAppBelastingkredietUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaTransferTaxCreditsAppBelastingkredietItemDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.TransferTaxCreditsAppTypes.TransferTaxCredits TransferTaxCreditsAppAanpassingenAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaTransferTaxCreditsAppAanpassingItemDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.TransferTaxCreditsAppTypes.TransferTaxCredits TransferTaxCreditsAppAanpassingenRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.TransferTaxCreditsAppTypes.TransferTaxCredits TransferTaxCreditsAppAanpassingenUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaTransferTaxCreditsAppAanpassingItemDataContract cedc);

        #endregion

        #endregion



        #region BerekeningVenBApp
        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.BerekeningVenBAppTypes.BerekeningVenB BerekeningVenBAppGetData(CriteriaFileDataContract cfdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.BerekeningVenBAppTypes.BerekeningVenB BerekeningVenBAppCalculate(CriteriaFileDataContract cfdc);

        #region Entity validation

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> BerekeningVenBAppGeneralValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBerekeningVenBAppGeneralDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> BerekeningVenBAppBelasteReservesItemValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBerekeningVenBAppBelasteReservesItemDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> BerekeningVenBAppVerworpenUitgavenItemValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBerekeningVenBAppVerworpenUitgavenItemDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> BerekeningVenBAppBelastbareBestZonderAftrekItemValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBerekeningVenBAppBelastbareBestZonderAftrekItemDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> BerekeningVenBAppOpdelingNaarOorsprongItemValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBerekeningVenBAppOpdelingNaarOorsprongItemDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> BerekeningVenBAppFiscaleAftrekItemValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBerekeningVenBAppFiscaleAftrekItemDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> BerekeningVenBAppTariefItemValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBerekeningVenBAppTariefItemDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> BerekeningVenBAppBelastingsBerekeningItemValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBerekeningVenBAppBelastingsBerekeningItemDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> BerekeningVenBAppBelastingsBerekeningItem2Validate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBerekeningVenBAppBelastingsBerekeningItem2DataContract cedc);



        #endregion



        #region Add/Remove


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.BerekeningVenBAppTypes.BerekeningVenB BerekeningVenBAppOverzichtUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBerekeningVenBAppGeneralDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.BerekeningVenBAppTypes.BerekeningVenB BerekeningVenBAppBelasteReservesUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBerekeningVenBAppBelasteReservesItemDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.BerekeningVenBAppTypes.BerekeningVenB BerekeningVenBAppVerworpenUitgavenUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBerekeningVenBAppVerworpenUitgavenItemDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.BerekeningVenBAppTypes.BerekeningVenB BerekeningVenBAppBelastbareBestZonderAftrekUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBerekeningVenBAppBelastbareBestZonderAftrekItemDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.BerekeningVenBAppTypes.BerekeningVenB BerekeningVenBAppOpdelingNaarOorsprongUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBerekeningVenBAppOpdelingNaarOorsprongItemDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.BerekeningVenBAppTypes.BerekeningVenB BerekeningVenBAppFiscaleAftrekUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBerekeningVenBAppFiscaleAftrekItemDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.BerekeningVenBAppTypes.BerekeningVenB BerekeningVenBAppTariefUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBerekeningVenBAppTariefItemDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.BerekeningVenBAppTypes.BerekeningVenB BerekeningVenBAppBelastingsBerekeningUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBerekeningVenBAppBelastingsBerekeningItemDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.BerekeningVenBAppTypes.BerekeningVenB BerekeningVenBAppBelastingsBerekening2Update(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBerekeningVenBAppBelastingsBerekeningItem2DataContract cedc);

        #endregion

        #endregion



        #region AangifteVenBApp
        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.AangifteVenBAppTypes.AangifteVenB AangifteVenBAppGetData(CriteriaFileDataContract cfdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.AangifteVenBAppTypes.AangifteVenB AangifteVenBAppCalculate(CriteriaFileDataContract cfdc);

        #region Entity validation

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> AangifteVenBAppReserveEntityValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaAangifteVenBAppReserveEntityDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> AangifteVenBAppVrijgesteldeReserveEntityValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaAangifteVenBAppVrijgesteldeReserveEntityDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> AangifteVenBAppVerworpenUitgavenEntityValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaAangifteVenBAppVerworpenUitgavenEntityDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> AangifteVenBAppUitgekeerdeDividendenEntityValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaAangifteVenBAppUitgekeerdeDividendenEntityDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> AangifteVenBAppWinstEntityValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaAangifteVenBAppWinstEntityDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> AangifteVenBAppAfzonderlijkeAanslagenEntityValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaAangifteVenBAppAfzonderlijkeAanslagenEntityDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> AangifteVenBAppDiamantEntityValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaAangifteVenBAppDiamantEntityDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> AangifteVenBAppDbiEntityValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaAangifteVenBAppDbiEntityDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> AangifteVenBAppOverdrachtDBIAftrekEntityValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaAangifteVenBAppOverdrachtDBIAftrekEntityDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> AangifteVenBAppOverdrachtNIDEntityValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaAangifteVenBAppOverdrachtNIDEntityDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> AangifteVenBAppCompenseerbareVerliezenEntityValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaAangifteVenBAppCompenseerbareVerliezenEntityDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> AangifteVenBAppTariefEntityValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaAangifteVenBAppTariefEntityDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> AangifteVenBAppVoorafBetalingEntityValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaAangifteVenBAppVoorafBetalingEntityDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> AangifteVenBAppVerrekenbareVoorheffingEntityValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaAangifteVenBAppVerrekenbareVoorheffingEntityDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> AangifteVenBAppTaxShelterEntityValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaAangifteVenBAppTaxShelterEntityDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> AangifteVenBAppGrootteVennootschapeEntityValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaAangifteVenBAppGrootteVennootschapeEntityDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> AangifteVenBAppOpgavenEntityValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaAangifteVenBAppOpgavenEntityDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> AangifteVenBAppBankEntityValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaAangifteVenBAppBankEntityDataContract cedc);



        #endregion



        #region Add/Remove


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.AangifteVenBAppTypes.AangifteVenB AangifteVenBAppReservesUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaAangifteVenBAppReserveEntityDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.AangifteVenBAppTypes.AangifteVenB AangifteVenBAppVrijgesteldeReserveUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaAangifteVenBAppVrijgesteldeReserveEntityDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.AangifteVenBAppTypes.AangifteVenB AangifteVenBAppVerworpenUitgavenUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaAangifteVenBAppVerworpenUitgavenEntityDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.AangifteVenBAppTypes.AangifteVenB AangifteVenBAppUitgekeerdeDividendenUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaAangifteVenBAppUitgekeerdeDividendenEntityDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.AangifteVenBAppTypes.AangifteVenB AangifteVenBAppUiteenzettingWinstUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaAangifteVenBAppWinstEntityDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.AangifteVenBAppTypes.AangifteVenB AangifteVenBAppAfzonderlijkeAanslagenUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaAangifteVenBAppAfzonderlijkeAanslagenEntityDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.AangifteVenBAppTypes.AangifteVenB AangifteVenBAppDiamantUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaAangifteVenBAppDiamantEntityDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.AangifteVenBAppTypes.AangifteVenB AangifteVenBAppDBIUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaAangifteVenBAppDbiEntityDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.AangifteVenBAppTypes.AangifteVenB AangifteVenBAppOverdrachtDBIAftrekUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaAangifteVenBAppOverdrachtDBIAftrekEntityDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.AangifteVenBAppTypes.AangifteVenB AangifteVenBAppOverdrachtNIDUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaAangifteVenBAppOverdrachtNIDEntityDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.AangifteVenBAppTypes.AangifteVenB AangifteVenBAppCompenseerbareVerliezenUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaAangifteVenBAppCompenseerbareVerliezenEntityDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.AangifteVenBAppTypes.AangifteVenB AangifteVenBAppTariefBelastingUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaAangifteVenBAppTariefEntityDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.AangifteVenBAppTypes.AangifteVenB AangifteVenBAppVoorafBetalingenUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaAangifteVenBAppVoorafBetalingEntityDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.AangifteVenBAppTypes.AangifteVenB AangifteVenBAppVerrekenbareVoorheffingUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaAangifteVenBAppVerrekenbareVoorheffingEntityDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.AangifteVenBAppTypes.AangifteVenB AangifteVenBAppTaxShelterUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaAangifteVenBAppTaxShelterEntityDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.AangifteVenBAppTypes.AangifteVenB AangifteVenBAppGrootteVennootschapUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaAangifteVenBAppGrootteVennootschapeEntityDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.AangifteVenBAppTypes.AangifteVenB AangifteVenBAppOpgaveBijlagenUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaAangifteVenBAppOpgavenEntityDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2009.AangifteVenBAppTypes.AangifteVenB AangifteVenBAppBankUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaAangifteVenBAppBankEntityDataContract cedc);

        #endregion

        #endregion



        #endregion
      

    }

    
}
