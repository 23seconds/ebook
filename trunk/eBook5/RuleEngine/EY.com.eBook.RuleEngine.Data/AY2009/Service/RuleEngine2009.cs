using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Activation;
using EY.com.eBook.Core.EF;
using System.ServiceModel.Web;
using EY.com.eBook.RuleEngine.Core;
using EY.com.eBook.RuleEngine.Data;
using EY.com.eBook.Core.Data;
using EY.com.eBook.Core;
using EY.com.eBook.Core.EF.Write;
using EY.com.eBook.API.Contracts.Proxies;
using EY.com.eBook.Caching;
using EY.com.eBook.RuleEngine.AY2009;
using EY.com.eBook.API.Contracts.Data;

namespace EY.com.eBook.RuleEngine
{
    [EntityFrameworkServiceBehavior]
    [ServiceBehavior(IncludeExceptionDetailInFaults = true, ConcurrencyMode = ConcurrencyMode.Multiple, InstanceContextMode = InstanceContextMode.PerCall)]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class RuleEngine2009 : IRuleEngine2009
    {

        #region IRuleEngine2009 Members

        public EY.com.eBook.RuleEngine.AY2009.WorksheetSet GetSet(Guid fileId)
        {
           // CacheProxy cp = new CacheProxy();
           // cp.Open();
            CacheRepository cp = new CacheRepository();
            if (!cp.FileInCache(fileId))
            {
                throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { ErrorCode=-99, Message = "CACHE-MISS" , FileId=fileId });
            }
            WorksheetWorkSetDataContract wwsdc = cp.GetWorksheetSet(fileId);

            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData sh = new EY.com.eBook.RuleEngine.AY2009.WorksheetSetData
            { 
               Coeffs = wwsdc.Coeffs
               , DataContract = wwsdc.DataContract
               , AutoBookings = wwsdc.AutoBookings== null ? new List<BookingDataContract>() : wwsdc.AutoBookings
               , Schema = wwsdc.Schema
               , Messages = wwsdc.Messages== null ? new List<FileMessageDataContract>() : wwsdc.Messages
               , IsEmpty = wwsdc.Worksheets == null || wwsdc.Worksheets.Count==0
               , LastSave = wwsdc.Worksheets==null || wwsdc.Worksheets.Count==0 ? new DateTime(1980,1,1) : wwsdc.Worksheets.LastSave
            };
            if (wwsdc.Worksheets != null && wwsdc.Worksheets.Count == 0) wwsdc.Worksheets = null;
            sh.Initialize(wwsdc.Worksheets);
            
            
           // set.DataContainer = sh;
            return new EY.com.eBook.RuleEngine.AY2009.WorksheetSet { DataContainer = sh };
            
        }


        public void CalculateAll(EY.com.eBook.RuleEngine.AY2009.WorksheetSet ws)
        {
            ws.Calculate(null);
        }

        public void Calculate(EY.com.eBook.RuleEngine.AY2009.WorksheetSet ws, string col)
        {
            ws.Calculate(col);
        }

        public void ReCalculate(CriteriaFileDataContract cfdc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cfdc.FileId);
            set.Calculate(null);
        }

        public void ImportPrevious(CriteriaFileDataContract cfdc)
        {
            throw new NotImplementedException();
        }
        
        public decimal? GetTaxCalculation(CriteriaFileDataContract cfdc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cfdc.FileId);

            return set.GetTaxCalculation();
        }

        #region Service implementation



        #region FacturatieApp

        public EY.com.eBook.RuleEngine.AY2009.FacturatieAppTypes.Facturatie FacturatieAppGetData(CriteriaFileDataContract cfdc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cfdc.FileId);

            return set.DataContainer.FacturatieApp.Data;
        }


        public EY.com.eBook.RuleEngine.AY2009.FacturatieAppTypes.Facturatie FacturatieAppCalculate(CriteriaFileDataContract cfdc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cfdc.FileId);
            set.Calculate("FacturatieApp");

            return set.DataContainer.FacturatieApp.Data;
        }

        #region Entity validation


        public List<FileMessageDataContract> FacturatieAppFacturatieKlantValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaFacturatieAppFacturatieKlantDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            ws.FacturatieApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == FacturatieApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> FacturatieAppFacturatieLeverancierCNValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaFacturatieAppFacturatieLeverancierCNDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            ws.FacturatieApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == FacturatieApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> FacturatieAppFacturatieLeverancierValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaFacturatieAppFacturatieLeverancierDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            ws.FacturatieApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == FacturatieApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        #endregion



        #region Add/Remove


        public EY.com.eBook.RuleEngine.AY2009.FacturatieAppTypes.Facturatie FacturatieAppOpTeMakenFacturenAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaFacturatieAppFacturatieKlantDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            FacturatieApp rp = ws.FacturatieApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToOpTeMakenFacturen(cedc.Entity, ref ws);
            set.Calculate("FacturatieApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.FacturatieAppTypes.Facturatie FacturatieAppOpTeMakenFacturenRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            FacturatieApp rp = ws.FacturatieApp;
            rp.DeleteOpTeMakenFacturen(cfrdc.RowId, ref ws);
            set.Calculate("FacturatieApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.FacturatieAppTypes.Facturatie FacturatieAppOpTeMakenFacturenUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaFacturatieAppFacturatieKlantDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            FacturatieApp rp = ws.FacturatieApp;
            rp.UpdateOpTeMakenFacturen(cedc.Entity, ref ws);
            set.Calculate("FacturatieApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.FacturatieAppTypes.Facturatie FacturatieAppTeOntvangenCNAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaFacturatieAppFacturatieLeverancierCNDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            FacturatieApp rp = ws.FacturatieApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToTeOntvangenCN(cedc.Entity, ref ws);
            set.Calculate("FacturatieApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.FacturatieAppTypes.Facturatie FacturatieAppTeOntvangenCNRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            FacturatieApp rp = ws.FacturatieApp;
            rp.DeleteTeOntvangenCN(cfrdc.RowId, ref ws);
            set.Calculate("FacturatieApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.FacturatieAppTypes.Facturatie FacturatieAppTeOntvangenCNUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaFacturatieAppFacturatieLeverancierCNDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            FacturatieApp rp = ws.FacturatieApp;
            rp.UpdateTeOntvangenCN(cedc.Entity, ref ws);
            set.Calculate("FacturatieApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.FacturatieAppTypes.Facturatie FacturatieAppTeOntvangenFacturenAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaFacturatieAppFacturatieLeverancierDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            FacturatieApp rp = ws.FacturatieApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToTeOntvangenFacturen(cedc.Entity, ref ws);
            set.Calculate("FacturatieApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.FacturatieAppTypes.Facturatie FacturatieAppTeOntvangenFacturenRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            FacturatieApp rp = ws.FacturatieApp;
            rp.DeleteTeOntvangenFacturen(cfrdc.RowId, ref ws);
            set.Calculate("FacturatieApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.FacturatieAppTypes.Facturatie FacturatieAppTeOntvangenFacturenUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaFacturatieAppFacturatieLeverancierDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            FacturatieApp rp = ws.FacturatieApp;
            rp.UpdateTeOntvangenFacturen(cedc.Entity, ref ws);
            set.Calculate("FacturatieApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.FacturatieAppTypes.Facturatie FacturatieAppOpTeMakenCNAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaFacturatieAppFacturatieKlantDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            FacturatieApp rp = ws.FacturatieApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToOpTeMakenCN(cedc.Entity, ref ws);
            set.Calculate("FacturatieApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.FacturatieAppTypes.Facturatie FacturatieAppOpTeMakenCNRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            FacturatieApp rp = ws.FacturatieApp;
            rp.DeleteOpTeMakenCN(cfrdc.RowId, ref ws);
            set.Calculate("FacturatieApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.FacturatieAppTypes.Facturatie FacturatieAppOpTeMakenCNUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaFacturatieAppFacturatieKlantDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            FacturatieApp rp = ws.FacturatieApp;
            rp.UpdateOpTeMakenCN(cedc.Entity, ref ws);
            set.Calculate("FacturatieApp");
            return rp.Data;
        }

        #endregion

        #endregion



        #region OverlopendeRekeningenApp

        public EY.com.eBook.RuleEngine.AY2009.OverlopendeRekeningenAppTypes.OverlopendeRekeningen OverlopendeRekeningenAppGetData(CriteriaFileDataContract cfdc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cfdc.FileId);

            return set.DataContainer.OverlopendeRekeningenApp.Data;
        }


        public EY.com.eBook.RuleEngine.AY2009.OverlopendeRekeningenAppTypes.OverlopendeRekeningen OverlopendeRekeningenAppCalculate(CriteriaFileDataContract cfdc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cfdc.FileId);
            set.Calculate("OverlopendeRekeningenApp");

            return set.DataContainer.OverlopendeRekeningenApp.Data;
        }

        #region Entity validation


        public List<FileMessageDataContract> OverlopendeRekeningenAppOverTeDragenKostValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaOverlopendeRekeningenAppOverTeDragenKostDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            ws.OverlopendeRekeningenApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == OverlopendeRekeningenApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> OverlopendeRekeningenAppVerkregenOpbrengstValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaOverlopendeRekeningenAppVerkregenOpbrengstDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            ws.OverlopendeRekeningenApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == OverlopendeRekeningenApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> OverlopendeRekeningenAppToeTeRekenenKostValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaOverlopendeRekeningenAppToeTeRekenenKostDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            ws.OverlopendeRekeningenApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == OverlopendeRekeningenApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> OverlopendeRekeningenAppOverTeDragenOpbrengstValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaOverlopendeRekeningenAppOverTeDragenOpbrengstDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            ws.OverlopendeRekeningenApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == OverlopendeRekeningenApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        #endregion



        #region Add/Remove


        public EY.com.eBook.RuleEngine.AY2009.OverlopendeRekeningenAppTypes.OverlopendeRekeningen OverlopendeRekeningenAppOverTeDragenKostenAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaOverlopendeRekeningenAppOverTeDragenKostDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            OverlopendeRekeningenApp rp = ws.OverlopendeRekeningenApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToOverTeDragenKosten(cedc.Entity, ref ws);
            set.Calculate("OverlopendeRekeningenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.OverlopendeRekeningenAppTypes.OverlopendeRekeningen OverlopendeRekeningenAppOverTeDragenKostenRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            OverlopendeRekeningenApp rp = ws.OverlopendeRekeningenApp;
            rp.DeleteOverTeDragenKosten(cfrdc.RowId, ref ws);
            set.Calculate("OverlopendeRekeningenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.OverlopendeRekeningenAppTypes.OverlopendeRekeningen OverlopendeRekeningenAppOverTeDragenKostenUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaOverlopendeRekeningenAppOverTeDragenKostDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            OverlopendeRekeningenApp rp = ws.OverlopendeRekeningenApp;
            rp.UpdateOverTeDragenKosten(cedc.Entity, ref ws);
            set.Calculate("OverlopendeRekeningenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.OverlopendeRekeningenAppTypes.OverlopendeRekeningen OverlopendeRekeningenAppVerkregenOpbrenstenAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaOverlopendeRekeningenAppVerkregenOpbrengstDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            OverlopendeRekeningenApp rp = ws.OverlopendeRekeningenApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToVerkregenOpbrensten(cedc.Entity, ref ws);
            set.Calculate("OverlopendeRekeningenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.OverlopendeRekeningenAppTypes.OverlopendeRekeningen OverlopendeRekeningenAppVerkregenOpbrenstenRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            OverlopendeRekeningenApp rp = ws.OverlopendeRekeningenApp;
            rp.DeleteVerkregenOpbrensten(cfrdc.RowId, ref ws);
            set.Calculate("OverlopendeRekeningenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.OverlopendeRekeningenAppTypes.OverlopendeRekeningen OverlopendeRekeningenAppVerkregenOpbrenstenUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaOverlopendeRekeningenAppVerkregenOpbrengstDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            OverlopendeRekeningenApp rp = ws.OverlopendeRekeningenApp;
            rp.UpdateVerkregenOpbrensten(cedc.Entity, ref ws);
            set.Calculate("OverlopendeRekeningenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.OverlopendeRekeningenAppTypes.OverlopendeRekeningen OverlopendeRekeningenAppToeTeRekenenKostenAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaOverlopendeRekeningenAppToeTeRekenenKostDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            OverlopendeRekeningenApp rp = ws.OverlopendeRekeningenApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToToeTeRekenenKosten(cedc.Entity, ref ws);
            set.Calculate("OverlopendeRekeningenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.OverlopendeRekeningenAppTypes.OverlopendeRekeningen OverlopendeRekeningenAppToeTeRekenenKostenRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            OverlopendeRekeningenApp rp = ws.OverlopendeRekeningenApp;
            rp.DeleteToeTeRekenenKosten(cfrdc.RowId, ref ws);
            set.Calculate("OverlopendeRekeningenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.OverlopendeRekeningenAppTypes.OverlopendeRekeningen OverlopendeRekeningenAppToeTeRekenenKostenUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaOverlopendeRekeningenAppToeTeRekenenKostDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            OverlopendeRekeningenApp rp = ws.OverlopendeRekeningenApp;
            rp.UpdateToeTeRekenenKosten(cedc.Entity, ref ws);
            set.Calculate("OverlopendeRekeningenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.OverlopendeRekeningenAppTypes.OverlopendeRekeningen OverlopendeRekeningenAppOverTeDragenOpbrengstenAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaOverlopendeRekeningenAppOverTeDragenOpbrengstDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            OverlopendeRekeningenApp rp = ws.OverlopendeRekeningenApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToOverTeDragenOpbrengsten(cedc.Entity, ref ws);
            set.Calculate("OverlopendeRekeningenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.OverlopendeRekeningenAppTypes.OverlopendeRekeningen OverlopendeRekeningenAppOverTeDragenOpbrengstenRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            OverlopendeRekeningenApp rp = ws.OverlopendeRekeningenApp;
            rp.DeleteOverTeDragenOpbrengsten(cfrdc.RowId, ref ws);
            set.Calculate("OverlopendeRekeningenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.OverlopendeRekeningenAppTypes.OverlopendeRekeningen OverlopendeRekeningenAppOverTeDragenOpbrengstenUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaOverlopendeRekeningenAppOverTeDragenOpbrengstDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            OverlopendeRekeningenApp rp = ws.OverlopendeRekeningenApp;
            rp.UpdateOverTeDragenOpbrengsten(cedc.Entity, ref ws);
            set.Calculate("OverlopendeRekeningenApp");
            return rp.Data;
        }

        #endregion

        #endregion



        #region VergelijkendeCijfersApp

        public EY.com.eBook.RuleEngine.AY2009.VergelijkendeCijfersAppTypes.WPVergelijkendeCijfers VergelijkendeCijfersAppGetData(CriteriaFileDataContract cfdc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cfdc.FileId);

            return set.DataContainer.VergelijkendeCijfersApp.Data;
        }


        public EY.com.eBook.RuleEngine.AY2009.VergelijkendeCijfersAppTypes.WPVergelijkendeCijfers VergelijkendeCijfersAppCalculate(CriteriaFileDataContract cfdc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cfdc.FileId);
            set.Calculate("VergelijkendeCijfersApp");

            return set.DataContainer.VergelijkendeCijfersApp.Data;
        }

        #region Entity validation


        public List<FileMessageDataContract> VergelijkendeCijfersAppCijferBlokValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVergelijkendeCijfersAppCijferBlokDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            ws.VergelijkendeCijfersApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == VergelijkendeCijfersApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        #endregion



        #region Add/Remove


        public EY.com.eBook.RuleEngine.AY2009.VergelijkendeCijfersAppTypes.WPVergelijkendeCijfers VergelijkendeCijfersAppKostenAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVergelijkendeCijfersAppCijferBlokDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            VergelijkendeCijfersApp rp = ws.VergelijkendeCijfersApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToKosten(cedc.Entity, ref ws);
            set.Calculate("VergelijkendeCijfersApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.VergelijkendeCijfersAppTypes.WPVergelijkendeCijfers VergelijkendeCijfersAppKostenRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            VergelijkendeCijfersApp rp = ws.VergelijkendeCijfersApp;
            rp.DeleteKosten(cfrdc.RowId, ref ws);
            set.Calculate("VergelijkendeCijfersApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.VergelijkendeCijfersAppTypes.WPVergelijkendeCijfers VergelijkendeCijfersAppKostenUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVergelijkendeCijfersAppCijferBlokDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            VergelijkendeCijfersApp rp = ws.VergelijkendeCijfersApp;
            rp.UpdateKosten(cedc.Entity, ref ws);
            set.Calculate("VergelijkendeCijfersApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.VergelijkendeCijfersAppTypes.WPVergelijkendeCijfers VergelijkendeCijfersAppOpbrengstenAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVergelijkendeCijfersAppCijferBlokDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            VergelijkendeCijfersApp rp = ws.VergelijkendeCijfersApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToOpbrengsten(cedc.Entity, ref ws);
            set.Calculate("VergelijkendeCijfersApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.VergelijkendeCijfersAppTypes.WPVergelijkendeCijfers VergelijkendeCijfersAppOpbrengstenRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            VergelijkendeCijfersApp rp = ws.VergelijkendeCijfersApp;
            rp.DeleteOpbrengsten(cfrdc.RowId, ref ws);
            set.Calculate("VergelijkendeCijfersApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.VergelijkendeCijfersAppTypes.WPVergelijkendeCijfers VergelijkendeCijfersAppOpbrengstenUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVergelijkendeCijfersAppCijferBlokDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            VergelijkendeCijfersApp rp = ws.VergelijkendeCijfersApp;
            rp.UpdateOpbrengsten(cedc.Entity, ref ws);
            set.Calculate("VergelijkendeCijfersApp");
            return rp.Data;
        }

        #endregion

        #endregion



        #region VoorschottenApp

        public EY.com.eBook.RuleEngine.AY2009.VoorschottenAppTypes.Voorschotten VoorschottenAppGetData(CriteriaFileDataContract cfdc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cfdc.FileId);

            return set.DataContainer.VoorschottenApp.Data;
        }


        public EY.com.eBook.RuleEngine.AY2009.VoorschottenAppTypes.Voorschotten VoorschottenAppCalculate(CriteriaFileDataContract cfdc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cfdc.FileId);
            set.Calculate("VoorschottenApp");

            return set.DataContainer.VoorschottenApp.Data;
        }

        #region Entity validation


        public List<FileMessageDataContract> VoorschottenAppGegevenValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoorschottenAppGegevenDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            ws.VoorschottenApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == VoorschottenApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> VoorschottenAppIntrestVoetValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoorschottenAppIntrestVoetDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            ws.VoorschottenApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == VoorschottenApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> VoorschottenAppRentegevendVoorschotValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoorschottenAppRentegevendVoorschotDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            ws.VoorschottenApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == VoorschottenApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        #endregion



        #region Add/Remove


        public EY.com.eBook.RuleEngine.AY2009.VoorschottenAppTypes.Voorschotten VoorschottenAppGegevensAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoorschottenAppGegevenDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            VoorschottenApp rp = ws.VoorschottenApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToGegevens(cedc.Entity, ref ws);
            set.Calculate("VoorschottenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.VoorschottenAppTypes.Voorschotten VoorschottenAppGegevensRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            VoorschottenApp rp = ws.VoorschottenApp;
            rp.DeleteGegevens(cfrdc.RowId, ref ws);
            set.Calculate("VoorschottenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.VoorschottenAppTypes.Voorschotten VoorschottenAppGegevensUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoorschottenAppGegevenDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            VoorschottenApp rp = ws.VoorschottenApp;
            rp.UpdateGegevens(cedc.Entity, ref ws);
            set.Calculate("VoorschottenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.VoorschottenAppTypes.Voorschotten VoorschottenAppInterestVoetenAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoorschottenAppIntrestVoetDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            VoorschottenApp rp = ws.VoorschottenApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToInterestVoeten(cedc.Entity, ref ws);
            set.Calculate("VoorschottenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.VoorschottenAppTypes.Voorschotten VoorschottenAppInterestVoetenRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            VoorschottenApp rp = ws.VoorschottenApp;
            rp.DeleteInterestVoeten(cfrdc.RowId, ref ws);
            set.Calculate("VoorschottenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.VoorschottenAppTypes.Voorschotten VoorschottenAppInterestVoetenUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoorschottenAppIntrestVoetDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            VoorschottenApp rp = ws.VoorschottenApp;
            rp.UpdateInterestVoeten(cedc.Entity, ref ws);
            set.Calculate("VoorschottenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.VoorschottenAppTypes.Voorschotten VoorschottenAppRentegevendeVoorschottenAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoorschottenAppRentegevendVoorschotDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            VoorschottenApp rp = ws.VoorschottenApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToRentegevendeVoorschotten(cedc.Entity, ref ws);
            set.Calculate("VoorschottenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.VoorschottenAppTypes.Voorschotten VoorschottenAppRentegevendeVoorschottenRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            VoorschottenApp rp = ws.VoorschottenApp;
            rp.DeleteRentegevendeVoorschotten(cfrdc.RowId, ref ws);
            set.Calculate("VoorschottenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.VoorschottenAppTypes.Voorschotten VoorschottenAppRentegevendeVoorschottenUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoorschottenAppRentegevendVoorschotDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            VoorschottenApp rp = ws.VoorschottenApp;
            rp.UpdateRentegevendeVoorschotten(cedc.Entity, ref ws);
            set.Calculate("VoorschottenApp");
            return rp.Data;
        }

        #endregion

        #endregion



        #region ErelonenHuurApp

        public EY.com.eBook.RuleEngine.AY2009.ErelonenHuurAppTypes.ErelonenHuur ErelonenHuurAppGetData(CriteriaFileDataContract cfdc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cfdc.FileId);

            return set.DataContainer.ErelonenHuurApp.Data;
        }


        public EY.com.eBook.RuleEngine.AY2009.ErelonenHuurAppTypes.ErelonenHuur ErelonenHuurAppCalculate(CriteriaFileDataContract cfdc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cfdc.FileId);
            set.Calculate("ErelonenHuurApp");

            return set.DataContainer.ErelonenHuurApp.Data;
        }

        #region Entity validation


        public List<FileMessageDataContract> ErelonenHuurAppImportItemValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaErelonenHuurAppImportItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            ws.ErelonenHuurApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == ErelonenHuurApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> ErelonenHuurAppImportedHistoryValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaErelonenHuurAppImportedHistoryDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            ws.ErelonenHuurApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == ErelonenHuurApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> ErelonenHuurAppVerdelingHistoriekValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaErelonenHuurAppVerdelingHistoriekDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            ws.ErelonenHuurApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == ErelonenHuurApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> ErelonenHuurAppFiche281_50Validate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaErelonenHuurAppFiche281_50DataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            ws.ErelonenHuurApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == ErelonenHuurApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        #endregion



        #region Add/Remove


        public EY.com.eBook.RuleEngine.AY2009.ErelonenHuurAppTypes.ErelonenHuur ErelonenHuurAppHistoriekAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaErelonenHuurAppVerdelingHistoriekDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            ErelonenHuurApp rp = ws.ErelonenHuurApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToHistoriek(cedc.Entity, ref ws);
            set.Calculate("ErelonenHuurApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.ErelonenHuurAppTypes.ErelonenHuur ErelonenHuurAppHistoriekRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            ErelonenHuurApp rp = ws.ErelonenHuurApp;
            rp.DeleteHistoriek(cfrdc.RowId, ref ws);
            set.Calculate("ErelonenHuurApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.ErelonenHuurAppTypes.ErelonenHuur ErelonenHuurAppHistoriekUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaErelonenHuurAppVerdelingHistoriekDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            ErelonenHuurApp rp = ws.ErelonenHuurApp;
            rp.UpdateHistoriek(cedc.Entity, ref ws);
            set.Calculate("ErelonenHuurApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.ErelonenHuurAppTypes.ErelonenHuur ErelonenHuurAppFiches281_50Update(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaErelonenHuurAppFiche281_50DataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            ErelonenHuurApp rp = ws.ErelonenHuurApp;
            rp.UpdateFiches281_50(cedc.Entity, ref ws);
            set.Calculate("ErelonenHuurApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.ErelonenHuurAppTypes.ErelonenHuur ErelonenHuurAppFiche281_50ColAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaErelonenHuurAppFiche281_50DataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            ErelonenHuurApp rp = ws.ErelonenHuurApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToFiche281_50Col(cedc.Entity, ref ws);
            set.Calculate("ErelonenHuurApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.ErelonenHuurAppTypes.ErelonenHuur ErelonenHuurAppFiche281_50ColRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            ErelonenHuurApp rp = ws.ErelonenHuurApp;
            rp.DeleteFiche281_50Col(cfrdc.RowId, ref ws);
            set.Calculate("ErelonenHuurApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.ErelonenHuurAppTypes.ErelonenHuur ErelonenHuurAppFiche281_50ColUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaErelonenHuurAppFiche281_50DataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            ErelonenHuurApp rp = ws.ErelonenHuurApp;
            rp.UpdateFiche281_50Col(cedc.Entity, ref ws);
            set.Calculate("ErelonenHuurApp");
            return rp.Data;
        }

        #endregion

        #endregion



        #region VoordelenApp

        public EY.com.eBook.RuleEngine.AY2009.VoordelenAppTypes.Voordelen VoordelenAppGetData(CriteriaFileDataContract cfdc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cfdc.FileId);

            return set.DataContainer.VoordelenApp.Data;
        }


        public EY.com.eBook.RuleEngine.AY2009.VoordelenAppTypes.Voordelen VoordelenAppCalculate(CriteriaFileDataContract cfdc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cfdc.FileId);
            set.Calculate("VoordelenApp");

            return set.DataContainer.VoordelenApp.Data;
        }

        #region Entity validation


        public List<FileMessageDataContract> VoordelenAppVAA_AutoValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoordelenAppVAA_AutoDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            ws.VoordelenApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == VoordelenApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> VoordelenAppVAA_AutoCO2Validate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoordelenAppVAA_AutoCO2DataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            ws.VoordelenApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == VoordelenApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> VoordelenAppVAA_NutsValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoordelenAppVAA_NutsDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            ws.VoordelenApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == VoordelenApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> VoordelenAppVAA_HUURValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoordelenAppVAA_HUURDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            ws.VoordelenApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == VoordelenApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> VoordelenAppVAA_HUURHERKValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoordelenAppVAA_HUURHERKDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            ws.VoordelenApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == VoordelenApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> VoordelenAppVAA_PCValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoordelenAppVAA_PCDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            ws.VoordelenApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == VoordelenApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> VoordelenAppVAA_TelefonieValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoordelenAppVAA_TelefonieDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            ws.VoordelenApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == VoordelenApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> VoordelenAppVAA_OverigeValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoordelenAppVAA_OverigeDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            ws.VoordelenApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == VoordelenApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> VoordelenAppVAA_Interesten_RC_BedrijfsleiderValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoordelenAppVAA_Interesten_RC_BedrijfsleiderDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            ws.VoordelenApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == VoordelenApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> VoordelenAppVAA_Soc_Bijdr_BedrijfsleiderValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoordelenAppVAA_Soc_Bijdr_BedrijfsleiderDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            ws.VoordelenApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == VoordelenApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> VoordelenAppVAA_Overige_NutsValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoordelenAppVAA_Overige_NutsDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            ws.VoordelenApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == VoordelenApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        #endregion



        #region Add/Remove


        public EY.com.eBook.RuleEngine.AY2009.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardAutoAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoordelenAppVAA_AutoDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            VoordelenApp rp = ws.VoordelenApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToVoordelenVanAlleAardAuto(cedc.Entity, ref ws);
            set.Calculate("VoordelenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardAutoRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            VoordelenApp rp = ws.VoordelenApp;
            rp.DeleteVoordelenVanAlleAardAuto(cfrdc.RowId, ref ws);
            set.Calculate("VoordelenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardAutoUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoordelenAppVAA_AutoDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            VoordelenApp rp = ws.VoordelenApp;
            rp.UpdateVoordelenVanAlleAardAuto(cedc.Entity, ref ws);
            set.Calculate("VoordelenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardAutoCO2Add(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoordelenAppVAA_AutoCO2DataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            VoordelenApp rp = ws.VoordelenApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToVoordelenVanAlleAardAutoCO2(cedc.Entity, ref ws);
            set.Calculate("VoordelenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardAutoCO2Remove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            VoordelenApp rp = ws.VoordelenApp;
            rp.DeleteVoordelenVanAlleAardAutoCO2(cfrdc.RowId, ref ws);
            set.Calculate("VoordelenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardAutoCO2Update(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoordelenAppVAA_AutoCO2DataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            VoordelenApp rp = ws.VoordelenApp;
            rp.UpdateVoordelenVanAlleAardAutoCO2(cedc.Entity, ref ws);
            set.Calculate("VoordelenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardNutsAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoordelenAppVAA_NutsDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            VoordelenApp rp = ws.VoordelenApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToVoordelenVanAlleAardNuts(cedc.Entity, ref ws);
            set.Calculate("VoordelenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardNutsRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            VoordelenApp rp = ws.VoordelenApp;
            rp.DeleteVoordelenVanAlleAardNuts(cfrdc.RowId, ref ws);
            set.Calculate("VoordelenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardNutsUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoordelenAppVAA_NutsDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            VoordelenApp rp = ws.VoordelenApp;
            rp.UpdateVoordelenVanAlleAardNuts(cedc.Entity, ref ws);
            set.Calculate("VoordelenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardHuurAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoordelenAppVAA_HUURDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            VoordelenApp rp = ws.VoordelenApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToVoordelenVanAlleAardHuur(cedc.Entity, ref ws);
            set.Calculate("VoordelenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardHuurRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            VoordelenApp rp = ws.VoordelenApp;
            rp.DeleteVoordelenVanAlleAardHuur(cfrdc.RowId, ref ws);
            set.Calculate("VoordelenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardHuurUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoordelenAppVAA_HUURDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            VoordelenApp rp = ws.VoordelenApp;
            rp.UpdateVoordelenVanAlleAardHuur(cedc.Entity, ref ws);
            set.Calculate("VoordelenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardHuurHerkwalificatieAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoordelenAppVAA_HUURHERKDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            VoordelenApp rp = ws.VoordelenApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToVoordelenVanAlleAardHuurHerkwalificatie(cedc.Entity, ref ws);
            set.Calculate("VoordelenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardHuurHerkwalificatieRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            VoordelenApp rp = ws.VoordelenApp;
            rp.DeleteVoordelenVanAlleAardHuurHerkwalificatie(cfrdc.RowId, ref ws);
            set.Calculate("VoordelenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardHuurHerkwalificatieUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoordelenAppVAA_HUURHERKDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            VoordelenApp rp = ws.VoordelenApp;
            rp.UpdateVoordelenVanAlleAardHuurHerkwalificatie(cedc.Entity, ref ws);
            set.Calculate("VoordelenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardPCAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoordelenAppVAA_PCDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            VoordelenApp rp = ws.VoordelenApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToVoordelenVanAlleAardPC(cedc.Entity, ref ws);
            set.Calculate("VoordelenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardPCRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            VoordelenApp rp = ws.VoordelenApp;
            rp.DeleteVoordelenVanAlleAardPC(cfrdc.RowId, ref ws);
            set.Calculate("VoordelenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardPCUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoordelenAppVAA_PCDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            VoordelenApp rp = ws.VoordelenApp;
            rp.UpdateVoordelenVanAlleAardPC(cedc.Entity, ref ws);
            set.Calculate("VoordelenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardTelefonieAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoordelenAppVAA_TelefonieDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            VoordelenApp rp = ws.VoordelenApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToVoordelenVanAlleAardTelefonie(cedc.Entity, ref ws);
            set.Calculate("VoordelenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardTelefonieRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            VoordelenApp rp = ws.VoordelenApp;
            rp.DeleteVoordelenVanAlleAardTelefonie(cfrdc.RowId, ref ws);
            set.Calculate("VoordelenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardTelefonieUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoordelenAppVAA_TelefonieDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            VoordelenApp rp = ws.VoordelenApp;
            rp.UpdateVoordelenVanAlleAardTelefonie(cedc.Entity, ref ws);
            set.Calculate("VoordelenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardOverigeAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoordelenAppVAA_OverigeDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            VoordelenApp rp = ws.VoordelenApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToVoordelenVanAlleAardOverige(cedc.Entity, ref ws);
            set.Calculate("VoordelenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardOverigeRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            VoordelenApp rp = ws.VoordelenApp;
            rp.DeleteVoordelenVanAlleAardOverige(cfrdc.RowId, ref ws);
            set.Calculate("VoordelenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardOverigeUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoordelenAppVAA_OverigeDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            VoordelenApp rp = ws.VoordelenApp;
            rp.UpdateVoordelenVanAlleAardOverige(cedc.Entity, ref ws);
            set.Calculate("VoordelenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.VoordelenAppTypes.Voordelen VoordelenAppVoordelenInterestenRCBedrijfsleiderAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoordelenAppVAA_Interesten_RC_BedrijfsleiderDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            VoordelenApp rp = ws.VoordelenApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToVoordelenInterestenRCBedrijfsleider(cedc.Entity, ref ws);
            set.Calculate("VoordelenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.VoordelenAppTypes.Voordelen VoordelenAppVoordelenInterestenRCBedrijfsleiderRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            VoordelenApp rp = ws.VoordelenApp;
            rp.DeleteVoordelenInterestenRCBedrijfsleider(cfrdc.RowId, ref ws);
            set.Calculate("VoordelenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.VoordelenAppTypes.Voordelen VoordelenAppVoordelenInterestenRCBedrijfsleiderUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoordelenAppVAA_Interesten_RC_BedrijfsleiderDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            VoordelenApp rp = ws.VoordelenApp;
            rp.UpdateVoordelenInterestenRCBedrijfsleider(cedc.Entity, ref ws);
            set.Calculate("VoordelenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.VoordelenAppTypes.Voordelen VoordelenAppVoordelenSocialeBijdragenBedrijfsleiderAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoordelenAppVAA_Soc_Bijdr_BedrijfsleiderDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            VoordelenApp rp = ws.VoordelenApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToVoordelenSocialeBijdragenBedrijfsleider(cedc.Entity, ref ws);
            set.Calculate("VoordelenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.VoordelenAppTypes.Voordelen VoordelenAppVoordelenSocialeBijdragenBedrijfsleiderRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            VoordelenApp rp = ws.VoordelenApp;
            rp.DeleteVoordelenSocialeBijdragenBedrijfsleider(cfrdc.RowId, ref ws);
            set.Calculate("VoordelenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.VoordelenAppTypes.Voordelen VoordelenAppVoordelenSocialeBijdragenBedrijfsleiderUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoordelenAppVAA_Soc_Bijdr_BedrijfsleiderDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            VoordelenApp rp = ws.VoordelenApp;
            rp.UpdateVoordelenSocialeBijdragenBedrijfsleider(cedc.Entity, ref ws);
            set.Calculate("VoordelenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.VoordelenAppTypes.Voordelen VoordelenAppVoordelenOverigeNutsAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoordelenAppVAA_Overige_NutsDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            VoordelenApp rp = ws.VoordelenApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToVoordelenOverigeNuts(cedc.Entity, ref ws);
            set.Calculate("VoordelenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.VoordelenAppTypes.Voordelen VoordelenAppVoordelenOverigeNutsRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            VoordelenApp rp = ws.VoordelenApp;
            rp.DeleteVoordelenOverigeNuts(cfrdc.RowId, ref ws);
            set.Calculate("VoordelenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.VoordelenAppTypes.Voordelen VoordelenAppVoordelenOverigeNutsUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoordelenAppVAA_Overige_NutsDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            VoordelenApp rp = ws.VoordelenApp;
            rp.UpdateVoordelenOverigeNuts(cedc.Entity, ref ws);
            set.Calculate("VoordelenApp");
            return rp.Data;
        }

        #endregion

        #endregion



        #region PersoneelApp

        public EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.WPPersoneel PersoneelAppGetData(CriteriaFileDataContract cfdc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cfdc.FileId);

            return set.DataContainer.PersoneelApp.Data;
        }


        public EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.WPPersoneel PersoneelAppCalculate(CriteriaFileDataContract cfdc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cfdc.FileId);
            set.Calculate("PersoneelApp");

            return set.DataContainer.PersoneelApp.Data;
        }

        #region Entity validation


        public List<FileMessageDataContract> PersoneelAppItem325Validate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaPersoneelAppItem325DataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            ws.PersoneelApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == PersoneelApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> PersoneelAppVerschilItemValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaPersoneelAppVerschilItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            ws.PersoneelApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == PersoneelApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> PersoneelAppWerknemerValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaPersoneelAppWerknemerDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            ws.PersoneelApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == PersoneelApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> PersoneelAppAlgemeenValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaPersoneelAppAlgemeenDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            ws.PersoneelApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == PersoneelApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> PersoneelAppVakantiegeldValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaPersoneelAppVakantiegeldDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            ws.PersoneelApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == PersoneelApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        #endregion



        #region Add/Remove


        public EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.WPPersoneel PersoneelApp_325_20CollAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaPersoneelAppItem325DataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            PersoneelApp rp = ws.PersoneelApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddTo_325_20Coll(cedc.Entity, ref ws);
            set.Calculate("PersoneelApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.WPPersoneel PersoneelApp_325_20CollRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            PersoneelApp rp = ws.PersoneelApp;
            rp.Delete_325_20Coll(cfrdc.RowId, ref ws);
            set.Calculate("PersoneelApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.WPPersoneel PersoneelApp_325_20CollUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaPersoneelAppItem325DataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            PersoneelApp rp = ws.PersoneelApp;
            rp.Update_325_20Coll(cedc.Entity, ref ws);
            set.Calculate("PersoneelApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.WPPersoneel PersoneelAppTeVerklarenVerschil325_20Add(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaPersoneelAppVerschilItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            PersoneelApp rp = ws.PersoneelApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToTeVerklarenVerschil325_20(cedc.Entity, ref ws);
            set.Calculate("PersoneelApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.WPPersoneel PersoneelAppTeVerklarenVerschil325_20Remove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            PersoneelApp rp = ws.PersoneelApp;
            rp.DeleteTeVerklarenVerschil325_20(cfrdc.RowId, ref ws);
            set.Calculate("PersoneelApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.WPPersoneel PersoneelAppTeVerklarenVerschil325_20Update(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaPersoneelAppVerschilItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            PersoneelApp rp = ws.PersoneelApp;
            rp.UpdateTeVerklarenVerschil325_20(cedc.Entity, ref ws);
            set.Calculate("PersoneelApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.WPPersoneel PersoneelApp_325_10CollAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaPersoneelAppItem325DataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            PersoneelApp rp = ws.PersoneelApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddTo_325_10Coll(cedc.Entity, ref ws);
            set.Calculate("PersoneelApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.WPPersoneel PersoneelApp_325_10CollRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            PersoneelApp rp = ws.PersoneelApp;
            rp.Delete_325_10Coll(cfrdc.RowId, ref ws);
            set.Calculate("PersoneelApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.WPPersoneel PersoneelApp_325_10CollUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaPersoneelAppItem325DataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            PersoneelApp rp = ws.PersoneelApp;
            rp.Update_325_10Coll(cedc.Entity, ref ws);
            set.Calculate("PersoneelApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.WPPersoneel PersoneelAppTeVerklarenVerschil325_10Add(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaPersoneelAppVerschilItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            PersoneelApp rp = ws.PersoneelApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToTeVerklarenVerschil325_10(cedc.Entity, ref ws);
            set.Calculate("PersoneelApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.WPPersoneel PersoneelAppTeVerklarenVerschil325_10Remove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            PersoneelApp rp = ws.PersoneelApp;
            rp.DeleteTeVerklarenVerschil325_10(cfrdc.RowId, ref ws);
            set.Calculate("PersoneelApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.WPPersoneel PersoneelAppTeVerklarenVerschil325_10Update(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaPersoneelAppVerschilItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            PersoneelApp rp = ws.PersoneelApp;
            rp.UpdateTeVerklarenVerschil325_10(cedc.Entity, ref ws);
            set.Calculate("PersoneelApp");
            return rp.Data;
        }


        public EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.WPPersoneel PersoneelAppArbeidersFormUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaPersoneelAppWerknemerDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            PersoneelApp rp = ws.PersoneelApp;
            rp.UpdateArbeidersForm(cedc.Entity, ref ws);
            set.Calculate("PersoneelApp");
            return rp.Data;
        }



        public EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.WPPersoneel PersoneelAppBediendeFormUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaPersoneelAppWerknemerDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            PersoneelApp rp = ws.PersoneelApp;
            rp.UpdateBediendeForm(cedc.Entity, ref ws);
            set.Calculate("PersoneelApp");
            return rp.Data;
        }


        public EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.WPPersoneel PersoneelAppAansluitingArbeidersAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaPersoneelAppVerschilItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            PersoneelApp rp = ws.PersoneelApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToAansluitingArbeiders(cedc.Entity, ref ws);
            set.Calculate("PersoneelApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.WPPersoneel PersoneelAppAansluitingArbeidersRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            PersoneelApp rp = ws.PersoneelApp;
            rp.DeleteAansluitingArbeiders(cfrdc.RowId, ref ws);
            set.Calculate("PersoneelApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.WPPersoneel PersoneelAppAansluitingArbeidersUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaPersoneelAppVerschilItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            PersoneelApp rp = ws.PersoneelApp;
            rp.UpdateAansluitingArbeiders(cedc.Entity, ref ws);
            set.Calculate("PersoneelApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.WPPersoneel PersoneelAppAansluitingBediendenAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaPersoneelAppVerschilItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            PersoneelApp rp = ws.PersoneelApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToAansluitingBedienden(cedc.Entity, ref ws);
            set.Calculate("PersoneelApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.WPPersoneel PersoneelAppAansluitingBediendenRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            PersoneelApp rp = ws.PersoneelApp;
            rp.DeleteAansluitingBedienden(cfrdc.RowId, ref ws);
            set.Calculate("PersoneelApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.WPPersoneel PersoneelAppAansluitingBediendenUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaPersoneelAppVerschilItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            PersoneelApp rp = ws.PersoneelApp;
            rp.UpdateAansluitingBedienden(cedc.Entity, ref ws);
            set.Calculate("PersoneelApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.WPPersoneel PersoneelAppRSZCollAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaPersoneelAppAlgemeenDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            PersoneelApp rp = ws.PersoneelApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToRSZColl(cedc.Entity, ref ws);
            set.Calculate("PersoneelApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.WPPersoneel PersoneelAppRSZCollRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            PersoneelApp rp = ws.PersoneelApp;
            rp.DeleteRSZColl(cfrdc.RowId, ref ws);
            set.Calculate("PersoneelApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.WPPersoneel PersoneelAppRSZCollUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaPersoneelAppAlgemeenDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            PersoneelApp rp = ws.PersoneelApp;
            rp.UpdateRSZColl(cedc.Entity, ref ws);
            set.Calculate("PersoneelApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.WPPersoneel PersoneelAppBVsCollAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaPersoneelAppAlgemeenDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            PersoneelApp rp = ws.PersoneelApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToBVsColl(cedc.Entity, ref ws);
            set.Calculate("PersoneelApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.WPPersoneel PersoneelAppBVsCollRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            PersoneelApp rp = ws.PersoneelApp;
            rp.DeleteBVsColl(cfrdc.RowId, ref ws);
            set.Calculate("PersoneelApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.WPPersoneel PersoneelAppBVsCollUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaPersoneelAppAlgemeenDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            PersoneelApp rp = ws.PersoneelApp;
            rp.UpdateBVsColl(cedc.Entity, ref ws);
            set.Calculate("PersoneelApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.WPPersoneel PersoneelAppNettoCollAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaPersoneelAppAlgemeenDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            PersoneelApp rp = ws.PersoneelApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToNettoColl(cedc.Entity, ref ws);
            set.Calculate("PersoneelApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.WPPersoneel PersoneelAppNettoCollRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            PersoneelApp rp = ws.PersoneelApp;
            rp.DeleteNettoColl(cfrdc.RowId, ref ws);
            set.Calculate("PersoneelApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.WPPersoneel PersoneelAppNettoCollUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaPersoneelAppAlgemeenDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            PersoneelApp rp = ws.PersoneelApp;
            rp.UpdateNettoColl(cedc.Entity, ref ws);
            set.Calculate("PersoneelApp");
            return rp.Data;
        }


        public EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.WPPersoneel PersoneelAppVakantieGeldCollUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaPersoneelAppVakantiegeldDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            PersoneelApp rp = ws.PersoneelApp;
            rp.UpdateVakantieGeldColl(cedc.Entity, ref ws);
            set.Calculate("PersoneelApp");
            return rp.Data;
        }


        #endregion

        #endregion



        #region BTWApp

        public EY.com.eBook.RuleEngine.AY2009.BTWAppTypes.WPBTW BTWAppGetData(CriteriaFileDataContract cfdc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cfdc.FileId);

            return set.DataContainer.BTWApp.Data;
        }


        public EY.com.eBook.RuleEngine.AY2009.BTWAppTypes.WPBTW BTWAppCalculate(CriteriaFileDataContract cfdc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cfdc.FileId);
            set.Calculate("BTWApp");

            return set.DataContainer.BTWApp.Data;
        }

        #region Entity validation


        public List<FileMessageDataContract> BTWAppOmzetAangifteValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBTWAppOmzetAangifteDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            ws.BTWApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == BTWApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> BTWAppOmzetGeboektValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBTWAppOmzetGeboektDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            ws.BTWApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == BTWApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> BTWAppOmzetVerklaringValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBTWAppOmzetVerklaringDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            ws.BTWApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == BTWApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> BTWAppRCAangifteValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBTWAppRCAangifteDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            ws.BTWApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == BTWApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> BTWAppRCGeboektValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBTWAppRCGeboektDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            ws.BTWApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == BTWApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> BTWAppRCVerklaringValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBTWAppRCVerklaringDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            ws.BTWApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == BTWApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        #endregion



        #region Add/Remove


        public EY.com.eBook.RuleEngine.AY2009.BTWAppTypes.WPBTW BTWAppOmzetAangiftesAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBTWAppOmzetAangifteDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            BTWApp rp = ws.BTWApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToOmzetAangiftes(cedc.Entity, ref ws);
            set.Calculate("BTWApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.BTWAppTypes.WPBTW BTWAppOmzetAangiftesRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            BTWApp rp = ws.BTWApp;
            rp.DeleteOmzetAangiftes(cfrdc.RowId, ref ws);
            set.Calculate("BTWApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.BTWAppTypes.WPBTW BTWAppOmzetAangiftesUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBTWAppOmzetAangifteDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            BTWApp rp = ws.BTWApp;
            rp.UpdateOmzetAangiftes(cedc.Entity, ref ws);
            set.Calculate("BTWApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.BTWAppTypes.WPBTW BTWAppOmzettenGeboektAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBTWAppOmzetGeboektDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            BTWApp rp = ws.BTWApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToOmzettenGeboekt(cedc.Entity, ref ws);
            set.Calculate("BTWApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.BTWAppTypes.WPBTW BTWAppOmzettenGeboektRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            BTWApp rp = ws.BTWApp;
            rp.DeleteOmzettenGeboekt(cfrdc.RowId, ref ws);
            set.Calculate("BTWApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.BTWAppTypes.WPBTW BTWAppOmzettenGeboektUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBTWAppOmzetGeboektDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            BTWApp rp = ws.BTWApp;
            rp.UpdateOmzettenGeboekt(cedc.Entity, ref ws);
            set.Calculate("BTWApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.BTWAppTypes.WPBTW BTWAppOmzetVerklaringenAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBTWAppOmzetVerklaringDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            BTWApp rp = ws.BTWApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToOmzetVerklaringen(cedc.Entity, ref ws);
            set.Calculate("BTWApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.BTWAppTypes.WPBTW BTWAppOmzetVerklaringenRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            BTWApp rp = ws.BTWApp;
            rp.DeleteOmzetVerklaringen(cfrdc.RowId, ref ws);
            set.Calculate("BTWApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.BTWAppTypes.WPBTW BTWAppOmzetVerklaringenUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBTWAppOmzetVerklaringDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            BTWApp rp = ws.BTWApp;
            rp.UpdateOmzetVerklaringen(cedc.Entity, ref ws);
            set.Calculate("BTWApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.BTWAppTypes.WPBTW BTWAppRCAangiftesAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBTWAppRCAangifteDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            BTWApp rp = ws.BTWApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToRCAangiftes(cedc.Entity, ref ws);
            set.Calculate("BTWApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.BTWAppTypes.WPBTW BTWAppRCAangiftesRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            BTWApp rp = ws.BTWApp;
            rp.DeleteRCAangiftes(cfrdc.RowId, ref ws);
            set.Calculate("BTWApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.BTWAppTypes.WPBTW BTWAppRCAangiftesUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBTWAppRCAangifteDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            BTWApp rp = ws.BTWApp;
            rp.UpdateRCAangiftes(cedc.Entity, ref ws);
            set.Calculate("BTWApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.BTWAppTypes.WPBTW BTWAppRCenGeboektAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBTWAppRCGeboektDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            BTWApp rp = ws.BTWApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToRCenGeboekt(cedc.Entity, ref ws);
            set.Calculate("BTWApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.BTWAppTypes.WPBTW BTWAppRCenGeboektRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            BTWApp rp = ws.BTWApp;
            rp.DeleteRCenGeboekt(cfrdc.RowId, ref ws);
            set.Calculate("BTWApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.BTWAppTypes.WPBTW BTWAppRCenGeboektUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBTWAppRCGeboektDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            BTWApp rp = ws.BTWApp;
            rp.UpdateRCenGeboekt(cedc.Entity, ref ws);
            set.Calculate("BTWApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.BTWAppTypes.WPBTW BTWAppRCVerklaringenAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBTWAppRCVerklaringDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            BTWApp rp = ws.BTWApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToRCVerklaringen(cedc.Entity, ref ws);
            set.Calculate("BTWApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.BTWAppTypes.WPBTW BTWAppRCVerklaringenRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            BTWApp rp = ws.BTWApp;
            rp.DeleteRCVerklaringen(cfrdc.RowId, ref ws);
            set.Calculate("BTWApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.BTWAppTypes.WPBTW BTWAppRCVerklaringenUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBTWAppRCVerklaringDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            BTWApp rp = ws.BTWApp;
            rp.UpdateRCVerklaringen(cedc.Entity, ref ws);
            set.Calculate("BTWApp");
            return rp.Data;
        }

        #endregion

        #endregion



        #region RVIntrestenApp

        public EY.com.eBook.RuleEngine.AY2009.RVIntrestenAppTypes.WPRVIntresten RVIntrestenAppGetData(CriteriaFileDataContract cfdc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cfdc.FileId);

            return set.DataContainer.RVIntrestenApp.Data;
        }


        public EY.com.eBook.RuleEngine.AY2009.RVIntrestenAppTypes.WPRVIntresten RVIntrestenAppCalculate(CriteriaFileDataContract cfdc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cfdc.FileId);
            set.Calculate("RVIntrestenApp");

            return set.DataContainer.RVIntrestenApp.Data;
        }

        #region Entity validation


        public List<FileMessageDataContract> RVIntrestenAppIntrestItemValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaRVIntrestenAppIntrestItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            ws.RVIntrestenApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == RVIntrestenApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> RVIntrestenAppAdjustmentValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaRVIntrestenAppAdjustmentDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            ws.RVIntrestenApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == RVIntrestenApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> RVIntrestenAppVerchilItemValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaRVIntrestenAppVerchilItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            ws.RVIntrestenApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == RVIntrestenApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        #endregion



        #region Add/Remove


        public EY.com.eBook.RuleEngine.AY2009.RVIntrestenAppTypes.WPRVIntresten RVIntrestenAppIntrestenUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaRVIntrestenAppIntrestItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            RVIntrestenApp rp = ws.RVIntrestenApp;
            rp.UpdateIntresten(cedc.Entity, ref ws);
            set.Calculate("RVIntrestenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.RVIntrestenAppTypes.WPRVIntresten RVIntrestenAppAdjustmentsAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaRVIntrestenAppAdjustmentDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            RVIntrestenApp rp = ws.RVIntrestenApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToAdjustments(cedc.Entity, ref ws);
            set.Calculate("RVIntrestenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.RVIntrestenAppTypes.WPRVIntresten RVIntrestenAppAdjustmentsRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            RVIntrestenApp rp = ws.RVIntrestenApp;
            rp.DeleteAdjustments(cfrdc.RowId, ref ws);
            set.Calculate("RVIntrestenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.RVIntrestenAppTypes.WPRVIntresten RVIntrestenAppAdjustmentsUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaRVIntrestenAppAdjustmentDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            RVIntrestenApp rp = ws.RVIntrestenApp;
            rp.UpdateAdjustments(cedc.Entity, ref ws);
            set.Calculate("RVIntrestenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.RVIntrestenAppTypes.WPRVIntresten RVIntrestenAppVerschillenAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaRVIntrestenAppVerchilItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            RVIntrestenApp rp = ws.RVIntrestenApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToVerschillen(cedc.Entity, ref ws);
            set.Calculate("RVIntrestenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.RVIntrestenAppTypes.WPRVIntresten RVIntrestenAppVerschillenRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            RVIntrestenApp rp = ws.RVIntrestenApp;
            rp.DeleteVerschillen(cfrdc.RowId, ref ws);
            set.Calculate("RVIntrestenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.RVIntrestenAppTypes.WPRVIntresten RVIntrestenAppVerschillenUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaRVIntrestenAppVerchilItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            RVIntrestenApp rp = ws.RVIntrestenApp;
            rp.UpdateVerschillen(cedc.Entity, ref ws);
            set.Calculate("RVIntrestenApp");
            return rp.Data;
        }

        #endregion

        #endregion



        #region DubieuzeDebiteurenApp

        public EY.com.eBook.RuleEngine.AY2009.DubieuzeDebiteurenAppTypes.DubieuzeDebiteuren DubieuzeDebiteurenAppGetData(CriteriaFileDataContract cfdc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cfdc.FileId);

            return set.DataContainer.DubieuzeDebiteurenApp.Data;
        }


        public EY.com.eBook.RuleEngine.AY2009.DubieuzeDebiteurenAppTypes.DubieuzeDebiteuren DubieuzeDebiteurenAppCalculate(CriteriaFileDataContract cfdc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cfdc.FileId);
            set.Calculate("DubieuzeDebiteurenApp");

            return set.DataContainer.DubieuzeDebiteurenApp.Data;
        }

        #region Entity validation


        public List<FileMessageDataContract> DubieuzeDebiteurenAppDubieuzeDebiteurTValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaDubieuzeDebiteurenAppDubieuzeDebiteurTDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            ws.DubieuzeDebiteurenApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == DubieuzeDebiteurenApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> DubieuzeDebiteurenAppDubieuzeDebiteurWValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaDubieuzeDebiteurenAppDubieuzeDebiteurWDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            ws.DubieuzeDebiteurenApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == DubieuzeDebiteurenApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> DubieuzeDebiteurenAppDubieuzeDebiteurNValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaDubieuzeDebiteurenAppDubieuzeDebiteurNDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            ws.DubieuzeDebiteurenApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == DubieuzeDebiteurenApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> DubieuzeDebiteurenAppDubieuzeDebiteurHValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaDubieuzeDebiteurenAppDubieuzeDebiteurHDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            ws.DubieuzeDebiteurenApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == DubieuzeDebiteurenApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        #endregion



        #region Add/Remove


        public EY.com.eBook.RuleEngine.AY2009.DubieuzeDebiteurenAppTypes.DubieuzeDebiteuren DubieuzeDebiteurenAppToestandEindVorigBoekjaarAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaDubieuzeDebiteurenAppDubieuzeDebiteurTDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            DubieuzeDebiteurenApp rp = ws.DubieuzeDebiteurenApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToToestandEindVorigBoekjaar(cedc.Entity, ref ws);
            set.Calculate("DubieuzeDebiteurenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.DubieuzeDebiteurenAppTypes.DubieuzeDebiteuren DubieuzeDebiteurenAppToestandEindVorigBoekjaarRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            DubieuzeDebiteurenApp rp = ws.DubieuzeDebiteurenApp;
            rp.DeleteToestandEindVorigBoekjaar(cfrdc.RowId, ref ws);
            set.Calculate("DubieuzeDebiteurenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.DubieuzeDebiteurenAppTypes.DubieuzeDebiteuren DubieuzeDebiteurenAppToestandEindVorigBoekjaarUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaDubieuzeDebiteurenAppDubieuzeDebiteurTDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            DubieuzeDebiteurenApp rp = ws.DubieuzeDebiteurenApp;
            rp.UpdateToestandEindVorigBoekjaar(cedc.Entity, ref ws);
            set.Calculate("DubieuzeDebiteurenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.DubieuzeDebiteurenAppTypes.DubieuzeDebiteuren DubieuzeDebiteurenAppWijzigingBestaandeTijdensBoekjaarAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaDubieuzeDebiteurenAppDubieuzeDebiteurWDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            DubieuzeDebiteurenApp rp = ws.DubieuzeDebiteurenApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToWijzigingBestaandeTijdensBoekjaar(cedc.Entity, ref ws);
            set.Calculate("DubieuzeDebiteurenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.DubieuzeDebiteurenAppTypes.DubieuzeDebiteuren DubieuzeDebiteurenAppWijzigingBestaandeTijdensBoekjaarRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            DubieuzeDebiteurenApp rp = ws.DubieuzeDebiteurenApp;
            rp.DeleteWijzigingBestaandeTijdensBoekjaar(cfrdc.RowId, ref ws);
            set.Calculate("DubieuzeDebiteurenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.DubieuzeDebiteurenAppTypes.DubieuzeDebiteuren DubieuzeDebiteurenAppWijzigingBestaandeTijdensBoekjaarUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaDubieuzeDebiteurenAppDubieuzeDebiteurWDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            DubieuzeDebiteurenApp rp = ws.DubieuzeDebiteurenApp;
            rp.UpdateWijzigingBestaandeTijdensBoekjaar(cedc.Entity, ref ws);
            set.Calculate("DubieuzeDebiteurenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.DubieuzeDebiteurenAppTypes.DubieuzeDebiteuren DubieuzeDebiteurenAppNieuweTijdensBoekjaarAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaDubieuzeDebiteurenAppDubieuzeDebiteurNDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            DubieuzeDebiteurenApp rp = ws.DubieuzeDebiteurenApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToNieuweTijdensBoekjaar(cedc.Entity, ref ws);
            set.Calculate("DubieuzeDebiteurenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.DubieuzeDebiteurenAppTypes.DubieuzeDebiteuren DubieuzeDebiteurenAppNieuweTijdensBoekjaarRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            DubieuzeDebiteurenApp rp = ws.DubieuzeDebiteurenApp;
            rp.DeleteNieuweTijdensBoekjaar(cfrdc.RowId, ref ws);
            set.Calculate("DubieuzeDebiteurenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.DubieuzeDebiteurenAppTypes.DubieuzeDebiteuren DubieuzeDebiteurenAppNieuweTijdensBoekjaarUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaDubieuzeDebiteurenAppDubieuzeDebiteurNDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            DubieuzeDebiteurenApp rp = ws.DubieuzeDebiteurenApp;
            rp.UpdateNieuweTijdensBoekjaar(cedc.Entity, ref ws);
            set.Calculate("DubieuzeDebiteurenApp");
            return rp.Data;
        }

        #endregion

        #endregion



        #region VerliesHandelsdebiteurenApp

        public EY.com.eBook.RuleEngine.AY2009.VerliesHandelsdebiteurenAppTypes.VerliesHandelsDebiteuren VerliesHandelsdebiteurenAppGetData(CriteriaFileDataContract cfdc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cfdc.FileId);

            return set.DataContainer.VerliesHandelsdebiteurenApp.Data;
        }


        public EY.com.eBook.RuleEngine.AY2009.VerliesHandelsdebiteurenAppTypes.VerliesHandelsDebiteuren VerliesHandelsdebiteurenAppCalculate(CriteriaFileDataContract cfdc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cfdc.FileId);
            set.Calculate("VerliesHandelsdebiteurenApp");

            return set.DataContainer.VerliesHandelsdebiteurenApp.Data;
        }

        #region Entity validation


        public List<FileMessageDataContract> VerliesHandelsdebiteurenAppVerliesHandelsDebiteurValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVerliesHandelsdebiteurenAppVerliesHandelsDebiteurDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            ws.VerliesHandelsdebiteurenApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == VerliesHandelsdebiteurenApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        #endregion



        #region Add/Remove


        public EY.com.eBook.RuleEngine.AY2009.VerliesHandelsdebiteurenAppTypes.VerliesHandelsDebiteuren VerliesHandelsdebiteurenAppVerliesHDAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVerliesHandelsdebiteurenAppVerliesHandelsDebiteurDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            VerliesHandelsdebiteurenApp rp = ws.VerliesHandelsdebiteurenApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToVerliesHD(cedc.Entity, ref ws);
            set.Calculate("VerliesHandelsdebiteurenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.VerliesHandelsdebiteurenAppTypes.VerliesHandelsDebiteuren VerliesHandelsdebiteurenAppVerliesHDRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            VerliesHandelsdebiteurenApp rp = ws.VerliesHandelsdebiteurenApp;
            rp.DeleteVerliesHD(cfrdc.RowId, ref ws);
            set.Calculate("VerliesHandelsdebiteurenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.VerliesHandelsdebiteurenAppTypes.VerliesHandelsDebiteuren VerliesHandelsdebiteurenAppVerliesHDUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVerliesHandelsdebiteurenAppVerliesHandelsDebiteurDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            VerliesHandelsdebiteurenApp rp = ws.VerliesHandelsdebiteurenApp;
            rp.UpdateVerliesHD(cedc.Entity, ref ws);
            set.Calculate("VerliesHandelsdebiteurenApp");
            return rp.Data;
        }

        #endregion

        #endregion



        #region VoorzieningenApp

        public EY.com.eBook.RuleEngine.AY2009.VoorzieningenAppTypes.Voorzieningen VoorzieningenAppGetData(CriteriaFileDataContract cfdc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cfdc.FileId);

            return set.DataContainer.VoorzieningenApp.Data;
        }


        public EY.com.eBook.RuleEngine.AY2009.VoorzieningenAppTypes.Voorzieningen VoorzieningenAppCalculate(CriteriaFileDataContract cfdc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cfdc.FileId);
            set.Calculate("VoorzieningenApp");

            return set.DataContainer.VoorzieningenApp.Data;
        }

        #region Entity validation


        public List<FileMessageDataContract> VoorzieningenAppVoorzieningenTValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoorzieningenAppVoorzieningenTDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            ws.VoorzieningenApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == VoorzieningenApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> VoorzieningenAppVoorzieningenWValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoorzieningenAppVoorzieningenWDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            ws.VoorzieningenApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == VoorzieningenApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> VoorzieningenAppVoorzieningenNValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoorzieningenAppVoorzieningenNDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            ws.VoorzieningenApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == VoorzieningenApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> VoorzieningenAppVoorzieningenHValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoorzieningenAppVoorzieningenHDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            ws.VoorzieningenApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == VoorzieningenApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        #endregion



        #region Add/Remove


        public EY.com.eBook.RuleEngine.AY2009.VoorzieningenAppTypes.Voorzieningen VoorzieningenAppToestandEindVorigBoekjaarAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoorzieningenAppVoorzieningenTDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            VoorzieningenApp rp = ws.VoorzieningenApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToToestandEindVorigBoekjaar(cedc.Entity, ref ws);
            set.Calculate("VoorzieningenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.VoorzieningenAppTypes.Voorzieningen VoorzieningenAppToestandEindVorigBoekjaarRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            VoorzieningenApp rp = ws.VoorzieningenApp;
            rp.DeleteToestandEindVorigBoekjaar(cfrdc.RowId, ref ws);
            set.Calculate("VoorzieningenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.VoorzieningenAppTypes.Voorzieningen VoorzieningenAppToestandEindVorigBoekjaarUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoorzieningenAppVoorzieningenTDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            VoorzieningenApp rp = ws.VoorzieningenApp;
            rp.UpdateToestandEindVorigBoekjaar(cedc.Entity, ref ws);
            set.Calculate("VoorzieningenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.VoorzieningenAppTypes.Voorzieningen VoorzieningenAppWijzigingBestaandeTijdensBoekjaarAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoorzieningenAppVoorzieningenWDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            VoorzieningenApp rp = ws.VoorzieningenApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToWijzigingBestaandeTijdensBoekjaar(cedc.Entity, ref ws);
            set.Calculate("VoorzieningenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.VoorzieningenAppTypes.Voorzieningen VoorzieningenAppWijzigingBestaandeTijdensBoekjaarRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            VoorzieningenApp rp = ws.VoorzieningenApp;
            rp.DeleteWijzigingBestaandeTijdensBoekjaar(cfrdc.RowId, ref ws);
            set.Calculate("VoorzieningenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.VoorzieningenAppTypes.Voorzieningen VoorzieningenAppWijzigingBestaandeTijdensBoekjaarUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoorzieningenAppVoorzieningenWDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            VoorzieningenApp rp = ws.VoorzieningenApp;
            rp.UpdateWijzigingBestaandeTijdensBoekjaar(cedc.Entity, ref ws);
            set.Calculate("VoorzieningenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.VoorzieningenAppTypes.Voorzieningen VoorzieningenAppNieuweTijdensBoekjaarAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoorzieningenAppVoorzieningenNDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            VoorzieningenApp rp = ws.VoorzieningenApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToNieuweTijdensBoekjaar(cedc.Entity, ref ws);
            set.Calculate("VoorzieningenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.VoorzieningenAppTypes.Voorzieningen VoorzieningenAppNieuweTijdensBoekjaarRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            VoorzieningenApp rp = ws.VoorzieningenApp;
            rp.DeleteNieuweTijdensBoekjaar(cfrdc.RowId, ref ws);
            set.Calculate("VoorzieningenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.VoorzieningenAppTypes.Voorzieningen VoorzieningenAppNieuweTijdensBoekjaarUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoorzieningenAppVoorzieningenNDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            VoorzieningenApp rp = ws.VoorzieningenApp;
            rp.UpdateNieuweTijdensBoekjaar(cedc.Entity, ref ws);
            set.Calculate("VoorzieningenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.VoorzieningenAppTypes.Voorzieningen VoorzieningenAppToestandEindHuidigBoekjaarAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoorzieningenAppVoorzieningenHDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            VoorzieningenApp rp = ws.VoorzieningenApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToToestandEindHuidigBoekjaar(cedc.Entity, ref ws);
            set.Calculate("VoorzieningenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.VoorzieningenAppTypes.Voorzieningen VoorzieningenAppToestandEindHuidigBoekjaarRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            VoorzieningenApp rp = ws.VoorzieningenApp;
            rp.DeleteToestandEindHuidigBoekjaar(cfrdc.RowId, ref ws);
            set.Calculate("VoorzieningenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.VoorzieningenAppTypes.Voorzieningen VoorzieningenAppToestandEindHuidigBoekjaarUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaVoorzieningenAppVoorzieningenHDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            VoorzieningenApp rp = ws.VoorzieningenApp;
            rp.UpdateToestandEindHuidigBoekjaar(cedc.Entity, ref ws);
            set.Calculate("VoorzieningenApp");
            return rp.Data;
        }

        #endregion

        #endregion



        #region BelastingenApp

        public EY.com.eBook.RuleEngine.AY2009.BelastingenAppTypes.WPBelastingen BelastingenAppGetData(CriteriaFileDataContract cfdc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cfdc.FileId);

            return set.DataContainer.BelastingenApp.Data;
        }


        public EY.com.eBook.RuleEngine.AY2009.BelastingenAppTypes.WPBelastingen BelastingenAppCalculate(CriteriaFileDataContract cfdc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cfdc.FileId);
            set.Calculate("BelastingenApp");

            return set.DataContainer.BelastingenApp.Data;
        }

        #region Entity validation


        public List<FileMessageDataContract> BelastingenAppAdjustmentValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBelastingenAppAdjustmentDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            ws.BelastingenApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == BelastingenApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> BelastingenAppBelastingValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBelastingenAppBelastingDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            ws.BelastingenApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == BelastingenApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> BelastingenAppKostItemValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBelastingenAppKostItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            ws.BelastingenApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == BelastingenApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        #endregion



        #region Add/Remove


        public EY.com.eBook.RuleEngine.AY2009.BelastingenAppTypes.WPBelastingen BelastingenAppAdjustmentsAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBelastingenAppAdjustmentDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            BelastingenApp rp = ws.BelastingenApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToAdjustments(cedc.Entity, ref ws);
            set.Calculate("BelastingenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.BelastingenAppTypes.WPBelastingen BelastingenAppAdjustmentsRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            BelastingenApp rp = ws.BelastingenApp;
            rp.DeleteAdjustments(cfrdc.RowId, ref ws);
            set.Calculate("BelastingenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.BelastingenAppTypes.WPBelastingen BelastingenAppAdjustmentsUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBelastingenAppAdjustmentDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            BelastingenApp rp = ws.BelastingenApp;
            rp.UpdateAdjustments(cedc.Entity, ref ws);
            set.Calculate("BelastingenApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.BelastingenAppTypes.WPBelastingen BelastingenAppBelastingenUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBelastingenAppBelastingDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            BelastingenApp rp = ws.BelastingenApp;
            rp.UpdateBelastingen(cedc.Entity, ref ws);
            set.Calculate("BelastingenApp");
            return rp.Data;
        }

        #endregion

        #endregion



        #region BelasteReservesApp

        public EY.com.eBook.RuleEngine.AY2009.BelasteReservesAppTypes.BelasteReserves BelasteReservesAppGetData(CriteriaFileDataContract cfdc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cfdc.FileId);

            return set.DataContainer.BelasteReservesApp.Data;
        }


        public EY.com.eBook.RuleEngine.AY2009.BelasteReservesAppTypes.BelasteReserves BelasteReservesAppCalculate(CriteriaFileDataContract cfdc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cfdc.FileId);
            set.Calculate("BelasteReservesApp");

            return set.DataContainer.BelasteReservesApp.Data;
        }

        #region Entity validation


        public List<FileMessageDataContract> BelasteReservesAppBelasteReservesItemValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBelasteReservesAppBelasteReservesItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            ws.BelasteReservesApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == BelasteReservesApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> BelasteReservesAppAanpassingBTReservesItemValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBelasteReservesAppAanpassingBTReservesItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            ws.BelasteReservesApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == BelasteReservesApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        #endregion



        #region Add/Remove


        public EY.com.eBook.RuleEngine.AY2009.BelasteReservesAppTypes.BelasteReserves BelasteReservesAppAllBelasteReservesAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBelasteReservesAppBelasteReservesItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            BelasteReservesApp rp = ws.BelasteReservesApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToAllBelasteReserves(cedc.Entity, ref ws);
            set.Calculate("BelasteReservesApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.BelasteReservesAppTypes.BelasteReserves BelasteReservesAppAllBelasteReservesRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            BelasteReservesApp rp = ws.BelasteReservesApp;
            rp.DeleteAllBelasteReserves(cfrdc.RowId, ref ws);
            set.Calculate("BelasteReservesApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.BelasteReservesAppTypes.BelasteReserves BelasteReservesAppAllBelasteReservesUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBelasteReservesAppBelasteReservesItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            BelasteReservesApp rp = ws.BelasteReservesApp;
            rp.UpdateAllBelasteReserves(cedc.Entity, ref ws);
            set.Calculate("BelasteReservesApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.BelasteReservesAppTypes.BelasteReserves BelasteReservesAppAanpassingBTReservesAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBelasteReservesAppAanpassingBTReservesItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            BelasteReservesApp rp = ws.BelasteReservesApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToAanpassingBTReserves(cedc.Entity, ref ws);
            set.Calculate("BelasteReservesApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.BelasteReservesAppTypes.BelasteReserves BelasteReservesAppAanpassingBTReservesRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            BelasteReservesApp rp = ws.BelasteReservesApp;
            rp.DeleteAanpassingBTReserves(cfrdc.RowId, ref ws);
            set.Calculate("BelasteReservesApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.BelasteReservesAppTypes.BelasteReserves BelasteReservesAppAanpassingBTReservesUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBelasteReservesAppAanpassingBTReservesItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            BelasteReservesApp rp = ws.BelasteReservesApp;
            rp.UpdateAanpassingBTReserves(cedc.Entity, ref ws);
            set.Calculate("BelasteReservesApp");
            return rp.Data;
        }

        #endregion

        #endregion



        #region BelastingVrijeReservesApp

        public EY.com.eBook.RuleEngine.AY2009.BelastingVrijeReservesAppTypes.BelastingVrijeReserves BelastingVrijeReservesAppGetData(CriteriaFileDataContract cfdc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cfdc.FileId);

            return set.DataContainer.BelastingVrijeReservesApp.Data;
        }


        public EY.com.eBook.RuleEngine.AY2009.BelastingVrijeReservesAppTypes.BelastingVrijeReserves BelastingVrijeReservesAppCalculate(CriteriaFileDataContract cfdc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cfdc.FileId);
            set.Calculate("BelastingVrijeReservesApp");

            return set.DataContainer.BelastingVrijeReservesApp.Data;
        }

        #region Entity validation


        public List<FileMessageDataContract> BelastingVrijeReservesAppDetailValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBelastingVrijeReservesAppDetailDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            ws.BelastingVrijeReservesApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == BelastingVrijeReservesApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        #endregion



        #region Add/Remove



        public EY.com.eBook.RuleEngine.AY2009.BelastingVrijeReservesAppTypes.BelastingVrijeReserves BelastingVrijeReservesAppVrijgesteldeReservesUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBelastingVrijeReservesAppDetailDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            BelastingVrijeReservesApp rp = ws.BelastingVrijeReservesApp;
            rp.UpdateVrijgesteldeReserves(cedc.Entity, ref ws);
            set.Calculate("BelastingVrijeReservesApp");
            return rp.Data;
        }


        #endregion

        #endregion



        #region BepalingBelastbareBasisApp

        public EY.com.eBook.RuleEngine.AY2009.BepalingBelastbareBasisAppTypes.BepalingBasis BepalingBelastbareBasisAppGetData(CriteriaFileDataContract cfdc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cfdc.FileId);

            return set.DataContainer.BepalingBelastbareBasisApp.Data;
        }


        public EY.com.eBook.RuleEngine.AY2009.BepalingBelastbareBasisAppTypes.BepalingBasis BepalingBelastbareBasisAppCalculate(CriteriaFileDataContract cfdc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cfdc.FileId);
            set.Calculate("BepalingBelastbareBasisApp");

            return set.DataContainer.BepalingBelastbareBasisApp.Data;
        }

        #region Entity validation


        public List<FileMessageDataContract> BepalingBelastbareBasisAppGeneralValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBepalingBelastbareBasisAppGeneralDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            ws.BepalingBelastbareBasisApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == BepalingBelastbareBasisApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> BepalingBelastbareBasisAppBelasteReservesItemValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBepalingBelastbareBasisAppBelasteReservesItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            ws.BepalingBelastbareBasisApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == BepalingBelastbareBasisApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> BepalingBelastbareBasisAppVerworpenUitgavenItemValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBepalingBelastbareBasisAppVerworpenUitgavenItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            ws.BepalingBelastbareBasisApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == BepalingBelastbareBasisApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> BepalingBelastbareBasisAppBelastbareBestZonderAftrekItemValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBepalingBelastbareBasisAppBelastbareBestZonderAftrekItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            ws.BepalingBelastbareBasisApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == BepalingBelastbareBasisApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> BepalingBelastbareBasisAppOpdelingNaarOorsprongItemValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBepalingBelastbareBasisAppOpdelingNaarOorsprongItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            ws.BepalingBelastbareBasisApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == BepalingBelastbareBasisApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> BepalingBelastbareBasisAppFiscaleAftrekItemValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBepalingBelastbareBasisAppFiscaleAftrekItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            ws.BepalingBelastbareBasisApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == BepalingBelastbareBasisApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        #endregion



        #region Add/Remove



        public EY.com.eBook.RuleEngine.AY2009.BepalingBelastbareBasisAppTypes.BepalingBasis BepalingBelastbareBasisAppOverzichtUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBepalingBelastbareBasisAppGeneralDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            BepalingBelastbareBasisApp rp = ws.BepalingBelastbareBasisApp;
            rp.UpdateOverzicht(cedc.Entity, ref ws);
            set.Calculate("BepalingBelastbareBasisApp");
            return rp.Data;
        }



        public EY.com.eBook.RuleEngine.AY2009.BepalingBelastbareBasisAppTypes.BepalingBasis BepalingBelastbareBasisAppBelasteReservesUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBepalingBelastbareBasisAppBelasteReservesItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            BepalingBelastbareBasisApp rp = ws.BepalingBelastbareBasisApp;
            rp.UpdateBelasteReserves(cedc.Entity, ref ws);
            set.Calculate("BepalingBelastbareBasisApp");
            return rp.Data;
        }



        public EY.com.eBook.RuleEngine.AY2009.BepalingBelastbareBasisAppTypes.BepalingBasis BepalingBelastbareBasisAppVerworpenUitgavenUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBepalingBelastbareBasisAppVerworpenUitgavenItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            BepalingBelastbareBasisApp rp = ws.BepalingBelastbareBasisApp;
            rp.UpdateVerworpenUitgaven(cedc.Entity, ref ws);
            set.Calculate("BepalingBelastbareBasisApp");
            return rp.Data;
        }



        public EY.com.eBook.RuleEngine.AY2009.BepalingBelastbareBasisAppTypes.BepalingBasis BepalingBelastbareBasisAppBelastbareBestZonderAftrekUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBepalingBelastbareBasisAppBelastbareBestZonderAftrekItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            BepalingBelastbareBasisApp rp = ws.BepalingBelastbareBasisApp;
            rp.UpdateBelastbareBestZonderAftrek(cedc.Entity, ref ws);
            set.Calculate("BepalingBelastbareBasisApp");
            return rp.Data;
        }



        public EY.com.eBook.RuleEngine.AY2009.BepalingBelastbareBasisAppTypes.BepalingBasis BepalingBelastbareBasisAppOpdelingNaarOorsprongUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBepalingBelastbareBasisAppOpdelingNaarOorsprongItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            BepalingBelastbareBasisApp rp = ws.BepalingBelastbareBasisApp;
            rp.UpdateOpdelingNaarOorsprong(cedc.Entity, ref ws);
            set.Calculate("BepalingBelastbareBasisApp");
            return rp.Data;
        }



        public EY.com.eBook.RuleEngine.AY2009.BepalingBelastbareBasisAppTypes.BepalingBasis BepalingBelastbareBasisAppFiscaleAftrekUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBepalingBelastbareBasisAppFiscaleAftrekItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            BepalingBelastbareBasisApp rp = ws.BepalingBelastbareBasisApp;
            rp.UpdateFiscaleAftrek(cedc.Entity, ref ws);
            set.Calculate("BepalingBelastbareBasisApp");
            return rp.Data;
        }


        #endregion

        #endregion



        #region DBIApp

        public EY.com.eBook.RuleEngine.AY2009.DBIAppTypes.DBIWorksheet DBIAppGetData(CriteriaFileDataContract cfdc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cfdc.FileId);

            return set.DataContainer.DBIApp.Data;
        }


        public EY.com.eBook.RuleEngine.AY2009.DBIAppTypes.DBIWorksheet DBIAppCalculate(CriteriaFileDataContract cfdc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cfdc.FileId);
            set.Calculate("DBIApp");

            return set.DataContainer.DBIApp.Data;
        }

        #region Entity validation


        public List<FileMessageDataContract> DBIAppParticipatieItemValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaDBIAppParticipatieItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            ws.DBIApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == DBIApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> DBIAppDividendItemValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaDBIAppDividendItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            ws.DBIApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == DBIApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        #endregion



        #region Add/Remove


        public EY.com.eBook.RuleEngine.AY2009.DBIAppTypes.DBIWorksheet DBIAppParticipationsAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaDBIAppParticipatieItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            DBIApp rp = ws.DBIApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToParticipations(cedc.Entity, ref ws);
            set.Calculate("DBIApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.DBIAppTypes.DBIWorksheet DBIAppParticipationsRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            DBIApp rp = ws.DBIApp;
            rp.DeleteParticipations(cfrdc.RowId, ref ws);
            set.Calculate("DBIApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.DBIAppTypes.DBIWorksheet DBIAppParticipationsUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaDBIAppParticipatieItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            DBIApp rp = ws.DBIApp;
            rp.UpdateParticipations(cedc.Entity, ref ws);
            set.Calculate("DBIApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.DBIAppTypes.DBIWorksheet DBIAppDividendsAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaDBIAppDividendItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            DBIApp rp = ws.DBIApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToDividends(cedc.Entity, ref ws);
            set.Calculate("DBIApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.DBIAppTypes.DBIWorksheet DBIAppDividendsRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            DBIApp rp = ws.DBIApp;
            rp.DeleteDividends(cfrdc.RowId, ref ws);
            set.Calculate("DBIApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.DBIAppTypes.DBIWorksheet DBIAppDividendsUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaDBIAppDividendItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            DBIApp rp = ws.DBIApp;
            rp.UpdateDividends(cedc.Entity, ref ws);
            set.Calculate("DBIApp");
            return rp.Data;
        }

        #endregion

        #endregion



        #region RisicoKapitaalApp

        public EY.com.eBook.RuleEngine.AY2009.RisicoKapitaalAppTypes.WPRisicokapitaal RisicoKapitaalAppGetData(CriteriaFileDataContract cfdc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cfdc.FileId);

            return set.DataContainer.RisicoKapitaalApp.Data;
        }


        public EY.com.eBook.RuleEngine.AY2009.RisicoKapitaalAppTypes.WPRisicokapitaal RisicoKapitaalAppCalculate(CriteriaFileDataContract cfdc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cfdc.FileId);
            set.Calculate("RisicoKapitaalApp");

            return set.DataContainer.RisicoKapitaalApp.Data;
        }

        #region Entity validation


        public List<FileMessageDataContract> RisicoKapitaalAppFicheItemValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaRisicoKapitaalAppFicheItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            ws.RisicoKapitaalApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == RisicoKapitaalApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        #endregion



        #region Add/Remove



        public EY.com.eBook.RuleEngine.AY2009.RisicoKapitaalAppTypes.WPRisicokapitaal RisicoKapitaalAppFicheUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaRisicoKapitaalAppFicheItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            RisicoKapitaalApp rp = ws.RisicoKapitaalApp;
            rp.UpdateFiche(cedc.Entity, ref ws);
            set.Calculate("RisicoKapitaalApp");
            return rp.Data;
        }


        #endregion

        #endregion



        #region TransferTaxCreditsApp

        public EY.com.eBook.RuleEngine.AY2009.TransferTaxCreditsAppTypes.TransferTaxCredits TransferTaxCreditsAppGetData(CriteriaFileDataContract cfdc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cfdc.FileId);

            return set.DataContainer.TransferTaxCreditsApp.Data;
        }


        public EY.com.eBook.RuleEngine.AY2009.TransferTaxCreditsAppTypes.TransferTaxCredits TransferTaxCreditsAppCalculate(CriteriaFileDataContract cfdc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cfdc.FileId);
            set.Calculate("TransferTaxCreditsApp");

            return set.DataContainer.TransferTaxCreditsApp.Data;
        }

        #region Entity validation


        public List<FileMessageDataContract> TransferTaxCreditsAppFiscaalVerliesValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaTransferTaxCreditsAppFiscaalVerliesDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            ws.TransferTaxCreditsApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == TransferTaxCreditsApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> TransferTaxCreditsAppNIDItemValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaTransferTaxCreditsAppNIDItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            ws.TransferTaxCreditsApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == TransferTaxCreditsApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> TransferTaxCreditsAppDBIItemValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaTransferTaxCreditsAppDBIItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            ws.TransferTaxCreditsApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == TransferTaxCreditsApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> TransferTaxCreditsAppInvestItemValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaTransferTaxCreditsAppInvestItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            ws.TransferTaxCreditsApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == TransferTaxCreditsApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> TransferTaxCreditsAppBelastingkredietItemValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaTransferTaxCreditsAppBelastingkredietItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            ws.TransferTaxCreditsApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == TransferTaxCreditsApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> TransferTaxCreditsAppAanpassingItemValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaTransferTaxCreditsAppAanpassingItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            ws.TransferTaxCreditsApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == TransferTaxCreditsApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        #endregion



        #region Add/Remove


        public EY.com.eBook.RuleEngine.AY2009.TransferTaxCreditsAppTypes.TransferTaxCredits TransferTaxCreditsAppFiscaleVerliezenAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaTransferTaxCreditsAppFiscaalVerliesDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            TransferTaxCreditsApp rp = ws.TransferTaxCreditsApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToFiscaleVerliezen(cedc.Entity, ref ws);
            set.Calculate("TransferTaxCreditsApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.TransferTaxCreditsAppTypes.TransferTaxCredits TransferTaxCreditsAppFiscaleVerliezenRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            TransferTaxCreditsApp rp = ws.TransferTaxCreditsApp;
            rp.DeleteFiscaleVerliezen(cfrdc.RowId, ref ws);
            set.Calculate("TransferTaxCreditsApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.TransferTaxCreditsAppTypes.TransferTaxCredits TransferTaxCreditsAppFiscaleVerliezenUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaTransferTaxCreditsAppFiscaalVerliesDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            TransferTaxCreditsApp rp = ws.TransferTaxCreditsApp;
            rp.UpdateFiscaleVerliezen(cedc.Entity, ref ws);
            set.Calculate("TransferTaxCreditsApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.TransferTaxCreditsAppTypes.TransferTaxCredits TransferTaxCreditsAppInvestAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaTransferTaxCreditsAppInvestItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            TransferTaxCreditsApp rp = ws.TransferTaxCreditsApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToInvest(cedc.Entity, ref ws);
            set.Calculate("TransferTaxCreditsApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.TransferTaxCreditsAppTypes.TransferTaxCredits TransferTaxCreditsAppInvestRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            TransferTaxCreditsApp rp = ws.TransferTaxCreditsApp;
            rp.DeleteInvest(cfrdc.RowId, ref ws);
            set.Calculate("TransferTaxCreditsApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.TransferTaxCreditsAppTypes.TransferTaxCredits TransferTaxCreditsAppInvestUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaTransferTaxCreditsAppInvestItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            TransferTaxCreditsApp rp = ws.TransferTaxCreditsApp;
            rp.UpdateInvest(cedc.Entity, ref ws);
            set.Calculate("TransferTaxCreditsApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.TransferTaxCreditsAppTypes.TransferTaxCredits TransferTaxCreditsAppBelastingkredietAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaTransferTaxCreditsAppBelastingkredietItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            TransferTaxCreditsApp rp = ws.TransferTaxCreditsApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToBelastingkrediet(cedc.Entity, ref ws);
            set.Calculate("TransferTaxCreditsApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.TransferTaxCreditsAppTypes.TransferTaxCredits TransferTaxCreditsAppBelastingkredietRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            TransferTaxCreditsApp rp = ws.TransferTaxCreditsApp;
            rp.DeleteBelastingkrediet(cfrdc.RowId, ref ws);
            set.Calculate("TransferTaxCreditsApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.TransferTaxCreditsAppTypes.TransferTaxCredits TransferTaxCreditsAppBelastingkredietUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaTransferTaxCreditsAppBelastingkredietItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            TransferTaxCreditsApp rp = ws.TransferTaxCreditsApp;
            rp.UpdateBelastingkrediet(cedc.Entity, ref ws);
            set.Calculate("TransferTaxCreditsApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.TransferTaxCreditsAppTypes.TransferTaxCredits TransferTaxCreditsAppAanpassingenAdd(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaTransferTaxCreditsAppAanpassingItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            TransferTaxCreditsApp rp = ws.TransferTaxCreditsApp;
            cedc.Entity.Id = Guid.NewGuid();
            rp.AddToAanpassingen(cedc.Entity, ref ws);
            set.Calculate("TransferTaxCreditsApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.TransferTaxCreditsAppTypes.TransferTaxCredits TransferTaxCreditsAppAanpassingenRemove(CriteriaFileAndRowDataContract cfrdc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cfrdc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            TransferTaxCreditsApp rp = ws.TransferTaxCreditsApp;
            rp.DeleteAanpassingen(cfrdc.RowId, ref ws);
            set.Calculate("TransferTaxCreditsApp");
            return rp.Data;
        }

        public EY.com.eBook.RuleEngine.AY2009.TransferTaxCreditsAppTypes.TransferTaxCredits TransferTaxCreditsAppAanpassingenUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaTransferTaxCreditsAppAanpassingItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            TransferTaxCreditsApp rp = ws.TransferTaxCreditsApp;
            rp.UpdateAanpassingen(cedc.Entity, ref ws);
            set.Calculate("TransferTaxCreditsApp");
            return rp.Data;
        }

        #endregion

        #endregion



        #region BerekeningVenBApp

        public EY.com.eBook.RuleEngine.AY2009.BerekeningVenBAppTypes.BerekeningVenB BerekeningVenBAppGetData(CriteriaFileDataContract cfdc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cfdc.FileId);

            return set.DataContainer.BerekeningVenBApp.Data;
        }


        public EY.com.eBook.RuleEngine.AY2009.BerekeningVenBAppTypes.BerekeningVenB BerekeningVenBAppCalculate(CriteriaFileDataContract cfdc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cfdc.FileId);
            set.Calculate("BerekeningVenBApp");

            return set.DataContainer.BerekeningVenBApp.Data;
        }

        #region Entity validation


        public List<FileMessageDataContract> BerekeningVenBAppGeneralValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBerekeningVenBAppGeneralDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            ws.BerekeningVenBApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == BerekeningVenBApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> BerekeningVenBAppBelasteReservesItemValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBerekeningVenBAppBelasteReservesItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            ws.BerekeningVenBApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == BerekeningVenBApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> BerekeningVenBAppVerworpenUitgavenItemValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBerekeningVenBAppVerworpenUitgavenItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            ws.BerekeningVenBApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == BerekeningVenBApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> BerekeningVenBAppBelastbareBestZonderAftrekItemValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBerekeningVenBAppBelastbareBestZonderAftrekItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            ws.BerekeningVenBApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == BerekeningVenBApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> BerekeningVenBAppOpdelingNaarOorsprongItemValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBerekeningVenBAppOpdelingNaarOorsprongItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            ws.BerekeningVenBApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == BerekeningVenBApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> BerekeningVenBAppFiscaleAftrekItemValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBerekeningVenBAppFiscaleAftrekItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            ws.BerekeningVenBApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == BerekeningVenBApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> BerekeningVenBAppTariefItemValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBerekeningVenBAppTariefItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            ws.BerekeningVenBApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == BerekeningVenBApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> BerekeningVenBAppBelastingsBerekeningItemValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBerekeningVenBAppBelastingsBerekeningItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            ws.BerekeningVenBApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == BerekeningVenBApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> BerekeningVenBAppBelastingsBerekeningItem2Validate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBerekeningVenBAppBelastingsBerekeningItem2DataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            ws.BerekeningVenBApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == BerekeningVenBApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        #endregion



        #region Add/Remove



        public EY.com.eBook.RuleEngine.AY2009.BerekeningVenBAppTypes.BerekeningVenB BerekeningVenBAppOverzichtUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBerekeningVenBAppGeneralDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            BerekeningVenBApp rp = ws.BerekeningVenBApp;
            rp.UpdateOverzicht(cedc.Entity, ref ws);
            set.Calculate("BerekeningVenBApp");
            return rp.Data;
        }



        public EY.com.eBook.RuleEngine.AY2009.BerekeningVenBAppTypes.BerekeningVenB BerekeningVenBAppBelasteReservesUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBerekeningVenBAppBelasteReservesItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            BerekeningVenBApp rp = ws.BerekeningVenBApp;
            rp.UpdateBelasteReserves(cedc.Entity, ref ws);
            set.Calculate("BerekeningVenBApp");
            return rp.Data;
        }



        public EY.com.eBook.RuleEngine.AY2009.BerekeningVenBAppTypes.BerekeningVenB BerekeningVenBAppVerworpenUitgavenUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBerekeningVenBAppVerworpenUitgavenItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            BerekeningVenBApp rp = ws.BerekeningVenBApp;
            rp.UpdateVerworpenUitgaven(cedc.Entity, ref ws);
            set.Calculate("BerekeningVenBApp");
            return rp.Data;
        }



        public EY.com.eBook.RuleEngine.AY2009.BerekeningVenBAppTypes.BerekeningVenB BerekeningVenBAppBelastbareBestZonderAftrekUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBerekeningVenBAppBelastbareBestZonderAftrekItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            BerekeningVenBApp rp = ws.BerekeningVenBApp;
            rp.UpdateBelastbareBestZonderAftrek(cedc.Entity, ref ws);
            set.Calculate("BerekeningVenBApp");
            return rp.Data;
        }



        public EY.com.eBook.RuleEngine.AY2009.BerekeningVenBAppTypes.BerekeningVenB BerekeningVenBAppOpdelingNaarOorsprongUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBerekeningVenBAppOpdelingNaarOorsprongItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            BerekeningVenBApp rp = ws.BerekeningVenBApp;
            rp.UpdateOpdelingNaarOorsprong(cedc.Entity, ref ws);
            set.Calculate("BerekeningVenBApp");
            return rp.Data;
        }



        public EY.com.eBook.RuleEngine.AY2009.BerekeningVenBAppTypes.BerekeningVenB BerekeningVenBAppFiscaleAftrekUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBerekeningVenBAppFiscaleAftrekItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            BerekeningVenBApp rp = ws.BerekeningVenBApp;
            rp.UpdateFiscaleAftrek(cedc.Entity, ref ws);
            set.Calculate("BerekeningVenBApp");
            return rp.Data;
        }



        public EY.com.eBook.RuleEngine.AY2009.BerekeningVenBAppTypes.BerekeningVenB BerekeningVenBAppTariefUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBerekeningVenBAppTariefItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            BerekeningVenBApp rp = ws.BerekeningVenBApp;
            rp.UpdateTarief(cedc.Entity, ref ws);
            set.Calculate("BerekeningVenBApp");
            return rp.Data;
        }



        public EY.com.eBook.RuleEngine.AY2009.BerekeningVenBAppTypes.BerekeningVenB BerekeningVenBAppBelastingsBerekeningUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBerekeningVenBAppBelastingsBerekeningItemDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            BerekeningVenBApp rp = ws.BerekeningVenBApp;
            rp.UpdateBelastingsBerekening(cedc.Entity, ref ws);
            set.Calculate("BerekeningVenBApp");
            return rp.Data;
        }



        public EY.com.eBook.RuleEngine.AY2009.BerekeningVenBAppTypes.BerekeningVenB BerekeningVenBAppBelastingsBerekening2Update(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaBerekeningVenBAppBelastingsBerekeningItem2DataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            BerekeningVenBApp rp = ws.BerekeningVenBApp;
            rp.UpdateBelastingsBerekening2(cedc.Entity, ref ws);
            set.Calculate("BerekeningVenBApp");
            return rp.Data;
        }


        #endregion

        #endregion



        #region AangifteVenBApp

        public EY.com.eBook.RuleEngine.AY2009.AangifteVenBAppTypes.AangifteVenB AangifteVenBAppGetData(CriteriaFileDataContract cfdc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cfdc.FileId);

            return set.DataContainer.AangifteVenBApp.Data;
        }


        public EY.com.eBook.RuleEngine.AY2009.AangifteVenBAppTypes.AangifteVenB AangifteVenBAppCalculate(CriteriaFileDataContract cfdc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cfdc.FileId);
            set.Calculate("AangifteVenBApp");

            return set.DataContainer.AangifteVenBApp.Data;
        }

        #region Entity validation


        public List<FileMessageDataContract> AangifteVenBAppReserveEntityValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaAangifteVenBAppReserveEntityDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            ws.AangifteVenBApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == AangifteVenBApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> AangifteVenBAppVrijgesteldeReserveEntityValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaAangifteVenBAppVrijgesteldeReserveEntityDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            ws.AangifteVenBApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == AangifteVenBApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> AangifteVenBAppVerworpenUitgavenEntityValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaAangifteVenBAppVerworpenUitgavenEntityDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            ws.AangifteVenBApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == AangifteVenBApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> AangifteVenBAppUitgekeerdeDividendenEntityValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaAangifteVenBAppUitgekeerdeDividendenEntityDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            ws.AangifteVenBApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == AangifteVenBApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> AangifteVenBAppWinstEntityValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaAangifteVenBAppWinstEntityDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            ws.AangifteVenBApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == AangifteVenBApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> AangifteVenBAppAfzonderlijkeAanslagenEntityValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaAangifteVenBAppAfzonderlijkeAanslagenEntityDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            ws.AangifteVenBApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == AangifteVenBApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> AangifteVenBAppDiamantEntityValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaAangifteVenBAppDiamantEntityDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            ws.AangifteVenBApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == AangifteVenBApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> AangifteVenBAppDbiEntityValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaAangifteVenBAppDbiEntityDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            ws.AangifteVenBApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == AangifteVenBApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> AangifteVenBAppOverdrachtDBIAftrekEntityValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaAangifteVenBAppOverdrachtDBIAftrekEntityDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            ws.AangifteVenBApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == AangifteVenBApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> AangifteVenBAppOverdrachtNIDEntityValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaAangifteVenBAppOverdrachtNIDEntityDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            ws.AangifteVenBApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == AangifteVenBApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> AangifteVenBAppCompenseerbareVerliezenEntityValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaAangifteVenBAppCompenseerbareVerliezenEntityDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            ws.AangifteVenBApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == AangifteVenBApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> AangifteVenBAppTariefEntityValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaAangifteVenBAppTariefEntityDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            ws.AangifteVenBApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == AangifteVenBApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> AangifteVenBAppVoorafBetalingEntityValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaAangifteVenBAppVoorafBetalingEntityDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            ws.AangifteVenBApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == AangifteVenBApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> AangifteVenBAppVerrekenbareVoorheffingEntityValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaAangifteVenBAppVerrekenbareVoorheffingEntityDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            ws.AangifteVenBApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == AangifteVenBApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> AangifteVenBAppTaxShelterEntityValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaAangifteVenBAppTaxShelterEntityDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            ws.AangifteVenBApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == AangifteVenBApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> AangifteVenBAppGrootteVennootschapeEntityValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaAangifteVenBAppGrootteVennootschapeEntityDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            ws.AangifteVenBApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == AangifteVenBApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> AangifteVenBAppOpgavenEntityValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaAangifteVenBAppOpgavenEntityDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            ws.AangifteVenBApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == AangifteVenBApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        public List<FileMessageDataContract> AangifteVenBAppBankEntityValidate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaAangifteVenBAppBankEntityDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            ws.AangifteVenBApp.Validate(cedc.Entity, cedc.CollectionPath, ref ws);
            List<FileMessageDataContract> msgs = ws.Messages.Where(m => m.ConnectionGuid == AangifteVenBApp.TypeId && m.Type == MessageTypeDataContract.Error && m.WorksheetTypeRowId == cedc.Entity.Id && m.Culture == cedc.Culture).ToList();
            return msgs;
        }



        #endregion



        #region Add/Remove



        public EY.com.eBook.RuleEngine.AY2009.AangifteVenBAppTypes.AangifteVenB AangifteVenBAppReservesUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaAangifteVenBAppReserveEntityDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            AangifteVenBApp rp = ws.AangifteVenBApp;
            rp.UpdateReserves(cedc.Entity, ref ws);
            set.Calculate("AangifteVenBApp");
            return rp.Data;
        }



        public EY.com.eBook.RuleEngine.AY2009.AangifteVenBAppTypes.AangifteVenB AangifteVenBAppVrijgesteldeReserveUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaAangifteVenBAppVrijgesteldeReserveEntityDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            AangifteVenBApp rp = ws.AangifteVenBApp;
            rp.UpdateVrijgesteldeReserve(cedc.Entity, ref ws);
            set.Calculate("AangifteVenBApp");
            return rp.Data;
        }



        public EY.com.eBook.RuleEngine.AY2009.AangifteVenBAppTypes.AangifteVenB AangifteVenBAppVerworpenUitgavenUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaAangifteVenBAppVerworpenUitgavenEntityDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            AangifteVenBApp rp = ws.AangifteVenBApp;
            rp.UpdateVerworpenUitgaven(cedc.Entity, ref ws);
            set.Calculate("AangifteVenBApp");
            return rp.Data;
        }



        public EY.com.eBook.RuleEngine.AY2009.AangifteVenBAppTypes.AangifteVenB AangifteVenBAppUitgekeerdeDividendenUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaAangifteVenBAppUitgekeerdeDividendenEntityDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            AangifteVenBApp rp = ws.AangifteVenBApp;
            rp.UpdateUitgekeerdeDividenden(cedc.Entity, ref ws);
            set.Calculate("AangifteVenBApp");
            return rp.Data;
        }



        public EY.com.eBook.RuleEngine.AY2009.AangifteVenBAppTypes.AangifteVenB AangifteVenBAppUiteenzettingWinstUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaAangifteVenBAppWinstEntityDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            AangifteVenBApp rp = ws.AangifteVenBApp;
            rp.UpdateUiteenzettingWinst(cedc.Entity, ref ws);
            set.Calculate("AangifteVenBApp");
            return rp.Data;
        }



        public EY.com.eBook.RuleEngine.AY2009.AangifteVenBAppTypes.AangifteVenB AangifteVenBAppAfzonderlijkeAanslagenUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaAangifteVenBAppAfzonderlijkeAanslagenEntityDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            AangifteVenBApp rp = ws.AangifteVenBApp;
            rp.UpdateAfzonderlijkeAanslagen(cedc.Entity, ref ws);
            set.Calculate("AangifteVenBApp");
            return rp.Data;
        }



        public EY.com.eBook.RuleEngine.AY2009.AangifteVenBAppTypes.AangifteVenB AangifteVenBAppDiamantUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaAangifteVenBAppDiamantEntityDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            AangifteVenBApp rp = ws.AangifteVenBApp;
            rp.UpdateDiamant(cedc.Entity, ref ws);
            set.Calculate("AangifteVenBApp");
            return rp.Data;
        }



        public EY.com.eBook.RuleEngine.AY2009.AangifteVenBAppTypes.AangifteVenB AangifteVenBAppDBIUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaAangifteVenBAppDbiEntityDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            AangifteVenBApp rp = ws.AangifteVenBApp;
            rp.UpdateDBI(cedc.Entity, ref ws);
            set.Calculate("AangifteVenBApp");
            return rp.Data;
        }



        public EY.com.eBook.RuleEngine.AY2009.AangifteVenBAppTypes.AangifteVenB AangifteVenBAppOverdrachtDBIAftrekUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaAangifteVenBAppOverdrachtDBIAftrekEntityDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            AangifteVenBApp rp = ws.AangifteVenBApp;
            rp.UpdateOverdrachtDBIAftrek(cedc.Entity, ref ws);
            set.Calculate("AangifteVenBApp");
            return rp.Data;
        }



        public EY.com.eBook.RuleEngine.AY2009.AangifteVenBAppTypes.AangifteVenB AangifteVenBAppOverdrachtNIDUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaAangifteVenBAppOverdrachtNIDEntityDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            AangifteVenBApp rp = ws.AangifteVenBApp;
            rp.UpdateOverdrachtNID(cedc.Entity, ref ws);
            set.Calculate("AangifteVenBApp");
            return rp.Data;
        }



        public EY.com.eBook.RuleEngine.AY2009.AangifteVenBAppTypes.AangifteVenB AangifteVenBAppCompenseerbareVerliezenUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaAangifteVenBAppCompenseerbareVerliezenEntityDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            AangifteVenBApp rp = ws.AangifteVenBApp;
            rp.UpdateCompenseerbareVerliezen(cedc.Entity, ref ws);
            set.Calculate("AangifteVenBApp");
            return rp.Data;
        }



        public EY.com.eBook.RuleEngine.AY2009.AangifteVenBAppTypes.AangifteVenB AangifteVenBAppTariefBelastingUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaAangifteVenBAppTariefEntityDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            AangifteVenBApp rp = ws.AangifteVenBApp;
            rp.UpdateTariefBelasting(cedc.Entity, ref ws);
            set.Calculate("AangifteVenBApp");
            return rp.Data;
        }



        public EY.com.eBook.RuleEngine.AY2009.AangifteVenBAppTypes.AangifteVenB AangifteVenBAppVoorafBetalingenUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaAangifteVenBAppVoorafBetalingEntityDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            AangifteVenBApp rp = ws.AangifteVenBApp;
            rp.UpdateVoorafBetalingen(cedc.Entity, ref ws);
            set.Calculate("AangifteVenBApp");
            return rp.Data;
        }



        public EY.com.eBook.RuleEngine.AY2009.AangifteVenBAppTypes.AangifteVenB AangifteVenBAppVerrekenbareVoorheffingUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaAangifteVenBAppVerrekenbareVoorheffingEntityDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            AangifteVenBApp rp = ws.AangifteVenBApp;
            rp.UpdateVerrekenbareVoorheffing(cedc.Entity, ref ws);
            set.Calculate("AangifteVenBApp");
            return rp.Data;
        }



        public EY.com.eBook.RuleEngine.AY2009.AangifteVenBAppTypes.AangifteVenB AangifteVenBAppTaxShelterUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaAangifteVenBAppTaxShelterEntityDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            AangifteVenBApp rp = ws.AangifteVenBApp;
            rp.UpdateTaxShelter(cedc.Entity, ref ws);
            set.Calculate("AangifteVenBApp");
            return rp.Data;
        }



        public EY.com.eBook.RuleEngine.AY2009.AangifteVenBAppTypes.AangifteVenB AangifteVenBAppGrootteVennootschapUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaAangifteVenBAppGrootteVennootschapeEntityDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            AangifteVenBApp rp = ws.AangifteVenBApp;
            rp.UpdateGrootteVennootschap(cedc.Entity, ref ws);
            set.Calculate("AangifteVenBApp");
            return rp.Data;
        }



        public EY.com.eBook.RuleEngine.AY2009.AangifteVenBAppTypes.AangifteVenB AangifteVenBAppOpgaveBijlagenUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaAangifteVenBAppOpgavenEntityDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            AangifteVenBApp rp = ws.AangifteVenBApp;
            rp.UpdateOpgaveBijlagen(cedc.Entity, ref ws);
            set.Calculate("AangifteVenBApp");
            return rp.Data;
        }



        public EY.com.eBook.RuleEngine.AY2009.AangifteVenBAppTypes.AangifteVenB AangifteVenBAppBankUpdate(EY.com.eBook.RuleEngine.Data.AY2009.CriteriaAangifteVenBAppBankEntityDataContract cedc)
        {
            EY.com.eBook.RuleEngine.AY2009.WorksheetSet set = GetSet(cedc.Id);
            EY.com.eBook.RuleEngine.AY2009.WorksheetSetData ws = set.DataContainer;
            AangifteVenBApp rp = ws.AangifteVenBApp;
            rp.UpdateBank(cedc.Entity, ref ws);
            set.Calculate("AangifteVenBApp");
            return rp.Data;
        }


        #endregion

        #endregion



        #endregion

        #endregion


    }
}
