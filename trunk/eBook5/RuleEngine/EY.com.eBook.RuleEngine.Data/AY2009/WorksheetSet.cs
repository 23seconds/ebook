﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.API.Contracts.Data.Cache;
using EY.com.eBook.Caching;
using EY.com.eBook.RuleEngine.Data;
using EY.com.eBook.API.Contracts.Proxies;
using System.Runtime.Serialization;
using EY.com.eBook.RuleEngine.Core;
using EY.com.eBook.RuleEngine.Core.Interfaces;
using EY.com.eBook.API.BL;


namespace EY.com.eBook.RuleEngine.AY2009
{
    public class WorksheetSet
    {
        
        public WorksheetSetData DataContainer;
    
        public decimal? GetTaxCalculation()
        {
           // return DataContainer.BerekeningVenBApp.Data.BelastingsBerekening.OutstandingLiability_BelastingsBerekening;
            return 0;
        }
        
        public void Initialize(WorksheetLibrary wl)
        {
            DataContainer.Initialize(wl);
        }
        
        public void Save()
        {
            DataContainer.Save();
        }
        
        public void Calculate(string appName)
        {
            
                if(string.IsNullOrEmpty(appName) || appName=="FacturatieApp" || !DataContainer.FacturatieApp.Data.LastUpdated.HasValue) {
                    DataContainer.FacturatieApp.Calculate(ref DataContainer);
                }
                
                if(string.IsNullOrEmpty(appName) || appName=="OverlopendeRekeningenApp" || !DataContainer.OverlopendeRekeningenApp.Data.LastUpdated.HasValue) {
                    DataContainer.OverlopendeRekeningenApp.Calculate(ref DataContainer);
                }
                
                if(string.IsNullOrEmpty(appName) || appName=="VergelijkendeCijfersApp" || !DataContainer.VergelijkendeCijfersApp.Data.LastUpdated.HasValue) {
                    DataContainer.VergelijkendeCijfersApp.Calculate(ref DataContainer);
                }
                
                if((string.IsNullOrEmpty(appName) || appName=="VoorschottenApp" || !DataContainer.VoorschottenApp.Data.LastUpdated.HasValue)
                  || (DataContainer.BelasteReservesApp.Data.LastUpdated.HasValue && DataContainer.BelasteReservesApp.Data.LastUpdated.Value > DataContainer.VoorschottenApp.Data.LastUpdated.Value)
                  ) {
                      DataContainer.VoorschottenApp.Calculate(ref DataContainer);
                }
                
                if(string.IsNullOrEmpty(appName) || appName=="ErelonenHuurApp" || !DataContainer.ErelonenHuurApp.Data.LastUpdated.HasValue) {
                    DataContainer.ErelonenHuurApp.Calculate(ref DataContainer);
                }
                
                if(string.IsNullOrEmpty(appName) || appName=="VoordelenApp" || !DataContainer.VoordelenApp.Data.LastUpdated.HasValue) {
                    DataContainer.VoordelenApp.Calculate(ref DataContainer);
                }
                
                if(string.IsNullOrEmpty(appName) || appName=="PersoneelApp" || !DataContainer.PersoneelApp.Data.LastUpdated.HasValue) {
                    DataContainer.PersoneelApp.Calculate(ref DataContainer);
                }
                
                if(string.IsNullOrEmpty(appName) || appName=="BTWApp" || !DataContainer.BTWApp.Data.LastUpdated.HasValue) {
                    DataContainer.BTWApp.Calculate(ref DataContainer);
                }
                
                if(string.IsNullOrEmpty(appName) || appName=="RVIntrestenApp" || !DataContainer.RVIntrestenApp.Data.LastUpdated.HasValue) {
                    DataContainer.RVIntrestenApp.Calculate(ref DataContainer);
                }
                
                if(string.IsNullOrEmpty(appName) || appName=="DubieuzeDebiteurenApp" || !DataContainer.DubieuzeDebiteurenApp.Data.LastUpdated.HasValue) {
                    DataContainer.DubieuzeDebiteurenApp.Calculate(ref DataContainer);
                }
                
                if((string.IsNullOrEmpty(appName) || appName=="VerliesHandelsdebiteurenApp" || !DataContainer.VerliesHandelsdebiteurenApp.Data.LastUpdated.HasValue)
                  || (DataContainer.DubieuzeDebiteurenApp.Data.LastUpdated.HasValue && DataContainer.DubieuzeDebiteurenApp.Data.LastUpdated.Value > DataContainer.VerliesHandelsdebiteurenApp.Data.LastUpdated.Value)
                  ) {
                      DataContainer.VerliesHandelsdebiteurenApp.Calculate(ref DataContainer);
                }
                
                if(string.IsNullOrEmpty(appName) || appName=="VoorzieningenApp" || !DataContainer.VoorzieningenApp.Data.LastUpdated.HasValue) {
                    DataContainer.VoorzieningenApp.Calculate(ref DataContainer);
                }
                
                if(string.IsNullOrEmpty(appName) || appName=="BelastingenApp" || !DataContainer.BelastingenApp.Data.LastUpdated.HasValue) {
                    DataContainer.BelastingenApp.Calculate(ref DataContainer);
                }
                
                if((string.IsNullOrEmpty(appName) || appName=="BelasteReservesApp" || !DataContainer.BelasteReservesApp.Data.LastUpdated.HasValue)
                  || (DataContainer.BelastingenApp.Data.LastUpdated.HasValue && DataContainer.BelastingenApp.Data.LastUpdated.Value > DataContainer.BelasteReservesApp.Data.LastUpdated.Value)
                  ) {
                      DataContainer.BelasteReservesApp.Calculate(ref DataContainer);
                }
                
                if(string.IsNullOrEmpty(appName) || appName=="BelastingVrijeReservesApp" || !DataContainer.BelastingVrijeReservesApp.Data.LastUpdated.HasValue) {
                    DataContainer.BelastingVrijeReservesApp.Calculate(ref DataContainer);
                }
                
                if((string.IsNullOrEmpty(appName) || appName=="BepalingBelastbareBasisApp" || !DataContainer.BepalingBelastbareBasisApp.Data.LastUpdated.HasValue)
                  || (DataContainer.BelastingenApp.Data.LastUpdated.HasValue && DataContainer.BelastingenApp.Data.LastUpdated.Value > DataContainer.BepalingBelastbareBasisApp.Data.LastUpdated.Value)|| (DataContainer.BelasteReservesApp.Data.LastUpdated.HasValue && DataContainer.BelasteReservesApp.Data.LastUpdated.Value > DataContainer.BepalingBelastbareBasisApp.Data.LastUpdated.Value)
                  ) {
                      DataContainer.BepalingBelastbareBasisApp.Calculate(ref DataContainer);
                }
                
                if(string.IsNullOrEmpty(appName) || appName=="DBIApp" || !DataContainer.DBIApp.Data.LastUpdated.HasValue) {
                    DataContainer.DBIApp.Calculate(ref DataContainer);
                }
                
                if(string.IsNullOrEmpty(appName) || appName=="RisicoKapitaalApp" || !DataContainer.RisicoKapitaalApp.Data.LastUpdated.HasValue) {
                    DataContainer.RisicoKapitaalApp.Calculate(ref DataContainer);
                }
                
                if(string.IsNullOrEmpty(appName) || appName=="TransferTaxCreditsApp" || !DataContainer.TransferTaxCreditsApp.Data.LastUpdated.HasValue) {
                    DataContainer.TransferTaxCreditsApp.Calculate(ref DataContainer);
                }
                
                if((string.IsNullOrEmpty(appName) || appName=="BerekeningVenBApp" || !DataContainer.BerekeningVenBApp.Data.LastUpdated.HasValue)
                  || (DataContainer.BelastingenApp.Data.LastUpdated.HasValue && DataContainer.BelastingenApp.Data.LastUpdated.Value > DataContainer.BerekeningVenBApp.Data.LastUpdated.Value)|| (DataContainer.BelasteReservesApp.Data.LastUpdated.HasValue && DataContainer.BelasteReservesApp.Data.LastUpdated.Value > DataContainer.BerekeningVenBApp.Data.LastUpdated.Value)|| (DataContainer.RisicoKapitaalApp.Data.LastUpdated.HasValue && DataContainer.RisicoKapitaalApp.Data.LastUpdated.Value > DataContainer.BerekeningVenBApp.Data.LastUpdated.Value)|| (DataContainer.TransferTaxCreditsApp.Data.LastUpdated.HasValue && DataContainer.TransferTaxCreditsApp.Data.LastUpdated.Value > DataContainer.BerekeningVenBApp.Data.LastUpdated.Value)
                  ) {
                      DataContainer.BerekeningVenBApp.Calculate(ref DataContainer);
                }
                
                if((string.IsNullOrEmpty(appName) || appName=="AangifteVenBApp" || !DataContainer.AangifteVenBApp.Data.LastUpdated.HasValue)
                  || (DataContainer.BelasteReservesApp.Data.LastUpdated.HasValue && DataContainer.BelasteReservesApp.Data.LastUpdated.Value > DataContainer.AangifteVenBApp.Data.LastUpdated.Value)|| (DataContainer.BelastingVrijeReservesApp.Data.LastUpdated.HasValue && DataContainer.BelastingVrijeReservesApp.Data.LastUpdated.Value > DataContainer.AangifteVenBApp.Data.LastUpdated.Value)|| (DataContainer.DBIApp.Data.LastUpdated.HasValue && DataContainer.DBIApp.Data.LastUpdated.Value > DataContainer.AangifteVenBApp.Data.LastUpdated.Value)|| (DataContainer.BerekeningVenBApp.Data.LastUpdated.HasValue && DataContainer.BerekeningVenBApp.Data.LastUpdated.Value > DataContainer.AangifteVenBApp.Data.LastUpdated.Value)
                  ) {
                      DataContainer.AangifteVenBApp.Calculate(ref DataContainer);
                }
                
            DataContainer.Save();
        }
        
        public void ImportPrevious()
        {
        }
        
    }   
    
    public class WorksheetSetData : WorksheetSetDataBase
    {
        public DateTime LastSave;
        
        #region Worksheets
        
        public FacturatieApp FacturatieApp {get;set;}
        
        public OverlopendeRekeningenApp OverlopendeRekeningenApp {get;set;}
        
        public VergelijkendeCijfersApp VergelijkendeCijfersApp {get;set;}
        
        public VoorschottenApp VoorschottenApp {get;set;}
        
        public ErelonenHuurApp ErelonenHuurApp {get;set;}
        
        public VoordelenApp VoordelenApp {get;set;}
        
        public PersoneelApp PersoneelApp {get;set;}
        
        public BTWApp BTWApp {get;set;}
        
        public RVIntrestenApp RVIntrestenApp {get;set;}
        
        public DubieuzeDebiteurenApp DubieuzeDebiteurenApp {get;set;}
        
        public VerliesHandelsdebiteurenApp VerliesHandelsdebiteurenApp {get;set;}
        
        public VoorzieningenApp VoorzieningenApp {get;set;}
        
        public BelastingenApp BelastingenApp {get;set;}
        
        public BelasteReservesApp BelasteReservesApp {get;set;}
        
        public BelastingVrijeReservesApp BelastingVrijeReservesApp {get;set;}
        
        public BepalingBelastbareBasisApp BepalingBelastbareBasisApp {get;set;}
        
        public DBIApp DBIApp {get;set;}
        
        public RisicoKapitaalApp RisicoKapitaalApp {get;set;}
        
        public TransferTaxCreditsApp TransferTaxCreditsApp {get;set;}
        
        public BerekeningVenBApp BerekeningVenBApp {get;set;}
        
        public AangifteVenBApp AangifteVenBApp {get;set;}
        
        
        #endregion
    
        //public AY2011.WorksheetSetData PreviousSet { get; set; }
        
        public WorksheetLibrary GetCacheLibrary()
        {
            WorksheetLibrary wl = new WorksheetLibrary();
            
            
            wl.Add(FacturatieApp.TypeId,new WorksheetWrapper { WorksheetData =FacturatieApp.Data});
            
            wl.Add(OverlopendeRekeningenApp.TypeId,new WorksheetWrapper { WorksheetData =OverlopendeRekeningenApp.Data});
            
            wl.Add(VergelijkendeCijfersApp.TypeId,new WorksheetWrapper { WorksheetData =VergelijkendeCijfersApp.Data});
            
            wl.Add(VoorschottenApp.TypeId,new WorksheetWrapper { WorksheetData =VoorschottenApp.Data});
            
            wl.Add(ErelonenHuurApp.TypeId,new WorksheetWrapper { WorksheetData =ErelonenHuurApp.Data});
            
            wl.Add(VoordelenApp.TypeId,new WorksheetWrapper { WorksheetData =VoordelenApp.Data});
            
            wl.Add(PersoneelApp.TypeId,new WorksheetWrapper { WorksheetData =PersoneelApp.Data});
            
            wl.Add(BTWApp.TypeId,new WorksheetWrapper { WorksheetData =BTWApp.Data});
            
            wl.Add(RVIntrestenApp.TypeId,new WorksheetWrapper { WorksheetData =RVIntrestenApp.Data});
            
            wl.Add(DubieuzeDebiteurenApp.TypeId,new WorksheetWrapper { WorksheetData =DubieuzeDebiteurenApp.Data});
            
            wl.Add(VerliesHandelsdebiteurenApp.TypeId,new WorksheetWrapper { WorksheetData =VerliesHandelsdebiteurenApp.Data});
            
            wl.Add(VoorzieningenApp.TypeId,new WorksheetWrapper { WorksheetData =VoorzieningenApp.Data});
            
            wl.Add(BelastingenApp.TypeId,new WorksheetWrapper { WorksheetData =BelastingenApp.Data});
            
            wl.Add(BelasteReservesApp.TypeId,new WorksheetWrapper { WorksheetData =BelasteReservesApp.Data});
            
            wl.Add(BelastingVrijeReservesApp.TypeId,new WorksheetWrapper { WorksheetData =BelastingVrijeReservesApp.Data});
            
            wl.Add(BepalingBelastbareBasisApp.TypeId,new WorksheetWrapper { WorksheetData =BepalingBelastbareBasisApp.Data});
            
            wl.Add(DBIApp.TypeId,new WorksheetWrapper { WorksheetData =DBIApp.Data});
            
            wl.Add(RisicoKapitaalApp.TypeId,new WorksheetWrapper { WorksheetData =RisicoKapitaalApp.Data});
            
            wl.Add(TransferTaxCreditsApp.TypeId,new WorksheetWrapper { WorksheetData =TransferTaxCreditsApp.Data});
            
            wl.Add(BerekeningVenBApp.TypeId,new WorksheetWrapper { WorksheetData =BerekeningVenBApp.Data});
            
            wl.Add(AangifteVenBApp.TypeId,new WorksheetWrapper { WorksheetData =AangifteVenBApp.Data});
            
            wl.LastSave = LastSave;
            return wl;
        }
        
        public void LoadAllFromDatabase() 
        {
            if(LastSave==new DateTime(1980,1,1)) LastSave = DateTime.Now.AddMinutes(-1);
            bool save=false;
            Dictionary<Guid, string> ws = GetWorksheetDataFromDB();
            
            if(ws.ContainsKey(FacturatieApp.TypeId)) {
              FacturatieApp.LoadXml(ws[FacturatieApp.TypeId]);
            } 
            FacturatieApp.SetMinimal();
            
            if(ws.ContainsKey(OverlopendeRekeningenApp.TypeId)) {
              OverlopendeRekeningenApp.LoadXml(ws[OverlopendeRekeningenApp.TypeId]);
            } 
            OverlopendeRekeningenApp.SetMinimal();
            
            if(ws.ContainsKey(VergelijkendeCijfersApp.TypeId)) {
              VergelijkendeCijfersApp.LoadXml(ws[VergelijkendeCijfersApp.TypeId]);
            } 
            VergelijkendeCijfersApp.SetMinimal();
            
            if(ws.ContainsKey(VoorschottenApp.TypeId)) {
              VoorschottenApp.LoadXml(ws[VoorschottenApp.TypeId]);
            } 
            VoorschottenApp.SetMinimal();
            
            if(ws.ContainsKey(ErelonenHuurApp.TypeId)) {
              ErelonenHuurApp.LoadXml(ws[ErelonenHuurApp.TypeId]);
            } 
            ErelonenHuurApp.SetMinimal();
            
            if(ws.ContainsKey(VoordelenApp.TypeId)) {
              VoordelenApp.LoadXml(ws[VoordelenApp.TypeId]);
            } 
            VoordelenApp.SetMinimal();
            
            if(ws.ContainsKey(PersoneelApp.TypeId)) {
              PersoneelApp.LoadXml(ws[PersoneelApp.TypeId]);
            } 
            PersoneelApp.SetMinimal();
            
            if(ws.ContainsKey(BTWApp.TypeId)) {
              BTWApp.LoadXml(ws[BTWApp.TypeId]);
            } 
            BTWApp.SetMinimal();
            
            if(ws.ContainsKey(RVIntrestenApp.TypeId)) {
              RVIntrestenApp.LoadXml(ws[RVIntrestenApp.TypeId]);
            } 
            RVIntrestenApp.SetMinimal();
            
            if(ws.ContainsKey(DubieuzeDebiteurenApp.TypeId)) {
              DubieuzeDebiteurenApp.LoadXml(ws[DubieuzeDebiteurenApp.TypeId]);
            } 
            DubieuzeDebiteurenApp.SetMinimal();
            
            if(ws.ContainsKey(VerliesHandelsdebiteurenApp.TypeId)) {
              VerliesHandelsdebiteurenApp.LoadXml(ws[VerliesHandelsdebiteurenApp.TypeId]);
            } 
            VerliesHandelsdebiteurenApp.SetMinimal();
            
            if(ws.ContainsKey(VoorzieningenApp.TypeId)) {
              VoorzieningenApp.LoadXml(ws[VoorzieningenApp.TypeId]);
            } 
            VoorzieningenApp.SetMinimal();
            
            if(ws.ContainsKey(BelastingenApp.TypeId)) {
              BelastingenApp.LoadXml(ws[BelastingenApp.TypeId]);
            } 
            BelastingenApp.SetMinimal();
            
            if(ws.ContainsKey(BelasteReservesApp.TypeId)) {
              BelasteReservesApp.LoadXml(ws[BelasteReservesApp.TypeId]);
            } 
            BelasteReservesApp.SetMinimal();
            
            if(ws.ContainsKey(BelastingVrijeReservesApp.TypeId)) {
              BelastingVrijeReservesApp.LoadXml(ws[BelastingVrijeReservesApp.TypeId]);
            } 
            BelastingVrijeReservesApp.SetMinimal();
            
            if(ws.ContainsKey(BepalingBelastbareBasisApp.TypeId)) {
              BepalingBelastbareBasisApp.LoadXml(ws[BepalingBelastbareBasisApp.TypeId]);
            } 
            BepalingBelastbareBasisApp.SetMinimal();
            
            if(ws.ContainsKey(DBIApp.TypeId)) {
              DBIApp.LoadXml(ws[DBIApp.TypeId]);
            } 
            DBIApp.SetMinimal();
            
            if(ws.ContainsKey(RisicoKapitaalApp.TypeId)) {
              RisicoKapitaalApp.LoadXml(ws[RisicoKapitaalApp.TypeId]);
            } 
            RisicoKapitaalApp.SetMinimal();
            
            if(ws.ContainsKey(TransferTaxCreditsApp.TypeId)) {
              TransferTaxCreditsApp.LoadXml(ws[TransferTaxCreditsApp.TypeId]);
            } 
            TransferTaxCreditsApp.SetMinimal();
            
            if(ws.ContainsKey(BerekeningVenBApp.TypeId)) {
              BerekeningVenBApp.LoadXml(ws[BerekeningVenBApp.TypeId]);
            } 
            BerekeningVenBApp.SetMinimal();
            
            if(ws.ContainsKey(AangifteVenBApp.TypeId)) {
              AangifteVenBApp.LoadXml(ws[AangifteVenBApp.TypeId]);
            } 
            AangifteVenBApp.SetMinimal();
             
            Save();
        }
        
        public void Initialize(WorksheetLibrary wl)
        {
            FacturatieApp = new FacturatieApp();
            OverlopendeRekeningenApp = new OverlopendeRekeningenApp();
            VergelijkendeCijfersApp = new VergelijkendeCijfersApp();
            VoorschottenApp = new VoorschottenApp();
            ErelonenHuurApp = new ErelonenHuurApp();
            VoordelenApp = new VoordelenApp();
            PersoneelApp = new PersoneelApp();
            BTWApp = new BTWApp();
            RVIntrestenApp = new RVIntrestenApp();
            DubieuzeDebiteurenApp = new DubieuzeDebiteurenApp();
            VerliesHandelsdebiteurenApp = new VerliesHandelsdebiteurenApp();
            VoorzieningenApp = new VoorzieningenApp();
            BelastingenApp = new BelastingenApp();
            BelasteReservesApp = new BelasteReservesApp();
            BelastingVrijeReservesApp = new BelastingVrijeReservesApp();
            BepalingBelastbareBasisApp = new BepalingBelastbareBasisApp();
            DBIApp = new DBIApp();
            RisicoKapitaalApp = new RisicoKapitaalApp();
            TransferTaxCreditsApp = new TransferTaxCreditsApp();
            BerekeningVenBApp = new BerekeningVenBApp();
            AangifteVenBApp = new AangifteVenBApp();
            
            if(wl==null) {
                LoadAllFromDatabase();
            } else {
            
                if(wl.ContainsKey(FacturatieApp.TypeId)) {
                    FacturatieApp.Data = (EY.com.eBook.RuleEngine.AY2009.FacturatieAppTypes.Facturatie)wl[FacturatieApp.TypeId].WorksheetData;
                }
            
                if(wl.ContainsKey(OverlopendeRekeningenApp.TypeId)) {
                    OverlopendeRekeningenApp.Data = (EY.com.eBook.RuleEngine.AY2009.OverlopendeRekeningenAppTypes.OverlopendeRekeningen)wl[OverlopendeRekeningenApp.TypeId].WorksheetData;
                }
            
                if(wl.ContainsKey(VergelijkendeCijfersApp.TypeId)) {
                    VergelijkendeCijfersApp.Data = (EY.com.eBook.RuleEngine.AY2009.VergelijkendeCijfersAppTypes.WPVergelijkendeCijfers)wl[VergelijkendeCijfersApp.TypeId].WorksheetData;
                }
            
                if(wl.ContainsKey(VoorschottenApp.TypeId)) {
                    VoorschottenApp.Data = (EY.com.eBook.RuleEngine.AY2009.VoorschottenAppTypes.Voorschotten)wl[VoorschottenApp.TypeId].WorksheetData;
                }
            
                if(wl.ContainsKey(ErelonenHuurApp.TypeId)) {
                    ErelonenHuurApp.Data = (EY.com.eBook.RuleEngine.AY2009.ErelonenHuurAppTypes.ErelonenHuur)wl[ErelonenHuurApp.TypeId].WorksheetData;
                }
            
                if(wl.ContainsKey(VoordelenApp.TypeId)) {
                    VoordelenApp.Data = (EY.com.eBook.RuleEngine.AY2009.VoordelenAppTypes.Voordelen)wl[VoordelenApp.TypeId].WorksheetData;
                }
            
                if(wl.ContainsKey(PersoneelApp.TypeId)) {
                    PersoneelApp.Data = (EY.com.eBook.RuleEngine.AY2009.PersoneelAppTypes.WPPersoneel)wl[PersoneelApp.TypeId].WorksheetData;
                }
            
                if(wl.ContainsKey(BTWApp.TypeId)) {
                    BTWApp.Data = (EY.com.eBook.RuleEngine.AY2009.BTWAppTypes.WPBTW)wl[BTWApp.TypeId].WorksheetData;
                }
            
                if(wl.ContainsKey(RVIntrestenApp.TypeId)) {
                    RVIntrestenApp.Data = (EY.com.eBook.RuleEngine.AY2009.RVIntrestenAppTypes.WPRVIntresten)wl[RVIntrestenApp.TypeId].WorksheetData;
                }
            
                if(wl.ContainsKey(DubieuzeDebiteurenApp.TypeId)) {
                    DubieuzeDebiteurenApp.Data = (EY.com.eBook.RuleEngine.AY2009.DubieuzeDebiteurenAppTypes.DubieuzeDebiteuren)wl[DubieuzeDebiteurenApp.TypeId].WorksheetData;
                }
            
                if(wl.ContainsKey(VerliesHandelsdebiteurenApp.TypeId)) {
                    VerliesHandelsdebiteurenApp.Data = (EY.com.eBook.RuleEngine.AY2009.VerliesHandelsdebiteurenAppTypes.VerliesHandelsDebiteuren)wl[VerliesHandelsdebiteurenApp.TypeId].WorksheetData;
                }
            
                if(wl.ContainsKey(VoorzieningenApp.TypeId)) {
                    VoorzieningenApp.Data = (EY.com.eBook.RuleEngine.AY2009.VoorzieningenAppTypes.Voorzieningen)wl[VoorzieningenApp.TypeId].WorksheetData;
                }
            
                if(wl.ContainsKey(BelastingenApp.TypeId)) {
                    BelastingenApp.Data = (EY.com.eBook.RuleEngine.AY2009.BelastingenAppTypes.WPBelastingen)wl[BelastingenApp.TypeId].WorksheetData;
                }
            
                if(wl.ContainsKey(BelasteReservesApp.TypeId)) {
                    BelasteReservesApp.Data = (EY.com.eBook.RuleEngine.AY2009.BelasteReservesAppTypes.BelasteReserves)wl[BelasteReservesApp.TypeId].WorksheetData;
                }
            
                if(wl.ContainsKey(BelastingVrijeReservesApp.TypeId)) {
                    BelastingVrijeReservesApp.Data = (EY.com.eBook.RuleEngine.AY2009.BelastingVrijeReservesAppTypes.BelastingVrijeReserves)wl[BelastingVrijeReservesApp.TypeId].WorksheetData;
                }
            
                if(wl.ContainsKey(BepalingBelastbareBasisApp.TypeId)) {
                    BepalingBelastbareBasisApp.Data = (EY.com.eBook.RuleEngine.AY2009.BepalingBelastbareBasisAppTypes.BepalingBasis)wl[BepalingBelastbareBasisApp.TypeId].WorksheetData;
                }
            
                if(wl.ContainsKey(DBIApp.TypeId)) {
                    DBIApp.Data = (EY.com.eBook.RuleEngine.AY2009.DBIAppTypes.DBIWorksheet)wl[DBIApp.TypeId].WorksheetData;
                }
            
                if(wl.ContainsKey(RisicoKapitaalApp.TypeId)) {
                    RisicoKapitaalApp.Data = (EY.com.eBook.RuleEngine.AY2009.RisicoKapitaalAppTypes.WPRisicokapitaal)wl[RisicoKapitaalApp.TypeId].WorksheetData;
                }
            
                if(wl.ContainsKey(TransferTaxCreditsApp.TypeId)) {
                    TransferTaxCreditsApp.Data = (EY.com.eBook.RuleEngine.AY2009.TransferTaxCreditsAppTypes.TransferTaxCredits)wl[TransferTaxCreditsApp.TypeId].WorksheetData;
                }
            
                if(wl.ContainsKey(BerekeningVenBApp.TypeId)) {
                    BerekeningVenBApp.Data = (EY.com.eBook.RuleEngine.AY2009.BerekeningVenBAppTypes.BerekeningVenB)wl[BerekeningVenBApp.TypeId].WorksheetData;
                }
            
                if(wl.ContainsKey(AangifteVenBApp.TypeId)) {
                    AangifteVenBApp.Data = (EY.com.eBook.RuleEngine.AY2009.AangifteVenBAppTypes.AangifteVenB)wl[AangifteVenBApp.TypeId].WorksheetData;
                }
            
            }
            
        }
        
        public void Save()
        {
            try {
              //CacheProxy cp = new CacheProxy();
              //cp.Open();
              CacheRepository cp = new CacheRepository();
              cp.SaveWorksheetSetToCache(GetCacheLibrary(), DataContract.Id);
              //cp.Close();
            } catch { }
            
            var ctx = EY.com.eBook.Core.EF.eBookWriteManager.Context;
            
            
              if(!FacturatieApp.Data.LastUpdated.HasValue
                    || FacturatieApp.Data.LastUpdated.Value > LastSave) {
              if (!FacturatieApp.Data.LastUpdated.HasValue) FacturatieApp.Data.LastUpdated = DateTime.Now;
              ctx.UpdateWorksheet(DataContract.Id, FacturatieApp.TypeId, FacturatieApp.ToXml());
              }
            
              if(!OverlopendeRekeningenApp.Data.LastUpdated.HasValue
                    || OverlopendeRekeningenApp.Data.LastUpdated.Value > LastSave) {
              if (!OverlopendeRekeningenApp.Data.LastUpdated.HasValue) OverlopendeRekeningenApp.Data.LastUpdated = DateTime.Now;
              ctx.UpdateWorksheet(DataContract.Id, OverlopendeRekeningenApp.TypeId, OverlopendeRekeningenApp.ToXml());
              }
            
              if(!VergelijkendeCijfersApp.Data.LastUpdated.HasValue
                    || VergelijkendeCijfersApp.Data.LastUpdated.Value > LastSave) {
              if (!VergelijkendeCijfersApp.Data.LastUpdated.HasValue) VergelijkendeCijfersApp.Data.LastUpdated = DateTime.Now;
              ctx.UpdateWorksheet(DataContract.Id, VergelijkendeCijfersApp.TypeId, VergelijkendeCijfersApp.ToXml());
              }
            
              if(!VoorschottenApp.Data.LastUpdated.HasValue
                    || VoorschottenApp.Data.LastUpdated.Value > LastSave) {
              if (!VoorschottenApp.Data.LastUpdated.HasValue) VoorschottenApp.Data.LastUpdated = DateTime.Now;
              ctx.UpdateWorksheet(DataContract.Id, VoorschottenApp.TypeId, VoorschottenApp.ToXml());
              }
            
              if(!ErelonenHuurApp.Data.LastUpdated.HasValue
                    || ErelonenHuurApp.Data.LastUpdated.Value > LastSave) {
              if (!ErelonenHuurApp.Data.LastUpdated.HasValue) ErelonenHuurApp.Data.LastUpdated = DateTime.Now;
              ctx.UpdateWorksheet(DataContract.Id, ErelonenHuurApp.TypeId, ErelonenHuurApp.ToXml());
              }
            
              if(!VoordelenApp.Data.LastUpdated.HasValue
                    || VoordelenApp.Data.LastUpdated.Value > LastSave) {
              if (!VoordelenApp.Data.LastUpdated.HasValue) VoordelenApp.Data.LastUpdated = DateTime.Now;
              ctx.UpdateWorksheet(DataContract.Id, VoordelenApp.TypeId, VoordelenApp.ToXml());
              }
            
              if(!PersoneelApp.Data.LastUpdated.HasValue
                    || PersoneelApp.Data.LastUpdated.Value > LastSave) {
              if (!PersoneelApp.Data.LastUpdated.HasValue) PersoneelApp.Data.LastUpdated = DateTime.Now;
              ctx.UpdateWorksheet(DataContract.Id, PersoneelApp.TypeId, PersoneelApp.ToXml());
              }
            
              if(!BTWApp.Data.LastUpdated.HasValue
                    || BTWApp.Data.LastUpdated.Value > LastSave) {
              if (!BTWApp.Data.LastUpdated.HasValue) BTWApp.Data.LastUpdated = DateTime.Now;
              ctx.UpdateWorksheet(DataContract.Id, BTWApp.TypeId, BTWApp.ToXml());
              }
            
              if(!RVIntrestenApp.Data.LastUpdated.HasValue
                    || RVIntrestenApp.Data.LastUpdated.Value > LastSave) {
              if (!RVIntrestenApp.Data.LastUpdated.HasValue) RVIntrestenApp.Data.LastUpdated = DateTime.Now;
              ctx.UpdateWorksheet(DataContract.Id, RVIntrestenApp.TypeId, RVIntrestenApp.ToXml());
              }
            
              if(!DubieuzeDebiteurenApp.Data.LastUpdated.HasValue
                    || DubieuzeDebiteurenApp.Data.LastUpdated.Value > LastSave) {
              if (!DubieuzeDebiteurenApp.Data.LastUpdated.HasValue) DubieuzeDebiteurenApp.Data.LastUpdated = DateTime.Now;
              ctx.UpdateWorksheet(DataContract.Id, DubieuzeDebiteurenApp.TypeId, DubieuzeDebiteurenApp.ToXml());
              }
            
              if(!VerliesHandelsdebiteurenApp.Data.LastUpdated.HasValue
                    || VerliesHandelsdebiteurenApp.Data.LastUpdated.Value > LastSave) {
              if (!VerliesHandelsdebiteurenApp.Data.LastUpdated.HasValue) VerliesHandelsdebiteurenApp.Data.LastUpdated = DateTime.Now;
              ctx.UpdateWorksheet(DataContract.Id, VerliesHandelsdebiteurenApp.TypeId, VerliesHandelsdebiteurenApp.ToXml());
              }
            
              if(!VoorzieningenApp.Data.LastUpdated.HasValue
                    || VoorzieningenApp.Data.LastUpdated.Value > LastSave) {
              if (!VoorzieningenApp.Data.LastUpdated.HasValue) VoorzieningenApp.Data.LastUpdated = DateTime.Now;
              ctx.UpdateWorksheet(DataContract.Id, VoorzieningenApp.TypeId, VoorzieningenApp.ToXml());
              }
            
              if(!BelastingenApp.Data.LastUpdated.HasValue
                    || BelastingenApp.Data.LastUpdated.Value > LastSave) {
              if (!BelastingenApp.Data.LastUpdated.HasValue) BelastingenApp.Data.LastUpdated = DateTime.Now;
              ctx.UpdateWorksheet(DataContract.Id, BelastingenApp.TypeId, BelastingenApp.ToXml());
              }
            
              if(!BelasteReservesApp.Data.LastUpdated.HasValue
                    || BelasteReservesApp.Data.LastUpdated.Value > LastSave) {
              if (!BelasteReservesApp.Data.LastUpdated.HasValue) BelasteReservesApp.Data.LastUpdated = DateTime.Now;
              ctx.UpdateWorksheet(DataContract.Id, BelasteReservesApp.TypeId, BelasteReservesApp.ToXml());
              }
            
              if(!BelastingVrijeReservesApp.Data.LastUpdated.HasValue
                    || BelastingVrijeReservesApp.Data.LastUpdated.Value > LastSave) {
              if (!BelastingVrijeReservesApp.Data.LastUpdated.HasValue) BelastingVrijeReservesApp.Data.LastUpdated = DateTime.Now;
              ctx.UpdateWorksheet(DataContract.Id, BelastingVrijeReservesApp.TypeId, BelastingVrijeReservesApp.ToXml());
              }
            
              if(!BepalingBelastbareBasisApp.Data.LastUpdated.HasValue
                    || BepalingBelastbareBasisApp.Data.LastUpdated.Value > LastSave) {
              if (!BepalingBelastbareBasisApp.Data.LastUpdated.HasValue) BepalingBelastbareBasisApp.Data.LastUpdated = DateTime.Now;
              ctx.UpdateWorksheet(DataContract.Id, BepalingBelastbareBasisApp.TypeId, BepalingBelastbareBasisApp.ToXml());
              }
            
              if(!DBIApp.Data.LastUpdated.HasValue
                    || DBIApp.Data.LastUpdated.Value > LastSave) {
              if (!DBIApp.Data.LastUpdated.HasValue) DBIApp.Data.LastUpdated = DateTime.Now;
              ctx.UpdateWorksheet(DataContract.Id, DBIApp.TypeId, DBIApp.ToXml());
              }
            
              if(!RisicoKapitaalApp.Data.LastUpdated.HasValue
                    || RisicoKapitaalApp.Data.LastUpdated.Value > LastSave) {
              if (!RisicoKapitaalApp.Data.LastUpdated.HasValue) RisicoKapitaalApp.Data.LastUpdated = DateTime.Now;
              ctx.UpdateWorksheet(DataContract.Id, RisicoKapitaalApp.TypeId, RisicoKapitaalApp.ToXml());
              }
            
              if(!TransferTaxCreditsApp.Data.LastUpdated.HasValue
                    || TransferTaxCreditsApp.Data.LastUpdated.Value > LastSave) {
              if (!TransferTaxCreditsApp.Data.LastUpdated.HasValue) TransferTaxCreditsApp.Data.LastUpdated = DateTime.Now;
              ctx.UpdateWorksheet(DataContract.Id, TransferTaxCreditsApp.TypeId, TransferTaxCreditsApp.ToXml());
              }
            
              if(!BerekeningVenBApp.Data.LastUpdated.HasValue
                    || BerekeningVenBApp.Data.LastUpdated.Value > LastSave) {
              if (!BerekeningVenBApp.Data.LastUpdated.HasValue) BerekeningVenBApp.Data.LastUpdated = DateTime.Now;
              ctx.UpdateWorksheet(DataContract.Id, BerekeningVenBApp.TypeId, BerekeningVenBApp.ToXml());
              }
            
              if(!AangifteVenBApp.Data.LastUpdated.HasValue
                    || AangifteVenBApp.Data.LastUpdated.Value > LastSave) {
              if (!AangifteVenBApp.Data.LastUpdated.HasValue) AangifteVenBApp.Data.LastUpdated = DateTime.Now;
              ctx.UpdateWorksheet(DataContract.Id, AangifteVenBApp.TypeId, AangifteVenBApp.ToXml());
              }
            
    
            LastSave = DateTime.Now;   
        }
        
     }       

}
  