﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.RuleEngine.Core;
using EY.com.eBook.RuleEngine.Core.Interfaces;
using EY.com.eBook.RuleEngine.Data;
using EY.com.eBook.Core;

namespace EY.com.eBook.RuleEngine.AY2009
{
    
    public class BerekeningVenBApp
    {
        #region standard
        public static WorksheetTypeSettings Settings = new WorksheetTypeSettings
                                                      {
                                                        Clients = true
                                                        ,
                                                        Suppliers = true
                                                        ,
                                                        AllAccounts = true
                                                        , 
                                                        PerformsBookings=true
                                                      };
                                                      
        public static Guid TypeId = new Guid("803d6a06-352e-4e28-ade5-214deaac76ca");

        public List<string> ImportablePreviousTypes  = new List<string>() {  "EY.com.eBook.RuleEngine.AY2011.FacturatieApp"  };

        public Guid GetMyType() { return TypeId; }
        
        public void SetMinimal() 
        {
            if(Data==null) {
                Data = new EY.com.eBook.RuleEngine.AY2009.BerekeningVenBAppTypes.BerekeningVenB();
                Data.LastUpdated = DateTime.Now;
            }              
            
              if(Data.Overzicht==null) {
                  Data.Overzicht = new EY.com.eBook.RuleEngine.AY2009.BerekeningVenBAppTypes.General {
                            Id=Guid.NewGuid()
                            , LastUpdated=null
                            , IsValid=true
                            , PreviousImported=false
                            
                         };
                  Data.LastUpdated = DateTime.Now;
              }
            
              if(Data.BelasteReserves==null) {
                  Data.BelasteReserves = new EY.com.eBook.RuleEngine.AY2009.BerekeningVenBAppTypes.BelasteReservesItem {
                            Id=Guid.NewGuid()
                            , LastUpdated=null
                            , IsValid=true
                            , PreviousImported=false
                            
                         };
                  Data.LastUpdated = DateTime.Now;
              }
            
              if(Data.VerworpenUitgaven==null) {
                  Data.VerworpenUitgaven = new EY.com.eBook.RuleEngine.AY2009.BerekeningVenBAppTypes.VerworpenUitgavenItem {
                            Id=Guid.NewGuid()
                            , LastUpdated=null
                            , IsValid=true
                            , PreviousImported=false
                            
                         };
                  Data.LastUpdated = DateTime.Now;
              }
            
              if(Data.BelastbareBestZonderAftrek==null) {
                  Data.BelastbareBestZonderAftrek = new EY.com.eBook.RuleEngine.AY2009.BerekeningVenBAppTypes.BelastbareBestZonderAftrekItem {
                            Id=Guid.NewGuid()
                            , LastUpdated=null
                            , IsValid=true
                            , PreviousImported=false
                            
                         };
                  Data.LastUpdated = DateTime.Now;
              }
            
              if(Data.OpdelingNaarOorsprong==null) {
                  Data.OpdelingNaarOorsprong = new EY.com.eBook.RuleEngine.AY2009.BerekeningVenBAppTypes.OpdelingNaarOorsprongItem {
                            Id=Guid.NewGuid()
                            , LastUpdated=null
                            , IsValid=true
                            , PreviousImported=false
                            
                         };
                  Data.LastUpdated = DateTime.Now;
              }
            
              if(Data.FiscaleAftrek==null) {
                  Data.FiscaleAftrek = new EY.com.eBook.RuleEngine.AY2009.BerekeningVenBAppTypes.FiscaleAftrekItem {
                            Id=Guid.NewGuid()
                            , LastUpdated=null
                            , IsValid=true
                            , PreviousImported=false
                            
                         };
                  Data.LastUpdated = DateTime.Now;
              }
            
              if(Data.Tarief==null) {
                  Data.Tarief = new EY.com.eBook.RuleEngine.AY2009.BerekeningVenBAppTypes.TariefItem {
                            Id=Guid.NewGuid()
                            , LastUpdated=null
                            , IsValid=true
                            , PreviousImported=false
                            
                         };
                  Data.LastUpdated = DateTime.Now;
              }
            
              if(Data.BelastingsBerekening==null) {
                  Data.BelastingsBerekening = new EY.com.eBook.RuleEngine.AY2009.BerekeningVenBAppTypes.BelastingsBerekeningItem {
                            Id=Guid.NewGuid()
                            , LastUpdated=null
                            , IsValid=true
                            , PreviousImported=false
                            
                         };
                  Data.LastUpdated = DateTime.Now;
              }
            
              if(Data.BelastingsBerekening2==null) {
                  Data.BelastingsBerekening2 = new EY.com.eBook.RuleEngine.AY2009.BerekeningVenBAppTypes.BelastingsBerekeningItem2 {
                            Id=Guid.NewGuid()
                            , LastUpdated=null
                            , IsValid=true
                            , PreviousImported=false
                            
                         };
                  Data.LastUpdated = DateTime.Now;
              }
            
        }
        
        public string ToXml() 
        {
            return CoreHelper.SerializeToString(Data);
        }

        public void LoadXml(string xml)
        {
            if (!string.IsNullOrEmpty(xml)) {
                Data = CoreHelper.DeserializeFromString<EY.com.eBook.RuleEngine.AY2009.BerekeningVenBAppTypes.BerekeningVenB>(xml);
            }
            SetMinimal();
            
        }
        
        public EY.com.eBook.RuleEngine.AY2009.BerekeningVenBAppTypes.BerekeningVenB Data { get; set; }
        
        public void Calculate(ref WorksheetSetData worksheetset)
        {
           // Data.LastCalculated = DateTime.Now;
        }
        #endregion
        
        #region AddUpdateDelete
        
          public void UpdateOverzicht(EY.com.eBook.RuleEngine.AY2009.BerekeningVenBAppTypes.General entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void UpdateBelasteReserves(EY.com.eBook.RuleEngine.AY2009.BerekeningVenBAppTypes.BelasteReservesItem entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void UpdateVerworpenUitgaven(EY.com.eBook.RuleEngine.AY2009.BerekeningVenBAppTypes.VerworpenUitgavenItem entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void UpdateBelastbareBestZonderAftrek(EY.com.eBook.RuleEngine.AY2009.BerekeningVenBAppTypes.BelastbareBestZonderAftrekItem entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void UpdateOpdelingNaarOorsprong(EY.com.eBook.RuleEngine.AY2009.BerekeningVenBAppTypes.OpdelingNaarOorsprongItem entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void UpdateFiscaleAftrek(EY.com.eBook.RuleEngine.AY2009.BerekeningVenBAppTypes.FiscaleAftrekItem entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void UpdateTarief(EY.com.eBook.RuleEngine.AY2009.BerekeningVenBAppTypes.TariefItem entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void UpdateBelastingsBerekening(EY.com.eBook.RuleEngine.AY2009.BerekeningVenBAppTypes.BelastingsBerekeningItem entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void UpdateBelastingsBerekening2(EY.com.eBook.RuleEngine.AY2009.BerekeningVenBAppTypes.BelastingsBerekeningItem2 entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
        #endregion
        
        #region VALIDATION
        
        public EY.com.eBook.RuleEngine.AY2009.BerekeningVenBAppTypes.General Validate(EY.com.eBook.RuleEngine.AY2009.BerekeningVenBAppTypes.General entity, string location,ref WorksheetSetData worksheetset)
          {
          entity.IsValid = true;
          worksheetset.ClearMessages(TypeId, entity.Id);
          

          return entity;
          }
        
        public EY.com.eBook.RuleEngine.AY2009.BerekeningVenBAppTypes.BelasteReservesItem Validate(EY.com.eBook.RuleEngine.AY2009.BerekeningVenBAppTypes.BelasteReservesItem entity, string location,ref WorksheetSetData worksheetset)
          {
          entity.IsValid = true;
          worksheetset.ClearMessages(TypeId, entity.Id);
          

          return entity;
          }
        
        public EY.com.eBook.RuleEngine.AY2009.BerekeningVenBAppTypes.VerworpenUitgavenItem Validate(EY.com.eBook.RuleEngine.AY2009.BerekeningVenBAppTypes.VerworpenUitgavenItem entity, string location,ref WorksheetSetData worksheetset)
          {
          entity.IsValid = true;
          worksheetset.ClearMessages(TypeId, entity.Id);
          

          return entity;
          }
        
        public EY.com.eBook.RuleEngine.AY2009.BerekeningVenBAppTypes.BelastbareBestZonderAftrekItem Validate(EY.com.eBook.RuleEngine.AY2009.BerekeningVenBAppTypes.BelastbareBestZonderAftrekItem entity, string location,ref WorksheetSetData worksheetset)
          {
          entity.IsValid = true;
          worksheetset.ClearMessages(TypeId, entity.Id);
          

          return entity;
          }
        
        public EY.com.eBook.RuleEngine.AY2009.BerekeningVenBAppTypes.OpdelingNaarOorsprongItem Validate(EY.com.eBook.RuleEngine.AY2009.BerekeningVenBAppTypes.OpdelingNaarOorsprongItem entity, string location,ref WorksheetSetData worksheetset)
          {
          entity.IsValid = true;
          worksheetset.ClearMessages(TypeId, entity.Id);
          

          return entity;
          }
        
        public EY.com.eBook.RuleEngine.AY2009.BerekeningVenBAppTypes.FiscaleAftrekItem Validate(EY.com.eBook.RuleEngine.AY2009.BerekeningVenBAppTypes.FiscaleAftrekItem entity, string location,ref WorksheetSetData worksheetset)
          {
          entity.IsValid = true;
          worksheetset.ClearMessages(TypeId, entity.Id);
          

          return entity;
          }
        
        public EY.com.eBook.RuleEngine.AY2009.BerekeningVenBAppTypes.TariefItem Validate(EY.com.eBook.RuleEngine.AY2009.BerekeningVenBAppTypes.TariefItem entity, string location,ref WorksheetSetData worksheetset)
          {
          entity.IsValid = true;
          worksheetset.ClearMessages(TypeId, entity.Id);
          

          return entity;
          }
        
        public EY.com.eBook.RuleEngine.AY2009.BerekeningVenBAppTypes.BelastingsBerekeningItem Validate(EY.com.eBook.RuleEngine.AY2009.BerekeningVenBAppTypes.BelastingsBerekeningItem entity, string location,ref WorksheetSetData worksheetset)
          {
          entity.IsValid = true;
          worksheetset.ClearMessages(TypeId, entity.Id);
          

          return entity;
          }
        
        public EY.com.eBook.RuleEngine.AY2009.BerekeningVenBAppTypes.BelastingsBerekeningItem2 Validate(EY.com.eBook.RuleEngine.AY2009.BerekeningVenBAppTypes.BelastingsBerekeningItem2 entity, string location,ref WorksheetSetData worksheetset)
          {
          entity.IsValid = true;
          worksheetset.ClearMessages(TypeId, entity.Id);
          

          return entity;
          }
        
        #endregion
        // CALCULATION
        
        // BOOKINGS
        
        // IMPORT PREVIOUS
        
    }
}
  