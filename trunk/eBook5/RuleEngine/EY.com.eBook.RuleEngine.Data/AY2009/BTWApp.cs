﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.RuleEngine.Core;
using EY.com.eBook.RuleEngine.Core.Interfaces;
using EY.com.eBook.RuleEngine.Data;
using EY.com.eBook.Core;

namespace EY.com.eBook.RuleEngine.AY2009
{
    
    public class BTWApp
    {
        #region standard
        public static WorksheetTypeSettings Settings = new WorksheetTypeSettings
                                                      {
                                                        Clients = true
                                                        ,
                                                        Suppliers = true
                                                        ,
                                                        AllAccounts = true
                                                        , 
                                                        PerformsBookings=true
                                                      };
                                                      
        public static Guid TypeId = new Guid("2ca965ed-eacb-43ff-9a91-cfe7ef09eef9");

        public List<string> ImportablePreviousTypes  = new List<string>() {  "EY.com.eBook.RuleEngine.AY2011.FacturatieApp"  };

        public Guid GetMyType() { return TypeId; }
        
        public void SetMinimal() 
        {
            if(Data==null) {
                Data = new EY.com.eBook.RuleEngine.AY2009.BTWAppTypes.WPBTW();
                Data.LastUpdated = DateTime.Now;
            }              
            
              if(Data.OmzetAangiftes==null) {
                  Data.OmzetAangiftes = new List<EY.com.eBook.RuleEngine.AY2009.BTWAppTypes.OmzetAangifte>();;
                  Data.LastUpdated = DateTime.Now;
              }
            
              if(Data.OmzettenGeboekt==null) {
                  Data.OmzettenGeboekt = new List<EY.com.eBook.RuleEngine.AY2009.BTWAppTypes.OmzetGeboekt>();;
                  Data.LastUpdated = DateTime.Now;
              }
            
              if(Data.OmzetVerklaringen==null) {
                  Data.OmzetVerklaringen = new List<EY.com.eBook.RuleEngine.AY2009.BTWAppTypes.OmzetVerklaring>();;
                  Data.LastUpdated = DateTime.Now;
              }
            
              if(Data.RCAangiftes==null) {
                  Data.RCAangiftes = new List<EY.com.eBook.RuleEngine.AY2009.BTWAppTypes.RCAangifte>();;
                  Data.LastUpdated = DateTime.Now;
              }
            
              if(Data.RCenGeboekt==null) {
                  Data.RCenGeboekt = new List<EY.com.eBook.RuleEngine.AY2009.BTWAppTypes.RCGeboekt>();;
                  Data.LastUpdated = DateTime.Now;
              }
            
              if(Data.RCVerklaringen==null) {
                  Data.RCVerklaringen = new List<EY.com.eBook.RuleEngine.AY2009.BTWAppTypes.RCVerklaring>();;
                  Data.LastUpdated = DateTime.Now;
              }
            
        }
        
        public string ToXml() 
        {
            return CoreHelper.SerializeToString(Data);
        }

        public void LoadXml(string xml)
        {
            if (!string.IsNullOrEmpty(xml)) {
                Data = CoreHelper.DeserializeFromString<EY.com.eBook.RuleEngine.AY2009.BTWAppTypes.WPBTW>(xml);
            }
            SetMinimal();
            
        }
        
        public EY.com.eBook.RuleEngine.AY2009.BTWAppTypes.WPBTW Data { get; set; }
        
        public void Calculate(ref WorksheetSetData worksheetset)
        {
           // Data.LastCalculated = DateTime.Now;
        }
        #endregion
        
        #region AddUpdateDelete
        
          public void AddToOmzetAangiftes(EY.com.eBook.RuleEngine.AY2009.BTWAppTypes.OmzetAangifte entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void DeleteOmzetAangiftes(Guid rowId,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void UpdateOmzetAangiftes(EY.com.eBook.RuleEngine.AY2009.BTWAppTypes.OmzetAangifte entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void AddToOmzettenGeboekt(EY.com.eBook.RuleEngine.AY2009.BTWAppTypes.OmzetGeboekt entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void DeleteOmzettenGeboekt(Guid rowId,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void UpdateOmzettenGeboekt(EY.com.eBook.RuleEngine.AY2009.BTWAppTypes.OmzetGeboekt entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void AddToOmzetVerklaringen(EY.com.eBook.RuleEngine.AY2009.BTWAppTypes.OmzetVerklaring entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void DeleteOmzetVerklaringen(Guid rowId,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void UpdateOmzetVerklaringen(EY.com.eBook.RuleEngine.AY2009.BTWAppTypes.OmzetVerklaring entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void AddToRCAangiftes(EY.com.eBook.RuleEngine.AY2009.BTWAppTypes.RCAangifte entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void DeleteRCAangiftes(Guid rowId,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void UpdateRCAangiftes(EY.com.eBook.RuleEngine.AY2009.BTWAppTypes.RCAangifte entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void AddToRCenGeboekt(EY.com.eBook.RuleEngine.AY2009.BTWAppTypes.RCGeboekt entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void DeleteRCenGeboekt(Guid rowId,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void UpdateRCenGeboekt(EY.com.eBook.RuleEngine.AY2009.BTWAppTypes.RCGeboekt entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void AddToRCVerklaringen(EY.com.eBook.RuleEngine.AY2009.BTWAppTypes.RCVerklaring entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void DeleteRCVerklaringen(Guid rowId,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void UpdateRCVerklaringen(EY.com.eBook.RuleEngine.AY2009.BTWAppTypes.RCVerklaring entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
        #endregion
        
        #region VALIDATION
        
        public EY.com.eBook.RuleEngine.AY2009.BTWAppTypes.OmzetAangifte Validate(EY.com.eBook.RuleEngine.AY2009.BTWAppTypes.OmzetAangifte entity, string location,ref WorksheetSetData worksheetset)
          {
          entity.IsValid = true;
          worksheetset.ClearMessages(TypeId, entity.Id);
          

          return entity;
          }
        
        public EY.com.eBook.RuleEngine.AY2009.BTWAppTypes.OmzetGeboekt Validate(EY.com.eBook.RuleEngine.AY2009.BTWAppTypes.OmzetGeboekt entity, string location,ref WorksheetSetData worksheetset)
          {
          entity.IsValid = true;
          worksheetset.ClearMessages(TypeId, entity.Id);
          

          return entity;
          }
        
        public EY.com.eBook.RuleEngine.AY2009.BTWAppTypes.OmzetVerklaring Validate(EY.com.eBook.RuleEngine.AY2009.BTWAppTypes.OmzetVerklaring entity, string location,ref WorksheetSetData worksheetset)
          {
          entity.IsValid = true;
          worksheetset.ClearMessages(TypeId, entity.Id);
          

          return entity;
          }
        
        public EY.com.eBook.RuleEngine.AY2009.BTWAppTypes.RCAangifte Validate(EY.com.eBook.RuleEngine.AY2009.BTWAppTypes.RCAangifte entity, string location,ref WorksheetSetData worksheetset)
          {
          entity.IsValid = true;
          worksheetset.ClearMessages(TypeId, entity.Id);
          

          return entity;
          }
        
        public EY.com.eBook.RuleEngine.AY2009.BTWAppTypes.RCGeboekt Validate(EY.com.eBook.RuleEngine.AY2009.BTWAppTypes.RCGeboekt entity, string location,ref WorksheetSetData worksheetset)
          {
          entity.IsValid = true;
          worksheetset.ClearMessages(TypeId, entity.Id);
          

          return entity;
          }
        
        public EY.com.eBook.RuleEngine.AY2009.BTWAppTypes.RCVerklaring Validate(EY.com.eBook.RuleEngine.AY2009.BTWAppTypes.RCVerklaring entity, string location,ref WorksheetSetData worksheetset)
          {
          entity.IsValid = true;
          worksheetset.ClearMessages(TypeId, entity.Id);
          

          return entity;
          }
        
        #endregion
        // CALCULATION
        
        // BOOKINGS
        
        // IMPORT PREVIOUS
        
    }
}
  