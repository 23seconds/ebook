﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.RuleEngine.Core;
using EY.com.eBook.RuleEngine.Core.Interfaces;
using EY.com.eBook.RuleEngine.Data;
using EY.com.eBook.Core;

namespace EY.com.eBook.RuleEngine.AY2009
{
    
    public class FacturatieApp
    {
        #region standard
        public static WorksheetTypeSettings Settings = new WorksheetTypeSettings
                                                      {
                                                        Clients = true
                                                        ,
                                                        Suppliers = true
                                                        ,
                                                        AllAccounts = true
                                                        , 
                                                        PerformsBookings=true
                                                      };
                                                      
        public static Guid TypeId = new Guid("699d94dc-6739-4fda-8941-5926521761b9");

        public List<string> ImportablePreviousTypes  = new List<string>() {  "EY.com.eBook.RuleEngine.AY2011.FacturatieApp"  };

        public Guid GetMyType() { return TypeId; }
        
        public void SetMinimal() 
        {
            if(Data==null) {
                Data = new EY.com.eBook.RuleEngine.AY2009.FacturatieAppTypes.Facturatie();
                Data.LastUpdated = DateTime.Now;
            }              
            
              if(Data.OpTeMakenFacturen==null) {
                  Data.OpTeMakenFacturen = new List<EY.com.eBook.RuleEngine.AY2009.FacturatieAppTypes.FacturatieKlant>();;
                  Data.LastUpdated = DateTime.Now;
              }
            
              if(Data.TeOntvangenCN==null) {
                  Data.TeOntvangenCN = new List<EY.com.eBook.RuleEngine.AY2009.FacturatieAppTypes.FacturatieLeverancierCN>();;
                  Data.LastUpdated = DateTime.Now;
              }
            
              if(Data.TeOntvangenFacturen==null) {
                  Data.TeOntvangenFacturen = new List<EY.com.eBook.RuleEngine.AY2009.FacturatieAppTypes.FacturatieLeverancier>();;
                  Data.LastUpdated = DateTime.Now;
              }
            
              if(Data.OpTeMakenCN==null) {
                  Data.OpTeMakenCN = new List<EY.com.eBook.RuleEngine.AY2009.FacturatieAppTypes.FacturatieKlant>();;
                  Data.LastUpdated = DateTime.Now;
              }
            
        }
        
        public string ToXml() 
        {
            return CoreHelper.SerializeToString(Data);
        }

        public void LoadXml(string xml)
        {
            if (!string.IsNullOrEmpty(xml)) {
                Data = CoreHelper.DeserializeFromString<EY.com.eBook.RuleEngine.AY2009.FacturatieAppTypes.Facturatie>(xml);
            }
            SetMinimal();
            
        }
        
        public EY.com.eBook.RuleEngine.AY2009.FacturatieAppTypes.Facturatie Data { get; set; }
        
        public void Calculate(ref WorksheetSetData worksheetset)
        {
           // Data.LastCalculated = DateTime.Now;
        }
        #endregion
        
        #region AddUpdateDelete
        
          public void AddToOpTeMakenFacturen(EY.com.eBook.RuleEngine.AY2009.FacturatieAppTypes.FacturatieKlant entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void DeleteOpTeMakenFacturen(Guid rowId,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void UpdateOpTeMakenFacturen(EY.com.eBook.RuleEngine.AY2009.FacturatieAppTypes.FacturatieKlant entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void AddToTeOntvangenCN(EY.com.eBook.RuleEngine.AY2009.FacturatieAppTypes.FacturatieLeverancierCN entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void DeleteTeOntvangenCN(Guid rowId,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void UpdateTeOntvangenCN(EY.com.eBook.RuleEngine.AY2009.FacturatieAppTypes.FacturatieLeverancierCN entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void AddToTeOntvangenFacturen(EY.com.eBook.RuleEngine.AY2009.FacturatieAppTypes.FacturatieLeverancier entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void DeleteTeOntvangenFacturen(Guid rowId,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void UpdateTeOntvangenFacturen(EY.com.eBook.RuleEngine.AY2009.FacturatieAppTypes.FacturatieLeverancier entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void AddToOpTeMakenCN(EY.com.eBook.RuleEngine.AY2009.FacturatieAppTypes.FacturatieKlant entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void DeleteOpTeMakenCN(Guid rowId,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void UpdateOpTeMakenCN(EY.com.eBook.RuleEngine.AY2009.FacturatieAppTypes.FacturatieKlant entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
        #endregion
        
        #region VALIDATION
        
        public EY.com.eBook.RuleEngine.AY2009.FacturatieAppTypes.FacturatieKlant Validate(EY.com.eBook.RuleEngine.AY2009.FacturatieAppTypes.FacturatieKlant entity, string location,ref WorksheetSetData worksheetset)
          {
          entity.IsValid = true;
          worksheetset.ClearMessages(TypeId, entity.Id);
          

          return entity;
          }
        
        public EY.com.eBook.RuleEngine.AY2009.FacturatieAppTypes.FacturatieLeverancierCN Validate(EY.com.eBook.RuleEngine.AY2009.FacturatieAppTypes.FacturatieLeverancierCN entity, string location,ref WorksheetSetData worksheetset)
          {
          entity.IsValid = true;
          worksheetset.ClearMessages(TypeId, entity.Id);
          

          return entity;
          }
        
        public EY.com.eBook.RuleEngine.AY2009.FacturatieAppTypes.FacturatieLeverancier Validate(EY.com.eBook.RuleEngine.AY2009.FacturatieAppTypes.FacturatieLeverancier entity, string location,ref WorksheetSetData worksheetset)
          {
          entity.IsValid = true;
          worksheetset.ClearMessages(TypeId, entity.Id);
          

          return entity;
          }
        
        #endregion
        // CALCULATION
        
        // BOOKINGS
        
        // IMPORT PREVIOUS
        
    }
}
  