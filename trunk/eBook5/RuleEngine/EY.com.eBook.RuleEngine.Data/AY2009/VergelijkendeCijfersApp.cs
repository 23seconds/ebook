﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.RuleEngine.Core;
using EY.com.eBook.RuleEngine.Core.Interfaces;
using EY.com.eBook.RuleEngine.Data;
using EY.com.eBook.Core;

namespace EY.com.eBook.RuleEngine.AY2009
{
    
    public class VergelijkendeCijfersApp
    {
        #region standard
        public static WorksheetTypeSettings Settings = new WorksheetTypeSettings
                                                      {
                                                        Clients = true
                                                        ,
                                                        Suppliers = true
                                                        ,
                                                        AllAccounts = true
                                                        , 
                                                        PerformsBookings=true
                                                      };
                                                      
        public static Guid TypeId = new Guid("d6b8e81c-670f-404a-bcdc-0c6deb0fa7d4");

        public List<string> ImportablePreviousTypes  = new List<string>() {  "EY.com.eBook.RuleEngine.AY2011.FacturatieApp"  };

        public Guid GetMyType() { return TypeId; }
        
        public void SetMinimal() 
        {
            if(Data==null) {
                Data = new EY.com.eBook.RuleEngine.AY2009.VergelijkendeCijfersAppTypes.WPVergelijkendeCijfers();
                Data.LastUpdated = DateTime.Now;
            }              
            
              if(Data.Kosten==null) {
                  Data.Kosten = new List<EY.com.eBook.RuleEngine.AY2009.VergelijkendeCijfersAppTypes.CijferBlok>();;
                  Data.LastUpdated = DateTime.Now;
              }
            
              if(Data.Opbrengsten==null) {
                  Data.Opbrengsten = new List<EY.com.eBook.RuleEngine.AY2009.VergelijkendeCijfersAppTypes.CijferBlok>();;
                  Data.LastUpdated = DateTime.Now;
              }
            
        }
        
        public string ToXml() 
        {
            return CoreHelper.SerializeToString(Data);
        }

        public void LoadXml(string xml)
        {
            if (!string.IsNullOrEmpty(xml)) {
                Data = CoreHelper.DeserializeFromString<EY.com.eBook.RuleEngine.AY2009.VergelijkendeCijfersAppTypes.WPVergelijkendeCijfers>(xml);
            }
            SetMinimal();
            
        }
        
        public EY.com.eBook.RuleEngine.AY2009.VergelijkendeCijfersAppTypes.WPVergelijkendeCijfers Data { get; set; }
        
        public void Calculate(ref WorksheetSetData worksheetset)
        {
           // Data.LastCalculated = DateTime.Now;
        }
        #endregion
        
        #region AddUpdateDelete
        
          public void AddToKosten(EY.com.eBook.RuleEngine.AY2009.VergelijkendeCijfersAppTypes.CijferBlok entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void DeleteKosten(Guid rowId,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void UpdateKosten(EY.com.eBook.RuleEngine.AY2009.VergelijkendeCijfersAppTypes.CijferBlok entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void AddToOpbrengsten(EY.com.eBook.RuleEngine.AY2009.VergelijkendeCijfersAppTypes.CijferBlok entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void DeleteOpbrengsten(Guid rowId,ref WorksheetSetData worksheetset) 
          {
          
          }
          
          public void UpdateOpbrengsten(EY.com.eBook.RuleEngine.AY2009.VergelijkendeCijfersAppTypes.CijferBlok entity,ref WorksheetSetData worksheetset) 
          {
          
          }
          
        #endregion
        
        #region VALIDATION
        
        public EY.com.eBook.RuleEngine.AY2009.VergelijkendeCijfersAppTypes.CijferBlok Validate(EY.com.eBook.RuleEngine.AY2009.VergelijkendeCijfersAppTypes.CijferBlok entity, string location,ref WorksheetSetData worksheetset)
          {
          entity.IsValid = true;
          worksheetset.ClearMessages(TypeId, entity.Id);
          

          return entity;
          }
        
        #endregion
        // CALCULATION
        
        // BOOKINGS
        
        // IMPORT PREVIOUS
        
    }
}
  