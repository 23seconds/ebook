﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.RuleEngine.Core;
using EY.com.eBook.RuleEngine.Core.Interfaces;
using EY.com.eBook.RuleEngine.Data;
using EY.com.eBook.Core;

namespace EY.com.eBook.RuleEngine.AY2012
{
    
    public class DubieuzeDebiteurenApp
    {
        public static WorksheetTypeSettings Settings = new WorksheetTypeSettings
                                                      {
                                                        Clients = true
                                                        ,
                                                        Suppliers = true
                                                        ,
                                                        AllAccounts = true
                                                        , 
                                                        PerformsBookings=true
                                                      };
                                                      
        public static Guid TypeId = new Guid("a8040411-be4e-4bc7-a463-04d4f52ded46");

        public List<string> ImportablePreviousTypes  = new List<string>() {  "EY.com.eBook.RuleEngine.AY2011.FacturatieApp"  };

        public Guid GetMyType() { return TypeId; }

        public void SetMinimal()
        {
            if (Data == null)
            {
                Data = new EY.com.eBook.RuleEngine.AY2012.DubieuzeDebiteurenAppTypes.DubieuzeDebiteuren();
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.ToestandEindVorigBoekjaar == null)
            {
                Data.ToestandEindVorigBoekjaar = new List<EY.com.eBook.RuleEngine.AY2012.DubieuzeDebiteurenAppTypes.DubieuzeDebiteurT>(); ;
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.WijzigingBestaandeTijdensBoekjaar == null)
            {
                Data.WijzigingBestaandeTijdensBoekjaar = new List<EY.com.eBook.RuleEngine.AY2012.DubieuzeDebiteurenAppTypes.DubieuzeDebiteurW>(); ;
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.NieuweTijdensBoekjaar == null)
            {
                Data.NieuweTijdensBoekjaar = new List<EY.com.eBook.RuleEngine.AY2012.DubieuzeDebiteurenAppTypes.DubieuzeDebiteurN>(); ;
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.ToestandEindHuidigBoekjaar == null)
            {
                Data.ToestandEindHuidigBoekjaar = new List<EY.com.eBook.RuleEngine.AY2012.DubieuzeDebiteurenAppTypes.DubieuzeDebiteurH>(); ;
                Data.LastUpdated = DateTime.Now;
            }

        }
        
        public string ToXml() 
        {
            return CoreHelper.SerializeToString(Data);
        }

        public void LoadXml(string xml)
        {
            if (!string.IsNullOrEmpty(xml)) {
                Data = CoreHelper.DeserializeFromString<EY.com.eBook.RuleEngine.AY2012.DubieuzeDebiteurenAppTypes.DubieuzeDebiteuren>(xml);
            }
            SetMinimal();
            
        }
        
        public EY.com.eBook.RuleEngine.AY2012.DubieuzeDebiteurenAppTypes.DubieuzeDebiteuren Data { get; set; }

        public void ForceRecalculation(ref WorksheetSetData worksheetset)
        {
            Data.LastUpdated = new DateTime(1980,1,1);
            Data.ToestandEindVorigBoekjaar.ForEach(e => e.LastUpdated = null);
            Data.WijzigingBestaandeTijdensBoekjaar.ForEach(e => e.LastUpdated = null);
            Data.NieuweTijdensBoekjaar.ForEach(e => e.LastUpdated = null);
            Data.ToestandEindHuidigBoekjaar.Clear();
            if (Data.List2043 != null) Data.List2043.Clear();
            Calculate(ref worksheetset);
        }

        public void Calculate(ref WorksheetSetData worksheetset)
        {
            worksheetset.ClearMessages(TypeId);
            foreach (DubieuzeDebiteurenAppTypes.DubieuzeDebiteurT entity in Data.ToestandEindVorigBoekjaar.Where(d => !d.LastUpdated.HasValue).ToList())
            {
                UpdateToestandEindVorigBoekjaar(entity, ref worksheetset);
            }
            foreach (DubieuzeDebiteurenAppTypes.DubieuzeDebiteurW entity in Data.WijzigingBestaandeTijdensBoekjaar.Where(d => !d.LastUpdated.HasValue).ToList())
            {
                UpdateWijzigingBestaandeTijdensBoekjaar(entity, ref worksheetset);
            }
            foreach (DubieuzeDebiteurenAppTypes.DubieuzeDebiteurN entity in Data.NieuweTijdensBoekjaar.Where(d => !d.LastUpdated.HasValue).ToList())
            {
                UpdateNieuweTijdensBoekjaar(entity, ref worksheetset);
            }

            Render2043(ref worksheetset);
            Data.LastUpdated = DateTime.Now;
        }

        public void Render2043(ref WorksheetSetData worksheetset)
        {
            if (Data.List2043 == null)
            {
                Data.List2043 = new List<EY.com.eBook.RuleEngine.AY2012.DubieuzeDebiteurenAppTypes.DubieuzeDebiteur2043>();
            }
            else
            {
                Data.List2043.Clear();
            }
            Guid vrijgesteld = new Guid("9AFE7536-2A45-4146-AD96-1F7839FDA38C");
            foreach (DubieuzeDebiteurenAppTypes.DubieuzeDebiteurH dd in Data.ToestandEindHuidigBoekjaar.Where(d => d.BelastbaarOfVrijgesteld != null && d.BelastbaarOfVrijgesteld.Id == vrijgesteld && d.IsValid))
            {
                DubieuzeDebiteurenAppTypes.DubieuzeDebiteur2043 entity = new EY.com.eBook.RuleEngine.AY2012.DubieuzeDebiteurenAppTypes.DubieuzeDebiteur2043
                {
                    Id = dd.Id
                    ,
                    IsValid = true
                    ,
                    LastUpdated = DateTime.Now
                    ,
                    VorderingStart = dd.VorderingStart
                    ,
                    VorderingEinde = dd.VorderingEinde
                    ,
                    WaardeverminderingEinde = dd.WaardeverminderingEinde
                    ,
                    WaardeverminderingStart = dd.WaardeverminderingStart
                    ,
                    VermeerderingWV = dd.VermeerderingWV
                    ,
                    VerminderingWVHerschat = dd.VerminderingWVHerschat
                    ,
                    VerminderingWVInning = dd.VerminderingWVInning
                    ,
                    VerminderingWVVerlies = dd.VerminderingWVVerlies
                    ,
                    PreviousImported = false
                    ,
                    Commentaar = dd.Commentaar.GetValueOrDefault("-")
                };
                entity.TypedMember = string.Format("{0}-{1}", dd.Assesmentyear.Value.ToString(), dd.Nr.Value.ToString()); //.PadLeft(2, '0')
                if (dd.IsNew == true)
                {
                    entity.ExplicitMember = "d-ec:NewWriteDownDebtClaimMember";
                }
                else
                {
                    if (Data.WijzigingBestaandeTijdensBoekjaar.Count(d => d.Id == entity.Id) > 0)
                    {
                        if (dd.VermeerderingWV.GetValueOrDefault() > 0)
                        {
                            entity.ExplicitMember = "d-ec:RemainingBalanceAfterIncreaseWriteDownDebtClaimMember";
                        }
                        else
                        {
                            entity.ExplicitMember = "d-ec:RemainingBalanceAfterDecreaseWriteDownDebtClaimMember";
                        }
                    }
                    else
                    {
                        entity.ExplicitMember = "d-ec:UnalteredBalanceWriteDownDebtClaimMember";
                    }
                }

                if (dd.AlgemeneProvisie == true)
                {
                    entity.Schuldenaar = "Algemene provisie";
                    entity.SchuldenaarAdres = "-";
                }
                else
                {
                    if (dd.Schuldenaar != null)
                    {
                        entity.Schuldenaar = dd.Schuldenaar.NL;
                        entity.SchuldenaarAdres = "-";
                        BusinessRelationDataContract brdc = worksheetset.GetBusinessRelation(new Guid(dd.Schuldenaar.Id));
                        if (brdc != null)
                        {
                            entity.SchuldenaarAdres = string.Format("{0}, {1} {2}", brdc.Address.GetValueOrDefault(), brdc.ZipCode.GetValueOrDefault(), brdc.City.GetValueOrDefault());
                        }

                    }
                    else
                    {
                        entity.Schuldenaar = "-";
                        entity.SchuldenaarAdres = "-";
                    }
                }
                Data.List2043.Add(entity);
            }
        }

        #region AddUpdateDelete

        public void AddToToestandEindVorigBoekjaar(EY.com.eBook.RuleEngine.AY2012.DubieuzeDebiteurenAppTypes.DubieuzeDebiteurT entity, ref WorksheetSetData worksheetset)
        {
            entity = Validate(entity, "ToestandEindVorigBoekjaar", ref worksheetset);
            if (entity.IsValid)
            {
                if (!entity.Assesmentyear.HasValue)
                {
                    entity.Assesmentyear = worksheetset.DataContract.AssessmentYear - 1;
                }

                if (!entity.Nr.HasValue)
                {
                    if (Data.ToestandEindVorigBoekjaar.Count(e => e.Assesmentyear.HasValue && e.Assesmentyear == entity.Assesmentyear) > 0)
                    {
                        entity.Nr = Data.ToestandEindVorigBoekjaar.Where(e => e.Assesmentyear.HasValue && e.Assesmentyear == entity.Assesmentyear && e.Nr.HasValue).Max(e => e.Nr.Value) + 1;
                    }
                    else
                    {
                        entity.Nr = 1;
                    }
                }
                entity.Label = entity.AlgemeneProvisie.HasValue && entity.AlgemeneProvisie.Value ? "Algemene provisie" : string.Format("{0} [{1}-{2}]", entity.Schuldenaar.NL, entity.Assesmentyear, entity.Nr);

                Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
                UpdateAtEnd(entity, ref worksheetset);
                Data.ToestandEindVorigBoekjaar.Add(entity);
            }
        }

        public void DeleteToestandEindVorigBoekjaar(Guid rowId, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2012.DubieuzeDebiteurenAppTypes.DubieuzeDebiteurT original = Data.ToestandEindVorigBoekjaar.FirstOrDefault(e => e.Id == rowId);
            if (original != null)
            {
                /*
                if (original.Boeken.HasValue && original.Boeken.Value)
                {
                    worksheetset.DeleteBooking(FacturatieApp.TypeId, "OpTeMakenCN", original.Id, null);
                    // DELETE BOOKING
                }*/
                Data.ToestandEindVorigBoekjaar.Remove(original);

                List<EY.com.eBook.RuleEngine.AY2012.DubieuzeDebiteurenAppTypes.DubieuzeDebiteurW> wijzigingen = Data.WijzigingBestaandeTijdensBoekjaar.Where(e => e.Bestaande.Id == rowId.ToString()).ToList();

                if (wijzigingen.Count > 0)
                {
                    foreach (EY.com.eBook.RuleEngine.AY2012.DubieuzeDebiteurenAppTypes.DubieuzeDebiteurW wijziging in wijzigingen)
                    {
                        DeleteWijzigingBestaandeTijdensBoekjaar(wijziging.Id, ref worksheetset);
                    }
                }

                EY.com.eBook.RuleEngine.AY2012.DubieuzeDebiteurenAppTypes.DubieuzeDebiteurH huidige = Data.ToestandEindHuidigBoekjaar.FirstOrDefault(e => e.Id == rowId);
                if (huidige != null)
                {
                    Data.ToestandEindHuidigBoekjaar.Remove(huidige);
                }

                Data.LastUpdated = DateTime.Now;
            }
        }

        public void UpdateToestandEindVorigBoekjaar(EY.com.eBook.RuleEngine.AY2012.DubieuzeDebiteurenAppTypes.DubieuzeDebiteurT entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2012.DubieuzeDebiteurenAppTypes.DubieuzeDebiteurT original = Data.ToestandEindVorigBoekjaar.FirstOrDefault(e => e.Id == entity.Id);
            if (original == null)
            {
                AddToToestandEindVorigBoekjaar(entity, ref worksheetset);
            }
            else
            {
                entity = Validate(entity, "ToestandEindVorigBoekjaar", ref worksheetset);
                if (entity.IsValid)
                {
                    if (!entity.Assesmentyear.HasValue)
                    {
                        entity.Assesmentyear = worksheetset.DataContract.AssessmentYear - 1;
                    }

                    if (!entity.Nr.HasValue)
                    {
                        if (Data.ToestandEindVorigBoekjaar.Count(e => e.Assesmentyear.HasValue && e.Assesmentyear == entity.Assesmentyear && e.Nr.HasValue) > 0)
                        {
                            entity.Nr = Data.ToestandEindVorigBoekjaar.Where(e => e.Assesmentyear.HasValue && e.Assesmentyear==entity.Assesmentyear && e.Nr.HasValue).Max(e => e.Nr.Value) + 1;
                        }
                        else
                        {
                            entity.Nr = 1;
                        }
                    }

                    entity.Label = entity.AlgemeneProvisie.HasValue && entity.AlgemeneProvisie.Value ? "Algemene provisie" : string.Format("{0} [{1}-{2}]", entity.Schuldenaar.NL, entity.Assesmentyear, entity.Nr);
                // Get All wijzigingen
                    foreach (EY.com.eBook.RuleEngine.AY2012.DubieuzeDebiteurenAppTypes.DubieuzeDebiteurW wijziging in Data.WijzigingBestaandeTijdensBoekjaar.Where(e => e.Bestaande.Id == e.Id.ToString()))
                    {
                        UpdateWijzigingBestaandeTijdensBoekjaar(wijziging, ref worksheetset);
                    }

               
                    int idx = Data.ToestandEindVorigBoekjaar.IndexOf(original);
                    Data.ToestandEindVorigBoekjaar.RemoveAt(idx);
                    Data.ToestandEindVorigBoekjaar.Insert(idx, entity);
                    UpdateAtEnd(entity, ref worksheetset);
                    Data.LastUpdated = DateTime.Now;
                    entity.LastUpdated = DateTime.Now;
                }
            }
        }

        public void PerformBookingWijziging(DubieuzeDebiteurenAppTypes.DubieuzeDebiteurW entity, ref WorksheetSetData worksheetset, bool update)
        {
            /*
            // TO DISCUSS WITH MIEKE
            decimal vorderingBedrag = entity.AlgemeneProvisie == true ? entity.VorderingStart.GetValueOrDefault() : entity.OrigineleFactuurWaarde.GetValueOrDefault();

            List<BookingLineDataContract> lines = new List<BookingLineDataContract>();
            if (entity.DefinitiefVerlies == true)
            {
                bool faillissement = entity.RedenVanAfname.Id.ToString().ToLower() == "ab8bc803-053d-45b7-9e64-60f5b0f17a4a";
                

                decimal total = vorderingBedrag;
                decimal btwCoeff = !entity.AlgemeneProvisie.GetValueOrDefault() && entity.OrigineleFactuurWaardeBTW.GetValueOrDefault() > 0 ? entity.OrigineleFactuurWaarde.GetValueOrDefault() / entity.OrigineleFactuurWaardeBTW.GetValueOrDefault() : 1;
                decimal btwLoss = entity.OrigineleFactuurWaardeBTW.GetValueOrDefault();
                decimal btwRemaining = 0;
                decimal geboekteWV = total - entity.VorderingStart.GetValueOrDefault();
                
              //  decimal meerwaarde = (((total - geboekteWV) - btwLoss) - btwRemaining) - remaining;

                decimal factuurBedragIncl = vorderingBedrag + entity.OrigineleFactuurWaardeBTW.GetValueOrDefault();
                decimal remainingWV = vorderingBedrag - entity.WaardeverminderingStart.GetValueOrDefault();



                lines.Add(new BookingLineDataContract { AccountNr = worksheetset.GetListAccount("409"), Amount = entity.WaardeverminderingStart.GetValueOrDefault() }); //debet
                lines.Add(new BookingLineDataContract { AccountNr = worksheetset.GetListAccount("407"), Amount = 0-factuurBedragIncl }); // credit
                if (btwLoss != 0) lines.Add(new BookingLineDataContract { AccountNr = worksheetset.GetListAccount("411"), Amount = btwLoss }); // debet
                if (remainingWV != 0) lines.Add(new BookingLineDataContract { AccountNr = worksheetset.GetListAccount("642"), Amount = remainingWV }); // debet
                

                if (entity.WaardeverminderingWaarde.GetValueOrDefault() < entity.WaardeverminderingStart.GetValueOrDefault())
                {
                    if (faillissement)
                    {
                        
                        if (btwLoss != 0) lines.Add(new BookingLineDataContract { AccountNr = worksheetset.GetListAccount("411"), Amount = btwLoss });
                        lines.Add(new BookingLineDataContract { AccountNr = worksheetset.GetListAccount("742"), Amount = meerwaarde });
                        lines.Add(new BookingLineDataContract { AccountNr = worksheetset.GetListAccount("400"), Amount = remaining + btwRemaining });
                    }
                    else
                    {
                        if (btwLoss != 0) lines.Add(new BookingLineDataContract { AccountNr = worksheetset.GetListAccount("64012"), Amount = btwLoss });
                        lines.Add(new BookingLineDataContract { AccountNr = worksheetset.GetListAccount("742"), Amount = meerwaarde });
                        lines.Add(new BookingLineDataContract { AccountNr = worksheetset.GetListAccount("400"), Amount = remaining + btwRemaining });
                    }
                }
                else
                {
                    if (faillissement)
                    {
                        if (btwLoss != 0) lines.Add(new BookingLineDataContract { AccountNr = worksheetset.GetListAccount("411"), Amount = btwLoss });
                        lines.Add(new BookingLineDataContract { AccountNr = worksheetset.GetListAccount("642"), Amount = meerwaarde });
                        lines.Add(new BookingLineDataContract { AccountNr = worksheetset.GetListAccount("400"), Amount = remaining + btwRemaining });
                    }
                    else
                    {
                        if (btwLoss != 0) lines.Add(new BookingLineDataContract { AccountNr = worksheetset.GetListAccount("64012"), Amount = btwLoss });
                        lines.Add(new BookingLineDataContract { AccountNr = worksheetset.GetListAccount("742"), Amount = meerwaarde });
                        lines.Add(new BookingLineDataContract { AccountNr = worksheetset.GetListAccount("400"), Amount = remaining + btwRemaining });
                    }
                }

            }
            else
            {
                switch (entity.ToenameOfTerugname.Id.ToString().ToLower())
                {
                    case "02080021-45dd-4397-9a56-1b1e11a248fc": //VERMINDERING
                        decimal vermindering = entity.WaardeverminderingWaarde.GetValueOrDefault();
                        if (vermindering > 0)
                        {
                            lines = new List<BookingLineDataContract>() {
                                        new BookingLineDataContract { AccountNr =worksheetset.GetListAccount("409"), Amount=entity.VermeerderingWV.GetValueOrDefault() }
                                        , new BookingLineDataContract { AccountNr = worksheetset.GetListAccount("6341"), Amount=0-entity.VermeerderingWV.GetValueOrDefault()}
                                };
                            if (Math.Abs(entity.VorderingEinde.GetValueOrDefault() - entity.VorderingStart.GetValueOrDefault()) == vermindering)
                            {
                                lines.Add(new BookingLineDataContract { AccountNr = worksheetset.GetListAccount("400"), Amount = entity.VorderingEinde.GetValueOrDefault() });
                                lines.Add(new BookingLineDataContract { AccountNr = worksheetset.GetListAccount("407"), Amount = 0 - entity.VorderingEinde.GetValueOrDefault() });
                            }
                        }
                        break;
                    case "29ff95cc-30dd-4ab5-9a59-ed8c5e3133b1": //VERHOGING
                        if (entity.VermeerderingWV.GetValueOrDefault() != 0)
                        {
                            lines = new List<BookingLineDataContract>() {
                                        new BookingLineDataContract { AccountNr =worksheetset.GetListAccount("634"), Amount=entity.VermeerderingWV.GetValueOrDefault() }
                                        , new BookingLineDataContract { AccountNr = worksheetset.GetListAccount("409"), Amount=0-entity.VermeerderingWV.GetValueOrDefault()}
                            };
                        }

                        break;
                }
            }

            if (update)
            {
                worksheetset.UpdateBooking(TypeId, "WijzigingBestaandeTijdensBoekjaar", entity.Id, null, lines);
            }
            else
            {
                worksheetset.AddBooking(TypeId, "WijzigingBestaandeTijdensBoekjaar", entity.Id, null, lines);
            }
            

              */      
        }

        public void AddToWijzigingBestaandeTijdensBoekjaar(EY.com.eBook.RuleEngine.AY2012.DubieuzeDebiteurenAppTypes.DubieuzeDebiteurW entity, ref WorksheetSetData worksheetset)
        {
            entity = Validate(entity, "WijzigingBestaandeTijdensBoekjaar", ref worksheetset);
            if (entity.IsValid)
            {
                EY.com.eBook.RuleEngine.AY2012.DubieuzeDebiteurenAppTypes.DubieuzeDebiteurT source = Data.ToestandEindVorigBoekjaar.FirstOrDefault(e => e.Id.ToString() == entity.Bestaande.Id);

                if (source != null)
                {
                    if (!entity.VorderingEinde.HasValue)
                    {
                        entity.VorderingEinde = entity.VorderingStart;
                    }
                    entity.AlgemeneProvisie = source.AlgemeneProvisie;
                    entity.Assesmentyear = source.Assesmentyear;
                    entity.BelastbaarOfVrijgesteld = source.BelastbaarOfVrijgesteld;
                    entity.FactuurJaar = source.FactuurJaar;
                    entity.FactuurNummer = source.FactuurNummer;
                    entity.Nr = source.Nr;
                    entity.OrigineleFactuurWaarde = source.OrigineleFactuurWaarde;
                    entity.Schuldenaar = source.Schuldenaar;
                    entity.VorderingStart = source.Vordering;
                    entity.WaardeverminderingStart = source.Waardevermindering;
                    switch (entity.ToenameOfTerugname.Id.ToString().ToLower())
                    {
                        case "02080021-45dd-4397-9a56-1b1e11a248fc":
                            entity.VermeerderingWV = null;
                            entity.VerminderingWVHerschat = null;
                            entity.VerminderingWVInning = null;
                            entity.VerminderingWVVerlies = null;
                            switch (entity.RedenVanAfname.Id.ToString().ToLower())
                            {
                                case "d0ea7f27-c6bb-4642-bce5-88b4cc2cc04e":
                                    entity.VerminderingWVHerschat = Math.Abs(entity.WaardeverminderingWaarde.Value);
                                    break;
                                case "e0bd15c4-ee1b-4ad4-aa3d-6002564e9657":
                                    entity.VerminderingWVInning = Math.Abs(entity.WaardeverminderingWaarde.Value);
                                    break;
                                /*
                                 * Ticket#64
                                case "27dffd1d-b124-421d-8684-6d22bd719601":
                                    entity.VerminderingWVVerlies = Math.Abs(entity.WaardeverminderingWaarde.Value);
                                    break;
                                 * 
                                */
                                case "870496c8-beda-44f1-98b0-5f3060055048":
                                    entity.VerminderingWVVerlies = Math.Abs(entity.WaardeverminderingWaarde.Value);
                                    break;
                            }
                            break;
                        case "29ff95cc-30dd-4ab5-9a59-ed8c5e3133b1":
                            entity.VermeerderingWV = Math.Abs(entity.WaardeverminderingWaarde.Value);
                            entity.VerminderingWVHerschat = null;
                            entity.VerminderingWVInning = null;
                            entity.VerminderingWVVerlies = null;
                            break;
                    }


                    decimal change = 0;
                    if (entity.VermeerderingWV.HasValue) change += entity.VermeerderingWV.Value;
                    if (entity.VerminderingWVHerschat.HasValue) change -= entity.VerminderingWVHerschat.Value;
                    if (entity.VerminderingWVInning.HasValue) change -= entity.VerminderingWVInning.Value;
                    if (entity.VerminderingWVVerlies.HasValue) change -= entity.VerminderingWVVerlies.Value;

                    entity.WaardeverminderingEinde = entity.WaardeverminderingStart + change;
                    if (entity.DefinitiefVerlies.HasValue && entity.DefinitiefVerlies.Value && source != null)
                    {
                        entity.VerminderingWVVerlies = source.Waardevermindering.Value;
                       // entity.WaardeverminderingEinde = source.Waardevermindering.Value;
                        entity.WaardeverminderingEinde = 0;
                        entity.VorderingEinde = 0;
                    }
                    UpdateAtEnd(entity, ref worksheetset);

                    if (entity.Book.HasValue && entity.Book.Value)
                    {
                        PerformBookingWijziging(entity, ref worksheetset, false);
                    }
                }

                Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
                Data.WijzigingBestaandeTijdensBoekjaar.Add(entity);
            }
        }

        public void DeleteWijzigingBestaandeTijdensBoekjaar(Guid rowId, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2012.DubieuzeDebiteurenAppTypes.DubieuzeDebiteurW original = Data.WijzigingBestaandeTijdensBoekjaar.FirstOrDefault(e => e.Id == rowId);
            if (original != null)
            {
                if (original.Book.HasValue && original.Book.Value)
                {
                    worksheetset.DeleteBooking(TypeId, "WijzigingBestaandeTijdensBoekjaar", original.Id, null);
                }
                Data.WijzigingBestaandeTijdensBoekjaar.Remove(original);
                EY.com.eBook.RuleEngine.AY2012.DubieuzeDebiteurenAppTypes.DubieuzeDebiteurH huidige = Data.ToestandEindHuidigBoekjaar.FirstOrDefault(e => e.Id == rowId);
                if (huidige != null)
                {
                    Data.ToestandEindHuidigBoekjaar.Remove(huidige);
                    if (original.Bestaande != null)
                    {
                        EY.com.eBook.RuleEngine.AY2012.DubieuzeDebiteurenAppTypes.DubieuzeDebiteurT prev = Data.ToestandEindVorigBoekjaar.FirstOrDefault(t => t.Id.ToString() == original.Bestaande.Id);
                        if(prev!=null) UpdateAtEnd(prev, ref worksheetset);
                    }
                }
                
                Data.LastUpdated = DateTime.Now;
            }
        }

        public void UpdateWijzigingBestaandeTijdensBoekjaar(EY.com.eBook.RuleEngine.AY2012.DubieuzeDebiteurenAppTypes.DubieuzeDebiteurW entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2012.DubieuzeDebiteurenAppTypes.DubieuzeDebiteurW original = Data.WijzigingBestaandeTijdensBoekjaar.FirstOrDefault(e => e.Id == entity.Id);
            if (original == null)
            {
                AddToWijzigingBestaandeTijdensBoekjaar(entity, ref worksheetset);
            }
            else
            {
                entity = Validate(entity, "WijzigingBestaandeTijdensBoekjaar", ref worksheetset);
                EY.com.eBook.RuleEngine.AY2012.DubieuzeDebiteurenAppTypes.DubieuzeDebiteurT source = Data.ToestandEindVorigBoekjaar.FirstOrDefault(e => e.Id.ToString() == entity.Bestaande.Id);


                if (source != null)
                {
                    entity.Label = source.Label;
                    entity.AlgemeneProvisie = source.AlgemeneProvisie;
                    entity.Assesmentyear = source.Assesmentyear;
                    entity.BelastbaarOfVrijgesteld = source.BelastbaarOfVrijgesteld;
                    if (entity.BelastbaarOfVrijgesteld == null)
                    {
                        worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", "WijzigingBestaandeTijdensBoekjaar", entity.Id, "BelastbaarOfVrijgesteld");
                        worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", "ToestandEindVorigBoekjaar", source.Id, "BelastbaarOfVrijgesteld");
                        entity.IsValid = false;
                    }
                    entity.FactuurJaar = source.FactuurJaar;
                    entity.FactuurNummer = source.FactuurNummer;
                    entity.Nr = source.Nr;
                    entity.OrigineleFactuurWaarde = source.OrigineleFactuurWaarde;
                    entity.OrigineleFactuurWaardeBTW = source.OrigineleFactuurWaardeBTW;
                    entity.Schuldenaar = source.Schuldenaar;
                    entity.VorderingStart = source.Vordering;
                    entity.WaardeverminderingStart = source.Waardevermindering;
                }
                else
                {
                }

                if (entity.IsValid)
                {
                    if (!entity.VorderingEinde.HasValue)
                    {
                        entity.VorderingEinde = entity.VorderingStart;
                    }

                    
                    switch (entity.ToenameOfTerugname.Id.ToString().ToLower())
                    {
                        case "02080021-45dd-4397-9a56-1b1e11a248fc":
                            entity.VermeerderingWV = null;
                            entity.VerminderingWVHerschat = null;
                            entity.VerminderingWVInning = null;
                            entity.VerminderingWVVerlies = null;
                            switch (entity.RedenVanAfname.Id.ToString().ToLower())
                            {
                                case "d0ea7f27-c6bb-4642-bce5-88b4cc2cc04e":
                                    entity.VerminderingWVHerschat = Math.Abs(entity.WaardeverminderingWaarde.Value);
                                    break;
                                case "e0bd15c4-ee1b-4ad4-aa3d-6002564e9657":
                                    entity.VerminderingWVInning = Math.Abs(entity.WaardeverminderingWaarde.Value);
                                    break;
                                /*
                                  * Ticket#64
                                 case "27dffd1d-b124-421d-8684-6d22bd719601":
                                     entity.VerminderingWVVerlies = Math.Abs(entity.WaardeverminderingWaarde.Value);
                                     break;
                                  * 
                                 */
                                case "870496c8-beda-44f1-98b0-5f3060055048":
                                    entity.VerminderingWVVerlies = Math.Abs(entity.WaardeverminderingWaarde.Value);
                                    break;
                            }
                            break;
                        case "29ff95cc-30dd-4ab5-9a59-ed8c5e3133b1":
                            entity.VermeerderingWV = Math.Abs(entity.WaardeverminderingWaarde.Value);
                            entity.VerminderingWVHerschat = null;
                            entity.VerminderingWVInning = null;
                            entity.VerminderingWVVerlies = null;
                            break;
                    }


                    decimal change = 0;
                    if (entity.VermeerderingWV.HasValue) change += entity.VermeerderingWV.Value;
                    if (entity.VerminderingWVHerschat.HasValue) change -= entity.VerminderingWVHerschat.Value;
                    if (entity.VerminderingWVInning.HasValue) change -= entity.VerminderingWVInning.Value;
                    if (entity.VerminderingWVVerlies.HasValue) change -= entity.VerminderingWVVerlies.Value;

                    entity.WaardeverminderingEinde = entity.WaardeverminderingStart + change;
                    if (entity.DefinitiefVerlies.HasValue && entity.DefinitiefVerlies.Value && source!=null) 
                    {
                        entity.VerminderingWVVerlies = source.Waardevermindering.Value;
                        entity.WaardeverminderingEinde = 0;
                        //entity.WaardeverminderingEinde = source.Waardevermindering.Value;
                        entity.VorderingEinde = 0;
                    }
                    
                    if (entity.Book.HasValue && entity.Book.Value)
                    {
                        PerformBookingWijziging(entity, ref worksheetset, (original.Book.HasValue && original.Book.Value));
                    }

                    int idx = Data.WijzigingBestaandeTijdensBoekjaar.IndexOf(original);
                    Data.WijzigingBestaandeTijdensBoekjaar.RemoveAt(idx);
                    Data.WijzigingBestaandeTijdensBoekjaar.Insert(idx, entity);
                    UpdateAtEnd(entity, ref worksheetset);
                    Data.LastUpdated = DateTime.Now;
                    entity.LastUpdated = DateTime.Now;
                }
            }
        }

        public void AddToNieuweTijdensBoekjaar(EY.com.eBook.RuleEngine.AY2012.DubieuzeDebiteurenAppTypes.DubieuzeDebiteurN entity, ref WorksheetSetData worksheetset)
        {
            entity = Validate(entity, "NieuweTijdensBoekjaar", ref worksheetset);
            if (entity.IsValid)
            {
                entity.Assesmentyear = worksheetset.DataContract.AssessmentYear;
                if (!entity.Nr.HasValue)
                {
                    if (Data.NieuweTijdensBoekjaar.Count > 0)
                    {
                        entity.Nr = Data.NieuweTijdensBoekjaar.Where(e => e.Assesmentyear.HasValue && e.Nr.HasValue).Max(e => e.Nr.Value) + 1;
                    }
                    else
                    {
                        entity.Nr = 1;
                    }
                }

                if (entity.Book.HasValue && entity.Book.Value)
                {
                    decimal amount = entity.OrigineleFactuurWaarde.GetValueOrDefault() + entity.OrigineleFactuurWaardeBTW.GetValueOrDefault();

                    List<BookingLineDataContract> lines = new List<BookingLineDataContract>() {
                                        new BookingLineDataContract { AccountNr =worksheetset.GetListAccount("407"), Amount=amount }
                                        , new BookingLineDataContract { AccountNr =worksheetset.GetListAccount("400"), Amount=0-amount }
                    };
                    if (entity.WaardeverminderingEinde.GetValueOrDefault() != 0)
                    {
                        lines.Add(new BookingLineDataContract { AccountNr = worksheetset.GetListAccount("634"), Amount = entity.WaardeverminderingEinde.GetValueOrDefault() });
                        lines.Add(new BookingLineDataContract { AccountNr = worksheetset.GetListAccount("409"), Amount = 0-entity.WaardeverminderingEinde.GetValueOrDefault() });
                    }
                    worksheetset.AddBooking(TypeId, "NieuweTijdensBoekjaar", entity.Id, null, lines);
                }

                Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
                Data.NieuweTijdensBoekjaar.Add(entity);
                UpdateAtEnd(entity, ref worksheetset);
            }
        }

        public void DeleteNieuweTijdensBoekjaar(Guid rowId, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2012.DubieuzeDebiteurenAppTypes.DubieuzeDebiteurN original = Data.NieuweTijdensBoekjaar.FirstOrDefault(e => e.Id == rowId);
            if (original != null)
            {
                if (original.Book.HasValue && original.Book.Value)
                {
                    worksheetset.DeleteBooking(TypeId, "NieuweTijdensBoekjaar", original.Id, null);
                }
                Data.NieuweTijdensBoekjaar.Remove(original);
                EY.com.eBook.RuleEngine.AY2012.DubieuzeDebiteurenAppTypes.DubieuzeDebiteurH huidige = Data.ToestandEindHuidigBoekjaar.FirstOrDefault(e => e.Id == rowId);
                if (huidige != null)
                {
                    Data.ToestandEindHuidigBoekjaar.Remove(huidige);
                }
                Data.LastUpdated = DateTime.Now;
            }
        }

        public void UpdateNieuweTijdensBoekjaar(EY.com.eBook.RuleEngine.AY2012.DubieuzeDebiteurenAppTypes.DubieuzeDebiteurN entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2012.DubieuzeDebiteurenAppTypes.DubieuzeDebiteurN original = Data.NieuweTijdensBoekjaar.FirstOrDefault(e => e.Id == entity.Id);
            if (original == null)
            {
                AddToNieuweTijdensBoekjaar(entity, ref worksheetset);
            }
            else
            {
                entity = Validate(entity, "NieuweTijdensBoekjaar", ref worksheetset);
                entity.Assesmentyear = worksheetset.DataContract.AssessmentYear;
                if (!entity.Nr.HasValue)
                {
                    if (Data.NieuweTijdensBoekjaar.Count(e => e.Assesmentyear.HasValue && e.Nr.HasValue) > 0)
                    {
                        entity.Nr = Data.NieuweTijdensBoekjaar.Where(e => e.Assesmentyear.HasValue && e.Nr.HasValue).Max(e => e.Nr.Value) + 1;
                    }
                    else
                    {
                        entity.Nr = 1;
                    }
                }
                if (entity.IsValid)
                {
                    if (original.Book.HasValue && original.Book.Value && (!entity.Book.HasValue || ! entity.Book.Value))
                    {
                        worksheetset.DeleteBooking(TypeId, "NieuweTijdensBoekjaar", original.Id, null);
                    }
                    if (entity.Book.HasValue && entity.Book.Value)
                    {
                        decimal amount = entity.OrigineleFactuurWaarde.GetValueOrDefault() + entity.OrigineleFactuurWaardeBTW.GetValueOrDefault();

                        List<BookingLineDataContract> lines = new List<BookingLineDataContract>() {
                                        new BookingLineDataContract { AccountNr =worksheetset.GetListAccount("407"), Amount=amount }
                                        , new BookingLineDataContract { AccountNr =worksheetset.GetListAccount("400"), Amount=0-amount }
                    };
                        if (entity.WaardeverminderingEinde.GetValueOrDefault() != 0)
                        {
                            lines.Add(new BookingLineDataContract { AccountNr = worksheetset.GetListAccount("634"), Amount = entity.WaardeverminderingEinde.GetValueOrDefault() });
                            lines.Add(new BookingLineDataContract { AccountNr = worksheetset.GetListAccount("409"), Amount = 0 - entity.WaardeverminderingEinde.GetValueOrDefault() });
                        }
                        if (original.Book.HasValue && original.Book.Value)
                        {
                            worksheetset.UpdateBooking(TypeId, "NieuweTijdensBoekjaar", entity.Id, null, lines);
                        }
                        else
                        {
                            worksheetset.AddBooking(TypeId, "NieuweTijdensBoekjaar", entity.Id, null, lines);
                        }
                    }

                    int idx = Data.NieuweTijdensBoekjaar.IndexOf(original);
                    Data.NieuweTijdensBoekjaar.RemoveAt(idx);
                    Data.NieuweTijdensBoekjaar.Insert(idx, entity);
                    UpdateAtEnd(entity, ref worksheetset);
                    Data.LastUpdated = DateTime.Now;
                    entity.LastUpdated = DateTime.Now;
                }
            }
        }



        public void UpdateAtEnd(EY.com.eBook.RuleEngine.AY2012.DubieuzeDebiteurenAppTypes.DubieuzeDebiteurN sourceEntity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2012.DubieuzeDebiteurenAppTypes.DubieuzeDebiteurH huidig = Data.ToestandEindHuidigBoekjaar.FirstOrDefault(e => e.Id == sourceEntity.Id);
            if (huidig == null)
            {
                huidig = new EY.com.eBook.RuleEngine.AY2012.DubieuzeDebiteurenAppTypes.DubieuzeDebiteurH
                {
                    Id = sourceEntity.Id
                };
                Data.ToestandEindHuidigBoekjaar.Add(huidig);
            }
            huidig.AlgemeneProvisie = sourceEntity.AlgemeneProvisie;
            huidig.Assesmentyear = sourceEntity.Assesmentyear;
            huidig.BelastbaarOfVrijgesteld = sourceEntity.BelastbaarOfVrijgesteld;
            huidig.Commentaar = sourceEntity.Commentaar;
            huidig.FactuurJaar = sourceEntity.FactuurJaar;
            huidig.FactuurNummer = sourceEntity.FactuurNummer;
            huidig.IsNew = true;
            huidig.IsValid = true;
            huidig.LastUpdated = DateTime.Now;
            huidig.Nr = sourceEntity.Nr;
            huidig.OrigineleFactuurWaarde = sourceEntity.OrigineleFactuurWaarde;
            huidig.OrigineleFactuurWaardeBTW = sourceEntity.OrigineleFactuurWaardeBTW;
            huidig.PreviousImported = false;
            huidig.Schuldenaar = sourceEntity.Schuldenaar;
            huidig.VermeerderingWV =0;
            huidig.VerminderingWVHerschat =0;
            huidig.VerminderingWVInning = 0;
            huidig.VerminderingWVVerlies = 0;
            huidig.VorderingEinde = sourceEntity.VorderingEinde;
            huidig.VorderingStart = 0;
            huidig.WaardeverminderingEinde = sourceEntity.WaardeverminderingEinde;
            huidig.WaardeverminderingStart = 0;
        }

        public void UpdateAtEnd(EY.com.eBook.RuleEngine.AY2012.DubieuzeDebiteurenAppTypes.DubieuzeDebiteurW sourceEntity, ref WorksheetSetData worksheetset)
        {

            EY.com.eBook.RuleEngine.AY2012.DubieuzeDebiteurenAppTypes.DubieuzeDebiteurH huidigVorige = Data.ToestandEindHuidigBoekjaar.FirstOrDefault(e => e.Id.ToString() == sourceEntity.Bestaande.Id);
            if (huidigVorige != null)
            {
                Data.ToestandEindHuidigBoekjaar.Remove(huidigVorige);
            }
            
            EY.com.eBook.RuleEngine.AY2012.DubieuzeDebiteurenAppTypes.DubieuzeDebiteurH huidig = Data.ToestandEindHuidigBoekjaar.FirstOrDefault(e => e.Id == sourceEntity.Id);
            if (huidig == null)
            {
                huidig = new EY.com.eBook.RuleEngine.AY2012.DubieuzeDebiteurenAppTypes.DubieuzeDebiteurH
                {
                    Id = sourceEntity.Id
                };
                Data.ToestandEindHuidigBoekjaar.Add(huidig);
            }
            huidig.AlgemeneProvisie = sourceEntity.AlgemeneProvisie;
            huidig.Assesmentyear = sourceEntity.Assesmentyear;
            huidig.BelastbaarOfVrijgesteld = sourceEntity.BelastbaarOfVrijgesteld;
            huidig.Commentaar = sourceEntity.Commentaar;
            huidig.FactuurJaar = sourceEntity.FactuurJaar;
            huidig.FactuurNummer = sourceEntity.FactuurNummer;
            huidig.IsNew = false;
            huidig.IsValid = true;
            huidig.LastUpdated = DateTime.Now;
            huidig.Nr = sourceEntity.Nr;
            huidig.OrigineleFactuurWaarde = sourceEntity.OrigineleFactuurWaarde;
            huidig.OrigineleFactuurWaardeBTW = sourceEntity.OrigineleFactuurWaardeBTW;
            huidig.PreviousImported = false;
            huidig.Schuldenaar = sourceEntity.Schuldenaar;
            huidig.VermeerderingWV = sourceEntity.VermeerderingWV;
            huidig.VerminderingWVHerschat = sourceEntity.VerminderingWVHerschat;
            huidig.VerminderingWVInning = sourceEntity.VerminderingWVInning;
            huidig.VerminderingWVVerlies = sourceEntity.VerminderingWVVerlies;
            huidig.VorderingEinde = sourceEntity.VorderingEinde;
            huidig.VorderingStart = sourceEntity.VorderingStart;
            huidig.WaardeverminderingEinde = sourceEntity.WaardeverminderingEinde;
            huidig.WaardeverminderingStart = sourceEntity.WaardeverminderingStart;
        }

        public void UpdateAtEnd(EY.com.eBook.RuleEngine.AY2012.DubieuzeDebiteurenAppTypes.DubieuzeDebiteurT sourceEntity, ref WorksheetSetData worksheetset)
        {
            List<EY.com.eBook.RuleEngine.AY2012.DubieuzeDebiteurenAppTypes.DubieuzeDebiteurW> wijzigingen = Data.WijzigingBestaandeTijdensBoekjaar.Where(e => e.Bestaande.Id == sourceEntity.Id.ToString()).ToList();

            if (wijzigingen.Count > 0)
            {
                foreach (EY.com.eBook.RuleEngine.AY2012.DubieuzeDebiteurenAppTypes.DubieuzeDebiteurW wijziging in wijzigingen)
                {
                    UpdateAtEnd(wijziging, ref worksheetset);
                }
            }
            else
            {
                EY.com.eBook.RuleEngine.AY2012.DubieuzeDebiteurenAppTypes.DubieuzeDebiteurH huidig = Data.ToestandEindHuidigBoekjaar.FirstOrDefault(e => e.Id == sourceEntity.Id);
                if (huidig == null)
                {
                    huidig = new EY.com.eBook.RuleEngine.AY2012.DubieuzeDebiteurenAppTypes.DubieuzeDebiteurH
                    {
                        Id = sourceEntity.Id
                    };
                    Data.ToestandEindHuidigBoekjaar.Add(huidig);
                }
                huidig.AlgemeneProvisie= sourceEntity.AlgemeneProvisie;
                huidig.Assesmentyear= sourceEntity.Assesmentyear;
                huidig.BelastbaarOfVrijgesteld = sourceEntity.BelastbaarOfVrijgesteld;
                huidig.Commentaar = "";
                huidig.FactuurJaar = sourceEntity.FactuurJaar;
                huidig.FactuurNummer = sourceEntity.FactuurNummer;
                huidig.IsNew = false;
                huidig.IsValid = true;
                huidig.LastUpdated = DateTime.Now;
                huidig.Nr = sourceEntity.Nr;
                huidig.OrigineleFactuurWaarde = sourceEntity.OrigineleFactuurWaarde;
                huidig.OrigineleFactuurWaardeBTW = sourceEntity.OrigineleFactuurWaardeBTW;
                huidig.PreviousImported = false;
                huidig.Schuldenaar = sourceEntity.Schuldenaar;
                huidig.VermeerderingWV = 0;
                huidig.VerminderingWVHerschat = 0;
                huidig.VerminderingWVInning = 0;
                huidig.VerminderingWVVerlies = 0;
                huidig.VorderingEinde = sourceEntity.Vordering;
                huidig.VorderingStart = sourceEntity.Vordering;
                huidig.WaardeverminderingEinde = sourceEntity.Waardevermindering;
                huidig.WaardeverminderingStart = sourceEntity.Waardevermindering;
                        
            }

        }

        #endregion
        
        // CALCULATION
        
        // BOOKINGS

        public void ImportPrevious(ref WorksheetSetData wsd)
        {
            if (wsd.DataContract.PreviousFileId.HasValue)
            {
                if (wsd.PreviousSet.AssessmentYear == 2011)
                {
                    EY.com.eBook.RuleEngine.AY2011.WorksheetSetData pset = (EY.com.eBook.RuleEngine.AY2011.WorksheetSetData)wsd.PreviousSet;

                    EY.com.eBook.RuleEngine.AY2011.DubieuzeDebiteurenApp previous = pset.DubieuzeDebiteurenApp;

                    int prev = 1;
                    int cur = 1;

                    foreach (AY2011.DubieuzeDebiteurenAppTypes.DubieuzeDebiteurH import in previous.Data.ToestandEindHuidigBoekjaar.Where(d => d.WaardeverminderingTotaal.HasValue && d.WaardeverminderingTotaal.Value != 0))
                    {
                        int assess = import.isNew.HasValue && import.isNew == true ? 2011 : 2010;
                        int nr = 1;
                        if (assess == 2010)
                        {
                            nr = prev;
                            prev++;
                        }
                        else
                        {
                            nr = cur;
                            cur++;
                        }
                        AY2012.DubieuzeDebiteurenAppTypes.DubieuzeDebiteurT item = Data.ToestandEindVorigBoekjaar.FirstOrDefault(t => t.Id == import.Id);
                        if (item == null)
                        {
                            item = new EY.com.eBook.RuleEngine.AY2012.DubieuzeDebiteurenAppTypes.DubieuzeDebiteurT
                            {
                                Id = import.Id
                            };
                            Data.ToestandEindVorigBoekjaar.Add(item);
                        }
                        item.AlgemeneProvisie = import.AlgemeneProvisie;
                        item.Assesmentyear = assess;
                        item.Nr = nr;
                        item.BelastbaarOfVrijgesteld = import.BelastbaarOfVrijgesteld;
                        item.FactuurJaar = import.FactuurJaar;
                        item.FactuurNummer = import.InvoiceNumber;
                        item.Label = import.label;
                        item.LastUpdated = null;
                        item.OrigineleFactuurWaarde = import.FactuurBedrag;
                        item.OrigineleFactuurWaardeBTW = import.FactuurVAT;
                        item.PreviousImported = true;
                        item.Schuldenaar = import.Klant;
                        item.Vordering = import.VorderingEinde;
                        item.Waardevermindering = import.WaardeverminderingTotaal;
                    }
                }
            }
        }


        #region VALIDATION

        public EY.com.eBook.RuleEngine.AY2012.DubieuzeDebiteurenAppTypes.DubieuzeDebiteurT Validate(EY.com.eBook.RuleEngine.AY2012.DubieuzeDebiteurenAppTypes.DubieuzeDebiteurT entity, string location, ref WorksheetSetData worksheetset)
        {
            entity.IsValid = true;
            worksheetset.ClearMessages(TypeId, entity.Id);

            if (!entity.Assesmentyear.HasValue)
            {
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Assesmentyear");
                entity.IsValid = false;
            }
            else
            {
                /*
                 * @version 5.2
                 * @ticket 50
                 */
                if (entity.Assesmentyear.Value >= worksheetset.DataContract.AssessmentYear)
                {
                    worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "SMALLERTHEN", location, entity.Id, "Assesmentyear");
                    entity.IsValid = false;
                }
            } 

            

            if (!entity.AlgemeneProvisie.HasValue || entity.AlgemeneProvisie == false)
            {
                
                if (entity.Schuldenaar == null)
                {
                    worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Schuldenaar");
                    entity.IsValid = false;
                }
                if (!entity.OrigineleFactuurWaarde.HasValue)
                {
                    worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "OrigineleFactuurWaarde");
                    entity.IsValid = false;
                }
                if (!entity.OrigineleFactuurWaardeBTW.HasValue)
                {
                    entity.OrigineleFactuurWaardeBTW = 0;
                }
                
                /* 
                 * @version 5.2 
                 * @ticket 50 
                 */
                if (entity.Waardevermindering > entity.OrigineleFactuurWaarde) {
                    worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "SMALLERTHEN", location, entity.Id, "Waardevermindering");
                    entity.IsValid = false;
                }
                  
            }

            if (entity.BelastbaarOfVrijgesteld == null)
            {
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "BelastbaarOfVrijgesteld");
                entity.IsValid = false;
            }

            if (!entity.Vordering.HasValue)
            {
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Vordering");
                entity.IsValid = false;
            }

            if (!entity.Waardevermindering.HasValue)
            {
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Waardevermindering");
                entity.IsValid = false;
            }


            /*
            if (!entity.Bedrag.HasValue || entity.Bedrag.Value==0)
            {
            worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Grootboek");
            entity.IsValid = false;
            }
            */

            return entity;
        }

        public EY.com.eBook.RuleEngine.AY2012.DubieuzeDebiteurenAppTypes.DubieuzeDebiteurW Validate(EY.com.eBook.RuleEngine.AY2012.DubieuzeDebiteurenAppTypes.DubieuzeDebiteurW entity, string location, ref WorksheetSetData worksheetset)
        {
            entity.IsValid = true;
            worksheetset.ClearMessages(TypeId, entity.Id);



            if (entity.Bestaande == null)
            {
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Bestaande");
                entity.IsValid = false;
            }
            else {
                /*
                 * @version 5.2
                 * @ticket 50
                 */

                var doubles = Data.WijzigingBestaandeTijdensBoekjaar != null ?
                    Data.WijzigingBestaandeTijdensBoekjaar.Count(d => d.Schuldenaar != null && d.Schuldenaar.Id == entity.Schuldenaar.Id && d.Id != entity.Id)
                    : 0;

                if (doubles > 0 ) {
                    worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "DOUBLE", location, entity.Id, "Bestaande");
                    entity.IsValid = false;
                }
            }

            if (!entity.WaardeverminderingWaarde.HasValue || entity.WaardeverminderingWaarde == 0)
            {
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "WaardeverminderingWaarde");
                entity.IsValid = false;
            }

            if (entity.IsValid)
            {
                if (entity.DefinitiefVerlies == true)
                {
                    entity.ToenameOfTerugname = worksheetset.GetListItem(new Guid("02080021-45dd-4397-9a56-1b1e11a248fc"));
                    entity.RedenVanAfname = worksheetset.GetListItem(new Guid("27dffd1d-b124-421d-8684-6d22bd719601"));
                    if (entity.RedenDefinitiefVerlies == null)
                    {
                        worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "RedenDefinitiefVerlies");
                        entity.IsValid = false;
                    }
                }
                else {
                    /*
                     * @version 5.2
                     * @ticket 50
                     */
                    switch (entity.ToenameOfTerugname.Id.ToString()){
                        case "02080021-45dd-4397-9a56-1b1e11a248fc": //afname
                            if ((entity.WaardeverminderingStart.Value - entity.WaardeverminderingWaarde.Value) > entity.VorderingEinde)
                            {
                                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "SUMSMALLERTHEN", location, entity.Id, "WaardeverminderingStart");
                                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "SUMSMALLERTHEN", location, entity.Id, "WaardeverminderingWaarde");
                                entity.IsValid = false;

                            }
                            break;
                        case "29ff95cc-30dd-4ab5-9a59-ed8c5e3133b1": //toename
                            if ((entity.WaardeverminderingStart.Value + entity.WaardeverminderingWaarde.Value) > entity.VorderingEinde) {
                                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "SUMSMALLERTHEN", location, entity.Id, "WaardeverminderingStart");
                                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "SUMSMALLERTHEN", location, entity.Id, "WaardeverminderingWaarde");
                                entity.IsValid = false;
                            }
                            break;
                    }
                }
            }

            

            if (entity.ToenameOfTerugname == null)
            {
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "ToenameOfTerugname");
                entity.IsValid = false;
            }
            else if (entity.ToenameOfTerugname.Id.ToString().ToLower() != "29ff95cc-30dd-4ab5-9a59-ed8c5e3133b1")
            {
                if (entity.RedenVanAfname == null)
                {
                    worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "RedenVanAfname");
                    entity.IsValid = false;
                }
            }

            
            
            
            

            return entity;
        }

        public EY.com.eBook.RuleEngine.AY2012.DubieuzeDebiteurenAppTypes.DubieuzeDebiteurN Validate(EY.com.eBook.RuleEngine.AY2012.DubieuzeDebiteurenAppTypes.DubieuzeDebiteurN entity, string location, ref WorksheetSetData worksheetset)
        {
            entity.IsValid = true;
            worksheetset.ClearMessages(TypeId, entity.Id);
            entity.Assesmentyear = worksheetset.DataContract.AssessmentYear;
            if (!entity.AlgemeneProvisie.HasValue || entity.AlgemeneProvisie == false)
            {
                if (entity.Schuldenaar == null)
                {
                    worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Schuldenaar");
                    entity.IsValid = false;
                }
                if (!entity.OrigineleFactuurWaarde.HasValue)
                {
                    worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "OrigineleFactuurWaarde");
                    entity.IsValid = false;
                }
                if (!entity.OrigineleFactuurWaardeBTW.HasValue)
                {
                    entity.OrigineleFactuurWaardeBTW = 0;
                }
            }

            if (entity.BelastbaarOfVrijgesteld == null)
            {
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "BelastbaarOfVrijgesteld");
                entity.IsValid = false;
            }

            if (!entity.VorderingEinde.HasValue)
            {
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "VorderingEinde");
                entity.IsValid = false;
            }

            if (!entity.WaardeverminderingEinde.HasValue)
            {
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "WaardeverminderingEinde");
                entity.IsValid = false;
            }
            else
            {

                /*
                 * @version 5.2
                 * @ticket 50
                 */
                if (entity.WaardeverminderingEinde.Value > entity.OrigineleFactuurWaarde)
                {
                    worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "SMALLERTHEN", location, entity.Id, "WaardeverminderingEinde");
                    entity.IsValid = false;
                }
            }
            return entity;
        }

        public EY.com.eBook.RuleEngine.AY2012.DubieuzeDebiteurenAppTypes.DubieuzeDebiteurH Validate(EY.com.eBook.RuleEngine.AY2012.DubieuzeDebiteurenAppTypes.DubieuzeDebiteurH entity, string location, ref WorksheetSetData worksheetset)
        {
            entity.IsValid = true;
            worksheetset.ClearMessages(TypeId, entity.Id);
            /*
            if (!entity.Bedrag.HasValue || entity.Bedrag.Value==0)
            {
            worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Grootboek");
            entity.IsValid = false;
            }
            */

            return entity;
        }

        #endregion
    }
}
  