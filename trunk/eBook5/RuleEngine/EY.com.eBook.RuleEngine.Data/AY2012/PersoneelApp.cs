﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.RuleEngine.Core;
using EY.com.eBook.RuleEngine.Core.Interfaces;
using EY.com.eBook.RuleEngine.Data;
using EY.com.eBook.Core;
using EY.com.eBook.API.Contracts.Data.Cache;

namespace EY.com.eBook.RuleEngine.AY2012
{
    
    public class PersoneelApp
    {
        public static WorksheetTypeSettings Settings = new WorksheetTypeSettings
                                                      {
                                                        Clients = true
                                                        ,
                                                        Suppliers = true
                                                        ,
                                                        AllAccounts = true
                                                        , 
                                                        PerformsBookings=true
                                                      };
                                                      
        public static Guid TypeId = new Guid("bd2374b4-42c3-4c09-96b2-d8debe3fa1b1");

        public List<string> ImportablePreviousTypes  = new List<string>() {  "EY.com.eBook.RuleEngine.AY2011.FacturatieApp"  };

        public Guid GetMyType() { return TypeId; }

        public void SetMinimal()
        {
            if (Data == null)
            {
                Data = new EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.WPPersoneel();
                Data.LastUpdated = DateTime.Now;
            }

            if (Data._325_20Coll == null)
            {
                Data._325_20Coll = new List<EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.Item325>(); ;
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.TeVerklarenVerschil325_20 == null)
            {
                Data.TeVerklarenVerschil325_20 = new List<EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.VerschilItem>(); ;
                Data.LastUpdated = DateTime.Now;
            }

            if (Data._325_10Coll == null)
            {
                Data._325_10Coll = new List<EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.Item325>(); ;
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.TeVerklarenVerschil325_10 == null)
            {
                Data.TeVerklarenVerschil325_10 = new List<EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.VerschilItem>(); ;
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.ArbeidersForm == null)
            {
                Data.ArbeidersForm = new EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.Werknemer
                {
                    Id = Guid.NewGuid()
                    ,
                    LastUpdated = null
                    ,
                    IsValid = true
                    ,
                    PreviousImported = false

                };
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.BediendeForm == null)
            {
                Data.BediendeForm = new EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.Werknemer
                {
                    Id = Guid.NewGuid()
                    ,
                    LastUpdated = null
                    ,
                    IsValid = true
                    ,
                    PreviousImported = false

                };
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.AansluitingArbeiders == null)
            {
                Data.AansluitingArbeiders = new List<EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.VerschilItem>(); ;
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.AansluitingBedienden == null)
            {
                Data.AansluitingBedienden = new List<EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.VerschilItem>(); ;
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.RSZColl == null)
            {
                Data.RSZColl = new List<EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.Algemeen>(); ;
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.BVsColl == null)
            {
                Data.BVsColl = new List<EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.Algemeen>(); ;
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.NettoColl == null)
            {
                Data.NettoColl = new List<EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.Algemeen>(); ;
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.VakantieGeldColl == null)
            {
                Data.VakantieGeldColl = new EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.Vakantiegeld
                {
                    Id = Guid.NewGuid()
                    ,
                    LastUpdated = null
                    ,
                    IsValid = true
                    ,
                    PreviousImported = false

                };
                Data.LastUpdated = DateTime.Now;
            }

        }
        
        public string ToXml() 
        {
            return CoreHelper.SerializeToString(Data);
        }

        public void LoadXml(string xml)
        {
            if (!string.IsNullOrEmpty(xml)) {
                Data = CoreHelper.DeserializeFromString<EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.WPPersoneel>(xml);
            }
            SetMinimal();
            
        }
        
        public EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.WPPersoneel Data { get; set; }


        #region VALIDATION

        public EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.Item325 Validate(EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.Item325 entity, string location, ref WorksheetSetData worksheetset)
        {
            entity.IsValid = true;
            worksheetset.ClearMessages(TypeId, entity.Id);
            /*
            if (!entity.Bedrag.HasValue || entity.Bedrag.Value==0)
            {
            worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Grootboek");
            entity.IsValid = false;
            }
            */

            return entity;
        }

        public EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.VerschilItem Validate(EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.VerschilItem entity, string location, ref WorksheetSetData worksheetset)
        {
            entity.IsValid = true;
            worksheetset.ClearMessages(TypeId, entity.Id);
            /*
            if (!entity.Bedrag.HasValue || entity.Bedrag.Value==0)
            {
            worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Grootboek");
            entity.IsValid = false;
            }
            */

            return entity;
        }

        public EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.Werknemer Validate(EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.Werknemer entity, string location, ref WorksheetSetData worksheetset)
        {
            entity.IsValid = true;
            worksheetset.ClearMessages(TypeId, entity.Id);
            /*
            if (!entity.Bedrag.HasValue || entity.Bedrag.Value==0)
            {
            worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Grootboek");
            entity.IsValid = false;
            }
            */

            return entity;
        }

        public EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.Algemeen Validate(EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.Algemeen entity, string location, ref WorksheetSetData worksheetset)
        {
            entity.IsValid = true;
            worksheetset.ClearMessages(TypeId, entity.Id);
            /*
            if (!entity.Bedrag.HasValue || entity.Bedrag.Value==0)
            {
            worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Grootboek");
            entity.IsValid = false;
            }
            */

            return entity;
        }

        public EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.Vakantiegeld Validate(EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.Vakantiegeld entity, string location, ref WorksheetSetData worksheetset)
        {
            entity.IsValid = true;
            worksheetset.ClearMessages(TypeId, entity.Id);
            /*
            if (!entity.Bedrag.HasValue || entity.Bedrag.Value==0)
            {
            worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Grootboek");
            entity.IsValid = false;
            }
            */

            return entity;
        }

        #endregion

        #region worksheet type rules
        // Only if worksheet does any calculation.

        public void ForceRecalculation(ref WorksheetSetData worksheetset)
        {
            Data.LastUpdated = new DateTime(1980, 1, 1);
            Data.ArbeidersForm.LastUpdated = null;
            Data.BediendeForm.LastUpdated = null;
            Data._325_10Coll.RemoveAll(r => r.AutoLine.HasValue && r.AutoLine.Value);
            Data._325_20Coll.RemoveAll(r => r.AutoLine.HasValue && r.AutoLine.Value);
            Data.AansluitingArbeiders.RemoveAll(r => r.AutoLine.HasValue && r.AutoLine.Value);
            Data.AansluitingBedienden.RemoveAll(r => r.AutoLine.HasValue && r.AutoLine.Value);
            Data.BVsColl.RemoveAll(r => r.AutoLine.HasValue && r.AutoLine.Value);
            Data.NettoColl.RemoveAll(r => r.AutoLine.HasValue && r.AutoLine.Value);
            Data.RSZColl.RemoveAll(r => r.AutoLine.HasValue && r.AutoLine.Value);
            Data.TeVerklarenVerschil325_10.RemoveAll(r => r.AutoLine.HasValue && r.AutoLine.Value);
            Data.TeVerklarenVerschil325_20.RemoveAll(r => r.AutoLine.HasValue && r.AutoLine.Value);
            Data.VakantieGeldColl.LastUpdated=null;

            Data._325_10Coll.ForEach(e => e.LastUpdated = null);
            Data._325_20Coll.ForEach(e => e.LastUpdated = null);
            Data.AansluitingArbeiders.ForEach(e => e.LastUpdated = null);
            Data.AansluitingBedienden.ForEach(e => e.LastUpdated = null);
            Data.BVsColl.ForEach(e => e.LastUpdated = null);
            Data.NettoColl.ForEach(e => e.LastUpdated = null);
            Data.RSZColl.ForEach(e => e.LastUpdated = null);
            Data.TeVerklarenVerschil325_10.ForEach(e => e.LastUpdated = null);
            Data.TeVerklarenVerschil325_20.ForEach(e => e.LastUpdated = null);


            Calculate(ref worksheetset);
        }

        public void Calculate(ref WorksheetSetData wsd)
        {
            wsd.ClearMessages(TypeId);
            FileDataContract fdc = wsd.DataContract;

            foreach (PersoneelAppTypes.Item325 entity
                in Data._325_10Coll.Where(v => !v.LastUpdated.HasValue).ToList())
            {
                Update_325_10Coll(entity, ref wsd);
            }
            foreach (PersoneelAppTypes.Item325 entity
                in Data._325_20Coll.Where(v => !v.LastUpdated.HasValue).ToList())
            {
                Update_325_20Coll(entity, ref wsd);
            }
            foreach (PersoneelAppTypes.VerschilItem entity
                in Data.AansluitingArbeiders.Where(v => !v.LastUpdated.HasValue).ToList())
            {
                UpdateAansluitingArbeiders(entity, ref wsd);
            }
            foreach (PersoneelAppTypes.VerschilItem entity
                in Data.AansluitingBedienden.Where(v => !v.LastUpdated.HasValue).ToList())
            {
                UpdateAansluitingBedienden(entity, ref wsd);
            }

            foreach (PersoneelAppTypes.Algemeen entity
                in Data.BVsColl.Where(v => !v.LastUpdated.HasValue).ToList())
            {
                UpdateBVsColl(entity, ref wsd);
            }

            foreach (PersoneelAppTypes.Algemeen entity
                in Data.NettoColl.Where(v => !v.LastUpdated.HasValue).ToList())
            {
                UpdateNettoColl(entity, ref wsd);
            }

            foreach (PersoneelAppTypes.Algemeen entity
                in Data.RSZColl.Where(v => !v.LastUpdated.HasValue).ToList())
            {
                UpdateRSZColl(entity, ref wsd);
            }

            foreach (PersoneelAppTypes.VerschilItem entity
                in Data.TeVerklarenVerschil325_10.Where(v => !v.LastUpdated.HasValue).ToList())
            {
                UpdateTeVerklarenVerschil325_10(entity, ref wsd);
            }
            foreach (PersoneelAppTypes.VerschilItem entity
                in Data.TeVerklarenVerschil325_20.Where(v => !v.LastUpdated.HasValue).ToList())
            {
                UpdateTeVerklarenVerschil325_20(entity, ref wsd);
            }


            if (!Data.ArbeidersForm.LastUpdated.HasValue || Data.ArbeidersForm.LastUpdated.Value < Data.LastUpdated)
            {
                if (!Data.ArbeidersForm.Bruto.HasValue) Data.ArbeidersForm.Bruto = 0;
                if (!Data.ArbeidersForm.VAA.HasValue) Data.ArbeidersForm.VAA = 0;
                if (!Data.ArbeidersForm.WW_Bel.HasValue) Data.ArbeidersForm.WW_Bel = 0;
                if (!Data.ArbeidersForm.RSZ_WN.HasValue) Data.ArbeidersForm.RSZ_WN = 0;
                if (!Data.ArbeidersForm.RSZ_WG.HasValue) Data.ArbeidersForm.RSZ_WG = 0;
                if (!Data.ArbeidersForm.Maaltijd.HasValue) Data.ArbeidersForm.Maaltijd = 0;
                if (!Data.ArbeidersForm.KEW.HasValue) Data.ArbeidersForm.KEW = 0;
                if (!Data.ArbeidersForm.WW_Vrij.HasValue) Data.ArbeidersForm.WW_Vrij = 0;
                if (!Data.ArbeidersForm.Bijz_Bijdrage.HasValue) Data.ArbeidersForm.Bijz_Bijdrage = 0;
                if (!Data.ArbeidersForm.BV.HasValue) Data.ArbeidersForm.BV = 0;

                Data.ArbeidersForm.EffectiefBruto = Data.ArbeidersForm.Bruto.Value + Data.ArbeidersForm.VAA.Value + Data.ArbeidersForm.WW_Bel.Value;
                Data.ArbeidersForm.Belastbaar = Data.ArbeidersForm.Bruto.Value - Data.ArbeidersForm.RSZ_WN.Value;
                Data.ArbeidersForm.Netto = ((((Data.ArbeidersForm.Belastbaar - Data.ArbeidersForm.BV.Value) - Data.ArbeidersForm.Maaltijd.Value) + Data.ArbeidersForm.KEW.Value) + Data.ArbeidersForm.WW_Vrij.Value) - Data.ArbeidersForm.Bijz_Bijdrage.Value;
            }

            if (!Data.BediendeForm.LastUpdated.HasValue || Data.BediendeForm.LastUpdated.Value < Data.LastUpdated)
            {
                if (!Data.BediendeForm.Bruto.HasValue) Data.BediendeForm.Bruto = 0;
                if (!Data.BediendeForm.VAA.HasValue) Data.BediendeForm.VAA = 0;
                if (!Data.BediendeForm.WW_Bel.HasValue) Data.BediendeForm.WW_Bel = 0;
                if (!Data.BediendeForm.RSZ_WN.HasValue) Data.BediendeForm.RSZ_WN = 0;
                if (!Data.BediendeForm.RSZ_WG.HasValue) Data.BediendeForm.RSZ_WG = 0;
                if (!Data.BediendeForm.Maaltijd.HasValue) Data.BediendeForm.Maaltijd = 0;
                if (!Data.BediendeForm.KEW.HasValue) Data.BediendeForm.KEW = 0;
                if (!Data.BediendeForm.WW_Vrij.HasValue) Data.BediendeForm.WW_Vrij = 0;
                if (!Data.BediendeForm.Bijz_Bijdrage.HasValue) Data.BediendeForm.Bijz_Bijdrage = 0;
                if (!Data.BediendeForm.BV.HasValue) Data.BediendeForm.BV = 0;

                Data.BediendeForm.EffectiefBruto = Data.BediendeForm.Bruto.Value + Data.BediendeForm.VAA.Value + Data.BediendeForm.WW_Bel.Value;
                Data.BediendeForm.Belastbaar = Data.BediendeForm.Bruto.Value - Data.BediendeForm.RSZ_WN.Value;
                Data.BediendeForm.Netto = ((((Data.BediendeForm.Belastbaar - Data.BediendeForm.BV.Value) - Data.BediendeForm.Maaltijd.Value) + Data.BediendeForm.KEW.Value) + Data.BediendeForm.WW_Vrij.Value) - Data.BediendeForm.Bijz_Bijdrage.Value;

            }


            // perform renders
            RenderRsz(ref wsd);
            RenderVakantiegeld(ref wsd);
            Render325_20(ref wsd);
            Render325_10(ref wsd);
            RenderAansluitingArbeiders(ref wsd);
            RenderAansluitingBedienden(ref wsd);
            RenderBV(ref wsd);
            RenderNetto(ref wsd);

            foreach (EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.Item325 item in Data._325_10Coll.Where(i => !i.LastUpdated.HasValue || i.LastUpdated.Value < Data.LastUpdated))
            {
                if (item.Kalenderjaar == null)
                {
                    int year = fdc.StartDate.Year;
                    if (fdc.StartDate.Day == 1 && fdc.StartDate.Month == 1) year = year - 1;

                    item.Kalenderjaar = new StringListItemDataContract
                    {
                        Id = year.ToString()
                        ,
                        NL = year.ToString()
                        ,
                        FR = year.ToString()
                        ,
                        EN = year.ToString()
                        ,
                        Value = year
                    };

                }
            }

            foreach (EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.Item325 item in Data._325_20Coll.Where(i => !i.LastUpdated.HasValue || i.LastUpdated.Value < Data.LastUpdated))
            {
                if (item.Kalenderjaar == null)
                {
                    int year = fdc.StartDate.Year;
                    if (fdc.StartDate.Day == 1 && fdc.StartDate.Month == 1) year = year - 1;

                    item.Kalenderjaar = new StringListItemDataContract
                    {
                        Id = year.ToString()
                        ,
                        NL = year.ToString()
                        ,
                        FR = year.ToString()
                        ,
                        EN = year.ToString()
                        ,
                        Value = year
                    };

                }
            }


            foreach (EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.VerschilItem item in Data.TeVerklarenVerschil325_10.Where(i => !i.LastUpdated.HasValue || i.LastUpdated.Value < Data.LastUpdated))
            {
                if (!item.AutoLine.HasValue || item.AutoLine.Value == false)
                {
                    item.PrintOmschrijving = item.Omschrijving;
                    item.order = 1000;
                }
            }

        }

        private void RenderRsz(ref WorksheetSetData wsd)
        {
            // cleanup old only one first use of exising data pre-release
            Data.RSZColl.RemoveAll(r => r.AutoLine.HasValue && r.AutoLine.Value && string.IsNullOrEmpty(r.AccountNr) && string.IsNullOrEmpty(r.AutoKey));

            string accountImportOms = wsd.GetTranslation(TypeId,  "RSZColl", "JOURNALACCOUNTS_WORKER", "label");

            List<AccountDataContract> accounts = wsd.GetAccountsOfMapping(new Guid("671092F6-1CA2-49A2-8709-5C527AB03BF0"), false, false, false, wsd.DataContract.Culture);
            accounts.RemoveAll(a => a.Saldo == 0);
            List<String> accountnrs = accounts.Select(a => a.InternalNr).ToList();

            decimal grandTotal = 0;

            // deleted mappings
            Data.RSZColl.RemoveAll(r => r.AutoKey == "ARBEIDER" && r.AutoLine == true && !string.IsNullOrEmpty(r.AccountNr) && !accountnrs.Contains(r.AccountNr));

            // make sure all manuals get correct order
            Data.RSZColl.Where(r => (!r.AutoLine.HasValue || r.AutoLine == false)
                                                    && (!r.order.HasValue || r.order.Value != 1000)
                                                    && (!r.LastUpdated.HasValue || r.LastUpdated.Value < Data.LastUpdated))
                .ToList().ForEach(r => r.order = 1000);

            List<string> rszAccounts = Data.RSZColl.Where(r => r.AutoKey == "ARBEIDER" && r.AutoLine == true && !string.IsNullOrEmpty(r.AccountNr)).Select(r => r.AccountNr).ToList();

            foreach (EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.Algemeen item in Data.RSZColl.Where(r => r.AutoKey == "ARBEIDER" && !string.IsNullOrEmpty(r.AccountNr) && accountnrs.Contains(r.AccountNr)))
            {
                AccountDataContract account = accounts.First(a => a.InternalNr == item.AccountNr);
                item.Bedrag = account.Saldo;
                item.Omschrijving = string.Format("{0} {1}", account.VisualNr, account.DefaultDescription);
                item.LastUpdated = DateTime.Now;
                item.Onderdeel = accountImportOms;
            }

            // add new
            foreach (AccountDataContract account in accounts.Where(a => !rszAccounts.Contains(a.InternalNr)))
            {
                Data.RSZColl.Add(new EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.Algemeen
                {
                    Id = Guid.NewGuid()
                    ,
                    AccountNr = account.InternalNr
                    ,
                    AutoLine = true
                    ,
                    Bedrag = account.Saldo
                    ,
                    LastUpdated = DateTime.Now
                    ,
                    IsValid = true
                    ,
                    Omschrijving = string.Format("{0} {1}", account.VisualNr, account.DefaultDescription)
                    ,
                    Onderdeel = accountImportOms
                    ,
                    order = 1
                    ,
                    Total = null
                    ,
                    AutoKey = "ARBEIDER"
                });
            }

            decimal arbeiders = Data.RSZColl.Where(r => r.AutoKey == "ARBEIDER").Sum(r => r.Bedrag.HasValue ? r.Bedrag.Value : 0);
            EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.Algemeen lastArbeider = Data.RSZColl.OrderBy(r => r.AccountNr).LastOrDefault(r => r.AutoKey == "ARBEIDER");
            if (lastArbeider != null)
            {
                lastArbeider.Total = arbeiders;
            }
            grandTotal += arbeiders;

            accountImportOms = wsd.GetTranslation(TypeId, "RSZColl", "JOURNALACCOUNTS_EMPLOYEE", "label");

            accounts = wsd.GetAccountsOfMapping(new Guid("D387DFA4-7DA3-4B70-AD19-92FC7AFA9981"), false, false, false, wsd.DataContract.Culture);
            accounts.RemoveAll(a => a.Saldo == 0);
            accountnrs = accounts.Select(a => a.InternalNr).ToList();
            // cleanup old only one first use of exising data pre-release
            Data.RSZColl.RemoveAll(r => r.AutoLine.HasValue && r.AutoLine.Value && string.IsNullOrEmpty(r.AccountNr));

            // deleted mappings
            Data.RSZColl.RemoveAll(r => r.AutoKey == "BEDIENDE" && r.AutoLine == true && !string.IsNullOrEmpty(r.AccountNr) && !accountnrs.Contains(r.AccountNr));

            rszAccounts = Data.RSZColl.Where(r => r.AutoKey == "BEDIENDE" && r.AutoLine == true && !string.IsNullOrEmpty(r.AccountNr)).Select(r => r.AccountNr).ToList();

            foreach (EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.Algemeen item in Data.RSZColl.Where(r => r.AutoKey == "BEDIENDE" && !string.IsNullOrEmpty(r.AccountNr) && accountnrs.Contains(r.AccountNr)))
            {
                AccountDataContract account = accounts.First(a => a.InternalNr == item.AccountNr);
                item.Bedrag = account.Saldo;
                item.Omschrijving = string.Format("{0} {1}", account.VisualNr, account.DefaultDescription);
                item.LastUpdated = DateTime.Now;
                item.Onderdeel = accountImportOms;
            }

            // add new
            foreach (AccountDataContract account in accounts.Where(a => !rszAccounts.Contains(a.InternalNr)))
            {
                Data.RSZColl.Add(new EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.Algemeen
                {
                    Id = Guid.NewGuid()
                    ,
                    AccountNr = account.InternalNr
                    ,
                    AutoLine = true
                    ,
                    Bedrag = account.Saldo
                    ,
                    LastUpdated = DateTime.Now
                    ,
                    IsValid = true
                    ,
                    Omschrijving = string.Format("{0} {1}", account.VisualNr, account.DefaultDescription)
                    ,
                    Onderdeel = accountImportOms
                    ,
                    order = 1
                    ,
                    Total = null
                    ,
                    AutoKey = "BEDIENDE"
                });
            }

            decimal bedienden = Data.RSZColl.Where(r => r.AutoKey == "BEDIENDE").Sum(r => r.Bedrag.HasValue ? r.Bedrag.Value : 0);
            grandTotal += bedienden;
            EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.Algemeen lastBediende = Data.RSZColl.OrderBy(r => r.AccountNr).LastOrDefault(r => r.AutoKey == "BEDIENDE");
            if (lastBediende != null)
            {
                lastBediende.Total = bedienden;
            }


            string lastmonth = wsd.GetTranslation(TypeId, "RSZColl", "LASTMONTH", "label");
            string transArbeiders = wsd.GetTranslation(TypeId, "RSZColl", "WORKERS", "label");
            string transBedienden = wsd.GetTranslation(TypeId,  "RSZColl", "EMPLOYEES", "label");
            string transToExplain = wsd.GetTranslation(TypeId, "RSZColl", "TOEXPLAIN", "label");
            string transRemaining = wsd.GetTranslation(TypeId, "RSZColl", "REMAINING", "label");


            EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.Algemeen lmWorker = Data.RSZColl.FirstOrDefault(r => r.AutoKey == "LASTMONTH_WORKERS");
            EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.Algemeen lmEmployee = Data.RSZColl.FirstOrDefault(r => r.AutoKey == "LASTMONTH_EMPLOYEES");

            if (lmWorker == null)
            {
                lmWorker = new EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.Algemeen
                {
                    Id = Guid.NewGuid()
                    ,
                    AccountNr = null
                    ,
                    AutoKey = "LASTMONTH_WORKERS"
                    ,
                    AutoLine = true
                    ,
                    IsValid = true
                    ,
                    LastUpdated = DateTime.Now
                    ,
                    Omschrijving = transArbeiders
                    ,
                    Onderdeel = lastmonth
                    ,
                    order = 3
                    ,
                    Bedrag = 0
                    ,
                    Total = null
                };
                Data.RSZColl.Add(lmWorker);
            }
            if (lmEmployee == null)
            {
                lmEmployee = new EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.Algemeen
                {
                    Id = Guid.NewGuid()
                    ,
                    AccountNr = null
                    ,
                    AutoKey = "LASTMONTH_EMPLOYEES"
                    ,
                    AutoLine = true
                    ,
                    IsValid = true
                    ,
                    LastUpdated = DateTime.Now
                    ,
                    Omschrijving = transArbeiders
                    ,
                    Onderdeel = lastmonth
                    ,
                    order = 4
                    ,
                    Bedrag = 0
                    ,
                    Total = null
                };
                Data.RSZColl.Add(lmEmployee);
            }
            lmWorker.Omschrijving = transArbeiders;
            lmWorker.Onderdeel = lastmonth;
            lmEmployee.Omschrijving = transBedienden;
            lmEmployee.Onderdeel = lastmonth;

            lmWorker.Bedrag = (Data.ArbeidersForm.RSZ_WG.Value + Data.ArbeidersForm.RSZ_WN.Value) + Data.ArbeidersForm.Bijz_Bijdrage.Value;
            lmEmployee.Bedrag = (Data.BediendeForm.RSZ_WG.Value + Data.BediendeForm.RSZ_WN.Value) + Data.BediendeForm.Bijz_Bijdrage.Value;
            lmEmployee.Total = lmWorker.Bedrag.Value + lmEmployee.Bedrag.Value;

            EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.Algemeen toExplain = Data.RSZColl.FirstOrDefault(r => r.AutoKey == "TOEXPLAIN");
            if (toExplain == null)
            {
                toExplain = new EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.Algemeen
                {
                    Id = Guid.NewGuid()
                    ,
                    AutoKey = "TOEXPLAIN"
                    ,
                    AutoLine = true
                    ,
                    Omschrijving = ""
                    ,
                    Onderdeel = transToExplain
                    ,
                    Bedrag = null
                    ,
                    order = 5
                };
                Data.RSZColl.Add(toExplain);
            }
            grandTotal -= lmEmployee.Total.Value;

            toExplain.Total = grandTotal;

            grandTotal -= Data.RSZColl.Where(r => !r.AutoLine.HasValue || r.AutoLine == false).Sum(r => r.Bedrag.HasValue ? r.Bedrag.Value : 0);

            EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.Algemeen total = Data.RSZColl.FirstOrDefault(r => r.AutoKey == "TOTAL");
            if (total == null)
            {
                total = new EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.Algemeen
                {
                    Id = Guid.NewGuid()
                    ,
                    AutoKey = "TOTAL"
                    ,
                    AutoLine = true
                    ,
                    IsValid = true
                    ,
                    LastUpdated = DateTime.Now
                    ,
                    Bedrag = null
                    ,
                    Omschrijving = transRemaining
                    ,
                    order = 99999
                };
            }
            total.Total = grandTotal;
            total.Omschrijving = transRemaining;
            total.LastUpdated = DateTime.Now;


            // ORDER FOR VIEW.
            Data.RSZColl = Data.RSZColl.OrderBy(r => r.order.HasValue ? r.order.Value : 0).ThenBy(r => string.IsNullOrEmpty(r.AccountNr) ? "" : r.AccountNr).ToList();

        }

        private void RenderVakantiegeld(ref WorksheetSetData wsd)
        {
            if (!Data.VakantieGeldColl.LastUpdated.HasValue || Data.VakantieGeldColl.LastUpdated.Value < Data.LastUpdated)
            {
                FileDataContract fdc = wsd.DataContract;
                if (!Data.VakantieGeldColl.gekozenSchuldArbeider.HasValue) Data.VakantieGeldColl.gekozenSchuldArbeider = 0;
                if (!Data.VakantieGeldColl.VBArbeider.HasValue) Data.VakantieGeldColl.VBArbeider = 0;

                if (!Data.VakantieGeldColl.gekozenSchuldBediende.HasValue) Data.VakantieGeldColl.gekozenSchuldBediende = 0;
                if (!Data.VakantieGeldColl.VBBediende.HasValue) Data.VakantieGeldColl.VBBediende = 0;

                Data.VakantieGeldColl.TeBoekenArbeider = Data.VakantieGeldColl.gekozenSchuldArbeider.Value - Data.VakantieGeldColl.VBArbeider.Value;
                Data.VakantieGeldColl.TeBoekenBediende = Data.VakantieGeldColl.gekozenSchuldBediende.Value - Data.VakantieGeldColl.VBBediende.Value;

                if (Data.VakantieGeldColl.previousGekozenSchuldArbeider.HasValue)
                {
                    Data.VakantieGeldColl.VBArbeider = Data.VakantieGeldColl.previousGekozenSchuldArbeider;
                }

                if (Data.VakantieGeldColl.previousGekozenSchuldBediende.HasValue)
                {
                    Data.VakantieGeldColl.VBBediende = Data.VakantieGeldColl.previousGekozenSchuldBediende;
                }

                Data.VakantieGeldColl.BrutoBed = Data.BediendeForm.Bruto.Value + Data.BediendeForm.VAA.Value;
                Data.VakantieGeldColl.BrutoArb = Data.ArbeidersForm.Bruto.Value + Data.ArbeidersForm.VAA.Value;

                CoefficientDataContract cdc = wsd.GetCoefficient("PERSONEEL_ARBEIDER", fdc.StartDate, fdc.EndDate);
                Data.VakantieGeldColl.PercArb = cdc != null ? cdc.Value : 0;

                cdc = wsd.GetCoefficient("PERSONEEL_BEDIENDE", fdc.StartDate, fdc.EndDate);
                Data.VakantieGeldColl.PercBed = cdc != null ? cdc.Value : 0;

                Data.VakantieGeldColl.theoretischArbeider = Math.Round(Data.VakantieGeldColl.BrutoArb.Value * ((decimal)1.08 * (Data.VakantieGeldColl.BrutoArb.Value / 100)), 2, MidpointRounding.AwayFromZero);
                Data.VakantieGeldColl.theoretischBediende = Math.Round(Data.VakantieGeldColl.BrutoBed.Value * (Data.VakantieGeldColl.BrutoBed.Value / 100), 2, MidpointRounding.AwayFromZero);


            }


        }


        private void Render325_20(ref WorksheetSetData wsd)
        {
            FileDataContract fdc = wsd.DataContract;
            string YM = string.Format("{0}/{1}", fdc.EndDate.Year, fdc.EndDate.Month);

            // cleanup old only one first use of exising data pre-release
            Data.TeVerklarenVerschil325_20.RemoveAll(r => r.AutoLine.HasValue && r.AutoLine.Value && string.IsNullOrEmpty(r.AccountNr) && string.IsNullOrEmpty(r.AutoKey));


            //TRANSLATIONS
            string transBooked = wsd.GetTranslation(TypeId, "TeVerklarenVerschil325_20", "BOOKED", "label");
            string transTaxable = wsd.GetTranslation(TypeId,  "TeVerklarenVerschil325_20", "TaxableAmount", "label");
            string transExplain = wsd.GetTranslation(TypeId, "TeVerklarenVerschil325_20", "DifferenceToExplain", "label");
            string transRest = wsd.GetTranslation(TypeId, "TeVerklarenVerschil325_20", "DifferenceResting", "label");

            // make sure all manuals get correct order
            Data.TeVerklarenVerschil325_20.Where(r => (!r.AutoLine.HasValue || r.AutoLine == false)
                                                    && (!r.order.HasValue || r.order.Value != 1000)
                                                    && (!r.LastUpdated.HasValue || r.LastUpdated.Value < Data.LastUpdated))
                .ToList().ForEach(r => r.order = 1000);


            List<AccountDataContract> accounts = wsd.GetAccountsOfMapping(new Guid("2978D3E2-2189-44B8-B369-55E0BF881C99"), false, false, false, wsd.DataContract.Culture);
            accounts.RemoveAll(a => a.Saldo == 0);
            List<String> accountnrs = accounts.Select(a => a.InternalNr).ToList();

            decimal grandTotal = 0;

            // deleted mappings
            Data.TeVerklarenVerschil325_20.RemoveAll(r => r.AutoKey == "BEDRIJFSLEIDER" && r.AutoLine == true && !string.IsNullOrEmpty(r.AccountNr) && !accountnrs.Contains(r.AccountNr));

            List<string> rszAccounts = Data.TeVerklarenVerschil325_20.Where(r => r.AutoKey == "BEDRIJFSLEIDER" && r.AutoLine == true && !string.IsNullOrEmpty(r.AccountNr)).Select(r => r.AccountNr).ToList();

            // UPDATES
            foreach (EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.VerschilItem item in Data.TeVerklarenVerschil325_20.Where(r => r.AutoKey == "BEDRIJFSLEIDER" && !string.IsNullOrEmpty(r.AccountNr) && accountnrs.Contains(r.AccountNr)))
            {
                AccountDataContract account = accounts.First(a => a.InternalNr == item.AccountNr);
                item.Bedrag = account.Saldo;
                item.Omschrijving = string.Format("{0} {1}", account.VisualNr, account.DefaultDescription);
                item.LastUpdated = DateTime.Now;
                item.Onderdeel = transBooked;
                item.PrintOmschrijving = account.DefaultDescription;
            }

            // add new
            foreach (AccountDataContract account in accounts.Where(a => !rszAccounts.Contains(a.InternalNr)))
            {
                Data.TeVerklarenVerschil325_20.Add(new EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.VerschilItem
                {
                    Id = Guid.NewGuid()
                    ,
                    AccountNr = account.InternalNr
                    ,
                    AutoLine = true
                    ,
                    Bedrag = account.Saldo
                    ,
                    LastUpdated = DateTime.Now
                    ,
                    IsValid = true
                    ,
                    Omschrijving = string.Format("{0} {1}", account.VisualNr, account.DefaultDescription)
                    ,
                    PrintOmschrijving = account.DefaultDescription
                    ,
                    Onderdeel = transBooked
                    ,
                    order = 1
                    ,
                    Total = null
                    ,
                    AutoKey = "BEDRIJFSLEIDER"
                });
            }

            decimal total = Data.TeVerklarenVerschil325_20.Where(r => r.AutoKey == "BEDRIJFSLEIDER").Sum(r => r.Bedrag.HasValue ? r.Bedrag.Value : 0);
            EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.VerschilItem lastOne = Data.TeVerklarenVerschil325_20.OrderBy(r => r.AccountNr).LastOrDefault(r => r.AutoKey == "BEDRIJFSLEIDER");
            if (lastOne != null)
            {
                lastOne.Total = total;
            }
            grandTotal += total;

            decimal sum325_20 = Data._325_20Coll.Sum(r => r.Bedrag.HasValue ? r.Bedrag.Value : 0);

            EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.VerschilItem ttl325 = Data.TeVerklarenVerschil325_20.FirstOrDefault(r => r.AutoKey == "325.20");
            if (ttl325 == null)
            {
                ttl325 = new EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.VerschilItem
                {
                    Id = Guid.NewGuid()
                    ,
                    AccountNr = ""
                    ,
                    AutoLine = true
                    ,
                    AutoKey = "325.20"
                    ,
                    Bedrag = 0
                    ,
                    LastUpdated = DateTime.Now
                    ,
                    Omschrijving = transTaxable
                    ,
                    Onderdeel = "325.20"
                    ,
                    order = 997
                    ,
                    PrintOmschrijving = transTaxable
                    ,
                    Total = 0
                };
                Data.TeVerklarenVerschil325_20.Add(ttl325);

            }
            ttl325.Omschrijving = transTaxable;
            ttl325.PrintOmschrijving = transTaxable;
            ttl325.Bedrag = Math.Abs(sum325_20);
            ttl325.Total = sum325_20;

            grandTotal -= sum325_20;

            EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.VerschilItem toExplain = Data.TeVerklarenVerschil325_20.FirstOrDefault(r => r.AutoKey == "TOEXPLAIN");
            if (toExplain == null)
            {
                toExplain = new EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.VerschilItem
                {
                    Id = Guid.NewGuid()
                    ,
                    AutoKey = "TOEXPLAIN"
                    ,
                    AutoLine = true
                    ,
                    Omschrijving = ""
                    ,
                    Onderdeel = transExplain
                    ,
                    Bedrag = null
                    ,
                    order = 998
                };
                Data.TeVerklarenVerschil325_20.Add(toExplain);
            }
            toExplain.Total = grandTotal;


            grandTotal += Data.TeVerklarenVerschil325_20.Where(r => !r.AutoLine.HasValue || r.AutoLine == false).Sum(r => r.Bedrag.HasValue ? r.Bedrag.Value : 0);



            EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.VerschilItem resting = Data.TeVerklarenVerschil325_20.FirstOrDefault(r => r.AutoKey == "TOTAL");
            if (resting == null)
            {
                resting = new EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.VerschilItem
                {
                    Id = Guid.NewGuid()
                    ,
                    AutoKey = "TOTAL"
                    ,
                    AutoLine = true
                    ,
                    IsValid = true
                    ,
                    LastUpdated = DateTime.Now
                    ,
                    Bedrag = null
                    ,
                    Omschrijving = transRest
                    ,
                    order = 99999
                };
                Data.TeVerklarenVerschil325_20.Add(resting);
            }
            resting.Total = grandTotal;
            resting.Omschrijving = transRest;
            resting.LastUpdated = DateTime.Now;


            // ORDER FOR VIEW.
            Data.TeVerklarenVerschil325_20 = Data.TeVerklarenVerschil325_20.OrderBy(r => r.order.HasValue ? r.order.Value : 999).ThenBy(r => string.IsNullOrEmpty(r.AccountNr) ? "" : r.AccountNr).ToList();

        }

        private void Render325_10(ref WorksheetSetData wsd)
        {
            FileDataContract fdc = wsd.DataContract;
            string YM = string.Format("{0}/{1}", fdc.EndDate.Year, fdc.EndDate.Month);

            // cleanup old only one first use of exising data pre-release
            Data.TeVerklarenVerschil325_10.RemoveAll(r => r.AutoLine.HasValue && r.AutoLine.Value && string.IsNullOrEmpty(r.AccountNr) && string.IsNullOrEmpty(r.AutoKey));


            //TRANSLATIONS
            string transHolidayEmployee = wsd.GetTranslation(TypeId,  "TeVerklarenVerschil325_10", "HolidayEmployee", "label");
            string transHolidayWorker = wsd.GetTranslation(TypeId, "TeVerklarenVerschil325_10", "HolidayWorker", "label");
            string transBookedWorker = wsd.GetTranslation(TypeId,  "TeVerklarenVerschil325_10", "BookedWorker", "label");
            string transBookedEmployee = wsd.GetTranslation(TypeId,  "TeVerklarenVerschil325_10", "BookedEmployee", "label");

            string transTaxable = wsd.GetTranslation(TypeId, "TeVerklarenVerschil325_10", "TaxableAmount", "label");
            string transExplain = wsd.GetTranslation(TypeId,  "TeVerklarenVerschil325_10", "DifferenceToExplain", "label");
            string transRest = wsd.GetTranslation(TypeId, "TeVerklarenVerschil325_10", "DifferenceResting", "label");

            // make sure all manuals get correct order
            Data.TeVerklarenVerschil325_10.Where(r => (!r.AutoLine.HasValue || r.AutoLine == false)
                                                    && (!r.order.HasValue || r.order.Value != 1000)
                                                    && (!r.LastUpdated.HasValue || r.LastUpdated.Value < Data.LastUpdated))
                .ToList().ForEach(r => r.order = 1000);


            #region vakantiegeld arbeiders
            string autoKey = "HolidayWorker";

            List<AccountDataContract> accounts = wsd.GetAccountsOfMapping(new Guid("A389669C-92A8-4B19-BCAE-2CEE5BC72B83"), false, false, false, wsd.DataContract.Culture);
            accounts.RemoveAll(a => a.Saldo == 0);
            List<String> accountnrs = accounts.Select(a => a.InternalNr).ToList();

            decimal grandTotal = 0;


            // deleted mappings
            Data.TeVerklarenVerschil325_10.RemoveAll(r => r.AutoKey == autoKey && r.AutoLine == true && !string.IsNullOrEmpty(r.AccountNr) && !accountnrs.Contains(r.AccountNr));

            List<string> rszAccounts = Data.TeVerklarenVerschil325_10.Where(r => r.AutoKey == autoKey && r.AutoLine == true && !string.IsNullOrEmpty(r.AccountNr)).Select(r => r.AccountNr).ToList();

            // UPDATES
            foreach (EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.VerschilItem item in Data.TeVerklarenVerschil325_10.Where(r => r.AutoKey == autoKey && !string.IsNullOrEmpty(r.AccountNr) && accountnrs.Contains(r.AccountNr)))
            {
                AccountDataContract account = accounts.First(a => a.InternalNr == item.AccountNr);
                item.Bedrag = account.Saldo;
                item.Omschrijving = string.Format("{0} {1}", account.VisualNr, account.DefaultDescription);
                item.LastUpdated = DateTime.Now;
                item.Onderdeel = transHolidayWorker;
                item.PrintOmschrijving = account.DefaultDescription;
            }

            // add new
            foreach (AccountDataContract account in accounts.Where(a => !rszAccounts.Contains(a.InternalNr)))
            {
                Data.TeVerklarenVerschil325_10.Add(new EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.VerschilItem
                {
                    Id = Guid.NewGuid()
                    ,
                    AccountNr = account.InternalNr
                    ,
                    AutoLine = true
                    ,
                    Bedrag = account.Saldo
                    ,
                    LastUpdated = DateTime.Now
                    ,
                    IsValid = true
                    ,
                    Omschrijving = string.Format("{0} {1}", account.VisualNr, account.DefaultDescription)
                    ,
                    Onderdeel = transHolidayWorker
                    ,
                    PrintOmschrijving = account.DefaultDescription
                    ,
                    order = 1
                    ,
                    Total = null
                    ,
                    AutoKey = autoKey
                });
            }

            decimal total = Data.TeVerklarenVerschil325_10.Where(r => r.AutoKey == autoKey).Sum(r => r.Bedrag.HasValue ? r.Bedrag.Value : 0);
            EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.VerschilItem lastOne = Data.TeVerklarenVerschil325_10.OrderBy(r => r.AccountNr).LastOrDefault(r => r.AutoKey == autoKey);
            if (lastOne != null)
            {
                lastOne.Total = total;
            }
            grandTotal += total;
            #endregion

            #region vakantiegeld bedienden
            autoKey = "HolidayEmployee";

            accounts = wsd.GetAccountsOfMapping(new Guid("6F65243E-BB70-4662-9EF3-0F5BA7AF3CB7"), false, false, false, wsd.DataContract.Culture);
            accounts.RemoveAll(a => a.Saldo == 0);
            accountnrs = accounts.Select(a => a.InternalNr).ToList();

            // deleted mappings
            Data.TeVerklarenVerschil325_10.RemoveAll(r => r.AutoKey == autoKey && r.AutoLine == true && !string.IsNullOrEmpty(r.AccountNr) && !accountnrs.Contains(r.AccountNr));

            rszAccounts = Data.TeVerklarenVerschil325_10.Where(r => r.AutoKey == autoKey && r.AutoLine == true && !string.IsNullOrEmpty(r.AccountNr)).Select(r => r.AccountNr).ToList();

            // UPDATES
            foreach (EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.VerschilItem item in Data.TeVerklarenVerschil325_10.Where(r => r.AutoKey == autoKey && !string.IsNullOrEmpty(r.AccountNr) && accountnrs.Contains(r.AccountNr)))
            {
                AccountDataContract account = accounts.First(a => a.InternalNr == item.AccountNr);
                item.Bedrag = account.Saldo;
                item.Omschrijving = string.Format("{0} {1}", account.VisualNr, account.DefaultDescription);
                item.LastUpdated = DateTime.Now;
                item.Onderdeel = transHolidayEmployee;
                item.PrintOmschrijving = account.DefaultDescription;
            }

            // add new
            foreach (AccountDataContract account in accounts.Where(a => !rszAccounts.Contains(a.InternalNr)))
            {
                Data.TeVerklarenVerschil325_10.Add(new EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.VerschilItem
                {
                    Id = Guid.NewGuid()
                    ,
                    AccountNr = account.InternalNr
                    ,
                    AutoLine = true
                    ,
                    Bedrag = account.Saldo
                    ,
                    LastUpdated = DateTime.Now
                    ,
                    IsValid = true
                    ,
                    Omschrijving = string.Format("{0} {1}", account.VisualNr, account.DefaultDescription)
                    ,
                    Onderdeel = transHolidayEmployee
                    ,
                    PrintOmschrijving = account.DefaultDescription
                    ,
                    order = 2
                    ,
                    Total = null
                    ,
                    AutoKey = autoKey
                });
            }

            total = Data.TeVerklarenVerschil325_10.Where(r => r.AutoKey == autoKey).Sum(r => r.Bedrag.HasValue ? r.Bedrag.Value : 0);
            lastOne = Data.TeVerklarenVerschil325_10.OrderBy(r => r.AccountNr).LastOrDefault(r => r.AutoKey == autoKey);
            if (lastOne != null)
            {
                lastOne.Total = total;
            }
            grandTotal += total;
            #endregion

            #region bruto arbeiders
            autoKey = "BookedWorker";

            accounts = wsd.GetAccountsOfMapping(new Guid("00CF8A7C-7C0B-4286-9BE7-FF74AAA5112F"), false, false, false, wsd.DataContract.Culture);
            accounts.RemoveAll(a => a.Saldo == 0);
            accountnrs = accounts.Select(a => a.InternalNr).ToList();

            // deleted mappings
            Data.TeVerklarenVerschil325_10.RemoveAll(r => r.AutoKey == autoKey && r.AutoLine == true && !string.IsNullOrEmpty(r.AccountNr) && !accountnrs.Contains(r.AccountNr));

            rszAccounts = Data.TeVerklarenVerschil325_10.Where(r => r.AutoKey == autoKey && r.AutoLine == true && !string.IsNullOrEmpty(r.AccountNr)).Select(r => r.AccountNr).ToList();

            // UPDATES
            foreach (EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.VerschilItem item in Data.TeVerklarenVerschil325_10.Where(r => r.AutoKey == autoKey && !string.IsNullOrEmpty(r.AccountNr) && accountnrs.Contains(r.AccountNr)))
            {
                AccountDataContract account = accounts.First(a => a.InternalNr == item.AccountNr);
                item.Bedrag = account.Saldo;
                item.Omschrijving = string.Format("{0} {1}", account.VisualNr, account.DefaultDescription);
                item.LastUpdated = DateTime.Now;
                item.Onderdeel = transBookedWorker;
                item.PrintOmschrijving = account.DefaultDescription;
            }

            // add new
            foreach (AccountDataContract account in accounts.Where(a => !rszAccounts.Contains(a.InternalNr)))
            {
                Data.TeVerklarenVerschil325_10.Add(new EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.VerschilItem
                {
                    Id = Guid.NewGuid()
                    ,
                    AccountNr = account.InternalNr
                    ,
                    AutoLine = true
                    ,
                    Bedrag = account.Saldo
                    ,
                    LastUpdated = DateTime.Now
                    ,
                    IsValid = true
                    ,
                    Omschrijving = string.Format("{0} {1}", account.VisualNr, account.DefaultDescription)
                    ,
                    Onderdeel = transBookedWorker
                    ,
                    PrintOmschrijving = account.DefaultDescription
                    ,
                    order = 3
                    ,
                    Total = null
                    ,
                    AutoKey = autoKey
                });
            }

            total = Data.TeVerklarenVerschil325_10.Where(r => r.AutoKey == autoKey).Sum(r => r.Bedrag.HasValue ? r.Bedrag.Value : 0);
            lastOne = Data.TeVerklarenVerschil325_10.OrderBy(r => r.AccountNr).LastOrDefault(r => r.AutoKey == autoKey);
            if (lastOne != null)
            {
                lastOne.Total = total;
            }
            grandTotal += total;
            #endregion

            #region bruto bedienden
            autoKey = "BookedEmployee";

            accounts = wsd.GetAccountsOfMapping(new Guid("F8CD8522-C3B1-4867-B89D-EFAD88942092"), false, false, false, wsd.DataContract.Culture);
            accounts.RemoveAll(a => a.Saldo == 0);
            accountnrs = accounts.Select(a => a.InternalNr).ToList();

            // deleted mappings
            Data.TeVerklarenVerschil325_10.RemoveAll(r => r.AutoKey == autoKey && r.AutoLine == true && !string.IsNullOrEmpty(r.AccountNr) && !accountnrs.Contains(r.AccountNr));

            rszAccounts = Data.TeVerklarenVerschil325_10.Where(r => r.AutoKey == autoKey && r.AutoLine == true && !string.IsNullOrEmpty(r.AccountNr)).Select(r => r.AccountNr).ToList();

            // UPDATES
            foreach (EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.VerschilItem item in Data.TeVerklarenVerschil325_10.Where(r => r.AutoKey == autoKey && !string.IsNullOrEmpty(r.AccountNr) && accountnrs.Contains(r.AccountNr)))
            {
                AccountDataContract account = accounts.First(a => a.InternalNr == item.AccountNr);
                item.Bedrag = account.Saldo;
                item.Omschrijving = string.Format("{0} {1}", account.VisualNr, account.DefaultDescription);
                item.LastUpdated = DateTime.Now;
                item.Onderdeel = transBookedEmployee;
                item.PrintOmschrijving = account.DefaultDescription;
            }

            // add new
            foreach (AccountDataContract account in accounts.Where(a => !rszAccounts.Contains(a.InternalNr)))
            {
                Data.TeVerklarenVerschil325_10.Add(new EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.VerschilItem
                {
                    Id = Guid.NewGuid()
                    ,
                    AccountNr = account.InternalNr
                    ,
                    AutoLine = true
                    ,
                    Bedrag = account.Saldo
                    ,
                    LastUpdated = DateTime.Now
                    ,
                    IsValid = true
                    ,
                    Omschrijving = string.Format("{0} {1}", account.VisualNr, account.DefaultDescription)
                    ,
                    Onderdeel = transBookedEmployee
                    ,
                    PrintOmschrijving = account.DefaultDescription
                    ,
                    order = 4
                    ,
                    Total = null
                    ,
                    AutoKey = autoKey
                });
            }

            total = Data.TeVerklarenVerschil325_10.Where(r => r.AutoKey == autoKey).Sum(r => r.Bedrag.HasValue ? r.Bedrag.Value : 0);
            lastOne = Data.TeVerklarenVerschil325_10.OrderBy(r => r.AccountNr).LastOrDefault(r => r.AutoKey == autoKey);
            if (lastOne != null)
            {
                lastOne.Total = total;
            }
            grandTotal += total;
            #endregion

            decimal sum325_10 = Data._325_10Coll.Sum(r => r.Bedrag.HasValue ? r.Bedrag.Value : 0);

            EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.VerschilItem ttl325 = Data.TeVerklarenVerschil325_10.FirstOrDefault(r => r.AutoKey == "325.10");
            if (ttl325 == null)
            {
                ttl325 = new EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.VerschilItem
                {
                    Id = Guid.NewGuid()
                    ,
                    AccountNr = ""
                    ,
                    AutoLine = true
                    ,
                    AutoKey = "325.10"
                    ,
                    Bedrag = 0
                    ,
                    LastUpdated = DateTime.Now
                    ,
                    Omschrijving = transTaxable
                    ,
                    Onderdeel = "325.10"
                    ,
                    order = 997
                    ,
                    PrintOmschrijving = transTaxable
                    ,
                    Total = 0
                };
                Data.TeVerklarenVerschil325_10.Add(ttl325);

            }
            ttl325.AutoKey = "325.10";
            ttl325.Omschrijving = transTaxable;
            ttl325.PrintOmschrijving = transTaxable;
            ttl325.Bedrag = Math.Abs(sum325_10);
            ttl325.Total = sum325_10;

            grandTotal -= sum325_10;

            EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.VerschilItem toExplain = Data.TeVerklarenVerschil325_10.FirstOrDefault(r => r.AutoKey == "TOEXPLAIN");
            if (toExplain == null)
            {
                toExplain = new EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.VerschilItem
                {
                    Id = Guid.NewGuid()
                    ,
                    AutoKey = "TOEXPLAIN"
                    ,
                    AutoLine = true
                    ,
                    Omschrijving = ""
                    ,
                    Onderdeel = transExplain
                    ,
                    Bedrag = null
                    ,
                    order = 998
                };
                Data.TeVerklarenVerschil325_10.Add(toExplain);
            }
            toExplain.Total = grandTotal;
            toExplain.Omschrijving = transExplain;


            grandTotal += Data.TeVerklarenVerschil325_10.Where(r => !r.AutoLine.HasValue || r.AutoLine == false).Sum(r => r.Bedrag.HasValue ? r.Bedrag.Value : 0);



            EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.VerschilItem resting = Data.TeVerklarenVerschil325_10.FirstOrDefault(r => r.AutoKey == "TOTAL");
            if (resting == null)
            {
                resting = new EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.VerschilItem
                {
                    Id = Guid.NewGuid()
                    ,
                    AutoKey = "TOTAL"
                    ,
                    AutoLine = true
                    ,
                    IsValid = true
                    ,
                    LastUpdated = DateTime.Now
                    ,
                    Bedrag = null
                    ,
                    Omschrijving = transRest
                    ,
                    order = 999999
                };
                Data.TeVerklarenVerschil325_10.Add(resting);
            }
            resting.Total = grandTotal;
            resting.Omschrijving = transRest;
            resting.LastUpdated = DateTime.Now;


            // ORDER FOR VIEW.
            Data.TeVerklarenVerschil325_10 = Data.TeVerklarenVerschil325_10.OrderBy(r => r.order.HasValue ? r.order.Value : 999).ThenBy(r => string.IsNullOrEmpty(r.AccountNr) ? "" : r.AccountNr).ToList();

        }


        private void RenderAansluitingArbeiders(ref WorksheetSetData wsd)
        {
            FileDataContract fdc = wsd.DataContract;
            string YM = string.Format("{0}/{1}", fdc.EndDate.Year, fdc.EndDate.Month);

            // cleanup old only one first use of exising data pre-release
            Data.AansluitingArbeiders.RemoveAll(r => r.AutoLine.HasValue && r.AutoLine.Value && string.IsNullOrEmpty(r.AccountNr) && string.IsNullOrEmpty(r.AutoKey));


            //TRANSLATIONS
            string transBooked = wsd.GetTranslation(TypeId,  "AansluitingArbeiders", "Booked", "label");
            string transEntity = wsd.GetTranslation(TypeId,  "AansluitingArbeiders", "Workers", "label");
            string transAnualAccounts = wsd.GetTranslation(TypeId, "AansluitingArbeiders", "AnualAccounts", "label");
            string transExplain = wsd.GetTranslation(TypeId,  "AansluitingArbeiders", "TOEXPLAIN", "label");
            string transRest = wsd.GetTranslation(TypeId,  "AansluitingArbeiders", "REMAINING", "label");

            // make sure all manuals get correct order
            Data.AansluitingArbeiders.Where(r => (!r.AutoLine.HasValue || r.AutoLine == false)
                                                    && (!r.order.HasValue || r.order.Value != 1000)
                                                    && (!r.LastUpdated.HasValue || r.LastUpdated.Value < Data.LastUpdated))
                .ToList().ForEach(r => r.order = 1000);


            List<AccountDataContract> accounts = wsd.GetAccountsOfMapping(new Guid("B9E4820C-0A1A-4284-9F2E-35064255294C"), false, false, false, wsd.DataContract.Culture);
            accounts.RemoveAll(a => a.Saldo == 0);
            List<String> accountnrs = accounts.Select(a => a.InternalNr).ToList();

            decimal grandTotal = 0;

            // deleted mappings
            Data.AansluitingArbeiders.RemoveAll(r => r.AutoKey == "Booked" && r.AutoLine == true && !string.IsNullOrEmpty(r.AccountNr) && !accountnrs.Contains(r.AccountNr));

            List<string> rszAccounts = Data.AansluitingArbeiders.Where(r => r.AutoKey == "Booked" && r.AutoLine == true && !string.IsNullOrEmpty(r.AccountNr)).Select(r => r.AccountNr).ToList();

            // UPDATES
            foreach (EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.VerschilItem item in Data.AansluitingArbeiders.Where(r => r.AutoKey == "Booked" && !string.IsNullOrEmpty(r.AccountNr) && accountnrs.Contains(r.AccountNr)))
            {
                AccountDataContract account = accounts.First(a => a.InternalNr == item.AccountNr);
                item.Bedrag = account.Saldo;
                item.Omschrijving = string.Format("{0} {1}", account.VisualNr, account.DefaultDescription);
                item.LastUpdated = DateTime.Now;
                item.Onderdeel = transBooked;
            }

            // add new
            foreach (AccountDataContract account in accounts.Where(a => !rszAccounts.Contains(a.InternalNr)))
            {
                Data.AansluitingArbeiders.Add(new EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.VerschilItem
                {
                    Id = Guid.NewGuid()
                    ,
                    AccountNr = account.InternalNr
                    ,
                    AutoLine = true
                    ,
                    Bedrag = account.Saldo
                    ,
                    LastUpdated = DateTime.Now
                    ,
                    IsValid = true
                    ,
                    Omschrijving = string.Format("{0} {1}", account.VisualNr, account.DefaultDescription)
                    ,
                    Onderdeel = transBooked
                    ,
                    order = 1
                    ,
                    Total = null
                    ,
                    AutoKey = "Booked"
                });
            }

            decimal total = Data.AansluitingArbeiders.Where(r => r.AutoKey == "Booked").Sum(r => r.Bedrag.HasValue ? r.Bedrag.Value : 0);
            EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.VerschilItem lastOne = Data.AansluitingArbeiders.OrderBy(r => r.AccountNr).LastOrDefault(r => r.AutoKey == "Booked");
            if (lastOne != null)
            {
                lastOne.Total = total;
            }
            grandTotal += total;



            decimal brut = Data.ArbeidersForm.Bruto.HasValue ? Data.ArbeidersForm.Bruto.Value : 0;

            EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.VerschilItem vitem = Data.AansluitingArbeiders.FirstOrDefault(r => r.AutoKey == "BRUTO");
            if (vitem == null)
            {
                vitem = new EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.VerschilItem
                {
                    Id = Guid.NewGuid()
                    ,
                    AccountNr = ""
                    ,
                    AutoLine = true
                    ,
                    AutoKey = "BRUTO"
                    ,
                    Bedrag = 0
                    ,
                    LastUpdated = DateTime.Now
                    ,
                    Omschrijving = transAnualAccounts
                    ,
                    Onderdeel = transEntity
                    ,
                    order = 997
                    ,
                    PrintOmschrijving = transAnualAccounts
                    ,
                    Total = 0
                };
                Data.AansluitingArbeiders.Add(vitem);

            }
            vitem.Omschrijving = transAnualAccounts;
            vitem.PrintOmschrijving = transAnualAccounts;

            vitem.Total = brut;

            grandTotal -= brut;

            EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.VerschilItem toExplain = Data.AansluitingArbeiders.FirstOrDefault(r => r.AutoKey == "TOEXPLAIN");
            if (toExplain == null)
            {
                toExplain = new EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.VerschilItem
                {
                    Id = Guid.NewGuid()
                    ,
                    AutoKey = "TOEXPLAIN"
                    ,
                    AutoLine = true
                    ,
                    Omschrijving = ""
                    ,
                    Onderdeel = transExplain
                    ,
                    Bedrag = null
                    ,
                    order = 998
                };
                Data.AansluitingArbeiders.Add(toExplain);
            }
            toExplain.Total = grandTotal;


            grandTotal -= Data.AansluitingArbeiders.Where(r => !r.AutoLine.HasValue || r.AutoLine == false).Sum(r => r.Bedrag.HasValue ? r.Bedrag.Value : 0);



            EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.VerschilItem resting = Data.AansluitingArbeiders.FirstOrDefault(r => r.AutoKey == "TOTAL");
            if (resting == null)
            {
                resting = new EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.VerschilItem
                {
                    Id = Guid.NewGuid()
                    ,
                    AutoKey = "TOTAL"
                    ,
                    AutoLine = true
                    ,
                    IsValid = true
                    ,
                    LastUpdated = DateTime.Now
                    ,
                    Bedrag = null
                    ,
                    Omschrijving = transRest
                    ,
                    order = 99999
                };
                Data.AansluitingArbeiders.Add(resting);
            }
            resting.Total = grandTotal;
            resting.Omschrijving = transRest;
            resting.LastUpdated = DateTime.Now;


            // ORDER FOR VIEW.
            Data.AansluitingArbeiders = Data.AansluitingArbeiders.OrderBy(r => r.order.HasValue ? r.order.Value : 0).ThenBy(r => string.IsNullOrEmpty(r.AccountNr) ? "" : r.AccountNr).ToList();

        }

        private void RenderAansluitingBedienden(ref WorksheetSetData wsd)
        {
            FileDataContract fdc = wsd.DataContract;
            string YM = string.Format("{0}/{1}", fdc.EndDate.Year, fdc.EndDate.Month);

            // cleanup old only one first use of exising data pre-release
            Data.AansluitingBedienden.RemoveAll(r => r.AutoLine.HasValue && r.AutoLine.Value && string.IsNullOrEmpty(r.AccountNr) && string.IsNullOrEmpty(r.AutoKey));


            //TRANSLATIONS
            string transBooked = wsd.GetTranslation(TypeId, "AansluitingBedienden", "Booked", "label");
            string transEntity = wsd.GetTranslation(TypeId,  "AansluitingBedienden", "Employees", "label");
            string transAnualAccounts = wsd.GetTranslation(TypeId, "AansluitingBedienden", "AnualAccounts", "label");
            string transExplain = wsd.GetTranslation(TypeId,"AansluitingBedienden", "TOEXPLAIN", "label");
            string transRest = wsd.GetTranslation(TypeId,  "AansluitingBedienden", "REMAINING", "label");

            // make sure all manuals get correct order
            Data.AansluitingBedienden.Where(r => (!r.AutoLine.HasValue || r.AutoLine == false)
                                                    && (!r.order.HasValue || r.order.Value != 1000)
                                                    && (!r.LastUpdated.HasValue || r.LastUpdated.Value < Data.LastUpdated))
                .ToList().ForEach(r => r.order = 1000);


            List<AccountDataContract> accounts = wsd.GetAccountsOfMapping(new Guid("8E9DE920-AD97-4EDD-91B0-0CB35ED67602"), false, false, false, wsd.DataContract.Culture);
            accounts.RemoveAll(a => a.Saldo == 0);
            List<String> accountnrs = accounts.Select(a => a.InternalNr).ToList();

            decimal grandTotal = 0;

            // deleted mappings
            Data.AansluitingBedienden.RemoveAll(r => r.AutoKey == "Booked" && r.AutoLine == true && !string.IsNullOrEmpty(r.AccountNr) && !accountnrs.Contains(r.AccountNr));

            List<string> rszAccounts = Data.AansluitingBedienden.Where(r => r.AutoKey == "Booked" && r.AutoLine == true && !string.IsNullOrEmpty(r.AccountNr)).Select(r => r.AccountNr).ToList();

            // UPDATES
            foreach (EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.VerschilItem item in Data.AansluitingBedienden.Where(r => r.AutoKey == "Booked" && !string.IsNullOrEmpty(r.AccountNr) && accountnrs.Contains(r.AccountNr)))
            {
                AccountDataContract account = accounts.First(a => a.InternalNr == item.AccountNr);
                item.Bedrag = account.Saldo;
                item.Omschrijving = string.Format("{0} {1}", account.VisualNr, account.DefaultDescription);
                item.LastUpdated = DateTime.Now;
                item.Onderdeel = transBooked;
            }

            // add new
            foreach (AccountDataContract account in accounts.Where(a => !rszAccounts.Contains(a.InternalNr)))
            {
                Data.AansluitingBedienden.Add(new EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.VerschilItem
                {
                    Id = Guid.NewGuid()
                    ,
                    AccountNr = account.InternalNr
                    ,
                    AutoLine = true
                    ,
                    Bedrag = account.Saldo
                    ,
                    LastUpdated = DateTime.Now
                    ,
                    IsValid = true
                    ,
                    Omschrijving = string.Format("{0} {1}", account.VisualNr, account.DefaultDescription)
                    ,
                    Onderdeel = transBooked
                    ,
                    order = 1
                    ,
                    Total = null
                    ,
                    AutoKey = "Booked"
                });
            }

            decimal total = Data.AansluitingBedienden.Where(r => r.AutoKey == "Booked").Sum(r => r.Bedrag.HasValue ? r.Bedrag.Value : 0);
            EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.VerschilItem lastOne = Data.AansluitingBedienden.OrderBy(r => r.AccountNr).LastOrDefault(r => r.AutoKey == "Booked");
            if (lastOne != null)
            {
                lastOne.Total = total;
            }
            grandTotal += total;



            decimal brut = Data.BediendeForm.Bruto.HasValue ? Data.BediendeForm.Bruto.Value : 0;

            EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.VerschilItem vitem = Data.AansluitingBedienden.FirstOrDefault(r => r.AutoKey == "BRUTO");
            if (vitem == null)
            {
                vitem = new EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.VerschilItem
                {
                    Id = Guid.NewGuid()
                    ,
                    AccountNr = ""
                    ,
                    AutoLine = true
                    ,
                    AutoKey = "BRUTO"
                    ,
                    Bedrag = 0
                    ,
                    LastUpdated = DateTime.Now
                    ,
                    Omschrijving = transAnualAccounts
                    ,
                    Onderdeel = transEntity
                    ,
                    order = 997
                    ,
                    PrintOmschrijving = transAnualAccounts
                    ,
                    Total = 0
                };
                Data.AansluitingBedienden.Add(vitem);

            }
            vitem.Omschrijving = transAnualAccounts;
            vitem.PrintOmschrijving = transAnualAccounts;

            vitem.Total = brut;

            grandTotal -= brut;

            EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.VerschilItem toExplain = Data.AansluitingBedienden.FirstOrDefault(r => r.AutoKey == "TOEXPLAIN");
            if (toExplain == null)
            {
                toExplain = new EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.VerschilItem
                {
                    Id = Guid.NewGuid()
                    ,
                    AutoKey = "TOEXPLAIN"
                    ,
                    AutoLine = true
                    ,
                    Omschrijving = ""
                    ,
                    Onderdeel = transExplain
                    ,
                    Bedrag = null
                    ,
                    order = 998
                };
                Data.AansluitingBedienden.Add(toExplain);
            }
            toExplain.Total = grandTotal;


            grandTotal -= Data.AansluitingBedienden.Where(r => !r.AutoLine.HasValue || r.AutoLine == false).Sum(r => r.Bedrag.HasValue ? r.Bedrag.Value : 0);



            EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.VerschilItem resting = Data.AansluitingBedienden.FirstOrDefault(r => r.AutoKey == "TOTAL");
            if (resting == null)
            {
                resting = new EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.VerschilItem
                {
                    Id = Guid.NewGuid()
                    ,
                    AutoKey = "TOTAL"
                    ,
                    AutoLine = true
                    ,
                    IsValid = true
                    ,
                    LastUpdated = DateTime.Now
                    ,
                    Bedrag = null
                    ,
                    Omschrijving = transRest
                    ,
                    order = 99999
                };
                Data.AansluitingBedienden.Add(resting);
            }
            resting.Total = grandTotal;
            resting.Omschrijving = transRest;
            resting.LastUpdated = DateTime.Now;


            // ORDER FOR VIEW.
            Data.AansluitingBedienden = Data.AansluitingBedienden.OrderBy(r => r.order.HasValue ? r.order.Value : 0).ThenBy(r => string.IsNullOrEmpty(r.AccountNr) ? "" : r.AccountNr).ToList();

        }


        private void RenderBV(ref WorksheetSetData wsd)
        {
            // cleanup old only one first use of exising data pre-release
            Data.BVsColl.RemoveAll(r => r.AutoLine.HasValue && r.AutoLine.Value && string.IsNullOrEmpty(r.AccountNr) && string.IsNullOrEmpty(r.AutoKey));

            string accountImportOms = wsd.GetTranslation(TypeId, "BVsColl", "JOURNALACCOUNTS_WORKER", "label");

            List<AccountDataContract> accounts = wsd.GetAccountsOfMapping(new Guid("08861715-AB4B-4EBE-9431-38F6784F6970"), false, false, false, wsd.DataContract.Culture);
            accounts.RemoveAll(a => a.Saldo == 0);
            List<String> accountnrs = accounts.Select(a => a.InternalNr).ToList();

            decimal grandTotal = 0;

            // deleted mappings
            Data.BVsColl.RemoveAll(r => r.AutoKey == "ARBEIDER" && r.AutoLine == true && !string.IsNullOrEmpty(r.AccountNr) && !accountnrs.Contains(r.AccountNr));

            // make sure all manuals get correct order
            Data.BVsColl.Where(r => (!r.AutoLine.HasValue || r.AutoLine == false)
                                                    && (!r.order.HasValue || r.order.Value != 1000)
                                                    && (!r.LastUpdated.HasValue || r.LastUpdated.Value < Data.LastUpdated))
                .ToList().ForEach(r => r.order = 1000);

            List<string> rszAccounts = Data.BVsColl.Where(r => r.AutoKey == "ARBEIDER" && r.AutoLine == true && !string.IsNullOrEmpty(r.AccountNr)).Select(r => r.AccountNr).ToList();

            foreach (EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.Algemeen item in Data.BVsColl.Where(r => r.AutoKey == "ARBEIDER" && !string.IsNullOrEmpty(r.AccountNr) && accountnrs.Contains(r.AccountNr)))
            {
                AccountDataContract account = accounts.First(a => a.InternalNr == item.AccountNr);
                item.Bedrag = account.Saldo;
                item.Omschrijving = string.Format("{0} {1}", account.VisualNr, account.DefaultDescription);
                item.LastUpdated = DateTime.Now;
                item.Onderdeel = accountImportOms;
            }

            // add new
            foreach (AccountDataContract account in accounts.Where(a => !rszAccounts.Contains(a.InternalNr)))
            {
                Data.BVsColl.Add(new EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.Algemeen
                {
                    Id = Guid.NewGuid()
                    ,
                    AccountNr = account.InternalNr
                    ,
                    AutoLine = true
                    ,
                    Bedrag = account.Saldo
                    ,
                    LastUpdated = DateTime.Now
                    ,
                    IsValid = true
                    ,
                    Omschrijving = string.Format("{0} {1}", account.VisualNr, account.DefaultDescription)
                    ,
                    Onderdeel = accountImportOms
                    ,
                    order = 1
                    ,
                    Total = null
                    ,
                    AutoKey = "ARBEIDER"
                });
            }

            decimal arbeiders = Data.BVsColl.Where(r => r.AutoKey == "ARBEIDER").Sum(r => r.Bedrag.HasValue ? r.Bedrag.Value : 0);
            EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.Algemeen lastArbeider = Data.BVsColl.OrderBy(r => r.AccountNr).LastOrDefault(r => r.AutoKey == "ARBEIDER");
            if (lastArbeider != null)
            {
                lastArbeider.Total = arbeiders;
            }
            grandTotal += arbeiders;

            accountImportOms = wsd.GetTranslation(TypeId,"BVsColl", "JOURNALACCOUNTS_EMPLOYEE", "label");

            accounts = wsd.GetAccountsOfMapping(new Guid("2C278B87-47E9-4F3F-A155-D065EB2F8552"), false, false, false, wsd.DataContract.Culture);
            accounts.RemoveAll(a => a.Saldo == 0);
            accountnrs = accounts.Select(a => a.InternalNr).ToList();
            // cleanup old only one first use of exising data pre-release
            Data.BVsColl.RemoveAll(r => r.AutoLine.HasValue && r.AutoLine.Value && string.IsNullOrEmpty(r.AccountNr));

            // deleted mappings
            Data.BVsColl.RemoveAll(r => r.AutoKey == "BEDIENDE" && r.AutoLine == true && !string.IsNullOrEmpty(r.AccountNr) && !accountnrs.Contains(r.AccountNr));

            rszAccounts = Data.BVsColl.Where(r => r.AutoKey == "BEDIENDE" && r.AutoLine == true && !string.IsNullOrEmpty(r.AccountNr)).Select(r => r.AccountNr).ToList();

            foreach (EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.Algemeen item in Data.BVsColl.Where(r => r.AutoKey == "BEDIENDE" && !string.IsNullOrEmpty(r.AccountNr) && accountnrs.Contains(r.AccountNr)))
            {
                AccountDataContract account = accounts.First(a => a.InternalNr == item.AccountNr);
                item.Bedrag = account.Saldo;
                item.Omschrijving = string.Format("{0} {1}", account.VisualNr, account.DefaultDescription);
                item.LastUpdated = DateTime.Now;
                item.Onderdeel = accountImportOms;
            }

            // add new
            foreach (AccountDataContract account in accounts.Where(a => !rszAccounts.Contains(a.InternalNr)))
            {
                Data.BVsColl.Add(new EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.Algemeen
                {
                    Id = Guid.NewGuid()
                    ,
                    AccountNr = account.InternalNr
                    ,
                    AutoLine = true
                    ,
                    Bedrag = account.Saldo
                    ,
                    LastUpdated = DateTime.Now
                    ,
                    IsValid = true
                    ,
                    Omschrijving = string.Format("{0} {1}", account.VisualNr, account.DefaultDescription)
                    ,
                    Onderdeel = accountImportOms
                    ,
                    order = 1
                    ,
                    Total = null
                    ,
                    AutoKey = "BEDIENDE"
                });
            }

            decimal bedienden = Data.BVsColl.Where(r => r.AutoKey == "BEDIENDE").Sum(r => r.Bedrag.HasValue ? r.Bedrag.Value : 0);
            grandTotal += bedienden;
            EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.Algemeen lastBediende = Data.BVsColl.OrderBy(r => r.AccountNr).LastOrDefault(r => r.AutoKey == "BEDIENDE");
            if (lastBediende != null)
            {
                lastBediende.Total = bedienden;
            }


            string lastmonth = wsd.GetTranslation(TypeId,"BVsColl", "LASTMONTH", "label");
            string transArbeiders = wsd.GetTranslation(TypeId,  "BVsColl", "WORKERS", "label");
            string transBedienden = wsd.GetTranslation(TypeId,  "BVsColl", "EMPLOYEES", "label");
            string transToExplain = wsd.GetTranslation(TypeId,  "BVsColl", "TOEXPLAIN", "label");
            string transRemaining = wsd.GetTranslation(TypeId,  "BVsColl", "REMAINING", "label");


            EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.Algemeen lmWorker = Data.BVsColl.FirstOrDefault(r => r.AutoKey == "LASTMONTH_WORKERS");
            EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.Algemeen lmEmployee = Data.BVsColl.FirstOrDefault(r => r.AutoKey == "LASTMONTH_EMPLOYEES");

            if (lmWorker == null)
            {
                lmWorker = new EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.Algemeen
                {
                    Id = Guid.NewGuid()
                    ,
                    AccountNr = null
                    ,
                    AutoKey = "LASTMONTH_WORKERS"
                    ,
                    AutoLine = true
                    ,
                    IsValid = true
                    ,
                    LastUpdated = DateTime.Now
                    ,
                    Omschrijving = transArbeiders
                    ,
                    Onderdeel = lastmonth
                    ,
                    order = 3
                    ,
                    Bedrag = 0
                    ,
                    Total = null
                };
                Data.BVsColl.Add(lmWorker);
            }
            if (lmEmployee == null)
            {
                lmEmployee = new EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.Algemeen
                {
                    Id = Guid.NewGuid()
                    ,
                    AccountNr = null
                    ,
                    AutoKey = "LASTMONTH_EMPLOYEES"
                    ,
                    AutoLine = true
                    ,
                    IsValid = true
                    ,
                    LastUpdated = DateTime.Now
                    ,
                    Omschrijving = transArbeiders
                    ,
                    Onderdeel = lastmonth
                    ,
                    order = 4
                    ,
                    Bedrag = 0
                    ,
                    Total = null
                };
                Data.BVsColl.Add(lmEmployee);
            }
            lmWorker.Omschrijving = transArbeiders;
            lmWorker.Onderdeel = lastmonth;
            lmEmployee.Omschrijving = transBedienden;
            lmEmployee.Onderdeel = lastmonth;

            lmWorker.Bedrag = Data.ArbeidersForm.BV;
            lmEmployee.Bedrag = Data.BediendeForm.BV;
            lmEmployee.Total = lmWorker.Bedrag.Value + lmEmployee.Bedrag.Value;

            EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.Algemeen toExplain = Data.BVsColl.FirstOrDefault(r => r.AutoKey == "TOEXPLAIN");
            if (toExplain == null)
            {
                toExplain = new EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.Algemeen
                {
                    Id = Guid.NewGuid()
                    ,
                    AutoKey = "TOEXPLAIN"
                    ,
                    AutoLine = true
                    ,
                    Omschrijving = ""
                    ,
                    Onderdeel = transToExplain
                    ,
                    Bedrag = null
                    ,
                    order = 5
                };
                Data.BVsColl.Add(toExplain);
            }
            grandTotal -= lmEmployee.Total.Value;

            toExplain.Total = grandTotal;

            grandTotal -= Data.BVsColl.Where(r => !r.AutoLine.HasValue || r.AutoLine == false).Sum(r => r.Bedrag.HasValue ? r.Bedrag.Value : 0);

            EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.Algemeen total = Data.BVsColl.FirstOrDefault(r => r.AutoKey == "TOTAL");
            if (total == null)
            {
                total = new EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.Algemeen
                {
                    Id = Guid.NewGuid()
                    ,
                    AutoKey = "TOTAL"
                    ,
                    AutoLine = true
                    ,
                    IsValid = true
                    ,
                    LastUpdated = DateTime.Now
                    ,
                    Bedrag = null
                    ,
                    Omschrijving = transRemaining
                    ,
                    order = 99999
                };
            }
            total.Total = grandTotal;
            total.Omschrijving = transRemaining;
            total.LastUpdated = DateTime.Now;


            // ORDER FOR VIEW.
            Data.BVsColl = Data.BVsColl.OrderBy(r => r.order.HasValue ? r.order.Value : 0).ThenBy(r => string.IsNullOrEmpty(r.AccountNr) ? "" : r.AccountNr).ToList();

        }

        private void RenderNetto(ref WorksheetSetData wsd)
        {
            // cleanup old only one first use of exising data pre-release
            Data.NettoColl.RemoveAll(r => r.AutoLine.HasValue && r.AutoLine.Value && string.IsNullOrEmpty(r.AccountNr) && string.IsNullOrEmpty(r.AutoKey));

            string accountImportOms = wsd.GetTranslation(TypeId,"NettoColl", "JOURNALACCOUNTS_WORKER", "label");

            List<AccountDataContract> accounts = wsd.GetAccountsOfMapping(new Guid("59F4E092-E1E7-43A2-BDA4-45C47AAC8DBE"), false, false, false, wsd.DataContract.Culture);
            accounts.RemoveAll(a => a.Saldo == 0);
            List<String> accountnrs = accounts.Select(a => a.InternalNr).ToList();

            decimal grandTotal = 0;

            // deleted mappings
            Data.NettoColl.RemoveAll(r => r.AutoKey == "ARBEIDER" && r.AutoLine == true && !string.IsNullOrEmpty(r.AccountNr) && !accountnrs.Contains(r.AccountNr));

            // make sure all manuals get correct order
            Data.NettoColl.Where(r => (!r.AutoLine.HasValue || r.AutoLine == false)
                                                    && (!r.order.HasValue || r.order.Value != 1000)
                                                    && (!r.LastUpdated.HasValue || r.LastUpdated.Value < Data.LastUpdated))
                .ToList().ForEach(r => r.order = 1000);

            List<string> rszAccounts = Data.NettoColl.Where(r => r.AutoKey == "ARBEIDER" && r.AutoLine == true && !string.IsNullOrEmpty(r.AccountNr)).Select(r => r.AccountNr).ToList();

            foreach (EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.Algemeen item in Data.NettoColl.Where(r => r.AutoKey == "ARBEIDER" && !string.IsNullOrEmpty(r.AccountNr) && accountnrs.Contains(r.AccountNr)))
            {
                AccountDataContract account = accounts.First(a => a.InternalNr == item.AccountNr);
                item.Bedrag = account.Saldo;
                item.Omschrijving = string.Format("{0} {1}", account.VisualNr, account.DefaultDescription);
                item.LastUpdated = DateTime.Now;
                item.Onderdeel = accountImportOms;
            }

            // add new
            foreach (AccountDataContract account in accounts.Where(a => !rszAccounts.Contains(a.InternalNr)))
            {
                Data.NettoColl.Add(new EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.Algemeen
                {
                    Id = Guid.NewGuid()
                    ,
                    AccountNr = account.InternalNr
                    ,
                    AutoLine = true
                    ,
                    Bedrag = account.Saldo
                    ,
                    LastUpdated = DateTime.Now
                    ,
                    IsValid = true
                    ,
                    Omschrijving = string.Format("{0} {1}", account.VisualNr, account.DefaultDescription)
                    ,
                    Onderdeel = accountImportOms
                    ,
                    order = 1
                    ,
                    Total = null
                    ,
                    AutoKey = "ARBEIDER"
                });
            }

            decimal arbeiders = Data.NettoColl.Where(r => r.AutoKey == "ARBEIDER").Sum(r => r.Bedrag.HasValue ? r.Bedrag.Value : 0);
            EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.Algemeen lastArbeider = Data.NettoColl.OrderBy(r => r.AccountNr).LastOrDefault(r => r.AutoKey == "ARBEIDER");
            if (lastArbeider != null)
            {
                lastArbeider.Total = arbeiders;
            }
            grandTotal += arbeiders;

            accountImportOms = wsd.GetTranslation(TypeId, "NettoColl", "JOURNALACCOUNTS_EMPLOYEE", "label");

            accounts = wsd.GetAccountsOfMapping(new Guid("A5425F72-7FCC-4999-8FE0-BB95C2B5F18E"), false, false, false, wsd.DataContract.Culture);
            accounts.RemoveAll(a => a.Saldo == 0);
            accountnrs = accounts.Select(a => a.InternalNr).ToList();
            // cleanup old only one first use of exising data pre-release
            Data.NettoColl.RemoveAll(r => r.AutoLine.HasValue && r.AutoLine.Value && string.IsNullOrEmpty(r.AccountNr));

            // deleted mappings
            Data.NettoColl.RemoveAll(r => r.AutoKey == "BEDIENDE" && r.AutoLine == true && !string.IsNullOrEmpty(r.AccountNr) && !accountnrs.Contains(r.AccountNr));

            rszAccounts = Data.NettoColl.Where(r => r.AutoKey == "BEDIENDE" && r.AutoLine == true && !string.IsNullOrEmpty(r.AccountNr)).Select(r => r.AccountNr).ToList();

            foreach (EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.Algemeen item in Data.NettoColl.Where(r => r.AutoKey == "BEDIENDE" && !string.IsNullOrEmpty(r.AccountNr) && accountnrs.Contains(r.AccountNr)))
            {
                AccountDataContract account = accounts.First(a => a.InternalNr == item.AccountNr);
                item.Bedrag = account.Saldo;
                item.Omschrijving = string.Format("{0} {1}", account.VisualNr, account.DefaultDescription);
                item.LastUpdated = DateTime.Now;
                item.Onderdeel = accountImportOms;
            }

            // add new
            foreach (AccountDataContract account in accounts.Where(a => !rszAccounts.Contains(a.InternalNr)))
            {
                Data.NettoColl.Add(new EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.Algemeen
                {
                    Id = Guid.NewGuid()
                    ,
                    AccountNr = account.InternalNr
                    ,
                    AutoLine = true
                    ,
                    Bedrag = account.Saldo
                    ,
                    LastUpdated = DateTime.Now
                    ,
                    IsValid = true
                    ,
                    Omschrijving = string.Format("{0} {1}", account.VisualNr, account.DefaultDescription)
                    ,
                    Onderdeel = accountImportOms
                    ,
                    order = 1
                    ,
                    Total = null
                    ,
                    AutoKey = "BEDIENDE"
                });
            }

            decimal bedienden = Data.NettoColl.Where(r => r.AutoKey == "BEDIENDE").Sum(r => r.Bedrag.HasValue ? r.Bedrag.Value : 0);
            grandTotal += bedienden;
            EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.Algemeen lastBediende = Data.NettoColl.OrderBy(r => r.AccountNr).LastOrDefault(r => r.AutoKey == "BEDIENDE");
            if (lastBediende != null)
            {
                lastBediende.Total = bedienden;
            }


            string lastmonth = wsd.GetTranslation(TypeId, "NettoColl", "LASTMONTH", "label");
            string transArbeiders = wsd.GetTranslation(TypeId,  "NettoColl", "WORKERS", "label");
            string transBedienden = wsd.GetTranslation(TypeId, "NettoColl", "EMPLOYEES", "label");
            string transToExplain = wsd.GetTranslation(TypeId,  "NettoColl", "TOEXPLAIN", "label");
            string transRemaining = wsd.GetTranslation(TypeId, "NettoColl", "REMAINING", "label");


            EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.Algemeen lmWorker = Data.NettoColl.FirstOrDefault(r => r.AutoKey == "LASTMONTH_WORKERS");
            EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.Algemeen lmEmployee = Data.NettoColl.FirstOrDefault(r => r.AutoKey == "LASTMONTH_EMPLOYEES");

            if (lmWorker == null)
            {
                lmWorker = new EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.Algemeen
                {
                    Id = Guid.NewGuid()
                    ,
                    AccountNr = null
                    ,
                    AutoKey = "LASTMONTH_WORKERS"
                    ,
                    AutoLine = true
                    ,
                    IsValid = true
                    ,
                    LastUpdated = DateTime.Now
                    ,
                    Omschrijving = transArbeiders
                    ,
                    Onderdeel = lastmonth
                    ,
                    order = 3
                    ,
                    Bedrag = 0
                    ,
                    Total = null
                };
                Data.NettoColl.Add(lmWorker);
            }
            if (lmEmployee == null)
            {
                lmEmployee = new EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.Algemeen
                {
                    Id = Guid.NewGuid()
                    ,
                    AccountNr = null
                    ,
                    AutoKey = "LASTMONTH_EMPLOYEES"
                    ,
                    AutoLine = true
                    ,
                    IsValid = true
                    ,
                    LastUpdated = DateTime.Now
                    ,
                    Omschrijving = transArbeiders
                    ,
                    Onderdeel = lastmonth
                    ,
                    order = 4
                    ,
                    Bedrag = 0
                    ,
                    Total = null
                };
                Data.NettoColl.Add(lmEmployee);
            }
            lmWorker.Omschrijving = transArbeiders;
            lmWorker.Onderdeel = lastmonth;
            lmEmployee.Omschrijving = transBedienden;
            lmEmployee.Onderdeel = lastmonth;

            lmWorker.Bedrag = Data.ArbeidersForm.BV;
            lmEmployee.Bedrag = Data.BediendeForm.BV;
            lmEmployee.Total = lmWorker.Bedrag.Value + lmEmployee.Bedrag.Value;

            EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.Algemeen toExplain = Data.NettoColl.FirstOrDefault(r => r.AutoKey == "TOEXPLAIN");
            if (toExplain == null)
            {
                toExplain = new EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.Algemeen
                {
                    Id = Guid.NewGuid()
                    ,
                    AutoKey = "TOEXPLAIN"
                    ,
                    AutoLine = true
                    ,
                    Omschrijving = ""
                    ,
                    Onderdeel = transToExplain
                    ,
                    Bedrag = null
                    ,
                    order = 5
                };
                Data.NettoColl.Add(toExplain);
            }
            grandTotal -= lmEmployee.Total.Value;

            toExplain.Total = grandTotal;

            grandTotal -= Data.NettoColl.Where(r => !r.AutoLine.HasValue || r.AutoLine == false).Sum(r => r.Bedrag.HasValue ? r.Bedrag.Value : 0);

            EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.Algemeen total = Data.NettoColl.FirstOrDefault(r => r.AutoKey == "TOTAL");
            if (total == null)
            {
                total = new EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.Algemeen
                {
                    Id = Guid.NewGuid()
                    ,
                    AutoKey = "TOTAL"
                    ,
                    AutoLine = true
                    ,
                    IsValid = true
                    ,
                    LastUpdated = DateTime.Now
                    ,
                    Bedrag = null
                    ,
                    Omschrijving = transRemaining
                    ,
                    order = 99999
                };
            }
            total.Total = grandTotal;
            total.Omschrijving = transRemaining;
            total.LastUpdated = DateTime.Now;


            // ORDER FOR VIEW.
            Data.NettoColl = Data.NettoColl.OrderBy(r => r.order.HasValue ? r.order.Value : 0).ThenBy(r => string.IsNullOrEmpty(r.AccountNr) ? "" : r.AccountNr).ToList();

        }

        #endregion


        #region AddUpdateDelete

        public void AddTo_325_20Coll(EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.Item325 entity, ref WorksheetSetData worksheetset)
        {
            entity = Validate(entity, "_325_20Coll", ref worksheetset);
            if (entity.IsValid)
            {
                //Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
                Data._325_20Coll.Add(entity);
            }
        }

        public void Delete_325_20Coll(Guid rowId, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.Item325 original = Data._325_20Coll.FirstOrDefault(e => e.Id == rowId);
            if (original != null)
            {
                /*
                if (original.Boeken.HasValue && original.Boeken.Value)
                {
                    worksheetset.DeleteBooking(FacturatieApp.TypeId, "OpTeMakenCN", original.Id, null);
                    // DELETE BOOKING
                }*/
                Data._325_20Coll.Remove(original);
               // Data.LastUpdated = DateTime.Now;
            }
        }

        public void Update_325_20Coll(EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.Item325 entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.Item325 original = Data._325_20Coll.FirstOrDefault(e => e.Id == entity.Id);
            if (original == null)
            {
                AddTo_325_20Coll(entity, ref worksheetset);
            }
            else
            {
                entity = Validate(entity, "_325_20Coll", ref worksheetset);
                if (entity.IsValid)
                {
                    int idx = Data._325_20Coll.IndexOf(original);
                    Data._325_20Coll.RemoveAt(idx);
                    Data._325_20Coll.Insert(idx, entity);
                 //   Data.LastUpdated = DateTime.Now;
                    entity.LastUpdated = DateTime.Now;
                }
            }
        }

        public void AddToTeVerklarenVerschil325_20(EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.VerschilItem entity, ref WorksheetSetData worksheetset)
        {
            entity = Validate(entity, "TeVerklarenVerschil325_20", ref worksheetset);
            if (entity.IsValid)
            {
                if (!entity.AutoLine.GetValueOrDefault()) entity.order = 999;
              //  Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
                Data.TeVerklarenVerschil325_20.Add(entity);
            }
        }

        public void DeleteTeVerklarenVerschil325_20(Guid rowId, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.VerschilItem original = Data.TeVerklarenVerschil325_20.FirstOrDefault(e => e.Id == rowId);
            if (original != null)
            {
                /*
                if (original.Boeken.HasValue && original.Boeken.Value)
                {
                    worksheetset.DeleteBooking(FacturatieApp.TypeId, "OpTeMakenCN", original.Id, null);
                    // DELETE BOOKING
                }*/
                Data.TeVerklarenVerschil325_20.Remove(original);
              //  Data.LastUpdated = DateTime.Now;
            }
        }

        public void UpdateTeVerklarenVerschil325_20(EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.VerschilItem entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.VerschilItem original = Data.TeVerklarenVerschil325_20.FirstOrDefault(e => e.Id == entity.Id);
            if (original == null)
            {
                AddToTeVerklarenVerschil325_20(entity, ref worksheetset);
            }
            else
            {
                entity = Validate(entity, "TeVerklarenVerschil325_20", ref worksheetset);
                if (entity.IsValid)
                {
                    if (!entity.AutoLine.GetValueOrDefault()) entity.order = 999;
                    int idx = Data.TeVerklarenVerschil325_20.IndexOf(original);
                    Data.TeVerklarenVerschil325_20.RemoveAt(idx);
                    Data.TeVerklarenVerschil325_20.Insert(idx, entity);
                //    Data.LastUpdated = DateTime.Now;
                    entity.LastUpdated = DateTime.Now;
                }
            }
        }

        public void AddTo_325_10Coll(EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.Item325 entity, ref WorksheetSetData worksheetset)
        {
            entity = Validate(entity, "_325_10Coll", ref worksheetset);
            if (entity.IsValid)
            {
             //   Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
                Data._325_10Coll.Add(entity);
            }
        }

        public void Delete_325_10Coll(Guid rowId, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.Item325 original = Data._325_10Coll.FirstOrDefault(e => e.Id == rowId);
            if (original != null)
            {
                /*
                if (original.Boeken.HasValue && original.Boeken.Value)
                {
                    worksheetset.DeleteBooking(FacturatieApp.TypeId, "OpTeMakenCN", original.Id, null);
                    // DELETE BOOKING
                }*/
                Data._325_10Coll.Remove(original);
              //  Data.LastUpdated = DateTime.Now;
            }
        }

        public void Update_325_10Coll(EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.Item325 entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.Item325 original = Data._325_10Coll.FirstOrDefault(e => e.Id == entity.Id);
            if (original == null)
            {
                AddTo_325_10Coll(entity, ref worksheetset);
            }
            else
            {
                entity = Validate(entity, "_325_10Coll", ref worksheetset);
                if (entity.IsValid)
                {
                    int idx = Data._325_10Coll.IndexOf(original);
                    Data._325_10Coll.RemoveAt(idx);
                    Data._325_10Coll.Insert(idx, entity);
             //       Data.LastUpdated = DateTime.Now;
                    entity.LastUpdated = DateTime.Now;
                }
            }
        }

        public void AddToTeVerklarenVerschil325_10(EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.VerschilItem entity, ref WorksheetSetData worksheetset)
        {
            entity = Validate(entity, "TeVerklarenVerschil325_10", ref worksheetset);
            if (entity.IsValid)
            {
            //    Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
                if(!entity.AutoLine.GetValueOrDefault()) entity.order = 999;
                Data.TeVerklarenVerschil325_10.Add(entity);
            }
        }

        public void DeleteTeVerklarenVerschil325_10(Guid rowId, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.VerschilItem original = Data.TeVerklarenVerschil325_10.FirstOrDefault(e => e.Id == rowId);
            if (original != null)
            {
                /*
                if (original.Boeken.HasValue && original.Boeken.Value)
                {
                    worksheetset.DeleteBooking(FacturatieApp.TypeId, "OpTeMakenCN", original.Id, null);
                    // DELETE BOOKING
                }*/
                Data.TeVerklarenVerschil325_10.Remove(original);
            //    Data.LastUpdated = DateTime.Now;
            }
        }

        public void UpdateTeVerklarenVerschil325_10(EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.VerschilItem entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.VerschilItem original = Data.TeVerklarenVerschil325_10.FirstOrDefault(e => e.Id == entity.Id);
            if (original == null)
            {
                AddToTeVerklarenVerschil325_10(entity, ref worksheetset);
            }
            else
            {
                entity = Validate(entity, "TeVerklarenVerschil325_10", ref worksheetset);
                if (entity.IsValid)
                {
                    if (!entity.AutoLine.GetValueOrDefault()) entity.order = 999;
                    int idx = Data.TeVerklarenVerschil325_10.IndexOf(original);
                    Data.TeVerklarenVerschil325_10.RemoveAt(idx);
                    Data.TeVerklarenVerschil325_10.Insert(idx, entity);
               //     Data.LastUpdated = DateTime.Now;
                    entity.LastUpdated = DateTime.Now;
                }
            }
        }

        public void UpdateArbeidersForm(EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.Werknemer entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.Werknemer original = Data.ArbeidersForm;

            entity = Validate(entity, "ArbeidersForm", ref worksheetset);
            if (entity.IsValid)
            {

                Data.ArbeidersForm = entity;
               // Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
            }
        }

        public void UpdateBediendeForm(EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.Werknemer entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.Werknemer original = Data.BediendeForm;

            entity = Validate(entity, "BediendeForm", ref worksheetset);
            if (entity.IsValid)
            {

                Data.BediendeForm = entity;
                //Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
            }
        }

        public void AddToAansluitingArbeiders(EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.VerschilItem entity, ref WorksheetSetData worksheetset)
        {
            entity = Validate(entity, "AansluitingArbeiders", ref worksheetset);
            if (entity.IsValid)
            {
                //Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
                Data.AansluitingArbeiders.Add(entity);
            }
        }

        public void DeleteAansluitingArbeiders(Guid rowId, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.VerschilItem original = Data.AansluitingArbeiders.FirstOrDefault(e => e.Id == rowId);
            if (original != null)
            {
                /*
                if (original.Boeken.HasValue && original.Boeken.Value)
                {
                    worksheetset.DeleteBooking(FacturatieApp.TypeId, "OpTeMakenCN", original.Id, null);
                    // DELETE BOOKING
                }*/
                Data.AansluitingArbeiders.Remove(original);
                //Data.LastUpdated = DateTime.Now;
            }
        }

        public void UpdateAansluitingArbeiders(EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.VerschilItem entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.VerschilItem original = Data.AansluitingArbeiders.FirstOrDefault(e => e.Id == entity.Id);
            if (original == null)
            {
                AddToAansluitingArbeiders(entity, ref worksheetset);
            }
            else
            {
                entity = Validate(entity, "AansluitingArbeiders", ref worksheetset);
                if (entity.IsValid)
                {
                    int idx = Data.AansluitingArbeiders.IndexOf(original);
                    Data.AansluitingArbeiders.RemoveAt(idx);
                    Data.AansluitingArbeiders.Insert(idx, entity);
                  //  Data.LastUpdated = DateTime.Now;
                    entity.LastUpdated = DateTime.Now;
                }
            }
        }

        public void AddToAansluitingBedienden(EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.VerschilItem entity, ref WorksheetSetData worksheetset)
        {
            entity = Validate(entity, "AansluitingBedienden", ref worksheetset);
            if (entity.IsValid)
            {
               //Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
                Data.AansluitingBedienden.Add(entity);
            }
        }

        public void DeleteAansluitingBedienden(Guid rowId, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.VerschilItem original = Data.AansluitingBedienden.FirstOrDefault(e => e.Id == rowId);
            if (original != null)
            {
                /*
                if (original.Boeken.HasValue && original.Boeken.Value)
                {
                    worksheetset.DeleteBooking(FacturatieApp.TypeId, "OpTeMakenCN", original.Id, null);
                    // DELETE BOOKING
                }*/
                Data.AansluitingBedienden.Remove(original);
                //Data.LastUpdated = DateTime.Now;
            }
        }

        public void UpdateAansluitingBedienden(EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.VerschilItem entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.VerschilItem original = Data.AansluitingBedienden.FirstOrDefault(e => e.Id == entity.Id);
            if (original == null)
            {
                AddToAansluitingBedienden(entity, ref worksheetset);
            }
            else
            {
                entity = Validate(entity, "AansluitingBedienden", ref worksheetset);
                if (entity.IsValid)
                {
                    int idx = Data.AansluitingBedienden.IndexOf(original);
                    Data.AansluitingBedienden.RemoveAt(idx);
                    Data.AansluitingBedienden.Insert(idx, entity);
                    //Data.LastUpdated = DateTime.Now;
                    entity.LastUpdated = DateTime.Now;
                }
            }
        }

        public void AddToRSZColl(EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.Algemeen entity, ref WorksheetSetData worksheetset)
        {
            entity = Validate(entity, "RSZColl", ref worksheetset);
            if (entity.IsValid)
            {
                //Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
                Data.RSZColl.Add(entity);
            }
        }

        public void DeleteRSZColl(Guid rowId, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.Algemeen original = Data.RSZColl.FirstOrDefault(e => e.Id == rowId);
            if (original != null)
            {
                /*
                if (original.Boeken.HasValue && original.Boeken.Value)
                {
                    worksheetset.DeleteBooking(FacturatieApp.TypeId, "OpTeMakenCN", original.Id, null);
                    // DELETE BOOKING
                }*/
                Data.RSZColl.Remove(original);
               // Data.LastUpdated = DateTime.Now;
            }
        }

        public void UpdateRSZColl(EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.Algemeen entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.Algemeen original = Data.RSZColl.FirstOrDefault(e => e.Id == entity.Id);
            if (original == null)
            {
                AddToRSZColl(entity, ref worksheetset);
            }
            else
            {
                entity = Validate(entity, "RSZColl", ref worksheetset);
                if (entity.IsValid)
                {
                    int idx = Data.RSZColl.IndexOf(original);
                    Data.RSZColl.RemoveAt(idx);
                    Data.RSZColl.Insert(idx, entity);
                    //Data.LastUpdated = DateTime.Now;
                    entity.LastUpdated = DateTime.Now;
                }
            }
        }

        public void AddToBVsColl(EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.Algemeen entity, ref WorksheetSetData worksheetset)
        {
            entity = Validate(entity, "BVsColl", ref worksheetset);
            if (entity.IsValid)
            {
                //Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
                Data.BVsColl.Add(entity);
            }
        }

        public void DeleteBVsColl(Guid rowId, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.Algemeen original = Data.BVsColl.FirstOrDefault(e => e.Id == rowId);
            if (original != null)
            {
                /*
                if (original.Boeken.HasValue && original.Boeken.Value)
                {
                    worksheetset.DeleteBooking(FacturatieApp.TypeId, "OpTeMakenCN", original.Id, null);
                    // DELETE BOOKING
                }*/
                Data.BVsColl.Remove(original);
                //Data.LastUpdated = DateTime.Now;
            }
        }

        public void UpdateBVsColl(EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.Algemeen entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.Algemeen original = Data.BVsColl.FirstOrDefault(e => e.Id == entity.Id);
            if (original == null)
            {
                AddToBVsColl(entity, ref worksheetset);
            }
            else
            {
                entity = Validate(entity, "BVsColl", ref worksheetset);
                if (entity.IsValid)
                {
                    int idx = Data.BVsColl.IndexOf(original);
                    Data.BVsColl.RemoveAt(idx);
                    Data.BVsColl.Insert(idx, entity);
                    //Data.LastUpdated = DateTime.Now;
                    entity.LastUpdated = DateTime.Now;
                }
            }
        }

        public void AddToNettoColl(EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.Algemeen entity, ref WorksheetSetData worksheetset)
        {
            entity = Validate(entity, "NettoColl", ref worksheetset);
            if (entity.IsValid)
            {
                //Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
                Data.NettoColl.Add(entity);
            }
        }

        public void DeleteNettoColl(Guid rowId, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.Algemeen original = Data.NettoColl.FirstOrDefault(e => e.Id == rowId);
            if (original != null)
            {
                /*
                if (original.Boeken.HasValue && original.Boeken.Value)
                {
                    worksheetset.DeleteBooking(FacturatieApp.TypeId, "OpTeMakenCN", original.Id, null);
                    // DELETE BOOKING
                }*/
                Data.NettoColl.Remove(original);
                //Data.LastUpdated = DateTime.Now;
            }
        }

        public void UpdateNettoColl(EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.Algemeen entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.Algemeen original = Data.NettoColl.FirstOrDefault(e => e.Id == entity.Id);
            if (original == null)
            {
                AddToNettoColl(entity, ref worksheetset);
            }
            else
            {
                entity = Validate(entity, "NettoColl", ref worksheetset);
                if (entity.IsValid)
                {
                    int idx = Data.NettoColl.IndexOf(original);
                    Data.NettoColl.RemoveAt(idx);
                    Data.NettoColl.Insert(idx, entity);
                    //Data.LastUpdated = DateTime.Now;
                    entity.LastUpdated = DateTime.Now;
                }
            }
        }

        public void UpdateVakantieGeldColl(EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.Vakantiegeld entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.Vakantiegeld original = Data.VakantieGeldColl;

            entity = Validate(entity, "VakantieGeldColl", ref worksheetset);
            if (entity.IsValid)
            {

                Data.VakantieGeldColl = entity;
                //Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
            }
        }

        #endregion


        #region worksheet type import previous

        //public void ImportPrevious(EY.com.eBook.RuleEngine.AY2011.PersoneelApp previous)
        //{
        //    if (previous.Data.VakantieGeldColl != null)
        //    {
        //        if (previous.Data.VakantieGeldColl.gekozenSchuldArbeider.HasValue || previous.Data.VakantieGeldColl.gekozenSchuldBediende.HasValue)
        //        {
        //            if (Data.VakantieGeldColl == null) Data.VakantieGeldColl = new EY.com.eBook.RuleEngine.AY2012.PersoneelAppTypes.Vakantiegeld { Id = Guid.NewGuid() };
        //            Data.VakantieGeldColl.previousGekozenSchuldArbeider = previous.Data.VakantieGeldColl.gekozenSchuldArbeider;
        //            Data.VakantieGeldColl.previousGekozenSchuldBediende = previous.Data.VakantieGeldColl.gekozenSchuldBediende;
        //        }

        //    }

        //}



        #endregion
        
    }
}
  