﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.RuleEngine.Core;
using EY.com.eBook.RuleEngine.Core.Interfaces;
using EY.com.eBook.RuleEngine.Data;
using EY.com.eBook.Core;
using EY.com.eBook.API.Contracts.Data.Cache;

namespace EY.com.eBook.RuleEngine.AY2012
{
    
    public class BepalingBelastbareBasisApp
    {
        public static WorksheetTypeSettings Settings = new WorksheetTypeSettings
                                                      {
                                                        Clients = true
                                                        ,
                                                        Suppliers = true
                                                        ,
                                                        AllAccounts = true
                                                        , 
                                                        PerformsBookings=true
                                                      };
                                                      
        public static Guid TypeId = new Guid("ceae70d1-20ef-4ab9-91e6-64b3f0f5eb98");

        public List<string> ImportablePreviousTypes  = new List<string>() {  "EY.com.eBook.RuleEngine.AY2011.FacturatieApp"  };

        public Guid GetMyType() { return TypeId; }

        public void SetMinimal()
        {
            if (Data == null)
            {
                Data = new EY.com.eBook.RuleEngine.AY2012.BepalingBelastbareBasisAppTypes.BepalingBasis();
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.Overzicht == null)
            {
                Data.Overzicht = new EY.com.eBook.RuleEngine.AY2012.BepalingBelastbareBasisAppTypes.General
                {
                    Id = Guid.NewGuid()
                    ,
                    LastUpdated = null
                    ,
                    IsValid = true
                    ,
                    PreviousImported = false

                };
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.BelasteReserves == null)
            {
                Data.BelasteReserves = new EY.com.eBook.RuleEngine.AY2012.BepalingBelastbareBasisAppTypes.BelasteReservesItem
                {
                    Id = Guid.NewGuid()
                    ,
                    LastUpdated = null
                    ,
                    IsValid = true
                    ,
                    PreviousImported = false

                };
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.VerworpenUitgaven == null)
            {
                Data.VerworpenUitgaven = new EY.com.eBook.RuleEngine.AY2012.BepalingBelastbareBasisAppTypes.VerworpenUitgavenItem
                {
                    Id = Guid.NewGuid()
                    ,
                    LastUpdated = null
                    ,
                    IsValid = true
                    ,
                    PreviousImported = false

                };
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.BelastbareBestZonderAftrek == null)
            {
                Data.BelastbareBestZonderAftrek = new EY.com.eBook.RuleEngine.AY2012.BepalingBelastbareBasisAppTypes.BelastbareBestZonderAftrekItem
                {
                    Id = Guid.NewGuid()
                    ,
                    LastUpdated = null
                    ,
                    IsValid = true
                    ,
                    PreviousImported = false

                };
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.OpdelingNaarOorsprong == null)
            {
                Data.OpdelingNaarOorsprong = new EY.com.eBook.RuleEngine.AY2012.BepalingBelastbareBasisAppTypes.OpdelingNaarOorsprongItem
                {
                    Id = Guid.NewGuid()
                    ,
                    LastUpdated = null
                    ,
                    IsValid = true
                    ,
                    PreviousImported = false

                };
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.FiscaleAftrek == null)
            {
                Data.FiscaleAftrek = new EY.com.eBook.RuleEngine.AY2012.BepalingBelastbareBasisAppTypes.FiscaleAftrekItem
                {
                    Id = Guid.NewGuid()
                    ,
                    LastUpdated = null
                    ,
                    IsValid = true
                    ,
                    PreviousImported = false

                };
                Data.LastUpdated = DateTime.Now;
            }

        }
        
        public string ToXml() 
        {
            return CoreHelper.SerializeToString(Data);
        }

        public void LoadXml(string xml)
        {
            if (!string.IsNullOrEmpty(xml)) {
                Data = CoreHelper.DeserializeFromString<EY.com.eBook.RuleEngine.AY2012.BepalingBelastbareBasisAppTypes.BepalingBasis>(xml);
            }
            SetMinimal();
            
        }
        
        public EY.com.eBook.RuleEngine.AY2012.BepalingBelastbareBasisAppTypes.BepalingBasis Data { get; set; }

        public void ForceRecalculation(ref WorksheetSetData worksheetset)
        {
            Data.LastUpdated = null;
            Calculate(ref worksheetset);
        }

        public void Calculate(ref WorksheetSetData worksheetset)
        {
            bool recalc = false;
            worksheetset.ClearMessages(TypeId);
            if (!Data.LastUpdated.HasValue) Data.LastUpdated = new DateTime(1980,1,1);
            recalc= LoadWorksheets(ref worksheetset);

            /*
            * @version 5.2
            * @ticket 55
            */
            List<AccountDataContract> accountsFinGift = worksheetset.GetAccountsOfMappingMeta("fingift", "VerworpenUitgaven", false, false, true, true, true, worksheetset.DataContract.Culture);
            Data.FiscaleAftrek.NTEBECharity = accountsFinGift.Count > 0 ? accountsFinGift.Sum(a => a.Saldo) : 0;

            decimal dividends = 0;
            decimal geresWinst = 0;
            
                List<AccountDataContract> accs = worksheetset.GetAccountsInRanges(new List<string> { "694", "696" }, false, false, worksheetset.DataContract.Culture);
               // Data.Overzicht.Dividenden = accs.Count > 0 ? 0- accs.Sum(a => a.Saldo) : 0;
                dividends = accs.Where(a => !a.IsTaxAdjusment).Count() > 0 ? accs.Where(a => !a.IsTaxAdjusment).Sum(a => a.Saldo) : 0;

                accs = worksheetset.GetAccountsInRanges(new List<string> { "70", "71", "72", "73", "74", "75", "76", "77", "78" }, false, false, worksheetset.DataContract.Culture);
                decimal saldo = 0;
                if (accs.Where(a => !a.IsTaxAdjusment).Count() > 0) saldo += accs.Where(a => !a.IsTaxAdjusment).Sum(a => a.Saldo);
                accs = worksheetset.GetAccountsInRanges(new List<string> { "60", "61", "62", "63", "64", "65", "66", "67", "68", "694", "696" }, false, false, worksheetset.DataContract.Culture);
                if (accs.Where(a => !a.IsTaxAdjusment).Count() > 0) saldo += accs.Where(a => !a.IsTaxAdjusment).Sum(a => a.Saldo);
                saldo = (0 - saldo);
                geresWinst = saldo;

            
            if (!Data.Overzicht.Manual.HasValue || !Data.Overzicht.Manual.Value)
            {
                Data.Overzicht.Dividenden = dividends;
                Data.Overzicht.GereserveerdeWinst = geresWinst;
            }
            else
            {
                if (Data.Overzicht.Dividenden.GetValueOrDefault() != dividends)
                {

                    worksheetset.SetMessage(TypeId, MessageTypeDataContract.Warning, "Dividenden_Check", "Overzicht", Data.Overzicht.Id, "Dividenden", new object[] { Data.Overzicht.Dividenden.GetValueOrDefault().ToString(), dividends.ToString() });
                }
                if (Data.Overzicht.GereserveerdeWinst.GetValueOrDefault() != geresWinst)
                {
                    worksheetset.SetMessage(TypeId, MessageTypeDataContract.Warning, "GereserveerdeWinst_Check", "Overzicht", Data.Overzicht.Id, "GereserveerdeWinst", new object[] { geresWinst , Data.Overzicht.GereserveerdeWinst.GetValueOrDefault()});
                }
            }


            Data.Overzicht.StatAccountingResult = Data.Overzicht.GereserveerdeWinst.GetValueOrDefault() + Data.Overzicht.Dividenden.GetValueOrDefault() + Data.Overzicht.VerkrijgingEigenAandelen.GetValueOrDefault()
                + Data.Overzicht.OverlijdenUittredingUitsluitingVennoot.GetValueOrDefault() + Data.Overzicht.VerdelingMaatschappelijkVermogen.GetValueOrDefault();


            Data.Overzicht.NietAftrekbareBelastingen = Data.VerworpenUitgaven.PDDisallowedCurrentIncomeTaxExpenses.GetValueOrDefault();

            Data.BelastbareBestZonderAftrek.TEVaaCar = Data.VerworpenUitgaven.DECarVAA.GetValueOrDefault();
            Data.BelastbareBestZonderAftrek.TECapitalAgriCulture = Data.BelastbareBestZonderAftrek.TECapitalAgriCulture.GetValueOrDefault();
            Data.BelastbareBestZonderAftrek.TETotal = Data.BelastbareBestZonderAftrek.TEReceivedAbnormal.GetValueOrDefault() + Data.BelastbareBestZonderAftrek.TETaxableInvestmentReserve.GetValueOrDefault()
                + Data.BelastbareBestZonderAftrek.TEEmployeeParticipationCapital.GetValueOrDefault() + Data.BelastbareBestZonderAftrek.TECapitalAgriCulture.GetValueOrDefault() + Data.BelastbareBestZonderAftrek.TEVaaCar.GetValueOrDefault();

            Data.Overzicht.TotalBelasteReserves = Data.BelasteReserves.TDTotal.GetValueOrDefault();
            Data.Overzicht.TotalAanpassingenBelasteReserves = Data.BelasteReserves.OATotal.GetValueOrDefault();
            Data.Overzicht.TotaalVerworpenUitgaven = Data.VerworpenUitgaven.DETotal.GetValueOrDefault();

            Data.Overzicht.BelastbaarResultaat = (((Data.Overzicht.StatAccountingResult.GetValueOrDefault() + Data.Overzicht.TotalBelasteReserves.GetValueOrDefault())
                                                   - Data.Overzicht.TotalAanpassingenBelasteReserves.GetValueOrDefault()) + Data.Overzicht.NietAftrekbareBelastingen.GetValueOrDefault())
                                                   + Data.Overzicht.TotaalVerworpenUitgaven.GetValueOrDefault();
            Data.Overzicht.TotaalBelastbareBestandenZonderAftrekMin = Data.BelastbareBestZonderAftrek.TETotal.GetValueOrDefault();
            Data.Overzicht.SubTotaalBelastbaarVoorAftrek = Data.Overzicht.BelastbaarResultaat.GetValueOrDefault() - Data.Overzicht.TotaalBelastbareBestandenZonderAftrekMin.GetValueOrDefault();

            if (!Data.Overzicht.Manual.HasValue || !Data.Overzicht.Manual.Value)
            {
                Data.OpdelingNaarOorsprong.IRBelgianTaxable = Data.Overzicht.SubTotaalBelastbaarVoorAftrek.GetValueOrDefault();
                Data.OpdelingNaarOorsprong.IRNonTreatyForeignIncome = 0;
                Data.OpdelingNaarOorsprong.IRTreatyForeignIncome = 0;
            }


            Data.OpdelingNaarOorsprong.IRRemainingResultBelgium = Data.Overzicht.SubTotaalBelastbaarVoorAftrek.GetValueOrDefault() - Data.OpdelingNaarOorsprong.IRBelgianTaxable.GetValueOrDefault()
                + Data.OpdelingNaarOorsprong.IRNonTreatyForeignIncome.GetValueOrDefault() - Data.OpdelingNaarOorsprong.IRTreatyForeignIncome.GetValueOrDefault();

            if (Data.OpdelingNaarOorsprong.IRRemainingResultBelgium.GetValueOrDefault() != 0)
            {
                //ROOT.eBookConnection.AddMessage(99; "Overzicht"; ROOT.Worksheet.OpdelingNaarOorsprong.ID; ""; "OPDELING_NOTEQUALTO")
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "OPDELING_NOTEQUALTO", "OpdelingNaarOorsprong", Data.OpdelingNaarOorsprong.Id, "IRRemainingResultBelgium");
              
                Data.OpdelingNaarOorsprong.IsValid = false;

            }
            else
            {
                Data.OpdelingNaarOorsprong.IsValid = true;
            }


            Data.Overzicht.OpdelingNaarOorsprongBijVerdragVrijgesteldInkomen = Data.OpdelingNaarOorsprong.IRTreatyForeignIncome.GetValueOrDefault();
            Data.Overzicht.BasisVoorFiscaleAftrek = Data.Overzicht.SubTotaalBelastbaarVoorAftrek.GetValueOrDefault() - Data.Overzicht.OpdelingNaarOorsprongBijVerdragVrijgesteldInkomen.GetValueOrDefault();


            Data.FiscaleAftrek.TotalNonTaxableElements = (((Data.FiscaleAftrek.NTEBECharity.GetValueOrDefault() + Data.FiscaleAftrek.NTEBEPersonnel.GetValueOrDefault())
                                                    + Data.FiscaleAftrek.NTEBEPersonnelSME.GetValueOrDefault()) + Data.FiscaleAftrek.NTEBEInternalshipPremium.GetValueOrDefault())
                                                    + Data.FiscaleAftrek.NTEBEOthers.GetValueOrDefault();


            if (Data.Overzicht.BasisVoorFiscaleAftrek.GetValueOrDefault() <= 0)
            {
                Data.FiscaleAftrek.ResterendBasisVoorAftrek = 0;
                Data.FiscaleAftrek.NTEBECharity = 0;
                Data.FiscaleAftrek.NTEBEPersonnel = 0;
                Data.FiscaleAftrek.NTEBEPersonnelSME = 0;
                Data.FiscaleAftrek.NTEBEInternalshipPremium = 0;
                Data.FiscaleAftrek.NTEBEOthers = 0;
                Data.FiscaleAftrek.TotalNonTaxableElements = 0;
            }
            else
            {
                Data.FiscaleAftrek.ResterendBasisVoorAftrek = Data.Overzicht.BasisVoorFiscaleAftrek.GetValueOrDefault() - Data.FiscaleAftrek.TotalNonTaxableElements.GetValueOrDefault();
            }
            if (Data.FiscaleAftrek.NTEBECharity > 0) { 
                decimal? test = (Data.OpdelingNaarOorsprong.IRBelgianTaxable / 100) * 5;
                if (Data.FiscaleAftrek.NTEBECharity > test || Data.FiscaleAftrek.NTEBECharity > 500001)
                {
                    worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "SMALLERTHEN", "FiscaleAftrek", Data.FiscaleAftrek.Id, "NTEBECharity");

                    Data.FiscaleAftrek.IsValid = false;
                }
                else {
                    Data.FiscaleAftrek.IsValid = true;
                }
            }
            

            Data.LastUpdated = DateTime.Now;
        }

        private bool LoadWorksheets(ref WorksheetSetData worksheetset)
        {
            Data.LastUpdated = Data.LastUpdated.Value.AddMinutes(-1);
            bool recalc = false;
            Data.VerworpenUitgaven.DECarVAA = Data.VerworpenUitgaven.DECarVAA.GetValueOrDefault();
            if (Data.LastUpdated <= worksheetset.BelastingenApp.Data.LastUpdated)
            {
                Data.VerworpenUitgaven.DEAbnormalAdvantages = worksheetset.BelastingenApp.Data.AbnormaleGoedgunstigeVoordelen.Sum(a => a.Bedrag.GetValueOrDefault());
                Data.VerworpenUitgaven.DEBepaaldeStaten = worksheetset.BelastingenApp.Data.BepaaldeStaten.Sum(a => a.Bedrag.GetValueOrDefault());
                Data.VerworpenUitgaven.DECar = worksheetset.BelastingenApp.Data.Auto.Sum(a => a.Bedrag.GetValueOrDefault());
                Data.VerworpenUitgaven.DECharity = worksheetset.BelastingenApp.Data.Liberaliteiten.Sum(a => a.Bedrag.GetValueOrDefault());
                Data.VerworpenUitgaven.DEClothes = worksheetset.BelastingenApp.Data.Kledij.Sum(a => a.Bedrag.GetValueOrDefault());
                Data.VerworpenUitgaven.DEDinerSportCultureEco = worksheetset.BelastingenApp.Data.MaaltijdSportCultuurEco.Sum(a => a.Bedrag.GetValueOrDefault());
                Data.VerworpenUitgaven.DEEmployeeCapital = worksheetset.BelastingenApp.Data.Werknemersparticipatie.Sum(a => a.Bedrag.GetValueOrDefault());
                Data.VerworpenUitgaven.DEExcessInterest = worksheetset.BelastingenApp.Data.OverdrevenIntresten.Sum(a => a.Bedrag.GetValueOrDefault());
                Data.VerworpenUitgaven.DEExpensesTaxShelter = worksheetset.BelastingenApp.Data.KostenTaxShelter.Sum(a => a.Bedrag.GetValueOrDefault());
                Data.VerworpenUitgaven.DEIndemnityMissingCoupon = worksheetset.BelastingenApp.Data.VergoedingCoupon.Sum(a => a.Bedrag.GetValueOrDefault());
                Data.VerworpenUitgaven.DEInterestLoans = worksheetset.BelastingenApp.Data.IntrestenLeningen.Sum(a => a.Bedrag.GetValueOrDefault());
                Data.VerworpenUitgaven.DEOther = worksheetset.BelastingenApp.Data.Andere.Sum(a => a.Bedrag.GetValueOrDefault());
                Data.VerworpenUitgaven.DEPenalties = worksheetset.BelastingenApp.Data.Boetes.Sum(a => a.Bedrag.GetValueOrDefault());
                Data.VerworpenUitgaven.DEPensions = worksheetset.BelastingenApp.Data.Pensioenen.Sum(a => a.Bedrag.GetValueOrDefault());
                Data.VerworpenUitgaven.DEReception = worksheetset.BelastingenApp.Data.Receptie.Sum(a => a.Bedrag.GetValueOrDefault());
                Data.VerworpenUitgaven.DEReductionValueShares = worksheetset.BelastingenApp.Data.WaardeverminderingAandelen.Sum(a => a.Bedrag.GetValueOrDefault());
                Data.VerworpenUitgaven.DERegionalPremiumsCapital = worksheetset.BelastingenApp.Data.GewestPremies.Sum(a => a.Bedrag.GetValueOrDefault());
                Data.VerworpenUitgaven.DERegionalTaxes = worksheetset.BelastingenApp.Data.GewestBelasting.Sum(a => a.Bedrag.GetValueOrDefault());
                Data.VerworpenUitgaven.DERestaurant = worksheetset.BelastingenApp.Data.Restaurant.Sum(a => a.Bedrag.GetValueOrDefault());
                Data.VerworpenUitgaven.DEReversalsPreviousTax = worksheetset.BelastingenApp.Data.TerugnemingVroegereVrijstellingen.Sum(a => a.Bedrag.GetValueOrDefault());
                Data.VerworpenUitgaven.DESocial = worksheetset.BelastingenApp.Data.SocialeVoordelen.Sum(a => a.Bedrag.GetValueOrDefault());
                
                if (worksheetset.BelastingenApp.Data.AutoVAA !=null && worksheetset.BelastingenApp.Data.AutoVAA.Count>0) Data.VerworpenUitgaven.DECarVAA = worksheetset.BelastingenApp.Data.AutoVAA.Sum(a => a.Bedrag.GetValueOrDefault());

                
            }
            Data.VerworpenUitgaven.DETotal = Data.VerworpenUitgaven.DEAbnormalAdvantages.GetValueOrDefault()
                                                 + Data.VerworpenUitgaven.DEBepaaldeStaten.GetValueOrDefault()
                                                 + Data.VerworpenUitgaven.DECar.GetValueOrDefault()
                                                 + Data.VerworpenUitgaven.DECarVAA.GetValueOrDefault()
                                                 + Data.VerworpenUitgaven.DECharity.GetValueOrDefault()
                                                 + Data.VerworpenUitgaven.DEClothes.GetValueOrDefault()
                                                 + Data.VerworpenUitgaven.DEDinerSportCultureEco.GetValueOrDefault()
                                                 + Data.VerworpenUitgaven.DEEmployeeCapital.GetValueOrDefault()
                                                 + Data.VerworpenUitgaven.DEExcessInterest.GetValueOrDefault()
                                                 + Data.VerworpenUitgaven.DEExpensesTaxShelter.GetValueOrDefault()
                                                 + Data.VerworpenUitgaven.DEIndemnityMissingCoupon.GetValueOrDefault()
                                                 + Data.VerworpenUitgaven.DEInterestLoans.GetValueOrDefault()
                                                 + Data.VerworpenUitgaven.DEOther.GetValueOrDefault()
                                                 + Data.VerworpenUitgaven.DEPenalties.GetValueOrDefault()
                                                 + Data.VerworpenUitgaven.DEPensions.GetValueOrDefault()
                                                 + Data.VerworpenUitgaven.DEReception.GetValueOrDefault()
                                                 + Data.VerworpenUitgaven.DEReductionValueShares.GetValueOrDefault()
                                                 + Data.VerworpenUitgaven.DERegionalPremiumsCapital.GetValueOrDefault()
                                                 + Data.VerworpenUitgaven.DERegionalTaxes.GetValueOrDefault()
                                                 + Data.VerworpenUitgaven.DERestaurant.GetValueOrDefault()
                                                 + Data.VerworpenUitgaven.DEReversalsPreviousTax.GetValueOrDefault()
                                                 + Data.VerworpenUitgaven.DESocial.GetValueOrDefault();
            Data.VerworpenUitgaven.PDDisallowedCurrentIncomeTaxExpenses = worksheetset.BelastingenApp.Data.Belastingen.Sum(a => a.Bedrag.GetValueOrDefault());
            Data.BelastbareBestZonderAftrek.TEEmployeeParticipationCapital = Data.VerworpenUitgaven.DEEmployeeCapital;
            recalc = true;


            if (Data.LastUpdated <= worksheetset.BelasteReservesApp.Data.LastUpdated)
            {
                Guid reserve = new Guid("0f5ff0f1-df29-4342-9e8c-1630304da12f");
                Data.BelasteReserves.TDTaxableProvisions = worksheetset.BelasteReservesApp.Data.AllBelasteReserves.Where(b =>b.TypeReseve !=null && b.TypeReseve.Id == reserve).Sum(b => b.ToenameTijdensBJ)
                                                            - worksheetset.BelasteReservesApp.Data.AllBelasteReserves.Where(b => b.TypeReseve != null && b.TypeReseve.Id == reserve).Sum(b => b.AfnameTijdensBJ);

                reserve = new Guid("244403c0-219f-4a3a-94da-c9eb19950b4f");
                Data.BelasteReserves.TDOtherTaxableReserves = worksheetset.BelasteReservesApp.Data.AllBelasteReserves.Where(b => b.TypeReseve != null && b.TypeReseve.Id == reserve).Sum(b => b.ToenameTijdensBJ)
                                                            - worksheetset.BelasteReservesApp.Data.AllBelasteReserves.Where(b => b.TypeReseve != null && b.TypeReseve.Id == reserve).Sum(b => b.AfnameTijdensBJ);

                reserve = new Guid("b97e3772-4090-4bad-a623-a3e0c6044855");
                Data.BelasteReserves.TDTaxableDoubtfullDebtors = worksheetset.BelasteReservesApp.Data.AllBelasteReserves.Where(b => b.TypeReseve != null && b.TypeReseve.Id == reserve).Sum(b => b.ToenameTijdensBJ)
                                                            - worksheetset.BelasteReservesApp.Data.AllBelasteReserves.Where(b => b.TypeReseve != null && b.TypeReseve.Id == reserve).Sum(b => b.AfnameTijdensBJ);

                reserve = new Guid("17e42920-cba9-43c4-90c5-2b74d75daec7");
                Data.BelasteReserves.TDExcessDeprications = worksheetset.BelasteReservesApp.Data.AllBelasteReserves.Where(b => b.TypeReseve != null && b.TypeReseve.Id == reserve).Sum(b => b.ToenameTijdensBJ)
                                                            - worksheetset.BelasteReservesApp.Data.AllBelasteReserves.Where(b => b.TypeReseve != null && b.TypeReseve.Id == reserve).Sum(b => b.AfnameTijdensBJ);

                reserve = new Guid("f67a17d7-8f7d-43d9-9955-b8a9693587a1");
                Data.BelasteReserves.TDUnderestimationAssetsOverestimationLiabilities = worksheetset.BelasteReservesApp.Data.AllBelasteReserves.Where(b => b.TypeReseve != null && b.TypeReseve.Id == reserve).Sum(b => b.ToenameTijdensBJ)
                                                            - worksheetset.BelasteReservesApp.Data.AllBelasteReserves.Where(b => b.TypeReseve != null && b.TypeReseve.Id == reserve).Sum(b => b.AfnameTijdensBJ);

                reserve = new Guid("bbc12ef9-3e15-4c70-92d8-11f7388ed64a");
                Data.BelasteReserves.TDKapitaalEnUitgifte = worksheetset.BelasteReservesApp.Data.AllBelasteReserves.Where(b => b.TypeReseve != null && b.TypeReseve.Id == reserve).Sum(b => b.ToenameTijdensBJ)
                                                            - worksheetset.BelasteReservesApp.Data.AllBelasteReserves.Where(b => b.TypeReseve != null && b.TypeReseve.Id == reserve).Sum(b => b.AfnameTijdensBJ);


                reserve = new Guid("4b49ee2c-90c4-4649-93db-df950d9b90e3");
                Data.BelasteReserves.TDOverestimationLiabilities = worksheetset.BelasteReservesApp.Data.AllBelasteReserves.Where(b => b.TypeReseve != null && b.TypeReseve.Id == reserve).Sum(b => b.ToenameTijdensBJ)
                                                            - worksheetset.BelasteReservesApp.Data.AllBelasteReserves.Where(b => b.TypeReseve != null && b.TypeReseve.Id == reserve).Sum(b => b.AfnameTijdensBJ);

                reserve = new Guid("8ca508c3-b59f-4106-9afa-30b1acda92b0");
                Data.BelasteReserves.TDHerwaardering = worksheetset.BelasteReservesApp.Data.AllBelasteReserves.Where(b => b.TypeReseve != null && b.TypeReseve.Id == reserve).Sum(b => b.ToenameTijdensBJ)
                                                            - worksheetset.BelasteReservesApp.Data.AllBelasteReserves.Where(b => b.TypeReseve != null && b.TypeReseve.Id == reserve).Sum(b => b.AfnameTijdensBJ);


                Data.BelasteReserves.TDTotal = Data.BelasteReserves.TDTaxableProvisions
                                                + Data.BelasteReserves.TDOtherTaxableReserves
                                                + Data.BelasteReserves.TDTaxableDoubtfullDebtors
                                                + Data.BelasteReserves.TDExcessDeprications
                                                + Data.BelasteReserves.TDUnderestimationAssetsOverestimationLiabilities
                                                + Data.BelasteReserves.TDKapitaalEnUitgifte
                                                + Data.BelasteReserves.TDOverestimationLiabilities
                                                + Data.BelasteReserves.TDHerwaardering;
                


                // AANPASSINGEN

                reserve = new Guid("f34c7d2b-428d-494d-a29b-f2bd2910a7a9");
                Data.BelasteReserves.OACapitalGainsOnShares = worksheetset.BelasteReservesApp.Data.AanpassingBTReserves.Where(b => b.TypeReseve != null && b.TypeReseve.Id == reserve).Sum(b => b.BedragNaPercentage);

                reserve = new Guid("f1875c70-b0dc-41be-8ada-46c702a954c1");
                Data.BelasteReserves.OADefinitiveExemptionTaxShelter = worksheetset.BelasteReservesApp.Data.AanpassingBTReserves.Where(b => b.TypeReseve != null && b.TypeReseve.Id == reserve).Sum(b => b.BedragNaPercentage);

                reserve = new Guid("6a716040-5635-4e9d-8161-f5eaaaff5db2");
                Data.BelasteReserves.OAExemptionRegionalPrem = worksheetset.BelasteReservesApp.Data.AanpassingBTReserves.Where(b => b.TypeReseve != null && b.TypeReseve.Id == reserve).Sum(b => b.BedragNaPercentage);

                List<Guid> reserves = new List<Guid> { new Guid("f34c7d2b-428d-494d-a29b-f2bd2910a7a9")
                                                        ,new Guid("f1875c70-b0dc-41be-8ada-46c702a954c1")
                                                        ,new Guid("6a716040-5635-4e9d-8161-f5eaaaff5db2") };


                Data.BelasteReserves.OAOthers = worksheetset.BelasteReservesApp.Data.AanpassingBTReserves.Where(b => b.TypeReseve != null && !reserves.Contains(b.TypeReseve.Id)).Sum(b => b.BedragNaPercentage);

                Data.BelasteReserves.OATotal = Data.BelasteReserves.OACapitalGainsOnShares
                                                + Data.BelasteReserves.OADefinitiveExemptionTaxShelter
                                                + Data.BelasteReserves.OAExemptionRegionalPrem
                                                + Data.BelasteReserves.OAOthers;

                recalc = true;
            }
            return recalc;
        }

        #region AddUpdateDelete

        public void UpdateOverzicht(EY.com.eBook.RuleEngine.AY2012.BepalingBelastbareBasisAppTypes.General entity, ref WorksheetSetData worksheetset)
        {
            Data.Overzicht.Manual = true;
            EY.com.eBook.RuleEngine.AY2012.BepalingBelastbareBasisAppTypes.General original = Data.Overzicht;

            entity = Validate(entity, "Overzicht", ref worksheetset);
            if (entity.IsValid)
            {
                entity.Manual = true;
                Data.Overzicht = entity;
                Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
            }
        }

        public void UpdateBelasteReserves(EY.com.eBook.RuleEngine.AY2012.BepalingBelastbareBasisAppTypes.BelasteReservesItem entity, ref WorksheetSetData worksheetset)
        {
            Data.Overzicht.Manual = true;
            EY.com.eBook.RuleEngine.AY2012.BepalingBelastbareBasisAppTypes.BelasteReservesItem original = Data.BelasteReserves;

            entity = Validate(entity, "BelasteReserves", ref worksheetset);
            if (entity.IsValid)
            {

                Data.BelasteReserves = entity;
                Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
            }
        }

        public void UpdateVerworpenUitgaven(EY.com.eBook.RuleEngine.AY2012.BepalingBelastbareBasisAppTypes.VerworpenUitgavenItem entity, ref WorksheetSetData worksheetset)
        {
            Data.Overzicht.Manual = true;
            EY.com.eBook.RuleEngine.AY2012.BepalingBelastbareBasisAppTypes.VerworpenUitgavenItem original = Data.VerworpenUitgaven;

            entity = Validate(entity, "VerworpenUitgaven", ref worksheetset);
            if (entity.IsValid)
            {

                Data.VerworpenUitgaven = entity;
                Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
            }
        }

        public void UpdateBelastbareBestZonderAftrek(EY.com.eBook.RuleEngine.AY2012.BepalingBelastbareBasisAppTypes.BelastbareBestZonderAftrekItem entity, ref WorksheetSetData worksheetset)
        {
            Data.Overzicht.Manual = true;
            EY.com.eBook.RuleEngine.AY2012.BepalingBelastbareBasisAppTypes.BelastbareBestZonderAftrekItem original = Data.BelastbareBestZonderAftrek;

            entity = Validate(entity, "BelastbareBestZonderAftrek", ref worksheetset);
            if (entity.IsValid)
            {

                Data.BelastbareBestZonderAftrek = entity;
                Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
            }
        }

        public void UpdateOpdelingNaarOorsprong(EY.com.eBook.RuleEngine.AY2012.BepalingBelastbareBasisAppTypes.OpdelingNaarOorsprongItem entity, ref WorksheetSetData worksheetset)
        {
            Data.Overzicht.Manual = true;
            EY.com.eBook.RuleEngine.AY2012.BepalingBelastbareBasisAppTypes.OpdelingNaarOorsprongItem original = Data.OpdelingNaarOorsprong;

            entity = Validate(entity, "OpdelingNaarOorsprong", ref worksheetset);
            if (entity.IsValid)
            {

                Data.OpdelingNaarOorsprong = entity;
                Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
            }
        }

        public void UpdateFiscaleAftrek(EY.com.eBook.RuleEngine.AY2012.BepalingBelastbareBasisAppTypes.FiscaleAftrekItem entity, ref WorksheetSetData worksheetset)
        {
            Data.Overzicht.Manual = true;
            EY.com.eBook.RuleEngine.AY2012.BepalingBelastbareBasisAppTypes.FiscaleAftrekItem original = Data.FiscaleAftrek;

            entity = Validate(entity, "FiscaleAftrek", ref worksheetset);
            if (entity.IsValid)
            {

                Data.FiscaleAftrek = entity;
                Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
            }
        }

        #endregion

        #region VALIDATION

        public EY.com.eBook.RuleEngine.AY2012.BepalingBelastbareBasisAppTypes.General Validate(EY.com.eBook.RuleEngine.AY2012.BepalingBelastbareBasisAppTypes.General entity, string location, ref WorksheetSetData worksheetset)
        {
            entity.IsValid = true;
            worksheetset.ClearMessages(TypeId, entity.Id);
            /*
            if (!entity.Bedrag.HasValue || entity.Bedrag.Value==0)
            {
            worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Grootboek");
            entity.IsValid = false;
            }
            */

            return entity;
        }

        public EY.com.eBook.RuleEngine.AY2012.BepalingBelastbareBasisAppTypes.BelasteReservesItem Validate(EY.com.eBook.RuleEngine.AY2012.BepalingBelastbareBasisAppTypes.BelasteReservesItem entity, string location, ref WorksheetSetData worksheetset)
        {
            entity.IsValid = true;
            worksheetset.ClearMessages(TypeId, entity.Id);
            /*
            if (!entity.Bedrag.HasValue || entity.Bedrag.Value==0)
            {
            worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Grootboek");
            entity.IsValid = false;
            }
            */

            return entity;
        }

        public EY.com.eBook.RuleEngine.AY2012.BepalingBelastbareBasisAppTypes.VerworpenUitgavenItem Validate(EY.com.eBook.RuleEngine.AY2012.BepalingBelastbareBasisAppTypes.VerworpenUitgavenItem entity, string location, ref WorksheetSetData worksheetset)
        {
            entity.IsValid = true;
            worksheetset.ClearMessages(TypeId, entity.Id);
            /*
            if (!entity.Bedrag.HasValue || entity.Bedrag.Value==0)
            {
            worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Grootboek");
            entity.IsValid = false;
            }
            */

            return entity;
        }

        public EY.com.eBook.RuleEngine.AY2012.BepalingBelastbareBasisAppTypes.BelastbareBestZonderAftrekItem Validate(EY.com.eBook.RuleEngine.AY2012.BepalingBelastbareBasisAppTypes.BelastbareBestZonderAftrekItem entity, string location, ref WorksheetSetData worksheetset)
        {
            entity.IsValid = true;
            worksheetset.ClearMessages(TypeId, entity.Id);
            /*
            if (!entity.Bedrag.HasValue || entity.Bedrag.Value==0)
            {
            worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Grootboek");
            entity.IsValid = false;
            }
            */

            return entity;
        }

        public EY.com.eBook.RuleEngine.AY2012.BepalingBelastbareBasisAppTypes.OpdelingNaarOorsprongItem Validate(EY.com.eBook.RuleEngine.AY2012.BepalingBelastbareBasisAppTypes.OpdelingNaarOorsprongItem entity, string location, ref WorksheetSetData worksheetset)
        {
            entity.IsValid = true;
            worksheetset.ClearMessages(TypeId, entity.Id);
            /*
            if (!entity.Bedrag.HasValue || entity.Bedrag.Value==0)
            {
            worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Grootboek");
            entity.IsValid = false;
            }
            */

            return entity;
        }

        public EY.com.eBook.RuleEngine.AY2012.BepalingBelastbareBasisAppTypes.FiscaleAftrekItem Validate(EY.com.eBook.RuleEngine.AY2012.BepalingBelastbareBasisAppTypes.FiscaleAftrekItem entity, string location, ref WorksheetSetData worksheetset)
        {
            entity.IsValid = true;
            worksheetset.ClearMessages(TypeId, entity.Id);
            /*
            if (!entity.Bedrag.HasValue || entity.Bedrag.Value==0)
            {
            worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Grootboek");
            entity.IsValid = false;
            }
            */

            return entity;
        }

        #endregion
        // VALIDATION
        
        // CALCULATION
        
        // BOOKINGS
        
        // IMPORT PREVIOUS


    }
}
  
