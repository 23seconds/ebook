﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.Caching;
using EY.com.eBook.Core;
using System.Xml.Linq;
using System.Text.RegularExpressions;
using System.Xml;
using EY.com.eBook.API.Contracts.Data.Cache;
using EY.com.eBook.API.BL;
using EY.com.eBook.Core.Data;
using System.ServiceModel;
using bzt = EY.com.eBook.BizTax;

namespace  EY.com.eBook.RuleEngine.AY2012
{


    public class BizTaxRenderer : bzt.BizTaxRenderer
    {

       

        public bzt.Contracts.BizTaxDataContract UpdateFromWorksheets(bzt.Contracts.BizTaxDataContract btdc, WorksheetSet set)
        {

           
            bzt.XbrlElementDataContract xedc = null;
            bzt.XbrlElementDataContract pxedc = null;
            string xvalue = null;

            string ident = btdc.EntityIdentifier.IdentifierValue;


            #region AangifteVenBApp - Reserves

            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "TaxableReservesCapitalSharePremiums"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.Reserves.AKapitaalBT.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-End"
              ,
                Decimals = "INF"
              ,
                Name = "TaxableReservesCapitalSharePremiums"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.Reserves.AKapitaalET.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "TaxablePortionRevaluationSurpluses"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.Reserves.AHerwaarderingBT.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-End"
              ,
                Decimals = "INF"
              ,
                Name = "TaxablePortionRevaluationSurpluses"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.Reserves.AHerwaarderingET.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "LegalReserve"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.Reserves.AWetReserveBT.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-End"
              ,
                Decimals = "INF"
              ,
                Name = "LegalReserve"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.Reserves.AWetReserveET.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "UnavailableReserves"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.Reserves.AOnbeschikReserveBT.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-End"
              ,
                Decimals = "INF"
              ,
                Name = "UnavailableReserves"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.Reserves.AOnbeschikReserveET.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "AvailableReserves"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.Reserves.ABeschikReserveBT.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-End"
              ,
                Decimals = "INF"
              ,
                Name = "AvailableReserves"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.Reserves.ABeschikReserveET.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "AccumulatedProfitsLosses"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.Reserves.AOvergedragenResWinstVerliesBT.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-End"
              ,
                Decimals = "INF"
              ,
                Name = "AccumulatedProfitsLosses"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.Reserves.AOvergedragenResWinstVerliesET.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "TaxableProvisions"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.Reserves.ABelastbareVoorzieningenBT.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-End"
              ,
                Decimals = "INF"
              ,
                Name = "TaxableProvisions"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.Reserves.ABelastbareVoorzieningenET.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "id__otherreserves"
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "OtherReserves"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.Reserves.AIBalansBT.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            Guid OtherReserves = "OtherReserves".DeterministicGuid();
            btdc.Contexts.RemoveAll(c => c.DefId == OtherReserves);


            string scenid = AddContext(new List<bzt.ContextScenarioDataContract> {
                    new bzt.ContextScenarioTypeDataContract {
                        AutoRendered=true
                        , Dimension = "d-ty:DescriptionTypedDimension"
                        , Type="d-ty:DescriptionTypedID"
                        , Value = set._datacontainer.DataContract.Culture=="nl-BE" ? "Andere reserves" : "Autres réserves"
                    }
                }
                , set._datacontainer.DataContract.StartDate
                , set._datacontainer.DataContract.EndDate
                , OtherReserves
                , "OtherReservesTitle"
                );




            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = scenid
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "OtherReserves"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.Reserves.AIBalansBT.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value; pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = scenid
              ,
                Period = "I-End"
              ,
                Decimals = "INF"
              ,
                Name = "OtherReserves"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.Reserves.AIBalansET.ToXbrlValue()
                 ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value; pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }

            }


            Guid OtherTaxableReserves = "OtherTaxableReserves".DeterministicGuid();
            btdc.Contexts.RemoveAll(c => c.DefId == OtherTaxableReserves);


            scenid = AddContext(new List<bzt.ContextScenarioDataContract> {
                    new bzt.ContextScenarioTypeDataContract {
                        AutoRendered=true
                        , Dimension = "d-ty:DescriptionTypedDimension"
                        , Type="d-ty:DescriptionTypedID"
                        , Value = set._datacontainer.DataContract.Culture=="nl-BE" ? "Andere belastbare reserves" : "Autres réserves imposable"
                    }
                }
                , set._datacontainer.DataContract.StartDate
                , set._datacontainer.DataContract.EndDate
                , OtherTaxableReserves
                 , "OtherTaxableReservesTitle"
                );

            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = scenid
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "OtherTaxableReserves"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.Reserves.AIBReserveBT.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value; pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }

            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = scenid
              ,
                Period = "I-End"
              ,
                Decimals = "INF"
              ,
                Name = "OtherTaxableReserves"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.Reserves.AIBReserveET.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value; pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }



            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "TaxableWriteDownsUndisclosedReserve"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.Reserves.AORBelastbareWVBT.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-End"
              ,
                Decimals = "INF"
              ,
                Name = "TaxableWriteDownsUndisclosedReserve"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.Reserves.AORBelastbareWVET.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "ExaggeratedDepreciationsUndisclosedReserve"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.Reserves.AOROverdrevenAfschrBT.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-End"
              ,
                Decimals = "INF"
              ,
                Name = "ExaggeratedDepreciationsUndisclosedReserve"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.Reserves.AOROverdrevenAfschrET.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "OtherUnderestimationsAssetsUndisclosedReserve"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.Reserves.AORAndereActBT.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-End"
              ,
                Decimals = "INF"
              ,
                Name = "OtherUnderestimationsAssetsUndisclosedReserve"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.Reserves.AORAndereActET.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "OtherOverestimationsLiabilitiesUndisclosedReserve"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.Reserves.AORAnderePassBT.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-End"
              ,
                Decimals = "INF"
              ,
                Name = "OtherOverestimationsLiabilitiesUndisclosedReserve"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.Reserves.AORAnderePassET.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "TaxableReserves"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.Reserves.AORSubTotaalBT.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-End"
              ,
                Decimals = "INF"
              ,
                Name = "TaxableReserves"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.Reserves.AORSubTotaalET.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "CapitalGainsShares"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.Reserves.AAMRBTRMeerwaardeAandelenBT.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "CapitalGainsSharesReversalPreviousExemptionsAdjustmentReservesPlus"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.Reserves.AAMRTerugnemingVUAandelen.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "DefinitiveExemptionTaxShelterAuthorisedAudiovisualWorksAdjustmentReservesPlus"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.Reserves.AAMRTaxShelterBT.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "ExemptionRegionalGrantsCapitalSubsidiesInterestSubsidiesAdjustmentReservesPlus"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.Reserves.AAMRGewestPremiesBT.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "FinalExemptionProfitHomologationReorganizationPlanAmicableSettlement"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.Reserves.AAMRDefVrijstellingWinst.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "OtherAdjustmentsReservesPlus"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.Reserves.AAMRAndereBT.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "AdjustmentsReservesMinus"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.Reserves.AAanpassingenMinBTReservesBT.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "TaxableReservesAfterAdjustments"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.Reserves.ASubTotaalNaAanpassingBT.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "TaxableReservedProfit"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.Reserves.ATotaalET.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            #endregion


            #region AangifteVenBApp - VrijgesteldeReserve

            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "ExemptWriteDownDebtClaim"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VrijgesteldeReserve.BWaardevHandelsvBT.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-End"
              ,
                Decimals = "INF"
              ,
                Name = "ExemptWriteDownDebtClaim"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VrijgesteldeReserve.BWaardevHandelsvET.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "ExemptProvisionRisksExpenses"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VrijgesteldeReserve.BVoorzieningenBT.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-End"
              ,
                Decimals = "INF"
              ,
                Name = "ExemptProvisionRisksExpenses"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VrijgesteldeReserve.BVoorzieningenET.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "UnrealisedExpressedCapitalGainsExemptReserve"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VrijgesteldeReserve.BNietVerwezMeerwBT.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-End"
              ,
                Decimals = "INF"
              ,
                Name = "UnrealisedExpressedCapitalGainsExemptReserve"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VrijgesteldeReserve.BNietVerwezMeerwET.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "CapitalGainsSpecificSecuritiesExemptReserve"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VrijgesteldeReserve.BGespreidVerwezMwBT.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-End"
              ,
                Decimals = "INF"
              ,
                Name = "CapitalGainsSpecificSecuritiesExemptReserve"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VrijgesteldeReserve.BGespreidVerwezMwET.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "OtherRealisedCapitalGainsExemptReserve"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VrijgesteldeReserve.BVerwezMeerwBT.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-End"
              ,
                Decimals = "INF"
              ,
                Name = "OtherRealisedCapitalGainsExemptReserve"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VrijgesteldeReserve.BVerwezMeerwET.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "CapitalGainsCorporateVehiclesExemptReserve"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VrijgesteldeReserve.BBedrijfsVoertuigenBT.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-End"
              ,
                Decimals = "INF"
              ,
                Name = "CapitalGainsCorporateVehiclesExemptReserve"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VrijgesteldeReserve.BBedrijfsVoertuigenET.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "CapitalGainsRiverVesselExemptReserve"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VrijgesteldeReserve.BBinnenschepenBT.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-End"
              ,
                Decimals = "INF"
              ,
                Name = "CapitalGainsRiverVesselExemptReserve"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VrijgesteldeReserve.BBinnenschepenET.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "CapitalGainsSeaVesselExemptReserve"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VrijgesteldeReserve.BZeeschepenBT.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-End"
              ,
                Decimals = "INF"
              ,
                Name = "CapitalGainsSeaVesselExemptReserve"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VrijgesteldeReserve.BZeeschepenET.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "ExemptInvestmentReserve"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VrijgesteldeReserve.BInvestReserveBT.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-End"
              ,
                Decimals = "INF"
              ,
                Name = "ExemptInvestmentReserve"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VrijgesteldeReserve.BInvestReserveET.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "TaxShelterAuthorisedAudiovisualWorkExemptReserve"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VrijgesteldeReserve.BTaxShelterBT.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-End"
              ,
                Decimals = "INF"
              ,
                Name = "TaxShelterAuthorisedAudiovisualWorkExemptReserve"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VrijgesteldeReserve.BTaxShelterET.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "ProfitHomologationReorganizationPlanAmicableSettlementExemptReserve"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VrijgesteldeReserve.BWinstHomologatieBT.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-End"
              ,
                Decimals = "INF"
              ,
                Name = "ProfitHomologationReorganizationPlanAmicableSettlementExemptReserve"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VrijgesteldeReserve.BWinstHomologatieET.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "OtherExemptReserves"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VrijgesteldeReserve.BAndereBT.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-End"
              ,
                Decimals = "INF"
              ,
                Name = "OtherExemptReserves"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VrijgesteldeReserve.BAndereET.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "ExemptReservedProfit"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VrijgesteldeReserve.BTotalenBT.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-End"
              ,
                Decimals = "INF"
              ,
                Name = "ExemptReservedProfit"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VrijgesteldeReserve.BTotalenET.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            #endregion


            #region AangifteVenBApp - VerworpenUitgaven

            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "NonDeductibleTaxes"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VerworpenUitgaven.VUNietAftrekbareBelasting.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "NonDeductibleRegionalTaxesDutiesRetributions"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VerworpenUitgaven.VUGewestBelast.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "NonDeductibleFinesConfiscationsPenaltiesAllKind"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VerworpenUitgaven.VUBoetes.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "NonDeductiblePensionsCapitalEmployerContributionsEmployerPremiums"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VerworpenUitgaven.VUPensioenen.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "NonDeductibleCarExpensesLossValuesCars"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VerworpenUitgaven.VUAutokosten.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "NonDeductibleCarExpensesPartBenefitsAllKind"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VerworpenUitgaven.VUAutokostenVAA.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "NonDeductibleReceptionBusinessGiftsExpenses"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VerworpenUitgaven.VUReceptie.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "NonDeductibleRestaurantExpenses"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VerworpenUitgaven.VURestaurant.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "NonDeductibleNonSpecificProfessionalClothsExpenses"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VerworpenUitgaven.VUKledij.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "ExaggeratedInterests"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VerworpenUitgaven.VUOverdrevenIntresten.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "NonDeductibleParticularPortionInterestsLoans"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VerworpenUitgaven.VUBepaaldeLeningen.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "AbnormalBenevolentAdvantages"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VerworpenUitgaven.VUAbnormaleGoedgVoordelen.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "NonDeductibleSocialAdvantages"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VerworpenUitgaven.VUSocialeVoordelen.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "NonDeductibleAdvantagesLuncheonSportsCultureEcoVouchers"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VerworpenUitgaven.VUMaaltijdSportCultuurEco.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "Liberalities"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VerworpenUitgaven.VULiberaliteiten.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "WriteDownsLossValuesShares"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VerworpenUitgaven.VUAandelen.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "ReversalPreviousExemptions"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VerworpenUitgaven.VUTerugnemingVrijstellingen.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "EmployeeParticipation"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VerworpenUitgaven.VUWerknemerParticipatie.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "IndemnityMissingCoupon"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VerworpenUitgaven.VUCoupon.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "ExpensesTaxShelterAuthorisedAudiovisualWork"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VerworpenUitgaven.VUTaxShelter.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "RegionalPremiumCapitalSubsidiesInterestSubsidies"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VerworpenUitgaven.VUGewestPremies.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "NonDeductiblePaymentsCertainStates"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VerworpenUitgaven.VUBepaaldeStaten.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "OtherDisallowedExpenses"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VerworpenUitgaven.VUAndere.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "DisallowedExpenses"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VerworpenUitgaven.VUTotaal.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            #endregion


            #region AangifteVenBApp - UitgekeerdeDividenden

            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "OrdinaryDividends"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.UitgekeerdeDividenden.UDGewone.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "AcquisitionOwnShares"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.UitgekeerdeDividenden.UDAandelen.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "DeceaseDepartureExclusionPartner"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.UitgekeerdeDividenden.UDOverlijden.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "DistributionCompanyAssets"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.UitgekeerdeDividenden.UDMaatschappelijk.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "TaxableDividendsPaid"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.UitgekeerdeDividenden.UDTotaal.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            #endregion


            #region AangifteVenBApp - UiteenzettingWinst

            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "FiscalResult"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.UiteenzettingWinst.WIResultaat.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "ShippingResultTonnageBased"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.UiteenzettingWinst.WIZeeschip.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "ShippingResultNotTonnageBased"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.UiteenzettingWinst.WIActiviteiten.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "DeductionLimitElementsFiscalResult"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.UiteenzettingWinst.WIBestanddelenAftrekBepTotaal.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "BenevolentAbnormalFinancialAdvantagesBenefitsAllKind"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.UiteenzettingWinst.WIBABAbnormaalGoedg.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "ViolationInvestmentCommitmentIntangibilityConditionInvestmentReserve"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.UiteenzettingWinst.WIBABInvestVerplicht.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "CapitalSubsidiesInterestSubsidiesAgriculturalSupport"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.UiteenzettingWinst.WIBABKapitaal.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "RemainingFiscalResultBeforeOriginDistribution"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.UiteenzettingWinst.WIBResterendResultaat.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "TaxTreatyMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "RemainingFiscalResult"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.UiteenzettingWinst.WVVOResterendResultaatVV.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "NoTaxTreatyMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "RemainingFiscalResult"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.UiteenzettingWinst.WVVOResterendResultaatNVV.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "BelgiumMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "RemainingFiscalResult"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.UiteenzettingWinst.WVVOResterendResultaatBelg.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "NoTaxTreatyMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "MiscellaneousExemptions"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.UiteenzettingWinst.WARWNietBelastbareBestandNVV.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "BelgiumMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "MiscellaneousExemptions"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.UiteenzettingWinst.WARWNietBelastbareBestandBelg.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "NoTaxTreatyMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "PEExemptIncomeMovableAssets"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.UiteenzettingWinst.WARWDefinitiefBelasteInkomstenNVV.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "BelgiumMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "PEExemptIncomeMovableAssets"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.UiteenzettingWinst.WARWDefinitiefBelasteInkomstenBelg.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "NoTaxTreatyMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "DeductionPatentsIncome"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.UiteenzettingWinst.WARWAftrekOctrooiNVV.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "BelgiumMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "DeductionPatentsIncome"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.UiteenzettingWinst.WARWAftrekOctrooiBelg.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "NoTaxTreatyMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "AllowanceCorporateEquity"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.UiteenzettingWinst.WARWAftrekRisicoNVV.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "BelgiumMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "AllowanceCorporateEquity"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.UiteenzettingWinst.WARWAftrekRisicoBelg.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "NoTaxTreatyMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "CompensatedTaxLosses"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.UiteenzettingWinst.WARWVorigeVerliezenNVV.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "BelgiumMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "CompensatedTaxLosses"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.UiteenzettingWinst.WARWVorigeVerliezenBelg.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "BelgiumMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "AllowanceInvestmentDeduction"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.UiteenzettingWinst.WARWInvesteringsAftrekBelg.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "NoTaxTreatyMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "RemainingFiscalProfitCommonRate"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.UiteenzettingWinst.WARWTotaalNVV.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "BelgiumMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "RemainingFiscalProfitCommonRate"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.UiteenzettingWinst.WARWTotaalBelg.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "BasicTaxableAmountCommonRate"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.UiteenzettingWinst.WIBGGewoonTarief.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "RemainingFiscalProfitCommonRate"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.UiteenzettingWinst.WIBGResterendWinst.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "ShippingProfitTonnageBased"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.UiteenzettingWinst.WIBGZeescheepvaarTonnage.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "CapitalGainsSharesRate2500"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.UiteenzettingWinst.WIBGMeerwaardeAandelenBelast.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "BasicTaxableAmountExitTaxRate"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.UiteenzettingWinst.WIBGExitTarief.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "CapitalSubsidiesInterestSubsidiesAgriculturalSupportRate500"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.UiteenzettingWinst.WIBGKapitaalInterestLandbouw.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            #endregion


            #region AangifteVenBApp - AfzonderlijkeAanslagen

            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "UnjustifiedExpensesHiddenSurplusProfitsFinancialAdvantagesBenefitsAllKind"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.AfzonderlijkeAanslagen.AAVoordelenAlleAard.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "SeparateAssessmentTaxedReservesCreditAssociationsInstitutionsOtherAuthorisedInstitutions"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.AfzonderlijkeAanslagen.AABelasteReserves.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "SeparateAssessmentDividendsPaidCreditCorporationTradeEquipmentHousingCorporation"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.AfzonderlijkeAanslagen.AAUitgekeerdeDividenden.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "SpecialAssessmentPre1990FullPartialDistributionCompanyAssetsRate3300"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.AfzonderlijkeAanslagen.AABAMaatschVermogen33.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "SpecialAssessmentPre1990FullPartialDistributionCompanyAssetsRate1650"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.AfzonderlijkeAanslagen.AABAMaatschVermogen165.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "SpecialAssessmentPre1990BenefitsAllKindCompanyLiquidation"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.AfzonderlijkeAanslagen.AABAVoordelenAlleAard.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            #endregion


            #region AangifteVenBApp - Diamant

            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "AdditionalDutiesDiamondTraders"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.Diamant.DIAanvullingErkend.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "RetributionTaxCreditResearchDevelopment"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.Diamant.DIOnderzoekOntwikkeling.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            #endregion


            #region AangifteVenBApp - NietBelastbareBestanddelen

            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "ExemptGifts"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.NietBelastbareBestanddelen.NTEBECharity.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "ExemptionAdditionalPersonnelMiscellaneousExemptions"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.NietBelastbareBestanddelen.NTEBEPersonnel.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "ExemptionAdditionalPersonnelSMEs"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.NietBelastbareBestanddelen.NTEBEPersonnelSME.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "ExemptionTrainingPeriodBonus"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.NietBelastbareBestanddelen.NTEBEInternalshipPremium.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "OtherMiscellaneousExemptions"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.NietBelastbareBestanddelen.NTEBEOthers.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            #endregion


            #region AangifteVenBApp - DBI

            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "BelgianBranchMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "NetBelgianIncomeSharesPEExemptIncomeMovableAssets"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.DBI.DBIAbeNettoBEI.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "ForeignBranchMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "NetBelgianIncomeSharesPEExemptIncomeMovableAssets"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.DBI.DBIAbeNettoBUI.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "BelgianBranchMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "WithholdingTaxBelgianIncomeSharesPEExemptIncomeMovableAssets"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.DBI.DBIAbeRoerendBEI.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "ForeignBranchMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "WithholdingTaxBelgianIncomeSharesPEExemptIncomeMovableAssets"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.DBI.DBIAbeRoerendBUI.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "BelgianBranchMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "NetForeignIncomeSharesPEExemptIncomeMovableAssets"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.DBI.DBIAbuNettoBEI.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "ForeignBranchMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "NetForeignIncomeSharesPEExemptIncomeMovableAssets"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.DBI.DBIAbuNettoBUI.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "BelgianBranchMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "WithholdingTaxForeignIncomeSharesPEExemptIncomeMovableAssets"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.DBI.DBIAbuRoerendBEI.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "ForeignBranchMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "WithholdingTaxForeignIncomeSharesPEExemptIncomeMovableAssets"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.DBI.DBIAbuRoerendBUI.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "BelgianBranchMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "NetOtherBelgianIncomeSharesPEExemptIncomeMovableAssets"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.DBI.DBIAAbeNettoBEI.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "ForeignBranchMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "NetOtherBelgianIncomeSharesPEExemptIncomeMovableAssets"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.DBI.DBIAAbeNettoBUI.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "BelgianBranchMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "WithholdingTaxOtherBelgianIncomeSharesPEExemptIncomeMovableAssets"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.DBI.DBIAAbeRoerendBEI.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "ForeignBranchMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "WithholdingTaxOtherBelgianIncomeSharesPEExemptIncomeMovableAssets"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.DBI.DBIAAbeRoerendBUI.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "BelgianBranchMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "NetOtherForeignIncomeSharesPEExemptIncomeMovableAssets"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.DBI.DBIAAbuNettoBEI.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "ForeignBranchMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "NetOtherForeignIncomeSharesPEExemptIncomeMovableAssets"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.DBI.DBIAAbuNettoBUI.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "BelgianBranchMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "WithholdingTaxOtherForeignIncomeSharesPEExemptIncomeMovableAssets"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.DBI.DBIAAbuRoerendBEI.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "ForeignBranchMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "WithholdingTaxOtherForeignIncomeSharesPEExemptIncomeMovableAssets"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.DBI.DBIAAbuRoerendBUI.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "BelgianBranchMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "OtherExemptIncomeMovableAssets"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.DBI.DBIVrijgestelRoerendBEI.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "ForeignBranchMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "OtherExemptIncomeMovableAssets"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.DBI.DBIVrijgestelRoerendBUI.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "BelgianBranchMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "GrossPEExemptIncomeMovableAssets"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.DBI.DBISubtotaalBEI.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "ForeignBranchMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "GrossPEExemptIncomeMovableAssets"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.DBI.DBISubtotaalBUI.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "BelgianBranchMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "ExpensesSharesPEExemptIncomeMovableAssets"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.DBI.DBIKostenBEI.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "ForeignBranchMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "ExpensesSharesPEExemptIncomeMovableAssets"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.DBI.DBIKostenBUI.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "BelgianBranchMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "NetPEExemptIncomeMovableAssets"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.DBI.DBIVerschilBEI.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "ForeignBranchMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "NetPEExemptIncomeMovableAssets"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.DBI.DBIVerschilBUI.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "BelgianBranchMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "IncomeMergerAcquisitionRetainedSharesSimilarRegulationOtherEUMemberState"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.DBI.DBILidEuBEI.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "ForeignBranchMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "IncomeMergerAcquisitionRetainedSharesSimilarRegulationOtherEUMemberState"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.DBI.DBILidEuBUI.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "BelgianBranchMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "ExemptIncomeMovableAssetsRefinancingLoans"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.DBI.DBIHerfinancBEI.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "ForeignBranchMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "ExemptIncomeMovableAssetsRefinancingLoans"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.DBI.DBIHerfinancBUI.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "BelgianBranchMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "DeductiblePEExemptIncomeMovableAssets"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.DBI.DBITotaalBEI.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "ForeignBranchMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "DeductiblePEExemptIncomeMovableAssets"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.DBI.DBITotaalBUI.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            #endregion


            #region AangifteVenBApp - OverdrachtDBIAftrek

            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "AccumulatedPEExemptIncomeMovableAssets"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.OverdrachtDBIAftrek.ODASaldoOvergedragen.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "CarryOverNextTaxPeriodInsufficientProfitsPEExemptIncomeMovableAssetsCurrentTaxPeriod"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.OverdrachtDBIAftrek.ODABedragHuidigNaarVolgend.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "DeductionPEExemptIncomeMovableAssetsPreviousTaxPeriod"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.OverdrachtDBIAftrek.ODABedragSub1Werkelijk.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "CarryOverNextTaxPeriodPEExemptIncomeMovableAssets"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.OverdrachtDBIAftrek.ODASaldoOverdraagbaarVolgend.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            #endregion


            #region AangifteVenBApp - OverdrachtNID

            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "AccumulatedAllowanceCorporateEquity"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.OverdrachtNID.ONIDSaldoOvergedragen.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "CarryOverNextTaxPeriodInsufficientProfitsAllowanceCorporateEquity"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.OverdrachtNID.ONIDOverdraagbaarVolgend.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            #endregion


            #region AangifteVenBApp - CompenseerbareVerliezen

            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "CompensableTaxLosses"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.CompenseerbareVerliezen.CVSaldoVorigeVerliezen.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "CompensatedTaxLosses"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.CompenseerbareVerliezen.CVGecompenseerdeVerliezen.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "LossCurrentTaxPeriod"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.CompenseerbareVerliezen.CVVerliesBelastbaarTijdperk.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "CarryOverTaxLosses"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.CompenseerbareVerliezen.CVOverTeBrengen.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            #endregion


            #region AangifteVenBApp - TariefBelasting

            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = ""
              ,
                Name = "ExclusionReducedRate"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = ""
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.TariefBelasting.TARUitgeslotenVerminderd.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = ""
              ,
                Name = "CreditCorporationTradeEquipmentHousingCorporationTaxRate"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = ""
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.TariefBelasting.TAROnderworpen.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            #endregion


            #region AangifteVenBApp - VoorafBetalingen

            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = ""
              ,
                Name = "FirstThreeAccountingYearsSmallCompanyCorporationCode"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = ""
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VoorafBetalingen.VBJongVennootschap.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "Prepayments"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VoorafBetalingen.VBTotaal.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "PrepaymentFirstQuarter"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VoorafBetalingen.VBEersteKwartaal.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "PrepaymentSecondQuarter"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VoorafBetalingen.VBTweedeKwartaal.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "PrepaymentThirdQuarter"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VoorafBetalingen.VBDerdeKwartaal.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "PrepaymentFourthQuarter"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VoorafBetalingen.VBVierdeKwartaal.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = ""
              ,
                Name = "PrepaymentReferenceNumberNotEntityIdentifierFirstOccurrence"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = ""
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VoorafBetalingen.VBEersteKwartaalReferentie.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = ""
              ,
                Name = "PrepaymentReferenceNumberNotEntityIdentifierSecondOccurrence"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = ""
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VoorafBetalingen.VBTweedeKwartaalReferentie.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = ""
              ,
                Name = "PrepaymentReferenceNumberNotEntityIdentifierThirdOccurrence"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = ""
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VoorafBetalingen.VBDerdeKwartaalReferentie.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = ""
              ,
                Name = "PrepaymentReferenceNumberNotEntityIdentifierFourthOccurrence"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = ""
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VoorafBetalingen.VBVierdeKwartaalReferentie.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            #endregion


            #region AangifteVenBApp - VerrekenbareVoorheffing

            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "NonRepayableAdvanceLevies"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VerrekenbareVoorheffing.VVNTTotaal.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "NonRepayableFictiousWitholdingTax"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VerrekenbareVoorheffing.VVNTFictieveRoerendeVoorheffing.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "NonRepayableLumpSumForeignTaxes"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VerrekenbareVoorheffing.VVNTForfaitairBuitenlands.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "TaxCreditResearchDevelopment"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VerrekenbareVoorheffing.VVNTBelastingKredietOnderzoekOntw.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "RepayableAdvanceLevies"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VerrekenbareVoorheffing.VVTTotaal.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "RepayableRealFictitiousWithholdingTaxOtherSharesPEExemptIncomeMovableAssetsBelgium"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VerrekenbareVoorheffing.VVTBelgisch.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "RepayableWithholdingTaxPEExemptionLiquidationBonusesBonusesAcquisitionOwnSharesBelgiumForeign"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VerrekenbareVoorheffing.VVTEigenBuitenlands.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "RepayableWithholdingTaxOtherPEForeign"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VerrekenbareVoorheffing.VVTAndereBuitenlands.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "RepayableWithholdingTaxOtherLiquidationBonusesBonusesAcquisitionOwnShares"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VerrekenbareVoorheffing.VVTAndereEigen.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "RepayableWithholdingTaxOtherDividends"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VerrekenbareVoorheffing.VVTAndereRoerendeVoorheffing.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "OtherRepayableWithholdingTaxes"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VerrekenbareVoorheffing.VVTAndere.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "TaxCreditResearchDevelopmentClearablePreviousTaxPeriodsRepayableCurrentAssessmentYear"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VerrekenbareVoorheffing.VBelastingkredietHuiidTijdperk.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            #endregion


            #region AangifteVenBApp - TaxShelter

            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = ""
              ,
                Name = "BelgianCorporationAudiovisualWorksTaxShelterAgreement"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = ""
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.TaxShelter.TASTaxShelter.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            #endregion


            #region AangifteVenBApp - GrootteVennootschap

            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = ""
              ,
                Name = "AssociatedCompanyCorporationCodeCurrentTaxPeriod"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = ""
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.GrootteVennootschap.GVBTVerbonden.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "AnnualWorkForceAverageCorporationCodeCurrentTaxPeriod"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.GrootteVennootschap.GVBTJaargemiddelde.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "AnnualTurnoverExcludingVATCorporationCodeCurrentTaxPeriod"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.GrootteVennootschap.GVBTJaarOmzet.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "BalanceSheetTotalCorporationCodeCurrentTaxPeriod"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.GrootteVennootschap.GVBTBalansTotaal.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            #endregion


            #region DubieuzeDebiteurenApp - List2043


            // CLEANUP EXISTING
            List<string> crefs = btdc.Contexts.Where(c => c.PresContextId == "WriteDownsDebtClaimsSection").Select(c => c.Id).ToList();
            btdc.Elements.RemoveAll(e => crefs.Contains(e.ContextRef));

            btdc.Contexts.RemoveAll(c => c.PresContextId == "WriteDownsDebtClaimsSection");

            foreach (DubieuzeDebiteurenAppTypes.DubieuzeDebiteur2043 entity in set._datacontainer.DubieuzeDebiteurenApp.Data.List2043)
            {

                UpdateGridDubieuzeDebiteurenAppList2043(ref btdc, set, entity);
            }



            #endregion


            #region VoorzieningenApp - List2043
            crefs = btdc.Contexts.Where(c => c.PresContextId == "ProvisionsRisksExpensesSection").Select(c => c.Id).ToList();
            btdc.Elements.RemoveAll(e => crefs.Contains(e.ContextRef));

            btdc.Contexts.RemoveAll(c => c.PresContextId == "ProvisionsRisksExpensesSection");

            foreach (VoorzieningenAppTypes.Voorzieningen2043 entity in set._datacontainer.VoorzieningenApp.Data.List2043)
            {

                UpdateGridVoorzieningenAppList2043(ref btdc, set, entity);
            }



            #endregion


            #region RisicoKapitaalApp - Fiche

            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "Equity"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.RisicoKapitaalApp.Data.Fiche.EigenVermogen.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "DeductionsEquityAllowanceCorporateEquity"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.RisicoKapitaalApp.Data.Fiche.Totaal.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "OwnSharesFiscalValue"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.RisicoKapitaalApp.Data.Fiche.EigenAandelen.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "FinancialFixedAssetsParticipationsOtherShares"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.RisicoKapitaalApp.Data.Fiche.FinancVasteActiva.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "SharesInvestmentCorporations"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.RisicoKapitaalApp.Data.Fiche.AandelenBelegging.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "BranchesCountryTaxTreaty"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.RisicoKapitaalApp.Data.Fiche.InrichtingVerdrag.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "ImmovablePropertyCountryTaxTreaty"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.RisicoKapitaalApp.Data.Fiche.OnroerendVerdrag.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "TangibleFixedAssetsUnreasonableRelatedCosts"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.RisicoKapitaalApp.Data.Fiche.MvaOnredelijk.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "InvestmentsNoPeriodicalIncome"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.RisicoKapitaalApp.Data.Fiche.BestanddelenBelegging.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "ImmovablePropertyUseManager"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.RisicoKapitaalApp.Data.Fiche.OnroerendeBedrijfsleiders.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "UnrealisedExpressedCapitalGains"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.RisicoKapitaalApp.Data.Fiche.UitgedrukteMeerwaarden.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "TaxCreditResearchDevelopmentDeductionEquityAllowanceCorporateEquity"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.RisicoKapitaalApp.Data.Fiche.Belastingkrediet.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "InvestmentGrants"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.RisicoKapitaalApp.Data.Fiche.KapitaalSubsidies.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "ActualisationStockRecognisedDiamondTraders"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.RisicoKapitaalApp.Data.Fiche.Voorraadactualisering.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "BorrowedFundsMainEstablishmentInterestChargedTaxableResultBelgianBranch"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.RisicoKapitaalApp.Data.Fiche.BelgischeInrichting.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "MovementEquityAfterDeductionsAllowanceCorporateEquity"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.RisicoKapitaalApp.Data.Fiche.WijzigingenCumul.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "AllowanceCorporateEquityCurrentTaxPeriod"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.RisicoKapitaalApp.Data.Fiche.RisicoKapitaalBelastbaar.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "DeductibleAllowanceCorporateEquityCurrentAssessmentYear"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.RisicoKapitaalApp.Data.Fiche.Aftrekbaar.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "DeductionAllowanceCorporateEquityCurrentAssessmentYear"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.RisicoKapitaalApp.Data.Fiche.WerkelijkAfgetrokken.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "CarryOverCurrentAssessmentYearInsufficientProfitsAllowanceCorporateEquity"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.RisicoKapitaalApp.Data.Fiche.OverdraagbaarSaldo.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "AllowanceCorporateEquityPreviousAssessmentYears"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.RisicoKapitaalApp.Data.Fiche.SaldoVorigeAJGevormd.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "DeductionAllowanceCorporateEquityPreviousAssessmentYears"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.RisicoKapitaalApp.Data.Fiche.WerkelijkVorigAJ.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "AllowanceCorporateEquity"
              ,
                Locked=true, NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.RisicoKapitaalApp.Data.Fiche.TotaalAftrekAJ.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            #endregion


            

            #region RisicoKapitaalApp - Wijzigingen
            int i = 1;

            crefs = btdc.Contexts.Where(c => c.PresContextId == "ApplicableDurationMovementEquityAllowanceCorporateEquity").Select(c => c.Id).ToList();
            btdc.Elements.RemoveAll(e => crefs.Contains(e.ContextRef));

            btdc.Contexts.RemoveAll(c => c.PresContextId == "ApplicableDurationMovementEquityAllowanceCorporateEquity");


            foreach (RisicoKapitaalAppTypes.WijzigingItem entity in set._datacontainer.RisicoKapitaalApp.Data.Wijzigingen)
            {

                UpdateGridRisicoKapitaalAppWijzigingen(ref btdc, set, entity, i);
                i++;
            }



            #endregion
          

            return btdc;

        }


        #region Worksheet specific BizTaxDataContract

        private void UpdateGridDubieuzeDebiteurenAppList2043(ref bzt.Contracts.BizTaxDataContract btdc, WorksheetSet set, DubieuzeDebiteurenAppTypes.DubieuzeDebiteur2043 entity)
        {

            bzt.XbrlElementDataContract xedc = null;
            bzt.XbrlElementDataContract pxedc = null;
            string xvalue = null;
            List<string> crefs = btdc.Contexts.Where(c => c.DefId == entity.Id).Select(c => c.Id).ToList();

            string ctxid = AddContext(new List<bzt.ContextScenarioDataContract> {
                new bzt.ContextScenarioExplicitDataContract {
                        AutoRendered=true
                        , Dimension = "d-ec:ExemptionCategoryDimension"
                        , Value = entity.ExplicitMember.ToXbrlValue()
                    }    
                ,new bzt.ContextScenarioTypeDataContract {
                        AutoRendered=true
                        , Dimension = "d-ty:WriteDownDebtClaimTypedDimension"
                        , Type="d-ty:WritedownDebtClaimTypedID"
                        , Value =  entity.TypedMember
                    }
                }
                , set._datacontainer.DataContract.StartDate
                , set._datacontainer.DataContract.EndDate
                , entity.Id
                , "WriteDownsDebtClaimsSection"
                );


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ctxid
              ,
                Period = "D"
              ,
                Decimals = ""
              ,
                Name = "IdentityTradeDebtor"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = ""
              ,
                Value = entity.Schuldenaar.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                btdc.Elements.Add(xedc);
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ctxid
              ,
                Period = "D"
              ,
                Decimals = ""
              ,
                Name = "AddressTradeDebtor"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = ""
              ,
                Value = entity.SchuldenaarAdres.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                btdc.Elements.Add(xedc);
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ctxid
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "DebtClaim"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = entity.VorderingStart.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                btdc.Elements.Add(xedc);
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ctxid
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "ExemptWriteDownDebtClaim"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = entity.WaardeverminderingStart.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                btdc.Elements.Add(xedc);
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ctxid
              ,
                Period = "I-End"
              ,
                Decimals = "INF"
              ,
                Name = "DebtClaim"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = entity.VorderingEinde.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                btdc.Elements.Add(xedc);
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ctxid
              ,
                Period = "I-End"
              ,
                Decimals = "INF"
              ,
                Name = "ExemptWriteDownDebtClaim"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = entity.WaardeverminderingEinde.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                btdc.Elements.Add(xedc);
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ctxid
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "DecreaseExemptWriteDownDebtClaimFinalWriteDownCurrentTaxPeriod"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = entity.VerminderingWVVerlies.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                btdc.Elements.Add(xedc);
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ctxid
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "DecreaseExemptWriteDownDebtClaimFullPartialCollectionCurrentTaxPeriod"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = entity.VerminderingWVInning.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                btdc.Elements.Add(xedc);
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ctxid
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "DecreaseExemptWriteDownDebtClaimNewEstimationProbableLoss"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = entity.VerminderingWVHerschat.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                btdc.Elements.Add(xedc);
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ctxid
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "IncreaseExemptWriteDownDebtClaim"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = entity.VermeerderingWV.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                btdc.Elements.Add(xedc);
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ctxid
              ,
                Period = "D"
              ,
                Decimals = ""
              ,
                Name = "JustificationExemptWriteDown"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = ""
              ,
                Value = entity.Commentaar.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                btdc.Elements.Add(xedc);
            }

        }

        private void UpdateGridVoorzieningenAppList2043(ref bzt.Contracts.BizTaxDataContract btdc, WorksheetSet set, VoorzieningenAppTypes.Voorzieningen2043 entity)
        {

            bzt.XbrlElementDataContract xedc = null;
            bzt.XbrlElementDataContract pxedc = null;
            string xvalue = null;
            List<string> crefs = btdc.Contexts.Where(c => c.DefId == entity.Id).Select(c => c.Id).ToList();

            string ctxid = AddContext(new List<bzt.ContextScenarioDataContract> {
                new bzt.ContextScenarioExplicitDataContract {
                        AutoRendered=true
                        , Dimension = "d-ec:ExemptionCategoryDimension"
                        , Value = entity.ExplicitMemberCategory.ToXbrlValue()
                    }  
                , new bzt.ContextScenarioExplicitDataContract {
                        AutoRendered=true
                        , Dimension = "d-expt:ExpenseTypeDimension"
                        , Value = entity.ExplicitMemberKind.ToXbrlValue()
                    }  
                ,new bzt.ContextScenarioTypeDataContract {
                        AutoRendered=true
                        , Dimension = "d-ty:ProvisionRiskExpenseTypedDimension"
                        , Type="d-ty:ProvisionRiskExpenseTypedID"
                        , Value =  entity.TypedMember.Trim()
                    }
                }
                , set._datacontainer.DataContract.StartDate
                , set._datacontainer.DataContract.EndDate
                , entity.Id
                , "ProvisionsRisksExpensesSection"
                );


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ctxid
              ,
                Period = "I-End"
              ,
                Decimals = "INF"
              ,
                Name = "ProbableCost"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = entity.WaarschijnlijkeKosten.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                btdc.Elements.Add(xedc);
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ctxid
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "ExemptProvisionRisksExpenses"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = entity.VoorzieningBeginBj.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                btdc.Elements.Add(xedc);
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ctxid
              ,
                Period = "I-End"
              ,
                Decimals = "INF"
              ,
                Name = "ExemptProvisionRisksExpenses"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = entity.VoorzieningEindeBJ.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                btdc.Elements.Add(xedc);
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ctxid
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "DecreaseExemptProvisionRisksExpensesBorneExpensesCurrentTaxPeriod"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = entity.VerminderingTijdensBJAanwending.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                btdc.Elements.Add(xedc);
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ctxid
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "DecreaseExemptProvisionRisksExpensesNewEstimationProbableExpenses"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = entity.VerminderingTijdensBJNieuweInschatting.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                btdc.Elements.Add(xedc);
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ctxid
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "IncreaseExemptProvisionRisksExpenses"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = entity.VerhogingTijdensBJ.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                btdc.Elements.Add(xedc);
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ctxid
              ,
                Period = "D"
              ,
                Decimals = ""
              ,
                Name = "JustificationExemptProvision"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = ""
              ,
                Value = entity.Commentaar.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                btdc.Elements.Add(xedc);
            }

        }

        private void UpdateGridRisicoKapitaalAppWijzigingen(ref bzt.Contracts.BizTaxDataContract btdc, WorksheetSet set, RisicoKapitaalAppTypes.WijzigingItem entity, int nr)
        {

            bzt.XbrlElementDataContract xedc = null;
            bzt.XbrlElementDataContract pxedc = null;
            string xvalue = null;
            string description = string.Format("{0}-{1}", nr, entity.Notitie);

            List<string> crefs = btdc.Contexts.Where(c => c.DefId == entity.Id).Select(c => c.Id).ToList();

            string ctxid = AddContext(new List<bzt.ContextScenarioDataContract> {
                new bzt.ContextScenarioTypeDataContract {
                        AutoRendered=true
                        , Dimension = "d-ty:DescriptionTypedDimension"
                        , Type="d-ty:DescriptionTypedID"
                        , Value = description.ToXbrlValue()
                    }
                ,new bzt.ContextScenarioTypeDataContract {
                        AutoRendered=true
                        , Dimension = "d-ty:DateTypedDimension"
                        , Type="d-ty:DateTypedID"
                        , Value =  entity.DatumTransactie.ToXbrlValue()
                    }
                }
                , set._datacontainer.DataContract.StartDate
                , set._datacontainer.DataContract.EndDate
                , entity.Id
                , "ApplicableDurationMovementEquityAllowanceCorporateEquity"
                );


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ctxid
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "MovementEquityAfterDeductionsAllowanceCorporateEquity"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = entity.XbrlValue.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                btdc.Elements.Add(xedc);
            }

        }

        #endregion
       


    }
}
