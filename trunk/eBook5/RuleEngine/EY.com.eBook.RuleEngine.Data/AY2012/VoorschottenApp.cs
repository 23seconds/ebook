﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.RuleEngine.Core;
using EY.com.eBook.RuleEngine.Core.Interfaces;
using EY.com.eBook.RuleEngine.Data;
using EY.com.eBook.Core;
using EY.com.eBook.API.Contracts.Data.Cache;

namespace EY.com.eBook.RuleEngine.AY2012
{
    
    public class VoorschottenApp
    {
        public static WorksheetTypeSettings Settings = new WorksheetTypeSettings
                                                      {
                                                        Clients = true
                                                        ,
                                                        Suppliers = true
                                                        ,
                                                        AllAccounts = true
                                                        , 
                                                        PerformsBookings=true
                                                      };
                                                      
        public static Guid TypeId = new Guid("997a80e4-ea39-42c7-95d5-cdf8322c4c11");

        public List<string> ImportablePreviousTypes  = new List<string>() {  "EY.com.eBook.RuleEngine.AY2011.FacturatieApp"  };

        public Guid GetMyType() { return TypeId; }

        public void SetMinimal()
        {
            if (Data == null)
            {
                Data = new EY.com.eBook.RuleEngine.AY2012.VoorschottenAppTypes.Voorschotten();
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.Gegevens == null)
            {
                Data.Gegevens = new List<EY.com.eBook.RuleEngine.AY2012.VoorschottenAppTypes.Gegeven>(); ;
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.InterestVoeten == null)
            {
                Data.InterestVoeten = new List<EY.com.eBook.RuleEngine.AY2012.VoorschottenAppTypes.IntrestVoet>(); ;
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.RentegevendeVoorschotten == null)
            {
                Data.RentegevendeVoorschotten = new List<EY.com.eBook.RuleEngine.AY2012.VoorschottenAppTypes.RentegevendVoorschot>(); ;
                Data.LastUpdated = DateTime.Now;
            }

        }
        
        public string ToXml() 
        {
            return CoreHelper.SerializeToString(Data);
        }

        public void LoadXml(string xml)
        {
            if (!string.IsNullOrEmpty(xml)) {
                Data = CoreHelper.DeserializeFromString<EY.com.eBook.RuleEngine.AY2012.VoorschottenAppTypes.Voorschotten>(xml);
            }
            SetMinimal();
            
        }
        
        public EY.com.eBook.RuleEngine.AY2012.VoorschottenAppTypes.Voorschotten Data { get; set; }


        public void ForceRecalculation(ref WorksheetSetData worksheetset)
        {
            Data.LastUpdated = new DateTime(1980, 1, 1);
           

            Calculate(ref worksheetset);
        }

        public void Calculate(ref WorksheetSetData worksheetset)
        {
            worksheetset.ClearMessages(TypeId);
            Guid actiefLI = new Guid("04f800fa-0e3c-4f53-aa2b-181de8743297");
            Guid passiefLI = new Guid("706d0798-cb1c-45dc-8744-e12dc1063d59");

            FileDataContract fdc = worksheetset.DataContract;
            List<AccountDataContract> accounts = worksheetset.GetAccountsOfMapping("RentegevendeVoorschotten", false, true, true, true, true, worksheetset.DataContract.Culture);
            List<string> accountNrs = accounts.Select(a => a.InternalNr).ToList();
            List<Guid> deletes = Data.Gegevens.Where(g =>
                                                        (g.ActiefRekening != null
                                                            && !accountNrs.Contains(g.ActiefRekening.Id))
                                                        || (g.PassiefRekening != null
                                                            && !accountNrs.Contains(g.PassiefRekening.Id))
                                                        || (g.Rekening65 != null
                                                            && !accountNrs.Contains(g.Rekening65.Id))
                                                        || (g.Rekening75 != null
                                                            && !accountNrs.Contains(g.Rekening75.Id))
                                                    ).Select(g => g.Id).ToList();

            // Cleanup of removed mappings
            Data.RentegevendeVoorschotten.RemoveAll(rv => deletes.Contains(new Guid(rv.Voorschot.Id)));
            Data.InterestVoeten.RemoveAll(rv => deletes.Contains(new Guid(rv.Voorschot.Id)));
            Data.Gegevens.RemoveAll(rv => deletes.Contains(rv.Id));


            VoorschottenAppTypes.IntrestVoet prev =null;
            foreach (VoorschottenAppTypes.IntrestVoet iv in Data.InterestVoeten.OrderBy(d => d.Voorschot.NL).ThenBy(d => d.EndDate))
            {
                if (prev == null)
                {
                    iv.StartDate = worksheetset.DataContract.StartDate;
                }
                else
                {
                    if (prev.Voorschot.Id == iv.Voorschot.Id)
                    {
                        iv.StartDate = prev.EndDate.Value.AddDays(1);
                    }
                }
            }

            // calculate existing
            foreach (EY.com.eBook.RuleEngine.AY2012.VoorschottenAppTypes.Gegeven gegeven in
                Data.Gegevens)
            {
                List<EY.com.eBook.RuleEngine.AY2012.VoorschottenAppTypes.IntrestVoet> intrestvoeten = Data.InterestVoeten
                    .Where(i => i.Voorschot.Id == gegeven.Id.ToString() && i.StartDate.HasValue).OrderBy(i => i.StartDate.Value).ToList();
                List<EY.com.eBook.RuleEngine.AY2012.VoorschottenAppTypes.RentegevendVoorschot> rvs = Data.RentegevendeVoorschotten
                    .Where(r => r.Voorschot.Id == gegeven.Id.ToString()).OrderBy(r => r.EndDate.Value).ToList();

                if (intrestvoeten.Count > 0)
                {
                    AccountDataContract rvaccount;
                    AccountDataContract account67;
                    bool reorder = false;

                    if (gegeven.Soort.Id == actiefLI)
                    {
                        rvaccount = accounts.FirstOrDefault(a => a.InternalNr == gegeven.ActiefRekening.Id);
                        account67 = accounts.FirstOrDefault(a => a.InternalNr == gegeven.Rekening75.Id);
                    }
                    else
                    {
                        rvaccount = accounts.FirstOrDefault(a => a.InternalNr == gegeven.PassiefRekening.Id);
                        account67 = accounts.FirstOrDefault(a => a.InternalNr == gegeven.Rekening65.Id);
                    }
                    if (rvs.Count == 0)
                    {
                        reorder = true;
                        RuleAddDefaultRVs(gegeven, rvaccount, true, true, ref worksheetset);
                    }
                    else
                    {
                        if (rvs.Where(r => r.AutoKey == "BT").Count() == 0)
                        {
                            reorder = true;
                            RuleAddDefaultRVs(gegeven, rvaccount, true, false, ref worksheetset);
                        }
                        if (rvs.Where(r => r.AutoKey == "ET").Count() == 0)
                        {
                            reorder = true;
                            RuleAddDefaultRVs(gegeven, rvaccount, false, true, ref worksheetset);
                        }
                        
                    }
                    // perform indexing
                    EY.com.eBook.RuleEngine.AY2012.VoorschottenAppTypes.RentegevendVoorschot previous = null;
                    EY.com.eBook.RuleEngine.AY2012.VoorschottenAppTypes.IntrestVoet intrest = intrestvoeten[0];
                    int intrestIdx = 0;



                    foreach (EY.com.eBook.RuleEngine.AY2012.VoorschottenAppTypes.IntrestVoet iv in intrestvoeten.Where(i => i.EndDate.Value < fdc.EndDate))
                    {
                        if (rvs.Where(r => r.EndDate.Value == iv.EndDate.Value).Count() == 0)
                        {
                            reorder = true;
                            RuleAddIndexRVs(iv, rvaccount, ref worksheetset);
                        }
                    }

                    // reorder
                    if (reorder)
                    {
                        rvs = Data.RentegevendeVoorschotten
                            .Where(r => r.Voorschot.Id == gegeven.Id.ToString()).OrderBy(r => r.EndDate.Value).ToList();
                    }
                    VoorschottenAppTypes.RentegevendVoorschot rBT = rvs.First(r => r.AutoKey == "BT");
                    VoorschottenAppTypes.RentegevendVoorschot rET = rvs.First(r => r.AutoKey == "ET");
                    int lastMovementIdx = rvs.IndexOf(rET) - 1;
                    AccountDataContract adcEt = accounts.FirstOrDefault(a => rET.Grootboekrekening!=null && a.InternalNr == rET.Grootboekrekening.Id);
                    if (adcEt != null) rET.Saldo = adcEt.Saldo;
                    rET.Fluctuatie = rET.Saldo - rvs[lastMovementIdx].Saldo;
                    // Aanpassing berekening
                    // Steeds per einde dag => BVB
                    // 01/01/2011  Saldo 1000
                    // 01/06/2011  Saldo 1500
                    // 31/12/2011 Saldo 2000
                    // WORDT DAN:
                    // 01/01/2011 Saldo 1000 Intrest n.v.t.
                    // 31/05/2011 Saldo 1000 Intrest 1000 (vanaf 1/1 tot en met 31/5)
                    // 01/06/2011 Saldo 1500 Intrest n.v.t
                    // 30/12/2011 Saldo 1500 Intrest 1500 (vanaf 1/6 tot en met 30/12)
                    // 31/12/2011 Saldo 2000 Intrest 2000 (1 dag: 31/12)
                    //


                    decimal saldo = rBT.Saldo.GetValueOrDefault();
                    foreach (EY.com.eBook.RuleEngine.AY2012.VoorschottenAppTypes.RentegevendVoorschot rv in rvs)
                    {
                        if (intrest.EndDate.Value < rv.EndDate.Value)
                        {
                            intrestIdx++;
                            intrest = intrestvoeten[intrestIdx];
                        }

                        

                        rv.Saldo = saldo;

                        rv.InterestPercentage = rv.Saldo.GetValueOrDefault() >= 0 ? intrest.DebetIntrest.GetValueOrDefault() : intrest.CreditIntrest.GetValueOrDefault();
                        rv.N = 0;
                        if (previous != null)
                        {
                            rv.N = Math.Abs((rv.EndDate.Value - previous.EndDate.Value).Days);
                            if (rv.EndDate == worksheetset.DataContract.EndDate) rv.N = rv.N + 1;
                        }
                        rv.Interest = Math.Round((rv.Saldo.GetValueOrDefault() / 365) * (rv.InterestPercentage.GetValueOrDefault() / 100) * rv.N.GetValueOrDefault(), 2, MidpointRounding.AwayFromZero);

                        if (rv.Saldo < 0)
                        {
                            rv.debetRente = 0;
                            rv.creditRente = rv.Interest;
                        }
                        else
                        {
                            rv.creditRente = 0;
                            rv.debetRente = rv.Interest;
                        }
                        if (rv.SoortRV==null || rv.SoortRV.Id == new Guid("DEDE879F-AA23-430F-8B12-9D051F53FF54")) // list item
                        {
                            rv.RoerendeVoorheffing = 0;
                        }
                        else
                        {
                            rv.RoerendeVoorheffing = Math.Round(rv.Interest.GetValueOrDefault() * (decimal)0.15, 2, MidpointRounding.AwayFromZero);
                        }

                        if (saldo < 0 && Math.Abs(saldo) > Data.Treshold.GetValueOrDefault() && gegeven.SubjectToTreshold.GetValueOrDefault())
                        {
                            gegeven.Herkwalificatie = true;
                        }
                        saldo += rv.Fluctuatie.GetValueOrDefault();
                        rv.Saldo = saldo;
                        previous = rv;
                    }
                    gegeven.Interest = rvs.Sum(r => r.Interest.GetValueOrDefault());
                }
                else
                {
                    // spwan message
                }

            }
            Data.RentegevendeVoorschotten = Data.RentegevendeVoorschotten.Where(r =>r.EndDate.HasValue && r.Voorschot != null).OrderBy(r => r.Voorschot.Id).ThenBy(r => r.EndDate.Value).ToList();
            Data.LastUpdated = DateTime.Now;
        }

        private void RuleAddIndexRVs(EY.com.eBook.RuleEngine.AY2012.VoorschottenAppTypes.IntrestVoet intrest, AccountDataContract account, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2012.VoorschottenAppTypes.RentegevendVoorschot rvIdx = new EY.com.eBook.RuleEngine.AY2012.VoorschottenAppTypes.RentegevendVoorschot
            {
                Id = Guid.NewGuid()
                ,
                Omschrijving = "INDEX CHANGE"
                ,
                Grootboekrekening = account.GetListItem()
                ,
                EndDate = intrest.EndDate
                ,
                Fluctuatie = 0
                ,
                AutoLine = true
                ,
                Voorschot = intrest.Voorschot
            };
            Validate(rvIdx, "RentegevendeVoorschotten", ref worksheetset);
            Data.RentegevendeVoorschotten.Add(rvIdx);
        }

        private void RuleAddDefaultRVs(EY.com.eBook.RuleEngine.AY2012.VoorschottenAppTypes.Gegeven gegeven, AccountDataContract account, bool BT, bool ET, ref WorksheetSetData worksheetset)
        {
            FileDataContract fdc = worksheetset.DataContract;
            EY.com.eBook.RuleEngine.AY2012.VoorschottenAppTypes.RentegevendVoorschot rvBT;
            EY.com.eBook.RuleEngine.AY2012.VoorschottenAppTypes.RentegevendVoorschot rvET;

            if (BT)
            {
                rvBT = new EY.com.eBook.RuleEngine.AY2012.VoorschottenAppTypes.RentegevendVoorschot
                {
                    Id = Guid.NewGuid()
                    ,
                    Omschrijving = worksheetset.GetTranslation(TypeId, "Voorschotten", "BeginBoekjaar", "label")
                    ,
                    Grootboekrekening = account.GetListItem()
                    ,
                    EndDate = fdc.StartDate
                    ,
                    AutoKey = "BT"
                    ,
                    Fluctuatie = 0
                    ,
                    AutoLine = true
                    ,
                    Saldo = account.PreviousSaldo
                    ,
                    Voorschot = new StringListItemDataContract { Id = gegeven.Id.ToString(), NL = gegeven.ListDescription, FR = gegeven.ListDescription, EN = gegeven.ListDescription }

                };
                Validate(rvBT, "", ref worksheetset);
                Data.RentegevendeVoorschotten.Add(rvBT);
            }

            if (ET)
            {
                rvET = new EY.com.eBook.RuleEngine.AY2012.VoorschottenAppTypes.RentegevendVoorschot
                {
                    Id = Guid.NewGuid()
                    ,
                    Omschrijving = worksheetset.GetTranslation(TypeId, "Voorschotten", "EindeBoekjaar", "label")
                    ,
                    Grootboekrekening = account.GetListItem()
                    ,
                    EndDate = fdc.EndDate
                    ,
                    AutoKey = "ET"
                    ,
                    Fluctuatie = 0
                    ,
                    AutoLine = true
                    ,
                    Saldo = account.Saldo
                    ,
                    Voorschot = new StringListItemDataContract { Id = gegeven.Id.ToString(), NL = gegeven.ListDescription, FR = gegeven.ListDescription, EN = gegeven.ListDescription }

                };
                Validate(rvET, "", ref worksheetset);
                Data.RentegevendeVoorschotten.Add(rvET);
            }


        }


        #region AddUpdateDelete

        public void AddToGegevens(EY.com.eBook.RuleEngine.AY2012.VoorschottenAppTypes.Gegeven entity, ref WorksheetSetData worksheetset)
        {
            entity = Validate(entity, "Gegevens", ref worksheetset);
            if (entity.IsValid)
            {
                Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
                Data.Gegevens.Add(entity);
            }
        }

        public void DeleteGegevens(Guid rowId, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2012.VoorschottenAppTypes.Gegeven original = Data.Gegevens.FirstOrDefault(e => e.Id == rowId);
            if (original != null)
            {
                /*
                if (original.Boeken.HasValue && original.Boeken.Value)
                {
                    worksheetset.DeleteBooking(FacturatieApp.TypeId, "OpTeMakenCN", original.Id, null);
                    // DELETE BOOKING
                }*/
                Data.Gegevens.Remove(original);
                Data.LastUpdated = DateTime.Now;
            }
        }

        public void UpdateGegevens(EY.com.eBook.RuleEngine.AY2012.VoorschottenAppTypes.Gegeven entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2012.VoorschottenAppTypes.Gegeven original = Data.Gegevens.FirstOrDefault(e => e.Id == entity.Id);
            if (original == null)
            {
                AddToGegevens(entity, ref worksheetset);
            }
            else
            {
                entity = Validate(entity, "Gegevens", ref worksheetset);
                if (entity.IsValid)
                {
                    
                    int idx = Data.Gegevens.IndexOf(original);
                    Data.Gegevens.RemoveAt(idx);
                    Data.Gegevens.Insert(idx, entity);
                    Data.LastUpdated = DateTime.Now;
                    entity.LastUpdated = DateTime.Now;
                }
            }
        }

        public void AddToInterestVoeten(EY.com.eBook.RuleEngine.AY2012.VoorschottenAppTypes.IntrestVoet entity, ref WorksheetSetData worksheetset)
        {
            entity = Validate(entity, "InterestVoeten", ref worksheetset);
            if (entity.IsValid)
            {
                Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
                Data.InterestVoeten.Add(entity);
            }
        }

        public void DeleteInterestVoeten(Guid rowId, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2012.VoorschottenAppTypes.IntrestVoet original = Data.InterestVoeten.FirstOrDefault(e => e.Id == rowId);
            if (original != null)
            {
                /*
                if (original.Boeken.HasValue && original.Boeken.Value)
                {
                    worksheetset.DeleteBooking(FacturatieApp.TypeId, "OpTeMakenCN", original.Id, null);
                    // DELETE BOOKING
                }*/
                Data.InterestVoeten.Remove(original);
                Data.LastUpdated = DateTime.Now;
            }
        }

        public void UpdateInterestVoeten(EY.com.eBook.RuleEngine.AY2012.VoorschottenAppTypes.IntrestVoet entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2012.VoorschottenAppTypes.IntrestVoet original = Data.InterestVoeten.FirstOrDefault(e => e.Id == entity.Id);
            if (original == null)
            {
                AddToInterestVoeten(entity, ref worksheetset);
            }
            else
            {
                entity = Validate(entity, "InterestVoeten", ref worksheetset);
                if (entity.IsValid)
                {
                    int idx = Data.InterestVoeten.IndexOf(original);
                    Data.InterestVoeten.RemoveAt(idx);
                    Data.InterestVoeten.Insert(idx, entity);
                    Data.LastUpdated = DateTime.Now;
                    entity.LastUpdated = DateTime.Now;
                }
            }
        }

        public void AddToRentegevendeVoorschotten(EY.com.eBook.RuleEngine.AY2012.VoorschottenAppTypes.RentegevendVoorschot entity, ref WorksheetSetData worksheetset)
        {
            entity = Validate(entity, "RentegevendeVoorschotten", ref worksheetset);
            if (entity.IsValid)
            {
                Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
                Data.RentegevendeVoorschotten.Add(entity);
            }
        }

        public void DeleteRentegevendeVoorschotten(Guid rowId, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2012.VoorschottenAppTypes.RentegevendVoorschot original = Data.RentegevendeVoorschotten.FirstOrDefault(e => e.Id == rowId);
            if (original != null)
            {
                /*
                if (original.Boeken.HasValue && original.Boeken.Value)
                {
                    worksheetset.DeleteBooking(FacturatieApp.TypeId, "OpTeMakenCN", original.Id, null);
                    // DELETE BOOKING
                }*/
                Data.RentegevendeVoorschotten.Remove(original);
                Data.LastUpdated = DateTime.Now;
            }
        }

        public void UpdateRentegevendeVoorschotten(EY.com.eBook.RuleEngine.AY2012.VoorschottenAppTypes.RentegevendVoorschot entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2012.VoorschottenAppTypes.RentegevendVoorschot original = Data.RentegevendeVoorschotten.FirstOrDefault(e => e.Id == entity.Id);
            if (original == null)
            {
                AddToRentegevendeVoorschotten(entity, ref worksheetset);
            }
            else
            {
                entity = Validate(entity, "RentegevendeVoorschotten", ref worksheetset);
                if (entity.IsValid)
                {
                    int idx = Data.RentegevendeVoorschotten.IndexOf(original);
                    Data.RentegevendeVoorschotten.RemoveAt(idx);
                    Data.RentegevendeVoorschotten.Insert(idx, entity);
                    Data.LastUpdated = DateTime.Now;
                    entity.LastUpdated = DateTime.Now;
                }
            }
        }

        #endregion


        #region VALIDATION

        public EY.com.eBook.RuleEngine.AY2012.VoorschottenAppTypes.Gegeven Validate(EY.com.eBook.RuleEngine.AY2012.VoorschottenAppTypes.Gegeven entity, string location, ref WorksheetSetData worksheetset)
        {
            entity.IsValid = true;
            worksheetset.ClearMessages(TypeId, entity.Id);

            if (entity.Soort == null)
            {
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Soort");
                entity.IsValid = false;
            }
            else
            {
                if (entity.Soort.Id == new Guid("04f800fa-0e3c-4f53-aa2b-181de8743297"))
                {
                    if (entity.ActiefRekening == null)
                    {
                        worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "ActiefRekening");
                        entity.IsValid = false;
                    }
                    if (entity.Rekening75 == null)
                    {
                        worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Rekening75");
                        entity.IsValid = false;
                    }
                }
                else
                {
                    if (entity.PassiefRekening == null)
                    {
                        worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "PassiefRekening");
                        entity.IsValid = false;
                    }
                    if (entity.Rekening65 == null)
                    {
                        worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Rekening65");
                        entity.IsValid = false;
                    }
                }
                if (entity.Type == null)
                {
                    worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Type");
                    entity.IsValid = false;
                }
            }
            
            
            

            if (entity.IsValid)
            {
                StringListItemDataContract slidc = entity.ActiefRekening;
                if (entity.Soort.Id != new Guid("04f800fa-0e3c-4f53-aa2b-181de8743297"))
                {
                    slidc = entity.PassiefRekening;
                }
                switch (worksheetset.DataContract.Culture)
                {
                    case "fr-FR":
                        entity.ListDescription = slidc.FR;
                        break;
                    case "en-US":
                        entity.ListDescription = slidc.EN;
                        break;
                    default:
                        entity.ListDescription = slidc.NL;
                        break;
                }
            }
            return entity;
        }

        public EY.com.eBook.RuleEngine.AY2012.VoorschottenAppTypes.IntrestVoet Validate(EY.com.eBook.RuleEngine.AY2012.VoorschottenAppTypes.IntrestVoet entity, string location, ref WorksheetSetData worksheetset)
        {
            entity.IsValid = true;
            worksheetset.ClearMessages(TypeId, entity.Id);
            if (entity.Voorschot == null)
            {
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Voorschot");
                entity.IsValid = false;
            }
            if (!entity.EndDate.HasValue)
            {
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "EndDate");
                entity.IsValid = false;
            }
            if (!entity.CreditIntrest.HasValue)
            {
                entity.CreditIntrest = 0;
            }
            if (!entity.DebetIntrest.HasValue)
            {
                entity.DebetIntrest = 0;
            }
            /*
            if (!entity.Bedrag.HasValue || entity.Bedrag.Value==0)
            {
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Grootboek");
                entity.IsValid = false;
            }
            */

            return entity;
        }

        public EY.com.eBook.RuleEngine.AY2012.VoorschottenAppTypes.RentegevendVoorschot Validate(EY.com.eBook.RuleEngine.AY2012.VoorschottenAppTypes.RentegevendVoorschot entity, string location, ref WorksheetSetData worksheetset)
        {
            entity.IsValid = true;
            worksheetset.ClearMessages(TypeId, entity.Id);
            /*
            if (!entity.Bedrag.HasValue || entity.Bedrag.Value==0)
            {
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Grootboek");
                entity.IsValid = false;
            }
            */
            if (entity.debet.HasValue || entity.credit.HasValue)
            {
                entity.Fluctuatie = entity.debet.GetValueOrDefault() - entity.credit.GetValueOrDefault();
            }

            return entity;
        }

        #endregion
        // CALCULATION
        
        // BOOKINGS
        
        // IMPORT PREVIOUS
        
    }
}
  