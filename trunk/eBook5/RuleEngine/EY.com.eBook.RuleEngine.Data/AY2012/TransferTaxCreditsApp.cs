﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.RuleEngine.Core;
using EY.com.eBook.RuleEngine.Core.Interfaces;
using EY.com.eBook.RuleEngine.Data;
using EY.com.eBook.Core;

namespace EY.com.eBook.RuleEngine.AY2012
{
    
    public class TransferTaxCreditsApp
    {
        public static WorksheetTypeSettings Settings = new WorksheetTypeSettings
                                                      {
                                                        Clients = true
                                                        ,
                                                        Suppliers = true
                                                        ,
                                                        AllAccounts = true
                                                        , 
                                                        PerformsBookings=true
                                                      };
                                                      
        public static Guid TypeId = new Guid("34b1fab3-1d1a-44d0-87e7-93fb703ef3a2");

        public List<string> ImportablePreviousTypes  = new List<string>() {  "EY.com.eBook.RuleEngine.AY2011.FacturatieApp"  };

        public Guid GetMyType() { return TypeId; }

        public void SetMinimal()
        {
            if (Data == null)
            {
                Data = new EY.com.eBook.RuleEngine.AY2012.TransferTaxCreditsAppTypes.TransferTaxCredits();
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.FiscaleVerliezen == null)
            {
                Data.FiscaleVerliezen = new List<EY.com.eBook.RuleEngine.AY2012.TransferTaxCreditsAppTypes.FiscaalVerlies>(); ;
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.NID == null)
            {
                Data.NID = new List<EY.com.eBook.RuleEngine.AY2012.TransferTaxCreditsAppTypes.NIDItem>(); ;
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.DBI == null)
            {
                Data.DBI = new EY.com.eBook.RuleEngine.AY2012.TransferTaxCreditsAppTypes.DBIItem
                {
                    Id = Guid.NewGuid()
                    ,
                    LastUpdated = null
                    ,
                    IsValid = true
                    ,
                    PreviousImported = false

                };
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.Invest == null)
            {
                Data.Invest = new List<EY.com.eBook.RuleEngine.AY2012.TransferTaxCreditsAppTypes.InvestItem>(); ;
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.Belastingkrediet == null)
            {
                Data.Belastingkrediet = new List<EY.com.eBook.RuleEngine.AY2012.TransferTaxCreditsAppTypes.BelastingkredietItem>(); ;
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.Aanpassingen == null)
            {
                Data.Aanpassingen = new List<EY.com.eBook.RuleEngine.AY2012.TransferTaxCreditsAppTypes.AanpassingItem>(); ;
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.Aanwendingen == null)
            {
                Data.Aanwendingen = new EY.com.eBook.RuleEngine.AY2012.TransferTaxCreditsAppTypes.Aanwending
                {
                    Id = Guid.NewGuid()
                    ,
                    LastUpdated = null
                    ,
                    IsValid = true
                    ,
                    PreviousImported = false

                };
                Data.LastUpdated = new DateTime(1980,1,1);
            } 

        }
        
        public string ToXml() 
        {
            return CoreHelper.SerializeToString(Data);
        }

        public void LoadXml(string xml)
        {
            if (!string.IsNullOrEmpty(xml)) {
                Data = CoreHelper.DeserializeFromString<EY.com.eBook.RuleEngine.AY2012.TransferTaxCreditsAppTypes.TransferTaxCredits>(xml);
            }
            SetMinimal();
            
        }
        
        public EY.com.eBook.RuleEngine.AY2012.TransferTaxCreditsAppTypes.TransferTaxCredits Data { get; set; }


        public void ForceRecalculation(ref WorksheetSetData worksheetset)
        {
            Data.LastUpdated = new DateTime(1980, 1, 1);

            for (int i = 0; i < Data.FiscaleVerliezen.Count;i++ )
            {
                Data.FiscaleVerliezen[i] = Validate(Data.FiscaleVerliezen[i], "FiscaleVerliezen", ref worksheetset);
            }

            

            Calculate(ref worksheetset);
        }

        public void Calculate(ref WorksheetSetData worksheetset)
        {
            worksheetset.ClearMessages(TypeId);
            SetupFiscaalVerlies(ref worksheetset);
            SetupInvest(ref worksheetset);
            
            // import belastbare basis
            Data.Aanwendingen.BepBelGereserveerdWinst = worksheetset.BepalingBelastbareBasisApp.Data.Overzicht.BasisVoorFiscaleAftrek.GetValueOrDefault();

            // import DBI
            Data.DBI.SaldoStart = worksheetset.DBIApp.Data.DBI.SaldoStart.GetValueOrDefault();
            Data.DBI.SaldoEinde = worksheetset.DBIApp.Data.DBI.SaldoEinde.GetValueOrDefault();
            Data.DBI.ToevoegingHuidigBJ = worksheetset.DBIApp.Data.DBI.Nieuwe.GetValueOrDefault();
            Data.DBI.Aanwending = worksheetset.DBIApp.Data.DBI.Aanwending.GetValueOrDefault();
            Data.Aanwendingen.AanwendingDBI = Data.DBI.Aanwending.GetValueOrDefault();

            Data.Aanwendingen.ResterendVoorAanwending = worksheetset.RisicoKapitaalApp.Data.Fiche.BelastbareBasisResterend.GetValueOrDefault();

            // import NID (aftrek risicokapitaal)
            DateTime dt = DateTime.Now;
            foreach (RisicoKapitaalAppTypes.HistoryItem history in worksheetset.RisicoKapitaalApp.Data.Historiek)
            {
                TransferTaxCreditsAppTypes.NIDItem nid = Data.NID.FirstOrDefault(n => n.Id == history.Id);
                if (nid == null)
                {
                    nid = new EY.com.eBook.RuleEngine.AY2012.TransferTaxCreditsAppTypes.NIDItem
                    {
                        Id = history.Id
                        ,
                        IsValid = true
                        ,
                        LastUpdated = dt
                        ,
                        PreviousImported = false
                    };
                    Data.NID.Add(nid);
                }
                nid.Aanslagjaar = int.Parse(history.AssessmentYear.Id);
                nid.Aanwending = history.GebruiktHuidigBJ.GetValueOrDefault();
                nid.BedragBeginBJ = history.BedragAftrek.GetValueOrDefault();
                nid.BedragEindeBJ = history.Saldo.GetValueOrDefault();
                nid.LastUpdated = dt;
            }
            Data.NID.RemoveAll(r => !r.LastUpdated.HasValue || r.LastUpdated.Value != dt);

            Data.Aanwendingen.AanwendingNID = Data.NID.Sum(n => n.Aanwending.GetValueOrDefault());


            // fiscaal verlies

            Data.FiscaleVerliezen = Data.FiscaleVerliezen.OrderBy(f => f.Order.Value).ThenBy(f=>f.Date.Value).ToList();

            Data.Aanwendingen.AanwendingVerlies = 0;
            decimal saldo = 0;

            foreach (TransferTaxCreditsAppTypes.FiscaalVerlies verlies in Data.FiscaleVerliezen)
            {
                switch (verlies.Order.Value)
                {
                    case 0:
                    case 1:
                        saldo = saldo + verlies.BedragHuidig.GetValueOrDefault();
                        if (saldo < 0)
                        {
                            worksheetset.SetMessage(TypeId, MessageTypeDataContract.Warning, "BELOWZERO", "FiscaleVerliezen", verlies.Id, "Cumul", new object[] { Math.Round(saldo, 2, MidpointRounding.AwayFromZero).ToString() });
                            saldo = 0;
                        }
                        verlies.Cumul = saldo;
                        break;
                    case 2:
                        verlies.Cumul = saldo;
                        TransferTaxCreditsAppTypes.FiscaalVerlies comp = Data.FiscaleVerliezen.FirstOrDefault(f => f.Type == "compenseerdverlieshuidig");
                        if (comp != null)
                        {
                            comp.Cumul = 0;
                        }
                        Data.Aanwendingen.ResterendVoorAanwending = Data.Aanwendingen.ResterendVoorAanwending.GetValueOrDefault();
                        if (Data.Aanwendingen.ResterendVoorAanwending > 0)
                        {
                            if (saldo > 0)
                            {
                                decimal toAttend = saldo;
                                if (toAttend > Data.Aanwendingen.ResterendVoorAanwending.GetValueOrDefault())
                                {
                                    toAttend = Data.Aanwendingen.ResterendVoorAanwending.GetValueOrDefault();
                                }
                                Data.Aanwendingen.AanwendingVerlies = toAttend;
                                if (comp != null) comp.Cumul = toAttend;
                                Data.Aanwendingen.ResterendVoorAanwending = Data.Aanwendingen.ResterendVoorAanwending.GetValueOrDefault() - toAttend;
                            }
                        }
                        break;
                    case 3:
                        verlies.Cumul = 0;
                        if (Data.Aanwendingen.BepBelGereserveerdWinst.GetValueOrDefault() < 0)
                        {
                            verlies.Cumul = Math.Abs(Data.Aanwendingen.BepBelGereserveerdWinst.GetValueOrDefault());
                        }
                        saldo = saldo + verlies.Cumul.GetValueOrDefault();
                        break;
                    case 4:
                        saldo = saldo - (verlies.Cumul.GetValueOrDefault());
                        break;
                    case 5:
                        verlies.Cumul = saldo;
                        break;
                }
            }

            
            // Omzet belastingkrediet

            if (Data.Invest.Count(i => i.Type!=null && i.Type.Id.ToString().ToLower() == "be5ebc07-0327-497c-a456-dcad58cdfc78") > 0)
            {
                TransferTaxCreditsAppTypes.InvestItem invest = Data.Invest.FirstOrDefault(f => f.Type != null && f.Type.Id.ToString().ToLower() == "be5ebc07-0327-497c-a456-dcad58cdfc78");
                if (invest != null)
                {
                    string ass = worksheetset.DataContract.AssessmentYear.ToString();
                    if (!invest.OnderzoekOntwikkeling.HasValue) invest.OnderzoekOntwikkeling = 0;
                    decimal amount = Math.Round((invest.OnderzoekOntwikkeling.GetValueOrDefault() * (decimal)0.3399), 2, MidpointRounding.AwayFromZero);
                    TransferTaxCreditsAppTypes.BelastingkredietItem belkred = Data.Belastingkrediet.FirstOrDefault(b => b.Disabled == true);
                    if (belkred == null)
                    {
                        belkred = new EY.com.eBook.RuleEngine.AY2012.TransferTaxCreditsAppTypes.BelastingkredietItem
                        {
                            Id = Guid.NewGuid()
                            ,
                            Saldo = 0
                            ,
                            Disabled = true
                            ,
                            Aanslagjaar = new StringListItemDataContract
                            {
                                Id = ass
                                ,
                                NL = ass
                                ,
                                EN = ass
                                ,
                                FR = ass
                            }
                        };
                        Data.Belastingkrediet.Add(belkred);
                    }
                    belkred.BedragAftrek = amount;
                    belkred.GebruiktHuidigBJ = amount;
                    worksheetset.SetMessage(TypeId, MessageTypeDataContract.Warning, "OMZETTING", "Belastingkrediet", belkred.Id ,string.Empty);
                }
            }



            // invest

            Data.Invest = Data.Invest.OrderBy(f => f.Order.Value).ThenBy(f => f.Date.Value).ToList();

            
            decimal toAttendInvest = 0;
            decimal resterend = Data.Aanwendingen.ResterendVoorAanwending.GetValueOrDefault();

            decimal saldoGewone = 0;
            decimal saldoOntwikkeling = 0;
            decimal saldoTotaal = 0;

            foreach (TransferTaxCreditsAppTypes.InvestItem investitem in Data.Invest)
            {
                investitem.Totaal = investitem.Gewone.GetValueOrDefault() + investitem.OnderzoekOntwikkeling.GetValueOrDefault();
                    if (!investitem.Totaal.HasValue) investitem.Totaal = 0;
                
                switch (investitem.Order.Value)
                {
                    case 0:
                    case 1:
                        saldoGewone = saldoGewone + (investitem.Gewone.GetValueOrDefault());
                        saldoOntwikkeling = saldoOntwikkeling + (investitem.OnderzoekOntwikkeling.GetValueOrDefault());
                        saldoTotaal = saldoTotaal + investitem.Totaal.GetValueOrDefault();
                        break;
                    case 2:
                        investitem.Gewone = saldoGewone;
                        investitem.OnderzoekOntwikkeling = saldoOntwikkeling;
                        investitem.Totaal = saldoTotaal;
                        toAttendInvest = saldoTotaal;

                        if (toAttendInvest > 858330)
                        {
                            if (((double)saldoTotaal * 0.25) > 858330)
                            {
                                toAttendInvest = saldoTotaal * (decimal)0.25;
                            }
                            else
                            {
                                toAttendInvest = 858330;
                            }
                        }
                        break;
                    case 3:
                        saldoGewone = saldoGewone + (investitem.Gewone.GetValueOrDefault());
                        saldoOntwikkeling = saldoOntwikkeling + (investitem.OnderzoekOntwikkeling.GetValueOrDefault());
                        saldoTotaal = saldoTotaal + investitem.Totaal.GetValueOrDefault();
                        toAttendInvest = toAttendInvest + investitem.Totaal.GetValueOrDefault();
                        if (saldoTotaal > 0 && resterend > 0)
                        {
                            if (toAttendInvest > resterend)
                            {
                                toAttendInvest = resterend;
                            }
                        }
                        else
                        {
                            toAttendInvest = 0;
                        }
                        Data.Aanwendingen.AanwendingInvest = toAttendInvest;
                        break;
                    case 4:
                        saldoGewone = saldoGewone - (investitem.Gewone.GetValueOrDefault());
                        saldoOntwikkeling = saldoOntwikkeling - (investitem.OnderzoekOntwikkeling.GetValueOrDefault());
                        investitem.Totaal = toAttendInvest;
                        saldoTotaal = saldoTotaal - investitem.Totaal.GetValueOrDefault();
                        break;
                    case 5:
                        saldoOntwikkeling = saldoOntwikkeling - (investitem.OnderzoekOntwikkeling.GetValueOrDefault());
                        break;
                    case 6:
                        investitem.Gewone = saldoGewone;
                        investitem.OnderzoekOntwikkeling = saldoOntwikkeling;
                        investitem.Totaal = saldoTotaal;
                        break;
                }
            }

            Data.LastUpdated = DateTime.Now;
        }

        private void SetupFiscaalVerlies(ref WorksheetSetData worksheetset)
        {
            
            TransferTaxCreditsAppTypes.FiscaalVerlies fv;
            fv = Data.FiscaleVerliezen.FirstOrDefault(e => e.Type == "startverlies");
            if (fv == null)
            {
                fv = new EY.com.eBook.RuleEngine.AY2012.TransferTaxCreditsAppTypes.FiscaalVerlies
                {
                    Id = Guid.NewGuid()
                    ,
                    BedragHuidig = 0
                    ,
                    BedragHuidigForm = 0
                    ,
                    Date = worksheetset.DataContract.StartDate.AddYears(-7)
                    ,
                    Order = 0
                    ,
                    Disabled = false
                    ,
                    Omschrijving = worksheetset.GetTranslation(TypeId, "FiscaleVerliezen", "startverlies") + " "+ worksheetset.DataContract.StartDate.AddYears(-7).ToString("dd/MM/yyyy")
                    ,
                    Cumul = null
                    ,
                    Type = "startverlies"
                    ,
                    IsValid = true
                    ,
                    LastUpdated = DateTime.Now
                    ,
                    PreviousImported = false
                };
                Data.FiscaleVerliezen.Add(fv);
            }

            fv = Data.FiscaleVerliezen.FirstOrDefault(e => e.Type == "saldostart");
            if (fv == null)
            {
                fv = new EY.com.eBook.RuleEngine.AY2012.TransferTaxCreditsAppTypes.FiscaalVerlies
                {
                    Id = Guid.NewGuid()
                    ,
                    Date = worksheetset.DataContract.StartDate
                    ,
                    Order = 2
                    ,
                    Disabled = true
                    ,
                    Omschrijving = worksheetset.GetTranslation(TypeId, "FiscaleVerliezen", "saldostart") + " " + worksheetset.DataContract.StartDate.ToString("dd/MM/yyyy")
                    ,
                    Cumul = null
                    ,
                    Type = "saldostart"
                    ,
                    IsValid = true
                    ,
                    LastUpdated = DateTime.Now
                    ,
                    PreviousImported = false
                };
                Data.FiscaleVerliezen.Add(fv);
            }

            fv = Data.FiscaleVerliezen.FirstOrDefault(e => e.Type == "verlieshuidig");
            if (fv == null)
            {
                fv = new EY.com.eBook.RuleEngine.AY2012.TransferTaxCreditsAppTypes.FiscaalVerlies
                {
                    Id = Guid.NewGuid()
                    ,
                    Date = worksheetset.DataContract.EndDate
                    ,
                    Order = 3
                    ,
                    Disabled = false
                    ,
                    Omschrijving = worksheetset.GetTranslation(TypeId, "FiscaleVerliezen", "verlieshuidig")
                    ,
                    Cumul = null
                    ,
                    Type = "verlieshuidig"
                    ,
                    IsValid = true
                    ,
                    LastUpdated = DateTime.Now
                    ,
                    PreviousImported = false
                };
                Data.FiscaleVerliezen.Add(fv);
            }

            fv = Data.FiscaleVerliezen.FirstOrDefault(e => e.Type == "compenseerdverlieshuidig");
            if (fv == null)
            {
                fv = new EY.com.eBook.RuleEngine.AY2012.TransferTaxCreditsAppTypes.FiscaalVerlies
                {
                    Id = Guid.NewGuid()
                    ,
                    Date = worksheetset.DataContract.EndDate
                    ,
                    Order = 4
                    ,
                    Disabled = true
                    ,
                    Omschrijving = worksheetset.GetTranslation(TypeId, "FiscaleVerliezen", "compenseerdverlieshuidig")
                    ,
                    Cumul = null
                    ,
                    Type = "compenseerdverlieshuidig"
                    ,
                    IsValid = true
                    ,
                    LastUpdated = DateTime.Now
                    ,
                    PreviousImported = false
                };
                Data.FiscaleVerliezen.Add(fv);
            }

            fv = Data.FiscaleVerliezen.FirstOrDefault(e => e.Type == "eindeverlies");
            if (fv == null)
            {
                fv = new EY.com.eBook.RuleEngine.AY2012.TransferTaxCreditsAppTypes.FiscaalVerlies
                {
                    Id = Guid.NewGuid()
                    ,
                    Date = worksheetset.DataContract.EndDate
                    ,
                    Order = 5
                    ,
                    Disabled = true
                    ,
                    Omschrijving = worksheetset.GetTranslation(TypeId, "FiscaleVerliezen", "eindeverlies") + " " + worksheetset.DataContract.EndDate.ToString("dd/MM/yyyy")
                    ,
                    Cumul = null
                    ,
                    Type = "eindeverlies"
                    ,
                    IsValid = true
                    ,
                    LastUpdated = DateTime.Now
                    ,
                    PreviousImported = false
                };
                Data.FiscaleVerliezen.Add(fv);
            }
        }

        private void SetupInvest(ref WorksheetSetData worksheetset)
        {
            TransferTaxCreditsAppTypes.InvestItem fv;
            fv = Data.Invest.FirstOrDefault(e => e.AutoKey == "saldoinvest");
            if (fv == null)
            {
                fv = new EY.com.eBook.RuleEngine.AY2012.TransferTaxCreditsAppTypes.InvestItem
                {
                    Id = Guid.NewGuid()
                    ,
                    Date = worksheetset.DataContract.StartDate.AddYears(-7)
                    ,
                    Order = 0
                    ,
                    Disabled = false
                    ,
                    Omschrijving = worksheetset.GetTranslation(TypeId, "Invest", "saldoinvest") + " " + worksheetset.DataContract.StartDate.AddYears(-7).ToString("dd/MM/yyyy")

                   , AutoKey ="saldoinvest"
                    ,
                    IsValid = true
                    , Gewone= 0
                    , OnderzoekOntwikkeling =0
                    ,
                    LastUpdated = DateTime.Now
                    ,
                    PreviousImported = false
                };
                Data.Invest.Add(fv);
            }

            fv = Data.Invest.FirstOrDefault(e => e.AutoKey == "saldostart");
            if (fv == null)
            {
                fv = new EY.com.eBook.RuleEngine.AY2012.TransferTaxCreditsAppTypes.InvestItem
                {
                    Id = Guid.NewGuid()
                    ,
                    Date = worksheetset.DataContract.StartDate
                    ,
                    Order = 2
                    ,
                    Disabled = true
                    ,
                    Omschrijving = worksheetset.GetTranslation(TypeId, "Invest", "saldostart") + " " + worksheetset.DataContract.StartDate.ToString("dd/MM/yyyy")
                    ,
                    Gewone = 0
                   ,
                    AutoKey = "saldostart"
                    ,
                    IsValid = true
                    ,
                    LastUpdated = DateTime.Now
                    ,
                    PreviousImported = false
                };
                Data.Invest.Add(fv);
            }

            fv = Data.Invest.FirstOrDefault(e => e.AutoKey == "investhuidig");
            if (fv == null)
            {
                fv = new EY.com.eBook.RuleEngine.AY2012.TransferTaxCreditsAppTypes.InvestItem
                {
                    Id = Guid.NewGuid()
                    ,
                    Date = worksheetset.DataContract.EndDate
                    ,
                    Order = 3
                    ,
                    Disabled = false
                    ,
                    Omschrijving = worksheetset.GetTranslation(TypeId, "Invest", "investhuidig")
                    ,
                    Gewone = 0
                    , OnderzoekOntwikkeling=0
                   ,
                    AutoKey = "investhuidig"
                    ,
                    IsValid = true
                    ,
                    LastUpdated = DateTime.Now
                    ,
                    PreviousImported = false
                };
                Data.Invest.Add(fv);
            }

            fv = Data.Invest.FirstOrDefault(e => e.AutoKey == "aanwendinginvest");
            if (fv == null)
            {
                fv = new EY.com.eBook.RuleEngine.AY2012.TransferTaxCreditsAppTypes.InvestItem
                {
                    Id = Guid.NewGuid()
                    ,
                    Date = worksheetset.DataContract.EndDate
                    ,
                    Order = 4
                    ,
                    Disabled = true
                    ,
                    Omschrijving = worksheetset.GetTranslation(TypeId, "Invest", "aanwendinginvest")
                    ,
                    Gewone = 0
                    ,
                    OnderzoekOntwikkeling = 0
                   ,
                    AutoKey = "aanwendinginvest"
                    ,
                    IsValid = true
                    ,
                    LastUpdated = DateTime.Now
                    ,
                    PreviousImported = false
                };
                Data.Invest.Add(fv);
            }

            fv = Data.Invest.FirstOrDefault(e => e.AutoKey == "investeinde");
            if (fv == null)
            {
                fv = new EY.com.eBook.RuleEngine.AY2012.TransferTaxCreditsAppTypes.InvestItem
                {
                    Id = Guid.NewGuid()
                    ,
                    Date = worksheetset.DataContract.EndDate
                    ,
                    Order = 6
                    ,
                    Disabled = true
                    ,
                    Omschrijving = worksheetset.GetTranslation(TypeId, "Invest", "investeinde") + " " + worksheetset.DataContract.EndDate.ToString("dd/MM/yyyy")
                    ,
                    Gewone = 0
                    ,
                    OnderzoekOntwikkeling = 0
                   ,
                    AutoKey = "investeinde"
                    ,
                    IsValid = true
                    ,
                    LastUpdated = DateTime.Now
                    ,
                    PreviousImported = false
                };
                Data.Invest.Add(fv);
            }
        }


        #region AddUpdateDelete

        public void AddToFiscaleVerliezen(EY.com.eBook.RuleEngine.AY2012.TransferTaxCreditsAppTypes.FiscaalVerlies entity, ref WorksheetSetData worksheetset)
        {
            entity = Validate(entity, "FiscaleVerliezen", ref worksheetset);
            if (entity.IsValid)
            {
                SetupFiscaalVerlies(ref worksheetset);
                entity.Type = "fiscaalverlies";
                entity.BedragHuidig = entity.BedragHuidigForm;
                string trans = worksheetset.GetTranslation(TypeId, "FiscaleVerliezen", "fiscaalverlies");
                entity.Omschrijving = trans + entity.Date.Value.ToString("dd/MM/yyyy");


                Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
                Data.FiscaleVerliezen.Add(entity);
            }
        }

        public void DeleteFiscaleVerliezen(Guid rowId, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2012.TransferTaxCreditsAppTypes.FiscaalVerlies original = Data.FiscaleVerliezen.FirstOrDefault(e => e.Id == rowId);
            if (original != null)
            {
                if (string.IsNullOrEmpty(original.Type) || original.Type == "fiscaalverlies")
                {
                    /*
                    if (original.Boeken.HasValue && original.Boeken.Value)
                    {
                        worksheetset.DeleteBooking(FacturatieApp.TypeId, "OpTeMakenCN", original.Id, null);
                        // DELETE BOOKING
                    }*/
                    Data.FiscaleVerliezen.Remove(original);
                    Data.LastUpdated = DateTime.Now;
                }
            }
        }

        public void UpdateFiscaleVerliezen(EY.com.eBook.RuleEngine.AY2012.TransferTaxCreditsAppTypes.FiscaalVerlies entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2012.TransferTaxCreditsAppTypes.FiscaalVerlies original = Data.FiscaleVerliezen.FirstOrDefault(e => e.Id == entity.Id);
            if (original == null)
            {
                AddToFiscaleVerliezen(entity, ref worksheetset);
            }
            else
            {
                entity = Validate(entity, "FiscaleVerliezen", ref worksheetset);
                if (entity.IsValid)
                {
                    
                    SetupFiscaalVerlies(ref worksheetset);
                    if (string.IsNullOrEmpty(entity.Type) || entity.Type == "fiscaalverlies")
                    {
                        entity.Type = "fiscaalverlies";
                        entity.BedragHuidig = entity.BedragHuidigForm;
                        string trans = worksheetset.GetTranslation(TypeId, "FiscaleVerliezen", "fiscaalverlies");
                        entity.Omschrijving = trans + entity.Date.Value.ToString("dd/MM/yyyy");
                    }
                    if (entity.Type == "startverlies" && entity.BedragHuidig != entity.BedragHuidigForm)
                    {
                        entity.BedragHuidig = entity.BedragHuidigForm;
                    }
                    int idx = Data.FiscaleVerliezen.IndexOf(original);
                    Data.FiscaleVerliezen.RemoveAt(idx);
                    Data.FiscaleVerliezen.Insert(idx, entity);
                    Data.LastUpdated = DateTime.Now;
                    entity.LastUpdated = DateTime.Now;
                }
            }
        }

        public void UpdateDBI(EY.com.eBook.RuleEngine.AY2012.TransferTaxCreditsAppTypes.DBIItem entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2012.TransferTaxCreditsAppTypes.DBIItem original = Data.DBI;

            entity = Validate(entity, "DBI", ref worksheetset);
            if (entity.IsValid)
            {

                Data.DBI = entity;
                Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
            }
        }


        public void AddToInvest(EY.com.eBook.RuleEngine.AY2012.TransferTaxCreditsAppTypes.InvestItem entity, ref WorksheetSetData worksheetset)
        {
            entity = Validate(entity, "Invest", ref worksheetset);
            if (entity.IsValid)
            {
                SetupInvest(ref worksheetset);
                

                Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
                Data.Invest.Add(entity);
            }
        }

        public void DeleteInvest(Guid rowId, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2012.TransferTaxCreditsAppTypes.InvestItem original = Data.Invest.FirstOrDefault(e => e.Id == rowId);
            if (original != null)
            {
                /*
                if (original.Boeken.HasValue && original.Boeken.Value)
                {
                    worksheetset.DeleteBooking(FacturatieApp.TypeId, "OpTeMakenCN", original.Id, null);
                    // DELETE BOOKING
                }*/
                Data.Invest.Remove(original);
                Data.LastUpdated = DateTime.Now;
            }
        }

        public void UpdateInvest(EY.com.eBook.RuleEngine.AY2012.TransferTaxCreditsAppTypes.InvestItem entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2012.TransferTaxCreditsAppTypes.InvestItem original = Data.Invest.FirstOrDefault(e => e.Id == entity.Id);
            if (original == null)
            {
                AddToInvest(entity, ref worksheetset);
            }
            else
            {
                entity = Validate(entity, "Invest", ref worksheetset);
                if (entity.IsValid)
                {
                    
                   
                    SetupInvest(ref worksheetset);

                    int idx = Data.Invest.IndexOf(original);
                    Data.Invest.RemoveAt(idx);
                    Data.Invest.Insert(idx, entity);
                    Data.LastUpdated = DateTime.Now;
                    entity.LastUpdated = DateTime.Now;
                }
            }
        }

        public void AddToBelastingkrediet(EY.com.eBook.RuleEngine.AY2012.TransferTaxCreditsAppTypes.BelastingkredietItem entity, ref WorksheetSetData worksheetset)
        {
            entity = Validate(entity, "Belastingkrediet", ref worksheetset);
            if (entity.IsValid)
            {
                Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
                Data.Belastingkrediet.Add(entity);
            }
        }

        public void DeleteBelastingkrediet(Guid rowId, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2012.TransferTaxCreditsAppTypes.BelastingkredietItem original = Data.Belastingkrediet.FirstOrDefault(e => e.Id == rowId);
            if (original != null)
            {
                /*
                if (original.Boeken.HasValue && original.Boeken.Value)
                {
                    worksheetset.DeleteBooking(FacturatieApp.TypeId, "OpTeMakenCN", original.Id, null);
                    // DELETE BOOKING
                }*/
                Data.Belastingkrediet.Remove(original);
                Data.LastUpdated = DateTime.Now;
            }
        }

        public void UpdateBelastingkrediet(EY.com.eBook.RuleEngine.AY2012.TransferTaxCreditsAppTypes.BelastingkredietItem entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2012.TransferTaxCreditsAppTypes.BelastingkredietItem original = Data.Belastingkrediet.FirstOrDefault(e => e.Id == entity.Id);
            if (original == null)
            {
                AddToBelastingkrediet(entity, ref worksheetset);
            }
            else
            {
                entity = Validate(entity, "Belastingkrediet", ref worksheetset);
                if (entity.IsValid)
                {
                    int idx = Data.Belastingkrediet.IndexOf(original);
                    Data.Belastingkrediet.RemoveAt(idx);
                    Data.Belastingkrediet.Insert(idx, entity);
                    Data.LastUpdated = DateTime.Now;
                    entity.LastUpdated = DateTime.Now;
                }
            }
        }

        private TransferTaxCreditsAppTypes.AanpassingItem PerformAanpassing(TransferTaxCreditsAppTypes.AanpassingItem entity, ref WorksheetSetData worksheetset, bool delete)
        {
            Guid itemid = Guid.NewGuid();
            switch (entity.AanTePassenTabblad.Id.ToString().ToLower())
            {
                case "a8b1a0e1-394a-48fa-baae-5d9dc0c9ad91": // Compenseerbare verliezen
                    itemid = new Guid(entity.AanTePassenVerlies.Id);
                    TransferTaxCreditsAppTypes.FiscaalVerlies fv = Data.FiscaleVerliezen.FirstOrDefault(f => f.Id == itemid);
                    if (fv != null && entity.BedragHuidig.HasValue)
                    {
                        if (entity.Reden.Id == new Guid("fb1a62c0-e4e8-4f4a-bef1-62276145f58d"))
                        {
                            if (delete)
                            {
                                fv.BedragHuidig = fv.BedragHuidigForm;
                            }
                            else
                            {
                                fv.BedragHuidig = fv.BedragHuidigForm - entity.BedragHuidig;
                            }

                            TransferTaxCreditsAppTypes.FiscaalVerlies history = null;
                            if (!string.IsNullOrEmpty(entity.HistoryID))
                            {
                                history = Data.FiscaleVerliezen.FirstOrDefault(f => f.Id == new Guid(entity.HistoryID));
                                if (delete)
                                {
                                    Data.FiscaleVerliezen.Remove(history);
                                }
                            }
                            if (history == null && !delete)
                            {
                                history = new EY.com.eBook.RuleEngine.AY2012.TransferTaxCreditsAppTypes.FiscaalVerlies
                                {
                                    Id = Guid.NewGuid()
                                    ,
                                    BedragHuidig = entity.BedragHuidig
                                    ,
                                    BedragHuidigForm = entity.BedragHuidig
                                    ,
                                    Date = fv.Date
                                    ,
                                    Disabled = true
                                    ,
                                    Omschrijving = "ADJ - " + fv.Omschrijving
                                    ,
                                    Order = fv.Order
                                    ,
                                    Type = fv.Type
                                    ,
                                    LastUpdated = DateTime.Now
                                };
                                Data.FiscaleVerliezen.Add(history);
                                entity.HistoryID = history.Id.ToString();
                            }
                            if (history != null && !delete)
                            {
                                history.BedragHuidig = entity.BedragHuidig;
                                history.Omschrijving = "ADJ - " + fv.Omschrijving;
                                history.LastUpdated = DateTime.Now;
                            }
                            if (history != null && delete)
                            {
                                Data.FiscaleVerliezen.Remove(history);
                            }
                            
                        }
                        else
                        {
                            if (delete)
                            {
                                fv.BedragHuidig = fv.BedragHuidigForm;
                            }
                            else
                            {
                                fv.BedragHuidig = entity.BedragHuidig;
                            }
                        }
                    }
                    break;
                case "9fde2590-8b73-449d-a4fa-7fac78e51c14": // Investeringsaftrek
                    itemid = new Guid(entity.AanTePassenInvest.Id);
                    TransferTaxCreditsAppTypes.InvestItem iv = Data.Invest.FirstOrDefault(f => f.Id == itemid);
                    if (iv != null)
                    {
                        if (entity.Reden.Id == new Guid("fb1a62c0-e4e8-4f4a-bef1-62276145f58d"))
                        {
                            if (delete)
                            {
                                iv.Gewone = iv.GewoneForm;
                                iv.OnderzoekOntwikkeling = iv.OnderzoekOntwikkelingForm;
                            }
                            else
                            {
                                iv.Gewone = iv.GewoneForm - entity.Gewone;
                                iv.OnderzoekOntwikkeling = iv.OnderzoekOntwikkelingForm - entity.OnderzoekOntwikkeling;
                            }

                            TransferTaxCreditsAppTypes.InvestItem ivhistory = null;
                            if (!string.IsNullOrEmpty(entity.HistoryID))
                            {
                                ivhistory = Data.Invest.FirstOrDefault(f => f.Id == new Guid(entity.HistoryID));
                                if (delete)
                                {
                                    Data.Invest.Remove(ivhistory);
                                }
                            }
                            if (ivhistory == null && !delete)
                            {
                                ivhistory = new EY.com.eBook.RuleEngine.AY2012.TransferTaxCreditsAppTypes.InvestItem
                                {
                                    Id = Guid.NewGuid()
                                    ,
                                    GewoneForm = entity.Gewone
                                    ,
                                    OnderzoekOntwikkelingForm = entity.OnderzoekOntwikkeling
                                    ,
                                    Date = iv.Date
                                    ,
                                    Disabled = true
                                    ,
                                    Omschrijving = "ADJ - " + iv.Omschrijving
                                    ,
                                    Order = iv.Order
                                    ,
                                    AutoKey = iv.AutoKey
                                    ,
                                    LastUpdated = DateTime.Now
                                };
                                Data.Invest.Add(ivhistory);
                                entity.HistoryID = ivhistory.Id.ToString();
                            }
                            if (ivhistory != null && !delete)
                            {
                                ivhistory.Gewone = entity.Gewone;
                                ivhistory.OnderzoekOntwikkeling = entity.OnderzoekOntwikkeling;
                                ivhistory.Omschrijving = "ADJ - " + iv.Omschrijving;
                                ivhistory.LastUpdated = DateTime.Now;
                            }
                            if (ivhistory != null && delete)
                            {
                                Data.Invest.Remove(ivhistory);
                            }
                            

                        }
                        else
                        {
                            if (delete)
                            {
                                iv.Gewone = iv.GewoneForm;
                                iv.OnderzoekOntwikkeling = iv.OnderzoekOntwikkelingForm;
                            }
                            else
                            {
                                iv.Gewone = entity.Gewone;
                                iv.OnderzoekOntwikkeling = entity.OnderzoekOntwikkeling;
                            }
                        }
                    }
                    break;
                case "7fb98bef-aa89-468b-a152-109fb4114069": // Belastingkrediet onderzoek/ontwikkeling
                    itemid = new Guid(entity.AanTePassenBelastingKrediet.Id);
                    TransferTaxCreditsAppTypes.BelastingkredietItem bv = Data.Belastingkrediet.FirstOrDefault(f => f.Id == itemid);
                    if (bv != null && entity.BedragAftrek.HasValue)
                    {
                        if (entity.Reden.Id == new Guid("fb1a62c0-e4e8-4f4a-bef1-62276145f58d"))
                        {
                            if (delete)
                            {
                                bv.BedragAftrek = bv.BedragAftrekImported;
                            }
                            else
                            {
                                bv.BedragAftrek = bv.BedragAftrekImported - entity.BedragAftrek;
                            }

                            TransferTaxCreditsAppTypes.BelastingkredietItem bhistory = null;
                            if (!string.IsNullOrEmpty(entity.HistoryID))
                            {
                                bhistory = Data.Belastingkrediet.FirstOrDefault(f => f.Id == new Guid(entity.HistoryID));
                                if (delete)
                                {
                                    Data.Belastingkrediet.Remove(bhistory);
                                }
                            }
                            if (bhistory == null && !delete)
                            {
                                bhistory = new EY.com.eBook.RuleEngine.AY2012.TransferTaxCreditsAppTypes.BelastingkredietItem
                                {
                                    Id = Guid.NewGuid()
                                    ,
                                    BedragAftrek = entity.BedragAftrek
                                    ,
                                    BedragAftrekImported = entity.BedragAftrek
                                    ,
                                    Disabled = true
                                    ,
                                    Aanslagjaar= bv.Aanslagjaar
                                    ,
                                    LastUpdated = DateTime.Now
                                };
                                Data.Belastingkrediet.Add(bhistory);
                                entity.HistoryID = bhistory.Id.ToString();
                            }
                            if (bhistory != null && !delete)
                            {
                                bhistory.BedragAftrek = entity.BedragAftrek;
                                bhistory.LastUpdated = DateTime.Now;
                            }
                            if (bhistory != null && delete)
                            {
                                Data.Belastingkrediet.Remove(bhistory);
                            }

                        }
                        else
                        {
                            if (delete)
                            {
                                bv.BedragAftrek = bv.BedragAftrekImported;
                            }
                            else
                            {
                                bv.BedragAftrek = entity.BedragAftrek;
                            }
                        }
                    }
                    break;
            }
            return entity;
        }

        public void AddToAanpassingen(EY.com.eBook.RuleEngine.AY2012.TransferTaxCreditsAppTypes.AanpassingItem entity, ref WorksheetSetData worksheetset)
        {
            entity = Validate(entity, "Aanpassingen", ref worksheetset);
            if (entity.IsValid)
            {
                entity = PerformAanpassing(entity, ref worksheetset,false);
                Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
                Data.Aanpassingen.Add(entity);
            }
        }

        public void DeleteAanpassingen(Guid rowId, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2012.TransferTaxCreditsAppTypes.AanpassingItem original = Data.Aanpassingen.FirstOrDefault(e => e.Id == rowId);
            if (original != null)
            {
                /*
                if (original.Boeken.HasValue && original.Boeken.Value)
                {
                    worksheetset.DeleteBooking(FacturatieApp.TypeId, "OpTeMakenCN", original.Id, null);
                    // DELETE BOOKING
                }*/
                PerformAanpassing(original, ref worksheetset, true);
                Data.Aanpassingen.Remove(original);
                Data.LastUpdated = DateTime.Now;
            }
        }

        public void UpdateAanpassingen(EY.com.eBook.RuleEngine.AY2012.TransferTaxCreditsAppTypes.AanpassingItem entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2012.TransferTaxCreditsAppTypes.AanpassingItem original = Data.Aanpassingen.FirstOrDefault(e => e.Id == entity.Id);
            if (original == null)
            {
                AddToAanpassingen(entity, ref worksheetset);
            }
            else
            {
                entity = Validate(entity, "Aanpassingen", ref worksheetset);
                if (entity.IsValid)
                {
                    entity = PerformAanpassing(entity, ref worksheetset, true);
                    entity = PerformAanpassing(entity, ref worksheetset, false);
                    int idx = Data.Aanpassingen.IndexOf(original);
                    Data.Aanpassingen.RemoveAt(idx);
                    Data.Aanpassingen.Insert(idx, entity);
                    Data.LastUpdated = DateTime.Now;
                    entity.LastUpdated = DateTime.Now;
                }
            }
        }

        public void UpdateAanwendingen(EY.com.eBook.RuleEngine.AY2012.TransferTaxCreditsAppTypes.Aanwending entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2012.TransferTaxCreditsAppTypes.Aanwending original = Data.Aanwendingen;

            entity = Validate(entity, "Aanwendingen", ref worksheetset);
            if (entity.IsValid)
            {
                
                Data.Aanwendingen = entity;
                Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
            }
        }

        #endregion

        #region VALIDATION

        public EY.com.eBook.RuleEngine.AY2012.TransferTaxCreditsAppTypes.FiscaalVerlies Validate(EY.com.eBook.RuleEngine.AY2012.TransferTaxCreditsAppTypes.FiscaalVerlies entity, string location, ref WorksheetSetData worksheetset)
        {
            entity.IsValid = true;
            worksheetset.ClearMessages(TypeId, entity.Id);
            if (string.IsNullOrEmpty(entity.Type) || entity.Type == "fiscaalverlies")
            {
                if (!entity.Date.HasValue)
                {
                    worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Date");
                    entity.IsValid = false;
                }
                else
                {
                    if (entity.Date.Value > worksheetset.DataContract.StartDate)
                    {
                        worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "DATETOHIGH", location, entity.Id, "Date");
                        entity.IsValid = false;
                    }
                }
                if (!entity.BedragHuidigForm.HasValue)
                {
                    worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "BedragHuidigForm");
                    entity.IsValid = false;
                }
                else
                {
                    entity.BedragHuidig = entity.BedragHuidigForm;
                }

            }
            if (string.IsNullOrEmpty(entity.Type) || entity.Type == "")
            {
                entity.Type = "fiscaalverlies";
                entity.Order = 1;
            }
            
            
            

            return entity;
        }

        public EY.com.eBook.RuleEngine.AY2012.TransferTaxCreditsAppTypes.NIDItem Validate(EY.com.eBook.RuleEngine.AY2012.TransferTaxCreditsAppTypes.NIDItem entity, string location, ref WorksheetSetData worksheetset)
        {
            entity.IsValid = true;
            worksheetset.ClearMessages(TypeId, entity.Id);
            /*
            if (!entity.Bedrag.HasValue || entity.Bedrag.Value==0)
            {
            worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Grootboek");
            entity.IsValid = false;
            }
            */

            return entity;
        }

        public EY.com.eBook.RuleEngine.AY2012.TransferTaxCreditsAppTypes.DBIItem Validate(EY.com.eBook.RuleEngine.AY2012.TransferTaxCreditsAppTypes.DBIItem entity, string location, ref WorksheetSetData worksheetset)
        {
            entity.IsValid = true;
            worksheetset.ClearMessages(TypeId, entity.Id);
            /*
            if (!entity.Bedrag.HasValue || entity.Bedrag.Value==0)
            {
            worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Grootboek");
            entity.IsValid = false;
            }
            */

            return entity;
        }

        public EY.com.eBook.RuleEngine.AY2012.TransferTaxCreditsAppTypes.InvestItem Validate(EY.com.eBook.RuleEngine.AY2012.TransferTaxCreditsAppTypes.InvestItem entity, string location, ref WorksheetSetData worksheetset)
        {
            entity.IsValid = true;
            worksheetset.ClearMessages(TypeId, entity.Id);

            

            if (string.IsNullOrEmpty(entity.AutoKey))
            {
                entity.AutoKey = "fiscaalverlies";

                
                switch (entity.Type.Id.ToString().ToLower())
                {
                    case "a06a55de-1076-4291-8ea2-2bf9735deb68": //Fiscaal verlies
                        if (!entity.Date.HasValue)
                        {
                            worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Date");
                            entity.IsValid = false;
                        }
                        if (entity.GewoneForm.GetValueOrDefault()==0 && entity.OnderzoekOntwikkelingForm.GetValueOrDefault() == 0)
                        {
                            worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "GewoneForm");
                            worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "OnderzoekOntwikkelingForm");
                            entity.IsValid = false;
                        }
                        if (entity.IsValid == true)
                        {
                            entity.Order = 1;
                            string trans = worksheetset.GetTranslation(TypeId, "Invest", "vorig");
                            entity.Omschrijving = trans + entity.Date.Value.ToString("dd/MM/yyyy");
                            entity.Gewone = entity.GewoneForm.GetValueOrDefault();
                            entity.OnderzoekOntwikkeling = entity.OnderzoekOntwikkelingForm.GetValueOrDefault();
                        }
                        break;
                    case "be5ebc07-0327-497c-a456-dcad58cdfc78": //Omzetting belastingkrediet
                        if (Data.Invest.Count(i => i.Type != null
                            && i.Type.Id.ToString().ToLower() == "be5ebc07-0327-497c-a456-dcad58cdfc78"
                            && i.Id != entity.Id) > 0)
                        {
                            worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "DOUBLE", location, entity.Id, "Type");
                            entity.IsValid = false;
                        }
                        else
                        {
                            if (entity.OmzettingType != null && entity.OmzettingType.Id.ToString().ToLower() == "6bbfcfd4-1098-42b5-b839-61743aa9af3e")
                            {
                                int jaar = worksheetset.DataContract.EndDate.Year;
                                int jaarMin = jaar - 4;
                                entity.OmzetBelasting = Data.Invest.Where(i => i.Type != null
                                                                            && i.Type.Id.ToString().ToLower() == "a06a55de-1076-4291-8ea2-2bf9735deb68"
                                                                            && i.Date.HasValue
                                                                            && i.Date.Value.Year < jaar
                                                                            && i.Date.Value.Year >= jaarMin
                                                                        ).Sum(i => i.OnderzoekOntwikkeling);
                            }
                            if (!entity.OmzetBelasting.HasValue) entity.OmzetBelasting = 0;
                            TransferTaxCreditsAppTypes.InvestItem saldostart = Data.Invest.FirstOrDefault(i => i.AutoKey == "saldostart");
                            if (saldostart != null)
                            {
                                decimal sst = saldostart.OnderzoekOntwikkeling.HasValue ? saldostart.OnderzoekOntwikkeling.Value : 0;
                                if (entity.OmzetBelasting.Value > sst)
                                {
                                    worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "TOOMUCH", location, entity.Id, "OmzetBelasting");
                                    entity.IsValid = false;
                                }
                            }
                            if (entity.IsValid==true)
                            {
                                entity.Order = 5;
                                entity.OnderzoekOntwikkeling = entity.OmzetBelasting;
                                entity.Gewone = entity.GewoneForm.GetValueOrDefault();
                                entity.Totaal = entity.OmzetBelasting;
                            }

                        }
                        

                        break;
                }
            }
            else if (entity.AutoKey == "saldoinvest" || entity.AutoKey=="investhuidig" )
            {
                entity.OnderzoekOntwikkeling = entity.OnderzoekOntwikkelingForm.GetValueOrDefault();
                entity.Gewone = entity.GewoneForm.GetValueOrDefault();
                entity.Totaal = entity.OnderzoekOntwikkeling.GetValueOrDefault() + entity.GewoneForm.GetValueOrDefault();
            }
            if (entity.AutoKey == "fiscaalverlies")
            {
                string trans = worksheetset.GetTranslation(TypeId, "Invest", "vorig");
                entity.Omschrijving = trans + entity.Date.Value.ToString("dd/MM/yyyy");
            }
            /*
            
            */

            return entity;
        }

        public EY.com.eBook.RuleEngine.AY2012.TransferTaxCreditsAppTypes.BelastingkredietItem Validate(EY.com.eBook.RuleEngine.AY2012.TransferTaxCreditsAppTypes.BelastingkredietItem entity, string location, ref WorksheetSetData worksheetset)
        {
            entity.IsValid = true;
            worksheetset.ClearMessages(TypeId, entity.Id);
            /*
            if (!entity.Bedrag.HasValue || entity.Bedrag.Value==0)
            {
            worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Grootboek");
            entity.IsValid = false;
            }
            */

            return entity;
        }

        public EY.com.eBook.RuleEngine.AY2012.TransferTaxCreditsAppTypes.AanpassingItem Validate(EY.com.eBook.RuleEngine.AY2012.TransferTaxCreditsAppTypes.AanpassingItem entity, string location, ref WorksheetSetData worksheetset)
        {
            entity.IsValid = true;
            worksheetset.ClearMessages(TypeId, entity.Id);
            /*
            if (!entity.Bedrag.HasValue || entity.Bedrag.Value==0)
            {
            worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Grootboek");
            entity.IsValid = false;
            }
            */

            return entity;
        }

        public EY.com.eBook.RuleEngine.AY2012.TransferTaxCreditsAppTypes.Aanwending Validate(EY.com.eBook.RuleEngine.AY2012.TransferTaxCreditsAppTypes.Aanwending entity, string location, ref WorksheetSetData worksheetset)
        {
            entity.IsValid = true;
            worksheetset.ClearMessages(TypeId, entity.Id);
            /*
            if (!entity.Bedrag.HasValue || entity.Bedrag.Value==0)
            {
            worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Grootboek");
            entity.IsValid = false;
            }
            */

            return entity;
        }

        #endregion
        
        // VALIDATION
        public void ImportPrevious(ref WorksheetSetData wsd)
        {
            if (wsd.DataContract.PreviousFileId.HasValue)
            {
                if (wsd.PreviousSet.AssessmentYear == 2011)
                {
                    EY.com.eBook.RuleEngine.AY2011.WorksheetSetData pset = (EY.com.eBook.RuleEngine.AY2011.WorksheetSetData)wsd.PreviousSet;

                    EY.com.eBook.RuleEngine.AY2011.TransferTaxCreditsApp previous = pset.TransferTaxCreditsApp;
                    SetupFiscaalVerlies(ref wsd);
                    SetupInvest(ref wsd);

                    Data.FiscaleVerliezen.RemoveAll(v => v.PreviousImported == true);

                    // fiscaal verlies
                    AY2011.TransferTaxCreditsAppTypes.FiscaalVerlies verlies = previous.Data.FiscaleVerliezen.FirstOrDefault(f => f.Order == 5);
                    if (verlies != null)
                    {
                        if (verlies.Cumul.GetValueOrDefault() != 0)
                        {
                            string trans = wsd.GetTranslation(TypeId, "FiscaleVerliezen", "fiscaalverlies");

                            AY2012.TransferTaxCreditsAppTypes.FiscaalVerlies huidig = new EY.com.eBook.RuleEngine.AY2012.TransferTaxCreditsAppTypes.FiscaalVerlies
                            {
                                Id = Guid.NewGuid()
                                ,
                                Order = 1
                                ,
                                BedragHuidig = verlies.Cumul.GetValueOrDefault()
                                ,
                                Date = verlies.Date
                                ,
                                Disabled = true
                                ,
                                Omschrijving = trans + verlies.Date.Value.ToString("dd/MM/yyyy")
                                ,
                                IsValid = true
                                ,
                                LastUpdated = new DateTime(1980, 1, 1)
                                ,
                                PreviousImported = true
                                ,
                                Type = "fiscaalverlies"
                                ,
                                BedragHuidigForm = verlies.Cumul.GetValueOrDefault()
                            };
                            Data.FiscaleVerliezen.Add(huidig);
                        }
                    }


                    Data.Invest.RemoveAll(v => v.PreviousImported == true);

                    // invest
                    AY2011.TransferTaxCreditsAppTypes.InvestItem invest = previous.Data.Invest.FirstOrDefault(f => f.Order == 6);
                    if (invest != null)
                    {
                        if (invest.Totaal.GetValueOrDefault() > 0)
                        {
                            string trans = wsd.GetTranslation(TypeId, "Invest", "vorig");
                            AY2012.TransferTaxCreditsAppTypes.InvestItem hinvest = new EY.com.eBook.RuleEngine.AY2012.TransferTaxCreditsAppTypes.InvestItem
                            {
                                Id = Guid.NewGuid()
                                ,
                                Order = 1
                                ,
                                Gewone = invest.Gewone.GetValueOrDefault()
                                ,
                                GewoneForm = invest.Gewone.GetValueOrDefault()
                                ,
                                OnderzoekOntwikkeling = invest.OnderzoekOntwikkeling.GetValueOrDefault()
                                ,
                                OnderzoekOntwikkelingForm = invest.OnderzoekOntwikkeling.GetValueOrDefault()
                                ,
                                Totaal = invest.Totaal.GetValueOrDefault()
                                ,
                                Omschrijving = trans + invest.Date.Value.ToString("dd/MM/yyyy")
                                ,
                                Date = verlies.Date
                                ,
                                Disabled = true
                                ,
                                IsValid = true
                                ,
                                LastUpdated = new DateTime(1980, 1, 1)
                                ,
                                PreviousImported = true
                                ,
                                AutoKey = "fiscaalverlies"
                            };
                            Data.Invest.Add(hinvest);
                        }
                    }
                    Calculate(ref wsd); //?

                }
            }
        }
        // CALCULATION
        
        // BOOKINGS
        
        // IMPORT PREVIOUS


    }
}
  