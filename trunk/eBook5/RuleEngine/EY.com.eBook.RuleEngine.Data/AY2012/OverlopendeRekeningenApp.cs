﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.RuleEngine.Core;
using EY.com.eBook.RuleEngine.Core.Interfaces;
using EY.com.eBook.RuleEngine.Data;
using EY.com.eBook.Core;

namespace EY.com.eBook.RuleEngine.AY2012
{
    
    public class OverlopendeRekeningenApp
    {
        public static WorksheetTypeSettings Settings = new WorksheetTypeSettings
                                                      {
                                                        Clients = true
                                                        ,
                                                        Suppliers = true
                                                        ,
                                                        AllAccounts = true
                                                        , 
                                                        PerformsBookings=true
                                                      };
                                                      
        public static Guid TypeId = new Guid("d145b5f5-b585-4844-a807-b89d7fecb4cf");

        public List<string> ImportablePreviousTypes  = new List<string>() {  "EY.com.eBook.RuleEngine.AY2011.FacturatieApp"  };

        public Guid GetMyType() { return TypeId; }

        public void SetMinimal()
        {
            if (Data == null)
            {
                Data = new EY.com.eBook.RuleEngine.AY2012.OverlopendeRekeningenAppTypes.OverlopendeRekeningen();
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.OverTeDragenKosten == null)
            {
                Data.OverTeDragenKosten = new List<EY.com.eBook.RuleEngine.AY2012.OverlopendeRekeningenAppTypes.OverTeDragenKost>(); ;
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.VerkregenOpbrensten == null)
            {
                Data.VerkregenOpbrensten = new List<EY.com.eBook.RuleEngine.AY2012.OverlopendeRekeningenAppTypes.VerkregenOpbrengst>(); ;
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.ToeTeRekenenKosten == null)
            {
                Data.ToeTeRekenenKosten = new List<EY.com.eBook.RuleEngine.AY2012.OverlopendeRekeningenAppTypes.ToeTeRekenenKost>(); ;
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.OverTeDragenOpbrengsten == null)
            {
                Data.OverTeDragenOpbrengsten = new List<EY.com.eBook.RuleEngine.AY2012.OverlopendeRekeningenAppTypes.OverTeDragenOpbrengst>(); ;
                Data.LastUpdated = DateTime.Now;
            }

        }
        
        public string ToXml() 
        {
            return CoreHelper.SerializeToString(Data);
        }

        public void LoadXml(string xml)
        {
            if (!string.IsNullOrEmpty(xml)) {
                Data = CoreHelper.DeserializeFromString<EY.com.eBook.RuleEngine.AY2012.OverlopendeRekeningenAppTypes.OverlopendeRekeningen>(xml);
            }
            SetMinimal();
            
        }
        
        public EY.com.eBook.RuleEngine.AY2012.OverlopendeRekeningenAppTypes.OverlopendeRekeningen Data { get; set; }

        public void ForceRecalculation(ref WorksheetSetData worksheetset)
        {
            Data.LastUpdated = new DateTime(1980, 1, 1);
            Data.VerkregenOpbrensten.ForEach(e => e.LastUpdated = null);
            Data.ToeTeRekenenKosten.ForEach(e => e.LastUpdated = null);
            Data.OverTeDragenOpbrengsten.ForEach(e => e.LastUpdated = null);
            Data.OverTeDragenKosten.ForEach(e => e.LastUpdated = null);

            Calculate(ref worksheetset);
        }

        public void Calculate(ref WorksheetSetData worksheetset)
        {
            worksheetset.ClearMessages(TypeId);
            DateTime fileEnd = worksheetset.DataContract.EndDate;

            foreach (EY.com.eBook.RuleEngine.AY2012.OverlopendeRekeningenAppTypes.VerkregenOpbrengst entity
                in Data.VerkregenOpbrensten.Where(v => (!v.LastUpdated.HasValue
                                                        || v.LastUpdated.Value > Data.LastUpdated.Value)
                                                        ).ToList())
            {
                UpdateVerkregenOpbrensten(entity, ref worksheetset);

            }

            foreach (EY.com.eBook.RuleEngine.AY2012.OverlopendeRekeningenAppTypes.ToeTeRekenenKost entity
                in Data.ToeTeRekenenKosten.Where(v => (!v.LastUpdated.HasValue
                                                        || v.LastUpdated.Value > Data.LastUpdated.Value)
                                                        ).ToList())
            {
                UpdateToeTeRekenenKosten(entity, ref worksheetset);
                

            }

            foreach (EY.com.eBook.RuleEngine.AY2012.OverlopendeRekeningenAppTypes.OverTeDragenOpbrengst entity
                in Data.OverTeDragenOpbrengsten.Where(v => (!v.LastUpdated.HasValue
                                                        || v.LastUpdated.Value > Data.LastUpdated.Value)
                                                        ).ToList())
            {
                UpdateOverTeDragenOpbrengsten(entity,  ref worksheetset);
                

            }

            foreach (EY.com.eBook.RuleEngine.AY2012.OverlopendeRekeningenAppTypes.OverTeDragenKost entity
                in Data.OverTeDragenKosten.Where(v => (!v.LastUpdated.HasValue
                                                        || v.LastUpdated.Value > Data.LastUpdated.Value)
                                                        ).ToList())
            {
                UpdateOverTeDragenKosten(entity,ref worksheetset);
                

            }

        }

        #region AddUpdateDelete

        public void AddToOverTeDragenKosten(EY.com.eBook.RuleEngine.AY2012.OverlopendeRekeningenAppTypes.OverTeDragenKost entity, ref WorksheetSetData worksheetset)
        {
            entity = Validate(entity, "OverTeDragenKosten", ref worksheetset);
            if (entity.IsValid)
            {
                if (entity.Book.HasValue && entity.Book.Value)
                {
                    worksheetset.AddBooking(TypeId, "OverTeDragenKosten", entity.Id, null, new List<BookingLineDataContract>() {
                        new BookingLineDataContract { AccountNr = entity.Grootboek, Amount=0-entity.DeferredIncome.Value }
                            , new BookingLineDataContract { AccountNr = worksheetset.GetListAccount("49"), Amount=entity.DeferredIncome.Value}
                    });
                }
                Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
                Data.OverTeDragenKosten.Add(entity);
            }
        }

        public void DeleteOverTeDragenKosten(Guid rowId, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2012.OverlopendeRekeningenAppTypes.OverTeDragenKost original = Data.OverTeDragenKosten.FirstOrDefault(e => e.Id == rowId);
            if (original != null)
            {
                if (original.Book.HasValue && original.Book.Value)
                {
                    worksheetset.DeleteBooking(TypeId, "OverTeDragenKosten", original.Id, null);
                    // DELETE BOOKING
                }
                Data.OverTeDragenKosten.Remove(original);
                Data.LastUpdated = DateTime.Now;
            }
        }

        public void UpdateOverTeDragenKosten(EY.com.eBook.RuleEngine.AY2012.OverlopendeRekeningenAppTypes.OverTeDragenKost entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2012.OverlopendeRekeningenAppTypes.OverTeDragenKost original = Data.OverTeDragenKosten.FirstOrDefault(e => e.Id == entity.Id);
            if (original == null)
            {
                AddToOverTeDragenKosten(entity, ref worksheetset);
            }
            else
            {
                entity = Validate(entity, "OverTeDragenKosten", ref worksheetset);
                if (entity.IsValid)
                {
                    if (entity != original || !entity.LastUpdated.HasValue)
                    {
                        entity.Book = entity.Book.HasValue ? entity.Book.Value : false;
                        original.Book = original.Book.HasValue ? original.Book.Value : false;

                        if (original.Book.Value && !entity.Book.Value)
                        {
                            worksheetset.DeleteBooking(TypeId, "OverTeDragenKosten", original.Id, null);
                        }
                        else if (!original.Book.Value && entity.Book.Value)
                        {
                            worksheetset.AddBooking(TypeId, "OverTeDragenKosten", entity.Id, null, new List<BookingLineDataContract>() {
                                new BookingLineDataContract { AccountNr = entity.Grootboek, Amount=0-entity.DeferredIncome.Value }
                                    , new BookingLineDataContract { AccountNr = worksheetset.GetListAccount("49"), Amount=entity.DeferredIncome.Value}
                            });
                        }
                        else if (original.Book.Value && entity.Book.Value && (original.DeferredIncome != entity.DeferredIncome || original.Grootboek != entity.Grootboek))
                        {
                            worksheetset.UpdateBooking(TypeId, "OverTeDragenKosten", entity.Id, null, new List<BookingLineDataContract>() {
                                new BookingLineDataContract { AccountNr = entity.Grootboek, Amount=0-entity.DeferredIncome.Value }
                                , new BookingLineDataContract { AccountNr = worksheetset.GetListAccount("49"), Amount=entity.DeferredIncome.Value}
                            });
                        }
                        else if (!entity.LastUpdated.HasValue && entity.Book.Value)
                        {
                            worksheetset.UpdateBooking(TypeId, "OverTeDragenKosten", entity.Id, null, new List<BookingLineDataContract>() {
                                new BookingLineDataContract { AccountNr = entity.Grootboek, Amount=0-entity.DeferredIncome.Value }
                                , new BookingLineDataContract { AccountNr = worksheetset.GetListAccount("49"), Amount=entity.DeferredIncome.Value}
                            });
                        }
                    }
                    int idx = Data.OverTeDragenKosten.IndexOf(original);
                    Data.OverTeDragenKosten.RemoveAt(idx);
                    Data.OverTeDragenKosten.Insert(idx, entity);
                    Data.LastUpdated = DateTime.Now;
                    entity.LastUpdated = DateTime.Now;
                }
            }
        }

        public void AddToVerkregenOpbrensten(EY.com.eBook.RuleEngine.AY2012.OverlopendeRekeningenAppTypes.VerkregenOpbrengst entity, ref WorksheetSetData worksheetset)
        {
            entity = Validate(entity, "VerkregenOpbrensten", ref worksheetset);
            if (entity.IsValid)
            {
                if (entity.Book.HasValue && entity.Book.Value)
                {
                    worksheetset.AddBooking(TypeId, "VerkregenOpbrensten", entity.Id, null, new List<BookingLineDataContract>() {
                        new BookingLineDataContract { AccountNr = entity.Grootboek, Amount=0-entity.DeferredIncome.Value }
                            , new BookingLineDataContract { AccountNr = worksheetset.GetListAccount("491"), Amount=entity.DeferredIncome.Value}
                    });
                }
                Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
                Data.VerkregenOpbrensten.Add(entity);
            }
        }

        public void DeleteVerkregenOpbrensten(Guid rowId, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2012.OverlopendeRekeningenAppTypes.VerkregenOpbrengst original = Data.VerkregenOpbrensten.FirstOrDefault(e => e.Id == rowId);
            if (original != null)
            {
                if (original.Book.HasValue && original.Book.Value)
                {
                    worksheetset.DeleteBooking(TypeId, "VerkregenOpbrensten", original.Id, null);
                    // DELETE BOOKING
                }
                Data.VerkregenOpbrensten.Remove(original);
                Data.LastUpdated = DateTime.Now;
            }
        }

        public void UpdateVerkregenOpbrensten(EY.com.eBook.RuleEngine.AY2012.OverlopendeRekeningenAppTypes.VerkregenOpbrengst entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2012.OverlopendeRekeningenAppTypes.VerkregenOpbrengst original = Data.VerkregenOpbrensten.FirstOrDefault(e => e.Id == entity.Id);
            if (original == null)
            {
                AddToVerkregenOpbrensten(entity, ref worksheetset);
            }
            else
            {
                entity = Validate(entity, "VerkregenOpbrensten", ref worksheetset);
                if (entity.IsValid)
                {
                    if (entity != original || !entity.LastUpdated.HasValue)
                    {
                        entity.Book = entity.Book.HasValue ? entity.Book.Value : false;
                        original.Book = original.Book.HasValue ? original.Book.Value : false;

                        if (original.Book.Value && !entity.Book.Value)
                        {
                            worksheetset.DeleteBooking(TypeId, "VerkregenOpbrensten", original.Id, null);
                        }
                        else if (!original.Book.Value && entity.Book.Value)
                        {
                            worksheetset.AddBooking(TypeId, "VerkregenOpbrensten", entity.Id, null, new List<BookingLineDataContract>() {
                                new BookingLineDataContract { AccountNr = entity.Grootboek, Amount=0-entity.DeferredIncome.Value }
                                    , new BookingLineDataContract { AccountNr = worksheetset.GetListAccount("491"), Amount=entity.DeferredIncome.Value}
                            });
                        }
                        else if (original.Book.Value && entity.Book.Value && (original.DeferredIncome != entity.DeferredIncome || original.Grootboek != entity.Grootboek))
                        {
                            worksheetset.UpdateBooking(TypeId, "VerkregenOpbrensten", entity.Id, null, new List<BookingLineDataContract>() {
                                new BookingLineDataContract { AccountNr = entity.Grootboek, Amount=0-entity.DeferredIncome.Value }
                                , new BookingLineDataContract { AccountNr = worksheetset.GetListAccount("491"), Amount=entity.DeferredIncome.Value}
                            });
                        }
                        else if (!entity.LastUpdated.HasValue && entity.Book.Value)
                        {
                            worksheetset.UpdateBooking(TypeId, "VerkregenOpbrensten", entity.Id, null, new List<BookingLineDataContract>() {
                                new BookingLineDataContract { AccountNr = entity.Grootboek, Amount=0-entity.DeferredIncome.Value }
                                , new BookingLineDataContract { AccountNr = worksheetset.GetListAccount("491"), Amount=entity.DeferredIncome.Value}
                            });
                        }
                    }
                    int idx = Data.VerkregenOpbrensten.IndexOf(original);
                    Data.VerkregenOpbrensten.RemoveAt(idx);
                    Data.VerkregenOpbrensten.Insert(idx, entity);
                    Data.LastUpdated = DateTime.Now;
                    entity.LastUpdated = DateTime.Now;
                }
            }
        }

        public void AddToToeTeRekenenKosten(EY.com.eBook.RuleEngine.AY2012.OverlopendeRekeningenAppTypes.ToeTeRekenenKost entity, ref WorksheetSetData worksheetset)
        {
            entity = Validate(entity, "ToeTeRekenenKosten", ref worksheetset);
            if (entity.IsValid)
            {
                if (entity.Book.HasValue && entity.Book.Value)
                {
                    worksheetset.AddBooking(TypeId, "ToeTeRekenenKosten", entity.Id, null, new List<BookingLineDataContract>() {
                        new BookingLineDataContract { AccountNr = entity.Grootboek, Amount=entity.DeferredIncome.Value }
                            , new BookingLineDataContract { AccountNr = worksheetset.GetListAccount("492"), Amount=0-entity.DeferredIncome.Value}
                    });
                }
                Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
                Data.ToeTeRekenenKosten.Add(entity);
            }
        }

        public void DeleteToeTeRekenenKosten(Guid rowId, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2012.OverlopendeRekeningenAppTypes.ToeTeRekenenKost original = Data.ToeTeRekenenKosten.FirstOrDefault(e => e.Id == rowId);
            if (original != null)
            {
                if (original.Book.HasValue && original.Book.Value)
                {
                    worksheetset.DeleteBooking(TypeId, "ToeTeRekenenKosten", original.Id, null);
                    // DELETE BOOKING
                }
                Data.ToeTeRekenenKosten.Remove(original);
                Data.LastUpdated = DateTime.Now;
            }
        }

        public void UpdateToeTeRekenenKosten(EY.com.eBook.RuleEngine.AY2012.OverlopendeRekeningenAppTypes.ToeTeRekenenKost entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2012.OverlopendeRekeningenAppTypes.ToeTeRekenenKost original = Data.ToeTeRekenenKosten.FirstOrDefault(e => e.Id == entity.Id);
            if (original == null)
            {
                AddToToeTeRekenenKosten(entity, ref worksheetset);
            }
            else
            {
                entity = Validate(entity, "ToeTeRekenenKosten", ref worksheetset);
                if (entity.IsValid)
                {
                    if (entity != original || !entity.LastUpdated.HasValue)
                    {
                        entity.Book = entity.Book.HasValue ? entity.Book.Value : false;
                        original.Book = original.Book.HasValue ? original.Book.Value : false;

                        if (original.Book.Value && !entity.Book.Value)
                        {
                            worksheetset.DeleteBooking(TypeId, "ToeTeRekenenKosten", original.Id, null);
                        }
                        else if (!original.Book.Value && entity.Book.Value)
                        {
                            worksheetset.AddBooking(TypeId, "ToeTeRekenenKosten", entity.Id, null, new List<BookingLineDataContract>() {
                                new BookingLineDataContract { AccountNr = entity.Grootboek, Amount=entity.DeferredIncome.Value }
                                    , new BookingLineDataContract { AccountNr = worksheetset.GetListAccount("492"), Amount=0-entity.DeferredIncome.Value}
                            });
                        }
                        else if (original.Book.Value && entity.Book.Value && (original.DeferredIncome != entity.DeferredIncome || original.Grootboek != entity.Grootboek))
                        {
                            worksheetset.UpdateBooking(TypeId, "ToeTeRekenenKosten", entity.Id, null, new List<BookingLineDataContract>() {
                                new BookingLineDataContract { AccountNr = entity.Grootboek, Amount=entity.DeferredIncome.Value }
                                , new BookingLineDataContract { AccountNr = worksheetset.GetListAccount("492"), Amount=0-entity.DeferredIncome.Value}
                            });
                        }
                        else if (!entity.LastUpdated.HasValue && entity.Book.Value)
                        {
                            worksheetset.UpdateBooking(TypeId, "ToeTeRekenenKosten", entity.Id, null, new List<BookingLineDataContract>() {
                                new BookingLineDataContract { AccountNr = entity.Grootboek, Amount=entity.DeferredIncome.Value }
                                , new BookingLineDataContract { AccountNr = worksheetset.GetListAccount("492"), Amount=0-entity.DeferredIncome.Value}
                            });
                        }
                    }
                    int idx = Data.ToeTeRekenenKosten.IndexOf(original);
                    Data.ToeTeRekenenKosten.RemoveAt(idx);
                    Data.ToeTeRekenenKosten.Insert(idx, entity);
                    Data.LastUpdated = DateTime.Now;
                    entity.LastUpdated = DateTime.Now;
                }
            }
        }

        public void AddToOverTeDragenOpbrengsten(EY.com.eBook.RuleEngine.AY2012.OverlopendeRekeningenAppTypes.OverTeDragenOpbrengst entity, ref WorksheetSetData worksheetset)
        {
            entity = Validate(entity, "OverTeDragenOpbrengsten", ref worksheetset);
            if (entity.IsValid)
            {
                if (entity.Book.HasValue && entity.Book.Value)
                {
                    worksheetset.AddBooking(TypeId, "OverTeDragenOpbrengsten", entity.Id, null, new List<BookingLineDataContract>() {
                        new BookingLineDataContract { AccountNr = entity.Grootboek, Amount=entity.DeferredIncome.Value }
                            , new BookingLineDataContract { AccountNr = worksheetset.GetListAccount("493"), Amount=0-entity.DeferredIncome.Value}
                    });
                }
                Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
                Data.OverTeDragenOpbrengsten.Add(entity);
            }
        }

        public void DeleteOverTeDragenOpbrengsten(Guid rowId, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2012.OverlopendeRekeningenAppTypes.OverTeDragenOpbrengst original = Data.OverTeDragenOpbrengsten.FirstOrDefault(e => e.Id == rowId);
            if (original != null)
            {
                if (original.Book.HasValue && original.Book.Value)
                {
                    worksheetset.DeleteBooking(TypeId, "OverTeDragenOpbrengsten", original.Id, null);
                    // DELETE BOOKING
                }
                Data.OverTeDragenOpbrengsten.Remove(original);
                Data.LastUpdated = DateTime.Now;
            }
        }

        public void UpdateOverTeDragenOpbrengsten(EY.com.eBook.RuleEngine.AY2012.OverlopendeRekeningenAppTypes.OverTeDragenOpbrengst entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2012.OverlopendeRekeningenAppTypes.OverTeDragenOpbrengst original = Data.OverTeDragenOpbrengsten.FirstOrDefault(e => e.Id == entity.Id);
            if (original == null)
            {
                AddToOverTeDragenOpbrengsten(entity, ref worksheetset);
            }
            else
            {
                entity = Validate(entity, "OverTeDragenOpbrengsten", ref worksheetset);
                if (entity.IsValid)
                {
                    if (entity != original || !entity.LastUpdated.HasValue)
                    {
                        entity.Book = entity.Book.HasValue ? entity.Book.Value : false;
                        original.Book = original.Book.HasValue ? original.Book.Value : false;

                        if (original.Book.Value && !entity.Book.Value)
                        {
                            worksheetset.DeleteBooking(TypeId, "OverTeDragenOpbrengsten", original.Id, null);
                        }
                        else if (!original.Book.Value && entity.Book.Value)
                        {
                            worksheetset.AddBooking(TypeId, "OverTeDragenOpbrengsten", entity.Id, null, new List<BookingLineDataContract>() {
                                new BookingLineDataContract { AccountNr = entity.Grootboek, Amount=entity.DeferredIncome.Value }
                                    , new BookingLineDataContract { AccountNr = worksheetset.GetListAccount("493"), Amount=0-entity.DeferredIncome.Value}
                            });
                        }
                        else if (original.Book.Value && entity.Book.Value && (original.DeferredIncome != entity.DeferredIncome || original.Grootboek != entity.Grootboek))
                        {
                            worksheetset.UpdateBooking(TypeId, "OverTeDragenOpbrengsten", entity.Id, null, new List<BookingLineDataContract>() {
                                new BookingLineDataContract { AccountNr = entity.Grootboek, Amount=entity.DeferredIncome.Value }
                                , new BookingLineDataContract { AccountNr = worksheetset.GetListAccount("493"), Amount=0-entity.DeferredIncome.Value}
                            });
                        }
                        else if (!entity.LastUpdated.HasValue && entity.Book.Value)
                        {
                            worksheetset.UpdateBooking(TypeId, "OverTeDragenOpbrengsten", entity.Id, null, new List<BookingLineDataContract>() {
                                new BookingLineDataContract { AccountNr = entity.Grootboek, Amount=entity.DeferredIncome.Value }
                                , new BookingLineDataContract { AccountNr = worksheetset.GetListAccount("493"), Amount=0-entity.DeferredIncome.Value}
                            });
                        }
                    }
                    int idx = Data.OverTeDragenOpbrengsten.IndexOf(original);
                    Data.OverTeDragenOpbrengsten.RemoveAt(idx);
                    Data.OverTeDragenOpbrengsten.Insert(idx, entity);
                    Data.LastUpdated = DateTime.Now;
                    entity.LastUpdated = DateTime.Now;
                }
            }
        }

        #endregion
   

        #region worksheet type validation

        public EY.com.eBook.RuleEngine.AY2012.OverlopendeRekeningenAppTypes.VerkregenOpbrengst Validate(EY.com.eBook.RuleEngine.AY2012.OverlopendeRekeningenAppTypes.VerkregenOpbrengst entity, string location, ref WorksheetSetData worksheetset)
        {
            DateTime fileEnd = worksheetset.DataContract.EndDate;
            entity.IsValid = true;
            worksheetset.ClearMessages(TypeId, entity.Id);

            if (!entity.BeginDate.HasValue)
            {
                //add message collection, field:"Grootboek", required
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "BeginDate");
                entity.IsValid = false;
            }

            if (!entity.EndDate.HasValue)
            {
                //add message collection, field:"Grootboek", required
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "EndDate");
                entity.IsValid = false;
            }

            if (entity.InvoiceYear == null)
            {
                //add message collection, field:"Grootboek", required
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "InvoiceYear");
                entity.IsValid = false;
            }

            if (!entity.Amount.HasValue || entity.Amount.Value == 0)
            {
                //add message collection, field:"Grootboek", required
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Amount");
                entity.IsValid = false;
            }

            if (entity.IsValid)
            {
                if (entity.BeginDate.Value.Year < DateTime.Now.Year - 10)
                {
                    worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "TOOLD", location, entity.Id, "BeginDate");
                    entity.IsValid = false;
                }
                if (entity.BeginDate.Value > entity.EndDate.Value)
                {
                    worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "AFTEREND", location, entity.Id, "BeginDate");
                    entity.IsValid = false;
                }
                if (entity.BeginDate.Value > fileEnd && !entity.PreviousImported)
                {
                    worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "AFTERENDBOOKYEAR", location, entity.Id, "BeginDate");
                    entity.IsValid = false;
                }
            }
            if (entity.IsValid)
            {
                //decimal defered = entity.Amount.Value;

                //DateTime calcdate = fileEnd;
                //if (fileEnd >= entity.BeginDate.Value.AddDays(-1))
                //{
                //    calcdate = entity.BeginDate.Value.AddDays(-1);
                //}
                //defered = defered * (Math.Abs((calcdate - fileEnd).Days));

                //calcdate = entity.EndDate.Value;
                //if (fileEnd > calcdate) calcdate = fileEnd;
                //defered = defered / (Math.Abs((entity.BeginDate.Value - calcdate).Days)+1);
                //entity.DeferredIncome = Math.Round(defered, 2, MidpointRounding.AwayFromZero);
                //if (!entity.LastUpdated.HasValue) entity.LastUpdated = DateTime.Now;
                //Data.LastUpdated = DateTime.Now;
                decimal defered = entity.Amount.Value;
                if (entity.BeginDate.Value <= fileEnd)
                {

                    int ttlDays = (entity.EndDate.Value - entity.BeginDate.Value).Days + 1;
                    int daysInPeriod = (fileEnd - entity.BeginDate.Value).Days + 1;

                    entity.DeferredIncome = Math.Round(((defered / ttlDays) * daysInPeriod), 2, MidpointRounding.AwayFromZero);

                }
                else if (entity.EndDate.Value < fileEnd)
                {
                    entity.DeferredIncome = entity.Amount.Value;
                }
                else
                {
                    entity.DeferredIncome = 0;
                }
                if (!entity.LastUpdated.HasValue) entity.LastUpdated = DateTime.Now;
                Data.LastUpdated = DateTime.Now;
            }

            return entity;
        }

        public EY.com.eBook.RuleEngine.AY2012.OverlopendeRekeningenAppTypes.ToeTeRekenenKost Validate(EY.com.eBook.RuleEngine.AY2012.OverlopendeRekeningenAppTypes.ToeTeRekenenKost entity, string location, ref WorksheetSetData worksheetset)
        {
            DateTime fileEnd = worksheetset.DataContract.EndDate;

            entity.IsValid = true;
            worksheetset.ClearMessages(TypeId, entity.Id);

            if (!entity.BeginDate.HasValue)
            {
                //add message collection, field:"Grootboek", required
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "BeginDate");
                entity.IsValid = false;
            }

            if (!entity.EndDate.HasValue)
            {
                //add message collection, field:"Grootboek", required
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "EndDate");
                entity.IsValid = false;
            }

            if (entity.InvoiceYear == null)
            {
                //add message collection, field:"Grootboek", required
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "InvoiceYear");
                entity.IsValid = false;
            }

            if (!entity.Amount.HasValue || entity.Amount.Value == 0)
            {
                //add message collection, field:"Grootboek", required
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Amount");
                entity.IsValid = false;
            }

            if (entity.IsValid)
            {
                if (entity.BeginDate.Value.Year < DateTime.Now.Year - 10)
                {
                    worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "TOOLD", location, entity.Id, "BeginDate");
                    entity.IsValid = false;
                }
                if (entity.BeginDate.Value > entity.EndDate.Value)
                {
                    worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "AFTEREND", location, entity.Id, "BeginDate");
                    entity.IsValid = false;
                }
                if (entity.BeginDate.Value > fileEnd && !entity.PreviousImported)
                {
                    worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "AFTERENDBOOKYEAR", location, entity.Id, "BeginDate");
                    entity.IsValid = false;
                }

                if (entity.EndDate.Value.Year > DateTime.Now.Year + 10)
                {
                    worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "TOOLD", location, entity.Id, "EndDate");
                    entity.IsValid = false;
                }
            }
            if (entity.IsValid)
            {
                //decimal defered = entity.Amount.Value;

                //DateTime calcdate = fileEnd;
                //if (fileEnd >= entity.BeginDate.Value.AddDays(-1))
                //{
                //    calcdate = entity.BeginDate.Value.AddDays(-1);
                //}
                //defered = defered * Math.Abs((calcdate - fileEnd).Days);

                //calcdate = entity.EndDate.Value;
                //if (fileEnd > calcdate) calcdate = fileEnd;
                //defered = defered / (Math.Abs((entity.BeginDate.Value - calcdate).Days)+1);
                //entity.DeferredIncome = Math.Round(defered, 2, MidpointRounding.AwayFromZero);
                //if (!entity.LastUpdated.HasValue) entity.LastUpdated = DateTime.Now;
                //Data.LastUpdated = DateTime.Now;
                decimal defered = entity.Amount.Value;
                if (entity.BeginDate.Value <= fileEnd)
                {

                    int ttlDays = (entity.EndDate.Value - entity.BeginDate.Value).Days + 1;
                    int daysInPeriod = (fileEnd - entity.BeginDate.Value).Days + 1;

                    entity.DeferredIncome = Math.Round(((defered / ttlDays) * daysInPeriod), 2, MidpointRounding.AwayFromZero);

                }
                else if (entity.EndDate.Value < fileEnd)
                {
                    entity.DeferredIncome = entity.Amount.Value;
                }
                else
                {
                    entity.DeferredIncome = 0;
                }
                if (!entity.LastUpdated.HasValue) entity.LastUpdated = DateTime.Now;
                Data.LastUpdated = DateTime.Now;
            }


            return entity;
        }

        public EY.com.eBook.RuleEngine.AY2012.OverlopendeRekeningenAppTypes.OverTeDragenOpbrengst Validate(EY.com.eBook.RuleEngine.AY2012.OverlopendeRekeningenAppTypes.OverTeDragenOpbrengst entity, string location, ref WorksheetSetData worksheetset)
        {
            DateTime fileEnd = worksheetset.DataContract.EndDate;

            entity.IsValid = true;
            worksheetset.ClearMessages(TypeId, entity.Id);

            if (!entity.BeginDate.HasValue)
            {
                //add message collection, field:"Grootboek", required
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "BeginDate");
                entity.IsValid = false;
            }

            if (!entity.EndDate.HasValue)
            {
                //add message collection, field:"Grootboek", required
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "EndDate");
                entity.IsValid = false;
            }

            if (entity.InvoiceYear == null)
            {
                //add message collection, field:"Grootboek", required
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "InvoiceYear");
                entity.IsValid = false;
            }

            if (!entity.Amount.HasValue || entity.Amount.Value == 0)
            {
                //add message collection, field:"Grootboek", required
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Amount");
                entity.IsValid = false;
            }

            if (entity.IsValid)
            {
                if (entity.BeginDate.Value.Year < DateTime.Now.Year - 10)
                {
                    worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "TOOLD", location, entity.Id, "BeginDate");
                    entity.IsValid = false;
                }
                if (entity.BeginDate.Value > entity.EndDate.Value)
                {
                    worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "AFTEREND", location, entity.Id, "BeginDate");
                    entity.IsValid = false;
                }
                //if (entity.BeginDate.Value > fileEnd && en && !entity.PreviousImported)
                //{
                //    worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "AFTERENDBOOKYEAR", location, entity.Id, "BeginDate");
                //    entity.IsValid = false;
                //}

                if (entity.EndDate.Value.Year > DateTime.Now.Year + 10)
                {
                    worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "TOOLD", location, entity.Id, "EndDate");
                    entity.IsValid = false;
                }
            }

            if (entity.IsValid)
            {
                //decimal defered = entity.Amount.Value;

                //DateTime calcdate = entity.EndDate.Value;
                //if (fileEnd > entity.EndDate.Value)
                //{
                //    calcdate = fileEnd;
                //}
                //defered = defered * (Math.Abs((calcdate - fileEnd).Days));

                //calcdate = entity.BeginDate.Value;
                //if (fileEnd < calcdate) calcdate = fileEnd;
                //defered = defered / (Math.Abs((entity.EndDate.Value - calcdate).Days) + 1);
                //entity.DeferredIncome = Math.Round(defered, 2, MidpointRounding.AwayFromZero);
                //if (!entity.LastUpdated.HasValue) entity.LastUpdated = DateTime.Now;
                //Data.LastUpdated = DateTime.Now;

                decimal defered = entity.Amount.Value;
                if (entity.BeginDate.Value <= fileEnd && entity.EndDate.Value > fileEnd)
                {

                    int ttlDays = (entity.EndDate.Value - entity.BeginDate.Value).Days + 1;
                    int daysInPeriod = (fileEnd - entity.BeginDate.Value).Days + 1;

                    entity.DeferredIncome = Math.Round((defered - (defered / ttlDays) * daysInPeriod), 2, MidpointRounding.AwayFromZero);

                }
                else if (entity.BeginDate.Value > fileEnd)
                {
                    entity.DeferredIncome = entity.Amount.Value;
                }
                else
                {
                    entity.DeferredIncome = 0;
                }
                if (!entity.LastUpdated.HasValue) entity.LastUpdated = DateTime.Now;
                Data.LastUpdated = DateTime.Now;
            }


            return entity;
        }

        public EY.com.eBook.RuleEngine.AY2012.OverlopendeRekeningenAppTypes.OverTeDragenKost Validate(EY.com.eBook.RuleEngine.AY2012.OverlopendeRekeningenAppTypes.OverTeDragenKost entity, string location, ref WorksheetSetData worksheetset)
        {
            DateTime fileEnd = worksheetset.DataContract.EndDate;

            entity.IsValid = true;
            worksheetset.ClearMessages(TypeId, entity.Id);

            if (!entity.BeginDate.HasValue)
            {
                //add message collection, field:"Grootboek", required
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "BeginDate");
                entity.IsValid = false;
            }

            if (!entity.EndDate.HasValue)
            {
                //add message collection, field:"Grootboek", required
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "EndDate");
                entity.IsValid = false;
            }

            if (entity.InvoiceYear == null)
            {
                //add message collection, field:"Grootboek", required
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "InvoiceYear");
                entity.IsValid = false;
            }

            if (!entity.Amount.HasValue || entity.Amount.Value == 0)
            {
                //add message collection, field:"Grootboek", required
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Amount");
                entity.IsValid = false;
            }

            if (entity.Leverancier == null)
            {
                //add message collection, field:"Grootboek", required
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Leverancier");
                entity.IsValid = false;
            }


            if (entity.IsValid)
            {
                if (entity.BeginDate.Value.Year < DateTime.Now.Year - 10)
                {
                    
                    worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "TOOLD", location, entity.Id, "BeginDate");
                    entity.IsValid = false;
                }
                if (entity.BeginDate.Value > entity.EndDate.Value)
                {
                   
                    worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "AFTEREND", location, entity.Id, "BeginDate");
                    entity.IsValid = false;
                }
                //if (entity.BeginDate.Value > fileEnd.AddDays(1) && !entity.PreviousImported)
                //{
                    
                //    worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "AFTERENDBOOKYEAR", location, entity.Id, "BeginDate");
                //    entity.IsValid = false;
                //}

                if (entity.EndDate.Value.Year > DateTime.Now.Year + 10)
                {
                    
                    worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "TOOLD", location, entity.Id, "EndDate");
                    entity.IsValid = false;
                }
            }
            if (entity.IsValid)
            {
                //decimal defered = entity.Amount.Value;

                //DateTime calcdate = entity.EndDate.Value;
                //if (fileEnd > entity.EndDate.Value)
                //{
                //    calcdate = fileEnd;
                //}
                //defered = defered * (Math.Abs((calcdate - fileEnd).Days));

                //calcdate = entity.BeginDate.Value;
                //if (fileEnd < calcdate) calcdate = fileEnd;
                //defered = defered / (Math.Abs((entity.EndDate.Value - calcdate).Days)+1);
                //entity.DeferredIncome = Math.Round(defered, 2, MidpointRounding.AwayFromZero);
                //if (!entity.LastUpdated.HasValue) entity.LastUpdated = DateTime.Now;
                //Data.LastUpdated = DateTime.Now;

                decimal defered = entity.Amount.Value;
                if (entity.BeginDate.Value <= fileEnd && entity.EndDate.Value > fileEnd)
                {

                    int ttlDays = (entity.EndDate.Value - entity.BeginDate.Value).Days + 1;
                    int daysInPeriod = (fileEnd - entity.BeginDate.Value).Days + 1;

                    entity.DeferredIncome = Math.Round((defered - (defered / ttlDays) * daysInPeriod), 2, MidpointRounding.AwayFromZero);

                }
                else if (entity.BeginDate.Value > fileEnd)
                {
                    entity.DeferredIncome = entity.Amount.Value;
                }
                else
                {
                    entity.DeferredIncome = 0;
                }
                if (!entity.LastUpdated.HasValue) entity.LastUpdated = DateTime.Now;
                Data.LastUpdated = DateTime.Now;
            }


            return entity;
        }


        #endregion



        #region worksheet type import previous
        
        public void ImportPrevious(ref  WorksheetSetData wsd)
        {
            if (wsd.PreviousSet.AssessmentYear == 2011)
            {
                EY.com.eBook.RuleEngine.AY2011.WorksheetSetData pset = (EY.com.eBook.RuleEngine.AY2011.WorksheetSetData)wsd.PreviousSet;

                EY.com.eBook.RuleEngine.AY2011.OverlopendeRekeningenApp previous = pset.OverlopendeRekeningenApp;

                foreach (EY.com.eBook.RuleEngine.AY2011.OverlopendeRekeningenAppTypes.OverTeDragenKost klant
                            in previous.Data.OverTeDragenKosten.Where(e => e.Book.HasValue && e.Book.Value))
                {
                    EY.com.eBook.RuleEngine.AY2012.OverlopendeRekeningenAppTypes.OverTeDragenKost newEnt = new EY.com.eBook.RuleEngine.AY2012.OverlopendeRekeningenAppTypes.OverTeDragenKost
                    {
                        Id = klant.Id
                        ,
                        Amount = 0 - klant.Amount
                        ,
                        Book = true
                        ,
                        InvoiceYear = klant.InvoiceYear
                        ,
                        InvoiceNumber = klant.InvoiceNumber
                        ,
                        BeginDate = klant.BeginDate
                        ,
                        EndDate = klant.EndDate
                        ,
                        DeferredIncome = klant.DeferredIncome
                        ,
                        Omschrijving = "REVERSED " + klant.Omschrijving
                        ,
                        AutoLine = klant.AutoLine
                        ,
                        Grootboek = klant.Grootboek
                        ,
                        PreviousImported = true
                        ,
                        LastUpdated = new DateTime(1980, 1, 1)
                        ,
                        Leverancier = klant.Leverancier
                        ,
                        LeverancierNaam = klant.LeverancierNaam
                    };
                    Data.OverTeDragenKosten.Add(newEnt);
                }

                foreach (EY.com.eBook.RuleEngine.AY2011.OverlopendeRekeningenAppTypes.OverTeDragenOpbrengst klant
                            in previous.Data.OverTeDragenOpbrengsten.Where(e => e.Book.HasValue && e.Book.Value))
                {
                    EY.com.eBook.RuleEngine.AY2012.OverlopendeRekeningenAppTypes.OverTeDragenOpbrengst newEnt = new EY.com.eBook.RuleEngine.AY2012.OverlopendeRekeningenAppTypes.OverTeDragenOpbrengst
                    {
                        Id = klant.Id
                        ,
                        Amount = 0 - klant.Amount
                        ,
                        Book = true
                        ,
                        InvoiceYear = klant.InvoiceYear
                        ,
                        InvoiceNumber = klant.InvoiceNumber
                        ,
                        BeginDate = klant.BeginDate
                        ,
                        EndDate = klant.EndDate
                        ,
                        DeferredIncome = klant.DeferredIncome
                        ,
                        Omschrijving = "REVERSED " + klant.Omschrijving
                        ,
                        AutoLine = klant.AutoLine
                        ,
                        Grootboek = klant.Grootboek
                        ,
                        PreviousImported = true
                        ,
                        LastUpdated = new DateTime(1980, 1, 1)

                    };
                    Data.OverTeDragenOpbrengsten.Add(newEnt);
                }

                foreach (EY.com.eBook.RuleEngine.AY2011.OverlopendeRekeningenAppTypes.ToeTeRekenenKost klant
                            in previous.Data.ToeTeRekenenKosten.Where(e => e.Book.HasValue && e.Book.Value))
                {
                    EY.com.eBook.RuleEngine.AY2012.OverlopendeRekeningenAppTypes.ToeTeRekenenKost newEnt = new EY.com.eBook.RuleEngine.AY2012.OverlopendeRekeningenAppTypes.ToeTeRekenenKost
                    {
                        Id = klant.Id
                        ,
                        Amount = 0 - klant.Amount
                        ,
                        Book = true
                        ,
                        InvoiceYear = klant.InvoiceYear
                        ,
                        InvoiceNumber = klant.InvoiceNumber
                        ,
                        BeginDate = klant.BeginDate
                        ,
                        EndDate = klant.EndDate
                        ,
                        DeferredIncome = klant.DeferredIncome
                        ,
                        Omschrijving = "REVERSED " + klant.Omschrijving
                        ,
                        AutoLine = klant.AutoLine
                        ,
                        Grootboek = klant.Grootboek
                        ,
                        PreviousImported = true
                        ,
                        LastUpdated = new DateTime(1980, 1, 1)

                    };
                    Data.ToeTeRekenenKosten.Add(newEnt);
                }

                foreach (EY.com.eBook.RuleEngine.AY2011.OverlopendeRekeningenAppTypes.VerkregenOpbrengst klant
                            in previous.Data.VerkregenOpbrensten.Where(e => e.Book.HasValue && e.Book.Value))
                {
                    EY.com.eBook.RuleEngine.AY2012.OverlopendeRekeningenAppTypes.VerkregenOpbrengst newEnt = new EY.com.eBook.RuleEngine.AY2012.OverlopendeRekeningenAppTypes.VerkregenOpbrengst
                    {
                        Id = klant.Id
                        ,
                        Amount = 0 - klant.Amount
                        ,
                        Book = true
                        ,
                        InvoiceYear = klant.InvoiceYear
                        ,
                        InvoiceNumber = klant.InvoiceNumber
                        ,
                        BeginDate = klant.BeginDate
                        ,
                        EndDate = klant.EndDate
                        ,
                        DeferredIncome = klant.DeferredIncome
                        ,
                        Omschrijving = "REVERSED " + klant.Omschrijving
                        ,
                        AutoLine = klant.AutoLine
                        ,
                        Grootboek = klant.Grootboek
                        ,
                        PreviousImported = true
                        ,
                        LastUpdated = new DateTime(1980, 1, 1)

                    };
                    Data.VerkregenOpbrensten.Add(newEnt);
                }
            }
        }


        #endregion
        
    }
}
  