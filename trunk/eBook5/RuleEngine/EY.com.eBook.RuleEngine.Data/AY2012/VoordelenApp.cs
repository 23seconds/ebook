﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.RuleEngine.Core;
using EY.com.eBook.RuleEngine.Core.Interfaces;
using EY.com.eBook.RuleEngine.Data;
using EY.com.eBook.Core;
using EY.com.eBook.API.Contracts.Data.Cache;

namespace EY.com.eBook.RuleEngine.AY2012
{
    
    public class VoordelenApp
    {
        public static WorksheetTypeSettings Settings = new WorksheetTypeSettings
                                                      {
                                                        Clients = true
                                                        ,
                                                        Suppliers = true
                                                        ,
                                                        AllAccounts = true
                                                        , 
                                                        PerformsBookings=true
                                                      };
                                                      
        public static Guid TypeId = new Guid("67855d3d-4e11-4c7b-96a3-e6602ddaaa6f");

        public List<string> ImportablePreviousTypes  = new List<string>() {  "EY.com.eBook.RuleEngine.AY2011.FacturatieApp"  };

        public Guid GetMyType() { return TypeId; }

        public void SetMinimal()
        {
            if (Data == null)
            {
                Data = new EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.Voordelen();
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.VoordelenVanAlleAardAuto == null)
            {
                Data.VoordelenVanAlleAardAuto = new List<EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_Auto>(); ;
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.VoordelenVanAlleAardAutoCO2 == null)
            {
                Data.VoordelenVanAlleAardAutoCO2 = new List<EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_AutoCO2>(); ;
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.VoordelenVanAlleAardNuts == null)
            {
                Data.VoordelenVanAlleAardNuts = new List<EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_Nuts>(); ;
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.VoordelenVanAlleAardHuur == null)
            {
                Data.VoordelenVanAlleAardHuur = new List<EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_HUUR>(); ;
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.VoordelenVanAlleAardHuurHerkwalificatie == null)
            {
                Data.VoordelenVanAlleAardHuurHerkwalificatie = new List<EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_HUURHERK>(); ;
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.VoordelenVanAlleAardPC == null)
            {
                Data.VoordelenVanAlleAardPC = new List<EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_PC>(); ;
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.VoordelenVanAlleAardTelefonie == null)
            {
                Data.VoordelenVanAlleAardTelefonie = new List<EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_Telefonie>(); ;
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.VoordelenVanAlleAardOverige == null)
            {
                Data.VoordelenVanAlleAardOverige = new List<EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_Overige>(); ;
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.VoordelenInterestenRCBedrijfsleider == null)
            {
                Data.VoordelenInterestenRCBedrijfsleider = new List<EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_Interesten_RC_Bedrijfsleider>(); ;
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.VoordelenSocialeBijdragenBedrijfsleider == null)
            {
                Data.VoordelenSocialeBijdragenBedrijfsleider = new List<EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_Soc_Bijdr_Bedrijfsleider>(); ;
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.VoordelenOverigeNuts == null)
            {
                Data.VoordelenOverigeNuts = new List<EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_Overige_Nuts>(); ;
                Data.LastUpdated = DateTime.Now;
            }

        }
        
        public string ToXml() 
        {
            return CoreHelper.SerializeToString(Data);
        }

        public void LoadXml(string xml)
        {
            if (!string.IsNullOrEmpty(xml)) {
                Data = CoreHelper.DeserializeFromString<EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.Voordelen>(xml);
            }
            SetMinimal();
            
        }
        
        public EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.Voordelen Data { get; set; }
        

        #region worksheet type validation


        public EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_Nuts Validate(EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_Nuts entity, string location, ref WorksheetSetData worksheetset)
        {
            worksheetset.ClearMessages(TypeId, entity.Id);
            entity.IsValid = true;
            FileDataContract fdc = worksheetset.DataContract;

            if (entity.GrootBoek == null)
            {
                //add message collection, field:"Grootboek", required
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "GrootBoek");
                entity.IsValid = false;
            }

            if (entity.Soort == null)
            {
                //add message collection, field:"Grootboek", required
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Soort");
                entity.IsValid = false;
            }

            if (!entity.StartDate.HasValue)
            {
                //add message collection, field:"Grootboek", required
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "StartDate");
                entity.IsValid = false;
            }
            if (!entity.EndDate.HasValue)
            {
                //add message collection, field:"Grootboek", required
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "EndDate");
                entity.IsValid = false;
            }

            if (entity.StartDate.HasValue && entity.EndDate.HasValue)
            {
                if (entity.StartDate.Value > entity.EndDate.Value)
                {
                    //add message collection, field:"Grootboek", required
                    worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "AFTEREND", location, entity.Id, "StartDate");
                    worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "AFTEREND", location, entity.Id, "EndDate");
                    entity.IsValid = false;
                }

                if (entity.StartDate.Value < fdc.StartDate)
                {
                    //add message collection, field:"Grootboek", required

                    worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "BEFORESTART", location, entity.Id, "StartDate");
                    entity.IsValid = false;
                }

                if (entity.EndDate.Value > fdc.EndDate)
                {
                    //add message collection, field:"Grootboek", required
                    worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "AFTEREND", location, entity.Id, "EndDate");
                    entity.IsValid = false;
                }

                if (entity.Book.HasValue && entity.Book.Value && entity.DebetRekening == null)
                {
                    worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "DebetRekening");
                    entity.IsValid = false;
                }

            }
            return entity;
        }

        public EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_HUUR Validate(EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_HUUR entity, string location, ref WorksheetSetData worksheetset)
        {
            worksheetset.ClearMessages(TypeId, entity.Id);
            entity.IsValid = true;
            FileDataContract fdc = worksheetset.DataContract;

            if (entity.GrootBoek == null)
            {
                //add message collection, field:"Grootboek", required
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "GrootBoek");
                entity.IsValid = false;
            }

            if (entity.EigenaarRechtstype == null)
            {
                //add message collection, field:"Grootboek", required
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "EigenaarRechtstype");
                entity.IsValid = false;
            }

            if (entity.Soort == null)
            {
                //add message collection, field:"Grootboek", required
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Soort");
                entity.IsValid = false;
            }

            if (!entity.StartDate.HasValue)
            {
                //add message collection, field:"Grootboek", required
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "StartDate");
                entity.IsValid = false;
            }
            if (!entity.EndDate.HasValue)
            {
                //add message collection, field:"Grootboek", required
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "EndDate");
                entity.IsValid = false;
            }

            if (entity.StartDate.HasValue && entity.EndDate.HasValue)
            {
                if (entity.StartDate.Value > entity.EndDate.Value)
                {
                    //add message collection, field:"Grootboek", required
                    worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "AFTEREND", location, entity.Id, "StartDate");
                    worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "AFTEREND", location, entity.Id, "EndDate");
                    entity.IsValid = false;
                }

                if (entity.StartDate.Value < fdc.StartDate)
                {
                    //add message collection, field:"Grootboek", required
                    worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "BEFORESTART", location, entity.Id, "StartDate");
                    entity.IsValid = false;
                }

                if (entity.EndDate.Value > fdc.EndDate)
                {
                    //add message collection, field:"Grootboek", required
                    worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "AFTEREND", location, entity.Id, "EndDate");
                    entity.IsValid = false;
                }

                if (entity.Book.HasValue && entity.Book.Value && entity.DebetRekening == null)
                {
                    worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "DebetRekening");
                    entity.IsValid = false;
                }

            }

            if (entity.AantalPerJaar.HasValue && entity.AantalPerJaar.Value > 20)
            {
                if ((entity.Soort.Id == new Guid("1DA7C412-4D53-46C5-B49A-59FD33683DB1")
                    || entity.Soort.Id == new Guid("5E399446-E8C8-4338-82E2-15E9889A53FB")
                    || entity.Soort.Id == new Guid("3CD236CA-8420-4BE0-B8B7-6F36B843606B")))
                {
                    worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "KADASTRAAL", location, entity.Id, "AantalPerJaar");
                    entity.IsValid = false;
                }
            }

            if (entity.IsValid)
            {
                string srt = fdc.Culture == "nl-BE" ? entity.Soort.NL : fdc.Culture == "fr-FR" ? entity.Soort.FR : entity.Soort.EN;
                entity.GridText = string.Format("{0}: ", srt, entity.StartDate.Value.ToShortDateString(), entity.EndDate.Value.ToShortDateString());
            }
            return entity;
        }

        public EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_PC Validate(EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_PC entity, string location, ref WorksheetSetData worksheetset)
        {
            worksheetset.ClearMessages(TypeId, entity.Id);
            entity.IsValid = true;
            FileDataContract fdc = worksheetset.DataContract;

            if (entity.GrootBoek == null)
            {
                //add message collection, field:"Grootboek", required
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "GrootBoek");
                entity.IsValid = false;
            }

            if (entity.Soort == null)
            {
                //add message collection, field:"Grootboek", required
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Soort");
                entity.IsValid = false;
            }

            if (!entity.StartDate.HasValue)
            {
                //add message collection, field:"Grootboek", required
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "StartDate");
                entity.IsValid = false;
            }
            if (!entity.EndDate.HasValue)
            {
                //add message collection, field:"Grootboek", required
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "EndDate");
                entity.IsValid = false;
            }

            if (entity.StartDate.HasValue && entity.EndDate.HasValue)
            {
                if (entity.StartDate.Value > entity.EndDate.Value)
                {
                    //add message collection, field:"Grootboek", required
                    worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "AFTEREND", location, entity.Id, "StartDate");
                    worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "AFTEREND", location, entity.Id, "EndDate");
                    entity.IsValid = false;
                }

                if (entity.StartDate.Value < fdc.StartDate)
                {
                    //add message collection, field:"Grootboek", required
                    worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "BEFORESTART", location, entity.Id, "StartDate");
                    entity.IsValid = false;
                }

                if (entity.EndDate.Value > fdc.EndDate)
                {
                    //add message collection, field:"Grootboek", required
                    worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "AFTEREND", location, entity.Id, "EndDate");
                    entity.IsValid = false;
                }

                if (entity.Book.HasValue && entity.Book.Value && entity.DebetRekening == null)
                {
                    worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "DebetRekening");
                    entity.IsValid = false;
                }

            }
            return entity;

        }

        public EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_Telefonie Validate(EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_Telefonie entity, string location, ref WorksheetSetData worksheetset)
        {
            worksheetset.ClearMessages(TypeId, entity.Id);
            entity.IsValid = true;
            FileDataContract fdc = worksheetset.DataContract;
            

            if (entity.GrootBoek == null)
            {
                //add message collection, field:"Grootboek", required
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "GrootBoek");
                entity.IsValid = false;
            }

            if (entity.Soort == null)
            {
                //add message collection, field:"Grootboek", required
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Soort");
                entity.IsValid = false;
            }

            if (!entity.StartDate.HasValue)
            {
                //add message collection, field:"Grootboek", required
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "StartDate");
                entity.IsValid = false;
            }
            if (!entity.EndDate.HasValue)
            {
                //add message collection, field:"Grootboek", required
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "EndDate");
                entity.IsValid = false;
            }

            if (entity.StartDate.HasValue && entity.EndDate.HasValue)
            {
                if (entity.StartDate.Value > entity.EndDate.Value)
                {
                    //add message collection, field:"Grootboek", required
                    worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "AFTEREND", location, entity.Id, "StartDate");
                    worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "AFTEREND", location, entity.Id, "EndDate");
                    entity.IsValid = false;
                }

                if (entity.StartDate.Value < fdc.StartDate)
                {
                    //add message collection, field:"Grootboek", required
                    worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "BEFORESTART", location, entity.Id, "StartDate");
                    entity.IsValid = false;
                }

                if (entity.EndDate.Value > fdc.EndDate)
                {
                    //add message collection, field:"Grootboek", required
                    worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "AFTEREND", location, entity.Id, "EndDate");
                    entity.IsValid = false;
                }

                if (entity.Book.HasValue && entity.Book.Value && entity.DebetRekening == null)
                {
                    worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "DebetRekening");
                    entity.IsValid = false;
                }

            }
            return entity;
        }

        public EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_Overige Validate(EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_Overige entity, string location, ref WorksheetSetData worksheetset)
        {
            worksheetset.ClearMessages(TypeId, entity.Id);
            entity.IsValid = true;
            FileDataContract fdc = worksheetset.DataContract;

            if (entity.GrootBoek == null)
            {
                //add message collection, field:"Grootboek", required
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "GrootBoek");
                entity.IsValid = false;
            }
            entity.GridText = entity.Tekst;

            //if (entity.Soort == null)
            //{
            //    //add message collection, field:"Grootboek", required
            //    worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Soort");
            //    entity.IsValid = false;
            //}

            if (!entity.StartDate.HasValue)
            {
                //add message collection, field:"Grootboek", required
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "StartDate");
                entity.IsValid = false;
            }
            if (!entity.EndDate.HasValue)
            {
                //add message collection, field:"Grootboek", required
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "EndDate");
                entity.IsValid = false;
            }

            if (!entity.Bedragopjaarbasis.HasValue)
            {
                //add message collection, field:"Grootboek", required
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Bedragopjaarbasis");
                entity.IsValid = false;
            }

            if (entity.BTWPerc == null)
            {
                //add message collection, field:"Grootboek", required
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "BTWPerc");
                entity.IsValid = false;
            }

            if (entity.StartDate.HasValue && entity.EndDate.HasValue)
            {
                if (entity.StartDate.Value > entity.EndDate.Value)
                {
                    //add message collection, field:"Grootboek", required
                    worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "AFTEREND", location, entity.Id, "StartDate");
                    worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "AFTEREND", location, entity.Id, "EndDate");
                    entity.IsValid = false;
                }

                if (entity.StartDate.Value < fdc.StartDate)
                {
                    //add message collection, field:"Grootboek", required
                    worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "BEFORESTART", location, entity.Id, "StartDate");
                    entity.IsValid = false;
                }

                if (entity.EndDate.Value > fdc.EndDate)
                {
                    //add message collection, field:"Grootboek", required
                    worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "AFTEREND", location, entity.Id, "EndDate");
                    entity.IsValid = false;
                }

                if (entity.Book.HasValue && entity.Book.Value && entity.DebetRekening == null)
                {
                    
                    worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "DebetRekening");
                    entity.IsValid = false;
                }

            }
            return entity;
        }

        public EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_Interesten_RC_Bedrijfsleider Validate(EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_Interesten_RC_Bedrijfsleider entity, string location, ref WorksheetSetData worksheetset)
        {
            worksheetset.ClearMessages(TypeId, entity.Id);
            entity.IsValid = true;
            FileDataContract fdc = worksheetset.DataContract;
            
            if (entity.GrootBoek == null)
            {
                //add message collection, field:"Grootboek", required
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "GrootBoek");
                entity.IsValid = false;
            }

            if (entity.Soort == null)
            {
                //add message collection, field:"Grootboek", required
               
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Soort");
                entity.IsValid = false;
            }

            if (!entity.StartDate.HasValue)
            {
                //add message collection, field:"Grootboek", required
               
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "StartDate");
                entity.IsValid = false;
            }
            if (!entity.EndDate.HasValue)
            {
                //add message collection, field:"Grootboek", required
                
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "EndDate");
                entity.IsValid = false;
            }

            if (!entity.BedragOpJaarbasis.HasValue)
            {
                //add message collection, field:"Grootboek", required
                
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "BedragOpJaarbasis");
                entity.IsValid = false;
            }

            if (!entity.RVPerc.HasValue)
            {
                //add message collection, field:"Grootboek", required
                
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "RVPerc");
                entity.IsValid = false;
            }

            if (entity.StartDate.HasValue && entity.EndDate.HasValue)
            {
                if (entity.StartDate.Value > entity.EndDate.Value)
                {
                    //add message collection, field:"Grootboek", required
                    
                    worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "AFTEREND", location, entity.Id, "StartDate");
                    worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "AFTEREND", location, entity.Id, "EndDate");
                    entity.IsValid = false;
                }

                if (entity.StartDate.Value < fdc.StartDate)
                {
                    //add message collection, field:"Grootboek", required
                    
                    worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "BEFORESTART", location, entity.Id, "StartDate");
                    entity.IsValid = false;
                }

                if (entity.EndDate.Value > fdc.EndDate)
                {
                    //add message collection, field:"Grootboek", required
                    
                    worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "AFTEREND", location, entity.Id, "EndDate");
                    entity.IsValid = false;
                }

                if (entity.Book.HasValue && entity.Book.Value && entity.CreditRekening == null)
                {
                    
                    worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "CreditRekening");
                    entity.IsValid = false;
                }

            }
            return entity;

        }

        public EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_Soc_Bijdr_Bedrijfsleider Validate(EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_Soc_Bijdr_Bedrijfsleider entity, string location, ref WorksheetSetData worksheetset)
        {
            worksheetset.ClearMessages(TypeId, entity.Id);
            entity.IsValid = true;
            FileDataContract fdc = worksheetset.DataContract;

            if (entity.GrootBoek == null)
            {
                //add message collection, field:"Grootboek", required
                
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "GrootBoek");
                entity.IsValid = false;
            }

            if (!entity.StartDate.HasValue)
            {
                //add message collection, field:"Grootboek", required
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "StartDate");
                entity.IsValid = false;
            }
            if (!entity.EndDate.HasValue)
            {
                //add message collection, field:"Grootboek", required
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "EndDate");
                entity.IsValid = false;
            }

            if (!entity.BedragOpJaarBasis.HasValue)
            {
                //add message collection, field:"Grootboek", required
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "BedragOpJaarBasis");
                entity.IsValid = false;
            }



            if (entity.StartDate.HasValue && entity.EndDate.HasValue)
            {
                if (entity.StartDate.Value > entity.EndDate.Value)
                {
                    //add message collection, field:"Grootboek", required
                    worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "AFTEREND", location, entity.Id, "StartDate");
                    worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "AFTEREND", location, entity.Id, "EndDate");
                    entity.IsValid = false;
                }

                if (entity.StartDate.Value < fdc.StartDate)
                {
                    //add message collection, field:"Grootboek", required
                    worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "BEFORESTART", location, entity.Id, "StartDate");
                    entity.IsValid = false;
                }

                if (entity.EndDate.Value > fdc.EndDate)
                {
                    //add message collection, field:"Grootboek", required
                    worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "AFTEREND", location, entity.Id, "EndDate");
                    entity.IsValid = false;
                }

                if (entity.Book.HasValue && entity.Book.Value && entity.DebetRekening == null)
                {
                    worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "DebetRekening");
                    entity.IsValid = false;
                }

            }
            return entity;
        }

        public EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_Overige_Nuts Validate(EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_Overige_Nuts entity, string location, ref WorksheetSetData worksheetset)
        {
            worksheetset.ClearMessages(TypeId, entity.Id);
            entity.IsValid = true;
            FileDataContract fdc = worksheetset.DataContract;
            
            if (entity.GrootBoek == null)
            {
                //add message collection, field:"Grootboek", required
                
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "GrootBoek");
                entity.IsValid = false;
            }

            if (entity.Soort == null)
            {
                //add message collection, field:"Grootboek", required
                
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Soort");
                entity.IsValid = false;
            }

            if (!entity.StartDate.HasValue)
            {
                //add message collection, field:"Grootboek", required
                
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "StartDate");
                entity.IsValid = false;
            }
            if (!entity.EndDate.HasValue)
            {
                //add message collection, field:"Grootboek", required
                
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "EndDate");
                entity.IsValid = false;
            }

            if (!entity.BedragOpJaarbasis.HasValue)
            {
                //add message collection, field:"Grootboek", required
                
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "BedragOpJaarbasis");
                entity.IsValid = false;
            }
            if (entity.BTWPerc == null)
            {
                //add message collection, field:"Grootboek", required
                
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "BTWPerc");
                entity.IsValid = false;
            }




            if (entity.StartDate.HasValue && entity.EndDate.HasValue)
            {
                if (entity.StartDate.Value > entity.EndDate.Value)
                {
                    //add message collection, field:"Grootboek", required
                    
                    worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "AFTEREND", location, entity.Id, "StartDate");
                    worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "AFTEREND", location, entity.Id, "EndDate");
                    entity.IsValid = false;
                }

                if (entity.StartDate.Value < fdc.StartDate)
                {
                    //add message collection, field:"Grootboek", required
                    
                    worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "BEFORESTART", location, entity.Id, "StartDate");
                    entity.IsValid = false;
                }

                if (entity.EndDate.Value > fdc.EndDate)
                {
                    //add message collection, field:"Grootboek", required
                    
                    worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "AFTEREND", location, entity.Id, "EndDate");
                    entity.IsValid = false;
                }

                if (entity.Book.HasValue && entity.Book.Value && entity.DebetRekening == null)
                {
                    
                    worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "DebetRekening");
                    entity.IsValid = false;
                }

            }
            return entity;
        }

        public EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_HUURHERK Validate(EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_HUURHERK entity, string location, ref WorksheetSetData worksheetset)
        {
            worksheetset.ClearMessages(TypeId, entity.Id);
            entity.IsValid = true;
            FileDataContract fdc = worksheetset.DataContract;
            
            if (entity.GrootBoek == null)
            {
                //add message collection, field:"Grootboek", required
                
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "GrootBoek");
                entity.IsValid = false;
            }

            if (entity.Gemeenschappelijk.HasValue)
            {
                if (entity.Gemeenschappelijk.Value && !entity.Percent.HasValue)
                {
                    
                    worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Percent");
                    entity.IsValid = false;
                }
            }

            if (!entity.KIVerhuurd.HasValue)
            {
                //add message collection, field:"Grootboek", required
                
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "KIVerhuurd");
                entity.IsValid = false;
            }
            else
            {
                if (entity.KIVerhuurd.Value <= 0)
                {
                    //add message collection, field:"Grootboek", required
                    
                    worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "KIVerhuurd");
                    entity.IsValid = false;
                }
            }

            if (!entity.OntvangenHuur.HasValue)
            {
                //add message collection, field:"Grootboek", required
                
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "OntvangenHuur");
                entity.IsValid = false;
            }
            else
            {
                if (entity.OntvangenHuur.Value <= 0)
                {
                    //add message collection, field:"Grootboek", required
                    
                    worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "OntvangenHuur");
                    entity.IsValid = false;
                }
            }



            if (entity.Book.HasValue && entity.Book.Value && entity.DebetRekening == null)
            {
                
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "DebetRekening");
                entity.IsValid = false;
            }
            return entity;
        }

        public EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_AutoCO2 Validate(EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_AutoCO2 entity, string location, ref WorksheetSetData worksheetset)
        {
            worksheetset.ClearMessages(TypeId, entity.Id);
            entity.IsValid = true;
            FileDataContract fdc = worksheetset.DataContract;
           
            if (entity.GrootBoek == null)
            {
                //add message collection, field:"Grootboek", required
                
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "GrootBoek");
                entity.IsValid = false;
            }


            if (!entity.StartDate.HasValue)
            {
                //add message collection, field:"Grootboek", required
                
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "StartDate");
                entity.IsValid = false;
            }
            if (!entity.EndDate.HasValue)
            {
                //add message collection, field:"Grootboek", required
                
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "EndDate");
                entity.IsValid = false;
            }





            if (entity.StartDate.HasValue && entity.EndDate.HasValue)
            {
                if (entity.StartDate.Value > entity.EndDate.Value)
                {
                    //add message collection, field:"Grootboek", required
                    
                    worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "AFTEREND", location, entity.Id, "StartDate");
                    worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "AFTEREND", location, entity.Id, "EndDate");
                    entity.IsValid = false;
                }

                if (entity.StartDate.Value < fdc.StartDate)
                {
                    //add message collection, field:"Grootboek", required
                    
                    worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "BEFORESTART", location, entity.Id, "StartDate");
                    entity.IsValid = false;
                }

                if (entity.EndDate.Value > fdc.EndDate)
                {
                    //add message collection, field:"Grootboek", required
                    
                    worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "AFTEREND", location, entity.Id, "EndDate");
                    entity.IsValid = false;
                }

                if (entity.Book.HasValue && entity.Book.Value && entity.TeBoekenDebetRekening == null)
                {
                    
                    worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "TeBoekenDebetRekening");
                    entity.IsValid = false;
                }

            }

            if (!entity.Elektrische.HasValue || entity.Elektrische == false)
            {
                if (entity.Motortype == null)
                {
                    
                    worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Motortype");
                    entity.IsValid = false;
                }

            }
            return entity;
        }


        public EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_AutoRupo Validate(EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_AutoRupo entity, string location, ref WorksheetSetData worksheetset)
        {
            worksheetset.ClearMessages(TypeId, entity.Id);
            entity.IsValid = true;
            FileDataContract fdc = worksheetset.DataContract;

            if (entity.GrootBoek == null)
            {
                //add message collection, field:"Grootboek", required

                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "GrootBoek");
                entity.IsValid = false;
            }

            if (string.IsNullOrEmpty(entity.MerkModel))
            {
                //add message collection, field:"Grootboek", required
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "MerkModel");
                entity.IsValid = false;
            }

            if (!entity.StartDate.HasValue)
            {
                //add message collection, field:"Grootboek", required

                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "StartDate");
                entity.IsValid = false;
            }
            if (!entity.EndDate.HasValue)
            {
                //add message collection, field:"Grootboek", required

                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "EndDate");
                entity.IsValid = false;
            }

            if (entity.IsValid)
            {
                if (entity.StartDate < new DateTime(2012, 1, 1))
                {
                    worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "MINIMUM_2012", location, entity.Id, "StartDate");
                    entity.IsValid = false;
                }
                if (entity.StartDate.Value > entity.EndDate.Value)
                {
                    //add message collection, field:"Grootboek", required

                    worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "AFTEREND", location, entity.Id, "StartDate");
                    worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "AFTEREND", location, entity.Id, "EndDate");
                    entity.IsValid = false;
                }

                if (entity.StartDate.Value < fdc.StartDate)
                {
                    //add message collection, field:"Grootboek", required

                    worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "BEFORESTART", location, entity.Id, "StartDate");
                    entity.IsValid = false;
                }

                if (entity.EndDate.Value > fdc.EndDate)
                {
                    //add message collection, field:"Grootboek", required

                    worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "AFTEREND", location, entity.Id, "EndDate");
                    entity.IsValid = false;
                }
            }

            if (entity.Motortype == null)
            {
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Motortype");
                entity.IsValid = false;
            }
            if (entity.TypeVoertuig == null)
            {
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "TypeVoertuig");
                entity.IsValid = false;
            }
            else
            {
                if (entity.TypeVoertuig.Id == new Guid("81219CAE-656C-4620-B1C3-CB679A063FA8")) // PERSONENWAGEN
                {
                    if (!entity.CatalogusWaarde.HasValue)
                    {
                        worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "CatalogusWaarde");
                        entity.IsValid = false;
                    }

                    if (!entity.DatumInschrijving.HasValue)
                    {
                        //add message collection, field:"Grootboek", required

                        worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "DatumInschrijving");
                        entity.IsValid = false;
                    }


                    if (entity.DatumInschrijving.HasValue && entity.StartDate.HasValue)
                    {
                        //add message collection, field:"Grootboek", required

                        if (entity.StartDate.Value < entity.DatumInschrijving.Value) entity.StartDate = entity.DatumInschrijving;
                    }

                    if (!entity.CO2.HasValue)
                    {
                        switch (entity.Motortype.Id.ToString().ToLower())
                        {
                            case "c516ca90-c767-4ff9-b7f3-7eb326a99ec0": // DIESEL
                                entity.CO2 = 195;
                                break;
                            case "7a871e25-01db-4f66-ba3a-8262fedf4557": // ELEKTRISCH
                                entity.CO2 = 0;
                                break;
                            default: // ANDERS
                                entity.CO2 = 205;
                                break;
                        }
                    }
                }
            }



            if (entity.StartDate.HasValue && entity.EndDate.HasValue)
            {

                if (entity.Book.HasValue && entity.Book.Value && entity.TeBoekenDebetRekening == null)
                {

                    worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "TeBoekenDebetRekening");
                    entity.IsValid = false;
                }

            }

            return entity;
        }



        #endregion

        public void ForceRecalculation(ref WorksheetSetData worksheetset)
        {
            Data.LastUpdated = new DateTime(1980, 1, 1);
            Data.VoordelenInterestenRCBedrijfsleider.ForEach(e => e.LastUpdated = null);
            Data.VoordelenOverigeNuts.ForEach(e => e.LastUpdated = null);
            Data.VoordelenSocialeBijdragenBedrijfsleider.ForEach(e => e.LastUpdated = null);
            Data.VoordelenVanAlleAardAuto.ForEach(e => e.LastUpdated = null);
            Data.VoordelenVanAlleAardAutoCO2.ForEach(e => e.LastUpdated = null);
            Data.VoordelenVanAlleAardHuur.ForEach(e => e.LastUpdated = null);
            Data.VoordelenVanAlleAardHuurHerkwalificatie.ForEach(e => e.LastUpdated = null);
            Data.VoordelenVanAlleAardNuts.ForEach(e => e.LastUpdated = null);
            Data.VoordelenVanAlleAardOverige.ForEach(e => e.LastUpdated = null);
            Data.VoordelenVanAlleAardPC.ForEach(e => e.LastUpdated = null);
            Data.VoordelenVanAlleAardTelefonie.ForEach(e => e.LastUpdated = null);
           
            Calculate(ref worksheetset);
        }

        #region worksheet type rules
        // Only if worksheet does any calculation.
        public void Calculate(ref WorksheetSetData wsd)
        {
            wsd.ClearMessages(TypeId);
            CalculateNuts(ref wsd);
            CalculateHuur(ref wsd);
            CalculatePC(ref wsd);
            CalculateTelefonie(ref wsd);
            CalculateOverige(ref wsd);
            CalculateIntrestenBedrijfsleider(ref wsd);
            CalculateSocialeBijdragenBedrijfsleider(ref wsd);
            CalculateOverigeNuts(ref wsd);
            CalculateHuurHerKw(ref wsd);
            CalculateAutoCO2(ref wsd);
            CalculateAutoRupo(ref wsd);
            Data.LastUpdated = DateTime.Now;
        }

        public void CalculateNuts(ref WorksheetSetData wsd)
        {
            List<VoordelenAppTypes.VAA_Nuts> list = Data.VoordelenVanAlleAardNuts.Where(v => !v.LastUpdated.HasValue || v.LastUpdated.Value > Data.LastUpdated).ToList();
            for(int i=0; i<list.Count;i++) {
                 if(!list[i].LastUpdated.HasValue) list[i] = Validate(list[i], "VoordelenVanAlleAardHuur", ref wsd);
                 CalculateNuts(list[i], ref wsd);
            }
        }

        public void CalculateHuur(ref WorksheetSetData wsd)
        {

            List<VoordelenAppTypes.VAA_HUUR> list = Data.VoordelenVanAlleAardHuur.Where(v => !v.LastUpdated.HasValue || v.LastUpdated.Value > Data.LastUpdated).ToList();
            for (int i = 0; i < list.Count; i++)
            {
                if (!list[i].LastUpdated.HasValue) list[i] = Validate(list[i], "VoordelenVanAlleAardHuur", ref wsd);
                CalculateHuur(list[i], ref wsd);
            }
        }

        public void CalculatePC(ref WorksheetSetData wsd)
        {
            List<VoordelenAppTypes.VAA_PC> list = Data.VoordelenVanAlleAardPC.Where(v => !v.LastUpdated.HasValue || v.LastUpdated.Value > Data.LastUpdated).ToList();
            for (int i = 0; i < list.Count; i++)
            {
                if (!list[i].LastUpdated.HasValue) list[i] = Validate(list[i], "VoordelenVanAlleAardPC", ref wsd);
                CalculatePC(list[i], ref wsd);
            }

        }

        public void CalculateTelefonie(ref WorksheetSetData wsd)
        {

            List<VoordelenAppTypes.VAA_Telefonie> list = Data.VoordelenVanAlleAardTelefonie.Where(v => !v.LastUpdated.HasValue || v.LastUpdated.Value > Data.LastUpdated).ToList();
            for (int i = 0; i < list.Count; i++)
            {
                if (!list[i].LastUpdated.HasValue) list[i] = Validate(list[i], "VoordelenVanAlleAardTelefonie", ref wsd);
                CalculateTelefonie(list[i], ref wsd);
            }
        }

        public void CalculateOverige(ref WorksheetSetData wsd)
        {
            List<VoordelenAppTypes.VAA_Overige> list = Data.VoordelenVanAlleAardOverige.Where(v => !v.LastUpdated.HasValue || v.LastUpdated.Value > Data.LastUpdated).ToList();
            for (int i = 0; i < list.Count; i++)
            {
                if (!list[i].LastUpdated.HasValue) list[i] = Validate(list[i], "VoordelenVanAlleAardOverige", ref wsd);
                CalculateOverige(list[i], ref wsd);
            }
            
        }

        public void CalculateIntrestenBedrijfsleider(ref WorksheetSetData wsd)
        {
            List<VoordelenAppTypes.VAA_Interesten_RC_Bedrijfsleider> list = Data.VoordelenInterestenRCBedrijfsleider.Where(v => !v.LastUpdated.HasValue || v.LastUpdated.Value > Data.LastUpdated).ToList();
            for (int i = 0; i < list.Count; i++)
            {
                if (!list[i].LastUpdated.HasValue) list[i] = Validate(list[i], "VoordelenInterestenRCBedrijfsleider", ref wsd);
                CalculateIntrestenBedrijfsleider(list[i], ref wsd);
            }
        }

        public void CalculateSocialeBijdragenBedrijfsleider(ref WorksheetSetData wsd)
        {
            List<VoordelenAppTypes.VAA_Soc_Bijdr_Bedrijfsleider> list = Data.VoordelenSocialeBijdragenBedrijfsleider.Where(v => !v.LastUpdated.HasValue || v.LastUpdated.Value > Data.LastUpdated).ToList();
            for (int i = 0; i < list.Count; i++)
            {
                if (!list[i].LastUpdated.HasValue) list[i] = Validate(list[i], "VoordelenSocialeBijdragenBedrijfsleider", ref wsd);
                CalculateSocialeBijdragenBedrijfsleider(list[i], ref wsd);
            }
            
        }

        public void CalculateOverigeNuts(ref WorksheetSetData wsd)
        {

            List<VoordelenAppTypes.VAA_Overige_Nuts> list = Data.VoordelenOverigeNuts.Where(v => !v.LastUpdated.HasValue || v.LastUpdated.Value > Data.LastUpdated).ToList();
            for (int i = 0; i < list.Count; i++)
            {
                if (!list[i].LastUpdated.HasValue) list[i] = Validate(list[i], "VoordelenOverigeNuts", ref wsd);
                CalculateOverigeNuts(list[i], ref wsd);
            }
        }

        public void CalculateHuurHerKw(ref WorksheetSetData wsd)
        {
            FileDataContract fdc = wsd.DataContract;

            List<VoordelenAppTypes.VAA_HUURHERK> list = Data.VoordelenVanAlleAardHuurHerkwalificatie.Where(v => !v.LastUpdated.HasValue || v.LastUpdated.Value > Data.LastUpdated).ToList();
            for (int i = 0; i < list.Count; i++)
            {
                if (!list[i].LastUpdated.HasValue) list[i] = Validate(list[i], "VoordelenVanAlleAardHuurHerkwalificatie", ref wsd);
                CalculateHuurHerKw(list[i], ref wsd);
            }
        }

        public void CalculateAutoCO2(ref WorksheetSetData wsd)
        {
            List<VoordelenAppTypes.VAA_AutoCO2> list = Data.VoordelenVanAlleAardAutoCO2.Where(v => !v.LastUpdated.HasValue || v.LastUpdated.Value > Data.LastUpdated).ToList();
            for (int i = 0; i < list.Count; i++)
            {
                if (!list[i].LastUpdated.HasValue) list[i] = Validate(list[i], "VoordelenVanAlleAardAutoCO2", ref wsd);
                CalculateAutoCO2(list[i], ref wsd);
            }
        }

        public void CalculateAutoRupo(ref WorksheetSetData wsd)
        {
            if (Data.VoordelenVanAlleAardAutoRupo == null) Data.VoordelenVanAlleAardAutoRupo = new List<EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_AutoRupo>();
            List<VoordelenAppTypes.VAA_AutoRupo> list = Data.VoordelenVanAlleAardAutoRupo.Where(v => !v.LastUpdated.HasValue || v.LastUpdated.Value > Data.LastUpdated).ToList();
            for (int i = 0; i < list.Count; i++)
            {
                if (!list[i].LastUpdated.HasValue) list[i] = Validate(list[i], "VoordelenVanAlleAardAutoRupo", ref wsd);
                CalculateAutoRupo(list[i], ref wsd);
            }
        }



        private void CalculateNuts(VoordelenAppTypes.VAA_Nuts nuts, ref WorksheetSetData wsd)
        {
            
            bool isVat = !string.IsNullOrEmpty(wsd.Client.Vat);
            FileDataContract fdc = wsd.DataContract;
            if (nuts.IsValid)
            {
                string srt = fdc.Culture == "nl-BE" ? nuts.Soort.NL : fdc.Culture == "fr-FR" ? nuts.Soort.FR : nuts.Soort.EN;
                nuts.GridText = string.Format("{0}: ", srt, nuts.StartDate.Value.ToShortDateString(), nuts.EndDate.Value.ToShortDateString());

                decimal total = 0;
                if (nuts.BedragFiscus.HasValue && nuts.BedragFiscus != 0 && (!nuts.WettelijkForfait.HasValue || !nuts.WettelijkForfait.Value))
                {
                    total = nuts.BedragFiscus.Value;
                    nuts.AantalPerJaar = 0;
                }
                else
                { 
                    if (!nuts.AantalPerJaar.HasValue) nuts.AantalPerJaar = 1;
                    nuts.Details = new List<EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_DetailProratatie>();
                    DateTime start = nuts.StartDate.Value;
                    foreach (CoefficientDataContract cdc in wsd.GetListItemCoefficients(nuts.Soort.Id, "VAA_NUTS_BASIS", null, nuts.StartDate.Value, nuts.EndDate.Value))
                    {
                        if (cdc.EndDate.Value > start)
                        {
                            DateTime end = nuts.EndDate.Value <= cdc.EndDate.Value ? nuts.EndDate.Value : cdc.EndDate.Value;
                            EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_DetailProratatie detail = new EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_DetailProratatie
                            {
                                CoeffStart = cdc.StartDate.Value
                                ,
                                CoeffEnd = cdc.EndDate.Value
                                ,
                                CoeffDays = (cdc.EndDate.Value - cdc.StartDate.Value).Days + 1
                                ,
                                CoeffValue = cdc.Value
                                ,
                                Start = start
                                ,
                                End = end
                                ,
                                Days = start.IncludedDaysDiff(end)
                            };

                            detail.Result = ((detail.CoeffValue * nuts.AantalPerJaar.Value) / detail.CoeffDays) * detail.Days;
                            detail.Result= Math.Round(detail.Result.GetValueOrDefault(), 2, MidpointRounding.AwayFromZero);
                            nuts.Details.Add(detail);
                            total += detail.Result.GetValueOrDefault();
                            start = end.AddDays(1);
                        }
                    }
                    //total = Math.Round(total, 2, MidpointRounding.AwayFromZero);
                }
                if (nuts.WettelijkForfait.HasValue && !nuts.WettelijkForfait.Value)
                {
                    nuts.BedragFiscus = 0;
                }
                if (isVat)
                {
                    nuts.MvH = Math.Round(total / (decimal)1.21, 2, MidpointRounding.AwayFromZero);
                }
                else
                {
                    nuts.MvH = total;
                }

                nuts.BTW = total - nuts.MvH;
                nuts.Totaal = total;
                nuts.LastUpdated = DateTime.Now;
            }

        }

        private void CalculateHuur(VoordelenAppTypes.VAA_HUUR huur, ref WorksheetSetData wsd)
        {
            FileDataContract fdc = wsd.DataContract;

            if (huur.IsValid)
            {
                string srt = fdc.Culture == "nl-BE" ? huur.Soort.NL : fdc.Culture == "fr-FR" ? huur.Soort.FR : huur.Soort.EN;
                huur.GridText = string.Format("{0}: ", srt, huur.StartDate.Value.ToShortDateString(), huur.EndDate.Value.ToShortDateString());

                if (!huur.AantalPerJaar.HasValue) huur.AantalPerJaar = 1;

                //
               // wsd.GetListItemCoefficients(huur.Soort.Id, "VAA_ONROEREND_BASIS", null, huur.StartDate.Value, huur.EndDate.Value))
                decimal total = 0;
                decimal totalIndex = 0;
                DateTime start = huur.StartDate.Value;
                huur.Details = new List<EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_DetailProratatieHuur>();
                foreach (CoefficientDataContract cdc in wsd.GetListItemCoefficients(huur.Soort.Id, "VAA_ONROEREND_BASIS", null, huur.StartDate.Value, huur.EndDate.Value))
                {
                    if (cdc.EndDate.Value > start)
                    {
                        DateTime end = huur.EndDate.Value <= cdc.EndDate.Value ? huur.EndDate.Value : cdc.EndDate.Value;

                        EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_DetailProratatieHuur detail = new EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_DetailProratatieHuur
                        {
                            CoeffStart = cdc.StartDate.Value
                            ,
                            CoeffEnd = cdc.EndDate.Value
                            ,
                            CoeffDays = (cdc.EndDate.Value - cdc.StartDate.Value).Days + 1
                            ,
                            Basis = cdc.Value
                            ,
                            Start = start
                            ,
                            End = end
                            ,
                            Days = start.IncludedDaysDiff(end)
                            ,
                            Result = 0
                        };


                        if (huur.Soort.Id == new Guid("DE0438C9-3412-4CF1-A783-D2446E943152"))
                        {
                            detail.Result = Math.Round(detail.Basis.GetValueOrDefault() * huur.AantalPerJaar.Value, 2, MidpointRounding.AwayFromZero);

                            if (detail.Days >= 364)
                            {
                                detail.Result = (decimal)266.4;
                            }
                            else
                            {
                                detail.Result = (detail.Result * detail.Days);
                            }
                            total += detail.Result.GetValueOrDefault();
                        }
                        else
                        {
                            detail.Basis = null;
                            // actual calculation
                            DateTime workStartDate = new DateTime(start.Year, start.Month, 1);
                            if (start.Day > 16) workStartDate = workStartDate.AddMonths(1);
                            DateTime workEndDate = new DateTime(end.Year, end.Month, 2);
                            if (end.Day > 16) workStartDate.AddMonths(1);

                            int months = start.MonthsBetween(end);
                            detail.MonthsKI = months;
                            CoefficientDataContract ecdc = wsd.GetCoefficientFullPeriod("KADASTRAAL_INKOMEN_JAARLIJKSE_INDEX", start, end);

                            detail.IndexKI = ecdc.Value;
                            if (ecdc == null)
                            {
                                // THROW EXCEPTION MESSAGE, STOP EXECUTION
                            }
                            else
                            {
                                //VAA_ONROEREND_BASIS

                                decimal ki = Math.Round(((huur.KI.Value * ecdc.Value) / 12) * months, 0, MidpointRounding.AwayFromZero);
                                detail.IndexedKI = ki;
                                decimal tempTotal = 0;

                                if (huur.Soort.Id != new Guid("DE0438C9-3412-4CF1-A783-D2446E943152") && huur.Soort.Id != new Guid("1DA7C412-4D53-46C5-B49A-59FD33683DB1")) // Geen enkele kamer of terrein
                                {
                                  
                                    detail.Result = (ki * 100) / 60; // MOVE TO COEFFICIENT?
                                    if (huur.EigenaarRechtstype.Id == new Guid("8FE420B3-32A2-4DEB-858E-EF961175DE15")) // rechtspersoon
                                    {
                                        if (huur.KI.Value <= 745)
                                        {
                                            CoefficientDataContract mcdc = wsd.GetCoefficientFullPeriod("VAA_ONROEREND_RP_LAAGKI_MULTIPLIER", start, end);
                                            detail.Multiplier = mcdc.Value;
                                            detail.Result = detail.Result * mcdc.Value; //(decimal)1.25;
                                        }
                                        else
                                        {
                                            CoefficientDataContract mcdc = wsd.GetCoefficientFullPeriod("VAA_ONROEREND_RP_HOOGKI_MULTIPLIER", start, end);
                                            detail.Multiplier = mcdc.Value;
                                            detail.Result = detail.Result * mcdc.Value;//2;  // moet 3,8 worden vanaf 01/01/2012 -> op te nemen in coëfficiënten
                                            detail.Result = Math.Round(detail.Result.GetValueOrDefault(), 2, MidpointRounding.AwayFromZero);
                                        }
                                    }
                                    if (huur.Soort.Id == new Guid("3CD236CA-8420-4BE0-B8B7-6F36B843606B")) // Gemeubelde woning
                                    {
                                        // MOVE TO COEFFICIENT?
                                        detail.Result = Math.Round(detail.Result.GetValueOrDefault() * 5 / 3, 2, MidpointRounding.AwayFromZero);
                                        detail.Multiplier = Math.Round((decimal)5 / (decimal)3, 2, MidpointRounding.AwayFromZero);
                                    }
                                }
                                else if (huur.Soort.Id == new Guid("1DA7C412-4D53-46C5-B49A-59FD33683DB1")) // TERREIN
                                {
                                    detail.Result = ki * 100 / 90;  // MOVE TO COEFFICIENT?
                                }

                                
                                totalIndex += ki;
                                total += detail.Result.GetValueOrDefault();


                            }
                        }
                        huur.Details.Add(detail);
                        start = end.AddDays(1);
                    }
                }
                huur.indexedKI = totalIndex;

                huur.Totaal = total;
                huur.BTW = 0;
                huur.MvH = huur.Totaal;
                huur.LastUpdated = DateTime.Now;
            }
        }

        private void CalculatePC(VoordelenAppTypes.VAA_PC pc, ref WorksheetSetData wsd)
        {

            bool isVat = !string.IsNullOrEmpty(wsd.Client.Vat);
            FileDataContract fdc = wsd.DataContract;
            
                if (pc.IsValid)
                {
                    string srt = fdc.Culture == "nl-BE" ? pc.Soort.NL : fdc.Culture == "fr-FR" ? pc.Soort.FR : pc.Soort.EN;
                    pc.GridText = string.Format("{0}: ", srt, pc.StartDate.Value.ToShortDateString(), pc.EndDate.Value.ToShortDateString());

                    decimal total = 0;
                    if (!pc.AantalPerJaar.HasValue) pc.AantalPerJaar = 1;

                    pc.Details = new List<EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_DetailProratatie>();

                    DateTime start = pc.StartDate.Value;
                    foreach (CoefficientDataContract cdc in wsd.GetListItemCoefficients(pc.Soort.Id, "VAA_PC_BASIS",null, pc.StartDate.Value, pc.EndDate.Value))
                    {
                        
                        if (cdc.EndDate.Value > start)
                        {
                            DateTime end = pc.EndDate.Value <= cdc.EndDate.Value ? pc.EndDate.Value : cdc.EndDate.Value;
                            EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_DetailProratatie detail = new EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_DetailProratatie
                            {
                                CoeffStart = cdc.StartDate.Value
                                ,
                                CoeffEnd = cdc.EndDate.Value
                                ,
                                CoeffDays = (cdc.EndDate.Value - cdc.StartDate.Value).Days + 1
                                ,
                                CoeffValue = cdc.Value
                                ,
                                Start = start
                                ,
                                End = end
                                ,
                                Days = start.IncludedDaysDiff(end)
                                ,
                                Result = 0
                            };

                            detail.Result = ((detail.CoeffValue * pc.AantalPerJaar.Value) / detail.CoeffDays) * detail.Days;
                            detail.Result = Math.Round(detail.Result.GetValueOrDefault(), 2, MidpointRounding.AwayFromZero);
                            total += detail.Result.GetValueOrDefault();
                            start = end.AddDays(1);
                        }
                    }
                    total = Math.Round(total, 2, MidpointRounding.AwayFromZero);

                    if (isVat)
                    {
                        pc.MvH = Math.Round(total / (decimal)1.21, 2, MidpointRounding.AwayFromZero);
                    }
                    else
                    {
                        pc.MvH = total;
                    }

                    pc.BTW = total - pc.MvH;
                    pc.Totaal = total;
                    pc.LastUpdated = DateTime.Now;
                }
        }

        private void CalculateTelefonie(VoordelenAppTypes.VAA_Telefonie entity, ref WorksheetSetData wsd)
        {

            bool isVat = !string.IsNullOrEmpty(wsd.Client.Vat);
            FileDataContract fdc = wsd.DataContract;
           
                if (entity.IsValid)
                {
                    string srt = fdc.Culture == "nl-BE" ? entity.Soort.NL : fdc.Culture == "fr-FR" ? entity.Soort.FR : entity.Soort.EN;
                    entity.GridText = string.Format("{0}: ", srt, entity.StartDate.Value.ToShortDateString(), entity.EndDate.Value.ToShortDateString());

                    decimal total = 0;
                    if (!entity.AantalPerJaar.HasValue) entity.AantalPerJaar = 1;

                    entity.Details = new List<EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_DetailProratatie>();


                    DateTime start = entity.StartDate.Value;
                    foreach (CoefficientDataContract cdc in wsd.GetListItemCoefficients(entity.Soort.Id, "VAA_TELEFONIE_BASIS", null, entity.StartDate.Value, entity.EndDate.Value))
                    {
                      
                        if (cdc.EndDate.Value > start)
                        {
                            DateTime end = entity.EndDate.Value <= cdc.EndDate.Value ? entity.EndDate.Value : cdc.EndDate.Value;
                            EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_DetailProratatie detail = new EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_DetailProratatie
                            {
                                CoeffStart = cdc.StartDate.Value
                                ,
                                CoeffEnd = cdc.EndDate.Value
                                ,
                                CoeffDays = (cdc.EndDate.Value - cdc.StartDate.Value).Days + 1
                                ,
                                CoeffValue = cdc.Value
                                ,
                                Start = start
                                ,
                                End = end
                                ,
                                Days = start.IncludedDaysDiff(end)
                                ,
                                Result = 0
                            };
                            detail.Result = ((detail.CoeffValue * entity.AantalPerJaar.Value) / detail.CoeffDays) * detail.Days;
                            detail.Result = Math.Round(detail.Result.GetValueOrDefault(), 2, MidpointRounding.AwayFromZero);
                            total += detail.Result.GetValueOrDefault();
                            start = end.AddDays(1);
                        }
                    }
                    total = Math.Round(total, 2, MidpointRounding.AwayFromZero);

                    if (isVat)
                    {
                        entity.MvH = Math.Round(total / (decimal)1.21, 2, MidpointRounding.AwayFromZero);
                    }
                    else
                    {
                        entity.MvH = total;
                    }

                    entity.BTW = total - entity.MvH;
                    entity.Totaal = total;
                    entity.LastUpdated = DateTime.Now;
                }
            
        }

        private void CalculateOverige(VoordelenAppTypes.VAA_Overige entity, ref WorksheetSetData wsd)
        {
            bool isVat = !string.IsNullOrEmpty(wsd.Client.Vat);
            
                if (entity.IsValid)
                {
                    // PRORATATIE NOG TOE TE VOEGEN ???
                    decimal noemer = (new DateTime(entity.EndDate.Value.Year + 1, 1, 1) - new DateTime(entity.EndDate.Value.Year, 1, 1)).Days;
                    int days = entity.StartDate.Value.IncludedDaysDiff(entity.EndDate.Value);
                    if (days >= 366 || days < 364)
                    {
                        entity.Totaal = (entity.Bedragopjaarbasis.Value / noemer) * days;
                    }
                    else
                    {
                        entity.Totaal = entity.Bedragopjaarbasis;
                    }
                    decimal btwPerc = 0;
                    switch (entity.BTWPerc.Id.ToString().ToLower())
                    {
                        case "004f529f-d803-4d98-8acf-8f76c7383ac6": //6%
                            btwPerc = (decimal)0.06;
                            break;
                        case "c28120df-b0e6-41c3-a7f2-9252b59935a9":
                            btwPerc = (decimal)0.21;
                            break; 
                    }
                    entity.MvH = Math.Round(entity.Totaal.GetValueOrDefault() / (1 + btwPerc), 2, MidpointRounding.AwayFromZero);
                    entity.BTWBedrag = entity.Totaal.Value - entity.MvH.Value;

                    entity.LastUpdated = DateTime.Now;
                }
        }

        private void CalculateIntrestenBedrijfsleider(VoordelenAppTypes.VAA_Interesten_RC_Bedrijfsleider entity, ref WorksheetSetData wsd)
        {
            bool isVat = !string.IsNullOrEmpty(wsd.Client.Vat);
            
                if (entity.IsValid)
                {
                    // PRORATATIE NOG TOE TE VOEGEN ???
                    decimal noemer = (new DateTime(entity.EndDate.Value.Year + 1, 1, 1) - new DateTime(entity.EndDate.Value.Year, 1, 1)).Days;
                    int days = entity.StartDate.Value.IncludedDaysDiff(entity.EndDate.Value);
                    if (days >= 366 || days < 364)
                    {
                        entity.Totaal = (entity.BedragOpJaarbasis.Value / noemer) * days;
                    }
                    else
                    {
                        entity.Totaal = entity.BedragOpJaarbasis;
                    }
                    entity.Totaal = Math.Round(entity.Totaal.Value, 2, MidpointRounding.AwayFromZero);
                    if (!entity.RVPerc.HasValue) entity.RVPerc = 0;
                    decimal btwPerc = entity.RVPerc.Value;
                    entity.MvH = Math.Round(entity.Totaal.Value / (1 + (entity.RVPerc.Value / 100)), 2, MidpointRounding.AwayFromZero);
                    entity.RV = entity.Totaal.Value - entity.MvH.Value;

                    entity.LastUpdated = DateTime.Now;
                  
                }
            
        }

        private void CalculateSocialeBijdragenBedrijfsleider(VoordelenAppTypes.VAA_Soc_Bijdr_Bedrijfsleider entity, ref WorksheetSetData wsd)
        {
            bool isVat = !string.IsNullOrEmpty(wsd.Client.Vat);
            
                if (entity.IsValid)
                {
                    // PRORATATIE NOG TOE TE VOEGEN ???
                    decimal noemer = (new DateTime(entity.EndDate.Value.Year + 1, 1, 1) - new DateTime(entity.EndDate.Value.Year, 1, 1)).Days;
                    int days = entity.StartDate.Value.IncludedDaysDiff(entity.EndDate.Value);
                    if (days >= 366 || days < 364)
                    {
                        entity.Totaal = (entity.BedragOpJaarBasis.Value / noemer) * days;
                    }
                    else
                    {
                        entity.Totaal = entity.BedragOpJaarBasis;
                    }
                    entity.Totaal = Math.Round(entity.Totaal.Value, 2, MidpointRounding.AwayFromZero);
                    entity.LastUpdated = DateTime.Now;

                }
            
        }

        private void CalculateOverigeNuts(VoordelenAppTypes.VAA_Overige_Nuts entity, ref WorksheetSetData wsd)
        {
            bool isVat = !string.IsNullOrEmpty(wsd.Client.Vat);
            
                if (entity.IsValid)
                {
                    // PRORATATIE NOG TOE TE VOEGEN ???
                    decimal noemer = (new DateTime(entity.EndDate.Value.Year + 1, 1, 1) - new DateTime(entity.EndDate.Value.Year, 1, 1)).Days;
                    int days = entity.StartDate.Value.IncludedDaysDiff(entity.EndDate.Value);
                    if (days >= 366 || days < 364)
                    {
                        entity.Totaal = (entity.BedragOpJaarbasis.Value / noemer) * days;
                    }
                    else
                    {
                        entity.Totaal = entity.BedragOpJaarbasis;
                    }
                    entity.Totaal = Math.Round(entity.Totaal.Value, 2, MidpointRounding.AwayFromZero);
                    if (!entity.BTWPerc.HasValue) entity.BTWPerc = 0;
                    decimal btwPerc = entity.BTWPerc.Value;
                    entity.MvH = Math.Round(entity.Totaal.Value / (1 + (entity.BTWPerc.Value / 100)), 2, MidpointRounding.AwayFromZero);
                    entity.BTW = entity.Totaal.Value - entity.MvH.Value;

                    entity.LastUpdated = DateTime.Now;
                   
                }
        }

        private void CalculateHuurHerKw(VoordelenAppTypes.VAA_HUURHERK huur, ref WorksheetSetData wsd)
        {
            FileDataContract fdc = wsd.DataContract;
            
                if (huur.IsValid)
                {
                    if (!huur.Gemeenschappelijk.HasValue) huur.Gemeenschappelijk = false;

                    decimal gemeenschappelijkCoeff = 1;

                    if (huur.Gemeenschappelijk.Value)
                    {
                        gemeenschappelijkCoeff = huur.Percent.Value / 100;
                    }
                    else
                    {
                        huur.Percent = 100;
                    }


                    decimal grenswaarde = 0;

                    DateTime start = new DateTime(fdc.EndDate.Year, 1, 1);
                    DateTime end = fdc.EndDate;

                    CoefficientDataContract cdc = wsd.GetCoefficient("VAA_HUURHERKWALIFICATIE", start, end);

                    huur.Grenswaarde = (((huur.KIVerhuurd.Value * 5) / 3) * cdc.Value) * gemeenschappelijkCoeff;
                    huur.Grenswaarde = Math.Round(huur.Grenswaarde.Value, 2, MidpointRounding.AwayFromZero);

                    if (huur.OntvangenHuur.Value > huur.Grenswaarde)
                    {
                        huur.Herkwalificatie = (huur.OntvangenHuur.Value * gemeenschappelijkCoeff) - huur.Grenswaarde.Value;
                    }
                    else
                    {
                        huur.Herkwalificatie = 0;
                    }
                    huur.Herkwalificatie = Math.Round(huur.Herkwalificatie.Value, 2, MidpointRounding.AwayFromZero);

                    huur.LastUpdated = DateTime.Now;
                }
            
        }

        private void CalculateAutoCO2(VoordelenAppTypes.VAA_AutoCO2 auto, ref WorksheetSetData wsd)
        {
            bool isVat = !string.IsNullOrEmpty(wsd.Client.Vat);
            bool lichteVracht = auto.LichteVracht.HasValue && auto.LichteVracht.Value;
            decimal btwVoordeelCoeff = isVat ? lichteVracht ? (decimal) 1.189 : (decimal)1.0855  : 1;
            decimal btwVoordeelCoeffRC = isVat ? (decimal)1.21 : 1;

            if (auto.IsValid)
            {

                int monthsBetween = auto.StartDate.Value.MonthsBetween(auto.EndDate.Value);
                decimal minimum = (decimal)0.10 * ((decimal)monthsBetween / 12); // PRORATATIE MINIMALE WAARDE (=0.10*aantalkm's per jaar)

                decimal total = 0;

                if (auto.Elektrische.HasValue && auto.Elektrische.Value)
                {
                    total = minimum * auto.AantalPerJaar.Value.Value;
                }
                else
                {
                    switch (auto.Motortype.Id.ToString())
                    {
                        case "953ef2e1-a5da-46aa-93b3-d026f709296d": // DIESEL
                            if (!auto.CO2.HasValue || auto.CO2.Value == 0)
                            {
                                auto.CO2 = 195;
                            }
                            break;
                        case "b437884a-b4c0-4f44-b1be-aee781a8c4d1": // ANDER
                            if (!auto.CO2.HasValue || auto.CO2.Value == 0)
                            {
                                auto.CO2 = 205;
                            }
                            break;
                    }

                    DateTime start = auto.StartDate.Value;
                    decimal val = auto.AantalPerJaar.Id.ToString().ToLower() == "6dfd49d6-af7d-4e07-9d0b-1b410a08c631" ? 5000 : 7500;
                    auto.Details = new List<EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_DetailProratatie>();

                    foreach (CoefficientDataContract cdc in wsd.GetListItemCoefficients(auto.Motortype.Id, "VAA_AUTO_2011_ESSENCETYPE",null, auto.StartDate.Value, auto.EndDate.Value))
                    {
                        decimal noemer = (cdc.EndDate.Value - cdc.StartDate.Value).Days + 1; // +1 is inclusief
                        if (cdc.EndDate.Value > start)
                        {
                            DateTime end = auto.EndDate.Value <= cdc.EndDate.Value ? auto.EndDate.Value : cdc.EndDate.Value;
                            EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_DetailProratatie detail = new EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_DetailProratatie
                            {
                                CoeffStart = cdc.StartDate.Value
                                , CoeffEnd = cdc.EndDate.Value
                                ,
                                CoeffDays = (cdc.EndDate.Value - cdc.StartDate.Value).Days + 1
                                , CoeffValue = cdc.Value
                                , Start = start
                                , End =end
                                ,
                                Days = start.IncludedDaysDiff(end)
                                , Result=0
                            };


                            detail.Result = ((auto.CO2.Value * val) * detail.CoeffValue) * ((decimal)detail.Days / (decimal)detail.CoeffDays);
                            detail.Result = Math.Round(detail.Result.GetValueOrDefault() , 2, MidpointRounding.AwayFromZero);
                            total += detail.Result.GetValueOrDefault();

                            start = end.AddDays(1);
                        }

                    }
                    if (total < (minimum * val))
                    {
                        total = (minimum * val);
                    }
                }
                auto.Totaal = total;
                auto.MvH = 0;
                if (auto.BookingType != null)
                {
                    switch (auto.BookingType.Id.ToString().ToLower())
                    {
                        case "4181ec7e-341e-49fd-bdd1-4a8644c75179": // Voordeel
                            auto.MvH = Math.Round((auto.Totaal.Value) / btwVoordeelCoeff, 2, MidpointRounding.AwayFromZero);
                            if (auto.PersoonlijkeBijdrage.HasValue)
                            {
                                if (auto.PersoonlijkeBijdrage.Value >= auto.Totaal.Value)
                                {
                                    auto.Totaal = auto.PersoonlijkeBijdrage.Value;
                                    auto.MvH = Math.Round(auto.Totaal.Value / btwVoordeelCoeffRC, 2, MidpointRounding.AwayFromZero);
                                }
                                else if (auto.PersoonlijkeBijdrage.Value > 0 && auto.PersoonlijkeBijdrage.Value < auto.Totaal.Value)
                                {
                                    auto.MvH = Math.Round(auto.PersoonlijkeBijdrage.Value / btwVoordeelCoeffRC, 2, MidpointRounding.AwayFromZero);
                                    auto.MvH += Math.Round((auto.Totaal.Value - auto.PersoonlijkeBijdrage.Value) / btwVoordeelCoeff, 2, MidpointRounding.AwayFromZero);
                                }
                            }
                            break;
                        default:

                            if (auto.PersoonlijkeBijdrage.HasValue)
                            {
                                if (auto.PersoonlijkeBijdrage.Value >= auto.Totaal.Value)
                                {
                                    auto.Totaal = auto.PersoonlijkeBijdrage.Value;
                                }
                            }
                            auto.MvH = Math.Round(auto.Totaal.Value / btwVoordeelCoeffRC, 2, MidpointRounding.AwayFromZero);
                            break;
                    }
                    auto.BTW = auto.Totaal.Value - auto.MvH.Value;
                }

                auto.LastUpdated = DateTime.Now;
            }

        }


        private void CalculateAutoRupo(VoordelenAppTypes.VAA_AutoRupo auto, ref WorksheetSetData wsd)
        {
            if (auto.IsValid)
            {

                bool isVat = !string.IsNullOrEmpty(wsd.Client.Vat);
                bool lichteVracht = auto.TypeVoertuig.Id == new Guid("9B23AC79-D132-4F12-8C37-AC4E2B4168CB");
                decimal btwVoordeelCoeff = isVat ? lichteVracht ? (decimal)1.189 : (decimal)1.0855 : 1;
                decimal btwVoordeelCoeffRC = isVat ? (decimal)1.21 : 1;


                decimal total = 0;

                if (auto.TypeVoertuig.Id == new Guid("81219CAE-656C-4620-B1C3-CB679A063FA8"))
                {
                    auto.WerkelijkeWaarde = null;
                    List<CoefficientDataContract> coeffs = wsd.GetCoefficients("VAA_AUTO_DIRUPO", null, auto.StartDate.Value, auto.EndDate.Value);

                    DateTime inschrijfDate = new DateTime(auto.DatumInschrijving.Value.Year, auto.DatumInschrijving.Value.Month, 1);

                    DateRange inschrijfPeriod = new DateRange();
                    inschrijfPeriod.Start = new DateTime(auto.StartDate.Value.Year, inschrijfDate.Month, 1);
                    inschrijfPeriod.End = inschrijfPeriod.Start.AddYears(1).AddDays(-1);

                    List<DateRange> ranges = new List<DateRange>();

                    if (inschrijfPeriod.Start > auto.StartDate.Value)
                    {
                        if (inschrijfPeriod.Start.AddDays(-1) > auto.EndDate.Value)
                        {
                            ranges.Add(new DateRange(auto.StartDate.Value, auto.EndDate.Value));
                        }
                        else
                        {
                            ranges.Add(new DateRange(auto.StartDate.Value, inschrijfPeriod.Start.AddDays(-1)));
                        }
                    }
                    if (inschrijfPeriod.Start < auto.StartDate.Value) inschrijfPeriod.Start = auto.StartDate.Value;
                    DateTime yearEnd = new DateTime(auto.StartDate.Value.Year, 12, 31);

                    while (inschrijfPeriod.Start < auto.EndDate.Value)
                    {
                        if (inschrijfPeriod.End > auto.EndDate.Value)
                        {
                            ranges.Add(new DateRange(inschrijfPeriod.Start, auto.EndDate.Value));
                            inschrijfPeriod.Start = auto.EndDate.Value.AddDays(1);
                        }
                        else if (inschrijfPeriod.End > yearEnd)
                        {
                            ranges.Add(new DateRange(inschrijfPeriod.Start, yearEnd));
                            inschrijfPeriod.Start = yearEnd.AddDays(1);
                            yearEnd = yearEnd.AddYears(1);
                        }
                        else
                        {
                            ranges.Add(new DateRange(inschrijfPeriod.Start, inschrijfPeriod.End));
                            inschrijfPeriod = new DateRange(inschrijfPeriod.End.AddDays(1), inschrijfPeriod.End.AddYears(1));
                        }
                    }

                    if (auto.Motortype.Id == new Guid("7A871E25-01DB-4F66-BA3A-8262FEDF4557"))
                    {
                        auto.CO2 = 0;
                    }


                    auto.Details = new List<EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_DetailProratatieAuto>();
                    foreach (DateRange range in ranges)
                    {


                        CoefficientDataContract cdcBasePercentage = wsd.GetCoefficientFromListFullPeriod(coeffs, "VAA_AUTO_BASEPERCENTAGE", range.Start, range.End);
                        CoefficientDataContract cdcMaxCO2Percentage = wsd.GetCoefficientFromListFullPeriod(coeffs, "VAA_AUTO_MAXCO2PERCENTAGE", range.Start, range.End);
                        CoefficientDataContract cdcMinCO2Percentage = wsd.GetCoefficientFromListFullPeriod(coeffs, "VAA_AUTO_MINCO2PERCENTAGE", range.Start, range.End);
                        CoefficientDataContract cdcMinimumVAA = wsd.GetCoefficientFromListFullPeriod(coeffs, "VAA_AUTO_MINVAA", range.Start, range.End);
                        CoefficientDataContract cdcCO2StepPercentage = wsd.GetCoefficientFromListFullPeriod(coeffs, "VAA_AUTO_STEPPERCENTAGE", range.Start, range.End);

                        CoefficientDataContract cdcReferentieCO2 = wsd.GetListItemCoefficientFullPeriod(auto.Motortype.Id, "VAA_AUTO_DIRUPO", null, range.Start, range.End);

                        AY2012.VoordelenAppTypes.VAA_DetailProratatieAuto detail = new EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_DetailProratatieAuto
                        {
                            AgeMonths = inschrijfDate.MonthsBetween(range.End)
                            ,
                            CO2BasePercentage = cdcBasePercentage.Value
                            ,
                            MaxCO2Percentage = cdcMaxCO2Percentage.Value
                            ,
                            MinCO2Percentage = cdcMinCO2Percentage.Value
                            ,
                            MinimumVAA = cdcMinimumVAA.Value
                            ,
                            ReferentieCO2 = cdcReferentieCO2.Value
                            ,
                            StepCO2Percentage = cdcCO2StepPercentage.Value
                            ,
                            CoeffStart = cdcBasePercentage.StartDate.Value
                            ,
                            CoeffEnd = cdcBasePercentage.EndDate.Value
                            ,
                            CoeffDays = (cdcBasePercentage.EndDate.Value - cdcBasePercentage.StartDate.Value).Days + 1
                            ,
                            Days = (range.End - range.Start).Days + 1
                            ,
                            Start = range.Start
                            ,
                            End = range.End
                            ,
                            Result = null
                        };
                        if (detail.End.Value == auto.EndDate.Value && detail.End.Value < wsd.DataContract.EndDate) detail.Days--;


                        decimal percCO2 = detail.CO2BasePercentage.Value + ((auto.CO2.Value - detail.ReferentieCO2.Value) * detail.StepCO2Percentage.Value);
                        if (percCO2 < detail.MinCO2Percentage.Value) percCO2 = detail.MinCO2Percentage.Value;
                        if (percCO2 > detail.MaxCO2Percentage.Value) percCO2 = detail.MaxCO2Percentage.Value;

                        int ageCalcYears = (int)Math.Ceiling(detail.AgeMonths.Value / (decimal)12) - 1;
                        //if (ageCalcYears < 0) ageCalcYears = 0;
                        if (ageCalcYears > 5) ageCalcYears = 5;
                        detail.CatalogusPercentage = ((decimal)100 - (ageCalcYears * (decimal)6));
                        detail.Catalogus = (auto.CatalogusWaarde.Value / (decimal)100) * detail.CatalogusPercentage.Value;
                        detail.Catalogus = Math.Round(detail.Catalogus.Value, 2, MidpointRounding.AwayFromZero);
                        detail.ProratedMinimum = (cdcMinimumVAA.Value / detail.CoeffDays.Value) * detail.Days.Value;
                        detail.Result = (((percCO2 / (decimal)100) * detail.Catalogus.Value * ((decimal)6 / (decimal)7)) / detail.CoeffDays.Value) * detail.Days.Value;
                        if (detail.Result.Value < detail.ProratedMinimum.Value) detail.Result = detail.ProratedMinimum.Value;
                        detail.Result = Math.Round(detail.Result.Value, 2, MidpointRounding.AwayFromZero);
                        detail.ProratedMinimum = Math.Round(detail.ProratedMinimum.Value, 2, MidpointRounding.AwayFromZero);
                        auto.Details.Add(detail);
                        total += detail.Result.Value;

                    }

                    auto.VUWaarde = total * (decimal)0.17;
                    auto.Totaal = total - auto.PersoonlijkeBijdrage.GetValueOrDefault();
                }
                else
                {
                    auto.Details = null;
                    auto.CatalogusWaarde = null;
                    auto.CO2 = null;
                    auto.DatumInschrijving = null;
                    auto.Motortype = null;
                    auto.TransferVU = null;
                    auto.VUWaarde = null;
                    auto.Totaal = auto.WerkelijkeWaarde.GetValueOrDefault() - auto.PersoonlijkeBijdrage.GetValueOrDefault();
                    //auto.VUWaarde = auto.WerkelijkeWaarde.GetValueOrDefault() * (decimal)0.17;
                }

                if (auto.Totaal.Value < 0) auto.Totaal = 0;
                auto.MvH = 0;
                if (auto.BookingType != null)
                {
                    switch (auto.BookingType.Id.ToString().ToLower())
                    {
                        case "4181ec7e-341e-49fd-bdd1-4a8644c75179": // Voordeel
                            auto.MvH = Math.Round((auto.Totaal.Value) / btwVoordeelCoeff, 2, MidpointRounding.AwayFromZero);
                            if (auto.PersoonlijkeBijdrage.HasValue)
                            {
                                if (auto.PersoonlijkeBijdrage.Value >= auto.Totaal.Value)
                                {
                                    auto.Totaal = auto.PersoonlijkeBijdrage.Value;
                                    auto.MvH = Math.Round(auto.Totaal.Value / btwVoordeelCoeffRC, 2, MidpointRounding.AwayFromZero);
                                }
                                else if (auto.PersoonlijkeBijdrage.Value > 0 && auto.PersoonlijkeBijdrage.Value < auto.Totaal.Value)
                                {
                                    auto.MvH = Math.Round(auto.PersoonlijkeBijdrage.Value / btwVoordeelCoeffRC, 2, MidpointRounding.AwayFromZero);
                                    auto.MvH += Math.Round((auto.Totaal.Value - auto.PersoonlijkeBijdrage.Value) / btwVoordeelCoeff, 2, MidpointRounding.AwayFromZero);
                                }
                            }
                            break;
                        default:

                            if (auto.PersoonlijkeBijdrage.HasValue)
                            {
                                if (auto.PersoonlijkeBijdrage.Value >= auto.Totaal.Value)
                                {
                                    auto.Totaal = auto.PersoonlijkeBijdrage.Value;
                                }
                            }
                            auto.MvH = Math.Round(auto.Totaal.Value / btwVoordeelCoeffRC, 2, MidpointRounding.AwayFromZero);
                            break;
                    }
                    auto.BTW = auto.Totaal.Value - auto.MvH.Value;
                }
                auto.LastUpdated = DateTime.Now;


            }


        }

        #endregion

        #region AddUpdateDelete

        
        public void AddToVoordelenVanAlleAardAutoCO2(EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_AutoCO2 entity, ref WorksheetSetData worksheetset)
        {
            entity = Validate(entity, "VoordelenVanAlleAardAutoCO2", ref worksheetset);
            if (entity.IsValid)
            {
                CalculateAutoCO2(entity, ref worksheetset);
                if (entity.Book.HasValue && entity.Book.Value)
                {
                    List<BookingLineDataContract> lines = new List<BookingLineDataContract>();

                    //switch (entity.BookingType.Id.ToString().ToLower())
                    //{
                    //    case "4181ec7e-341e-49fd-bdd1-4a8644c75179": //VOODEEL
                            lines.Add(new BookingLineDataContract { AccountNr = entity.TeBoekenDebetRekening, Amount=entity.Totaal.GetValueOrDefault() });
                            if(entity.BTW.GetValueOrDefault()!=0) lines.Add(new BookingLineDataContract { AccountNr = worksheetset.GetListAccount("4516"), Amount=0-entity.BTW.GetValueOrDefault() });
                            lines.Add(new BookingLineDataContract { AccountNr = entity.GrootBoek, Amount=0-entity.MvH.GetValueOrDefault() });
                    //        break;
                    //    case "15799168-a7c8-4dd3-bb2f-754bea707701": //RC
                    //        lines.Add(new BookingLineDataContract { AccountNr = entity.TeBoekenDebetRekening, Amount=entity.Totaal.GetValueOrDefault() });
                    //        if(entity.BTW.GetValueOrDefault()!=0) lines.Add(new BookingLineDataContract { AccountNr = worksheetset.GetListAccount("4516"), Amount=0-entity.BTW.GetValueOrDefault() });
                    //        lines.Add(new BookingLineDataContract { AccountNr = entity.GrootBoek, Amount=entity.MvH.GetValueOrDefault() });
                    //        break;
                    //    case "eb8181bc-f3e5-4de9-98c5-60f8b91fabff": //DOORREKENING
                    //        lines.Add(new BookingLineDataContract { AccountNr = entity.TeBoekenDebetRekening, Amount=entity.Totaal.GetValueOrDefault() });
                    //        if(entity.BTW.GetValueOrDefault()!=0) lines.Add(new BookingLineDataContract { AccountNr = worksheetset.GetListAccount("4516"), Amount=0-entity.BTW.GetValueOrDefault() });
                    //        lines.Add(new BookingLineDataContract { AccountNr = entity.GrootBoek, Amount=entity.MvH.GetValueOrDefault() });
                    //        break;
                    //}
                   
                    if(lines.Count>0) worksheetset.AddBooking(TypeId, "VoordelenVanAlleAardAutoCO2", entity.Id, null,lines);
                }
                Data.LastUpdated = DateTime.Now;
                Data.VoordelenVanAlleAardAutoCO2.Add(entity);
            }
        }

        public void DeleteVoordelenVanAlleAardAutoCO2(Guid rowId, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_AutoCO2 original = Data.VoordelenVanAlleAardAutoCO2.FirstOrDefault(e => e.Id == rowId);
            if (original != null)
            {
                if (original.Book.HasValue && original.Book.Value)
                {
                    worksheetset.DeleteBooking(TypeId, "VoordelenVanAlleAardAutoCO2", original.Id, null);
                }
                Data.VoordelenVanAlleAardAutoCO2.Remove(original);
                Data.LastUpdated = DateTime.Now;
            }
        }

        public void UpdateVoordelenVanAlleAardAutoCO2(EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_AutoCO2 entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_AutoCO2 original = Data.VoordelenVanAlleAardAutoCO2.FirstOrDefault(e => e.Id == entity.Id);
            if (original == null)
            {
                AddToVoordelenVanAlleAardAutoCO2(entity, ref worksheetset);
            }
            else
            {
                entity = Validate(entity, "VoordelenVanAlleAardAutoCO2", ref worksheetset);
                if (entity.IsValid)
                {
                    CalculateAutoCO2(entity, ref worksheetset);
                    if (original.Book.HasValue && original.Book.Value && (!entity.Book.HasValue || !entity.Book.Value))
                    {
                        worksheetset.DeleteBooking(TypeId, "VoordelenVanAlleAardAutoCO2", original.Id, null);
                    }

                    if (entity.Book.HasValue && entity.Book.Value)
                    {
                        List<BookingLineDataContract> lines = new List<BookingLineDataContract>();

                        //switch (entity.BookingType.Id.ToString().ToLower())
                        //{
                        //    case "4181ec7e-341e-49fd-bdd1-4a8644c75179": //VOODEEL
                        lines.Add(new BookingLineDataContract { AccountNr = entity.TeBoekenDebetRekening, Amount = entity.Totaal.GetValueOrDefault() });
                        if (entity.BTW.GetValueOrDefault() != 0) lines.Add(new BookingLineDataContract { AccountNr = worksheetset.GetListAccount("4516"), Amount = 0 - entity.BTW.GetValueOrDefault() });
                        lines.Add(new BookingLineDataContract { AccountNr = entity.GrootBoek, Amount = 0-entity.MvH.GetValueOrDefault() });
                        //        break;
                        //    case "15799168-a7c8-4dd3-bb2f-754bea707701": //RC
                        //        lines.Add(new BookingLineDataContract { AccountNr = entity.TeBoekenDebetRekening, Amount=entity.Totaal.GetValueOrDefault() });
                        //        if(entity.BTW.GetValueOrDefault()!=0) lines.Add(new BookingLineDataContract { AccountNr = worksheetset.GetListAccount("4516"), Amount=0-entity.BTW.GetValueOrDefault() });
                        //        lines.Add(new BookingLineDataContract { AccountNr = entity.GrootBoek, Amount=entity.MvH.GetValueOrDefault() });
                        //        break;
                        //    case "eb8181bc-f3e5-4de9-98c5-60f8b91fabff": //DOORREKENING
                        //        lines.Add(new BookingLineDataContract { AccountNr = entity.TeBoekenDebetRekening, Amount=entity.Totaal.GetValueOrDefault() });
                        //        if(entity.BTW.GetValueOrDefault()!=0) lines.Add(new BookingLineDataContract { AccountNr = worksheetset.GetListAccount("4516"), Amount=0-entity.BTW.GetValueOrDefault() });
                        //        lines.Add(new BookingLineDataContract { AccountNr = entity.GrootBoek, Amount=entity.MvH.GetValueOrDefault() });
                        //        break;
                        //}

                        if (lines.Count > 0)
                        {
                            if (original.Book.HasValue && original.Book.Value)
                            {
                                worksheetset.UpdateBooking(TypeId, "VoordelenVanAlleAardAutoCO2", entity.Id, null, lines);
                            }
                            else
                            {
                                worksheetset.AddBooking(TypeId, "VoordelenVanAlleAardAutoCO2", entity.Id, null, lines);
                            }
                        } 
                    }
                    int idx = Data.VoordelenVanAlleAardAutoCO2.IndexOf(original);
                    Data.VoordelenVanAlleAardAutoCO2.RemoveAt(idx);
                    Data.VoordelenVanAlleAardAutoCO2.Insert(idx, entity);
                    Data.LastUpdated = DateTime.Now;
                }
            }
        }


        public void AddToVoordelenVanAlleAardAutoRupo(EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_AutoRupo entity, ref WorksheetSetData worksheetset)
        {

            entity = Validate(entity, "VoordelenVanAlleAardAutoRupo", ref worksheetset);
            if (entity.IsValid)
            {
                CalculateAutoRupo(entity, ref worksheetset);

                if (entity.Book.HasValue && entity.Book.Value)
                {
                    List<BookingLineDataContract> lines = new List<BookingLineDataContract>();

                    //switch (entity.BookingType.Id.ToString().ToLower())
                    //{
                    //    case "4181ec7e-341e-49fd-bdd1-4a8644c75179": //VOODEEL
                    lines.Add(new BookingLineDataContract { AccountNr = entity.TeBoekenDebetRekening, Amount = entity.Totaal.GetValueOrDefault() });
                    if (entity.BTW.GetValueOrDefault() != 0) lines.Add(new BookingLineDataContract { AccountNr = worksheetset.GetListAccount("4516"), Amount = 0 - entity.BTW.GetValueOrDefault() });
                    lines.Add(new BookingLineDataContract { AccountNr = entity.GrootBoek, Amount = 0 - entity.MvH.GetValueOrDefault() });
                    //        break;
                    //    case "15799168-a7c8-4dd3-bb2f-754bea707701": //RC
                    //        lines.Add(new BookingLineDataContract { AccountNr = entity.TeBoekenDebetRekening, Amount=entity.Totaal.GetValueOrDefault() });
                    //        if(entity.BTW.GetValueOrDefault()!=0) lines.Add(new BookingLineDataContract { AccountNr = worksheetset.GetListAccount("4516"), Amount=0-entity.BTW.GetValueOrDefault() });
                    //        lines.Add(new BookingLineDataContract { AccountNr = entity.GrootBoek, Amount=entity.MvH.GetValueOrDefault() });
                    //        break;
                    //    case "eb8181bc-f3e5-4de9-98c5-60f8b91fabff": //DOORREKENING
                    //        lines.Add(new BookingLineDataContract { AccountNr = entity.TeBoekenDebetRekening, Amount=entity.Totaal.GetValueOrDefault() });
                    //        if(entity.BTW.GetValueOrDefault()!=0) lines.Add(new BookingLineDataContract { AccountNr = worksheetset.GetListAccount("4516"), Amount=0-entity.BTW.GetValueOrDefault() });
                    //        lines.Add(new BookingLineDataContract { AccountNr = entity.GrootBoek, Amount=entity.MvH.GetValueOrDefault() });
                    //        break;
                    //}

                    if (lines.Count > 0) worksheetset.AddBooking(TypeId, "VoordelenVanAlleAardAutoRupo", entity.Id, null, lines);
                }

                Data.LastUpdated = DateTime.Now;
                Data.VoordelenVanAlleAardAutoRupo.Add(entity);
            }
        }

        public void DeleteVoordelenVanAlleAardAutoRupo(Guid rowId, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_AutoRupo original = Data.VoordelenVanAlleAardAutoRupo.FirstOrDefault(e => e.Id == rowId);
            if (original != null)
            {

                if (original.Book.HasValue && original.Book.Value)
                {
                    worksheetset.DeleteBooking(TypeId, "VoordelenVanAlleAardAutoRupo", original.Id, null);
                }

                Data.VoordelenVanAlleAardAutoRupo.Remove(original);
                Data.LastUpdated = DateTime.Now;
            }
        }

        public void UpdateVoordelenVanAlleAardAutoRupo(EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_AutoRupo entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_AutoRupo original = Data.VoordelenVanAlleAardAutoRupo.FirstOrDefault(e => e.Id == entity.Id);
            if (original == null)
            {
                AddToVoordelenVanAlleAardAutoRupo(entity, ref worksheetset);
            }
            else
            {
                entity = Validate(entity, "VoordelenVanAlleAardAutoRupo", ref worksheetset);
                if (entity.IsValid)
                {
                    CalculateAutoRupo(entity, ref worksheetset);

                    if (original.Book.HasValue && original.Book.Value && (!entity.Book.HasValue || !entity.Book.Value))
                    {
                        worksheetset.DeleteBooking(TypeId, "VoordelenVanAlleAardAutoRupo", original.Id, null);
                    }

                    if (entity.Book.HasValue && entity.Book.Value)
                    {
                        List<BookingLineDataContract> lines = new List<BookingLineDataContract>();

                        //switch (entity.BookingType.Id.ToString().ToLower())
                        //{
                        //    case "4181ec7e-341e-49fd-bdd1-4a8644c75179": //VOODEEL
                        lines.Add(new BookingLineDataContract { AccountNr = entity.TeBoekenDebetRekening, Amount = entity.Totaal.GetValueOrDefault() });
                        if (entity.BTW.GetValueOrDefault() != 0) lines.Add(new BookingLineDataContract { AccountNr = worksheetset.GetListAccount("4516"), Amount = 0 - entity.BTW.GetValueOrDefault() });
                        lines.Add(new BookingLineDataContract { AccountNr = entity.GrootBoek, Amount = 0 - entity.MvH.GetValueOrDefault() });
                        //        break;
                        //    case "15799168-a7c8-4dd3-bb2f-754bea707701": //RC
                        //        lines.Add(new BookingLineDataContract { AccountNr = entity.TeBoekenDebetRekening, Amount=entity.Totaal.GetValueOrDefault() });
                        //        if(entity.BTW.GetValueOrDefault()!=0) lines.Add(new BookingLineDataContract { AccountNr = worksheetset.GetListAccount("4516"), Amount=0-entity.BTW.GetValueOrDefault() });
                        //        lines.Add(new BookingLineDataContract { AccountNr = entity.GrootBoek, Amount=entity.MvH.GetValueOrDefault() });
                        //        break;
                        //    case "eb8181bc-f3e5-4de9-98c5-60f8b91fabff": //DOORREKENING
                        //        lines.Add(new BookingLineDataContract { AccountNr = entity.TeBoekenDebetRekening, Amount=entity.Totaal.GetValueOrDefault() });
                        //        if(entity.BTW.GetValueOrDefault()!=0) lines.Add(new BookingLineDataContract { AccountNr = worksheetset.GetListAccount("4516"), Amount=0-entity.BTW.GetValueOrDefault() });
                        //        lines.Add(new BookingLineDataContract { AccountNr = entity.GrootBoek, Amount=entity.MvH.GetValueOrDefault() });
                        //        break;
                        //}

                        if (lines.Count > 0)
                        {
                            if (original.Book.HasValue && original.Book.Value)
                            {
                                worksheetset.UpdateBooking(TypeId, "VoordelenVanAlleAardAutoRupo", entity.Id, null, lines);
                            }
                            else
                            {
                                worksheetset.AddBooking(TypeId, "VoordelenVanAlleAardAutoRupo", entity.Id, null, lines);
                            }
                        }
                    }

                    int idx = Data.VoordelenVanAlleAardAutoRupo.IndexOf(original);
                    Data.VoordelenVanAlleAardAutoRupo.RemoveAt(idx);
                    Data.VoordelenVanAlleAardAutoRupo.Insert(idx, entity);
                    Data.LastUpdated = DateTime.Now;
                }
            }
        }


        public void AddToVoordelenVanAlleAardNuts(EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_Nuts entity, ref WorksheetSetData worksheetset)
        {
            entity = Validate(entity, "VoordelenVanAlleAardNuts", ref worksheetset);
            if (entity.IsValid)
            {
                CalculateNuts(entity, ref worksheetset);
                if (entity.Book.HasValue && entity.Book.Value)
                {
                    List<BookingLineDataContract> lines = new List<BookingLineDataContract>() {
                            new BookingLineDataContract { AccountNr = entity.DebetRekening, Amount=entity.Totaal.GetValueOrDefault() }
                            ,new BookingLineDataContract { AccountNr = entity.GrootBoek, Amount=0-entity.MvH.GetValueOrDefault() }
                        };
                    if (entity.BTW.GetValueOrDefault() != 0) lines.Add(new BookingLineDataContract { AccountNr = worksheetset.GetListAccount("4516"), Amount = 0 - entity.BTW.GetValueOrDefault() });
                    worksheetset.AddBooking(TypeId, "VoordelenVanAlleAardNuts", entity.Id, null, lines);
                }
                Data.LastUpdated = DateTime.Now;
                Data.VoordelenVanAlleAardNuts.Add(entity);
            }
        }

        public void DeleteVoordelenVanAlleAardNuts(Guid rowId, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_Nuts original = Data.VoordelenVanAlleAardNuts.FirstOrDefault(e => e.Id == rowId);
            if (original != null)
            {

                if (original.Book.HasValue && original.Book.Value)
                {
                    worksheetset.DeleteBooking(TypeId, "VoordelenVanAlleAardNuts", original.Id, null);
                    // DELETE BOOKING
                }
                Data.VoordelenVanAlleAardNuts.Remove(original);
                Data.LastUpdated = DateTime.Now;
            }
        }

        public void UpdateVoordelenVanAlleAardNuts(EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_Nuts entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_Nuts original = Data.VoordelenVanAlleAardNuts.FirstOrDefault(e => e.Id == entity.Id);
            if (original == null)
            {
                AddToVoordelenVanAlleAardNuts(entity, ref worksheetset);
            }
            else
            {
                entity = Validate(entity, "VoordelenVanAlleAardNuts", ref worksheetset);
                if (entity.IsValid)
                {
                    CalculateNuts(entity, ref worksheetset);

                    if (entity.Book.HasValue && entity.Book.Value)
                    {
                        List<BookingLineDataContract> lines = new List<BookingLineDataContract>() {
                            new BookingLineDataContract { AccountNr = entity.DebetRekening, Amount=entity.Totaal.GetValueOrDefault() }
                            ,new BookingLineDataContract { AccountNr = entity.GrootBoek, Amount=0-entity.MvH.GetValueOrDefault() }
                        };
                        if (entity.BTW.GetValueOrDefault() != 0) lines.Add(new BookingLineDataContract { AccountNr = worksheetset.GetListAccount("4516"), Amount = 0 - entity.BTW.GetValueOrDefault() });
                        if (original.Book.HasValue && original.Book.Value)
                        {
                            worksheetset.UpdateBooking(TypeId, "VoordelenVanAlleAardNuts", entity.Id, null, lines);
                        }
                        else
                        {
                            worksheetset.AddBooking(TypeId, "VoordelenVanAlleAardNuts", entity.Id, null, lines);
                        }
                    }
                    else
                    {
                        if (original.Book.HasValue && original.Book.Value)
                        {
                            worksheetset.DeleteBooking(TypeId, "VoordelenVanAlleAardNuts", original.Id, null);
                            // DELETE BOOKING
                        }
                    }

                    int idx = Data.VoordelenVanAlleAardNuts.IndexOf(original);
                    Data.VoordelenVanAlleAardNuts.RemoveAt(idx);
                    Data.VoordelenVanAlleAardNuts.Insert(idx, entity);
                    Data.LastUpdated = DateTime.Now;
                }
            }
        }

        public void AddToVoordelenVanAlleAardHuur(EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_HUUR entity, ref WorksheetSetData worksheetset)
        {
            entity = Validate(entity, "VoordelenVanAlleAardHuur", ref worksheetset);
            if (entity.IsValid)
            {
                CalculateHuur(entity, ref worksheetset);
                if (entity.Book.HasValue && entity.Book.Value)
                {
                    List<BookingLineDataContract> lines = new List<BookingLineDataContract>() {
                            new BookingLineDataContract { AccountNr = entity.DebetRekening, Amount=entity.Totaal.GetValueOrDefault() }
                            ,new BookingLineDataContract { AccountNr = entity.GrootBoek, Amount=0-entity.Totaal.GetValueOrDefault() }
                        };
                    worksheetset.AddBooking(TypeId, "VoordelenVanAlleAardHuur", entity.Id, null, lines);
                }
                Data.LastUpdated = DateTime.Now;
                Data.VoordelenVanAlleAardHuur.Add(entity);
            }
        }

        public void DeleteVoordelenVanAlleAardHuur(Guid rowId, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_HUUR original = Data.VoordelenVanAlleAardHuur.FirstOrDefault(e => e.Id == rowId);
            if (original != null)
            {
                
                if (original.Book.HasValue && original.Book.Value)
                {
                    worksheetset.DeleteBooking(TypeId, "VoordelenVanAlleAardHuur", original.Id, null);
                    // DELETE BOOKING
                }
                Data.VoordelenVanAlleAardHuur.Remove(original);
                Data.LastUpdated = DateTime.Now;
            }
        }

        public void UpdateVoordelenVanAlleAardHuur(EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_HUUR entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_HUUR original = Data.VoordelenVanAlleAardHuur.FirstOrDefault(e => e.Id == entity.Id);
            if (original == null)
            {
                AddToVoordelenVanAlleAardHuur(entity, ref worksheetset);
            }
            else
            {
                entity = Validate(entity, "VoordelenVanAlleAardHuur", ref worksheetset);
                if (entity.IsValid)
                {
                    CalculateHuur(entity, ref worksheetset);
                    if (entity.Book.HasValue && entity.Book.Value)
                    {
                        List<BookingLineDataContract> lines = new List<BookingLineDataContract>() {
                            new BookingLineDataContract { AccountNr = entity.DebetRekening, Amount=entity.Totaal.GetValueOrDefault() }
                            ,new BookingLineDataContract { AccountNr = entity.GrootBoek, Amount=0-entity.Totaal.GetValueOrDefault() }
                        };
                        if (original.Book.HasValue && original.Book.Value)
                        {
                            worksheetset.UpdateBooking(TypeId, "VoordelenVanAlleAardHuur", entity.Id, null, lines);
                        }
                        else
                        {
                            worksheetset.AddBooking(TypeId, "VoordelenVanAlleAardHuur", entity.Id, null, lines);
                        }
                    }
                    else
                    {
                        if (original.Book.HasValue && original.Book.Value)
                        {
                            worksheetset.DeleteBooking(TypeId, "VoordelenVanAlleAardHuur", original.Id, null);
                            // DELETE BOOKING
                        }
                    }
                    int idx = Data.VoordelenVanAlleAardHuur.IndexOf(original);
                    Data.VoordelenVanAlleAardHuur.RemoveAt(idx);
                    Data.VoordelenVanAlleAardHuur.Insert(idx, entity);
                    Data.LastUpdated = DateTime.Now;
                }
            }
        }

        public void AddToVoordelenVanAlleAardHuurHerkwalificatie(EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_HUURHERK entity, ref WorksheetSetData worksheetset)
        {
            entity = Validate(entity, "VoordelenVanAlleAardHuurHerkwalificatie", ref worksheetset);
            if (entity.IsValid)
            {
                CalculateHuurHerKw(entity, ref worksheetset);
                if (entity.Book.HasValue && entity.Book.Value)
                {
                    List<BookingLineDataContract> lines = new List<BookingLineDataContract>() {
                            new BookingLineDataContract { AccountNr = entity.DebetRekening, Amount=entity.Herkwalificatie.GetValueOrDefault() }
                            ,new BookingLineDataContract { AccountNr = entity.GrootBoek, Amount=0-entity.Herkwalificatie.GetValueOrDefault() }
                        };
                    worksheetset.AddBooking(TypeId, "VoordelenVanAlleAardHuurHerkwalificatie", entity.Id, null, lines);
                }
                Data.LastUpdated = DateTime.Now;
                
                Data.VoordelenVanAlleAardHuurHerkwalificatie.Add(entity);
            }
        }

        public void DeleteVoordelenVanAlleAardHuurHerkwalificatie(Guid rowId, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_HUURHERK original = Data.VoordelenVanAlleAardHuurHerkwalificatie.FirstOrDefault(e => e.Id == rowId);
            if (original != null)
            {

                if (original.Book.HasValue && original.Book.Value)
                {
                    worksheetset.DeleteBooking(TypeId, "VoordelenVanAlleAardHuurHerkwalificatie", original.Id, null);
                    // DELETE BOOKING
                }
                Data.VoordelenVanAlleAardHuurHerkwalificatie.Remove(original);
                Data.LastUpdated = DateTime.Now;
            }
        }

        public void UpdateVoordelenVanAlleAardHuurHerkwalificatie(EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_HUURHERK entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_HUURHERK original = Data.VoordelenVanAlleAardHuurHerkwalificatie.FirstOrDefault(e => e.Id == entity.Id);
            if (original == null)
            {
                AddToVoordelenVanAlleAardHuurHerkwalificatie(entity, ref worksheetset);
            }
            else
            {
                entity = Validate(entity, "VoordelenVanAlleAardHuurHerkwalificatie", ref worksheetset);
                if (entity.IsValid)
                {
                    CalculateHuurHerKw(entity, ref worksheetset);
                    if (entity.Book.HasValue && entity.Book.Value)
                    {
                        List<BookingLineDataContract> lines = new List<BookingLineDataContract>() {
                            new BookingLineDataContract { AccountNr = entity.DebetRekening, Amount=entity.Herkwalificatie.GetValueOrDefault() }
                            ,new BookingLineDataContract { AccountNr = entity.GrootBoek, Amount=0-entity.Herkwalificatie.GetValueOrDefault() }
                        };
                        if (original.Book.HasValue && original.Book.Value)
                        {
                            worksheetset.UpdateBooking(TypeId, "VoordelenVanAlleAardHuurHerkwalificatie", entity.Id, null, lines);
                        }
                        else
                        {
                            worksheetset.AddBooking(TypeId, "VoordelenVanAlleAardHuurHerkwalificatie", entity.Id, null, lines);
                        }
                    }
                    else
                    {
                        if (original.Book.HasValue && original.Book.Value)
                        {
                            worksheetset.DeleteBooking(TypeId, "VoordelenVanAlleAardHuurHerkwalificatie", original.Id, null);
                            // DELETE BOOKING
                        }
                    }
                    int idx = Data.VoordelenVanAlleAardHuurHerkwalificatie.IndexOf(original);
                    Data.VoordelenVanAlleAardHuurHerkwalificatie.RemoveAt(idx);
                    Data.VoordelenVanAlleAardHuurHerkwalificatie.Insert(idx, entity);
                    Data.LastUpdated = DateTime.Now;
                    
                }
            }
        }

        public void AddToVoordelenVanAlleAardPC(EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_PC entity, ref WorksheetSetData worksheetset)
        {
            entity = Validate(entity, "VoordelenVanAlleAardPC", ref worksheetset);
            if (entity.IsValid)
            {
                CalculatePC(entity, ref worksheetset);
                if (entity.Book.HasValue && entity.Book.Value)
                {
                    List<BookingLineDataContract> lines = new List<BookingLineDataContract>() {
                            new BookingLineDataContract { AccountNr = entity.DebetRekening, Amount=entity.Totaal.GetValueOrDefault() }
                            ,new BookingLineDataContract { AccountNr = entity.GrootBoek, Amount=0-entity.MvH.GetValueOrDefault() }
                        };
                    if (entity.BTW.GetValueOrDefault() != 0) lines.Add(new BookingLineDataContract { AccountNr = worksheetset.GetListAccount("4516"), Amount = 0 - entity.BTW.GetValueOrDefault() });
                    worksheetset.AddBooking(TypeId, "VoordelenVanAlleAardPC", entity.Id, null, lines);
                }
                Data.LastUpdated = DateTime.Now;
                
                Data.VoordelenVanAlleAardPC.Add(entity);
            }
        }

        public void DeleteVoordelenVanAlleAardPC(Guid rowId, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_PC original = Data.VoordelenVanAlleAardPC.FirstOrDefault(e => e.Id == rowId);
            if (original != null)
            {
                
                if (original.Book.HasValue && original.Book.Value)
                {
                    worksheetset.DeleteBooking(TypeId, "VoordelenVanAlleAardPC", original.Id, null);
                    // DELETE BOOKING
                }
                Data.VoordelenVanAlleAardPC.Remove(original);
                Data.LastUpdated = DateTime.Now;
            }
        }

        public void UpdateVoordelenVanAlleAardPC(EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_PC entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_PC original = Data.VoordelenVanAlleAardPC.FirstOrDefault(e => e.Id == entity.Id);
            if (original == null)
            {
                AddToVoordelenVanAlleAardPC(entity, ref worksheetset);
            }
            else
            {
                entity = Validate(entity, "VoordelenVanAlleAardPC", ref worksheetset);
                if (entity.IsValid)
                {
                    CalculatePC(entity, ref worksheetset);
                    if (entity.Book.HasValue && entity.Book.Value)
                    {
                        List<BookingLineDataContract> lines = new List<BookingLineDataContract>() {
                            new BookingLineDataContract { AccountNr = entity.DebetRekening, Amount=entity.Totaal.GetValueOrDefault() }
                            ,new BookingLineDataContract { AccountNr = entity.GrootBoek, Amount=0-entity.MvH.GetValueOrDefault() }
                        };
                        if (entity.BTW.GetValueOrDefault() != 0) lines.Add(new BookingLineDataContract { AccountNr = worksheetset.GetListAccount("4516"), Amount = 0 - entity.BTW.GetValueOrDefault() });
                        if (original.Book.HasValue && original.Book.Value)
                        {
                            worksheetset.UpdateBooking(TypeId, "VoordelenVanAlleAardPC", entity.Id, null, lines);
                        }
                        else
                        {
                            worksheetset.AddBooking(TypeId, "VoordelenVanAlleAardPC", entity.Id, null, lines);
                        }
                    }
                    else
                    {
                        if (original.Book.HasValue && original.Book.Value)
                        {
                            worksheetset.DeleteBooking(TypeId, "VoordelenVanAlleAardPC", original.Id, null);
                            // DELETE BOOKING
                        }
                    }
                    int idx = Data.VoordelenVanAlleAardPC.IndexOf(original);
                    Data.VoordelenVanAlleAardPC.RemoveAt(idx);
                    Data.VoordelenVanAlleAardPC.Insert(idx, entity);
                    Data.LastUpdated = DateTime.Now;
                    
                }
            }
        }

        public void AddToVoordelenVanAlleAardTelefonie(EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_Telefonie entity, ref WorksheetSetData worksheetset)
        {
            entity = Validate(entity, "VoordelenVanAlleAardTelefonie", ref worksheetset);
            if (entity.IsValid)
            {
                CalculateTelefonie(entity, ref worksheetset);
                if (entity.Book.HasValue && entity.Book.Value)
                {
                    List<BookingLineDataContract> lines = new List<BookingLineDataContract>() {
                            new BookingLineDataContract { AccountNr = entity.DebetRekening, Amount=entity.Totaal.GetValueOrDefault() }
                            ,new BookingLineDataContract { AccountNr = entity.GrootBoek, Amount=0-entity.MvH.GetValueOrDefault() }
                        };
                    if (entity.BTW.GetValueOrDefault() != 0) lines.Add(new BookingLineDataContract { AccountNr = worksheetset.GetListAccount("4516"), Amount = 0 - entity.BTW.GetValueOrDefault() });
                    worksheetset.AddBooking(TypeId, "VoordelenVanAlleAardTelefonie", entity.Id, null, lines);
                    
                }
                Data.LastUpdated = DateTime.Now;
                Data.VoordelenVanAlleAardTelefonie.Add(entity);
            }
        }

        public void DeleteVoordelenVanAlleAardTelefonie(Guid rowId, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_Telefonie original = Data.VoordelenVanAlleAardTelefonie.FirstOrDefault(e => e.Id == rowId);
            if (original != null)
            {
                
                if (original.Book.HasValue && original.Book.Value)
                {
                    worksheetset.DeleteBooking(TypeId, "VoordelenVanAlleAardTelefonie", original.Id, null);
                    // DELETE BOOKING
                }
                
                Data.VoordelenVanAlleAardTelefonie.Remove(original);
                Data.LastUpdated = DateTime.Now;
            }
        }

        public void UpdateVoordelenVanAlleAardTelefonie(EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_Telefonie entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_Telefonie original = Data.VoordelenVanAlleAardTelefonie.FirstOrDefault(e => e.Id == entity.Id);
            if (original == null)
            {
                AddToVoordelenVanAlleAardTelefonie(entity, ref worksheetset);
            }
            else
            {
                entity = Validate(entity, "VoordelenVanAlleAardTelefonie", ref worksheetset);
                if (entity.IsValid)
                {
                    CalculateTelefonie(entity, ref worksheetset);
                    if (entity.Book.HasValue && entity.Book.Value)
                    {
                        List<BookingLineDataContract> lines = new List<BookingLineDataContract>() {
                            new BookingLineDataContract { AccountNr = entity.DebetRekening, Amount=entity.Totaal.GetValueOrDefault() }
                            ,new BookingLineDataContract { AccountNr = entity.GrootBoek, Amount=0-entity.MvH.GetValueOrDefault() }
                        };
                        if (entity.BTW.GetValueOrDefault() != 0) lines.Add(new BookingLineDataContract { AccountNr = worksheetset.GetListAccount("4516"), Amount = 0 - entity.BTW.GetValueOrDefault() });
                        if (original.Book.HasValue && original.Book.Value)
                        {
                            worksheetset.UpdateBooking(TypeId, "VoordelenVanAlleAardTelefonie", entity.Id, null, lines);
                        }
                        else
                        {
                            worksheetset.AddBooking(TypeId, "VoordelenVanAlleAardTelefonie", entity.Id, null, lines);
                        }
                    }
                    else
                    {
                        if (original.Book.HasValue && original.Book.Value)
                        {
                            worksheetset.DeleteBooking(TypeId, "VoordelenVanAlleAardTelefonie", original.Id, null);
                            // DELETE BOOKING
                        }
                    }
                    int idx = Data.VoordelenVanAlleAardTelefonie.IndexOf(original);
                    Data.VoordelenVanAlleAardTelefonie.RemoveAt(idx);
                    Data.VoordelenVanAlleAardTelefonie.Insert(idx, entity);
                    Data.LastUpdated = DateTime.Now;
                }
            }
        }

        public void AddToVoordelenVanAlleAardOverige(EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_Overige entity, ref WorksheetSetData worksheetset)
        {
            entity = Validate(entity, "VoordelenVanAlleAardOverige", ref worksheetset);
            if (entity.IsValid)
            {
                CalculateOverige(entity, ref worksheetset);
                if (entity.Book.HasValue && entity.Book.Value)
                {
                    List<BookingLineDataContract> lines = new List<BookingLineDataContract>() {
                            new BookingLineDataContract { AccountNr = entity.DebetRekening, Amount=entity.Totaal.GetValueOrDefault() }
                            ,new BookingLineDataContract { AccountNr = entity.GrootBoek, Amount=0-entity.MvH.GetValueOrDefault() }
                        };
                    if (entity.BTWBedrag.GetValueOrDefault() != 0) lines.Add(new BookingLineDataContract { AccountNr = worksheetset.GetListAccount("4516"), Amount = 0 - entity.BTWBedrag.GetValueOrDefault() });
                    worksheetset.AddBooking(TypeId, "VoordelenVanAlleAardOverige", entity.Id, null, lines);
                    
                }
                Data.LastUpdated = DateTime.Now;
                Data.VoordelenVanAlleAardOverige.Add(entity);
            }
        }

        public void DeleteVoordelenVanAlleAardOverige(Guid rowId, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_Overige original = Data.VoordelenVanAlleAardOverige.FirstOrDefault(e => e.Id == rowId);
            if (original != null)
            {
                if (original.Book.HasValue && original.Book.Value)
                {
                    worksheetset.DeleteBooking(TypeId, "VoordelenVanAlleAardOverige", original.Id, null);
                    // DELETE BOOKING
                }
                
                Data.VoordelenVanAlleAardOverige.Remove(original);
                Data.LastUpdated = DateTime.Now;
            }
        }

        public void UpdateVoordelenVanAlleAardOverige(EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_Overige entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_Overige original = Data.VoordelenVanAlleAardOverige.FirstOrDefault(e => e.Id == entity.Id);
            if (original == null)
            {
                AddToVoordelenVanAlleAardOverige(entity, ref worksheetset);
            }
            else
            {
                entity = Validate(entity, "VoordelenVanAlleAardOverige", ref worksheetset);
                if (entity.IsValid)
                {
                    CalculateOverige(entity, ref worksheetset);
                    if (entity.Book.HasValue && entity.Book.Value)
                    {
                        List<BookingLineDataContract> lines = new List<BookingLineDataContract>() {
                            new BookingLineDataContract { AccountNr = entity.DebetRekening, Amount=entity.Totaal.GetValueOrDefault() }
                            ,new BookingLineDataContract { AccountNr = entity.GrootBoek, Amount=0-entity.MvH.GetValueOrDefault() }
                        };
                        if (entity.BTWBedrag.GetValueOrDefault() != 0) lines.Add(new BookingLineDataContract { AccountNr = worksheetset.GetListAccount("4516"), Amount = 0 - entity.BTWBedrag.GetValueOrDefault() });
                        if (original.Book.HasValue && original.Book.Value)
                        {
                            worksheetset.UpdateBooking(TypeId, "VoordelenVanAlleAardOverige", entity.Id, null, lines);
                        }
                        else
                        {
                            worksheetset.AddBooking(TypeId, "VoordelenVanAlleAardOverige", entity.Id, null, lines);
                        }
                    }
                    else
                    {
                        if (original.Book.HasValue && original.Book.Value)
                        {
                            worksheetset.DeleteBooking(TypeId, "VoordelenVanAlleAardOverige", original.Id, null);
                            // DELETE BOOKING
                        }
                    }
                    int idx = Data.VoordelenVanAlleAardOverige.IndexOf(original);
                    Data.VoordelenVanAlleAardOverige.RemoveAt(idx);
                    Data.VoordelenVanAlleAardOverige.Insert(idx, entity);
                    Data.LastUpdated = DateTime.Now;
                    
                }
            }
        }

        public void AddToVoordelenInterestenRCBedrijfsleider(EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_Interesten_RC_Bedrijfsleider entity, ref WorksheetSetData worksheetset)
        {
            entity = Validate(entity, "VoordelenInterestenRCBedrijfsleider", ref worksheetset);
            if (entity.IsValid)
            {
                CalculateIntrestenBedrijfsleider(entity, ref worksheetset);
                if (entity.Book.HasValue && entity.Book.Value)
                {
                    List<BookingLineDataContract> lines = new List<BookingLineDataContract>() {
                            new BookingLineDataContract { AccountNr = entity.GrootBoek, Amount=entity.Totaal.GetValueOrDefault() }
                            ,new BookingLineDataContract { AccountNr = entity.CreditRekening, Amount=0-entity.MvH.GetValueOrDefault() }
                        };
                    if (entity.RVPerc == 15) lines.Add(new BookingLineDataContract { AccountNr = worksheetset.GetListAccount("4533"), Amount = 0 - entity.RV.GetValueOrDefault() });
                    if (entity.RVPerc == 25) lines.Add(new BookingLineDataContract { AccountNr = worksheetset.GetListAccount("4532"), Amount = 0 - entity.RV.GetValueOrDefault() });
                    worksheetset.AddBooking(TypeId, "VoordelenInterestenRCBedrijfsleider", entity.Id, null, lines);
                    
                }
                Data.LastUpdated = DateTime.Now;
                
                Data.VoordelenInterestenRCBedrijfsleider.Add(entity);
            }
        }

        public void DeleteVoordelenInterestenRCBedrijfsleider(Guid rowId, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_Interesten_RC_Bedrijfsleider original = Data.VoordelenInterestenRCBedrijfsleider.FirstOrDefault(e => e.Id == rowId);
            if (original != null)
            {
                 if (original.Book.HasValue && original.Book.Value)
                    {
                        worksheetset.DeleteBooking(TypeId, "VoordelenInterestenRCBedrijfsleider", original.Id, null);
                        // DELETE BOOKING
                    }
                Data.VoordelenInterestenRCBedrijfsleider.Remove(original);
                Data.LastUpdated = DateTime.Now;
            }
        }

        public void UpdateVoordelenInterestenRCBedrijfsleider(EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_Interesten_RC_Bedrijfsleider entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_Interesten_RC_Bedrijfsleider original = Data.VoordelenInterestenRCBedrijfsleider.FirstOrDefault(e => e.Id == entity.Id);
            if (original == null)
            {
                AddToVoordelenInterestenRCBedrijfsleider(entity, ref worksheetset);
            }
            else
            {
                entity = Validate(entity, "VoordelenInterestenRCBedrijfsleider", ref worksheetset);
                if (entity.IsValid)
                {
                    CalculateIntrestenBedrijfsleider(entity, ref worksheetset);
                    if (entity.Book.HasValue && entity.Book.Value)
                    {
                        List<BookingLineDataContract> lines = new List<BookingLineDataContract>() {
                            new BookingLineDataContract { AccountNr = entity.GrootBoek, Amount=entity.Totaal.GetValueOrDefault() }
                            ,new BookingLineDataContract { AccountNr = entity.CreditRekening, Amount=0-entity.MvH.GetValueOrDefault() }
                        };
                        if (entity.RVPerc == 15) lines.Add(new BookingLineDataContract { AccountNr = worksheetset.GetListAccount("4533"), Amount = 0 - entity.RV.GetValueOrDefault() });
                        if (entity.RVPerc == 25) lines.Add(new BookingLineDataContract { AccountNr = worksheetset.GetListAccount("4532"), Amount = 0 - entity.RV.GetValueOrDefault() });
                        if (original.Book.HasValue && original.Book.Value)
                        {
                            worksheetset.UpdateBooking(TypeId, "VoordelenInterestenRCBedrijfsleider", entity.Id, null, lines);
                        }
                        else
                        {
                            worksheetset.AddBooking(TypeId, "VoordelenInterestenRCBedrijfsleider", entity.Id, null, lines);
                        }
                    }
                    else
                    {
                        if (original.Book.HasValue && original.Book.Value)
                        {
                            worksheetset.DeleteBooking(TypeId, "VoordelenInterestenRCBedrijfsleider", original.Id, null);
                            // DELETE BOOKING
                        }
                    }
                    int idx = Data.VoordelenInterestenRCBedrijfsleider.IndexOf(original);
                    Data.VoordelenInterestenRCBedrijfsleider.RemoveAt(idx);
                    Data.VoordelenInterestenRCBedrijfsleider.Insert(idx, entity);
                    Data.LastUpdated = DateTime.Now;
                }
            }
        }

        public void AddToVoordelenSocialeBijdragenBedrijfsleider(EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_Soc_Bijdr_Bedrijfsleider entity, ref WorksheetSetData worksheetset)
        {
            entity = Validate(entity, "VoordelenSocialeBijdragenBedrijfsleider", ref worksheetset);
            if (entity.IsValid)
            {
                CalculateSocialeBijdragenBedrijfsleider(entity, ref worksheetset);
                if (entity.Book.HasValue && entity.Book.Value)
                {
                    List<BookingLineDataContract> lines = new List<BookingLineDataContract>() {
                            new BookingLineDataContract { AccountNr = entity.DebetRekening, Amount=entity.Totaal.GetValueOrDefault() }
                            ,new BookingLineDataContract { AccountNr = entity.GrootBoek, Amount=0-entity.Totaal.GetValueOrDefault() }
                        };
                    worksheetset.AddBooking(TypeId, "VoordelenSocialeBijdragenBedrijfsleider", entity.Id, null, lines);
                }
                Data.LastUpdated = DateTime.Now;
                
                Data.VoordelenSocialeBijdragenBedrijfsleider.Add(entity);
            }
        }

        public void DeleteVoordelenSocialeBijdragenBedrijfsleider(Guid rowId, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_Soc_Bijdr_Bedrijfsleider original = Data.VoordelenSocialeBijdragenBedrijfsleider.FirstOrDefault(e => e.Id == rowId);
            if (original != null)
            {
                if (original.Book.HasValue && original.Book.Value)
                    {
                        worksheetset.DeleteBooking(TypeId, "VoordelenSocialeBijdragenBedrijfsleider", original.Id, null);
                        // DELETE BOOKING
                    }
                
                Data.VoordelenSocialeBijdragenBedrijfsleider.Remove(original);
                Data.LastUpdated = DateTime.Now;
            }
        }

        public void UpdateVoordelenSocialeBijdragenBedrijfsleider(EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_Soc_Bijdr_Bedrijfsleider entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_Soc_Bijdr_Bedrijfsleider original = Data.VoordelenSocialeBijdragenBedrijfsleider.FirstOrDefault(e => e.Id == entity.Id);
            if (original == null)
            {
                AddToVoordelenSocialeBijdragenBedrijfsleider(entity, ref worksheetset);
            }
            else
            {
                entity = Validate(entity, "VoordelenSocialeBijdragenBedrijfsleider", ref worksheetset);
                if (entity.IsValid)
                {
                    CalculateSocialeBijdragenBedrijfsleider(entity, ref worksheetset);
                    if (entity.Book.HasValue && entity.Book.Value)
                    {
                        List<BookingLineDataContract> lines = new List<BookingLineDataContract>() {
                            new BookingLineDataContract { AccountNr = entity.DebetRekening, Amount=entity.Totaal.GetValueOrDefault() }
                            ,new BookingLineDataContract { AccountNr = entity.GrootBoek, Amount=0-entity.Totaal.GetValueOrDefault() }
                        };
                        if (original.Book.HasValue && original.Book.Value)
                        {
                            worksheetset.UpdateBooking(TypeId, "VoordelenSocialeBijdragenBedrijfsleider", entity.Id, null, lines);
                        }
                        else
                        {
                            worksheetset.AddBooking(TypeId, "VoordelenSocialeBijdragenBedrijfsleider", entity.Id, null, lines);
                        }
                    }
                    else
                    {
                        if (original.Book.HasValue && original.Book.Value)
                        {
                            worksheetset.DeleteBooking(TypeId, "VoordelenSocialeBijdragenBedrijfsleider", original.Id, null);
                            // DELETE BOOKING
                        }
                    }
                    int idx = Data.VoordelenSocialeBijdragenBedrijfsleider.IndexOf(original);
                    Data.VoordelenSocialeBijdragenBedrijfsleider.RemoveAt(idx);
                    Data.VoordelenSocialeBijdragenBedrijfsleider.Insert(idx, entity);
                    Data.LastUpdated = DateTime.Now;
                }
            }
        }

        public void AddToVoordelenOverigeNuts(EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_Overige_Nuts entity, ref WorksheetSetData worksheetset)
        {
            entity = Validate(entity, "VoordelenOverigeNuts", ref worksheetset);
            if (entity.IsValid)
            {
                CalculateOverigeNuts(entity, ref worksheetset);
                if (entity.Book.HasValue && entity.Book.Value)
                {
                    List<BookingLineDataContract> lines = new List<BookingLineDataContract>() {
                            new BookingLineDataContract { AccountNr = entity.DebetRekening, Amount=entity.Totaal.GetValueOrDefault() }
                            ,new BookingLineDataContract { AccountNr = entity.GrootBoek, Amount=0-entity.MvH.GetValueOrDefault() }
                        };
                    if (entity.BTW.GetValueOrDefault() != 0) lines.Add(new BookingLineDataContract { AccountNr = worksheetset.GetListAccount("4516"), Amount = 0 - entity.BTW.GetValueOrDefault() });
                    worksheetset.AddBooking(TypeId, "VoordelenOverigeNuts", entity.Id, null, lines);
                }
                Data.LastUpdated = DateTime.Now;
                Data.VoordelenOverigeNuts.Add(entity);
            }
        }

        public void DeleteVoordelenOverigeNuts(Guid rowId, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_Overige_Nuts original = Data.VoordelenOverigeNuts.FirstOrDefault(e => e.Id == rowId);
            if (original != null)
            {
                if (original.Book.HasValue && original.Book.Value)
                    {
                        worksheetset.DeleteBooking(TypeId, "VoordelenOverigeNuts", original.Id, null);
                        // DELETE BOOKING
                    }
                
                Data.VoordelenOverigeNuts.Remove(original);
                Data.LastUpdated = DateTime.Now;
            }
        }

        public void UpdateVoordelenOverigeNuts(EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_Overige_Nuts entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2012.VoordelenAppTypes.VAA_Overige_Nuts original = Data.VoordelenOverigeNuts.FirstOrDefault(e => e.Id == entity.Id);
            if (original == null)
            {
                AddToVoordelenOverigeNuts(entity, ref worksheetset);
            }
            else
            {
                entity = Validate(entity, "VoordelenOverigeNuts", ref worksheetset);
                if (entity.IsValid)
                {
                    CalculateOverigeNuts(entity, ref worksheetset);
                    if (entity.Book.HasValue && entity.Book.Value)
                    {
                        List<BookingLineDataContract> lines = new List<BookingLineDataContract>() {
                            new BookingLineDataContract { AccountNr = entity.DebetRekening, Amount=entity.Totaal.GetValueOrDefault() }
                            ,new BookingLineDataContract { AccountNr = entity.GrootBoek, Amount=0-entity.MvH.GetValueOrDefault() }
                        };
                        if (entity.BTW.GetValueOrDefault() != 0) lines.Add(new BookingLineDataContract { AccountNr = worksheetset.GetListAccount("4516"), Amount = 0 - entity.BTW.GetValueOrDefault() });
                        if (original.Book.HasValue && original.Book.Value)
                        {
                            worksheetset.UpdateBooking(TypeId, "VoordelenOverigeNuts", entity.Id, null, lines);
                        }
                        else
                        {
                            worksheetset.AddBooking(TypeId, "VoordelenOverigeNuts", entity.Id, null, lines);
                        }
                    }
                    else
                    {
                        if (original.Book.HasValue && original.Book.Value)
                        {
                            worksheetset.DeleteBooking(TypeId, "VoordelenOverigeNuts", original.Id, null);
                            // DELETE BOOKING
                        }
                    }
                    int idx = Data.VoordelenOverigeNuts.IndexOf(original);
                    Data.VoordelenOverigeNuts.RemoveAt(idx);
                    Data.VoordelenOverigeNuts.Insert(idx, entity);
                    Data.LastUpdated = DateTime.Now;
                }
            }
        }

        #endregion
        
        // BOOKINGS
        
        // IMPORT PREVIOUS
        
    }
}
  