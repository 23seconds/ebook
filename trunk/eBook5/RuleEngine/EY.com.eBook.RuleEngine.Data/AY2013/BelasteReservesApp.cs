
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.RuleEngine.Core;
using EY.com.eBook.RuleEngine.Core.Interfaces;
using EY.com.eBook.RuleEngine.Data;
using EY.com.eBook.Core;
using EY.com.eBook.API.Contracts.Data.Cache;

namespace EY.com.eBook.RuleEngine.AY2013
{
    
    public class BelasteReservesApp
    {
        public static WorksheetTypeSettings Settings = new WorksheetTypeSettings
                                                      {
                                                        Clients = true
                                                        ,
                                                        Suppliers = true
                                                        ,
                                                        AllAccounts = true
                                                        , 
                                                        PerformsBookings=true
                                                      };
                                                      
        public static Guid TypeId = new Guid("7df44f01-2f13-4cd5-ada1-7ad8003a85c1");

        public List<string> ImportablePreviousTypes  = new List<string>() {  "EY.com.eBook.RuleEngine.AY2012.FacturatieApp"  };

        public Guid GetMyType() { return TypeId; }

        public void SetMinimal()
        {
            if (Data == null)
            {
                Data = new EY.com.eBook.RuleEngine.AY2013.BelasteReservesAppTypes.BelasteReserves();
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.AllBelasteReserves == null)
            {
                Data.AllBelasteReserves = new List<EY.com.eBook.RuleEngine.AY2013.BelasteReservesAppTypes.BelasteReservesItem>(); ;
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.AanpassingBTReserves == null)
            {
                Data.AanpassingBTReserves = new List<EY.com.eBook.RuleEngine.AY2013.BelasteReservesAppTypes.AanpassingBTReservesItem>(); ;
                Data.LastUpdated = DateTime.Now;
            }

        }
        
        public string ToXml() 
        {
            return CoreHelper.SerializeToString(Data);
        }

        public void LoadXml(string xml)
        {
            if (!string.IsNullOrEmpty(xml)) {
                Data = CoreHelper.DeserializeFromString<EY.com.eBook.RuleEngine.AY2013.BelasteReservesAppTypes.BelasteReserves>(xml);
            }
            SetMinimal();
            
        }
        
        public EY.com.eBook.RuleEngine.AY2013.BelasteReservesAppTypes.BelasteReserves Data { get; set; }
        
        public void Calculate(ref WorksheetSetData worksheetset)
        {
            worksheetset.ClearMessages(TypeId);
            if (!Data.LastUpdated.HasValue) Data.LastUpdated = new DateTime(1980, 1, 1);
            LoadMappings(ref worksheetset);
            ProcessRegularisations(ref worksheetset);
            LoadWorksheets(ref worksheetset);
            ValidateAll(ref worksheetset);
            Data.LastUpdated = DateTime.Now;
            
        }

        public void ForceRecalculation(ref WorksheetSetData worksheetset)
        {
            Data.LastUpdated = null;
            Calculate(ref worksheetset);
        }

        public void ValidateAll(ref WorksheetSetData worksheetset)
        {
            worksheetset.ClearMessages(TypeId);
            for (int i = 0; i < Data.AllBelasteReserves.Count; i++)
            {
                Data.AllBelasteReserves[i] = Validate(Data.AllBelasteReserves[i], "AllBelasteReserves", ref worksheetset);
            }
            for (int i = 0; i < Data.AanpassingBTReserves.Count; i++)
            {
                Data.AanpassingBTReserves[i] = Validate(Data.AanpassingBTReserves[i], "AanpassingBTReserves", ref worksheetset);
            }

        }

        private void LoadWorksheets(ref WorksheetSetData worksheetset)
        {
            Guid belastbaar = new Guid("defdbea0-0fde-48c2-9fe0-193e0cb7e42f");
            decimal? valStart = worksheetset.DubieuzeDebiteurenApp.Data.ToestandEindHuidigBoekjaar.Where(d => d.BelastbaarOfVrijgesteld.Id == belastbaar).Sum(d=>d.WaardeverminderingStart);
            decimal? valEinde = worksheetset.DubieuzeDebiteurenApp.Data.ToestandEindHuidigBoekjaar.Where(d => d.BelastbaarOfVrijgesteld.Id == belastbaar).Sum(d => d.WaardeverminderingEinde);

            // VRIJGESTELDE MEERWAARDEN OP AANDELEN
            Guid vrijgesteldaandeel = new Guid("d9be6d3d-d561-46b4-87fc-9401c9c7ae68");
            decimal? vrijgesteldemeerwaardeaandelen = worksheetset.MeerwaardeAandelenApp.Data.MeerwaardeAandelenLijst.Where(d => d.Resultaat != null && d.Resultaat.Id == vrijgesteldaandeel).Sum(d => d.Saldo);

            valStart = valStart.HasValue ? valStart.Value : 0;
            valEinde = valEinde.HasValue ? valEinde.Value : 0;
            vrijgesteldemeerwaardeaandelen = vrijgesteldemeerwaardeaandelen.HasValue ? vrijgesteldemeerwaardeaandelen.Value : 0;

            BelasteReservesAppTypes.BelasteReservesItem aanpas = Data.AllBelasteReserves.FirstOrDefault(a => a.AutoKey == "DUBDEB");
            
            if (valStart.Value != 0 || valEinde.Value != 0)
            {
                if (aanpas == null)
                {
                    aanpas = new EY.com.eBook.RuleEngine.AY2013.BelasteReservesAppTypes.BelasteReservesItem
                    {
                        Id = Guid.NewGuid()
                        ,
                        Autoline = true
                        ,
                        AutoKey = "DUBDEB"
                        ,
                        TypeReseve = worksheetset.GetListItem(new Guid("b97e3772-4090-4bad-a623-a3e0c6044855"))
                    };
                    Data.AllBelasteReserves.Add(aanpas);
                }
                aanpas.BeginToestand = valStart;
                aanpas.EindToestand = valEinde;
                decimal diff = valEinde.Value - valStart.Value;
                aanpas.ToenameTijdensBJ = diff > 0 ? diff : 0;
                aanpas.AfnameTijdensBJ = diff < 0 ? Math.Abs(diff) : 0;
                
            }
            else
            {
                if (aanpas != null) Data.AllBelasteReserves.Remove(aanpas);
            }


            valStart = worksheetset.VoorzieningenApp.Data.ToestandEindHuidigBoekjaar.Where(d =>d.Belastbaar!=null && d.Belastbaar.Id == belastbaar).Sum(d => d.VoorzieningBeginBj);
            valEinde = worksheetset.VoorzieningenApp.Data.ToestandEindHuidigBoekjaar.Where(d => d.Belastbaar != null && d.Belastbaar.Id == belastbaar).Sum(d => d.VoorzieningEindeBJ);
            valStart = valStart.HasValue ? valStart.Value : 0;
            valEinde = valEinde.HasValue ? valEinde.Value : 0;

            aanpas = Data.AllBelasteReserves.FirstOrDefault(a => a.AutoKey == "VOORZIENING");

            if (valStart.Value != 0 || valEinde.Value != 0)
            {
                if (aanpas == null)
                {
                    aanpas = new EY.com.eBook.RuleEngine.AY2013.BelasteReservesAppTypes.BelasteReservesItem
                    {
                        Id = Guid.NewGuid()
                        ,
                        Autoline = true
                        ,
                        AutoKey = "VOORZIENING"
                        ,
                        TypeReseve = worksheetset.GetListItem(new Guid("0f5ff0f1-df29-4342-9e8c-1630304da12f"))
                    };
                    Data.AllBelasteReserves.Add(aanpas);
                }
                aanpas.BeginToestand = valStart;
                aanpas.EindToestand = valEinde;
                decimal diff = valEinde.Value - valStart.Value;
                aanpas.ToenameTijdensBJ = diff > 0 ? diff : 0;
                aanpas.AfnameTijdensBJ = diff < 0 ? Math.Abs(diff) : 0;

            }
            else
            {
                if (aanpas != null) Data.AllBelasteReserves.Remove(aanpas);
            }

            // VRIJGESTELDE MEERWAARDEN OP AANDELEN
            BelasteReservesAppTypes.AanpassingBTReservesItem aanpasBT = Data.AanpassingBTReserves.FirstOrDefault(a => a.AutoKey == "MEERWAARDEAANDELEN");

            if (vrijgesteldemeerwaardeaandelen.GetValueOrDefault() != 0)
            {
                if (aanpasBT == null)
                {
                    aanpasBT = new EY.com.eBook.RuleEngine.AY2013.BelasteReservesAppTypes.AanpassingBTReservesItem
                    {
                        Id = Guid.NewGuid()
                        ,
                        Auto = true
                        ,
                        AutoKey = "MEERWAARDEAANDELEN"
                        ,
                        TypeReseve = worksheetset.GetListItem(new Guid("F34C7D2B-428D-494D-A29B-F2BD2910A7A9"))
                    };
                    Data.AanpassingBTReserves.Add(aanpasBT);
                }
                aanpasBT.Bedrag = vrijgesteldemeerwaardeaandelen.GetValueOrDefault();
                aanpasBT.AutoPercentage = 100;
                aanpasBT.BedragNaPercentage = 100;
                aanpasBT.Omschrijving = worksheetset.DataContract.Culture == "nl-BE" ? "Meerwaarden op aandelen" : worksheetset.DataContract.Culture == "fr-FR" ? "Plus-values sur actions ou parts imposables" : "Capital Gains Shares";

            }
            else
            {
                if (aanpasBT != null) Data.AanpassingBTReserves.Remove(aanpasBT);
            }

        }

        private void ProcessRegularisations(ref WorksheetSetData worksheetset)
        {
            BelastingenAppTypes.KostItem autoreg = worksheetset.BelastingenApp.Data.Auto.FirstOrDefault(a => a.UNID == "REGULATE");
            BelasteReservesAppTypes.AanpassingBTReservesItem autobelreg = Data.AanpassingBTReserves.FirstOrDefault(a => a.AutoKey == "VU_AUTO");
            if (autoreg != null)
            {
                if (autobelreg == null)
                {
                    autobelreg = new EY.com.eBook.RuleEngine.AY2013.BelasteReservesAppTypes.AanpassingBTReservesItem
                    {
                        Id = Guid.NewGuid()
                        ,
                        Auto = true
                        ,
                        AutoKey = "VU_AUTO"
                        ,
                        TypeReseve = worksheetset.GetListItem(new Guid("0355b0d0-46be-46d7-a59e-6858c6f7afa4"))
                    };
                    Data.AanpassingBTReserves.Add(autobelreg);
                }
                autobelreg.AutoPercentage = autoreg.Percent;
                autobelreg.Percentage = autoreg.Percent;
                autobelreg.Bedrag = autoreg.Bedrag;
                autobelreg.BedragNaPercentage = Math.Round((autobelreg.Bedrag.Value / 100) * autobelreg.Percentage.Value, 2, MidpointRounding.AwayFromZero);
                autobelreg.Omschrijving = autoreg.GrootBoek.NL;

            }
            else
            {
                if (autobelreg != null) Data.AanpassingBTReserves.Remove(autobelreg);
            }


            BelastingenAppTypes.Belasting belreg = worksheetset.BelastingenApp.Data.Belastingen.FirstOrDefault(a => a.UNID == "REGULATE");
            autobelreg = Data.AanpassingBTReserves.FirstOrDefault(a => a.AutoKey == "VU_TAX");
            if (belreg != null)
            {
                if (autobelreg == null)
                {
                    autobelreg = new EY.com.eBook.RuleEngine.AY2013.BelasteReservesAppTypes.AanpassingBTReservesItem
                    {
                        Id = Guid.NewGuid()
                        ,
                        Auto = true
                        ,
                        AutoKey = "VU_TAX"
                        ,
                        TypeReseve = worksheetset.GetListItem(new Guid("0355b0d0-46be-46d7-a59e-6858c6f7afa4"))
                    };
                    Data.AanpassingBTReserves.Add(autobelreg);
                }
                autobelreg.AutoPercentage = belreg.Percent;
                autobelreg.Percentage = belreg.Percent;
                autobelreg.Bedrag = belreg.Bedrag;
                autobelreg.BedragNaPercentage = Math.Round((autobelreg.Bedrag.Value / 100) * autobelreg.Percentage.Value, 2, MidpointRounding.AwayFromZero);
                autobelreg.Omschrijving = belreg.GrootBoek.NL;

            }
            else
            {
                if (autobelreg != null) Data.AanpassingBTReserves.Remove(autobelreg);
            }
            
        }

        private void LoadMappings(ref WorksheetSetData worksheetset)
        {
            List<AccountDataContract> accounts = worksheetset.GetAccountsOfMappingMeta("belastereserve", "Reserves", false, true, false, worksheetset.DataContract.Culture);
            List<string> nrs = accounts.Select(a=>a.InternalNr).ToList();

            Data.AllBelasteReserves.RemoveAll(d => d.AutoKey == "IMPORT" && !nrs.Contains(d.Grootboek.Id));

            foreach (AccountDataContract account in accounts)
            {
                BelasteReservesAppTypes.BelasteReservesItem entity = Data.AllBelasteReserves.FirstOrDefault(a => a.AutoKey == "IMPORT" && a.Grootboek.Id == account.InternalNr);
                if (entity == null)
                {
                    entity = new EY.com.eBook.RuleEngine.AY2013.BelasteReservesAppTypes.BelasteReservesItem
                    {
                        Id = Guid.NewGuid()
                        ,
                        AutoKey = "IMPORT"
                        ,
                        Autoline = true
                        ,
                        Grootboek = worksheetset.GetListAccount(account)
                        ,
                        Order = 1
                    };
                    Data.AllBelasteReserves.Add(entity);
                }
                entity.BeginToestand = (0-account.PreviousSaldo);
                entity.EindToestand = (0-account.Saldo);
                decimal diff = entity.EindToestand.Value - entity.BeginToestand.Value;
                entity.AfnameTijdensBJ = diff < 0 ? Math.Abs(diff) : 0;
                entity.ToenameTijdensBJ = diff > 0 ? Math.Abs(diff) : 0;
                entity = Validate(entity, "AllBelasteReserves", ref worksheetset);
            }


            accounts = worksheetset.GetAccountsOfMappingMeta("aanpassingBT", "Reserves", false, true, false, worksheetset.DataContract.Culture);
            nrs = accounts.Select(a => a.InternalNr).ToList();

            Data.AanpassingBTReserves.RemoveAll(d => d.Auto == true && !nrs.Contains(d.AutoKey));

            foreach (AccountDataContract account in accounts)
            {
                BelasteReservesAppTypes.AanpassingBTReservesItem entity = Data.AanpassingBTReserves.FirstOrDefault(a => a.AutoKey == account.InternalNr);
                if (entity == null)
                {
                    entity = new EY.com.eBook.RuleEngine.AY2013.BelasteReservesAppTypes.AanpassingBTReservesItem
                    {
                        Id = Guid.NewGuid()
                        ,
                        AutoKey = account.InternalNr
                        ,
                        Auto = true
                        ,
                        Omschrijving = account.DefaultDescription
                        , AutoPercentage = 100
                        ,Soort =null
                        , Percentage=100
                       
                    };
                    Data.AanpassingBTReserves.Add(entity);
                }
                entity.Bedrag = (0-account.Saldo);
                entity.BedragNaPercentage = Math.Round((entity.Bedrag.Value / 100) * entity.Percentage.Value, 2, MidpointRounding.AwayFromZero);
                entity = Validate(entity, "AllBelasteReserves", ref worksheetset);
            }


        }
        
        // VALIDATION
        
        // CALCULATION
        
        // BOOKINGS
        
        // IMPORT PREVIOUS


        #region AddUpdateDelete

        public void AddToAllBelasteReserves(EY.com.eBook.RuleEngine.AY2013.BelasteReservesAppTypes.BelasteReservesItem entity, ref WorksheetSetData worksheetset)
        {
            entity = Validate(entity, "AllBelasteReserves", ref worksheetset);
            if (entity.IsValid)
            {
                Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
                Data.AllBelasteReserves.Add(entity);
            }
        }

        public void DeleteAllBelasteReserves(Guid rowId, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2013.BelasteReservesAppTypes.BelasteReservesItem original = Data.AllBelasteReserves.FirstOrDefault(e => e.Id == rowId);
            if (original != null)
            {
                /*
                if (original.Boeken.HasValue && original.Boeken.Value)
                {
                    worksheetset.DeleteBooking(FacturatieApp.TypeId, "OpTeMakenCN", original.Id, null);
                    // DELETE BOOKING
                }*/
                Data.AllBelasteReserves.Remove(original);
                Data.LastUpdated = DateTime.Now;
            }
        }

        public void UpdateAllBelasteReserves(EY.com.eBook.RuleEngine.AY2013.BelasteReservesAppTypes.BelasteReservesItem entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2013.BelasteReservesAppTypes.BelasteReservesItem original = Data.AllBelasteReserves.FirstOrDefault(e => e.Id == entity.Id);
            if (original == null)
            {
                AddToAllBelasteReserves(entity, ref worksheetset);
            }
            else
            {
                entity = Validate(entity, "AllBelasteReserves", ref worksheetset);
                if (entity.IsValid)
                {
                    int idx = Data.AllBelasteReserves.IndexOf(original);
                    Data.AllBelasteReserves.RemoveAt(idx);
                    Data.AllBelasteReserves.Insert(idx, entity);
                    Data.LastUpdated = DateTime.Now;
                    entity.LastUpdated = DateTime.Now;
                }
            }
        }

        public void AddToAanpassingBTReserves(EY.com.eBook.RuleEngine.AY2013.BelasteReservesAppTypes.AanpassingBTReservesItem entity, ref WorksheetSetData worksheetset)
        {
            entity = Validate(entity, "AanpassingBTReserves", ref worksheetset);
            if (entity.IsValid)
            {
                Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
                Data.AanpassingBTReserves.Add(entity);
            }
        }

        public void DeleteAanpassingBTReserves(Guid rowId, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2013.BelasteReservesAppTypes.AanpassingBTReservesItem original = Data.AanpassingBTReserves.FirstOrDefault(e => e.Id == rowId);
            if (original != null)
            {
                /*
                if (original.Boeken.HasValue && original.Boeken.Value)
                {
                    worksheetset.DeleteBooking(FacturatieApp.TypeId, "OpTeMakenCN", original.Id, null);
                    // DELETE BOOKING
                }*/
                Data.AanpassingBTReserves.Remove(original);
                Data.LastUpdated = DateTime.Now;
            }
        }

        public void UpdateAanpassingBTReserves(EY.com.eBook.RuleEngine.AY2013.BelasteReservesAppTypes.AanpassingBTReservesItem entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2013.BelasteReservesAppTypes.AanpassingBTReservesItem original = Data.AanpassingBTReserves.FirstOrDefault(e => e.Id == entity.Id);
            if (original == null)
            {
                AddToAanpassingBTReserves(entity, ref worksheetset);
            }
            else
            {
                entity = Validate(entity, "AanpassingBTReserves", ref worksheetset);
                if (entity.IsValid)
                {
                    int idx = Data.AanpassingBTReserves.IndexOf(original);
                    Data.AanpassingBTReserves.RemoveAt(idx);
                    Data.AanpassingBTReserves.Insert(idx, entity);
                    Data.LastUpdated = DateTime.Now;
                    entity.LastUpdated = DateTime.Now;
                }
            }
        }

        #endregion

        #region VALIDATION

        public EY.com.eBook.RuleEngine.AY2013.BelasteReservesAppTypes.BelasteReservesItem Validate(EY.com.eBook.RuleEngine.AY2013.BelasteReservesAppTypes.BelasteReservesItem entity, string location, ref WorksheetSetData worksheetset)
        {
            entity.IsValid = true;
            worksheetset.ClearMessages(TypeId, entity.Id);
            
            if (!entity.BeginToestand.HasValue)
            {
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "BeginToestand");
                entity.IsValid = false;
            }
            if (!entity.EindToestand.HasValue)
            {
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "EindToestand");
                entity.IsValid = false;
            }
            if (entity.IsValid)
            {
                entity.AfnameTijdensBJ = 0;
                entity.ToenameTijdensBJ = 0;
                decimal diff = entity.EindToestand.GetValueOrDefault() - entity.BeginToestand.GetValueOrDefault();
                if (diff < 0)
                {
                    entity.AfnameTijdensBJ = Math.Abs(diff);
                }
                else
                {
                    entity.ToenameTijdensBJ = diff;
                }
            }
            if (entity.TypeReseve == null)
            {
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "TypeReseve");
                entity.IsValid = false;
            }

            return entity;
        }

        public EY.com.eBook.RuleEngine.AY2013.BelasteReservesAppTypes.AanpassingBTReservesItem Validate(EY.com.eBook.RuleEngine.AY2013.BelasteReservesAppTypes.AanpassingBTReservesItem entity, string location, ref WorksheetSetData worksheetset)
        {
            entity.IsValid = true;
            worksheetset.ClearMessages(TypeId, entity.Id);
            
            if (entity.TypeReseve == null)
            {
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "TypeReseve");
                entity.IsValid = false;
            }
            if (!entity.Bedrag.HasValue)
            {
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Bedrag");
                entity.IsValid = false;
            } else {

                if (!entity.Percentage.HasValue) entity.Percentage = 100;
                entity.BedragNaPercentage = Math.Round((entity.Bedrag.GetValueOrDefault() / 100) * entity.Percentage.GetValueOrDefault(),2);
            }

            return entity;
        }

        #endregion
    }
}
  