
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.RuleEngine.Core;
using EY.com.eBook.RuleEngine.Core.Interfaces;
using EY.com.eBook.RuleEngine.Data;
using EY.com.eBook.Core;

namespace EY.com.eBook.RuleEngine.AY2013
{
    
    public class BerekeningVenBApp
    {
        public static WorksheetTypeSettings Settings = new WorksheetTypeSettings
                                                      {
                                                        Clients = true
                                                        ,
                                                        Suppliers = true
                                                        ,
                                                        AllAccounts = true
                                                        , 
                                                        PerformsBookings=true
                                                      };
                                                      
        public static Guid TypeId = new Guid("803d6a06-352e-4e28-ade5-214deaac76ca");

        public List<string> ImportablePreviousTypes  = new List<string>() {  "EY.com.eBook.RuleEngine.AY2012.FacturatieApp"  };

        public Guid GetMyType() { return TypeId; }

        public void SetMinimal()
        {
            if (Data == null)
            {
                Data = new EY.com.eBook.RuleEngine.AY2013.BerekeningVenBAppTypes.BerekeningVenB();
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.Overzicht == null)
            {
                Data.Overzicht = new EY.com.eBook.RuleEngine.AY2013.BerekeningVenBAppTypes.General
                {
                    Id = Guid.NewGuid()
                    ,
                    LastUpdated = null
                    ,
                    IsValid = true
                    ,
                    PreviousImported = false

                };
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.BelasteReserves == null)
            {
                Data.BelasteReserves = new EY.com.eBook.RuleEngine.AY2013.BerekeningVenBAppTypes.BelasteReservesItem
                {
                    Id = Guid.NewGuid()
                    ,
                    LastUpdated = null
                    ,
                    IsValid = true
                    ,
                    PreviousImported = false

                };
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.VerworpenUitgaven == null)
            {
                Data.VerworpenUitgaven = new EY.com.eBook.RuleEngine.AY2013.BerekeningVenBAppTypes.VerworpenUitgavenItem
                {
                    Id = Guid.NewGuid()
                    ,
                    LastUpdated = null
                    ,
                    IsValid = true
                    ,
                    PreviousImported = false

                };
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.BelastbareBestZonderAftrek == null)
            {
                Data.BelastbareBestZonderAftrek = new EY.com.eBook.RuleEngine.AY2013.BerekeningVenBAppTypes.BelastbareBestZonderAftrekItem
                {
                    Id = Guid.NewGuid()
                    ,
                    LastUpdated = null
                    ,
                    IsValid = true
                    ,
                    PreviousImported = false

                };
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.OpdelingNaarOorsprong == null)
            {
                Data.OpdelingNaarOorsprong = new EY.com.eBook.RuleEngine.AY2013.BerekeningVenBAppTypes.OpdelingNaarOorsprongItem
                {
                    Id = Guid.NewGuid()
                    ,
                    LastUpdated = null
                    ,
                    IsValid = true
                    ,
                    PreviousImported = false

                };
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.FiscaleAftrek == null)
            {
                Data.FiscaleAftrek = new EY.com.eBook.RuleEngine.AY2013.BerekeningVenBAppTypes.FiscaleAftrekItem
                {
                    Id = Guid.NewGuid()
                    ,
                    LastUpdated = null
                    ,
                    IsValid = true
                    ,
                    PreviousImported = false

                };
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.Tarief == null)
            {
                Data.Tarief = new EY.com.eBook.RuleEngine.AY2013.BerekeningVenBAppTypes.TariefItem
                {
                    Id = Guid.NewGuid()
                    ,
                    LastUpdated = null
                    ,
                    IsValid = true
                    ,
                    PreviousImported = false

                };
                // STANDARD ON TRUE.
                Data.Tarief.BezitAandelenDoorVennootschappen = true;
                Data.Tarief.BezitAandelenMeerDan50Percent = true;
                Data.Tarief.ToegekendeBezoldiging = true;
                Data.Tarief.UitgekeerdDividendHogerDan13 = true;

                Data.LastUpdated = DateTime.Now;
            }

            if (Data.Aanslag == null)
            {
                Data.Aanslag = new EY.com.eBook.RuleEngine.AY2013.BerekeningVenBAppTypes.AanslagItem
                {
                    Id = Guid.NewGuid()
                    ,
                    LastUpdated = null
                    ,
                    IsValid = true
                    ,
                    PreviousImported = false
                };
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.BelastingsBerekening == null)
            {
                Data.BelastingsBerekening = new EY.com.eBook.RuleEngine.AY2013.BerekeningVenBAppTypes.BelastingsBerekeningItem
                {
                    Id = Guid.NewGuid()
                    ,
                    LastUpdated = null
                    ,
                    IsValid = true
                    ,
                    PreviousImported = false

                };
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.BelastingsBerekening2 == null)
            {
                Data.BelastingsBerekening2 = new EY.com.eBook.RuleEngine.AY2013.BerekeningVenBAppTypes.BelastingsBerekeningItem2
                {
                    Id = Guid.NewGuid()
                    ,
                    LastUpdated = null
                    ,
                    IsValid = true
                    ,
                    PreviousImported = false

                };
                Data.LastUpdated = DateTime.Now;
            }

        }
        
        public string ToXml() 
        {
            return CoreHelper.SerializeToString(Data);
        }

        public void LoadXml(string xml)
        {
            if (!string.IsNullOrEmpty(xml)) {
                Data = CoreHelper.DeserializeFromString<EY.com.eBook.RuleEngine.AY2013.BerekeningVenBAppTypes.BerekeningVenB>(xml);
            }
            SetMinimal();
            
        }
        
        public EY.com.eBook.RuleEngine.AY2013.BerekeningVenBAppTypes.BerekeningVenB Data { get; set; }

        public void ForceRecalculation(ref WorksheetSetData worksheetset)
        {
            Data.LastUpdated = null;
            Calculate(ref worksheetset);
        }

        public void Calculate(ref WorksheetSetData worksheetset)
        {

           

            if (!Data.LastUpdated.HasValue) Data.LastUpdated = new DateTime(1980, 1, 1);
            //worksheetset.Messages.RemoveAll(m => m.ConnectionGuid == TypeId);
            worksheetset.ClearMessages(TypeId);
            List<BelastingenAppTypes.Belasting> bels = worksheetset.BelastingenApp
                                                            .Data.Belastingen
                                                            .Where(b => b.Soort != null
                                                                && b.Soort.Id.ToString().ToLower() == "7e073d47-c12f-4e0d-9f38-5005e29afc78"
                                                            ).ToList();
            Data.BelastingsBerekening.JongVennootschap = worksheetset.Settings.OneOfFirstThree;

            if (bels.Count == 0)
            {
                Data.BelastingsBerekening.AP1Base = 0;
                Data.BelastingsBerekening.AP2Base = 0;
                Data.BelastingsBerekening.AP3Base = 0;
                Data.BelastingsBerekening.AP4Base = 0;
            }
            else
            {
                Data.BelastingsBerekening.AP1Base = bels.Sum(b => b.VA1) ;
                Data.BelastingsBerekening.AP2Base = bels.Sum(b => b.VA2);
                Data.BelastingsBerekening.AP3Base = bels.Sum(b => b.VA3);
                Data.BelastingsBerekening.AP4Base = bels.Sum(b => b.VA4);
            }

            BepalingBelastbareBasisAppTypes.BepalingBasis basis= worksheetset.BepalingBelastbareBasisApp.Data;


            Data.Aanslag.IPT2013 = 0;
            Data.Aanslag.IPT2014 = 0;
            Data.Aanslag.IPT2015 = 0;
            if (Data.Aanslag.TotaalBedragVoorzieningen.GetValueOrDefault() > 0)
            {
                if (Data.Aanslag.AfzonderlijkeAanslagGespreid.GetValueOrDefault())
                {
                    Data.Aanslag.IPT2013 = Math.Round(Data.Aanslag.TotaalBedragVoorzieningen.GetValueOrDefault() * (decimal)0.006, 2, MidpointRounding.AwayFromZero);
                    Data.Aanslag.IPT2014 = Math.Round(Data.Aanslag.TotaalBedragVoorzieningen.GetValueOrDefault() * (decimal)0.006, 2, MidpointRounding.AwayFromZero);
                    Data.Aanslag.IPT2015 = Math.Round(Data.Aanslag.TotaalBedragVoorzieningen.GetValueOrDefault() * (decimal)0.006, 2, MidpointRounding.AwayFromZero);
                }
                else
                {
                    Data.Aanslag.IPT2013 = Math.Round(Data.Aanslag.TotaalBedragVoorzieningen.GetValueOrDefault() * (decimal)0.0175, 2, MidpointRounding.AwayFromZero);
                }
            }

            // MEERWAARDEN OP AANDELEN TEGEN VERMINDERD TARIEF
            Guid mwaandelenid = new Guid("071EFA9C-4FA7-47BD-BCAB-DCB98F437651");

            /*
             * Ticket#19 situatie 1 & 2 & 3 */
            /*
            if (Data.BelastingsBerekening.CapitalGainsShares25 > Data.BelastingsBerekening.CorporateIncomeTaxFromResult){
                Data.BelastingsBerekening.CapitalGainsShares25 = Data.BelastingsBerekening.CorporateIncomeTaxFromResult;
            }else{
                Data.BelastingsBerekening.CapitalGainsShares25 = worksheetset.MeerwaardeAandelenApp.Data.MeerwaardeAandelenLijst.Where(d => d.Resultaat != null && d.Resultaat.Id == mwaandelenid).Sum(d => d.Saldo).GetValueOrDefault();
            }*/

            Data.Overzicht.GereserveerdeWinst = basis.Overzicht.GereserveerdeWinst;
            Data.Overzicht.Dividenden = basis.Overzicht.Dividenden;
            Data.Overzicht.VerkrijgingEigenAandelen = basis.Overzicht.VerkrijgingEigenAandelen;
            Data.Overzicht.OverlijdenUittredingUitsluitingVennoot = basis.Overzicht.OverlijdenUittredingUitsluitingVennoot;
            Data.Overzicht.VerdelingMaatschappelijkVermogen = basis.Overzicht.VerdelingMaatschappelijkVermogen;
            Data.Overzicht.StatAccountingResult = basis.Overzicht.StatAccountingResult;
            Data.Overzicht.TotalBelasteReserves = basis.Overzicht.TotalBelasteReserves;
            Data.Overzicht.TotalAanpassingenBelasteReserves = basis.Overzicht.TotalAanpassingenBelasteReserves;
            Data.Overzicht.NietAftrekbareBelastingen = basis.Overzicht.NietAftrekbareBelastingen;
            Data.Overzicht.TotaalVerworpenUitgaven = basis.Overzicht.TotaalVerworpenUitgaven;
            Data.Overzicht.BelastbaarResultaat = basis.Overzicht.BelastbaarResultaat;
            Data.Overzicht.TotaalBelastbareBestandenZonderAftrekMin = basis.Overzicht.TotaalBelastbareBestandenZonderAftrekMin;
            Data.Overzicht.SubTotaalBelastbaarVoorAftrek = basis.Overzicht.SubTotaalBelastbaarVoorAftrek;
            Data.Overzicht.OpdelingNaarOorsprongBijVerdragVrijgesteldInkomen = basis.Overzicht.OpdelingNaarOorsprongBijVerdragVrijgesteldInkomen;

            Data.BelasteReserves.OACapitalGainsOnShares = basis.BelasteReserves.OACapitalGainsOnShares;
            Data.BelasteReserves.OADefinitiveExemptionTaxShelter = basis.BelasteReserves.OADefinitiveExemptionTaxShelter;
            Data.BelasteReserves.OAExemptionRegionalPrem = basis.BelasteReserves.OAExemptionRegionalPrem;
            Data.BelasteReserves.OAOthers = basis.BelasteReserves.OAOthers;
            Data.BelasteReserves.OATotal = basis.BelasteReserves.OATotal;
            Data.BelasteReserves.OhterAdjustments = basis.BelasteReserves.OhterAdjustments;
            Data.BelasteReserves.TDExcessDeprications = basis.BelasteReserves.TDExcessDeprications;
            Data.BelasteReserves.TDHerwaardering = basis.BelasteReserves.TDHerwaardering;
            Data.BelasteReserves.TDKapitaalEnUitgifte = basis.BelasteReserves.TDKapitaalEnUitgifte;
            Data.BelasteReserves.TDOthers = basis.BelasteReserves.TDOthers;
            Data.BelasteReserves.TDOtherTaxableReserves = basis.BelasteReserves.TDOtherTaxableReserves;
            Data.BelasteReserves.TDOverestimationLiabilities = basis.BelasteReserves.TDOverestimationLiabilities;
            Data.BelasteReserves.TDTaxableDoubtfullDebtors = basis.BelasteReserves.TDTaxableDoubtfullDebtors;
            Data.BelasteReserves.TDTaxableProvisions = basis.BelasteReserves.TDTaxableProvisions;
            Data.BelasteReserves.TDTotal = basis.BelasteReserves.TDTotal;
            Data.BelasteReserves.TDUnderestimationAssetsOverestimationLiabilities = basis.BelasteReserves.TDUnderestimationAssetsOverestimationLiabilities;

            Data.VerworpenUitgaven.DEAbnormalAdvantages = basis.VerworpenUitgaven.DEAbnormalAdvantages;
            Data.VerworpenUitgaven.DEBepaaldeStaten = basis.VerworpenUitgaven.DEBepaaldeStaten;
            Data.VerworpenUitgaven.DECar = basis.VerworpenUitgaven.DECar;
            Data.VerworpenUitgaven.DECarVAA = basis.VerworpenUitgaven.DECarVAA;
            Data.VerworpenUitgaven.DECharity = basis.VerworpenUitgaven.DECharity;
            Data.VerworpenUitgaven.DEClothes = basis.VerworpenUitgaven.DEClothes;
            Data.VerworpenUitgaven.DEDinerSportCultureEco = basis.VerworpenUitgaven.DEDinerSportCultureEco;
            Data.VerworpenUitgaven.DEEmployeeCapital = basis.VerworpenUitgaven.DEEmployeeCapital;
            Data.VerworpenUitgaven.DEExcessInterest = basis.VerworpenUitgaven.DEExcessInterest;
            Data.VerworpenUitgaven.DEExpensesTaxShelter = basis.VerworpenUitgaven.DEExpensesTaxShelter;
            Data.VerworpenUitgaven.DEIndemnityMissingCoupon = basis.VerworpenUitgaven.DEIndemnityMissingCoupon;
            Data.VerworpenUitgaven.DEInterestLoans = basis.VerworpenUitgaven.DEInterestLoans;
            Data.VerworpenUitgaven.DEOther = basis.VerworpenUitgaven.DEOther;
            Data.VerworpenUitgaven.DEPenalties = basis.VerworpenUitgaven.DEPenalties;
            Data.VerworpenUitgaven.DEPensions = basis.VerworpenUitgaven.DEPensions;
            Data.VerworpenUitgaven.DEReception = basis.VerworpenUitgaven.DEReception;
            Data.VerworpenUitgaven.DEReductionValueShares = basis.VerworpenUitgaven.DEReductionValueShares;
            Data.VerworpenUitgaven.DERegionalPremiumsCapital = basis.VerworpenUitgaven.DERegionalPremiumsCapital;
            Data.VerworpenUitgaven.DERegionalTaxes = basis.VerworpenUitgaven.DERegionalTaxes;
            Data.VerworpenUitgaven.DERestaurant = basis.VerworpenUitgaven.DERestaurant;
            Data.VerworpenUitgaven.DEReversalsPreviousTax = basis.VerworpenUitgaven.DEReversalsPreviousTax;
            Data.VerworpenUitgaven.DESocial = basis.VerworpenUitgaven.DESocial;
            Data.VerworpenUitgaven.DETotal = basis.VerworpenUitgaven.DETotal;
            Data.VerworpenUitgaven.PDDisallowedCurrentIncomeTaxExpenses = basis.VerworpenUitgaven.PDDisallowedCurrentIncomeTaxExpenses;

            Data.BelastbareBestZonderAftrek.TEEmployeeParticipationCapital = basis.BelastbareBestZonderAftrek.TEEmployeeParticipationCapital.GetValueOrDefault();
            Data.BelastbareBestZonderAftrek.TEReceivedAbnormal = basis.BelastbareBestZonderAftrek.TEReceivedAbnormal.GetValueOrDefault();
            Data.BelastbareBestZonderAftrek.TETaxableInvestmentReserve = basis.BelastbareBestZonderAftrek.TETaxableInvestmentReserve.GetValueOrDefault();
            Data.BelastbareBestZonderAftrek.TECapitalAgriCulture = basis.BelastbareBestZonderAftrek.TECapitalAgriCulture.GetValueOrDefault();
            Data.BelastbareBestZonderAftrek.TEVaaCar = basis.BelastbareBestZonderAftrek.TEVaaCar.GetValueOrDefault();
            Data.BelastbareBestZonderAftrek.TETotal = basis.BelastbareBestZonderAftrek.TETotal.GetValueOrDefault();

            Data.OpdelingNaarOorsprong.IRBelgianTaxable = basis.OpdelingNaarOorsprong.IRBelgianTaxable;
            Data.OpdelingNaarOorsprong.IRNonTreatyForeignIncome = basis.OpdelingNaarOorsprong.IRNonTreatyForeignIncome;
            Data.OpdelingNaarOorsprong.IRRemainingResultBelgium = basis.OpdelingNaarOorsprong.IRRemainingResultBelgium;
            Data.OpdelingNaarOorsprong.IRRemainingResultNonTreaty = basis.OpdelingNaarOorsprong.IRRemainingResultNonTreaty;
            Data.OpdelingNaarOorsprong.IRTreatyForeignIncome = basis.OpdelingNaarOorsprong.IRTreatyForeignIncome;

            Data.FiscaleAftrek.NTEBECharity = basis.FiscaleAftrek.NTEBECharity;
            Data.FiscaleAftrek.NTEBEInternalshipPremium = basis.FiscaleAftrek.NTEBEInternalshipPremium;
            Data.FiscaleAftrek.NTEBEOthers = basis.FiscaleAftrek.NTEBEOthers;
            Data.FiscaleAftrek.NTEBEPersonnel = basis.FiscaleAftrek.NTEBEPersonnel;
            Data.FiscaleAftrek.NTEBEPersonnelSME = basis.FiscaleAftrek.NTEBEPersonnelSME;
            Data.FiscaleAftrek.TotalNonTaxableElements = basis.FiscaleAftrek.TotalNonTaxableElements;


            TransferTaxCreditsAppTypes.Aanwending aanwendingen = worksheetset.TransferTaxCreditsApp.Data.Aanwendingen;
            Data.FiscaleAftrek.DTLBelgium = aanwendingen.AanwendingVerlies;
            Data.FiscaleAftrek.PEBelgium = aanwendingen.AanwendingDBI;
            Data.FiscaleAftrek.NIDBelgium = aanwendingen.AanwendingNID;
            Data.FiscaleAftrek.IDBelgium = aanwendingen.AanwendingInvest;

            /* new fiche 2013 */
            //Data.FiscaleAftrek.NIDHistoryBelgium = worksheetset.RisicoKapitaalHistoriekApp.Data.Fiche.WerkelijkVorigAJ.GetValueOrDefault();
            Data.FiscaleAftrek.NIDHistoryBelgium = worksheetset.RisicoKapitaalHistoriekApp.Data.Fiche.WerkelijkAfgetrokkenVrijstellingen.GetValueOrDefault();

            
            TransferTaxCreditsAppTypes.BelastingkredietItem bki = worksheetset.TransferTaxCreditsApp.Data.Belastingkrediet.LastOrDefault();



            

            Data.BelastingsBerekening.TaxCreditResearchDevelopment = bki != null ? bki.BedragAftrek : 0;


            // FISCALE AFTREK
            Data.Overzicht.SubTotaalBelastbaarVoorAftrek = Data.Overzicht.SubTotaalBelastbaarVoorAftrek.HasValue ? Data.Overzicht.SubTotaalBelastbaarVoorAftrek.Value : 0;
            Data.Overzicht.OpdelingNaarOorsprongBijVerdragVrijgesteldInkomen = Data.Overzicht.OpdelingNaarOorsprongBijVerdragVrijgesteldInkomen.HasValue ? Data.Overzicht.OpdelingNaarOorsprongBijVerdragVrijgesteldInkomen.Value : 0;

            decimal resterend = Data.Overzicht.SubTotaalBelastbaarVoorAftrek.Value - Data.Overzicht.OpdelingNaarOorsprongBijVerdragVrijgesteldInkomen.Value;
            Data.FiscaleAftrek.TotalNonTaxableElements = Data.FiscaleAftrek.TotalNonTaxableElements.HasValue ? Data.FiscaleAftrek.TotalNonTaxableElements.Value : 0;
            if (resterend < 0) resterend = 0;
            if (Data.FiscaleAftrek.TotalNonTaxableElements.Value > resterend)
            {
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "LARGERTHENGRONDSLAG", "FiscaleAftrek", Data.FiscaleAftrek.Id, "TotalNonTaxableElements");
            }
            else
            {
                resterend -= Data.FiscaleAftrek.TotalNonTaxableElements.Value;
                Data.FiscaleAftrek.PEBelgium = Data.FiscaleAftrek.PEBelgium.HasValue ? Data.FiscaleAftrek.PEBelgium.Value : 0;

                if ((resterend - Data.FiscaleAftrek.PEBelgium.Value) < 0)
                {
                    worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "LARGERTHENGRONDSLAG", "FiscaleAftrek", Data.FiscaleAftrek.Id, "PEBelgium");
                }
                else
                {

                    resterend -= Data.FiscaleAftrek.PEBelgium.Value;
                    Data.FiscaleAftrek.PIDBelgium = Data.FiscaleAftrek.PIDBelgium.HasValue ? Data.FiscaleAftrek.PIDBelgium.Value : 0;
                    if ((resterend - Data.FiscaleAftrek.PIDBelgium.Value) < 0)
                    {
                        worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "LARGERTHENGRONDSLAG", "FiscaleAftrek", Data.FiscaleAftrek.Id, "PIDBelgium");
                    }
                    else
                    {
                        resterend -= Data.FiscaleAftrek.PIDBelgium.Value;
                        Data.FiscaleAftrek.NIDBelgium = Data.FiscaleAftrek.NIDBelgium.HasValue ? Data.FiscaleAftrek.NIDBelgium.Value : 0;
                        if ((resterend - Data.FiscaleAftrek.NIDBelgium.Value) < 0)
                        {
                            worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "LARGERTHENGRONDSLAG", "FiscaleAftrek", Data.FiscaleAftrek.Id, "NIDBelgium");
                        }
                        else
                        {
                            resterend -= Data.FiscaleAftrek.NIDBelgium.Value;
                            Data.FiscaleAftrek.DTLBelgium = Data.FiscaleAftrek.DTLBelgium.HasValue ? Data.FiscaleAftrek.DTLBelgium.Value : 0;
                            if ((resterend - Data.FiscaleAftrek.DTLBelgium.Value) < 0)
                            {
                                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "LARGERTHENGRONDSLAG", "FiscaleAftrek", Data.FiscaleAftrek.Id, "DTLBelgium");
                            }
                            else
                            {
                                resterend -= Data.FiscaleAftrek.DTLBelgium.Value;
                                Data.FiscaleAftrek.IDBelgium = Data.FiscaleAftrek.IDBelgium.HasValue ? Data.FiscaleAftrek.IDBelgium.Value : 0;
                                if ((resterend - Data.FiscaleAftrek.IDBelgium.Value) < 0)
                                {
                                    worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "LARGERTHENGRONDSLAG", "FiscaleAftrek", Data.FiscaleAftrek.Id, "IDBelgium");
                                }
                                else
                                {
                                    resterend -= Data.FiscaleAftrek.IDBelgium.Value;
                                }
                            }
                        }
                    }
                }

            }
            Data.FiscaleAftrek.TotalFiscaleAftrek = ((((Data.FiscaleAftrek.TotalNonTaxableElements + Data.FiscaleAftrek.PEBelgium) + Data.FiscaleAftrek.PIDBelgium) + Data.FiscaleAftrek.NIDBelgium) + Data.FiscaleAftrek.DTLBelgium) + Data.FiscaleAftrek.IDBelgium + Data.FiscaleAftrek.NIDHistoryBelgium.GetValueOrDefault();



            Data.Overzicht.TotaalFiscaleAftrek = Data.FiscaleAftrek.TotalFiscaleAftrek;
            Data.Overzicht.OpdelingNaarOorsprongBijVerdragVrijgesteldInkomen = Data.Overzicht.OpdelingNaarOorsprongBijVerdragVrijgesteldInkomen.HasValue ? Data.Overzicht.OpdelingNaarOorsprongBijVerdragVrijgesteldInkomen.Value : 0;

            if (!Data.Overzicht.OpdelingNaarOorsprongBijVerdragVrijgesteldInkomen.HasValue) Data.Overzicht.OpdelingNaarOorsprongBijVerdragVrijgesteldInkomen = 0;
            if (!Data.Overzicht.TotaalFiscaleAftrek.HasValue) Data.Overzicht.TotaalFiscaleAftrek = 0;

            Data.Overzicht.SubTotaalNaAftrek = (Data.Overzicht.SubTotaalBelastbaarVoorAftrek - Math.Abs(Data.Overzicht.OpdelingNaarOorsprongBijVerdragVrijgesteldInkomen.Value)) - Data.Overzicht.TotaalFiscaleAftrek;
            if (!Data.Overzicht.SubTotaalNaAftrek.HasValue) Data.Overzicht.SubTotaalNaAftrek = 0;
            if (Data.Overzicht.SubTotaalNaAftrek.Value < 0)
            {
                Data.Overzicht.SubTotaalNaAftrek = 0;
            }

            Data.Overzicht.TotaalBelastbareBestandenZonderAftrekPlus = Data.BelastbareBestZonderAftrek.TETotal;
            if (!Data.Overzicht.TotaalBelastbareBestandenZonderAftrekPlus.HasValue) Data.Overzicht.TotaalBelastbareBestandenZonderAftrekPlus = 0;

            Data.Overzicht.BelastbareGrondslag = Data.Overzicht.SubTotaalNaAftrek + Data.Overzicht.TotaalBelastbareBestandenZonderAftrekPlus;

            // TAX CALCULATION
            int assessment = worksheetset.DataContract.AssessmentYear;
            Data.BelastingsBerekening.AssessmentYear = worksheetset.DataContract.AssessmentYear;

            // belastbare grondslag is reeds incl. VAA
            Data.BelastingsBerekening.CorporateIncomeTaxFromResult = Data.Overzicht.BelastbareGrondslag;

            /*
             * Meerwaarden aandelen:
             * Als belastbare grondslag negatief, dan niks
             * Als belastbare grondslag positief, eerst meerwaarden aandelen aanwenden
             * MAAR zonder VAA deel auto (=STEEDS standaard belast)
             */

            decimal? meerwaardenAandelen = worksheetset.MeerwaardeAandelenApp.Data.MeerwaardeAandelenLijst.Where(d => d.Resultaat != null && d.Resultaat.Id == mwaandelenid).Sum(d => d.Saldo).GetValueOrDefault();

            decimal? vaaDeelAuto = Data.BelastbareBestZonderAftrek.TEVaaCar.GetValueOrDefault();
            Data.BelastingsBerekening.CapitalGainsShares25 = meerwaardenAandelen.GetValueOrDefault();

            decimal belastbareGrondslagZonderVAA = Data.BelastingsBerekening.CorporateIncomeTaxFromResult.GetValueOrDefault() - vaaDeelAuto.GetValueOrDefault();

            

            Data.BelastingsBerekening.CapitalGainsShares25Perc = worksheetset.GetCoefficient("VENB_MEERWAARDENAANDELEN_PERCENT", assessment).Value;
            Data.BelastingsBerekening.ExitTarifPerc = worksheetset.GetCoefficient("VENB_EXITTARIEF_PERCENT", assessment).Value;

            if (belastbareGrondslagZonderVAA <= 0)
            {
                meerwaardenAandelen = 0;
            }
            if (belastbareGrondslagZonderVAA < meerwaardenAandelen.GetValueOrDefault())
            {
                meerwaardenAandelen = belastbareGrondslagZonderVAA;
            }
            Data.BelastingsBerekening.CapitalGainsShares25 = meerwaardenAandelen;

            //Data.Overzicht.BelastbareGrondslag

            Data.BelastingsBerekening.EenduidigTariefPerc = worksheetset.GetCoefficient("VENB_EENDUIDIGTARIEF_PERCENT", assessment).Value;
            decimal verminderdBovenGrens = worksheetset.GetCoefficient("VENB_GRENS_STAP3_TOT", assessment).Value;
            
            Data.BelastingsBerekening.VerminderdtariefStap1Base = 0;
            Data.BelastingsBerekening.VerminderdtariefStap1Perc = 0;
            Data.BelastingsBerekening.VerminderdtariefStap1 = 0;
            Data.BelastingsBerekening.VerminderdtariefStap2Base = 0;
            Data.BelastingsBerekening.VerminderdtariefStap2Perc = 0;
            Data.BelastingsBerekening.VerminderdtariefStap2 = 0;
            Data.BelastingsBerekening.VerminderdtariefStap3Base = 0;
            Data.BelastingsBerekening.VerminderdtariefStap3Perc = 0;
            Data.BelastingsBerekening.VerminderdtariefStap3 = 0;

            Data.BelastingsBerekening.CorporateIncomeTaxFromResult = Data.BelastingsBerekening.CorporateIncomeTaxFromResult.GetValueOrDefault();


            Data.BelastingsBerekening.CapitalGainsShares25Result = Math.Round(Data.BelastingsBerekening.CapitalGainsShares25.GetValueOrDefault() * (Data.BelastingsBerekening.CapitalGainsShares25Perc.GetValueOrDefault() / 100), 2, MidpointRounding.AwayFromZero);
            Data.BelastingsBerekening.ExitTarifResult = Math.Round(Data.BelastingsBerekening.ExitTarif.GetValueOrDefault() * (Data.BelastingsBerekening.ExitTarifPerc.GetValueOrDefault() / 100), 2, MidpointRounding.AwayFromZero);

            decimal remainingTaxNormalTarif = Data.BelastingsBerekening.CorporateIncomeTaxFromResult.GetValueOrDefault()
                - Data.BelastingsBerekening.CapitalGainsShares25.GetValueOrDefault()
                - Data.BelastingsBerekening.ExitTarif.GetValueOrDefault();
            if (remainingTaxNormalTarif < 0) remainingTaxNormalTarif = 0;

            if ((((Data.Tarief.BezitAandelenMeerDan50Percent == true || Data.Tarief.BezitAandelenDoorVennootschappen == true) || Data.Tarief.UitgekeerdDividendHogerDan13 == true) || Data.Tarief.ToegekendeBezoldiging == true) || (Data.BelastingsBerekening.CorporateIncomeTaxFromResult.Value > verminderdBovenGrens))
            {
                Data.BelastingsBerekening.EenduidigTariefBedrag = remainingTaxNormalTarif;
                Data.BelastingsBerekening.EenduidigTariefTot = Math.Round(Data.BelastingsBerekening.EenduidigTariefBedrag.Value * (Data.BelastingsBerekening.EenduidigTariefPerc.Value / 100), 2, MidpointRounding.AwayFromZero);
                Data.BelastingsBerekening.Verminderd = false;
            }
            else
            {
                Data.BelastingsBerekening.Verminderd = true;
                Data.BelastingsBerekening.EenduidigTariefBedrag = 0;
                Data.BelastingsBerekening.EenduidigTariefTot = 0;

                Data.BelastingsBerekening.VerminderdtariefStap1Base = worksheetset.GetCoefficient("VENB_GRENS_STAP1_TOT", assessment).Value;
                Data.BelastingsBerekening.VerminderdtariefStap2Base = worksheetset.GetCoefficient("VENB_GRENS_STAP2_TOT", assessment).Value;
                Data.BelastingsBerekening.VerminderdtariefStap3Base = worksheetset.GetCoefficient("VENB_GRENS_STAP3_TOT", assessment).Value;

                Data.BelastingsBerekening.VerminderdtariefStap1Perc = worksheetset.GetCoefficient("VENB_VERMINDERTARIEF_STAP1_PERCENT", assessment).Value;
                Data.BelastingsBerekening.VerminderdtariefStap2Perc = worksheetset.GetCoefficient("VENB_VERMINDERTARIEF_STAP2_PERCENT", assessment).Value;
                Data.BelastingsBerekening.VerminderdtariefStap3Perc = worksheetset.GetCoefficient("VENB_VERMINDERTARIEF_STAP3_PERCENT", assessment).Value;

                if (remainingTaxNormalTarif < Data.BelastingsBerekening.VerminderdtariefStap1Base)
                {
                    Data.BelastingsBerekening.VerminderdtariefStap1Base = remainingTaxNormalTarif;
                    Data.BelastingsBerekening.VerminderdtariefStap1 = Math.Round(Data.BelastingsBerekening.VerminderdtariefStap1Base.Value * (Data.BelastingsBerekening.VerminderdtariefStap1Perc.Value / 100), 2, MidpointRounding.AwayFromZero);
                    Data.BelastingsBerekening.VerminderdtariefStap2 = 0;
                    Data.BelastingsBerekening.VerminderdtariefStap3 = 0;
                    Data.BelastingsBerekening.VerminderdtariefStap2Base = 0;
                    Data.BelastingsBerekening.VerminderdtariefStap3Base = 0;
                }
                else
                {
                    Data.BelastingsBerekening.VerminderdtariefStap1 = Math.Round(Data.BelastingsBerekening.VerminderdtariefStap1Base.Value * (Data.BelastingsBerekening.VerminderdtariefStap1Perc.Value / 100), 2, MidpointRounding.AwayFromZero);
                    if (remainingTaxNormalTarif < Data.BelastingsBerekening.VerminderdtariefStap2Base)
                    {
                        Data.BelastingsBerekening.VerminderdtariefStap2Base = remainingTaxNormalTarif - Data.BelastingsBerekening.VerminderdtariefStap1Base;
                        Data.BelastingsBerekening.VerminderdtariefStap2 = Math.Round(Data.BelastingsBerekening.VerminderdtariefStap2Base.Value * (Data.BelastingsBerekening.VerminderdtariefStap2Perc.Value / 100), 2, MidpointRounding.AwayFromZero);
                        
                        Data.BelastingsBerekening.VerminderdtariefStap3 = 0;
                        Data.BelastingsBerekening.VerminderdtariefStap3Base = 0;
                    }
                    else
                    {
                        Data.BelastingsBerekening.VerminderdtariefStap2Base = Data.BelastingsBerekening.VerminderdtariefStap2Base - Data.BelastingsBerekening.VerminderdtariefStap1Base;
                        Data.BelastingsBerekening.VerminderdtariefStap2 = Math.Round(Data.BelastingsBerekening.VerminderdtariefStap2Base.Value * (Data.BelastingsBerekening.VerminderdtariefStap2Perc.Value / 100), 2, MidpointRounding.AwayFromZero);
                        if (remainingTaxNormalTarif < Data.BelastingsBerekening.VerminderdtariefStap3Base)
                        {
                            Data.BelastingsBerekening.VerminderdtariefStap3Base = (remainingTaxNormalTarif - Data.BelastingsBerekening.VerminderdtariefStap2Base) - Data.BelastingsBerekening.VerminderdtariefStap1Base;
                            Data.BelastingsBerekening.VerminderdtariefStap3 = Math.Round(Data.BelastingsBerekening.VerminderdtariefStap3Base.Value * (Data.BelastingsBerekening.VerminderdtariefStap3Perc.Value / 100), 2, MidpointRounding.AwayFromZero);

                        }
                        else
                        {
                            Data.BelastingsBerekening.EenduidigTariefBedrag = remainingTaxNormalTarif;
                            Data.BelastingsBerekening.EenduidigTariefTot = Math.Round(Data.BelastingsBerekening.EenduidigTariefBedrag.Value * (Data.BelastingsBerekening.EenduidigTariefPerc.Value / 100), 2, MidpointRounding.AwayFromZero);

                        }
                    }
                }
            }
            Data.BelastingsBerekening.CorporateIncomeTax =
                ((Data.BelastingsBerekening.EenduidigTariefTot + Data.BelastingsBerekening.VerminderdtariefStap1) + Data.BelastingsBerekening.VerminderdtariefStap2) + Data.BelastingsBerekening.VerminderdtariefStap3
                + Data.BelastingsBerekening.ExitTarifResult.GetValueOrDefault() + Data.BelastingsBerekening.CapitalGainsShares25Result.GetValueOrDefault();

            // basis voor vermeerdering inclusief  meerwaarden & exit tarief  of niet?
            Data.BelastingsBerekening.BasisForIncrease = ((Data.BelastingsBerekening.CorporateIncomeTax - Data.BelastingsBerekening.NonRefundableWithholdingTaxes_ForeignTaxCredit) - Data.BelastingsBerekening.RefundableWithholdingTaxes) - Data.BelastingsBerekening.RefundableTaxeCredit;
            if(!Data.BelastingsBerekening.BasisForIncrease.HasValue) Data.BelastingsBerekening.BasisForIncrease =0;

            if (Data.BelastingsBerekening.BasisForIncrease < 0)
            {
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Warning, "BELOWZERO", "BelastingsBerekening", Data.BelastingsBerekening.Id, "BasisForIncrease", new object[] { Data.BelastingsBerekening.BasisForIncrease.ToString() });
                Data.BelastingsBerekening.BasisForIncrease = 0;
            }
            if (!Data.BelastingsBerekening.BasisForIncrease.HasValue) Data.BelastingsBerekening.BasisForIncrease = 0;

            Data.BelastingsBerekening.BTIBase = Data.BelastingsBerekening.BasisForIncrease;
            Data.BelastingsBerekening.AP1Percentage = worksheetset.GetCoefficient("VENB_VERMEERDERING_VA1_PERCENT", assessment).Value;
            Data.BelastingsBerekening.AP2Percentage = worksheetset.GetCoefficient("VENB_VERMEERDERING_VA2_PERCENT", assessment).Value;
            Data.BelastingsBerekening.AP3Percentage = worksheetset.GetCoefficient("VENB_VERMEERDERING_VA3_PERCENT", assessment).Value;
            Data.BelastingsBerekening.AP4Percentage = worksheetset.GetCoefficient("VENB_VERMEERDERING_VA4_PERCENT", assessment).Value;

            Data.BelastingsBerekening.BTIPercentage = (((Data.BelastingsBerekening.AP1Percentage + Data.BelastingsBerekening.AP2Percentage) + Data.BelastingsBerekening.AP3Percentage) + Data.BelastingsBerekening.AP4Percentage) / 4;

            Data.BelastingsBerekening.BTITotal = Math.Round(Data.BelastingsBerekening.BTIBase.Value * (Data.BelastingsBerekening.BTIPercentage.Value / 100), 2, MidpointRounding.AwayFromZero);

            if (!Data.BelastingsBerekening.AP1Base.HasValue) Data.BelastingsBerekening.AP1Base = 0;
            if (!Data.BelastingsBerekening.AP1Base.HasValue) Data.BelastingsBerekening.AP2Base = 0;
            if (!Data.BelastingsBerekening.AP1Base.HasValue) Data.BelastingsBerekening.AP3Base = 0;
            if (!Data.BelastingsBerekening.AP1Base.HasValue) Data.BelastingsBerekening.AP4Base = 0;

            Data.BelastingsBerekening.AP1Total = Math.Round(Data.BelastingsBerekening.AP1Base.Value * (Data.BelastingsBerekening.AP1Percentage.Value / 100), 2, MidpointRounding.AwayFromZero);
            Data.BelastingsBerekening.AP2Total = Math.Round(Data.BelastingsBerekening.AP2Base.Value * (Data.BelastingsBerekening.AP2Percentage.Value / 100), 2, MidpointRounding.AwayFromZero);
            Data.BelastingsBerekening.AP3Total = Math.Round(Data.BelastingsBerekening.AP3Base.Value * (Data.BelastingsBerekening.AP3Percentage.Value / 100), 2, MidpointRounding.AwayFromZero);
            Data.BelastingsBerekening.AP4Total = Math.Round(Data.BelastingsBerekening.AP4Base.Value * (Data.BelastingsBerekening.AP4Percentage.Value / 100), 2, MidpointRounding.AwayFromZero);

            Data.BelastingsBerekening.APBaseTotal = ((Data.BelastingsBerekening.AP1Base + Data.BelastingsBerekening.AP2Base) + Data.BelastingsBerekening.AP3Base) + Data.BelastingsBerekening.AP4Base;

            Data.BelastingsBerekening.APTotal = ((Data.BelastingsBerekening.AP1Total + Data.BelastingsBerekening.AP2Total) + Data.BelastingsBerekening.AP3Total) + Data.BelastingsBerekening.AP4Total;

            Data.BelastingsBerekening.Test1percRule = Data.BelastingsBerekening.BasisForIncrease * (decimal)0.01;

            if(((Data.BelastingsBerekening.BTITotal - Data.BelastingsBerekening.APTotal) < Data.BelastingsBerekening.Test1percRule) || ((Data.BelastingsBerekening.BTITotal - Data.BelastingsBerekening.APTotal) < 30)) {
                Data.BelastingsBerekening.TaxIncreaseInsufficientAdvancePayments= 0;
            } else {
                Data.BelastingsBerekening.TaxIncreaseInsufficientAdvancePayments = Data.BelastingsBerekening.BTITotal - Data.BelastingsBerekening.APTotal;
            }

            Data.BelastingsBerekening.BasisCorporateIncomeTax= Data.BelastingsBerekening.CorporateIncomeTax;
            Data.BelastingsBerekening.ApplicableTaxIncrease = Data.BelastingsBerekening.TaxIncreaseInsufficientAdvancePayments;

            if(Data.BelastingsBerekening.JongVennootschap==true) {
                Data.BelastingsBerekening.ApplicableTaxIncrease=0;
            }

            
            if (!Data.BelastingsBerekening.NonRefundableWithholdingTaxes_TaxCredit.HasValue) Data.BelastingsBerekening.NonRefundableWithholdingTaxes_TaxCredit = 0;
            Data.BelastingsBerekening.NonRefundableWithholdingTaxes_ForeignTaxCredit = (Data.BelastingsBerekening.BasisCorporateIncomeTax + Data.BelastingsBerekening.ApplicableTaxIncrease) > 0 ? Math.Abs(Data.BelastingsBerekening.NonRefundableWithholdingTaxes_TaxCredit.Value) : 0;

            if(Data.BelastingsBerekening.NonRefundableWithholdingTaxes_ForeignTaxCredit > (Data.BelastingsBerekening.BasisCorporateIncomeTax + Data.BelastingsBerekening.ApplicableTaxIncrease)) {
                Data.BelastingsBerekening.NonRefundableWithholdingTaxes_ForeignTaxCredit= Data.BelastingsBerekening.BasisCorporateIncomeTax + Data.BelastingsBerekening.ApplicableTaxIncrease;
            }
            if (!Data.BelastingsBerekening.RefundableTaxeCredit.HasValue) Data.BelastingsBerekening.RefundableTaxeCredit = 0;
            Data.BelastingsBerekening.RefundableTaxCredit= Math.Abs(Data.BelastingsBerekening.RefundableTaxeCredit.Value);

            if (!Data.BelastingsBerekening.RefundableWithholdingTaxes.HasValue) Data.BelastingsBerekening.RefundableWithholdingTaxes = 0;
            Data.BelastingsBerekening.RefundableWithholdingTaxesEnd = Math.Abs(Data.BelastingsBerekening.RefundableWithholdingTaxes.Value);

            if (!Data.BelastingsBerekening.APBaseTotal.HasValue) Data.BelastingsBerekening.APBaseTotal = 0;
            Data.BelastingsBerekening.AdvancePaymentsEnd = Math.Abs(Data.BelastingsBerekening.APBaseTotal.Value);

            Data.BelastingsBerekening.OutstandingLiability_BelastingsBerekening = ((((Data.BelastingsBerekening.BasisCorporateIncomeTax + Data.BelastingsBerekening.ApplicableTaxIncrease) - Data.BelastingsBerekening.NonRefundableWithholdingTaxes_ForeignTaxCredit) - Data.BelastingsBerekening.RefundableTaxCredit) - Data.BelastingsBerekening.RefundableWithholdingTaxesEnd) - Data.BelastingsBerekening.AdvancePaymentsEnd;

            

            
          


            Data.FiscaleAftrek.PIDBelgium = Data.FiscaleAftrek.PIDBelgium.HasValue ? Data.FiscaleAftrek.PIDBelgium.Value : 0;

            Data.Aanslag.KapitaalEnInterestsubsidies = Data.Aanslag.KapitaalEnInterestsubsidies.HasValue ? Data.Aanslag.KapitaalEnInterestsubsidies : 0;
            if (Data.Aanslag.KapitaalEnInterestsubsidies > 0)
            {
                Data.BelastingsBerekening.OutstandingLiability_BelastingsBerekening += Data.Aanslag.KapitaalEnInterestsubsidies * (decimal)0.0515;
                worksheetset.BerekeningVenBApp.Data.BelastingsBerekening.KapitaalEnInterestsubsidies = Data.Aanslag.KapitaalEnInterestsubsidies * (decimal)0.0515;
            }
            else {
                worksheetset.BerekeningVenBApp.Data.BelastingsBerekening.KapitaalEnInterestsubsidies = 0;
            }

            // afzonderlijke aanslagen
            Data.Aanslag.NietVerantwoordeKosten = Data.Aanslag.NietVerantwoordeKosten.HasValue ? Data.Aanslag.NietVerantwoordeKosten : 0;
            if (Data.Aanslag.NietVerantwoordeKosten > 0) {
                Data.BelastingsBerekening.OutstandingLiability_BelastingsBerekening += Data.Aanslag.NietVerantwoordeKosten * (decimal)3.09;
                worksheetset.BerekeningVenBApp.Data.BelastingsBerekening.NietVerantwoordeKosten = Data.Aanslag.NietVerantwoordeKosten * (decimal)3.09;
            }
            else
            {
                worksheetset.BerekeningVenBApp.Data.BelastingsBerekening.NietVerantwoordeKosten = 0;
            }

            Data.Aanslag.AfzonderlijkeAanslag34 = Data.Aanslag.AfzonderlijkeAanslag34.HasValue ? Data.Aanslag.AfzonderlijkeAanslag34 : 0;
            if (Data.Aanslag.AfzonderlijkeAanslag34 > 0)
            {
                Data.BelastingsBerekening.OutstandingLiability_BelastingsBerekening += Data.Aanslag.AfzonderlijkeAanslag34 * (decimal)0.3502;
                worksheetset.BerekeningVenBApp.Data.BelastingsBerekening.AfzonderlijkeAanslag34 = Data.Aanslag.AfzonderlijkeAanslag34 * (decimal)0.3502;
            }
            else {
                worksheetset.BerekeningVenBApp.Data.BelastingsBerekening.AfzonderlijkeAanslag34 = 0;
            }
            Data.Aanslag.AfzonderlijkeAanslag28 = Data.Aanslag.AfzonderlijkeAanslag28.HasValue ? Data.Aanslag.AfzonderlijkeAanslag28 : 0;
            if (Data.Aanslag.AfzonderlijkeAanslag28 > 0)
            {
                Data.BelastingsBerekening.OutstandingLiability_BelastingsBerekening += Data.Aanslag.AfzonderlijkeAanslag28 * (decimal)0.2884;
                worksheetset.BerekeningVenBApp.Data.BelastingsBerekening.AfzonderlijkeAanslag28 = Data.Aanslag.AfzonderlijkeAanslag28 * (decimal)0.2884;
            }
            else {
                worksheetset.BerekeningVenBApp.Data.BelastingsBerekening.AfzonderlijkeAanslag28 = 0;
            }
            Data.Aanslag.AfzonderlijkeAanslagDividenden = Data.Aanslag.AfzonderlijkeAanslagDividenden.HasValue ? Data.Aanslag.AfzonderlijkeAanslagDividenden : 0;
            if (Data.Aanslag.AfzonderlijkeAanslagDividenden > 0)
            {
                Data.BelastingsBerekening.OutstandingLiability_BelastingsBerekening += Data.Aanslag.AfzonderlijkeAanslagDividenden * (decimal)0.3502;
                worksheetset.BerekeningVenBApp.Data.BelastingsBerekening.AfzonderlijkeAanslagDividenden = Data.Aanslag.AfzonderlijkeAanslagDividenden * (decimal)0.3502;
            }
            else {
                worksheetset.BerekeningVenBApp.Data.BelastingsBerekening.AfzonderlijkeAanslagDividenden = 0;
            }

            // bijzondere aanslagen
            Data.Aanslag.VerdelingVermogen33 = Data.Aanslag.VerdelingVermogen33.HasValue ? Data.Aanslag.VerdelingVermogen33 : 0;
            if (Data.Aanslag.VerdelingVermogen33 > 0)
            {
                Data.BelastingsBerekening.OutstandingLiability_BelastingsBerekening += Data.Aanslag.VerdelingVermogen33 * (decimal)0.33;
                worksheetset.BerekeningVenBApp.Data.BelastingsBerekening.VerdelingVermogen33 = Data.Aanslag.VerdelingVermogen33 * (decimal)0.33;
            }
            else {
                worksheetset.BerekeningVenBApp.Data.BelastingsBerekening.VerdelingVermogen33 = 0;
            }
            Data.Aanslag.VerdelingVermogen16 = Data.Aanslag.VerdelingVermogen16.HasValue ? Data.Aanslag.VerdelingVermogen16 : 0;
            if (Data.Aanslag.VerdelingVermogen16 > 0)
            {
                Data.BelastingsBerekening.OutstandingLiability_BelastingsBerekening += Data.Aanslag.VerdelingVermogen16 * (decimal)0.165;
                worksheetset.BerekeningVenBApp.Data.BelastingsBerekening.VerdelingVermogen16 = Data.Aanslag.VerdelingVermogen16 * (decimal)0.165;
            }
            else {
                worksheetset.BerekeningVenBApp.Data.BelastingsBerekening.VerdelingVermogen16 = 0;
            }
            Data.Aanslag.VAAVennootschappen = Data.Aanslag.VAAVennootschappen.HasValue ? Data.Aanslag.VAAVennootschappen : 0;
            if (Data.Aanslag.VAAVennootschappen > 0)
            {
                Data.BelastingsBerekening.OutstandingLiability_BelastingsBerekening += Data.Aanslag.VAAVennootschappen * (decimal)0.45;
                worksheetset.BerekeningVenBApp.Data.BelastingsBerekening.VAAVennootschappen = Data.Aanslag.VAAVennootschappen * (decimal)0.45;
            }
            else {
                worksheetset.BerekeningVenBApp.Data.BelastingsBerekening.VAAVennootschappen = 0;
            }

            // diamant
            Data.Aanslag.AanvullendeHeffingDiamant = Data.Aanslag.AanvullendeHeffingDiamant.HasValue ? Data.Aanslag.AanvullendeHeffingDiamant : 0;
            if (Data.Aanslag.AanvullendeHeffingDiamant > 0)
            {
                Data.BelastingsBerekening.OutstandingLiability_BelastingsBerekening += Data.Aanslag.AanvullendeHeffingDiamant * (decimal)0.15;
                worksheetset.BerekeningVenBApp.Data.BelastingsBerekening.AanvullendeHeffingDiamant = Data.Aanslag.AanvullendeHeffingDiamant * (decimal)0.15;
            }
            else {
                worksheetset.BerekeningVenBApp.Data.BelastingsBerekening.AanvullendeHeffingDiamant = 0;
            }
            Data.Aanslag.TerugbetalingBelastingskrediet = Data.Aanslag.TerugbetalingBelastingskrediet.HasValue ? Data.Aanslag.TerugbetalingBelastingskrediet : 0;
            if (Data.Aanslag.TerugbetalingBelastingskrediet > 0)
            {
                Data.BelastingsBerekening.OutstandingLiability_BelastingsBerekening += Data.Aanslag.TerugbetalingBelastingskrediet;
                worksheetset.BerekeningVenBApp.Data.BelastingsBerekening.TerugbetalingBelastingskrediet = Data.Aanslag.TerugbetalingBelastingskrediet;
            }
            else {
                worksheetset.BerekeningVenBApp.Data.BelastingsBerekening.TerugbetalingBelastingskrediet = 0;
            }
            Data.Aanslag.TotaalBedragVoorzieningen = Data.Aanslag.TotaalBedragVoorzieningen.HasValue ? Data.Aanslag.TotaalBedragVoorzieningen : 0;

            Data.Aanslag.AfzonderlijkeAanslagGespreid = Data.Aanslag.AfzonderlijkeAanslagGespreid.GetValueOrDefault();

            Data.BelastingsBerekening.OutstandingLiability_BelastingsBerekening += Data.Aanslag.IPT2014.GetValueOrDefault();
            Data.BelastingsBerekening.TotaalBedragVoorzieningen = Data.Aanslag.IPT2014.GetValueOrDefault();

            /*
            if (Data.Aanslag.TotaalBedragVoorzieningen > 0)
            {
                if ((bool)Data.Aanslag.AfzonderlijkeAanslagGespreid)
                {
                    Data.BelastingsBerekening.OutstandingLiability_BelastingsBerekening += Data.Aanslag.TotaalBedragVoorzieningen * (decimal)0.06;
                    worksheetset.BerekeningVenBApp.Data.BelastingsBerekening.TotaalBedragVoorzieningen = Data.Aanslag.TotaalBedragVoorzieningen * (decimal)0.06;
                }
                else
                {
                    Data.BelastingsBerekening.OutstandingLiability_BelastingsBerekening += Data.Aanslag.TotaalBedragVoorzieningen * (decimal)0.175;
                    worksheetset.BerekeningVenBApp.Data.BelastingsBerekening.TotaalBedragVoorzieningen = Data.Aanslag.TotaalBedragVoorzieningen * (decimal)0.175;
                }
            }
            else {
                worksheetset.BerekeningVenBApp.Data.BelastingsBerekening.TotaalBedragVoorzieningen = 0;
            }
            */
            
           
              /*                   
          // AANVULLENDE HEFFING
          decimal AdditionalDutiesDiamondTraders = Number(Xbrl.Elements.Where(x => x.Name == "AdditionalDutiesDiamondTraders"), "0");
          if (AdditionalDutiesDiamondTraders > 0)
          {
              taxcalc += AdditionalDutiesDiamondTraders * (decimal)0.15;
          }

          // TOCHECK !!!
          decimal RetributionTaxCreditResearchDevelopment = Number(Xbrl.Elements.Where(x => x.Name == "RetributionTaxCreditResearchDevelopment"), "0");
          if (RetributionTaxCreditResearchDevelopment > 0)
          {
              taxcalc += RetributionTaxCreditResearchDevelopment;
          }

          bool SeparateAssessmentAdditionalIndividualPensionProvisionsSpreadThreeAssessmentYears2013Until2015 = Bool(Xbrl.Elements.FirstOrDefault(x => x.Name == "SeparateAssessmentAdditionalIndividualPensionProvisionsSpreadThreeAssessmentYears2013Until2015"));
          decimal SeparateAssessmentAdditionalIndividualPensionProvisionsOnEndTaxPeriodClosingDateBefore20120101 = Number(Xbrl.Elements.Where(x => x.Name == "SeparateAssessmentAdditionalIndividualPensionProvisionsOnEndTaxPeriodClosingDateBefore2012-01-01"), "0");

          if (SeparateAssessmentAdditionalIndividualPensionProvisionsOnEndTaxPeriodClosingDateBefore20120101 > 0)
          {
              if (SeparateAssessmentAdditionalIndividualPensionProvisionsSpreadThreeAssessmentYears2013Until2015)
              {
                  taxcalc += SeparateAssessmentAdditionalIndividualPensionProvisionsOnEndTaxPeriodClosingDateBefore20120101 * (decimal)0.06;
              }
              else
              {
                  taxcalc += SeparateAssessmentAdditionalIndividualPensionProvisionsOnEndTaxPeriodClosingDateBefore20120101 * (decimal)0.175;
              }
          }

           */



            Data.LastUpdated = DateTime.Now;

//            Data.BelastingsBerekening.AP1Base = bels


           // Data.LastCalculated = DateTime.Now;
        }

        #region AddUpdateDelete

        public void UpdateOverzicht(EY.com.eBook.RuleEngine.AY2013.BerekeningVenBAppTypes.General entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2013.BerekeningVenBAppTypes.General original = Data.Overzicht;

            entity = Validate(entity, "Overzicht", ref worksheetset);
            if (entity.IsValid)
            {

                Data.Overzicht = entity;
                Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
            }
        }

        public void UpdateBelasteReserves(EY.com.eBook.RuleEngine.AY2013.BerekeningVenBAppTypes.BelasteReservesItem entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2013.BerekeningVenBAppTypes.BelasteReservesItem original = Data.BelasteReserves;

            entity = Validate(entity, "BelasteReserves", ref worksheetset);
            if (entity.IsValid)
            {

                Data.BelasteReserves = entity;
                Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
            }
        }

        public void UpdateVerworpenUitgaven(EY.com.eBook.RuleEngine.AY2013.BerekeningVenBAppTypes.VerworpenUitgavenItem entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2013.BerekeningVenBAppTypes.VerworpenUitgavenItem original = Data.VerworpenUitgaven;

            entity = Validate(entity, "VerworpenUitgaven", ref worksheetset);
            if (entity.IsValid)
            {

                Data.VerworpenUitgaven = entity;
                Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
            }
        }

        public void UpdateBelastbareBestZonderAftrek(EY.com.eBook.RuleEngine.AY2013.BerekeningVenBAppTypes.BelastbareBestZonderAftrekItem entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2013.BerekeningVenBAppTypes.BelastbareBestZonderAftrekItem original = Data.BelastbareBestZonderAftrek;

            entity = Validate(entity, "BelastbareBestZonderAftrek", ref worksheetset);
            if (entity.IsValid)
            {

                Data.BelastbareBestZonderAftrek = entity;
                Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
            }
        }

        public void UpdateOpdelingNaarOorsprong(EY.com.eBook.RuleEngine.AY2013.BerekeningVenBAppTypes.OpdelingNaarOorsprongItem entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2013.BerekeningVenBAppTypes.OpdelingNaarOorsprongItem original = Data.OpdelingNaarOorsprong;

            entity = Validate(entity, "OpdelingNaarOorsprong", ref worksheetset);
            if (entity.IsValid)
            {

                Data.OpdelingNaarOorsprong = entity;
                Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
            }
        }

        public void UpdateFiscaleAftrek(EY.com.eBook.RuleEngine.AY2013.BerekeningVenBAppTypes.FiscaleAftrekItem entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2013.BerekeningVenBAppTypes.FiscaleAftrekItem original = Data.FiscaleAftrek;

            entity = Validate(entity, "FiscaleAftrek", ref worksheetset);
            if (entity.IsValid)
            {

                Data.FiscaleAftrek = entity;
                Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
            }
        }

        public void UpdateTarief(EY.com.eBook.RuleEngine.AY2013.BerekeningVenBAppTypes.TariefItem entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2013.BerekeningVenBAppTypes.TariefItem original = Data.Tarief;

            entity = Validate(entity, "Tarief", ref worksheetset);
            if (entity.IsValid)
            {

                Data.Tarief = entity;
                Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
            }
        }

        public void UpdateAanslag(EY.com.eBook.RuleEngine.AY2013.BerekeningVenBAppTypes.AanslagItem entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2013.BerekeningVenBAppTypes.AanslagItem original = Data.Aanslag;

            entity = Validate(entity,"Aanslag", ref worksheetset);
            if (entity.IsValid)
            {
                entity.IPT2013 = 0;
                entity.IPT2014 = 0;
                entity.IPT2015 = 0;
                if(entity.TotaalBedragVoorzieningen.GetValueOrDefault()>0) {
                    if (entity.AfzonderlijkeAanslagGespreid.GetValueOrDefault())
                    {
                        entity.IPT2013 = Math.Round(entity.TotaalBedragVoorzieningen.GetValueOrDefault() * (decimal)0.006, 2, MidpointRounding.AwayFromZero);
                        entity.IPT2014 = Math.Round(entity.TotaalBedragVoorzieningen.GetValueOrDefault() * (decimal)0.006, 2, MidpointRounding.AwayFromZero);
                        entity.IPT2015 = Math.Round(entity.TotaalBedragVoorzieningen.GetValueOrDefault() * (decimal)0.006, 2, MidpointRounding.AwayFromZero);
                    }
                    else
                    {
                        entity.IPT2013 = Math.Round(entity.TotaalBedragVoorzieningen.GetValueOrDefault() * (decimal)0.0175, 2, MidpointRounding.AwayFromZero);
                    }
                }
                

                Data.Aanslag = entity;
                Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
            }
        }

        public void UpdateBelastingsBerekening(EY.com.eBook.RuleEngine.AY2013.BerekeningVenBAppTypes.BelastingsBerekeningItem entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2013.BerekeningVenBAppTypes.BelastingsBerekeningItem original = Data.BelastingsBerekening;

            entity = Validate(entity, "BelastingsBerekening", ref worksheetset);
            if (entity.IsValid)
            {

                Data.BelastingsBerekening = entity;
                Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
            }
        }

        public void UpdateBelastingsBerekening2(EY.com.eBook.RuleEngine.AY2013.BerekeningVenBAppTypes.BelastingsBerekeningItem2 entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2013.BerekeningVenBAppTypes.BelastingsBerekeningItem2 original = Data.BelastingsBerekening2;

            entity = Validate(entity, "BelastingsBerekening2", ref worksheetset);
            if (entity.IsValid)
            {

                Data.BelastingsBerekening2 = entity;
                Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
            }
        }

        #endregion

        #region VALIDATION

        public EY.com.eBook.RuleEngine.AY2013.BerekeningVenBAppTypes.General Validate(EY.com.eBook.RuleEngine.AY2013.BerekeningVenBAppTypes.General entity, string location, ref WorksheetSetData worksheetset)
        {
            entity.IsValid = true;
            worksheetset.ClearMessages(TypeId, entity.Id);
            /*
            if (!entity.Bedrag.HasValue || entity.Bedrag.Value==0)
            {
            worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Grootboek");
            entity.IsValid = false;
            }
            */

            return entity;
        }

        public EY.com.eBook.RuleEngine.AY2013.BerekeningVenBAppTypes.BelasteReservesItem Validate(EY.com.eBook.RuleEngine.AY2013.BerekeningVenBAppTypes.BelasteReservesItem entity, string location, ref WorksheetSetData worksheetset)
        {
            entity.IsValid = true;
            worksheetset.ClearMessages(TypeId, entity.Id);
            /*
            if (!entity.Bedrag.HasValue || entity.Bedrag.Value==0)
            {
            worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Grootboek");
            entity.IsValid = false;
            }
            */

            return entity;
        }

        public EY.com.eBook.RuleEngine.AY2013.BerekeningVenBAppTypes.VerworpenUitgavenItem Validate(EY.com.eBook.RuleEngine.AY2013.BerekeningVenBAppTypes.VerworpenUitgavenItem entity, string location, ref WorksheetSetData worksheetset)
        {
            entity.IsValid = true;
            worksheetset.ClearMessages(TypeId, entity.Id);
            /*
            if (!entity.Bedrag.HasValue || entity.Bedrag.Value==0)
            {
            worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Grootboek");
            entity.IsValid = false;
            }
            */

            return entity;
        }

        public EY.com.eBook.RuleEngine.AY2013.BerekeningVenBAppTypes.BelastbareBestZonderAftrekItem Validate(EY.com.eBook.RuleEngine.AY2013.BerekeningVenBAppTypes.BelastbareBestZonderAftrekItem entity, string location, ref WorksheetSetData worksheetset)
        {
            entity.IsValid = true;
            worksheetset.ClearMessages(TypeId, entity.Id);
            /*
            if (!entity.Bedrag.HasValue || entity.Bedrag.Value==0)
            {
            worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Grootboek");
            entity.IsValid = false;
            }
            */

            return entity;
        }

        public EY.com.eBook.RuleEngine.AY2013.BerekeningVenBAppTypes.OpdelingNaarOorsprongItem Validate(EY.com.eBook.RuleEngine.AY2013.BerekeningVenBAppTypes.OpdelingNaarOorsprongItem entity, string location, ref WorksheetSetData worksheetset)
        {
            entity.IsValid = true;
            worksheetset.ClearMessages(TypeId, entity.Id);
            /*
            if (!entity.Bedrag.HasValue || entity.Bedrag.Value==0)
            {
            worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Grootboek");
            entity.IsValid = false;
            }
            */

            return entity;
        }

        public EY.com.eBook.RuleEngine.AY2013.BerekeningVenBAppTypes.FiscaleAftrekItem Validate(EY.com.eBook.RuleEngine.AY2013.BerekeningVenBAppTypes.FiscaleAftrekItem entity, string location, ref WorksheetSetData worksheetset)
        {
            entity.IsValid = true;
            worksheetset.ClearMessages(TypeId, entity.Id);
            /*
            if (!entity.Bedrag.HasValue || entity.Bedrag.Value==0)
            {
            worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Grootboek");
            entity.IsValid = false;
            }
            */

            return entity;
        }

        public EY.com.eBook.RuleEngine.AY2013.BerekeningVenBAppTypes.TariefItem Validate(EY.com.eBook.RuleEngine.AY2013.BerekeningVenBAppTypes.TariefItem entity, string location, ref WorksheetSetData worksheetset)
        {
            entity.IsValid = true;
            worksheetset.ClearMessages(TypeId, entity.Id);
            /*
            if (!entity.Bedrag.HasValue || entity.Bedrag.Value==0)
            {
            worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Grootboek");
            entity.IsValid = false;
            }
            */

            return entity;
        }

        public EY.com.eBook.RuleEngine.AY2013.BerekeningVenBAppTypes.AanslagItem Validate(EY.com.eBook.RuleEngine.AY2013.BerekeningVenBAppTypes.AanslagItem entity, string location, ref WorksheetSetData worksheetset)
        {
            entity.IsValid = true;
            worksheetset.ClearMessages(TypeId, entity.Id);
            /*
            if (!entity.Bedrag.HasValue || entity.Bedrag.Value==0)
            {
            worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Grootboek");
            entity.IsValid = false;
            }
            */

            return entity;
        }

        public EY.com.eBook.RuleEngine.AY2013.BerekeningVenBAppTypes.BelastingsBerekeningItem Validate(EY.com.eBook.RuleEngine.AY2013.BerekeningVenBAppTypes.BelastingsBerekeningItem entity, string location, ref WorksheetSetData worksheetset)
        {
            entity.IsValid = true;
            worksheetset.ClearMessages(TypeId, entity.Id);
            /*
            if (!entity.Bedrag.HasValue || entity.Bedrag.Value==0)
            {
            worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Grootboek");
            entity.IsValid = false;
            }
            */

            return entity;
        }

        public EY.com.eBook.RuleEngine.AY2013.BerekeningVenBAppTypes.BelastingsBerekeningItem2 Validate(EY.com.eBook.RuleEngine.AY2013.BerekeningVenBAppTypes.BelastingsBerekeningItem2 entity, string location, ref WorksheetSetData worksheetset)
        {
            entity.IsValid = true;
            worksheetset.ClearMessages(TypeId, entity.Id);
            /*
            if (!entity.Bedrag.HasValue || entity.Bedrag.Value==0)
            {
            worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Grootboek");
            entity.IsValid = false;
            }
            */

            return entity;
        }

        #endregion
        
        // VALIDATION
        
        // CALCULATION
        
        // BOOKINGS
        
        // IMPORT PREVIOUS


        
    }
}
  
