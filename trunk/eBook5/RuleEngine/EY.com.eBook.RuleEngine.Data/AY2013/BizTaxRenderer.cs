using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.Caching;
using EY.com.eBook.Core;
using System.Xml.Linq;
using System.Text.RegularExpressions;
using System.Xml;
using EY.com.eBook.API.Contracts.Data.Cache;
using EY.com.eBook.API.BL;
using EY.com.eBook.Core.Data;
using System.ServiceModel;
using EY.com.eBook.Core.EF;
using bzt=EY.com.eBook.BizTax;

namespace  EY.com.eBook.RuleEngine.AY2013
{
    

    public class BizTaxRenderer : bzt.BizTaxRenderer
    {

      
        #region Rendered Methods

        #region BizTaxDataContract

        

        public bzt.Contracts.BizTaxDataContract UpdateFromWorksheets(bzt.Contracts.BizTaxDataContract btdc, WorksheetSet set)
        {
            
            // clear all automated rendered items
            bzt.XbrlElementDataContract xedc = null;
            bzt.XbrlElementDataContract pxedc = null;
            string xvalue = null;
           
            string ident = btdc.EntityIdentifier.IdentifierValue;

            #region AangifteVenBApp - Reserves

            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "TaxableReservesCapitalSharePremiums"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.Reserves.AKapitaalBT.ToXbrlValue()
                , Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                        pxedc.Locked = xedc.Locked ;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-End"
              ,
                Decimals = "INF"
              ,
                Name = "TaxableReservesCapitalSharePremiums"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.Reserves.AKapitaalET.ToXbrlValue()
                , Locked =true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                        pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "TaxablePortionRevaluationSurpluses"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.Reserves.AHerwaarderingBT.ToXbrlValue()
                ,Locked=true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                        pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-End"
              ,
                Decimals = "INF"
              ,
                Name = "TaxablePortionRevaluationSurpluses"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.Reserves.AHerwaarderingET.ToXbrlValue()
                , Locked=true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                        pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "LegalReserve"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.Reserves.AWetReserveBT.ToXbrlValue()
                ,Locked=true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-End"
              ,
                Decimals = "INF"
              ,
                Name = "LegalReserve"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.Reserves.AWetReserveET.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "UnavailableReserves"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.Reserves.AOnbeschikReserveBT.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-End"
              ,
                Decimals = "INF"
              ,
                Name = "UnavailableReserves"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.Reserves.AOnbeschikReserveET.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "AvailableReserves"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.Reserves.ABeschikReserveBT.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-End"
              ,
                Decimals = "INF"
              ,
                Name = "AvailableReserves"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.Reserves.ABeschikReserveET.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "AccumulatedProfitsLosses"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.Reserves.AOvergedragenResWinstVerliesBT.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-End"
              ,
                Decimals = "INF"
              ,
                Name = "AccumulatedProfitsLosses"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.Reserves.AOvergedragenResWinstVerliesET.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "TaxableProvisions"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.Reserves.ABelastbareVoorzieningenBT.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-End"
              ,
                Decimals = "INF"
              ,
                Name = "TaxableProvisions"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.Reserves.ABelastbareVoorzieningenET.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }

            Guid OtherReserves = "OtherReserves".DeterministicGuid();
            btdc.Contexts.RemoveAll(c => c.DefId == OtherReserves);

            
            string scenid = AddContext(new List<bzt.ContextScenarioDataContract> {
                    new bzt.ContextScenarioTypeDataContract {
                        AutoRendered=true
                        , Dimension = "d-ty:DescriptionTypedDimension"
                        , Type="d-ty:DescriptionTypedID"
                        , Value = set._datacontainer.DataContract.Culture=="nl-BE" ? "Andere reserves" : "Autres r�serves"
                    }
                }
                , set._datacontainer.DataContract.StartDate
                , set._datacontainer.DataContract.EndDate
                , OtherReserves
                , "OtherReservesTitle"
                );
            
            


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = scenid
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "OtherReserves"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.Reserves.AIBalansBT.ToXbrlValue()
                , Locked =true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = scenid
              ,
                Period = "I-End"
              ,
                Decimals = "INF"
              ,
                Name = "OtherReserves"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.Reserves.AIBalansET.ToXbrlValue()
                 ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }

            }


            Guid OtherTaxableReserves = "OtherTaxableReserves".DeterministicGuid();
            btdc.Contexts.RemoveAll(c => c.DefId == OtherTaxableReserves);


            scenid = AddContext(new List<bzt.ContextScenarioDataContract> {
                    new bzt.ContextScenarioTypeDataContract {
                        AutoRendered=true
                        , Dimension = "d-ty:DescriptionTypedDimension"
                        , Type="d-ty:DescriptionTypedID"
                        , Value = set._datacontainer.DataContract.Culture=="nl-BE" ? "Andere belastbare reserves" : "Autres r�serves imposable"
                    }
                }
                , set._datacontainer.DataContract.StartDate
                , set._datacontainer.DataContract.EndDate
                , OtherTaxableReserves
                 , "OtherTaxableReservesTitle"
                );

            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = scenid
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "OtherTaxableReserves"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.Reserves.AIBReserveBT.ToXbrlValue()
                , Locked=true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
               
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = scenid
              ,
                Period = "I-End"
              ,
                Decimals = "INF"
              ,
                Name = "OtherTaxableReserves"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.Reserves.AIBReserveET.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "TaxableWriteDownsUndisclosedReserve"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.Reserves.AORBelastbareWVBT.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-End"
              ,
                Decimals = "INF"
              ,
                Name = "TaxableWriteDownsUndisclosedReserve"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.Reserves.AORBelastbareWVET.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "ExaggeratedDepreciationsUndisclosedReserve"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.Reserves.AOROverdrevenAfschrBT.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-End"
              ,
                Decimals = "INF"
              ,
                Name = "ExaggeratedDepreciationsUndisclosedReserve"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.Reserves.AOROverdrevenAfschrET.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "OtherUnderestimationsAssetsUndisclosedReserve"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.Reserves.AORAndereActBT.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-End"
              ,
                Decimals = "INF"
              ,
                Name = "OtherUnderestimationsAssetsUndisclosedReserve"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.Reserves.AORAndereActET.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "OtherOverestimationsLiabilitiesUndisclosedReserve"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.Reserves.AORAnderePassBT.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-End"
              ,
                Decimals = "INF"
              ,
                Name = "OtherOverestimationsLiabilitiesUndisclosedReserve"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.Reserves.AORAnderePassET.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "CapitalGainsShares"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.Reserves.AAMRBTRMeerwaardeAandelenBT.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "CapitalGainsSharesReversalPreviousExemptionsAdjustmentReservesPlus"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.Reserves.AAMRTerugnemingVUAandelen.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "DefinitiveExemptionTaxShelterAuthorisedAudiovisualWorksAdjustmentReservesPlus"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.Reserves.AAMRTaxShelterBT.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "ExemptionRegionalGrantsCapitalSubsidiesInterestSubsidiesAdjustmentReservesPlus"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.Reserves.AAMRGewestPremiesBT.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "FinalExemptionProfitHomologationReorganizationPlanAmicableSettlement"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.Reserves.AAMRDefVrijstellingWinst.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "OtherAdjustmentsReservesPlus"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.Reserves.AAMRAndereBT.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "AdjustmentsReservesMinus"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.Reserves.AAanpassingenMinBTReservesBT.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }

            


            #endregion


            #region AangifteVenBApp - VrijgesteldeReserve

            
            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "UnrealisedExpressedCapitalGainsExemptReserve"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VrijgesteldeReserve.BNietVerwezMeerwBT.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-End"
              ,
                Decimals = "INF"
              ,
                Name = "UnrealisedExpressedCapitalGainsExemptReserve"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VrijgesteldeReserve.BNietVerwezMeerwET.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "CapitalGainsSpecificSecuritiesExemptReserve"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VrijgesteldeReserve.BGespreidVerwezMwBT.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-End"
              ,
                Decimals = "INF"
              ,
                Name = "CapitalGainsSpecificSecuritiesExemptReserve"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VrijgesteldeReserve.BGespreidVerwezMwET.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            
            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "CapitalGainsTangibleIntangibleFixedAssetsExemptReserve"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VrijgesteldeReserve.BGespreidVerwezMwMatImmatVasteActivaBT.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-End"
              ,
                Decimals = "INF"
              ,
                Name = "CapitalGainsTangibleIntangibleFixedAssetsExemptReserve"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VrijgesteldeReserve.BGespreidVerwezMwMatImmatVasteActivaET.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }
            


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "OtherRealisedCapitalGainsExemptReserve"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VrijgesteldeReserve.BVerwezMeerwBT.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-End"
              ,
                Decimals = "INF"
              ,
                Name = "OtherRealisedCapitalGainsExemptReserve"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VrijgesteldeReserve.BVerwezMeerwET.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "CapitalGainsCorporateVehiclesExemptReserve"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VrijgesteldeReserve.BBedrijfsVoertuigenBT.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-End"
              ,
                Decimals = "INF"
              ,
                Name = "CapitalGainsCorporateVehiclesExemptReserve"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VrijgesteldeReserve.BBedrijfsVoertuigenET.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "CapitalGainsRiverVesselExemptReserve"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VrijgesteldeReserve.BBinnenschepenBT.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-End"
              ,
                Decimals = "INF"
              ,
                Name = "CapitalGainsRiverVesselExemptReserve"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VrijgesteldeReserve.BBinnenschepenET.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "CapitalGainsSeaVesselExemptReserve"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VrijgesteldeReserve.BZeeschepenBT.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-End"
              ,
                Decimals = "INF"
              ,
                Name = "CapitalGainsSeaVesselExemptReserve"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VrijgesteldeReserve.BZeeschepenET.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "ExemptInvestmentReserve"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VrijgesteldeReserve.BInvestReserveBT.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-End"
              ,
                Decimals = "INF"
              ,
                Name = "ExemptInvestmentReserve"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VrijgesteldeReserve.BInvestReserveET.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "TaxShelterAuthorisedAudiovisualWorkExemptReserve"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VrijgesteldeReserve.BTaxShelterBT.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-End"
              ,
                Decimals = "INF"
              ,
                Name = "TaxShelterAuthorisedAudiovisualWorkExemptReserve"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VrijgesteldeReserve.BTaxShelterET.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "ProfitHomologationReorganizationPlanAmicableSettlementExemptReserve"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VrijgesteldeReserve.BWinstHomologatieBT.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-End"
              ,
                Decimals = "INF"
              ,
                Name = "ProfitHomologationReorganizationPlanAmicableSettlementExemptReserve"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VrijgesteldeReserve.BWinstHomologatieET.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "OtherExemptReserves"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VrijgesteldeReserve.BAndereBT.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-End"
              ,
                Decimals = "INF"
              ,
                Name = "OtherExemptReserves"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VrijgesteldeReserve.BAndereET.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


           


            #endregion


            #region AangifteVenBApp - VerworpenUitgaven

            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "NonDeductibleTaxes"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VerworpenUitgaven.VUNietAftrekbareBelasting.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "NonDeductibleRegionalTaxesDutiesRetributions"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VerworpenUitgaven.VUGewestBelast.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "NonDeductibleFinesConfiscationsPenaltiesAllKind"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VerworpenUitgaven.VUBoetes.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "NonDeductiblePensionsCapitalEmployerContributionsEmployerPremiums"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VerworpenUitgaven.VUPensioenen.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "NonDeductibleCarExpensesLossValuesCars"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VerworpenUitgaven.VUAutokosten.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "NonDeductibleCarExpensesPartBenefitsAllKind"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VerworpenUitgaven.VUAutokostenVAA.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "NonDeductibleReceptionBusinessGiftsExpenses"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VerworpenUitgaven.VUReceptie.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "NonDeductibleRestaurantExpenses"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VerworpenUitgaven.VURestaurant.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "NonDeductibleNonSpecificProfessionalClothsExpenses"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VerworpenUitgaven.VUKledij.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "ExaggeratedInterests"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VerworpenUitgaven.VUOverdrevenIntresten.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "NonDeductibleParticularPortionInterestsLoans"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VerworpenUitgaven.VUBepaaldeLeningen.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "AbnormalBenevolentAdvantages"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VerworpenUitgaven.VUAbnormaleGoedgVoordelen.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "NonDeductibleSocialAdvantages"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VerworpenUitgaven.VUSocialeVoordelen.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "NonDeductibleAdvantagesLuncheonSportsCultureEcoVouchers"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VerworpenUitgaven.VUMaaltijdSportCultuurEco.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "Liberalities"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VerworpenUitgaven.VULiberaliteiten.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "WriteDownsLossValuesShares"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VerworpenUitgaven.VUAandelen.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "ReversalPreviousExemptions"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VerworpenUitgaven.VUTerugnemingVrijstellingen.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "EmployeeParticipation"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VerworpenUitgaven.VUWerknemerParticipatie.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "IndemnityMissingCoupon"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VerworpenUitgaven.VUCoupon.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "ExpensesTaxShelterAuthorisedAudiovisualWork"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VerworpenUitgaven.VUTaxShelter.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "RegionalPremiumCapitalSubsidiesInterestSubsidies"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VerworpenUitgaven.VUGewestPremies.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "NonDeductiblePaymentsCertainStates"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VerworpenUitgaven.VUBepaaldeStaten.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "OtherDisallowedExpenses"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VerworpenUitgaven.VUAndere.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


           

            #endregion


            #region AangifteVenBApp - UitgekeerdeDividenden

            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "OrdinaryDividends"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.UitgekeerdeDividenden.UDGewone.ToXbrlValue()
                , Locked=true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "AcquisitionOwnShares"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.UitgekeerdeDividenden.UDAandelen.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "DeceaseDepartureExclusionPartner"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.UitgekeerdeDividenden.UDOverlijden.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "DistributionCompanyAssets"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.UitgekeerdeDividenden.UDMaatschappelijk.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


           


            #endregion


            #region AangifteVenBApp - UiteenzettingWinst

            

            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "ShippingResultTonnageBased"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.UiteenzettingWinst.WIZeeschip.ToXbrlValue()
                , Locked=true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }



            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "BenevolentAbnormalFinancialAdvantagesBenefitsAllKind"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.UiteenzettingWinst.WIBABAbnormaalGoedg.ToXbrlValue()
                ,Locked=true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "ViolationInvestmentCommitmentIntangibilityConditionInvestmentReserve"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.UiteenzettingWinst.WIBABInvestVerplicht.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "CapitalSubsidiesInterestSubsidiesAgriculturalSupport"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.UiteenzettingWinst.WIBABKapitaal.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            

            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "id__TaxTreatyMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "RemainingFiscalResult"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.UiteenzettingWinst.WVVOResterendResultaatVV.ToXbrlValue()
                , Locked=true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "id__NoTaxTreatyMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "RemainingFiscalResult"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.UiteenzettingWinst.WVVOResterendResultaatNVV.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "id__BelgiumMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "RemainingFiscalResult"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.UiteenzettingWinst.WVVOResterendResultaatBelg.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "id__NoTaxTreatyMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "MiscellaneousExemptions"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.UiteenzettingWinst.WARWNietBelastbareBestandNVV.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "id__BelgiumMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "MiscellaneousExemptions"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.UiteenzettingWinst.WARWNietBelastbareBestandBelg.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "id__NoTaxTreatyMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "PEExemptIncomeMovableAssets"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.UiteenzettingWinst.WARWDefinitiefBelasteInkomstenNVV.ToXbrlValue()
                 ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "id__BelgiumMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "PEExemptIncomeMovableAssets"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.UiteenzettingWinst.WARWDefinitiefBelasteInkomstenBelg.ToXbrlValue()
                 ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "id__NoTaxTreatyMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "DeductionPatentsIncome"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.UiteenzettingWinst.WARWAftrekOctrooiNVV.ToXbrlValue()
                 ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "id__BelgiumMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "DeductionPatentsIncome"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.UiteenzettingWinst.WARWAftrekOctrooiBelg.ToXbrlValue()
                 ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "id__NoTaxTreatyMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "AllowanceCorporateEquity"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.UiteenzettingWinst.WARWAftrekRisicoNVV.ToXbrlValue()
                 ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "id__BelgiumMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "AllowanceCorporateEquity"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.UiteenzettingWinst.WARWAftrekRisicoBelg.ToXbrlValue()
                 ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "id__NoTaxTreatyMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "CompensatedTaxLosses"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.UiteenzettingWinst.WARWVorigeVerliezenNVV.ToXbrlValue()
                 ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "id__BelgiumMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "CompensatedTaxLosses"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.UiteenzettingWinst.WARWVorigeVerliezenBelg.ToXbrlValue()
                 ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "id__BelgiumMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "AllowanceInvestmentDeduction"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.UiteenzettingWinst.WARWInvesteringsAftrekBelg.ToXbrlValue()
                 ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }

            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "id__NoTaxTreatyMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "DeductionExemptionsAllowanceCorporateEquityAccumulatedUpToAndIncludingAssessmentYear2012"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.UiteenzettingWinst.WARWAftrekRisicoHistoriekNVV.ToXbrlValue()
                 ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "id__BelgiumMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "DeductionExemptionsAllowanceCorporateEquityAccumulatedUpToAndIncludingAssessmentYear2012"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.UiteenzettingWinst.WARWAftrekRisicoHistoriekBelg.ToXbrlValue()
                 ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "ShippingProfitTonnageBased"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.UiteenzettingWinst.WIBGZeescheepvaarTonnage.ToXbrlValue()
                ,Locked=true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "CapitalGainsSharesRate2500"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.UiteenzettingWinst.WIBGMeerwaardeAandelenBelast.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "BasicTaxableAmountExitTaxRate"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.UiteenzettingWinst.WIBGExitTarief.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "CapitalSubsidiesInterestSubsidiesAgriculturalSupportRate500"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.UiteenzettingWinst.WIBGKapitaalInterestLandbouw.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            #endregion


            #region AangifteVenBApp - AfzonderlijkeAanslagen

            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "UnjustifiedExpensesHiddenSurplusProfitsFinancialAdvantagesBenefitsAllKind"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.AfzonderlijkeAanslagen.AAVoordelenAlleAard.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "SeparateAssessmentTaxedReservesCreditAssociationsInstitutionsOtherAuthorisedInstitutionsRate3400"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.AfzonderlijkeAanslagen.AABelasteReserves34.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }

            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "SeparateAssessmentTaxedReservesCreditAssociationsInstitutionsOtherAuthorisedInstitutionsRate2800"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.AfzonderlijkeAanslagen.AABelasteReserves28.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "SeparateAssessmentDividendsPaidCreditCorporationTradeEquipmentHousingCorporation"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.AfzonderlijkeAanslagen.AAUitgekeerdeDividenden.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "SpecialAssessmentPre1990FullPartialDistributionCompanyAssetsRate3300"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.AfzonderlijkeAanslagen.AABAMaatschVermogen33.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "SpecialAssessmentPre1990FullPartialDistributionCompanyAssetsRate1650"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.AfzonderlijkeAanslagen.AABAMaatschVermogen165.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "SpecialAssessmentPre1990BenefitsAllKindCompanyLiquidation"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.AfzonderlijkeAanslagen.AABAVoordelenAlleAard.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            #endregion


            #region AangifteVenBApp - Diamant

            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "AdditionalDutiesDiamondTraders"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.Diamant.DIAanvullingErkend.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "RetributionTaxCreditResearchDevelopment"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.Diamant.DIOnderzoekOntwikkeling.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }

            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "SeparateAssessmentAdditionalIndividualPensionProvisionsOnEndTaxPeriodClosingDateBefore2012-01-01"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.Diamant.DIVoorzieningenVoor2012.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }

            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = ""
              ,
                Name = "SeparateAssessmentAdditionalIndividualPensionProvisionsSpreadThreeAssessmentYears2013Until2015"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.Diamant.DIVoorzieningenSpreiding.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            #endregion


            #region AangifteVenBApp - NietBelastbareBestanddelen

            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "ExemptGifts"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.NietBelastbareBestanddelen.NTEBECharity.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "ExemptionAdditionalPersonnelMiscellaneousExemptions"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.NietBelastbareBestanddelen.NTEBEPersonnel.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "ExemptionAdditionalPersonnelSMEs"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.NietBelastbareBestanddelen.NTEBEPersonnelSME.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "ExemptionTrainingPeriodBonus"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.NietBelastbareBestanddelen.NTEBEInternalshipPremium.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "OtherMiscellaneousExemptions"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.NietBelastbareBestanddelen.NTEBEOthers.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            #endregion


            #region AangifteVenBApp - DBI

            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "id__BelgianBranchMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "NetBelgianIncomeSharesPEExemptIncomeMovableAssets"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.DBI.DBIAbeNettoBEI.ToXbrlValue()
                , Locked=true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "id__ForeignBranchMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "NetBelgianIncomeSharesPEExemptIncomeMovableAssets"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.DBI.DBIAbeNettoBUI.ToXbrlValue()
                ,Locked= true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "id__BelgianBranchMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "WithholdingTaxBelgianIncomeSharesPEExemptIncomeMovableAssets"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.DBI.DBIAbeRoerendBEI.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "id__ForeignBranchMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "WithholdingTaxBelgianIncomeSharesPEExemptIncomeMovableAssets"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.DBI.DBIAbeRoerendBUI.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "id__BelgianBranchMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "NetForeignIncomeSharesPEExemptIncomeMovableAssets"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.DBI.DBIAbuNettoBEI.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "id__ForeignBranchMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "NetForeignIncomeSharesPEExemptIncomeMovableAssets"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.DBI.DBIAbuNettoBUI.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "id__BelgianBranchMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "WithholdingTaxForeignIncomeSharesPEExemptIncomeMovableAssets"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.DBI.DBIAbuRoerendBEI.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "id__ForeignBranchMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "WithholdingTaxForeignIncomeSharesPEExemptIncomeMovableAssets"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.DBI.DBIAbuRoerendBUI.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "id__BelgianBranchMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "NetOtherBelgianIncomeSharesPEExemptIncomeMovableAssets"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.DBI.DBIAAbeNettoBEI.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "id__ForeignBranchMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "NetOtherBelgianIncomeSharesPEExemptIncomeMovableAssets"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.DBI.DBIAAbeNettoBUI.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "id__BelgianBranchMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "WithholdingTaxOtherBelgianIncomeSharesPEExemptIncomeMovableAssets"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.DBI.DBIAAbeRoerendBEI.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "id__ForeignBranchMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "WithholdingTaxOtherBelgianIncomeSharesPEExemptIncomeMovableAssets"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.DBI.DBIAAbeRoerendBUI.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "id__BelgianBranchMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "NetOtherForeignIncomeSharesPEExemptIncomeMovableAssets"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.DBI.DBIAAbuNettoBEI.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "id__ForeignBranchMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "NetOtherForeignIncomeSharesPEExemptIncomeMovableAssets"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.DBI.DBIAAbuNettoBUI.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "id__BelgianBranchMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "WithholdingTaxOtherForeignIncomeSharesPEExemptIncomeMovableAssets"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.DBI.DBIAAbuRoerendBEI.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "id__ForeignBranchMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "WithholdingTaxOtherForeignIncomeSharesPEExemptIncomeMovableAssets"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.DBI.DBIAAbuRoerendBUI.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "id__BelgianBranchMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "OtherExemptIncomeMovableAssets"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.DBI.DBIVrijgestelRoerendBEI.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "id__ForeignBranchMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "OtherExemptIncomeMovableAssets"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.DBI.DBIVrijgestelRoerendBUI.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            

            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "id__BelgianBranchMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "IncomeMergerAcquisitionRetainedSharesSimilarRegulationOtherEUMemberState"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.DBI.DBILidEuBEI.ToXbrlValue()
                , Locked=true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "id__ForeignBranchMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "IncomeMergerAcquisitionRetainedSharesSimilarRegulationOtherEUMemberState"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.DBI.DBILidEuBUI.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "id__BelgianBranchMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "ExemptIncomeMovableAssetsRefinancingLoans"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.DBI.DBIHerfinancBEI.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = "id__ForeignBranchMember"
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "ExemptIncomeMovableAssetsRefinancingLoans"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.DBI.DBIHerfinancBUI.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


           


            #endregion


            #region AangifteVenBApp - OverdrachtDBIAftrek

            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "AccumulatedPEExemptIncomeMovableAssets"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.OverdrachtDBIAftrek.ODASaldoOvergedragen.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "CarryOverNextTaxPeriodInsufficientProfitsPEExemptIncomeMovableAssetsCurrentTaxPeriod"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.OverdrachtDBIAftrek.ODABedragHuidigNaarVolgend.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "DeductionPEExemptIncomeMovableAssetsPreviousTaxPeriod"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.OverdrachtDBIAftrek.ODABedragSub1Werkelijk.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


           

            #endregion
   #region AangifteVenBApp - CompenseerbareVerliezen

            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "CompensableTaxLosses"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.CompenseerbareVerliezen.CVSaldoVorigeVerliezen.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "CompensatedTaxLosses"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.CompenseerbareVerliezen.CVGecompenseerdeVerliezen.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "LossCurrentTaxPeriod"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.CompenseerbareVerliezen.CVVerliesBelastbaarTijdperk.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


           


            #endregion

            #region AangifteVenBApp - OverdrachtNID

            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "ExemptionsAllowanceCorporateEquityAccumulatedUpToAndIncludingAssessmentYear2012"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.OverdrachtNID.ONIDSaldoOvergedragen.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "CarryOverExemptionsAllowanceCorporateEquityAccumulatedUpToAndIncludingAssessmentYear2012"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.OverdrachtNID.ONIDOverdraagbaarVolgend.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            #endregion


         


            #region AangifteVenBApp - TariefBelasting

            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = ""
              ,
                Name = "ExclusionReducedRate"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = ""
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.TariefBelasting.TARUitgeslotenVerminderd.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = ""
              ,
                Name = "CreditCorporationTradeEquipmentHousingCorporationTaxRate"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = ""
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.TariefBelasting.TAROnderworpen.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            #endregion


            #region AangifteVenBApp - VoorafBetalingen

            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = ""
              ,
                Name = "FirstThreeAccountingYearsSmallCompanyCorporationCode"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = ""
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VoorafBetalingen.VBJongVennootschap.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "Prepayments"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VoorafBetalingen.VBTotaal.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "PrepaymentFirstQuarter"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VoorafBetalingen.VBEersteKwartaal.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "PrepaymentSecondQuarter"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VoorafBetalingen.VBTweedeKwartaal.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "PrepaymentThirdQuarter"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VoorafBetalingen.VBDerdeKwartaal.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "PrepaymentFourthQuarter"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VoorafBetalingen.VBVierdeKwartaal.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = ""
              ,
                Name = "PrepaymentReferenceNumberNotEntityIdentifierFirstOccurrence"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = ""
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VoorafBetalingen.VBEersteKwartaalReferentie.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = ""
              ,
                Name = "PrepaymentReferenceNumberNotEntityIdentifierSecondOccurrence"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = ""
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VoorafBetalingen.VBTweedeKwartaalReferentie.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = ""
              ,
                Name = "PrepaymentReferenceNumberNotEntityIdentifierThirdOccurrence"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = ""
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VoorafBetalingen.VBDerdeKwartaalReferentie.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = ""
              ,
                Name = "PrepaymentReferenceNumberNotEntityIdentifierFourthOccurrence"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = ""
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VoorafBetalingen.VBVierdeKwartaalReferentie.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            #endregion


            #region AangifteVenBApp - VerrekenbareVoorheffing

            


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "NonRepayableFictiousWitholdingTax"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VerrekenbareVoorheffing.VVNTFictieveRoerendeVoorheffing.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "NonRepayableLumpSumForeignTaxes"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VerrekenbareVoorheffing.VVNTForfaitairBuitenlands.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "TaxCreditResearchDevelopment"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VerrekenbareVoorheffing.VVNTBelastingKredietOnderzoekOntw.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            

            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "RepayableRealFictitiousWithholdingTaxOtherSharesPEExemptIncomeMovableAssetsBelgium"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VerrekenbareVoorheffing.VVTBelgisch.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "RepayableWithholdingTaxPEExemptionLiquidationBonusesBonusesAcquisitionOwnSharesBelgiumForeign"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VerrekenbareVoorheffing.VVTEigenBuitenlands.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "RepayableWithholdingTaxOtherPEForeign"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VerrekenbareVoorheffing.VVTAndereBuitenlands.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "RepayableWithholdingTaxOtherLiquidationBonusesBonusesAcquisitionOwnShares"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VerrekenbareVoorheffing.VVTAndereEigen.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "RepayableWithholdingTaxOtherDividends"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VerrekenbareVoorheffing.VVTAndereRoerendeVoorheffing.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "OtherRepayableWithholdingTaxes"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VerrekenbareVoorheffing.VVTAndere.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "TaxCreditResearchDevelopmentClearablePreviousTaxPeriodsRepayableCurrentAssessmentYear"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.VerrekenbareVoorheffing.VBelastingkredietHuiidTijdperk.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            #endregion


            #region AangifteVenBApp - TaxShelter

            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = ""
              ,
                Name = "BelgianCorporationAudiovisualWorksTaxShelterAgreement"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = ""
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.TaxShelter.TASTaxShelter.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            #endregion


            #region AangifteVenBApp - GrootteVennootschap

            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = ""
              ,
                Name = "AssociatedCompanyCorporationCodeCurrentTaxPeriod"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = ""
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.GrootteVennootschap.GVBTVerbonden.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "AnnualWorkForceAverageCorporationCodeCurrentTaxPeriod"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.GrootteVennootschap.GVBTJaargemiddelde.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "AnnualTurnoverExcludingVATCorporationCodeCurrentTaxPeriod"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.GrootteVennootschap.GVBTJaarOmzet.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "BalanceSheetTotalCorporationCodeCurrentTaxPeriod"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.AangifteVenBApp.Data.GrootteVennootschap.GVBTBalansTotaal.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            #endregion


            #region DubieuzeDebiteurenApp - List2043


            // CLEANUP EXISTING
            List<string> crefs = btdc.Contexts.Where(c => c.PresContextId == "WriteDownsDebtClaimsSection").Select(c => c.Id).ToList();
            btdc.Elements.RemoveAll(e => crefs.Contains(e.ContextRef));

            btdc.Contexts.RemoveAll(c => c.PresContextId == "WriteDownsDebtClaimsSection");

            foreach (DubieuzeDebiteurenAppTypes.DubieuzeDebiteur2043 entity in set._datacontainer.DubieuzeDebiteurenApp.Data.List2043)
            {

                UpdateGridDubieuzeDebiteurenAppList2043(ref btdc, set, entity);
            }



            #endregion


            #region VoorzieningenApp - List2043
            crefs = btdc.Contexts.Where(c => c.PresContextId == "ProvisionsRisksExpensesSection").Select(c => c.Id).ToList();
            btdc.Elements.RemoveAll(e => crefs.Contains(e.ContextRef));

            btdc.Contexts.RemoveAll(c => c.PresContextId == "ProvisionsRisksExpensesSection");

            foreach (VoorzieningenAppTypes.Voorzieningen2043 entity in set._datacontainer.VoorzieningenApp.Data.List2043)
            {

                UpdateGridVoorzieningenAppList2043(ref btdc, set, entity);
            }



            #endregion


            #region RisicoKapitaalApp - Fiche

            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "Equity"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.RisicoKapitaalApp.Data.Fiche.EigenVermogen.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


           

            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "OwnSharesFiscalValue"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.RisicoKapitaalApp.Data.Fiche.EigenAandelen.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "FinancialFixedAssetsParticipationsOtherShares"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.RisicoKapitaalApp.Data.Fiche.FinancVasteActiva.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "SharesInvestmentCorporations"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.RisicoKapitaalApp.Data.Fiche.AandelenBelegging.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "BranchesCountryTaxTreaty"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.RisicoKapitaalApp.Data.Fiche.InrichtingVerdrag.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "ImmovablePropertyCountryTaxTreaty"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.RisicoKapitaalApp.Data.Fiche.OnroerendVerdrag.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "TangibleFixedAssetsUnreasonableRelatedCosts"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.RisicoKapitaalApp.Data.Fiche.MvaOnredelijk.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "InvestmentsNoPeriodicalIncome"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.RisicoKapitaalApp.Data.Fiche.BestanddelenBelegging.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "ImmovablePropertyUseManager"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.RisicoKapitaalApp.Data.Fiche.OnroerendeBedrijfsleiders.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "UnrealisedExpressedCapitalGains"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.RisicoKapitaalApp.Data.Fiche.UitgedrukteMeerwaarden.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "TaxCreditResearchDevelopmentDeductionEquityAllowanceCorporateEquity"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.RisicoKapitaalApp.Data.Fiche.Belastingkrediet.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "InvestmentGrants"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.RisicoKapitaalApp.Data.Fiche.KapitaalSubsidies.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "ActualisationStockRecognisedDiamondTraders"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.RisicoKapitaalApp.Data.Fiche.Voorraadactualisering.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "BorrowedFundsMainEstablishmentInterestChargedTaxableResultBelgianBranch"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.RisicoKapitaalApp.Data.Fiche.BelgischeInrichting.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


           

            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "AllowanceCorporateEquityCurrentTaxPeriod"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.RisicoKapitaalApp.Data.Fiche.RisicoKapitaalBelastbaar.ToXbrlValue()
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "DeductibleAllowanceCorporateEquityCurrentAssessmentYear"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.RisicoKapitaalApp.Data.Fiche.Aftrekbaar.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }

            
            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "AllowanceCorporateEquity"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.RisicoKapitaalApp.Data.Fiche.WerkelijkAfgetrokken.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "ExemptionsAllowanceCorporateEquityAccumulatedUpToAndIncludingAssessmentYear2012"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.RisicoKapitaalHistoriekApp.Data.Fiche.GevormdeVrijstellingen.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "DeductionExemptionsAllowanceCorporateEquityAccumulatedUpToAndIncludingAssessmentYear2012"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.RisicoKapitaalHistoriekApp.Data.Fiche.WerkelijkAfgetrokkenVrijstellingen.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "CarryOverExemptionsAllowanceCorporateEquityAccumulatedUpToAndIncludingAssessmentYear2012"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = set._datacontainer.RisicoKapitaalHistoriekApp.Data.Fiche.SaldoOvergedragenVrijstellingen.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (btdc.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = btdc.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc!=null)
                    {
                        pxedc.Value = xedc.Value;  pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    btdc.Elements.Add(pxedc);
                }
            }

            #endregion


            #region RisicoKapitaalApp - Wijzigingen
            int i = 1;

            crefs = btdc.Contexts.Where(c => c.PresContextId == "ApplicableDurationMovementEquityAllowanceCorporateEquity").Select(c => c.Id).ToList();
            btdc.Elements.RemoveAll(e => crefs.Contains(e.ContextRef));

            btdc.Contexts.RemoveAll(c => c.PresContextId == "ApplicableDurationMovementEquityAllowanceCorporateEquity");


            foreach (RisicoKapitaalAppTypes.WijzigingItem entity in set._datacontainer.RisicoKapitaalApp.Data.Wijzigingen)
            {

                UpdateGridRisicoKapitaalAppWijzigingen(ref btdc, set, entity, i);
                i++;
            }



            #endregion


            return btdc;

        }
        #endregion

        #region Worksheet specific BizTaxDataContract

        private void UpdateGridDubieuzeDebiteurenAppList2043(ref bzt.Contracts.BizTaxDataContract btdc, WorksheetSet set, DubieuzeDebiteurenAppTypes.DubieuzeDebiteur2043 entity)
        {

            bzt.XbrlElementDataContract xedc = null;
            bzt.XbrlElementDataContract pxedc = null;
            string xvalue = null;
            List<string> crefs = btdc.Contexts.Where(c=>c.DefId==entity.Id).Select(c=>c.Id).ToList();

            string ctxid = AddContext(new List<bzt.ContextScenarioDataContract> {
                new bzt.ContextScenarioExplicitDataContract {
                        AutoRendered=true
                        , Dimension = "d-ec:ExemptionCategoryDimension"
                        , Value = entity.ExplicitMember.ToXbrlValue()
                    }    
                ,new bzt.ContextScenarioTypeDataContract {
                        AutoRendered=true
                        , Dimension = "d-ty:WriteDownDebtClaimTypedDimension"
                        , Type="d-ty:WritedownDebtClaimTypedID"
                        , Value =  entity.TypedMember
                    }
                }
                , set._datacontainer.DataContract.StartDate
                , set._datacontainer.DataContract.EndDate
                , entity.Id
                , "WriteDownsDebtClaimsSection"
                );


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ctxid
              ,
                Period = "D"
              ,
                Decimals = ""
              ,
                Name = "IdentityTradeDebtor"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = ""
              ,
                Value = entity.Schuldenaar.ToXbrlValue()
                , Locked=true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                btdc.Elements.Add(xedc);
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ctxid
              ,
                Period = "D"
              ,
                Decimals = ""
              ,
                Name = "AddressTradeDebtor"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = ""
              ,
                Value = entity.SchuldenaarAdres.ToXbrlValue()
                ,Locked=true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                btdc.Elements.Add(xedc);
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ctxid
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "DebtClaim"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = entity.VorderingStart.ToXbrlValue()
                ,Locked=true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                btdc.Elements.Add(xedc);
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ctxid
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "ExemptWriteDownDebtClaim"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = entity.WaardeverminderingStart.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                btdc.Elements.Add(xedc);
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ctxid
              ,
                Period = "I-End"
              ,
                Decimals = "INF"
              ,
                Name = "DebtClaim"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = entity.VorderingEinde.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                btdc.Elements.Add(xedc);
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ctxid
              ,
                Period = "I-End"
              ,
                Decimals = "INF"
              ,
                Name = "ExemptWriteDownDebtClaim"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = entity.WaardeverminderingEinde.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                btdc.Elements.Add(xedc);
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ctxid
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "DecreaseExemptWriteDownDebtClaimFinalWriteDownCurrentTaxPeriod"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = entity.VerminderingWVVerlies.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                btdc.Elements.Add(xedc);
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context =ctxid
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "DecreaseExemptWriteDownDebtClaimFullPartialCollectionCurrentTaxPeriod"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = entity.VerminderingWVInning.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                btdc.Elements.Add(xedc);
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ctxid
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "DecreaseExemptWriteDownDebtClaimNewEstimationProbableLoss"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = entity.VerminderingWVHerschat.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                btdc.Elements.Add(xedc);
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ctxid
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "IncreaseExemptWriteDownDebtClaim"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = entity.VermeerderingWV.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                btdc.Elements.Add(xedc);
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ctxid
              ,
                Period = "D"
              ,
                Decimals = ""
              ,
                Name = "JustificationExemptWriteDown"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = ""
              ,
                Value = entity.Commentaar.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                btdc.Elements.Add(xedc);
            }

        }

        private void UpdateGridVoorzieningenAppList2043(ref bzt.Contracts.BizTaxDataContract btdc, WorksheetSet set, VoorzieningenAppTypes.Voorzieningen2043 entity)
        {

            bzt.XbrlElementDataContract xedc = null;
            bzt.XbrlElementDataContract pxedc = null;
            string xvalue = null;
            List<string> crefs = btdc.Contexts.Where(c => c.DefId == entity.Id).Select(c => c.Id).ToList();

            string ctxid = AddContext(new List<bzt.ContextScenarioDataContract> {
                new bzt.ContextScenarioExplicitDataContract {
                        AutoRendered=true
                        , Dimension = "d-ec:ExemptionCategoryDimension"
                        , Value = entity.ExplicitMemberCategory.ToXbrlValue()
                    }  
                , new bzt.ContextScenarioExplicitDataContract {
                        AutoRendered=true
                        , Dimension = "d-expt:ExpenseTypeDimension"
                        , Value = entity.ExplicitMemberKind.ToXbrlValue()
                    }  
                ,new bzt.ContextScenarioTypeDataContract {
                        AutoRendered=true
                        , Dimension = "d-ty:ProvisionRiskExpenseTypedDimension"
                        , Type="d-ty:ProvisionRiskExpenseTypedID"
                        , Value =  entity.TypedMember.Trim()
                    }
                }
                , set._datacontainer.DataContract.StartDate
                , set._datacontainer.DataContract.EndDate
                , entity.Id
                , "ProvisionsRisksExpensesSection"
                );


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ctxid
              ,
                Period = "I-End"
              ,
                Decimals = "INF"
              ,
                Name = "ProbableCost"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = entity.WaarschijnlijkeKosten.ToXbrlValue()
                ,Locked=true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                btdc.Elements.Add(xedc);
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ctxid
              ,
                Period = "I-Start"
              ,
                Decimals = "INF"
              ,
                Name = "ExemptProvisionRisksExpenses"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = entity.VoorzieningBeginBj.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                btdc.Elements.Add(xedc);
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ctxid
              ,
                Period = "I-End"
              ,
                Decimals = "INF"
              ,
                Name = "ExemptProvisionRisksExpenses"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = entity.VoorzieningEindeBJ.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                btdc.Elements.Add(xedc);
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ctxid
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "DecreaseExemptProvisionRisksExpensesBorneExpensesCurrentTaxPeriod"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = entity.VerminderingTijdensBJAanwending.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                btdc.Elements.Add(xedc);
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ctxid
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "DecreaseExemptProvisionRisksExpensesNewEstimationProbableExpenses"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = entity.VerminderingTijdensBJNieuweInschatting.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                btdc.Elements.Add(xedc);
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ctxid
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "IncreaseExemptProvisionRisksExpenses"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = entity.VerhogingTijdensBJ.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                btdc.Elements.Add(xedc);
            }


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ctxid
              ,
                Period = "D"
              ,
                Decimals = ""
              ,
                Name = "JustificationExemptProvision"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = ""
              ,
                Value = entity.Commentaar.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                btdc.Elements.Add(xedc);
            }

        }

        private void UpdateGridRisicoKapitaalAppWijzigingen(ref bzt.Contracts.BizTaxDataContract btdc, WorksheetSet set, RisicoKapitaalAppTypes.WijzigingItem entity,int nr)
        {

            bzt.XbrlElementDataContract xedc = null;
            bzt.XbrlElementDataContract pxedc = null;
            string xvalue = null;
            string description = string.Format("{0}-{1}", nr, entity.Notitie);

            List<string> crefs = btdc.Contexts.Where(c => c.DefId == entity.Id).Select(c => c.Id).ToList();

            string ctxid = AddContext(new List<bzt.ContextScenarioDataContract> {
                new bzt.ContextScenarioTypeDataContract {
                        AutoRendered=true
                        , Dimension = "d-ty:DescriptionTypedDimension"
                        , Type="d-ty:DescriptionTypedID"
                        , Value = description.ToXbrlValue()
                    }
                ,new bzt.ContextScenarioTypeDataContract {
                        AutoRendered=true
                        , Dimension = "d-ty:DateTypedDimension"
                        , Type="d-ty:DateTypedID"
                        , Value =  entity.DatumTransactie.ToXbrlValue()
                    }
                }
                , set._datacontainer.DataContract.StartDate
                , set._datacontainer.DataContract.EndDate
                , entity.Id
                , "ApplicableDurationMovementEquityAllowanceCorporateEquity"
                );


            xedc = new bzt.XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ctxid
              ,
                Period = "D"
              ,
                Decimals = "INF"
              ,
                Name = "MovementEquityAfterDeductionsAllowanceCorporateEquity"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = "EUR"
              ,
                Value = entity.XbrlValue.ToXbrlValue()
                , Locked=true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                btdc.Elements.Add(xedc);
            }

        }

        #endregion

      

        #endregion
    

    }
}
