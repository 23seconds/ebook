using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using EY.com.eBook.RuleEngine.Data;
using EY.com.eBook.Core.Data;
using EY.com.eBook.API.Contracts.Data;

namespace EY.com.eBook.RuleEngine
{
    
    [ServiceContract]
    public interface 
    IRuleEngine2013
    {
        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        void UpdateBizTax(CriteriaFileTypeDataContract cfdc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        decimal? GetTaxCalculation(CriteriaFileDataContract cfdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        void ReCalculate(CriteriaFileDataContract cfdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        void ForceReCalculate(CriteriaFileDataContract cfdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        void ImportPrevious(CriteriaFileDataContract cfdc);

        #region Interface



        #region FacturatieApp
        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.FacturatieAppTypes.Facturatie FacturatieAppGetData(CriteriaFileDataContract cfdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.FacturatieAppTypes.Facturatie FacturatieAppCalculate(CriteriaFileDataContract cfdc);

        #region Entity validation

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> FacturatieAppFacturatieKlantValidate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaFacturatieAppFacturatieKlantDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> FacturatieAppFacturatieLeverancierCNValidate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaFacturatieAppFacturatieLeverancierCNDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> FacturatieAppFacturatieLeverancierValidate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaFacturatieAppFacturatieLeverancierDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> FacturatieAppFacturatieKlantCNValidate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaFacturatieAppFacturatieKlantCNDataContract cedc);



        #endregion



        #region Add/Remove


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.FacturatieAppTypes.Facturatie FacturatieAppOpTeMakenFacturenAdd(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaFacturatieAppFacturatieKlantDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.FacturatieAppTypes.Facturatie FacturatieAppOpTeMakenFacturenRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.FacturatieAppTypes.Facturatie FacturatieAppOpTeMakenFacturenUpdate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaFacturatieAppFacturatieKlantDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.FacturatieAppTypes.Facturatie FacturatieAppTeOntvangenCNAdd(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaFacturatieAppFacturatieLeverancierCNDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.FacturatieAppTypes.Facturatie FacturatieAppTeOntvangenCNRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.FacturatieAppTypes.Facturatie FacturatieAppTeOntvangenCNUpdate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaFacturatieAppFacturatieLeverancierCNDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.FacturatieAppTypes.Facturatie FacturatieAppTeOntvangenFacturenAdd(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaFacturatieAppFacturatieLeverancierDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.FacturatieAppTypes.Facturatie FacturatieAppTeOntvangenFacturenRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.FacturatieAppTypes.Facturatie FacturatieAppTeOntvangenFacturenUpdate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaFacturatieAppFacturatieLeverancierDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.FacturatieAppTypes.Facturatie FacturatieAppOpTeMakenCNAdd(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaFacturatieAppFacturatieKlantCNDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.FacturatieAppTypes.Facturatie FacturatieAppOpTeMakenCNRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.FacturatieAppTypes.Facturatie FacturatieAppOpTeMakenCNUpdate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaFacturatieAppFacturatieKlantCNDataContract cedc);

        #endregion

        #endregion



        #region OverlopendeRekeningenApp
        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.OverlopendeRekeningenAppTypes.OverlopendeRekeningen OverlopendeRekeningenAppGetData(CriteriaFileDataContract cfdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.OverlopendeRekeningenAppTypes.OverlopendeRekeningen OverlopendeRekeningenAppCalculate(CriteriaFileDataContract cfdc);

        #region Entity validation

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> OverlopendeRekeningenAppOverTeDragenKostValidate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaOverlopendeRekeningenAppOverTeDragenKostDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> OverlopendeRekeningenAppVerkregenOpbrengstValidate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaOverlopendeRekeningenAppVerkregenOpbrengstDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> OverlopendeRekeningenAppToeTeRekenenKostValidate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaOverlopendeRekeningenAppToeTeRekenenKostDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> OverlopendeRekeningenAppOverTeDragenOpbrengstValidate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaOverlopendeRekeningenAppOverTeDragenOpbrengstDataContract cedc);



        #endregion



        #region Add/Remove


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.OverlopendeRekeningenAppTypes.OverlopendeRekeningen OverlopendeRekeningenAppOverTeDragenKostenAdd(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaOverlopendeRekeningenAppOverTeDragenKostDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.OverlopendeRekeningenAppTypes.OverlopendeRekeningen OverlopendeRekeningenAppOverTeDragenKostenRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.OverlopendeRekeningenAppTypes.OverlopendeRekeningen OverlopendeRekeningenAppOverTeDragenKostenUpdate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaOverlopendeRekeningenAppOverTeDragenKostDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.OverlopendeRekeningenAppTypes.OverlopendeRekeningen OverlopendeRekeningenAppVerkregenOpbrenstenAdd(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaOverlopendeRekeningenAppVerkregenOpbrengstDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.OverlopendeRekeningenAppTypes.OverlopendeRekeningen OverlopendeRekeningenAppVerkregenOpbrenstenRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.OverlopendeRekeningenAppTypes.OverlopendeRekeningen OverlopendeRekeningenAppVerkregenOpbrenstenUpdate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaOverlopendeRekeningenAppVerkregenOpbrengstDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.OverlopendeRekeningenAppTypes.OverlopendeRekeningen OverlopendeRekeningenAppToeTeRekenenKostenAdd(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaOverlopendeRekeningenAppToeTeRekenenKostDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.OverlopendeRekeningenAppTypes.OverlopendeRekeningen OverlopendeRekeningenAppToeTeRekenenKostenRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.OverlopendeRekeningenAppTypes.OverlopendeRekeningen OverlopendeRekeningenAppToeTeRekenenKostenUpdate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaOverlopendeRekeningenAppToeTeRekenenKostDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.OverlopendeRekeningenAppTypes.OverlopendeRekeningen OverlopendeRekeningenAppOverTeDragenOpbrengstenAdd(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaOverlopendeRekeningenAppOverTeDragenOpbrengstDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.OverlopendeRekeningenAppTypes.OverlopendeRekeningen OverlopendeRekeningenAppOverTeDragenOpbrengstenRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.OverlopendeRekeningenAppTypes.OverlopendeRekeningen OverlopendeRekeningenAppOverTeDragenOpbrengstenUpdate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaOverlopendeRekeningenAppOverTeDragenOpbrengstDataContract cedc);

        #endregion

        #endregion



        #region VergelijkendeCijfersApp
        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.VergelijkendeCijfersAppTypes.WPVergelijkendeCijfers VergelijkendeCijfersAppGetData(CriteriaFileDataContract cfdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.VergelijkendeCijfersAppTypes.WPVergelijkendeCijfers VergelijkendeCijfersAppCalculate(CriteriaFileDataContract cfdc);

        #region Entity validation

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> VergelijkendeCijfersAppCijferBlokValidate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaVergelijkendeCijfersAppCijferBlokDataContract cedc);



        #endregion



        #region Add/Remove



        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.VergelijkendeCijfersAppTypes.WPVergelijkendeCijfers VergelijkendeCijfersAppKostenUpdate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaVergelijkendeCijfersAppCijferBlokDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.VergelijkendeCijfersAppTypes.WPVergelijkendeCijfers VergelijkendeCijfersAppOpbrengstenUpdate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaVergelijkendeCijfersAppCijferBlokDataContract cedc);

        #endregion

        #endregion



        #region VoorschottenApp
        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.VoorschottenAppTypes.Voorschotten VoorschottenAppGetData(CriteriaFileDataContract cfdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.VoorschottenAppTypes.Voorschotten VoorschottenAppCalculate(CriteriaFileDataContract cfdc);

        #region Entity validation

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> VoorschottenAppGegevenValidate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaVoorschottenAppGegevenDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> VoorschottenAppIntrestVoetValidate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaVoorschottenAppIntrestVoetDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> VoorschottenAppRentegevendVoorschotValidate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaVoorschottenAppRentegevendVoorschotDataContract cedc);



        #endregion



        #region Add/Remove


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.VoorschottenAppTypes.Voorschotten VoorschottenAppGegevensAdd(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaVoorschottenAppGegevenDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.VoorschottenAppTypes.Voorschotten VoorschottenAppGegevensRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.VoorschottenAppTypes.Voorschotten VoorschottenAppGegevensUpdate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaVoorschottenAppGegevenDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.VoorschottenAppTypes.Voorschotten VoorschottenAppInterestVoetenAdd(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaVoorschottenAppIntrestVoetDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.VoorschottenAppTypes.Voorschotten VoorschottenAppInterestVoetenRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.VoorschottenAppTypes.Voorschotten VoorschottenAppInterestVoetenUpdate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaVoorschottenAppIntrestVoetDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.VoorschottenAppTypes.Voorschotten VoorschottenAppRentegevendeVoorschottenAdd(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaVoorschottenAppRentegevendVoorschotDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.VoorschottenAppTypes.Voorschotten VoorschottenAppRentegevendeVoorschottenRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.VoorschottenAppTypes.Voorschotten VoorschottenAppRentegevendeVoorschottenUpdate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaVoorschottenAppRentegevendVoorschotDataContract cedc);

        #endregion

        #endregion



        #region ErelonenHuurApp
        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.ErelonenHuurAppTypes.ErelonenHuur ErelonenHuurAppGetData(CriteriaFileDataContract cfdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.ErelonenHuurAppTypes.ErelonenHuur ErelonenHuurAppCalculate(CriteriaFileDataContract cfdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.ErelonenHuurAppTypes.ErelonenHuur ErelonenHuurAppProAccImportHistory(CriteriaFileDataContract cfdc);

        #region Entity validation

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> ErelonenHuurAppImportItemValidate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaErelonenHuurAppImportItemDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> ErelonenHuurAppImportedHistoryValidate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaErelonenHuurAppImportedHistoryDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> ErelonenHuurAppVerdelingHistoriekValidate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaErelonenHuurAppVerdelingHistoriekDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> ErelonenHuurAppFiche281_50Validate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaErelonenHuurAppFiche281_50DataContract cedc);



        #endregion



        #region Add/Remove


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.ErelonenHuurAppTypes.ErelonenHuur ErelonenHuurAppHistoriekAdd(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaErelonenHuurAppVerdelingHistoriekDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.ErelonenHuurAppTypes.ErelonenHuur ErelonenHuurAppHistoriekRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.ErelonenHuurAppTypes.ErelonenHuur ErelonenHuurAppHistoriekUpdate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaErelonenHuurAppVerdelingHistoriekDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.ErelonenHuurAppTypes.ErelonenHuur ErelonenHuurAppFiches281_50Update(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaErelonenHuurAppFiche281_50DataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.ErelonenHuurAppTypes.ErelonenHuur ErelonenHuurAppFiche281_50ColAdd(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaErelonenHuurAppFiche281_50DataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.ErelonenHuurAppTypes.ErelonenHuur ErelonenHuurAppFiche281_50ColRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.ErelonenHuurAppTypes.ErelonenHuur ErelonenHuurAppFiche281_50ColUpdate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaErelonenHuurAppFiche281_50DataContract cedc);

        #endregion

        #endregion



        #region VoordelenApp
        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.VoordelenAppTypes.Voordelen VoordelenAppGetData(CriteriaFileDataContract cfdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.VoordelenAppTypes.Voordelen VoordelenAppCalculate(CriteriaFileDataContract cfdc);

        #region Entity validation

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> VoordelenAppVAA_AutoCO2Validate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaVoordelenAppVAA_AutoCO2DataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> VoordelenAppVAA_AutoRupoValidate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaVoordelenAppVAA_AutoRupoDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> VoordelenAppVAA_NutsValidate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaVoordelenAppVAA_NutsDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> VoordelenAppVAA_HUURValidate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaVoordelenAppVAA_HUURDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> VoordelenAppVAA_HUURHERKValidate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaVoordelenAppVAA_HUURHERKDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> VoordelenAppVAA_PCValidate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaVoordelenAppVAA_PCDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> VoordelenAppVAA_TelefonieValidate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaVoordelenAppVAA_TelefonieDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> VoordelenAppVAA_OverigeValidate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaVoordelenAppVAA_OverigeDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> VoordelenAppVAA_Interesten_RC_BedrijfsleiderValidate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaVoordelenAppVAA_Interesten_RC_BedrijfsleiderDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> VoordelenAppVAA_Soc_Bijdr_BedrijfsleiderValidate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaVoordelenAppVAA_Soc_Bijdr_BedrijfsleiderDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> VoordelenAppVAA_Overige_NutsValidate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaVoordelenAppVAA_Overige_NutsDataContract cedc);



        #endregion



        #region Add/Remove


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardAutoCO2Add(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaVoordelenAppVAA_AutoCO2DataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardAutoCO2Remove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardAutoCO2Update(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaVoordelenAppVAA_AutoCO2DataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardAutoRupoAdd(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaVoordelenAppVAA_AutoRupoDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardAutoRupoRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardAutoRupoUpdate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaVoordelenAppVAA_AutoRupoDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardNutsAdd(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaVoordelenAppVAA_NutsDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardNutsRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardNutsUpdate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaVoordelenAppVAA_NutsDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardHuurAdd(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaVoordelenAppVAA_HUURDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardHuurRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardHuurUpdate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaVoordelenAppVAA_HUURDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardHuurHerkwalificatieAdd(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaVoordelenAppVAA_HUURHERKDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardHuurHerkwalificatieRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardHuurHerkwalificatieUpdate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaVoordelenAppVAA_HUURHERKDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardPCAdd(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaVoordelenAppVAA_PCDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardPCRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardPCUpdate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaVoordelenAppVAA_PCDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardTelefonieAdd(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaVoordelenAppVAA_TelefonieDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardTelefonieRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardTelefonieUpdate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaVoordelenAppVAA_TelefonieDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardOverigeAdd(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaVoordelenAppVAA_OverigeDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardOverigeRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.VoordelenAppTypes.Voordelen VoordelenAppVoordelenVanAlleAardOverigeUpdate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaVoordelenAppVAA_OverigeDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.VoordelenAppTypes.Voordelen VoordelenAppVoordelenInterestenRCBedrijfsleiderAdd(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaVoordelenAppVAA_Interesten_RC_BedrijfsleiderDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.VoordelenAppTypes.Voordelen VoordelenAppVoordelenInterestenRCBedrijfsleiderRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.VoordelenAppTypes.Voordelen VoordelenAppVoordelenInterestenRCBedrijfsleiderUpdate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaVoordelenAppVAA_Interesten_RC_BedrijfsleiderDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.VoordelenAppTypes.Voordelen VoordelenAppVoordelenSocialeBijdragenBedrijfsleiderAdd(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaVoordelenAppVAA_Soc_Bijdr_BedrijfsleiderDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.VoordelenAppTypes.Voordelen VoordelenAppVoordelenSocialeBijdragenBedrijfsleiderRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.VoordelenAppTypes.Voordelen VoordelenAppVoordelenSocialeBijdragenBedrijfsleiderUpdate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaVoordelenAppVAA_Soc_Bijdr_BedrijfsleiderDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.VoordelenAppTypes.Voordelen VoordelenAppVoordelenOverigeNutsAdd(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaVoordelenAppVAA_Overige_NutsDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.VoordelenAppTypes.Voordelen VoordelenAppVoordelenOverigeNutsRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.VoordelenAppTypes.Voordelen VoordelenAppVoordelenOverigeNutsUpdate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaVoordelenAppVAA_Overige_NutsDataContract cedc);

        #endregion

        #endregion



        #region PersoneelApp
        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.PersoneelAppTypes.WPPersoneel PersoneelAppGetData(CriteriaFileDataContract cfdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.PersoneelAppTypes.WPPersoneel PersoneelAppCalculate(CriteriaFileDataContract cfdc);

        #region Entity validation

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> PersoneelAppItem325Validate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaPersoneelAppItem325DataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> PersoneelAppVerschilItemValidate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaPersoneelAppVerschilItemDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> PersoneelAppWerknemerValidate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaPersoneelAppWerknemerDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> PersoneelAppAlgemeenValidate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaPersoneelAppAlgemeenDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> PersoneelAppVakantiegeldValidate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaPersoneelAppVakantiegeldDataContract cedc);



        #endregion



        #region Add/Remove


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.PersoneelAppTypes.WPPersoneel PersoneelApp_325_20CollAdd(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaPersoneelAppItem325DataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.PersoneelAppTypes.WPPersoneel PersoneelApp_325_20CollRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.PersoneelAppTypes.WPPersoneel PersoneelApp_325_20CollUpdate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaPersoneelAppItem325DataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.PersoneelAppTypes.WPPersoneel PersoneelAppTeVerklarenVerschil325_20Add(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaPersoneelAppVerschilItemDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.PersoneelAppTypes.WPPersoneel PersoneelAppTeVerklarenVerschil325_20Remove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.PersoneelAppTypes.WPPersoneel PersoneelAppTeVerklarenVerschil325_20Update(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaPersoneelAppVerschilItemDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.PersoneelAppTypes.WPPersoneel PersoneelApp_325_10CollAdd(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaPersoneelAppItem325DataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.PersoneelAppTypes.WPPersoneel PersoneelApp_325_10CollRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.PersoneelAppTypes.WPPersoneel PersoneelApp_325_10CollUpdate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaPersoneelAppItem325DataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.PersoneelAppTypes.WPPersoneel PersoneelAppTeVerklarenVerschil325_10Add(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaPersoneelAppVerschilItemDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.PersoneelAppTypes.WPPersoneel PersoneelAppTeVerklarenVerschil325_10Remove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.PersoneelAppTypes.WPPersoneel PersoneelAppTeVerklarenVerschil325_10Update(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaPersoneelAppVerschilItemDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.PersoneelAppTypes.WPPersoneel PersoneelAppArbeidersFormUpdate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaPersoneelAppWerknemerDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.PersoneelAppTypes.WPPersoneel PersoneelAppBediendeFormUpdate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaPersoneelAppWerknemerDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.PersoneelAppTypes.WPPersoneel PersoneelAppAansluitingArbeidersAdd(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaPersoneelAppVerschilItemDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.PersoneelAppTypes.WPPersoneel PersoneelAppAansluitingArbeidersRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.PersoneelAppTypes.WPPersoneel PersoneelAppAansluitingArbeidersUpdate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaPersoneelAppVerschilItemDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.PersoneelAppTypes.WPPersoneel PersoneelAppAansluitingBediendenAdd(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaPersoneelAppVerschilItemDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.PersoneelAppTypes.WPPersoneel PersoneelAppAansluitingBediendenRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.PersoneelAppTypes.WPPersoneel PersoneelAppAansluitingBediendenUpdate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaPersoneelAppVerschilItemDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.PersoneelAppTypes.WPPersoneel PersoneelAppRSZCollAdd(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaPersoneelAppAlgemeenDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.PersoneelAppTypes.WPPersoneel PersoneelAppRSZCollRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.PersoneelAppTypes.WPPersoneel PersoneelAppRSZCollUpdate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaPersoneelAppAlgemeenDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.PersoneelAppTypes.WPPersoneel PersoneelAppBVsCollAdd(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaPersoneelAppAlgemeenDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.PersoneelAppTypes.WPPersoneel PersoneelAppBVsCollRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.PersoneelAppTypes.WPPersoneel PersoneelAppBVsCollUpdate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaPersoneelAppAlgemeenDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.PersoneelAppTypes.WPPersoneel PersoneelAppNettoCollAdd(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaPersoneelAppAlgemeenDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.PersoneelAppTypes.WPPersoneel PersoneelAppNettoCollRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.PersoneelAppTypes.WPPersoneel PersoneelAppNettoCollUpdate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaPersoneelAppAlgemeenDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.PersoneelAppTypes.WPPersoneel PersoneelAppVakantieGeldCollUpdate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaPersoneelAppVakantiegeldDataContract cedc);

        #endregion

        #endregion



        #region BTWApp
        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.BTWAppTypes.WPBTW BTWAppGetData(CriteriaFileDataContract cfdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.BTWAppTypes.WPBTW BTWAppCalculate(CriteriaFileDataContract cfdc);

        #region Entity validation

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> BTWAppOmzetAangifteValidate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaBTWAppOmzetAangifteDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> BTWAppOmzetGeboektValidate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaBTWAppOmzetGeboektDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> BTWAppOmzetVerklaringValidate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaBTWAppOmzetVerklaringDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> BTWAppRCAangifteValidate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaBTWAppRCAangifteDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> BTWAppRCGeboektValidate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaBTWAppRCGeboektDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> BTWAppRCVerklaringValidate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaBTWAppRCVerklaringDataContract cedc);



        #endregion



        #region Add/Remove


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.BTWAppTypes.WPBTW BTWAppOmzetAangiftesAdd(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaBTWAppOmzetAangifteDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.BTWAppTypes.WPBTW BTWAppOmzetAangiftesRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.BTWAppTypes.WPBTW BTWAppOmzetAangiftesUpdate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaBTWAppOmzetAangifteDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.BTWAppTypes.WPBTW BTWAppOmzettenGeboektAdd(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaBTWAppOmzetGeboektDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.BTWAppTypes.WPBTW BTWAppOmzettenGeboektRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.BTWAppTypes.WPBTW BTWAppOmzettenGeboektUpdate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaBTWAppOmzetGeboektDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.BTWAppTypes.WPBTW BTWAppOmzetVerklaringenAdd(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaBTWAppOmzetVerklaringDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.BTWAppTypes.WPBTW BTWAppOmzetVerklaringenRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.BTWAppTypes.WPBTW BTWAppOmzetVerklaringenUpdate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaBTWAppOmzetVerklaringDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.BTWAppTypes.WPBTW BTWAppRCAangiftesAdd(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaBTWAppRCAangifteDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.BTWAppTypes.WPBTW BTWAppRCAangiftesRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.BTWAppTypes.WPBTW BTWAppRCAangiftesUpdate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaBTWAppRCAangifteDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.BTWAppTypes.WPBTW BTWAppRCenGeboektAdd(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaBTWAppRCGeboektDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.BTWAppTypes.WPBTW BTWAppRCenGeboektRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.BTWAppTypes.WPBTW BTWAppRCenGeboektUpdate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaBTWAppRCGeboektDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.BTWAppTypes.WPBTW BTWAppRCVerklaringenAdd(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaBTWAppRCVerklaringDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.BTWAppTypes.WPBTW BTWAppRCVerklaringenRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.BTWAppTypes.WPBTW BTWAppRCVerklaringenUpdate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaBTWAppRCVerklaringDataContract cedc);

        #endregion

        #endregion



        #region RVIntrestenApp
        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.RVIntrestenAppTypes.WPRVIntresten RVIntrestenAppGetData(CriteriaFileDataContract cfdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.RVIntrestenAppTypes.WPRVIntresten RVIntrestenAppCalculate(CriteriaFileDataContract cfdc);

        #region Entity validation

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> RVIntrestenAppIntrestItemValidate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaRVIntrestenAppIntrestItemDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> RVIntrestenAppAdjustmentValidate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaRVIntrestenAppAdjustmentDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> RVIntrestenAppVerchilItemValidate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaRVIntrestenAppVerchilItemDataContract cedc);



        #endregion



        #region Add/Remove


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.RVIntrestenAppTypes.WPRVIntresten RVIntrestenAppIntrestenUpdate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaRVIntrestenAppIntrestItemDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.RVIntrestenAppTypes.WPRVIntresten RVIntrestenAppAdjustmentsAdd(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaRVIntrestenAppAdjustmentDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.RVIntrestenAppTypes.WPRVIntresten RVIntrestenAppAdjustmentsRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.RVIntrestenAppTypes.WPRVIntresten RVIntrestenAppAdjustmentsUpdate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaRVIntrestenAppAdjustmentDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.RVIntrestenAppTypes.WPRVIntresten RVIntrestenAppVerschillenAdd(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaRVIntrestenAppVerchilItemDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.RVIntrestenAppTypes.WPRVIntresten RVIntrestenAppVerschillenRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.RVIntrestenAppTypes.WPRVIntresten RVIntrestenAppVerschillenUpdate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaRVIntrestenAppVerchilItemDataContract cedc);

        #endregion

        #endregion



        #region DubieuzeDebiteurenApp
        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.DubieuzeDebiteurenAppTypes.DubieuzeDebiteuren DubieuzeDebiteurenAppGetData(CriteriaFileDataContract cfdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.DubieuzeDebiteurenAppTypes.DubieuzeDebiteuren DubieuzeDebiteurenAppCalculate(CriteriaFileDataContract cfdc);

        #region Entity validation

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> DubieuzeDebiteurenAppDubieuzeDebiteurTValidate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaDubieuzeDebiteurenAppDubieuzeDebiteurTDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> DubieuzeDebiteurenAppDubieuzeDebiteurWValidate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaDubieuzeDebiteurenAppDubieuzeDebiteurWDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> DubieuzeDebiteurenAppDubieuzeDebiteurNValidate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaDubieuzeDebiteurenAppDubieuzeDebiteurNDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> DubieuzeDebiteurenAppDubieuzeDebiteurHValidate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaDubieuzeDebiteurenAppDubieuzeDebiteurHDataContract cedc);



        #endregion



        #region Add/Remove


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.DubieuzeDebiteurenAppTypes.DubieuzeDebiteuren DubieuzeDebiteurenAppToestandEindVorigBoekjaarAdd(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaDubieuzeDebiteurenAppDubieuzeDebiteurTDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.DubieuzeDebiteurenAppTypes.DubieuzeDebiteuren DubieuzeDebiteurenAppToestandEindVorigBoekjaarRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.DubieuzeDebiteurenAppTypes.DubieuzeDebiteuren DubieuzeDebiteurenAppToestandEindVorigBoekjaarUpdate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaDubieuzeDebiteurenAppDubieuzeDebiteurTDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.DubieuzeDebiteurenAppTypes.DubieuzeDebiteuren DubieuzeDebiteurenAppWijzigingBestaandeTijdensBoekjaarAdd(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaDubieuzeDebiteurenAppDubieuzeDebiteurWDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.DubieuzeDebiteurenAppTypes.DubieuzeDebiteuren DubieuzeDebiteurenAppWijzigingBestaandeTijdensBoekjaarRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.DubieuzeDebiteurenAppTypes.DubieuzeDebiteuren DubieuzeDebiteurenAppWijzigingBestaandeTijdensBoekjaarUpdate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaDubieuzeDebiteurenAppDubieuzeDebiteurWDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.DubieuzeDebiteurenAppTypes.DubieuzeDebiteuren DubieuzeDebiteurenAppNieuweTijdensBoekjaarAdd(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaDubieuzeDebiteurenAppDubieuzeDebiteurNDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.DubieuzeDebiteurenAppTypes.DubieuzeDebiteuren DubieuzeDebiteurenAppNieuweTijdensBoekjaarRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.DubieuzeDebiteurenAppTypes.DubieuzeDebiteuren DubieuzeDebiteurenAppNieuweTijdensBoekjaarUpdate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaDubieuzeDebiteurenAppDubieuzeDebiteurNDataContract cedc);

        #endregion

        #endregion



        #region VerliesHandelsdebiteurenApp
        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.VerliesHandelsdebiteurenAppTypes.VerliesHandelsDebiteuren VerliesHandelsdebiteurenAppGetData(CriteriaFileDataContract cfdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.VerliesHandelsdebiteurenAppTypes.VerliesHandelsDebiteuren VerliesHandelsdebiteurenAppCalculate(CriteriaFileDataContract cfdc);

        #region Entity validation

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> VerliesHandelsdebiteurenAppVerliesHandelsDebiteurValidate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaVerliesHandelsdebiteurenAppVerliesHandelsDebiteurDataContract cedc);



        #endregion



        #region Add/Remove


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.VerliesHandelsdebiteurenAppTypes.VerliesHandelsDebiteuren VerliesHandelsdebiteurenAppVerliesHDAdd(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaVerliesHandelsdebiteurenAppVerliesHandelsDebiteurDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.VerliesHandelsdebiteurenAppTypes.VerliesHandelsDebiteuren VerliesHandelsdebiteurenAppVerliesHDRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.VerliesHandelsdebiteurenAppTypes.VerliesHandelsDebiteuren VerliesHandelsdebiteurenAppVerliesHDUpdate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaVerliesHandelsdebiteurenAppVerliesHandelsDebiteurDataContract cedc);

        #endregion

        #endregion



        #region VoorzieningenApp
        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.VoorzieningenAppTypes.Voorzieningen VoorzieningenAppGetData(CriteriaFileDataContract cfdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.VoorzieningenAppTypes.Voorzieningen VoorzieningenAppCalculate(CriteriaFileDataContract cfdc);

        #region Entity validation

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> VoorzieningenAppVoorzieningenTValidate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaVoorzieningenAppVoorzieningenTDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> VoorzieningenAppVoorzieningenWValidate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaVoorzieningenAppVoorzieningenWDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> VoorzieningenAppVoorzieningenNValidate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaVoorzieningenAppVoorzieningenNDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> VoorzieningenAppVoorzieningenHValidate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaVoorzieningenAppVoorzieningenHDataContract cedc);



        #endregion



        #region Add/Remove


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.VoorzieningenAppTypes.Voorzieningen VoorzieningenAppToestandEindVorigBoekjaarAdd(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaVoorzieningenAppVoorzieningenTDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.VoorzieningenAppTypes.Voorzieningen VoorzieningenAppToestandEindVorigBoekjaarRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.VoorzieningenAppTypes.Voorzieningen VoorzieningenAppToestandEindVorigBoekjaarUpdate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaVoorzieningenAppVoorzieningenTDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.VoorzieningenAppTypes.Voorzieningen VoorzieningenAppWijzigingBestaandeTijdensBoekjaarAdd(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaVoorzieningenAppVoorzieningenWDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.VoorzieningenAppTypes.Voorzieningen VoorzieningenAppWijzigingBestaandeTijdensBoekjaarRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.VoorzieningenAppTypes.Voorzieningen VoorzieningenAppWijzigingBestaandeTijdensBoekjaarUpdate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaVoorzieningenAppVoorzieningenWDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.VoorzieningenAppTypes.Voorzieningen VoorzieningenAppNieuweTijdensBoekjaarAdd(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaVoorzieningenAppVoorzieningenNDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.VoorzieningenAppTypes.Voorzieningen VoorzieningenAppNieuweTijdensBoekjaarRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.VoorzieningenAppTypes.Voorzieningen VoorzieningenAppNieuweTijdensBoekjaarUpdate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaVoorzieningenAppVoorzieningenNDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.VoorzieningenAppTypes.Voorzieningen VoorzieningenAppToestandEindHuidigBoekjaarAdd(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaVoorzieningenAppVoorzieningenHDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.VoorzieningenAppTypes.Voorzieningen VoorzieningenAppToestandEindHuidigBoekjaarRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.VoorzieningenAppTypes.Voorzieningen VoorzieningenAppToestandEindHuidigBoekjaarUpdate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaVoorzieningenAppVoorzieningenHDataContract cedc);

        #endregion

        #endregion



        #region BelastingenApp
        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.BelastingenAppTypes.WPBelastingen BelastingenAppGetData(CriteriaFileDataContract cfdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.BelastingenAppTypes.WPBelastingen BelastingenAppCalculate(CriteriaFileDataContract cfdc);

        #region Entity validation

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> BelastingenAppAdjustmentValidate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaBelastingenAppAdjustmentDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> BelastingenAppBelastingValidate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaBelastingenAppBelastingDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> BelastingenAppKostItemValidate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaBelastingenAppKostItemDataContract cedc);



        #endregion



        #region Add/Remove


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.BelastingenAppTypes.WPBelastingen BelastingenAppAdjustmentsAdd(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaBelastingenAppAdjustmentDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.BelastingenAppTypes.WPBelastingen BelastingenAppAdjustmentsRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.BelastingenAppTypes.WPBelastingen BelastingenAppAdjustmentsUpdate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaBelastingenAppAdjustmentDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.BelastingenAppTypes.WPBelastingen BelastingenAppBelastingenUpdate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaBelastingenAppBelastingDataContract cedc);

        #endregion

        #endregion


        #region MeerwaardeAandelenApp
        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.MeerwaardeAandelenAppTypes.MeerwaardeAandelen MeerwaardeAandelenAppGetData(CriteriaFileDataContract cfdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.MeerwaardeAandelenAppTypes.MeerwaardeAandelen MeerwaardeAandelenAppCalculate(CriteriaFileDataContract cfdc);

        #region Entity validation

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> MeerwaardeAandelenAppMeerwaardeAandelenItemValidate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaMeerwaardeAandelenAppMeerwaardeAandelenItemDataContract cedc);

        #endregion



        #region Add/Remove


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.MeerwaardeAandelenAppTypes.MeerwaardeAandelen MeerwaardeAandelenAppMeerwaardeAandelenLijstAdd(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaMeerwaardeAandelenAppMeerwaardeAandelenItemDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.MeerwaardeAandelenAppTypes.MeerwaardeAandelen MeerwaardeAandelenAppMeerwaardeAandelenLijstRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.MeerwaardeAandelenAppTypes.MeerwaardeAandelen MeerwaardeAandelenAppMeerwaardeAandelenLijstUpdate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaMeerwaardeAandelenAppMeerwaardeAandelenItemDataContract cedc);

        #endregion

        #endregion

        #region BelasteReservesApp
        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.BelasteReservesAppTypes.BelasteReserves BelasteReservesAppGetData(CriteriaFileDataContract cfdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.BelasteReservesAppTypes.BelasteReserves BelasteReservesAppCalculate(CriteriaFileDataContract cfdc);

        #region Entity validation

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> BelasteReservesAppBelasteReservesItemValidate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaBelasteReservesAppBelasteReservesItemDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> BelasteReservesAppAanpassingBTReservesItemValidate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaBelasteReservesAppAanpassingBTReservesItemDataContract cedc);



        #endregion



        #region Add/Remove


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.BelasteReservesAppTypes.BelasteReserves BelasteReservesAppAllBelasteReservesAdd(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaBelasteReservesAppBelasteReservesItemDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.BelasteReservesAppTypes.BelasteReserves BelasteReservesAppAllBelasteReservesRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.BelasteReservesAppTypes.BelasteReserves BelasteReservesAppAllBelasteReservesUpdate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaBelasteReservesAppBelasteReservesItemDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.BelasteReservesAppTypes.BelasteReserves BelasteReservesAppAanpassingBTReservesAdd(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaBelasteReservesAppAanpassingBTReservesItemDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.BelasteReservesAppTypes.BelasteReserves BelasteReservesAppAanpassingBTReservesRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.BelasteReservesAppTypes.BelasteReserves BelasteReservesAppAanpassingBTReservesUpdate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaBelasteReservesAppAanpassingBTReservesItemDataContract cedc);

        #endregion

        #endregion



        #region BelastingVrijeReservesApp
        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.BelastingVrijeReservesAppTypes.BelastingVrijeReserves BelastingVrijeReservesAppGetData(CriteriaFileDataContract cfdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.BelastingVrijeReservesAppTypes.BelastingVrijeReserves BelastingVrijeReservesAppCalculate(CriteriaFileDataContract cfdc);

        #region Entity validation

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> BelastingVrijeReservesAppDetailValidate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaBelastingVrijeReservesAppDetailDataContract cedc);



        #endregion



        #region Add/Remove


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.BelastingVrijeReservesAppTypes.BelastingVrijeReserves BelastingVrijeReservesAppVrijgesteldeReservesUpdate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaBelastingVrijeReservesAppDetailDataContract cedc);

        #endregion

        #endregion



        #region BepalingBelastbareBasisApp
        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.BepalingBelastbareBasisAppTypes.BepalingBasis BepalingBelastbareBasisAppGetData(CriteriaFileDataContract cfdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.BepalingBelastbareBasisAppTypes.BepalingBasis BepalingBelastbareBasisAppCalculate(CriteriaFileDataContract cfdc);

        #region Entity validation

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> BepalingBelastbareBasisAppGeneralValidate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaBepalingBelastbareBasisAppGeneralDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> BepalingBelastbareBasisAppBelasteReservesItemValidate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaBepalingBelastbareBasisAppBelasteReservesItemDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> BepalingBelastbareBasisAppVerworpenUitgavenItemValidate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaBepalingBelastbareBasisAppVerworpenUitgavenItemDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> BepalingBelastbareBasisAppBelastbareBestZonderAftrekItemValidate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaBepalingBelastbareBasisAppBelastbareBestZonderAftrekItemDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> BepalingBelastbareBasisAppOpdelingNaarOorsprongItemValidate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaBepalingBelastbareBasisAppOpdelingNaarOorsprongItemDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> BepalingBelastbareBasisAppFiscaleAftrekItemValidate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaBepalingBelastbareBasisAppFiscaleAftrekItemDataContract cedc);



        #endregion



        #region Add/Remove


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.BepalingBelastbareBasisAppTypes.BepalingBasis BepalingBelastbareBasisAppOverzichtUpdate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaBepalingBelastbareBasisAppGeneralDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.BepalingBelastbareBasisAppTypes.BepalingBasis BepalingBelastbareBasisAppBelasteReservesUpdate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaBepalingBelastbareBasisAppBelasteReservesItemDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.BepalingBelastbareBasisAppTypes.BepalingBasis BepalingBelastbareBasisAppVerworpenUitgavenUpdate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaBepalingBelastbareBasisAppVerworpenUitgavenItemDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.BepalingBelastbareBasisAppTypes.BepalingBasis BepalingBelastbareBasisAppBelastbareBestZonderAftrekUpdate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaBepalingBelastbareBasisAppBelastbareBestZonderAftrekItemDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.BepalingBelastbareBasisAppTypes.BepalingBasis BepalingBelastbareBasisAppOpdelingNaarOorsprongUpdate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaBepalingBelastbareBasisAppOpdelingNaarOorsprongItemDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.BepalingBelastbareBasisAppTypes.BepalingBasis BepalingBelastbareBasisAppFiscaleAftrekUpdate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaBepalingBelastbareBasisAppFiscaleAftrekItemDataContract cedc);

        #endregion

        #endregion



        #region DBIApp
        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.DBIAppTypes.DBIWorksheet DBIAppGetData(CriteriaFileDataContract cfdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.DBIAppTypes.DBIWorksheet DBIAppCalculate(CriteriaFileDataContract cfdc);

        #region Entity validation

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> DBIAppParticipatieItemValidate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaDBIAppParticipatieItemDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> DBIAppDividendItemValidate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaDBIAppDividendItemDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> DBIAppDbiEntityValidate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaDBIAppDbiEntityDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> DBIAppHistoryItemValidate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaDBIAppHistoryItemDataContract cedc);



        #endregion



        #region Add/Remove


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.DBIAppTypes.DBIWorksheet DBIAppParticipationsAdd(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaDBIAppParticipatieItemDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.DBIAppTypes.DBIWorksheet DBIAppParticipationsRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.DBIAppTypes.DBIWorksheet DBIAppParticipationsUpdate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaDBIAppParticipatieItemDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.DBIAppTypes.DBIWorksheet DBIAppDividendsAdd(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaDBIAppDividendItemDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.DBIAppTypes.DBIWorksheet DBIAppDividendsRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.DBIAppTypes.DBIWorksheet DBIAppDividendsUpdate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaDBIAppDividendItemDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.DBIAppTypes.DBIWorksheet DBIAppDBIUpdate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaDBIAppDbiEntityDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.DBIAppTypes.DBIWorksheet DBIAppHistoriekAdd(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaDBIAppHistoryItemDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.DBIAppTypes.DBIWorksheet DBIAppHistoriekRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.DBIAppTypes.DBIWorksheet DBIAppHistoriekUpdate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaDBIAppHistoryItemDataContract cedc);

        #endregion

        #endregion



        #region RisicoKapitaalApp
        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.RisicoKapitaalAppTypes.WPRisicokapitaal RisicoKapitaalAppGetData(CriteriaFileDataContract cfdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.RisicoKapitaalAppTypes.WPRisicokapitaal RisicoKapitaalAppCalculate(CriteriaFileDataContract cfdc);

        #region Entity validation

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> RisicoKapitaalAppFicheItemValidate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaRisicoKapitaalAppFicheItemDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> RisicoKapitaalAppEigenVermogenValidate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaRisicoKapitaalAppEigenVermogenDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> RisicoKapitaalAppBestanddeelValidate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaRisicoKapitaalAppBestanddeelDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> RisicoKapitaalAppWijzigingItemValidate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaRisicoKapitaalAppWijzigingItemDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> RisicoKapitaalAppTariefItemValidate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaRisicoKapitaalAppTariefItemDataContract cedc);

        /*
        * new NID
        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> RisicoKapitaalAppHistoryItemValidate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaRisicoKapitaalAppHistoryItemDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> RisicoKapitaalAppHistoriekAanpassingItemValidate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaRisicoKapitaalAppHistoriekAanpassingItemDataContract cedc);
        */


        #endregion



        #region Add/Remove


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.RisicoKapitaalAppTypes.WPRisicokapitaal RisicoKapitaalAppFicheUpdate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaRisicoKapitaalAppFicheItemDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.RisicoKapitaalAppTypes.WPRisicokapitaal RisicoKapitaalAppEigenVermogensAdd(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaRisicoKapitaalAppEigenVermogenDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.RisicoKapitaalAppTypes.WPRisicokapitaal RisicoKapitaalAppEigenVermogensRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.RisicoKapitaalAppTypes.WPRisicokapitaal RisicoKapitaalAppEigenVermogensUpdate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaRisicoKapitaalAppEigenVermogenDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.RisicoKapitaalAppTypes.WPRisicokapitaal RisicoKapitaalAppBestanddelenAdd(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaRisicoKapitaalAppBestanddeelDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.RisicoKapitaalAppTypes.WPRisicokapitaal RisicoKapitaalAppBestanddelenRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.RisicoKapitaalAppTypes.WPRisicokapitaal RisicoKapitaalAppBestanddelenUpdate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaRisicoKapitaalAppBestanddeelDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.RisicoKapitaalAppTypes.WPRisicokapitaal RisicoKapitaalAppWijzigingenAdd(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaRisicoKapitaalAppWijzigingItemDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.RisicoKapitaalAppTypes.WPRisicokapitaal RisicoKapitaalAppWijzigingenRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.RisicoKapitaalAppTypes.WPRisicokapitaal RisicoKapitaalAppWijzigingenUpdate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaRisicoKapitaalAppWijzigingItemDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.RisicoKapitaalAppTypes.WPRisicokapitaal RisicoKapitaalAppTariefUpdate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaRisicoKapitaalAppTariefItemDataContract cedc);


        /*
        * new NID
        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.RisicoKapitaalAppTypes.WPRisicokapitaal RisicoKapitaalAppHistoriekAdd(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaRisicoKapitaalAppHistoryItemDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.RisicoKapitaalAppTypes.WPRisicokapitaal RisicoKapitaalAppHistoriekRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.RisicoKapitaalAppTypes.WPRisicokapitaal RisicoKapitaalAppHistoriekUpdate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaRisicoKapitaalAppHistoryItemDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.RisicoKapitaalAppTypes.WPRisicokapitaal RisicoKapitaalAppAanpassingenHistoriekAdd(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaRisicoKapitaalAppHistoriekAanpassingItemDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.RisicoKapitaalAppTypes.WPRisicokapitaal RisicoKapitaalAppAanpassingenHistoriekRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.RisicoKapitaalAppTypes.WPRisicokapitaal RisicoKapitaalAppAanpassingenHistoriekUpdate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaRisicoKapitaalAppHistoriekAanpassingItemDataContract cedc);
        */
        #endregion

        #endregion


        #region RisicoKapitaalHistoriekApp
        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.RisicoKapitaalHistoriekAppTypes.WPRisicokapitaalHistoriek RisicoKapitaalHistoriekAppGetData(CriteriaFileDataContract cfdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.RisicoKapitaalHistoriekAppTypes.WPRisicokapitaalHistoriek RisicoKapitaalHistoriekAppCalculate(CriteriaFileDataContract cfdc);

        #region Entity validation

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> RisicoKapitaalHistoriekAppFicheItemValidate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaRisicoKapitaalHistoriekAppFicheItemDataContract cedc);

        /*
        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> RisicoKapitaalHistoriekAppEigenVermogenValidate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaRisicoKapitaalHistoriekAppEigenVermogenDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> RisicoKapitaalHistoriekAppBestanddeelValidate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaRisicoKapitaalHistoriekAppBestanddeelDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> RisicoKapitaalHistoriekAppWijzigingItemValidate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaRisicoKapitaalHistoriekAppWijzigingItemDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> RisicoKapitaalHistoriekAppTariefItemValidate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaRisicoKapitaalHistoriekAppTariefItemDataContract cedc);

        */
        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> RisicoKapitaalHistoriekAppHistoryItemValidate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaRisicoKapitaalHistoriekAppHistoryItemDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> RisicoKapitaalHistoriekAppHistoriekAanpassingItemValidate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaRisicoKapitaalHistoriekAppHistoriekAanpassingItemDataContract cedc);



        #endregion



        #region Add/Remove


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.RisicoKapitaalHistoriekAppTypes.WPRisicokapitaalHistoriek RisicoKapitaalHistoriekAppFicheUpdate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaRisicoKapitaalHistoriekAppFicheItemDataContract cedc);
        /*
        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.RisicoKapitaalHistoriekAppTypes.WPRisicokapitaalHistoriek RisicoKapitaalHistoriekAppEigenVermogensAdd(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaRisicoKapitaalHistoriekAppEigenVermogenDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.RisicoKapitaalHistoriekAppTypes.WPRisicokapitaalHistoriek RisicoKapitaalHistoriekAppEigenVermogensRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.RisicoKapitaalHistoriekAppTypes.WPRisicokapitaalHistoriek RisicoKapitaalHistoriekAppEigenVermogensUpdate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaRisicoKapitaalHistoriekAppEigenVermogenDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.RisicoKapitaalHistoriekAppTypes.WPRisicokapitaalHistoriek RisicoKapitaalHistoriekAppBestanddelenAdd(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaRisicoKapitaalHistoriekAppBestanddeelDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.RisicoKapitaalHistoriekAppTypes.WPRisicokapitaalHistoriek RisicoKapitaalHistoriekAppBestanddelenRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.RisicoKapitaalHistoriekAppTypes.WPRisicokapitaalHistoriek RisicoKapitaalHistoriekAppBestanddelenUpdate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaRisicoKapitaalHistoriekAppBestanddeelDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.RisicoKapitaalHistoriekAppTypes.WPRisicokapitaalHistoriek RisicoKapitaalHistoriekAppWijzigingenAdd(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaRisicoKapitaalHistoriekAppWijzigingItemDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.RisicoKapitaalHistoriekAppTypes.WPRisicokapitaalHistoriek RisicoKapitaalHistoriekAppWijzigingenRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.RisicoKapitaalHistoriekAppTypes.WPRisicokapitaalHistoriek RisicoKapitaalHistoriekAppWijzigingenUpdate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaRisicoKapitaalHistoriekAppWijzigingItemDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.RisicoKapitaalHistoriekAppTypes.WPRisicokapitaalHistoriek RisicoKapitaalHistoriekAppTariefUpdate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaRisicoKapitaalHistoriekAppTariefItemDataContract cedc);
        */
        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.RisicoKapitaalHistoriekAppTypes.WPRisicokapitaalHistoriek RisicoKapitaalHistoriekAppHistoriekAdd(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaRisicoKapitaalHistoriekAppHistoryItemDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.RisicoKapitaalHistoriekAppTypes.WPRisicokapitaalHistoriek RisicoKapitaalHistoriekAppHistoriekRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.RisicoKapitaalHistoriekAppTypes.WPRisicokapitaalHistoriek RisicoKapitaalHistoriekAppHistoriekUpdate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaRisicoKapitaalHistoriekAppHistoryItemDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.RisicoKapitaalHistoriekAppTypes.WPRisicokapitaalHistoriek RisicoKapitaalHistoriekAppAanpassingenHistoriekAdd(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaRisicoKapitaalHistoriekAppHistoriekAanpassingItemDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.RisicoKapitaalHistoriekAppTypes.WPRisicokapitaalHistoriek RisicoKapitaalHistoriekAppAanpassingenHistoriekRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.RisicoKapitaalHistoriekAppTypes.WPRisicokapitaalHistoriek RisicoKapitaalHistoriekAppAanpassingenHistoriekUpdate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaRisicoKapitaalHistoriekAppHistoriekAanpassingItemDataContract cedc);

        #endregion

        #endregion



        #region TransferTaxCreditsApp
        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.TransferTaxCreditsAppTypes.TransferTaxCredits TransferTaxCreditsAppGetData(CriteriaFileDataContract cfdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.TransferTaxCreditsAppTypes.TransferTaxCredits TransferTaxCreditsAppCalculate(CriteriaFileDataContract cfdc);

        #region Entity validation

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> TransferTaxCreditsAppFiscaalVerliesValidate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaTransferTaxCreditsAppFiscaalVerliesDataContract cedc);

        /*
        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> TransferTaxCreditsAppNIDItemValidate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaTransferTaxCreditsAppNIDItemDataContract cedc);
        */

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> TransferTaxCreditsAppDBIItemValidate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaTransferTaxCreditsAppDBIItemDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> TransferTaxCreditsAppInvestItemValidate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaTransferTaxCreditsAppInvestItemDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> TransferTaxCreditsAppBelastingkredietItemValidate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaTransferTaxCreditsAppBelastingkredietItemDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> TransferTaxCreditsAppAanpassingItemValidate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaTransferTaxCreditsAppAanpassingItemDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> TransferTaxCreditsAppAanwendingValidate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaTransferTaxCreditsAppAanwendingDataContract cedc);



        #endregion



        #region Add/Remove


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.TransferTaxCreditsAppTypes.TransferTaxCredits TransferTaxCreditsAppFiscaleVerliezenAdd(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaTransferTaxCreditsAppFiscaalVerliesDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.TransferTaxCreditsAppTypes.TransferTaxCredits TransferTaxCreditsAppFiscaleVerliezenRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.TransferTaxCreditsAppTypes.TransferTaxCredits TransferTaxCreditsAppFiscaleVerliezenUpdate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaTransferTaxCreditsAppFiscaalVerliesDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.TransferTaxCreditsAppTypes.TransferTaxCredits TransferTaxCreditsAppDBIUpdate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaTransferTaxCreditsAppDBIItemDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.TransferTaxCreditsAppTypes.TransferTaxCredits TransferTaxCreditsAppInvestAdd(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaTransferTaxCreditsAppInvestItemDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.TransferTaxCreditsAppTypes.TransferTaxCredits TransferTaxCreditsAppInvestRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.TransferTaxCreditsAppTypes.TransferTaxCredits TransferTaxCreditsAppInvestUpdate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaTransferTaxCreditsAppInvestItemDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.TransferTaxCreditsAppTypes.TransferTaxCredits TransferTaxCreditsAppBelastingkredietAdd(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaTransferTaxCreditsAppBelastingkredietItemDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.TransferTaxCreditsAppTypes.TransferTaxCredits TransferTaxCreditsAppBelastingkredietRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.TransferTaxCreditsAppTypes.TransferTaxCredits TransferTaxCreditsAppBelastingkredietUpdate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaTransferTaxCreditsAppBelastingkredietItemDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.TransferTaxCreditsAppTypes.TransferTaxCredits TransferTaxCreditsAppAanpassingenAdd(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaTransferTaxCreditsAppAanpassingItemDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.TransferTaxCreditsAppTypes.TransferTaxCredits TransferTaxCreditsAppAanpassingenRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.TransferTaxCreditsAppTypes.TransferTaxCredits TransferTaxCreditsAppAanpassingenUpdate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaTransferTaxCreditsAppAanpassingItemDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.TransferTaxCreditsAppTypes.TransferTaxCredits TransferTaxCreditsAppAanwendingenUpdate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaTransferTaxCreditsAppAanwendingDataContract cedc);

        #endregion

        #endregion



        #region BerekeningVenBApp
        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.BerekeningVenBAppTypes.BerekeningVenB BerekeningVenBAppGetData(CriteriaFileDataContract cfdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.BerekeningVenBAppTypes.BerekeningVenB BerekeningVenBAppCalculate(CriteriaFileDataContract cfdc);

        #region Entity validation

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> BerekeningVenBAppGeneralValidate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaBerekeningVenBAppGeneralDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> BerekeningVenBAppBelasteReservesItemValidate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaBerekeningVenBAppBelasteReservesItemDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> BerekeningVenBAppVerworpenUitgavenItemValidate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaBerekeningVenBAppVerworpenUitgavenItemDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> BerekeningVenBAppBelastbareBestZonderAftrekItemValidate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaBerekeningVenBAppBelastbareBestZonderAftrekItemDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> BerekeningVenBAppOpdelingNaarOorsprongItemValidate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaBerekeningVenBAppOpdelingNaarOorsprongItemDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> BerekeningVenBAppFiscaleAftrekItemValidate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaBerekeningVenBAppFiscaleAftrekItemDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> BerekeningVenBAppTariefItemValidate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaBerekeningVenBAppTariefItemDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> BerekeningVenBAppAanslagValidate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaBerekeningVenBAppAanslagDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> BerekeningVenBAppBelastingsBerekeningItemValidate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaBerekeningVenBAppBelastingsBerekeningItemDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> BerekeningVenBAppBelastingsBerekeningItem2Validate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaBerekeningVenBAppBelastingsBerekeningItem2DataContract cedc);



        #endregion



        #region Add/Remove


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.BerekeningVenBAppTypes.BerekeningVenB BerekeningVenBAppOverzichtUpdate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaBerekeningVenBAppGeneralDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.BerekeningVenBAppTypes.BerekeningVenB BerekeningVenBAppBelasteReservesUpdate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaBerekeningVenBAppBelasteReservesItemDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.BerekeningVenBAppTypes.BerekeningVenB BerekeningVenBAppVerworpenUitgavenUpdate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaBerekeningVenBAppVerworpenUitgavenItemDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.BerekeningVenBAppTypes.BerekeningVenB BerekeningVenBAppBelastbareBestZonderAftrekUpdate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaBerekeningVenBAppBelastbareBestZonderAftrekItemDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.BerekeningVenBAppTypes.BerekeningVenB BerekeningVenBAppOpdelingNaarOorsprongUpdate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaBerekeningVenBAppOpdelingNaarOorsprongItemDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.BerekeningVenBAppTypes.BerekeningVenB BerekeningVenBAppFiscaleAftrekUpdate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaBerekeningVenBAppFiscaleAftrekItemDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.BerekeningVenBAppTypes.BerekeningVenB BerekeningVenBAppTariefUpdate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaBerekeningVenBAppTariefItemDataContract cedc);


        
        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.BerekeningVenBAppTypes.BerekeningVenB BerekeningVenBAppBelastingsBerekeningUpdate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaBerekeningVenBAppBelastingsBerekeningItemDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.BerekeningVenBAppTypes.BerekeningVenB BerekeningVenBAppBelastingsBerekening2Update(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaBerekeningVenBAppBelastingsBerekeningItem2DataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.BerekeningVenBAppTypes.BerekeningVenB BerekeningVenBAppAanslagUpdate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaBerekeningVenBAppAanslagDataContract cedc);


        #endregion

        #endregion



        #region AangifteVenBApp
        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.AangifteVenBAppTypes.AangifteVenB AangifteVenBAppGetData(CriteriaFileDataContract cfdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.AangifteVenBAppTypes.AangifteVenB AangifteVenBAppCalculate(CriteriaFileDataContract cfdc);

        #region Entity validation

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> AangifteVenBAppReserveEntityValidate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaAangifteVenBAppReserveEntityDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> AangifteVenBAppVrijgesteldeReserveEntityValidate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaAangifteVenBAppVrijgesteldeReserveEntityDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> AangifteVenBAppVerworpenUitgavenEntityValidate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaAangifteVenBAppVerworpenUitgavenEntityDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> AangifteVenBAppUitgekeerdeDividendenEntityValidate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaAangifteVenBAppUitgekeerdeDividendenEntityDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> AangifteVenBAppWinstEntityValidate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaAangifteVenBAppWinstEntityDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> AangifteVenBAppAfzonderlijkeAanslagenEntityValidate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaAangifteVenBAppAfzonderlijkeAanslagenEntityDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> AangifteVenBAppDiamantEntityValidate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaAangifteVenBAppDiamantEntityDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> AangifteVenBAppNietBelastbareBestandelenItemValidate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaAangifteVenBAppNietBelastbareBestandelenItemDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> AangifteVenBAppDbiEntityValidate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaAangifteVenBAppDbiEntityDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> AangifteVenBAppOverdrachtDBIAftrekEntityValidate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaAangifteVenBAppOverdrachtDBIAftrekEntityDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> AangifteVenBAppOverdrachtNIDEntityValidate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaAangifteVenBAppOverdrachtNIDEntityDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> AangifteVenBAppCompenseerbareVerliezenEntityValidate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaAangifteVenBAppCompenseerbareVerliezenEntityDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> AangifteVenBAppTariefEntityValidate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaAangifteVenBAppTariefEntityDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> AangifteVenBAppVoorafBetalingEntityValidate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaAangifteVenBAppVoorafBetalingEntityDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> AangifteVenBAppVerrekenbareVoorheffingEntityValidate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaAangifteVenBAppVerrekenbareVoorheffingEntityDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> AangifteVenBAppTaxShelterEntityValidate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaAangifteVenBAppTaxShelterEntityDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> AangifteVenBAppGrootteVennootschapeEntityValidate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaAangifteVenBAppGrootteVennootschapeEntityDataContract cedc);



        #endregion



        #region Add/Remove


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.AangifteVenBAppTypes.AangifteVenB AangifteVenBAppReservesUpdate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaAangifteVenBAppReserveEntityDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.AangifteVenBAppTypes.AangifteVenB AangifteVenBAppVrijgesteldeReserveUpdate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaAangifteVenBAppVrijgesteldeReserveEntityDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.AangifteVenBAppTypes.AangifteVenB AangifteVenBAppVerworpenUitgavenUpdate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaAangifteVenBAppVerworpenUitgavenEntityDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.AangifteVenBAppTypes.AangifteVenB AangifteVenBAppUitgekeerdeDividendenUpdate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaAangifteVenBAppUitgekeerdeDividendenEntityDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.AangifteVenBAppTypes.AangifteVenB AangifteVenBAppUiteenzettingWinstUpdate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaAangifteVenBAppWinstEntityDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.AangifteVenBAppTypes.AangifteVenB AangifteVenBAppAfzonderlijkeAanslagenUpdate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaAangifteVenBAppAfzonderlijkeAanslagenEntityDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.AangifteVenBAppTypes.AangifteVenB AangifteVenBAppDiamantUpdate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaAangifteVenBAppDiamantEntityDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.AangifteVenBAppTypes.AangifteVenB AangifteVenBAppNietBelastbareBestanddelenUpdate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaAangifteVenBAppNietBelastbareBestandelenItemDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.AangifteVenBAppTypes.AangifteVenB AangifteVenBAppDBIUpdate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaAangifteVenBAppDbiEntityDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.AangifteVenBAppTypes.AangifteVenB AangifteVenBAppOverdrachtDBIAftrekUpdate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaAangifteVenBAppOverdrachtDBIAftrekEntityDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.AangifteVenBAppTypes.AangifteVenB AangifteVenBAppOverdrachtNIDUpdate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaAangifteVenBAppOverdrachtNIDEntityDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.AangifteVenBAppTypes.AangifteVenB AangifteVenBAppCompenseerbareVerliezenUpdate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaAangifteVenBAppCompenseerbareVerliezenEntityDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.AangifteVenBAppTypes.AangifteVenB AangifteVenBAppTariefBelastingUpdate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaAangifteVenBAppTariefEntityDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.AangifteVenBAppTypes.AangifteVenB AangifteVenBAppVoorafBetalingenUpdate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaAangifteVenBAppVoorafBetalingEntityDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.AangifteVenBAppTypes.AangifteVenB AangifteVenBAppVerrekenbareVoorheffingUpdate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaAangifteVenBAppVerrekenbareVoorheffingEntityDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.AangifteVenBAppTypes.AangifteVenB AangifteVenBAppTaxShelterUpdate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaAangifteVenBAppTaxShelterEntityDataContract cedc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.AangifteVenBAppTypes.AangifteVenB AangifteVenBAppGrootteVennootschapUpdate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaAangifteVenBAppGrootteVennootschapeEntityDataContract cedc);

        #endregion

        #endregion

        #region DetailRekeningKapitaal
        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.DetailRekeningKapitaalAppTypes.DetailRekeningKapitaal DetailRekeningKapitaalAppGetData(CriteriaFileDataContract cfdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.DetailRekeningKapitaalAppTypes.DetailRekeningKapitaal DetailRekeningKapitaalAppCalculate(CriteriaFileDataContract cfdc);

        #region Entity validation

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> DetailRekeningKapitaalAppOverzichtValidate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaDetailRekeningKapitaalEntityDataContract cedc);
        

        #endregion



        #region Add/Remove

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.DetailRekeningKapitaalAppTypes.DetailRekeningKapitaal DetailRekeningKapitaalAppOverzichtAdd(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaDetailRekeningKapitaalEntityDataContract cedc);


        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.DetailRekeningKapitaalAppTypes.DetailRekeningKapitaal DetailRekeningKapitaalAppOverzichtRemove(CriteriaFileAndRowDataContract cfrdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.RuleEngine.AY2013.DetailRekeningKapitaalAppTypes.DetailRekeningKapitaal DetailRekeningKapitaalAppOverzichtUpdate(EY.com.eBook.RuleEngine.Data.AY2013.CriteriaDetailRekeningKapitaalEntityDataContract cedc);
                
        #endregion

        #endregion



        #endregion

   // #endregion

    }

    
}
