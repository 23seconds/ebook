
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.RuleEngine.Core;
using EY.com.eBook.RuleEngine.Core.Interfaces;
using EY.com.eBook.RuleEngine.Data;
using EY.com.eBook.Core;
using EY.com.eBook.API.Contracts.Data.Cache;
using System.Globalization;

namespace EY.com.eBook.RuleEngine.AY2013
{
    
    public class DBIApp
    {
        public static WorksheetTypeSettings Settings = new WorksheetTypeSettings
                                                      {
                                                        Clients = true
                                                        ,
                                                        Suppliers = true
                                                        ,
                                                        AllAccounts = true
                                                        , 
                                                        PerformsBookings=true
                                                      };
                                                      
        public static Guid TypeId = new Guid("77d8dbbc-feec-478e-9e11-83838e346caf");

        public List<string> ImportablePreviousTypes  = new List<string>() {  "EY.com.eBook.RuleEngine.AY2012.FacturatieApp"  };

        public Guid GetMyType() { return TypeId; }

        public void SetMinimal()
        {
            if (Data == null)
            {
                Data = new EY.com.eBook.RuleEngine.AY2013.DBIAppTypes.DBIWorksheet();
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.Participations == null)
            {
                Data.Participations = new List<EY.com.eBook.RuleEngine.AY2013.DBIAppTypes.ParticipatieItem>(); 
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.Dividends == null)
            {
                Data.Dividends = new List<EY.com.eBook.RuleEngine.AY2013.DBIAppTypes.DividendItem>(); ;
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.DBI == null)
            {
                Data.DBI = new EY.com.eBook.RuleEngine.AY2013.DBIAppTypes.DbiEntity
                {
                    Id = Guid.NewGuid()
                    ,
                    LastUpdated = null
                    ,
                    IsValid = true
                    ,
                    PreviousImported = false

                };
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.Historiek == null)
            {
                Data.Historiek = new List<EY.com.eBook.RuleEngine.AY2013.DBIAppTypes.HistoryItem>(); ;
                Data.LastUpdated = DateTime.Now;
            }

        }
        
        public string ToXml() 
        {
            return CoreHelper.SerializeToString(Data);
        }

        public void LoadXml(string xml)
        {
            if (!string.IsNullOrEmpty(xml)) {
                Data = CoreHelper.DeserializeFromString<EY.com.eBook.RuleEngine.AY2013.DBIAppTypes.DBIWorksheet>(xml);
            }
            SetMinimal();
            
        }
        
        public EY.com.eBook.RuleEngine.AY2013.DBIAppTypes.DBIWorksheet Data { get; set; }

        public void ForceRecalculation(ref WorksheetSetData worksheetset)
        {
            Data.LastUpdated = null;
           

            Calculate(ref worksheetset);
        }

        public void Calculate(ref WorksheetSetData worksheetset)
        {
            if (!Data.LastUpdated.HasValue) Data.LastUpdated = new DateTime(1980, 1, 1);
            worksheetset.ClearMessages(TypeId);

            Data.DBI.DBIAbeNettoTotaal = Data.DBI.DBIAbeNettoBEI.GetValueOrDefault() + Data.DBI.DBIAbeNettoBUI.GetValueOrDefault();
            Data.DBI.DBIAbeRoerendTotaal = Data.DBI.DBIAbeRoerendBEI.GetValueOrDefault() + Data.DBI.DBIAbeRoerendBUI.GetValueOrDefault();
            Data.DBI.DBIAbuNettoTotaal = Data.DBI.DBIAbuNettoBEI.GetValueOrDefault() + Data.DBI.DBIAbuNettoBUI.GetValueOrDefault();
            Data.DBI.DBIAbuRoerendTotaal = Data.DBI.DBIAbuRoerendBEI.GetValueOrDefault() + Data.DBI.DBIAbuRoerendBUI.GetValueOrDefault();
            Data.DBI.DBIAAbeNettoTotaal = Data.DBI.DBIAAbeNettoBEI.GetValueOrDefault() + Data.DBI.DBIAAbeNettoBUI.GetValueOrDefault();
            Data.DBI.DBIAAbeRoerendTotaal = Data.DBI.DBIAAbeRoerendBEI.GetValueOrDefault() + Data.DBI.DBIAAbeRoerendBUI.GetValueOrDefault();
            Data.DBI.DBIAAbuNettoTotaal = Data.DBI.DBIAAbuNettoBEI.GetValueOrDefault() + Data.DBI.DBIAAbuNettoBUI.GetValueOrDefault();
            Data.DBI.DBIAAbuRoerendTotaal = Data.DBI.DBIAAbuRoerendBEI.GetValueOrDefault() + Data.DBI.DBIAAbuRoerendBUI.GetValueOrDefault();
            Data.DBI.DBIVrijgestelRoerendTotaal = Data.DBI.DBIVrijgestelRoerendBEI.GetValueOrDefault() + Data.DBI.DBIVrijgestelRoerendBUI.GetValueOrDefault();
            Data.DBI.DBISubtotaalBEI = (((((((Data.DBI.DBIAbeNettoBEI.GetValueOrDefault() + Data.DBI.DBIAbeRoerendBEI.GetValueOrDefault()) + Data.DBI.DBIAbuNettoBEI.GetValueOrDefault()) + Data.DBI.DBIAbuRoerendBEI.GetValueOrDefault()) + Data.DBI.DBIAAbeNettoBEI.GetValueOrDefault()) + Data.DBI.DBIAAbeRoerendBEI.GetValueOrDefault()) + Data.DBI.DBIAAbuNettoBEI.GetValueOrDefault()) + Data.DBI.DBIAAbuRoerendBEI.GetValueOrDefault()) + Data.DBI.DBIVrijgestelRoerendBEI.GetValueOrDefault();
            Data.DBI.DBISubtotaalBUI = (((((((Data.DBI.DBIAbeNettoBUI.GetValueOrDefault() + Data.DBI.DBIAbeRoerendBUI.GetValueOrDefault()) + Data.DBI.DBIAbuNettoBUI.GetValueOrDefault()) + Data.DBI.DBIAbuRoerendBUI.GetValueOrDefault()) + Data.DBI.DBIAAbeNettoBUI.GetValueOrDefault()) + Data.DBI.DBIAAbeRoerendBUI.GetValueOrDefault()) + Data.DBI.DBIAAbuNettoBUI.GetValueOrDefault()) + Data.DBI.DBIAAbuRoerendBUI.GetValueOrDefault()) + Data.DBI.DBIVrijgestelRoerendBUI.GetValueOrDefault();

            Data.DBI.DBISubtotaalTotaal = Data.DBI.DBISubtotaalBEI.GetValueOrDefault() + Data.DBI.DBISubtotaalBUI.GetValueOrDefault();
            Data.DBI.DBIKostenBEI = Math.Round((Data.DBI.DBISubtotaalBEI.GetValueOrDefault() / 100) * 5, 2, MidpointRounding.AwayFromZero);
            Data.DBI.DBIKostenBUI = Math.Round((Data.DBI.DBISubtotaalBUI.GetValueOrDefault() / 100) * 5, 2, MidpointRounding.AwayFromZero);
            Data.DBI.DBIKostenTotaal = Data.DBI.DBIKostenBEI.GetValueOrDefault() + Data.DBI.DBIKostenBUI.GetValueOrDefault();
            Data.DBI.DBIVerschilBEI = Data.DBI.DBISubtotaalBEI.GetValueOrDefault() - Data.DBI.DBIKostenBEI.GetValueOrDefault();
            Data.DBI.DBIVerschilBUI = Data.DBI.DBISubtotaalBUI.GetValueOrDefault() - Data.DBI.DBIKostenBUI.GetValueOrDefault();
            Data.DBI.DBIVerschilTotaal = Data.DBI.DBIVerschilBEI.GetValueOrDefault() + Data.DBI.DBIVerschilBUI.GetValueOrDefault();
            Data.DBI.DBIHerfinancTotaal = Data.DBI.DBIHerfinancBEI.GetValueOrDefault() + Data.DBI.DBIHerfinancBUI.GetValueOrDefault();
            Data.DBI.DBILidEuTotaal = Data.DBI.DBILidEuBEI.GetValueOrDefault() + Data.DBI.DBILidEuBUI.GetValueOrDefault();
            Data.DBI.DBITotaalBEI = (Data.DBI.DBIVerschilBEI.GetValueOrDefault() + Data.DBI.DBILidEuBEI.GetValueOrDefault()) + Data.DBI.DBIHerfinancBEI.GetValueOrDefault();
            Data.DBI.DBITotaalBUI = (Data.DBI.DBIVerschilBUI.GetValueOrDefault() + Data.DBI.DBILidEuBUI.GetValueOrDefault()) + Data.DBI.DBIHerfinancBUI.GetValueOrDefault();
            Data.DBI.DBITotaal = Data.DBI.DBITotaalBEI.GetValueOrDefault() + Data.DBI.DBITotaalBUI.GetValueOrDefault();


            // aanwending
            decimal rest = worksheetset.BepalingBelastbareBasisApp.Data.FiscaleAftrek.ResterendBasisVoorAftrek.GetValueOrDefault();

            Data.DBI.BelastbareBasis = rest;
            Data.DBI.BelastbareBasisResterend = rest;

            Data.Historiek = Data.Historiek.OrderBy(h => h.Aanslagjaar.Id).ToList();

            Data.DBI.Aanwending = 0;
            Data.DBI.SaldoStart = Data.DBI.DBITotaal.GetValueOrDefault();
            Data.DBI.SaldoEinde = Data.DBI.DBITotaal.GetValueOrDefault();
            Data.DBI.Nieuwe = 0;

            int assess = worksheetset.DataContract.AssessmentYear;

            DBIAppTypes.HistoryItem current = Data.Historiek.FirstOrDefault(h => h.Aanslagjaar.Id == assess.ToString());
            if (current == null)
            {
                current = new EY.com.eBook.RuleEngine.AY2013.DBIAppTypes.HistoryItem
                {
                    Aanslagjaar = new StringListItemDataContract
                    {
                        Id = assess.ToString()
                        ,
                        FR = assess.ToString()
                        ,
                        NL = assess.ToString()
                        ,
                        EN = assess.ToString()
                    }
                    ,
                    GebruiktBJ = 0
                    ,
                    Id = Guid.NewGuid()
                    ,
                    IsValid = true
                    ,
                    LastUpdated = DateTime.Now
                    ,
                    PreviousImported = false
                    ,
                    SaldoStart = Data.DBI.SaldoStart.GetValueOrDefault()
                    ,
                    SaldoEinde = Data.DBI.SaldoEinde.GetValueOrDefault()
                    , NietOverdraagbaarDeel=0
                };
                Data.Historiek.Add(current);
            }
            else
            {
                current.SaldoStart = Data.DBI.SaldoStart.GetValueOrDefault();
                current.SaldoEinde = Data.DBI.SaldoStart.GetValueOrDefault();
            }
            

            if (rest > 0)
            {
                foreach (DBIAppTypes.HistoryItem history in Data.Historiek)
                {
                    if (history.SaldoStart > 0 && Data.DBI.BelastbareBasisResterend > 0)
                    {
                        if (history.SaldoStart < Data.DBI.BelastbareBasisResterend)
                        {
                            history.GebruiktBJ = history.SaldoStart.GetValueOrDefault();
                        }
                        else
                        {
                            history.GebruiktBJ = Data.DBI.BelastbareBasisResterend.GetValueOrDefault();
                        }
                        Data.DBI.BelastbareBasisResterend = Data.DBI.BelastbareBasisResterend.GetValueOrDefault() - history.GebruiktBJ.GetValueOrDefault();
                    }
                    else
                    {
                        history.GebruiktBJ = 0;
                    }
                    history.SaldoEinde = history.SaldoStart.GetValueOrDefault() - history.GebruiktBJ.GetValueOrDefault();
                    if (history.Aanslagjaar != null 
                        && int.Parse(history.Aanslagjaar.Id) == worksheetset.DataContract.AssessmentYear
                        && history.NietOverdraagbaarDeel.GetValueOrDefault() > 0 
                        && history.NietOverdraagbaarDeel.GetValueOrDefault() > history.SaldoEinde.GetValueOrDefault())
                    {
                        history.NietOverdraagbaarDeel = history.SaldoEinde.GetValueOrDefault();
                        worksheetset.SetMessage(TypeId, MessageTypeDataContract.Warning, "ALTERED_NONTRANSFERABLEPART", "NietOverdraagbaarDeel");
                    }
                }

                Data.DBI.Aanwending = Data.DBI.BelastbareBasis.GetValueOrDefault() - Data.DBI.BelastbareBasisResterend.GetValueOrDefault();
                Data.DBI.SaldoStart = current.SaldoStart;
                Data.DBI.Nieuwe = current.GebruiktBJ;
                Data.DBI.SaldoEinde = current.SaldoEinde;

            }

            Data.LastUpdated = DateTime.Now;
           // Data.LastCalculated = DateTime.Now;
        }

        #region AddUpdateDelete

        public void AddToParticipations(EY.com.eBook.RuleEngine.AY2013.DBIAppTypes.ParticipatieItem entity, ref WorksheetSetData worksheetset)
        {
            entity = Validate(entity, "Participations", ref worksheetset);
            if (entity.IsValid)
            {
                Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
                Data.Participations.Add(entity);
            }
        }

        public void DeleteParticipations(Guid rowId, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2013.DBIAppTypes.ParticipatieItem original = Data.Participations.FirstOrDefault(e => e.Id == rowId);
            if (original != null)
            {
                /*
                if (original.Boeken.HasValue && original.Boeken.Value)
                {
                    worksheetset.DeleteBooking(FacturatieApp.TypeId, "OpTeMakenCN", original.Id, null);
                    // DELETE BOOKING
                }*/
                Data.Participations.Remove(original);
                Data.LastUpdated = DateTime.Now;
            }
        }

        public void UpdateParticipations(EY.com.eBook.RuleEngine.AY2013.DBIAppTypes.ParticipatieItem entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2013.DBIAppTypes.ParticipatieItem original = Data.Participations.FirstOrDefault(e => e.Id == entity.Id);
            if (original == null)
            {
                AddToParticipations(entity, ref worksheetset);
            }
            else
            {
                entity = Validate(entity, "Participations", ref worksheetset);
                if (entity.IsValid)
                {
                    int idx = Data.Participations.IndexOf(original);
                    Data.Participations.RemoveAt(idx);
                    Data.Participations.Insert(idx, entity);
                    Data.LastUpdated = DateTime.Now;
                    entity.LastUpdated = DateTime.Now;
                }
            }
        }

        public void AddToDividends(EY.com.eBook.RuleEngine.AY2013.DBIAppTypes.DividendItem entity, ref WorksheetSetData worksheetset)
        {
            entity = Validate(entity, "Dividends", ref worksheetset);
            if (entity.IsValid)
            {
                Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
                Data.Dividends.Add(entity);
            }
        }

        public void DeleteDividends(Guid rowId, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2013.DBIAppTypes.DividendItem original = Data.Dividends.FirstOrDefault(e => e.Id == rowId);
            if (original != null)
            {
                /*
                if (original.Boeken.HasValue && original.Boeken.Value)
                {
                    worksheetset.DeleteBooking(FacturatieApp.TypeId, "OpTeMakenCN", original.Id, null);
                    // DELETE BOOKING
                }*/
                Data.Dividends.Remove(original);
                Data.LastUpdated = DateTime.Now;
            }
        }

        public void UpdateDividends(EY.com.eBook.RuleEngine.AY2013.DBIAppTypes.DividendItem entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2013.DBIAppTypes.DividendItem original = Data.Dividends.FirstOrDefault(e => e.Id == entity.Id);
            if (original == null)
            {
                AddToDividends(entity, ref worksheetset);
            }
            else
            {
                entity = Validate(entity, "Dividends", ref worksheetset);
                if (entity.IsValid)
                {
                    int idx = Data.Dividends.IndexOf(original);
                    Data.Dividends.RemoveAt(idx);
                    Data.Dividends.Insert(idx, entity);
                    Data.LastUpdated = DateTime.Now;
                    entity.LastUpdated = DateTime.Now;
                }
            }
        }

        public void UpdateDBI(EY.com.eBook.RuleEngine.AY2013.DBIAppTypes.DbiEntity entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2013.DBIAppTypes.DbiEntity original = Data.DBI;

            entity = Validate(entity, "DBI", ref worksheetset);
            if (entity.IsValid)
            {

                Data.DBI = entity;
                Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
            }
        }

        public void AddToHistoriek(EY.com.eBook.RuleEngine.AY2013.DBIAppTypes.HistoryItem entity, ref WorksheetSetData worksheetset)
        {
            entity = Validate(entity, "Historiek", ref worksheetset);
            if (entity.IsValid)
            {
                Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
                Data.Historiek.Add(entity);
            }
        }

        public void DeleteHistoriek(Guid rowId, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2013.DBIAppTypes.HistoryItem original = Data.Historiek.FirstOrDefault(e => e.Id == rowId);
            if (original != null)
            {
                /*
                if (original.Boeken.HasValue && original.Boeken.Value)
                {
                    worksheetset.DeleteBooking(FacturatieApp.TypeId, "OpTeMakenCN", original.Id, null);
                    // DELETE BOOKING
                }*/
                Data.Historiek.Remove(original);
                Data.LastUpdated = DateTime.Now;
            }
        }

        public void UpdateHistoriek(EY.com.eBook.RuleEngine.AY2013.DBIAppTypes.HistoryItem entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2013.DBIAppTypes.HistoryItem original = Data.Historiek.FirstOrDefault(e => e.Id == entity.Id);
            if (original == null)
            {
                AddToHistoriek(entity, ref worksheetset);
            }
            else
            {
                entity = Validate(entity, "Historiek", ref worksheetset);
                if (entity.IsValid)
                {
                    int idx = Data.Historiek.IndexOf(original);
                    Data.Historiek.RemoveAt(idx);
                    Data.Historiek.Insert(idx, entity);
                    Data.LastUpdated = DateTime.Now;
                    entity.LastUpdated = DateTime.Now;
                }
            }
        }

        #endregion

        #region VALIDATION

        public EY.com.eBook.RuleEngine.AY2013.DBIAppTypes.ParticipatieItem Validate(EY.com.eBook.RuleEngine.AY2013.DBIAppTypes.ParticipatieItem entity, string location, ref WorksheetSetData worksheetset)
        {
            entity.IsValid = true;
            worksheetset.ClearMessages(TypeId, entity.Id);

            entity.AcquisitionAmount = Math.Abs(entity.AcquisitionAmount.GetValueOrDefault());
            if (string.IsNullOrEmpty(entity.Name))
            {
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Name");
                entity.IsValid = false;
            }
            if (entity.Country==null)
            {
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Country");
                entity.IsValid = false;
            }
            if (!entity.DateBought.HasValue)
            {
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "DateBought");
                entity.IsValid = false;
            }
            if (entity.AcquisitionAmount.GetValueOrDefault()==0)
            {
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "AcquisitionAmount");
                entity.IsValid = false;
            }
            if (entity.AccountParticipation==null)
            {
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "AccountParticipation");
                entity.IsValid = false;
            }
            if (entity.PercentageAcquired.GetValueOrDefault() <0 || entity.PercentageAcquired.GetValueOrDefault()>100)
            {
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "VALID", location, entity.Id, "PercentageAcquired");
                entity.IsValid = false;
            }
            if (entity.DateBought.HasValue && entity.DateBought.Value > worksheetset.DataContract.EndDate)
            {
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "OLDERTHENEND", location, entity.Id, "DateBought");
                entity.IsValid = false;
            }

            if (entity.IsValid)
            {
                AccountDataContract adc = worksheetset.GetAccount(entity.AccountParticipation.Id, false, false, worksheetset.DataContract.Culture);
                decimal amountReserved = Data.Participations.Where(p => p.AccountParticipation != null && p.AccountParticipation.Id == entity.AccountParticipation.Id && p.Id!=entity.Id).Sum(a => a.AcquisitionAmount.GetValueOrDefault());
                decimal amountAvailable = adc.Saldo - amountReserved;
                if (Math.Abs(adc.Saldo) < Math.Abs(amountReserved) + entity.AcquisitionAmount.GetValueOrDefault())
                {
                    CultureInfo ci = CultureInfo.CreateSpecificCulture("nl-BE");
                    string key = "";
                    List<object> values = new List<object>();
                    values.Add(entity.AccountParticipation.NL);
                    if (amountReserved > 0)
                    {
                        key = "TOOMUCH";

                        values.Add(amountReserved.ToString("C", ci.NumberFormat));
                        values.Add(entity.AcquisitionAmount.GetValueOrDefault().ToString("C", ci.NumberFormat));
                        values.Add(amountAvailable.ToString("C", ci.NumberFormat));
                    }
                    else
                    {
                        key = "TOOMUCHSALDI";
                        values.Add(entity.AcquisitionAmount.GetValueOrDefault().ToString("C", ci.NumberFormat));
                        values.Add(adc.Saldo.ToString("C", ci.NumberFormat));
                    }
                    worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, key, location, entity.Id, "AcquisitionAmount",values.ToArray());
                    entity.IsValid = false;
                }

            }

            return entity;
        }

        public EY.com.eBook.RuleEngine.AY2013.DBIAppTypes.DividendItem Validate(EY.com.eBook.RuleEngine.AY2013.DBIAppTypes.DividendItem entity, string location, ref WorksheetSetData worksheetset)
        {
            entity.IsValid = true;
            worksheetset.ClearMessages(TypeId, entity.Id);

            if (entity.Participatie == null)
            {
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Participatie");
                entity.IsValid = false;
            }
            if (!entity.DateDividend.HasValue)
            {
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "DateDividend");
                entity.IsValid = false;
            }
            else
            {
                if (entity.DateDividend.Value < worksheetset.DataContract.StartDate || entity.DateDividend.Value>worksheetset.DataContract.EndDate)
                {
                    worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "NOTINBOOKYEAR", location, entity.Id, "DateDividend");
                    entity.IsValid = false;
                }

                if (entity.IsValid)
                {
                    DBIAppTypes.ParticipatieItem pi = Data.Participations.FirstOrDefault(p => p.Id.ToString().ToLower() == entity.Participatie.Id.ToLower());
                    if (pi == null)
                    {
                        worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Participatie");
                        entity.IsValid = false;
                    }
                    else
                    {
                        if (pi.DateBought.HasValue && pi.DateBought.Value > entity.DateDividend.Value)
                        {
                            worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "BEFOREDATEBOUGHT", location, entity.Id, "DateDividend");
                            entity.IsValid = false;
                        }
                        else if(pi.DateBought.HasValue)
                        {
                            entity.Perc95 = 0;
                            decimal bought = pi.AcquisitionAmount.GetValueOrDefault();
                            if (((entity.DateDividend.Value - pi.DateBought.Value).Days >= 365 || pi.overrule == true) && entity.Taxation == true
                                    && (pi.PercentageAcquired.GetValueOrDefault() >= 10 || bought >= 1200000))
                            {
                                entity.Perc95 = Math.Round(entity.Bruto.GetValueOrDefault() * (decimal)0.95, 2, MidpointRounding.AwayFromZero);
                                if (pi.overrule == true)
                                {
                                    worksheetset.SetMessage(TypeId, MessageTypeDataContract.Warning, "OVERRULE", location, entity.Id, null, new object[] { pi.Name });
                                }
                            }
                            else
                            {
                                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Warning, "NOTILLEGILBE", location, entity.Id);
                                if (((entity.DateDividend.Value - pi.DateBought.Value).Days < 365))
                                {
                                    worksheetset.SetMessage(TypeId, MessageTypeDataContract.Warning, "YOUNGER", location, entity.Id, null, new object[] { pi.Name });
                                }
                                if (!(entity.Taxation == true))
                                {
                                    worksheetset.SetMessage(TypeId, MessageTypeDataContract.Warning, "NOTAX", location, entity.Id, null, new object[] { pi.Name });
                                }
                                if (pi.PercentageAcquired.GetValueOrDefault() < 10)
                                {
                                    worksheetset.SetMessage(TypeId, MessageTypeDataContract.Warning, "PERCENTAGESMALLER", location, entity.Id, null, new object[] { pi.Name });
                                }
                                if (bought <1200000)
                                {
                                    worksheetset.SetMessage(TypeId, MessageTypeDataContract.Warning, "LESSDIVIDEND", location, entity.Id, null, new object[] { pi.Name });
                                }
                            }
                            entity.Bedrag = entity.Bruto.GetValueOrDefault() - entity.Perc95.GetValueOrDefault();
                            if (entity.Forex == true)
                            {
                                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Warning, "FOREX", location, entity.Id);
                            }
                        }

                    }
                }
            }

            return entity;
        }

        public EY.com.eBook.RuleEngine.AY2013.DBIAppTypes.DbiEntity Validate(EY.com.eBook.RuleEngine.AY2013.DBIAppTypes.DbiEntity entity, string location, ref WorksheetSetData worksheetset)
        {
            entity.IsValid = true;
            worksheetset.ClearMessages(TypeId, entity.Id);
            /*
            if (!entity.Bedrag.HasValue || entity.Bedrag.Value==0)
            {
            worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Grootboek");
            entity.IsValid = false;
            }
            */

            return entity;
        }

        public EY.com.eBook.RuleEngine.AY2013.DBIAppTypes.HistoryItem Validate(EY.com.eBook.RuleEngine.AY2013.DBIAppTypes.HistoryItem entity, string location, ref WorksheetSetData worksheetset)
        {
            entity.IsValid = true;
            worksheetset.ClearMessages(TypeId, entity.Id);

            if (entity.Aanslagjaar == null)
            {
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Aanslagjaar");
                entity.IsValid = false;
            }
            

            return entity;
        }

        #endregion


        public void ImportPrevious(ref WorksheetSetData wsd)
        {
            if (wsd.DataContract.PreviousEndDate.HasValue)
            {
                if (wsd.PreviousSet.AssessmentYear == 2012)
                {
                    EY.com.eBook.RuleEngine.AY2012.WorksheetSetData pset = (EY.com.eBook.RuleEngine.AY2012.WorksheetSetData)wsd.PreviousSet;
                    EY.com.eBook.RuleEngine.AY2012.DBIApp previous = pset.DBIApp;

                    int prevAssess = wsd.DataContract.PreviousEndDate.Value.AddDays(1).Year;

                    Data.Historiek.RemoveAll(h => h.PreviousImported == true);

                    bool includesprev = false;

                    foreach (AY2012.DBIAppTypes.HistoryItem prevhistory in previous.Data.Historiek.Where(h => h.Aanslagjaar != null))
                    {
                        if (prevhistory.Aanslagjaar.Id == prevAssess.ToString()) includesprev = true;
                        if (prevhistory.SaldoEinde > 0)
                        {
                            Data.Historiek.Add(new AY2013.DBIAppTypes.HistoryItem
                            {
                                Id = prevhistory.Id
                                ,
                                Aanslagjaar = prevhistory.Aanslagjaar
                                ,
                                GebruiktBJ = 0
                                ,
                                IsValid = true
                                ,
                                LastUpdated = new DateTime(1980, 1, 1)
                                ,
                                PreviousImported = true
                                ,
                                SaldoStart = prevhistory.SaldoEinde
                                ,
                                SaldoEinde = prevhistory.SaldoEinde
                                //,
                                //NietOverdraagbaarDeel = prevhistory.NietOverdraagbaarDeel ->2013
                            });
                        }
                    }

                    if (!includesprev)
                    {
                        if (previous.Data.DBI.SaldoEinde > 0)
                        {
                            Data.Historiek.Add(new AY2013.DBIAppTypes.HistoryItem
                            {
                                Id = Guid.NewGuid()
                                ,
                                Aanslagjaar = new StringListItemDataContract
                                {
                                    Id = prevAssess.ToString()
                                    ,
                                    EN = prevAssess.ToString()
                                    ,
                                    NL = prevAssess.ToString()
                                    ,
                                    FR = prevAssess.ToString()
                                }
                                ,
                                GebruiktBJ = 0
                                ,
                                IsValid = true
                                ,
                                LastUpdated = new DateTime(1980, 1, 1)
                                ,
                                PreviousImported = true
                                ,
                                SaldoEinde = previous.Data.DBI.SaldoEinde
                                ,
                                SaldoStart = previous.Data.DBI.SaldoEinde
                            });
                        }
                    }


                }
                if (wsd.PreviousSet.AssessmentYear == 2013)
                {
                    EY.com.eBook.RuleEngine.AY2013.WorksheetSetData pset = (EY.com.eBook.RuleEngine.AY2013.WorksheetSetData)wsd.PreviousSet;
                    EY.com.eBook.RuleEngine.AY2013.DBIApp previous = pset.DBIApp;

                    int prevAssess = wsd.DataContract.PreviousEndDate.Value.AddDays(1).Year;

                    Data.Historiek.RemoveAll(h => h.PreviousImported == true);

                    bool includesprev = false;

                    foreach (AY2013.DBIAppTypes.HistoryItem prevhistory in previous.Data.Historiek.Where(h => h.Aanslagjaar != null))
                    {
                        if (prevhistory.Aanslagjaar.Id == prevAssess.ToString()) includesprev = true;
                        if (prevhistory.SaldoEinde > 0)
                        {
                            Data.Historiek.Add(new AY2013.DBIAppTypes.HistoryItem
                            {
                                Id = prevhistory.Id
                                ,
                                Aanslagjaar = prevhistory.Aanslagjaar
                                ,
                                GebruiktBJ = 0
                                ,
                                IsValid = true
                                ,
                                LastUpdated = new DateTime(1980, 1, 1)
                                ,
                                PreviousImported = true
                                ,
                                SaldoStart = prevhistory.SaldoEinde
                                ,
                                SaldoEinde = prevhistory.SaldoEinde
                                //,
                                //NietOverdraagbaarDeel = prevhistory.NietOverdraagbaarDeel ->2013
                            });
                        }
                    }

                    if (!includesprev)
                    {
                        if (previous.Data.DBI.SaldoEinde > 0)
                        {
                            Data.Historiek.Add(new AY2013.DBIAppTypes.HistoryItem
                            {
                                Id = Guid.NewGuid()
                                ,
                                Aanslagjaar = new StringListItemDataContract
                                {
                                    Id = prevAssess.ToString()
                                    ,
                                    EN = prevAssess.ToString()
                                    ,
                                    NL = prevAssess.ToString()
                                    ,
                                    FR = prevAssess.ToString()
                                }
                                ,
                                GebruiktBJ = 0
                                ,
                                IsValid = true
                                ,
                                LastUpdated = new DateTime(1980, 1, 1)
                                ,
                                PreviousImported = true
                                ,
                                SaldoEinde = previous.Data.DBI.SaldoEinde
                                ,
                                SaldoStart = previous.Data.DBI.SaldoEinde
                            });
                        }
                    }


                }
            }
            
        }
        // VALIDATION
        
        // CALCULATION
        
        // BOOKINGS
        
        // IMPORT PREVIOUS



    }
}
  