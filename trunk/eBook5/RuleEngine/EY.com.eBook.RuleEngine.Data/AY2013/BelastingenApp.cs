
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.RuleEngine.Core;
using EY.com.eBook.RuleEngine.Core.Interfaces;
using EY.com.eBook.RuleEngine.Data;
using EY.com.eBook.Core;
using EY.com.eBook.API.Contracts.Data.Cache;
using System.Reflection;

namespace EY.com.eBook.RuleEngine.AY2013
{

    public class BelastingenApp
    {
        public static WorksheetTypeSettings Settings = new WorksheetTypeSettings
        {
            Clients = true
          ,
            Suppliers = true
          ,
            AllAccounts = true
          ,
            PerformsBookings = true
        };

        public static Guid TypeId = new Guid("10eea163-5e43-4501-9f1c-35ae838be615");

        public List<string> ImportablePreviousTypes = new List<string>() { "EY.com.eBook.RuleEngine.AY2012.FacturatieApp" };

        public Guid GetMyType() { return TypeId; }

        public void SetMinimal()
        {
            if (Data == null)
            {
                Data = new EY.com.eBook.RuleEngine.AY2013.BelastingenAppTypes.WPBelastingen();
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.Adjustments == null)
            {
                Data.Adjustments = new List<EY.com.eBook.RuleEngine.AY2013.BelastingenAppTypes.Adjustment>(); ;
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.Belastingen == null)
            {
                Data.Belastingen = new List<EY.com.eBook.RuleEngine.AY2013.BelastingenAppTypes.Belasting>(); ;
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.GewestBelasting == null)
            {
                Data.GewestBelasting = new List<EY.com.eBook.RuleEngine.AY2013.BelastingenAppTypes.KostItem>(); ;
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.Boetes == null)
            {
                Data.Boetes = new List<EY.com.eBook.RuleEngine.AY2013.BelastingenAppTypes.KostItem>(); ;
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.Pensioenen == null)
            {
                Data.Pensioenen = new List<EY.com.eBook.RuleEngine.AY2013.BelastingenAppTypes.KostItem>(); ;
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.Auto == null)
            {
                Data.Auto = new List<EY.com.eBook.RuleEngine.AY2013.BelastingenAppTypes.KostItem>(); ;
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.Receptie == null)
            {
                Data.Receptie = new List<EY.com.eBook.RuleEngine.AY2013.BelastingenAppTypes.KostItem>(); ;
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.Restaurant == null)
            {
                Data.Restaurant = new List<EY.com.eBook.RuleEngine.AY2013.BelastingenAppTypes.KostItem>(); ;
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.Kledij == null)
            {
                Data.Kledij = new List<EY.com.eBook.RuleEngine.AY2013.BelastingenAppTypes.KostItem>(); ;
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.OverdrevenIntresten == null)
            {
                Data.OverdrevenIntresten = new List<EY.com.eBook.RuleEngine.AY2013.BelastingenAppTypes.KostItem>(); ;
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.IntrestenLeningen == null)
            {
                Data.IntrestenLeningen = new List<EY.com.eBook.RuleEngine.AY2013.BelastingenAppTypes.KostItem>(); ;
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.AbnormaleGoedgunstigeVoordelen == null)
            {
                Data.AbnormaleGoedgunstigeVoordelen = new List<EY.com.eBook.RuleEngine.AY2013.BelastingenAppTypes.KostItem>(); ;
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.SocialeVoordelen == null)
            {
                Data.SocialeVoordelen = new List<EY.com.eBook.RuleEngine.AY2013.BelastingenAppTypes.KostItem>(); ;
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.MaaltijdSportCultuurEco == null)
            {
                Data.MaaltijdSportCultuurEco = new List<EY.com.eBook.RuleEngine.AY2013.BelastingenAppTypes.KostItem>(); ;
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.Liberaliteiten == null)
            {
                Data.Liberaliteiten = new List<EY.com.eBook.RuleEngine.AY2013.BelastingenAppTypes.KostItem>(); ;
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.WaardeverminderingAandelen == null)
            {
                Data.WaardeverminderingAandelen = new List<EY.com.eBook.RuleEngine.AY2013.BelastingenAppTypes.KostItem>(); ;
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.TerugnemingVroegereVrijstellingen == null)
            {
                Data.TerugnemingVroegereVrijstellingen = new List<EY.com.eBook.RuleEngine.AY2013.BelastingenAppTypes.KostItem>(); ;
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.Werknemersparticipatie == null)
            {
                Data.Werknemersparticipatie = new List<EY.com.eBook.RuleEngine.AY2013.BelastingenAppTypes.KostItem>(); ;
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.VergoedingCoupon == null)
            {
                Data.VergoedingCoupon = new List<EY.com.eBook.RuleEngine.AY2013.BelastingenAppTypes.KostItem>(); ;
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.KostenTaxShelter == null)
            {
                Data.KostenTaxShelter = new List<EY.com.eBook.RuleEngine.AY2013.BelastingenAppTypes.KostItem>(); ;
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.GewestPremies == null)
            {
                Data.GewestPremies = new List<EY.com.eBook.RuleEngine.AY2013.BelastingenAppTypes.KostItem>(); ;
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.BepaaldeStaten == null)
            {
                Data.BepaaldeStaten = new List<EY.com.eBook.RuleEngine.AY2013.BelastingenAppTypes.KostItem>(); ;
                Data.LastUpdated = DateTime.Now;
            }

            if (Data.Andere == null)
            {
                Data.Andere = new List<EY.com.eBook.RuleEngine.AY2013.BelastingenAppTypes.KostItem>(); ;
                Data.LastUpdated = DateTime.Now;
            }

        }

        public string ToXml()
        {
            return CoreHelper.SerializeToString(Data);
        }

        public void LoadXml(string xml)
        {
            if (!string.IsNullOrEmpty(xml))
            {
                Data = CoreHelper.DeserializeFromString<EY.com.eBook.RuleEngine.AY2013.BelastingenAppTypes.WPBelastingen>(xml);
            }
            SetMinimal();

        }

        public EY.com.eBook.RuleEngine.AY2013.BelastingenAppTypes.WPBelastingen Data { get; set; }

        public void ForceRecalculation(ref WorksheetSetData worksheetset)
        {
            Data.LastUpdated = null;
            Data.AbnormaleGoedgunstigeVoordelen.Clear();
            Data.Andere.Clear();
            Data.Auto.Clear();
            Data.Belastingen.Where(b => b.UNID.IsGuid() && !b.AdjustmentId.HasValue).ToList().ForEach(b => b.AdjustmentId = new Guid(b.UNID));
            Data.BepaaldeStaten.Clear();
            Data.Boetes.Clear();
            Data.GewestBelasting.Clear();
            Data.GewestPremies.Clear();
            Data.IntrestenLeningen.Clear();
            Data.Kledij.Clear();
            Data.KostenTaxShelter.Clear();
            Data.Liberaliteiten.Clear();
            Data.MaaltijdSportCultuurEco.Clear();
            Data.OverdrevenIntresten.Clear();
            Data.Pensioenen.Clear();
            Data.Receptie.Clear();
            Data.Restaurant.Clear();
            Data.SocialeVoordelen.Clear();
            Data.TerugnemingVroegereVrijstellingen.Clear();
            Data.VergoedingCoupon.Clear();
            Data.WaardeverminderingAandelen.Clear();
            Data.Werknemersparticipatie.Clear();

            Calculate(ref worksheetset);

            for (int i = 0; i < Data.Adjustments.Count; i++)
            {
                UpdateAdjustments(Data.Adjustments[i], ref worksheetset);
            }


        }


        public void Calculate(ref WorksheetSetData worksheetset)
        {
            if (!Data.LastUpdated.HasValue) Data.LastUpdated = new DateTime(1980, 1, 1);
            worksheetset.ClearMessages(TypeId);
            MappedBelastingen(ref worksheetset);
            MappedGewestBelastingen(ref worksheetset);
            MappedBoetes(ref worksheetset);
            MappedPensioenen(ref worksheetset);
            MappedAuto(ref worksheetset);
            MappedAutoVAA(ref worksheetset);
            MappedReceptie(ref worksheetset);
            MappedRestaurant(ref worksheetset);
            MappedKledij(ref worksheetset);
            MappedOverdrevenIntresten(ref worksheetset);
            MappedIntrestLening(ref worksheetset);
            MappedAbnormaal(ref worksheetset);
            MappedSocialeVoordelen(ref worksheetset);
            MappedMaaltijdSportCultuurEco(ref worksheetset);
            MappedLiberaliteiten(ref worksheetset);
            MappedWaardeverminderingAandelen(ref worksheetset);
            MappedTerugnemingVroegereVrijstellingen(ref worksheetset);
            MappedWerknemersparticipatie(ref worksheetset);
            MappedVergoedingCoupon(ref worksheetset);
            MappedKostenTaxShelter(ref worksheetset);
            MappedGewestPremies(ref worksheetset);
            MappedBepaaldeStaten(ref worksheetset);
            MappedAndere(ref worksheetset);

            RegulateCars(ref worksheetset);
            ImportFromVAA(ref worksheetset);
            RegulateTaxes(ref worksheetset);

            CheckNegatives(ref worksheetset);

            Data.LastUpdated = DateTime.Now;
        }

        private void CheckNegatives(ref WorksheetSetData worksheetset)
        {
            SetMinimal();
            worksheetset.ClearMessages(TypeId, "NEGATIVE");
            if (Data.AbnormaleGoedgunstigeVoordelen.Count > 0 && Data.AbnormaleGoedgunstigeVoordelen.Sum(e => e.Bedrag.GetValueOrDefault()) < 0)
            {
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "NEGATIVE", "AbnormaleGoedgunstigeVoordelen");
            }
            if (Data.Andere.Count > 0 && Data.Andere.Sum(e => e.Bedrag.GetValueOrDefault()) < 0)
            {
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "NEGATIVE", "Andere");
            }
            if (Data.BepaaldeStaten.Count > 0 && Data.BepaaldeStaten.Sum(e => e.Bedrag.GetValueOrDefault()) < 0)
            {
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "NEGATIVE", "BepaaldeStaten");
            }
            if (Data.Boetes.Count > 0 && Data.Boetes.Sum(e => e.Bedrag.GetValueOrDefault()) < 0)
            {
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "NEGATIVE", "Boetes");
            }
            if (Data.GewestBelasting.Count > 0 && Data.GewestBelasting.Sum(e => e.Bedrag.GetValueOrDefault()) < 0)
            {
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "NEGATIVE", "GewestBelasting");
            }
            if (Data.GewestPremies.Count > 0 && Data.GewestPremies.Sum(e => e.Bedrag.GetValueOrDefault()) < 0)
            {
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "NEGATIVE", "GewestPremies");
            }
            if (Data.IntrestenLeningen.Count > 0 && Data.IntrestenLeningen.Sum(e => e.Bedrag.GetValueOrDefault()) < 0)
            {
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "NEGATIVE", "IntrestenLeningen");
            }
            if (Data.Kledij.Count > 0 && Data.Kledij.Sum(e => e.Bedrag.GetValueOrDefault()) < 0)
            {
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "NEGATIVE", "Kledij");
            }
            if (Data.KostenTaxShelter.Count > 0 && Data.KostenTaxShelter.Sum(e => e.Bedrag.GetValueOrDefault()) < 0)
            {
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "NEGATIVE", "KostenTaxShelter");
            }
            if (Data.Liberaliteiten.Count > 0 && Data.Liberaliteiten.Sum(e => e.Bedrag.GetValueOrDefault()) < 0)
            {
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "NEGATIVE", "Liberaliteiten");
            }
            if (Data.MaaltijdSportCultuurEco.Count > 0 && Data.MaaltijdSportCultuurEco.Sum(e => e.Bedrag.GetValueOrDefault()) < 0)
            {
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "NEGATIVE", "MaaltijdSportCultuurEco");
            }
            if (Data.OverdrevenIntresten.Count > 0 && Data.OverdrevenIntresten.Sum(e => e.Bedrag.GetValueOrDefault()) < 0)
            {
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "NEGATIVE", "OverdrevenIntresten");
            }
            if (Data.Pensioenen.Count > 0 && Data.Pensioenen.Sum(e => e.Bedrag.GetValueOrDefault()) < 0)
            {
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "NEGATIVE", "Pensioenen");
            }
            if (Data.Receptie.Count > 0 && Data.Receptie.Sum(e => e.Bedrag.GetValueOrDefault()) < 0)
            {
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "NEGATIVE", "Receptie");
            }
            if (Data.Restaurant.Count > 0 && Data.Restaurant.Sum(e => e.Bedrag.GetValueOrDefault()) < 0)
            {
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "NEGATIVE", "Restaurant");
            }
            if (Data.SocialeVoordelen.Count > 0 && Data.SocialeVoordelen.Sum(e => e.Bedrag.GetValueOrDefault()) < 0)
            {
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "NEGATIVE", "SocialeVoordelen");
            }
            if (Data.TerugnemingVroegereVrijstellingen.Count > 0 && Data.TerugnemingVroegereVrijstellingen.Sum(e => e.Bedrag.GetValueOrDefault()) < 0)
            {
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "NEGATIVE", "TerugnemingVroegereVrijstellingen");
            }
            if (Data.VergoedingCoupon.Count > 0 && Data.VergoedingCoupon.Sum(e => e.Bedrag.GetValueOrDefault()) < 0)
            {
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "NEGATIVE", "VergoedingCoupon");
            }
            if (Data.WaardeverminderingAandelen.Count > 0 && Data.WaardeverminderingAandelen.Sum(e => e.Bedrag.GetValueOrDefault()) < 0)
            {
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "NEGATIVE", "WaardeverminderingAandelen");
            }
            if (Data.Werknemersparticipatie.Count > 0 && Data.Werknemersparticipatie.Sum(e => e.Bedrag.GetValueOrDefault()) < 0)
            {
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "NEGATIVE", "Werknemersparticipatie");
            }
        }

        private void RegulateCars(ref WorksheetSetData worksheetset)
        {
            BelastingenAppTypes.KostItem regulate = Data.Auto.FirstOrDefault(e => e.UNID == "REGULATE" || e.UNID == "Auto_xxxxxx");
            if (Data.Auto.Count > 0)
            {
                decimal sm = Data.Auto.Where(a => a.Bedrag.HasValue && a.UNID != "REGULATE").Sum(a => a.Bedrag.Value);
                if (sm < 0)
                {
                    if (regulate == null)
                    {
                        regulate = new EY.com.eBook.RuleEngine.AY2013.BelastingenAppTypes.KostItem
                        {
                            UNID = "REGULATE"
                            ,
                            excludeRegulate = true
                            ,
                            GrootBoek = new StringListItemDataContract
                            {
                                Id = Guid.Empty.ToString()
                                ,
                                NL = "REGULARISTATIE"
                                ,
                                FR = "REGULARISATION"
                                ,
                                EN = "REGULARISATION"
                            }
                            ,
                            Id = Guid.NewGuid()
                            ,
                            Percent = 100
                        };
                        Data.Auto.Add(regulate);
                    }
                    regulate.GrootBoek = new StringListItemDataContract
                    {
                        Id = Guid.Empty.ToString()
                        ,
                        NL = "REGULARISTATIE"
                        ,
                        FR = "REGULARISATION"
                        ,
                        EN = "REGULARISATION"
                    };
                    regulate.UNID = "REGULATE";
                    regulate.Bedrag = Math.Abs(sm);
                    regulate.Saldo = Math.Abs(sm);
                }
                else
                {
                    if (regulate != null)
                    {
                        Data.Auto.Remove(regulate);
                    }
                }
            }
            else
            {
                if (regulate != null)
                {
                    Data.Auto.Remove(regulate);
                }
            }
        }

        private void RegulateTaxes(ref WorksheetSetData worksheetset)
        {
            BelastingenAppTypes.Belasting regulate = Data.Belastingen.FirstOrDefault(e => e.UNID == "REGULATE" || e.UNID == "Belastingen_xxxxxx");
            if (Data.Belastingen.Count > 0)
            {
                decimal sm = Data.Belastingen.Where(a => a.Bedrag.HasValue && a.UNID != "REGULATE").Sum(a => a.Bedrag.Value);
                if (sm < 0)
                {
                    if (regulate == null)
                    {
                        regulate = new EY.com.eBook.RuleEngine.AY2013.BelastingenAppTypes.Belasting
                        {
                            UNID = "REGULATE"
                            ,
                            GrootBoek = new StringListItemDataContract
                            {
                                Id = Guid.Empty.ToString()
                                ,
                                NL = "REGULARISTATIE"
                                ,
                                FR = "REGULARISATION"
                                ,
                                EN = "REGULARISATION"
                            }
                            ,
                            Id = Guid.NewGuid()
                            ,
                            Percent = 100
                        };
                        Data.Belastingen.Add(regulate);
                    }
                    regulate.Aanslagjaar = new StringListItemDataContract
                    {
                        Id = worksheetset.DataContract.AssessmentYear.ToString()
                        ,
                        NL = worksheetset.DataContract.AssessmentYear.ToString()
                        ,
                        FR = worksheetset.DataContract.AssessmentYear.ToString()
                        ,
                        EN = worksheetset.DataContract.AssessmentYear.ToString()
                    };
                    regulate.GrootBoek = new StringListItemDataContract
                    {
                        Id = Guid.Empty.ToString()
                        ,
                        NL = "REGULARISTATIE"
                        ,
                        FR = "REGULARISATION"
                        ,
                        EN = "REGULARISATION"
                    };
                    regulate.UNID = "REGULATE";
                    regulate.Bedrag = Math.Abs(sm);
                    regulate.Saldo = Math.Abs(sm);
                }
                else
                {
                    if (regulate != null)
                    {
                        Data.Belastingen.Remove(regulate);
                    }
                }
            }
            else
            {
                if (regulate != null)
                {
                    Data.Belastingen.Remove(regulate);
                }
            }
        }


        private void MappedBelastingen(ref WorksheetSetData worksheetset)
        {
            CoefficientDataContract cdc = worksheetset.GetCoefficient("VU_BELASTINGEN", worksheetset.DataContract.StartDate, worksheetset.DataContract.EndDate);


            List<AccountDataContract> accounts = worksheetset.GetAccountsOfMappingMeta("nbelasting", "VerworpenUitgaven", false, false, true, true, true, worksheetset.DataContract.Culture);
            accounts.RemoveAll(a => a.Saldo == 0);
            List<string> nrs = accounts.Select(a => a.InternalNr).ToList();

            List<Guid> adjustments = Data.Adjustments.Select(d => d.Id).ToList();
            Data.Belastingen.RemoveAll(b => b.AdjustmentId.HasValue && !adjustments.Contains(b.AdjustmentId.Value));


            foreach (BelastingenAppTypes.Belasting entity in Data.Belastingen.Where(b => !b.AdjustmentId.HasValue &&
                                            (b.GrootBoek != null && !nrs.Contains(b.GrootBoek.Id))))
            {
                worksheetset.ClearMessages(TypeId, entity.Id);
            }

            Data.Belastingen.RemoveAll(b => !b.AdjustmentId.HasValue &&
                                            (b.GrootBoek != null && !nrs.Contains(b.GrootBoek.Id)));


            string culture = worksheetset.DataContract.Culture;
            //List<AccountDataContract> accountsToUpdate = accounts.Where(a => a.LastChanged >= Data.LastUpdated.Value).ToList();

            foreach (AccountDataContract account in accounts)
            {
                AccountMappingDataContract amdc = account.Mappings.First(m => m.MappingKey == "VerworpenUitgaven");
                AccountDescriptionDataContract addc = account.Descriptions.FirstOrDefault(d => d.Culture == culture);
                if (addc == null) addc = account.Descriptions.FirstOrDefault();
                string desc = addc == null ? account.VisualNr : addc.Description;
                BelastingenAppTypes.Belasting entity = Data.Belastingen.FirstOrDefault(e => e.GrootBoek != null && e.GrootBoek.Id == account.InternalNr);
                if (entity != null)
                {
                    if (!entity.LastUpdated.HasValue) entity.LastUpdated = new DateTime(1980, 1, 1);
                    if (account.LastChanged >= entity.LastUpdated.Value)
                    {
                        entity.Saldo = account.Saldo;
                        entity.Percent = cdc.Value;
                        entity.Bedrag = Math.Round((account.Saldo / 100) * cdc.Value, 2, MidpointRounding.AwayFromZero);
                        entity.GrootBoek = worksheetset.GetListAccount(account);
                        entity.ListDescription = desc;
                        entity.LastUpdated = DateTime.Now;
                        entity.SoortID = amdc.MappingItemId.ToString();
                    }
                    Validate(entity, "Belastingen", ref worksheetset);
                }
                else
                {
                    entity = new EY.com.eBook.RuleEngine.AY2013.BelastingenAppTypes.Belasting
                    {
                        Id = Guid.NewGuid()
                        ,
                        Aanslagjaar = new StringListItemDataContract
                        {
                            Id = worksheetset.DataContract.AssessmentYear.ToString()
                            ,
                            NL = worksheetset.DataContract.AssessmentYear.ToString()
                            ,
                            FR = worksheetset.DataContract.AssessmentYear.ToString()
                            ,
                            EN = worksheetset.DataContract.AssessmentYear.ToString()
                        }
                        ,
                        ListDescription = desc
                        ,
                        Bedrag = Math.Round((account.Saldo / 100) * cdc.Value, 2, MidpointRounding.AwayFromZero)
                        ,
                        Percent = cdc.Value
                        ,
                        Saldo = account.Saldo
                        ,
                        GrootBoek = worksheetset.GetListAccount(account)
                        ,
                        LastUpdated = DateTime.Now
                        ,
                        SoortID = amdc.MappingItemId.ToString()
                    };
                    Validate(entity, "Belastingen", ref worksheetset);
                    Data.Belastingen.Add(entity);
                }

            }


        }

        private void MappedGewestBelastingen(ref WorksheetSetData worksheetset)
        {
            CoefficientDataContract cdc = worksheetset.GetCoefficient("VU_GEWESTELIJKEBELASTINGEN", worksheetset.DataContract.StartDate, worksheetset.DataContract.EndDate);


            List<AccountDataContract> accounts = worksheetset.GetAccountsOfMappingMeta("belasting", "VerworpenUitgaven", false, false, true, true, true, worksheetset.DataContract.Culture);
            accounts.RemoveAll(a => a.Saldo == 0);
            List<string> nrs = accounts.Select(a => a.InternalNr).ToList();

            Data.GewestBelasting.RemoveAll(b => !b.AdjustmentId.HasValue &&
                                            (b.GrootBoek != null && !nrs.Contains(b.GrootBoek.Id)));

            List<Guid> adjustments = Data.Adjustments.Select(d => d.Id).ToList();
            Data.GewestBelasting.RemoveAll(b => b.AdjustmentId.HasValue && !adjustments.Contains(b.AdjustmentId.Value));

            string culture = worksheetset.DataContract.Culture;
            //List<AccountDataContract> accountsToUpdate = accounts.Where(a => a.LastChanged >= Data.LastUpdated.Value).ToList();

            foreach (AccountDataContract account in accounts)
            {
                AccountMappingDataContract amdc = account.Mappings.First(m => m.MappingKey == "VerworpenUitgaven");
                AccountDescriptionDataContract addc = account.Descriptions.FirstOrDefault(d => d.Culture == culture);
                if (addc == null) addc = account.Descriptions.FirstOrDefault();
                string desc = addc == null ? account.VisualNr : addc.Description;
                BelastingenAppTypes.KostItem entity = Data.GewestBelasting.FirstOrDefault(e => e.GrootBoek != null && e.GrootBoek.Id == account.InternalNr);
                if (entity != null)
                {
                    if (!entity.LastUpdated.HasValue) entity.LastUpdated = new DateTime(1980, 1, 1);
                    if (account.LastChanged >= entity.LastUpdated.Value)
                    {
                        entity.Saldo = account.Saldo;
                        entity.Percent = cdc.Value;
                        entity.Bedrag = Math.Round((account.Saldo / 100) * cdc.Value, 2, MidpointRounding.AwayFromZero);
                        entity.GrootBoek = worksheetset.GetListAccount(account);
                        entity.LastUpdated = DateTime.Now;
                        entity.SoortID = amdc.MappingItemId.ToString();
                        entity.ListDescription = desc;
                    }
                    Validate(entity, "GewestBelasting", ref worksheetset);
                }
                else
                {
                    entity = new EY.com.eBook.RuleEngine.AY2013.BelastingenAppTypes.KostItem
                    {
                        Id = Guid.NewGuid()
                        ,
                        Bedrag = Math.Round((account.Saldo / 100) * cdc.Value, 2, MidpointRounding.AwayFromZero)
                        ,
                        Percent = cdc.Value
                        ,
                        Saldo = account.Saldo
                        ,
                        GrootBoek = worksheetset.GetListAccount(account)
                        ,
                        LastUpdated = DateTime.Now
                        ,
                        SoortID = amdc.MappingItemId.ToString()
                        ,
                        ListDescription = desc
                    };
                    Validate(entity, "GewestBelasting", ref worksheetset);
                    Data.GewestBelasting.Add(entity);
                }

            }


        }

        private void MappedBoetes(ref WorksheetSetData worksheetset)
        {
            CoefficientDataContract cdc = worksheetset.GetCoefficient("VU_BOETE", worksheetset.DataContract.StartDate, worksheetset.DataContract.EndDate);


            List<AccountDataContract> accounts = worksheetset.GetAccountsOfMappingMeta("boete", "VerworpenUitgaven", false, false, true, true, true, worksheetset.DataContract.Culture);
            accounts.RemoveAll(a => a.Saldo == 0);
            List<string> nrs = accounts.Select(a => a.InternalNr).ToList();


            Data.Boetes.RemoveAll(b => !b.AdjustmentId.HasValue &&
                                            (b.GrootBoek != null && !nrs.Contains(b.GrootBoek.Id)));
            List<Guid> adjustments = Data.Adjustments.Select(d => d.Id).ToList();
            Data.Boetes.RemoveAll(b => b.AdjustmentId.HasValue && !adjustments.Contains(b.AdjustmentId.Value));

            string culture = worksheetset.DataContract.Culture;

            //List<AccountDataContract> accountsToUpdate = accounts.Where(a => a.LastChanged >= Data.LastUpdated.Value).ToList();

            foreach (AccountDataContract account in accounts)
            {
                AccountMappingDataContract amdc = account.Mappings.First(m => m.MappingKey == "VerworpenUitgaven");
                AccountDescriptionDataContract addc = account.Descriptions.FirstOrDefault(d => d.Culture == culture);
                if (addc == null) addc = account.Descriptions.FirstOrDefault();
                string desc = addc == null ? account.VisualNr : addc.Description;
                BelastingenAppTypes.KostItem entity = Data.Boetes.FirstOrDefault(e => e.GrootBoek != null && e.GrootBoek.Id == account.InternalNr);
                if (entity != null)
                {
                    if (!entity.LastUpdated.HasValue) entity.LastUpdated = new DateTime(1980, 1, 1);
                    if (account.LastChanged >= entity.LastUpdated.Value)
                    {
                        entity.Saldo = account.Saldo;
                        entity.Percent = cdc.Value;
                        entity.Bedrag = Math.Round((account.Saldo / 100) * cdc.Value, 2, MidpointRounding.AwayFromZero);
                        entity.GrootBoek = worksheetset.GetListAccount(account);
                        entity.LastUpdated = DateTime.Now;
                        entity.SoortID = amdc.MappingItemId.ToString();
                        entity.ListDescription = desc;
                    }
                    Validate(entity, "Boetes", ref worksheetset);
                }
                else
                {
                    entity = new EY.com.eBook.RuleEngine.AY2013.BelastingenAppTypes.KostItem
                    {
                        Id = Guid.NewGuid()
                        ,
                        Bedrag = Math.Round((account.Saldo / 100) * cdc.Value, 2, MidpointRounding.AwayFromZero)
                        ,
                        Percent = cdc.Value
                        ,
                        Saldo = account.Saldo
                        ,
                        GrootBoek = worksheetset.GetListAccount(account)
                        ,
                        LastUpdated = DateTime.Now
                        ,
                        SoortID = amdc.MappingItemId.ToString()
                        ,
                        ListDescription = desc
                    };
                    Validate(entity, "Boetes", ref worksheetset);
                    Data.Boetes.Add(entity);
                }

            }


        }

        private void MappedPensioenen(ref WorksheetSetData worksheetset)
        {
            CoefficientDataContract cdc = worksheetset.GetCoefficient("VU_PENSIOENEN", worksheetset.DataContract.StartDate, worksheetset.DataContract.EndDate);


            List<AccountDataContract> accounts = worksheetset.GetAccountsOfMappingMeta("pensioenen", "VerworpenUitgaven", false, false, true, true, true, worksheetset.DataContract.Culture);
            accounts.RemoveAll(a => a.Saldo == 0);
            List<string> nrs = accounts.Select(a => a.InternalNr).ToList();


            Data.Pensioenen.RemoveAll(b => !b.AdjustmentId.HasValue &&
                                            (b.GrootBoek != null && !nrs.Contains(b.GrootBoek.Id)));

            List<Guid> adjustments = Data.Adjustments.Select(d => d.Id).ToList();
            Data.Pensioenen.RemoveAll(b => b.AdjustmentId.HasValue && !adjustments.Contains(b.AdjustmentId.Value));


            string culture = worksheetset.DataContract.Culture;
            //List<AccountDataContract> accountsToUpdate = accounts.Where(a => a.LastChanged >= Data.LastUpdated.Value).ToList();

            foreach (AccountDataContract account in accounts)
            {
                AccountMappingDataContract amdc = account.Mappings.First(m => m.MappingKey == "VerworpenUitgaven");
                AccountDescriptionDataContract addc = account.Descriptions.FirstOrDefault(d => d.Culture == culture);
                if (addc == null) addc = account.Descriptions.FirstOrDefault();
                string desc = addc == null ? account.VisualNr : addc.Description;
                BelastingenAppTypes.KostItem entity = Data.Pensioenen.FirstOrDefault(e => e.GrootBoek != null && e.GrootBoek.Id == account.InternalNr);
                if (entity != null)
                {
                    if (!entity.LastUpdated.HasValue) entity.LastUpdated = new DateTime(1980, 1, 1);
                    if (account.LastChanged >= entity.LastUpdated.Value)
                    {
                        entity.Saldo = account.Saldo;
                        entity.Percent = cdc.Value;
                        entity.Bedrag = Math.Round((account.Saldo / 100) * cdc.Value, 2, MidpointRounding.AwayFromZero);
                        entity.GrootBoek = worksheetset.GetListAccount(account);
                        entity.LastUpdated = DateTime.Now;
                        entity.ListDescription = desc;
                        entity.SoortID = amdc.MappingItemId.ToString();
                    }
                    Validate(entity, "Pensioenen", ref worksheetset);
                }
                else
                {
                    entity = new EY.com.eBook.RuleEngine.AY2013.BelastingenAppTypes.KostItem
                    {
                        Id = Guid.NewGuid()
                        ,
                        Bedrag = Math.Round((account.Saldo / 100) * cdc.Value, 2, MidpointRounding.AwayFromZero)
                        ,
                        Percent = cdc.Value
                        ,
                        Saldo = account.Saldo
                        ,
                        GrootBoek = worksheetset.GetListAccount(account)
                        ,
                        LastUpdated = DateTime.Now
                        ,
                        ListDescription = desc
                        ,
                        SoortID = amdc.MappingItemId.ToString()
                    };
                    Validate(entity, "Pensioenen", ref worksheetset);
                    Data.Pensioenen.Add(entity);
                }

            }


        }

        private void MappedAuto(ref WorksheetSetData worksheetset)
        {
            try
            {
                CoefficientDataContract cdcBrandstof;
                List<AccountDataContract> accounts;
                List<AccountDataContract> accounts70;

                cdcBrandstof = worksheetset.GetCoefficient("VU_AUTO_BRANDSTOF", worksheetset.DataContract.StartDate, worksheetset.DataContract.EndDate);

                accounts = worksheetset.GetAccountsOfMappingMeta("auto", "VerworpenUitgaven", false, false, true, true, true, worksheetset.DataContract.Culture);
                accounts70 = worksheetset.GetAccountsOfMappingMeta("auto70", "VerworpenUitgaven", false, false, true, true, true, worksheetset.DataContract.Culture);
                if (accounts70.Count > 0) accounts.AddRange(accounts70);

                accounts.RemoveAll(a => a.Saldo == 0);
                List<string> nrs = accounts.Select(a => a.InternalNr).ToList();

                Data.Auto.RemoveAll(b => (!b.AdjustmentId.HasValue &&
                                                (b.GrootBoek != null && !nrs.Contains(b.GrootBoek.Id))) || (!string.IsNullOrEmpty(b.UNID) && b.UNID.Contains("_ESSENCE")));

                List<Guid> adjustments = Data.Adjustments.Select(d => d.Id).ToList();
                Data.Auto.RemoveAll(b => (b.AdjustmentId.HasValue && !adjustments.Contains(b.AdjustmentId.Value)) || (b.UNID != null && b.UNID.IsGuid()));

                string culture = worksheetset.DataContract.Culture;
                //List<AccountDataContract> accountsToUpdate = accounts.Where(a => a.LastChanged >= Data.LastUpdated.Value).ToList();

                foreach (AccountDataContract account in accounts)
                {
                    AccountMappingDataContract amdc = account.Mappings.First(m => m.MappingKey == "VerworpenUitgaven");
                    AccountDescriptionDataContract addc = account.Descriptions.FirstOrDefault(d => d.Culture == culture);
                    if (addc == null) addc = account.Descriptions.FirstOrDefault();
                    string desc = addc == null ? account.VisualNr : addc.Description;

                    List<CoefficientDataContract> cdcs = worksheetset.GetMappingCoefficients(amdc.MappingItemId.Value, worksheetset.DataContract.StartDate, worksheetset.DataContract.EndDate);
                    BelastingenAppTypes.KostItem entity = Data.Auto.FirstOrDefault(e => e.GrootBoek != null && e.GrootBoek.Id == account.InternalNr && e.UNID != "ESSENCE");
                    decimal percent = 0;
                    if (cdcs.Count == 1)
                    {
                        percent = cdcs.First().Value;
                    }
                    else
                    {
                        foreach (CoefficientDataContract cdc in cdcs.Where(c => c.Key.Contains("VU_AUTO_") && !c.Key.Contains("BRANDSTOF")))
                        {
                            if (percent == 0)
                            {
                                percent = cdc.Value;
                            }
                            else
                            {
                                percent = percent / 100 * cdc.Value;
                            }

                        }
                    }

                    if (entity != null)
                    {
                        if (!entity.LastUpdated.HasValue) entity.LastUpdated = new DateTime(1980, 1, 1);
                        if (account.LastChanged >= entity.LastUpdated.Value)
                        {
                            entity.Saldo = account.Saldo;
                            entity.Percent = percent;
                            entity.Bedrag = Math.Round((account.Saldo / 100) * percent, 2, MidpointRounding.AwayFromZero);
                            entity.GrootBoek = worksheetset.GetListAccount(account);
                            entity.LastUpdated = DateTime.Now;
                            entity.SoortID = amdc.MappingItemId.ToString();
                            entity.ListDescription = desc;
                        }
                        Validate(entity, "Auto", ref worksheetset);
                    }
                    else
                    {
                        entity = new EY.com.eBook.RuleEngine.AY2013.BelastingenAppTypes.KostItem
                        {
                            Id = Guid.NewGuid()
                            ,
                            Bedrag = Math.Round((account.Saldo / 100) * percent, 2, MidpointRounding.AwayFromZero)
                            ,
                            Percent = percent
                            ,
                            Saldo = account.Saldo
                            ,
                            GrootBoek = worksheetset.GetListAccount(account)
                            ,
                            LastUpdated = DateTime.Now
                            ,
                            SoortID = amdc.MappingItemId.ToString()
                            ,
                            ListDescription = desc
                        };
                        Validate(entity, "Auto", ref worksheetset);
                        Data.Auto.Add(entity);
                    }

                    if (amdc.MappingMeta == "auto70")
                    {

                        entity = Data.Auto.FirstOrDefault(e => e.GrootBoek != null && e.GrootBoek.Id == account.InternalNr && e.UNID == "ESSENCE");
                        if (entity != null)
                        {
                            if (!entity.LastUpdated.HasValue) entity.LastUpdated = new DateTime(1980, 1, 1);
                            if (account.LastChanged >= entity.LastUpdated.Value)
                            {
                                entity.Saldo = account.Saldo;
                                entity.Percent = cdcBrandstof.Value;
                                entity.Bedrag = Math.Round((account.Saldo / 100) * cdcBrandstof.Value, 2, MidpointRounding.AwayFromZero);
                                entity.GrootBoek = worksheetset.GetListAccount(account);
                                entity.UNID = "ESSENCE";
                                entity.LastUpdated = DateTime.Now;
                                entity.ListDescription = desc;
                                entity.SoortID = null;
                            }
                            Validate(entity, "Auto", ref worksheetset);
                        }
                        else
                        {
                            entity = new EY.com.eBook.RuleEngine.AY2013.BelastingenAppTypes.KostItem
                            {
                                Id = Guid.NewGuid()
                                ,
                                Bedrag = Math.Round((account.Saldo / 100) * cdcBrandstof.Value, 2, MidpointRounding.AwayFromZero)
                                ,
                                UNID = "ESSENCE"
                                ,
                                Percent = cdcBrandstof.Value
                                ,
                                Saldo = account.Saldo
                                ,
                                GrootBoek = worksheetset.GetListAccount(account)
                                ,
                                LastUpdated = DateTime.Now
                                ,
                                ListDescription = desc
                                ,
                                SoortID = null
                            };
                            Validate(entity, "Auto", ref worksheetset);
                            Data.Auto.Add(entity);
                        }
                    }

                }
            }
            catch (Exception e)
            {
                // Add extra info so I can find the causing file (very strange error know, line:0)
                throw new Exception(string.Format("Belastingen App, MappedAuto, file {0} : {1}", worksheetset.DataContract.Id.ToString(), e.Message), e);
            }


        }

        private void ImportFromVAA(ref WorksheetSetData worksheetset)
        {
            Data.AutoVAA.RemoveAll(a => a.UNID == "VAA_IMPORT");
            foreach (AY2013.VoordelenAppTypes.VAA_AutoRupo item in worksheetset.VoordelenApp.Data.VoordelenVanAlleAardAutoRupo.Where(v => v.TransferVU == true))
            {
                string name = string.Format("[{0}] {1}", worksheetset.VoordelenApp.Data.VoordelenVanAlleAardAutoRupo.IndexOf(item)+1,item.MerkModel);
                EY.com.eBook.RuleEngine.AY2013.BelastingenAppTypes.KostItem vaaItem = new EY.com.eBook.RuleEngine.AY2013.BelastingenAppTypes.KostItem
                {
                    UNID = "VAA_IMPORT"
                    , Bedrag = item.VUWaarde.GetValueOrDefault()
                    ,
                    GrootBoek = new StringListItemDataContract
                    {
                        Id = Guid.Empty.ToString()
                        ,
                        NL = name
                        ,
                        FR = name
                        ,
                        EN = name
                    }
                    ,
                    Id = Guid.NewGuid()
                    ,
                    Percent = 100
                    , Saldo = item.Totaal.GetValueOrDefault() + item.PersoonlijkeBijdrage.GetValueOrDefault()
                };
                Data.AutoVAA.Add(vaaItem);
            }
        }

        private void MappedAutoVAA(ref WorksheetSetData worksheetset)
        {


            List<AccountDataContract> accounts = worksheetset.GetAccountsOfMappingMeta("autoVAA", "VerworpenUitgaven", false, false, true, true, true, worksheetset.DataContract.Culture);
            if (Data.AutoVAA == null) Data.AutoVAA = new List<EY.com.eBook.RuleEngine.AY2013.BelastingenAppTypes.KostItem>();
            accounts.RemoveAll(a => a.Saldo == 0);
            List<string> nrs = accounts.Select(a => a.InternalNr).ToList();

            Data.AutoVAA.RemoveAll(b => (!b.AdjustmentId.HasValue &&
                                            (b.GrootBoek != null && !nrs.Contains(b.GrootBoek.Id))) || (!string.IsNullOrEmpty(b.UNID)));

            List<Guid> adjustments = Data.Adjustments.Select(d => d.Id).ToList();
            Data.AutoVAA.RemoveAll(b => (b.AdjustmentId.HasValue && !adjustments.Contains(b.AdjustmentId.Value)) || (b.UNID != null && b.UNID.IsGuid()));

            string culture = worksheetset.DataContract.Culture;
            //List<AccountDataContract> accountsToUpdate = accounts.Where(a => a.LastChanged >= Data.LastUpdated.Value).ToList();

            foreach (AccountDataContract account in accounts)
            {
                AccountMappingDataContract amdc = account.Mappings.First(m => m.MappingKey == "VerworpenUitgaven");
                AccountDescriptionDataContract addc = account.Descriptions.FirstOrDefault(d => d.Culture == culture);
                if (addc == null) addc = account.Descriptions.FirstOrDefault();
                string desc = addc == null ? account.VisualNr : addc.Description;

                List<CoefficientDataContract> cdcs = worksheetset.GetMappingCoefficients(amdc.MappingItemId.Value, worksheetset.DataContract.StartDate, worksheetset.DataContract.EndDate);
                BelastingenAppTypes.KostItem entity = Data.AutoVAA.FirstOrDefault(e => e.GrootBoek != null && e.GrootBoek.Id == account.InternalNr && e.UNID != "ESSENCE");
                decimal percent = 0;
                percent = cdcs.First().Value;


                if (entity != null)
                {
                    if (!entity.LastUpdated.HasValue) entity.LastUpdated = new DateTime(1980, 1, 1);
                    if (account.LastChanged >= entity.LastUpdated.Value)
                    {
                        entity.Saldo = Math.Abs(account.Saldo);
                        entity.Percent = 100; // percent;
                        entity.Bedrag = Math.Round((Math.Abs(account.Saldo) / 100) * percent, 2, MidpointRounding.AwayFromZero);
                        entity.GrootBoek = worksheetset.GetListAccount(account);
                        entity.LastUpdated = DateTime.Now;
                        entity.SoortID = amdc.MappingItemId.ToString();
                        entity.ListDescription = desc;
                    }
                    Validate(entity, "AutoVAA", ref worksheetset);
                }
                else
                {
                    entity = new EY.com.eBook.RuleEngine.AY2013.BelastingenAppTypes.KostItem
                    {
                        Id = Guid.NewGuid()
                        ,
                        Bedrag = Math.Round((Math.Abs(account.Saldo) / 100) * percent, 2, MidpointRounding.AwayFromZero)
                        ,
                        Percent = 100 //percent 
                        ,
                        Saldo = Math.Abs(account.Saldo)
                        ,
                        GrootBoek = worksheetset.GetListAccount(account)
                        ,
                        LastUpdated = DateTime.Now
                        ,
                        SoortID = amdc.MappingItemId.ToString()
                        ,
                        ListDescription = desc
                    };
                    Validate(entity, "AutoVAA", ref worksheetset);
                    Data.AutoVAA.Add(entity);
                }



            }


        }


        private void MappedReceptie(ref WorksheetSetData worksheetset)
        {
            CoefficientDataContract cdc = worksheetset.GetCoefficient("VU_RECEPTIE", worksheetset.DataContract.StartDate, worksheetset.DataContract.EndDate);


            List<AccountDataContract> accounts = worksheetset.GetAccountsOfMappingMeta("receptie", "VerworpenUitgaven", false, false, true, true, true, worksheetset.DataContract.Culture);
            accounts.RemoveAll(a => a.Saldo == 0);
            List<string> nrs = accounts.Select(a => a.InternalNr).ToList();

            Data.Receptie.RemoveAll(b => !b.AdjustmentId.HasValue &&
                                            (b.GrootBoek != null && !nrs.Contains(b.GrootBoek.Id)));

            List<Guid> adjustments = Data.Adjustments.Select(d => d.Id).ToList();
            Data.Receptie.RemoveAll(b => b.AdjustmentId.HasValue && !adjustments.Contains(b.AdjustmentId.Value));


            string culture = worksheetset.DataContract.Culture;
            //List<AccountDataContract> accountsToUpdate = accounts.Where(a => a.LastChanged >= Data.LastUpdated.Value).ToList();

            foreach (AccountDataContract account in accounts)
            {
                AccountMappingDataContract amdc = account.Mappings.First(m => m.MappingKey == "VerworpenUitgaven");
                AccountDescriptionDataContract addc = account.Descriptions.FirstOrDefault(d => d.Culture == culture);
                if (addc == null) addc = account.Descriptions.FirstOrDefault();
                string desc = addc == null ? account.VisualNr : addc.Description;

                BelastingenAppTypes.KostItem entity = Data.Receptie.FirstOrDefault(e => e.GrootBoek != null && e.GrootBoek.Id == account.InternalNr);
                if (entity != null)
                {
                    if (!entity.LastUpdated.HasValue) entity.LastUpdated = new DateTime(1980, 1, 1);
                    if (account.LastChanged >= entity.LastUpdated.Value)
                    {
                        entity.Saldo = account.Saldo;
                        entity.Percent = cdc.Value;
                        entity.Bedrag = Math.Round((account.Saldo / 100) * cdc.Value, 2, MidpointRounding.AwayFromZero);
                        entity.GrootBoek = worksheetset.GetListAccount(account);
                        entity.LastUpdated = DateTime.Now;
                        entity.ListDescription = desc;
                        entity.SoortID = amdc.MappingItemId.ToString();
                    }
                    Validate(entity, "Receptie", ref worksheetset);
                }
                else
                {
                    entity = new EY.com.eBook.RuleEngine.AY2013.BelastingenAppTypes.KostItem
                    {
                        Id = Guid.NewGuid()
                        ,
                        Bedrag = Math.Round((account.Saldo / 100) * cdc.Value, 2, MidpointRounding.AwayFromZero)
                        ,
                        Percent = cdc.Value
                        ,
                        Saldo = account.Saldo
                        ,
                        GrootBoek = worksheetset.GetListAccount(account)
                        ,
                        LastUpdated = DateTime.Now
                        ,
                        ListDescription = desc
                        ,
                        SoortID = amdc.MappingItemId.ToString()
                    };
                    Validate(entity, "Receptie", ref worksheetset);
                    Data.Receptie.Add(entity);
                }

            }


        }

        private void MappedRestaurant(ref WorksheetSetData worksheetset)
        {
            CoefficientDataContract cdc = worksheetset.GetCoefficient("VU_RESTAURANT", worksheetset.DataContract.StartDate, worksheetset.DataContract.EndDate);


            List<AccountDataContract> accounts = worksheetset.GetAccountsOfMappingMeta("restaurant", "VerworpenUitgaven", false, false, true, true, true, worksheetset.DataContract.Culture);
            accounts.RemoveAll(a => a.Saldo == 0);
            List<string> nrs = accounts.Select(a => a.InternalNr).ToList();


            Data.Restaurant.RemoveAll(b => !b.AdjustmentId.HasValue &&
                                            (b.GrootBoek != null && !nrs.Contains(b.GrootBoek.Id)));

            List<Guid> adjustments = Data.Adjustments.Select(d => d.Id).ToList();
            Data.Restaurant.RemoveAll(b => b.AdjustmentId.HasValue && !adjustments.Contains(b.AdjustmentId.Value));


            string culture = worksheetset.DataContract.Culture;

            //List<AccountDataContract> accountsToUpdate = accounts.Where(a => a.LastChanged >= Data.LastUpdated.Value).ToList();

            foreach (AccountDataContract account in accounts)
            {
                AccountMappingDataContract amdc = account.Mappings.First(m => m.MappingKey == "VerworpenUitgaven");
                AccountDescriptionDataContract addc = account.Descriptions.FirstOrDefault(d => d.Culture == culture);
                if (addc == null) addc = account.Descriptions.FirstOrDefault();
                string desc = addc == null ? account.VisualNr : addc.Description;

                BelastingenAppTypes.KostItem entity = Data.Restaurant.FirstOrDefault(e => e.GrootBoek != null && e.GrootBoek.Id == account.InternalNr);
                if (entity != null)
                {
                    if (!entity.LastUpdated.HasValue) entity.LastUpdated = new DateTime(1980, 1, 1);
                    if (account.LastChanged >= entity.LastUpdated.Value)
                    {
                        entity.Saldo = account.Saldo;
                        entity.Percent = cdc.Value;
                        entity.Bedrag = Math.Round((account.Saldo / 100) * cdc.Value, 2, MidpointRounding.AwayFromZero);
                        entity.GrootBoek = worksheetset.GetListAccount(account);
                        entity.LastUpdated = DateTime.Now;
                        entity.ListDescription = desc;
                        entity.SoortID = amdc.MappingItemId.ToString();
                    }
                    Validate(entity, "Restaurant", ref worksheetset);
                }
                else
                {
                    entity = new EY.com.eBook.RuleEngine.AY2013.BelastingenAppTypes.KostItem
                    {
                        Id = Guid.NewGuid()
                        ,
                        Bedrag = Math.Round((account.Saldo / 100) * cdc.Value, 2, MidpointRounding.AwayFromZero)
                        ,
                        Percent = cdc.Value
                        ,
                        Saldo = account.Saldo
                        ,
                        GrootBoek = worksheetset.GetListAccount(account)
                        ,
                        LastUpdated = DateTime.Now
                        ,
                        ListDescription = desc
                        ,
                        SoortID = amdc.MappingItemId.ToString()
                    };
                    Validate(entity, "Restaurant", ref worksheetset);
                    Data.Restaurant.Add(entity);
                }

            }


        }

        private void MappedKledij(ref WorksheetSetData worksheetset)
        {
            CoefficientDataContract cdc = worksheetset.GetCoefficient("VU_KLEDIJ", worksheetset.DataContract.StartDate, worksheetset.DataContract.EndDate);


            List<AccountDataContract> accounts = worksheetset.GetAccountsOfMappingMeta("kledij", "VerworpenUitgaven", false, false, true, true, true, worksheetset.DataContract.Culture);
            accounts.RemoveAll(a => a.Saldo == 0);
            List<string> nrs = accounts.Select(a => a.InternalNr).ToList();

            Data.Kledij.RemoveAll(b => !b.AdjustmentId.HasValue &&
                                            (b.GrootBoek != null && !nrs.Contains(b.GrootBoek.Id)));
            List<Guid> adjustments = Data.Adjustments.Select(d => d.Id).ToList();
            Data.Kledij.RemoveAll(b => b.AdjustmentId.HasValue && !adjustments.Contains(b.AdjustmentId.Value));

            string culture = worksheetset.DataContract.Culture;

            //List<AccountDataContract> accountsToUpdate = accounts.Where(a => a.LastChanged >= Data.LastUpdated.Value).ToList();

            foreach (AccountDataContract account in accounts)
            {
                AccountMappingDataContract amdc = account.Mappings.First(m => m.MappingKey == "VerworpenUitgaven");
                AccountDescriptionDataContract addc = account.Descriptions.FirstOrDefault(d => d.Culture == culture);
                if (addc == null) addc = account.Descriptions.FirstOrDefault();
                string desc = addc == null ? account.VisualNr : addc.Description;

                BelastingenAppTypes.KostItem entity = Data.Kledij.FirstOrDefault(e => e.GrootBoek != null && e.GrootBoek.Id == account.InternalNr);
                if (entity != null)
                {
                    if (!entity.LastUpdated.HasValue) entity.LastUpdated = new DateTime(1980, 1, 1);
                    if (account.LastChanged >= entity.LastUpdated.Value)
                    {
                        entity.Saldo = account.Saldo;
                        entity.Percent = cdc.Value;
                        entity.Bedrag = Math.Round((account.Saldo / 100) * cdc.Value, 2, MidpointRounding.AwayFromZero);
                        entity.GrootBoek = worksheetset.GetListAccount(account);
                        entity.LastUpdated = DateTime.Now;
                        entity.ListDescription = desc;
                        entity.SoortID = amdc.MappingItemId.ToString();
                    }
                    Validate(entity, "Kledij", ref worksheetset);
                }
                else
                {
                    entity = new EY.com.eBook.RuleEngine.AY2013.BelastingenAppTypes.KostItem
                    {
                        Id = Guid.NewGuid()
                        ,
                        Bedrag = Math.Round((account.Saldo / 100) * cdc.Value, 2, MidpointRounding.AwayFromZero)
                        ,
                        Percent = cdc.Value
                        ,
                        Saldo = account.Saldo
                        ,
                        GrootBoek = worksheetset.GetListAccount(account)
                        ,
                        LastUpdated = DateTime.Now
                        ,
                        ListDescription = desc
                        ,
                        SoortID = amdc.MappingItemId.ToString()
                    };
                    Validate(entity, "Kledij", ref worksheetset);
                    Data.Kledij.Add(entity);
                }

            }


        }

        private void MappedOverdrevenIntresten(ref WorksheetSetData worksheetset)
        {
            CoefficientDataContract cdc = worksheetset.GetCoefficient("VU_OVERDREVENINTRESTEN", worksheetset.DataContract.StartDate, worksheetset.DataContract.EndDate);


            List<AccountDataContract> accounts = worksheetset.GetAccountsOfMappingMeta("intresten", "VerworpenUitgaven", false, false, true, true, true, worksheetset.DataContract.Culture);
            accounts.RemoveAll(a => a.Saldo == 0);
            List<string> nrs = accounts.Select(a => a.InternalNr).ToList();


            Data.OverdrevenIntresten.RemoveAll(b => !b.AdjustmentId.HasValue &&
                                            (b.GrootBoek != null && !nrs.Contains(b.GrootBoek.Id)));
            List<Guid> adjustments = Data.Adjustments.Select(d => d.Id).ToList();
            Data.OverdrevenIntresten.RemoveAll(b => b.AdjustmentId.HasValue && !adjustments.Contains(b.AdjustmentId.Value));


            string culture = worksheetset.DataContract.Culture;
            //List<AccountDataContract> accountsToUpdate = accounts.Where(a => a.LastChanged >= Data.LastUpdated.Value).ToList();

            foreach (AccountDataContract account in accounts)
            {
                AccountMappingDataContract amdc = account.Mappings.First(m => m.MappingKey == "VerworpenUitgaven");
                AccountDescriptionDataContract addc = account.Descriptions.FirstOrDefault(d => d.Culture == culture);
                if (addc == null) addc = account.Descriptions.FirstOrDefault();
                string desc = addc == null ? account.VisualNr : addc.Description;

                BelastingenAppTypes.KostItem entity = Data.OverdrevenIntresten.FirstOrDefault(e => e.GrootBoek != null && e.GrootBoek.Id == account.InternalNr);
                if (entity != null)
                {
                    if (!entity.LastUpdated.HasValue) entity.LastUpdated = new DateTime(1980, 1, 1);
                    if (account.LastChanged >= entity.LastUpdated.Value)
                    {
                        entity.Saldo = account.Saldo;
                        entity.Percent = cdc.Value;
                        entity.Bedrag = Math.Round((account.Saldo / 100) * cdc.Value, 2, MidpointRounding.AwayFromZero);
                        entity.GrootBoek = worksheetset.GetListAccount(account);
                        entity.LastUpdated = DateTime.Now;
                        entity.ListDescription = desc;
                        entity.SoortID = amdc.MappingItemId.ToString();
                    }
                    Validate(entity, "OverdrevenIntresten", ref worksheetset);
                }
                else
                {
                    entity = new EY.com.eBook.RuleEngine.AY2013.BelastingenAppTypes.KostItem
                    {
                        Id = Guid.NewGuid()
                        ,
                        Bedrag = Math.Round((account.Saldo / 100) * cdc.Value, 2, MidpointRounding.AwayFromZero)
                        ,
                        Percent = cdc.Value
                        ,
                        Saldo = account.Saldo
                        ,
                        GrootBoek = worksheetset.GetListAccount(account)
                        ,
                        LastUpdated = DateTime.Now
                        ,
                        ListDescription = desc
                        ,
                        SoortID = amdc.MappingItemId.ToString()
                    };
                    Validate(entity, "OverdrevenIntresten", ref worksheetset);
                    Data.OverdrevenIntresten.Add(entity);
                }

            }


        }

        private void MappedIntrestLening(ref WorksheetSetData worksheetset)
        {
            CoefficientDataContract cdc = worksheetset.GetCoefficient("VU_INTRESTLENING", worksheetset.DataContract.StartDate, worksheetset.DataContract.EndDate);

            List<AccountDataContract> accounts = worksheetset.GetAccountsOfMappingMeta("intrestlening", "VerworpenUitgaven", false, false, true, true, true, worksheetset.DataContract.Culture);
            accounts.RemoveAll(a => a.Saldo == 0);
            List<string> nrs = accounts.Select(a => a.InternalNr).ToList();

            Data.IntrestenLeningen.RemoveAll(b => !b.AdjustmentId.HasValue &&
                                            (b.GrootBoek != null && !nrs.Contains(b.GrootBoek.Id)));

            List<Guid> adjustments = Data.Adjustments.Select(d => d.Id).ToList();
            Data.IntrestenLeningen.RemoveAll(b => b.AdjustmentId.HasValue && !adjustments.Contains(b.AdjustmentId.Value));

            string culture = worksheetset.DataContract.Culture;
            //List<AccountDataContract> accountsToUpdate = accounts.Where(a => a.LastChanged >= Data.LastUpdated.Value).ToList();

            foreach (AccountDataContract account in accounts)
            {
                AccountMappingDataContract amdc = account.Mappings.First(m => m.MappingKey == "VerworpenUitgaven");
                AccountDescriptionDataContract addc = account.Descriptions.FirstOrDefault(d => d.Culture == culture);
                if (addc == null) addc = account.Descriptions.FirstOrDefault();
                string desc = addc == null ? account.VisualNr : addc.Description;

                BelastingenAppTypes.KostItem entity = Data.IntrestenLeningen.FirstOrDefault(e => e.GrootBoek != null && e.GrootBoek.Id == account.InternalNr);
                if (entity != null)
                {
                    if (!entity.LastUpdated.HasValue) entity.LastUpdated = new DateTime(1980, 1, 1);
                    if (account.LastChanged >= entity.LastUpdated.Value)
                    {
                        entity.Saldo = account.Saldo;
                        entity.Percent = cdc.Value;
                        entity.Bedrag = Math.Round((account.Saldo / 100) * cdc.Value, 2, MidpointRounding.AwayFromZero);
                        entity.GrootBoek = worksheetset.GetListAccount(account);
                        entity.LastUpdated = DateTime.Now;
                        entity.ListDescription = desc;
                        entity.SoortID = amdc.MappingItemId.ToString();
                    }
                    Validate(entity, "IntrestenLeningen", ref worksheetset);
                }
                else
                {
                    entity = new EY.com.eBook.RuleEngine.AY2013.BelastingenAppTypes.KostItem
                    {
                        Id = Guid.NewGuid()
                        ,
                        Bedrag = Math.Round((account.Saldo / 100) * cdc.Value, 2, MidpointRounding.AwayFromZero)
                        ,
                        Percent = cdc.Value
                        ,
                        Saldo = account.Saldo
                        ,
                        GrootBoek = worksheetset.GetListAccount(account)
                        ,
                        LastUpdated = DateTime.Now
                        ,
                        ListDescription = desc
                        ,
                        SoortID = amdc.MappingItemId.ToString()
                    };
                    Validate(entity, "IntrestenLeningen", ref worksheetset);
                    Data.IntrestenLeningen.Add(entity);
                }

            }


        }

        private void MappedAbnormaal(ref WorksheetSetData worksheetset)
        {
            CoefficientDataContract cdc = worksheetset.GetCoefficient("VU_ABNORMAALGOEDGUNSTIG", worksheetset.DataContract.StartDate, worksheetset.DataContract.EndDate);

            List<AccountDataContract> accounts = worksheetset.GetAccountsOfMappingMeta("agv", "VerworpenUitgaven", false, false, true, true, true, worksheetset.DataContract.Culture);
            accounts.RemoveAll(a => a.Saldo == 0);
            List<string> nrs = accounts.Select(a => a.InternalNr).ToList();


            Data.AbnormaleGoedgunstigeVoordelen.RemoveAll(b => !b.AdjustmentId.HasValue &&
                                            (b.GrootBoek != null && !nrs.Contains(b.GrootBoek.Id)));
            List<Guid> adjustments = Data.Adjustments.Select(d => d.Id).ToList();
            Data.AbnormaleGoedgunstigeVoordelen.RemoveAll(b => b.AdjustmentId.HasValue && !adjustments.Contains(b.AdjustmentId.Value));

            string culture = worksheetset.DataContract.Culture;
            //List<AccountDataContract> accountsToUpdate = accounts.Where(a => a.LastChanged >= Data.LastUpdated.Value).ToList();

            foreach (AccountDataContract account in accounts)
            {
                AccountMappingDataContract amdc = account.Mappings.First(m => m.MappingKey == "VerworpenUitgaven");
                AccountDescriptionDataContract addc = account.Descriptions.FirstOrDefault(d => d.Culture == culture);
                if (addc == null) addc = account.Descriptions.FirstOrDefault();
                string desc = addc == null ? account.VisualNr : addc.Description;

                BelastingenAppTypes.KostItem entity = Data.AbnormaleGoedgunstigeVoordelen.FirstOrDefault(e => e.GrootBoek != null && e.GrootBoek.Id == account.InternalNr);
                if (entity != null)
                {
                    if (!entity.LastUpdated.HasValue) entity.LastUpdated = new DateTime(1980, 1, 1);
                    if (account.LastChanged >= entity.LastUpdated.Value)
                    {
                        entity.Saldo = account.Saldo;
                        entity.Percent = cdc.Value;
                        entity.Bedrag = Math.Round((account.Saldo / 100) * cdc.Value, 2, MidpointRounding.AwayFromZero);
                        entity.GrootBoek = worksheetset.GetListAccount(account);
                        entity.LastUpdated = DateTime.Now;
                        entity.ListDescription = desc;
                        entity.SoortID = amdc.MappingItemId.ToString();
                    }
                    Validate(entity, "AbnormaleGoedgunstigeVoordelen", ref worksheetset);
                }
                else
                {
                    entity = new EY.com.eBook.RuleEngine.AY2013.BelastingenAppTypes.KostItem
                    {
                        Id = Guid.NewGuid()
                        ,
                        Bedrag = Math.Round((account.Saldo / 100) * cdc.Value, 2, MidpointRounding.AwayFromZero)
                        ,
                        Percent = cdc.Value
                        ,
                        Saldo = account.Saldo
                        ,
                        GrootBoek = worksheetset.GetListAccount(account)
                        ,
                        LastUpdated = DateTime.Now
                        ,
                        ListDescription = desc
                        ,
                        SoortID = amdc.MappingItemId.ToString()
                    };
                    Validate(entity, "AbnormaleGoedgunstigeVoordelen", ref worksheetset);
                    Data.AbnormaleGoedgunstigeVoordelen.Add(entity);
                }

            }


        }

        private void MappedSocialeVoordelen(ref WorksheetSetData worksheetset)
        {
            CoefficientDataContract cdc = worksheetset.GetCoefficient("VU_SOCIALEVOORDELEN", worksheetset.DataContract.StartDate, worksheetset.DataContract.EndDate);

            List<AccountDataContract> accounts = worksheetset.GetAccountsOfMappingMeta("voordeel", "VerworpenUitgaven", false, false, true, true, true, worksheetset.DataContract.Culture);
            accounts.RemoveAll(a => a.Saldo == 0);
            List<string> nrs = accounts.Select(a => a.InternalNr).ToList();

            Data.SocialeVoordelen.RemoveAll(b => !b.AdjustmentId.HasValue &&
                                            (b.GrootBoek != null && !nrs.Contains(b.GrootBoek.Id)));

            List<Guid> adjustments = Data.Adjustments.Select(d => d.Id).ToList();
            Data.SocialeVoordelen.RemoveAll(b => b.AdjustmentId.HasValue && !adjustments.Contains(b.AdjustmentId.Value));


            string culture = worksheetset.DataContract.Culture;
            //List<AccountDataContract> accountsToUpdate = accounts.Where(a => a.LastChanged >= Data.LastUpdated.Value).ToList();

            foreach (AccountDataContract account in accounts)
            {
                AccountMappingDataContract amdc = account.Mappings.First(m => m.MappingKey == "VerworpenUitgaven");
                AccountDescriptionDataContract addc = account.Descriptions.FirstOrDefault(d => d.Culture == culture);
                if (addc == null) addc = account.Descriptions.FirstOrDefault();
                string desc = addc == null ? account.VisualNr : addc.Description;

                BelastingenAppTypes.KostItem entity = Data.SocialeVoordelen.FirstOrDefault(e => e.GrootBoek != null && e.GrootBoek.Id == account.InternalNr);
                if (entity != null)
                {
                    if (!entity.LastUpdated.HasValue) entity.LastUpdated = new DateTime(1980, 1, 1);
                    if (account.LastChanged >= entity.LastUpdated.Value)
                    {
                        entity.Saldo = account.Saldo;
                        entity.Percent = cdc.Value;
                        entity.Bedrag = Math.Round((account.Saldo / 100) * cdc.Value, 2, MidpointRounding.AwayFromZero);
                        entity.GrootBoek = worksheetset.GetListAccount(account);
                        entity.LastUpdated = DateTime.Now;
                        entity.ListDescription = desc;
                        entity.SoortID = amdc.MappingItemId.ToString();
                    }
                    Validate(entity, "SocialeVoordelen", ref worksheetset);
                }
                else
                {
                    entity = new EY.com.eBook.RuleEngine.AY2013.BelastingenAppTypes.KostItem
                    {
                        Id = Guid.NewGuid()
                        ,
                        Bedrag = Math.Round((account.Saldo / 100) * cdc.Value, 2, MidpointRounding.AwayFromZero)
                        ,
                        Percent = cdc.Value
                        ,
                        Saldo = account.Saldo
                        ,
                        GrootBoek = worksheetset.GetListAccount(account)
                        ,
                        LastUpdated = DateTime.Now
                        ,
                        ListDescription = desc
                        ,
                        SoortID = amdc.MappingItemId.ToString()
                    };
                    Validate(entity, "SocialeVoordelen", ref worksheetset);
                    Data.SocialeVoordelen.Add(entity);
                }

            }


        }

        private void MappedMaaltijdSportCultuurEco(ref WorksheetSetData worksheetset)
        {
            CoefficientDataContract cdc = worksheetset.GetCoefficient("VU_MAALTIJDSPORTCULTUURECO", worksheetset.DataContract.StartDate, worksheetset.DataContract.EndDate);

            List<AccountDataContract> accounts = worksheetset.GetAccountsOfMappingMeta("maaltijdsportcultuureco", "VerworpenUitgaven", false, false, true, true, true, worksheetset.DataContract.Culture);
            accounts.RemoveAll(a => a.Saldo == 0);
            List<string> nrs = accounts.Select(a => a.InternalNr).ToList();


            Data.MaaltijdSportCultuurEco.RemoveAll(b => !b.AdjustmentId.HasValue &&
                                            (b.GrootBoek != null && !nrs.Contains(b.GrootBoek.Id)));

            List<Guid> adjustments = Data.Adjustments.Select(d => d.Id).ToList();
            Data.MaaltijdSportCultuurEco.RemoveAll(b => b.AdjustmentId.HasValue && !adjustments.Contains(b.AdjustmentId.Value));


            string culture = worksheetset.DataContract.Culture;
            //List<AccountDataContract> accountsToUpdate = accounts.Where(a => a.LastChanged >= Data.LastUpdated.Value).ToList();

            foreach (AccountDataContract account in accounts)
            {
                AccountMappingDataContract amdc = account.Mappings.First(m => m.MappingKey == "VerworpenUitgaven");
                AccountDescriptionDataContract addc = account.Descriptions.FirstOrDefault(d => d.Culture == culture);
                if (addc == null) addc = account.Descriptions.FirstOrDefault();
                string desc = addc == null ? account.VisualNr : addc.Description;

                BelastingenAppTypes.KostItem entity = Data.MaaltijdSportCultuurEco.FirstOrDefault(e => e.GrootBoek != null && e.GrootBoek.Id == account.InternalNr);
                if (entity != null)
                {
                    if (!entity.LastUpdated.HasValue) entity.LastUpdated = new DateTime(1980, 1, 1);
                    if (account.LastChanged >= entity.LastUpdated.Value)
                    {
                        entity.Saldo = account.Saldo;
                        entity.Percent = cdc.Value;
                        entity.Bedrag = Math.Round((account.Saldo / 100) * cdc.Value, 2, MidpointRounding.AwayFromZero);
                        entity.GrootBoek = worksheetset.GetListAccount(account);
                        entity.LastUpdated = DateTime.Now;
                        entity.ListDescription = desc;
                        entity.SoortID = amdc.MappingItemId.ToString();
                    }
                    Validate(entity, "MaaltijdSportCultuurEco", ref worksheetset);
                }
                else
                {
                    entity = new EY.com.eBook.RuleEngine.AY2013.BelastingenAppTypes.KostItem
                    {
                        Id = Guid.NewGuid()
                        ,
                        Bedrag = Math.Round((account.Saldo / 100) * cdc.Value, 2, MidpointRounding.AwayFromZero)
                        ,
                        Percent = cdc.Value
                        ,
                        Saldo = account.Saldo
                        ,
                        GrootBoek = worksheetset.GetListAccount(account)
                        ,
                        LastUpdated = DateTime.Now
                        ,
                        ListDescription = desc
                        ,
                        SoortID = amdc.MappingItemId.ToString()
                    };
                    Validate(entity, "MaaltijdSportCultuurEco", ref worksheetset);
                    Data.MaaltijdSportCultuurEco.Add(entity);
                }

            }


        }

        private void MappedLiberaliteiten(ref WorksheetSetData worksheetset)
        {
            CoefficientDataContract cdcnorm = worksheetset.GetCoefficient("VU_GIFT", worksheetset.DataContract.StartDate, worksheetset.DataContract.EndDate);
            CoefficientDataContract cdcfin = worksheetset.GetCoefficient("VU_FINANCIELEGIFT", worksheetset.DataContract.StartDate, worksheetset.DataContract.EndDate);

            List<AccountDataContract> accounts = worksheetset.GetAccountsOfMappingMeta("gift", "VerworpenUitgaven", false, false, true, true, true, worksheetset.DataContract.Culture);
            /*
             * @version 5.2
             * @ticket 55 -> UNDO @ticket ?
             */
            List<AccountDataContract> accounts2 = worksheetset.GetAccountsOfMappingMeta("fingift", "VerworpenUitgaven", false, false, true, true, true, worksheetset.DataContract.Culture);

            string culture = worksheetset.DataContract.Culture;

            /*
             * @version 5.2
             * @ticket 55
             */
            if (accounts.Count > 0)
            {
                if (accounts2.Count > 0) accounts.AddRange(accounts2);
            }
            else if (accounts2.Count > 0)
            {
                accounts = accounts2;
            }
             

            accounts.RemoveAll(a => a.Saldo == 0);

            List<string> nrs = accounts.Select(a => a.InternalNr).ToList();


            Data.Liberaliteiten.RemoveAll(b => !b.AdjustmentId.HasValue &&
                                            (b.GrootBoek != null && !nrs.Contains(b.GrootBoek.Id)));

            List<Guid> adjustments = Data.Adjustments.Select(d => d.Id).ToList();
            Data.Liberaliteiten.RemoveAll(b => b.AdjustmentId.HasValue && !adjustments.Contains(b.AdjustmentId.Value));

            //List<AccountDataContract> accountsToUpdate = accounts.Where(a => a.LastChanged >= Data.LastUpdated.Value).ToList();

            foreach (AccountDataContract account in accounts)
            {
                AccountMappingDataContract amdc = account.Mappings.First(m => m.MappingKey == "VerworpenUitgaven");
                AccountDescriptionDataContract addc = account.Descriptions.FirstOrDefault(d => d.Culture == culture);
                if (addc == null) addc = account.Descriptions.FirstOrDefault();
                string desc = addc == null ? account.VisualNr : addc.Description;

                CoefficientDataContract cdc = amdc.MappingMeta == "gift" ? cdcnorm : cdcfin;

                BelastingenAppTypes.KostItem entity = Data.Liberaliteiten.FirstOrDefault(e => e.GrootBoek != null && e.GrootBoek.Id == account.InternalNr);
                if (entity != null)
                {
                    if (!entity.LastUpdated.HasValue) entity.LastUpdated = new DateTime(1980, 1, 1);
                    if (account.LastChanged >= entity.LastUpdated.Value)
                    {
                        entity.Saldo = account.Saldo;
                        entity.Percent = cdc.Value;
                        entity.Bedrag = Math.Round((account.Saldo / 100) * cdc.Value, 2, MidpointRounding.AwayFromZero);
                        entity.GrootBoek = worksheetset.GetListAccount(account);
                        entity.LastUpdated = DateTime.Now;
                        entity.ListDescription = desc;
                        entity.SoortID = amdc.MappingItemId.ToString();
                    }
                    Validate(entity, "Liberaliteiten", ref worksheetset);
                }
                else
                {
                    entity = new EY.com.eBook.RuleEngine.AY2013.BelastingenAppTypes.KostItem
                    {
                        Id = Guid.NewGuid()
                        ,
                        Bedrag = Math.Round((account.Saldo / 100) * cdc.Value, 2, MidpointRounding.AwayFromZero)
                        ,
                        Percent = cdc.Value
                        ,
                        Saldo = account.Saldo
                        ,
                        GrootBoek = worksheetset.GetListAccount(account)
                        ,
                        LastUpdated = DateTime.Now
                        ,
                        ListDescription = desc
                        ,
                        SoortID = amdc.MappingItemId.ToString()
                    };
                    Validate(entity, "Liberaliteiten", ref worksheetset);
                    Data.Liberaliteiten.Add(entity);
                }

            }


        }

        private void MappedWaardeverminderingAandelen(ref WorksheetSetData worksheetset)
        {
            CoefficientDataContract cdc = worksheetset.GetCoefficient("VU_WAARDEVERMINDERINGAANDELEN", worksheetset.DataContract.StartDate, worksheetset.DataContract.EndDate);

            List<AccountDataContract> accounts = worksheetset.GetAccountsOfMappingMeta("aandelen", "VerworpenUitgaven", false, false, true, true, true, worksheetset.DataContract.Culture);
            accounts.RemoveAll(a => a.Saldo == 0);
            List<string> nrs = accounts.Select(a => a.InternalNr).ToList();

            string culture = worksheetset.DataContract.Culture;

            Data.WaardeverminderingAandelen.RemoveAll(b => !b.AdjustmentId.HasValue &&
                                            (b.GrootBoek != null && !nrs.Contains(b.GrootBoek.Id)));

            List<Guid> adjustments = Data.Adjustments.Select(d => d.Id).ToList();
            Data.WaardeverminderingAandelen.RemoveAll(b => b.AdjustmentId.HasValue && !adjustments.Contains(b.AdjustmentId.Value));

            //List<AccountDataContract> accountsToUpdate = accounts.Where(a => a.LastChanged >= Data.LastUpdated.Value).ToList();

            foreach (AccountDataContract account in accounts)
            {
                AccountMappingDataContract amdc = account.Mappings.First(m => m.MappingKey == "VerworpenUitgaven");
                AccountDescriptionDataContract addc = account.Descriptions.FirstOrDefault(d => d.Culture == culture);
                if (addc == null) addc = account.Descriptions.FirstOrDefault();
                string desc = addc == null ? account.VisualNr : addc.Description;

                BelastingenAppTypes.KostItem entity = Data.WaardeverminderingAandelen.FirstOrDefault(e => e.GrootBoek != null && e.GrootBoek.Id == account.InternalNr);
                if (entity != null)
                {
                    if (!entity.LastUpdated.HasValue) entity.LastUpdated = new DateTime(1980, 1, 1);
                    if (account.LastChanged >= entity.LastUpdated.Value)
                    {
                        entity.Saldo = account.Saldo;
                        entity.Percent = cdc.Value;
                        entity.Bedrag = Math.Round((account.Saldo / 100) * cdc.Value, 2, MidpointRounding.AwayFromZero);
                        entity.GrootBoek = worksheetset.GetListAccount(account);
                        entity.LastUpdated = DateTime.Now;
                        entity.ListDescription = desc;
                        entity.SoortID = amdc.MappingItemId.ToString();
                    }
                    Validate(entity, "WaardeverminderingAandelen", ref worksheetset);
                }
                else
                {
                    entity = new EY.com.eBook.RuleEngine.AY2013.BelastingenAppTypes.KostItem
                    {
                        Id = Guid.NewGuid()
                        ,
                        Bedrag = Math.Round((account.Saldo / 100) * cdc.Value, 2, MidpointRounding.AwayFromZero)
                        ,
                        Percent = cdc.Value
                        ,
                        Saldo = account.Saldo
                        ,
                        GrootBoek = worksheetset.GetListAccount(account)
                        ,
                        LastUpdated = DateTime.Now
                        ,
                        ListDescription = desc
                        ,
                        SoortID = amdc.MappingItemId.ToString()
                    };
                    Validate(entity, "WaardeverminderingAandelen", ref worksheetset);
                    Data.WaardeverminderingAandelen.Add(entity);
                }

            }


        }

        private void MappedTerugnemingVroegereVrijstellingen(ref WorksheetSetData worksheetset)
        {
            CoefficientDataContract cdc = worksheetset.GetCoefficient("VU_TERUGNEMINGVROEGEREVRIJSTELLINGEN", worksheetset.DataContract.StartDate, worksheetset.DataContract.EndDate);

            List<AccountDataContract> accounts = worksheetset.GetAccountsOfMappingMeta("vrijstellingen", "VerworpenUitgaven", false, false, true, true, true, worksheetset.DataContract.Culture);
            accounts.RemoveAll(a => a.Saldo == 0);
            List<string> nrs = accounts.Select(a => a.InternalNr).ToList();

            string culture = worksheetset.DataContract.Culture;
            Data.TerugnemingVroegereVrijstellingen.RemoveAll(b => !b.AdjustmentId.HasValue &&
                                            (b.GrootBoek != null && !nrs.Contains(b.GrootBoek.Id)));

            List<Guid> adjustments = Data.Adjustments.Select(d => d.Id).ToList();
            Data.TerugnemingVroegereVrijstellingen.RemoveAll(b => b.AdjustmentId.HasValue && !adjustments.Contains(b.AdjustmentId.Value));


            //List<AccountDataContract> accountsToUpdate = accounts.Where(a => a.LastChanged >= Data.LastUpdated.Value).ToList();

            foreach (AccountDataContract account in accounts)
            {
                AccountMappingDataContract amdc = account.Mappings.First(m => m.MappingKey == "VerworpenUitgaven");
                AccountDescriptionDataContract addc = account.Descriptions.FirstOrDefault(d => d.Culture == culture);
                if (addc == null) addc = account.Descriptions.FirstOrDefault();
                string desc = addc == null ? account.VisualNr : addc.Description;

                BelastingenAppTypes.KostItem entity = Data.TerugnemingVroegereVrijstellingen.FirstOrDefault(e => e.GrootBoek != null && e.GrootBoek.Id == account.InternalNr);
                if (entity != null)
                {
                    if (!entity.LastUpdated.HasValue) entity.LastUpdated = new DateTime(1980, 1, 1);
                    if (account.LastChanged >= entity.LastUpdated.Value)
                    {
                        entity.Saldo = account.Saldo;
                        entity.Percent = cdc.Value;
                        entity.Bedrag = Math.Round((account.Saldo / 100) * cdc.Value, 2, MidpointRounding.AwayFromZero);
                        entity.GrootBoek = worksheetset.GetListAccount(account);
                        entity.LastUpdated = DateTime.Now;
                        entity.ListDescription = desc;
                        entity.SoortID = amdc.MappingItemId.ToString();
                    }
                    Validate(entity, "TerugnemingVroegereVrijstellingen", ref worksheetset);
                }
                else
                {
                    entity = new EY.com.eBook.RuleEngine.AY2013.BelastingenAppTypes.KostItem
                    {
                        Id = Guid.NewGuid()
                        ,
                        Bedrag = Math.Round((account.Saldo / 100) * cdc.Value, 2, MidpointRounding.AwayFromZero)
                        ,
                        Percent = cdc.Value
                        ,
                        Saldo = account.Saldo
                        ,
                        GrootBoek = worksheetset.GetListAccount(account)
                        ,
                        LastUpdated = DateTime.Now
                        ,
                        ListDescription = desc
                        ,
                        SoortID = amdc.MappingItemId.ToString()
                    };
                    Validate(entity, "TerugnemingVroegereVrijstellingen", ref worksheetset);
                    Data.TerugnemingVroegereVrijstellingen.Add(entity);
                }

            }


        }

        private void MappedWerknemersparticipatie(ref WorksheetSetData worksheetset)
        {
            CoefficientDataContract cdc = worksheetset.GetCoefficient("VU_WERKNEMERSPARTICIPATIE", worksheetset.DataContract.StartDate, worksheetset.DataContract.EndDate);

            List<AccountDataContract> accounts = worksheetset.GetAccountsOfMappingMeta("werknemersparticipatie", "VerworpenUitgaven", false, false, true, true, true, worksheetset.DataContract.Culture);
            accounts.RemoveAll(a => a.Saldo == 0);
            List<string> nrs = accounts.Select(a => a.InternalNr).ToList();

            string culture = worksheetset.DataContract.Culture;

            Data.Werknemersparticipatie.RemoveAll(b => !b.AdjustmentId.HasValue &&
                                            (b.GrootBoek != null && !nrs.Contains(b.GrootBoek.Id)));

            List<Guid> adjustments = Data.Adjustments.Select(d => d.Id).ToList();
            Data.Werknemersparticipatie.RemoveAll(b => b.AdjustmentId.HasValue && !adjustments.Contains(b.AdjustmentId.Value));

            //List<AccountDataContract> accountsToUpdate = accounts.Where(a => a.LastChanged >= Data.LastUpdated.Value).ToList();

            foreach (AccountDataContract account in accounts)
            {
                AccountMappingDataContract amdc = account.Mappings.First(m => m.MappingKey == "VerworpenUitgaven");
                AccountDescriptionDataContract addc = account.Descriptions.FirstOrDefault(d => d.Culture == culture);
                if (addc == null) addc = account.Descriptions.FirstOrDefault();
                string desc = addc == null ? account.VisualNr : addc.Description;

                BelastingenAppTypes.KostItem entity = Data.Werknemersparticipatie.FirstOrDefault(e => e.GrootBoek != null && e.GrootBoek.Id == account.InternalNr);
                if (entity != null)
                {
                    if (!entity.LastUpdated.HasValue) entity.LastUpdated = new DateTime(1980, 1, 1);
                    if (account.LastChanged >= entity.LastUpdated.Value)
                    {
                        entity.Saldo = account.Saldo;
                        entity.Percent = cdc.Value;
                        entity.Bedrag = Math.Round((account.Saldo / 100) * cdc.Value, 2, MidpointRounding.AwayFromZero);
                        entity.GrootBoek = worksheetset.GetListAccount(account);
                        entity.LastUpdated = DateTime.Now;
                        entity.ListDescription = desc;
                        entity.SoortID = amdc.MappingItemId.ToString();
                    }
                    Validate(entity, "Werknemersparticipatie", ref worksheetset);
                }
                else
                {
                    entity = new EY.com.eBook.RuleEngine.AY2013.BelastingenAppTypes.KostItem
                    {
                        Id = Guid.NewGuid()
                        ,
                        Bedrag = Math.Round((account.Saldo / 100) * cdc.Value, 2, MidpointRounding.AwayFromZero)
                        ,
                        Percent = cdc.Value
                        ,
                        Saldo = account.Saldo
                        ,
                        GrootBoek = worksheetset.GetListAccount(account)
                        ,
                        LastUpdated = DateTime.Now
                        ,
                        ListDescription = desc
                        ,
                        SoortID = amdc.MappingItemId.ToString()
                    };
                    Validate(entity, "Werknemersparticipatie", ref worksheetset);
                    Data.Werknemersparticipatie.Add(entity);
                }

            }


        }

        private void MappedVergoedingCoupon(ref WorksheetSetData worksheetset)
        {
            CoefficientDataContract cdc = new CoefficientDataContract { Value = 100 };

            //worksheetset.GetCoefficient("VU_VERGOEDINGCOUPON", worksheetset.DataContract.StartDate, worksheetset.DataContract.EndDate);

            List<AccountDataContract> accounts = worksheetset.GetAccountsOfMappingMeta("coupon", "VerworpenUitgaven", false, false, true, true, true, worksheetset.DataContract.Culture);
            accounts.RemoveAll(a => a.Saldo == 0);
            List<string> nrs = accounts.Select(a => a.InternalNr).ToList();

            string culture = worksheetset.DataContract.Culture;

            Data.VergoedingCoupon.RemoveAll(b => !b.AdjustmentId.HasValue &&
                                            (b.GrootBoek != null && !nrs.Contains(b.GrootBoek.Id)));

            List<Guid> adjustments = Data.Adjustments.Select(d => d.Id).ToList();
            Data.VergoedingCoupon.RemoveAll(b => b.AdjustmentId.HasValue && !adjustments.Contains(b.AdjustmentId.Value));


            //List<AccountDataContract> accountsToUpdate = accounts.Where(a => a.LastChanged >= Data.LastUpdated.Value).ToList();

            foreach (AccountDataContract account in accounts)
            {
                AccountMappingDataContract amdc = account.Mappings.First(m => m.MappingKey == "VerworpenUitgaven");
                AccountDescriptionDataContract addc = account.Descriptions.FirstOrDefault(d => d.Culture == culture);
                if (addc == null) addc = account.Descriptions.FirstOrDefault();
                string desc = addc == null ? account.VisualNr : addc.Description;

                BelastingenAppTypes.KostItem entity = Data.VergoedingCoupon.FirstOrDefault(e => e.GrootBoek != null && e.GrootBoek.Id == account.InternalNr);
                if (entity != null)
                {
                    if (!entity.LastUpdated.HasValue) entity.LastUpdated = new DateTime(1980, 1, 1);
                    if (account.LastChanged >= entity.LastUpdated.Value)
                    {
                        entity.Saldo = account.Saldo;
                        entity.Percent = cdc.Value;
                        entity.Bedrag = Math.Round((account.Saldo / 100) * cdc.Value, 2, MidpointRounding.AwayFromZero);
                        entity.GrootBoek = worksheetset.GetListAccount(account);
                        entity.LastUpdated = DateTime.Now;
                        entity.ListDescription = desc;
                        entity.SoortID = amdc.MappingItemId.ToString();
                    }
                    Validate(entity, "VergoedingCoupon", ref worksheetset);
                }
                else
                {
                    entity = new EY.com.eBook.RuleEngine.AY2013.BelastingenAppTypes.KostItem
                    {
                        Id = Guid.NewGuid()
                        ,
                        Bedrag = Math.Round((account.Saldo / 100) * cdc.Value, 2, MidpointRounding.AwayFromZero)
                        ,
                        Percent = cdc.Value
                        ,
                        Saldo = account.Saldo
                        ,
                        GrootBoek = worksheetset.GetListAccount(account)
                        ,
                        LastUpdated = DateTime.Now
                        ,
                        ListDescription = desc
                        ,
                        SoortID = amdc.MappingItemId.ToString()
                    };
                    Validate(entity, "VergoedingCoupon", ref worksheetset);
                    Data.VergoedingCoupon.Add(entity);
                }

            }


        }

        private void MappedKostenTaxShelter(ref WorksheetSetData worksheetset)
        {
            CoefficientDataContract cdc = worksheetset.GetCoefficient("VU_KOSTENTAXSHELTER", worksheetset.DataContract.StartDate, worksheetset.DataContract.EndDate);

            List<AccountDataContract> accounts = worksheetset.GetAccountsOfMappingMeta("taxshelter", "VerworpenUitgaven", false, false, true, true, true, worksheetset.DataContract.Culture);
            accounts.RemoveAll(a => a.Saldo == 0);
            List<string> nrs = accounts.Select(a => a.InternalNr).ToList();


            Data.KostenTaxShelter.RemoveAll(b => !b.AdjustmentId.HasValue &&
                                            (b.GrootBoek != null && !nrs.Contains(b.GrootBoek.Id)));

            List<Guid> adjustments = Data.Adjustments.Select(d => d.Id).ToList();
            Data.KostenTaxShelter.RemoveAll(b => b.AdjustmentId.HasValue && !adjustments.Contains(b.AdjustmentId.Value));


            string culture = worksheetset.DataContract.Culture;
            //List<AccountDataContract> accountsToUpdate = accounts.Where(a => a.LastChanged >= Data.LastUpdated.Value).ToList();

            foreach (AccountDataContract account in accounts)
            {
                AccountMappingDataContract amdc = account.Mappings.First(m => m.MappingKey == "VerworpenUitgaven");
                AccountDescriptionDataContract addc = account.Descriptions.FirstOrDefault(d => d.Culture == culture);
                if (addc == null) addc = account.Descriptions.FirstOrDefault();
                string desc = addc == null ? account.VisualNr : addc.Description;

                BelastingenAppTypes.KostItem entity = Data.KostenTaxShelter.FirstOrDefault(e => e.GrootBoek != null && e.GrootBoek.Id == account.InternalNr);
                if (entity != null)
                {
                    if (!entity.LastUpdated.HasValue) entity.LastUpdated = new DateTime(1980, 1, 1);
                    if (account.LastChanged >= entity.LastUpdated.Value)
                    {
                        entity.Saldo = account.Saldo;
                        entity.Percent = cdc.Value;
                        entity.Bedrag = Math.Round((account.Saldo / 100) * cdc.Value, 2, MidpointRounding.AwayFromZero);
                        entity.GrootBoek = worksheetset.GetListAccount(account);
                        entity.LastUpdated = DateTime.Now;
                        entity.ListDescription = desc;
                        entity.SoortID = amdc.MappingItemId.ToString();
                    }
                    Validate(entity, "KostenTaxShelter", ref worksheetset);
                }
                else
                {
                    entity = new EY.com.eBook.RuleEngine.AY2013.BelastingenAppTypes.KostItem
                    {
                        Id = Guid.NewGuid()
                        ,
                        Bedrag = Math.Round((account.Saldo / 100) * cdc.Value, 2, MidpointRounding.AwayFromZero)
                        ,
                        Percent = cdc.Value
                        ,
                        Saldo = account.Saldo
                        ,
                        GrootBoek = worksheetset.GetListAccount(account)
                        ,
                        LastUpdated = DateTime.Now
                        ,
                        ListDescription = desc
                        ,
                        SoortID = amdc.MappingItemId.ToString()
                    };
                    Validate(entity, "KostenTaxShelter", ref worksheetset);
                    Data.KostenTaxShelter.Add(entity);
                }

            }


        }

        private void MappedGewestPremies(ref WorksheetSetData worksheetset)
        {
            CoefficientDataContract cdc = worksheetset.GetCoefficient("VU_GEWESTELIJKEPREMIES", worksheetset.DataContract.StartDate, worksheetset.DataContract.EndDate);

            List<AccountDataContract> accounts = worksheetset.GetAccountsOfMappingMeta("gewestpremies", "VerworpenUitgaven", false, false, true, true, true, worksheetset.DataContract.Culture);
            accounts.RemoveAll(a => a.Saldo == 0);
            List<string> nrs = accounts.Select(a => a.InternalNr).ToList();


            Data.GewestPremies.RemoveAll(b => !b.AdjustmentId.HasValue &&
                                            (b.GrootBoek != null && !nrs.Contains(b.GrootBoek.Id)));
            List<Guid> adjustments = Data.Adjustments.Select(d => d.Id).ToList();
            Data.GewestPremies.RemoveAll(b => b.AdjustmentId.HasValue && !adjustments.Contains(b.AdjustmentId.Value));


            string culture = worksheetset.DataContract.Culture;
            //List<AccountDataContract> accountsToUpdate = accounts.Where(a => a.LastChanged >= Data.LastUpdated.Value).ToList();

            foreach (AccountDataContract account in accounts)
            {
                AccountMappingDataContract amdc = account.Mappings.First(m => m.MappingKey == "VerworpenUitgaven");
                AccountDescriptionDataContract addc = account.Descriptions.FirstOrDefault(d => d.Culture == culture);
                if (addc == null) addc = account.Descriptions.FirstOrDefault();
                string desc = addc == null ? account.VisualNr : addc.Description;

                BelastingenAppTypes.KostItem entity = Data.GewestPremies.FirstOrDefault(e => e.GrootBoek != null && e.GrootBoek.Id == account.InternalNr);
                if (entity != null)
                {
                    if (!entity.LastUpdated.HasValue) entity.LastUpdated = new DateTime(1980, 1, 1);
                    if (account.LastChanged >= entity.LastUpdated.Value)
                    {
                        entity.Saldo = account.Saldo;
                        entity.Percent = cdc.Value;
                        entity.Bedrag = Math.Round((account.Saldo / 100) * cdc.Value, 2, MidpointRounding.AwayFromZero);
                        entity.GrootBoek = worksheetset.GetListAccount(account);
                        entity.LastUpdated = DateTime.Now;
                        entity.ListDescription = desc;
                        entity.SoortID = amdc.MappingItemId.ToString();
                    }
                    Validate(entity, "GewestPremies", ref worksheetset);
                }
                else
                {
                    entity = new EY.com.eBook.RuleEngine.AY2013.BelastingenAppTypes.KostItem
                    {
                        Id = Guid.NewGuid()
                        ,
                        Bedrag = Math.Round((account.Saldo / 100) * cdc.Value, 2, MidpointRounding.AwayFromZero)
                        ,
                        Percent = cdc.Value
                        ,
                        Saldo = account.Saldo
                        ,
                        GrootBoek = worksheetset.GetListAccount(account)
                        ,
                        LastUpdated = DateTime.Now
                        ,
                        ListDescription = desc
                        ,
                        SoortID = amdc.MappingItemId.ToString()
                    };
                    Validate(entity, "GewestPremies", ref worksheetset);
                    Data.GewestPremies.Add(entity);
                }

            }


        }

        private void MappedBepaaldeStaten(ref WorksheetSetData worksheetset)
        {
            CoefficientDataContract cdc = worksheetset.GetCoefficient("VU_BEPAALDESTATEN", worksheetset.DataContract.StartDate, worksheetset.DataContract.EndDate);

            List<AccountDataContract> accounts = worksheetset.GetAccountsOfMappingMeta("bepaaldestaten", "VerworpenUitgaven", false, false, true, true, true, worksheetset.DataContract.Culture);
            accounts.RemoveAll(a => a.Saldo == 0);
            List<string> nrs = accounts.Select(a => a.InternalNr).ToList();

            string culture = worksheetset.DataContract.Culture;

            Data.BepaaldeStaten.RemoveAll(b => !b.AdjustmentId.HasValue &&
                                            (b.GrootBoek != null && !nrs.Contains(b.GrootBoek.Id)));
            List<Guid> adjustments = Data.Adjustments.Select(d => d.Id).ToList();
            Data.BepaaldeStaten.RemoveAll(b => b.AdjustmentId.HasValue && !adjustments.Contains(b.AdjustmentId.Value));


            //List<AccountDataContract> accountsToUpdate = accounts.Where(a => a.LastChanged >= Data.LastUpdated.Value).ToList();

            foreach (AccountDataContract account in accounts)
            {
                AccountMappingDataContract amdc = account.Mappings.First(m => m.MappingKey == "VerworpenUitgaven");
                AccountDescriptionDataContract addc = account.Descriptions.FirstOrDefault(d => d.Culture == culture);
                if (addc == null) addc = account.Descriptions.FirstOrDefault();
                string desc = addc == null ? account.VisualNr : addc.Description;

                BelastingenAppTypes.KostItem entity = Data.BepaaldeStaten.FirstOrDefault(e => e.GrootBoek != null && e.GrootBoek.Id == account.InternalNr);
                if (entity != null)
                {
                    if (!entity.LastUpdated.HasValue) entity.LastUpdated = new DateTime(1980, 1, 1);
                    if (account.LastChanged >= entity.LastUpdated.Value)
                    {
                        entity.Saldo = account.Saldo;
                        entity.Percent = cdc.Value;
                        entity.Bedrag = Math.Round((account.Saldo / 100) * cdc.Value, 2, MidpointRounding.AwayFromZero);
                        entity.GrootBoek = worksheetset.GetListAccount(account);
                        entity.LastUpdated = DateTime.Now;
                        entity.ListDescription = desc;
                        entity.SoortID = amdc.MappingItemId.ToString();
                    }
                    Validate(entity, "BepaaldeStaten", ref worksheetset);
                }
                else
                {
                    entity = new EY.com.eBook.RuleEngine.AY2013.BelastingenAppTypes.KostItem
                    {
                        Id = Guid.NewGuid()
                        ,
                        Bedrag = Math.Round((account.Saldo / 100) * cdc.Value, 2, MidpointRounding.AwayFromZero)
                        ,
                        Percent = cdc.Value
                        ,
                        Saldo = account.Saldo
                        ,
                        GrootBoek = worksheetset.GetListAccount(account)
                        ,
                        LastUpdated = DateTime.Now
                        ,
                        ListDescription = desc
                        ,
                        SoortID = amdc.MappingItemId.ToString()
                    };
                    Validate(entity, "BepaaldeStaten", ref worksheetset);
                    Data.BepaaldeStaten.Add(entity);
                }

            }


        }

        private void MappedAndere(ref WorksheetSetData worksheetset)
        {
            CoefficientDataContract cdc = worksheetset.GetCoefficient("VU_ANDERE", worksheetset.DataContract.StartDate, worksheetset.DataContract.EndDate);

            List<AccountDataContract> accounts = worksheetset.GetAccountsOfMappingMeta("andere", "VerworpenUitgaven", false, false, true, true, true, worksheetset.DataContract.Culture);
            accounts.RemoveAll(a => a.Saldo == 0);
            List<string> nrs = accounts.Select(a => a.InternalNr).ToList();

            string culture = worksheetset.DataContract.Culture;

            Data.Andere.RemoveAll(b => !b.AdjustmentId.HasValue &&
                                            (b.GrootBoek != null && !nrs.Contains(b.GrootBoek.Id)));

            List<Guid> adjustments = Data.Adjustments.Select(d => d.Id).ToList();
            Data.Andere.RemoveAll(b => b.AdjustmentId.HasValue && !adjustments.Contains(b.AdjustmentId.Value));


            //List<AccountDataContract> accountsToUpdate = accounts.Where(a => a.LastChanged >= Data.LastUpdated.Value).ToList();

            foreach (AccountDataContract account in accounts)
            {
                AccountMappingDataContract amdc = account.Mappings.First(m => m.MappingKey == "VerworpenUitgaven");
                AccountDescriptionDataContract addc = account.Descriptions.FirstOrDefault(d => d.Culture == culture);
                if (addc == null) addc = account.Descriptions.FirstOrDefault();
                string desc = addc == null ? account.VisualNr : addc.Description;

                BelastingenAppTypes.KostItem entity = Data.Andere.FirstOrDefault(e => e.GrootBoek != null && e.GrootBoek.Id == account.InternalNr);
                if (entity != null)
                {
                    if (!entity.LastUpdated.HasValue) entity.LastUpdated = new DateTime(1980, 1, 1);
                    if (account.LastChanged >= entity.LastUpdated.Value)
                    {
                        entity.Saldo = account.Saldo;
                        entity.Percent = cdc.Value;
                        entity.Bedrag = Math.Round((account.Saldo / 100) * cdc.Value, 2, MidpointRounding.AwayFromZero);
                        entity.GrootBoek = worksheetset.GetListAccount(account);
                        entity.LastUpdated = DateTime.Now;
                        entity.ListDescription = desc;
                        entity.SoortID = amdc.MappingItemId.ToString();
                    }
                    Validate(entity, "Andere", ref worksheetset);
                }
                else
                {
                    entity = new EY.com.eBook.RuleEngine.AY2013.BelastingenAppTypes.KostItem
                    {
                        Id = Guid.NewGuid()
                        ,
                        Bedrag = Math.Round((account.Saldo / 100) * cdc.Value, 2, MidpointRounding.AwayFromZero)
                        ,
                        Percent = cdc.Value
                        ,
                        Saldo = account.Saldo
                        ,
                        GrootBoek = worksheetset.GetListAccount(account)
                        ,
                        LastUpdated = DateTime.Now
                        ,
                        ListDescription = desc
                        ,
                        SoortID = amdc.MappingItemId.ToString()
                    };
                    Validate(entity, "Andere", ref worksheetset);
                    Data.Andere.Add(entity);
                }

            }


        }

        #region AddUpdateDelete

        public void AddToAdjustments(EY.com.eBook.RuleEngine.AY2013.BelastingenAppTypes.Adjustment entity, ref WorksheetSetData worksheetset)
        {
            entity = Validate(entity, "Adjustments", ref worksheetset);
            if (entity.IsValid)
            {

                entity = AddAdjustment(entity, ref worksheetset);

                Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
                Data.Adjustments.Add(entity);
            }
        }


        private BelastingenAppTypes.Adjustment AddAdjustment(BelastingenAppTypes.Adjustment entity, ref WorksheetSetData worksheetset)
        {
            Guid mappingId = new Guid(entity.SoortAdjust.Id);
            MappingItemDataContract midc = worksheetset.GetMappingItem(mappingId);


            if (!entity.Percent.HasValue)
            {
                CoefficientDataContract cdc = worksheetset.GetMappingCoefficient(midc.Id, worksheetset.DataContract.StartDate, worksheetset.DataContract.EndDate);
                entity.Percent = cdc.Value;
            }
            entity.VerworpenUitgaven = Math.Round((entity.Bedrag.GetValueOrDefault() / 100) * entity.Percent.GetValueOrDefault(), 2, MidpointRounding.AwayFromZero);

            BelastingenAppTypes.KostItem kostItem = new EY.com.eBook.RuleEngine.AY2013.BelastingenAppTypes.KostItem
            {
                AdjustmentId = entity.Id
                ,
                Bedrag = entity.VerworpenUitgaven
                ,
                Percent = entity.Percent
                ,
                Saldo = entity.Bedrag
                ,
                SoortID = midc.Id.ToString()
                ,
                excludeRegulate = false
                ,
                ListDescription = entity.Name
                ,
                GrootBoek = new StringListItemDataContract
                {
                    Id = entity.Id.ToString()
                    ,
                    NL = entity.Name
                    ,
                    EN = entity.Name
                    ,
                    FR = entity.Name
                }
                ,
                Id = Guid.NewGuid()
                ,
                LastUpdated = DateTime.Now
            };

            switch (midc.Meta.ToLower())
            {
                case "auto":
                    Data.Auto.Add(kostItem);
                    entity.Collection = "Auto";
                    break;
                case "auto70":
                    Data.Auto.Add(kostItem);
                    CoefficientDataContract cdcBrandstof = worksheetset.GetCoefficient("VU_AUTO_BRANDSTOF", worksheetset.DataContract.StartDate, worksheetset.DataContract.EndDate);
                    Data.Auto.Add(new EY.com.eBook.RuleEngine.AY2013.BelastingenAppTypes.KostItem
                    {
                        Id = Guid.NewGuid()
                        ,
                        Bedrag = entity.VerworpenUitgaven
                        ,
                        UNID = "ESSENCE"
                        ,
                        Percent = cdcBrandstof.Value
                        ,
                        Saldo = entity.Bedrag.Value
                        ,
                        GrootBoek = kostItem.GrootBoek
                        ,
                        ListDescription = kostItem.ListDescription
                        ,
                        LastUpdated = DateTime.Now
                        ,
                        AdjustmentId = kostItem.AdjustmentId
                    });
                    entity.Collection = "Auto";
                    break;
                case "aandelen":
                    Data.WaardeverminderingAandelen.Add(kostItem);
                    entity.Collection = "WaardeverminderingAandelen";
                    break;
                case "agv":
                    Data.AbnormaleGoedgunstigeVoordelen.Add(kostItem);
                    entity.Collection = "AbnormaleGoedgunstigeVoordelen";
                    break;
                case "andere":
                    Data.Andere.Add(kostItem);
                    entity.Collection = "Andere";
                    break;
                case "nbelasting":
                    BelastingenAppTypes.Belasting bel = new BelastingenAppTypes.Belasting
                    {
                        AdjustmentId = kostItem.AdjustmentId
                        ,
                        ListDescription = kostItem.ListDescription
                        ,
                        GrootBoek = kostItem.GrootBoek
                        ,
                        Aanslagjaar = new StringListItemDataContract
                        {
                            Id = worksheetset.DataContract.AssessmentYear.ToString()
                            ,
                            NL = worksheetset.DataContract.AssessmentYear.ToString()
                            ,
                            FR = worksheetset.DataContract.AssessmentYear.ToString()
                            ,
                            EN = worksheetset.DataContract.AssessmentYear.ToString()
                        }
                        ,
                        Id = kostItem.Id
                        ,
                        Percent = kostItem.Percent
                        ,
                        Saldo = kostItem.Saldo
                        ,
                        Bedrag = kostItem.Bedrag
                        ,
                        LastUpdated = kostItem.LastUpdated
                        ,
                        SoortID = kostItem.SoortID
                    };
                    bel = Validate(bel, "Belastingen", ref worksheetset);
                    Data.Belastingen.Add(bel);
                    entity.Collection = "Belastingen";
                    break;
                case "bepaaldestaten":
                    Data.BepaaldeStaten.Add(kostItem);
                    entity.Collection = "BepaaldeStaten";
                    break;
                case "boete":
                    Data.Boetes.Add(kostItem);
                    entity.Collection = "Boetes";
                    break;
                case "coupon":
                    Data.VergoedingCoupon.Add(kostItem);
                    entity.Collection = "VergoedingCoupon";
                    break;
                case "fingift":
                    Data.Liberaliteiten.Add(kostItem);
                    entity.Collection = "Liberaliteiten";
                    break;
                case "gewestpremies":
                    Data.GewestPremies.Add(kostItem);
                    entity.Collection = "GewestPremies";
                    break;
                case "gift":
                    Data.Liberaliteiten.Add(kostItem);
                    entity.Collection = "Liberaliteiten";
                    break;
                case "intresten":
                    Data.OverdrevenIntresten.Add(kostItem);
                    entity.Collection = "OverdrevenIntresten";
                    break;
                case "intrestlening":
                    Data.IntrestenLeningen.Add(kostItem);
                    entity.Collection = "IntrestenLeningen";
                    break;
                case "kledij":
                    Data.Kledij.Add(kostItem);
                    entity.Collection = "Kledij";
                    break;
                case "maaltijdsportcultuureco":
                    Data.MaaltijdSportCultuurEco.Add(kostItem);
                    entity.Collection = "MaaltijdSportCultuurEco";
                    break;
                case "belasting":
                    Data.GewestBelasting.Add(kostItem);
                    entity.Collection = "GewestBelasting";
                    break;
                case "pensioenen":
                    Data.Pensioenen.Add(kostItem);
                    entity.Collection = "Pensioenen";
                    break;
                case "receptie":
                    Data.Receptie.Add(kostItem);
                    entity.Collection = "Receptie";
                    break;
                case "restaurant":
                    Data.Restaurant.Add(kostItem);
                    entity.Collection = "Restaurant";
                    break;
                case "taxshelter":
                    Data.KostenTaxShelter.Add(kostItem);
                    entity.Collection = "KostenTaxShelter";
                    break;
                case "voordeel":
                    Data.SocialeVoordelen.Add(kostItem);
                    entity.Collection = "SocialeVoordelen";
                    break;
                case "vrijstellingen":
                    Data.TerugnemingVroegereVrijstellingen.Add(kostItem);
                    entity.Collection = "TerugnemingVroegereVrijstellingen";
                    break;
                case "werknemersparticipatie":
                    Data.Werknemersparticipatie.Add(kostItem);
                    entity.Collection = "Werknemersparticipatie";
                    break;

            }
            return entity;
        }

        private void RemoveAdjustment(BelastingenAppTypes.Adjustment original, ref WorksheetSetData worksheetset)
        {
            if (original.Collection == "Belastingen")
            {
                Data.Belastingen.RemoveAll(b => b.AdjustmentId == original.Id);
            }
            else
            {
                if (!string.IsNullOrEmpty(original.Collection))
                {
                    PropertyInfo pi = typeof(BelastingenAppTypes.WPBelastingen).GetProperty(original.Collection);
                    List<BelastingenAppTypes.KostItem> prop = (List<BelastingenAppTypes.KostItem>)pi.GetValue(Data, null);
                    prop.RemoveAll(p => p.AdjustmentId == original.Id);
                }
            }
        }

        public void DeleteAdjustments(Guid rowId, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2013.BelastingenAppTypes.Adjustment original = Data.Adjustments.FirstOrDefault(e => e.Id == rowId);
            if (original != null)
            {
                RemoveAdjustment(original, ref worksheetset);

                /*
                if (original.Boeken.HasValue && original.Boeken.Value)
                {
                    worksheetset.DeleteBooking(FacturatieApp.TypeId, "OpTeMakenCN", original.Id, null);
                    // DELETE BOOKING
                }*/
                Data.Adjustments.Remove(original);
                Data.LastUpdated = DateTime.Now;
            }
        }

        public void UpdateAdjustments(EY.com.eBook.RuleEngine.AY2013.BelastingenAppTypes.Adjustment entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2013.BelastingenAppTypes.Adjustment original = Data.Adjustments.FirstOrDefault(e => e.Id == entity.Id);
            if (original == null)
            {
                AddToAdjustments(entity, ref worksheetset);
            }
            else
            {
                entity = Validate(entity, "Adjustments", ref worksheetset);
                if (entity.IsValid)
                {
                    RemoveAdjustment(original, ref worksheetset);
                    entity = AddAdjustment(entity, ref worksheetset);

                    int idx = Data.Adjustments.IndexOf(original);
                    Data.Adjustments.RemoveAt(idx);
                    Data.Adjustments.Insert(idx, entity);
                    Data.LastUpdated = DateTime.Now;
                    entity.LastUpdated = DateTime.Now;
                }
            }
        }

        public void AddToBelastingen(EY.com.eBook.RuleEngine.AY2013.BelastingenAppTypes.Belasting entity, ref WorksheetSetData worksheetset)
        {
            entity = Validate(entity, "Belastingen", ref worksheetset);
            if (entity.IsValid)
            {
                Data.LastUpdated = DateTime.Now;
                entity.LastUpdated = DateTime.Now;
                Data.Belastingen.Add(entity);
            }
        }

        public void UpdateBelastingen(EY.com.eBook.RuleEngine.AY2013.BelastingenAppTypes.Belasting entity, ref WorksheetSetData worksheetset)
        {
            EY.com.eBook.RuleEngine.AY2013.BelastingenAppTypes.Belasting original = Data.Belastingen.FirstOrDefault(e => e.Id == entity.Id);
            if (original == null)
            {
                AddToBelastingen(entity, ref worksheetset);
            }
            else
            {
                entity = Validate(entity, "Belastingen", ref worksheetset);
                if (entity.IsValid)
                {
                    int idx = Data.Belastingen.IndexOf(original);
                    Data.Belastingen.RemoveAt(idx);
                    Data.Belastingen.Insert(idx, entity);
                    Data.LastUpdated = DateTime.Now;
                    entity.LastUpdated = DateTime.Now;
                }
            }
        }

        #endregion

        #region VALIDATION

        public EY.com.eBook.RuleEngine.AY2013.BelastingenAppTypes.Adjustment Validate(EY.com.eBook.RuleEngine.AY2013.BelastingenAppTypes.Adjustment entity, string location, ref WorksheetSetData worksheetset)
        {
            entity.IsValid = true;
            worksheetset.ClearMessages(TypeId, entity.Id);

            if (!entity.Bedrag.HasValue || entity.Bedrag.Value == 0)
            {
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Bedrag");
                entity.IsValid = false;
            }
            if (entity.SoortAdjust == null)
            {
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "SoortAdjust");
                entity.IsValid = false;
            }


            return entity;
        }

        public EY.com.eBook.RuleEngine.AY2013.BelastingenAppTypes.Belasting Validate(EY.com.eBook.RuleEngine.AY2013.BelastingenAppTypes.Belasting entity, string location, ref WorksheetSetData worksheetset)
        {
            entity.IsValid = true;
            worksheetset.ClearMessages(TypeId, entity.Id);
            if (entity.UNID == "REGULATE") return entity;

            if (entity.Aanslagjaar == null)
            {
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Aanslagjaar");
                entity.IsValid = false;
            }
            if (entity.Soort == null)
            {
                worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Soort");
                entity.IsValid = false;
            }
            /*
            if (!entity.Bedrag.HasValue || entity.Bedrag.Value==0)
            {
            worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Grootboek");
            entity.IsValid = false;
            }
            */

            return entity;
        }

        public EY.com.eBook.RuleEngine.AY2013.BelastingenAppTypes.KostItem Validate(EY.com.eBook.RuleEngine.AY2013.BelastingenAppTypes.KostItem entity, string location, ref WorksheetSetData worksheetset)
        {
            entity.IsValid = true;
            worksheetset.ClearMessages(TypeId, entity.Id);
            /*
            if (!entity.Bedrag.HasValue || entity.Bedrag.Value==0)
            {
            worksheetset.SetMessage(TypeId, MessageTypeDataContract.Error, "EMPTY", location, entity.Id, "Grootboek");
            entity.IsValid = false;
            }
            */

            return entity;
        }

        #endregion

        // VALIDATION

        // CALCULATION

        // BOOKINGS

        // IMPORT PREVIOUS



    }
}

