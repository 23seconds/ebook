﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EY.com.eBook.Core.EF;
using EY.com.eBook.Core.EF.Write;
using EY.com.eBook.Core;
using System.Data.Entity;
using System.Xml.Linq;
using EY.com.eBook.API.Contracts;
using System.Text.RegularExpressions;

namespace FixPrefixXbrlBiztax2014
{
    public class Program
    {
        static void Main(string[] args)
        {
            FixPrefix(args);
            //GetCountOnCorruptedFiles();
        }

        public static void GetCountOnCorruptedFiles(){
            List<Guid> files = eBookReadManager.Context.Files.ToList().Where(f =>f.EndDate.AddDays(1).Year == 2014).Select(f => f.Id).ToList();
            int cntFiles = eBookReadManager.Context.Files.ToList().Where(f =>f.EndDate.AddDays(1).Year == 2014).Count();
            int cntXbrl = 0;
            int cntMissingPrefixXbrl =0;
            foreach (Guid f in files){
                FileXbrl lfx = eBookWriteManager.Context.FileXbrlSet.FirstOrDefault(x => x.FileId == f);

                if (lfx != null)
                {
                    if (lfx.XbrlData != null)
                    {
                        cntXbrl++;
                        int count = Regex.Matches(lfx.XbrlData, "<d2p1:Prefix i:nil=\"true\" />").Count;
                        if (count > 0)
                        {
                            cntMissingPrefixXbrl++;
                        }
                    }
                }
            }
            Console.WriteLine("# Files: " + cntFiles);
            Console.WriteLine("# Xbrl's: " + cntXbrl);
            Console.WriteLine("# Xbrl's with missing prefix: " + cntMissingPrefixXbrl);

            Console.ReadLine();
        }

        public static void FixPrefix(string[] args) { 
            int amount = 400;
            int skip = 0;
            int throttle = 100;
            int cnt = 0;
            

            if (args != null && args.Length > 0)
            {
                int.TryParse(args[0], out skip);
                cnt = skip;
            }
             
            int ttl = 0;

            // Get file ids
            Console.WriteLine("Getting filelist");

            // SINGLE FILE
            //Guid fileId = new Guid("4AB816C0-348D-404C-8F32-9289B3A83672");
            //List<Guid> files = eBookReadManager.Context.Files.Where(f =>f.Id == fileId).Select(f => f.Id).ToList();
            //ttl = eBookReadManager.Context.Files.Where(f =>f.Id == fileId).Count();

            // ALL FILES 2014
            List<Guid> files = eBookReadManager.Context.Files.Include("Client").ToList().Where(f => f.Client != null && f.EndDate.AddDays(1).Year == 2014).Select(f => f.Id).Skip(skip).Take(amount).ToList();
            ttl = eBookReadManager.Context.Files.Include("Client").ToList().Where(f => f.Client != null && f.EndDate.AddDays(1).Year == 2014).Count();

           
            foreach (Guid f in files){
                cnt++;

                FileXbrl lfx = eBookWriteManager.Context.FileXbrlSet.FirstOrDefault(x => x.FileId == f);
                Console.WriteLine(cnt + " / " + ttl + " ");
                if (lfx != null)
                {
                    if (lfx.XbrlData != null)
                    {

                        int count = Regex.Matches(lfx.XbrlData, "<d2p1:Prefix i:nil=\"true\" />").Count;
                        string updatedXBRLdata = "";
                        if (count > 0)
                        {
                            Console.Write(" -> " + count + " x found");
                            updatedXBRLdata = lfx.XbrlData.Replace("<d2p1:Prefix i:nil=\"true\" />", "<d2p1:Prefix>pfs-gcd</d2p1:Prefix>");
                            lfx.XbrlData = updatedXBRLdata;
                            eBookWriteManager.Context.SaveChanges();

                        }
                        
                    }
                }
                
            }            
        }
        }
    }

