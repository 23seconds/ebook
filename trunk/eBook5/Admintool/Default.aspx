﻿<%@ Page Language="C#" AutoEventWireup="true"  CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <script type="text/javascript">
        var uid = '<%=Context.User.Identity.Name.Replace(@"\",@"\\") %>';
    </script>
    <title>eBook Admintool</title>
    <link rel="Stylesheet" type="text/css" href="ext-4.1.1a/resources/css/ext-all-gray.css" />
    <link rel="Stylesheet" type="text/css" href="css/eBookAT.css" />
    <link rel="Stylesheet" type="text/css" href="css/eBookAT-icons.css" />
    <link rel="Stylesheet" type="text/css" href="ext-4.1.1a/src/ux/css/CheckHeader.css" />
    
</head>
<body>
<div id="eBookAT-mask"></div>
		<div id="eBookAT-startload" class="eBookAT-masked-msg-container">&nbsp;
			<div class="eBookAT-masked-msg-centered">
				<div id="eBookAT-splash" class="eBookAT-masked-msg-window">
					<div id="eBookAT-splash-header" class="eBookAT-masked-msg-header">
						<img height="42" width="185" alt="Ernst &amp; Young" src="images/ey/eyLogo-en.png" class="eylogo" title="Site name home" id="Img1">
					</div>
					<div id="eBookAT-icon" class="eBookAT-masked-msg-icon"></div>
					<div id="eBookAT-startload-title">eBook Admintool</div>
					<div id="eBookAT-startload-loader">
						<div id="eBookAT-start-loader-loading"></div>
						<div id="eBookAT-start-loader-text">Loading ExtJS...</div>
					</div>
				</div>
 			</div>
 		</div>

    <!--div id="PMT-mask"></div>
    <div id="PMT-startload" class="PMT-masked-msg-container">&nbsp;
			<div class="PMT-masked-msg-centered">
				<div id="PMT-splash" class="PMT-masked-msg-window">
					<div id="PMT-splash-header" class="PMT-masked-msg-header">
						<img height="42" width="185" alt="Ernst &amp; Young" src="images/ey/eyLogo-en.png" class="eylogo" title="Site name home" id="ImgHome">
					</div>
					<div id="PMT-icon" class="PMT-masked-msg-icon"></div>
					<div id="PMT-startload-title">Admintool</div>
					<div id="PMT-startload-loader">
						<div id="PMT-start-loader-loading"></div>
						<div id="PMT-start-loader-text">Building interface...</div>
					</div>
				</div>
 			</div>
 		</div-->
    <%Response.Flush(); %>
    
    <script type="text/javascript" src="ext-4.1.1a/ext-all.js"></script>
    <script type="text/javascript" src="app.js"></script>
    
    
</body>
</html>
