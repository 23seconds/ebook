﻿Ext.define('eBookAT.view.DefaultWindow', {
    extend: 'Ext.window.Window',
    alias: 'widget.defaultwindow',

    requires: [],
    
    iconCls: 'eBookAT-defaultwindow',
    
    initComponent: function() {
        var vpSize = Ext.getBody().getViewSize();
        var hg = vpSize.height * 0.9;
        var wd = vpSize.width * 0.9;


        Ext.apply(this, {
            height: this.height ? this.height : hg,
            width: this.width ? this.width : wd            
        });

        this.callParent(arguments);
    }

});
