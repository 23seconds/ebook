﻿Ext.define('eBookAT.view.person.SearchPerson', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.searchperson',

    store: 'Person',
    scroll: false,
    viewConfig: {
        style: { overflow: 'auto', overflowX: 'hidden' }
    },
    controller: undefined,
    initComponent: function() {
        Ext.apply(this, {
            loadMask: true,
            columns: [{
                text: 'First name',
                flex: 1,
                dataIndex: 'FirstName',
                sortable: true
            }, {
                text: 'Last name',
                flex: 1,
                dataIndex: 'LastName',
                sortable: true
            }],

            dockedItems: [{
                xtype: 'toolbar',
                dock: 'top',
                items: ['Search', {
                    xtype: 'textfield',
                    name: 'searchField',
                    hideLabel: true,
                    width: 200,
                    listeners: {
                        change: {
                            fn: this.onTextFieldChange,
                            scope: this,
                            buffer: 500
                        }
                    }
}]
                }, {
                    xtype: 'pagingtoolbar',
                    store: this.store,   // same store GridPanel is using
                    dock: 'bottom',
                    displayInfo: true
                }
			]
            });

            this.callParent(arguments);
        },

        onTextFieldChange: function(txtfld, newValue, oldValue, eOpts) {
            var search = this.down('textfield[name=searchField]').getValue().toLowerCase();
            var person = Ext.encode(eBookAT.User.id);

            var cbdc = { cbdc: { 'Start': 0, 'Limit': 25, 'Query': null} }; //this.store.lastParams;

            cbdc.cbdc.Query = !Ext.isEmpty(search) && search.length > 0 ? search : null;
            this.store.load({ params: cbdc });
            /*
            switch (txtfld.up('window').type) {
                case 'files':
                    var fileStore = txtfld.up('window').down('filegrid').store
                    fileStore.removeAll();
                    break;

                case 'biztax':
                    var biztaxStore = txtfld.up('window').down('filebiztaxgrid').store;
                    biztaxStore.removeAll();
                    break;
            }

            var search = this.down('textfield[name=searchField]').getValue().toLowerCase();
            var person = Ext.encode(eBookAT.User.id);

            var ccdc = { ccdc: { 'Start': 0, 'Limit': 25, 'Query': null} }; //this.store.lastParams;

            ccdc.ccdc.Query = !Ext.isEmpty(search) && search.length > 0 ? search : null;
            this.store.load({ params: ccdc });
            */
        }

    });