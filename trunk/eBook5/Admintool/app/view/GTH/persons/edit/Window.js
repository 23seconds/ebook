﻿Ext.define('eBookAT.view.GTH.persons.edit.Window', {
    extend: 'eBookAT.view.DefaultWindow',
    alias: 'widget.personeditwindow',

    requires: [

              ],

    title: 'Edit person',

    layout: 'fit',
    bbar: [
        { xtype: 'button', itemId: 'personEditCancel', text: 'Cancel', iconCls: 'eBookAT-icon-cancel-32', scale: 'large', iconAlign: 'left' }, '->',
        { xtype: 'button', itemId: 'personEditSave', text: 'Save', iconCls: 'eBookAT-icon-save-32', scale: 'large', iconAlign: 'left' }
    ],



        initComponent: function() {
        var personGrid = Ext.ComponentQuery.query('gthpersongrid')[0];
        var record = personGrid.getSelectionModel().getSelection()[0].data;

        Ext.apply(this, {
            height: 250,
            width: 300,

            items: [
                { xtype: 'panel',
                    align: 'fit',
                    flex: 1,
                    items: [
                        {
                            xtype: 'textfield',
                            name: 'GPN',
                            fieldLabel: 'GPN',
                            cls: 'eBookAT-PersonAddWindow-Field eBookAT-PersonAddWindow-Field-first',
                            value: record.GPN,
                            disabled: true
                        },
                        {
                            xtype: 'textfield',
                            name: 'FirstName',
                            fieldLabel: 'First name',
                            cls: 'eBookAT-PersonAddWindow-Field',
                            value: record.FirstName,
                        },
                        {
                            xtype: 'textfield',
                            name: 'LastName',
                            fieldLabel: 'Last name',
                            cls: 'eBookAT-PersonAddWindow-Field',
                            value: record.LastName,
                        },
                        {
                            xtype: 'textfield',
                            name: 'Email',
                            fieldLabel: 'Email',
                            cls: 'eBookAT-PersonAddWindow-Field',
                            vtype: 'email',
                            value: record.Email,
                        }
                    ]
                }

            ]
        });

        Ext.apply(Ext.form.field.VTypes, {
            GPN: function(val, field) {
                var substring = val.substring(0, 2);
                if (substring == 'IN') {
                    return true
                }
            },


            GPNText: 'GPN must start with "IN"'
        });
        Ext.tip.QuickTipManager.init();
        this.callParent(arguments);
    }
});