﻿Ext.define('eBookAT.view.GTH.persons.add.Window', {
    extend: 'eBookAT.view.DefaultWindow',
    alias: 'widget.personaddwindow',

    requires: [

              ],

    title: 'Add a new person',

    period: null,

    layout: 'fit',
    bbar: [
        { xtype: 'button', itemId: 'personAddCancel', text: 'Cancel', iconCls: 'eBookAT-icon-cancel-32', scale: 'large', iconAlign: 'left' }, '->',
        { xtype: 'button', itemId: 'personAddSave', text: 'Save', iconCls: 'eBookAT-icon-save-32', scale: 'large', iconAlign: 'left' }
    ],



    initComponent: function() {

        Ext.apply(this, {
            height: 250,
            width: 300,

            items: [
                { xtype: 'panel',
                    align: 'fit',
                    flex: 1,
                    items: [
                        {
                            xtype: 'textfield',
                            name: 'GPN',
                            fieldLabel: 'GPN',
                            cls: 'eBookAT-PersonAddWindow-Field eBookAT-PersonAddWindow-Field-first',
                            value: 'IN',
                            vtype: 'GPN'
                        },
                        {
                            xtype: 'textfield',
                            name: 'FirstName',
                            fieldLabel: 'First name',
                            cls: 'eBookAT-PersonAddWindow-Field'
                        },
                        {
                            xtype: 'textfield',
                            name: 'LastName',
                            fieldLabel: 'Last name',
                            cls: 'eBookAT-PersonAddWindow-Field'
                        },
                        {
                            xtype: 'textfield',
                            name: 'Email',
                            fieldLabel: 'Email',
                            cls: 'eBookAT-PersonAddWindow-Field',
                            vtype: 'email'
                        }
                    ]
                }

            ]
        });

        Ext.apply(Ext.form.field.VTypes, {
            GPN: function(val, field) {
                var substring = val.substring(0, 2);
                if (substring == 'IN') {
                    return true
                }
            },


            GPNText: 'GPN must start with "IN"'
        });
        Ext.tip.QuickTipManager.init();
        this.callParent(arguments);
    }
});