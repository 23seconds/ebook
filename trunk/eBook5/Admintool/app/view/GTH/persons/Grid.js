﻿Ext.define('eBookAT.view.GTH.persons.Grid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.gthpersongrid',

    store: 'GTHPerson',

    initComponent: function() {
        Ext.apply(this, {
            loadMask: true,
            title: 'Persons',
            tbar: [
                { xtype: 'button', itemId: 'personAdd', type: 'gthpersonbutton', action: 'add', text: 'Add person', iconCls: 'eBookAT-icon-add-16' },
                { xtype: 'button', itemId: 'personEdit', type: 'gthpersonbutton', action: 'edit', text: 'Edit person', iconCls: 'eBookAT-icon-edit-16', disabled:true }, '->',
                'Search',{ xtype: 'textfield',
                    name: 'searchField',
                    hideLabel: true,
                    width: 200,
                    fieldCls: 'eBookAT-Person-Grid-Search',
                    listeners: {
                        change: {
                            fn: this.onTextFieldChange,
                            scope: this
                        }
                    }
                }
            ],
            columns: [{
                text: 'GPN',
                flex: 1,
                dataIndex: 'GPN'
                //editor: 'textfield'
            },
                {
                    text: 'First name',
                    flex: 1,
                    dataIndex: 'FirstName',
                    sortable: true
                },
                {
                    text: 'Last name',
                    flex: 1,
                    dataIndex: 'LastName',
                    sortable: true
                },
                {
                    text: 'Email',
                    flex: 1,
                    dataIndex: 'Email',
                    sortable: true
                }
            ]
            //            ,
            //            bbar: Ext.create('Ext.PagingToolbar', {
            //                store: 'Person',
            //                displayInfo: true,
            //                displayMsg: 'Displaying persons {0} - {1} of {2}',
            //                emptyMsg: "No topics to display"                
            //            })
        });

        this.callParent(arguments);
    },

    onTextFieldChange: function() {
        var search = this.down('textfield[name=searchField]').getValue().toLowerCase();
        var store = this.store;
        store.clearFilter();
        //alert(search);
        store.filterBy(function(record) {
        if ((record.get('FirstName').toLowerCase().indexOf(search) >= 0) || (record.get('LastName').toLowerCase().indexOf(search) >= 0) || record.get('GPN').toLowerCase().indexOf(search) >= 0) { return true }
            //return (record.get('FirstName') == 'ARTI');
        });
    }


});