﻿Ext.define('eBookAT.view.GTH.persons.Window', {
    extend: 'eBookAT.view.DefaultWindow',
    alias: 'widget.gthpersonwindow',

    requires: [
                'eBookAT.view.GTH.persons.Grid'
              ],

    title: 'GTH manage persons',

    layout: {
        type: 'fit',
        align: 'stretch'
    },

    items: [
        { xtype: 'gthpersongrid', flex: 1, cls:'eBookAT-GTH-Persongrid'}
    ],

    initComponent: function() {

        this.callParent(arguments);
    }

});