﻿Ext.define('eBookAT.view.GTH.replacement.Window', {
    extend: 'eBookAT.view.DefaultWindow',
    alias: 'widget.gthreplacementwindow',

    requires: [
                    'eBookAT.view.GTH.replacement.GridRoles',
                    'eBookAT.view.GTH.replacement.GridPersons'
              ],

    title: 'GTH replacements',

    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    bbar: [
        '->',{ xtype: 'button', itemId: 'replacePersons', text: 'Replace', iconCls: 'eBookAT-icon-save-32', scale: 'large', iconAlign: 'left' }
    ],

    items: [
        { xtype: 'gthrolesgrid', action: 'CopyFrom', flex: 1, cls: 'eBookAT-replace-panel' },
        
        { xtype: 'container', /*title: 'TO', cls: 'eBookAT-replace-panel',*/ flex: 2, layout: { type: 'hbox', align: 'stretch' }, items: [{ xtype: 'gthreplacementpersonsgrid', store: 'PersonReplacementA', type:'From', flex: 1, cls: 'eBookAT-replace-panel', title:'Person X' }, { xtype: 'gthreplacementpersonsgrid', store: 'PersonReplacementB', type:'To', flex: 1, cls: 'eBookAT-replace-panel', title:'Person Y'}] }
    ],

    initComponent: function() {


        this.callParent(arguments);
    }

});