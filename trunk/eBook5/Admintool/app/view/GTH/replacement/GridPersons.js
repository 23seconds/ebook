﻿Ext.define('eBookAT.view.GTH.replacement.GridPersons', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.gthreplacementpersonsgrid',

    

    initComponent: function() {
        Ext.apply(this, {
            loadMask: true,
            tbar: [
                'Search',{ xtype: 'textfield',
                    name: 'searchField',
                    hideLabel: true,
                    width: 200,
                    fieldCls: 'eBookAT-Person-Grid-Search',
                    listeners: {
                        change: {
                            fn: this.onTextFieldChange,
                            scope: this
                        }
                    }
                }
            ],
            columns: [
                {
                    text: 'First name',
                    flex: 1,
                    dataIndex: 'FirstName',
                    sortable: true
                    //                    renderer: function(value, p, r) { 
                    //                        return r.data['FirstName'] + ' ' + r.data['LastName']
                    //                    }
                },
                {
                    text: 'Last name',
                    flex: 1,
                    dataIndex: 'LastName',
                    sortable: true

                },
                {
                    text: 'Clients in progress with',
                    flex: 1,
                    dataIndex: 'ActiveClientCount',
                    sortable: true

                }
        ]
        });

        this.callParent(arguments);
    },

    onTextFieldChange: function() {
        var search = this.down('textfield[name=searchField]').getValue().toLowerCase();
        var store = this.store;
        store.clearFilter();
        //alert(search);
        store.filterBy(function(record) {
            if ((record.get('FirstName').toLowerCase().indexOf(search) >= 0) || (record.get('LastName').toLowerCase().indexOf(search) >= 0)) { return true }
            //return (record.get('FirstName') == 'ARTI');
        });
    }


});