﻿Ext.define('eBookAT.view.GTH.replacement.GridRoles', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.gthrolesgrid',

    store: 'GTHRoles',
    title: 'Teams',
    
    columns: [
        {
            text: 'Name',
            flex: 1,
            dataIndex: 'Role',
            sortable: true                    
        }
    ],
    initComponent: function() {
        

        this.callParent(arguments);
    }
});