﻿Ext.define('eBookAT.view.GTH.roles.DataView', {
    extend: 'Ext.view.View',
    alias: 'widget.gthpersonrolesdataview',
    
    
    initComponent: function() {
    var imageTpl = new Ext.XTemplate(
            '<tpl for=".">',
                '<div class="roles"><input type="checkbox" class="eBootAT-Roles" name="Roles" value="{Role}" <tpl if="Check==true">Checked</tpl>> {Role}</div>',                
            '</tpl>'
        );
        Ext.apply(this, {
            store: 'PersonRoles',
    
            itemSelector: 'div.roles',
            
            tpl: imageTpl
            
        });
        this.callParent(arguments);
    }
    
    
});