﻿Ext.define('eBookAT.view.GTH.roles.Grid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.gthpersonrolesgrid',

    store: 'GTHPerson',

    initComponent: function() {
        Ext.apply(this, {
            loadMask: true,
            title: 'Persons',
            tbar: [ 'Search',
                { xtype: 'textfield',
                    name: 'searchField',
                    hideLabel: true,
                    width: 200,
                    fieldCls: 'eBookAT-Person-Grid-Search',
                    listeners: {
                        change: {
                            fn: this.onTextFieldChange,
                            scope: this
                        }
                    }
                }
            ],
            columns: [
                {
                    text: 'Name',
                    flex: 1,
                    dataIndex: 'FirstName' ,
                    sortable: true
                },
                {
                    text: 'Name',
                    flex: 1,
                    dataIndex: 'LastName',
                    sortable: true                    
                }
        ]
        });

        this.callParent(arguments);
    },

    onTextFieldChange: function() {
        var search = this.down('textfield[name=searchField]').getValue().toLowerCase();
        var store = this.store;
        store.clearFilter();
        store.filterBy(function(record) {
            if ((record.get('FirstName').toLowerCase().indexOf(search) >= 0) || (record.get('LastName').toLowerCase().indexOf(search) >= 0)) { return true }
        });
    }


});