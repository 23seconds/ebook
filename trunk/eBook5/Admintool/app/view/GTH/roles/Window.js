﻿Ext.define('eBookAT.view.GTH.roles.Window', {
    extend: 'eBookAT.view.DefaultWindow',
    alias: 'widget.gthroleswindow',

    requires: [
                'eBookAT.view.GTH.roles.Grid',
                'eBookAT.view.GTH.roles.DataView'
              ],

    title: 'GTH manage roles',

    layout: {
        type: 'hbox',
        align:'stretch'
    },

    items: [
        { xtype: 'gthpersonrolesgrid', flex: 1, cls: 'eBookAT-GTH-Rolesgrid' },
        { xtype: 'panel', title: 'Roles', items: [{ xtype: 'gthpersonrolesdataview'}], flex: 1, cls: 'eBookAT-Roles-Dataview' }
    ],
        height: 400,
    width: 600,

        initComponent: function() {
        

        this.callParent(arguments);
    }

});