﻿Ext.define('eBookAT.view.file.close.Window', {
    extend: 'eBookAT.view.DefaultWindow',
    alias: 'widget.fileclosewindow',

    requires: ['eBookAT.view.client.SearchClient',
                'eBookAT.view.file.FileGrid',
                'eBookAT.view.file.FilesToProcessGrid'],

    title: 'Close files',
    layout: {
        type: 'vbox',
        align: 'stretch'
    },

    initComponent: function() {
        this.action = 'close';
        this.type = 'files';
        Ext.apply(this, {
            items: [{ xtype: 'searchclient', title: 'Clients', cls: 'eBookAT-replace-panel', flex: 1.2, autoScroll: true },
                    { xtype: 'container', flex: 1, layout: { type: 'hbox', align: 'stretch' }, items: [
                        { xtype: 'filegrid', title: 'Files', cls: 'eBookAT-replace-panel', flex: 1 },
                        { xtype: 'container', layout: { type: 'vbox', align: 'center', pack: 'center' }, items: [
                            { xtype: 'button', itemId: 'addFileToList', iconCls: 'eBookAT-icon-addToList-32', height: 40, width: 40, scale: 'large', text: null, disabled: true}]
                        },
                        { xtype: 'filestoprocessgrid', title: 'Files to close', cls: 'eBookAT-replace-panel', flex: 1, yearEndService: true}]
}],

            bbar: ['->',
                    { xtype: 'button', itemId: 'fileClose', text: 'Close files', iconCls: 'eBookAT-icon-save-32', scale: 'large', iconAlign: 'top'}]
        });

        this.callParent(arguments);
    }

});
