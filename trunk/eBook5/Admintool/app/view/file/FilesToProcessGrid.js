﻿Ext.define('eBookAT.view.file.FilesToProcessGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.filestoprocessgrid',

    store: 'FilesToProcess',

    initComponent: function () {
        var test = this.yearEndService;
        Ext.apply(this, {
            loadMask: true,
            columns: [{
                text: 'Client',
                flex: 2,
                dataIndex: 'ClientName'
            }, {
                text: 'Name',
                flex: 2,
                dataIndex: 'Name'
            }, {
                text: 'Start date',
                flex: 1,
                dataIndex: 'StartDate',
                renderer: function (value) { return Ext.Date.format(Ext.Date.parse(value, 'MS'), 'm/d/Y') }
            }, {
                text: 'End date',
                flex: 1,
                dataIndex: 'EndDate',
                renderer: function (value) { return Ext.Date.format(Ext.Date.parse(value, 'MS'), 'm/d/Y') }
            }, {
                xtype: 'checkcolumn',
                text: 'Open Year-end Service?',
                align: 'center',
                flex: 2,
                dataIndex: 'YearEndService',
                stopSelection: false,
                hidden: this.yearEndService
            }]
        });

        this.callParent(arguments);
    }


});