﻿Ext.define('eBookAT.view.file.FileGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.filegrid',

    store: 'File',

    initComponent: function() {
        Ext.apply(this, {
            loadMask: true,
            columns: [{
                text: 'Name',
                flex: 2,
                dataIndex: 'Name'
            }, {
                text: 'Start date',
                flex: 1,
                dataIndex: 'StartDate',
                renderer: function(value) { return Ext.Date.format(Ext.Date.parse(value, 'MS'), 'm/d/Y') }
            }, {
                text: 'End date',
                flex: 1,
                dataIndex: 'EndDate',
                renderer: function(value) { return Ext.Date.format(Ext.Date.parse(value, 'MS'), 'm/d/Y') }
            }]
        });

        this.callParent(arguments);
    }


});