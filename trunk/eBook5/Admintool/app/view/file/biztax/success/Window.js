﻿Ext.define('eBookAT.view.file.biztax.success.Window', {
    extend: 'eBookAT.view.DefaultWindow',
    alias: 'widget.filebiztaxsuccesswindow',

    requires: ['eBookAT.view.client.SearchClient',
                'eBookAT.view.file.biztax.BiztaxGrid',
                'eBookAT.view.file.biztax.BiztaxToProcessGrid'],

    title: 'Mark biztax declarations as success',
    layout: {
        type: 'vbox',
        align: 'stretch'
    },

    initComponent: function() {
        this.action = 'success';
        this.type = 'biztax';
        this.status = 99;
        Ext.apply(this, {
            items: [{ xtype: 'searchclient', title: 'Clients', cls: 'eBookAT-replace-panel', flex: 1.2, autoScroll: true },
                    { xtype: 'container', flex: 1, layout: { type: 'hbox', align: 'stretch' }, items: [
                        { xtype: 'filebiztaxgrid', title: 'Biztax declarations', cls: 'eBookAT-replace-panel', flex: 1 },
                        { xtype: 'container', layout: { type: 'vbox', align: 'center', pack: 'center' }, items: [
                            { xtype: 'button', itemId: 'addFileToList', iconCls: 'eBookAT-icon-addToList-32', height: 40, width: 40, scale: 'large', text: null, disabled: true}]
                        },
                        { xtype: 'filebiztaxtoprocessgrid', title: 'Mark biztax declarations as success', cls: 'eBookAT-replace-panel', flex: 1}]
}],

            bbar: [{ xtype: 'label', html: '<div style="background-color:#F5A9A9; width:30px; height:15px;display: inline-block;margin-left: 10px;border: 1px solid black;"></div> : Failed' },
{ xtype: 'label', html: '<div style="background-color:#D0F5A9; width:30px; height:15px;display: inline-block;margin-left: 10px;border: 1px solid black;"></div> : Success' },
        { xtype: 'label', html: '<div style="background-color:#81BEF7; width:30px; height:15px;display: inline-block;margin-left: 10px;border: 1px solid black;"></div> : Send to be declared' },

            '->',
                    { xtype: 'button', itemId: 'biztaxSuccess', text: 'Change status', iconCls: 'eBookAT-icon-save-32', scale: 'large', iconAlign: 'top'}]
        });

        this.callParent(arguments);
    }

});
