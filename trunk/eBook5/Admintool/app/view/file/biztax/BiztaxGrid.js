﻿Ext.define('eBookAT.view.file.biztax.BiztaxGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.filebiztaxgrid',

    store: 'BiztaxDeclaration',

    initComponent: function() {
        Ext.apply(this, {
            loadMask: true,
            columns: [{
                text: 'Department',
                flex: 1,
                dataIndex: 'Department'
            }, {
                text: 'Partner name',
                flex: 2,
                dataIndex: 'PartnerName'
            }, {
                text: 'Assessment year',
                //flex: 1,
                dataIndex: 'AssessmentYear'
            }, {
                text: 'Sender name',
                flex: 2,
                dataIndex: 'SendToDeclareBy'
            }, {
                text: 'Sender date',
                flex: 1,
                dataIndex: 'SendToDeclareDate',
                renderer: function(value) { return Ext.Date.format(Ext.Date.parse(value, 'MS'), 'm/d/Y') }
            }],
            
            viewConfig: {                
                enableRowBody: true, // required to create a second, full-width row to show expanded Record data
                getRowClass: function(record, rowIndex, rp, ds){ // rp = rowParams
                    switch (record.get('Status')){
                        case 10:
                           return 'blue';
                           break;
                        case -99:
                           return 'red';
                           break;
                        case 99:
                           return 'green';
                           break;
                    }
                }
            }
            });

            this.callParent(arguments);
        }


    });