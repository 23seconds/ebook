﻿Ext.define('eBookAT.view.file.biztax.BiztaxToProcessGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.filebiztaxtoprocessgrid',

    store: 'BiztaxDeclarationsToProcess',

    initComponent: function() {
        Ext.apply(this, {
            loadMask: true,
            columns: [{
                text: 'Client',
                flex: 2,
                dataIndex: 'ClientName'
            }, {
                text: 'Department',
                flex: 2,
                dataIndex: 'Department'
            }, {
                text: 'Partner',
                flex: 1,
                dataIndex: 'PartnerName'
            }, {
                text: 'Assessment year',
                flex: 1,
                dataIndex: 'AssessmentYear'
}],

                viewConfig: {
                    enableRowBody: true, // required to create a second, full-width row to show expanded Record data
                    getRowClass: function(record, rowIndex, rp, ds) { // rp = rowParams
                        switch (record.get('Status')) {
                            case 10:
                                return 'blue';
                                break;
                            case -99:
                                return 'red';
                                break;
                            case 99:
                                return 'green';
                                break;
                        }
                    }
                },

                tbar: [
                {xtype:'button', itemId:'declarationDelete', text:'Delete from list', iconCls:'eBookAT-icon-delete-16', disabled: true}
            ]

            });

            this.callParent(arguments);
        }


    });