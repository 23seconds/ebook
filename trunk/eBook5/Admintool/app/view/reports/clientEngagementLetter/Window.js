﻿Ext.define('eBookAT.view.reports.clientEngagementLetter.Window', {
    extend: 'eBookAT.view.DefaultWindow',
    alias: 'widget.clientengagementletterwindow',
    id: 'clientEngagementLetterWindow',

    requires: [
                'eBookAT.view.reports.clientEngagementLetter.Grid',
                'eBookAT.view.reports.clientEngagementLetter.CriteriaContainer'
              ],

    title: 'Client - Engagement letter Report',

    layout: {
        type: 'vbox',
        align: 'stretch'
    },

    items: [
        { xtype: 'clientengagementlettercriteria', flex: 1 },
        { xtype: 'clientengagementlettergrid', flex: 9, cls: 'eBookAT-GTH-Persongrid' }
    ],

    initComponent: function() {

        this.callParent(arguments);
    }

});