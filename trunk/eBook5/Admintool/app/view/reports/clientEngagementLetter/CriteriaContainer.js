﻿Ext.define('eBookAT.view.reports.clientEngagementLetter.CriteriaContainer', {
    extend: 'Ext.container.Container',
    alias: 'widget.clientengagementlettercriteria',


    initComponent: function() {
        Ext.apply(this, {
            loadMask: true,
            title: 'Criteria',
            
            layout: {
                type: 'table',
                columns: 4
            },
           
            items: [{
                        xtype: 'datefield',
                        fieldLabel: 'Start date',
                        html: '1,1' ,
                        margin: '10, 50, 5, 10'                
                    },
                    {
                        xtype: 'combobox',
                        group: 'EL',
                        fieldLabel: 'Status',
                        store: 'ClientEngagementLetterStatus',
                        displayField: 'NL',
                        valueField: 'Id',
                        html: '1,2' ,
                        margin: '10, 50, 5, 10',
                        tpl:'<tpl for=".">' + '<li class="x-boundlist-item" role="option">' + '{NL}&nbsp;' + '</li></tpl> '       ,
                        queryMode:'local'          
                    },
                    /*
                    {
                        xtype: 'checkboxfield',
                        //itemId: 'department',
                        boxLabel: 'ACR',
                        name: 'acr',
                        html: '1,3',
                        margin: '5, 50, 5, 10'
                    },
                    */
                    {
                        xtype: 'button',
                        text: 'Search',
                        itemId: 'searchBtn',
                        cls: 'report-search-btn',
                        iconCls: 'eBookAT-icon-search-32',
                        scale: 'large',
                        html: '1,3',
                        rowspan: 2,
                        margin: '10, 20, 10, 0'  
                    },
                    {
                        xtype: 'button',
                        text: 'Export',
                        itemId: 'exportBtn',
                        cls: 'report-search-btn',
                        iconCls: 'eBookAT-icon-excel-32',
                        scale: 'large',
                        html: '1,4',
                        rowspan: 2,
                        margin: '10, 0, 10, 0'  
                    },
                    {
                        xtype: 'datefield',
                        fieldLabel: 'End date',
                        html: '2,1',
                        padding: '5, 0, 5, 10'
                    },
                    {
                        xtype: 'combobox',
                        group: 'EL',
                        fieldLabel: 'Template',
                        store: 'ClientEngagementLetterTemplate',
                        displayField: 'NL',
                        valueField: 'Id',
                        html: '2,2',
                        padding: '5, 50, 5, 10',
                        width: 400,
                        tpl:'<tpl for=".">' + '<li class="x-boundlist-item" role="option">' + '{NL}&nbsp;' + '</li></tpl>'  ,
                        queryMode:'local'   
                    }
                    /*,
                    {
                        xtype: 'checkboxfield',
                        //itemId: 'department',
                        boxLabel: 'TAX',                        
                        name: 'tax',
                        html: '2,3',
                        margin: '5, 50, 5, 10'
                    }
                    */
                    
            ]            
        });

        this.callParent(arguments);
    },
});