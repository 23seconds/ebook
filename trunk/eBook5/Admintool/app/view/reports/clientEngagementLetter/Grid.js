﻿Ext.define('eBookAT.view.reports.clientEngagementLetter.Grid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.clientengagementlettergrid',

    store: 'ClientEngagementLetter',

    initComponent: function() {
        Ext.apply(this, {
            loadMask: true,
            title: 'Clients',
//            dockedItems:[
//                {
//                    xtype: 'toolbar',
//                    dock: 'top',
//                    cls: 'testcls',
//                    items:[
//                        {
//                            xtype: 'datefield',
//                            fieldLabel: 'Start date'
//                            
//                        },'-',
//                        {
//                            xtype: 'combobox',
//                            fieldLabel: 'Status',
//                            store: 'ClientEngagementLetterStatus',
//                            displayField: 'NL',
//                            valueField: 'Id'
//                            
//                        },
//                        {
//                            xtype: 'button',
//                            text: 'Search',
//                            id: 'celBtn',
//                            cls: 'testbtncls'
//                        }
//                    ]
//                },
//                {
//                    xtype: 'toolbar',
//                    dock: 'top',
//                    cls: 'testcls',
//                    items:[
//                        {
//                            xtype: 'datefield',
//                            fieldLabel: 'End date'
//                        },'-',
//                        {
//                            xtype: 'combobox',
//                            fieldLabel: 'Template',
//                            store: 'ClientEngagementLetterTemplate',
//                            displayField: 'NL',
//                            valueField: 'Id'
//                        }
//                    ]
//                }
//            ],
           
            columns: [{
                    text: 'Client',
                    flex: 1,
                    dataIndex: 'Name'
                    //editor: 'textfield'
                },
                {
                    text: 'Status',
                    flex: 1,
                    dataIndex: 'Status'
                    //editor: 'textfield'
                },
                {
                    text: 'Template',
                    flex: 1,
                    dataIndex: 'Template',
                    renderer: function(value) {
                        var obj = JSON.parse(value) ;
                        return obj.nl;
                    }
                },
                {
                    text: 'Start date',
                    flex: 1,
                    dataIndex: 'StartDate',
                    renderer: function(value) { return Ext.Date.format(Ext.Date.parse(value, 'MS'), 'm/d/Y') }
                },
                {
                    text: 'End date',
                    flex: 1,
                    dataIndex: 'EndDate',
                    renderer: function(value) { return Ext.Date.format(Ext.Date.parse(value, 'MS'), 'm/d/Y') }
                    //editor: 'textfield'
                },
                {
                    text: 'Office',
                    flex: 1,
                    dataIndex: 'Office'
                }                    
            ]            
        });

        this.callParent(arguments);
    },
});