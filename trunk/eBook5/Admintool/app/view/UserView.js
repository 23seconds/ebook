﻿Ext.define('eBookAT.view.UserView', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.userview',

    requires: [
        'eBookAT.view.TabPanel'
	],
    autoScroll: true,
    layout: {
        type: 'card',
        //padding:'5',
        align: 'stretch'
    },
    layoutConfig: {
        animate: true
    },

    initComponent: function() {
        Ext.apply(this, {

            items: [
                { xtype: 'ebookattabpanel', id: 'EBOOKATtab', border: false, padding: '0 12 10 12'}
			]


        });
        this.callParent(arguments);
    }



});
