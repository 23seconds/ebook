﻿Ext.define('eBookAT.view.client.SearchClient', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.searchclient',

    store: 'ClientBase',
    scroll: false,
    viewConfig: {
        style: { overflow: 'auto', overflowX: 'hidden' }
    },
    controller: undefined,
    initComponent: function() {
        //var store = Ext.StoreMgr.get('Clients');
        Ext.apply(this, {


            loadMask: true,
            columns: [{
                text: 'GFIS code',
                flex: 1,
                //width: 120,
                dataIndex: 'GFIScode',
                sortable: true
            },
			    {
			        text: 'Name',
			        flex: 3,
			        //width: 300,
			        dataIndex: 'Name',
			        sortable: true
			    },
			    {
			        text: 'Address',
			        flex: 2,
			        //width: 160,
			        dataIndex: 'Address',
			        sortable: true
			    },
			    {
			        text: 'Zip code',
			        flex: 1,
			        //width: 120,
			        dataIndex: 'ZipCode',
			        sortable: true
			    },
			    {
			        text: 'City',
			        flex: 1,
			        //width: 120,
			        dataIndex: 'City',
			        sortable: true
			    },
			    {
			        text: 'Country',
			        flex: 1,
			        //width: 120,
			        dataIndex: 'Country',
			        sortable: true
}],

            dockedItems: [{
                xtype: 'toolbar',
                dock: 'top',
                items: ['Search', {
                    xtype: 'textfield',
                    name: 'searchField',
                    hideLabel: true,
                    width: 200,
                    listeners: {
                        change: {
                            fn: this.onTextFieldChange,
                            scope: this,
                            buffer: 500
                        }
                    }
}]
                }, {
                    xtype: 'pagingtoolbar',
                    store: this.store,   // same store GridPanel is using
                    dock: 'bottom',
                    displayInfo: true
                }
			]
            });

            this.callParent(arguments);
        },

        onTextFieldChange: function(txtfld, newValue, oldValue, eOpts) {
            switch (txtfld.up('window').type) {
                case 'files':
                    var fileStore = txtfld.up('window').down('filegrid').store
                    fileStore.removeAll();
                    break;

                case 'biztax':
                    var biztaxStore = txtfld.up('window').down('filebiztaxgrid').store;
                    biztaxStore.removeAll();
                    break;
            }

            var search = this.down('textfield[name=searchField]').getValue().toLowerCase();
            var person = Ext.encode(eBookAT.User.id);

            var ccdc = { ccdc: { 'Start': 0, 'Limit': 25, 'Query': null} }; //this.store.lastParams;

            ccdc.ccdc.Query = !Ext.isEmpty(search) && search.length > 0 ? search : null;
            this.store.load({ params: ccdc });
        }

    });