﻿Ext.define('eBookAT.view.administration.coefficient.edit.Window', {
    extend: 'eBookAT.view.DefaultWindow',
    alias: 'widget.coefficienteditwindow',

    title: 'Edit coëfficient',

    layout: 'fit',
    bbar: [
        { xtype: 'button', itemId: 'coefficientEditCancel', text: 'Cancel', iconCls: 'eBookAT-icon-cancel-32', scale: 'large', iconAlign: 'left' }, '->',
        { xtype: 'button', itemId: 'coefficientEditSave', text: 'Save', iconCls: 'eBookAT-icon-save-32', scale: 'large', iconAlign: 'left' }
    ],



    initComponent: function() {
        var coefficientGrid = Ext.ComponentQuery.query('coefficient')[0];
        var record = coefficientGrid.getSelectionModel().getSelection()[0].data;
        record.StartDate = Ext.Date.parse(record.StartDate, 'MS');
        record.EndDate = Ext.Date.parse(record.EndDate, 'MS');
        Ext.apply(this, {
            height: 250,
            width: 300,

            items: [
                { xtype: 'panel',
                    align: 'fit',
                    flex: 1,
                    items: [
                        {
                            xtype: 'textfield',
                            name: 'Description',
                            fieldLabel: 'Description',
                            cls: 'eBookAT-CoefficientAddWindow-Field eBookAT-CoefficientAddWindow-Field-first',
                            value: record.Description
                        },
                        {
                            xtype: 'textfield',
                            name: 'AssessmentYear',
                            fieldLabel: 'Assessment year',
                            cls: 'eBookAT-CoefficientAddWindow-Field',
                            value: record.AssessmentYear,
                            disabled: record.AssessmentYear ? false : true
                        },
                        {
                            xtype: 'datefield',
                            anchor: '100%',
                            fieldLabel: 'Start date',
                            format: 'd/m/Y',
                            altformats: 'd m Y',
                            name: 'StartDate',
                            cls: 'eBookAT-CoefficientAddWindow-Field',
                            value: record.StartDate,
                            disabled: record.StartDate ? false : true
                            //renderer: function() { return Ext.Date.parse(record.StartDate, 'MS') }
                        },
                        {
                            xtype: 'datefield',
                            anchor: '100%',
                            fieldLabel: 'End date',
                            format: 'd/m/Y',
                            altformats: 'd m Y',
                            name: 'EndDate',
                            cls: 'eBookAT-CoefficientAddWindow-Field',
                            value: record.EndDate,
                            disabled: record.EndDate ? false : true
                        },
                        {
                            xtype: 'textfield',
                            name: 'Value',
                            fieldLabel: 'Value',
                            cls: 'eBookAT-CoefficientAddWindow-Field',
                            value: record.Value
                        }
                    ]
                }

            ]
        });
        this.callParent(arguments);
    }
});