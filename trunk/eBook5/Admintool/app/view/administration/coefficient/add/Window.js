﻿Ext.define('eBookAT.view.administration.coefficient.add.Window', {
    extend: 'eBookAT.view.DefaultWindow',
    alias: 'widget.coefficientaddwindow',

    requires: [

              ],

    title: 'Add a new coëfficient',

    period: null,

    layout: 'fit',
    bbar: [
        { xtype: 'button', itemId: 'coefficientAddCancel', text: 'Cancel', iconCls: 'eBookAT-icon-cancel-32', scale: 'large', iconAlign: 'left' }, '->',
        { xtype: 'button', itemId: 'coefficientAddSave', text: 'Save', iconCls: 'eBookAT-icon-save-32', scale: 'large', iconAlign: 'left' }
    ],



    initComponent: function() {
        var coefficientKeyGrid = Ext.ComponentQuery.query('coefficientkey')[0];
        var period = coefficientKeyGrid.getSelectionModel().getSelection()[0].data.Settings.Period;

        Ext.apply(this, {

            height: 250,
            width: 300,

            items: [
                { xtype: 'panel',
                    align: 'fit',
                    flex: 1,
                    items: [
                        {
                            xtype: 'textfield',
                            name: 'Description',
                            fieldLabel: 'Description',
                            cls: 'eBookAT-CoefficientAddWindow-Field eBookAT-CoefficientAddWindow-Field-first'
                        },
                        {
                            xtype: 'textfield',
                            name: 'AssessmentYear',
                            fieldLabel: 'Assessment year',
                            cls: 'eBookAT-CoefficientAddWindow-Field',
                            disabled: period == 'DateRange' ? true : false
                        },
                        {
                            xtype: 'datefield',
                            anchor: '100%',
                            fieldLabel: 'Start date',
                            format: 'd/m/Y',
                            altformats: 'd m Y',
                            name: 'StartDate',
                            cls: 'eBookAT-CoefficientAddWindow-Field',
                            disabled: period == 'AY' ? true : false
                        },
                        {
                            xtype: 'datefield',
                            anchor: '100%',
                            fieldLabel: 'End date',
                            format: 'd/m/Y',
                            altformats: 'd m Y',
                            name: 'EndDate',
                            cls: 'eBookAT-CoefficientAddWindow-Field',
                            disabled: period == 'AY' ? true : false
                        },
                        {
                            xtype: 'textfield',
                            name: 'Value',
                            fieldLabel: 'Value',
                            cls: 'eBookAT-CoefficientAddWindow-Field'
                        }
                    ]
                }

            ]
        });

        




        this.callParent(arguments);
    }

});