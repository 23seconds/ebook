﻿Ext.define('eBookAT.view.administration.coefficient.CoefficientKey', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.coefficientkey',

    store: 'CoefficientKey',

    initComponent: function() {
        //var store = Ext.StoreMgr.get('Clients');
        Ext.apply(this, {
            //store:store,

            loadMask: true,
            title: 'Coëfficient keys',
            columns: [{
                text: 'Description',
                flex: 1,
                dataIndex: 'Description',
                sortable: true
}]
            });

            this.callParent(arguments);
        }


    });