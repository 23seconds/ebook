﻿Ext.define('eBookAT.view.administration.coefficient.Coefficient', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.coefficient',

    store: 'Coefficient',

    initComponent: function() {
        Ext.apply(this, {
            loadMask: true,
            title: 'Coefficients',
            tbar: [
                { xtype: 'button', itemId: 'coefficientAdd', type: 'coefficientbutton', action: 'add', text: 'Add', iconCls: 'eBookAT-icon-add-16', disabled: true },
                { xtype: 'button', itemId: 'coefficientEdit', type: 'coefficientbutton', action: 'edit', text: 'Edit', iconCls: 'eBookAT-icon-edit-16', disabled: true },
                { xtype: 'button', itemId: 'coefficientDelete', type: 'coefficientbutton', action: 'delete', text: 'Delete', iconCls: 'eBookAT-icon-delete-16', disabled: true }
            ],
//            plugins: [
//                Ext.create('Ext.grid.plugin.CellEditing', {
//                    clicksToEdit: 1
//                })   
//            ],
            columns: [{
                    text: 'Description',
                    flex: 1,
                    dataIndex: 'Description'
                    //editor: 'textfield'
                },
                {
                    text: 'Start date',
                    flex: 1,
                    dataIndex: 'StartDate',
                    sortable: true,
                    renderer: function(value) { return Ext.Date.format(Ext.Date.parse(value, 'MS'), 'd/m/Y') }
                },
                {
                    text: 'End date',
                    flex: 1,
                    dataIndex: 'EndDate',
                    sortable: true,
                    renderer: function(value) { return Ext.Date.format(Ext.Date.parse(value, 'MS'), 'd/m/Y') }
                },
                {
                    text: 'Assessment year',
                    flex: 1,
                    dataIndex: 'AssessmentYear',
                    sortable: true
                },
                {
                    text: 'Value',
                    flex: 1,
                    dataIndex: 'Value'
                    //editor: 'textfield'
                }
            ]
        });

        this.callParent(arguments);
    }


});