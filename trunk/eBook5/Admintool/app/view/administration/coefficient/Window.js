﻿Ext.define('eBookAT.view.administration.coefficient.Window', {
    extend: 'eBookAT.view.DefaultWindow',
    alias: 'widget.coefficientwindow',

    requires: [ 
                'eBookAT.view.administration.coefficient.CoefficientGroup',
                'eBookAT.view.administration.coefficient.CoefficientKey',
                'eBookAT.view.administration.coefficient.Coefficient'
              ],

    title: 'Manage coëfficients',

    layout: {
        type: 'vbox',
        align: 'stretch'
    },

    items: [
        { xtype: 'container',
            flex: 1,
            layout: { type: 'hbox', align: 'stretch' },
            items: [
                { xtype: 'coefficientgroup', flex: 1, cls: 'eBookAT-coefficient-panel'},
                { xtype: 'coefficientkey', flex: 1, cls: 'eBookAT-coefficient-panel' }
            ]
        },
        { xtype: 'coefficient', flex: 1, align: 'fit', cls: 'eBookAT-coefficient-panel' }
    ],

    initComponent: function() {

        this.callParent(arguments);
    }

});