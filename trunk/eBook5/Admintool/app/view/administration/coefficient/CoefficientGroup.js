﻿Ext.define('eBookAT.view.administration.coefficient.CoefficientGroup', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.coefficientgroup',

    store: 'CoefficientGroup',
    
    initComponent: function() {
        //var store = Ext.StoreMgr.get('Clients');
        Ext.apply(this, {
            //store:store,

            loadMask: true,
            title: 'Coëfficient groups',
            columns: [{
			        text: 'Desciption',
			        flex: 1,
			        dataIndex: 'Description'
			    }]
            });

            this.callParent(arguments);
        }

        
    });