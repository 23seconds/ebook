﻿Ext.define('eBookAT.view.administration.champions.add.Window', {
    extend: 'eBookAT.view.DefaultWindow',
    alias: 'widget.championsaddwindow',

    requires: [
                    'eBookAT.view.person.SearchPerson',
                    'eBookAT.view.administration.champions.add.OfficeGrid',
                    'eBookAT.view.administration.champions.add.ConfirmPnl'
              ],

    title: 'Add a new champion',

    layout: {
        type: 'card',
        align: 'stretch'
    },

    items: [
       { xtype: 'searchperson', title: 'STEP 1: Select a person', itemId: 'personGrid', cls: 'eBookAT-default-margin' },
       { xtype: 'officegrid', title: 'STEP 2: Select an office', itemId: 'officeGrid', cls: 'eBookAT-default-margin' },
       { xtype: 'confirmpnl', title: 'STEP 3: Confirm', itemId: 'confirmPnl', cls: 'eBookAT-default-margin eBookAT-confirmpnl' },
    ],

    bbar: [
        { xtype: 'button', text: 'Back', itemId: 'championBack', disabled: true },
        '->',
        { xtype: 'button', text: 'Next', itemId: 'championNext' }
    ],

    initComponent: function() {

        Ext.apply(this, {

            height: 500,
            width: 500
        });
        this.callParent(arguments);

    }

});