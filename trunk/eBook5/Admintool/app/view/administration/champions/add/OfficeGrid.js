﻿Ext.define('eBookAT.view.administration.champions.add.OfficeGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.officegrid',

    


    initComponent: function() {

        Ext.apply(this, {
        store: 'Office',
            columns: [{
                    text: 'Name',
                    flex: 1,
                    dataIndex: 'Name'
                }]
        }),
        this.callParent(arguments);
    }


});