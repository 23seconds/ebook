﻿Ext.define('eBookAT.view.administration.champions.add.ConfirmPnl', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.confirmpnl',

    initComponent: function() {

        Ext.apply(this, {
            html: 'Person X will become a CHAMPION for office Y. </br>' +
                    'Please press next to confirm'
        });
        this.callParent(arguments);
    }


});