﻿Ext.define('eBookAT.view.administration.champions.Window', {
    extend: 'eBookAT.view.DefaultWindow',
    alias: 'widget.championswindow',

    requires: [
                    'eBookAT.view.administration.champions.Grid'
              ],

    title: 'Manage champions',

    layout: {
        type: 'vbox',
        align: 'stretch'
    },

    items: [
        { xtype: 'championsgrid', flex: 1 }
    ],

        initComponent: function() {
    
         Ext.apply(this, {

            height: 700,
            width: 480
            });
        this.callParent(arguments);

    }

});