﻿Ext.define('eBookAT.view.administration.champions.Grid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.championsgrid',

    


    initComponent: function() {
        var groupingFeature = Ext.create('Ext.grid.feature.Grouping', {
            groupHeaderTpl: '{name} ({rows.length} Champion{[values.rows.length > 1 ? "s" : ""]})',
            hideGroupedHeader: true,
            startCollapsed: true,
            id: 'restaurantGrouping'
        });

        Ext.apply(this, {
        loadMask: true,
            features: [groupingFeature],
            store: 'Champion',
            title: 'Champions',

            columns: [{
                text: 'First name',
                flex: 1,
                dataIndex: 'FirstName'
            }, {
                text: 'Last name',
                flex: 1,
                dataIndex: 'LastName'
            }, {
                text: 'E-mail',
                flex: 2,
                dataIndex: 'Email'
            }, {
                text: 'Department',
                flex: 1,
                dataIndex: 'Department'
            }, {
                text: 'Office',
                flex: 0.5,
                dataIndex: 'Office'
            }],

            tbar: [ { xtype: 'button', text: 'Add', itemId: 'btnChampionAdd', iconCls: 'eBookAT-icon-add-16' },
                    { xtype:'button', text:'Delete', itemId: 'btnChampionDelete', iconCls:'eBookAT-icon-delete-16', disabled:true}]
            });

            this.callParent(arguments);
        }


    });