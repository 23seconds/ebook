﻿Ext.define('eBookAT.view.administration.help.WorksheetGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.worksheetgrid',


    initComponent: function() {
        var imageTpl = new Ext.XTemplate(
            '<tpl for=".">',
                '<div class="eBookAT-worksheet" id={id}>{name}</div>',
            '</tpl>'
        );
        Ext.apply(this, {
            store: 'Worksheet',
            title: 'Worksheets',
            columns: [{
                    //text: 'Worksheets',
                    flex: 1,
                    dataIndex: 'name'
                }]

        });
        this.callParent(arguments);
    }


});