﻿Ext.define('eBookAT.view.administration.help.add.Window', {
    extend: 'eBookAT.view.DefaultWindow',
    alias: 'widget.helpaddwindow',
    type: 'add',
    modal: true,
    requires: [
                    'eBookAT.view.administration.help.TabPanel'
              ],

    
    layout:'fit',
    items: [
        { xtype: 'helpadd' }
    ],

    bbar: [
        '->',{ xtype: 'button', text: 'Save', itemId: 'btnSaveHelp' }
    ],

    initComponent: function() {

        Ext.apply(this, {

            height: 500,
            width: 700
        });
        this.callParent(arguments);

    }

});