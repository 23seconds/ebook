﻿Ext.define('eBookAT.view.administration.help.edit.Window', {
    extend: 'eBookAT.view.DefaultWindow',
    alias: 'widget.helpeditwindow',
    type: 'edit',
    modal: true,
    title: 'Edit help message',
    requires: [
                    'eBookAT.view.administration.help.TabPanel'
              ],


    layout: 'fit',
    items: [
        { xtype: 'helpadd' }
    ],

    bbar: [
        '->', { xtype: 'button', text: 'Save', itemId: 'btnUpdateHelp' }
    ],

    initComponent: function() {

        Ext.apply(this, {

            height: 500,
            width: 700
        });
        this.callParent(arguments);

    }

});