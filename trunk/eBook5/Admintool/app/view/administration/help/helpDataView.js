﻿Ext.define('eBookAT.view.administration.help.helpDataView', {
    extend: 'Ext.view.View',
    alias: 'widget.helpdataview',



    initComponent: function() {
        Ext.apply(this, {
            itemSelector: 'div.eBookAT-worksheet-help',
            store: 'WorksheetHelp',
            cls: 'eBookAT-worksheet-help-view',
            autoScroll: true,
            emptyText: 'No help messages for this worksheet.',
            tpl: new Ext.XTemplate(
                '<tpl for=".">',
                    '<tpl if="Culture == \'nl-BE\'">',
                        '<div class="eBookAT-worksheet-help">',
                            '<div class="eBookAT-worksheet-help-wrapper">',
                                '<div class="eBookAT-worksheet-help-subject">{[this.cutText(values.Subject, 70)]}</div>',
                                '<div class="eBookAT-worksheet-help-buttons">',
                                    '<button class="eBookAT-worksheet-help-button-move-up button"><img src="/eBookAdmin/images/icons/16/up_arrow.png"></button>',
                                    '<button class="eBookAT-worksheet-help-button-move-down button"><img src="/eBookAdmin/images/icons/16/down_arrow.png"></button>',
            //'<button class="eBookAT-worksheet-help-button-move x-btn"></button>',
                                    '<button class="eBookAT-worksheet-help-button-edit button" helpid={WorksheetHelpId}><img src="/eBookAdmin/images/icons/16/edit.png"></button>',
                                    '<button class="eBookAT-worksheet-help-button-delete button" helpid={WorksheetHelpId}><img src="/eBookAdmin/images/icons/16/delete.png"></button>',
                                '</div>',
                            '</div>',
                            '<div class="eBookAT-worksheet-help-body">{[this.cutText(values.Body, 250)]}</div>',
            //'<div class="eBookAT-worksheet-help-body">{Body}</div>',
                        '</div>',
                    '</tpl>',
                '</tpl>', {
                    cutText: function(text, length) {
                        if (text.length > length) {
                            return text.substring(0, length) + '...';
                        } else {
                            return text;
                        }

                    }
                })
        });

        this.callParent(arguments);
    }


});