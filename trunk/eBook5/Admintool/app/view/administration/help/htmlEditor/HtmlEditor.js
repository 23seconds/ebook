﻿Ext.define('eBookAT.view.administration.help.htmlEditor.HtmlEditor', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.htmleditorblock',
    layout: {
        type: 'vbox',
        align:'stretch'
    },
    margins: '10 10 10 10',
    border: false,
    items: [
        {
            xtype: 'textfield',
            name: 'name',
            fieldLabel: 'Subject',
            allowBlank: false,
            height: '100'
        },{
            xtype: 'htmleditor',
            enableColors: false,
            //enableAlignments: false,
            flex: 1,
            margins: '0 0 5 0',
            
                
    }]
});