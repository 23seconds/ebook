﻿Ext.define('eBookAT.view.administration.help.Window', {
    extend: 'eBookAT.view.DefaultWindow',
    alias: 'widget.helpwindow',

    requires: [
                    'eBookAT.view.administration.help.WorksheetGrid',
                    'eBookAT.view.administration.help.helpDataView'
              ],

    title: 'Manage help worksheets',

    layout: {
        type: 'hbox',
        align: 'stretch'
    },

    items: [
        { xtype: 'worksheetgrid', flex: 1, cls: 'eBookAT-Worksheets' },
        { xtype: 'panel', flex: 2, layout: 'border', cls: 'eBookAT-Worksheets', title: 'Help', items: [
            { xtype: 'toolbar', region: 'north', items: [{ xtype: 'label', itemId:'lblWorksheet'},'->',{ xtype: 'button', text: 'Add', iconCls:'eBookAT-worksheet-help-add-btn', itemId: 'btnHelpAdd'}] }, 
            { xtype: 'helpdataview', region: 'center'}] }
    ],

    initComponent: function() {
        /*
        Ext.apply(this, {

            height: 700,
        width: 480
        });
        */
        this.callParent(arguments);

    }

});