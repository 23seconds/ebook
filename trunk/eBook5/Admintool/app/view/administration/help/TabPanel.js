﻿Ext.define('eBookAT.view.administration.help.TabPanel', {
    extend: 'Ext.tab.Panel',
    alias: 'widget.helpadd',
    requires: [
        'eBookAT.view.administration.help.htmlEditor.HtmlEditor'
    ],

    items: [
        {
            title: 'NL',
            culture: 'nl-BE',            
            layout: 'fit',
            items: [ { xtype: 'htmleditorblock', culture:'nl-BE', hasData: false} ]
        }
        , {
            title: 'FR',
            culture: 'fr-FR',
            hasData: false,
            layout: 'fit',
            items: [{ xtype: 'htmleditorblock', culture: 'fr-FR', hasData: false}]
        }, {
            title: 'EN',
            culture: 'en-US',
            hasData: false,
            layout: 'fit',
            items: [{ xtype: 'htmleditorblock', culture: 'en-US', hasData: false}]
        }
      ]
});