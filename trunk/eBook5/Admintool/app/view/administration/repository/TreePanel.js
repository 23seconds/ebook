﻿Ext.define('eBookAT.view.administration.repository.TreePanel', {
    extend: 'Ext.tree.Panel',
    alias: 'widget.repository',
    title: 'Simple Tree',
    width: 200,
    height: 150,
    store: 'Repository',
    rootVisible: false,
    renderTo: Ext.getBody()
});