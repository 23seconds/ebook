﻿Ext.define('eBookAT.view.administration.repository.Window', {
    extend: 'eBookAT.view.DefaultWindow',
    alias: 'widget.repositorywindow',

    requires: [
                    'eBookAT.view.administration.repository.TreePanel'
              ],

    title: 'Manage repository',

    layout: {
        type: 'vbox',
        align: 'stretch'
    },

    items: [
        { xtype: 'repository', flex: 1 }
    ],

    initComponent: function() {

        Ext.apply(this, {

            height: 700,
            width: 480
        });
        this.callParent(arguments);

    }

});