﻿Ext.define('eBookAT.view.GlobalView', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.GlobalView',
    itemId: 'GlobalView',

    requires: [ 'eBookAT.view.Header',
                'eBookAT.view.UserView'],
    layout: {
        type: 'vbox',
        align: 'stretch'
    },

    initComponent: function() {
        Ext.apply(this, {
            items: [
                { xtype: 'Header', height:85, border: false, padding: '3 0 0 3' },
                { xtype: 'userview', flex: 1 , border:false}
            ]
        });
        this.callParent(arguments);
    }
});