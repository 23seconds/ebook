﻿Ext.define('eBookAT.view.TabPanel', {
    extend: 'Ext.tab.Panel',
    alias: 'widget.ebookattabpanel',

    requires: [
		'eBookAT.view.tabs.File'
	],

    activetab: 0,

    initComponent: function() {
        Ext.apply(this, {
            tabBar: {
                bodyStyle: {
                    background: '#f00'
                }
            }


        });
        this.callParent(arguments);
    }

});