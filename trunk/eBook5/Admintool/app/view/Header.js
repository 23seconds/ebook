﻿Ext.define('eBookAT.view.Header', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.Header',

    layout: { type: 'hbox', align: 'stretch' },
    
    html:   '<div id="utilities">' +
                        '<div class="user">'+
                            '<div id="eBookAT-User">User: '+
                                '<div id="eBookAT-User-inner"> </div>'+
                            '</div>'+
                        '</div>'+
                        '<div id="eBookAT-RoleFilter"></div>'+
                    '</div>'+
                    '<div style="clear: both;"></div>'+
                    '<div id="header">'+
                        '<img height="42" width="185" alt="Ernst &amp; Young" src="images/EY/eyLogo-en.png" class="eylogo" title="Site name home" id="Img1">'+
                        '<h1 id="eBookAT-Title">eBook Admintool</h1>'+
                        '<div id="eBookAT-header-logo" ></div>'+
                    '</div>',
                    
    initComponent: function() {
        Ext.apply(this, {
            
        });
        this.callParent(arguments);
    }

});