﻿Ext.define('eBookAT.view.tabs.File', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.filetab',

    requires: [
	],

    html: '<div class="eBookAT-file-menu">' +
    // REOPEN
                '<div class="eBookAT-menu-item" id="eBookAT-file-reopen">' +
                    '<div class="eBookAT-menu-item-icon eBookAT-file-reopen-icon"></div>' +
                    '<div class="eBookAT-menu-title">Reopen files</div>' +
                '</div>' +
    // CLOSE
                '<div class="eBookAT-menu-item" id="eBookAT-file-close">' +
                    '<div class="eBookAT-menu-item-icon eBookAT-file-close-icon"></div>' +
                    '<div class="eBookAT-menu-title">Close files</div>' +
                '</div>' +
    // Biztax reopen
                '<div class="eBookAT-menu-item" id="eBookAT-biztax-failed">' +
                    '<div class="eBookAT-menu-item-icon eBookAT-biztax-failed-icon"></div>' +
                    '<div class="eBookAT-menu-title">BizTax failed (reopen)</div>' +
                '</div>' +
    // Biztax success
                '<div class="eBookAT-menu-item" id="eBookAT-biztax-success">' +
                    '<div class="eBookAT-menu-item-icon eBookAT-biztax-success-icon"></div>' +
                    '<div class="eBookAT-menu-title">BizTax success</div>' +
                '</div>' +
            '</div>',
    


    initComponent: function() {
        this.addEvents('click');

        this.callParent(arguments);
    }
});
