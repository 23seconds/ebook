﻿Ext.define('eBookAT.view.tabs.Reports', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.reportsTab',

    requires: [
	],

    html: '<div class="eBookAT-reports-menu">' +
                // CLIENT ENGAGEMENT LETTER
                '<div class="eBookAT-menu-item" id="eBookAT-clientEngagementLetter">' +
                    '<div class="eBookAT-menu-item-icon eBookAT-report-icon"></div>' +
                    '<div class="eBookAT-menu-title">Client - Engagement letter</div>' +
                '</div>' +
            '</div>',



    initComponent: function() {
        this.addEvents('click');

        this.callParent(arguments);
    }
});
