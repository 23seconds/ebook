﻿Ext.define('eBookAT.view.tabs.GTH', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.gthtab',

    requires: [
	],

    html: '<div class="eBookAT-GTH-menu">' +
    // MANAGE
                '<div class="eBookAT-menu-item" id="eBookAT-GTH-persons">' +
                    '<div class="eBookAT-menu-item-icon eBookAT-GTH-persons-icon"></div>' +
                    '<div class="eBookAT-menu-title">Manage persons</div>' +
                '</div>' +

                '<div class="eBookAT-menu-item" id="eBookAT-GTH-roles">' +
                    '<div class="eBookAT-menu-item-icon eBookAT-GTH-roles-icon"></div>' +
                    '<div class="eBookAT-menu-title">Manage roles</div>' +
                '</div>' +

                '<div class="eBookAT-menu-item" id="eBookAT-GTH-replacements">' +
                    '<div class="eBookAT-menu-item-icon eBookAT-GTH-replacements-icon"></div>' +
                    '<div class="eBookAT-menu-title">Replacements</div>' +
                '</div>' +
          '</div>',



    initComponent: function() {
        this.addEvents('click');

        this.callParent(arguments);
    }
});
