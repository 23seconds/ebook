﻿Ext.define('eBookAT.view.tabs.Administration', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.administrationtab',

    requires: [
	],

    html: '<div class="eBookAT-administration-menu">' +
                // MANAGE
                '<div class="eBookAT-menu-item" id="eBookAT-coefficient-manage">' +
                    '<div class="eBookAT-menu-item-icon eBookAT-coefficient-manage-icon"></div>' +
                    '<div class="eBookAT-menu-title">Manage coëfficients</div>' +
                '</div>' +

                // CHAMPIONS
                '<div class="eBookAT-menu-item" id="eBookAT-champions-manage">' +
                    '<div class="eBookAT-menu-item-icon eBookAT-champions-manage-icon"></div>' +
                    '<div class="eBookAT-menu-title">Manage champions</div>' +
                '</div>' +

                // CACHING SERVICE
                '<div class="eBookAT-menu-item" id="eBookAT-caching">' +
                    '<div class="eBookAT-menu-item-icon eBookAT-caching-icon"></div>' +
                    '<div class="eBookAT-menu-title">Reload static data in cache</div>' +
                '</div>' +

                /* REPOSITORY*/
                '<div class="eBookAT-menu-item" id="eBookAT-repository">' +
                    '<div class="eBookAT-menu-item-icon eBookAT-repository-icon"></div>' +
                    '<div class="eBookAT-menu-title">Manage repository</div>' +
                '</div>' + 

                /* HELP */
                '<div class="eBookAT-menu-item" id="eBookAT-help">' +
                    '<div class="eBookAT-menu-item-icon eBookAT-help-icon"></div>' +
                    '<div class="eBookAT-menu-title">Manage help worksheets</div>' +
                '</div>' + 
            '</div>',



    initComponent: function() {
        this.addEvents('click');

        this.callParent(arguments);
    }
});
