﻿Ext.define('eBookAT.model.BiztaxDeclaration', {
    extend: 'Ext.data.Model',
    fields:
	    [{ name: 'Id', mapping: 'id' },
		    { name: 'Status', mapping: 's' },
		    { name: 'Department', mapping: 'dep' },
		    { name: 'PartnerName', mapping: 'pn' },
		    { name: 'PartnerId', mapping: 'pid' },
		    { name: 'PartnerName', mapping: 'pn' },
		    { name: 'ClientName', mapping: 'cn' },
		    { name: 'ClientEnterpriseNumber', mapping: 'ce' },
		    { name: 'AssessmentYear', mapping: 'ay' },
		    { name: 'CurrentPath', mapping: 'cp' },
		    { name: 'Proxy', mapping: 'px' },
		    { name: 'Type', mapping: 't' },
		    { name: 'SendToDeclareBy', mapping: 'sdb' },
		    { name: 'SendToDeclareDate', mapping: 'sdd' },
		    { name: 'InProgressBy', mapping: 'ip' },
		    { name: 'EBookTaxCalculation', mapping: 'ec' },
		    { name: 'BizTaxTaxCalculation', mapping: 'bc' },
		    { name: 'Errors', mapping: 'ers' }
		    
	    ]
});




     


