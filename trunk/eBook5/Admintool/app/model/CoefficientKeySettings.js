﻿Ext.define('eBookAT.model.CoefficientKeySettings', {
    extend: 'Ext.data.Model',
    fields: [   
        { name: 'Period', mapping: 'Period' }
    ],
    
    belongsTo: 'CoefficientKey'
});


