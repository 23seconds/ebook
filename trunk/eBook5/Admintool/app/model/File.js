﻿Ext.define('eBookAT.model.File', {
    extend: 'Ext.data.Model',
    fields:
	    [{ name: 'Id', mapping: 'id' },
		    { name: 'Name', mapping: 'n' },
		    { name: 'StartDate', mapping: 'sd' },
		    { name: 'EndDate', mapping: 'ed' },
		    { name: 'ClientName', mapping: 'cn' },
            { name: 'YearEndService', type: 'bool', mapping: 'yes'}
	    ]
});


