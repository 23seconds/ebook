﻿Ext.define('eBookAT.model.ClientBase', {
    extend: 'Ext.data.Model',
    fields:
	    [{  name: 'Id', mapping: 'id' }
		, { name: 'Name', mapping: 'n' }
		, { name: 'GFIScode', mapping: 'gc' }
		, { name: 'Address', mapping: 'a' }
        , { name: 'ZipCode', mapping: 'zip' }
        , { name: 'City', mapping: 'ci' }
        , { name: 'Country', mapping: 'co' }
        , { name: 'VatNumber', mapping: 'vnr' }
        , { name: 'EnterpriseNumber', mapping: 'enr' }
        , { name: 'ProAccServer', mapping: 'pas' }
        , { name: 'ProAccDatabase', mapping: 'pad' }
        , { name: 'ProAccLinkUpdated', mapping: 'palu' }
        , { name: 'Shared', mapping: 'sh' }
        , { name: 'bni', mapping: 'bni' }
	]
});


