﻿Ext.define('eBookAT.model.WorksheetHelp', {
    extend: 'Ext.data.Model',
    fields: [{ name: 'Id', mapping: 'id' },
            { name: 'WorksheetHelpId', mapping: 'whid' },
		    { name: 'WorksheetTypeId', mapping: 'wtid' },
		    { name: 'Culture', mapping: 'c' },
		    { name: 'Subject', mapping: 's' },
		    { name: 'Body', mapping: 'b' },
		    { name: 'Order', mapping: 'o' }
	    ]
});


