﻿Ext.define('eBookAT.model.ClientEngagementLetterComboboxItems', {
    extend: 'Ext.data.Model',
    fields: [
        { name: 'Id', mapping: 'id'},
	    { name: 'NL', mapping: 'nl' },
	    { name: 'FR', mapping: 'fr' },
	    { name: 'EN', mapping: 'en' }
	]
});


