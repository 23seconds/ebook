﻿Ext.define('eBookAT.model.Coefficient', {
    extend: 'Ext.data.Model',
    fields:
	    [   { name: 'Groupkey', mapping: 'gk' },
		    { name: 'Key', mapping: 'k' },
		    { name: 'Value', mapping: 'v' },
		    { name: 'AssessmentYear', mapping: 'ay' },
		    { name: 'StartDate', mapping: 'sd' },
		    { name: 'EndDate', mapping: 'ed' },
		    { name: 'Description', mapping: 'd' },
		    { name: 'Id', mapping: 'id' }
	    ]
});
