﻿Ext.define('eBookAT.model.Worksheet', {
    extend: 'Ext.data.Model',
    requires: ['Ext.data.SequentialIdGenerator'],
    fields:
	    [{ name: 'id' },
        { name: 'name' },
        { name: 'ruleApp' },
        { name: 'type' }
	    ],
    idProperty: '',
    idgen: 'sequential'
});


