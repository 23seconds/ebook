﻿Ext.define('eBookAT.model.Office', {
    extend: 'Ext.data.Model',
    fields:
	    [   { name: 'Id', mapping: 'id' },
		    { name: 'Name', mapping: 'n' }
	    ]
});


