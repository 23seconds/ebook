﻿Ext.define('eBookAT.model.CoefficientKey', {
    extend: 'Ext.data.Model',
    fields:
	    [   { name: 'Id', mapping: 'id' },
		    { name: 'Key', mapping: 'k' },
		    { name: 'Description', mapping: 'd' },
		    { name: 'Settings', mapping: 's' }
	    ],
    associations: [{ type: 'hasOne', model: 'CoefficientKeySettings'}]
});


