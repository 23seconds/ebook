﻿Ext.define('eBookAT.model.Person', {
    extend: 'Ext.data.Model',
     requires: ['Ext.data.SequentialIdGenerator'],
    fields:
	    [{ name: 'Id', mapping: 'id' },
		    { name: 'FirstName', mapping: 'fn' },
		    { name: 'LastName', mapping: 'ln' },
		    { name: 'Email', mapping: 'em' },
		    { name: 'WindowsAccount', mapping: 'wa' },
		    { name: 'GPN', mapping: 'gpn' },
		    { name: 'Department', mapping: 'dep' },
		    { name: 'Office', mapping: 'o' },
		    { name: 'OfficeId', mapping: 'oid' }
	    ],
	 idProperty:'',
     idgen: 'sequential'
});


