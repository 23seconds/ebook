﻿Ext.define('eBookAT.model.ClientEngagementLetter', {
    extend: 'Ext.data.Model',
    fields: [
	    { name: 'Name', mapping: 'c' },
	    { name: 'Status', mapping: 's' },
	    { name: 'Template', mapping: 't' },
	    { name: 'StartDate', mapping: 'sd' },
	    { name: 'EndDate', mapping: 'ed' },
	    { name: 'Office', mapping: 'o' }
	]
});


