﻿Ext.define('eBookAT.model.CoefficientGroup', {
    extend: 'Ext.data.Model',
    fields:
	    [   { name: 'Id', mapping: 'id' },
		    { name: 'Key', mapping: 'k' },
		    { name: 'Description', mapping: 'd' }
	    ]
});


