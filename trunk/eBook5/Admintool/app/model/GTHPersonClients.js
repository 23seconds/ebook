﻿Ext.define('eBookAT.model.GTHPersonClients', {
    extend: 'Ext.data.Model',
    fields:
	    [{ name: 'PersonId', mapping: 'pid' },
		    { name: 'FirstName', mapping: 'fn' },
		    { name: 'LastName', mapping: 'ln' },
		    { name: 'ActiveClientCount', mapping: 'cc' }
	    ]
});


