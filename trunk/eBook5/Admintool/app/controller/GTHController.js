﻿Ext.define('eBookAT.controller.GTHController', {
    extend: 'Ext.app.Controller',

    stores: ['GTHPerson', 'PersonRoles', 'GTHRoles', 'PersonReplacementA', 'PersonReplacementB'],

    models: ['Person', 'PersonRoles', 'GTHRoles', 'GTHPersonClients'],

    requires: [
        'eBookAT.view.GTH.persons.add.Window',
        'eBookAT.view.GTH.persons.edit.Window'
    ],

    init: function() {
        this.control({
            'gthpersonwindow': {
                render: this.loadPersons
            },
            'gthroleswindow': {
                render: this.loadPersons,
                close: this.closeRolesWindow
            },
            '#personAdd': {
                click: this.btnPersonAddClick
            },
            '#personEdit': {
                click: this.btnPersonEditClick
            },
            '#personEditSave': {
                click: this.btnPersonEditSaveClick
            },
            '#personEditCancel': {
                click: this.btnPersonCancelClick
            },
            '#personAddSave': {
                click: this.btnPersonSaveClick
            },
            '#personAddCancel': {
                click: this.btnPersonCancelClick
            },
            'gthpersongrid': {
                itemclick: this.personGridItemClick
            },

            //roles
            'gthpersonrolesgrid': {
                itemclick: this.personRolesItemClick
            },
            'gthpersonrolesdataview': {
                afterrender: this.addClickEventToPanel
            },

            //replacement
            'gthreplacementwindow': {
                render: this.loadGTHRoles,
                close: this.replacementWindowClose
            },
            'gthrolesgrid': {
                itemclick: this.rolesItemClick
            },
            '#replacePersons': {
                click: this.replacePersonsClick
            }


        });
    },

    loadPersons: function() {
        var personStore = this.getGTHPersonStore();
        personStore.load();
    },

    btnPersonAddClick: function(btn) {
        var personAddWindow = Ext.widget('personaddwindow');

        personAddWindow.show();
    },

    btnPersonEditClick: function(btn) {
        var personEditWindow = Ext.widget('personeditwindow');
        personEditWindow.show();
    },

    btnPersonEditSaveClick: function(btn) {
        var gpn = btn.up('window').down('textfield[name=GPN]').value;
        var fname = btn.up('window').down('textfield[name=FirstName]').value;
        var lname = btn.up('window').down('textfield[name=LastName]').value;
        var email = btn.up('window').down('textfield[name=Email]').value;
        if (fname && lname && email) {
            Ext.Ajax.request({
                url: '/EY.com.eBook.API.5/GTHTeamService.svc/SavePerson'
                    , method: 'POST'
                    , jsonData: { cpdc: { GPN: gpn, FirstName: fname, LastName: lname, Email: email} }
                    , callback: function(options, success, response) {
                        if (success) {
                            this.loadPersons();
                            btn.up('window').close();

                        }
                    },
                scope: this

            });
        } else {
            Ext.Msg.show({
                title: 'Warning',
                msg: 'Please fill in all fields',
                buttons: Ext.Msg.OK,
                icon: Ext.Msg.WARNING
            });
        }

    },

    btnPersonSaveClick: function(btn) {
        var gpn = btn.up('window').down('textfield[name=GPN]').value;
        var personStore = this.getGTHPersonStore();
        if (personStore.findExact('GPN', gpn) == 0) {
            Ext.Msg.show({
                title: 'Warning',
                msg: 'This GPN is already in use, please use another one',
                buttons: Ext.Msg.OK,
                icon: Ext.Msg.WARNING
            });
        } else {
            var fname = btn.up('window').down('textfield[name=FirstName]').value;
            var lname = btn.up('window').down('textfield[name=LastName]').value;
            var email = btn.up('window').down('textfield[name=Email]').value;
            if (fname && lname && email) {
                Ext.Ajax.request({
                    url: '/EY.com.eBook.API.5/GTHTeamService.svc/SavePerson'
                        , method: 'POST'
                        , jsonData: { cpdc: { GPN: gpn, FirstName: fname, LastName: lname, Email: email} }
                        , callback: function(options, success, response) {
                            if (success) {
                                this.loadPersons();
                                btn.up('window').close();

                            }
                        },
                    scope: this

                });
            } else {
                Ext.Msg.show({
                    title: 'Warning',
                    msg: 'Please fill in all fields',
                    buttons: Ext.Msg.OK,
                    icon: Ext.Msg.WARNING
                });
            }
        }
    },

    btnPersonCancelClick: function(btn) {
        btn.up('window').close();
    },

    personRolesItemClick: function(grid, record, item, index, e, eOpts) {

        var storePersonRoles = this.getPersonRolesStore();
        //storePersonRoles.removeAll();
        storePersonRoles.getProxy().extraParams = {
            cbidc: { Id: record.data.Id }
        }
        storePersonRoles.load();

        this.savePersonRoles();



    },

    addClickEventToPanel: function(panel) {
        panel.container.on('click', this.onBodyClick, this);
    },

    onBodyClick: function(e, t, eOpts) {
        var menuItem = e.getTarget('.eBootAT-Roles');
        var personGrid = Ext.ComponentQuery.query('gthpersonrolesgrid');
        var pid = personGrid[0].getSelectionModel().getSelection()[0].data.Id;
        if (menuItem) {
            if (this.counter == 0) {
                this.rolesToBeSaved.push({ 'pid': pid, 'r': menuItem.value, 'c': menuItem.checked });
            } else {
                var duplicateFound = false;
                for (var i = 0; i < this.rolesToBeSaved.length; i++) {
                    if (this.rolesToBeSaved[i].Role == menuItem.value) {
                        this.rolesToBeSaved[i].Check = menuItem.checked;
                        duplicateFound = true;
                    }
                }
                if (!duplicateFound)
                { this.rolesToBeSaved.push({ 'pid': pid, 'r': menuItem.value, 'c': menuItem.checked }); }
            }

            this.counter++;
        }
    },

    closeRolesWindow: function() {
        this.savePersonRoles();
        var stores = [
                  this.getGTHPersonStore(),
                  this.getPersonRolesStore()
            ];

        this.emptyAllStores(stores);
    },

    replacementWindowClose: function() {
        var stores = [
            this.getPersonReplacementAStore(),
            this.getPersonReplacementBStore()
        ];
        this.emptyAllStores(stores);
    },

    savePersonRoles: function() {
        if (this.rolesToBeSaved && this.rolesToBeSaved.length > 0) {
            Ext.Ajax.request({
                url: '/EY.com.eBook.API.5/GTHTeamService.svc/SavePersonRoles'
                        , method: 'POST'
                        , jsonData: { lcprdc: this.rolesToBeSaved }
                        , callback: function(options, success, response) {
                            if (success) {

                            }
                        },
                scope: this
            });
        }

        this.rolesToBeSaved = new Array();
        this.counter = 0;
    },

    emptyAllStores: function(stores) {
        Ext.Array.each(stores, function(store, index, storesItSelf) {
            store.removeAll();
        });
    },

    loadGTHRoles: function() {
        var gthRolesStore = this.getGTHRolesStore();
        gthRolesStore.load();
    },

    rolesItemClick: function(grid, record, item, index, e, eOpts) {
        this.loadPersonReplacementStores(record.data.Role);



    },

    replacePersonsClick: function(btn) {
        var gridFrom = btn.up('window').down('gthreplacementpersonsgrid[type=From]').getSelectionModel().getSelection(); //[0].data.PersonId;
        var gridTo = btn.up('window').down('gthreplacementpersonsgrid[type=To]').getSelectionModel().getSelection(); //[0].data.PersonId;
        var role = btn.up('window').down('gthrolesgrid').getSelectionModel().getSelection(); //[0].data.Role;

        if (role.length > 0) {
            var team = role[0].data.Role.substring(role[0].data.Role.length - 1);
            if (gridFrom.length > 0 && gridTo.length > 0) {
                Ext.Msg.show({
                    title: 'Warning',
                    msg: 'Are you sure you want to replace all the clients (from team ' + team + ') that are in progress from "' + gridFrom[0].data.FirstName + ' ' + gridFrom[0].data.LastName + '" to "' + gridTo[0].data.FirstName + ' ' + gridTo[0].data.LastName + '"?',
                    buttons: Ext.Msg.YESNO,
                    icon: Ext.Msg.QUESTION,
                    fn: function(btne) {
                        if (btne == 'yes') {
                            var personFrom = gridFrom[0].data.PersonId;
                            var personTo = gridTo[0].data.PersonId;
                            Ext.Ajax.request({
                                url: '/EY.com.eBook.API.5/GTHTeamService.svc/ReplacePersons'
                                , method: 'POST'
                                , jsonData: { crpdc: { pf: personFrom, pt: personTo, t: team} }
                                , callback: function(options, success, response) {
                                    if (success) {
                                        this.loadPersonReplacementStores(role[0].data.Role);
                                    }
                                }
                                , scope: this
                            });
                        }
                    },
                    scope: this
                });

            } else {
                Ext.Msg.show({
                    title: 'Error',
                    msg: 'Please select 2 persons in order to make the replacement',
                    buttons: Ext.Msg.OK,
                    icon: Ext.Msg.WARNING
                });
            }

        } else {
            Ext.Msg.show({
                title: 'Error',
                msg: 'Please select a team',
                buttons: Ext.Msg.OK,
                icon: Ext.Msg.WARNING
            });
        }
    },

    loadPersonReplacementStores: function(role) {
        var gthPersonReplacementA = this.getPersonReplacementAStore();
        gthPersonReplacementA.getProxy().extraParams = {
            csdc: { String: role }
        };
        gthPersonReplacementA.load();

        var gthPersonReplacementB = this.getPersonReplacementBStore();
        gthPersonReplacementB.getProxy().extraParams = {
            csdc: { String: role }
        };
        gthPersonReplacementB.load();
    },

    personGridItemClick: function(grid, record, item, index, e, eOpts) {
        var btnEdit = grid.panel.down('button[itemId=personEdit]');
        btnEdit.enable();
    }




});