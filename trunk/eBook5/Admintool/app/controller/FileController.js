﻿Ext.define('eBookAT.controller.FileController', {
    extend: 'Ext.app.Controller',

    stores: ['File', 'FilesToProcess', 'ClientBase', 'BiztaxDeclaration', 'BiztaxDeclarationsToProcess'],

    models: ['File', 'BiztaxDeclaration'],

    requires: [
        'eBookAT.view.client.SearchClient',
        'eBookAT.view.file.FileGrid'
    ],

    init: function () {
        this.control({
            'searchclient': {
                itemclick: this.clientItemClick
            },
            'filegrid': {
                itemdblclick: this.fileDblClick,
                itemclick: this.fileClick
            },
            'filebiztaxgrid': {
                itemclick: this.declarationClick,
                itemdblclick: this.declarationDblClick
            },
            'filebiztaxtoprocessgrid': {
                itemclick: this.biztaxItemClick
            },
            '#addFileToList': {
                click: this.btnAddFileToListClick
            },
            '#declarationDelete': {
                click: this.btnDeleteFileFromListClick
            },
            'filereopenwindow': {
                close: this.closeWindow
            },
            'fileclosewindow': {
                close: this.closeWindow
            },
            'filebiztaxfailwindow': {
                close: this.closeWindow
            },
            'filebiztaxsuccesswindow': {
                close: this.closeWindow
            },
            '#biztaxFailed': {
                click: this.btnSetFailedClick
            },
            '#biztaxSuccess': {
                click: this.btnSetSuccessClick
            },
            '#fileReopen': {
                click: this.btnReopenFile
            },
            '#fileClose': {
                click: this.btnCloseFile
            },
            'checkbox': {
                check: function(){ alert('test')}
            }


        });
    },

    btnCloseFile: function (btn) {
        Ext.Msg.show({
            title: 'Warning',
            msg: 'Are you sure you want to close all the selected files?',
            buttons: Ext.Msg.YESNO,
            icon: Ext.Msg.QUESTION,
            fn: function (btne) {
                if (btne == 'yes') {
                    btn.up('window').getEl().mask('Loading');
                    var store = this.getFilesToProcessStore();
                    var records = new Array();
                    store.each(function (rec) {
                        records.push({ Id: rec.data.Id });
                    });
                    Ext.Ajax.request({
                        url: '/EY.com.eBook.API.5/FileService.svc/CloseFile'
                                            , method: 'POST'
                                            , jsonData: { lcidc: records }
                                            , callback: function (options, success, response) {
                                                btn.up('window').getEl().unmask();
                                                if (success) {
                                                    Ext.Msg.show({
                                                        title: 'Success',
                                                        msg: 'The selected files have been closed.',
                                                        buttons: Ext.Msg.OK,
                                                        icon: Ext.Msg.INFO
                                                    });

                                                    this.emptyFilesStores();
                                                    this.emptyClientStore();
                                                } else {
                                                    Ext.Msg.show({
                                                        title: 'Failure',
                                                        msg: 'Something went wrong. Please contact someone from the eBook-team',
                                                        buttons: Ext.Msg.OK,
                                                        icon: Ext.Msg.ERROR
                                                    });
                                                }
                                            },
                        scope: this
                    });
                }
            },
            scope: this
        });
    },

    btnReopenFile: function (btn) {
        Ext.Msg.show({
            title: 'Warning',
            msg: 'Are you sure you want to reopen all the selected files?',
            buttons: Ext.Msg.YESNO,
            icon: Ext.Msg.QUESTION,
            fn: function (btne) {
                if (btne == 'yes') {
                    btn.up('window').getEl().mask('Loading');
                    var store = this.getFilesToProcessStore();
                    var records = new Array();
                    store.each(function (rec) {
                        records.push({ Id: rec.data.Id, Boolean: rec.data.YearEndService });
                    });
                    Ext.Ajax.request({
                        url: '/EY.com.eBook.API.5/FileService.svc/ReopenFile'
                                        , method: 'POST'
                                        , jsonData: { lcibdc: records }
                                        , callback: function (options, success, response) {
                                            btn.up('window').getEl().unmask();
                                            if (success) {
                                                Ext.Msg.show({
                                                    title: 'Success',
                                                    msg: 'The selected files have been reopened.',
                                                    buttons: Ext.Msg.OK,
                                                    icon: Ext.Msg.INFO
                                                });

                                                this.emptyFilesStores();
                                                this.emptyClientStore();
                                            } else {
                                                Ext.Msg.show({
                                                    title: 'Failure',
                                                    msg: 'Something went wrong. Please contact someone from the eBook-team',
                                                    buttons: Ext.Msg.OK,
                                                    icon: Ext.Msg.ERROR
                                                });
                                            }
                                        },
                        scope: this
                    });
                }
            },
            scope: this
        });
    },

    clientItemClick: function (grid, record, item, index, e, eOpts) {
        var btn = grid.up('window').down('#addFileToList');
        btn.disable();

        var action = grid.up('window').action;
        var bool = (action == 'close') ? false : true;

        switch (grid.up('window').type) {
            case 'files':
                var fileStore = this.getFileStore();
                fileStore.getProxy().extraParams = {
                    cfdc: { ClientId: record.data.Id, MarkedForDeletion: false, Deleted: false, Closed: bool }
                }
                fileStore.load();
                break;

            case 'biztax':
                var biztaxStore = this.getBiztaxDeclarationStore();
                biztaxStore.getProxy().extraParams = {
                    cbidc: { Id: record.data.Id }
                }
                biztaxStore.load();
                break;
        }
    },

    fileDblClick: function (grid, record, item, index, e, eOpts) {
        var clientName = grid.up('window').down('searchclient').getSelectionModel().getSelection()[0].data.Name;
        var record = { Id: record.data.Id, Name: record.data.Name, StartDate: record.data.StartDate, EndDate: record.data.EndDate, ClientName: clientName, YearEndService: 'false' };
        var window = grid.up('window');
        this.addFile(record, window);
    },

    fileClick: function (grid, record, item, index, e, eOpts) {
        var btn = grid.up('window').down('#addFileToList');
        btn.enable();
    },

    declarationDblClick: function (grid, record, item, index, e, eOpts) {
        var clientName = grid.up('window').down('searchclient').getSelectionModel().getSelection()[0].data.Name;
        var record = { Id: record.data.Id, Status: record.data.Status, ClientName: record.data.ClientName, Department: record.data.Department, PartnerName: record.data.PartnerName, AssessmentYear: record.data.AssessmentYear };
        var window = grid.up('window');
        this.addFile(record, window);
    },

    declarationClick: function (grid, record, item, index, e, eOpts) {
        var btn = grid.up('window').down('#addFileToList');
        btn.enable();
    },

    btnAddFileToListClick: function (btn) {
        var window = btn.up('window');
        switch (window.type) {
            case 'files':
                var selectedFile = btn.up('window').down('filegrid').getSelectionModel().getSelection()[0].data;
                var selectedClient = btn.up('window').down('searchclient').getSelectionModel().getSelection()[0].data;
                var record = { Id: selectedFile.Id, Name: selectedFile.Name, StartDate: selectedFile.StartDate, EndDate: selectedFile.EndDate, ClientName: selectedClient.Name, YearEndService: 'false' }
                break;

            case 'biztax':
                var selectedDeclaration = btn.up('window').down('filebiztaxgrid').getSelectionModel().getSelection()[0].data;
                var record = { Id: selectedDeclaration.Id, Status: selectedDeclaration.Status, ClientName: selectedDeclaration.ClientName, Department: selectedDeclaration.Department, PartnerName: selectedDeclaration.PartnerName, AssessmentYear: selectedDeclaration.AssessmentYear };
                break;
        }

        this.addFile(record, window);
    },

    addFile: function (record, window) {
        switch (window.type) {
            case 'files':
                var filesToProcessStore = this.getFilesToProcessStore();
                if (filesToProcessStore.findExact('Id', record.Id) < 0) {
                    filesToProcessStore.add(record);
                } else {
                    Ext.Msg.show({
                        title: 'Error',
                        msg: 'This file is already added to the list',
                        buttons: Ext.Msg.OK,
                        icon: Ext.Msg.WARNING
                    });
                }
                break;

            case 'biztax':
                var declarationsToProcessStore = this.getBiztaxDeclarationsToProcessStore();
                if (window.action == 'failed' && record.Status == -99) {
                    Ext.Msg.show({
                        title: 'Error',
                        msg: 'This biztax declaration is already marked as failed.',
                        buttons: Ext.Msg.OK,
                        icon: Ext.Msg.WARNING
                    });
                } else if (window.action == 'success' && record.Status == 99) {
                    Ext.Msg.show({
                        title: 'Error',
                        msg: 'This biztax declaration is already marked as success.',
                        buttons: Ext.Msg.OK,
                        icon: Ext.Msg.WARNING
                    });
                } else if (declarationsToProcessStore.findExact('Id', record.Id) < 0) {
                    declarationsToProcessStore.add(record);
                } else {
                    Ext.Msg.show({
                        title: 'Error',
                        msg: 'This biztax declaration is already added to the list',
                        buttons: Ext.Msg.OK,
                        icon: Ext.Msg.WARNING
                    });
                }
                break;
        }


    },

    closeWindow: function (window) {
        this.emptyClientStore();
        switch (window.type) {
            case 'files':
                this.emptyFilesStores();
                break;
            case 'biztax':
                this.emptyBiztaxStores();
                break;
        }

    },

    emptyClientStore: function () {
        var clientStore = this.getClientBaseStore();
        clientStore.removeAll();
    },

    emptyBiztaxStores: function () {
        var biztaxStore = this.getBiztaxDeclarationStore();
        var biztaxToProcessStore = this.getBiztaxDeclarationsToProcessStore();

        biztaxStore.removeAll();
        biztaxToProcessStore.removeAll();

        var btnDelete = Ext.getCmp('#declarationDelete');
        btnDelete.disable();
    },

    emptyFilesStores: function () {
        var fileStore = this.getFileStore();
        var filesToProcessStore = this.getFilesToProcessStore();

        fileStore.removeAll();
        filesToProcessStore.removeAll();
    },

    btnSetFailedClick: function (btn) {
        this.changeStatus(btn);
    },

    btnSetSuccessClick: function (btn) {
        this.changeStatus(btn);
    },

    changeStatus: function (btn) {
        var biztaxToProcessStore = this.getBiztaxDeclarationsToProcessStore();
        if (biztaxToProcessStore.data.length == 0) {
            Ext.Msg.show({
                title: 'Error',
                msg: 'No biztax declarations selected to process.',
                buttons: Ext.Msg.OK,
                icon: Ext.Msg.WARNING
            });
        } else {
            var status = btn.up('window').action;
            Ext.Msg.show({
                title: 'Warning',
                msg: 'Are you sure you want to change the status to ' + status + ' for all the selected declarations?',
                buttons: Ext.Msg.YESNO,
                icon: Ext.Msg.QUESTION,
                fn: function (btne) {
                    if (btne == 'yes') {
                        btn.up('window').getEl().mask('Loading');
                        var store = this.getBiztaxDeclarationsToProcessStore();
                        var records = new Array();
                        store.each(function (rec) {
                            records.push({ Id: rec.data.Id, Status: btn.up('window').status });
                        });
                        Ext.Ajax.request({
                            url: '/EY.com.eBook.API.5/BizTaxDeclarationService.svc/ChangeStatus'
                                    , method: 'POST'
                                    , jsonData: { lcisdc: records }
                                    , callback: function (options, success, response) {
                                        btn.up('window').getEl().unmask();
                                        if (success) {
                                            Ext.Msg.show({
                                                title: 'Success',
                                                msg: 'The biztax declarations have been processed.',
                                                buttons: Ext.Msg.OK,
                                                icon: Ext.Msg.INFO
                                            });

                                            this.emptyBiztaxStores();
                                            this.emptyClientStore();
                                        } else {
                                            Ext.Msg.show({
                                                title: 'Failure',
                                                msg: 'Something went wrong. Please contact someone from the eBook-team',
                                                buttons: Ext.Msg.OK,
                                                icon: Ext.Msg.ERROR
                                            });
                                        }
                                    },
                            scope: this
                        });
                    }
                },
                scope: this
            });
        }
    },

    biztaxItemClick: function (grid, record, item, index, e, eOpts) {
        var btnDelete = grid.up().down('#declarationDelete');
        btnDelete.enable();
    },

    btnDeleteFileFromListClick: function (btn) {
        var grid = btn.up('grid');
        var selectedRecord = grid.getSelectionModel().getSelection()[0];
        var index = grid.store.indexOf(selectedRecord);

        grid.store.removeAt(index);


        btn.disable();
    }


});