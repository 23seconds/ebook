﻿Ext.define('eBookAT.controller.MainController', {
    extend: 'Ext.app.Controller',

    stores: ['ClientBase', 'Person'],

    models: ['ClientBase', 'Person'],

    requires: ['eBookAT.view.GlobalView',

                'eBookAT.view.tabs.File',
                'eBookAT.view.file.reopen.Window',
                'eBookAT.view.file.close.Window',
                'eBookAT.view.file.biztax.failed.Window',
                'eBookAT.view.file.biztax.success.Window',

                'eBookAT.view.tabs.Administration',
                'eBookAT.view.administration.coefficient.Window',
                'eBookAT.view.administration.champions.Window',

                'eBookAT.view.tabs.GTH',
                'eBookAT.view.GTH.persons.Window',
                'eBookAT.view.GTH.roles.Window',
                'eBookAT.view.GTH.replacement.Window',

                'eBookAT.view.tabs.Reports',
                'eBookAT.view.reports.clientEngagementLetter.Window',

                'eBookAT.view.administration.repository.Window',

                'eBookAT.view.administration.help.Window'

    ],

    init: function() {
        this.control({

            'GlobalView': {
                render: this.loadPerson
            },
            'filetab': {
                afterrender: this.addClickEventToPanel
            },
            'administrationtab': {
                afterrender: this.addClickEventToPanel
            },
            'gthtab': {
                afterrender: this.addClickEventToPanel
            },
            'reportsTab': {
                afterrender: this.addClickEventToPanel
            }

        });
    },

    loadPerson: function(component, opts) {
        // uid is created in default.aspx (windowsaccount)
        eBookAT.Splash.show();
        if (uid != '' && uid != null) {
            Ext.Ajax.request({
                url: '/EY.com.eBook.API.5/HomeService.svc/Login'
                , method: 'POST'
                //, params: Ext.encode({ cwadc: { WindowsAccount: uid} })
                , jsonData: { cwadc: { WindowsAccount: uid} }
                , success: this.onLoginSuccess
                , failure: this.onLoginFailure
                , scope: this
            });
        } else {
            return;
        }
    },

    onLoginSuccess: function(resp, opts) {
        eBookAT.Splash.hide();
        var obj = Ext.decode(resp.responseText);
        var result = obj.LoginResult;

        eBookAT.User = result;
        eBookAT.User.LoggedOnEl = Ext.get('eBookAT-User-inner');
        eBookAT.User.LoggedOnEl.update(eBookAT.User.fn + ' ' + eBookAT.User.ln);

        this.loadUserView();
    },

    onLoginFailure: function(resp, opts) {
        eBookAT.Splash.hide();
        this.loadPerson();
        //Ext.Msg.alert('Something went wrong, please reload the application');
        //console.log(resp.responseText);
    },

    loadUserView: function() {
        var tabPanel = Ext.getCmp('EBOOKATtab');
        var hasRole = false;
        Ext.Array.each(eBookAT.User.rs, function(role, index, roles) {
            if ((role.r).indexOf('eBookAT') >= 0) {
                hasRole = true;
                switch (role.r) {
                    case 'eBookAT Files':
                        tabPanel.add({ title: 'Files', xtype: 'filetab', flex: 1 });
                        break;
                    case 'eBookAT Administration':
                        tabPanel.add({ title: 'Administration', xtype: 'administrationtab', flex: 1 });
                        break;
                    case 'eBookAT GTH':
                        tabPanel.add({ title: 'GTH', xtype: 'gthtab', flex: 1 });
                        break;
                    case 'eBookAT Reports':
                        tabPanel.add({ title: 'Reports', xtype: 'reportsTab', flex: 1 });
                        break;
                }
            }
        });

        if (!hasRole) {
            Ext.Msg.show({
                title: 'Error',
                msg: 'You have insufficient access rights. Please contact the eBook-team',
                buttons: Ext.Msg.OK,
                icon: Ext.Msg.WARNING
            });
        } else {
            tabPanel.setActiveTab(0);
        }
    },

    addClickEventToPanel: function(panel) {
        panel.body.on('click', this.onBodyClick, this);
    },

    onBodyClick: function(e, t, eOpts) {
        var menuItem = e.getTarget('.eBookAT-menu-item');

        if (menuItem) {
            this.process(menuItem.id);
        }
    },

    process: function(id) {
        switch (id) {
            case 'eBookAT-file-reopen':
                var fileReopenWindow = Ext.widget('filereopenwindow');
                fileReopenWindow.show();
                break;
            case 'eBookAT-file-close':
                var fileCloseWindow = Ext.widget('fileclosewindow');
                fileCloseWindow.show();
                break;
            case 'eBookAT-biztax-failed':
                var biztaxFailedWindow = Ext.widget('filebiztaxfailwindow');
                biztaxFailedWindow.show();
                break;
            case 'eBookAT-biztax-success':
                var biztaxSuccessWindow = Ext.widget('filebiztaxsuccesswindow');
                biztaxSuccessWindow.show();
                break;
            case 'eBookAT-coefficient-manage':
                var coefficientWindow = Ext.widget('coefficientwindow');
                coefficientWindow.show();
                break;
            case 'eBookAT-champions-manage':
                var championsWindow = Ext.widget('championswindow');
                championsWindow.show();
                break;
            case 'eBookAT-GTH-persons':
                var GTHPersonWindow = Ext.widget('gthpersonwindow');
                GTHPersonWindow.show();
                break
            case 'eBookAT-GTH-roles':
                var GTHRolesWindow = Ext.widget('gthroleswindow');
                GTHRolesWindow.show();
                break;
            case 'eBookAT-GTH-replacements':
                var GTHReplacementWindow = Ext.widget('gthreplacementwindow');
                GTHReplacementWindow.show();
                break;
            case 'eBookAT-caching':
                this.reloadStaticDataToCache();
                break;
            case 'eBookAT-clientEngagementLetter':
                var window = Ext.widget('clientengagementletterwindow');
                window.show();
                break;
            case 'eBookAT-repository':
                var window = Ext.widget('repositorywindow');
                window.show();
                break;
            case 'eBookAT-help':
                var window = Ext.widget('helpwindow');
                window.show();
                break;  
        }
    },

    reloadStaticDataToCache: function () {

        Ext.Msg.show({
            title: 'Warning',
            msg: 'Reloading the static data into the LIVE cache applies all altered translations and coefficients to live production. Are you sure you wish to reload the static data?',
            buttons: Ext.Msg.YESNO,
            icon: Ext.Msg.WARNING,
            fn: function(btn) {
                if (btn == 'yes') {
                    Ext.Ajax.request({
                        url: '/EY.com.eBook.API.5/AdministrationService.svc/ReloadMainCache'
                        , method: 'POST'
                        , callback: function(options, success, response) {
                            if (success) {
                                Ext.Msg.show({
                                    title: 'Success',
                                    msg: 'The static data is succesfully reloaded into the LIVE cache!',
                                    buttons: Ext.Msg.OK,
                                    icon: Ext.Msg.SUCCESS
                                });
                            }
                        }
                        , scope: this
                    });
                }
            },
            scope: this
        });

    }
});