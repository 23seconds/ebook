﻿Ext.define('eBookAT.controller.HelpController', {
    extend: 'Ext.app.Controller',

    stores: ['Worksheet', 'WorksheetHelp', 'WorksheetHelpEdit'],

    models: ['Worksheet', 'WorksheetHelp'],

    requires: [
        'eBookAT.view.administration.help.Window',
        'eBookAT.view.administration.help.WorksheetGrid',
        'eBookAT.view.administration.help.add.Window',
        'eBookAT.view.administration.help.edit.Window'
    ],

    init: function() {
        this.control({
            'helpwindow': {
                render: this.loadWorksheets,
                close: this.onHelpWindowClose
            },
            'worksheetgrid': {

                itemclick: this.loadHelp
            },
            '#btnHelpAdd': {
                click: this.openAddHelpWindow
            },
            '#btnSaveHelp': {
                click: this.saveHelp
            },
            '#btnUpdateHelp': {
                click: this.updateHelp
            },
            'helpdataview': {
                afterrender: this.addClickEventToPanel
            },
            'htmleditorblock': {
                afterrender: this.populateFields
            }
        });
    },

    onHelpWindowClose: function() {
        var worksheetHelpStore = this.getWorksheetHelpStore();
        this.saveOrder(worksheetHelpStore);
        var worksheetHelpEditStore = this.getWorksheetHelpEditStore();
        worksheetHelpEditStore.removeAll();
        worksheetHelpStore.removeAll();
    },

    loadWorksheets: function() {
        var worksheetStore = this.getWorksheetStore();
        worksheetStore.load();


    },

    addClickEventToPanel: function(view) {
        view.on('itemclick', this.onBodyClick, this);
    },

    onBodyClick: function(click, record, item, index, e, eOpts) {
        var button = e.getTarget('.button');
        var store = this.getWorksheetHelpStore();
        switch (button.className) {
            case 'eBookAT-worksheet-help-button-move-up button':
                if (index > 0) {
                    store.remove(record);
                    store.insert(index - 1, record);
                }

                break;
            case 'eBookAT-worksheet-help-button-move-down button':
                if ((index + 1) < store.getCount()) {
                    store.remove(record);
                    store.insert(index + 1, record);
                }
                break;
            case 'eBookAT-worksheet-help-button-edit button':
                this.openEditHelpWindow(button.attributes['helpid'].nodeValue, click.up('window').down('#lblWorksheet').worksheetTypeID);
                break;

            case 'eBookAT-worksheet-help-button-delete button':
                this.deleteHelp(button.attributes['helpid'].nodeValue);
                break;

        }
    },

    deleteHelp: function(worksheetHelpId) {
        Ext.Msg.show({
            title: 'Warning',
            msg: "Are you sure you want to delete this help message?",
            buttons: Ext.Msg.YESNO,
            icon: Ext.Msg.QUESTION,
            fn: function(btne) {
                if (btne == 'yes') {
                    Ext.Ajax.request({
                        url: '/EY.com.eBook.API.5/AdministrationService.svc/DeleteHelp'
                            , method: 'POST'
                            , jsonData: { cidc: { Id: worksheetHelpId} }
                            , callback: function(options, success, response) {
                                if (success) {
                                    var worksheetHelpStore = this.getWorksheetHelpStore();
                                    var worksheetHelpEditStore = this.getWorksheetHelpEditStore();
                                    worksheetHelpStore.load();
                                    worksheetHelpEditStore.load();
                                }
                            }
                            , scope: this
                    });
                }
            },
            scope: this
        });

    },

    loadHelp: function(view, record, item, index, e, eOpts) {
        var helppanel = view.up('window').down('helpdataview');
        var worksheetHelpStore = this.getWorksheetHelpStore();
        this.saveOrder(worksheetHelpStore);

        worksheetHelpStore.getProxy().extraParams = {
            cidc: { Id: record.data.id }
        }
        worksheetHelpStore.load();
        var label = view.up('window').down('#lblWorksheet');
        label.setText(record.data.name);
        label.worksheetTypeID = record.data.id;
        helppanel.update();

        var worksheetHelpEditStore = this.getWorksheetHelpEditStore();
        worksheetHelpEditStore.getProxy().extraParams = {
            cidc: { Id: record.data.id }
        }
        worksheetHelpEditStore.load();


    },

    saveOrder: function(worksheetHelpStore) {
        var records = new Array();
        if (worksheetHelpStore.getCount() > 0) {
            for (var i = 0; i < worksheetHelpStore.getCount(); i++) {
                var record = worksheetHelpStore.getAt(i);
                records.push({ Id: record.get('WorksheetHelpId'), Order: i + 1 });
            }

            Ext.Ajax.request({
                url: '/EY.com.eBook.API.5/AdministrationService.svc/UpdateHelpOrder'
                                            , method: 'POST'
                                            , jsonData: { lciodc: records }
                                            , callback: function(options, success, response) {


                                            },
                scope: this
            });
        }
    },

    openAddHelpWindow: function(btn) {
        var addHelpWindow = Ext.widget('helpaddwindow');
        addHelpWindow.show();
        var label = btn.up().down('#lblWorksheet');
        addHelpWindow.setTitle("Add a help message to worksheet '" + label.text + "'");
        addHelpWindow.worksheetTypeID = label.worksheetTypeID;
    },

    openEditHelpWindow: function(worksheetHelpId, worksheetTypeId) {
        var editHelpWindow = Ext.widget('helpeditwindow');
        editHelpWindow.worksheetHelpId = worksheetHelpId;
        editHelpWindow.worksheetTypeId = worksheetTypeId
        editHelpWindow.show();
    },

    updateHelp: function(btn) {
        var window = btn.up('window')
        var tabs = window.down('helpadd').items.items;
        var records = new Array();
        for (var i = 0; i < tabs.length; i++) {
            if (tabs[i].hasData) {
                var culture = tabs[i].culture;
                var worksheetHelpId = window.worksheetHelpId;
                var worksheetTypeID = window.worksheetTypeId;
                var subject = tabs[i].down('textfield').getValue();
                var body = tabs[i].down('htmleditor').getValue();
                var order = 1;
                records.push({ whid: worksheetHelpId, wid: worksheetTypeID, c: culture, s: subject, b: body, o: order });
            }
        }

        if (records.length > 0) {
            Ext.Ajax.request({
                url: '/EY.com.eBook.API.5/AdministrationService.svc/UpdateHelp'
                                        , method: 'POST'
                                        , jsonData: { lcwhdc: records }
                                        , callback: function(options, success, response) {


                                        },
                scope: this
            });
        }

        console.log(records);
    },

    saveHelp: function(btn) {
        var window = btn.up('window')
        var tabs = window.down('helpadd').items.items;
        var isValid = true;
        var records = new Array();
        var worksheetHelpStore = this.getWorksheetHelpStore();
        var order = worksheetHelpStore.getCount() + 1;
        for (var i = 0; i < tabs.length; i++) {
            var culture = tabs[i].culture;
            var worksheetTypeID = window.worksheetTypeID;
            var subject = tabs[i].down('textfield').getValue();
            var body = tabs[i].down('htmleditor').getValue();

            if (!subject || !body) {
                Ext.Msg.show({
                    title: 'Error',
                    msg: 'Please enter a translation for all languages (Subject & Body)',
                    buttons: Ext.Msg.OK,
                    icon: Ext.Msg.WARNING
                });
                isValid = false;
                break;
            }


            records.push({ wid: worksheetTypeID, c: culture, s: subject, b: body, o: order });

        }
        if (isValid) {
            Ext.Ajax.request({
                url: '/EY.com.eBook.API.5/AdministrationService.svc/InsertHelp'
                                        , method: 'POST'
                                        , jsonData: { lcwhdc: records }
                                        , callback: function(options, success, response) {
                                            var worksheetHelpStore = this.getWorksheetHelpStore();
                                            var worksheetHelpEditStore = this.getWorksheetHelpEditStore();
                                            worksheetHelpStore.load();
                                            worksheetHelpEditStore.load();
                                            window.close();
                                        },
                scope: this
            });

        }
    },

    populateFields: function(pnl, eOpts) {
        var window = pnl.up('window');
        if (window.type == 'edit') {
            var worksheetHelpEditStore = this.getWorksheetHelpEditStore();
            var record = worksheetHelpEditStore.queryBy(function(record, id) {
                if (record.get('Culture') == pnl.culture && record.get('WorksheetHelpId') == window.worksheetHelpId) {
                    return true;
                }
            });

            var txtSubject = pnl.down('textfield');
            txtSubject.setValue(record.items[0].get('Subject'));
            var htmleditor = pnl.down('htmleditor');
            htmleditor.setValue(record.items[0].get('Body'));
            pnl.ownerCt.hasData = true;
        }
    }




});