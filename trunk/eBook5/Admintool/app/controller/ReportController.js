﻿Ext.define('eBookAT.controller.ReportController', {
    extend: 'Ext.app.Controller',

    stores: ['ClientEngagementLetter',
                'ClientEngagementLetterStatus',
                'ClientEngagementLetterTemplate'],

    models: ['ClientEngagementLetter',
                'ClientEngagementLetterComboboxItems'],

    requires: [

    ],

    init: function() {
        this.control({
            'clientengagementletterwindow': {
                afterrender: this.loadComboboxes,
                close: this.emptyStore
            },
            'clientengagementlettercriteria button[itemId=searchBtn]': {
                click: this.getResults
            },
            'clientengagementlettercriteria button[itemId=exportBtn]': {
                click: this.exportResults,
                render: this.disableExportBtn
            },
            '#clientEngagementLetterStore': {
                load: this.clientEngagementLetterAfterLoad
            },
            'combobox[group=EL]': {
                expand: this.expandCombo
            }
        });
    },

    expandCombo: function(combo) {
        var statusStore = this.getClientEngagementLetterStatusStore();
        if (statusStore.data.length == 2) {
            statusStore.insert(0, { id: '', nl: '', fr: '', en: '' });
        }

        var templateStore = this.getClientEngagementLetterTemplateStore();
        if (templateStore.data.length == 3) {
            templateStore.insert(0, { id: '', nl: '', fr: '', en: '' });
        }
    },

    emptyStore: function() {
        var store = this.getClientEngagementLetterStore();
        store.removeAll();

        /*
        var statusStore = this.getClientEngagementLetterStatusStore();
        //statusStore.removeAll();
        statusStore.remove(statusStore.getRange());

        var templateStore = this.getClientEngagementLetterTemplateStore();
        templateStore.removeAll();
        */
    },

    disableExportBtn: function(btn) {
        btn.disable();
    },

    clientEngagementLetterAfterLoad: function(store) {
        console.log(store);
        // grid bottom bar info
        //btn.up('grid').down('label[itemId=total]').setText('Total: ' + store.getCount());
    },

    exportResults: function(btn) {
        var grid = btn.up('window').down('grid');
        if (grid.cceldc) {
            grid.getEl().mask('Exporting to excel...');
            Ext.Ajax.request({
                url: '/EY.com.eBook.API.5/OutputService.svc/ExportClientEngagementLetterReport'
            , method: 'POST'
            , jsonData: { cceldc: grid.cceldc}//{ cceldc: { sd: Ext.Date.format(startDate.getValue(), 'MS'), ed: Ext.Date.format(endDate.getValue(), 'MS'), stid: structureId, sid: statusCombo.getValue(), tid: templateCombo.getValue(), t: templateCombo.getRawValue()} }
            , callback: function(options, success, response) {
                if (success) {
                    grid.getEl().unmask();
                    var obj = Ext.decode(response.responseText);
                    var id = obj.ExportClientEngagementLetterReportResult.Id;
                    window.open("pickup/" + id + ".xlsx");
                    //alert('report success');
                } else {
                    alert('Export failed. Restart accountancy tools and try again.');
                    grid.getEl().unmask();
                }
            }
            , scope: this
            });
        } else {
            alert('no data in grid');
        }

    },

    getResults: function(btn) {
        var statusCombo = btn.up().down('combobox[fieldLabel=Status]');
        var templateCombo = btn.up().down('combobox[fieldLabel=Template]');
        var startDate = btn.up().down('datefield[fieldLabel=Start date]'); //.getValue();
        //startDate = ;
        var endDate = btn.up().down('datefield[fieldLabel=End date]'); //.getValue();
        //endDate = ;
        var structureId = '32F0391E-E0BD-41F9-A9C1-C04EAC3C8CB1' // 2.Engagement Letter
        var checkboxes = Ext.ComponentQuery.query('checkboxfield');
        var checkboxArray = new Array();

        if (!startDate.getValue()) {
            startDate = new Date(1900, 01, 01);
        } else {
            startDate = startDate.getValue();
        }
        if (!endDate.getValue()) {
            // today
            endDate = new Date();
        } else {
            endDate = endDate.getValue();
        }
        //load store
        var store = this.getClientEngagementLetterStore();
        var cceldc = { sd: Ext.Date.format(startDate, 'MS'), ed: Ext.Date.format(endDate, 'MS'), stid: structureId, sid: statusCombo.getValue(), tid: templateCombo.getValue(), t: templateCombo.getRawValue()/*, d: checkboxArray*/ }
        store.getProxy().extraParams = {
            cceldc: cceldc
        }
        store.load();

        store.on('load', function() {
            var window = Ext.getCmp('clientEngagementLetterWindow');

            var exportBtn = window.down('container button[itemId=exportBtn]');
            if (exportBtn) exportBtn.enable();
            var grid = window.down('grid');
            grid.cceldc = cceldc;
            var gridBbar = grid.getDockedItems('toolbar[dock="bottom"]');
            if (gridBbar.length > 0) {
                grid.removeDocked(gridBbar[0]);
            }
            var bbar = new Ext.Toolbar({
                dock: 'bottom',
                items: [
                    { xtype: 'label', html: '<b>Total: </b>' + store.getCount() },
                    '->'
                    /*
                    ,
                    { xtype: 'label', html: '<b>Date range: </b>' + startDate + ' - ' + endDate }, '-',
                    { xtype: 'label', html: '<b>Status: </b>' + statusCombo.getRawValue() }, '-',
                    { xtype: 'label', html: '<b>Template: </b>' + templateCombo.getRawValue() }
                    */
                ]
            });
            grid.addDocked(bbar);


        });





    },

    loadComboboxes: function(window) {
        var statusStore = this.getClientEngagementLetterStatusStore();
        if (statusStore.data.length == 0) {
            statusStore.getProxy().extraParams = {
                csdc: { id: "SENTSIGNEDCLIENT" }
            }
            statusStore.load();
        }

        var templateStore = this.getClientEngagementLetterTemplateStore();
        if (templateStore.data.length == 0) {
            templateStore.getProxy().extraParams = {
                cldc: { lik: "EngagementTemplate", lid: null, c: "nl-BE", ay: 2012, sd: null, ed: null, query: "" }
            }
            templateStore.load();
        }
    }



});