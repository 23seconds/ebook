﻿Ext.define('eBookAT.controller.CoefficientController', {
    extend: 'Ext.app.Controller',

    stores: ['CoefficientGroup', 'CoefficientKey', 'Coefficient'],

    models: ['CoefficientGroup', 'CoefficientKey', 'Coefficient', 'CoefficientKeySettings'],

    requires: [
        'eBookAT.view.administration.coefficient.Window',
        'eBookAT.view.administration.coefficient.add.Window',
        'eBookAT.view.administration.coefficient.edit.Window'
    ],

    init: function() {
        this.control({
            // Coefficients window
            'coefficientgroup': {
                itemclick: this.loadCoefficientKeys
            },
            'coefficientkey': {
                itemclick: this.coefficientKeyItemClick
            },
            'coefficient': {
                itemclick: this.enableDeleteButton,
                itemdblclick: this.coefficientItemDblClick
            },
            'button[type=coefficientbutton]': {
                click: this.coefficientTbarButtonClick
            },
            'coefficientwindow': {
                render: this.loadCoefficientGroups,
                close: this.closeCoefficientWindow
            },

            // Add coefficients window
            'button[itemId=coefficientAddSave]': {
                click: this.saveNewCoefficient
            },
            'button[itemId=coefficientAddCancel]': {
                click: this.CloseCoefficientAddWindow
            },
            'coefficientaddwindow': {
                beforedestroy: this.disableButtons
            },

            // Edit coefficients window
            'button[itemId=coefficientEditSave]': {
                click: this.updateCoefficient
            },
            'button[itemId=coefficientEditCancel]': {
                click: this.CloseCoefficientAddWindow
            },
            'coefficienteditwindow': {
                beforedestroy: this.disableButtons
            }

        });
    },

    loadCoefficientGroups: function() {
        var storeGroup = this.getCoefficientGroupStore();
        storeGroup.load();
    },

    loadCoefficientKeys: function(grid, record, item, index, e, eOpts) {
        var storeCoeffs = this.getCoefficientStore();
        storeCoeffs.removeAll();
        var coefficientTbarButtons = Ext.ComponentQuery.query('button[type=coefficientbutton]');
        this.disableTbarButtons(coefficientTbarButtons, true);

        var storeKeys = this.getCoefficientKeyStore();
        storeKeys.getProxy().extraParams = {
            cicdc: { Id: record.data.Id, Culture: 'nl-BE' }
        }
        storeKeys.load();
    },

    coefficientKeyItemClick: function(grid, record, item, index, e, eOpts) {
        var coefficientAddButton = Ext.ComponentQuery.query('#coefficientAdd');
        this.disableTbarButtons(coefficientAddButton, false);
        this.disableButtons();

        this.loadCoefficients(record.data.Key);
    },

    loadCoefficients: function(key) {
        var storeCoeffs = this.getCoefficientStore();
        storeCoeffs.getProxy().extraParams = {
            csdc: { String: key }
        }
        storeCoeffs.load();
    },

    closeCoefficientWindow: function(window, eOpts) {
        var stores = [
              this.getCoefficientGroupStore(),
              this.getCoefficientKeyStore(),
              this.getCoefficientStore()
        ];

        this.emptyAllStores(stores);
    },

    emptyAllStores: function(stores) {
        Ext.Array.each(stores, function(store, index, storesItSelf) {
            store.removeAll();
        });
    },

    coefficientTbarButtonClick: function(btn, e, eOpts) {
        var coefficientKeyGrid = btn.up('coefficientwindow').down('coefficientkey');
        if (coefficientKeyGrid.getSelectionModel().hasSelection()) {
            switch (btn.action) {
                case 'add':
                    var coefficientAddWindow = Ext.widget('coefficientaddwindow');
                    coefficientAddWindow.period = coefficientKeyGrid.getSelectionModel().getSelection()[0].data.Settings.Period;
                    coefficientAddWindow.show();
                    break;

                case 'delete':
                    Ext.Msg.show({
                        title: 'Warning',
                        msg: 'Are you sure you want to delete this coefficient?',
                        buttons: Ext.Msg.YESNO,
                        icon: Ext.Msg.QUESTION,
                        fn: function(btne) {
                            if (btne == 'yes') {
                                var selectedCoeff = btn.up('coefficientwindow').down('coefficient').getSelectionModel().getSelection()[0].data;
                                var id = selectedCoeff.Id;
                                var key = selectedCoeff.Key;
                                Ext.Ajax.request({
                                    url: '/EY.com.eBook.API.5/AdministrationService.svc/DeleteCoefficient'
                                    , method: 'POST'
                                    , jsonData: { cbidc: { Id: id} }
                                    , callback: function(opts, succes) {
                                        if (succes) {
                                            this.loadCoefficients(key)
                                        }
                                    }
                                    , scope: this
                                });
                                this.disableButtons();
                            }
                        },
                        scope: this
                    });
                    break;

                case 'edit':
                    this.openCoefficientEditWindow();
                    break;

            }
        }
    },

    disableTbarButtons: function(buttons, bool) {


        if (bool) {
            Ext.Array.each(buttons, function(button, index, buttonsItSelf) {
                button.disable()
            });
        } else {
            Ext.Array.each(buttons, function(button, index, buttonsItSelf) {
                button.enable()
            });
        }

    },

    enableDeleteButton: function() {
        var coefficientDeleteButton = Ext.ComponentQuery.query('#coefficientDelete')[0];
        var coefficientEditButton = Ext.ComponentQuery.query('#coefficientEdit')[0];
        var buttons = [coefficientDeleteButton, coefficientEditButton]
        this.disableTbarButtons(buttons, false);
    },

    saveNewCoefficient: function(btn) {
        var coefficientGroupGrid = Ext.ComponentQuery.query('coefficientgroup')[0];
        var coefficientKeyGrid = Ext.ComponentQuery.query('coefficientkey')[0];

        var groupId = coefficientGroupGrid.getSelectionModel().getSelection()[0].data.Id;
        var key = coefficientKeyGrid.getSelectionModel().getSelection()[0].data.Key;
        var coeff = this.getCoefficientInput(btn, coefficientKeyGrid);
        Ext.Ajax.request({
            url: '/EY.com.eBook.API.5/AdministrationService.svc/CreateCoefficient'
            , method: 'POST'
            , jsonData: { ccdc: { GroupId: groupId, Key: key, Description: coeff.description, StartDate: coeff.startDate, EndDate: coeff.endDate, AssessmentYear: coeff.assessmentYear, Value: coeff.value} }
            , callback: function(options, success, response) {
                if (success) {
                    this.loadCoefficients(key);
                    btn.up('window').close();

                }
            },
            scope: this

        });
    },

    updateCoefficient: function(btn) {
        var coefficientGrid = Ext.ComponentQuery.query('coefficient')[0];
        var coefficientKeyGrid = Ext.ComponentQuery.query('coefficientkey')[0];
        var coeff = this.getCoefficientInput(btn, coefficientKeyGrid);
        coeff.id = coefficientGrid.getSelectionModel().getSelection()[0].data.Id;
        coeff.key = coefficientGrid.getSelectionModel().getSelection()[0].data.Key;
        Ext.Ajax.request({
            url: '/EY.com.eBook.API.5/AdministrationService.svc/UpdateCoefficient'
            , method: 'POST'
            , jsonData: { ccdc: { Description: coeff.description, StartDate: coeff.startDate, EndDate: coeff.endDate, AssessmentYear: coeff.assessmentYear, Value: coeff.value, Id: coeff.id} }
            , callback: function(options, success, response) {
                if (success) {
                    this.loadCoefficients(coeff.key);
                    btn.up('window').close();
                }
            },
            scope: this
        });
    },

    getCoefficientInput: function(btn, coefficientKeyGrid) {
        var coeff = {
            description: btn.up('window').down('textfield[name=Description]').getValue(),
            value: parseFloat(btn.up('window').down('textfield[name=Value]').getValue(), 16).toFixed(6)
        };

        var period = coefficientKeyGrid.getSelectionModel().getSelection()[0].data.Settings.Period;
        if (period == 'DateRange') {
            coeff.startDate = Ext.Date.format(btn.up('window').down('textfield[name=StartDate]').getValue(), 'MS');
            coeff.endDate = Ext.Date.format(btn.up('window').down('textfield[name=EndDate]').getValue(), 'MS');
            coeff.assessmentYear = null;
        } else {
            coeff.startDate = null;
            coeff.endDate = null;
            coeff.assessmentYear = btn.up('window').down('textfield[name=AssessmentYear]').getValue();
        }

        return coeff;
    },

    CloseCoefficientAddWindow: function(btn) {
        btn.up('window').close();
    },

    coefficientItemDblClick: function(grid, record, item, index, e, eOpts) {
        this.openCoefficientEditWindow();
    },

    openCoefficientEditWindow: function() {
        var coefficientEditWindow = Ext.widget('coefficienteditwindow');
        coefficientEditWindow.show();
    },

    disableButtons: function() {
        var coefficientDeleteButton = Ext.ComponentQuery.query('#coefficientDelete')[0];
        var coefficientEditButton = Ext.ComponentQuery.query('#coefficientEdit')[0];
        var buttons = [coefficientDeleteButton, coefficientEditButton]
        this.disableTbarButtons(buttons, true);
    }





});