﻿Ext.define('eBookAT.controller.ChampionController', {
    extend: 'Ext.app.Controller',

    stores: ['Champion', 'Office'],

    models: ['Person', 'Office'],

    requires: [
        'eBookAT.view.administration.champions.Window',
        'eBookAT.view.administration.champions.add.Window'
    ],

    init: function() {
        this.control({
            'championswindow': {
                render: this.loadChampions,
                close: this.emptyStores
            },
            '#btnChampionAdd': {
                click: this.btnChampionAddClick
            },
            '#championNext': {
                click: this.navigateCard
            },
            '#championBack': {
                click: this.navigateCard
            },
            '#btnChampionDelete': {
                click: this.deleteChampion
            },
            'championsgrid': {
                itemclick: this.championItemClick
            }


        });
    },

    loadChampions: function() {
        this.loadStores();
    },

    btnChampionAddClick: function(btn) {
        var addChampionWindow = Ext.widget('championsaddwindow');
        addChampionWindow.show();
    },

    navigateCard: function(btn) {
        var window = btn.up('window');
        var windowLayout = window.getLayout();

        switch (windowLayout.getActiveItem().itemId) {
            case 'personGrid':
                if (btn.itemId == 'championNext') {
                    if (window.down('searchperson').getSelectionModel().hasSelection()) {
                        windowLayout.setActiveItem('officeGrid');
                        window.down('#championBack').enable();
                    } else {
                        Ext.Msg.show({
                            title: 'Error',
                            msg: 'Please select a person',
                            buttons: Ext.Msg.OK,
                            icon: Ext.Msg.WARNING
                        });
                    }
                }
                break;

            case 'officeGrid':
                if (btn.itemId == 'championNext') {
                    if (window.down('officegrid').getSelectionModel().hasSelection()) {
                        windowLayout.setActiveItem('confirmPnl');
                        this.selectedPerson = window.down('searchperson').getSelectionModel().getSelection()[0].data;
                        this.selectedOffice = window.down('officegrid').getSelectionModel().getSelection()[0].data;
                        window.down('confirmpnl').getEl().setHTML("Person '" + this.selectedPerson.FirstName + " " + this.selectedPerson.LastName + "' will become a CHAMPION for office '" + this.selectedOffice.Name + "'. </br></br>" +
                                                                    "Please press next to confirm");
                    } else {
                        Ext.Msg.show({
                            title: 'Error',
                            msg: 'Please select an office',
                            buttons: Ext.Msg.OK,
                            icon: Ext.Msg.WARNING
                        });
                    }

                } else if (btn.itemId == 'championBack') {
                    windowLayout.setActiveItem('personGrid');
                    window.down('#championBack').disable();
                }
                break;

            case 'confirmPnl':
                if (btn.itemId == 'championNext') {
                    Ext.Ajax.request({
                        url: '/EY.com.eBook.API.5/AdministrationService.svc/InsertNewChampion'
                        , method: 'POST'
                        , jsonData: { ccdc: { pid: this.selectedPerson.Id, oid: this.selectedOffice.Id} }
                        , callback: function(options, success, response) {
                            if (success) {
                                window.close();
                                this.loadStores();
                            }
                        }
                        , scope: this
                    });

                } else if (btn.itemId == 'championBack') {
                    windowLayout.setActiveItem('officeGrid');
                    window.down('confirmpnl').getEl().setHTML('');
                }
                break;
        }


    },

    deleteChampion: function(btn) {
        var grid = btn.up('grid');
        var selectedRecord = grid.getSelectionModel().getSelection()[0].data;
        Ext.Msg.show({
            title: 'Warning',
            msg: "Are you sure you want to delete '" + selectedRecord.FirstName + " " + selectedRecord.LastName + "' as CHAMPION for office '" + selectedRecord.Office + "'?",
            buttons: Ext.Msg.YESNO,
            icon: Ext.Msg.QUESTION,
            fn: function(btne) {
                if (btne == 'yes') {
                    Ext.Ajax.request({
                        url: '/EY.com.eBook.API.5/AdministrationService.svc/DeleteChampion'
                        , method: 'POST'
                        , jsonData: { ccdc: { pid: selectedRecord.Id, oid: selectedRecord.OfficeId} }
                        , callback: function(options, success, response) {
                            if (success) {
                                this.loadStores();

                            }
                        }
                        , scope: this
                    });
                }
            },
            scope: this
        });

    },

    championItemClick: function(grid, record, item, index, e, eOpts) {
        grid.up().down('#btnChampionDelete').enable();
    },

    loadStores: function() {
        var store = this.getChampionStore();
        store.load();
        var officeStore = this.getOfficeStore();
        officeStore.load();
        
        
        //btn.up('championAdd').disable();

    },

    emptyStores: function() {
        var store = this.getChampionStore();
        store.removeAll();
        var officeStore = this.getOfficeStore();
        officeStore.removeAll();
    }



});