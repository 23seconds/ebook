﻿Ext.define('eBookAT.store.BiztaxDeclaration', {
    extend: 'Ext.data.Store',
    model: 'eBookAT.model.BiztaxDeclaration',
    pageSize: 25,
    proxy: {
        type: 'aspwebajaxproxy',
        url: '/EY.com.eBook.API.5/BizTaxDeclarationService.svc/GetDeclarationsByClient',
        actionMethods: {
            create: 'POST',
            destroy: 'DELETE',
            read: 'POST',
            update: 'POST'
        },
        reader: {
            type: 'json',
            root: 'GetDeclarationsByClientResult'
        },
        headers: {
            'Content-Type': 'application/json; charset=utf-8'
        }
    }
});
