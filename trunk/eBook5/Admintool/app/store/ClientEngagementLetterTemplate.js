﻿Ext.define('eBookAT.store.ClientEngagementLetterTemplate', {
    extend: 'Ext.data.Store',
    model: 'eBookAT.model.ClientEngagementLetterComboboxItems',
    proxy: {
        type: 'aspwebajaxproxy',
        url: '/EY.com.eBook.API.5/ListService.svc/GetList',
        actionMethods: {
            create: 'POST',
            destroy: 'DELETE',
            read: 'POST',
            update: 'POST'
        },
        reader: {
            type: 'json',
            root: 'GetListResult'
        },
        headers: {
            'Content-Type': 'application/json; charset=utf-8'
        }
    }

});
