﻿Ext.define('eBookAT.store.File', {
    extend: 'Ext.data.Store',
    model: 'eBookAT.model.File',
    pageSize: 25,
    proxy: {
        type: 'aspwebajaxproxy',
        url: '/EY.com.eBook.API.5/FileService.svc/GetFileInfos',
        actionMethods: {
            create: 'POST',
            destroy: 'DELETE',
            read: 'POST',
            update: 'POST'
        },
        reader: {
            type: 'json',
            root: 'GetFileInfosResult'
        },
        headers: {
            'Content-Type': 'application/json; charset=utf-8'
        }
    }
});
