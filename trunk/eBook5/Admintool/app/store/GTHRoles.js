﻿Ext.define('eBookAT.store.GTHRoles', {
    extend: 'Ext.data.Store',
    model: 'eBookAT.model.GTHRoles',
    proxy: {
        type: 'aspwebajaxproxy',
        url: '/EY.com.eBook.API.5/GTHTeamService.svc/GetRoles',
        actionMethods: {
            create: 'POST',
            destroy: 'DELETE',
            read: 'POST',
            update: 'POST'
        },
        reader: {
            type: 'json',
            root: 'GetRolesResult'
        },
        headers: {
            'Content-Type': 'application/json; charset=utf-8'
        }
    }
});
