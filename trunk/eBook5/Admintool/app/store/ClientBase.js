﻿Ext.define('eBookAT.store.ClientBase', {
    extend: 'Ext.data.Store',
    model: 'eBookAT.model.ClientBase',
    proxy: {
        type: 'aspwebajaxproxy',
        url: '/EY.com.eBook.API.5/HomeService.svc/GetClients',
        actionMethods: {
            create: 'POST',
            destroy: 'DELETE',
            read: 'POST',
            update: 'POST'
        },
        reader: {
            type: 'json',
            root: 'GetClientsResult.Data'
        },
        headers: {
            'Content-Type': 'application/json; charset=utf-8'
        }
    }

});
