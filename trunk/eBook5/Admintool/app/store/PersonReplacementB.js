﻿Ext.define('eBookAT.store.PersonReplacementB', {
    extend: 'Ext.data.Store',
    model: 'eBookAT.model.GTHPersonClients',
    pageSize: 25,
    proxy: {
        type: 'aspwebajaxproxy',
        url: '/EY.com.eBook.API.5/GTHTeamService.svc/GetPersonClients',
        actionMethods: {
            create: 'POST',
            destroy: 'DELETE',
            read: 'POST',
            update: 'POST'
        },
        reader: {
            type: 'json',
            root: 'GetPersonClientsResult'
        },
        headers: {
            'Content-Type': 'application/json; charset=utf-8'
        }
    }
});
