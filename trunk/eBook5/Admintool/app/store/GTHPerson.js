﻿Ext.define('eBookAT.store.GTHPerson', {
    extend: 'Ext.data.Store',
    model: 'eBookAT.model.Person',
    pageSize: 25,
    proxy: {
        type: 'aspwebajaxproxy',
        url: '/EY.com.eBook.API.5/GTHTeamService.svc/getPersons',
        actionMethods: {
            create: 'POST',
            destroy: 'DELETE',
            read: 'POST',
            update: 'POST'
        },
        reader: {
            type: 'json',
            root: 'getPersonsResult'
        },
        headers: {
            'Content-Type': 'application/json; charset=utf-8'
        }
    }
});
