﻿Ext.define('eBookAT.store.ClientEngagementLetterStatus', {
    extend: 'Ext.data.Store',
    model: 'eBookAT.model.ClientEngagementLetterComboboxItems',
    proxy: {
        type: 'aspwebajaxproxy',
        url: '/EY.com.eBook.API.5/RepositoryService.svc/GetStatusList',
        actionMethods: {
            create: 'POST',
            destroy: 'DELETE',
            read: 'POST',
            update: 'POST'
        },
        reader: {
            type: 'json',
            root: 'GetStatusListResult'
        },
        headers: {
            'Content-Type': 'application/json; charset=utf-8'
        }
    }

});
