﻿Ext.define('eBookAT.store.Worksheet', {
    extend: 'Ext.data.Store',
    model: 'eBookAT.model.Worksheet',
    proxy: {
        type: 'aspwebajaxproxy',
        url: '../silentebook5/js/json/worksheettypes--nl-BE.json',
        actionMethods: {
            create: 'POST',
            destroy: 'DELETE',
            read: 'GET',
            update: 'POST'
        },
        reader: {
            type: 'json',
            root: 'types'
        },
        headers: {
            'Content-Type': 'application/json; charset=utf-8'
        }
    }
});
