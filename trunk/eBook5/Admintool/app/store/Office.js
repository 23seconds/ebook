﻿Ext.define('eBookAT.store.Office', {
    extend: 'Ext.data.Store',
    model: 'eBookAT.model.Office',
    pageSize: 25,
    proxy: {
        type: 'aspwebajaxproxy',
        url: '/EY.com.eBook.API.5/HomeService.svc/GetAllOffices',
        actionMethods: {
            create: 'POST',
            destroy: 'DELETE',
            read: 'POST',
            update: 'POST'
        },
        reader: {
            type: 'json',
            root: 'GetAllOfficesResult'
        },
        headers: {
            'Content-Type': 'application/json; charset=utf-8'
        }
    }
});
