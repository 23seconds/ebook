﻿Ext.define('eBookAT.store.WorksheetHelpEdit', {
    extend: 'Ext.data.Store',
    model: 'eBookAT.model.WorksheetHelp',
    proxy: {
        type: 'aspwebajaxproxy',
        url: '/EY.com.eBook.API.5/AdministrationService.svc/GetWorksheetHelpById',
        actionMethods: {
            create: 'POST',
            destroy: 'DELETE',
            read: 'POST',
            update: 'POST'
        },
        reader: {
            type: 'json',
            root: 'GetWorksheetHelpByIdResult'
        },
        headers: {
            'Content-Type': 'application/json; charset=utf-8'
        }
    }
});
