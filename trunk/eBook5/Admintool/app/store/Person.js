﻿Ext.define('eBookAT.store.Person', {
    extend: 'Ext.data.Store',
    model: 'eBookAT.model.Person',
    pageSize: 25,
    proxy: {
        type: 'aspwebajaxproxy',
        url: '/EY.com.eBook.API.5/HomeService.svc/GetPersons',
        actionMethods: {
            create: 'POST',
            destroy: 'DELETE',
            read: 'POST',
            update: 'POST'
        },
        reader: {
            type: 'json',
            root: 'GetPersonsResult.Data'
        },
        headers: {
            'Content-Type': 'application/json; charset=utf-8'
        }
    }
});
