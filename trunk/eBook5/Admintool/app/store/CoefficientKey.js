﻿Ext.define('eBookAT.store.CoefficientKey', {
    extend: 'Ext.data.Store',
    model: 'eBookAT.model.CoefficientKey',
    proxy: {
        type: 'aspwebajaxproxy',
        url: '/EY.com.eBook.API.5/AdministrationService.svc/GetCoefficientKeysInGroup',
        actionMethods: {
            create: 'POST',
            destroy: 'DELETE',
            read: 'POST',
            update: 'POST'
        },
        reader: {
            type: 'json',
            root: 'GetCoefficientKeysInGroupResult'
        },
        headers: {
            'Content-Type': 'application/json; charset=utf-8'
        }

    }
});
