﻿Ext.define('eBookAT.store.CoefficientGroup', {
    extend: 'Ext.data.Store',
    model: 'eBookAT.model.CoefficientGroup',
    proxy: {
        type: 'aspwebajaxproxy',
        url: '/EY.com.eBook.API.5/AdministrationService.svc/GetCoefficientGroups',
        actionMethods: {
            create: 'POST',
            destroy: 'DELETE',
            read: 'POST',
            update: 'POST'
        },
        reader: {
            type: 'json',
            root: 'GetCoefficientGroupsResult'
        },
        headers: {
            'Content-Type': 'application/json; charset=utf-8'
        },
        extraParams: {
            ccdc: { Culture: 'nl-BE' }
        }
    }
});
