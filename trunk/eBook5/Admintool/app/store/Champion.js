﻿Ext.define('eBookAT.store.Champion', {
    extend: 'Ext.data.Store',
    model: 'eBookAT.model.Person',
    groupField: 'Office',
    proxy: {
        type: 'aspwebajaxproxy',
        url: '/EY.com.eBook.API.5/HomeService.svc/GetAllChampions',        
        actionMethods: {
            create: 'POST',
            destroy: 'DELETE',
            read: 'POST',
            update: 'POST'
        },
        reader: {
            type: 'json',
            root: 'GetAllChampionsResult'
        },
        headers: {
            'Content-Type': 'application/json; charset=utf-8'
        }

    }
    
});
