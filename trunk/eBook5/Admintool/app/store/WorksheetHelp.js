﻿Ext.define('eBookAT.store.WorksheetHelp', {
    extend: 'Ext.data.Store',
    model: 'eBookAT.model.WorksheetHelp',
    proxy: {
        type: 'aspwebajaxproxy',
        url: '/EY.com.eBook.API.5/AdministrationService.svc/GetWorksheetHelp',
        actionMethods: {
            create: 'POST',
            destroy: 'DELETE',
            read: 'POST',
            update: 'POST'
        },
        reader: {
            type: 'json',
            root: 'GetWorksheetHelpResult'
        },
        headers: {
            'Content-Type': 'application/json; charset=utf-8'
        }
    }
});
