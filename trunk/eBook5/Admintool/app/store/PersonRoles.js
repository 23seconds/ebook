﻿Ext.define('eBookAT.store.PersonRoles', {
    extend: 'Ext.data.Store',
    model: 'eBookAT.model.PersonRoles',
    proxy: {
        type: 'aspwebajaxproxy',
        url: '/EY.com.eBook.API.5/GTHTeamService.svc/GetPersonRoles',
        actionMethods: {
            create: 'POST',
            destroy: 'DELETE',
            read: 'POST',
            update: 'POST'
        },
        reader: {
            type: 'json',
            root: 'GetPersonRolesResult'
        },
        headers: {
            'Content-Type': 'application/json; charset=utf-8'
        }
    }
});
