﻿Ext.override(Ext.data.proxy.Ajax, { timeout: 120000 });
Ext.define('eBookAT.store.ClientEngagementLetter', {
    extend: 'Ext.data.Store',
    model: 'eBookAT.model.ClientEngagementLetter',
    id: 'clientEngagementLetterStore',
    proxy: {
        type: 'aspwebajaxproxy',
        url: '/EY.com.eBook.API.5/AdministrationService.svc/GetClientEngagementLetterReport',
        actionMethods: {
            create: 'POST',
            destroy: 'DELETE',
            read: 'POST',
            update: 'POST'
        },
        reader: {
            type: 'json',
            root: 'GetClientEngagementLetterReportResult'
        },
        headers: {
            'Content-Type': 'application/json; charset=utf-8'
        }
    }

});
