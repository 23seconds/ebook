﻿gohExt.define('eBookAT.store.Repository', {
    extend: 'Ext.data.TreeStore',
    model: 'eBookAT.model.Office',
    pageSize: 25,
    proxy: {
        type: 'aspwebajaxproxy',
        url: '/EY.com.eBook.API.5/RepositoryService.svc/GetStructureTree',
        actionMethods: {
            create: 'POST',
            destroy: 'DELETE',
            read: 'POST',
            update: 'POST'
        },
        reader: {
            type: 'json',
            root: 'GetStructureTreeResult'
        },
        headers: {
            'Content-Type': 'application/json; charset=utf-8'
        }
    }
});
