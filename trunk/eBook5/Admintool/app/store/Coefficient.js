﻿Ext.define('eBookAT.store.Coefficient', {
    extend: 'Ext.data.Store',
    model: 'eBookAT.model.Coefficient',
    proxy: {
        type: 'aspwebajaxproxy',
        url: '/EY.com.eBook.API.5/AdministrationService.svc/GetCoefficientsInKeys',
        actionMethods: {
            create: 'POST',
            destroy: 'DELETE',
            read: 'POST',
            update: 'POST'
        },
        reader: {
            type: 'json',
            root: 'GetCoefficientsInKeysResult'
        },
        headers: {
            'Content-Type': 'application/json; charset=utf-8'
        }        
    }
});
