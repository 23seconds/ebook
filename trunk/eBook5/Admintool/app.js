﻿Ext.require('Ext.app.Controller');
//Ext.require('Ext.chart.*');
//Ext.require('Ext.chart.axis.Gauge'); //
//Ext.require('Ext.chart.series.*');
Ext.require('Ext.container.Viewport');
Ext.require('Ext.container.Container');
Ext.require('Ext.data.Model');
Ext.require('Ext.data.proxy.Ajax');
Ext.require('Ext.data.Store');
Ext.require('Ext.data.TreeStore');
Ext.require('Ext.form.*');
Ext.require('Ext.form.field.*');
Ext.require('Ext.grid.*');
Ext.require('Ext.grid.Panel');
Ext.require('Ext.grid.column.Action');
Ext.require('Ext.grid.column.Date');
Ext.require('Ext.ux.CheckColumn');
Ext.require('Ext.grid.plugin.DragDrop');
Ext.require('Ext.grid.plugin.CellEditing');
Ext.require('Ext.grid.feature.Grouping');
Ext.require('Ext.grid.feature.GroupingSummary');
Ext.require('Ext.view.View');
Ext.require('Ext.panel.Panel');
Ext.require('Ext.tab.Panel');
Ext.require('Ext.toolbar.Toolbar');
Ext.require('Ext.toolbar.Paging');
Ext.require('Ext.Template');
Ext.require('Ext.tree.Panel');
//Ext.require('Ext.ux.GMapPanel');
//Ext.require('Ext.ux.grid.FiltersFeature');
//Ext.require('Ext.ux.LiveSearchGridPanel');
Ext.require('Ext.window.Window');
Ext.require('Ext.Date');
Ext.require('Ext.Ajax');
Ext.require('Ext.layout.container.*');
Ext.require('Ext.selection.CheckboxModel');
Ext.require('Ext.ComponentManager');
Ext.require('Ext.tip.QuickTipManager');
Ext.require('eBookAT.*');

Ext.Date.formatFunctions.MS = function() {

    return '\/Date(' + this.getTime() + ')\/';
};

//Ext.require('PMT.*');
//Ext.Loader.setConfig({
//    disableCaching: false
//});
Ext.application({
    name: 'eBookAT',
    appFolder: 'app',

    requires: [
		'eBookAT.store.AspWebAjaxProxy'
	],

    controllers: [
        'MainController',
        'CoefficientController',
        'GTHController',
        'FileController',
        'ChampionController',
        'ReportController',
        'HelpController'
    //,
    //'ReposityController'
    ],

    launch: function () {
        Ext.create('Ext.container.Viewport', {
            layout: 'border',
            items: [
                {
                    xtype: 'GlobalView',
                    region: 'center'
                }
            ],
            listeners: {
                render: {
                    fn: function () {
                        eBookAT.Splash = {
                            el: Ext.get('eBookAT-startload'),
                            maskEl: Ext.get('eBookAT-mask'),
                            msgEl: Ext.get('eBookAT-start-loader-text'),
                            hide: function () {
                                eBookAT.Splash.maskEl.hide();
                                eBookAT.Splash.el.hide();
                            },
                            show: function (txt) {
                                if (txt != '' && txt) {
                                    eBookAT.Splash.setMessage(txt);
                                }
                                eBookAT.Splash.maskEl.show();
                                eBookAT.Splash.el.show();
                            },
                            setMessage: function (txt) {
                                eBookAT.Splash.msgEl.update(txt);
                            }
                        };

                    }
                }
            }

        });
    }

});

