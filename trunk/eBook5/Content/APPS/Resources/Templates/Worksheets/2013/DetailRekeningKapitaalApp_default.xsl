<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" exclude-result-prefixes="Helper Translations File Client" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:Helper="urn:Helper" xmlns:Translations="urn:Translations" xmlns:File="urn:File" xmlns:Client="urn:Client">
  <xsl:decimal-format name="NonNumber" NaN="0" zero-digit="0"/>
  <xsl:variable name="WorksheetLabelFontsize" select="string('10')"/>
  <xsl:variable name="WorksheetFontsize" select="string('10')"/>
  <xsl:variable name="WorksheetNumbersize" select="string('9')"/>
  <xsl:variable name="WorksheetSmallFontsize" select="string('8')"/>
  <xsl:variable name="WorksheetTotalsFontsize" select="string('10')"/>
  <xsl:variable name="WorksheetSpacing" select="1.1"/>
  <xsl:decimal-format name="euro" decimal-separator="," grouping-separator="."/>
  <xsl:variable name="NumberFormatting" select="string('#.##0,00')"/>
  <xsl:variable name="NumberFormattingNoDigit" select="string('#.##0')"/>
  <xsl:variable name="FONT" select="string('C:\WINDOWS\Fonts\EYInterstate-Light.ttf')"/>
  <xsl:param name="RuleAppDef"/>
  <xsl:param name="Culture"/>
  <xsl:template match="DetailRekeningKapitaal">
    <Root orientation="landscape">
      <xsl:if test="count(Overzicht/*)>0">        
		  <xsl:call-template name="RENDERTAB">
			<xsl:with-param name="nodelist" select="Overzicht/*"/>
			<xsl:with-param name="title" select="Translations:GetTranslation('DetailRekeningKapitaal','Overzicht','Overzicht')"/>
		  </xsl:call-template>              
      </xsl:if>
      <xsl:if test="count(Overzicht/*)=0">
        <phrase>WORKSHEET HAS NO DATA count(Overzicht)</phrase>
      </xsl:if>
    </Root>
  </xsl:template>
  <xsl:template name="RENDERTAB">
    <xsl:param name="nodelist"/>
    <xsl:param name="title"/>
    <xsl:if test="count($nodelist)>0">
      <table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" columns="6" width="100%" align="Left" cellspacing="0">
        <row>
          <cell verticalalign="top" header="true" colspan="6" horizontalalign="left">
            <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}" fontstyle="bold underline">
              <xsl:value-of select="$title"/>
            </phrase>
            <newline/>
          </cell>
        </row>		
		<row>
          <cell horizontalalign="Left" >
            <phrase size="{$WorksheetLabelFontsize}" fontstyle="underline">
				<xsl:value-of select="Translations:GetTranslation('DetailRekeningKapitaalOverzicht','Datum','Datum')"/>
			</phrase>
			<newline/>
          </cell>
		  <cell horizontalalign="Left">
            <phrase size="{$WorksheetLabelFontsize}" fontstyle="underline">
				<xsl:value-of select="Translations:GetTranslation('DetailRekeningKapitaalOverzicht','Omschrijving','Omschrijving')"/>
			</phrase>
          </cell>
		  <cell horizontalalign="right">
            <phrase size="{$WorksheetLabelFontsize}" fontstyle="underline">
				<xsl:value-of select="Translations:GetTranslation('DetailRekeningKapitaalOverzicht','InbrengVanGeld','InbrengVanGeld')"/>
			</phrase>
          </cell>
		  <cell horizontalalign="right">
            <phrase size="{$WorksheetLabelFontsize}" fontstyle="underline">
				<xsl:value-of select="Translations:GetTranslation('DetailRekeningKapitaalOverzicht','InbrengVanNatura','InbrengVanNatura')"/>
			</phrase>
          </cell>
		  <cell horizontalalign="right">
            <phrase size="{$WorksheetLabelFontsize}" fontstyle="underline">
				<xsl:value-of select="Translations:GetTranslation('DetailRekeningKapitaalOverzicht','BelasteReserves','BelasteReserves')"/>
			</phrase>
          </cell>
		  <cell horizontalalign="right">
            <phrase size="{$WorksheetLabelFontsize}" fontstyle="underline">
				<xsl:value-of select="Translations:GetTranslation('DetailRekeningKapitaalOverzicht','VrijgesteldeReserves','VrijgesteldeReserves')"/>
			</phrase>
          </cell>
        </row>
		<row>
          <cell colspan="6">
            <phrase>&#160;</phrase>
            <newline/>
          </cell>
        </row>
        <xsl:for-each select="$nodelist">
          <row>
            <cell verticalalign="top" horizontalalign="left"  colspan="1">
              <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}">
				<xsl:if test="Datum">
                  <xsl:value-of select="ms:format-date(Datum, 'dd/MM/yyyy')"/>
                </xsl:if>                
              </phrase>
            </cell>
			<cell verticalalign="top" horizontalalign="left"  colspan="1">
              <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}">				
                <xsl:if test="Omschrijving">
                  <xsl:value-of select="Omschrijving"/>
                </xsl:if>
              </phrase>
            </cell>
            <cell verticalalign="top" horizontalalign="right"  colspan="1">
              <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
                <xsl:value-of select="format-number(InbrengVanGeld,$NumberFormatting,'euro')"/>€
              </phrase>
            </cell>
			<cell verticalalign="top" horizontalalign="right" colspan="1">
              <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
                <xsl:value-of select="format-number(InbrengVanNatura,$NumberFormatting,'euro')"/>€
              </phrase>
            </cell>
			<cell verticalalign="top" horizontalalign="right" colspan="1">
              <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
                <xsl:value-of select="format-number(BelasteReserves,$NumberFormatting,'euro')"/>€
              </phrase>
            </cell>
			<cell verticalalign="top" horizontalalign="right" colspan="1">
              <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
                <xsl:value-of select="format-number(VrijgesteldeReserves,$NumberFormatting,'euro')"/>€
              </phrase>
            </cell>
          </row>
        </xsl:for-each>
        <row>
          <cell colspan="6">
            <phrase>&#160;</phrase>
            <newline/>
          </cell>
        </row>
        <row>
          <cell verticalalign="top" horizontalalign="left" colspan="2">
            <phrase size="{$WorksheetLabelFontsize}" fontstyle="underline">
				<xsl:value-of select="Translations:GetTranslation('DetailRekeningKapitaalOverzicht','Subtotaal','Subtotaal')"/>
			</phrase>
          </cell>
          <cell verticalalign="top" horizontalalign="right">
            <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" fontstyle="bold">
              <xsl:value-of select="format-number(sum($nodelist/InbrengVanGeld),$NumberFormatting,'euro')"/>€
            </phrase>
          </cell>
		  <cell verticalalign="top" horizontalalign="right">
            <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" fontstyle="bold">
              <xsl:value-of select="format-number(sum($nodelist/InbrengVanNatura),$NumberFormatting,'euro')"/>€
            </phrase>
          </cell>
		  <cell verticalalign="top" horizontalalign="right">
            <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" fontstyle="bold">
              <xsl:value-of select="format-number(sum($nodelist/BelasteReserves),$NumberFormatting,'euro')"/>€
            </phrase>
          </cell>
		  <cell verticalalign="top" horizontalalign="right">
            <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" fontstyle="bold">
              <xsl:value-of select="format-number(sum($nodelist/VrijgesteldeReserves),$NumberFormatting,'euro')"/>€
            </phrase>
          </cell>		  
        </row>
		<row>
          <cell verticalalign="top" horizontalalign="left" colspan="5">
            <phrase size="{$WorksheetLabelFontsize}" fontstyle="underline">
				<xsl:value-of select="Translations:GetTranslation('DetailRekeningKapitaalOverzicht','Grandtotaal','Grandtotaal')"/>
			</phrase>
          </cell>
          <cell verticalalign="top" horizontalalign="right">
            <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" fontstyle="bold  underline">
              <xsl:value-of select="format-number((sum($nodelist/InbrengVanGeld) + sum($nodelist/InbrengVanNatura) + sum($nodelist/BelasteReserves) + sum($nodelist/VrijgesteldeReserves)),$NumberFormatting,'euro')"/>€
            </phrase>
          </cell>	  
        </row>
      </table>
      <newline/>
      <newline/>
    </xsl:if>
  </xsl:template>
</xsl:stylesheet>