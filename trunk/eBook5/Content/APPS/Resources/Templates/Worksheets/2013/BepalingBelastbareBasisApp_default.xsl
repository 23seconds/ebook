<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" exclude-result-prefixes="Helper Translations File Client" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:Helper="urn:Helper" xmlns:Translations="urn:Translations" xmlns:File="urn:File" xmlns:Client="urn:Client">
  <xsl:decimal-format name="NonNumber" NaN="0" zero-digit="0"/>
  <xsl:variable name="WorksheetLabelFontsize" select="string('10')"/>
  <xsl:variable name="WorksheetFontsize" select="string('8')"/>
  <xsl:variable name="WorksheetNumbersize" select="string('6')"/>
  <xsl:variable name="WorksheetSmallFontsize" select="string('8')"/>
  <xsl:variable name="WorksheetTotalsFontsize" select="string('10')"/>
  <xsl:variable name="WorksheetSpacing" select="1.3"/>
  <xsl:decimal-format name="euro" decimal-separator="," grouping-separator="."/>
  <xsl:variable name="NumberFormatting" select="string('#.##0,00')"/>
  <xsl:variable name="NumberFormattingNoDigit" select="string('#.##0')"/>
  <xsl:variable name="FONT" select="string('C:\WINDOWS\Fonts\EYInterstate-Light.ttf')"/>
  <xsl:param name="RuleAppDef"/>
  <xsl:param name="Culture"/>
  <xsl:template match="BepalingBasis">
    <Root>
      <xsl:apply-templates select="*"/>
    </Root>
  </xsl:template>
 
  <xsl:template match="Overzicht">
    <table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" width="100%" align="Center" cellspacing="0" columns="4" widths="64;12;12;12">
      <row>
        <cell colspan="4">
          <paragraph style="text-decoration:underline;">
            <xsl:value-of select="Translations:GetTranslation('BepalingBasis','Overzicht','Overzicht')"></xsl:value-of>
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="4">
          <paragraph>
            <space></space>
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="10">
            <xsl:value-of select="Translations:GetTranslation('General','GereserveerdeWinst','GereserveerdeWinst')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="1">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(GereserveerdeWinst[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
        <cell colspan="1">
          <paragraph>
            <space></space>
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="10">
            <xsl:value-of select="Translations:GetTranslation('General','Dividenden','Dividenden')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="1">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(Dividenden[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
        <cell colspan="1">
          <paragraph>
            <space></space>
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="10">
            <xsl:value-of select="Translations:GetTranslation('General','VerkrijgingEigenAandelen','VerkrijgingEigenAandelen')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="1">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(VerkrijgingEigenAandelen[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
        <cell colspan="1">
          <paragraph>
            <space></space>
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="10">
            <xsl:value-of select="Translations:GetTranslation('General','OverlijdenUittredingUitsluitingVennoot','OverlijdenUittredingUitsluitingVennoot')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="1">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(OverlijdenUittredingUitsluitingVennoot[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
        <cell colspan="1">
          <paragraph>
            <space></space>
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="10">
            <xsl:value-of select="Translations:GetTranslation('General','VerdelingMaatschappelijkVermogen','VerdelingMaatschappelijkVermogen')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="1">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(VerdelingMaatschappelijkVermogen[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
        <cell colspan="1">
          <paragraph>
            <space></space>
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="3">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="text-decoration:underline;">
            <xsl:value-of select="Translations:GetTranslation('General','StatAccountingResult','StatAccountingResult')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="1">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="text-decoration:underline;">
            <xsl:value-of select="format-number(sum(StatAccountingResult[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="4">
          <paragraph>
            <space></space>
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="10">
            <xsl:value-of select="Translations:GetTranslation('General','TotalBelasteReserves','TotalBelasteReserves')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="1">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(TotalBelasteReserves[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
        <cell colspan="1">
          <paragraph>
            <space/>
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="10">
            <xsl:value-of select="Translations:GetTranslation('General','TotalAanpassingenBelasteReserves','TotalAanpassingenBelasteReserves')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="1">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(TotalAanpassingenBelasteReserves[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
        <cell colspan="1">
          <paragraph>
            <space/>
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="10">
            <xsl:value-of select="Translations:GetTranslation('General','NietAftrekbareBelastingen','NietAftrekbareBelastingen')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="1">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(NietAftrekbareBelastingen[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
        <cell colspan="1">
          <paragraph>
            <space/>
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="10">
            <xsl:value-of select="Translations:GetTranslation('General','TotaalVerworpenUitgaven','TotaalVerworpenUitgaven')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="1">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(TotaalVerworpenUitgaven[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
        <cell colspan="1">
          <paragraph>
            <space/>
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="3">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="text-decoration:underline;">
            <xsl:value-of select="Translations:GetTranslation('General','BelastbaarResultaat','BelastbaarResultaat')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="1">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="text-decoration:underline;">
            <xsl:value-of select="format-number(sum(BelastbaarResultaat[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="4">
          <paragraph>
            <space></space>
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="10">
            <xsl:value-of select="Translations:GetTranslation('General','TotaalBelastbareBestandenZonderAftrekMin','TotaalBelastbareBestandenZonderAftrekMin')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="1">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(TotaalBelastbareBestandenZonderAftrekMin[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
        <cell colspan="1">
          <paragraph>
            <space/>
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="3">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="text-decoration:underline;">
            <xsl:value-of select="Translations:GetTranslation('General','SubTotaalBelastbaarVoorAftrek','SubTotaalBelastbaarVoorAftrek')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="1">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="text-decoration:underline;">
            <xsl:value-of select="format-number(sum(SubTotaalBelastbaarVoorAftrek[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="4">
          <paragraph>
            <space></space>
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="10">
            <xsl:value-of select="Translations:GetTranslation('General','OpdelingNaarOorsprongBijVerdragVrijgesteldInkomen','OpdelingNaarOorsprongBijVerdragVrijgesteldInkomen')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="1">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(OpdelingNaarOorsprongBijVerdragVrijgesteldInkomen[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
        <cell colspan="1">
          <paragraph>
            <space/>
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="3">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="font-weight:bold;text-decoration:underline;">
            <xsl:value-of select="Translations:GetTranslation('General','BasisVoorFiscaleAftrek','BasisVoorFiscaleAftrek')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="1">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="font-weight:bold;text-decoration:underline;">
            <xsl:value-of select="format-number(sum(BasisVoorFiscaleAftrek[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
    </table>
    <newpage/>
  </xsl:template>
  <xsl:template match="BelasteReserves">
    <table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" width="100%" align="Center" cellspacing="0" columns="4" widths="64;12;12;12">
      <row>
        <cell colspan="4">
          <paragraph style="text-decoration:underline;">
            <xsl:value-of select="Translations:GetTranslation('BepalingBasis','BelasteReserves','BelasteReserves')"></xsl:value-of>
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="4">
          <paragraph>
            <space></space>
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="4">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="text-decoration:underline;">
            <xsl:value-of select="Translations:GetTranslation('BelasteReservesItem','TempDiff','TempDiff')"></xsl:value-of>
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="10">
            <xsl:value-of select="Translations:GetTranslation('BelasteReservesItem','TDKapitaalEnUitgifte','TDKapitaalEnUitgifte')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(TDKapitaalEnUitgifte[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="10">
            <xsl:value-of select="Translations:GetTranslation('BelasteReservesItem','TDHerwaardering','TDHerwaardering')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(TDHerwaardering[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="10">
            <xsl:value-of select="Translations:GetTranslation('BelasteReservesItem','TDTaxableProvisions','TDTaxableProvisions')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(TDTaxableProvisions[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="10">
            <xsl:value-of select="Translations:GetTranslation('BelasteReservesItem','TDOtherTaxableReserves','TDOtherTaxableReserves')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(TDOtherTaxableReserves[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="10">
            <xsl:value-of select="Translations:GetTranslation('BelasteReservesItem','TDTaxableDoubtfullDebtors','TDTaxableDoubtfullDebtors')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(TDTaxableDoubtfullDebtors[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="10">
            <xsl:value-of select="Translations:GetTranslation('BelasteReservesItem','TDExcessDeprications','TDExcessDeprications')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(TDExcessDeprications[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="10">
            <xsl:value-of select="Translations:GetTranslation('BelasteReservesItem','TDUnderestimationAssetsOverestimationLiabilities','TDUnderestimationAssetsOverestimationLiabilities')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(TDUnderestimationAssetsOverestimationLiabilities[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="10">
            <xsl:value-of select="Translations:GetTranslation('BelasteReservesItem','TDOverestimationLiabilities','TDOverestimationLiabilities')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(TDOverestimationLiabilities[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="3">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="10">
            <xsl:value-of select="Translations:GetTranslation('BelasteReservesItem','TDTotal','TDTotal')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="1">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(TDTotal[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="4">
          <paragraph>
            <space/>
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="4">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="text-decoration:underline;">
            <xsl:value-of select="Translations:GetTranslation('BelasteReservesItem','OhterAdjustments','OhterAdjustments')"></xsl:value-of>
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="10">
            <xsl:value-of select="Translations:GetTranslation('BelasteReservesItem','OACapitalGainsOnShares','OACapitalGainsOnShares')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(OACapitalGainsOnShares[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="10">
            <xsl:value-of select="Translations:GetTranslation('BelasteReservesItem','OADefinitiveExemptionTaxShelter','OADefinitiveExemptionTaxShelter')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(OADefinitiveExemptionTaxShelter[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="10">
            <xsl:value-of select="Translations:GetTranslation('BelasteReservesItem','OAExemptionRegionalPrem','OAExemptionRegionalPrem')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(OAExemptionRegionalPrem[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="10">
            <xsl:value-of select="Translations:GetTranslation('BelasteReservesItem','OAOthers','OAOthers')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(OAOthers[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="3">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="font-weight:bold;" indentationleft="10">
            <xsl:value-of select="Translations:GetTranslation('BelasteReservesItem','OATotal','OATotal')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="1">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="font-weight:bold;">
            <xsl:value-of select="format-number(sum(OATotal[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
    </table>
    <newpage></newpage>
  </xsl:template>
  <xsl:template match="VerworpenUitgaven">
    <table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" width="100%" align="Center" cellspacing="0" columns="4" widths="64;12;12;12">
      <row>
        <cell colspan="4">
          <paragraph>
            <xsl:value-of select="Translations:GetTranslation('BepalingBasis','VerworpenUitgaven','VerworpenUitgaven')"></xsl:value-of>
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="4">
          <paragraph>
            <space></space>
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="4">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="font-weight:bold">
            <xsl:value-of select="DisallowedExpenses"></xsl:value-of>
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="3">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="10">
            <xsl:value-of select="Translations:GetTranslation('VerworpenUitgavenItem','PDDisallowedCurrentIncomeTaxExpenses','PDDisallowedCurrentIncomeTaxExpenses')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="1">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(PDDisallowedCurrentIncomeTaxExpenses[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="20">
            <xsl:value-of select="Translations:GetTranslation('VerworpenUitgavenItem','DERegionalTaxes','DERegionalTaxes')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(DERegionalTaxes[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="20">
            <xsl:value-of select="Translations:GetTranslation('VerworpenUitgavenItem','DEPenalties','DEPenalties')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(DEPenalties[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="20">
            <xsl:value-of select="Translations:GetTranslation('VerworpenUitgavenItem','DEPensions','DEPensions')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(DEPensions[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="20">
            <xsl:value-of select="Translations:GetTranslation('VerworpenUitgavenItem','DECar','DECar')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(DECar[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="20">
            <xsl:value-of select="Translations:GetTranslation('VerworpenUitgavenItem','DEReception','DEReception')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(DEReception[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="20">
            <xsl:value-of select="Translations:GetTranslation('VerworpenUitgavenItem','DERestaurant','DERestaurant')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(DERestaurant[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="20">
            <xsl:value-of select="Translations:GetTranslation('VerworpenUitgavenItem','DEClothes','DEClothes')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(DEClothes[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="20">
            <xsl:value-of select="Translations:GetTranslation('VerworpenUitgavenItem','DEExcessInterest','DEExcessInterest')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(DEExcessInterest[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="20">
            <xsl:value-of select="Translations:GetTranslation('VerworpenUitgavenItem','DEInterestLoans','DEInterestLoans')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(DEInterestLoans[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="20">
            <xsl:value-of select="Translations:GetTranslation('VerworpenUitgavenItem','DEAbnormalAdvantages','DEAbnormalAdvantages')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(DEAbnormalAdvantages[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="20">
            <xsl:value-of select="Translations:GetTranslation('VerworpenUitgavenItem','DESocial','DESocial')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(DESocial[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="20">
            <xsl:value-of select="Translations:GetTranslation('VerworpenUitgavenItem','DEDinerSportCultureEco','DEDinerSportCultureEco')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(DEDinerSportCultureEco[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="20">
            <xsl:value-of select="Translations:GetTranslation('VerworpenUitgavenItem','DECharity','DECharity')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(DECharity[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="20">
            <xsl:value-of select="Translations:GetTranslation('VerworpenUitgavenItem','DEReductionValueShares','DEReductionValueShares')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(DEReductionValueShares[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="20">
            <xsl:value-of select="Translations:GetTranslation('VerworpenUitgavenItem','DEReversalsPreviousTax','DEReversalsPreviousTax')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(DEReversalsPreviousTax[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="20">
            <xsl:value-of select="Translations:GetTranslation('VerworpenUitgavenItem','DEEmployeeCapital','DEEmployeeCapital')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(DEEmployeeCapital[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="20">
            <xsl:value-of select="Translations:GetTranslation('VerworpenUitgavenItem','DEIndemnityMissingCoupon','DEIndemnityMissingCoupon')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(DEIndemnityMissingCoupon[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="20">
            <xsl:value-of select="Translations:GetTranslation('VerworpenUitgavenItem','DEExpensesTaxShelter','DEExpensesTaxShelter')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(DEExpensesTaxShelter[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="20">
            <xsl:value-of select="Translations:GetTranslation('VerworpenUitgavenItem','DERegionalPremiumsCapital','DERegionalPremiumsCapital')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(DERegionalPremiumsCapital[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="20">
            <xsl:value-of select="Translations:GetTranslation('VerworpenUitgavenItem','DEBepaaldeStaten','DEBepaaldeStaten')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(DEBepaaldeStaten[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="20">
            <xsl:value-of select="Translations:GetTranslation('VerworpenUitgavenItem','DEOther','DEOther')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(DEOther[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="3">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="font-weight:bold">
            <xsl:value-of select="Translations:GetTranslation('VerworpenUitgavenItem','DETotal','DETotal')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="1">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="font-weight:bold">
            <xsl:value-of select="format-number(sum(DETotal[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
    </table>
    <newpage></newpage>
  </xsl:template>
  <xsl:template match="BelastbareBestZonderAftrek">
    <table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" width="100%" align="Center" cellspacing="0" columns="4" widths="64;12;12;12">
      <row>
        <cell colspan="4">
          <paragraph>
            <xsl:value-of select="Translations:GetTranslation('BepalingBasis','BelastbareBestZonderAftrek','BelastbareBestZonderAftrek')"></xsl:value-of>
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="4">
          <paragraph>
            <space></space>
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="4">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="text-decoration:underline;">
            <xsl:value-of select="TaxableElementsWithoutDeductions"></xsl:value-of>
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="20">
            <xsl:value-of select="Translations:GetTranslation('BelastbareBestZonderAftrekItem','TEReceivedAbnormal','TEReceivedAbnormal')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(TEReceivedAbnormal[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="20">
            <xsl:value-of select="Translations:GetTranslation('BelastbareBestZonderAftrekItem','TETaxableInvestmentReserve','TETaxableInvestmentReserve')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(TETaxableInvestmentReserve[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="20">
            <xsl:value-of select="Translations:GetTranslation('BelastbareBestZonderAftrekItem','TEEmployeeParticipationCapital','TEEmployeeParticipationCapital')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(TEEmployeeParticipationCapital[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="3">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="font-weight:bold;" indentationleft="10">
            <xsl:value-of select="Translations:GetTranslation('BelastbareBestZonderAftrekItem','TETotal','TETotal')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="1">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="font-weight:bold;">
            <xsl:value-of select="format-number(sum(TETotal[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
    </table>
    <newpage></newpage>
  </xsl:template>
  <xsl:template match="OpdelingNaarOorsprong">
    <table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" width="100%" align="Center" cellspacing="0" columns="4" widths="64;12;12;12">
      <row>
        <cell colspan="4">
          <paragraph>
            <xsl:value-of select="Translations:GetTranslation('BepalingBasis','OpdelingNaarOorsprong','OpdelingNaarOorsprong')"></xsl:value-of>
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="4">
          <paragraph>
            <space></space>
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="4">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="text-decoration:underline;">
            <xsl:value-of select="ItemizationResult"></xsl:value-of>
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="10">
            <xsl:value-of select="Translations:GetTranslation('OpdelingNaarOorsprongItem','IRBelgianTaxable','IRBelgianTaxable')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(IRBelgianTaxable[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="10">
            <xsl:value-of select="Translations:GetTranslation('OpdelingNaarOorsprongItem','IRNonTreatyForeignIncome','IRNonTreatyForeignIncome')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(IRNonTreatyForeignIncome[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="10">
            <xsl:value-of select="Translations:GetTranslation('OpdelingNaarOorsprongItem','IRTreatyForeignIncome','IRTreatyForeignIncome')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(IRTreatyForeignIncome[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="font-weight:bold;">
            <xsl:value-of select="Translations:GetTranslation('OpdelingNaarOorsprongItem','IRRemainingResult','IRRemainingResult')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell colspan="2" horizontalalign="right">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="font-weight:bold;">
            <xsl:value-of select="format-number(sum(IRRemainingResultBelgium[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
    </table>
    <newpage></newpage>
  </xsl:template>
  <xsl:template match="FiscaleAftrek">
    <table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" width="100%" align="Center" cellspacing="0" columns="4" widths="64;12;12;12">
      <row>
        <cell colspan="4">
          <paragraph>
            <xsl:value-of select="Translations:GetTranslation('BepalingBasis','FiscaleAftrek','FiscaleAftrek')"></xsl:value-of>
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="4">
          <paragraph>
            <space></space>
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="4">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="text-decoration:underline;">
            <xsl:value-of select="NonTaxableElements"></xsl:value-of>
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="10">
            <xsl:value-of select="Translations:GetTranslation('FiscaleAftrekItem','NTEBECharity','NTEBECharity')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(NTEBECharity[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="10">
            <xsl:value-of select="Translations:GetTranslation('FiscaleAftrekItem','NTEBEPersonnel','NTEBEPersonnel')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(NTEBEPersonnel[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="10">
            <xsl:value-of select="Translations:GetTranslation('FiscaleAftrekItem','NTEBEPersonnelSME','NTEBEPersonnelSME')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(NTEBEPersonnelSME[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="10">
            <xsl:value-of select="Translations:GetTranslation('FiscaleAftrekItem','NTEBEInternalshipPremium','NTEBEInternalshipPremium')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(NTEBEInternalshipPremium[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="10">
            <xsl:value-of select="Translations:GetTranslation('FiscaleAftrekItem','NTEBEOthers','NTEBEOthers')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(NTEBEOthers[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="10">
            <xsl:value-of select="Translations:GetTranslation('FiscaleAftrekItem','TotalNonTaxableElements','TotalNonTaxableElements')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(TotalNonTaxableElements[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="4">
          <paragraph>
            <space/>
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="font-weight:bold;" indentationleft="10">
            <xsl:value-of select="Translations:GetTranslation('FiscaleAftrekItem','ResterendBasisVoorAftrek','ResterendBasisVoorAftrek')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="font-weight:bold;">
            <xsl:value-of select="format-number(sum(ResterendBasisVoorAftrek[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
    </table>
    <newpage></newpage>
  </xsl:template>
  <xsl:template match="*"></xsl:template>
</xsl:stylesheet>