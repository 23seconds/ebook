<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" exclude-result-prefixes="Helper Translations File Client msxsl" xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:Helper="urn:Helper" xmlns:Translations="urn:Translations" xmlns:File="urn:File" xmlns:Client="urn:Client">
  <xsl:decimal-format name="NonNumber" NaN="0" zero-digit="0"/>
  <xsl:variable name="WorksheetLabelFontsize" select="string('10')"/>
  <xsl:variable name="WorksheetFontsize" select="string('10')"/>
  <xsl:variable name="WorksheetNumbersize" select="string('8')"/>
  <xsl:variable name="WorksheetSmallFontsize" select="string('8')"/>
  <xsl:variable name="WorksheetTotalsFontsize" select="string('10')"/>
  <xsl:variable name="WorksheetSpacing" select="1.1"/>
  <xsl:decimal-format name="euro" decimal-separator="," grouping-separator="."/>
  <xsl:variable name="NumberFormatting" select="string('#.##0,00')"/>
  <xsl:variable name="NumberFormattingNoDigit" select="string('#.##0')"/>
  <xsl:variable name="FONT" select="string('C:\WINDOWS\Fonts\EYInterstate-Light.ttf')"/>
  <xsl:param name="RuleAppDef"/>
  <xsl:param name="Culture"/>
  <xsl:template match="WPBTW">
    <Root orientation="landscape">
      <xsl:apply-templates select="." mode="WORKSHEET"/>
    </Root>
  </xsl:template>

  <xsl:template match="WPBTW" mode="OMZET">
    <xsl:variable name="ags" select="count(OmzetAangiftes/*)"/>
    <xsl:variable name="gbs" select="count(OmzettenGeboekt/*)"/>

    <xsl:variable name="omzetverschil">
      <xsl:call-template name="abs">
        <xsl:with-param name="n" select="(sum(OmzetAangiftes/*/Totaal))-(sum(OmzettenGeboekt/*/Saldo))"/>
      </xsl:call-template>
    </xsl:variable>
    
    <xsl:choose>
      <xsl:when test="$ags>0">
        <xsl:apply-templates select="." mode="OmzetAangiftes"/>
      </xsl:when>
      <xsl:when test="$gbs>0">
        <table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" columns="3" width="100%" align="Center" widths="60;15;15" cellspacing="0" cellpadding="1">
          <row>
            <cell verticalalign="middle" colspan="2" header="true">
              <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}" fontstyle="bold">
                <xsl:value-of select="Translations:GetTranslation('WPBTW','OmzetAangiftes','OmzetAangiftes')"/>
              </phrase>
            </cell>
            <cell verticalalign="middle" header="true" horizontalalign="right">
              <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" fontstyle="bold">
                <xsl:value-of select="format-number(0,$NumberFormatting,'euro')"/>€
              </phrase>
            </cell>
          </row>
        </table>
      </xsl:when>
    </xsl:choose>

    <xsl:choose>
      <xsl:when test="$gbs>0">
        <xsl:apply-templates select="." mode="OmzettenGeboekt">
          <xsl:with-param name="omzetverschil" select="$omzetverschil"/>
        </xsl:apply-templates>
      </xsl:when>
      <xsl:otherwise>
        <table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" columns="3" width="100%" align="Center" widths="60;15;15" cellspacing="0" cellpadding="1">
          <row>
            <cell verticalalign="middle" colspan="2" header="true">
              <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}" fontstyle="bold">
                <xsl:value-of select="Translations:GetTranslation('WPBTW','OmzetBoekhouding','OmzetBoekhouding')"/>
              </phrase>
            </cell>
            <cell verticalalign="middle" header="true" horizontalalign="right">
              <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" fontstyle="bold">
                <xsl:value-of select="format-number(0,$NumberFormatting,'euro')"/>€
              </phrase>
            </cell>
          </row>
          <row>
            <cell colspan="3">
              <phrase>&#160;</phrase>
              <newline/>
            </cell>
          </row>
          <row>
            <cell verticalalign="middle" colspan="2">
              <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}" fontstyle="bold">
                <xsl:value-of select="Translations:GetTranslation('','TeVerklarenVerschil','Te verklaren verschil')"/>
              </phrase>
            </cell>
            <cell verticalalign="middle" horizontalalign="right">
              <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" fontstyle="bold">
               <xsl:value-of select="format-number($omzetverschil,$NumberFormatting,'euro')"/>€
              </phrase>
            </cell>
          </row>
        </table>
      </xsl:otherwise>
    </xsl:choose>

    <xsl:if test="$omzetverschil>0">
      <table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" columns="3" width="100%" align="Center" widths="60;15;15" cellspacing="0" cellpadding="1">
        <row>
          <cell colspan="3">
            <phrase lineheight="{$WorksheetSpacing}"></phrase>
            <newline/>
            <phrase lineheight="{$WorksheetSpacing}"></phrase>
          </cell>
        </row>
        <row>
          <cell verticalalign="middle" colspan="3" header="true">
            <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}" fontstyle="bold">
              <xsl:value-of select="Translations:GetTranslation('','Verklaring','Verklaring')"/>
            </phrase>
          </cell>
        </row>
        <row>
          <cell verticalalign="middle" horizontalalign="left" header="true">
            <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" fontstyle="bold">
            </phrase>
          </cell>
          <cell verticalalign="middle" horizontalalign="center" header="true">
            <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}" fontstyle="bold">
              <xsl:value-of select="Translations:GetTranslation('OmzetVerklaring','Jaar','Jaar')"/>
            </phrase>
          </cell>
          <cell verticalalign="middle" horizontalalign="center" header="true">
            <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}" fontstyle="bold">
              <xsl:value-of select="Translations:GetTranslation('OmzetVerklaring','Bedrag','Bedrag')"/>
            </phrase>
          </cell>
        </row>
        <xsl:for-each select="OmzetVerklaringen/*[AutoKey != 'TV' and AutoKey != 'RV' and AutoLine!='true']">
          <row>
            <cell verticalalign="middle" horizontalalign="left">
              <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}">
                <xsl:value-of select="Omschrijving"/>
              </phrase>
            </cell>
            <cell verticalalign="middle" horizontalalign="center">
              <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
                <xsl:value-of select="Jaar/nl"/>
              </phrase>
            </cell>
            <cell verticalalign="middle" horizontalalign="right">
              <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
                <xsl:value-of select="format-number(Bedrag,$NumberFormatting,'euro')"/>€
              </phrase>
            </cell>
          </row>
        </xsl:for-each>
        <row>
          <cell>
            <phrase lineheight="{$WorksheetSpacing}"/>
          </cell>
          <cell verticalalign="middle" horizontalalign="center">
            <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}" fontstyle="bold">
              <xsl:value-of select="Translations:GetTranslation('WPBTW','Totaal','Totaal')"/>
            </phrase>
          </cell>
          <cell verticalalign="middle" horizontalalign="right">
            <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" fontstyle="bold">
              <xsl:variable name="TotalExpl" select="sum(OmzetVerklaringen/*[AutoKey != 'TV' and AutoKey != 'RV' and AutoLine!='true']/Bedrag)"/><xsl:value-of select="format-number($TotalExpl,$NumberFormatting,'euro')"/>€
            </phrase>
          </cell>
        </row>
        <row>
          <cell colspan="3">
            <phrase lineheight="{$WorksheetSpacing}"/>
          </cell>
        </row>
        <row>
          <cell colspan="3">
            <phrase lineheight="{$WorksheetSpacing}"/>
            <newline/>
          </cell>
        </row>
        <row>
          <cell colspan="2">
            <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}" fontstyle="bold">
              <xsl:value-of select="Translations:GetTranslation('WPBTW','ResterendVerschil','ResterendVerschil')"/>
            </phrase>
          </cell>
            <cell verticalalign="middle" horizontalalign="right" borderwidth="0" top="false" bordercolor="black">
              <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" fontstyle="bold">
                <xsl:variable name="TotalOmzetTvv" select="(sum(OmzetAangiftes/*/Totaal))-(sum(OmzettenGeboekt/*/Saldo))"/>
                <xsl:variable name="TotalOmzetVerklaringen" select="sum(OmzetVerklaringen/*[AutoLine = 'false']/Bedrag)"/>
                 <xsl:variable name="omzetTvv"><xsl:call-template name="abs">
		    <xsl:with-param name="n" select="$TotalOmzetTvv"/>
                  </xsl:call-template>
                  </xsl:variable>
                   <xsl:variable name="omzetVerkl"><xsl:call-template name="abs">
		      <xsl:with-param name="n" select="($TotalOmzetVerklaringen)"/>
                  </xsl:call-template>
                  </xsl:variable>
                <xsl:variable name="result">
                  <xsl:call-template name="abs">
                    <xsl:with-param name="n" select="($omzetTvv) - ($omzetVerkl)"/>
                  </xsl:call-template>
                </xsl:variable><xsl:value-of select="format-number($result,$NumberFormatting,'euro')"/>€
              </phrase>
            </cell>
        </row>
      </table>
    </xsl:if>
   
  </xsl:template>

  <xsl:template match="*" mode="OmzetAangiftes">
   
    <xsl:variable name="tarifsource">
      <root>
        <item id="Tarief_0"><xsl:value-of select="sum(OmzetAangiftes/*/Tarief_0[number()=number()])"/></item>
        <item id="Tarief_6"><xsl:value-of select="sum(OmzetAangiftes/*/Tarief_6[number()=number()])"/></item>
        <item id="Tarief_12"><xsl:value-of select="sum(OmzetAangiftes/*/Tarief_12[number()=number()])"/></item>
        <item id="Tarief_21"><xsl:value-of select="sum(OmzetAangiftes/*/Tarief_21[number()=number()])"/></item>
        <item id="Tarief_Medec"><xsl:value-of select="sum(OmzetAangiftes/*/Tarief_Medec[number()=number()])"/></item>
        <item id="Tarief_EU"><xsl:value-of select="sum(OmzetAangiftes/*/Tarief_EU[number()=number()])"/></item>
        <item id="Tarief_Uitvoer"><xsl:value-of select="sum(OmzetAangiftes/*/Tarief_Uitvoer[number()=number()])"/></item>
        <item id="Tarief_44"><xsl:value-of select="sum(OmzetAangiftes/*/Tarief_44[number()=number()])"/></item>
        <item id="Tarief_48"><xsl:value-of select="sum(OmzetAangiftes/*/Tarief_48[number()=number()])"/></item>
        <item id="Tarief_49"><xsl:value-of select="sum(OmzetAangiftes/*/Tarief_49[number()=number()])"/></item>
      </root>
    </xsl:variable>
    <xsl:variable name="tarifs" select="msxsl:node-set($tarifsource)"/>
    
      <xsl:variable name="filledColumns" select="count($tarifs//root/item[not(./text()='0')])+1"/>
      <xsl:variable name="DetailColumns" select="($filledColumns)*2"/>
      <xsl:variable name="Columns" select="($DetailColumns)+1"/>
      <xsl:variable name="ColumnSizes" select="concat('10;',Helper:GetColumnsSizesX(90,$filledColumns,-1,20))"/>
    <xsl:variable name="fontSize" select="Helper:GetFontSize($filledColumns)"/>
    <!--table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" columns="10" width="100%" align="Center" widths="50;50;5;50;5;50;5;50;5;50;" cellspacing="0" cellpadding="2"-->
    <table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" columns="{$Columns}" width="100%" align="Center" widths="{$ColumnSizes}" cellspacing="0" cellpadding="1">
      <row>
        <cell verticalalign="middle" colspan="{$Columns}" header="true" border="1">
          <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}" fontstyle="bold">
            <xsl:value-of select="Translations:GetTranslation('WPBTW','AansluitingOmzet','AansluitingOmzet')"/>
          </phrase>
        </cell>
      </row>
      <row>
        <cell verticalalign="middle" colspan="{$Columns}" header="true" border="1">
          <newline/>
          <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}" fontstyle="bold">
            <xsl:value-of select="Translations:GetTranslation('WPBTW','OmzetAangiftes','OmzetAangiftes')"/>
          </phrase>
        </cell>
      </row>
      <row>
        <cell verticalalign="middle" horizontalalign="center" header="true" border="1">
          <phrase lineheight="{$WorksheetSpacing}" size="{$fontSize}" fontstyle="bold">
            <xsl:value-of select="Translations:GetTranslation('OmzetAangifte','Y_M','Y_M')"/>
          </phrase>
        </cell>
        <xsl:for-each select="$tarifs//root/item[not(./text()='0')]">
          <cell></cell>
          <cell verticalalign="middle" horizontalalign="right" header="true">
            <phrase lineheight="{$WorksheetSpacing}" size="{$fontSize}" fontstyle="bold">
              <xsl:value-of select="Translations:GetTranslation('OmzetAangifte',@id,@id)"/>
            </phrase>
          </cell>
        </xsl:for-each>
        <cell></cell>
        <cell verticalalign="middle" horizontalalign="right" header="true">
          <phrase lineheight="{$WorksheetSpacing}" size="{$fontSize}" fontstyle="bold">
            <xsl:value-of select="Translations:GetTranslation('OmzetAangifte','Totaal','Totaal')"/>
          </phrase>
        </cell>
      </row>
      <xsl:for-each select="OmzetAangiftes/*">
        <xsl:sort select="Y_M" order="ascending"/>
        <row>
          <cell verticalalign="middle" horizontalalign="center">
            <phrase lineheight="{$WorksheetSpacing}" size="{$fontSize}">
              <xsl:value-of select="Y_M/nl"/>
            </phrase>
          </cell>
          <xsl:variable name="me" select="."/>
          <xsl:for-each select="$tarifs//root/item[not(./text()='0')]">
            <xsl:variable name="id" select="@id"/>
            <cell></cell>
            <cell verticalalign="middle" horizontalalign="right">
              <phrase lineheight="{$WorksheetSpacing}" size="{$fontSize}">
                <xsl:value-of select="format-number($me/*[name()=$id],$NumberFormatting,'euro')"/>€
              </phrase>
            </cell>
          </xsl:for-each>
          <cell></cell>
          <cell verticalalign="middle" horizontalalign="right">
            <phrase lineheight="{$WorksheetSpacing}" size="{$fontSize}">
              <xsl:value-of select="format-number(Totaal,$NumberFormatting,'euro')"/>€
            </phrase>
          </cell>
        </row>
      </xsl:for-each>
      <row>
        <cell colspan="{$Columns}">
          <phrase lineheight="0.5">&#160;</phrase>
        </cell>
      </row>
      <row>
        <cell>
          <phrase lineheight="{$WorksheetSpacing}"/>
        </cell>
        <xsl:for-each select="$tarifs//root/item[not(./text()='0')]">
          <cell></cell>
          <cell verticalalign="middle" horizontalalign="right" borderwidth="0.5" top="true" bordercolor="black">
            <phrase lineheight="{$WorksheetSpacing}" size="{$fontSize}" fontstyle="bold">
              <xsl:value-of select="format-number(.,$NumberFormatting,'euro')"/>€
            </phrase>
          </cell>
        </xsl:for-each>
        <cell></cell>
        <cell verticalalign="middle" horizontalalign="right" borderwidth="0.5" top="true" bordercolor="black">
          <phrase lineheight="{$WorksheetSpacing}" size="{$fontSize}" fontstyle="bold">
            <xsl:value-of select="format-number(sum(OmzetAangiftes/*/Totaal),$NumberFormatting,'euro')"/>€
          </phrase>
        </cell>
      </row>
    </table>
  </xsl:template>

  <xsl:template match="*" mode="OmzettenGeboekt">
    <xsl:param name="omzetverschil"/>
    
    <table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" columns="3" width="100%" align="Center" widths="60;15;15" cellspacing="0" cellpadding="1">
      <row>
        <cell verticalalign="middle" colspan="3" header="true">
          <newline/>
          <newline/>
          <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}" fontstyle="bold">
            <xsl:value-of select="Translations:GetTranslation('WPBTW','OmzettenGeboekt','OmzettenGeboekt')"/>
          </phrase>
        </cell>
      </row>
      <xsl:for-each select="OmzettenGeboekt/*">
        <row>
          <cell verticalalign="middle" horizontalalign="left" colspan="2">
            <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}">
              <xsl:value-of select="NaamRekening"/>
            </phrase>
          </cell>
          <cell verticalalign="middle" horizontalalign="right">
            <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
              <xsl:value-of select="format-number(Saldo,$NumberFormatting,'euro')"/>€
            </phrase>
          </cell>
        </row>
      </xsl:for-each>
      <row>
        <cell>
          <phrase lineheight="{$WorksheetSpacing}"/>
        </cell>
        <cell verticalalign="middle" horizontalalign="center">
          <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}" fontstyle="bold">
            <xsl:value-of select="Translations:GetTranslation('WPBTW','Totaal','Totaal')"/>
          </phrase>
        </cell>
        <cell verticalalign="middle" horizontalalign="right">
          <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" fontstyle="bold">
            <xsl:variable name="DummyToAddAmountBTW" select="sum(OmzettenGeboekt/*/Saldo)"/><xsl:value-of select="format-number($DummyToAddAmountBTW,$NumberFormatting,'euro')"/>€
          </phrase>
        </cell>
      </row>
      <row>
        <cell colspan="3">
          <phrase>&#160;</phrase>
          <newline/>
        </cell>
      </row>
      <row>
        <cell verticalalign="middle" colspan="2">
          <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}" fontstyle="bold">
            <xsl:value-of select="Translations:GetTranslation('','TeVerklarenVerschil','Te verklaren verschil')"/>
          </phrase>
        </cell>
        <cell verticalalign="middle" horizontalalign="right">
          <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" fontstyle="bold">
           <xsl:value-of select="format-number($omzetverschil,$NumberFormatting,'euro')"/>€
          </phrase>
        </cell>
      </row>
    </table>
  </xsl:template>

  <xsl:template match="WPBTW" mode="RC">
    <xsl:variable name="ags" select="count(RCAangiftes/*)"/>
    <xsl:variable name="gbs" select="count(RCenGeboekt/*)"/>

    <xsl:variable name="rcverschil" select="number(RCVerklaringen/*/Bedrag[../AutoKey = 'TV'])"/>

    <xsl:choose>
      <xsl:when test="$ags>0">
        <xsl:apply-templates select="." mode="RCAangiftes"/>
      </xsl:when>
      <xsl:when test="$gbs>0">
        <table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" columns="3" width="100%" align="Center" widths="60;15;15" cellspacing="0" cellpadding="1">
          <row>
            <cell verticalalign="middle" colspan="2" header="true">
              <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}" fontstyle="bold">
                <xsl:value-of select="Translations:GetTranslation('WPBTW','RCAangiftes','RCAangiftes')"/>
              </phrase>
            </cell>
            <cell verticalalign="middle" header="true" horizontalalign="right">
              <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" fontstyle="bold">
                <xsl:value-of select="format-number(0,$NumberFormatting,'euro')"/>€
              </phrase>
            </cell>
          </row>
        </table>
      </xsl:when>
    </xsl:choose>

    <xsl:choose>
      <xsl:when test="$gbs>0">
        <xsl:apply-templates select="." mode="RCenGeboekt">
          <xsl:with-param name="rcverschil" select="$rcverschil"/>
        </xsl:apply-templates>
      </xsl:when>
      <xsl:otherwise>
        <table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" columns="3" width="100%" align="Center" widths="60;15;15" cellspacing="0" cellpadding="1">
          <row>
            <cell verticalalign="middle" colspan="2" header="true">
              <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}" fontstyle="bold">
                <xsl:value-of select="Translations:GetTranslation('WPBTW','RCenGeboekt','RCenGeboekt')"/>
              </phrase>
            </cell>
            <cell verticalalign="middle" header="true" horizontalalign="right">
              <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" fontstyle="bold">
                <xsl:value-of select="format-number(0,$NumberFormatting,'euro')"/>€
              </phrase>
            </cell>
          </row>
          <row>
            <cell colspan="3">
              <phrase>&#160;</phrase>
              <newline/>
            </cell>
          </row>
          <row>
            <cell verticalalign="middle" colspan="2">
              <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}" fontstyle="bold">
                <xsl:value-of select="Translations:GetTranslation('','TeVerklarenVerschil','Te verklaren verschil')"/>
              </phrase>
            </cell>
            <cell verticalalign="middle" horizontalalign="right">
              <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" fontstyle="bold">
                <xsl:value-of select="format-number($rcverschil,$NumberFormatting,'euro')"/>€
              </phrase>
            </cell>
          </row>
        </table>
      </xsl:otherwise>
    </xsl:choose>

    <xsl:if test="$rcverschil>0">
      <table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" columns="3" width="100%" align="Center" widths="60;15;15" cellspacing="0" cellpadding="1">
        <row>
          <cell colspan="3">
            <phrase lineheight="{$WorksheetSpacing}"></phrase>
            <newline/>
            <phrase lineheight="{$WorksheetSpacing}"></phrase>
          </cell>
        </row>
        <row>
          <cell verticalalign="middle" colspan="3" header="true">
            <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}" fontstyle="bold">
              <xsl:value-of select="Translations:GetTranslation('','Verklaring','Verklaring')"/>
            </phrase>
          </cell>
        </row>
        <row>
          <cell verticalalign="middle" horizontalalign="left" header="true">
            <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" fontstyle="bold">
            </phrase>
          </cell>
          <cell verticalalign="middle" horizontalalign="center" header="true">
            <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}" fontstyle="bold">
              <xsl:value-of select="Translations:GetTranslation('RCVerklaring','Jaar','Jaar')"/>
            </phrase>
          </cell>
          <cell verticalalign="middle" horizontalalign="center" header="true">
            <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}" fontstyle="bold">
              <xsl:value-of select="Translations:GetTranslation('RCVerklaring','Bedrag','Bedrag')"/>
            </phrase>
          </cell>
        </row>
        <xsl:for-each select="RCVerklaringen/*[AutoLine != 'true']">
          <row>
            <cell verticalalign="middle" horizontalalign="left">
              <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}">
                <xsl:value-of select="Omschrijving"/>
              </phrase>
            </cell>
            <cell verticalalign="middle" horizontalalign="center">
              <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
                <xsl:value-of select="Jaar/nl"/>
              </phrase>
            </cell>
            <cell verticalalign="middle" horizontalalign="right">
              <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
                <xsl:value-of select="format-number(Bedrag,$NumberFormatting,'euro')"/>€
              </phrase>
            </cell>
          </row>
        </xsl:for-each>
        <row>
          <cell>
            <phrase lineheight="{$WorksheetSpacing}"/>
          </cell>
          <cell verticalalign="middle" horizontalalign="center">
            <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}" fontstyle="bold">
              <xsl:value-of select="Translations:GetTranslation('WPBTW','Totaal','Totaal')"/>
            </phrase>
          </cell>
          <cell verticalalign="middle" horizontalalign="right">
            <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" fontstyle="bold">
              <xsl:variable name="TotalExpl" select="sum(RCVerklaringen/*[AutoLine != 'true']/Bedrag)"/><xsl:value-of select="format-number($TotalExpl,$NumberFormatting,'euro')"/>€
            </phrase>
          </cell>
        </row>
        <row>
          <cell colspan="3">
            <phrase lineheight="{$WorksheetSpacing}"/>
          </cell>
        </row>
        <row>
          <cell colspan="3">
            <phrase lineheight="{$WorksheetSpacing}"/>
            <newline/>
          </cell>
        </row>
        <row>
          <cell colspan="2">
            <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}" fontstyle="bold">
              <xsl:value-of select="Translations:GetTranslation('WPBTW','ResterendVerschil','ResterendVerschil')"/>
            </phrase>
          </cell>
          <cell verticalalign="middle" horizontalalign="right" borderwidth="0" top="false" bordercolor="black">
            <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" fontstyle="bold">
              <xsl:value-of select="format-number(RCVerklaringen/*/Bedrag[../AutoKey = 'RV'],$NumberFormatting,'euro')"/>€
            </phrase>
          </cell>
        </row>
      </table>
    </xsl:if>

  </xsl:template>

  <xsl:template match="*" mode="RCAangiftes">
    <newline/>
    <xsl:variable name="endmonth" select="File:GetValue('EndDate','MM/yyyy')"/>
    <xsl:variable name="endmonth2" select="File:GetValue('EndDate','yyyy/MM')"/>
    <xsl:variable name="postsource">
      <root>
        <item id="Post54">
          <xsl:value-of select="sum(RCAangiftes/*/Post54[number()=number()])"/>
        </item>
        <item id="Post55">
          <xsl:value-of select="sum(RCAangiftes/*/Post55[number()=number()])"/>
        </item>
        <item id="Post56">
          <xsl:value-of select="sum(RCAangiftes/*/Post56[number()=number()])"/>
        </item>
        <item id="Post57">
          <xsl:value-of select="sum(RCAangiftes/*/Post57[number()=number()])"/>
        </item>
        <item id="Post59">
          <xsl:value-of select="sum(RCAangiftes/*/Post59[number()=number()])"/>
        </item>
        <item id="Post61">
          <xsl:value-of select="sum(RCAangiftes/*/Post61[number()=number()])"/>
        </item>
        <item id="Post62">
          <xsl:value-of select="sum(RCAangiftes/*/Post62[number()=number()])"/>
        </item>
        <item id="Post63">
          <xsl:value-of select="sum(RCAangiftes/*/Post63[number()=number()])"/>
        </item>
        <item id="Post64">
          <xsl:value-of select="sum(RCAangiftes/*/Post64[number()=number()])"/>
        </item>
      </root>
    </xsl:variable>
    <xsl:variable name="posts" select="msxsl:node-set($postsource)"/>

    <xsl:variable name="filledColumns" select="count($posts//root/item[not(./text()='0')])+1"/>
    <xsl:variable name="DetailColumns" select="($filledColumns)*2"/>
    <xsl:variable name="Columns" select="($DetailColumns)+1"/>
    <xsl:variable name="ColumnSizes" select="concat('10;',Helper:GetColumnsSizesX(90,$filledColumns,-1,20))"/>
    <xsl:variable name="fontSize" select="Helper:GetFontSize($filledColumns)"/>
    <!--table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" columns="10" width="100%" align="Center" widths="50;50;5;50;5;50;5;50;5;50;" cellspacing="0" cellpadding="2"-->
    <table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" columns="{$Columns}" width="100%" align="Center" widths="{$ColumnSizes}" cellspacing="0" cellpadding="1">
      <row>
        <cell verticalalign="middle" colspan="{$Columns}" header="true" border="1">
          <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}" fontstyle="bold">
            <xsl:value-of select="Translations:GetTranslation('WPBTW','RCAangiftes','RCAangiftes')"/>
          </phrase>
        </cell>
      </row>
      <row>
        <cell verticalalign="middle" horizontalalign="center" header="true" border="1">
          <phrase lineheight="{$WorksheetSpacing}" size="{$fontSize}" fontstyle="bold">
            <xsl:value-of select="Translations:GetTranslation('RCAangifte','Y_M','Y_M')"/>
          </phrase>
        </cell>
        <xsl:for-each select="$posts//root/item[not(./text()='0')]">
          <cell></cell>
          <cell verticalalign="middle" horizontalalign="right" header="true">
            <phrase lineheight="{$WorksheetSpacing}" size="{$fontSize}" fontstyle="bold">
              <xsl:value-of select="Translations:GetTranslation('RCAangifte',@id,@id)"/>
            </phrase>
          </cell>
        </xsl:for-each>
        <cell></cell>
        <cell verticalalign="middle" horizontalalign="right" header="true">
          <phrase lineheight="{$WorksheetSpacing}" size="{$fontSize}" fontstyle="bold">
            <xsl:value-of select="Translations:GetTranslation('RCAangifte','Totaal','Totaal')"/>
          </phrase>
        </cell>
      </row>
      <row>
        <cell colspan="{$Columns}">
          <phrase lineheight="1.5">&#160;</phrase>
        </cell>
      </row>
      <xsl:for-each select="RCAangiftes/*[Y_M/id=$endmonth or Y_M/id=$endmonth2]">
        <xsl:sort select="Y_M" order="ascending"/>
        <row>
          <cell verticalalign="middle" horizontalalign="center">
            <phrase lineheight="{$WorksheetSpacing}" size="{$fontSize}">
              <xsl:value-of select="Y_M/nl"/>
            </phrase>
          </cell>
          <xsl:variable name="me" select="."/>
          <xsl:for-each select="$posts//root/item[not(./text()='0')]">
            <xsl:variable name="id" select="@id"/>
            <cell></cell>
            <cell verticalalign="middle" horizontalalign="right">
              <phrase lineheight="{$WorksheetSpacing}" size="{$fontSize}">
                <xsl:value-of select="format-number($me/*[name()=$id],$NumberFormatting,'euro')"/>€
              </phrase>
            </cell>
          </xsl:for-each>
          <cell></cell>
          <cell verticalalign="middle" horizontalalign="right">
            <phrase lineheight="{$WorksheetSpacing}" size="{$fontSize}">
              <xsl:value-of select="format-number(Totaal,$NumberFormatting,'euro')"/>€
            </phrase>
          </cell>
        </row>
      </xsl:for-each>
      <!--row>
        <cell colspan="{$Columns}">
          <phrase lineheight="0.5">&#160;</phrase>
        </cell>
      </row>
      <row>
        <cell>
          <phrase lineheight="{$WorksheetSpacing}"/>
        </cell>
        <xsl:for-each select="$posts//root/item[not(./text()='0')]">
          <cell></cell>
          <cell verticalalign="middle" horizontalalign="right" borderwidth="0.5" top="true" bordercolor="black">
            <phrase lineheight="{$WorksheetSpacing}" size="{$fontSize}" fontstyle="bold">
              <xsl:value-of select="format-number(.,$NumberFormatting,'euro')"/>€
            </phrase>
          </cell>
        </xsl:for-each>
        <cell></cell>
        <cell verticalalign="middle" horizontalalign="right" borderwidth="0.5" top="true" bordercolor="black">
          <phrase lineheight="{$WorksheetSpacing}" size="{$fontSize}" fontstyle="bold">
            <xsl:value-of select="format-number(sum(RCAangiftes/*/Totaal),$NumberFormatting,'euro')"/>€
          </phrase>
        </cell>
      </row-->
    </table>
  </xsl:template>

  <xsl:template match="*" mode="RCenGeboekt">
    <xsl:param name="rcverschil"/>

    <table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" columns="3" width="100%" align="Center" widths="60;15;15" cellspacing="0" cellpadding="1">
      <row>
        <cell verticalalign="middle" colspan="3" header="true">
          <newline/>
          <newline/>
          <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}" fontstyle="bold">
            <xsl:value-of select="Translations:GetTranslation('WPBTW','RCenGeboekt','RCenGeboekt')"/>
          </phrase>
        </cell>
      </row>
      <xsl:for-each select="RCenGeboekt/*">
        <row>
          <cell verticalalign="middle" horizontalalign="left" colspan="2">
            <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}">
              <xsl:value-of select="NaamRekening"/>
            </phrase>
          </cell>
          <cell verticalalign="middle" horizontalalign="right">
            <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
              <xsl:value-of select="format-number(Saldo,$NumberFormatting,'euro')"/>€
            </phrase>
          </cell>
        </row>
      </xsl:for-each>
      <row>
        <cell>
          <phrase lineheight="{$WorksheetSpacing}"/>
        </cell>
        <cell verticalalign="middle" horizontalalign="center">
          <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}" fontstyle="bold">
            <xsl:value-of select="Translations:GetTranslation('WPBTW','Totaal','Totaal')"/>
          </phrase>
        </cell>
        <cell verticalalign="middle" horizontalalign="right">
          <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" fontstyle="bold">
            <xsl:variable name="DummyToAddAmountBTW" select="sum(RCenGeboekt/*/Saldo)"/><xsl:value-of select="format-number($DummyToAddAmountBTW,$NumberFormatting,'euro')"/>€
          </phrase>
        </cell>
      </row>
      <row>
        <cell colspan="3">
          <phrase>&#160;</phrase>
          <newline/>
        </cell>
      </row>
      <row>
        <cell verticalalign="middle" colspan="2">
          <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}" fontstyle="bold">
            <xsl:value-of select="Translations:GetTranslation('','TeVerklarenVerschil','Te verklaren verschil')"/>
          </phrase>
        </cell>
        <cell verticalalign="middle" horizontalalign="right">
          <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" fontstyle="bold">
            <xsl:value-of select="format-number($rcverschil,$NumberFormatting,'euro')"/>€
          </phrase>
        </cell>
      </row>
    </table>
  </xsl:template>
  
  <xsl:template match="WPBTW" mode="WORKSHEET">
    <xsl:variable name="omzets"><xsl:choose><xsl:when test="count(OmzetAangiftes/*) >0 or count(OmzettenGeboekt/*) >0">1</xsl:when><xsl:otherwise>0</xsl:otherwise></xsl:choose></xsl:variable>
    <xsl:variable name="rcs"><xsl:choose><xsl:when test="count(RCAangiftes/*) >0 or count(RCenGeboekt/*) >0">1</xsl:when><xsl:otherwise>0</xsl:otherwise></xsl:choose></xsl:variable>
    
    <xsl:if test="$omzets='1'">
      <xsl:apply-templates select="." mode="OMZET"/>
    </xsl:if>
    <xsl:if test="$rcs='1'">
      <xsl:if test="$omzets='1'"><newpage/></xsl:if>
      <xsl:apply-templates select="." mode="RC"/>
    </xsl:if>
  </xsl:template>
  

  <xsl:template name="abs">
    <xsl:param name="n" />
    <xsl:choose>
      <xsl:when test="$n &gt;= 0">
        <xsl:value-of select="$n" />
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="0 - $n" />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
</xsl:stylesheet>