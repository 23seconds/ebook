<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" exclude-result-prefixes="Helper Translations File Client" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:Helper="urn:Helper" xmlns:Translations="urn:Translations" xmlns:File="urn:File" xmlns:Client="urn:Client">
  <xsl:decimal-format name="NonNumber" NaN="0" zero-digit="0"/>
  <xsl:variable name="WorksheetLabelFontsize" select="string('10')"/>
  <xsl:variable name="WorksheetFontsize" select="string('10')"/>
  <xsl:variable name="WorksheetNumbersize" select="string('8')"/>
  <xsl:variable name="WorksheetSmallFontsize" select="string('8')"/>
  <xsl:variable name="WorksheetTotalsFontsize" select="string('10')"/>
  <xsl:variable name="WorksheetSpacing" select="1.1"/>
  <xsl:variable name="WorksheetTitleSpacing" select="1.5"/>
  <xsl:decimal-format name="euro" decimal-separator="," grouping-separator="."/>
  <xsl:variable name="NumberFormatting" select="string('#.##0,00')"/>
  <xsl:variable name="NumberFormattingNoDigit" select="string('#.##0')"/>
  <xsl:variable name="FONT" select="string('C:\WINDOWS\Fonts\EYInterstate-Light.ttf')"/>
  <xsl:variable name="smallcase" select="'abcdefghijklmnopqrstuvwxyz'" />
  <xsl:variable name="uppercase" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZ'" />
  <xsl:variable name="vrijgesteldId">9afe7536-2a45-4146-ad96-1f7839fda38c</xsl:variable>
  <xsl:variable name="belastId">defdbea0-0fde-48c2-9fe0-193e0cb7e42f</xsl:variable>
  <xsl:param name="RuleAppDef"/>
  <xsl:param name="Culture"/>
  <xsl:template match="DubieuzeDebiteuren">
    <Root orientation="landscape">
      <newline/>
      <xsl:variable name="vrijgesteld" select="count(ToestandEindHuidigBoekjaar/*[translate(BelastbaarOfVrijgesteld/id, $uppercase, $smallcase)='9afe7536-2a45-4146-ad96-1f7839fda38c'])&gt;0"/>
      <xsl:variable name="belast" select="count(ToestandEindHuidigBoekjaar/*[translate(BelastbaarOfVrijgesteld/id, $uppercase, $smallcase)='defdbea0-0fde-48c2-9fe0-193e0cb7e42f'])&gt;0"/>
      <xsl:if test="$vrijgesteld">
        <phrase lineheight="{$WorksheetSpacing}" font="{$FONT}" size="{$WorksheetLabelFontsize}" fontstyle="bold">
          <xsl:value-of select="Translations:GetTranslation('DubieuzeDebiteuren','TitelVrijgesteldeWV','TitelVrijgesteldeWV')"/>
        </phrase>
        <xsl:apply-templates select="." mode="RENDER">
          <xsl:with-param name="BelastbaarOfVrijgesteld" select="$vrijgesteldId"/>
        </xsl:apply-templates>
      </xsl:if>
      <xsl:if test="$vrijgesteld and $belast">
        <newpage/>
      </xsl:if>
      <xsl:if test="$belast">
        <phrase lineheight="{$WorksheetSpacing}" font="{$FONT}" size="{$WorksheetLabelFontsize}" fontstyle="bold">
          <xsl:value-of select="Translations:GetTranslation('DubieuzeDebiteuren','TitelBelasteWV','TitelBelasteWV')"/>
        </phrase>
        <xsl:apply-templates select="." mode="RENDER">
          <xsl:with-param name="BelastbaarOfVrijgesteld" select="$belastId"/>
        </xsl:apply-templates>
      </xsl:if>
    </Root>
  </xsl:template>
  <xsl:template match="DubieuzeDebiteuren" mode="RENDER">
    <xsl:param name="BelastbaarOfVrijgesteld"/>
    <xsl:variable name="EndDate" select="File:GetValue('EndDate','dd/MM/yyyy')"/>
    <xsl:variable name="StartDate" select="File:GetValue('StartDate','dd/MM/yyyy')"/>
    <newline/>
    <xsl:if test="count(ToestandEindHuidigBoekjaar/*[translate(BelastbaarOfVrijgesteld/id, $uppercase, $smallcase)=$BelastbaarOfVrijgesteld and IsNew/text()='false'])>0">
      <table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" columns="9" width="100%" align="Center" widths="20;10;10;10;10;10;10;10;10" cellspacing="0" borderwidth="0.5" left="false" right="false" top="false" bottom="false" bordercolor="black" cellpadding="2">
        <row>
          <cell verticalalign="middle" colspan="9" header="true" borderwidth="0.5" left="false" right="false" top="false" bottom="false" bordercolor="black">
            <phrase lineheight="{$WorksheetTitleSpacing}" size="{$WorksheetLabelFontsize}" fontstyle="bold">
              <xsl:value-of select="Translations:GetTranslation('DubieuzeDebiteuren','TitelVorigeBoekjaren','TitelVorigeBoekjaren')"/>
            </phrase>
          </cell>
        </row>
        <row>
          <cell verticalalign="middle" colspan="9" header="true" borderwidth="0.5" left="false" right="false" top="false" bottom="false" bordercolor="black">
            <phrase lineheight="{$WorksheetTitleSpacing}">&#160;</phrase>
          </cell>
        </row>
        <row>
          <cell verticalalign="middle" horizontalalign="left" header="true" borderwidth="0.5" left="false" right="false" bottom="false" bordercolor="black">
            <phrase lineheight="{$WorksheetTitleSpacing}" size="{$WorksheetFontsize}" fontstyle="underline">
              <xsl:value-of select="Translations:GetTranslation('DubieuzeDebiteuren','Klant','Klant')"/>
            </phrase>
          </cell>
          <cell verticalalign="middle" horizontalalign="right" header="true" borderwidth="0.5" right="false" bottom="false" bordercolor="black">
            <phrase lineheight="{$WorksheetTitleSpacing}" size="{$WorksheetFontsize}" fontstyle="underline">
              <xsl:value-of select="Translations:GetTranslation('DubieuzeDebiteuren','VorderingStart','VorderingStart')"/>
            </phrase>
          </cell>
          <cell verticalalign="middle" horizontalalign="right" header="true" borderwidth="0.5" right="false" bottom="false" bordercolor="black">
            <phrase lineheight="{$WorksheetTitleSpacing}" size="{$WorksheetFontsize}" fontstyle="underline">
              <xsl:value-of select="Translations:GetTranslation('DubieuzeDebiteuren','VorderingEinde','VorderingEinde')"/>
            </phrase>
          </cell>
          <cell verticalalign="middle" horizontalalign="right" header="true" borderwidth="0.5" right="false" bottom="false" bordercolor="black">
            <phrase lineheight="{$WorksheetTitleSpacing}" size="{$WorksheetFontsize}" fontstyle="underline">
              <xsl:value-of select="Translations:GetTranslation('DubieuzeDebiteuren','Waardevermindering','Waardevermindering')"/>
            </phrase>
            <phrase lineheight="{$WorksheetTitleSpacing}" size="{$WorksheetNumbersize}" fontstyle="underline">
              <xsl:value-of select="$StartDate"/>
            </phrase>
          </cell>
          <cell verticalalign="middle" horizontalalign="right" header="true" borderwidth="0.5" right="false" bottom="false" bordercolor="black">
            <phrase lineheight="{$WorksheetTitleSpacing}" size="{$WorksheetFontsize}" fontstyle="underline">
              <xsl:value-of select="Translations:GetTranslation('DubieuzeDebiteuren','VerminderingWVVerlies','VerminderingWVVerlies')"/>
            </phrase>
          </cell>
          <cell verticalalign="middle" horizontalalign="right" header="true" borderwidth="0.5" right="false" bottom="false" bordercolor="black">
            <phrase lineheight="{$WorksheetTitleSpacing}" size="{$WorksheetFontsize}" fontstyle="underline">
              <xsl:value-of select="Translations:GetTranslation('DubieuzeDebiteuren','VerminderingWVInning','VerminderingWVInning')"/>
            </phrase>
          </cell>
          <cell verticalalign="middle" horizontalalign="right" header="true" borderwidth="0.5" right="false" bottom="false" bordercolor="black">
            <phrase lineheight="{$WorksheetTitleSpacing}" size="{$WorksheetFontsize}" fontstyle="underline">
              <xsl:value-of select="Translations:GetTranslation('DubieuzeDebiteuren','VerminderingWVHerschat','VerminderingWVHerschat')"/>
            </phrase>
          </cell>
          <cell verticalalign="middle" horizontalalign="right" header="true" borderwidth="0.5" right="false" bottom="false" bordercolor="black">
            <phrase lineheight="{$WorksheetTitleSpacing}" size="{$WorksheetFontsize}" fontstyle="underline">
              <xsl:value-of select="Translations:GetTranslation('DubieuzeDebiteuren','VermeerderingWV','VermeerderingWV')"/>
            </phrase>
          </cell>
          <cell verticalalign="middle" horizontalalign="right" header="true" borderwidth="0.5" right="false" bottom="false" bordercolor="black">
            <phrase lineheight="{$WorksheetTitleSpacing}" size="{$WorksheetFontsize}" fontstyle="underline">
              <xsl:value-of select="Translations:GetTranslation('DubieuzeDebiteurW','Waardevermindering','Waardevermindering')"/>
            </phrase>
            <phrase lineheight="{$WorksheetTitleSpacing}" size="{$WorksheetNumbersize}" fontstyle="underline">
              <xsl:value-of select="$EndDate"/>
            </phrase>
          </cell>
        </row>
        <xsl:for-each select="ToestandEindHuidigBoekjaar/*[translate(BelastbaarOfVrijgesteld/id, $uppercase, $smallcase)=$BelastbaarOfVrijgesteld and IsNew/text()='false']">
          <row>
            <cell verticalalign="middle" horizontalalign="left" borderwidth="0.5" left="false" right="false" bordercolor="black">
              <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}">
                <xsl:choose>
                  <xsl:when test="AlgemeneProvisie/text()='true'">
                    <xsl:value-of select="Translations:GetTranslation('DubieuzeDebiteuren','AlgemeneProvisie','AlgemeneProvisie')"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="Schuldenaar/nl"/>
                  </xsl:otherwise>
                </xsl:choose>
              </phrase>
            </cell>
            
            <cell verticalalign="middle" horizontalalign="right" borderwidth="0.5" right="false" bordercolor="black">
              <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
                <xsl:value-of select="format-number(VorderingStart,$NumberFormatting,'euro')"/>€
              </phrase>
            </cell>
            <cell verticalalign="middle" horizontalalign="right" borderwidth="0.5" right="false" bordercolor="black">
              <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
                <xsl:value-of select="format-number(VorderingEinde,$NumberFormatting,'euro')"/>€
              </phrase>
            </cell>
            <cell verticalalign="middle" horizontalalign="right" borderwidth="0.5" right="false" bordercolor="black">
              <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
                <xsl:value-of select="format-number(WaardeverminderingStart,$NumberFormatting,'euro')"/>€
              </phrase>
            </cell>
            <cell verticalalign="middle" horizontalalign="right" borderwidth="0.5" right="false" bordercolor="black">
              <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
                <xsl:if test="VerminderingWVVerlies[number()=number()] and not(VerminderingWVVerlies='0')"><xsl:value-of select="format-number(VerminderingWVVerlies,$NumberFormatting,'euro')"/>€</xsl:if>
              </phrase>
            </cell>
            <cell verticalalign="middle" horizontalalign="right" borderwidth="0.5" right="false" bordercolor="black">
              <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
                <xsl:if test="VerminderingWVInning[number()=number()] and not(VerminderingWVInning='0')">
                  <xsl:value-of select="format-number(VerminderingWVInning,$NumberFormatting,'euro')"/>€
                </xsl:if>
              </phrase>
            </cell>
            <cell verticalalign="middle" horizontalalign="right" borderwidth="0.5" right="false" bordercolor="black">
              <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
                <xsl:if test="VerminderingWVHerschat[number()=number()] and not(VerminderingWVHerschat='0')">
                  <xsl:value-of select="format-number(VerminderingWVHerschat,$NumberFormatting,'euro')"/>€
                </xsl:if>
              </phrase>
            </cell>
            <cell verticalalign="middle" horizontalalign="right" borderwidth="0.5" right="false" bordercolor="black">
              <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
                <xsl:if test="VermeerderingWV[number()=number()] and not(VermeerderingWV='0')">
                  <xsl:value-of select="format-number(VermeerderingWV,$NumberFormatting,'euro')"/>€
                </xsl:if>
              </phrase>
            </cell>
            <cell verticalalign="middle" horizontalalign="right" borderwidth="0.5" right="false" bordercolor="black">
              <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
                <xsl:value-of select="format-number(WaardeverminderingEinde,$NumberFormatting,'euro')"/>€
              </phrase>
            </cell>
          </row>
        </xsl:for-each>
        <row>
          <cell verticalalign="middle" colspan="9" header="true" borderwidth="0.5" left="false" right="false" top="false" bottom="true" bordercolor="black">
            <phrase lineheight="{$WorksheetSpacing}">&#160;</phrase>
          </cell>
        </row>
        <row>
          <cell verticalalign="middle" horizontalalign="left" borderwidth="0.5" top="false" right="false" bordercolor="black">
            <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}">
              <xsl:value-of select="Translations:GetTranslation('DubieuzeDebiteuren','Totaal','Totaal')"/>
            </phrase>
          </cell>
          <cell verticalalign="middle" horizontalalign="right" borderwidth="0.5" top="false" right="false" bordercolor="black">
            <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
              <xsl:variable name="voordeelStartTot" select="sum(ToestandEindHuidigBoekjaar/*[translate(BelastbaarOfVrijgesteld/id, $uppercase, $smallcase)=$BelastbaarOfVrijgesteld and IsNew/text()='false']/VorderingStart)"/><xsl:value-of select="format-number($voordeelStartTot,$NumberFormatting,'euro')"/>€
            </phrase>
          </cell>
          <cell verticalalign="middle" horizontalalign="right" borderwidth="0.5" top="false" right="false" bordercolor="black">
            <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
              <xsl:variable name="voordeelEindeTot" select="sum(ToestandEindHuidigBoekjaar/*[translate(BelastbaarOfVrijgesteld/id, $uppercase, $smallcase)=$BelastbaarOfVrijgesteld and IsNew/text()='false']/VorderingEinde)"/><xsl:value-of select="format-number($voordeelEindeTot,$NumberFormatting,'euro')"/>€
            </phrase>
          </cell>
          <cell verticalalign="middle" horizontalalign="right" borderwidth="0.5" top="false" right="false" bordercolor="black">
            <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
              <xsl:value-of select="format-number(sum(ToestandEindHuidigBoekjaar/*[translate(BelastbaarOfVrijgesteld/id, $uppercase, $smallcase)=$BelastbaarOfVrijgesteld and IsNew/text()='false']/WaardeverminderingStart),$NumberFormatting,'euro')"/>€
            </phrase>
          </cell>
          <cell verticalalign="middle" horizontalalign="right" borderwidth="0.5" top="false" right="false" bordercolor="black">
            <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
              <xsl:value-of select="format-number(sum(ToestandEindHuidigBoekjaar/*[translate(BelastbaarOfVrijgesteld/id, $uppercase, $smallcase)=$BelastbaarOfVrijgesteld and IsNew/text()='false']/VerminderingWVVerlies[number()=number()]),$NumberFormatting,'euro')"/>€
            </phrase>
          </cell>
          <cell verticalalign="middle" horizontalalign="right" borderwidth="0.5" top="false" right="false" bordercolor="black">
            <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
              <xsl:value-of select="format-number(sum(ToestandEindHuidigBoekjaar/*[translate(BelastbaarOfVrijgesteld/id, $uppercase, $smallcase)=$BelastbaarOfVrijgesteld and IsNew/text()='false']/VerminderingWVInning[number()=number()]),$NumberFormatting,'euro')"/>€
            </phrase>
          </cell>
          <cell verticalalign="middle" horizontalalign="right" borderwidth="0.5" top="false" right="false" bordercolor="black">
            <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
              <xsl:value-of select="format-number(sum(ToestandEindHuidigBoekjaar/*[translate(BelastbaarOfVrijgesteld/id, $uppercase, $smallcase)=$BelastbaarOfVrijgesteld and IsNew/text()='false']/VerminderingWVHerschat[number()=number()]),$NumberFormatting,'euro')"/>€
            </phrase>
          </cell>
          <cell verticalalign="middle" horizontalalign="right" borderwidth="0.5" top="false" right="false" bordercolor="black">
            <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
              <xsl:value-of select="format-number(sum(ToestandEindHuidigBoekjaar/*[translate(BelastbaarOfVrijgesteld/id, $uppercase, $smallcase)=$BelastbaarOfVrijgesteld and IsNew/text()='false']/VermeerderingWV[number()=number()]),$NumberFormatting,'euro')"/>€
            </phrase>
          </cell>
          <cell verticalalign="middle" horizontalalign="right" borderwidth="0.5" top="false" right="false" bordercolor="black">
            <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" fontstyle="bold">
              <xsl:variable name="TotWV" select="sum(ToestandEindHuidigBoekjaar/*[translate(BelastbaarOfVrijgesteld/id, $uppercase, $smallcase)=$BelastbaarOfVrijgesteld and IsNew/text()='false']/WaardeverminderingEinde)"/><xsl:value-of select="format-number($TotWV,$NumberFormatting,'euro')"/>€
            </phrase>
          </cell>
        </row>
      </table>
    </xsl:if>
    <xsl:if test="count(NieuweTijdensBoekjaar/*[translate(BelastbaarOfVrijgesteld/id, $uppercase, $smallcase)=$BelastbaarOfVrijgesteld])&gt;0">
      <table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" columns="9" width="100%" align="Center" widths="20;10;10;10;10;10;10;10;10" cellspacing="0" borderwidth="0.5" left="false" right="false" top="false" bottom="false" bordercolor="black" cellpadding="2">
        <row>
          <cell verticalalign="middle" colspan="9" header="true" borderwidth="0.5" left="false" right="false" top="false" bottom="false" bordercolor="black">
            <phrase lineheight="{$WorksheetTitleSpacing}" size="{$WorksheetLabelFontsize}" fontstyle="bold">
              <xsl:value-of select="Translations:GetTranslation('DubieuzeDebiteuren','TitelHuidigBoekjaar','TitelHuidigBoekjaar')"/>
            </phrase>
          </cell>
        </row>
        <row>
          <cell verticalalign="middle" colspan="9" header="true" borderwidth="0.5" left="false" right="false" top="false" bottom="false" bordercolor="black">
            <phrase lineheight="{$WorksheetTitleSpacing}">&#160;</phrase>
          </cell>
        </row>
        <row>
          <cell verticalalign="middle" horizontalalign="left" header="true" borderwidth="0.5" left="false" right="false" bottom="false" bordercolor="black">
            <phrase lineheight="{$WorksheetTitleSpacing}" size="{$WorksheetFontsize}" fontstyle="underline">
              <xsl:value-of select="Translations:GetTranslation('DubieuzeDebiteuren','Klant','Klant')"/>
            </phrase>
          </cell>
          <cell verticalalign="middle" colspan="6" header="true" borderwidth="0.5" left="false" right="false" top="false" bottom="false" bordercolor="black">
            <phrase lineheight="{$WorksheetTitleSpacing}">&#160;</phrase>
          </cell>
          <cell verticalalign="middle" horizontalalign="right" header="true" borderwidth="0.5" right="false" bottom="false" bordercolor="black">
            <phrase lineheight="{$WorksheetTitleSpacing}" size="{$WorksheetFontsize}" fontstyle="underline">
              <xsl:value-of select="Translations:GetTranslation('DubieuzeDebiteurN','VorderingEinde','VorderingEinde')"/>
            </phrase>
          </cell>
          <cell verticalalign="middle" horizontalalign="right" header="true" borderwidth="0.5" right="false" bottom="false" bordercolor="black">
            <phrase lineheight="{$WorksheetTitleSpacing}" size="{$WorksheetFontsize}" fontstyle="underline">
              <xsl:value-of select="Translations:GetTranslation('DubieuzeDebiteurW','Waardevermindering','Waardevermindering')"/>
            </phrase>
            <phrase lineheight="{$WorksheetTitleSpacing}" size="{$WorksheetNumbersize}" fontstyle="underline">
              <xsl:value-of select="$EndDate"/>
            </phrase>
          </cell>
        </row>
        <xsl:for-each select="NieuweTijdensBoekjaar/*[translate(BelastbaarOfVrijgesteld/id, $uppercase, $smallcase)=$BelastbaarOfVrijgesteld]">
          <xsl:variable name="DDid" select="ID/text()"/>
          <row>
            <cell verticalalign="middle" horizontalalign="left" borderwidth="0.5" left="false" right="false" bordercolor="black">
              <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}">
                <xsl:choose>
                  <xsl:when test="AlgemeneProvisie/text()='true'">
                    <xsl:value-of select="Translations:GetTranslation('DubieuzeDebiteuren','AlgemeneProvisie','AlgemeneProvisie')"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="Schuldenaar/nl"/>
                  </xsl:otherwise>
                </xsl:choose>
              </phrase>
            </cell>
            <cell verticalalign="middle" colspan="6" header="true" borderwidth="0.5" left="false" right="false" top="false" bottom="false" bordercolor="black">
              <phrase lineheight="{$WorksheetSpacing}">&#160;</phrase>
            </cell>
            <cell verticalalign="middle" horizontalalign="right" borderwidth="0.5" right="false" bordercolor="black">
              <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
                <xsl:value-of select="format-number(VorderingEinde,$NumberFormatting,'euro')"/>€
              </phrase>
            </cell>
            <cell verticalalign="middle" horizontalalign="right" borderwidth="0.5" right="false" bordercolor="black">
              <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
                <xsl:value-of select="format-number(WaardeverminderingEinde,$NumberFormatting,'euro')"/>€
              </phrase>
            </cell>
          </row>
        </xsl:for-each>
        <row>
          <cell verticalalign="middle" colspan="7" header="true" borderwidth="0.5" left="false" right="false" top="false" bottom="true" bordercolor="black">
            <phrase lineheight="{$WorksheetSpacing}">&#160;</phrase>
          </cell>
          <cell verticalalign="middle" colspan="2" header="true" borderwidth="0.5" left="false" right="false" top="false" bottom="true" bordercolor="black">
            <phrase lineheight="{$WorksheetSpacing}">&#160;</phrase>
          </cell>
        </row>
        <row>
          <cell verticalalign="middle" horizontalalign="left" borderwidth="0.5" top="false" right="false" bordercolor="black">
            <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}">
              <xsl:value-of select="Translations:GetTranslation('DubieuzeDebiteuren','Totaal','Totaal')"/>
            </phrase>
          </cell>
          <cell verticalalign="middle" colspan="6" header="true" borderwidth="0.5" left="false" right="false" top="false" bottom="false" bordercolor="black">
            <phrase lineheight="{$WorksheetSpacing}">&#160;</phrase>
          </cell>
          <cell verticalalign="middle" horizontalalign="right" borderwidth="0.5" top="false" right="false" bordercolor="black">
            <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
              <xsl:variable name="factuur" select="sum(NieuweTijdensBoekjaar/*[translate(BelastbaarOfVrijgesteld/id, $uppercase, $smallcase)=$BelastbaarOfVrijgesteld]/VorderingEinde)"/><xsl:value-of select="format-number($factuur,$NumberFormatting,'euro')"/>€
            </phrase>
          </cell>
          <cell verticalalign="middle" horizontalalign="right" borderwidth="0.5" top="false" right="false" bordercolor="black">
            <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" fontstyle="bold">
              <xsl:value-of select="format-number(sum(NieuweTijdensBoekjaar/*[translate(BelastbaarOfVrijgesteld/id, $uppercase, $smallcase)=$BelastbaarOfVrijgesteld]/WaardeverminderingEinde),$NumberFormatting,'euro')"/>€
            </phrase>
          </cell>
        </row>
      </table>
    </xsl:if>
  </xsl:template>
</xsl:stylesheet>