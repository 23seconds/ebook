<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" exclude-result-prefixes="Helper Translations File Client" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:Helper="urn:Helper" xmlns:Translations="urn:Translations" xmlns:File="urn:File" xmlns:Client="urn:Client">
  <xsl:decimal-format name="NonNumber" NaN="0" zero-digit="0"/>
  <xsl:variable name="WorksheetLabelFontsize" select="string('10')"/>
  <xsl:variable name="WorksheetFontsize" select="string('10')"/>
  <xsl:variable name="WorksheetNumbersize" select="string('8')"/>
  <xsl:variable name="WorksheetSmallFontsize" select="string('8')"/>
  <xsl:variable name="WorksheetTotalsFontsize" select="string('10')"/>
  <xsl:variable name="WorksheetSpacing" select="1.1"/>
  <xsl:decimal-format name="euro" decimal-separator="," grouping-separator="."/>
  <xsl:variable name="NumberFormatting" select="string('#.##0,00')"/>
  <xsl:variable name="NumberFormattingNoDigit" select="string('#.##0')"/>
  <xsl:variable name="FONT" select="string('C:\WINDOWS\Fonts\EYInterstate-Light.ttf')"/>
  <xsl:param name="RuleAppDef"/>
  <xsl:param name="Culture"/>
  <xsl:template match="Voordelen">
    <Root orientation="landscape">
      <xsl:apply-templates select="." mode="WORKSHEET"/>
    </Root>
  </xsl:template>
  <xsl:template match="Voordelen" mode="WORKSHEET">
    
    <xsl:for-each select="*">
      <xsl:variable name="EntityName" select="name()"/>
      <xsl:variable name="EntityTitle">
        <xsl:value-of select="Translations:GetTranslation('Voordelen',$EntityName,$EntityName)"/>
      </xsl:variable>
      <xsl:if test="count(*)>0">
        <xsl:if test="position()&gt;1">
          <newline/>
          <phrase>&#160;</phrase>
          <newline/>
        </xsl:if>
        <xsl:if test="$EntityName = 'VoordelenVanAlleAardAuto'">
          <table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" columns="7" width="100%" align="Center" widths="20;20;16;8;12;12;12" cellspacing="0">
            <row>
              <cell verticalalign="top" header="true" colspan="7">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}" fontstyle="bold">
                  <xsl:value-of select="Translations:GetTranslation('Voordelen',$EntityName,$EntityName)"/>
                </phrase>
                <newline/>
              </cell>
            </row>
            <row>
              <cell verticalalign="top" horizontalalign="left" header="true">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}" fontstyle="bold">
                  <xsl:value-of select="Translations:GetTranslation($EntityName,'Tekst','Tekst')"/>
                </phrase>
              </cell>
              <cell verticalalign="top" horizontalalign="left" header="true">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}" fontstyle="bold">
                  <xsl:value-of select="Translations:GetTranslation(EntityName,'Soort','Soort')"/>
                </phrase>
              </cell>
              <cell verticalalign="top" horizontalalign="center" header="true">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}" fontstyle="bold">
                  <xsl:value-of select="Translations:GetTranslation(EntityName,'Periode','Periode')"/>
                </phrase>
                <newline/>
              </cell>
              <cell verticalalign="top" horizontalalign="right" header="true">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}" fontstyle="bold">
                  <xsl:value-of select="Translations:GetTranslation(EntityName,'AantalPerJaar','AantalPerJaar')"/>
                </phrase>
              </cell>
              <cell verticalalign="top" horizontalalign="right" header="true">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}" fontstyle="bold">
                  <xsl:value-of select="Translations:GetTranslation(EntityName,'MvH','MvH')"/>
                </phrase>
                <newline/>
              </cell>
              <cell verticalalign="top" horizontalalign="right" header="true">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}" fontstyle="bold">
                  <xsl:value-of select="Translations:GetTranslation(EntityName,'BTW','BTW')"/>
                </phrase>
              </cell>
              <cell verticalalign="top" horizontalalign="right" header="true">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}" fontstyle="bold">
                  <xsl:value-of select="Translations:GetTranslation(EntityName,'Totaal','Totaal')"/>
                </phrase>
              </cell>
            </row>
            <xsl:for-each select="*">
              <row>
                <cell verticalalign="top" horizontalalign="left">
                  <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}">
                    <xsl:value-of select="Tekst"/>
                  </phrase>
                </cell>
                <cell verticalalign="top" horizontalalign="left">
                  <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}">
                    <xsl:choose>
                      <xsl:when test="$Culture='nl-BE'">
                        <xsl:value-of select="Soort/nl"/>
                      </xsl:when>
                      <xsl:when test="$Culture='fr-FR'">
                        <xsl:value-of select="Soort/fr"/>
                      </xsl:when>
                      <xsl:when test="$Culture='en-US'">
                        <xsl:value-of select="Soort/en"/>
                      </xsl:when>
                      <xsl:otherwise>
                        <xsl:value-of select="Soort/nl"/>
                      </xsl:otherwise>
                    </xsl:choose>
                  </phrase>
                </cell>
                <cell verticalalign="top" horizontalalign="center">
                  <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
                    <xsl:value-of select="Helper:ConvertDate(string(StartDate))"/>-<xsl:value-of select="Helper:ConvertDate(string('EndDate'))"/>
                  </phrase>
                </cell>
                <cell verticalalign="top" horizontalalign="right">
                  <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
                    <xsl:value-of select="format-number(AantalPerJaar, $NumberFormattingNoDigit, 'euro')"/>
                  </phrase>
                </cell>
                <cell verticalalign="top" horizontalalign="right">
                  <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
                    <xsl:value-of select="format-number(MvH,$NumberFormatting,'euro')"/>€
                  </phrase>
                </cell>
                <cell verticalalign="top" horizontalalign="right">
                  <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
                    <xsl:value-of select="format-number(BTW[number()=number()],$NumberFormatting,'euro')"/>€
                  </phrase>
                </cell>
                <cell verticalalign="top" horizontalalign="right">
                  <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
                    <xsl:value-of select="format-number(Totaal,$NumberFormatting,'euro')"/>€
                  </phrase>
                </cell>
              </row>
            </xsl:for-each>
            <row>
              <cell verticalalign="top" horizontalalign="left" colspan="4">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}" fontstyle="bold">
                  <xsl:value-of select="Translations:GetTranslation($EntityName,'Totaal','Totaal')"/>
                </phrase>
              </cell>
              <cell verticalalign="top" horizontalalign="right">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" fontstyle="bold">
                  <xsl:value-of select="format-number(sum(*/MvH),$NumberFormatting,'euro')"/>€
                </phrase>
              </cell>
              <cell verticalalign="top" horizontalalign="right">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" fontstyle="bold">
                  <xsl:value-of select="format-number(sum(*/BTW),$NumberFormatting,'euro')"/>€
                </phrase>
              </cell>
              <cell verticalalign="top" horizontalalign="right">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" fontstyle="bold">
                  <xsl:value-of select="format-number(sum(*/Totaal),$NumberFormatting,'euro')"/>€
                </phrase>
              </cell>
            </row>
          </table>
        </xsl:if>
        <xsl:if test="$EntityName = 'VoordelenVanAlleAardAutoCO2'">
          <table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" columns="8" width="100%" align="Center" widths="20;12;8;12;12;12;12;12" cellspacing="0">
            <row>
              <cell verticalalign="top" header="true" colspan="8">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}" fontstyle="bold">
                  <xsl:value-of select="Translations:GetTranslation('Voordelen',$EntityName,$EntityName)"/>
                </phrase>
                <newline/>
              </cell>
            </row>
            <row>
              <cell verticalalign="top" horizontalalign="left" header="true">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}" fontstyle="bold">
                  <xsl:value-of select="Translations:GetTranslation($EntityName,'Tekst','Tekst')"/>
                </phrase>
              </cell>
              <cell verticalalign="top" horizontalalign="left" header="true">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}" fontstyle="bold">
                  <xsl:value-of select="Translations:GetTranslation($EntityName,'Motortype','Motortype')"/>
                </phrase>
              </cell>
              <cell verticalalign="top" horizontalalign="right" header="true">
	      <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}" fontstyle="bold">
		<xsl:value-of select="Translations:GetTranslation($EntityName,'CO2','CO2')"/>
	      </phrase>
              </cell>
              <cell verticalalign="top" horizontalalign="right" header="true">
	      <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}" fontstyle="bold">
		<xsl:value-of select="Translations:GetTranslation($EntityName,'AantalPerJaar','AantalPerJaar')"/>
	      </phrase>
              </cell>
              <cell verticalalign="top" horizontalalign="center" header="true">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}" fontstyle="bold">
                  <xsl:value-of select="Translations:GetTranslation($EntityName,'Periode','Periode')"/>
                </phrase>
                <newline/>
              </cell>
              <cell verticalalign="top" horizontalalign="right" header="true">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}" fontstyle="bold">
                  <xsl:value-of select="Translations:GetTranslation($EntityName,'MvH','MvH')"/>
                </phrase>
                <newline/>
              </cell>
              <cell verticalalign="top" horizontalalign="right" header="true">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}" fontstyle="bold">
                  <xsl:value-of select="Translations:GetTranslation($EntityName,'BTW','BTW')"/>
                </phrase>
              </cell>
              <cell verticalalign="top" horizontalalign="right" header="true">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}" fontstyle="bold">
                  <xsl:value-of select="Translations:GetTranslation($EntityName,'Totaal','Totaal')"/>
                </phrase>
              </cell>
            </row>
            
            <xsl:for-each select="*">
              <row>
                <cell verticalalign="top" horizontalalign="left">
                  <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}">
                    <xsl:value-of select="Tekst"/>
                  </phrase>
                </cell>
                <cell verticalalign="top" horizontalalign="left">
                  <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}">
                    <xsl:if test="Motortype">
                      <xsl:choose>
                        <xsl:when test="$Culture='nl-BE'">
                          <xsl:value-of select="Motortype/nl"/>
                        </xsl:when>
                        <xsl:when test="$Culture='fr-FR'">
                          <xsl:value-of select="Motortype/fr"/>
                        </xsl:when>
                        <xsl:when test="$Culture='en-US'">
                          <xsl:value-of select="Motortype/en"/>
                        </xsl:when>
                        <xsl:otherwise>
                          <xsl:value-of select="Motortype/nl"/>
                        </xsl:otherwise>
                      </xsl:choose>
                    </xsl:if>
                    <xsl:if test="Elektrische/text()='true'">
                      <xsl:value-of select="Translations:GetTranslation($EntityName,'Elektrische','Elektrische')"/>
                    </xsl:if>
                    <xsl:if test="Hybride/text()='true'">
                      &#160;<xsl:value-of select="Translations:GetTranslation($EntityName,'Hybride','Hybride')"/>
                    </xsl:if>
                  </phrase>
                </cell>
               <cell verticalalign="top" horizontalalign="right">
		 <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
		   <!--xsl:value-of select="format-number(AantalPerJaar/nl, $NumberFormattingNoDigit, 'euro')"/-->
		   <xsl:value-of select="CO2"/>
		 </phrase>
                </cell>
                <cell verticalalign="top" horizontalalign="right">
                  <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
                    <!--xsl:value-of select="format-number(AantalPerJaar/nl, $NumberFormattingNoDigit, 'euro')"/-->
                    <xsl:value-of select="AantalPerJaar/nl"/>
                  </phrase>
                </cell>
                 <cell verticalalign="top" horizontalalign="center">
		  <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
		    <xsl:value-of select="Helper:ConvertDate(string(StartDate))"/>-<xsl:value-of select="Helper:ConvertDate(string(EndDate))"/>
		  </phrase>
                </cell>
                <cell verticalalign="top" horizontalalign="right">
                  <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
                    <xsl:value-of select="format-number(MvH,$NumberFormatting,'euro')"/>€
                  </phrase>
                </cell>
                <cell verticalalign="top" horizontalalign="right">
                  <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
                    <xsl:if test="BTW[number()=number()]"><xsl:value-of select="format-number(BTW[number()=number()],$NumberFormatting,'euro')"/></xsl:if><xsl:if test="not(BTW[number()=number()])"><xsl:value-of select="format-number(0,$NumberFormatting,'euro')"/></xsl:if>€
                  </phrase>
                </cell>
                <cell verticalalign="top" horizontalalign="right">
                  <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
                    <xsl:value-of select="format-number(Totaal,$NumberFormatting,'euro')"/>€
                  </phrase>
                </cell>
              </row>
            </xsl:for-each>
            <row>
              <cell verticalalign="top" horizontalalign="left" colspan="5">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}" fontstyle="bold">
                  <xsl:value-of select="Translations:GetTranslation($EntityName,'Totaal','Totaal')"/>
                </phrase>
              </cell>
              <cell verticalalign="top" horizontalalign="right">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" fontstyle="bold">
                  <xsl:value-of select="format-number(sum(*/MvH),$NumberFormatting,'euro')"/>€
                </phrase>
              </cell>
              <cell verticalalign="top" horizontalalign="right">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" fontstyle="bold">
                  <xsl:value-of select="format-number(sum(*/BTW[number()=number()]),$NumberFormatting,'euro')"/>€
                </phrase>
              </cell>
              <cell verticalalign="top" horizontalalign="right">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" fontstyle="bold">
                  <xsl:value-of select="format-number(sum(*/Totaal),$NumberFormatting,'euro')"/>€
                </phrase>
              </cell>
            </row>
          </table>
        </xsl:if>
        <xsl:if test="$EntityName = 'VoordelenVanAlleAardAutoRupo'">
	          <table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" columns="8" width="100%" align="Center" widths="20;12;8;12;12;12;12;12" cellspacing="0">
	            <row>
	              <cell verticalalign="top" header="true" colspan="8">
	                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}" fontstyle="bold">
	                  <xsl:value-of select="Translations:GetTranslation('Voordelen',$EntityName,$EntityName)"/>
	                </phrase>
	                <newline/>
	              </cell>
	            </row>
	            <row>
	              <cell verticalalign="top" horizontalalign="left" header="true">
	                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}" fontstyle="bold">
	                  <xsl:value-of select="Translations:GetTranslation($EntityName,'MerkModel','MerkModel')"/>
	                </phrase>
	              </cell>
	              <cell verticalalign="top" horizontalalign="left" header="true">
	                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}" fontstyle="bold">
	                  <xsl:value-of select="Translations:GetTranslation($EntityName,'Motortype','Motortype')"/>
	                </phrase>
	              </cell>
	              
	              <cell verticalalign="top" horizontalalign="right" header="true">
			<phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}" fontstyle="bold">
			  <xsl:value-of select="Translations:GetTranslation($EntityName,'CO2','CO2')"/>
			</phrase>
	              </cell>
	              
	              <cell verticalalign="top" horizontalalign="right" header="true">
	                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}" fontstyle="bold">
	                  <xsl:value-of select="Translations:GetTranslation($EntityName,'CatalogusWaarde','CatalogusWaarde')"/>
	                </phrase>
	              </cell>
	              <cell verticalalign="top" horizontalalign="center" header="true">
			<phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}" fontstyle="bold">
			  <xsl:value-of select="Translations:GetTranslation($EntityName,'Periode','Periode')"/>
			</phrase>
			<newline/>
	              </cell>
	              <cell verticalalign="top" horizontalalign="right" header="true">
	                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}" fontstyle="bold">
	                  <xsl:value-of select="Translations:GetTranslation($EntityName,'MvH','MvH')"/>
	                </phrase>
	                <newline/>
	              </cell>
	              <cell verticalalign="top" horizontalalign="right" header="true">
	                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}" fontstyle="bold">
	                  <xsl:value-of select="Translations:GetTranslation($EntityName,'BTW','BTW')"/>
	                </phrase>
	              </cell>
	              <cell verticalalign="top" horizontalalign="right" header="true">
	                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}" fontstyle="bold">
	                  <xsl:value-of select="Translations:GetTranslation($EntityName,'Totaal','Totaal')"/>
	                </phrase>
	              </cell>
	            </row>
	            
	            <xsl:for-each select="*">
	              <row>
	                <cell verticalalign="top" horizontalalign="left">
	                  <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}">
	                    <xsl:value-of select="MerkModel"/>
	                  </phrase>
	                </cell>
	                <cell verticalalign="top" horizontalalign="left">
	                  <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}">
	                    <xsl:if test="Motortype">
	                      <xsl:choose>
	                        <xsl:when test="$Culture='nl-BE'">
	                          <xsl:value-of select="Motortype/nl"/>
	                        </xsl:when>
	                        <xsl:when test="$Culture='fr-FR'">
	                          <xsl:value-of select="Motortype/fr"/>
	                        </xsl:when>
	                        <xsl:when test="$Culture='en-US'">
	                          <xsl:value-of select="Motortype/en"/>
	                        </xsl:when>
	                        <xsl:otherwise>
	                          <xsl:value-of select="Motortype/nl"/>
	                        </xsl:otherwise>
	                      </xsl:choose>
	                    </xsl:if>
	                  </phrase>
	                </cell>
	                <cell verticalalign="top" horizontalalign="right">
				  <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
				    <xsl:value-of select="CO2"/>
				  </phrase>
	                </cell>
	                <cell verticalalign="top" horizontalalign="right">
			  <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
			    <xsl:value-of select="format-number(CatalogusWaarde, $NumberFormattingNoDigit, 'euro')"/>
			  </phrase>
	                </cell>
	                <cell verticalalign="top" horizontalalign="center">
	                  <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
	                    <xsl:value-of select="Helper:ConvertDate(string(StartDate))"/>-<xsl:value-of select="Helper:ConvertDate(string(EndDate))"/>
	                  </phrase>
	                </cell>
	                
	                <cell verticalalign="top" horizontalalign="right">
	                  <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
	                    <xsl:value-of select="format-number(MvH,$NumberFormatting,'euro')"/>€
	                  </phrase>
	                </cell>
	                <cell verticalalign="top" horizontalalign="right">
	                  <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
	                    <xsl:choose><xsl:when test="BTW[number()=number()]"><xsl:value-of select="format-number(BTW[number()=number()],$NumberFormatting,'euro')"/></xsl:when><xsl:otherwise><xsl:value-of select="format-number(0,$NumberFormatting,'euro')"/></xsl:otherwise></xsl:choose>€
	                  </phrase>
	                </cell>
	                <cell verticalalign="top" horizontalalign="right">
	                  <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
	                    <xsl:value-of select="format-number(Totaal,$NumberFormatting,'euro')"/>€
	                  </phrase>
	                </cell>
	              </row>
	            </xsl:for-each>
	            <row>
	              <cell verticalalign="top" horizontalalign="left" colspan="5">
	                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}" fontstyle="bold">
	                  <xsl:value-of select="Translations:GetTranslation($EntityName,'Totaal','Totaal')"/>
	                </phrase>
	              </cell>
	              <cell verticalalign="top" horizontalalign="right">
	                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" fontstyle="bold">
	                  <xsl:value-of select="format-number(sum(*/MvH),$NumberFormatting,'euro')"/>€
	                </phrase>
	              </cell>
	              <cell verticalalign="top" horizontalalign="right">
	                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" fontstyle="bold">
	                  <xsl:value-of select="format-number(sum(*/BTW[number()=number()]),$NumberFormatting,'euro')"/>€
	                </phrase>
	              </cell>
	              <cell verticalalign="top" horizontalalign="right">
	                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" fontstyle="bold">
	                  <xsl:value-of select="format-number(sum(*/Totaal),$NumberFormatting,'euro')"/>€
	                </phrase>
	              </cell>
	            </row>
	          </table>
        </xsl:if>
        
        <xsl:if test="$EntityName = 'VoordelenVanAlleAardNuts' or $EntityName = 'VoordelenVanAlleAardPC' or $EntityName = 'VoordelenVanAlleAardTelefonie'">
          <table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" columns="7" width="100%" align="Center" widths="20;20;16;8;12;12;12" cellspacing="0">
            <row>
              <cell verticalalign="top" header="true" colspan="7">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}" fontstyle="bold">
                  <xsl:value-of select="Translations:GetTranslation('Voordelen',$EntityName,$EntityName)"/>
                </phrase>
                <newline/>
              </cell>
            </row>
            <row>
              <cell verticalalign="top" horizontalalign="left" header="true">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}" fontstyle="bold">
                  <xsl:value-of select="Translations:GetTranslation($EntityName,'Tekst','Tekst')"/>
                </phrase>
              </cell>
              <cell verticalalign="top" horizontalalign="left" header="true">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}" fontstyle="bold">
                  <xsl:value-of select="Translations:GetTranslation($EntityName,'Soort','Soort')"/>
                </phrase>
              </cell>
              <cell verticalalign="top" horizontalalign="center" header="true">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}" fontstyle="bold">
                  <xsl:value-of select="Translations:GetTranslation($EntityName,'Periode','Periode')"/>
                </phrase>
              </cell>
              <cell verticalalign="top" horizontalalign="right" header="true">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}" fontstyle="bold">
                  <xsl:if test="not($EntityName = 'VoordelenVanAlleAardTelefonie')">
                    <xsl:value-of select="Translations:GetTranslation($EntityName,'AantalPerJaar','AantalPerJaar')"/>
                  </xsl:if>
                </phrase>
              </cell>
              <cell verticalalign="top" horizontalalign="right" header="true">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}" fontstyle="bold">
                  <xsl:value-of select="Translations:GetTranslation($EntityName,'MvH','MvH')"/>
                </phrase>
                <newline/>
              </cell>
              <cell verticalalign="top" horizontalalign="right" header="true">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}" fontstyle="bold">
                  <xsl:value-of select="Translations:GetTranslation($EntityName,'BTW','BTW')"/>
                </phrase>
              </cell>
              <cell verticalalign="top" horizontalalign="right" header="true">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}" fontstyle="bold">
                  <xsl:value-of select="Translations:GetTranslation($EntityName,'Totaal','Totaal')"/>
                </phrase>
              </cell>
            </row>
            <xsl:for-each select="*">
              <row>
                <cell verticalalign="top" horizontalalign="left">
                  <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}">
                    <xsl:value-of select="Tekst"/>
                  </phrase>
                </cell>
                <cell verticalalign="top" horizontalalign="left">
                  <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}">
                    <xsl:choose>
                      <xsl:when test="$Culture='nl-BE'">
                        <xsl:value-of select="Soort/nl"/>
                      </xsl:when>
                      <xsl:when test="$Culture='fr-FR'">
                        <xsl:value-of select="Soort/fr"/>
                      </xsl:when>
                      <xsl:when test="$Culture='en-US'">
                        <xsl:value-of select="Soort/en"/>
                      </xsl:when>
                      <xsl:otherwise>
                        <xsl:value-of select="Soort/nl"/>
                      </xsl:otherwise>
                    </xsl:choose>
                  </phrase>
                </cell>
                <cell verticalalign="top" horizontalalign="center">
                  <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
                    <xsl:value-of select="Helper:ConvertDate(string(StartDate))"/>-<xsl:value-of select="Helper:ConvertDate(string(EndDate))"/>
                  </phrase>
                </cell>
                <cell verticalalign="top" horizontalalign="right">
                  <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
                    <xsl:if test="not($EntityName = 'VoordelenVanAlleAardTelefonie') and not(WettelijkForfait/text()='false')">
                      <xsl:value-of select="format-number(AantalPerJaar, $NumberFormattingNoDigit, 'euro')"/>
                    </xsl:if>
                    <xsl:if test="WettelijkForfait/text()='false'">
                      <xsl:value-of select="Translations:GetTranslation($EntityName,'WettelijkForfait','WettelijkForfait')"/>
                    </xsl:if>
                  </phrase>
                </cell>
                <cell verticalalign="top" horizontalalign="right">
                  <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
                    <xsl:value-of select="format-number(MvH,$NumberFormatting,'euro')"/>€
                  </phrase>
                </cell>
                <cell verticalalign="top" horizontalalign="right">
                  <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
                    <xsl:value-of select="format-number(BTW[number()=number()],$NumberFormatting,'euro')"/>€
                  </phrase>
                </cell>
                <cell verticalalign="top" horizontalalign="right">
                  <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
                    <xsl:value-of select="format-number(Totaal,$NumberFormatting,'euro')"/>€
                  </phrase>
                </cell>
              </row>
            </xsl:for-each>
            <row>
              <cell verticalalign="top" horizontalalign="left" colspan="4">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}" fontstyle="bold">
                  <xsl:value-of select="Translations:GetTranslation($EntityName,'Totaal','Totaal')"/>
                </phrase>
              </cell>
              <cell verticalalign="top" horizontalalign="right">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" fontstyle="bold">
                  <xsl:value-of select="format-number(sum(*/MvH),$NumberFormatting,'euro')"/>€
                </phrase>
              </cell>
              <cell verticalalign="top" horizontalalign="right">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" fontstyle="bold">
                  <xsl:value-of select="format-number(sum(*/BTW),$NumberFormatting,'euro')"/>€
                </phrase>
              </cell>
              <cell verticalalign="top" horizontalalign="right">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" fontstyle="bold">
                  <xsl:value-of select="format-number(sum(*/Totaal),$NumberFormatting,'euro')"/>€
                </phrase>
              </cell>
            </row>
          </table>
        </xsl:if>
        <xsl:if test="$EntityName = 'VoordelenVanAlleAardHuur'">
          <table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" columns="7" width="100%" align="Center" widths="20;20;16;8;12;12;12" cellspacing="0">
            <row>
              <cell verticalalign="top" header="true" colspan="7">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}" fontstyle="bold">
                  <xsl:value-of select="$EntityTitle"/>
                </phrase>
                <newline/>
              </cell>
            </row>
            <row>
              <cell verticalalign="top" horizontalalign="left" header="true">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}" fontstyle="bold">
                  <xsl:value-of select="Translations:GetTranslation($EntityName,'Tekst','Tekst')"/>
                </phrase>
              </cell>
              <cell verticalalign="top" horizontalalign="left" header="true">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}" fontstyle="bold">
                  <xsl:value-of select="Translations:GetTranslation($EntityName,'Soort','Soort')"/>
                </phrase>
              </cell>
              <cell verticalalign="top" horizontalalign="center" header="true">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}" fontstyle="bold">
                  <xsl:value-of select="Translations:GetTranslation($EntityName,'Periode','Periode')"/>
                </phrase>
              </cell>
              <cell>
                <phrase>&#160;</phrase>
              </cell>
              <cell verticalalign="top" horizontalalign="right" header="true">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}" fontstyle="bold">
                  <xsl:value-of select="Translations:GetTranslation($EntityName,'KI','KI')"/>
                </phrase>
              </cell>
              <cell verticalalign="top" horizontalalign="right" header="true">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}" fontstyle="bold">
                  <xsl:value-of select="Translations:GetTranslation($EntityName,'indexedKI','indexedKI')"/>
                </phrase>
                <newline/>
              </cell>
              <cell verticalalign="top" horizontalalign="right" header="true">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}" fontstyle="bold">
                  <xsl:value-of select="Translations:GetTranslation($EntityName,'Totaal','Totaal')"/>
                </phrase>
              </cell>
            </row>
            <xsl:for-each select="*">
              <row>
                <cell verticalalign="top" horizontalalign="left">
                  <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}">
                    <xsl:value-of select="Tekst"/>
                  </phrase>
                </cell>
                <cell verticalalign="top" horizontalalign="left">
                  <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}">
                    <xsl:choose>
                      <xsl:when test="$Culture='nl-BE'">
                        <xsl:value-of select="Soort/nl"/>
                      </xsl:when>
                      <xsl:when test="$Culture='fr-FR'">
                        <xsl:value-of select="Soort/fr"/>
                      </xsl:when>
                      <xsl:when test="$Culture='en-US'">
                        <xsl:value-of select="Soort/en"/>
                      </xsl:when>
                      <xsl:otherwise>
                        <xsl:value-of select="Soort/nl"/>
                      </xsl:otherwise>
                    </xsl:choose>
                  </phrase>
                </cell>
                <cell verticalalign="top" horizontalalign="center">
                  <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
                    <xsl:value-of select="Helper:ConvertDate(string(StartDate))"/>-<xsl:value-of select="Helper:ConvertDate(string(EndDate))"/>
                  </phrase>
                </cell>
                <cell>
                  <phrase>&#160;</phrase>
                </cell>
                <cell verticalalign="top" horizontalalign="right">
                  <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
                    <xsl:if test="not(number(./node()[name()='Soort'])=6)">
                      <xsl:value-of select="format-number(KI,$NumberFormatting,'euro')"/>€
                    </xsl:if>
                  </phrase>
                </cell>
                <cell verticalalign="top" horizontalalign="right">
                  <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
                    <xsl:if test="not(number(./node()[name()='Soort'])=6)">
                      <xsl:value-of select="format-number(indexedKI,$NumberFormatting,'euro')"/>€
                    </xsl:if>
                  </phrase>
                </cell>
                <cell verticalalign="top" horizontalalign="right">
                  <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
                    <xsl:value-of select="format-number(Totaal,$NumberFormatting,'euro')"/>€
                  </phrase>
                </cell>
              </row>
            </xsl:for-each>
            <row>
              <cell verticalalign="top" horizontalalign="left" colspan="6">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}" fontstyle="bold">
                  <xsl:value-of select="Translations:GetTranslation($EntityName,'Totaal','Totaal')"/>
                </phrase>
              </cell>
              <cell verticalalign="top" horizontalalign="right">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" fontstyle="bold">
                  <xsl:value-of select="format-number(sum(*/Totaal),$NumberFormatting,'euro')"/>€
                </phrase>
              </cell>
            </row>
          </table>
        </xsl:if>
        <xsl:if test="$EntityName = 'VoordelenVanAlleAardHuurHerkwalificatie'">
          <table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" columns="7" width="100%" align="Center" widths="20;20;16;8;12;12;12" cellspacing="0">
            <row>
              <cell verticalalign="top" header="true" colspan="7">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}" fontstyle="bold">
                  <xsl:value-of select="$EntityTitle"/>
                </phrase>
                <newline/>
              </cell>
            </row>
            <row>
              <cell verticalalign="top" horizontalalign="left" header="true">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}" fontstyle="bold">
                  <xsl:value-of select="Translations:GetTranslation($EntityName,'Tekst','Tekst')"/>
                </phrase>
                <newline/>
              </cell>
              <cell verticalalign="top" horizontalalign="left" header="true">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}" fontstyle="bold">
                  <xsl:value-of select="Translations:GetTranslation($EntityName,'Periode','Periode')"/>
                </phrase>
              </cell>
              <cell verticalalign="top" horizontalalign="right" header="true">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}" fontstyle="bold">
                  <xsl:value-of select="Translations:GetTranslation($EntityName,'KIVerhuurd','KIVerhuurd')"/>
                </phrase>
              </cell>
              <cell verticalalign="top" horizontalalign="right" header="true">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}" fontstyle="bold">
                  <xsl:value-of select="Translations:GetTranslation($EntityName,'Percent','Percent')"/>
                </phrase>
              </cell>
              <cell verticalalign="top" horizontalalign="right" header="true">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}" fontstyle="bold">
                  <xsl:value-of select="Translations:GetTranslation($EntityName,'OntvangenHuur','OntvangenHuur')"/>
                  <newline/>
                </phrase>
              </cell>
              <cell verticalalign="top" horizontalalign="right" header="true">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}" fontstyle="bold">
                  <xsl:value-of select="Translations:GetTranslation($EntityName,'Grenswaarde','Grenswaarde')"/>
                </phrase>
              </cell>
              <cell verticalalign="top" horizontalalign="right" header="true">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}" fontstyle="bold">
                  <xsl:value-of select="Translations:GetTranslation($EntityName,'Herkwalificatie','Herkwalificatie')"/>
                </phrase>
              </cell>
            </row>
            <xsl:for-each select="*">
              <row>
                <cell verticalalign="top" horizontalalign="left">
                  <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}">
                    <xsl:value-of select="Tekst"/>
                  </phrase>
                </cell>
                <cell verticalalign="top" horizontalalign="center">
		  <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
		    <xsl:value-of select="Helper:ConvertDate(string(StartDate))"/>-<xsl:value-of select="Helper:ConvertDate(string(EndDate))"/>
		  </phrase>
                </cell>
                <cell verticalalign="top" horizontalalign="right">
                  <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
                    <xsl:value-of select="format-number(KIVerhuurd,$NumberFormatting,'euro')"/>€
                  </phrase>
                </cell>
                <cell verticalalign="top" horizontalalign="right">
                  <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
                    <xsl:value-of select="format-number(Percent,$NumberFormatting,'euro')"/>%
                  </phrase>
                </cell>
                <cell verticalalign="top" horizontalalign="right">
                  <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
                    <xsl:value-of select="format-number(OntvangenHuur,$NumberFormatting,'euro')"/>€
                  </phrase>
                </cell>
                <cell verticalalign="top" horizontalalign="right">
                  <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
                    <xsl:value-of select="format-number(Grenswaarde,$NumberFormatting,'euro')"/>€
                  </phrase>
                </cell>
                <cell verticalalign="top" horizontalalign="right">
                  <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
                    <xsl:value-of select="format-number(Herkwalificatie,$NumberFormatting,'euro')"/>€
                  </phrase>
                </cell>
              </row>
            </xsl:for-each>
            <row>
              <cell verticalalign="top" horizontalalign="left" colspan="4">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}" fontstyle="bold">
                  <xsl:value-of select="Translations:GetTranslation('Voordelen','Totaal','Totaal')"/>
                </phrase>
              </cell>
              <cell verticalalign="top" horizontalalign="right">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" fontstyle="bold">
                  <xsl:value-of select="format-number(sum(*/OntvangenHuur),$NumberFormatting,'euro')"/>€
                </phrase>
              </cell>
              <cell verticalalign="top" horizontalalign="right">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" fontstyle="bold">
                  <xsl:value-of select="format-number(sum(*/Grenswaarde),$NumberFormatting,'euro')"/>€
                </phrase>
              </cell>
              <cell verticalalign="top" horizontalalign="right">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" fontstyle="bold">
                  <xsl:value-of select="format-number(sum(*/Herkwalificatie),$NumberFormatting,'euro')"/>€
                </phrase>
              </cell>
            </row>
          </table>
        </xsl:if>
        <xsl:if test="$EntityName = 'VoordelenVanAlleAardOverige'">
          <table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" columns="7" width="100%" align="Center" widths="20;20;16;8;12;12;12" cellspacing="0">
            <row>
              <cell verticalalign="top" header="true" colspan="7">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}" fontstyle="bold">
                  <xsl:value-of select="$EntityTitle"/>
                </phrase>
                <newline/>
              </cell>
            </row>
            <row>
              <cell verticalalign="top" horizontalalign="left" header="true">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}" fontstyle="bold">
                  <xsl:value-of select="Translations:GetTranslation($EntityName,'Tekst','Tekst')"/>
                </phrase>
              </cell>
              <cell verticalalign="top" horizontalalign="left" header="true">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}" fontstyle="bold">
                  <xsl:value-of select="Translations:GetTranslation($EntityName,'Soort','Soort')"/>
                </phrase>
              </cell>
              <cell verticalalign="top" horizontalalign="center" header="true">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}" fontstyle="bold">
                  <xsl:value-of select="Translations:GetTranslation($EntityName,'Periode','Periode')"/>
                </phrase>
              </cell>
              <cell>
                <phrase>&#160;</phrase>
              </cell>
              <cell verticalalign="top" horizontalalign="right" header="true">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}" fontstyle="bold">
                  <xsl:value-of select="Translations:GetTranslation($EntityName,'MvH','MvH')"/>
                </phrase>
                <newline/>
              </cell>
              <cell verticalalign="top" horizontalalign="right" header="true">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}" fontstyle="bold">
                  <xsl:value-of select="Translations:GetTranslation($EntityName,'BTWBedrag','BTWBedrag')"/>
                </phrase>
              </cell>
              <cell verticalalign="top" horizontalalign="right" header="true">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}" fontstyle="bold">
                  <xsl:value-of select="Translations:GetTranslation($EntityName,'Totaal','Totaal')"/>
                </phrase>
              </cell>
            </row>
            <xsl:for-each select="*">
              <row>
                <cell verticalalign="top" horizontalalign="left">
                  <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}">
                    <xsl:value-of select="Tekst"/>
                  </phrase>
                </cell>
                <cell verticalalign="top" horizontalalign="left">
                  <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}">
                    <xsl:value-of select="$EntityTitle"/>
                  </phrase>
                </cell>
                <cell verticalalign="top" horizontalalign="center">
                  <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
                    <xsl:value-of select="Helper:ConvertDate(string(StartDate))"/>-<xsl:value-of select="Helper:ConvertDate(string(EndDate))"/>
                  </phrase>
                </cell>
                <cell>
                  <phrase>&#160;</phrase>
                </cell>
                <cell verticalalign="top" horizontalalign="right">
                  <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
                    <xsl:value-of select="format-number(MvH,$NumberFormatting,'euro')"/>€
                  </phrase>
                </cell>
                <cell verticalalign="top" horizontalalign="right">
                  <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
                    <xsl:value-of select="format-number(BTWBedrag,$NumberFormatting,'euro')"/>€
                  </phrase>
                </cell>
                <cell verticalalign="top" horizontalalign="right">
                  <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
                    <xsl:value-of select="format-number(Totaal,$NumberFormatting,'euro')"/>€
                  </phrase>
                </cell>
              </row>
            </xsl:for-each>
            <row>
              <cell verticalalign="top" horizontalalign="left" colspan="4">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}" fontstyle="bold">
                  <xsl:value-of select="Translations:GetTranslation($EntityName,'Totaal','Totaal')"/>
                </phrase>
              </cell>
              <cell verticalalign="top" horizontalalign="right">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" fontstyle="bold">
                  <xsl:value-of select="format-number(sum(*/MvH),$NumberFormatting,'euro')"/>€
                </phrase>
              </cell>
              <cell verticalalign="top" horizontalalign="right">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" fontstyle="bold">
                  <xsl:value-of select="format-number(sum(*/BTWBedrag),$NumberFormatting,'euro')"/>€
                </phrase>
              </cell>
              <cell verticalalign="top" horizontalalign="right">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" fontstyle="bold">
                  <xsl:value-of select="format-number(sum(*/Totaal),$NumberFormatting,'euro')"/>€
                </phrase>
              </cell>
            </row>
          </table>
        </xsl:if>
        <xsl:if test="$EntityName = 'VoordelenOverigeNuts'">
          <table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" columns="7" width="100%" align="Center" widths="20;20;16;8;12;12;12" cellspacing="0" cellpadding="0">
            <row>
              <cell verticalalign="top" header="true" colspan="7">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}" fontstyle="bold">
                  <xsl:value-of select="$EntityTitle"/>
                </phrase>
                <newline/>
              </cell>
            </row>
            <row>
              <cell verticalalign="top" horizontalalign="left" header="true">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}" fontstyle="bold">
                  <xsl:value-of select="Translations:GetTranslation($EntityName,'Tekst','Tekst')"/>
                </phrase>
              </cell>
              <cell verticalalign="top" horizontalalign="left" header="true">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}" fontstyle="bold">
                  <xsl:value-of select="Translations:GetTranslation($EntityName,'Soort','Soort')"/>
                </phrase>
              </cell>
              <cell verticalalign="top" horizontalalign="center" header="true">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}" fontstyle="bold">
                  <xsl:value-of select="Translations:GetTranslation($EntityName,'Periode','Periode')"/>
                </phrase>
              </cell>
              <cell>
                <phrase>&#160;</phrase>
              </cell>
              <cell verticalalign="top" horizontalalign="right" header="true">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}" fontstyle="bold">
                  <xsl:value-of select="Translations:GetTranslation($EntityName,'MvH','MvH')"/>
                </phrase>
                <newline/>
              </cell>
              <cell verticalalign="top" horizontalalign="right" header="true">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}" fontstyle="bold">
                  <xsl:value-of select="Translations:GetTranslation($EntityName,'BTW','BTW')"/>
                </phrase>
              </cell>
              <cell verticalalign="top" horizontalalign="right" header="true">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}" fontstyle="bold">
                  <xsl:value-of select="Translations:GetTranslation($EntityName,'Totaal','Totaal')"/>
                </phrase>
              </cell>
            </row>
            <xsl:for-each select="*">
              <row>
                <cell verticalalign="top" horizontalalign="left">
                  <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}">
                    <xsl:value-of select="Tekst"/>
                  </phrase>
                </cell>
                <cell verticalalign="top" horizontalalign="left">
                  <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}">
                    <xsl:value-of select="$EntityTitle"/>
                  </phrase>
                </cell>
                <cell verticalalign="top" horizontalalign="center">
                  <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
                    <xsl:value-of select="Helper:ConvertDate(string(StartDate))"/>-<xsl:value-of select="Helper:ConvertDate(string(EndDate))"/>
                  </phrase>
                </cell>
                <cell>
                  <phrase>&#160;</phrase>
                </cell>
                <cell verticalalign="top" horizontalalign="right">
                  <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
                    <xsl:value-of select="format-number(MvH,$NumberFormatting,'euro')"/>€
                  </phrase>
                </cell>
                <cell verticalalign="top" horizontalalign="right">
                  <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
                    <xsl:value-of select="format-number(BTW[number()=number()],$NumberFormatting,'euro')"/>€
                  </phrase>
                </cell>
                <cell verticalalign="top" horizontalalign="right">
                  <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
                    <xsl:value-of select="format-number(Totaal,$NumberFormatting,'euro')"/>€
                  </phrase>
                </cell>
              </row>
            </xsl:for-each>
            <row>
              <cell verticalalign="top" horizontalalign="left" colspan="4">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}" fontstyle="bold">
                  <xsl:value-of select="Translations:GetTranslation($EntityName,'Totaal','Totaal')"/>
                </phrase>
              </cell>
              <cell verticalalign="top" horizontalalign="right">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" fontstyle="bold">
                  <xsl:value-of select="format-number(sum(*/MvH),$NumberFormatting,'euro')"/>€
                </phrase>
              </cell>
              <cell verticalalign="top" horizontalalign="right">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" fontstyle="bold">
                  <xsl:value-of select="format-number(sum(*/BTW),$NumberFormatting,'euro')"/>€
                </phrase>
              </cell>
              <cell verticalalign="top" horizontalalign="right">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" fontstyle="bold">
                  <xsl:value-of select="format-number(sum(*/Totaal),$NumberFormatting,'euro')"/>€
                </phrase>
              </cell>
            </row>
          </table>
        </xsl:if>
        <xsl:if test="$EntityName = 'VoordelenSocialeBijdragenBedrijfsleider'">
          <table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" columns="7" width="100%" align="Center" widths="20;20;16;8;12;12;12" cellspacing="0">
            <row>
              <cell verticalalign="top" header="true" colspan="7">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}" fontstyle="bold">
                  <xsl:value-of select="$EntityTitle"/>
                </phrase>
                <newline/>
              </cell>
            </row>
            <row>
              <cell verticalalign="top" horizontalalign="left" header="true">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}" fontstyle="bold">
                  <xsl:value-of select="Translations:GetTranslation($EntityName,'Tekst','Tekst')"/>
                </phrase>
              </cell>
              <cell verticalalign="top" horizontalalign="left" header="true">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}" fontstyle="bold">
                  <xsl:value-of select="Translations:GetTranslation($EntityName,'Soort','Soort')"/>
                </phrase>
              </cell>
              <cell verticalalign="top" horizontalalign="center" header="true">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}" fontstyle="bold">
                  <xsl:value-of select="Translations:GetTranslation($EntityName,'Periode','Periode')"/>
                </phrase>
              </cell>
              <cell colspan="3">
                <phrase>&#160;</phrase>
              </cell>
              <cell verticalalign="top" horizontalalign="right" header="true">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}" fontstyle="bold">
                  <xsl:value-of select="Translations:GetTranslation($EntityName,'Totaal','Totaal')"/>
                </phrase>
                <newline/>
              </cell>
            </row>
            <xsl:for-each select="*">
              <row>
                <cell verticalalign="top" horizontalalign="left">
                  <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}">
                    <xsl:value-of select="Tekst"/>
                  </phrase>
                </cell>
                <cell verticalalign="top" horizontalalign="left">
                  <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}">
                    <xsl:value-of select="$EntityTitle"/>
                  </phrase>
                </cell>
                <cell verticalalign="top" horizontalalign="center">
                  <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
                    <xsl:value-of select="Helper:ConvertDate(string(StartDate))"/>-<xsl:value-of select="Helper:ConvertDate(string(EndDate))"/>
                  </phrase>
                </cell>
                <cell colspan="3">
                  <phrase>&#160;</phrase>
                </cell>
                <cell verticalalign="top" horizontalalign="right">
                  <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
                    <xsl:value-of select="format-number(Totaal,$NumberFormatting,'euro')"/>€
                  </phrase>
                </cell>
              </row>
            </xsl:for-each>
            <row>
              <cell colspan="6" verticalalign="top" horizontalalign="left">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}" fontstyle="bold">
                  <xsl:value-of select="Translations:GetTranslation($EntityName,'Totaal','Totaal')"/>
                </phrase>
              </cell>
              <cell verticalalign="top" horizontalalign="right">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" fontstyle="bold">
                  <xsl:value-of select="format-number(sum(*/Totaal),$NumberFormatting,'euro')"/>€
                </phrase>
              </cell>
            </row>
          </table>
        </xsl:if>
        <xsl:if test="$EntityName = 'VoordelenInterestenRCBedrijfsleider'">
          <table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" columns="7" width="100%" align="Center" widths="20;20;16;8;12;12;12" cellspacing="0" cellpadding="0">
            <row>
              <cell verticalalign="top" header="true" colspan="7">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}" fontstyle="bold">
                  <xsl:value-of select="$EntityTitle"/>
                </phrase>
              </cell>
            </row>
            <row>
              <cell verticalalign="top" horizontalalign="left" header="true">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}" fontstyle="bold">
                  <xsl:value-of select="Translations:GetTranslation($EntityName,'Tekst','Tekst')"/>
                </phrase>
              </cell>
              <cell verticalalign="top" horizontalalign="left" header="true">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}" fontstyle="bold">
                  <xsl:value-of select="Translations:GetTranslation($EntityName,'Soort','Soort')"/>
                </phrase>
              </cell>
              <cell verticalalign="top" horizontalalign="center" header="true">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}" fontstyle="bold">
                  <xsl:value-of select="Translations:GetTranslation($EntityName,'Periode','Periode')"/>
                </phrase>
              </cell>
              <cell>
                <phrase>&#160;</phrase>
              </cell>
              <cell verticalalign="top" horizontalalign="right" header="true">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}" fontstyle="bold">
                  <xsl:value-of select="Translations:GetTranslation($EntityName,'MvH','MvH')"/>
                </phrase>
              </cell>
              <cell verticalalign="top" horizontalalign="right" header="true">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}" fontstyle="bold">
                  <xsl:value-of select="Translations:GetTranslation($EntityName,'RV','RV')"/>
                </phrase>
              </cell>
              <cell verticalalign="top" horizontalalign="right" header="true">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}" fontstyle="bold">
                  <xsl:value-of select="Translations:GetTranslation($EntityName,'Totaal','Totaal')"/>
                </phrase>
                <newline/>
              </cell>
            </row>
            <xsl:for-each select="*">
              <row>
                <cell verticalalign="top" horizontalalign="left">
                  <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}">
                    <xsl:value-of select="Tekst"/>
                  </phrase>
                </cell>
                <cell verticalalign="top" horizontalalign="left">
                  <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}">
                    <xsl:value-of select="$EntityTitle"/>(RV:<xsl:value-of select="format-number(RVPerc,$NumberFormatting,'euro')"/>%)
                  </phrase>
                </cell>
                <cell verticalalign="top" horizontalalign="center">
                  <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
                    <xsl:value-of select="Helper:ConvertDate(string(StartDate))"/>-<xsl:value-of select="Helper:ConvertDate(string(EndDate))"/>
                  </phrase>
                </cell>
                <cell>
                  <phrase>&#160;</phrase>
                </cell>
                <cell verticalalign="top" horizontalalign="right">
                  <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
                    <xsl:value-of select="format-number(MvH,$NumberFormatting,'euro')"/>€
                  </phrase>
                </cell>
                <cell verticalalign="top" horizontalalign="right">
                  <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
                    <xsl:value-of select="format-number(RV,$NumberFormatting,'euro')"/>€
                  </phrase>
                </cell>
                <cell verticalalign="top" horizontalalign="right">
                  <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
                    <xsl:value-of select="format-number(Totaal,$NumberFormatting,'euro')"/>€
                  </phrase>
                </cell>
              </row>
            </xsl:for-each>
            <row>
              <cell verticalalign="top" horizontalalign="left" colspan="4">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}" fontstyle="bold">
                  <xsl:value-of select="Translations:GetTranslation($EntityName,'Totaal','Totaal')"/>
                </phrase>
              </cell>
              <cell verticalalign="top" horizontalalign="right">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" fontstyle="bold">
                  <xsl:value-of select="format-number(sum(*/MvH),$NumberFormatting,'euro')"/>€
                </phrase>
              </cell>
              <cell verticalalign="top" horizontalalign="right">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" fontstyle="bold">
                  <xsl:value-of select="format-number(sum(*/RV),$NumberFormatting,'euro')"/>€
                </phrase>
              </cell>
              <cell verticalalign="top" horizontalalign="right">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" fontstyle="bold">
                  <xsl:value-of select="format-number(sum(*/Totaal),$NumberFormatting,'euro')"/>€
                </phrase>
              </cell>
            </row>
          </table>
        </xsl:if>
      </xsl:if>
    </xsl:for-each>
  </xsl:template>
</xsl:stylesheet>