<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" exclude-result-prefixes="Helper Translations File Client" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:Helper="urn:Helper" xmlns:Translations="urn:Translations" xmlns:File="urn:File" xmlns:Client="urn:Client">
  <xsl:decimal-format name="NonNumber" NaN="0" zero-digit="0"/>
  <xsl:variable name="WorksheetLabelFontsize" select="string('10')"/>
  <xsl:variable name="WorksheetFontsize" select="string('10')"/>
  <xsl:variable name="WorksheetNumbersize" select="string('8')"/>
  <xsl:variable name="WorksheetSmallFontsize" select="string('8')"/>
  <xsl:variable name="WorksheetTotalsFontsize" select="string('10')"/>
  <xsl:variable name="WorksheetSpacing" select="1.1"/>
  <xsl:decimal-format name="euro" decimal-separator="," grouping-separator="."/>
  <xsl:variable name="NumberFormatting" select="string('#.##0,00')"/>
  <xsl:variable name="NumberFormattingNoDigit" select="string('#.##0')"/>
  <xsl:variable name="FONT" select="string('C:\WINDOWS\Fonts\EYInterstate-Light.ttf')"/>
  <xsl:param name="RuleAppDef"/>
  <xsl:param name="Culture"/>
  <xsl:template match="VerliesHandelsDebiteuren">
    <Root orientation="landscape">
      <xsl:apply-templates select="." mode="WORKSHEET">
        <xsl:with-param name="results" select="."/>
      </xsl:apply-templates>
    </Root>
  </xsl:template>
  <xsl:template match="VerliesHandelsDebiteuren" mode="WORKSHEET">
        <table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" columns="5" width="100%" align="Center" widths="35;12;12;12;12" cellspacing="0" cellpadding="5">
          <row>
            <cell verticalalign="middle" colspan="5" header="true" borderwidth="0.5" left="false" right="false" top="false" bottom="true" bordercolor="black"/>
          </row>
          <row>
            <cell verticalalign="middle" horizontalalign="left" header="true" borderwidth="0.5" left="true" right="true" bottom="true" bordercolor="black">
              <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}" fontstyle="bold">
                <xsl:value-of select="Translations:GetTranslation('VerliesHD','Klant','Klant')"/>
              </phrase>
            </cell>
            <cell verticalalign="top" horizontalalign="center" header="true" borderwidth="0.5" right="true" bottom="true" bordercolor="black" >
              <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}" fontstyle="bold">
                <xsl:value-of select="Translations:GetTranslation('VerliesHD','Faillissement','Faillissement')"/>
              </phrase>
              <newline/>
            </cell>
            <cell verticalalign="middle" horizontalalign="center" header="true" borderwidth="0.5" right="true" bottom="true" bordercolor="black">
              <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}" fontstyle="bold">
                <xsl:value-of select="Translations:GetTranslation('VerliesHD','AndereReden','AndereReden')"/>
              </phrase>
              <newline/>
            </cell>
            <cell verticalalign="middle" horizontalalign="center" header="true" borderwidth="0.5" right="true" bottom="true" bordercolor="black">
              <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}" fontstyle="bold">
                <xsl:value-of select="Translations:GetTranslation('VerliesHD','BTW','BTW')"/>
              </phrase>
            </cell>
            <cell verticalalign="middle" horizontalalign="center" header="true" borderwidth="0.5" right="true" bottom="true" bordercolor="black">
              <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}" fontstyle="bold">
                <xsl:value-of select="Translations:GetTranslation('VerliesHD','Jaar','Jaar')"/>
              </phrase>
            </cell>
          </row>
          <xsl:for-each select="VerliesHD/*">
            <row>
              <cell verticalalign="middle" horizontalalign="left" borderwidth="0.5" left="true" right="true" bordercolor="black">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}">
                  <xsl:value-of select="Klant/nl"/>
                </phrase>
                <newline/>
              </cell>
              <cell verticalalign="middle" horizontalalign="right" borderwidth="0.5" right="true" bordercolor="black">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
                  <xsl:if test="Faillissement[number()=number()]">
                  <xsl:value-of select="format-number(Faillissement,$NumberFormatting,'euro')"/>€
                  </xsl:if>
                </phrase>
              </cell>
              <cell verticalalign="middle" horizontalalign="right" borderwidth="0.5" right="true" bordercolor="black">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
                  <xsl:if test="AndereReden[number()=number()]">
                    <xsl:value-of select="format-number(AndereReden,$NumberFormatting,'euro')"/>€
                  </xsl:if>
                </phrase>
              </cell>
              <cell verticalalign="middle" horizontalalign="right" borderwidth="0.5" right="true" bordercolor="black">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
                  <xsl:if test="BTW[number()=number()]">
                    <xsl:value-of select="format-number(BTW,$NumberFormatting,'euro')"/>€
                  </xsl:if>
                </phrase>
              </cell>
              <cell verticalalign="middle" horizontalalign="center" borderwidth="0.5" right="true" bordercolor="black">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
                  <xsl:value-of select="Jaar/nl"/>
                </phrase>
              </cell>
            </row>
          </xsl:for-each>
          <row>
            <cell verticalalign="middle" horizontalalign="left" borderwidth="0.5" bottom="true" left="true" right="true" top="true" bordercolor="black">
              <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}" fontstyle="bold">
                <xsl:value-of select="Translations:GetTranslation('','Totaal','Totaal')"/>
              </phrase>
              <newline/>
            </cell>
            <cell verticalalign="middle" horizontalalign="right" borderwidth="0.5" bottom="true" right="true" top="true" bordercolor="black">
              <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" fontstyle="bold">
                <xsl:value-of select="format-number(sum(VerliesHD/*/Faillissement[number()=number()]),$NumberFormatting,'euro')"/>€
              </phrase>
              <newline/>
            </cell>
            <cell verticalalign="top" horizontalalign="right" borderwidth="0.5" bottom="true" right="true" top="true" bordercolor="black">
              <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" fontstyle="bold">
                <xsl:value-of select="format-number(sum(VerliesHD/*/AndereReden[number()=number()]),$NumberFormatting,'euro')"/>€
              </phrase>
            </cell>
            <cell verticalalign="top" horizontalalign="right" borderwidth="0.5" bottom="true" right="true" top="true" bordercolor="black">
              <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" fontstyle="bold">
                <xsl:value-of select="format-number(sum(VerliesHD/*/BTW[number()=number()]),$NumberFormatting,'euro')"/>€
              </phrase>
            </cell>
            <cell verticalalign="middle" horizontalalign="right" borderwidth="0.5" bottom="true" right="true" top="true" bordercolor="black">
              <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" fontstyle="bold"></phrase>
            </cell>
          </row>
        </table>
  </xsl:template>
</xsl:stylesheet>