<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" exclude-result-prefixes="Helper Translations File Client" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:Helper="urn:Helper" xmlns:Translations="urn:Translations" xmlns:File="urn:File" xmlns:Client="urn:Client">
  <xsl:decimal-format name="NonNumber" NaN="0" zero-digit="0"/>
  <xsl:variable name="WorksheetLabelFontsize" select="string('10')"/>
  <xsl:variable name="WorksheetFontsize" select="string('10')"/>
  <xsl:variable name="WorksheetNumbersize" select="string('8')"/>
  <xsl:variable name="WorksheetSmallFontsize" select="string('8')"/>
  <xsl:variable name="WorksheetTotalsFontsize" select="string('10')"/>
  <xsl:variable name="WorksheetSpacing" select="1.1"/>
  <xsl:decimal-format name="euro" decimal-separator="," grouping-separator="."/>
  <xsl:variable name="NumberFormatting" select="string('#.##0,00')"/>
  <xsl:variable name="NumberFormattingNoDigit" select="string('#.##0')"/>
  <xsl:variable name="FONT" select="string('C:\WINDOWS\Fonts\EYInterstate-Light.ttf')"/>
  <xsl:variable name="smallcase" select="'abcdefghijklmnopqrstuvwxyz'" />
  <xsl:variable name="uppercase" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZ'" />
  <xsl:param name="RuleAppDef"/>
  <xsl:param name="Culture"/>
  <xsl:template match="BelasteReserves">
    <Root orientation="landscape">
      <xsl:if test="count(AllBelasteReserves/*)>0">
        <xsl:apply-templates select="." mode="AllBelasteReserves"/>
      </xsl:if>
      <!--xsl:if test="count(AllBelasteReserves)>0 and count(AanpassingBTReserves)>0"><newpage/></xsl:if-->
      <xsl:if test="count(AanpassingBTReserves/*)>0">
        <xsl:apply-templates select="." mode="AanpassingBTReserves"/>
      </xsl:if>
      <xsl:choose>
        <xsl:when test="count(AanpassingBTReserves/*)=0 and count(AllBelasteReserves/*) = 0">
          <phrase>NO DATA IN WORKSHEET</phrase>
        </xsl:when>
        <xsl:otherwise>
          <xsl:apply-templates select="." mode="TotalenEnBeweging"/>
        </xsl:otherwise>
      </xsl:choose>
    </Root>
  </xsl:template>
  <xsl:template name="AllBelasteReserves.Group">
    <xsl:param name="AllBelasteReserves"/>
    <xsl:param name="TypeReserve"/>
    <xsl:variable name="shortList" select="$AllBelasteReserves/*[translate(TypeReseve/id, $uppercase, $smallcase)=$TypeReserve]"/>

    <xsl:variable name="cnt" select="count($shortList)"/>
    <xsl:if test="$cnt>0">
      <xsl:variable name="first" select="$shortList[1]"/>
      <xsl:variable name="type">
        <xsl:choose>
          <xsl:when test="$Culture='nl-BE'"><xsl:value-of select="$first/TypeReseve/nl"/></xsl:when>
          <xsl:when test="$Culture='fr-FR'"><xsl:value-of select="$first/TypeReseve/fr"/></xsl:when>
          <xsl:otherwise><xsl:value-of select="$first/TypeReseve/en"/></xsl:otherwise>
        </xsl:choose>
      </xsl:variable>
      <xsl:choose>
        <xsl:when test="$cnt=1">
          <row>
            <cell>
              <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}" fontstyle="italic">
                  <xsl:value-of select="$type"/>
              </phrase>
            </cell>
            <cell verticalalign="middle" horizontalalign="right">
              <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
                <xsl:if test="$first/BeginToestand[number()=number()]">
                  <xsl:value-of select="format-number($first/BeginToestand,$NumberFormatting, 'euro')"/>€
                </xsl:if>
              </phrase>
            </cell>
            <cell>
              <phrase>
                <space/>
              </phrase>
            </cell>
            <cell verticalalign="middle" horizontalalign="right">
              <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
                <xsl:if test="$first/EindToestand[number()=number()]">
                  <xsl:value-of select="format-number($first/EindToestand,$NumberFormatting, 'euro')"/>€
                </xsl:if>
              </phrase>
            </cell>
            <cell>
              <phrase>
                <space/>
              </phrase>
            </cell>
            <cell verticalalign="middle" horizontalalign="right">
              <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
                <xsl:if test="$first/ToenameTijdensBJ[number()=number()]">
                  <xsl:value-of select="format-number($first/ToenameTijdensBJ,$NumberFormatting, 'euro')"/>€
                </xsl:if>
              </phrase>
            </cell>
            <cell>
              <phrase>
                <space/>
              </phrase>
            </cell>
            <cell verticalalign="middle" horizontalalign="right">
              <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
                <xsl:if test="$first/AfnameTijdensBJ[number()=number()]">
                  <xsl:value-of select="format-number($first/AfnameTijdensBJ, $NumberFormatting, 'euro')"/>€
                </xsl:if>
              </phrase>
            </cell>
          </row>
        </xsl:when>
        <xsl:when test="$cnt>1">
          <row>
            <cell colspan="8">
              <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}" fontstyle="italic">
                <xsl:value-of select="$type"/>
              </phrase>
            </cell>
          </row>
          <xsl:apply-templates select="$shortList"/>
        </xsl:when>
      </xsl:choose>
    </xsl:if>
  </xsl:template>
  <xsl:template name="AanpassingBTReserves.Group">
    <xsl:param name="AanpassingBTReserves"/>
    <xsl:param name="TypeReserve"/>
    <xsl:variable name="shortList" select="$AanpassingBTReserves/*[translate(TypeReseve/id, $uppercase, $smallcase)=$TypeReserve]"/>

    <xsl:variable name="cnt" select="count($shortList)"/>
    <xsl:if test="$cnt>0">
      <xsl:variable name="first" select="$shortList[1]"/>
      <xsl:variable name="type">
        <xsl:choose>
          <xsl:when test="$Culture='nl-BE'">
            <xsl:value-of select="$first/TypeReseve/nl"/>
          </xsl:when>
          <xsl:when test="$Culture='fr-FR'">
            <xsl:value-of select="$first/TypeReseve/fr"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="$first/TypeReseve/en"/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:variable>
      <xsl:choose>
        <xsl:when test="$cnt=1">
          <row>
            <cell>
              <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}" fontstyle="italic">
                <xsl:value-of select="$type"/>
              </phrase>
            </cell>
            <cell verticalalign="middle" horizontalalign="right">
              <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
                <xsl:value-of select="format-number($first/BedragNaPercentage,$NumberFormatting, 'euro')"/>€
              </phrase>
            </cell>
            <cell>
              <phrase>
                <space/>
              </phrase>
            </cell>
            <cell>
              <phrase>
                <space/>
              </phrase>
            </cell>
            <cell>
              <phrase>
                <space/>
              </phrase>
            </cell>
            <cell>
              <phrase>
                <space/>
              </phrase>
            </cell>
            <cell>
              <phrase>
                <space/>
              </phrase>
            </cell>
            <cell>
              <phrase>
                <space/>
              </phrase>
            </cell>
          </row>
        </xsl:when>
        <xsl:when test="$cnt>1">
          <row>
            <cell colspan="8">
              <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}" fontstyle="italic">
                <xsl:value-of select="$type"/>
              </phrase>
            </cell>
          </row>
          <xsl:apply-templates select="$shortList"/>
        </xsl:when>
      </xsl:choose>
    </xsl:if>
  </xsl:template>
  <xsl:template match="BelasteReserves" mode="AllBelasteReserves">
    <xsl:variable name="BeginToestand" select="sum(AllBelasteReserves/*/BeginToestand)"/>
    <xsl:variable name="EindToestand" select="sum(AllBelasteReserves/*/EindToestand)"/>
    <xsl:variable name="ToenameTijdensBJ" select="sum(AllBelasteReserves/*/ToenameTijdensBJ)"/>
    <xsl:variable name="AfnameTijdensBJ" select="sum(AllBelasteReserves/*/AfnameTijdensBJ)"/>
    <newline/>
    <newline/>
    <table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" columns="8" width="100%" align="Center" widths="45;30;1;30;1;30;1;30" cellspacing="0">
      <row>
        <cell verticalalign="middle" horizontalalign="right" colspan="2">
          <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}" fontstyle="underline">
            <xsl:variable name="Year" select="number(File:GetValue('EndDate','yyyy')) - 1" /><xsl:value-of select="File:GetValue('EndDate','dd/MM')"/>/<xsl:value-of select="$Year"/>
          </phrase>
        </cell>
        <cell>
          <phrase>
            <space/>
          </phrase>
        </cell>
        <cell verticalalign="middle" horizontalalign="right">
          <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}" fontstyle="underline">
            <xsl:value-of select="File:GetValue('EndDate','dd/MM/yyyy')"/>
          </phrase>
        </cell>
        <cell>
          <phrase>
            <space/>
          </phrase>
        </cell>
        <cell verticalalign="middle" horizontalalign="right">
          <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}" fontstyle="underline">
            <xsl:value-of select="Translations:GetTranslation('BelasteReserves','Verhoging','Verhoging')"/>
          </phrase>
        </cell>
        <cell>
          <phrase>
            <space/>
          </phrase>
        </cell>
        <cell verticalalign="middle" horizontalalign="right">
          <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}" fontstyle="underline">
            <xsl:value-of select="Translations:GetTranslation('BelasteReserves','Vermindering','Vermindering')"/>
          </phrase>
        </cell>
      </row>
      <row>
        <cell colspan="8">
          <newline/>
        </cell>
      </row>
      <row>
        <cell verticalaling="middle" horizontalalign="left">
          <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}" fontstyle="underline">
            <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}" fontstyle="underline">
              <xsl:value-of select="Translations:GetTranslation('BelasteReserves','Omschrijving','Omschrijving')"/>
            </phrase>
          </phrase>
        </cell>
        <cell verticalalign="middle" horizontalalign="right">
          <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}" fontstyle="underline">EUR</phrase>
        </cell>
        <cell>
          <phrase>
            <space/>
          </phrase>
        </cell>
        <cell verticalalign="middle" horizontalalign="right">
          <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}" fontstyle="underline">EUR</phrase>
        </cell>
        <cell>
          <phrase>
            <space/>
          </phrase>
        </cell>
        <cell verticalalign="middle" horizontalalign="right">
          <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}" fontstyle="underline">EUR</phrase>
        </cell>
        <cell>
          <phrase>
            <space/>
          </phrase>
        </cell>
        <cell verticalalign="middle" horizontalalign="right">
          <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}" fontstyle="underline">EUR</phrase>
        </cell>
      </row>
      <row>
        <cell colspan="8">
          <newline/>
        </cell>
      </row>
      <xsl:call-template name="AllBelasteReserves.Group">
        <xsl:with-param name="AllBelasteReserves" select="AllBelasteReserves"/>
        <xsl:with-param name="TypeReserve" select="'bbc12ef9-3e15-4c70-92d8-11f7388ed64a'"/>
      </xsl:call-template>
      <xsl:call-template name="AllBelasteReserves.Group">
        <xsl:with-param name="AllBelasteReserves" select="AllBelasteReserves"/>
        <xsl:with-param name="TypeReserve" select="'8ca508c3-b59f-4106-9afa-30b1acda92b0'"/>
      </xsl:call-template>
      <xsl:call-template name="AllBelasteReserves.Group">
        <xsl:with-param name="AllBelasteReserves" select="AllBelasteReserves"/>
        <xsl:with-param name="TypeReserve" select="'85ef3af8-230f-4c81-b10d-4ff87534492c'"/>
      </xsl:call-template>
      <xsl:call-template name="AllBelasteReserves.Group">
        <xsl:with-param name="AllBelasteReserves" select="AllBelasteReserves"/>
        <xsl:with-param name="TypeReserve" select="'04f61c72-243d-4673-80b2-5e8dfd576f8d'"/>
      </xsl:call-template>
      <xsl:call-template name="AllBelasteReserves.Group">
        <xsl:with-param name="AllBelasteReserves" select="AllBelasteReserves"/>
        <xsl:with-param name="TypeReserve" select="'43d09724-06db-4985-8f7c-aa03f6a4736e'"/>
      </xsl:call-template>
      <xsl:call-template name="AllBelasteReserves.Group">
        <xsl:with-param name="AllBelasteReserves" select="AllBelasteReserves"/>
        <xsl:with-param name="TypeReserve" select="'b9048cb3-37f2-42b3-9fcd-e8d1b7277bd2'"/>
      </xsl:call-template>
      <xsl:call-template name="AllBelasteReserves.Group">
        <xsl:with-param name="AllBelasteReserves" select="AllBelasteReserves"/>
        <xsl:with-param name="TypeReserve" select="'0f5ff0f1-df29-4342-9e8c-1630304da12f'"/>
      </xsl:call-template>
      <xsl:call-template name="AllBelasteReserves.Group">
        <xsl:with-param name="AllBelasteReserves" select="AllBelasteReserves"/>
        <xsl:with-param name="TypeReserve" select="'244403c0-219f-4a3a-94da-c9eb19950b4f'"/>
      </xsl:call-template>
      <xsl:call-template name="AllBelasteReserves.Group">
        <xsl:with-param name="AllBelasteReserves" select="AllBelasteReserves"/>
        <xsl:with-param name="TypeReserve" select="'ea78dbb2-acd3-4fad-812e-029b66f8fecc'"/>
      </xsl:call-template>
      <xsl:call-template name="AllBelasteReserves.Group">
        <xsl:with-param name="AllBelasteReserves" select="AllBelasteReserves"/>
        <xsl:with-param name="TypeReserve" select="'b97e3772-4090-4bad-a623-a3e0c6044855'"/>
      </xsl:call-template>
      <xsl:call-template name="AllBelasteReserves.Group">
        <xsl:with-param name="AllBelasteReserves" select="AllBelasteReserves"/>
        <xsl:with-param name="TypeReserve" select="'17e42920-cba9-43c4-90c5-2b74d75daec7'"/>
      </xsl:call-template>
      <xsl:call-template name="AllBelasteReserves.Group">
        <xsl:with-param name="AllBelasteReserves" select="AllBelasteReserves"/>
        <xsl:with-param name="TypeReserve" select="'f67a17d7-8f7d-43d9-9955-b8a9693587a1'"/>
      </xsl:call-template>
      <xsl:call-template name="AllBelasteReserves.Group">
        <xsl:with-param name="AllBelasteReserves" select="AllBelasteReserves"/>
        <xsl:with-param name="TypeReserve" select="'4b49ee2c-90c4-4649-93db-df950d9b90e3'"/>
      </xsl:call-template>
      <row>
        <cell>
          <phrase>
            <space/>
          </phrase>
        </cell>
        <cell verticalalign="middle" horizontalalign="right" border="0.5" bordercolor="black" top="true" >
          <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
            <xsl:value-of select="format-number($BeginToestand, $NumberFormatting, 'euro')"/>€
          </phrase>
        </cell>
        <cell>
          <phrase>
            <space/>
          </phrase>
        </cell>
        <cell verticalalign="middle" horizontalalign="right" border="0.5" bordercolor="black" top="true">
          <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
            <xsl:value-of select="format-number($EindToestand, $NumberFormatting, 'euro')"/>€
          </phrase>
        </cell>
        <cell>
          <phrase>
            <space/>
          </phrase>
        </cell>
        <cell verticalalign="middle" horizontalalign="right" border="0.5" bordercolor="black" top="true">
          <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
            <xsl:value-of select="format-number($ToenameTijdensBJ, $NumberFormatting, 'euro')"/>€
          </phrase>
        </cell>
        <cell>
          <phrase>
            <space/>
          </phrase>
        </cell>
        <cell verticalalign="middle" horizontalalign="right" border="0.5" bordercolor="black" top="true">
          <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
            <xsl:value-of select="format-number($AfnameTijdensBJ, $NumberFormatting, 'euro')"/>€
          </phrase>
        </cell>
      </row>
    </table>
  </xsl:template>
  <xsl:template match="BelasteReservesItem">
    <row>
      <cell verticalalign="middle" horizontalalign="left">
        <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}">
          &#160;&#160;&#160;&#160;<xsl:value-of select="Omschrijving"/>
        </phrase>
      </cell>
      <cell verticalalign="middle" horizontalalign="right">
        <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
          <xsl:if test="BeginToestand[number()=number()]">
          <xsl:value-of select="format-number(BeginToestand,$NumberFormatting, 'euro')"/>€
          </xsl:if>
        </phrase>
      </cell>
      <cell>
        <phrase>
          <space/>
        </phrase>
      </cell>
      <cell verticalalign="middle" horizontalalign="right">
        <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
          <xsl:if test="EindToestand[number()=number()]">
            <xsl:value-of select="format-number(EindToestand,$NumberFormatting, 'euro')"/>€
          </xsl:if>
        </phrase>
      </cell>
      <cell>
        <phrase>
          <space/>
        </phrase>
      </cell>
      <cell verticalalign="middle" horizontalalign="right">
        <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
          <xsl:if test="ToenameTijdensBJ[number()=number()]">
            <xsl:value-of select="format-number(ToenameTijdensBJ,$NumberFormatting, 'euro')"/>€
          </xsl:if>
        </phrase>
      </cell>
      <cell>
        <phrase>
          <space/>
        </phrase>
      </cell>
      <cell verticalalign="middle" horizontalalign="right">
        <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
          <xsl:if test="AfnameTijdensBJ[number()=number()]">
            <xsl:value-of select="format-number(AfnameTijdensBJ, $NumberFormatting, 'euro')"/>€
          </xsl:if>
        </phrase>
      </cell>
    </row>
  </xsl:template>
  <xsl:template match="BelasteReserves" mode="AanpassingBTReserves">
    
    <table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" columns="8" width="100%" align="Center" widths="45;30;1;30;1;30;1;30" cellspacing="0" >
      <row>
        <cell verticalaling="middle" horizontalalign="left" colspan="8">
          <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}" fontstyle="bold">
            <xsl:value-of select="Translations:GetTranslation('BelasteReserves','AanpassingenBTReservesItem','Aanpassingen BT Reserves')"/>
          </phrase>
        </cell>
      </row>
      <xsl:call-template name="AanpassingBTReserves.Group">
        <xsl:with-param name="AanpassingBTReserves" select="AanpassingBTReserves"/>
        <xsl:with-param name="TypeReserve" select="'f34c7d2b-428d-494d-a29b-f2bd2910a7a9'"/>
      </xsl:call-template>
      <xsl:call-template name="AanpassingBTReserves.Group">
        <xsl:with-param name="AanpassingBTReserves" select="AanpassingBTReserves"/>
        <xsl:with-param name="TypeReserve" select="'9deb5faf-a095-420d-8f40-1becdcde72fc'"/>
      </xsl:call-template>
      <xsl:call-template name="AanpassingBTReserves.Group">
        <xsl:with-param name="AanpassingBTReserves" select="AanpassingBTReserves"/>
        <xsl:with-param name="TypeReserve" select="'f1875c70-b0dc-41be-8ada-46c702a954c1'"/>
      </xsl:call-template>
      <xsl:call-template name="AanpassingBTReserves.Group">
        <xsl:with-param name="AanpassingBTReserves" select="AanpassingBTReserves"/>
        <xsl:with-param name="TypeReserve" select="'6a716040-5635-4e9d-8161-f5eaaaff5db2'"/>
      </xsl:call-template>
      <xsl:call-template name="AanpassingBTReserves.Group">
        <xsl:with-param name="AanpassingBTReserves" select="AanpassingBTReserves"/>
        <xsl:with-param name="TypeReserve" select="'609700e4-ebba-44b2-8ed3-f47ac30bcd12'"/>
      </xsl:call-template>
      <xsl:call-template name="AanpassingBTReserves.Group">
        <xsl:with-param name="AanpassingBTReserves" select="AanpassingBTReserves"/>
        <xsl:with-param name="TypeReserve" select="'0355b0d0-46be-46d7-a59e-6858c6f7afa4'"/>
      </xsl:call-template>
      <row>
        <cell colspan="8">
          <phrase>
            <space/>
          </phrase>
          <newline/>
        </cell>
      </row>
    </table>
  </xsl:template>
  <xsl:template match="AanpassingBTReservesItem">
    <row>
      <cell verticalalign="middle" horizontalalign="left">
        <paragraph indentationleft="10" lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}">
          <xsl:value-of select="Omschrijving"/>
        </paragraph>
      </cell>
      <cell verticalalign="middle" horizontalalign="right">
        <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
          <xsl:if test="BedragNaPercentage[number()=number()]">
          <xsl:value-of select="format-number(BedragNaPercentage,$NumberFormatting, 'euro')"/>€
          </xsl:if>
        </phrase>
      </cell>
      <cell>
        <phrase>
          <space/>
        </phrase>
      </cell>
      <cell>
        <phrase>
          <space/>
        </phrase>
      </cell>
      <cell>
        <phrase>
          <space/>
        </phrase>
      </cell>
      <cell>
        <phrase>
          <space/>
        </phrase>
      </cell>
      <cell>
        <phrase>
          <space/>
        </phrase>
      </cell>
      <cell>
        <phrase>
          <space/>
        </phrase>
      </cell>
    </row>
  </xsl:template>
  <xsl:template match="BelasteReserves" mode="TotalenEnBeweging">
    <xsl:variable name="BTAll" select="sum(AllBelasteReserves/*/BeginToestand)"/>
    <xsl:variable name="BTAanpas" select="sum(AanpassingBTReserves/*/BedragNaPercentage)"/>
    <xsl:variable name="BeginToestand" select="$BTAll + $BTAanpas"/>
    <xsl:variable name="EindToestand" select="sum(AllBelasteReserves/*/EindToestand)"/>
    <xsl:variable name="Beweging" select="$EindToestand - $BeginToestand"/>
    <table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" columns="8" width="100%" align="Center" widths="45;30;1;30;1;30;1;30" cellspacing="0" >
      <row>
        <cell verticalalign="middle" horizontalalign="left">
          <phrase lineheight="{$WorksheetSpacing}" fontstyle="bold" size="{$WorksheetLabelFontsize}">
            <xsl:value-of select="Translations:GetTranslation('BelasteReserves','Totaal','Totaal')"/>
          </phrase>
        </cell>
        <cell verticalalign="middle" horizontalalign="right">
          <phrase lineheight="{$WorksheetSpacing}" fontstyle="bold" size="{$WorksheetNumbersize}">
            <xsl:value-of select="format-number($BeginToestand,$NumberFormatting, 'euro')"/>€
          </phrase>
        </cell>
        <cell>
          <phrase>
            <space/>
          </phrase>
        </cell>
        <cell verticalalign="middle" horizontalalign="right">
          <phrase lineheight="{$WorksheetSpacing}" fontstyle="bold" size="{$WorksheetNumbersize}">
            <xsl:value-of select="format-number($EindToestand,$NumberFormatting, 'euro')"/>€
          </phrase>
        </cell>
        <cell>
          <phrase>
            <space/>
          </phrase>
        </cell>
        <cell verticalalign="middle" horizontalalign="right">
          <phrase>
            <space/>
          </phrase>
        </cell>
        <cell>
          <phrase>
            <space/>
          </phrase>
        </cell>
        <cell verticalalign="middle" horizontalalign="right">
          <phrase>
            <space/>
          </phrase>
        </cell>
      </row>
      <row>
        <cell colspan="8">
          <phrase>
            <space/>
          </phrase>
        </cell>
      </row>
      <row>
        <cell verticalalign="middle" horizontalalign="left" colspan="2">
          <phrase lineheight="{$WorksheetSpacing}" fontstyle="bold" size="{$WorksheetLabelFontsize}">
            <xsl:value-of select="Translations:GetTranslation('BelasteReserves','Beweging','Beweging')"/>
          </phrase>
        </cell>
        <cell>
          <phrase>
            <space/>
          </phrase>
        </cell>
        <cell verticalalign="middle" horizontalalign="right">
          <phrase lineheight="{$WorksheetSpacing}" fontstyle="bold" size="{$WorksheetNumbersize}">
            <xsl:value-of select="format-number($Beweging,$NumberFormatting, 'euro')"/>€
          </phrase>
        </cell>
        <cell>
          <phrase>
            <space/>
          </phrase>
        </cell>
        <cell>
          <phrase>
            <space/>
          </phrase>
        </cell>
        <cell>
          <phrase>
            <space/>
          </phrase>
        </cell>
        <cell>
          <phrase>
            <space/>
          </phrase>
        </cell>
      </row>
    </table>
  </xsl:template>
</xsl:stylesheet>