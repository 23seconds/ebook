<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" exclude-result-prefixes="Helper Translations File Client" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:Helper="urn:Helper" xmlns:Translations="urn:Translations" xmlns:File="urn:File" xmlns:Client="urn:Client">
  <xsl:decimal-format name="NonNumber" NaN="0" zero-digit="0"/>
  <xsl:variable name="WorksheetLabelFontsize" select="string('10')"/>
  <xsl:variable name="WorksheetFontsize" select="string('10')"/>
  <xsl:variable name="WorksheetNumbersize" select="string('8')"/>
  <xsl:variable name="WorksheetSmallFontsize" select="string('8')"/>
  <xsl:variable name="WorksheetTotalsFontsize" select="string('10')"/>
  <xsl:variable name="WorksheetSpacing" select="1.1"/>
  <xsl:decimal-format name="euro" decimal-separator="," grouping-separator="."/>
  <xsl:variable name="NumberFormatting" select="string('#.##0,00')"/>
  <xsl:variable name="NumberFormattingNoDigit" select="string('#.##0')"/>
  <xsl:variable name="FONT" select="string('C:\WINDOWS\Fonts\EYInterstate-Light.ttf')"/>
  <xsl:param name="RuleAppDef"/>
  <xsl:param name="Culture"/>
  <xsl:template match="TransferTaxCredits">
    <Root>
      <xsl:if test="count(NID/*)>0">
        <xsl:call-template name="RENDER_NID">
          <xsl:with-param name="nids" select="NID"/>
        </xsl:call-template>
        <paragraph>&#160;</paragraph>
      </xsl:if>
      <xsl:if test="not(sum(FiscaleVerliezen/*[Type/text()='eindeverlies']/Cumul[number()=number()])=0)">
        <xsl:call-template name="RENDER_VERLIES">
          <xsl:with-param name="verliezen" select="FiscaleVerliezen"/>
        </xsl:call-template>
        <paragraph>&#160;</paragraph>
      </xsl:if>
      <xsl:if test="count(DBI/*[Aanslagjaar/id/text()=File:GetValue('AssessmentYear','')])>0">
        <xsl:call-template name="RENDER_DBI">
          <xsl:with-param name="dbis" select="DBI"/>
        </xsl:call-template>
        <paragraph>&#160;</paragraph>
      </xsl:if>
      <xsl:variable name="ic_1" select="Helper:AddSum('investCheck',Helper:CheckNaNAbsolute(Invest/*[AutoKey/text()='investhuidig']/Totaal))"/>
      <xsl:variable name="ic_2" select="Helper:AddSum('investCheck',Helper:CheckNaNAbsolute(Invest/*[AutoKey/text()='aanwendinginvest']/Totaal))"/>
      <xsl:variable name="ic_3" select="Helper:AddSum('investCheck',Helper:CheckNaNAbsolute(Invest/*[AutoKey/text()='saldostart']/Totaal))"/>
      <xsl:variable name="ic_4" select="Helper:AddSum('investCheck',Helper:CheckNaNAbsolute(Invest/*[AutoKey/text()='investeinde']/Totaal))"/>
      <xsl:if test="Helper:AddSum('investCheck',0)>0">
        <xsl:call-template name="RENDER_INVEST">
          <xsl:with-param name="invests" select="Invest"/>
        </xsl:call-template>
        <paragraph>&#160;</paragraph>
      </xsl:if>
      <xsl:if test="count(Belastingkrediet/*)>0">
        <xsl:call-template name="RENDER_BK">
          <xsl:with-param name="bks" select="Belastingkrediet"/>
        </xsl:call-template>
        <paragraph>&#160;</paragraph>
      </xsl:if>
    </Root>
  </xsl:template>
  <xsl:template name="RENDER_NID">
    <xsl:param name="nids"/>
    <table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" columns="4" width="100%" align="Center" widths="25;25;25;25" cellspacing="0">
      <row>
        <cell verticalalign="top" header="true" colspan="4" horizontalalign="left">
          <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}" fontstyle="bold underline">
            <xsl:value-of select="Translations:GetTranslation('TransferTaxCredits','NID','NID')"/>
          </phrase>
          <newline/>
        </cell>
      </row>
      <row>
        <cell verticalalign="top" header="true" horizontalalign="left">
          <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}" fontstyle="bold underline">
            <xsl:value-of select="Translations:GetTranslation('TransferTaxCredits','Aanslagjaar','Aanslagjaar')"/>
          </phrase>
        </cell>
        <cell verticalalign="top" header="true" horizontalalign="right">
          <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}" fontstyle="bold underline">
            <xsl:value-of select="Translations:GetTranslation('TransferTaxCredits','SaldoOp','SaldoOp')"/>&#160;<xsl:value-of select="File:GetValue('PreviousEndDate','dd/MM/yyyy')"/>
          </phrase>
        </cell>
        <cell verticalalign="top" header="true" horizontalalign="right">
          <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}" fontstyle="bold underline">
            <xsl:value-of select="Translations:GetTranslation('TransferTaxCredits','GebruiktTijdensBJ','GebruiktTijdensBJ')"/>
          </phrase>
        </cell>
        <cell verticalalign="top" header="true" horizontalalign="right">
          <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}" fontstyle="bold underline">
            <xsl:value-of select="Translations:GetTranslation('TransferTaxCredits','SaldoOp','SaldoOp')"/>&#160;<xsl:value-of select="File:GetValue('EndDate','dd/MM/yyyy')"/>
          </phrase>
        </cell>
      </row>
      <row>
        <cell verticalalign="top" header="true" colspan="4" horizontalalign="left">
          <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}">&#160;</phrase>
        </cell>
      </row>
      <xsl:for-each select="$nids/NIDItem">
        <row>
          <cell verticalalign="top" horizontalalign="left">
            <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
              <xsl:value-of select="Aanslagjaar"/>
            </phrase>
          </cell>
          <cell verticalalign="top" horizontalalign="right">
            <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
              <xsl:value-of select="format-number(sum(BedragBeginBJ[number()=number()]),$NumberFormatting,'euro')"/>€
            </phrase>
          </cell>
          <cell verticalalign="top" horizontalalign="right">
            <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
              <xsl:value-of select="format-number(sum(Aanwending[number()=number()]),$NumberFormatting,'euro')"/>€
            </phrase>
          </cell>
          <cell verticalalign="top" horizontalalign="right">
            <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
              <xsl:value-of select="format-number(sum(BedragEindeBJ[number()=number()]),$NumberFormatting,'euro')"/>€
            </phrase>
          </cell>
        </row>
      </xsl:for-each>
      <row>
        <cell verticalalign="top" horizontalalign="left">
          <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" fontstyle="bold">
            <xsl:value-of select="Translations:GetTranslation('TransferTaxCredits','Totaal','Totaal')"/>
          </phrase>
        </cell>
        <cell verticalalign="top" horizontalalign="right">
          <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" fontstyle="bold">
            <xsl:value-of select="format-number(sum($nids/*/BedragBeginBJ[number()=number()]),$NumberFormatting,'euro')"/>€
          </phrase>
        </cell>
        <cell verticalalign="top" horizontalalign="right">
          <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" fontstyle="bold">
            <xsl:value-of select="format-number(sum($nids/*/Aanwending[number()=number()]),$NumberFormatting,'euro')"/>€
          </phrase>
        </cell>
        <cell verticalalign="top" horizontalalign="right">
          <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" fontstyle="bold">
            <xsl:value-of select="format-number(sum($nids/*/BedragEindeBJ[number()=number()]),$NumberFormatting,'euro')"/>€
          </phrase>
        </cell>
      </row>
    </table>
  </xsl:template>
  <xsl:template name="RENDER_BK">
    <xsl:param name="bks"/>
    <table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" columns="4" width="100%" align="Center" widths="25;25;25;25" cellspacing="0">
      <row>
        <cell verticalalign="top" header="true" colspan="4" horizontalalign="left">
          <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}" fontstyle="bold underline">
            <xsl:value-of select="Translations:GetTranslation('TransferTaxCredits','Belastingkrediet','Belastingkrediet')"/>
          </phrase>
          <newline/>
        </cell>
      </row>
      <row>
        <cell verticalalign="top" header="true" horizontalalign="left">
          <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}" fontstyle="bold underline">
            <xsl:value-of select="Translations:GetTranslation('TransferTaxCredits','Aanslagjaar','Aanslagjaar')"/>
          </phrase>
        </cell>
        <cell verticalalign="top" header="true" horizontalalign="left">
          <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}" fontstyle="bold underline">
            <xsl:value-of select="Translations:GetTranslation('TransferTaxCredits','SaldoOp','SaldoOp')"/>&#160;<xsl:value-of select="File:GetValue('PreviousEndDate','dd/MM/yyyy')"/>
          </phrase>
        </cell>
        <cell verticalalign="top" header="true" horizontalalign="left">
          <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}" fontstyle="bold underline">
            <xsl:value-of select="Translations:GetTranslation('TransferTaxCredits','GebruiktTijdensBJ','GebruiktTijdensBJ')"/>
          </phrase>
        </cell>
        <cell verticalalign="top" header="true" horizontalalign="left">
          <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}" fontstyle="bold underline">
            <xsl:value-of select="Translations:GetTranslation('TransferTaxCredits','SaldoOp','SaldoOp')"/>&#160;<xsl:value-of select="File:GetValue('EndDate','dd/MM/yyyy')"/>
          </phrase>
        </cell>
      </row>
      <xsl:for-each select="$bks">
        <row>
          <cell verticalalign="top" horizontalalign="left">
            <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
              <xsl:value-of select="Aanslagjaar/nl"/>
            </phrase>
          </cell>
          <cell verticalalign="top" horizontalalign="right">
            <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
              <xsl:value-of select="format-number(sum(BedragAftrek[number()=number()]),$NumberFormatting,'euro')"/>€
            </phrase>
          </cell>
          <cell verticalalign="top" horizontalalign="right">
            <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
              <xsl:value-of select="format-number(sum(GebruiktHuidigBJ[number()=number()]),$NumberFormatting,'euro')"/>€
            </phrase>
          </cell>
          <cell verticalalign="top" horizontalalign="right">
            <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
              <xsl:value-of select="format-number(sum(Saldo[number()=number()]),$NumberFormatting,'euro')"/>€
            </phrase>
          </cell>
        </row>
      </xsl:for-each>
      <row>
        <cell verticalalign="top" horizontalalign="left">
          <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" fontstyle="bold">
            <xsl:value-of select="Translations:GetTranslation('TransferTaxCredits','Totaal','Totaal')"/>
          </phrase>
        </cell>
        <cell verticalalign="top" horizontalalign="right">
          <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" fontstyle="bold">
            <xsl:value-of select="format-number(sum($bks/BedragAftrek[number()=number()]),$NumberFormatting,'euro')"/>€
          </phrase>
        </cell>
        <cell verticalalign="top" horizontalalign="right">
          <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" fontstyle="bold">
            <xsl:value-of select="format-number(sum($bks/GebruiktHuidigBJ[number()=number()]),$NumberFormatting,'euro')"/>€
          </phrase>
        </cell>
        <cell verticalalign="top" horizontalalign="right">
          <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" fontstyle="bold">
            <xsl:value-of select="format-number(sum($bks/BedragEindeBJ[number()=number()]),$NumberFormatting,'euro')"/>€
          </phrase>
        </cell>
      </row>
    </table>
  </xsl:template>
  <xsl:template name="RENDER_VERLIES">
    <xsl:param name="verliezen"/>
    <table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" columns="2" width="100%" align="Center" widths="60;40" cellspacing="0" left="false" right="false" top="false" bottom="false">
      <row>
        <cell verticalalign="top" header="true" colspan="2" horizontalalign="left">
          <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}" fontstyle="bold underline">
            <xsl:value-of select="Translations:GetTranslation('TransferTaxCredits','FiscaleVerliezen','FiscaleVerliezen')"/>
          </phrase>
          <newline/>
        </cell>
      </row>
      <row>
        <cell verticalalign="top" horizontalalign="left">
          <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}">
            <xsl:value-of select="Translations:GetTranslation('TransferTaxCredits','SituatiePer','SituatiePer')"/>&#160;<xsl:value-of select="File:GetValue('PreviousEndDate','dd/MM/yyyy')"/>
          </phrase>
        </cell>
        <cell verticalalign="top" horizontalalign="right">
          <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
            <xsl:value-of select="format-number(sum($verliezen/*[Type/text()='saldostart']/Cumul[number()=number()]),$NumberFormatting,'euro')"/>€
          </phrase>
        </cell>
      </row>
      <row>
        <cell verticalalign="top" horizontalalign="left">
          <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}">
            <xsl:value-of select="Translations:GetTranslation('TransferTaxCredits','VerliesAJ','VerliesAJ')"/>&#160;<xsl:value-of select="File:GetValue('AssessmentYear','')"/>
          </phrase>
        </cell>
        <cell verticalalign="top" horizontalalign="right">
          <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
            <xsl:value-of select="format-number(sum($verliezen/*[Type/text()='verlieshuidig']/Cumul[number()=number()]),$NumberFormatting,'euro')"/>€
          </phrase>
        </cell>
      </row>
      <row>
        <cell verticalalign="top" horizontalalign="left">
          <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}">
            <xsl:value-of select="Translations:GetTranslation('TransferTaxCredits','AanwendingAJ','AanwendingAJ')"/>&#160;<xsl:value-of select="File:GetValue('AssessmentYear','')"/>
          </phrase>
        </cell>
        <cell verticalalign="top" horizontalalign="right">
          <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
            <xsl:value-of select="format-number(sum($verliezen/*[Type/text()='compenseerdverlieshuidig']/Cumul[number()=number()]),$NumberFormatting,'euro')"/>€
          </phrase>
        </cell>
      </row>
      <row>
        <cell verticalalign="top" horizontalalign="left">
          <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}">
            <xsl:value-of select="Translations:GetTranslation('TransferTaxCredits','SituatiePer','SituatiePer')"/>&#160;<xsl:value-of select="File:GetValue('EndDate','dd/MM/yyyy')"/>
          </phrase>
        </cell>
        <cell verticalalign="top" horizontalalign="right">
          <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
            <xsl:value-of select="format-number(sum($verliezen/*[Type/text()='eindeverlies']/Cumul[number()=number()]),$NumberFormatting,'euro')"/>€
          </phrase>
        </cell>
      </row>
    </table>
  </xsl:template>
  <xsl:template name="RENDER_DBI">
    <xsl:param name="dbis"/>
    <table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" columns="2" width="100%" align="Center" widths="60;40" cellspacing="0" left="false" right="false" top="false" bottom="false">
      <row>
        <cell verticalalign="top" header="true" colspan="2" horizontalalign="left">
          <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}" fontstyle="bold underline">
            <xsl:value-of select="Translations:GetTranslation('TransferTaxCredits','DBI','FiscaleVerliezen')"/>
          </phrase>
          <newline/>
        </cell>
      </row>
      <row>
        <cell verticalalign="top" horizontalalign="left">
          <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}">
            <xsl:value-of select="Translations:GetTranslation('TransferTaxCredits','SituatiePer','SituatiePer')"/>&#160;<xsl:value-of select="File:GetValue('PreviousEndDate','dd/MM/yyyy')"/>
          </phrase>
        </cell>
        <cell verticalalign="top" horizontalalign="right">
          <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
            <xsl:value-of select="format-number(sum($dbis[Aanslagjaar/id/text()=File:GetValue('AssessmentYear','')]/SaldoStart[number()=number()]),$NumberFormatting,'euro')"/>€
          </phrase>
        </cell>
      </row>
      <row>
        <cell verticalalign="top" horizontalalign="left">
          <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}">
            <xsl:value-of select="Translations:GetTranslation('TransferTaxCredits','AanwendingAJ','AanwendingAJ')"/>&#160;<xsl:value-of select="File:GetValue('AssessmentYear','')"/>
          </phrase>
        </cell>
        <cell verticalalign="top" horizontalalign="right">
          <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
            <xsl:value-of select="format-number(sum($dbis[Aanslagjaar/id/text()=File:GetValue('AssessmentYear','')]/GebruiktBJ[number()=number()]),$NumberFormatting,'euro')"/>€
          </phrase>
        </cell>
      </row>
      <row>
        <cell verticalalign="top" horizontalalign="left">
          <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}">
            <xsl:value-of select="Translations:GetTranslation('TransferTaxCredits','VerliesAJ','VerliesAJ')"/>&#160;<xsl:value-of select="File:GetValue('AssessmentYear','')"/>
          </phrase>
        </cell>
        <cell verticalalign="top" horizontalalign="right">
          <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
            <xsl:value-of select="format-number(sum($dbis[Aanslagjaar/id/text()=File:GetValue('AssessmentYear','')]/SaldoEinde[number()=number()]),$NumberFormatting,'euro')"/>€
          </phrase>
        </cell>
      </row>
    </table>
  </xsl:template>
  <xsl:template name="RENDER_INVEST">
    <xsl:param name="invests"/>
    <table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" columns="2" width="100%" align="Center" widths="60;40" cellspacing="0" left="false" right="false" top="false" bottom="false">
      <row>
        <cell verticalalign="top" header="true" colspan="2" horizontalalign="left">
          <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}" fontstyle="bold underline">
            <xsl:value-of select="Translations:GetTranslation('TransferTaxCredits','Invest','Invest')"/>
          </phrase>
          <newline/>
        </cell>
      </row>
      <row>
        <cell verticalalign="top" horizontalalign="left">
          <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}">
            <xsl:value-of select="Translations:GetTranslation('TransferTaxCredits','SituatiePer','SituatiePer')"/>&#160;<xsl:value-of select="File:GetValue('PreviousEndDate','dd/MM/yyyy')"/>
          </phrase>
        </cell>
        <cell verticalalign="top" horizontalalign="right">
          <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
            <xsl:value-of select="format-number(Helper:CheckNaN($invests/*[AutoKey/text()='saldostart']/Totaal),$NumberFormatting,'euro')"/>€
          </phrase>
        </cell>
      </row>
      <row>
        <cell verticalalign="top" horizontalalign="left">
          <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}">
            <xsl:value-of select="Translations:GetTranslation('TransferTaxCredits','AanwendingAJ','AanwendingAJ')"/>&#160;<xsl:value-of select="File:GetValue('AssessmentYear','')"/>
          </phrase>
        </cell>
        <cell verticalalign="top" horizontalalign="right">
          <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
            <xsl:value-of select="format-number(Helper:CheckNaN($invests/*[AutoKey/text()='aanwendinginvest']/Totaal),$NumberFormatting,'euro')"/>€
          </phrase>
        </cell>
      </row>
      <row>
        <cell verticalalign="top" horizontalalign="left">
          <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}">
            <xsl:value-of select="Translations:GetTranslation('TransferTaxCredits','ToevoegingAJ','ToevoegingAJ')"/>&#160;<xsl:value-of select="File:GetValue('AssessmentYear','')"/>
          </phrase>
        </cell>
        <cell verticalalign="top" horizontalalign="right">
          <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
            <xsl:value-of select="format-number(Helper:CheckNaN($invests/*[AutoKey/text()='investhuidig']/Totaal),$NumberFormatting,'euro')"/>€
          </phrase>
        </cell>
      </row>
      <row>
        <cell verticalalign="top" horizontalalign="left">
          <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}">
            <xsl:value-of select="Translations:GetTranslation('TransferTaxCredits','SituatiePer','SituatiePer')"/>&#160;<xsl:value-of select="File:GetValue('EndDate','dd/MM/yyyy')"/>
          </phrase>
        </cell>
        <cell verticalalign="top" horizontalalign="right">
          <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
            <xsl:value-of select="format-number(sum($invests/*[AutoKey/text()='investeinde']/Totaal[number()=number()]),$NumberFormatting,'euro')"/>€
          </phrase>
        </cell>
      </row>
    </table>
  </xsl:template>
</xsl:stylesheet>