<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" exclude-result-prefixes="Helper Translations File Client" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:Helper="urn:Helper" xmlns:Translations="urn:Translations" xmlns:File="urn:File" xmlns:Client="urn:Client">
  <xsl:decimal-format name="NonNumber" NaN="0" zero-digit="0"/>
  <xsl:variable name="WorksheetLabelFontsize" select="string('10')"/>
  <xsl:variable name="WorksheetFontsize" select="string('8')"/>
  <xsl:variable name="WorksheetNumbersize" select="string('6')"/>
  <xsl:variable name="WorksheetSmallFontsize" select="string('8')"/>
  <xsl:variable name="WorksheetTotalsFontsize" select="string('10')"/>
  <xsl:variable name="WorksheetSpacing" select="1.3"/>
  <xsl:decimal-format name="euro" decimal-separator="," grouping-separator="."/>
  <xsl:variable name="NumberFormatting" select="string('#.##0,00')"/>
  <xsl:variable name="NumberFormattingNoDigit" select="string('#.##0')"/>
  <xsl:variable name="FONT" select="string('C:\WINDOWS\Fonts\EYInterstate-Light.ttf')"/>
  <xsl:param name="RuleAppDef"/>
  <xsl:param name="Culture"/>
  <xsl:template match="BerekeningVenB">
    <Root>
      <xsl:apply-templates select="*"/>
    </Root>
  </xsl:template>
 
  <xsl:template match="Overzicht">
    <table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" width="100%" align="Center" cellspacing="0" columns="4" widths="64;12;12;12">
      <row>
        <cell colspan="4">
          <paragraph style="text-decoration:underline;">
            <xsl:value-of select="Translations:GetTranslation('BerekeningVenB','Overzicht','Overzicht')"></xsl:value-of>
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="4">
          <paragraph>
            <space></space>
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="10">
            <xsl:value-of select="Translations:GetTranslation('General','GereserveerdeWinst','GereserveerdeWinst')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="1">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(GereserveerdeWinst[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
        <cell colspan="1">
          <paragraph>
            <space></space>
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="10">
            <xsl:value-of select="Translations:GetTranslation('General','Dividenden','Dividenden')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="1">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(Dividenden[number()=number()][number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
        <cell colspan="1">
          <paragraph>
            <space></space>
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="10">
            <xsl:value-of select="Translations:GetTranslation('General','VerkrijgingEigenAandelen','VerkrijgingEigenAandelen')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="1">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(VerkrijgingEigenAandelen[number()=number()][number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
        <cell colspan="1">
          <paragraph>
            <space></space>
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="10">
            <xsl:value-of select="Translations:GetTranslation('General','OverlijdenUittredingUitsluitingVennoot','OverlijdenUittredingUitsluitingVennoot')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="1">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(OverlijdenUittredingUitsluitingVennoot[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
        <cell colspan="1">
          <paragraph>
            <space></space>
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="10">
            <xsl:value-of select="Translations:GetTranslation('General','VerdelingMaatschappelijkVermogen','VerdelingMaatschappelijkVermogen')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="1">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(VerdelingMaatschappelijkVermogen[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
        <cell colspan="1">
          <paragraph>
            <space></space>
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="3">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="text-decoration:underline;">
            <xsl:value-of select="Translations:GetTranslation('General','StatAccountingResult','StatAccountingResult')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="1">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="text-decoration:underline;">
            <xsl:value-of select="format-number(sum(StatAccountingResult[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="4">
          <paragraph>
            <space></space>
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="10">
            <xsl:value-of select="Translations:GetTranslation('General','TotalBelasteReserves','TotalBelasteReserves')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="1">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(TotalBelasteReserves[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
        <cell colspan="1">
          <paragraph>
            <space/>
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="10">
            <xsl:value-of select="Translations:GetTranslation('General','TotalAanpassingenBelasteReserves','TotalAanpassingenBelasteReserves')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="1">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(TotalAanpassingenBelasteReserves[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
        <cell colspan="1">
          <paragraph>
            <space/>
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="10">
            <xsl:value-of select="Translations:GetTranslation('General','NietAftrekbareBelastingen','NietAftrekbareBelastingen')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="1">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(NietAftrekbareBelastingen[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
        <cell colspan="1">
          <paragraph>
            <space/>
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="10">
            <xsl:value-of select="Translations:GetTranslation('General','TotaalVerworpenUitgaven','TotaalVerworpenUitgaven')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="1">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(TotaalVerworpenUitgaven[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
        <cell colspan="1">
          <paragraph>
            <space/>
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="3">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="text-decoration:underline;">
            <xsl:value-of select="Translations:GetTranslation('General','BelastbaarResultaat','BelastbaarResultaat')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="1">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="text-decoration:underline;">
            <xsl:value-of select="format-number(sum(BelastbaarResultaat[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="4">
          <paragraph>
            <space></space>
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="10">
            <xsl:value-of select="Translations:GetTranslation('General','TotaalBelastbareBestandenZonderAftrekMin','TotaalBelastbareBestandenZonderAftrekMin')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="1">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(TotaalBelastbareBestandenZonderAftrekMin[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
        <cell colspan="1">
          <paragraph>
            <space/>
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="3">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="text-decoration:underline;">
            <xsl:value-of select="Translations:GetTranslation('General','SubTotaalBelastbaarVoorAftrek','SubTotaalBelastbaarVoorAftrek')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="1">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="text-decoration:underline;">
            <xsl:value-of select="format-number(sum(SubTotaalBelastbaarVoorAftrek[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="4">
          <paragraph>
            <space></space>
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="10">
            <xsl:value-of select="Translations:GetTranslation('General','OpdelingNaarOorsprongBijVerdragVrijgesteldInkomen','OpdelingNaarOorsprongBijVerdragVrijgesteldInkomen')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="1">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(OpdelingNaarOorsprongBijVerdragVrijgesteldInkomen[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
        <cell colspan="1">
          <paragraph>
            <space/>
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="10">
            <xsl:value-of select="Translations:GetTranslation('General','TotaalFiscaleAftrek','TotaalFiscaleAftrek')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="1">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(TotaalFiscaleAftrek[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
        <cell colspan="1">
          <paragraph>
            <space/>
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="3">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="text-decoration:underline;">
            <xsl:value-of select="Translations:GetTranslation('General','SubTotaalNaAftrek','SubTotaalNaAftrek')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="1">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="text-decoration:underline;">
            <xsl:value-of select="format-number(sum(SubTotaalNaAftrek[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="4">
          <paragraph>
            <space></space>
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="10">
            <xsl:value-of select="Translations:GetTranslation('General','TotaalBelastbareBestandenZonderAftrekPlus','TotaalBelastbareBestandenZonderAftrekPlus')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="1">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(TotaalBelastbareBestandenZonderAftrekPlus[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
        <cell colspan="1">
          <paragraph>
            <space/>
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="3">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="font-weight:bold;text-decoration:underline;">
            <xsl:value-of select="Translations:GetTranslation('General','BelastbareGrondslag','BelastbareGrondslag')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="1">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="font-weight:bold;text-decoration:underline;">
            <xsl:value-of select="format-number(sum(BelastbareGrondslag[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
    </table>
    <newpage/>
  </xsl:template>
  <xsl:template match="BelasteReserves">
    <table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" width="100%" align="Center" cellspacing="0" columns="4" widths="64;12;12;12">
      <row>
        <cell colspan="4">
          <paragraph style="text-decoration:underline;">
            <xsl:value-of select="Translations:GetTranslation('BerekeningVenB','BelasteReserves','BelasteReserves')"></xsl:value-of>
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="4">
          <paragraph>
            <space></space>
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="4">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="underline">
            <xsl:value-of select="Translations:GetTranslation('BelasteReservesItem','TempDiff','TempDiff')"></xsl:value-of>
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="10">
            <xsl:value-of select="Translations:GetTranslation('BelasteReservesItem','TDKapitaalEnUitgifte','TDKapitaalEnUitgifte')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(TDKapitaalEnUitgifte[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="10">
            <xsl:value-of select="Translations:GetTranslation('BelasteReservesItem','TDHerwaardering','TDHerwaardering')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(TDHerwaardering[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="10">
            <xsl:value-of select="Translations:GetTranslation('BelasteReservesItem','TDTaxableProvisions','TDTaxableProvisions')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(TDTaxableProvisions[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="10">
            <xsl:value-of select="Translations:GetTranslation('BelasteReservesItem','TDOtherTaxableReserves','TDOtherTaxableReserves')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(TDOtherTaxableReserves[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="10">
            <xsl:value-of select="Translations:GetTranslation('BelasteReservesItem','TDTaxableDoubtfullDebtors','TDTaxableDoubtfullDebtors')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(TDTaxableDoubtfullDebtors[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="10">
            <xsl:value-of select="Translations:GetTranslation('BelasteReservesItem','TDExcessDeprications','TDExcessDeprications')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(TDExcessDeprications[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="10">
            <xsl:value-of select="Translations:GetTranslation('BelasteReservesItem','TDUnderestimationAssetsOverestimationLiabilities','TDUnderestimationAssetsOverestimationLiabilities')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(TDUnderestimationAssetsOverestimationLiabilities[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="10">
            <xsl:value-of select="Translations:GetTranslation('BelasteReservesItem','TDOverestimationLiabilities','TDOverestimationLiabilities')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(TDOverestimationLiabilities[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="3">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="bold" indentationleft="10">
            <xsl:value-of select="Translations:GetTranslation('BelasteReservesItem','TDTotal','TDTotal')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="1">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="bold">
            <xsl:value-of select="format-number(sum(TDTotal[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="4">
          <paragraph>
            <space/>
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="4">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="text-decoration:underline;">
            <xsl:value-of select="Translations:GetTranslation('BelasteReservesItem','OhterAdjustments','OhterAdjustments')"></xsl:value-of>
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="10">
            <xsl:value-of select="Translations:GetTranslation('BelasteReservesItem','OACapitalGainsOnShares','OACapitalGainsOnShares')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(OACapitalGainsOnShares[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="10">
            <xsl:value-of select="Translations:GetTranslation('BelasteReservesItem','OADefinitiveExemptionTaxShelter','OADefinitiveExemptionTaxShelter')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(OADefinitiveExemptionTaxShelter[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="10">
            <xsl:value-of select="Translations:GetTranslation('BelasteReservesItem','OAExemptionRegionalPrem','OAExemptionRegionalPrem')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(OAExemptionRegionalPrem[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="10">
            <xsl:value-of select="Translations:GetTranslation('BelasteReservesItem','OAOthers','OAOthers')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(OAOthers[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="3">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="font-weight:bold;" indentationleft="10">
            <xsl:value-of select="Translations:GetTranslation('BelasteReservesItem','OATotal','OATotal')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="1">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="font-weight:bold;">
            <xsl:value-of select="format-number(sum(OATotal[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
    </table>
    <newpage></newpage>
  </xsl:template>
  <xsl:template match="VerworpenUitgaven">
    <table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" width="100%" align="Center" cellspacing="0" columns="4" widths="64;12;12;12">
      <row>
        <cell colspan="4" style="underline">
          <paragraph>
            <xsl:value-of select="Translations:GetTranslation('BerekeningVenB','VerworpenUitgaven','VerworpenUitgaven')"></xsl:value-of>
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="4">
          <paragraph>
            <space></space>
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="4">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="font-weight:bold">
            <xsl:value-of select="DisallowedExpenses"></xsl:value-of>
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="3">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="font-weight:bold" indentationleft="10">
            <xsl:value-of select="Translations:GetTranslation('VerworpenUitgavenItem','PDDisallowedCurrentIncomeTaxExpenses','PDDisallowedCurrentIncomeTaxExpenses')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="1">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="font-weight:bold">
            <xsl:value-of select="format-number(sum(PDDisallowedCurrentIncomeTaxExpenses[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="4">
          <paragraph>
            <space></space>
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="20">
            <xsl:value-of select="Translations:GetTranslation('VerworpenUitgavenItem','DERegionalTaxes','DERegionalTaxes')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(DERegionalTaxes[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="20">
            <xsl:value-of select="Translations:GetTranslation('VerworpenUitgavenItem','DEPenalties','DEPenalties')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(DEPenalties[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="20">
            <xsl:value-of select="Translations:GetTranslation('VerworpenUitgavenItem','DEPensions','DEPensions')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(DEPensions[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="20">
            <xsl:value-of select="Translations:GetTranslation('VerworpenUitgavenItem','DECar','DECar')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(DECar[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="20">
            <xsl:value-of select="Translations:GetTranslation('VerworpenUitgavenItem','DECarVAA','DECarVAA')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(DECarVAA[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="20">
            <xsl:value-of select="Translations:GetTranslation('VerworpenUitgavenItem','DEReception','DEReception')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(DEReception[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="20">
            <xsl:value-of select="Translations:GetTranslation('VerworpenUitgavenItem','DERestaurant','DERestaurant')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(DERestaurant[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="20">
            <xsl:value-of select="Translations:GetTranslation('VerworpenUitgavenItem','DEClothes','DEClothes')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(DEClothes[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="20">
            <xsl:value-of select="Translations:GetTranslation('VerworpenUitgavenItem','DEExcessInterest','DEExcessInterest')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(DEExcessInterest[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="20">
            <xsl:value-of select="Translations:GetTranslation('VerworpenUitgavenItem','DEInterestLoans','DEInterestLoans')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(DEInterestLoans[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="20">
            <xsl:value-of select="Translations:GetTranslation('VerworpenUitgavenItem','DEAbnormalAdvantages','DEAbnormalAdvantages')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(DEAbnormalAdvantages[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="20">
            <xsl:value-of select="Translations:GetTranslation('VerworpenUitgavenItem','DESocial','DESocial')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(DESocial[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="20">
            <xsl:value-of select="Translations:GetTranslation('VerworpenUitgavenItem','DEDinerSportCultureEco','DEDinerSportCultureEco')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(DEDinerSportCultureEco[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="20">
            <xsl:value-of select="Translations:GetTranslation('VerworpenUitgavenItem','DECharity','DECharity')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(DECharity[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="20">
            <xsl:value-of select="Translations:GetTranslation('VerworpenUitgavenItem','DEReductionValueShares','DEReductionValueShares')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(DEReductionValueShares[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="20">
            <xsl:value-of select="Translations:GetTranslation('VerworpenUitgavenItem','DEReversalsPreviousTax','DEReversalsPreviousTax')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(DEReversalsPreviousTax[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="20">
            <xsl:value-of select="Translations:GetTranslation('VerworpenUitgavenItem','DEEmployeeCapital','DEEmployeeCapital')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(DEEmployeeCapital[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="20">
            <xsl:value-of select="Translations:GetTranslation('VerworpenUitgavenItem','DEIndemnityMissingCoupon','DEIndemnityMissingCoupon')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(DEIndemnityMissingCoupon[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="20">
            <xsl:value-of select="Translations:GetTranslation('VerworpenUitgavenItem','DEExpensesTaxShelter','DEExpensesTaxShelter')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(DEExpensesTaxShelter[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="20">
            <xsl:value-of select="Translations:GetTranslation('VerworpenUitgavenItem','DERegionalPremiumsCapital','DERegionalPremiumsCapital')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(DERegionalPremiumsCapital[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="20">
            <xsl:value-of select="Translations:GetTranslation('VerworpenUitgavenItem','DEBepaaldeStaten','DEBepaaldeStaten')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(DEBepaaldeStaten[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="20">
            <xsl:value-of select="Translations:GetTranslation('VerworpenUitgavenItem','DEOther','DEOther')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(DEOther[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="3">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="font-weight:bold">
            <xsl:value-of select="Translations:GetTranslation('VerworpenUitgavenItem','DETotal','DETotal')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="1">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="font-weight:bold">
            <xsl:value-of select="format-number(sum(DETotal[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
    </table>
    <newpage></newpage>
  </xsl:template>
  <xsl:template match="BelastbareBestZonderAftrek">
    <table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" width="100%" align="Center" cellspacing="0" columns="4" widths="64;12;12;12">
      <row>
        <cell colspan="4">
          <paragraph>
            <xsl:value-of select="Translations:GetTranslation('BerekeningVenB','BelastbareBestZonderAftrek','BelastbareBestZonderAftrek')"></xsl:value-of>
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="4">
          <paragraph>
            <space></space>
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="4">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="text-decoration:underline;">
            <xsl:value-of select="TaxableElementsWithoutDeductions"></xsl:value-of>
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="20">
            <xsl:value-of select="Translations:GetTranslation('BelastbareBestZonderAftrekItem','TEReceivedAbnormal','TEReceivedAbnormal')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(TEReceivedAbnormal[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="20">
            <xsl:value-of select="Translations:GetTranslation('BelastbareBestZonderAftrekItem','TEVaaCar','TEVaaCar')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(TEVaaCar[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="20">
            <xsl:value-of select="Translations:GetTranslation('BelastbareBestZonderAftrekItem','TETaxableInvestmentReserve','TETaxableInvestmentReserve')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(TETaxableInvestmentReserve[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="20">
            <xsl:value-of select="Translations:GetTranslation('BelastbareBestZonderAftrekItem','TEEmployeeParticipationCapital','TEEmployeeParticipationCapital')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(TEEmployeeParticipationCapital[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="3">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="font-weight:bold;" indentationleft="10">
            <xsl:value-of select="Translations:GetTranslation('BelastbareBestZonderAftrekItem','TETotal','TETotal')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="1">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="font-weight:bold;">
            <xsl:value-of select="format-number(sum(TETotal[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
    </table>
    <newpage></newpage>
  </xsl:template>
  <xsl:template match="OpdelingNaarOorsprong">
    <table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" width="100%" align="Center" cellspacing="0" columns="4" widths="64;12;12;12">
      <row>
        <cell colspan="4">
          <paragraph>
            <xsl:value-of select="Translations:GetTranslation('BerekeningVenB','OpdelingNaarOorsprong','OpdelingNaarOorsprong')"></xsl:value-of>
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="4">
          <paragraph>
            <space></space>
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="4">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="text-decoration:underline;">
            <xsl:value-of select="ItemizationResult"></xsl:value-of>
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="10">
            <xsl:value-of select="Translations:GetTranslation('OpdelingNaarOorsprongItem','IRBelgianTaxable','IRBelgianTaxable')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(IRBelgianTaxable[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="10">
            <xsl:value-of select="Translations:GetTranslation('OpdelingNaarOorsprongItem','IRNonTreatyForeignIncome','IRNonTreatyForeignIncome')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(IRNonTreatyForeignIncome[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="10">
            <xsl:value-of select="Translations:GetTranslation('OpdelingNaarOorsprongItem','IRTreatyForeignIncome','IRTreatyForeignIncome')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(IRTreatyForeignIncome[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="font-weight:bold;">
            <xsl:value-of select="Translations:GetTranslation('OpdelingNaarOorsprongItem','IRRemainingResult','IRRemainingResult')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell colspan="2" horizontalalign="right">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="font-weight:bold;">
            <xsl:value-of select="format-number(sum(IRRemainingResultBelgium[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
    </table>
    <newpage></newpage>
  </xsl:template>
  <xsl:template match="FiscaleAftrek">
    <table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" width="100%" align="Center" cellspacing="0" columns="4" widths="64;12;12;12">
      <row>
        <cell colspan="4">
          <paragraph>
            <xsl:value-of select="Translations:GetTranslation('BerekeningVenB','FiscaleAftrek','FiscaleAftrek')"></xsl:value-of>
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="4">
          <paragraph>
            <space></space>
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="4">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="text-decoration:underline;">
            <xsl:value-of select="NonTaxableElements"></xsl:value-of>
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="10">
            <xsl:value-of select="Translations:GetTranslation('FiscaleAftrekItem','NTEBECharity','NTEBECharity')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(NTEBECharity[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="10">
            <xsl:value-of select="Translations:GetTranslation('FiscaleAftrekItem','NTEBEPersonnel','NTEBEPersonnel')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(NTEBEPersonnel[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="10">
            <xsl:value-of select="Translations:GetTranslation('FiscaleAftrekItem','NTEBEPersonnelSME','NTEBEPersonnelSME')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(NTEBEPersonnelSME[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="10">
            <xsl:value-of select="Translations:GetTranslation('FiscaleAftrekItem','NTEBEInternalshipPremium','NTEBEInternalshipPremium')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(NTEBEInternalshipPremium[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="10">
            <xsl:value-of select="Translations:GetTranslation('FiscaleAftrekItem','NTEBEOthers','NTEBEOthers')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(NTEBEOthers[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="10">
            <xsl:value-of select="Translations:GetTranslation('FiscaleAftrekItem','TotalNonTaxableElements','TotalNonTaxableElements')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(TotalNonTaxableElements[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="4">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="text-decoration:underline;">
            <xsl:value-of select="ParticipationExemption"></xsl:value-of>
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="10">
            <xsl:value-of select="Translations:GetTranslation('FiscaleAftrekItem','PEBelgium','PEBelgium')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(PEBelgium[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="4">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="text-decoration:underline;">
            <xsl:value-of select="PatentIncomeDeduction"></xsl:value-of>
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="10">
            <xsl:value-of select="Translations:GetTranslation('FiscaleAftrekItem','PIDBelgium','PIDBelgium')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(PIDBelgium[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="4">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="text-decoration:underline;">
            <xsl:value-of select="NotionalIntrestDeduction"></xsl:value-of>
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="10">
            <xsl:value-of select="Translations:GetTranslation('FiscaleAftrekItem','NIDBelgium','NIDBelgium')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(NIDBelgium[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="4">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="text-decoration:underline;">
            <xsl:value-of select="DeductionTaxLosses"></xsl:value-of>
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="10">
            <xsl:value-of select="Translations:GetTranslation('FiscaleAftrekItem','DTLBelgium','DTLBelgium')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(DTLBelgium[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="4">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="text-decoration:underline;">
            <xsl:value-of select="InvestmentDeduction"></xsl:value-of>
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" indentationleft="10">
            <xsl:value-of select="Translations:GetTranslation('FiscaleAftrekItem','IDBelgium','IDBelgium')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(IDBelgium[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="font-weight:bold;" indentationleft="10">
            <xsl:value-of select="Translations:GetTranslation('FiscaleAftrekItem','TotalFiscaleAftrek','TotalFiscaleAftrek')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="font-weight:bold;">
            <xsl:value-of select="format-number(sum(TotalFiscaleAftrek[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
    </table>
    <newpage></newpage>
  </xsl:template>
  <xsl:template match="Tarief">
    <table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" width="100%" align="Center" cellspacing="5" cellpadding="0" columns="2" widths="10;90">
      <row>
        <cell colspan="2">
          <paragraph>
            <xsl:value-of select="Translations:GetTranslation('BerekeningVenB','Tarief','Tarief')"></xsl:value-of>
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph>
            <space></space>
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2" horizontalalign="right">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="font-weight:bold"></paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="1">
          <xsl:choose>
            <xsl:when test="BezitAandelenMeerDan50Percent='true'">
              <image id="checked"></image>
            </xsl:when>
            <xsl:otherwise>
              <image id="unchecked"></image>
            </xsl:otherwise>
          </xsl:choose>
        </cell>
        <cell colspan="1">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="">
            <xsl:value-of select="Translations:GetTranslation('TariefItem','BezitAandelenMeerDan50Percent','BezitAandelenMeerDan50Percent')"></xsl:value-of>
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="1">
          <xsl:choose>
            <xsl:when test="BezitAandelenDoorVennootschappen='true'">
              <image id="checked"></image>
            </xsl:when>
            <xsl:otherwise>
              <image id="unchecked"></image>
            </xsl:otherwise>
          </xsl:choose>
        </cell>
        <cell colspan="1">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="">
            <xsl:value-of select="Translations:GetTranslation('TariefItem','BezitAandelenDoorVennootschappen','BezitAandelenDoorVennootschappen')"></xsl:value-of>
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="1">
          <xsl:choose>
            <xsl:when test="UitgekeerdDividendHogerDan13='true'">
              <image id="checked"></image>
            </xsl:when>
            <xsl:otherwise>
              <image id="unchecked"></image>
            </xsl:otherwise>
          </xsl:choose>
        </cell>
        <cell colspan="1">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="">
            <xsl:value-of select="Translations:GetTranslation('TariefItem','UitgekeerdDividendHogerDan13','UitgekeerdDividendHogerDan13')"></xsl:value-of>
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="1">
          <xsl:choose>
            <xsl:when test="ToegekendeBezoldiging='true'">
              <image id="checked"></image>
            </xsl:when>
            <xsl:otherwise>
              <image id="unchecked"></image>
            </xsl:otherwise>
          </xsl:choose>
        </cell>
        <cell colspan="1">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="">
            <xsl:value-of select="Translations:GetTranslation('TariefItem','ToegekendeBezoldiging','ToegekendeBezoldiging')"></xsl:value-of>
          </paragraph>
        </cell>
      </row>
    </table>
    <newpage></newpage>
  </xsl:template>
  <xsl:template match="BelastingsBerekening">
    <table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" width="100%" align="Center" cellspacing="0" columns="5" widths="20;20;20;20;20">
      <!--row><cell colspan="5"><paragraph><xsl:value-of select="Translations:GetTranslation('BerekeningVenB','BelastingsBerekening','BelastingsBerekening')"></xsl:value-of></paragraph></cell></row-->
      <row>
        <cell colspan="5">
          <paragraph>
            <space></space>
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="5">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="font-weight:boldtext-decoration:underline;">
            <xsl:value-of select="LabelCALCULATIONOTCORPORATEINCOMETAX"></xsl:value-of>
          </paragraph>
        </cell>
      </row>
      <!--row><cell colspan="4"><paragraph lineheight="1.3" size="{$WorksheetFontsize}" style=""><xsl:value-of select="Translations:GetTranslation('BelastingsBerekeningItem','AssessmentYear','AssessmentYear')"></xsl:value-of></paragraph></cell><cell colspan="1"><paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style=""><xsl:value-of select="AssessmentYear"></xsl:value-of></paragraph></cell></row-->
      <row>
        <cell colspan="4">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="text-decoration:underline;">
            <xsl:value-of select="Translations:GetTranslation('BelastingsBerekeningItem','CorporateIncomeTaxFromResult','CorporateIncomeTaxFromResult')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="1">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="text-decoration:underline;">
            <xsl:value-of select="format-number(sum(CorporateIncomeTaxFromResult[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="1">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="">
            <xsl:value-of select="Translations:GetTranslation('BelastingsBerekeningItem','CapitalGainsShares25','CapitalGainsShares25')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="1">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(CapitalGainsShares25[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
        <cell colspan="1" horizontalalign="right">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(CapitalGainsShares25Perc[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>%
          </paragraph>
        </cell>
        <cell colspan="2" horizontalalign="right">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(CapitalGainsShares25Result[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="1">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="">
            <xsl:value-of select="Translations:GetTranslation('BelastingsBerekeningItem','ExitTarif','ExitTarif')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="1">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(ExitTarif[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
        <cell colspan="1" horizontalalign="right">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(ExitTarifPerc[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>%
          </paragraph>
        </cell>
        <cell colspan="2" horizontalalign="right">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(ExitTarifResult[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <xsl:if test="not(Verminderd='true')">
        <row>
          <cell colspan="1">
            <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="">
              <xsl:value-of select="Translations:GetTranslation('BelastingsBerekeningItem','EenduidigTariefBedrag','EenduidigTariefBedrag')"></xsl:value-of>
            </paragraph>
          </cell>
          <cell horizontalalign="right" colspan="1">
            <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
              <xsl:value-of select="format-number(sum(EenduidigTariefBedrag[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
            </paragraph>
          </cell>
          <cell colspan="1" horizontalalign="right">
            <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
              <xsl:value-of select="format-number(sum(EenduidigTariefPerc[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>%
            </paragraph>
          </cell>
          <cell colspan="2" horizontalalign="right">
            <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
              <xsl:value-of select="format-number(sum(EenduidigTariefTot[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
            </paragraph>
          </cell>
        </row>
      </xsl:if>
      <xsl:if test="Verminderd='true'">
        <row>
          <cell colspan="1">
            <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="">
              <xsl:value-of select="Translations:GetTranslation('BelastingsBerekeningItem','VerminderdtariefStap1Base','VerminderdtariefStap1Base')"></xsl:value-of>
            </paragraph>
          </cell>
          <cell horizontalalign="right" colspan="1">
            <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
              <xsl:value-of select="format-number(sum(VerminderdtariefStap1Base[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
            </paragraph>
          </cell>
          <cell colspan="1" horizontalalign="right">
            <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
              <xsl:value-of select="format-number(sum(VerminderdtariefStap1Perc[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>%
            </paragraph>
          </cell>
          <cell colspan="2" horizontalalign="right">
            <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
              <xsl:value-of select="format-number(sum(VerminderdtariefStap1[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
            </paragraph>
          </cell>
        </row>
        <row>
          <cell colspan="1">
            <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="">
              <xsl:value-of select="Translations:GetTranslation('BelastingsBerekeningItem','VerminderdtariefStap2Base','VerminderdtariefStap2Base')"></xsl:value-of>
            </paragraph>
          </cell>
          <cell horizontalalign="right" colspan="1">
            <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
              <xsl:value-of select="format-number(sum(VerminderdtariefStap2Base[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
            </paragraph>
          </cell>
          <cell colspan="1" horizontalalign="right">
            <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
              <xsl:value-of select="format-number(sum(VerminderdtariefStap2Perc[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>%
            </paragraph>
          </cell>
          <cell colspan="2" horizontalalign="right">
            <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
              <xsl:value-of select="format-number(sum(VerminderdtariefStap2[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
            </paragraph>
          </cell>
        </row>
        <row>
          <cell colspan="1">
            <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="">
              <xsl:value-of select="Translations:GetTranslation('BelastingsBerekeningItem','VerminderdtariefStap3Base','VerminderdtariefStap3Base')"></xsl:value-of>
            </paragraph>
          </cell>
          <cell horizontalalign="right" colspan="1">
            <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
              <xsl:value-of select="format-number(sum(VerminderdtariefStap3Base[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
            </paragraph>
          </cell>
          <cell colspan="1" horizontalalign="right">
            <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
              <xsl:value-of select="format-number(sum(VerminderdtariefStap3Perc[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>%
            </paragraph>
          </cell>
          <cell colspan="2" horizontalalign="right">
            <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
              <xsl:value-of select="format-number(sum(VerminderdtariefStap3[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
            </paragraph>
          </cell>
        </row>
      </xsl:if>
      <row>
        <cell colspan="5">
          <paragraph>
            <space/>
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="4">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="text-decoration:underline;">
            <xsl:value-of select="Translations:GetTranslation('BelastingsBerekeningItem','CorporateIncomeTax','CorporateIncomeTax')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="1">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(CorporateIncomeTax[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="4">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="">
            <xsl:value-of select="Translations:GetTranslation('BelastingsBerekeningItem','NonRefundableWithholdingTaxes_TaxCredit','NonRefundableWithholdingTaxes_TaxCredit')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="1">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(NonRefundableWithholdingTaxes_TaxCredit[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="4">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="">
            <xsl:value-of select="Translations:GetTranslation('BelastingsBerekeningItem','RefundableTaxeCredit','RefundableTaxeCredit')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="1">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(RefundableTaxeCredit[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="4">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="">
            <xsl:value-of select="Translations:GetTranslation('BelastingsBerekeningItem','RefundableWithholdingTaxes','RefundableWithholdingTaxes')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="1">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(RefundableWithholdingTaxes[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="4">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="">
            <xsl:value-of select="Translations:GetTranslation('BelastingsBerekeningItem','BasisForIncrease','BasisForIncrease')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="1">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(BasisForIncrease[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="2">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="">
            <xsl:value-of select="Translations:GetTranslation('BelastingsBerekeningItem','JongVennootschap','JongVennootschap')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell colspan="3" horizontalalign="left">
          <xsl:choose>
            <xsl:when test="JongVennootschap='true'">
              <image id="checked"></image>
            </xsl:when>
            <xsl:otherwise>
              <image id="unchecked"></image>
            </xsl:otherwise>
          </xsl:choose>
        </cell>
      </row>
      <row>
        <cell colspan="5">
          <paragraph>
            <space/>
          </paragraph>
        </cell>
      </row>
      <xsl:if test="JongVennootschap='false'">
        <row>
          <cell colspan="5">
            <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="text-decoration:underline;">
              <xsl:value-of select="Translations:GetTranslation('BelastingsBerekeningItem','BasisTaxIncreaseInsufficientAdvancePayments','BasisTaxIncreaseInsufficientAdvancePayments')"></xsl:value-of>
            </paragraph>
          </cell>
        </row>
        <row>
          <cell colspan="5">
            <paragraph lineheight="1" size="6" style="">
              <space/>
            </paragraph>
          </cell>
        </row>
        <row>
          <cell horizontalalign="right">
            <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="">
              <xsl:value-of select="Translations:GetTranslation('BelastingsBerekeningItem','Base','Base')"></xsl:value-of>
            </paragraph>
          </cell>
          <cell horizontalalign="right">
            <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
              <xsl:value-of select="format-number(sum(BTIBase[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
            </paragraph>
          </cell>
          <cell colspan="1" horizontalalign="right">
            <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
              <xsl:value-of select="format-number(sum(BTIPercentage[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>%
            </paragraph>
          </cell>
          <cell colspan="1" horizontalalign="right">
            <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
              <xsl:value-of select="format-number(sum(BTITotal[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
            </paragraph>
          </cell>
          <cell colspan="1">
            <paragraph>
              <space/>
            </paragraph>
          </cell>
        </row>
        <row>
          <cell colspan="5">
            <paragraph>
              <space/>
            </paragraph>
          </cell>
        </row>
        <row>
          <cell colspan="5">
            <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="text-decoration:underline;">
              <xsl:value-of select="Translations:GetTranslation('BelastingsBerekeningItem','AdvancePayments','AdvancePayments')"></xsl:value-of>
            </paragraph>
          </cell>
        </row>
        <row>
          <cell colspan="1">
            <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="">
              <xsl:value-of select="Translations:GetTranslation('BelastingsBerekeningItem','AP1Base','AP1Base')"></xsl:value-of>
            </paragraph>
          </cell>
          <cell horizontalalign="right" colspan="1">
            <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
              <xsl:value-of select="format-number(sum(AP1Base[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
            </paragraph>
          </cell>
          <cell colspan="1" horizontalalign="right">
            <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
              <xsl:value-of select="format-number(sum(AP1Percentage[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>%
            </paragraph>
          </cell>
          <cell colspan="1" horizontalalign="right">
            <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
              <xsl:value-of select="format-number(sum(AP1Total[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
            </paragraph>
          </cell>
          <cell colspan="1">
            <paragraph>
              <space/>
            </paragraph>
          </cell>
        </row>
        <row>
          <cell colspan="1">
            <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="">
              <xsl:value-of select="Translations:GetTranslation('BelastingsBerekeningItem','AP2Base','AP2Base')"></xsl:value-of>
            </paragraph>
          </cell>
          <cell horizontalalign="right" colspan="1">
            <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
              <xsl:value-of select="format-number(sum(AP2Base[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
            </paragraph>
          </cell>
          <cell colspan="1" horizontalalign="right">
            <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
              <xsl:value-of select="format-number(sum(AP2Percentage[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>%
            </paragraph>
          </cell>
          <cell colspan="1" horizontalalign="right">
            <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
              <xsl:value-of select="format-number(sum(AP2Total[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
            </paragraph>
          </cell>
          <cell colspan="1">
            <paragraph>
              <space/>
            </paragraph>
          </cell>
        </row>
        <row>
          <cell colspan="1">
            <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="">
              <xsl:value-of select="Translations:GetTranslation('BelastingsBerekeningItem','AP3Base','AP3Base')"></xsl:value-of>
            </paragraph>
          </cell>
          <cell horizontalalign="right" colspan="1">
            <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
              <xsl:value-of select="format-number(sum(AP3Base[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
            </paragraph>
          </cell>
          <cell colspan="1" horizontalalign="right">
            <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
              <xsl:value-of select="format-number(sum(AP3Percentage[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>%
            </paragraph>
          </cell>
          <cell colspan="1" horizontalalign="right">
            <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
              <xsl:value-of select="format-number(sum(AP3Total[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
            </paragraph>
          </cell>
          <cell colspan="1">
            <paragraph>
              <space/>
            </paragraph>
          </cell>
        </row>
        <row>
          <cell colspan="1">
            <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="">
              <xsl:value-of select="Translations:GetTranslation('BelastingsBerekeningItem','AP4Base','AP4Base')"></xsl:value-of>
            </paragraph>
          </cell>
          <cell horizontalalign="right" colspan="1">
            <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
              <xsl:value-of select="format-number(sum(AP4Base[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
            </paragraph>
          </cell>
          <cell colspan="1" horizontalalign="right">
            <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
              <xsl:value-of select="format-number(sum(AP4Percentage[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>%
            </paragraph>
          </cell>
          <cell colspan="1" horizontalalign="right">
            <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
              <xsl:value-of select="format-number(sum(AP4Total[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
            </paragraph>
          </cell>
          <cell colspan="1">
            <paragraph>
              <space/>
            </paragraph>
          </cell>
        </row>
        <row>
          <cell colspan="1">
            <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="font-weight:bold;">
              <xsl:value-of select="Translations:GetTranslation('BelastingsBerekeningItem','APBaseTotal','APBaseTotal')"></xsl:value-of>
            </paragraph>
          </cell>
          <cell horizontalalign="right" colspan="1">
            <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="font-weight:bold;">
              <xsl:value-of select="format-number(sum(APBaseTotal[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
            </paragraph>
          </cell>
          <cell colspan="2">
            <paragraph>
              <space></space>
            </paragraph>
          </cell>
          <cell colspan="1" horizontalalign="right">
            <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="font-weight:bold;">
              <xsl:value-of select="format-number(sum(APTotal[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
            </paragraph>
          </cell>
        </row>
        <row>
          <cell colspan="5">
            <paragraph>
              <space/>
            </paragraph>
          </cell>
        </row>
        <row>
          <cell colspan="3">
            <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="">
              <xsl:value-of select="Translations:GetTranslation('BelastingsBerekeningItem','Test1percRule','Test1percRule')"></xsl:value-of>
            </paragraph>
          </cell>
          <cell horizontalalign="right" colspan="1">
            <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
              <xsl:value-of select="format-number(sum(Test1percRule[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
            </paragraph>
          </cell>
          <cell colspan="1">
            <paragraph>
              <space></space>
            </paragraph>
          </cell>
        </row>
        <row>
          <cell colspan="4">
            <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="">
              <xsl:value-of select="Translations:GetTranslation('BelastingsBerekeningItem','TaxIncreaseInsufficientAdvancePayments','TaxIncreaseInsufficientAdvancePayments')"></xsl:value-of>
            </paragraph>
          </cell>
          <cell horizontalalign="right" colspan="1">
            <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
              <xsl:value-of select="format-number(sum(TaxIncreaseInsufficientAdvancePayments[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
            </paragraph>
          </cell>
        </row>
        <row>
          <cell colspan="5">
            <paragraph>
              <space/>
            </paragraph>
          </cell>
        </row>
      </xsl:if>
      <row>
        <cell colspan="5">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="font-weight:boldtext-decoration:underline;">
            <xsl:value-of select="labelTotalCorporateTaxDue"></xsl:value-of>
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="1">
          <paragraph>
            <space></space>
          </paragraph>
        </cell>
        <cell colspan="3">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="">
            <xsl:value-of select="Translations:GetTranslation('BelastingsBerekeningItem','BasisCorporateIncomeTax','BasisCorporateIncomeTax')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="1">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(BasisCorporateIncomeTax[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="1">
          <paragraph>
            <space></space>
          </paragraph>
        </cell>
        <cell colspan="3">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="">
            <xsl:value-of select="Translations:GetTranslation('BelastingsBerekeningItem','ApplicableTaxIncrease','ApplicableTaxIncrease')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="1">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(ApplicableTaxIncrease[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="1">
          <paragraph>
            <space></space>
          </paragraph>
        </cell>
        <cell colspan="3">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="">
            <xsl:value-of select="Translations:GetTranslation('BelastingsBerekeningItem','NonRefundableWithholdingTaxes_ForeignTaxCredit','NonRefundableWithholdingTaxes_ForeignTaxCredit')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="1">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(NonRefundableWithholdingTaxes_ForeignTaxCredit[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="1">
          <paragraph>
            <space></space>
          </paragraph>
        </cell>
        <cell colspan="3">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="">
            <xsl:value-of select="Translations:GetTranslation('BelastingsBerekeningItem','RefundableTaxCredit','RefundableTaxCredit')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="1">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(RefundableTaxCredit[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="1">
          <paragraph>
            <space></space>
          </paragraph>
        </cell>
        <cell colspan="3">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="">
            <xsl:value-of select="Translations:GetTranslation('BelastingsBerekeningItem','RefundableWithholdingTaxesEnd','RefundableWithholdingTaxesEnd')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="1">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(RefundableWithholdingTaxesEnd[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="1">
          <paragraph>
            <space></space>
          </paragraph>
        </cell>
        <cell colspan="3">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="">
            <xsl:value-of select="Translations:GetTranslation('BelastingsBerekeningItem','AdvancePaymentsEnd','AdvancePaymentsEnd')"></xsl:value-of>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="1">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
            <xsl:value-of select="format-number(sum(AdvancePaymentsEnd[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
      <row>
        <cell colspan="1">
          <paragraph>
            <space></space>
          </paragraph>
        </cell>
        <cell colspan="3">
          <paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="font-weight:bold;text-decoration:underline;">
            <xsl:choose>
              <xsl:when test="number(OutstandingLiability_BelastingsBerekening)&lt;0">
                <xsl:value-of select="Translations:GetTranslation('BelastingsBerekeningItem','RECIEVE','RECIEVE')"/>
              </xsl:when>
              <xsl:otherwise>
                <xsl:value-of select="Translations:GetTranslation('BelastingsBerekeningItem','PAY','PAY')"/>
              </xsl:otherwise>
            </xsl:choose>
          </paragraph>
        </cell>
        <cell horizontalalign="right" colspan="1">
          <paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="font-weight:bold;text-decoration:underline;">
            <xsl:value-of select="format-number(sum(OutstandingLiability_BelastingsBerekening[number()=number()]),$NumberFormatting,'euro')"></xsl:value-of>€
          </paragraph>
        </cell>
      </row>
    </table>
  </xsl:template>
  <xsl:template match="*"></xsl:template>
</xsl:stylesheet>