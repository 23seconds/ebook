<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" exclude-result-prefixes="Helper Translations File Client" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:Helper="urn:Helper" xmlns:Translations="urn:Translations" xmlns:File="urn:File" xmlns:Client="urn:Client">
  <xsl:decimal-format name="NonNumber" NaN="0" zero-digit="0"/>
  <xsl:variable name="WorksheetLabelFontsize" select="string('10')"/>
  <xsl:variable name="WorksheetFontsize" select="string('10')"/>
  <xsl:variable name="WorksheetNumbersize" select="string('8')"/>
  <xsl:variable name="WorksheetSmallFontsize" select="string('8')"/>
  <xsl:variable name="WorksheetTotalsFontsize" select="string('10')"/>
  <xsl:variable name="WorksheetSpacing" select="1.1"/>
  <xsl:decimal-format name="euro" decimal-separator="," grouping-separator="."/>
  <xsl:variable name="NumberFormatting" select="string('#.##0,00')"/>
  <xsl:variable name="NumberFormattingNoDigit" select="string('#.##0')"/>
  <xsl:variable name="FONT" select="string('C:\WINDOWS\Fonts\EYInterstate-Light.ttf')"/>
  <xsl:param name="RuleAppDef"/>
  <xsl:param name="Culture"/>
  <xsl:template match="BelastingVrijeReserves">
    <Root>
    <xsl:apply-templates select="$RuleAppDef//RuleApplicationDef/Entities/EntityDef[@Name='BelastingVrijeReserves']/Fields/FieldDef[@DataType='Entity']" mode="test">
      <xsl:with-param name="results" select="."/>
    </xsl:apply-templates>
    </Root>
    <!--Root>
      <xsl:apply-templates select="$RuleAppDef//RuleApplicationDef" mode="WORKSHEET">
        <xsl:with-param name="results" select="."/>
      </xsl:apply-templates>
    </Root-->
  </xsl:template>

  <xsl:template match="FieldDef" mode="test">
    <xsl:param name="results"/>
    <xsl:variable name="entity" select="@DataTypeEntityName"/>
    <xsl:variable name="fname" select="@Name"/>
    <phrase>
      <xsl:value-of select="@DisplayName"/>
    </phrase>
    <xsl:apply-templates select="$RuleAppDef//RuleApplicationDef/Entities/EntityDef[@Name=$entity]">
      <xsl:with-param name="results" select="$results//fname"/>
    </xsl:apply-templates>
  </xsl:template>

  <xsl:template match="EntityDef">
    <xsl:param name="results"/>
    <xsl:for-each select="Fields/FieldDef">
      <xsl:variable name="fname" select="@Name"/>
      <xsl:variable name="resultNode" select="$results//node()[name()=$fname]"/>
      <phrase>
      <xsl:value-of select="@Name"/>:
      <xsl:if test="Attributes/anyType/Collection/anyType[@Key='Grid']/@Value='false' and Attributes/anyType/Collection/anyType[@Key='Form']/@Value='false'">Hidden,</xsl:if>
      <xsl:if test="Attributes/anyType/Collection/anyType[@Key='Interface']/@Value='LabelField'">Label,</xsl:if>
      <xsl:if test="Attributes/anyType/Collection/anyType[@Key='LabelSeperate']/@Value='true'">Label seperate,</xsl:if>
      <xsl:if test="not(Attributes/anyType/Collection/anyType[@Key='LabelColspan']/@Value='')">Label Colspan:<xsl:value-of select="Attributes/anyType/Collection/anyType[@Key='LabelColspan']/@Value"/>,</xsl:if>
      <xsl:if test="not(Attributes/anyType/Collection/anyType[@Key='ItemColspan']/@Value='')">Item Colspan:<xsl:value-of select="Attributes/anyType/Collection/anyType[@Key='ItemColspan']/@Value"/>,</xsl:if>
      <xsl:if test="Attributes/anyType/Collection/anyType[@Key='LabelHidden']/@Value='true'">Label Hidden,</xsl:if>
        ,Value:<xsl:apply-templates select="FieldDef" mode="PRINT_VALUE">
          <xsl:with-param name="resultNode" select="$resultNode"/>
        </xsl:apply-templates>
      </phrase>
    </xsl:for-each>
  </xsl:template>

  <xsl:template match="FieldDef" mode="PRINT_VALUE">
    <xsl:param name="resultNode"/>
    <xsl:choose>
      <xsl:when test="$resultNode">
        <xsl:value-of select="$resultNode"/>
      </xsl:when>
      <xsl:otherwise>EMPTY/NOT FOUND</xsl:otherwise>
    </xsl:choose> 
  </xsl:template>
  
  <xsl:template match="RuleApplicationDef[@Name='BerekeningVenBApp']" mode="WORKSHEET">
    <xsl:param name="results"/>
    <xsl:for-each select="Entities/EntityDef[@Name='BerekeningVenB']/Fields/FieldDef[@DataType='Entity']">
      <xsl:variable name="fieldName" select="@Name"/>
      <xsl:variable name="entityName" select="@DataTypeEntityName"/>
      <xsl:if test="count($results//node()[name()=$fieldName])>0">
        <xsl:apply-templates select="//Entities/EntityDef[@Name=$entityName]" mode="RENDERTAB">
          <xsl:with-param name="data" select="$results//node()[name()=$fieldName][1]"/>
          <xsl:with-param name="entityName" select="$entityName"/>
          <xsl:with-param name="entityFieldName" select="$fieldName"/>
        </xsl:apply-templates>
      </xsl:if>
    </xsl:for-each>
  </xsl:template>
  <xsl:template match="EntityDef" mode="RENDERTAB">
    <xsl:param name="data"/>
    <xsl:param name="entityName"/>
    <xsl:param name="entityFieldName"/>
    <table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" columns="2" width="100%" align="Center" widths="70;30" cellspacing="0">
      <row>
        <cell colspan="2" verticalalign="top" horizontalalign="right" header="true">
          <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}" fontstyle="bold">
            <xsl:value-of select="Translations:GetTranslation('BerekeningVenB',$entityFieldName,$entityFieldName)"/>
          </phrase>
        </cell>
      </row>
      <xsl:for-each select="Fields/FieldDef">
        <xsl:variable name="fieldName" select="@Name"/>
        <row>
          <cell verticalalign="top" horizontalalign="right" header="true">
            <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" fontstyle="bold">
              <xsl:value-of select="Translations:GetTranslation($entityName,@Name,@Name)"/>
            </phrase>
          </cell>
          <cell verticalalign="top" horizontalalign="right">
            <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
              <xsl:choose>
                <xsl:when test="@DataType='Number'">
                  <xsl:value-of select="format-number($data/node()[name()=$fieldName],$NumberFormatting,'euro')"/>€
                </xsl:when>
                <xsl:otherwise>
                  <xsl:value-of select="$data/node()[name()=$fieldName]"/>
                </xsl:otherwise>
              </xsl:choose>
            </phrase>
          </cell>
        </row>
      </xsl:for-each>
    </table>
  </xsl:template>
</xsl:stylesheet>