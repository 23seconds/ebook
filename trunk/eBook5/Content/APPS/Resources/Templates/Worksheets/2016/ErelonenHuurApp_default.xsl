<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" exclude-result-prefixes="Helper Translations File Client" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:Helper="urn:Helper" xmlns:Translations="urn:Translations" xmlns:File="urn:File" xmlns:Client="urn:Client">
  <xsl:decimal-format name="NonNumber" NaN="0" zero-digit="0"/>
  <xsl:variable name="WorksheetLabelFontsize" select="string('10')"/>
  <xsl:variable name="WorksheetFontsize" select="string('10')"/>
  <xsl:variable name="WorksheetNumbersize" select="string('8')"/>
  <xsl:variable name="WorksheetSmallFontsize" select="string('8')"/>
  <xsl:variable name="WorksheetTotalsFontsize" select="string('10')"/>
  <xsl:variable name="WorksheetSpacing" select="1.1"/>
  <xsl:decimal-format name="euro" decimal-separator="," grouping-separator="."/>
  <xsl:variable name="NumberFormatting" select="string('#.##0,00')"/>
  <xsl:variable name="NumberFormattingNoDigit" select="string('#.##0')"/>
  <xsl:variable name="FONT" select="string('C:\WINDOWS\Fonts\EYInterstate-Light.ttf')"/>
  <xsl:param name="RuleAppDef"/>
  <xsl:param name="Culture"/>
  <xsl:template match="ErelonenHuur">
    <xsl:variable name="btw">
      <xsl:value-of select="Translations:GetTranslation('ErelonenHuur','Vat','Vat')"/>
    </xsl:variable>
    <Root>
      <xsl:if test="count(Import/*)=0">
        <phrase>WORKSHEET HAS NO DATA</phrase>
      </xsl:if>
      <xsl:for-each select="Import/*">
        <xsl:variable name="importId" select="ID/text()"/>
        <xsl:variable name="resultCount" select="count(//Historiek/*[ImportedAccount/id = $importId])"></xsl:variable>
        <xsl:if test="$resultCount>0">
          <table tablefitspage="true" convert2pdfp="true" cellsfitpage="true" columns="4" width="100%" align="Center" widths="55;15;15;15" cellspacing="1" cellpadding="1">
            <row>
              <cell verticalalign="middle" horizontalalign="left" header="true" colspan="4" border="1">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}" fontstyle="bold">
                  <xsl:value-of select="GrootboekOmschrijving"/>
                  <!--xsl:choose>
                    <xsl:when test="$Culture='nl-BE'">
                      <xsl:value-of select="GrootBoek/nl"/>
                    </xsl:when>
                    <xsl:when test="$Culture='fr-FR'">
                      <xsl:value-of select="GrootBoek/fr"/>
                    </xsl:when>
                    <xsl:when test="$Culture='en-US'">
                      <xsl:value-of select="GrootBoek/en"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:value-of select="GrootBoek/nl"/>
                    </xsl:otherwise>
                  </xsl:choose-->
                </phrase>
                <newline/>
              </cell>
            </row>
            <xsl:for-each select="//Historiek/*[ImportedAccount/id = $importId]">
              <xsl:variable name="GroupCount" select="last()"/>
              <xsl:variable name="CustomerID" select="Leverancier/id"/>
              <xsl:variable name="TempBetaald" select="number(FiscaleWaarde/text())"/>
              <xsl:variable name="TempToename" select="number(Toename/text())"/>
              <xsl:variable name="TempTerugname" select="number(Terugname/text())"/>
              <xsl:variable name="TempVoorzienCheck" select="(not($TempToename=0) or not($TempTerugname=0))"/>
              <xsl:variable name="TempBedrag" select="$TempBetaald + $TempToename - $TempTerugname"/>
              <xsl:variable name="supplierName" select="Leverancier/nl"/>
              <row>
                <cell verticalalign="middle" horizontalalign="left">
                  <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}">
                    <xsl:value-of select="$supplierName"/>
                  </phrase>
                  <xsl:if test="not($supplierName ='')">
                    <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}">
                      <xsl:value-of select="Client:GetSupplier(string($CustomerID),'Address','')"/>
                    </phrase>
                    <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}">
                      
                      <xsl:value-of select="Client:GetSupplier(string($CustomerID),'ZipCode','')"/>&#160;<xsl:value-of select="Client:GetSupplier(string($CustomerID),'City','')"/>
                    </phrase>
                    <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}">
                      <xsl:value-of select="Client:GetSupplier(string($CustomerID),'Country','')"/>
                    </phrase>
                    <xsl:variable name="supplierVAT" select="Client:GetSupplier(string($CustomerID),'Vat','')"/>
                    <xsl:if test="not($supplierVAT ='')">
                      <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}">
                        <xsl:value-of select="concat($btw,concat(': ',$supplierVAT))"/>
                      </phrase>
                    </xsl:if>
                  </xsl:if>
                </cell>
                <cell verticalalign="bottom" horizontalalign="right">
                  <xsl:if test="$TempVoorzienCheck and not(number($TempBetaald )= 0)">
                    <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
                      <xsl:value-of select="format-number($TempBetaald,$NumberFormatting,'euro')"/>€
                    </phrase>
                  </xsl:if>
                </cell>
                <cell verticalalign="bottom" horizontalalign="right">
                  <xsl:if test="not($TempVoorzienCheck) and not(number($TempBedrag )= 0) and $resultCount &gt; 1">
                    <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
                      <xsl:value-of select="format-number($TempBetaald,$NumberFormatting,'euro')"/>€
                    </phrase>
                  </xsl:if>
                </cell>
                <cell verticalalign="bottom" horizontalalign="right">
                  <xsl:if test="not($TempVoorzienCheck) and not(number($TempBedrag )= 0) and $resultCount = 1">
                    <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" style="bold">
                      <xsl:value-of select="format-number($TempBedrag,$NumberFormatting, 'euro')"/>€
                    </phrase>
                  </xsl:if>
                </cell>
              </row>
              <xsl:if test="$TempVoorzienCheck">
                <row>
                  <cell verticalalign="bottom" horizontalalign="left" colspan="4">
                    <phrase lineheight="1.5">&#160;</phrase>
                  </cell>
                </row>
                <xsl:if test="$TempTerugname != 0 and not(string(number($TempTerugname))='NaN')">
                  <row>
                    <cell verticalalign="bottom" horizontalalign="left">
                      <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}">
                        <xsl:value-of select="Translations:GetTranslation('ErelonenHuur','TerugnameVoorziening','TerugnameVoorziening')"/>
                      </phrase>
                    </cell>
                    <cell verticalalign="bottom" horizontalalign="right">
                      <xsl:if test="not(number($TempBedrag )= 0) or (number($TempBedrag )= 0 and $resultCount &gt; 1)">
                        <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
                          -<xsl:value-of select="format-number($TempTerugname, $NumberFormatting, 'euro')"/>€
                        </phrase>
                      </xsl:if>
                    </cell>
                    <cell verticalalign="bottom" horizontalalign="right">
                      <!--xsl:if test="number($TempBedrag )= 0 and $resultCount &gt; 1"><phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}"><xsl:value-of select="format-number($TempVoorzien, $NumberFormatting, 'euro')"/>€</phrase></xsl:if-->
                    </cell>
                    <cell verticalalign="bottom" horizontalalign="right">
                      <xsl:if test="number($TempBedrag )= 0 and $resultCount = 1">
                        <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" style="bold">
                          <xsl:value-of select="format-number($TempTerugname, $NumberFormatting, 'euro')"/>€
                        </phrase>
                      </xsl:if>
                    </cell>
                  </row>
                </xsl:if>
                <xsl:if test="$TempToename != 0 and not(string(number($TempToename))='NaN')">
                  <row>
                    <cell verticalalign="bottom" horizontalalign="left">
                      <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}">
                        <xsl:value-of select="Translations:GetTranslation('ErelonenHuur','ToenameVoorziening','ToenameVoorziening')"/>
                        <!--<xsl:value-of select="Translations:GetTranslation('ErelonenHuur','TerugnameVoorziening','TerugnameVoorziening')"/>-->
                      </phrase>
                    </cell>
                    <cell verticalalign="bottom" horizontalalign="right">
                      <xsl:if test="not(number($TempBedrag )= 0) or (number($TempBedrag )= 0 and $resultCount &gt; 1)">
                        <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
                          <xsl:value-of select="format-number($TempToename, $NumberFormatting, 'euro')"/>€
                        </phrase>
                      </xsl:if>
                    </cell>
                    <cell verticalalign="bottom" horizontalalign="right">
                      <!--xsl:if test="number($TempBedrag )= 0 and $resultCount &gt; 1"><phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}"><xsl:value-of select="format-number($TempVoorzien, $NumberFormatting, 'euro')"/>€</phrase></xsl:if-->
                    </cell>
                    <cell verticalalign="bottom" horizontalalign="right">
                      <xsl:if test="number($TempBedrag )= 0 and $resultCount = 1">
                        <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" style="bold">
                          <xsl:value-of select="format-number($TempToename, $NumberFormatting, 'euro')"/>€
                        </phrase>
                      </xsl:if>
                    </cell>
                  </row>
                </xsl:if>
                <xsl:if test="not(number($TempBedrag )= 0) and $resultCount &gt; 1">
                  <row>
                    <cell verticalalign="bottom" horizontalalign="left" colspan="2"></cell>
                    <cell verticalalign="bottom" horizontalalign="right">
                      <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
                        <xsl:value-of select="format-number($TempBedrag,$NumberFormatting, 'euro')"/>€
                      </phrase>
                    </cell>
                    <cell verticalalign="bottom" horizontalalign="left"></cell>
                  </row>
                </xsl:if>
                <xsl:if test="not(number($TempBedrag )= 0) and $resultCount = 1">
                  <row>
                    <cell verticalalign="bottom" horizontalalign="left" colspan="3"></cell>
                    <cell verticalalign="bottom" horizontalalign="right">
                      <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" style="bold">
                        <xsl:value-of select="format-number($TempBedrag,$NumberFormatting, 'euro')"/>€
                      </phrase>
                    </cell>
                  </row>
                </xsl:if>
              </xsl:if>
              <row>
                <cell verticalalign="middle" horizontalalign="left" colspan="4">
                  <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}">&#160;</phrase>
                  <newline/>
                </cell>
              </row>
            </xsl:for-each>
            <xsl:if test="$resultCount &gt; 1">
              <row>
                <cell horizontalalign="right" colspan="3">
                  <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}" fontstyle="bold">&#160;</phrase>
                </cell>
                <cell horizontalalign="right" verticalalign="bottom">
                  <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" style="bold">
                    <xsl:variable name="totRek" select="sum(//Historiek/*[ImportedAccount/id = $importId]/FiscaleWaarde)+sum(//Historiek/*[ImportedAccount/id = $importId]/Toename)-sum(//Historiek/*[ImportedAccount/id = $importId]/Terugname)"/><xsl:value-of select="format-number($totRek,$NumberFormatting, 'euro')"/>€
                  </phrase>
                </cell>
              </row>
            </xsl:if>
          </table>
          <newline/>
        </xsl:if>
      </xsl:for-each>
    </Root>
  </xsl:template>
</xsl:stylesheet>