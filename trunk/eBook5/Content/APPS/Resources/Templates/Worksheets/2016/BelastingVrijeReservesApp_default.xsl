<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" exclude-result-prefixes="Helper Translations File Client" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:Helper="urn:Helper" xmlns:Translations="urn:Translations" xmlns:File="urn:File" xmlns:Client="urn:Client">
	<xsl:decimal-format name="NonNumber" NaN="0" zero-digit="0"/>
	<xsl:variable name="WorksheetLabelFontsize" select="string('10')"/>
	<xsl:variable name="WorksheetFontsize" select="string('10')"/>
	<xsl:variable name="WorksheetNumbersize" select="string('8')"/>
	<xsl:variable name="WorksheetNumberSmallsize" select="string('6')"/>
	<xsl:variable name="WorksheetSmallFontsize" select="string('8')"/>
	<xsl:variable name="WorksheetTotalsFontsize" select="string('10')"/>
	<xsl:variable name="WorksheetSpacing" select="1.1"/>
	<xsl:decimal-format name="euro" decimal-separator="," grouping-separator="."/>
	<xsl:variable name="NumberFormatting" select="string('#.##0,00')"/>
	<xsl:variable name="NumberFormattingNoDigit" select="string('#.##0')"/>
	<xsl:variable name="FONT" select="string('C:\WINDOWS\Fonts\EYInterstate-Light.ttf')"/>
	<xsl:param name="RuleAppDef"/>
	<xsl:param name="Culture"/>
	<xsl:template match="BelastingVrijeReserves">
		<Root orientation="landscape">
			<xsl:apply-templates select="VrijgesteldeReserves" mode="RENDER_BV"/>
		</Root>
		<!--Root><xsl:apply-templates select="$RuleAppDef//RuleApplicationDef" mode="WORKSHEET"><xsl:with-param name="results" select="."/></xsl:apply-templates></Root-->
	</xsl:template>
	<!--<xsl:variable name="group" select="Attributes/anyType/Collection/anyType[@Key='Group']/@Value"/><xsl:variable name="groupMembers" select="$lines//FieldDef[Attributes/anyType/Collection/anyType[@Key='Group']/@Value=$group]"/>--><xsl:template match="VrijgesteldeReserves" mode="RENDER_BV">
		<table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" columns="5" width="100%" align="Center" widths="52;12;12;12;12" cellspacing="0">
			<row>
				<cell verticalalign="top" horizontalalign="left">
					<phrase lineheight="1.1" fontstyle="bold" size="8">
						<xsl:value-of select="Translations:GetTranslation('Detail','Title','Title')" />
					</phrase>
				</cell>
				<cell verticalalign="top" horizontalalign="right">
					<phrase lineheight="1.1" fontstyle="bold" size="8">
						<xsl:value-of select="Translations:GetTranslation('Detail','TitleStart','TitleStart')" />
					</phrase>
				</cell>
				<cell verticalalign="top" horizontalalign="right">
					<phrase lineheight="1.1" fontstyle="bold" size="8">
						<xsl:value-of select="Translations:GetTranslation('Detail','TitlePos','TitlePos')" />
					</phrase>
				</cell>
				<cell verticalalign="top" horizontalalign="right">
					<phrase lineheight="1.1" fontstyle="bold" size="8">
						<xsl:value-of select="Translations:GetTranslation('Detail','TitleNeg','TitleNeg')" />
					</phrase>
				</cell>
				<cell verticalalign="top" horizontalalign="right">
					<phrase lineheight="1.1" fontstyle="bold" size="8">
						<xsl:value-of select="Translations:GetTranslation('Detail','TitleEnd','TitleEnd')" />
					</phrase>
				</cell>
			</row>
		</table>
		<table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" columns="5" width="100%" align="Center" widths="52;12;12;12;12" cellspacing="0">
			<row>
				<cell colspan="5" verticalalign="top" horizontalalign="left" header="true">
					<phrase lineheight="1.1" size="10">
						<xsl:value-of select="Translations:GetTranslation('Detail','ATitle','ATitle')" />
					</phrase>
				</cell>
			</row>
			<xsl:variable name="dummyATitle" select="Helper:ResetSum('1_')" />
			<xsl:if test="not(number(AWaardeverminderingA[number()=number()])=0 and number(AWaardeverminderingB[number()=number()])=0 and number(AWaardeverminderingC[number()=number()])=0 and number(AWaardeverminderingD[number()=number()])=0)">
				<xsl:variable name="dummyBTitleAWaardeverminderingA" select="Helper:AddSum('1_',1)" />
				<row>
					<cell verticalalign="top" horizontalalign="left">
						<phrase lineheight="1.1" size="8"> &#160;&#160;&#160;&#160;<xsl:value-of select="Translations:GetTranslation('Detail','AWaardeverminderingA','AWaardeverminderingA')" />
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" size="8">
							<xsl:if test="AWaardeverminderingA[number()=number()]">
								<xsl:value-of select="format-number(number(AWaardeverminderingA[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" size="8">
							<xsl:if test="AWaardeverminderingB[number()=number()]">
								<xsl:value-of select="format-number(number(AWaardeverminderingB[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" size="8">
							<xsl:if test="AWaardeverminderingC[number()=number()]">
								<xsl:value-of select="format-number(number(AWaardeverminderingC[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" size="8">
							<xsl:if test="AWaardeverminderingD[number()=number()]">
								<xsl:value-of select="format-number(number(AWaardeverminderingD[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
				</row>
			</xsl:if>
		</table>
		<table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" columns="5" width="100%" align="Center" widths="52;12;12;12;12" cellspacing="0">
			<row>
				<cell colspan="5" verticalalign="top" horizontalalign="left" header="true">
					<phrase lineheight="1.1" size="10">
						<xsl:value-of select="Translations:GetTranslation('Detail','BTitle','BTitle')" />
					</phrase>
				</cell>
			</row>
			<xsl:variable name="dummyBTitle" select="Helper:ResetSum('2_')" />
			<xsl:if test="not(number(BVoorzieningA[number()=number()])=0 and number(BVoorzieningB[number()=number()])=0 and number(BVoorzieningC[number()=number()])=0 and number(BVoorzieningD[number()=number()])=0)">
				<xsl:variable name="dummyBTitleBVoorzieningA" select="Helper:AddSum('2_',1)" />
				<row>
					<cell verticalalign="top" horizontalalign="left">
						<phrase lineheight="1.1" size="8"> &#160;&#160;&#160;&#160;<xsl:value-of select="Translations:GetTranslation('Detail','BVoorzieningA','BVoorzieningA')" />
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" size="8">
							<xsl:if test="BVoorzieningA[number()=number()]">
								<xsl:value-of select="format-number(number(BVoorzieningA[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" size="8">
							<xsl:if test="BVoorzieningB[number()=number()]">
								<xsl:value-of select="format-number(number(BVoorzieningB[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" size="8">
							<xsl:if test="BVoorzieningC[number()=number()]">
								<xsl:value-of select="format-number(number(BVoorzieningC[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" size="8">
							<xsl:if test="BVoorzieningD[number()=number()]">
								<xsl:value-of select="format-number(number(BVoorzieningD[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
				</row>
			</xsl:if>
		</table>
		<table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" columns="5" width="100%" align="Center" widths="52;12;12;12;12" cellspacing="0">
			<row>
				<cell colspan="5" verticalalign="top" horizontalalign="left" header="true">
					<phrase lineheight="1.1" size="10">
						<xsl:value-of select="Translations:GetTranslation('Detail','CTitle','CTitle')" />
					</phrase>
				</cell>
			</row>
			<xsl:variable name="dummyCTitle" select="Helper:ResetSum('3_')" />
			<xsl:if test="not(number(CNietVerwezMWA[number()=number()])=0 and number(CNietVerwezMWB[number()=number()])=0 and number(CNietVerwezMWC[number()=number()])=0 and number(CNietVerwezMWD[number()=number()])=0)">
				<xsl:variable name="dummyCTitleCNietVerwezMWA" select="Helper:AddSum('3_',1)" />
				<row>
					<cell verticalalign="top" horizontalalign="left">
						<phrase lineheight="1.1" style="italic" size="6"> &#160;&#160;&#160;&#160;<xsl:value-of select="Translations:GetTranslation('Detail','CNietVerwezMWA','CNietVerwezMWA')" />
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" style="italic" size="6">
							<xsl:if test="CNietVerwezMWA[number()=number()]">
								<xsl:value-of select="format-number(number(CNietVerwezMWA[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" style="italic" size="6">
							<xsl:if test="CNietVerwezMWB[number()=number()]">
								<xsl:value-of select="format-number(number(CNietVerwezMWB[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" style="italic" size="6">
							<xsl:if test="CNietVerwezMWC[number()=number()]">
								<xsl:value-of select="format-number(number(CNietVerwezMWC[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" style="italic" size="6">
							<xsl:if test="CNietVerwezMWD[number()=number()]">
								<xsl:value-of select="format-number(number(CNietVerwezMWD[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
				</row>
			</xsl:if>
			<xsl:if test="not(number(CHerschattingA[number()=number()])=0 and number(CHerschattingB[number()=number()])=0 and number(CHerschattingC[number()=number()])=0 and number(CHerschattingD[number()=number()])=0)">
				<xsl:variable name="dummyCTitleCHerschattingA" select="Helper:AddSum('3_',1)" />
				<row>
					<cell verticalalign="top" horizontalalign="left">
						<phrase lineheight="1.1" style="italic" size="6"> &#160;&#160;&#160;&#160;<xsl:value-of select="Translations:GetTranslation('Detail','CHerschattingA','CHerschattingA')" />
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" style="italic" size="6">
							<xsl:if test="CHerschattingA[number()=number()]">
								<xsl:value-of select="format-number(number(CHerschattingA[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" style="italic" size="6">
							<xsl:if test="CHerschattingB[number()=number()]">
								<xsl:value-of select="format-number(number(CHerschattingB[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" style="italic" size="6">
							<xsl:if test="CHerschattingC[number()=number()]">
								<xsl:value-of select="format-number(number(CHerschattingC[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" style="italic" size="6">
							<xsl:if test="CHerschattingD[number()=number()]">
								<xsl:value-of select="format-number(number(CHerschattingD[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
				</row>
			</xsl:if>
			<xsl:if test="not(number(CInbrengA[number()=number()])=0 and number(CInbrengB[number()=number()])=0 and number(CInbrengC[number()=number()])=0 and number(CInbrengD[number()=number()])=0)">
				<xsl:variable name="dummyCTitleCInbrengA" select="Helper:AddSum('3_',1)" />
				<row>
					<cell verticalalign="top" horizontalalign="left">
						<phrase lineheight="1.1" style="italic" size="6"> &#160;&#160;&#160;&#160;<xsl:value-of select="Translations:GetTranslation('Detail','CInbrengA','CInbrengA')" />
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" style="italic" size="6">
							<xsl:if test="CInbrengA[number()=number()]">
								<xsl:value-of select="format-number(number(CInbrengA[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" style="italic" size="6">
							<xsl:if test="CInbrengB[number()=number()]">
								<xsl:value-of select="format-number(number(CInbrengB[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" style="italic" size="6">
							<xsl:if test="CInbrengC[number()=number()]">
								<xsl:value-of select="format-number(number(CInbrengC[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" style="italic" size="6">
							<xsl:if test="CInbrengD[number()=number()]">
								<xsl:value-of select="format-number(number(CInbrengD[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
				</row>
			</xsl:if>
			<xsl:if test="not(number(CAndereA[number()=number()])=0 and number(CAndereB[number()=number()])=0 and number(CAndereC[number()=number()])=0 and number(CAndereD[number()=number()])=0)">
				<xsl:variable name="dummyCTitleCAndereA" select="Helper:AddSum('3_',1)" />
				<row>
					<cell verticalalign="top" horizontalalign="left">
						<phrase lineheight="1.1" style="italic" size="6"> &#160;&#160;&#160;&#160;<xsl:value-of select="Translations:GetTranslation('Detail','CAndereA','CAndereA')" />
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" style="italic" size="6">
							<xsl:if test="CAndereA[number()=number()]">
								<xsl:value-of select="format-number(number(CAndereA[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" style="italic" size="6">
							<xsl:if test="CAndereB[number()=number()]">
								<xsl:value-of select="format-number(number(CAndereB[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" style="italic" size="6">
							<xsl:if test="CAndereC[number()=number()]">
								<xsl:value-of select="format-number(number(CAndereC[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" style="italic" size="6">
							<xsl:if test="CAndereD[number()=number()]">
								<xsl:value-of select="format-number(number(CAndereD[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
				</row>
			</xsl:if>
			<xsl:if test="not(number(CTotaalA[number()=number()])=0 and number(CTotaalB[number()=number()])=0 and number(CTotaalC[number()=number()])=0 and number(CTotaalD[number()=number()])=0) and Helper:AddSum('3_',0)&gt;1">
				<xsl:variable name="dummyCTitleCTotaalA" select="Helper:AddSum('3_',1)" />
				<row>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" size="8"> &#160;&#160;&#160;&#160;<xsl:value-of select="Translations:GetTranslation('Detail','CTotaalA','CTotaalA')" />&#160;&#160;&#160;&#160; </phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" size="8">
							<xsl:if test="CTotaalA[number()=number()]">
								<xsl:value-of select="format-number(number(CTotaalA[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" size="8">
							<xsl:if test="CTotaalB[number()=number()]">
								<xsl:value-of select="format-number(number(CTotaalB[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" size="8">
							<xsl:if test="CTotaalC[number()=number()]">
								<xsl:value-of select="format-number(number(CTotaalC[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" size="8">
							<xsl:if test="CTotaalD[number()=number()]">
								<xsl:value-of select="format-number(number(CTotaalD[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
				</row>
			</xsl:if>
		</table>
		<table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" columns="5" width="100%" align="Center" widths="52;12;12;12;12" cellspacing="0">
			<row>
				<cell colspan="5" verticalalign="top" horizontalalign="left" header="true">
					<phrase lineheight="1.1" size="10">
						<xsl:value-of select="Translations:GetTranslation('Detail','DTitle','DTitle')" />
					</phrase>
				</cell>
			</row>
			<xsl:variable name="dummyDTitle" select="Helper:ResetSum('4_')" />
			<xsl:if test="not(number(DMonetairA[number()=number()])=0 and number(DMonetairB[number()=number()])=0 and number(DMonetairC[number()=number()])=0 and number(DMonetairD[number()=number()])=0)">
				<xsl:variable name="dummyDTitleDMonetairA" select="Helper:AddSum('4_',1)" />
				<row>
					<cell verticalalign="top" horizontalalign="left">
						<phrase lineheight="1.1" style="italic" size="6"> &#160;&#160;&#160;&#160;<xsl:value-of select="Translations:GetTranslation('Detail','DMonetairA','DMonetairA')" />
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" style="italic" size="6">
							<xsl:if test="DMonetairA[number()=number()]">
								<xsl:value-of select="format-number(number(DMonetairA[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" style="italic" size="6">
							<xsl:if test="DMonetairB[number()=number()]">
								<xsl:value-of select="format-number(number(DMonetairB[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" style="italic" size="6">
							<xsl:if test="DMonetairC[number()=number()]">
								<xsl:value-of select="format-number(number(DMonetairC[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" style="italic" size="6">
							<xsl:if test="DMonetairD[number()=number()]">
								<xsl:value-of select="format-number(number(DMonetairD[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
				</row>
			</xsl:if>
			<xsl:if test="not(number(DNietVerwezenA[number()=number()])=0 and number(DNietVerwezenB[number()=number()])=0 and number(DNietVerwezenC[number()=number()])=0 and number(DNietVerwezenD[number()=number()])=0)">
				<xsl:variable name="dummyDTitleDNietVerwezenA" select="Helper:AddSum('4_',1)" />
				<row>
					<cell verticalalign="top" horizontalalign="left">
						<phrase lineheight="1.1" style="italic" size="6"> &#160;&#160;&#160;&#160;<xsl:value-of select="Translations:GetTranslation('Detail','DNietVerwezenA','DNietVerwezenA')" />
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" style="italic" size="6">
							<xsl:if test="DNietVerwezenA[number()=number()]">
								<xsl:value-of select="format-number(number(DNietVerwezenA[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" style="italic" size="6">
							<xsl:if test="DNietVerwezenB[number()=number()]">
								<xsl:value-of select="format-number(number(DNietVerwezenB[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" style="italic" size="6">
							<xsl:if test="DNietVerwezenC[number()=number()]">
								<xsl:value-of select="format-number(number(DNietVerwezenC[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" style="italic" size="6">
							<xsl:if test="DNietVerwezenD[number()=number()]">
								<xsl:value-of select="format-number(number(DNietVerwezenD[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
				</row>
			</xsl:if>
			<xsl:if test="not(number(DBedrijfsAfdelingenA[number()=number()])=0 and number(DBedrijfsAfdelingenB[number()=number()])=0 and number(DBedrijfsAfdelingenC[number()=number()])=0 and number(DBedrijfsAfdelingenD[number()=number()])=0)">
				<xsl:variable name="dummyDTitleDBedrijfsAfdelingenA" select="Helper:AddSum('4_',1)" />
				<row>
					<cell verticalalign="top" horizontalalign="left">
						<phrase lineheight="1.1" style="italic" size="6"> &#160;&#160;&#160;&#160;<xsl:value-of select="Translations:GetTranslation('Detail','DBedrijfsAfdelingenA','DBedrijfsAfdelingenA')" />
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" style="italic" size="6">
							<xsl:if test="DBedrijfsAfdelingenA[number()=number()]">
								<xsl:value-of select="format-number(number(DBedrijfsAfdelingenA[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" style="italic" size="6">
							<xsl:if test="DBedrijfsAfdelingenB[number()=number()]">
								<xsl:value-of select="format-number(number(DBedrijfsAfdelingenB[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" style="italic" size="6">
							<xsl:if test="DBedrijfsAfdelingenC[number()=number()]">
								<xsl:value-of select="format-number(number(DBedrijfsAfdelingenC[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" style="italic" size="6">
							<xsl:if test="DBedrijfsAfdelingenD[number()=number()]">
								<xsl:value-of select="format-number(number(DBedrijfsAfdelingenD[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
				</row>
			</xsl:if>
			<xsl:if test="not(number(DGedwongenA[number()=number()])=0 and number(DGedwongenB[number()=number()])=0 and number(DGedwongenC[number()=number()])=0 and number(DGedwongenD[number()=number()])=0)">
				<xsl:variable name="dummyDTitleDGedwongenA" select="Helper:AddSum('4_',1)" />
				<row>
					<cell verticalalign="top" horizontalalign="left">
						<phrase lineheight="1.1" style="italic" size="6"> &#160;&#160;&#160;&#160;<xsl:value-of select="Translations:GetTranslation('Detail','DGedwongenA','DGedwongenA')" />
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" style="italic" size="6">
							<xsl:if test="DGedwongenA[number()=number()]">
								<xsl:value-of select="format-number(number(DGedwongenA[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" style="italic" size="6">
							<xsl:if test="DGedwongenB[number()=number()]">
								<xsl:value-of select="format-number(number(DGedwongenB[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" style="italic" size="6">
							<xsl:if test="DGedwongenC[number()=number()]">
								<xsl:value-of select="format-number(number(DGedwongenC[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" style="italic" size="6">
							<xsl:if test="DGedwongenD[number()=number()]">
								<xsl:value-of select="format-number(number(DGedwongenD[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
				</row>
			</xsl:if>
			<xsl:if test="not(number(DNietMonetairA[number()=number()])=0 and number(DNietMonetairB[number()=number()])=0 and number(DNietMonetairC[number()=number()])=0 and number(DNietMonetairD[number()=number()])=0)">
				<xsl:variable name="dummyDTitleDNietMonetairA" select="Helper:AddSum('4_',1)" />
				<row>
					<cell verticalalign="top" horizontalalign="left">
						<phrase lineheight="1.1" style="italic" size="6"> &#160;&#160;&#160;&#160;<xsl:value-of select="Translations:GetTranslation('Detail','DNietMonetairA','DNietMonetairA')" />
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" style="italic" size="6">
							<xsl:if test="DNietMonetairA[number()=number()]">
								<xsl:value-of select="format-number(number(DNietMonetairA[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" style="italic" size="6">
							<xsl:if test="DNietMonetairB[number()=number()]">
								<xsl:value-of select="format-number(number(DNietMonetairB[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" style="italic" size="6">
							<xsl:if test="DNietMonetairC[number()=number()]">
								<xsl:value-of select="format-number(number(DNietMonetairC[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" style="italic" size="6">
							<xsl:if test="DNietMonetairD[number()=number()]">
								<xsl:value-of select="format-number(number(DNietMonetairD[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
				</row>
			</xsl:if>
			<xsl:if test="not(number(DVastgoedHandelaarsA[number()=number()])=0 and number(DVastgoedHandelaarsB[number()=number()])=0 and number(DVastgoedHandelaarsC[number()=number()])=0 and number(DVastgoedHandelaarsD[number()=number()])=0)">
				<xsl:variable name="dummyDTitleDVastgoedHandelaarsA" select="Helper:AddSum('4_',1)" />
				<row>
					<cell verticalalign="top" horizontalalign="left">
						<phrase lineheight="1.1" style="italic" size="6"> &#160;&#160;&#160;&#160;<xsl:value-of select="Translations:GetTranslation('Detail','DVastgoedHandelaarsA','DVastgoedHandelaarsA')" />
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" style="italic" size="6">
							<xsl:if test="DVastgoedHandelaarsA[number()=number()]">
								<xsl:value-of select="format-number(number(DVastgoedHandelaarsA[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" style="italic" size="6">
							<xsl:if test="DVastgoedHandelaarsB[number()=number()]">
								<xsl:value-of select="format-number(number(DVastgoedHandelaarsB[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" style="italic" size="6">
							<xsl:if test="DVastgoedHandelaarsC[number()=number()]">
								<xsl:value-of select="format-number(number(DVastgoedHandelaarsC[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" style="italic" size="6">
							<xsl:if test="DVastgoedHandelaarsD[number()=number()]">
								<xsl:value-of select="format-number(number(DVastgoedHandelaarsD[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
				</row>
			</xsl:if>
			<xsl:if test="not(number(DProbleemgebiedenA[number()=number()])=0 and number(DProbleemgebiedenB[number()=number()])=0 and number(DProbleemgebiedenC[number()=number()])=0 and number(DProbleemgebiedenD[number()=number()])=0)">
				<xsl:variable name="dummyDTitleDProbleemgebiedenA" select="Helper:AddSum('4_',1)" />
				<row>
					<cell verticalalign="top" horizontalalign="left">
						<phrase lineheight="1.1" style="italic" size="6"> &#160;&#160;&#160;&#160;<xsl:value-of select="Translations:GetTranslation('Detail','DProbleemgebiedenA','DProbleemgebiedenA')" />
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" style="italic" size="6">
							<xsl:if test="DProbleemgebiedenA[number()=number()]">
								<xsl:value-of select="format-number(number(DProbleemgebiedenA[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" style="italic" size="6">
							<xsl:if test="DProbleemgebiedenB[number()=number()]">
								<xsl:value-of select="format-number(number(DProbleemgebiedenB[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" style="italic" size="6">
							<xsl:if test="DProbleemgebiedenC[number()=number()]">
								<xsl:value-of select="format-number(number(DProbleemgebiedenC[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" style="italic" size="6">
							<xsl:if test="DProbleemgebiedenD[number()=number()]">
								<xsl:value-of select="format-number(number(DProbleemgebiedenD[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
				</row>
			</xsl:if>
			<xsl:if test="not(number(DAndereA[number()=number()])=0 and number(DAndereB[number()=number()])=0 and number(DAndereC[number()=number()])=0 and number(DAndereD[number()=number()])=0)">
				<xsl:variable name="dummyDTitleDAndereA" select="Helper:AddSum('4_',1)" />
				<row>
					<cell verticalalign="top" horizontalalign="left">
						<phrase lineheight="1.1" style="italic" size="6"> &#160;&#160;&#160;&#160;<xsl:value-of select="Translations:GetTranslation('Detail','DAndereA','DAndereA')" />
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" style="italic" size="6">
							<xsl:if test="DAndereA[number()=number()]">
								<xsl:value-of select="format-number(number(DAndereA[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" style="italic" size="6">
							<xsl:if test="DAndereB[number()=number()]">
								<xsl:value-of select="format-number(number(DAndereB[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" style="italic" size="6">
							<xsl:if test="DAndereC[number()=number()]">
								<xsl:value-of select="format-number(number(DAndereC[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" style="italic" size="6">
							<xsl:if test="DAndereD[number()=number()]">
								<xsl:value-of select="format-number(number(DAndereD[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
				</row>
			</xsl:if>
			<xsl:if test="not(number(DTotaalA[number()=number()])=0 and number(DTotaalB[number()=number()])=0 and number(DTotaalC[number()=number()])=0 and number(DTotaalD[number()=number()])=0) and Helper:AddSum('4_',0)&gt;1">
				<xsl:variable name="dummyDTitleDTotaalA" select="Helper:AddSum('4_',1)" />
				<row>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" size="8"> &#160;&#160;&#160;&#160;<xsl:value-of select="Translations:GetTranslation('Detail','DTotaalA','DTotaalA')" />&#160;&#160;&#160;&#160; </phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" size="8">
							<xsl:if test="DTotaalA[number()=number()]">
								<xsl:value-of select="format-number(number(DTotaalA[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" size="8">
							<xsl:if test="DTotaalB[number()=number()]">
								<xsl:value-of select="format-number(number(DTotaalB[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" size="8">
							<xsl:if test="DTotaalC[number()=number()]">
								<xsl:value-of select="format-number(number(DTotaalC[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" size="8">
							<xsl:if test="DTotaalD[number()=number()]">
								<xsl:value-of select="format-number(number(DTotaalD[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
				</row>
			</xsl:if>
		</table>
		<table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" columns="5" width="100%" align="Center" widths="52;12;12;12;12" cellspacing="0">
			<row>
				<cell colspan="5" verticalalign="top" horizontalalign="left" header="true">
					<phrase lineheight="1.1" size="10">
						<xsl:value-of select="Translations:GetTranslation('Detail','ETitle','ETitle')" />
					</phrase>
				</cell>
			</row>
			<xsl:variable name="dummyETitle" select="Helper:ResetSum('5_')" />
			<xsl:if test="not(number(EGespreidVerwezenA[number()=number()])=0 and number(EGespreidVerwezenB[number()=number()])=0 and number(EGespreidVerwezenC[number()=number()])=0 and number(EGespreidVerwezenD[number()=number()])=0)">
				<xsl:variable name="dummyETitleEGespreidVerwezenA" select="Helper:AddSum('5_',1)" />
				<row>
					<cell verticalalign="top" horizontalalign="left">
						<phrase lineheight="1.1" size="8"> &#160;&#160;&#160;&#160;<xsl:value-of select="Translations:GetTranslation('Detail','EGespreidVerwezenA','EGespreidVerwezenA')" />
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" size="8">
							<xsl:if test="EGespreidVerwezenA[number()=number()]">
								<xsl:value-of select="format-number(number(EGespreidVerwezenA[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" size="8">
							<xsl:if test="EGespreidVerwezenB[number()=number()]">
								<xsl:value-of select="format-number(number(EGespreidVerwezenB[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" size="8">
							<xsl:if test="EGespreidVerwezenC[number()=number()]">
								<xsl:value-of select="format-number(number(EGespreidVerwezenC[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" size="8">
							<xsl:if test="EGespreidVerwezenD[number()=number()]">
								<xsl:value-of select="format-number(number(EGespreidVerwezenD[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
				</row>
			</xsl:if>
			<xsl:if test="not(number(EMatImmatVasteActivaA[number()=number()])=0 and number(EMatImmatVasteActivaB[number()=number()])=0 and number(EMatImmatVasteActivaC[number()=number()])=0 and number(EMatImmatVasteActivaD[number()=number()])=0)">
				<xsl:variable name="dummyETitleEGespreidVerwezenA" select="Helper:AddSum('5_',1)" />
				<row>
					<cell verticalalign="top" horizontalalign="left">
						<phrase lineheight="1.1" size="8"> &#160;&#160;&#160;&#160;<xsl:value-of select="Translations:GetTranslation('Detail','EMatImmatVasteActivaA','EMatImmatVasteActivaA')" />
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" size="8">
							<xsl:if test="EMatImmatVasteActivaA[number()=number()]">
								<xsl:value-of select="format-number(number(EMatImmatVasteActivaA[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" size="8">
							<xsl:if test="EMatImmatVasteActivaB[number()=number()]">
								<xsl:value-of select="format-number(number(EMatImmatVasteActivaB[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" size="8">
							<xsl:if test="EMatImmatVasteActivaC[number()=number()]">
								<xsl:value-of select="format-number(number(EMatImmatVasteActivaC[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" size="8">
							<xsl:if test="EMatImmatVasteActivaD[number()=number()]">
								<xsl:value-of select="format-number(number(EMatImmatVasteActivaD[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
				</row>
			</xsl:if>
		</table>
		<table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" columns="5" width="100%" align="Center" widths="52;12;12;12;12" cellspacing="0">
			<row>
				<cell colspan="5" verticalalign="top" horizontalalign="left" header="true">
					<phrase lineheight="1.1" size="10">
						<xsl:value-of select="Translations:GetTranslation('Detail','FTitle','FTitle')" />
					</phrase>
				</cell>
			</row>
			<xsl:variable name="dummyFTitle" select="Helper:ResetSum('6_')" />
			<xsl:if test="not(number(FBedrijfsvoertuigenA[number()=number()])=0 and number(FBedrijfsvoertuigenB[number()=number()])=0 and number(FBedrijfsvoertuigenC[number()=number()])=0 and number(FBedrijfsvoertuigenD[number()=number()])=0)">
				<xsl:variable name="dummyFTitleFBedrijfsvoertuigenA" select="Helper:AddSum('6_',1)" />
				<row>
					<cell verticalalign="top" horizontalalign="left">
						<phrase lineheight="1.1" size="8"> &#160;&#160;&#160;&#160;<xsl:value-of select="Translations:GetTranslation('Detail','FBedrijfsvoertuigenA','FBedrijfsvoertuigenA')" />
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" size="8">
							<xsl:if test="FBedrijfsvoertuigenA[number()=number()]">
								<xsl:value-of select="format-number(number(FBedrijfsvoertuigenA[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" size="8">
							<xsl:if test="FBedrijfsvoertuigenB[number()=number()]">
								<xsl:value-of select="format-number(number(FBedrijfsvoertuigenB[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" size="8">
							<xsl:if test="FBedrijfsvoertuigenC[number()=number()]">
								<xsl:value-of select="format-number(number(FBedrijfsvoertuigenC[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" size="8">
							<xsl:if test="FBedrijfsvoertuigenD[number()=number()]">
								<xsl:value-of select="format-number(number(FBedrijfsvoertuigenD[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
				</row>
			</xsl:if>
		</table>
		<table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" columns="5" width="100%" align="Center" widths="52;12;12;12;12" cellspacing="0">
			<row>
				<cell colspan="5" verticalalign="top" horizontalalign="left" header="true">
					<phrase lineheight="1.1" size="10">
						<xsl:value-of select="Translations:GetTranslation('Detail','GTitle','GTitle')" />
					</phrase>
				</cell>
			</row>
			<xsl:variable name="dummyGTitle" select="Helper:ResetSum('7_')" />
			<xsl:if test="not(number(GBinnenschepenA[number()=number()])=0 and number(GBinnenschepenB[number()=number()])=0 and number(GBinnenschepenC[number()=number()])=0 and number(GBinnenschepenD[number()=number()])=0)">
				<xsl:variable name="dummyGTitleGBinnenschepenA" select="Helper:AddSum('7_',1)" />
				<row>
					<cell verticalalign="top" horizontalalign="left">
						<phrase lineheight="1.1" size="8"> &#160;&#160;&#160;&#160;<xsl:value-of select="Translations:GetTranslation('Detail','GBinnenschepenA','GBinnenschepenA')" />
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" size="8">
							<xsl:if test="GBinnenschepenA[number()=number()]">
								<xsl:value-of select="format-number(number(GBinnenschepenA[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" size="8">
							<xsl:if test="GBinnenschepenB[number()=number()]">
								<xsl:value-of select="format-number(number(GBinnenschepenB[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" size="8">
							<xsl:if test="GBinnenschepenC[number()=number()]">
								<xsl:value-of select="format-number(number(GBinnenschepenC[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" size="8">
							<xsl:if test="GBinnenschepenD[number()=number()]">
								<xsl:value-of select="format-number(number(GBinnenschepenD[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
				</row>
			</xsl:if>
		</table>
		<table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" columns="5" width="100%" align="Center" widths="52;12;12;12;12" cellspacing="0">
			<row>
				<cell colspan="5" verticalalign="top" horizontalalign="left" header="true">
					<phrase lineheight="1.1" size="10">
						<xsl:value-of select="Translations:GetTranslation('Detail','HTitle','HTitle')" />
					</phrase>
				</cell>
			</row>
			<xsl:variable name="dummyHTitle" select="Helper:ResetSum('8_')" />
			<xsl:if test="not(number(HZeeschepenA[number()=number()])=0 and number(HZeeschepenB[number()=number()])=0 and number(HZeeschepenC[number()=number()])=0 and number(HZeeschepenD[number()=number()])=0)">
				<xsl:variable name="dummyHTitleHZeeschepenA" select="Helper:AddSum('8_',1)" />
				<row>
					<cell verticalalign="top" horizontalalign="left">
						<phrase lineheight="1.1" size="8"> &#160;&#160;&#160;&#160;<xsl:value-of select="Translations:GetTranslation('Detail','HZeeschepenA','HZeeschepenA')" />
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" size="8">
							<xsl:if test="HZeeschepenA[number()=number()]">
								<xsl:value-of select="format-number(number(HZeeschepenA[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" size="8">
							<xsl:if test="HZeeschepenB[number()=number()]">
								<xsl:value-of select="format-number(number(HZeeschepenB[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" size="8">
							<xsl:if test="HZeeschepenC[number()=number()]">
								<xsl:value-of select="format-number(number(HZeeschepenC[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" size="8">
							<xsl:if test="HZeeschepenD[number()=number()]">
								<xsl:value-of select="format-number(number(HZeeschepenD[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
				</row>
			</xsl:if>
		</table>
		<table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" columns="5" width="100%" align="Center" widths="52;12;12;12;12" cellspacing="0">
			<row>
				<cell colspan="5" verticalalign="top" horizontalalign="left" header="true">
					<phrase lineheight="1.1" size="10">
						<xsl:value-of select="Translations:GetTranslation('Detail','ITitle','ITitle')" />
					</phrase>
				</cell>
			</row>
			<xsl:variable name="dummyITitle" select="Helper:ResetSum('9_')" />
			<xsl:if test="not(number(IInvesteringsreserveA[number()=number()])=0 and number(IInvesteringsreserveB[number()=number()])=0 and number(IInvesteringsreserveC[number()=number()])=0 and number(IInvesteringsreserveD[number()=number()])=0)">
				<xsl:variable name="dummyITitleIInvesteringsreserveA" select="Helper:AddSum('9_',1)" />
				<row>
					<cell verticalalign="top" horizontalalign="left">
						<phrase lineheight="1.1" size="8"> &#160;&#160;&#160;&#160;<xsl:value-of select="Translations:GetTranslation('Detail','IInvesteringsreserveA','IInvesteringsreserveA')" />
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" size="8">
							<xsl:if test="IInvesteringsreserveA[number()=number()]">
								<xsl:value-of select="format-number(number(IInvesteringsreserveA[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" size="8">
							<xsl:if test="IInvesteringsreserveB[number()=number()]">
								<xsl:value-of select="format-number(number(IInvesteringsreserveB[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" size="8">
							<xsl:if test="IInvesteringsreserveC[number()=number()]">
								<xsl:value-of select="format-number(number(IInvesteringsreserveC[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" size="8">
							<xsl:if test="IInvesteringsreserveD[number()=number()]">
								<xsl:value-of select="format-number(number(IInvesteringsreserveD[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
				</row>
			</xsl:if>
		</table>
		<table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" columns="5" width="100%" align="Center" widths="52;12;12;12;12" cellspacing="0">
			<row>
				<cell colspan="5" verticalalign="top" horizontalalign="left" header="true">
					<phrase lineheight="1.1" size="10">
						<xsl:value-of select="Translations:GetTranslation('Detail','JTitle','JTitle')" />
					</phrase>
				</cell>
			</row>
			<xsl:variable name="dummyJTitle" select="Helper:ResetSum('10_')" />
			<xsl:if test="not(number(JTaxShelterAudioVisueelA[number()=number()])=0 and number(JTaxShelterAudioVisueelB[number()=number()])=0 and number(JTaxShelterAudioVisueelC[number()=number()])=0 and number(JTaxShelterAudioVisueelD[number()=number()])=0)">
				<xsl:variable name="dummyJTitleJTaxShelterAudioVisueelA" select="Helper:AddSum('10_',1)" />
				<row>
					<cell verticalalign="top" horizontalalign="left">
						<phrase lineheight="1.1" size="8"> &#160;&#160;&#160;&#160;<xsl:value-of select="Translations:GetTranslation('Detail','JTaxShelterAudioVisueelA','JTaxShelterAudioVisueelA')" />
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" size="8">
							<xsl:if test="JTaxShelterAudioVisueelA[number()=number()]">
								<xsl:value-of select="format-number(number(JTaxShelterAudioVisueelA[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" size="8">
							<xsl:if test="JTaxShelterAudioVisueelB[number()=number()]">
								<xsl:value-of select="format-number(number(JTaxShelterAudioVisueelB[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" size="8">
							<xsl:if test="JTaxShelterAudioVisueelC[number()=number()]">
								<xsl:value-of select="format-number(number(JTaxShelterAudioVisueelC[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" size="8">
							<xsl:if test="JTaxShelterAudioVisueelD[number()=number()]">
								<xsl:value-of select="format-number(number(JTaxShelterAudioVisueelD[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
				</row>
			</xsl:if>
			<xsl:if test="not(number(JWinstHomologatieA[number()=number()])=0 and number(JWinstHomologatieB[number()=number()])=0 and number(JWinstHomologatieC[number()=number()])=0 and number(JWinstHomologatieD[number()=number()])=0)">
				<xsl:variable name="dummyJTitleJWinstHomologatieA" select="Helper:AddSum('10_',1)" />
				<row>
					<cell verticalalign="top" horizontalalign="left">
						<phrase lineheight="1.1" size="8"> &#160;&#160;&#160;&#160;<xsl:value-of select="Translations:GetTranslation('Detail','JWinstHomologatieA','JWinstHomologatieA')" />
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" size="8">
							<xsl:if test="JWinstHomologatieA[number()=number()]">
								<xsl:value-of select="format-number(number(JWinstHomologatieA[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" size="8">
							<xsl:if test="JWinstHomologatieB[number()=number()]">
								<xsl:value-of select="format-number(number(JWinstHomologatieB[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" size="8">
							<xsl:if test="JWinstHomologatieC[number()=number()]">
								<xsl:value-of select="format-number(number(JWinstHomologatieC[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" size="8">
							<xsl:if test="JWinstHomologatieD[number()=number()]">
								<xsl:value-of select="format-number(number(JWinstHomologatieD[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
				</row>
			</xsl:if>
		</table>
		<table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" columns="5" width="100%" align="Center" widths="52;12;12;12;12" cellspacing="0">
			<row>
				<cell colspan="5" verticalalign="top" horizontalalign="left" header="true">
					<phrase lineheight="1.1" size="10">
						<xsl:value-of select="Translations:GetTranslation('Detail','KTitle','KTitle')" />
					</phrase>
				</cell>
			</row>
			<xsl:variable name="dummyKTitle" select="Helper:ResetSum('11_')" />
			<xsl:if test="not(number(KKapitaalSubsidiesA[number()=number()])=0 and number(KKapitaalSubsidiesB[number()=number()])=0 and number(KKapitaalSubsidiesC[number()=number()])=0 and number(KKapitaalSubsidiesD[number()=number()])=0)">
				<xsl:variable name="dummyKTitleKKapitaalSubsidiesA" select="Helper:AddSum('11_',1)" />
				<row>
					<cell verticalalign="top" horizontalalign="left">
						<phrase lineheight="1.1" style="italic" size="6"> &#160;&#160;&#160;&#160;<xsl:value-of select="Translations:GetTranslation('Detail','KKapitaalSubsidiesA','KKapitaalSubsidiesA')" />
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" style="italic" size="6">
							<xsl:if test="KKapitaalSubsidiesA[number()=number()]">
								<xsl:value-of select="format-number(number(KKapitaalSubsidiesA[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" style="italic" size="6">
							<xsl:if test="KKapitaalSubsidiesB[number()=number()]">
								<xsl:value-of select="format-number(number(KKapitaalSubsidiesB[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" style="italic" size="6">
							<xsl:if test="KKapitaalSubsidiesC[number()=number()]">
								<xsl:value-of select="format-number(number(KKapitaalSubsidiesC[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" style="italic" size="6">
							<xsl:if test="KKapitaalSubsidiesD[number()=number()]">
								<xsl:value-of select="format-number(number(KKapitaalSubsidiesD[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
				</row>
			</xsl:if>
			<xsl:if test="not(number(KGeorganisGemVervoerA[number()=number()])=0 and number(KGeorganisGemVervoerB[number()=number()])=0 and number(KGeorganisGemVervoerC[number()=number()])=0 and number(KGeorganisGemVervoerD[number()=number()])=0)">
				<xsl:variable name="dummyKTitleKGeorganisGemVervoerA" select="Helper:AddSum('11_',1)" />
				<row>
					<cell verticalalign="top" horizontalalign="left">
						<phrase lineheight="1.1" style="italic" size="6"> &#160;&#160;&#160;&#160;<xsl:value-of select="Translations:GetTranslation('Detail','KGeorganisGemVervoerA','KGeorganisGemVervoerA')" />
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" style="italic" size="6">
							<xsl:if test="KGeorganisGemVervoerA[number()=number()]">
								<xsl:value-of select="format-number(number(KGeorganisGemVervoerA[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" style="italic" size="6">
							<xsl:if test="KGeorganisGemVervoerB[number()=number()]">
								<xsl:value-of select="format-number(number(KGeorganisGemVervoerB[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" style="italic" size="6">
							<xsl:if test="KGeorganisGemVervoerC[number()=number()]">
								<xsl:value-of select="format-number(number(KGeorganisGemVervoerC[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" style="italic" size="6">
							<xsl:if test="KGeorganisGemVervoerD[number()=number()]">
								<xsl:value-of select="format-number(number(KGeorganisGemVervoerD[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
				</row>
			</xsl:if>
			<xsl:if test="not(number(KBeveiligingA[number()=number()])=0 and number(KBeveiligingB[number()=number()])=0 and number(KBeveiligingC[number()=number()])=0 and number(KBeveiligingD[number()=number()])=0)">
				<xsl:variable name="dummyKTitleKBeveiligingA" select="Helper:AddSum('11_',1)" />
				<row>
					<cell verticalalign="top" horizontalalign="left">
						<phrase lineheight="1.1" style="italic" size="6"> &#160;&#160;&#160;&#160;<xsl:value-of select="Translations:GetTranslation('Detail','KBeveiligingA','KBeveiligingA')" />
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" style="italic" size="6">
							<xsl:if test="KBeveiligingA[number()=number()]">
								<xsl:value-of select="format-number(number(KBeveiligingA[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" style="italic" size="6">
							<xsl:if test="KBeveiligingB[number()=number()]">
								<xsl:value-of select="format-number(number(KBeveiligingB[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" style="italic" size="6">
							<xsl:if test="KBeveiligingC[number()=number()]">
								<xsl:value-of select="format-number(number(KBeveiligingC[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" style="italic" size="6">
							<xsl:if test="KBeveiligingD[number()=number()]">
								<xsl:value-of select="format-number(number(KBeveiligingD[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
				</row>
			</xsl:if>
			<xsl:if test="not(number(KBedrijfsfietsenA[number()=number()])=0 and number(KBedrijfsfietsenB[number()=number()])=0 and number(KBedrijfsfietsenC[number()=number()])=0 and number(KBedrijfsfietsenD[number()=number()])=0)">
				<xsl:variable name="dummyKTitleKBedrijfsfietsenA" select="Helper:AddSum('11_',1)" />
				<row>
					<cell verticalalign="top" horizontalalign="left">
						<phrase lineheight="1.1" style="italic" size="6"> &#160;&#160;&#160;&#160;<xsl:value-of select="Translations:GetTranslation('Detail','KBedrijfsfietsenA','KBedrijfsfietsenA')" />
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" style="italic" size="6">
							<xsl:if test="KBedrijfsfietsenA[number()=number()]">
								<xsl:value-of select="format-number(number(KBedrijfsfietsenA[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" style="italic" size="6">
							<xsl:if test="KBedrijfsfietsenB[number()=number()]">
								<xsl:value-of select="format-number(number(KBedrijfsfietsenB[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" style="italic" size="6">
							<xsl:if test="KBedrijfsfietsenC[number()=number()]">
								<xsl:value-of select="format-number(number(KBedrijfsfietsenC[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" style="italic" size="6">
							<xsl:if test="KBedrijfsfietsenD[number()=number()]">
								<xsl:value-of select="format-number(number(KBedrijfsfietsenD[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
				</row>
			</xsl:if>
			<xsl:if test="not(number(KC02UitstootloosA[number()=number()])=0 and number(KC02UitstootloosB[number()=number()])=0 and number(KC02UitstootloosC[number()=number()])=0 and number(KC02UitstootloosD[number()=number()])=0)">
				<xsl:variable name="dummyKTitleKC02UitstootloosA" select="Helper:AddSum('11_',1)" />
				<row>
					<cell verticalalign="top" horizontalalign="left">
						<phrase lineheight="1.1" style="italic" size="6"> &#160;&#160;&#160;&#160;<xsl:value-of select="Translations:GetTranslation('Detail','KC02UitstootloosA','KC02UitstootloosA')" />
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" style="italic" size="6">
							<xsl:if test="KC02UitstootloosA[number()=number()]">
								<xsl:value-of select="format-number(number(KC02UitstootloosA[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" style="italic" size="6">
							<xsl:if test="KC02UitstootloosB[number()=number()]">
								<xsl:value-of select="format-number(number(KC02UitstootloosB[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" style="italic" size="6">
							<xsl:if test="KC02UitstootloosC[number()=number()]">
								<xsl:value-of select="format-number(number(KC02UitstootloosC[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" style="italic" size="6">
							<xsl:if test="KC02UitstootloosD[number()=number()]">
								<xsl:value-of select="format-number(number(KC02UitstootloosD[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
				</row>
			</xsl:if>
			<xsl:if test="not(number(KFIVReconversieVennA[number()=number()])=0 and number(KFIVReconversieVennB[number()=number()])=0 and number(KFIVReconversieVennC[number()=number()])=0 and number(KFIVReconversieVennD[number()=number()])=0)">
				<xsl:variable name="dummyKTitleKFIVReconversieVennA" select="Helper:AddSum('11_',1)" />
				<row>
					<cell verticalalign="top" horizontalalign="left">
						<phrase lineheight="1.1" style="italic" size="6"> &#160;&#160;&#160;&#160;<xsl:value-of select="Translations:GetTranslation('Detail','KFIVReconversieVennA','KFIVReconversieVennA')" />
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" style="italic" size="6">
							<xsl:if test="KFIVReconversieVennA[number()=number()]">
								<xsl:value-of select="format-number(number(KFIVReconversieVennA[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" style="italic" size="6">
							<xsl:if test="KFIVReconversieVennB[number()=number()]">
								<xsl:value-of select="format-number(number(KFIVReconversieVennB[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" style="italic" size="6">
							<xsl:if test="KFIVReconversieVennC[number()=number()]">
								<xsl:value-of select="format-number(number(KFIVReconversieVennC[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" style="italic" size="6">
							<xsl:if test="KFIVReconversieVennD[number()=number()]">
								<xsl:value-of select="format-number(number(KFIVReconversieVennD[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
				</row>
			</xsl:if>
			<xsl:if test="not(number(KVrijWinstInnovatieVennA[number()=number()])=0 and number(KVrijWinstInnovatieVennB[number()=number()])=0 and number(KVrijWinstInnovatieVennC[number()=number()])=0 and number(KVrijWinstInnovatieVennD[number()=number()])=0)">
				<xsl:variable name="dummyKTitleKVrijWinstInnovatieVennA" select="Helper:AddSum('11_',1)" />
				<row>
					<cell verticalalign="top" horizontalalign="left">
						<phrase lineheight="1.1" style="italic" size="6"> &#160;&#160;&#160;&#160;<xsl:value-of select="Translations:GetTranslation('Detail','KVrijWinstInnovatieVennA','KVrijWinstInnovatieVennA')" />
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" style="italic" size="6">
							<xsl:if test="KVrijWinstInnovatieVennA[number()=number()]">
								<xsl:value-of select="format-number(number(KVrijWinstInnovatieVennA[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" style="italic" size="6">
							<xsl:if test="KVrijWinstInnovatieVennB[number()=number()]">
								<xsl:value-of select="format-number(number(KVrijWinstInnovatieVennB[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" style="italic" size="6">
							<xsl:if test="KVrijWinstInnovatieVennC[number()=number()]">
								<xsl:value-of select="format-number(number(KVrijWinstInnovatieVennC[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" style="italic" size="6">
							<xsl:if test="KVrijWinstInnovatieVennD[number()=number()]">
								<xsl:value-of select="format-number(number(KVrijWinstInnovatieVennD[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
				</row>
			</xsl:if>
			<xsl:if test="not(number(KSociaalPassiefA[number()=number()])=0 and number(KSociaalPassiefB[number()=number()])=0 and number(KSociaalPassiefC[number()=number()])=0 and number(KSociaalPassiefD[number()=number()])=0)">
				<xsl:variable name="dummyKTitleKSociaalPassiefA" select="Helper:AddSum('11_',1)" />
				<row>
					<cell verticalalign="top" horizontalalign="left">
						<phrase lineheight="1.1" style="italic" size="6"> &#160;&#160;&#160;&#160;<xsl:value-of select="Translations:GetTranslation('Detail','KSociaalPassiefA','KSociaalPassiefA')" />
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" style="italic" size="6">
							<xsl:if test="KSociaalPassiefA[number()=number()]">
								<xsl:value-of select="format-number(number(KSociaalPassiefA[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" style="italic" size="6">
							<xsl:if test="KSociaalPassiefB[number()=number()]">
								<xsl:value-of select="format-number(number(KSociaalPassiefB[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" style="italic" size="6">
							<xsl:if test="KSociaalPassiefC[number()=number()]">
								<xsl:value-of select="format-number(number(KSociaalPassiefC[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" style="italic" size="6">
							<xsl:if test="KSociaalPassiefD[number()=number()]">
								<xsl:value-of select="format-number(number(KSociaalPassiefD[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
				</row>
			</xsl:if>
			<xsl:if test="not(number(KAfschrAanschafBelegWaardeA[number()=number()])=0 and number(KAfschrAanschafBelegWaardeB[number()=number()])=0 and number(KAfschrAanschafBelegWaardeC[number()=number()])=0 and number(KAfschrAanschafBelegWaardeD[number()=number()])=0)">
				<xsl:variable name="dummyKTitleKAfschrAanschafBelegWaardeA" select="Helper:AddSum('11_',1)" />
				<row>
					<cell verticalalign="top" horizontalalign="left">
						<phrase lineheight="1.1" style="italic" size="6"> &#160;&#160;&#160;&#160;<xsl:value-of select="Translations:GetTranslation('Detail','KAfschrAanschafBelegWaardeA','KAfschrAanschafBelegWaardeA')" />
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" style="italic" size="6">
							<xsl:if test="KAfschrAanschafBelegWaardeA[number()=number()]">
								<xsl:value-of select="format-number(number(KAfschrAanschafBelegWaardeA[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" style="italic" size="6">
							<xsl:if test="KAfschrAanschafBelegWaardeB[number()=number()]">
								<xsl:value-of select="format-number(number(KAfschrAanschafBelegWaardeB[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" style="italic" size="6">
							<xsl:if test="KAfschrAanschafBelegWaardeC[number()=number()]">
								<xsl:value-of select="format-number(number(KAfschrAanschafBelegWaardeC[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" style="italic" size="6">
							<xsl:if test="KAfschrAanschafBelegWaardeD[number()=number()]">
								<xsl:value-of select="format-number(number(KAfschrAanschafBelegWaardeD[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
				</row>
			</xsl:if>
			<xsl:if test="not(number(KProvisiesSocVoordA[number()=number()])=0 and number(KProvisiesSocVoordB[number()=number()])=0 and number(KProvisiesSocVoordC[number()=number()])=0 and number(KProvisiesSocVoordD[number()=number()])=0)">
				<xsl:variable name="dummyKTitleKProvisiesSocVoordA" select="Helper:AddSum('11_',1)" />
				<row>
					<cell verticalalign="top" horizontalalign="left">
						<phrase lineheight="1.1" style="italic" size="6"> &#160;&#160;&#160;&#160;<xsl:value-of select="Translations:GetTranslation('Detail','KProvisiesSocVoordA','KProvisiesSocVoordA')" />
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" style="italic" size="6">
							<xsl:if test="KProvisiesSocVoordA[number()=number()]">
								<xsl:value-of select="format-number(number(KProvisiesSocVoordA[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" style="italic" size="6">
							<xsl:if test="KProvisiesSocVoordB[number()=number()]">
								<xsl:value-of select="format-number(number(KProvisiesSocVoordB[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" style="italic" size="6">
							<xsl:if test="KProvisiesSocVoordC[number()=number()]">
								<xsl:value-of select="format-number(number(KProvisiesSocVoordC[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" style="italic" size="6">
							<xsl:if test="KProvisiesSocVoordD[number()=number()]">
								<xsl:value-of select="format-number(number(KProvisiesSocVoordD[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
				</row>
			</xsl:if>
			<xsl:if test="not(number(KInvesteringsres1982A[number()=number()])=0 and number(KInvesteringsres1982B[number()=number()])=0 and number(KInvesteringsres1982C[number()=number()])=0 and number(KInvesteringsres1982D[number()=number()])=0)">
				<xsl:variable name="dummyKTitleKInvesteringsres1982A" select="Helper:AddSum('11_',1)" />
				<row>
					<cell verticalalign="top" horizontalalign="left">
						<phrase lineheight="1.1" style="italic" size="6"> &#160;&#160;&#160;&#160;<xsl:value-of select="Translations:GetTranslation('Detail','KInvesteringsres1982A','KInvesteringsres1982A')" />
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" style="italic" size="6">
							<xsl:if test="KInvesteringsres1982A[number()=number()]">
								<xsl:value-of select="format-number(number(KInvesteringsres1982A[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" style="italic" size="6">
							<xsl:if test="KInvesteringsres1982B[number()=number()]">
								<xsl:value-of select="format-number(number(KInvesteringsres1982B[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" style="italic" size="6">
							<xsl:if test="KInvesteringsres1982C[number()=number()]">
								<xsl:value-of select="format-number(number(KInvesteringsres1982C[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" style="italic" size="6">
							<xsl:if test="KInvesteringsres1982D[number()=number()]">
								<xsl:value-of select="format-number(number(KInvesteringsres1982D[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
				</row>
			</xsl:if>
			<xsl:if test="not(number(KRSZInschakelingA[number()=number()])=0 and number(KRSZInschakelingB[number()=number()])=0 and number(KRSZInschakelingC[number()=number()])=0 and number(KRSZInschakelingD[number()=number()])=0)">
				<xsl:variable name="dummyKTitleKRSZInschakelingA" select="Helper:AddSum('11_',1)" />
				<row>
					<cell verticalalign="top" horizontalalign="left">
						<phrase lineheight="1.1" style="italic" size="6"> &#160;&#160;&#160;&#160;<xsl:value-of select="Translations:GetTranslation('Detail','KRSZInschakelingA','KRSZInschakelingA')" />
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" style="italic" size="6">
							<xsl:if test="KRSZInschakelingA[number()=number()]">
								<xsl:value-of select="format-number(number(KRSZInschakelingA[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" style="italic" size="6">
							<xsl:if test="KRSZInschakelingB[number()=number()]">
								<xsl:value-of select="format-number(number(KRSZInschakelingB[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" style="italic" size="6">
							<xsl:if test="KRSZInschakelingC[number()=number()]">
								<xsl:value-of select="format-number(number(KRSZInschakelingC[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" style="italic" size="6">
							<xsl:if test="KRSZInschakelingD[number()=number()]">
								<xsl:value-of select="format-number(number(KRSZInschakelingD[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
				</row>
			</xsl:if>
			<xsl:if test="not(number(KVoorraadDiamantA[number()=number()])=0 and number(KVoorraadDiamantB[number()=number()])=0 and number(KVoorraadDiamantC[number()=number()])=0 and number(KVoorraadDiamantD[number()=number()])=0)">
				<xsl:variable name="dummyKTitleKVoorraadDiamantA" select="Helper:AddSum('11_',1)" />
				<row>
					<cell verticalalign="top" horizontalalign="left">
						<phrase lineheight="1.1" style="italic" size="6"> &#160;&#160;&#160;&#160;<xsl:value-of select="Translations:GetTranslation('Detail','KVoorraadDiamantA','KVoorraadDiamantA')" />
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" style="italic" size="6">
							<xsl:if test="KVoorraadDiamantA[number()=number()]">
								<xsl:value-of select="format-number(number(KVoorraadDiamantA[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" style="italic" size="6">
							<xsl:if test="KVoorraadDiamantB[number()=number()]">
								<xsl:value-of select="format-number(number(KVoorraadDiamantB[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" style="italic" size="6">
							<xsl:if test="KVoorraadDiamantC[number()=number()]">
								<xsl:value-of select="format-number(number(KVoorraadDiamantC[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" style="italic" size="6">
							<xsl:if test="KVoorraadDiamantD[number()=number()]">
								<xsl:value-of select="format-number(number(KVoorraadDiamantD[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
				</row>
			</xsl:if>
			<xsl:if test="not(number(KNettoActiefA[number()=number()])=0 and number(KNettoActiefB[number()=number()])=0 and number(KNettoActiefC[number()=number()])=0 and number(KNettoActiefD[number()=number()])=0)">
				<xsl:variable name="dummyKTitleKNettoActiefA" select="Helper:AddSum('11_',1)" />
				<row>
					<cell verticalalign="top" horizontalalign="left">
						<phrase lineheight="1.1" style="italic" size="6"> &#160;&#160;&#160;&#160;<xsl:value-of select="Translations:GetTranslation('Detail','KNettoActiefA','KNettoActiefA')" />
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" style="italic" size="6">
							<xsl:if test="KNettoActiefA[number()=number()]">
								<xsl:value-of select="format-number(number(KNettoActiefA[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" style="italic" size="6">
							<xsl:if test="KNettoActiefB[number()=number()]">
								<xsl:value-of select="format-number(number(KNettoActiefB[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" style="italic" size="6">
							<xsl:if test="KNettoActiefC[number()=number()]">
								<xsl:value-of select="format-number(number(KNettoActiefC[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" style="italic" size="6">
							<xsl:if test="KNettoActiefD[number()=number()]">
								<xsl:value-of select="format-number(number(KNettoActiefD[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
				</row>
			</xsl:if>
			<xsl:if test="not(number(KAndereA[number()=number()])=0 and number(KAndereB[number()=number()])=0 and number(KAndereC[number()=number()])=0 and number(KAndereD[number()=number()])=0)">
				<xsl:variable name="dummyKTitleKAndereA" select="Helper:AddSum('11_',1)" />
				<row>
					<cell verticalalign="top" horizontalalign="left">
						<phrase lineheight="1.1" style="italic" size="6"> &#160;&#160;&#160;&#160;<xsl:value-of select="Translations:GetTranslation('Detail','KAndereA','KAndereA')" />
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" style="italic" size="6">
							<xsl:if test="KAndereA[number()=number()]">
								<xsl:value-of select="format-number(number(KAndereA[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" style="italic" size="6">
							<xsl:if test="KAndereB[number()=number()]">
								<xsl:value-of select="format-number(number(KAndereB[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" style="italic" size="6">
							<xsl:if test="KAndereC[number()=number()]">
								<xsl:value-of select="format-number(number(KAndereC[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" style="italic" size="6">
							<xsl:if test="KAndereD[number()=number()]">
								<xsl:value-of select="format-number(number(KAndereD[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
				</row>
			</xsl:if>
			<xsl:if test="not(number(KTotaalA[number()=number()])=0 and number(KTotaalB[number()=number()])=0 and number(KTotaalC[number()=number()])=0 and number(KTotaalD[number()=number()])=0) and Helper:AddSum('11_',0)&gt;1">
				<xsl:variable name="dummyKTitleKTotaalA" select="Helper:AddSum('11_',1)" />
				<row>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" size="8"> &#160;&#160;&#160;&#160;<xsl:value-of select="Translations:GetTranslation('Detail','KTotaalA','KTotaalA')" />&#160;&#160;&#160;&#160; </phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" size="8">
							<xsl:if test="KTotaalA[number()=number()]">
								<xsl:value-of select="format-number(number(KTotaalA[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" size="8">
							<xsl:if test="KTotaalB[number()=number()]">
								<xsl:value-of select="format-number(number(KTotaalB[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" size="8">
							<xsl:if test="KTotaalC[number()=number()]">
								<xsl:value-of select="format-number(number(KTotaalC[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" size="8">
							<xsl:if test="KTotaalD[number()=number()]">
								<xsl:value-of select="format-number(number(KTotaalD[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
				</row>
			</xsl:if>
		</table>
		<table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" columns="5" width="100%" align="Center" widths="52;12;12;12;12" cellspacing="0">
			<row>
				<cell colspan="5" verticalalign="top" horizontalalign="left" header="true">
					<phrase lineheight="1.1" size="10"> &#160;<newline />&#160; </phrase>
				</cell>
			</row>
			<xsl:variable name="dummy" select="Helper:ResetSum('12_')" />
			<xsl:if test="not(number(TotaalVrijgesteldA[number()=number()])=0 and number(TotaalVrijgesteldB[number()=number()])=0 and number(TotaalVrijgesteldC[number()=number()])=0 and number(TotaalVrijgesteldD[number()=number()])=0)">
				<xsl:variable name="dummyTotaalVrijgesteldA" select="Helper:AddSum('12_',1)" />
				<row>
					<cell verticalalign="top" horizontalalign="left">
						<phrase lineheight="1.1" fontstyle="bold underline" size="8">
							<xsl:value-of select="Translations:GetTranslation('Detail','TotaalVrijgesteldA','TotaalVrijgesteldA')" />
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" fontstyle="bold underline" size="8">
							<xsl:if test="TotaalVrijgesteldA[number()=number()]">
								<xsl:value-of select="format-number(number(TotaalVrijgesteldA[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" fontstyle="bold underline" size="8">
							<xsl:if test="TotaalVrijgesteldB[number()=number()]">
								<xsl:value-of select="format-number(number(TotaalVrijgesteldB[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" fontstyle="bold underline" size="8">
							<xsl:if test="TotaalVrijgesteldC[number()=number()]">
								<xsl:value-of select="format-number(number(TotaalVrijgesteldC[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="1.1" fontstyle="bold underline" size="8">
							<xsl:if test="TotaalVrijgesteldD[number()=number()]">
								<xsl:value-of select="format-number(number(TotaalVrijgesteldD[number()=number()]),$NumberFormatting,'euro')" />€ </xsl:if>
						</phrase>
					</cell>
				</row>
			</xsl:if>
		</table>
	</xsl:template>
</xsl:stylesheet>