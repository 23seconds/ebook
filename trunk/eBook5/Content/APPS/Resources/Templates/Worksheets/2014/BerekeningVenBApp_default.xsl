<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" exclude-result-prefixes="Helper Translations File Client" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:Helper="urn:Helper" xmlns:Translations="urn:Translations" xmlns:File="urn:File" xmlns:Client="urn:Client">
	<xsl:decimal-format name="NonNumber" NaN="0" zero-digit="0"/>
	<xsl:variable name="WorksheetLabelFontsize" select="string('10')"/>
	<xsl:variable name="WorksheetFontsize" select="string('8')"/>
	<xsl:variable name="WorksheetNumbersize" select="string('6')"/>
	<xsl:variable name="WorksheetSmallFontsize" select="string('8')"/>
	<xsl:variable name="WorksheetTotalsFontsize" select="string('10')"/>
	<xsl:variable name="WorksheetSpacing" select="1.3"/>
	<xsl:decimal-format name="euro" decimal-separator="," grouping-separator="."/>
	<xsl:variable name="NumberFormatting" select="string('#.##0,00')"/>
	<xsl:variable name="PercFormatting" select="string('#.##0,000')"/>
	<xsl:variable name="NumberFormattingNoDigit" select="string('#.##0')"/>
	<xsl:variable name="FONT" select="string('C:\WINDOWS\Fonts\EYInterstate-Light.ttf')"/>
	<xsl:variable name="up" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZ'"/>
	<xsl:variable name="lo" select="'abcdefghijklmnopqrstuvwxyz'"/>
	<xsl:param name="RuleAppDef"/>
	<xsl:param name="Culture"/>
	<xsl:template match="BerekeningVenB|TaxCalc">
		<Root>
			<xsl:apply-templates select="." mode="BelastbaarResultaat"/>
			<xsl:apply-templates select="." mode="AftrekBeperkingVoorFiscaleAftrek"/>
			<xsl:apply-templates select="." mode="FiscaleAftrek"/>
			<xsl:apply-templates select="." mode="GewoonTarief"/>
			<xsl:apply-templates select="BerekeningOverzicht" mode="GewoonTariefDetail"/>
			<xsl:apply-templates select="BerekeningOverzicht" mode="VerrekenbareVoorheffingen"/>
			<xsl:apply-templates select="BerekeningOverzicht" mode="BasisVoorVermeerdering"/>
			<xsl:apply-templates select="BerekeningOverzicht" mode="Voorafbetalingen"/>
			<xsl:apply-templates select="BerekeningOverzicht" mode="AfzonderlijkeAanslag"/>
			<xsl:apply-templates select="BerekeningOverzicht" mode="RESULT"/>
		</Root>
	</xsl:template>
	<xsl:template match="BerekeningVenB|TaxCalc" mode="BelastbaarResultaat">
		<table tablefitspage="true" convert2pdfp="true" cellsfitpage="true" width="100%" align="Center" cellspacing="0" columns="4" widths="64;12;12;12">
			<!--xsl:if test="sum(BerekeningOverzicht/StatAccountingResult[number()=number()])!=0"-->
			<row>
				<cell colspan="3">
					<paragraph lineheight="1.3" size="{$WorksheetFontsize}" fontstyle="bold">
						<xsl:value-of select="translate(Translations:GetTranslation('General','StatAccountingResult','StatAccountingResult'),$lo,$up)"/>
					</paragraph>
				</cell>
				<cell horizontalalign="right" colspan="1">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="bold">
						<xsl:value-of select="format-number(sum(BerekeningOverzicht/StatAccountingResult[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
				</cell>
			</row>
			<!--/xsl:if-->
		</table>
		<table tablefitspage="true" convert2pdfp="true" cellsfitpage="true" width="100%" align="Center" cellspacing="0" columns="4" widths="64;12;12;12">
			<!--xsl:if test="sum(BerekeningOverzicht/TotalTaxableReserves[number()=number()])!=0"-->
			<row>
				<cell colspan="3">
					<paragraph lineheight="1.3" size="{$WorksheetFontsize}" fontstyle="bold" indentationleft="10">
						<xsl:value-of select="Translations:GetTranslation('BelasteReservesItem','TempDiff','TempDiff')"/>
					</paragraph>
				</cell>
				<cell horizontalalign="right" colspan="1">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="bold">
						<xsl:value-of select="format-number(sum(BerekeningOverzicht/TotalTaxableReserves[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
				</cell>
			</row>
			<xsl:apply-templates select="BelasteReserves" mode="DetailNormal"/>
			<!--/xsl:if-->
		</table>
		<table tablefitspage="true" convert2pdfp="true" cellsfitpage="true" width="100%" align="Center" cellspacing="0" columns="4" widths="64;12;12;12">
			<!--xsl:if test="sum(BerekeningOverzicht/TotalTaxableReservesPlus[number()=number()])!=0"-->
			<row>
				<cell colspan="3">
					<paragraph lineheight="1.3" size="{$WorksheetFontsize}" fontstyle="bold" indentationleft="10">
						<xsl:value-of select="Translations:GetTranslation('BelasteReservesItem','OhterAdjustments','OhterAdjustments')"/>
					</paragraph>
				</cell>
				<cell horizontalalign="right" colspan="1">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="bold">- <xsl:value-of select="format-number(sum(BerekeningOverzicht/TotalTaxableReservesPlus[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
				</cell>
			</row>
			<xsl:apply-templates select="BelasteReserves" mode="DetailBTPlus"/>
			<!--/xsl:if-->
			<!--xsl:if test="sum(BerekeningOverzicht/TotalTaxableReservesMinus[number()=number()])!=0"-->
			<row>
				<cell colspan="3">
					<paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="font-weight:bold" indentationleft="10">
						<xsl:value-of select="Translations:GetTranslation('BelasteReservesItem','OAInTheMin','OAInTheMin')"/>
					</paragraph>
				</cell>
				<cell horizontalalign="right" colspan="1">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="font-weight:bold">
						<xsl:value-of select="format-number(sum(BerekeningOverzicht/TotalTaxableReservesMinus[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
				</cell>
			</row>
			<!--/xsl:if-->
		</table>
		<table tablefitspage="false" convert2pdfp="true" cellsfitpage="true" width="100%" align="Center" cellspacing="0" columns="4" widths="64;12;12;12">
			<!--xsl:if test="sum(BerekeningOverzicht/TotalNonDeductibleTaxes[number()=number()])!=0"-->
			<row>
				<cell colspan="3">
					<paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="font-weight:bold" indentationleft="10">
						<xsl:value-of select="Translations:GetTranslation('General','NietAftrekbareBelastingen','NietAftrekbareBelastingen')"/>
					</paragraph>
				</cell>
				<cell horizontalalign="right" colspan="1">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="font-weight:bold">
						<xsl:value-of select="format-number(sum(BerekeningOverzicht/TotalNonDeductibleTaxes[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
				</cell>
			</row>
			<!--/xsl:if-->
			<!--xsl:if test="sum(BerekeningOverzicht/TotalDissalowedExpensesWithoutNonDeductibleTax[number()=number()])!=0"-->
			<row>
				<cell colspan="3">
					<paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="font-weight:bold" indentationleft="10">
						<xsl:value-of select="Translations:GetTranslation('BerekeningVenB','VerworpenUitgaven','VerworpenUitgaven')"/>
					</paragraph>
				</cell>
				<cell horizontalalign="right" colspan="1">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="font-weight:bold">
						<xsl:value-of select="format-number(sum(BerekeningOverzicht/TotalDissalowedExpensesWithoutNonDeductibleTax[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
				</cell>
			</row>
			<xsl:apply-templates select="VerworpenUitgaven" mode="DetailNormal"/>
			<!--/xsl:if-->
			<!--xsl:if test="sum(BerekeningOverzicht/TaxableResult[number()=number()])!=0"-->
			<row>
				<cell colspan="3" border="1" bordercolor="black" top="true">
					<paragraph lineheight="1.3" size="{$WorksheetFontsize}" fontstyle="bold">
						<xsl:value-of select="translate(Translations:GetTranslation('General','BelastbaarResultaat','BelastbaarResultaat'), $lo,$up)"/>
					</paragraph>
				</cell>
				<cell horizontalalign="right" colspan="1" border="1" bordercolor="black" top="true">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="bold">
						<xsl:value-of select="format-number(sum(BerekeningOverzicht/TaxableResult[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
				</cell>
			</row>
			<!--/xsl:if-->
		</table>
	</xsl:template> <xsl:template match="BelasteReserves" mode="DetailNormal">
		<!--xsl:if test="sum(TDKapitaalEnUitgifte[number()=number()])!=0"-->
		<row>
			<cell colspan="2">
				<paragraph lineheight="1.3" size="{$WorksheetFontsize}" fontstyle="italic" indentationleft="30">
					<xsl:value-of select="Translations:GetTranslation('BelasteReservesItem','TDKapitaalEnUitgifte','TDKapitaalEnUitgifte')"/>
				</paragraph>
			</cell>
			<cell horizontalalign="right" colspan="1">
				<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="italic">
					<xsl:value-of select="format-number(sum(TDKapitaalEnUitgifte[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
			</cell>
			<cell colspan="1">
				<paragraph>
					<space/>
				</paragraph>
			</cell>
		</row>
		<!--/xsl:if-->
		<!--xsl:if test="sum(TDHerwaardering[number()=number()])!=0"-->
		<row>
			<cell colspan="2">
				<paragraph lineheight="1.3" size="{$WorksheetFontsize}" fontstyle="italic" indentationleft="30">
					<xsl:value-of select="Translations:GetTranslation('BelasteReservesItem','TDHerwaardering','TDHerwaardering')"/>
				</paragraph>
			</cell>
			<cell horizontalalign="right" colspan="1">
				<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="italic">
					<xsl:value-of select="format-number(sum(TDHerwaardering[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
			</cell>
			<cell colspan="1">
				<paragraph>
					<space/>
				</paragraph>
			</cell>
		</row>
		<!--/xsl:if-->
		<!--xsl:if test="sum(TDTaxableProvisions[number()=number()])!=0"-->
		<row>
			<cell colspan="2">
				<paragraph lineheight="1.3" size="{$WorksheetFontsize}" fontstyle="italic" indentationleft="30">
					<xsl:value-of select="Translations:GetTranslation('BelasteReservesItem','TDTaxableProvisions','TDTaxableProvisions')"/>
				</paragraph>
			</cell>
			<cell horizontalalign="right" colspan="1">
				<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="italic">
					<xsl:value-of select="format-number(sum(TDTaxableProvisions[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
			</cell>
			<cell colspan="1">
				<paragraph>
					<space/>
				</paragraph>
			</cell>
		</row>
		<!--/xsl:if-->
		<!--xsl:if test="sum(TDOtherTaxableReserves[number()=number()])!=0"-->
		<row>
			<cell colspan="2">
				<paragraph lineheight="1.3" size="{$WorksheetFontsize}" fontstyle="italic" indentationleft="30">
					<xsl:value-of select="Translations:GetTranslation('BelasteReservesItem','TDOtherTaxableReserves','TDOtherTaxableReserves')"/>
				</paragraph>
			</cell>
			<cell horizontalalign="right" colspan="1">
				<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="italic">
					<xsl:value-of select="format-number(sum(TDOtherTaxableReserves[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
			</cell>
			<cell colspan="1">
				<paragraph>
					<space/>
				</paragraph>
			</cell>
		</row>
		<!--/xsl:if-->
		<!--xsl:if test="sum(TDOtherNonBalanceTaxableReserves[number()=number()])!=0"-->
		<row>
			<cell colspan="2">
				<paragraph lineheight="1.3" size="{$WorksheetFontsize}" fontstyle="italic" indentationleft="30">
					<xsl:value-of select="Translations:GetTranslation('BelasteReservesItem','TDOtherNonBalanceTaxableReserves','TDOtherNonBalanceTaxableReserves')"/>
				</paragraph>
			</cell>
			<cell horizontalalign="right" colspan="1">
				<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="italic">
					<xsl:value-of select="format-number(sum(TDOtherNonBalanceTaxableReserves[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
			</cell>
			<cell colspan="1">
				<paragraph>
					<space/>
				</paragraph>
			</cell>
		</row>
		<!--/xsl:if-->
		<!--xsl:if test="sum(TDTaxableDoubtfullDebtors[number()=number()])!=0"-->
		<row>
			<cell colspan="2">
				<paragraph lineheight="1.3" size="{$WorksheetFontsize}" fontstyle="italic" indentationleft="30">
					<xsl:value-of select="Translations:GetTranslation('BelasteReservesItem','TDTaxableDoubtfullDebtors','TDTaxableDoubtfullDebtors')"/>
				</paragraph>
			</cell>
			<cell horizontalalign="right" colspan="1">
				<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="italic">
					<xsl:value-of select="format-number(sum(TDTaxableDoubtfullDebtors[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
			</cell>
			<cell colspan="1">
				<paragraph>
					<space/>
				</paragraph>
			</cell>
		</row>
		<!--/xsl:if-->
		<!--xsl:if test="sum(TDExcessDeprications[number()=number()])!=0"-->
		<row>
			<cell colspan="2">
				<paragraph lineheight="1.3" size="{$WorksheetFontsize}" fontstyle="italic" indentationleft="30">
					<xsl:value-of select="Translations:GetTranslation('BelasteReservesItem','TDExcessDeprications','TDExcessDeprications')"/>
				</paragraph>
			</cell>
			<cell horizontalalign="right" colspan="1">
				<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="italic">
					<xsl:value-of select="format-number(sum(TDExcessDeprications[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
			</cell>
			<cell colspan="1">
				<paragraph>
					<space/>
				</paragraph>
			</cell>
		</row>
		<!--/xsl:if-->
		<!--xsl:if test="sum(TDUnderestimationAssetsOverestimationLiabilities[number()=number()])!=0"-->
		<row>
			<cell colspan="2">
				<paragraph lineheight="1.3" size="{$WorksheetFontsize}" fontstyle="italic" indentationleft="30">
					<xsl:value-of select="Translations:GetTranslation('BelasteReservesItem','TDUnderestimationAssetsOverestimationLiabilities','TDUnderestimationAssetsOverestimationLiabilities')"/>
				</paragraph>
			</cell>
			<cell horizontalalign="right" colspan="1">
				<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="italic">
					<xsl:value-of select="format-number(sum(TDUnderestimationAssetsOverestimationLiabilities[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
			</cell>
			<cell colspan="1">
				<paragraph>
					<space/>
				</paragraph>
			</cell>
		</row>
		<!--/xsl:if-->
		<!--xsl:if test="sum(TDOverestimationLiabilities[number()=number()])!=0"-->
		<row>
			<cell colspan="2">
				<paragraph lineheight="1.3" size="{$WorksheetFontsize}" fontstyle="italic" indentationleft="30">
					<xsl:value-of select="Translations:GetTranslation('BelasteReservesItem','TDOverestimationLiabilities','TDOverestimationLiabilities')"/>
				</paragraph>
			</cell>
			<cell horizontalalign="right" colspan="1">
				<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="italic">
					<xsl:value-of select="format-number(sum(TDOverestimationLiabilities[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
			</cell>
			<cell colspan="1">
				<paragraph>
					<space/>
				</paragraph>
			</cell>
		</row>
		<!--/xsl:if-->
	</xsl:template>
	<xsl:template match="BelasteReserves" mode="DetailBTPlus">
		<xsl:variable name="totalAanpMeerPrep" select="sum(OACapitalGainsOnShares[number()=number()]) + sum(OACapitalGainsSharesReversal[number()=number()]) + sum(OADefinitiveExemptionTaxShelter[number()=number()]) + sum(OAExemptionRegionalPrem[number()=number()]) + sum(OAFinalExemptionProfit[number()=number()]) + sum(OAOthers[number()=number()])"/>
		<!--xsl:if test="sum(OACapitalGainsOnShares[number()=number()])!=0"-->
		<row>
			<cell colspan="2">
				<paragraph lineheight="1.3" size="{$WorksheetFontsize}" fontstyle="italic" indentationleft="30"> - <xsl:value-of select="Translations:GetTranslation('BelasteReservesItem','OACapitalGainsOnShares','OACapitalGainsOnShares')"/>
				</paragraph>
			</cell>
			<cell horizontalalign="right" colspan="1">
				<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="italic"> - <xsl:value-of select="format-number(sum(OACapitalGainsOnShares[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
			</cell>
			<cell colspan="1">
				<paragraph>
					<space/>
				</paragraph>
			</cell>
		</row>
		<!--/xsl:if-->
		<!--xsl:if test="sum(OACapitalGainsSharesReversal[number()=number()])!=0"-->
		<row>
			<cell colspan="2">
				<paragraph lineheight="1.3" size="{$WorksheetFontsize}" fontstyle="italic" indentationleft="30"> - <xsl:value-of select="Translations:GetTranslation('BelasteReservesItem','OACapitalGainsSharesReversal','OACapitalGainsSharesReversal')"/>
				</paragraph>
			</cell>
			<cell horizontalalign="right" colspan="1">
				<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="italic"> - <xsl:value-of select="format-number(sum(OACapitalGainsSharesReversal[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
			</cell>
			<cell colspan="1">
				<paragraph>
					<space/>
				</paragraph>
			</cell>
		</row>
		<!--/xsl:if-->
		<!--xsl:if test="sum(OADefinitiveExemptionTaxShelter[number()=number()])!=0"-->
		<row>
			<cell colspan="2">
				<paragraph lineheight="1.3" size="{$WorksheetFontsize}" fontstyle="italic" indentationleft="30"> - <xsl:value-of select="Translations:GetTranslation('BelasteReservesItem','OADefinitiveExemptionTaxShelter','OADefinitiveExemptionTaxShelter')"/>
				</paragraph>
			</cell>
			<cell horizontalalign="right" colspan="1">
				<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="italic"> - <xsl:value-of select="format-number(sum(OADefinitiveExemptionTaxShelter[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
			</cell>
			<cell colspan="1">
				<paragraph>
					<space/>
				</paragraph>
			</cell>
		</row>
		<!--/xsl:if-->
		<!--xsl:if test="sum(OAExemptionRegionalPrem[number()=number()])!=0"-->
		<row>
			<cell colspan="2">
				<paragraph lineheight="1.3" size="{$WorksheetFontsize}" fontstyle="italic" indentationleft="30"> - <xsl:value-of select="Translations:GetTranslation('BelasteReservesItem','OAExemptionRegionalPrem','OAExemptionRegionalPrem')"/>
				</paragraph>
			</cell>
			<cell horizontalalign="right" colspan="1">
				<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="italic"> - <xsl:value-of select="format-number(sum(OAExemptionRegionalPrem[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
			</cell>
			<cell colspan="1">
				<paragraph>
					<space/>
				</paragraph>
			</cell>
		</row>
		<!--/xsl:if-->
		<!--xsl:if test="sum(OAFinalExemptionProfit[number()=number()])!=0"-->
		<row>
			<cell colspan="2">
				<paragraph lineheight="1.3" size="{$WorksheetFontsize}" fontstyle="italic" indentationleft="30"> - <xsl:value-of select="Translations:GetTranslation('BelasteReservesItem','OAFinalExemptionProfit','OAFinalExemptionProfit')"/>
				</paragraph>
			</cell>
			<cell horizontalalign="right" colspan="1">
				<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="italic"> - <xsl:value-of select="format-number(sum(OAFinalExemptionProfit[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
			</cell>
			<cell colspan="1">
				<paragraph>
					<space/>
				</paragraph>
			</cell>
		</row>
		<!--/xsl:if-->
		<!--xsl:if test="sum(OAOthers[number()=number()])!=0"-->
		<row>
			<cell colspan="2">
				<paragraph lineheight="1.3" size="{$WorksheetFontsize}" fontstyle="italic" indentationleft="30"> - <xsl:value-of select="Translations:GetTranslation('BelasteReservesItem','OAOthers','OAOthers')"/>
				</paragraph>
			</cell>
			<cell horizontalalign="right" colspan="1">
				<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="italic"> - <xsl:value-of select="format-number(sum(OAOthers[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
			</cell>
			<cell colspan="1">
				<paragraph>
					<space/>
				</paragraph>
			</cell>
		</row>
		<!--/xsl:if-->
	</xsl:template>
	<xsl:template match="VerworpenUitgaven" mode="DetailNormal">
		<!--xsl:if test="sum(DERegionalTaxes[number()=number()])!=0"-->
		<row>
			<cell colspan="2">
				<paragraph lineheight="1.3" size="{$WorksheetFontsize}" fontstyle="italic" indentationleft="30">
					<xsl:value-of select="Translations:GetTranslation('VerworpenUitgavenItem','DERegionalTaxes','DERegionalTaxes')"/>
				</paragraph>
			</cell>
			<cell horizontalalign="right" colspan="1">
				<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="italic">
					<xsl:value-of select="format-number(sum(DERegionalTaxes[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
			</cell>
			<cell colspan="1">
				<paragraph>
					<space/>
				</paragraph>
			</cell>
		</row>
		<!--/xsl:if-->
		<!--xsl:if test="sum(DEPenalties[number()=number()])!=0"-->
		<row>
			<cell colspan="2">
				<paragraph lineheight="1.3" size="{$WorksheetFontsize}" fontstyle="italic" indentationleft="30">
					<xsl:value-of select="Translations:GetTranslation('VerworpenUitgavenItem','DEPenalties','DEPenalties')"/>
				</paragraph>
			</cell>
			<cell horizontalalign="right" colspan="1">
				<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="italic">
					<xsl:value-of select="format-number(sum(DEPenalties[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
			</cell>
			<cell colspan="1">
				<paragraph>
					<space/>
				</paragraph>
			</cell>
		</row>
		<!--/xsl:if-->
		<!--xsl:if test="sum(DEPensions[number()=number()])!=0"-->
		<row>
			<cell colspan="2">
				<paragraph lineheight="1.3" size="{$WorksheetFontsize}" fontstyle="italic" indentationleft="30">
					<xsl:value-of select="Translations:GetTranslation('VerworpenUitgavenItem','DEPensions','DEPensions')"/>
				</paragraph>
			</cell>
			<cell horizontalalign="right" colspan="1">
				<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="italic">
					<xsl:value-of select="format-number(sum(DEPensions[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
			</cell>
			<cell colspan="1">
				<paragraph>
					<space/>
				</paragraph>
			</cell>
		</row>
		<!--/xsl:if-->
		<!--xsl:if test="sum(DECar[number()=number()])!=0"-->
		<row>
			<cell colspan="2">
				<paragraph lineheight="1.3" size="{$WorksheetFontsize}" fontstyle="italic" indentationleft="30">
					<xsl:value-of select="Translations:GetTranslation('VerworpenUitgavenItem','DECar','DECar')"/>
				</paragraph>
			</cell>
			<cell horizontalalign="right" colspan="1">
				<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="italic">
					<xsl:value-of select="format-number(sum(DECar[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
			</cell>
			<cell colspan="1">
				<paragraph>
					<space/>
				</paragraph>
			</cell>
		</row>
		<!--/xsl:if-->
		<!--xsl:if test="sum(DECarVAA[number()=number()])!=0"-->
		<row>
			<cell colspan="2">
				<paragraph lineheight="1.3" size="{$WorksheetFontsize}" fontstyle="italic" indentationleft="30">
					<xsl:value-of select="Translations:GetTranslation('VerworpenUitgavenItem','DECarVAA','DECarVAA')"/>
				</paragraph>
			</cell>
			<cell horizontalalign="right" colspan="1">
				<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="italic">
					<xsl:value-of select="format-number(sum(DECarVAA[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
			</cell>
			<cell colspan="1">
				<paragraph>
					<space/>
				</paragraph>
			</cell>
		</row>
		<!--/xsl:if-->
		<!--xsl:if test="sum(DEReception[number()=number()])!=0"-->
		<row>
			<cell colspan="2">
				<paragraph lineheight="1.3" size="{$WorksheetFontsize}" fontstyle="italic" indentationleft="30">
					<xsl:value-of select="Translations:GetTranslation('VerworpenUitgavenItem','DEReception','DEReception')"/>
				</paragraph>
			</cell>
			<cell horizontalalign="right" colspan="1">
				<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="italic">
					<xsl:value-of select="format-number(sum(DEReception[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
			</cell>
			<cell colspan="1">
				<paragraph>
					<space/>
				</paragraph>
			</cell>
		</row>
		<!--/xsl:if-->
		<!--xsl:if test="sum(DERestaurant[number()=number()])!=0"-->
		<row>
			<cell colspan="2">
				<paragraph lineheight="1.3" size="{$WorksheetFontsize}" fontstyle="italic" indentationleft="30">
					<xsl:value-of select="Translations:GetTranslation('VerworpenUitgavenItem','DERestaurant','DERestaurant')"/>
				</paragraph>
			</cell>
			<cell horizontalalign="right" colspan="1">
				<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="italic">
					<xsl:value-of select="format-number(sum(DERestaurant[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
			</cell>
			<cell colspan="1">
				<paragraph>
					<space/>
				</paragraph>
			</cell>
		</row>
		<!--/xsl:if-->
		<!--xsl:if test="sum(DEClothes[number()=number()])!=0"-->
		<row>
			<cell colspan="2">
				<paragraph lineheight="1.3" size="{$WorksheetFontsize}" fontstyle="italic" indentationleft="30">
					<xsl:value-of select="Translations:GetTranslation('VerworpenUitgavenItem','DEClothes','DEClothes')"/>
				</paragraph>
			</cell>
			<cell horizontalalign="right" colspan="1">
				<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="italic">
					<xsl:value-of select="format-number(sum(DEClothes[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
			</cell>
			<cell colspan="1">
				<paragraph>
					<space/>
				</paragraph>
			</cell>
		</row>
		<!--/xsl:if-->
		<!--xsl:if test="sum(DEExcessInterest[number()=number()])!=0"-->
		<row>
			<cell colspan="2">
				<paragraph lineheight="1.3" size="{$WorksheetFontsize}" fontstyle="italic" indentationleft="30">
					<xsl:value-of select="Translations:GetTranslation('VerworpenUitgavenItem','DEExcessInterest','DEExcessInterest')"/>
				</paragraph>
			</cell>
			<cell horizontalalign="right" colspan="1">
				<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="italic">
					<xsl:value-of select="format-number(sum(DEExcessInterest[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
			</cell>
			<cell colspan="1">
				<paragraph>
					<space/>
				</paragraph>
			</cell>
		</row>
		<!--/xsl:if-->
		<!--xsl:if test="sum(DEInterestLoans[number()=number()])!=0"-->
		<row>
			<cell colspan="2">
				<paragraph lineheight="1.3" size="{$WorksheetFontsize}" fontstyle="italic" indentationleft="30">
					<xsl:value-of select="Translations:GetTranslation('VerworpenUitgavenItem','DEInterestLoans','DEInterestLoans')"/>
				</paragraph>
			</cell>
			<cell horizontalalign="right" colspan="1">
				<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="italic">
					<xsl:value-of select="format-number(sum(DEInterestLoans[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
			</cell>
			<cell colspan="1">
				<paragraph>
					<space/>
				</paragraph>
			</cell>
		</row>
		<!--/xsl:if-->
		<!--xsl:if test="sum(DEAbnormalAdvantages[number()=number()])!=0"-->
		<row>
			<cell colspan="2">
				<paragraph lineheight="1.3" size="{$WorksheetFontsize}" fontstyle="italic" indentationleft="30">
					<xsl:value-of select="Translations:GetTranslation('VerworpenUitgavenItem','DEAbnormalAdvantages','DEAbnormalAdvantages')"/>
				</paragraph>
			</cell>
			<cell horizontalalign="right" colspan="1">
				<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="italic">
					<xsl:value-of select="format-number(sum(DEAbnormalAdvantages[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
			</cell>
			<cell colspan="1">
				<paragraph>
					<space/>
				</paragraph>
			</cell>
		</row>
		<!--/xsl:if-->
		<!--xsl:if test="sum(DESocial[number()=number()])!=0"-->
		<row>
			<cell colspan="2">
				<paragraph lineheight="1.3" size="{$WorksheetFontsize}" fontstyle="italic" indentationleft="30">
					<xsl:value-of select="Translations:GetTranslation('VerworpenUitgavenItem','DESocial','DESocial')"/>
				</paragraph>
			</cell>
			<cell horizontalalign="right" colspan="1">
				<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="italic">
					<xsl:value-of select="format-number(sum(DESocial[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
			</cell>
			<cell colspan="1">
				<paragraph>
					<space/>
				</paragraph>
			</cell>
		</row>
		<!--/xsl:if-->
		<!--xsl:if test="sum(DEDinerSportCultureEco[number()=number()])!=0"-->
		<row>
			<cell colspan="2">
				<paragraph lineheight="1.3" size="{$WorksheetFontsize}" fontstyle="italic" indentationleft="30">
					<xsl:value-of select="Translations:GetTranslation('VerworpenUitgavenItem','DEDinerSportCultureEco','DEDinerSportCultureEco')"/>
				</paragraph>
			</cell>
			<cell horizontalalign="right" colspan="1">
				<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="italic">
					<xsl:value-of select="format-number(sum(DEDinerSportCultureEco[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
			</cell>
			<cell colspan="1">
				<paragraph>
					<space/>
				</paragraph>
			</cell>
		</row>
		<!--/xsl:if-->
		<!--xsl:if test="sum(DECharity[number()=number()])!=0"-->
		<row>
			<cell colspan="2">
				<paragraph lineheight="1.3" size="{$WorksheetFontsize}" fontstyle="italic" indentationleft="30">
					<xsl:value-of select="Translations:GetTranslation('VerworpenUitgavenItem','DECharity','DECharity')"/>
				</paragraph>
			</cell>
			<cell horizontalalign="right" colspan="1">
				<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="italic">
					<xsl:value-of select="format-number(sum(DECharity[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
			</cell>
			<cell colspan="1">
				<paragraph>
					<space/>
				</paragraph>
			</cell>
		</row>
		<!--/xsl:if-->
		<!--xsl:if test="sum(DEReductionValueShares[number()=number()])!=0"-->
		<row>
			<cell colspan="2">
				<paragraph lineheight="1.3" size="{$WorksheetFontsize}" fontstyle="italic" indentationleft="30">
					<xsl:value-of select="Translations:GetTranslation('VerworpenUitgavenItem','DEReductionValueShares','DEReductionValueShares')"/>
				</paragraph>
			</cell>
			<cell horizontalalign="right" colspan="1">
				<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="italic">
					<xsl:value-of select="format-number(sum(DEReductionValueShares[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
			</cell>
			<cell colspan="1">
				<paragraph>
					<space/>
				</paragraph>
			</cell>
		</row>
		<!--/xsl:if-->
		<!--xsl:if test="sum(DEReversalsPreviousTax[number()=number()])!=0"-->
		<row>
			<cell colspan="2">
				<paragraph lineheight="1.3" size="{$WorksheetFontsize}" fontstyle="italic" indentationleft="30">
					<xsl:value-of select="Translations:GetTranslation('VerworpenUitgavenItem','DEReversalsPreviousTax','DEReversalsPreviousTax')"/>
				</paragraph>
			</cell>
			<cell horizontalalign="right" colspan="1">
				<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="italic">
					<xsl:value-of select="format-number(sum(DEReversalsPreviousTax[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
			</cell>
			<cell colspan="1">
				<paragraph>
					<space/>
				</paragraph>
			</cell>
		</row>
		<!--/xsl:if-->
		<!--xsl:if test="sum(DEEmployeeCapital[number()=number()])!=0"-->
		<row>
			<cell colspan="2">
				<paragraph lineheight="1.3" size="{$WorksheetFontsize}" fontstyle="italic" indentationleft="30">
					<xsl:value-of select="Translations:GetTranslation('VerworpenUitgavenItem','DEEmployeeCapital','DEEmployeeCapital')"/>
				</paragraph>
			</cell>
			<cell horizontalalign="right" colspan="1">
				<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="italic">
					<xsl:value-of select="format-number(sum(DEEmployeeCapital[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
			</cell>
			<cell colspan="1">
				<paragraph>
					<space/>
				</paragraph>
			</cell>
		</row>
		<!--/xsl:if-->
		<!--xsl:if test="sum(DEIndemnityMissingCoupon[number()=number()])!=0"-->
		<row>
			<cell colspan="2">
				<paragraph lineheight="1.3" size="{$WorksheetFontsize}" fontstyle="italic" indentationleft="30">
					<xsl:value-of select="Translations:GetTranslation('VerworpenUitgavenItem','DEIndemnityMissingCoupon','DEIndemnityMissingCoupon')"/>
				</paragraph>
			</cell>
			<cell horizontalalign="right" colspan="1">
				<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="italic">
					<xsl:value-of select="format-number(sum(DEIndemnityMissingCoupon[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
			</cell>
			<cell colspan="1">
				<paragraph>
					<space/>
				</paragraph>
			</cell>
		</row>
		<!--/xsl:if-->
		<!--xsl:if test="sum(DEExpensesTaxShelter[number()=number()])!=0"-->
		<row>
			<cell colspan="2">
				<paragraph lineheight="1.3" size="{$WorksheetFontsize}" fontstyle="italic" indentationleft="30">
					<xsl:value-of select="Translations:GetTranslation('VerworpenUitgavenItem','DEExpensesTaxShelter','DEExpensesTaxShelter')"/>
				</paragraph>
			</cell>
			<cell horizontalalign="right" colspan="1">
				<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="italic">
					<xsl:value-of select="format-number(sum(DEExpensesTaxShelter[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
			</cell>
			<cell colspan="1">
				<paragraph>
					<space/>
				</paragraph>
			</cell>
		</row>
		<!--/xsl:if-->
		<!--xsl:if test="sum(DERegionalPremiumsCapital[number()=number()])!=0"-->
		<row>
			<cell colspan="2">
				<paragraph lineheight="1.3" size="{$WorksheetFontsize}" fontstyle="italic" indentationleft="30">
					<xsl:value-of select="Translations:GetTranslation('VerworpenUitgavenItem','DERegionalPremiumsCapital','DERegionalPremiumsCapital')"/>
				</paragraph>
			</cell>
			<cell horizontalalign="right" colspan="1">
				<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="italic">
					<xsl:value-of select="format-number(sum(DERegionalPremiumsCapital[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
			</cell>
			<cell colspan="1">
				<paragraph>
					<space/>
				</paragraph>
			</cell>
		</row>
		<!--/xsl:if-->
		<!--xsl:if test="sum(DEBepaaldeStaten[number()=number()])!=0"-->
		<row>
			<cell colspan="2">
				<paragraph lineheight="1.3" size="{$WorksheetFontsize}" fontstyle="italic" indentationleft="30">
					<xsl:value-of select="Translations:GetTranslation('VerworpenUitgavenItem','DEBepaaldeStaten','DEBepaaldeStaten')"/>
				</paragraph>
			</cell>
			<cell horizontalalign="right" colspan="1">
				<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="italic">
					<xsl:value-of select="format-number(sum(DEBepaaldeStaten[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
			</cell>
			<cell colspan="1">
				<paragraph>
					<space/>
				</paragraph>
			</cell>
		</row>
		<!--/xsl:if-->
		<!--xsl:if test="sum(DEOther[number()=number()])!=0"-->
		<row>
			<cell colspan="2">
				<paragraph lineheight="1.3" size="{$WorksheetFontsize}" fontstyle="italic" indentationleft="30">
					<xsl:value-of select="Translations:GetTranslation('VerworpenUitgavenItem','DEOther','DEOther')"/>
				</paragraph>
			</cell>
			<cell horizontalalign="right" colspan="1">
				<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="italic">
					<xsl:value-of select="format-number(sum(DEOther[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
			</cell>
			<cell colspan="1">
				<paragraph>
					<space/>
				</paragraph>
			</cell>
		</row>
		<!--/xsl:if-->
		<!--xsl:if test="sum(DEUnjustifiedExpenses[number()=number()])!=0"-->
		<row>
			<cell colspan="2">
				<paragraph lineheight="1.3" size="{$WorksheetFontsize}" fontstyle="italic" indentationleft="30">
					<xsl:value-of select="Translations:GetTranslation('VerworpenUitgavenItem','DEUnjustifiedExpenses','DEUnjustifiedExpenses')"/>
				</paragraph>
			</cell>
			<cell horizontalalign="right" colspan="1">
				<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="italic">
					<xsl:value-of select="format-number(sum(DEUnjustifiedExpenses[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
			</cell>
			<cell colspan="1">
				<paragraph>
					<space/>
				</paragraph>
			</cell>
		</row>
		<!--/xsl:if-->
	</xsl:template>
	<xsl:template match="BerekeningVenB|TaxCalc" mode="AftrekBeperkingVoorFiscaleAftrek">
		<table tablefitspage="true" convert2pdfp="true" cellsfitpage="true" width="100%" align="Center" cellspacing="0" columns="4" widths="64;12;12;12">
			<!--xsl:if test="sum(BerekeningOverzicht/TotalDeductionLimit[number()=number()])!=0"-->
			<row>
				<cell colspan="3">
					<paragraph lineheight="1.3" size="{$WorksheetFontsize}" fontstyle="bold" indentationleft="10">
						<xsl:value-of select="Translations:GetTranslation('General','TotaalBelastbareBestandenZonderAftrekMin','TotaalBelastbareBestandenZonderAftrekMin')"/>
					</paragraph>
				</cell>
				<cell horizontalalign="right" colspan="1">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="bold">
						<xsl:value-of select="format-number(sum(BerekeningOverzicht/TotalDeductionLimit[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
				</cell>
			</row>
			<!-- DETAIL -->
			<xsl:apply-templates select="BelastbareBestZonderAftrek" mode="DetailAftrekBeperkingVoorFiscaleAftrek"/>
			<!--/xsl:if-->
			<!--xsl:if test="sum(BerekeningOverzicht/RemainingTaxableResult[number()=number()])!=0"-->
			<row>
				<cell colspan="3" border="1" bordercolor="black" top="true">
					<paragraph lineheight="1.3" size="{$WorksheetFontsize}" fontstyle="bold">
						<xsl:value-of select="translate(Translations:GetTranslation('General','SubTotaalBelastbaarVoorAftrek','SubTotaalBelastbaarVoorAftrek'),$lo,$up)"/>
					</paragraph>
				</cell>
				<cell horizontalalign="right" colspan="1" border="1" bordercolor="black" top="true">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="bold">
						<xsl:value-of select="format-number(sum(BerekeningOverzicht/RemainingTaxableResult[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
				</cell>
			</row>
			<!--/xsl:if-->
		</table>
	</xsl:template>
	<xsl:template match="BelastbareBestZonderAftrek" mode="DetailAftrekBeperkingVoorFiscaleAftrek">
		<!--xsl:if test="sum(TECapitalGains0412[number()=number()])!=0"-->
		<row>
			<cell colspan="2">
				<paragraph lineheight="1.3" size="{$WorksheetFontsize}" fontstyle="italic" indentationleft="30">
					<xsl:value-of select="Translations:GetTranslation('BelastbareBestZonderAftrekItem','TECapitalGains0412','TECapitalGains0412')"/>
				</paragraph>
			</cell>
			<cell horizontalalign="right" colspan="1">
				<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="italic">
					<xsl:value-of select="format-number(sum(TECapitalGains0412[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
			</cell>
			<cell colspan="1">
				<paragraph>
					<space/>
				</paragraph>
			</cell>
		</row>
		<!--/xsl:if-->
		<!--xsl:if test="sum(TEReceivedAbnormal[number()=number()])!=0"-->
		<row>
			<cell colspan="2">
				<paragraph lineheight="1.3" size="{$WorksheetFontsize}" fontstyle="italic" indentationleft="30">
					<xsl:value-of select="Translations:GetTranslation('BelastbareBestZonderAftrekItem','TEReceivedAbnormal','TEReceivedAbnormal')"/>
				</paragraph>
			</cell>
			<cell horizontalalign="right" colspan="1">
				<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="italic">
					<xsl:value-of select="format-number(sum(TEReceivedAbnormal[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
			</cell>
			<cell colspan="1">
				<paragraph>
					<space/>
				</paragraph>
			</cell>
		</row>
		<!--/xsl:if-->
		<!--xsl:if test="sum(TETaxableInvestmentReserve[number()=number()])!=0"-->
		<row>
			<cell colspan="2">
				<paragraph lineheight="1.3" size="{$WorksheetFontsize}" fontstyle="italic" indentationleft="30">
					<xsl:value-of select="Translations:GetTranslation('BelastbareBestZonderAftrekItem','TETaxableInvestmentReserve','TETaxableInvestmentReserve')"/>
				</paragraph>
			</cell>
			<cell horizontalalign="right" colspan="1">
				<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" sfontstyle="italic">
					<xsl:value-of select="format-number(sum(TETaxableInvestmentReserve[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
			</cell>
			<cell colspan="1">
				<paragraph>
					<space/>
				</paragraph>
			</cell>
		</row>
		<!--/xsl:if-->
		<!--xsl:if test="sum(TEVaaCar[number()=number()])!=0"-->
		<row>
			<cell colspan="2">
				<paragraph lineheight="1.3" size="{$WorksheetFontsize}" fontstyle="italic" indentationleft="30">
					<xsl:value-of select="Translations:GetTranslation('BelastbareBestZonderAftrekItem','TEVaaCar','TEVaaCar')"/>
				</paragraph>
			</cell>
			<cell horizontalalign="right" colspan="1">
				<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="italic">
					<xsl:value-of select="format-number(sum(TEVaaCar[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
			</cell>
			<cell colspan="1">
				<paragraph>
					<space/>
				</paragraph>
			</cell>
		</row>
		<!--/xsl:if-->
		<!--xsl:if test="sum(TEEmployeeParticipationCapital[number()=number()])!=0"-->
		<row>
			<cell colspan="2">
				<paragraph lineheight="1.3" size="{$WorksheetFontsize}" fontstyle="italic" indentationleft="30">
					<xsl:value-of select="Translations:GetTranslation('BelastbareBestZonderAftrekItem','TEEmployeeParticipationCapital','TEEmployeeParticipationCapital')"/>
				</paragraph>
			</cell>
			<cell horizontalalign="right" colspan="1">
				<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="italic">
					<xsl:value-of select="format-number(sum(TEEmployeeParticipationCapital[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
			</cell>
			<cell colspan="1">
				<paragraph>
					<space/>
				</paragraph>
			</cell>
		</row>
		<!--/xsl:if-->
		<!--xsl:if test="sum(TECapitalAgriCulture[number()=number()])!=0"-->
		<row>
			<cell colspan="2">
				<paragraph lineheight="1.3" size="{$WorksheetFontsize}" fontstyle="italic" indentationleft="30">
					<xsl:value-of select="Translations:GetTranslation('BelastbareBestZonderAftrekItem','TECapitalAgriCulture','TECapitalAgriCulture')"/>
				</paragraph>
			</cell>
			<cell horizontalalign="right" colspan="1">
				<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="italic">
					<xsl:value-of select="format-number(sum(TECapitalAgriCulture[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
			</cell>
			<cell colspan="1">
				<paragraph>
					<space/>
				</paragraph>
			</cell>
		</row>
		<!--/xsl:if-->
	</xsl:template>
	<xsl:template match="BerekeningVenB|TaxCalc" mode="FiscaleAftrek">
		<table tablefitspage="false" convert2pdfp="true" cellsfitpage="true" width="100%" align="Center" cellspacing="0" columns="4" widths="64;12;12;12">
			<!--xsl:if test="sum(BerekeningOverzicht/TotalTaxTreaty[number()=number()])!=0"-->
			<xsl:if test="sum(BerekeningOverzicht/TotalTaxTreaty[number()=number()])&gt;0">				<row>					<cell colspan="3">						<paragraph lineheight="1.3" size="{$WorksheetFontsize}" fontstyle="bold" indentationleft="10">							<xsl:value-of select="Translations:GetTranslation('General','OpdelingNaarOorsprongBijVerdragVrijgesteldInkomen','OpdelingNaarOorsprongBijVerdragVrijgesteldInkomen')"/>						</paragraph>					</cell>					<cell horizontalalign="right" colspan="1">						<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="bold">							<xsl:value-of select="format-number(sum(BerekeningOverzicht/TotalTaxTreaty[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>					</cell>				</row>
			</xsl:if>
			<xsl:if test="sum(BerekeningOverzicht/TotalTaxTreaty[number()=number()])&lt;0">
				<row>
					<cell colspan="3">
						<paragraph lineheight="1.3" size="{$WorksheetFontsize}" fontstyle="bold" indentationleft="10">
							<xsl:value-of select="Translations:GetTranslation('General','OpdelingNaarOorsprongBijVerdragVrijgesteldInkomen','OpdelingNaarOorsprongBijVerdragVrijgesteldInkomen')"/>
						</paragraph>
					</cell>
					<cell horizontalalign="right" colspan="1">
						<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="bold">0,00€ </paragraph>
					</cell>
				</row>
			</xsl:if>
			<!--/xsl:if-->
			<!--xsl:if test="sum(BerekeningOverzicht/TotalFiscalDeductions[number()=number()])!=0"-->
			<row>
				<cell colspan="3">
					<paragraph lineheight="1.3" size="{$WorksheetFontsize}" fontstyle="bold" indentationleft="10">
						<xsl:value-of select="Translations:GetTranslation('General','TotaalFiscaleAftrek','TotaalFiscaleAftrek')"/>
					</paragraph>
				</cell>
				<cell horizontalalign="right" colspan="1">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="bold">
						<xsl:value-of select="format-number(sum(BerekeningOverzicht/TotalFiscalDeductions[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
				</cell>
			</row>
			<xsl:apply-templates select="FiscaleAftrek" mode="DetailFiscaleAftrek"/>
			<!--/xsl:if-->
			<!--xsl:if test="sum(BerekeningOverzicht/RemainingFiscalProfit[number()=number()])!=0"-->
			<row>
				<cell colspan="3" border="1" bordercolor="black" top="true">
					<paragraph lineheight="1.3" size="{$WorksheetFontsize}" fontstyle="bold">
						<xsl:value-of select="translate(Translations:GetTranslation('General','SubTotaalNaAftrek','SubTotaalNaAftrek'),$lo,$up)"/>
					</paragraph>
				</cell>
				<cell horizontalalign="right" colspan="1" border="1" bordercolor="black" top="true">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="bold">
						<xsl:value-of select="format-number(sum(BerekeningOverzicht/RemainingFiscalProfit[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
				</cell>
			</row>
			<!--/xsl:if-->
		</table>
	</xsl:template>
	<xsl:template match="FiscaleAftrek" mode="DetailFiscaleAftrek">
		<!--xsl:if test="sum(TotalNonTaxableElements[number()=number()])!=0"-->
		<row>
			<cell colspan="2">
				<paragraph lineheight="1.3" size="{$WorksheetFontsize}" fontstyle="italic" indentationleft="30">
					<xsl:value-of select="Translations:GetTranslation('FiscaleAftrekItem','TotalNonTaxableElements','TotalNonTaxableElements')"/>
				</paragraph>
			</cell>
			<cell horizontalalign="right" colspan="1">
				<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="italic">
					<xsl:value-of select="format-number(sum(TotalNonTaxableElements[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
			</cell>
			<cell colspan="1">
				<paragraph>
					<space/>
				</paragraph>
			</cell>
		</row>
		<!--/xsl:if-->
		<!--xsl:if test="sum(PEBelgium[number()=number()])!=0"-->
		<row>
			<cell colspan="2">
				<paragraph lineheight="1.3" size="{$WorksheetFontsize}" fontstyle="italic" indentationleft="30">
					<xsl:value-of select="Translations:GetTranslation('FiscaleAftrekItem','PEBelgium','PEBelgium')"/>
				</paragraph>
			</cell>
			<cell horizontalalign="right" colspan="1">
				<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="italic">
					<xsl:value-of select="format-number(sum(PEBelgium[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
			</cell>
			<cell colspan="1">
				<paragraph>
					<space/>
				</paragraph>
			</cell>
		</row>
		<!--/xsl:if-->
		<!--xsl:if test="sum(PIDBelgium[number()=number()])!=0"-->
		<row>
			<cell colspan="2">
				<paragraph lineheight="1.3" size="{$WorksheetFontsize}" fontstyle="italic" indentationleft="30">
					<xsl:value-of select="Translations:GetTranslation('FiscaleAftrekItem','PIDBelgium','PIDBelgium')"/>
				</paragraph>
			</cell>
			<cell horizontalalign="right" colspan="1">
				<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="italic">
					<xsl:value-of select="format-number(sum(PIDBelgium[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
			</cell>
			<cell colspan="1">
				<paragraph>
					<space/>
				</paragraph>
			</cell>
		</row>
		<!--/xsl:if-->
		<!--xsl:if test="sum(NIDBelgium[number()=number()])!=0"-->
		<row>
			<cell colspan="2">
				<paragraph lineheight="1.3" size="{$WorksheetFontsize}" fontstyle="italic" indentationleft="30">
					<xsl:value-of select="Translations:GetTranslation('FiscaleAftrekItem','NIDBelgium','NIDBelgium')"/>
				</paragraph>
			</cell>
			<cell horizontalalign="right" colspan="1">
				<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="italic">
					<xsl:value-of select="format-number(sum(NIDBelgium[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
			</cell>
			<cell colspan="1">
				<paragraph>
					<space/>
				</paragraph>
			</cell>
		</row>
		<!--/xsl:if-->
		<!--xsl:if test="sum(DTLBelgium[number()=number()])!=0"-->
		<row>
			<cell colspan="2">
				<paragraph lineheight="1.3" size="{$WorksheetFontsize}" fontstyle="italic" indentationleft="30">
					<xsl:value-of select="Translations:GetTranslation('FiscaleAftrekItem','DTLBelgium','DTLBelgium')"/>
				</paragraph>
			</cell>
			<cell horizontalalign="right" colspan="1">
				<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="italic">
					<xsl:value-of select="format-number(sum(DTLBelgium[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
			</cell>
			<cell colspan="1">
				<paragraph>
					<space/>
				</paragraph>
			</cell>
		</row>
		<!--/xsl:if-->
		<!--xsl:if test="sum(IDBelgium[number()=number()])!=0"-->
		<row>
			<cell colspan="2">
				<paragraph lineheight="1.3" size="{$WorksheetFontsize}" fontstyle="italic" indentationleft="30">
					<xsl:value-of select="Translations:GetTranslation('FiscaleAftrekItem','IDBelgium','IDBelgium')"/>
				</paragraph>
			</cell>
			<cell horizontalalign="right" colspan="1">
				<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="italic">
					<xsl:value-of select="format-number(sum(IDBelgium[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
			</cell>
			<cell colspan="1">
				<paragraph>
					<space/>
				</paragraph>
			</cell>
		</row>
		<!--/xsl:if-->
		<!--xsl:if test="sum(NIDHistoryBelgium[number()=number()])!=0"-->
		<row>
			<cell colspan="2">
				<paragraph lineheight="1.3" size="{$WorksheetFontsize}" fontstyle="italic" indentationleft="30">
					<xsl:value-of select="Translations:GetTranslation('FiscaleAftrekItem','NIDHistoryBelgium','NIDHistoryBelgium')"/>
				</paragraph>
			</cell>
			<cell horizontalalign="right" colspan="1">
				<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="italic">
					<xsl:value-of select="format-number(sum(NIDHistoryBelgium[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
			</cell>
			<cell colspan="1">
				<paragraph>
					<space/>
				</paragraph>
			</cell>
		</row>
		<!--/xsl:if-->
	</xsl:template>
	<xsl:template match="BerekeningVenB|TaxCalc" mode="GewoonTarief">
		<table tablefitspage="false" convert2pdfp="true" cellsfitpage="true" width="100%" align="Center" cellspacing="0" columns="4" widths="64;12;12;12">
			<row>
				<cell colspan="4">
					<paragraph>
						<space/>
					</paragraph>
				</cell>
			</row>
			<!--xsl:if test="sum(BerekeningOverzicht/CommonRateResult[number()=number()])!=0"-->
			<row>
				<cell colspan="3">
					<paragraph lineheight="1.3" size="{$WorksheetFontsize}" fontstyle="bold">
						<xsl:value-of select="Translations:GetTranslation('General','BelastbareGrondslag','BelastbareGrondslag')"/>
					</paragraph>
				</cell>
				<cell horizontalalign="right" colspan="1">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="bold">
						<xsl:value-of select="format-number(sum(BerekeningOverzicht/CommonRateResult[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
				</cell>
			</row>
			<!--/xsl:if-->
			<!--xsl:if test="sum(BerekeningOverzicht/CommonRateRemainingFiscalProfit[number()=number()])!=0"-->
			<row>
				<cell colspan="2">
					<paragraph lineheight="1.3" size="{$WorksheetFontsize}" fontstyle="italic" indentationleft="10">
						<xsl:value-of select="Translations:GetTranslation('General','SubTotaalNaAftrek','SubTotaalNaAftrek')"/>
					</paragraph>
				</cell>
				<cell horizontalalign="right" colspan="1">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="italic">
						<xsl:value-of select="format-number(sum(BerekeningOverzicht/CommonRateRemainingFiscalProfit[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
				</cell>
				<cell colspan="1">
					<paragraph>
						<space/>
					</paragraph>
				</cell>
			</row>
			<!--/xsl:if-->
			<!--xsl:if test="sum(BerekeningOverzicht/CommonRateDeductionLimit[number()=number()])!=0"-->
			<row>
				<cell colspan="2">
					<paragraph lineheight="1.3" size="{$WorksheetFontsize}" fontstyle="italic" indentationleft="10">
						<xsl:value-of select="Translations:GetTranslation('General','TotaalBelastbareBestandenZonderAftrekPlus','TotaalBelastbareBestandenZonderAftrekPlus')"/>
					</paragraph>
				</cell>
				<cell horizontalalign="right" colspan="1">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="italic">
						<xsl:value-of select="format-number(sum(BerekeningOverzicht/CommonRateDeductionLimit[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
				</cell>
				<cell colspan="1">
					<paragraph>
						<space/>
					</paragraph>
				</cell>
			</row>
			<!--/xsl:if-->
			<!--xsl:if test="sum(BerekeningOverzicht/CommonRateCapitalGains25[number()=number()])!=0"-->
			<row>
				<cell colspan="2">
					<paragraph lineheight="1.3" size="{$WorksheetFontsize}" fontstyle="italic" indentationleft="10"> - <xsl:value-of select="Translations:GetTranslation('BelastingsBerekeningItem','CapitalGainsShares25','CapitalGainsShares25')"/>
					</paragraph>
				</cell>
				<cell horizontalalign="right" colspan="1">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="italic">
						<xsl:value-of select="format-number(-sum(BerekeningOverzicht/CommonRateCapitalGains25[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
				</cell>
				<cell colspan="1">
					<paragraph>
						<space/>
					</paragraph>
				</cell>
			</row>
			<!--/xsl:if-->
		</table>
	</xsl:template>
	<xsl:template match="BerekeningOverzicht" mode="GewoonTariefDetail">
		<table tablefitspage="true" convert2pdfp="true" cellsfitpage="true" width="100%" align="Center" cellspacing="0" columns="5" widths="52;12;12;12;12">
			<!--xsl:if test="sum(EenduidigTariefTot[number()=number()])!=0"-->
			<row>
				<cell colspan="1">
					<paragraph lineheight="1.3" size="{$WorksheetFontsize}" fontstyle="bold">
						<xsl:value-of select="Translations:GetTranslation('BelastingsBerekeningItem','CorporateIncomeTax','CorporateIncomeTax')"/>
					</paragraph>
				</cell>
				<cell horizontalalign="right" colspan="1">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="italic">
						<xsl:value-of select="format-number(sum(EenduidigTariefBedrag[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
				</cell>
				<cell horizontalalign="right" colspan="1">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="italic">
						<xsl:value-of select="format-number(sum(EenduidigTariefPerc[number()=number()]),$PercFormatting,'euro')"/>% </paragraph>
				</cell>
				<cell horizontalalign="right" colspan="2">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="bold">
						<xsl:value-of select="format-number(sum(EenduidigTariefTot[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
				</cell>
			</row>
			<!--/xsl:if-->
			<xsl:variable name="verminderdTarTot" select="sum(VerminderdtariefStap1[number()=number()])+sum(VerminderdtariefStap2[number()=number()])+sum(VerminderdtariefStap3[number()=number()])"/>
			<!--xsl:if test="$verminderdTarTot!=0"-->
			<row>
				<cell colspan="4">
					<paragraph lineheight="1.3" size="{$WorksheetFontsize}" fontstyle="bold">
						<xsl:value-of select="Translations:GetTranslation('BelastingsBerekeningItem','VerminderdTariefTotaal','VerminderdTariefTotaal')"/>
					</paragraph>
				</cell>
				<cell horizontalalign="right" colspan="1">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="bold">
						<xsl:value-of select="format-number($verminderdTarTot,$NumberFormatting,'euro')"/>€ </paragraph>
				</cell>
			</row>
			<!--/xsl:if-->
			<!--xsl:if test="sum(VerminderdtariefStap1[number()=number()])!=0"-->
			<row>
				<cell colspan="1">
					<paragraph lineheight="1.3" size="{$WorksheetFontsize}" fontstyle="italic" indentationleft="10">
						<xsl:value-of select="Translations:GetTranslation('BelastingsBerekeningItem','VerminderdtariefStap1','VerminderdtariefStap1')"/>
					</paragraph>
				</cell>
				<cell horizontalalign="right" colspan="1">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="italic">
						<xsl:value-of select="format-number(sum(VerminderdtariefStap1Base[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
				</cell>
				<cell horizontalalign="right" colspan="1">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="italic">
						<xsl:value-of select="format-number(sum(VerminderdtariefStap1Perc[number()=number()]),$PercFormatting,'euro')"/>% </paragraph>
				</cell>
				<cell horizontalalign="right" colspan="1">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="italic">
						<xsl:value-of select="format-number(sum(VerminderdtariefStap1[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
				</cell>
				<cell horizontalalign="right" colspan="1">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="italic"/>
				</cell>
			</row>
			<!--/xsl:if-->
			<!--xsl:if test="sum(VerminderdtariefStap2[number()=number()])!=0"-->
			<row>
				<cell colspan="1">
					<paragraph lineheight="1.3" size="{$WorksheetFontsize}" fontstyle="italic" indentationleft="10">
						<xsl:value-of select="Translations:GetTranslation('BelastingsBerekeningItem','VerminderdtariefStap2','VerminderdtariefStap2')"/>
					</paragraph>
				</cell>
				<cell horizontalalign="right" colspan="1">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="italic">
						<xsl:value-of select="format-number(sum(VerminderdtariefStap2Base[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
				</cell>
				<cell horizontalalign="right" colspan="1">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="italic">
						<xsl:value-of select="format-number(sum(VerminderdtariefStap2Perc[number()=number()]),$PercFormatting,'euro')"/>% </paragraph>
				</cell>
				<cell horizontalalign="right" colspan="1">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="italic">
						<xsl:value-of select="format-number(sum(VerminderdtariefStap2[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
				</cell>
				<cell horizontalalign="right" colspan="1">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="italic"/>
				</cell>
			</row>
			<!--/xsl:if-->
			<!--xsl:if test="sum(VerminderdtariefStap3[number()=number()])!=0"-->
			<row>
				<cell colspan="1">
					<paragraph lineheight="1.3" size="{$WorksheetFontsize}" fontstyle="italic" indentationleft="10">
						<xsl:value-of select="Translations:GetTranslation('BelastingsBerekeningItem','VerminderdtariefStap3','VerminderdtariefStap3')"/>
					</paragraph>
				</cell>
				<cell horizontalalign="right" colspan="1">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="italic">
						<xsl:value-of select="format-number(sum(VerminderdtariefStap3Base[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
				</cell>
				<cell horizontalalign="right" colspan="1">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="italic">
						<xsl:value-of select="format-number(sum(VerminderdtariefStap3Perc[number()=number()]),$PercFormatting,'euro')"/>% </paragraph>
				</cell>
				<cell horizontalalign="right" colspan="1">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="italic">
						<xsl:value-of select="format-number(sum(VerminderdtariefStap3[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
				</cell>
				<cell horizontalalign="right" colspan="1">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="italic"/>
				</cell>
			</row>
			<!--/xsl:if-->
			<!--xsl:if test="sum(CapitalGainsShares25Result[number()=number()])!=0"-->
			<row>
				<cell colspan="1">
					<paragraph lineheight="1.3" size="{$WorksheetFontsize}" fontstyle="bold">
						<xsl:value-of select="Translations:GetTranslation('BelastingsBerekeningItem','CapitalGainsShares25','CapitalGainsShares25')"/>
					</paragraph>
				</cell>
				<cell horizontalalign="right" colspan="1">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="italic">
						<xsl:value-of select="format-number(sum(CapitalGainsShares25[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
				</cell>
				<cell horizontalalign="right" colspan="1">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="italic">
						<xsl:value-of select="format-number(sum(CapitalGainsShares25Perc[number()=number()]),$PercFormatting,'euro')"/>% </paragraph>
				</cell>
				<cell horizontalalign="right" colspan="2">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="bold">
						<xsl:value-of select="format-number(sum(CapitalGainsShares25Result[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
				</cell>
			</row>
			<!--/xsl:if-->
			<!--xsl:if test="sum(CapitalGainsShares0412Result[number()=number()])!=0"-->
			<row>
				<cell colspan="1">
					<paragraph lineheight="1.3" size="{$WorksheetFontsize}" fontstyle="bold">
						<xsl:value-of select="Translations:GetTranslation('BelastingsBerekeningItem','CapitalGainsShares0412','CapitalGainsShares0412')"/>
					</paragraph>
				</cell>
				<cell horizontalalign="right" colspan="1">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="italic">
						<xsl:value-of select="format-number(sum(CapitalGainsShares0412[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
				</cell>
				<cell horizontalalign="right" colspan="1">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="italic">
						<xsl:value-of select="format-number(sum(CapitalGainsShares0412Perc[number()=number()]),$PercFormatting,'euro')"/>% </paragraph>
				</cell>
				<cell horizontalalign="right" colspan="2">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="bold">
						<xsl:value-of select="format-number(sum(CapitalGainsShares0412Result[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
				</cell>
			</row>
			<!--/xsl:if-->
			<!--xsl:if test="sum(ExitTarifResult[number()=number()])!=0"-->
			<row>
				<cell colspan="1">
					<paragraph lineheight="1.3" size="{$WorksheetFontsize}" fontstyle="bold">
						<xsl:value-of select="Translations:GetTranslation('BelastingsBerekeningItem','ExitTarif','ExitTarif')"/>
					</paragraph>
				</cell>
				<cell horizontalalign="right" colspan="1">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="italic">
						<xsl:value-of select="format-number(sum(ExitTarif[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
				</cell>
				<cell horizontalalign="right" colspan="1">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="italic">
						<xsl:value-of select="format-number(sum(ExitTarifPerc[number()=number()]),$PercFormatting,'euro')"/>% </paragraph>
				</cell>
				<cell horizontalalign="right" colspan="2">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="bold">
						<xsl:value-of select="format-number(sum(ExitTarifResult[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
				</cell>
			</row>
			<!--/xsl:if-->
			<!--xsl:if test="sum(CorporateTaxBeforeRepayable[number()=number()])!=0"-->
			<row>
				<cell colspan="4" border="1" bordercolor="black" top="true">
					<paragraph lineheight="1.3" size="{$WorksheetFontsize}" fontstyle="bold">
						<xsl:value-of select="translate(Translations:GetTranslation('BelastingsBerekeningItem','BasisCorporateIncomeTax','BasisCorporateIncomeTax'),$lo,$up)"/>
					</paragraph>
				</cell>
				<cell horizontalalign="right" colspan="1" border="1" bordercolor="black" top="true">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="bold">
						<xsl:value-of select="format-number(sum(CorporateTaxBeforeRepayable[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
				</cell>
			</row>
			<!--/xsl:if-->
		</table>
	</xsl:template>
	<xsl:template match="BerekeningOverzicht" mode="VerrekenbareVoorheffingen">
		<table tablefitspage="true" convert2pdfp="true" cellsfitpage="true" width="100%" align="Center" cellspacing="0" columns="5" widths="52;12;12;12;12">
			<!--xsl:if test="sum(RepayableTotal[number()=number()])!=0"-->
			<row>
				<cell colspan="4">
					<paragraph lineheight="1.3" size="{$WorksheetFontsize}" fontstyle="bold">
						<xsl:value-of select="Translations:GetTranslation('BelastingsBerekeningItem','RepayableTotal','RepayableTotal')"/>
					</paragraph>
				</cell>
				<cell horizontalalign="right" colspan="1">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="bold"> - <xsl:value-of select="format-number(sum(RepayableTotal[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
				</cell>
			</row>
			<!--/xsl:if-->
			<!--xsl:if test="sum(NonRepayableAdvanceLevies[number()=number()])!=0"-->
			<row>
				<cell colspan="3">
					<paragraph lineheight="1.3" size="{$WorksheetFontsize}" fontstyle="italic" indentationleft="10">
						<xsl:value-of select="Translations:GetTranslation('BelastingsBerekeningItem','NonRefundableWithholdingTaxes_TaxCredit','NonRefundableWithholdingTaxes_TaxCredit')"/>
					</paragraph>
				</cell>
				<cell horizontalalign="right" colspan="1">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="italic"> - <xsl:value-of select="format-number(sum(NonRepayableAdvanceLevies[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
				</cell>
				<cell horizontalalign="right" colspan="1">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle=""/>
				</cell>
			</row>
			<!--/xsl:if-->
			<!--xsl:if test="sum(RepayableAdvanceLevies[number()=number()])!=0"-->
			<row>
				<cell colspan="3">
					<paragraph lineheight="1.3" size="{$WorksheetFontsize}" fontstyle="italic" indentationleft="10">
						<xsl:value-of select="Translations:GetTranslation('BelastingsBerekeningItem','RefundableWithholdingTaxes','RefundableWithholdingTaxes')"/>
					</paragraph>
				</cell>
				<cell horizontalalign="right" colspan="1">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="italic"> - <xsl:value-of select="format-number(sum(RepayableAdvanceLevies[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
				</cell>
				<cell horizontalalign="right" colspan="1">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle=""/>
				</cell>
			</row>
			<!--/xsl:if-->
			<!--xsl:if test="sum(RepayableResearchDevelopment[number()=number()])!=0"-->
			<row>
				<cell colspan="3">
					<paragraph lineheight="1.3" size="{$WorksheetFontsize}" fontstyle="italic" indentationleft="10">
						<xsl:value-of select="Translations:GetTranslation('BelastingsBerekeningItem','RefundableTaxeCredit','RefundableTaxeCredit')"/>
					</paragraph>
				</cell>
				<cell horizontalalign="right" colspan="1">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="italic"> - <xsl:value-of select="format-number(sum(RepayableResearchDevelopment[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
				</cell>
				<cell horizontalalign="right" colspan="1">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle=""/>
				</cell>
			</row>
			<!--/xsl:if-->
		</table>
	</xsl:template>  <xsl:template match="BerekeningOverzicht" mode="BasisVoorVermeerdering">
		<table tablefitspage="false" convert2pdfp="true" cellsfitpage="true" width="100%" align="Center" cellspacing="0" columns="5" widths="52;12;12;12;12">
			<row>
				<cell colspan="5">
					<paragraph>
						<space/>
					</paragraph>
				</cell>
			</row>
			<!--xsl:if test="sum(BaseForIncrease[number()=number()])!=0"-->
			<row>
				<cell colspan="3">
					<paragraph lineheight="1.3" size="{$WorksheetFontsize}" fontstyle="" indentationleft="10">
						<xsl:value-of select="Translations:GetTranslation('BelastingsBerekeningItem','BasisForIncrease','BasisForIncrease')"/>
					</paragraph>
				</cell>
				<cell horizontalalign="right" colspan="1">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="">
						<xsl:value-of select="format-number(sum(BaseForIncrease[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
				</cell>
				<cell horizontalalign="right" colspan="1">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle=""/>
				</cell>
			</row>
			<!--/xsl:if-->
			<!--xsl:if test="sum(CorporateTaxAfterRepayable[number()=number()])!=0"-->
			<row>
				<cell colspan="2">
					<paragraph lineheight="1.3" size="{$WorksheetFontsize}" fontstyle="italic" indentationleft="15">
						<xsl:value-of select="Translations:GetTranslation('BelastingsBerekeningItem','CorporateTaxAfterRepayable','CorporateTaxAfterRepayable')"/>
					</paragraph>
				</cell>
				<cell horizontalalign="right" colspan="1">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="italic">
						<xsl:value-of select="format-number(sum(CorporateTaxAfterRepayable[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
				</cell>
				<cell horizontalalign="right" colspan="2">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="italic"/>
				</cell>
			</row>
			<!--/xsl:if-->
			<!--xsl:if test="sum(BaseForIncreaseFairnessTax[number()=number()])!=0"-->
			<row>
				<cell colspan="2">
					<paragraph lineheight="1.3" size="{$WorksheetFontsize}" fontstyle="italic" indentationleft="15">+ <xsl:value-of select="Translations:GetTranslation('BelastingsBerekeningItem','BaseForIncreaseFairnessTax','BaseForIncreaseFairnessTax')"/>
					</paragraph>
				</cell>
				<cell horizontalalign="right" colspan="1">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="italic">+ <xsl:value-of select="format-number(sum(BaseForIncreaseFairnessTax[number()=number()]),$NumberFormatting,'euro')"/>€</paragraph>
				</cell>
				<cell horizontalalign="right" colspan="2">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="italic"/>
				</cell>
			</row>
			<!--/xsl:if-->
			<!--xsl:if test="sum(RetributionResearchDevelopmentTotal[number()=number()])!=0"-->
			<row>
				<cell colspan="2">
					<paragraph lineheight="1.3" size="{$WorksheetFontsize}" fontstyle="italic" indentationleft="15">+ <xsl:value-of select="Translations:GetTranslation('AanslagItem','TerugbetalingBelastingskrediet','TerugbetalingBelastingskrediet')"/>
					</paragraph>
				</cell>
				<cell horizontalalign="right" colspan="1">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="italic">+ <xsl:value-of select="format-number(sum(RetributionResearchDevelopmentTotal[number()=number()]),$NumberFormatting,'euro')"/>€</paragraph>
				</cell>
				<cell horizontalalign="right" colspan="2">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="italic"/>
				</cell>
			</row>
			<!--/xsl:if-->
			<!--xsl:if test="sum(BaseForIncrease[number()=number()])!=0"-->
			<row>
				<cell colspan="1">
					<paragraph lineheight="1.3" size="{$WorksheetFontsize}" fontstyle="" indentationleft="10">
						<xsl:value-of select="Translations:GetTranslation('BelastingsBerekeningItem','BTITotal','BTITotal')"/>
					</paragraph>
				</cell>
				<cell horizontalalign="right" colspan="1">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="italic">
						<xsl:value-of select="format-number(sum(BaseForIncrease[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
				</cell>
				<cell horizontalalign="right" colspan="1">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="italic">
						<xsl:value-of select="format-number(sum(BaseForIncreasePerc[number()=number()]),$PercFormatting,'euro')"/>% </paragraph>
				</cell>
				<cell horizontalalign="right" colspan="1">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="">
						<xsl:value-of select="format-number(sum(BaseForIncreaseTotal[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
				</cell>
				<cell horizontalalign="right" colspan="1">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle=""/>
				</cell>
			</row>
			<!--/xsl:if-->
		</table>
	</xsl:template>
	<xsl:template match="BerekeningOverzicht" mode="Voorafbetalingen">
		<table tablefitspage="false" convert2pdfp="true" cellsfitpage="true" width="100%" align="Center" cellspacing="0" columns="5" widths="30;12;12;12;34">
			<row>
				<cell colspan="5">
					<paragraph>
						<space/>
					</paragraph>
				</cell>
			</row>
			<row>
				<cell colspan="4">
					<paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="" fontstyle="bold">
						<xsl:value-of select="Translations:GetTranslation('BelastingsBerekeningItem','AdvancePayments','AdvancePayments')"/>
					</paragraph>
				</cell>
				<cell horizontalalign="right" colspan="1">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="bold">
						<xsl:value-of select="format-number(sum(AdvancePayments[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
				</cell>
			</row>
			<row>
				<cell colspan="1">
					<paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="">
						<xsl:value-of select="Translations:GetTranslation('BelastingsBerekeningItem','AP1Base','AP1Base')"/>
					</paragraph>
				</cell>
				<cell horizontalalign="right" colspan="1">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
						<xsl:value-of select="format-number(sum(AP1Base[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
				</cell>
				<cell colspan="1" horizontalalign="right">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
						<xsl:value-of select="format-number(sum(AP1Percentage[number()=number()]),$PercFormatting,'euro')"/>% </paragraph>
				</cell>
				<cell colspan="1" horizontalalign="right">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
						<xsl:value-of select="format-number(sum(AP1Total[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
				</cell>
				<cell colspan="1" horizontalalign="right">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style=""/>
				</cell>
			</row>
			<row>
				<cell colspan="1">
					<paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="">
						<xsl:value-of select="Translations:GetTranslation('BelastingsBerekeningItem','AP2Base','AP2Base')"/>
					</paragraph>
				</cell>
				<cell horizontalalign="right" colspan="1">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
						<xsl:value-of select="format-number(sum(AP2Base[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
				</cell>
				<cell colspan="1" horizontalalign="right">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
						<xsl:value-of select="format-number(sum(AP2Percentage[number()=number()]),$PercFormatting,'euro')"/>% </paragraph>
				</cell>
				<cell colspan="1" horizontalalign="right">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
						<xsl:value-of select="format-number(sum(AP2Total[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
				</cell>
				<cell colspan="1" horizontalalign="right">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style=""/>
				</cell>
			</row>
			<row>
				<cell colspan="1">
					<paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="">
						<xsl:value-of select="Translations:GetTranslation('BelastingsBerekeningItem','AP3Base','AP3Base')"/>
					</paragraph>
				</cell>
				<cell horizontalalign="right" colspan="1">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
						<xsl:value-of select="format-number(sum(AP3Base[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
				</cell>
				<cell colspan="1" horizontalalign="right">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
						<xsl:value-of select="format-number(sum(AP3Percentage[number()=number()]),$PercFormatting,'euro')"/>% </paragraph>
				</cell>
				<cell colspan="1" horizontalalign="right">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
						<xsl:value-of select="format-number(sum(AP3Total[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
				</cell>
				<cell colspan="1" horizontalalign="right">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style=""/>
				</cell>
			</row>
			<row>
				<cell colspan="1">
					<paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="">
						<xsl:value-of select="Translations:GetTranslation('BelastingsBerekeningItem','AP4Base','AP4Base')"/>
					</paragraph>
				</cell>
				<cell horizontalalign="right" colspan="1">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
						<xsl:value-of select="format-number(sum(AP4Base[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
				</cell>
				<cell colspan="1" horizontalalign="right">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
						<xsl:value-of select="format-number(sum(AP4Percentage[number()=number()]),$PercFormatting,'euro')"/>% </paragraph>
				</cell>
				<cell colspan="1" horizontalalign="right">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
						<xsl:value-of select="format-number(sum(AP4Total[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
				</cell>
				<cell colspan="1" horizontalalign="right">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style=""/>
				</cell>
			</row>
			<row>
				<cell colspan="1" border="1" bordercolor="black" top="true">
					<paragraph lineheight="1.3" size="{$WorksheetFontsize}" style="">
						<xsl:value-of select="Translations:GetTranslation('BelastingsBerekeningItem','APBaseTotal','APBaseTotal')"/>
					</paragraph>
				</cell>
				<cell horizontalalign="right" colspan="1" border="1" bordercolor="black" top="true">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}">
						<xsl:value-of select="format-number(sum(APBaseTotal[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
				</cell>
				<cell colspan="1" border="1" bordercolor="black" top="true">
					<paragraph>
						<space/>
					</paragraph>
				</cell>
				<cell colspan="1" horizontalalign="right" border="1" bordercolor="black" top="true">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="">
						<xsl:value-of select="format-number(sum(APTotal[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
				</cell>
				<cell colspan="1" horizontalalign="right" border="0" bordercolor="black" top="false">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style=""/>
				</cell>
			</row>
		</table>
		<table tablefitspage="false" convert2pdfp="true" cellsfitpage="true" width="100%" align="Center" cellspacing="0" columns="5" widths="52;12;12;12;12">
			<!--xsl:if test="sum(BaseForIncreaseApplied[number()=number()])!=0"-->
			<row>
				<cell colspan="4">
					<paragraph lineheight="1.3" size="{$WorksheetFontsize}" fontstyle="bold">+ <xsl:value-of select="Translations:GetTranslation('BelastingsBerekeningItem','ApplicableTaxIncrease','ApplicableTaxIncrease')"/>
					</paragraph>
				</cell>
				<cell horizontalalign="right" colspan="1">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="bold">
						<xsl:value-of select="format-number(sum(BaseForIncreaseApplied[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
				</cell>
			</row>
			<!--/xsl:if-->
		</table>
	</xsl:template>
	<xsl:template match="BerekeningOverzicht" mode="AfzonderlijkeAanslag">
		<table tablefitspage="true" convert2pdfp="true" cellsfitpage="true" width="100%" align="Center" cellspacing="0" columns="4" widths="64;12;12;12">
			<row>
				<cell colspan="4">
					<paragraph>
						<space/>
					</paragraph>
				</cell>
			</row>
			<!--xsl:if test="sum(KapitaalEnInterestsubsidies[number()=number()])!=0"-->
			<row>
				<cell colspan="1">
					<paragraph lineheight="1.3" size="{$WorksheetFontsize}" fontstyle="bold">
						<xsl:value-of select="translate(Translations:GetTranslation('BelastbareBestZonderAftrekItem','TECapitalAgriCulture','TECapitalAgriCulture'), $lo,$up)"/>
						<newline/>
						<space/>
					</paragraph>
				</cell>
				<cell horizontalalign="right" colspan="1" verticalalign="top">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="italic">
						<xsl:value-of select="format-number(sum(KapitaalEnInterestsubsidies[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
				</cell>
				<cell horizontalalign="right" colspan="1" verticalalign="top">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="italic">
						<xsl:value-of select="format-number(sum(KapitaalEnInterestsubsidiesPerc[number()=number()]),$PercFormatting,'euro')"/>% </paragraph>
				</cell>
				<cell horizontalalign="right" colspan="1" verticalalign="top">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="bold">
						<xsl:value-of select="format-number(sum(KapitaalEnInterestsubsidiesResult[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
				</cell>
			</row>
			<!--/xsl:if-->
			<!--xsl:if test="sum(UnjustifiedExpensesHiddenSurplusBase[number()=number()])!=0"-->
			<row>
				<cell colspan="1">
					<paragraph lineheight="1.3" size="{$WorksheetFontsize}" fontstyle="bold">
						<xsl:value-of select="translate(Translations:GetTranslation('AanslagItem','NietVerantwoordeKosten','NietVerantwoordeKosten'), $lo,$up)"/>
						<newline/>
						<space/>
					</paragraph>
				</cell>
				<cell horizontalalign="right" colspan="1" verticalalign="top">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="italic">
						<xsl:value-of select="format-number(sum(UnjustifiedExpensesHiddenSurplusBase[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
				</cell>
				<cell horizontalalign="right" colspan="1" verticalalign="top">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="italic">
						<xsl:value-of select="format-number(sum(UnjustifiedExpensesHiddenSurplusPerc[number()=number()]),$PercFormatting,'euro')"/>% </paragraph>
				</cell>
				<cell horizontalalign="right" colspan="1" verticalalign="top">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="bold">
						<xsl:value-of select="format-number(sum(UnjustifiedExpensesHiddenSurplusTotal[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
				</cell>
			</row>
			<!--/xsl:if-->
			<!--xsl:if test="sum(SeparateAssessmentTaxedReserves3400Base[number()=number()])!=0"-->
			<row>
				<cell colspan="1">
					<paragraph lineheight="1.3" size="{$WorksheetFontsize}" fontstyle="bold">
						<xsl:value-of select="translate(Translations:GetTranslation('AanslagItem','AfzonderlijkeAanslag34','AfzonderlijkeAanslag34'), $lo,$up)"/>
						<newline/>
						<space/>
					</paragraph>
				</cell>
				<cell horizontalalign="right" colspan="1" verticalalign="top">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="italic">
						<xsl:value-of select="format-number(sum(SeparateAssessmentTaxedReserves3400Base[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
				</cell>
				<cell horizontalalign="right" colspan="1" verticalalign="top">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="italic">
						<xsl:value-of select="format-number(sum(SeparateAssessmentTaxedReserves3400Perc[number()=number()]),$PercFormatting,'euro')"/>% </paragraph>
				</cell>
				<cell horizontalalign="right" colspan="1" verticalalign="top">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="bold">
						<xsl:value-of select="format-number(sum(SeparateAssessmentTaxedReserves3400Total[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
				</cell>
			</row>
			<!--/xsl:if-->
			<!--xsl:if test="sum(SeparateAssessmentTaxedReserves2800Base[number()=number()])!=0"-->
			<row>
				<cell colspan="1">
					<paragraph lineheight="1.3" size="{$WorksheetFontsize}" fontstyle="bold">
						<xsl:value-of select="translate(Translations:GetTranslation('AanslagItem','AfzonderlijkeAanslag28','AfzonderlijkeAanslag28'), $lo,$up)"/>
						<newline/>
						<space/>
					</paragraph>
				</cell>
				<cell horizontalalign="right" colspan="1" verticalalign="top">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="italic">
						<xsl:value-of select="format-number(sum(SeparateAssessmentTaxedReserves2800Base[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
				</cell>
				<cell horizontalalign="right" colspan="1" verticalalign="top">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="italic">
						<xsl:value-of select="format-number(sum(SeparateAssessmentTaxedReserves2800Perc[number()=number()]),$PercFormatting,'euro')"/>% </paragraph>
				</cell>
				<cell horizontalalign="right" colspan="1" verticalalign="top">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="bold">
						<xsl:value-of select="format-number(sum(SeparateAssessmentTaxedReserves2800Total[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
				</cell>
			</row>
			<!--/xsl:if-->
			<!--xsl:if test="sum(SeparateAssessmentDividendsPaidBase[number()=number()])!=0"-->
			<row>
				<cell colspan="1">
					<paragraph lineheight="1.3" size="{$WorksheetFontsize}" fontstyle="bold">
						<xsl:value-of select="translate(Translations:GetTranslation('AanslagItem','AfzonderlijkeAanslagDividenden','AfzonderlijkeAanslagDividenden'), $lo,$up)"/>
						<newline/>
						<space/>
					</paragraph>
				</cell>
				<cell horizontalalign="right" colspan="1" verticalalign="top">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="italic">
						<xsl:value-of select="format-number(sum(SeparateAssessmentDividendsPaidBase[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
				</cell>
				<cell horizontalalign="right" colspan="1" verticalalign="top">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="italic">
						<xsl:value-of select="format-number(sum(SeparateAssessmentDividendsPaidPerc[number()=number()]),$PercFormatting,'euro')"/>% </paragraph>
				</cell>
				<cell horizontalalign="right" colspan="1" verticalalign="top">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="bold">
						<xsl:value-of select="format-number(sum(SeparateAssessmentDividendsPaidTotal[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
				</cell>
			</row>
			<!--/xsl:if-->
			<!--xsl:if test="sum(FairnessTaxBase[number()=number()])!=0"-->
			<row>
				<cell colspan="1">
					<paragraph lineheight="1.3" size="{$WorksheetFontsize}" fontstyle="bold">
						<xsl:value-of select="translate(Translations:GetTranslation('FairnessTax','FairnessTaxResult','FairnessTaxResult'), $lo,$up)"/>
						<newline/>
						<space/>
					</paragraph>
				</cell>
				<cell horizontalalign="right" colspan="1" verticalalign="top">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="italic">
						<xsl:value-of select="format-number(sum(FairnessTaxBase[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
				</cell>
				<cell horizontalalign="right" colspan="1" verticalalign="top">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="italic">
						<xsl:value-of select="format-number(sum(FairnessTaxPerc[number()=number()]),$PercFormatting,'euro')"/>% </paragraph>
				</cell>
				<cell horizontalalign="right" colspan="1" verticalalign="top">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="bold">
						<xsl:value-of select="format-number(sum(FairnessTaxTotal[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
				</cell>
			</row>
			<!--/xsl:if-->
			<!--xsl:if test="sum(SeparateAssessmentTaxPaidTaxedReservesBase[number()=number()])!=0"-->
			<row>
				<cell colspan="1">
					<paragraph lineheight="1.3" size="{$WorksheetFontsize}" fontstyle="bold">
						<xsl:value-of select="translate(Translations:GetTranslation('AanslagItem','AfzonderlijkeAanslagRV','AfzonderlijkeAanslagRV'), $lo,$up)"/>
						<newline/>
						<space/>
					</paragraph>
				</cell>
				<cell horizontalalign="right" colspan="1" verticalalign="top">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="italic">
						<xsl:value-of select="format-number(sum(SeparateAssessmentTaxPaidTaxedReservesBase[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
				</cell>
				<cell horizontalalign="right" colspan="1" verticalalign="top">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="italic">
						<xsl:value-of select="format-number(sum(SeparateAssessmentTaxPaidTaxedReservesPerc[number()=number()]),$PercFormatting,'euro')"/>% </paragraph>
				</cell>
				<cell horizontalalign="right" colspan="1" verticalalign="top">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="bold">
						<xsl:value-of select="format-number(sum(SeparateAssessmentTaxPaidTaxedReservesTotal[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
				</cell>
			</row>
			<!--/xsl:if-->
			<!--xsl:if test="sum(DistributionCompanyAssets3300Base[number()=number()])!=0"-->
			<row>
				<cell colspan="1">
					<paragraph lineheight="1.3" size="{$WorksheetFontsize}" fontstyle="bold">
						<xsl:value-of select="translate(Translations:GetTranslation('AanslagItem','VerdelingVermogen33','VerdelingVermogen33'), $lo,$up)"/>
						<newline/>
						<space/>
					</paragraph>
				</cell>
				<cell horizontalalign="right" colspan="1" verticalalign="top">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="italic">
						<xsl:value-of select="format-number(sum(DistributionCompanyAssets3300Base[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
				</cell>
				<cell horizontalalign="right" colspan="1" verticalalign="top">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="italic">
						<xsl:value-of select="format-number(sum(DistributionCompanyAssets3300Perc[number()=number()]),$PercFormatting,'euro')"/>% </paragraph>
				</cell>
				<cell horizontalalign="right" colspan="1" verticalalign="top">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="bold">
						<xsl:value-of select="format-number(sum(DistributionCompanyAssets3300Total[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
				</cell>
			</row>
			<!--/xsl:if-->
			<!--xsl:if test="sum(DistributionCompanyAssets1650Base[number()=number()])!=0"-->
			<row>
				<cell colspan="1">
					<paragraph lineheight="1.3" size="{$WorksheetFontsize}" fontstyle="bold">
						<xsl:value-of select="translate(Translations:GetTranslation('AanslagItem','VerdelingVermogen16','VerdelingVermogen16'), $lo,$up)"/>
						<newline/>
						<space/>
					</paragraph>
				</cell>
				<cell horizontalalign="right" colspan="1" verticalalign="top">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="italic">
						<xsl:value-of select="format-number(sum(DistributionCompanyAssets1650Base[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
				</cell>
				<cell horizontalalign="right" colspan="1" verticalalign="top">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="italic">
						<xsl:value-of select="format-number(sum(DistributionCompanyAssets1650Perc[number()=number()]),$PercFormatting,'euro')"/>% </paragraph>
				</cell>
				<cell horizontalalign="right" colspan="1" verticalalign="top">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="bold">
						<xsl:value-of select="format-number(sum(DistributionCompanyAssets1650Total[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
				</cell>
			</row>
			<!--/xsl:if-->
			<!--xsl:if test="sum(BenefitsAllKindCompanyLiquidationBase[number()=number()])!=0"-->
			<row>
				<cell colspan="1">
					<paragraph lineheight="1.3" size="{$WorksheetFontsize}" fontstyle="bold">
						<xsl:value-of select="translate(Translations:GetTranslation('AanslagItem','VAAVennootschappen','VAAVennootschappen'), $lo,$up)"/>
						<newline/>
						<space/>
					</paragraph>
				</cell>
				<cell horizontalalign="right" colspan="1" verticalalign="top">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="italic">
						<xsl:value-of select="format-number(sum(BenefitsAllKindCompanyLiquidationBase[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
				</cell>
				<cell horizontalalign="right" colspan="1" verticalalign="top">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="italic">
						<xsl:value-of select="format-number(sum(BenefitsAllKindCompanyLiquidationPerc[number()=number()]),$PercFormatting,'euro')"/>% </paragraph>
				</cell>
				<cell horizontalalign="right" colspan="1" verticalalign="top">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="bold">
						<xsl:value-of select="format-number(sum(BenefitsAllKindCompanyLiquidationTotal[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
				</cell>
			</row>
			<!--/xsl:if-->
			<!--xsl:if test="sum(AdditionalDutiesDiamondTradersBase[number()=number()])!=0"-->
			<row>
				<cell colspan="1">
					<paragraph lineheight="1.3" size="{$WorksheetFontsize}" fontstyle="bold">
						<xsl:value-of select="translate(Translations:GetTranslation('AanslagItem','AanvullendeHeffingDiamant','AanvullendeHeffingDiamant'), $lo,$up)"/>
						<newline/>
						<space/>
					</paragraph>
				</cell>
				<cell horizontalalign="right" colspan="1" verticalalign="top">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="italic">
						<xsl:value-of select="format-number(sum(AdditionalDutiesDiamondTradersBase[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
				</cell>
				<cell horizontalalign="right" colspan="1" verticalalign="top">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="italic">
						<xsl:value-of select="format-number(sum(AdditionalDutiesDiamondTradersPerc[number()=number()]),$PercFormatting,'euro')"/>% </paragraph>
				</cell>
				<cell horizontalalign="right" colspan="1" verticalalign="top">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="bold">
						<xsl:value-of select="format-number(sum(AdditionalDutiesDiamondTradersTotal[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
				</cell>
			</row>
			<!--/xsl:if-->
			<!--xsl:if test="sum(RetributionResearchDevelopmentBase[number()=number()])!=0"-->
			<row>
				<cell colspan="1">
					<paragraph lineheight="1.3" size="{$WorksheetFontsize}" fontstyle="bold">
						<xsl:value-of select="translate(Translations:GetTranslation('AanslagItem','TerugbetalingBelastingskrediet','TerugbetalingBelastingskrediet'), $lo,$up)"/>
						<newline/>
						<space/>
					</paragraph>
				</cell>
				<cell horizontalalign="right" colspan="1" verticalalign="top">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="italic">
						<xsl:value-of select="format-number(sum(RetributionResearchDevelopmentBase[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
				</cell>
				<cell horizontalalign="right" colspan="1" verticalalign="top">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="italic">
						<xsl:value-of select="format-number(sum(RetributionResearchDevelopmentPerc[number()=number()]),$PercFormatting,'euro')"/>% </paragraph>
				</cell>
				<cell horizontalalign="right" colspan="1" verticalalign="top">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="bold">
						<xsl:value-of select="format-number(sum(RetributionResearchDevelopmentTotal[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
				</cell>
			</row>
			<!--/xsl:if-->
			<!--xsl:if test="sum(IPTBase[number()=number()])!=0"-->
			<row>
				<cell colspan="1" verticalalign="center" >
					<paragraph lineheight="1" size="{$WorksheetFontsize}" fontstyle="bold" >
						<xsl:value-of select="translate(Translations:GetTranslation('AanslagItem','AfzonderlijkeAanslagPensioenToezeggingen','AfzonderlijkeAanslagPensioenToezeggingen'), $lo,$up)"/>
						<newline/>
						<space/>
					</paragraph>
				</cell>
				<cell horizontalalign="right" colspan="1" verticalalign="top">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="italic">
						<xsl:value-of select="format-number(sum(IPTBase[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
				</cell>
				<cell horizontalalign="right" colspan="1" verticalalign="top">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="italic">
						<xsl:value-of select="format-number(sum(IPTPerc[number()=number()]),$PercFormatting,'euro')"/>% </paragraph>
				</cell>
				<cell horizontalalign="right" colspan="1" verticalalign="top">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="bold">
						<xsl:value-of select="format-number(sum(IPTTotal[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
				</cell>
			</row>
			<!--/xsl:if-->
		</table>
	</xsl:template>
	<xsl:template match="BerekeningOverzicht" mode="RESULT">
		<table tablefitspage="true" convert2pdfp="true" cellsfitpage="true" width="100%" align="Center" cellspacing="0" columns="5" widths="64;12;12;12">
			<row>
				<cell colspan="4" top="true" bottom="true">
					<paragraph lineheight="1" size="{$WorksheetFontsize}" style="font-weight:bold;">
						<newline/>
						<xsl:choose>
							<xsl:when test="number(TaxResult)&lt;0">
								<xsl:value-of select="translate(Translations:GetTranslation('BelastingsBerekeningItem','RECIEVE','RECIEVE'), $lo,$up)"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="translate(Translations:GetTranslation('BelastingsBerekeningItem','PAY','PAY'), $lo,$up)"/>
							</xsl:otherwise>
						</xsl:choose>
						<newline/>
						<space/>
					</paragraph>
				</cell>
				<cell horizontalalign="right" colspan="1" top="true" bottom="true">
					<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" style="font-weight:bold;">
						<newline/>
						<xsl:value-of select="format-number(sum(TaxResult[number()=number()]),$NumberFormatting,'euro')"/>€ <newline/>
						<space/>
					</paragraph>
				</cell>
			</row>
		</table>
	</xsl:template>
	<xsl:template match="*"/>
</xsl:stylesheet>