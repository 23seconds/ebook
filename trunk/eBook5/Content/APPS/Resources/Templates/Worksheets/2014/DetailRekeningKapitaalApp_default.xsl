<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" exclude-result-prefixes="Helper Translations File Client" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:Helper="urn:Helper" xmlns:Translations="urn:Translations" xmlns:File="urn:File" xmlns:Client="urn:Client">
	<xsl:decimal-format name="NonNumber" NaN="0" zero-digit="0"/>
	<xsl:variable name="WorksheetLabelFontsize" select="string('10')"/>
	<xsl:variable name="WorksheetFontsize" select="string('10')"/>
	<xsl:variable name="WorksheetNumbersize" select="string('9')"/>
	<xsl:variable name="WorksheetSmallFontsize" select="string('8')"/>
	<xsl:variable name="WorksheetTotalsFontsize" select="string('10')"/>
	<xsl:variable name="WorksheetSpacing" select="1.1"/>
	<xsl:decimal-format name="euro" decimal-separator="," grouping-separator="."/>
	<xsl:variable name="NumberFormatting" select="string('#.##0,00')"/>
	<xsl:variable name="NumberFormattingNoDigit" select="string('#.##0')"/>
	<xsl:variable name="FONT" select="string('C:\WINDOWS\Fonts\EYInterstate-Light.ttf')"/>
	<xsl:param name="RuleAppDef"/>
	<xsl:param name="Culture"/>
	<xsl:template match="DetailRekeningKapitaal">
		<Root orientation="landscape">
			<xsl:if test="count(Overzicht/*)>0"> 		 <xsl:call-template name="RENDERTAB">
					<xsl:with-param name="nodelist" select="Overzicht/*"/>
					<xsl:with-param name="title" select="Translations:GetTranslation('DetailRekeningKapitaal','Overzicht','Overzicht')"/>
				</xsl:call-template>
				<xsl:apply-templates select="Overzicht/*" mode="RENDERDETAIL"/>
			</xsl:if>
			<xsl:if test="count(Overzicht/*)=0">
				<phrase>WORKSHEET HAS NO DATA count(Overzicht)</phrase>
			</xsl:if>
		</Root>
	</xsl:template>
	<xsl:template name="RENDERTAB">
		<xsl:param name="nodelist"/>
		<xsl:param name="title"/>
		<xsl:if test="count($nodelist)>0"><table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" columns="8" width="100%" align="Left" cellspacing="0">
				<row>
					<cell verticalalign="top" header="true" colspan="8" horizontalalign="left">
						<phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}" fontstyle="bold underline">
							<xsl:value-of select="$title"/>
						</phrase>
						<newline/>
					</cell>
				</row>
				<row>
					<cell horizontalalign="center" >
						<phrase size="{$WorksheetLabelFontsize}" fontstyle="underline">
							<xsl:value-of select="Translations:GetTranslation('DetailRekeningKapitaalOverzicht','Datum','Datum')"/>
						</phrase>
						<newline/>
					</cell>
					<cell horizontalalign="center">
						<phrase size="{$WorksheetLabelFontsize}" fontstyle="underline">
							<xsl:value-of select="Translations:GetTranslation('DetailRekeningKapitaalOverzicht','Omschrijving','Omschrijving')"/>
						</phrase>
					</cell>
					<cell horizontalalign="center">
						<phrase size="{$WorksheetLabelFontsize}" fontstyle="underline">
							<xsl:value-of select="Translations:GetTranslation('DetailRekeningKapitaalOverzicht','InbrengVanGeld','InbrengVanGeld')"/>
						</phrase>
					</cell>
					<cell horizontalalign="center">
						<phrase size="{$WorksheetLabelFontsize}" fontstyle="underline">
							<xsl:value-of select="Translations:GetTranslation('DetailRekeningKapitaalOverzicht','InbrengVanNatura','InbrengVanNatura')"/>
						</phrase>
					</cell>
					<cell horizontalalign="center">
						<phrase size="{$WorksheetLabelFontsize}" fontstyle="underline">
							<xsl:value-of select="Translations:GetTranslation('DetailRekeningKapitaalOverzicht','BelasteReserves','BelasteReserves')"/>
						</phrase>
					</cell>
					<cell horizontalalign="center">
						<phrase size="{$WorksheetLabelFontsize}" fontstyle="underline">
							<xsl:value-of select="Translations:GetTranslation('DetailRekeningKapitaalOverzicht','VrijgesteldeReserves','VrijgesteldeReserves')"/>
						</phrase>
					</cell>
					<cell horizontalalign="center">
						<phrase size="{$WorksheetLabelFontsize}" fontstyle="underline">
							<xsl:value-of select="Translations:GetTranslation('DetailRekeningKapitaalOverzicht','LiquidatieBonus','LiquidatieBonus')"/>
						</phrase>
					</cell>
					<cell horizontalalign="center">
						<phrase size="{$WorksheetLabelFontsize}" fontstyle="underline">
							<xsl:value-of select="Translations:GetTranslation('DetailRekeningKapitaalOverzicht','VvprRegime','VvprRegime')"/>
						</phrase>
					</cell>
				</row>
				<row>
					<cell colspan="8">
						<phrase>&#160;</phrase>
						<newline/>
					</cell>
				</row>
				<xsl:for-each select="$nodelist">
					<row>
						<cell verticalalign="top" horizontalalign="left" colspan="1">
							<phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}">
								<xsl:if test="Datum[text()]">
									<xsl:value-of select="ms:format-date(Datum, 'dd/MM/yyyy')"/>
								</xsl:if>
								<xsl:if test="Seq[text()]">(*<xsl:value-of select="Seq"/>)</xsl:if>
							</phrase>
						</cell>
						<cell verticalalign="top" horizontalalign="left" colspan="1">
							<phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}">
								<xsl:if test="Omschrijving[text()]">
									<xsl:value-of select="Omschrijving"/>
								</xsl:if>
							</phrase>
						</cell>
						<cell verticalalign="top" horizontalalign="right" colspan="1">
							<phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
								<xsl:if test="InbrengVanGeld[text()]">
									<xsl:value-of select="format-number(InbrengVanGeld,$NumberFormatting,'euro')"/>€</xsl:if>
							</phrase>
						</cell>
						<cell verticalalign="top" horizontalalign="right" colspan="1">
							<phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
								<xsl:if test="InbrengVanNatura[text()]">
									<xsl:value-of select="format-number(InbrengVanNatura,$NumberFormatting,'euro')"/>€</xsl:if>
							</phrase>
						</cell>
						<cell verticalalign="top" horizontalalign="right" colspan="1">
							<phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
								<xsl:if test="BelasteReserves[text()]">
									<xsl:value-of select="format-number(BelasteReserves,$NumberFormatting,'euro')"/>€</xsl:if>
							</phrase>
						</cell>
						<cell verticalalign="top" horizontalalign="right" colspan="1">
							<phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
								<xsl:if test="VrijgesteldeReserves[text()]">
									<xsl:value-of select="format-number(VrijgesteldeReserves,$NumberFormatting,'euro')"/>€</xsl:if>
							</phrase>
						</cell>
						<cell verticalalign="top" horizontalalign="right" colspan="1">
							<phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
								<xsl:if test="LiquidatieBonus[text()]">
									<xsl:value-of select="format-number(LiquidatieBonus,$NumberFormatting,'euro')"/>€</xsl:if>
							</phrase>
						</cell>
						<cell verticalalign="top" horizontalalign="right" colspan="1">
							<phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
								<xsl:if test="VvprRegime[text()]">
									<xsl:value-of select="format-number(VvprRegime,$NumberFormatting,'euro')"/>€</xsl:if>
							</phrase>
						</cell>
					</row>
				</xsl:for-each>
				<row>
					<cell colspan="8">
						<phrase>&#160;</phrase>
						<newline/>
					</cell>
				</row>
				<row>
					<cell verticalalign="top" horizontalalign="left" colspan="2">
						<phrase size="{$WorksheetLabelFontsize}" fontstyle="underline">
							<xsl:value-of select="Translations:GetTranslation('DetailRekeningKapitaalOverzicht','Subtotaal','Subtotaal')"/>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" fontstyle="bold">
							<xsl:value-of select="format-number(sum($nodelist/InbrengVanGeld[text()]),$NumberFormatting,'euro')"/>€ </phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" fontstyle="bold">
							<xsl:value-of select="format-number(sum($nodelist/InbrengVanNatura[text()]),$NumberFormatting,'euro')"/>€ </phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" fontstyle="bold">
							<xsl:value-of select="format-number(sum($nodelist/BelasteReserves[text()]),$NumberFormatting,'euro')"/>€ </phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" fontstyle="bold">
							<xsl:value-of select="format-number(sum($nodelist/VrijgesteldeReserves[text()]),$NumberFormatting,'euro')"/>€ </phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" fontstyle="bold">
							<xsl:value-of select="format-number(sum($nodelist/LiquidatieBonus[text()]),$NumberFormatting,'euro')"/>€ </phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" fontstyle="bold">
							<xsl:value-of select="format-number(sum($nodelist/VvprRegime[text()]),$NumberFormatting,'euro')"/>€ </phrase>
					</cell>
				</row>
				<row>
					<cell verticalalign="top" horizontalalign="left" colspan="7">
						<phrase size="{$WorksheetLabelFontsize}" fontstyle="underline">
							<xsl:value-of select="Translations:GetTranslation('DetailRekeningKapitaalOverzicht','Grandtotaal','Grandtotaal')"/>
						</phrase>
					</cell>
					<cell verticalalign="top" horizontalalign="right">
						<phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" fontstyle="bold underline">
							<xsl:value-of select="format-number((sum($nodelist/LiquidatieBonus[text()]) + sum($nodelist/VvprRegime[text()]) + sum($nodelist/InbrengVanGeld[text()]) + sum($nodelist/InbrengVanNatura[text()]) + sum($nodelist/BelasteReserves[text()]) + sum($nodelist/VrijgesteldeReserves[text()])),$NumberFormatting,'euro')"/>€ </phrase>
					</cell>
				</row>
			</table>
			<phrase>
				<newline/>&#160; <newline/>
			</phrase>
		</xsl:if>
	</xsl:template>
	<xsl:template mode="RENDERDETAIL" match="DetailRekeningKapitaalOverzicht">
		<table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" columns="2" width="48%" align="Left" cellspacing="0">
			<xsl:for-each select="Details/OverzichtDetail">
				<xsl:if test="not(preceding-sibling::*[1]) or (StartDate[text()] and not(preceding-sibling::*[1]/StartDate[text()])) or (Assessment[text()] and not(preceding-sibling::*[1]/Assessment[text()]))">
					<row>
						<cell colspan="2" >
							<phrase>&#160;</phrase>
						</cell>
					</row>
					<row>
						<cell verticalalign="top" header="true" colspan="2" horizontalalign="left">
							
							<xsl:if test="StartDate[text()]">
								<phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}" fontstyle="bold"> (*) 
									<xsl:if test="../../Datum">
										<xsl:value-of select="ms:format-date(../../Datum, 'dd/MM/yyyy')"/>
									</xsl:if>
								</phrase>
								<phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}">
									<xsl:value-of select="Translations:GetTranslation('DetailRekeningKapitaalOverzicht','InbrengBR','InbrengBR')"/>
									<newline/>
									<xsl:value-of select="Translations:GetTranslation('DetailRekeningKapitaalOverzicht','InbrengBRopgelet','InbrengBRopgelet')"/>
									<newline/>
								</phrase>
							</xsl:if>
							<xsl:if test="Assessment[text()]">
								<phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}" fontstyle="bold"> (**) 
									<xsl:if test="../../Datum">
										<xsl:value-of select="ms:format-date(../../Datum, 'dd/MM/yyyy')"/>
									</xsl:if>
								</phrase>
								<phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}">  
									<xsl:value-of select="Translations:GetTranslation('DetailRekeningKapitaalOverzicht','InbrengVVPR','InbrengVVPR')"/>
									<newline/>
									<xsl:value-of select="Translations:GetTranslation('DetailRekeningKapitaalOverzicht','InbrengVVPRopgelet','InbrengVVPRopgelet')"/>
								</phrase>
							</xsl:if>
							<newline/>
						</cell>
					</row>
					<xsl:if test="preceding-sibling::*[1]">
					</xsl:if>
					<row>
						<cell horizontalalign="center" left="true" top="true">
							<phrase size="{$WorksheetLabelFontsize}" fontstyle="bold">
								<xsl:value-of select="Translations:GetTranslation('DetailRekeningKapitaalOverzicht','DatumKapitaal','DatumKapitaal')"/>
							</phrase>
							<newline/>
						</cell>
						<cell horizontalalign="center" left="true" top="true" right="true">
							<phrase size="{$WorksheetLabelFontsize}" fontstyle="bold">
								<xsl:value-of select="Translations:GetTranslation('DetailRekeningKapitaalOverzicht','AdditioneleRV','AdditioneleRV')"/>
							</phrase>
							<newline/>
						</cell>
					</row>
				</xsl:if>
				<row>
					<cell horizontalalign="center" colspan="1" left="true" top="true" bottom="true">
						<phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}">
							<xsl:if test="Assessment[text()]">Boekjaar<xsl:value-of select="Assessment"/>
							</xsl:if>
							<xsl:if test="not(Assessment[text()])">
								<xsl:if test="EndDate[text()]">
									<xsl:value-of select="ms:format-date(StartDate, 'dd MMMM yyyy')"/> tot <xsl:value-of select="ms:format-date(EndDate, 'dd MMMM yyyy')"/>
								</xsl:if>
								<xsl:if test="not(EndDate[text()])"> Vanaf <xsl:value-of select="ms:format-date(StartDate, 'dd MMMM yyyy')"/>
								</xsl:if>
							</xsl:if>
						</phrase>
					</cell>
					<cell horizontalalign="center" colspan="1" left="true" top="true" right="true" bottom="true">
						<phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}">
						
							<xsl:value-of select="WithholdingTaxPerc"/>% 
						</phrase>
					</cell>
				</row>
			</xsl:for-each>
			<row>
				<cell colspan="2" top="true">
					<phrase>&#160;</phrase>
				</cell>
			</row>
		</table>
	</xsl:template>
</xsl:stylesheet>