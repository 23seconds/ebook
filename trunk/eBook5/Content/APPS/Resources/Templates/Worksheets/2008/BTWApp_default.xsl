<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" exclude-result-prefixes="Helper Translations File Client" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:Helper="urn:Helper" xmlns:Translations="urn:Translations" xmlns:File="urn:File" xmlns:Client="urn:Client">
  <xsl:decimal-format name="NonNumber" NaN="0" zero-digit="0"/>
  <xsl:variable name="WorksheetLabelFontsize" select="string('10')"/>
  <xsl:variable name="WorksheetFontsize" select="string('10')"/>
  <xsl:variable name="WorksheetNumbersize" select="string('8')"/>
  <xsl:variable name="WorksheetSmallFontsize" select="string('8')"/>
  <xsl:variable name="WorksheetTotalsFontsize" select="string('10')"/>
  <xsl:variable name="WorksheetSpacing" select="1.1"/>
  <xsl:decimal-format name="euro" decimal-separator="," grouping-separator="."/>
  <xsl:variable name="NumberFormatting" select="string('#.##0,00')"/>
  <xsl:variable name="NumberFormattingNoDigit" select="string('#.##0')"/>
  <xsl:variable name="FONT" select="string('C:\WINDOWS\Fonts\EYInterstate-Light.ttf')"/>
  <xsl:param name="RuleAppDef"/>
  <xsl:param name="Culture"/>
  <xsl:template match="WPBTW">
    <Root orientation="landscape">
      <xsl:apply-templates select="$RuleAppDef//RuleApplicationDef" mode="WORKSHEET">
        <xsl:with-param name="results" select="."/>
      </xsl:apply-templates>
    </Root>
  </xsl:template>
  <xsl:template match="RuleApplicationDef[@Name='BTWApp']" mode="WORKSHEET">
    <xsl:param name="results"/>
    <xsl:variable name="WorkSheetType" select="@Name"/>
    <xsl:variable name="EntityDefinitionName" select="Entities/EntityDef[@Name='WPBTW']/@Name"/>
    <xsl:variable name="DataElements" select="DataElements"/>
    <xsl:if test="count($results//OmzetAangiftes) > 0 or count($results//OmzettenGeboekt) > 0">
      <xsl:apply-templates select="Entities/EntityDef[@Name='WPBTW']/Fields/FieldDef[@DataType='Entity' and @Name='OmzetAangiftes']" mode="BTWApp_FieldDef">
        <xsl:with-param name="results" select="$results"/>
      </xsl:apply-templates>
      <xsl:apply-templates select="Entities/EntityDef[@Name='WPBTW']/Fields/FieldDef[@DataType='Entity' and @Name='OmzettenGeboekt']" mode="BTWApp_FieldDef">
        <xsl:with-param name="results" select="$results"/>
      </xsl:apply-templates>
      <xsl:apply-templates select="Entities/EntityDef[@Name='WPBTW']/Fields/FieldDef[@DataType='Entity' and @Name='OmzetVerklaringen']" mode="BTWApp_FieldDef">
        <xsl:with-param name="results" select="$results"/>
      </xsl:apply-templates>
    </xsl:if>
    <xsl:if test="count($results//RCAangiftes) > 0 or count($results//RCenGeboekt) > 0">
      <newpage/>
      <xsl:apply-templates select="Entities/EntityDef[@Name='WPBTW']/Fields/FieldDef[@DataType='Entity' and @Name='RCAangiftes']" mode="BTWApp_FieldDef">
        <xsl:with-param name="results" select="$results"/>
      </xsl:apply-templates>
      <xsl:apply-templates select="Entities/EntityDef[@Name='WPBTW']/Fields/FieldDef[@DataType='Entity' and @Name='RCenGeboekt']" mode="BTWApp_FieldDef">
        <xsl:with-param name="results" select="$results"/>
      </xsl:apply-templates>
      <xsl:apply-templates select="Entities/EntityDef[@Name='WPBTW']/Fields/FieldDef[@DataType='Entity' and @Name='RCVerklaringen']" mode="BTWApp_FieldDef">
        <xsl:with-param name="results" select="$results"/>
      </xsl:apply-templates>
    </xsl:if>
  </xsl:template>
  <xsl:template match="FieldDef" mode="BTWApp_FieldDef">
    <xsl:param name="results"/>
    <xsl:variable name="EntityName" select="@Name"/>
    <xsl:variable name="entName" select="@DataTypeEntityName"/>
    <xsl:variable name="EntityDefinition" select="//Entities/EntityDef[@Name=$entName]"/>
    <xsl:variable name="omzetverschil">
      <xsl:call-template name="abs">
        <xsl:with-param name="n" select="(sum($results//node()[name()='OmzetAangiftes']/node()[name()='Totaal']))-(sum($results//node()[name()='OmzettenGeboekt']/node()[name()='Saldo']))"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="rcverschil" select="$results//RCVerklaringen/Bedrag[../AutoKey = 'TVV' and ../order/text()='0']"/>
    <xsl:if test="$EntityName = 'OmzetAangiftes' and not(not($results//OmzetAangiftes))">
      <xsl:variable name="tarief0" select="sum($results//node()[name()=$EntityName]/node()[name()='Tarief_0'])"/>
      <xsl:variable name="tarief6" select="sum($results//node()[name()=$EntityName]/node()[name()='Tarief_6'])"/>
      <xsl:variable name="tarief12" select="sum($results//node()[name()=$EntityName]/node()[name()='Tarief_12'])"/>
      <xsl:variable name="tarief21" select="sum($results//node()[name()=$EntityName]/node()[name()='Tarief_21'])"/>
      <xsl:variable name="tariefmedec" select="sum($results//node()[name()=$EntityName]/node()[name()='Tarief_Medec'])"/>
      <xsl:variable name="tariefeu" select="sum($results//node()[name()=$EntityName]/node()[name()='Tarief_EU'])"/>
      <xsl:variable name="tariefuitvoer" select="sum($results//node()[name()=$EntityName]/node()[name()='Tarief_Uitvoer'])"/>
      <xsl:variable name="tarief44" select="sum($results//node()[name()=$EntityName]/node()[name()='Tarief_44'])"/>
      <xsl:variable name="tarief48" select="sum($results//node()[name()=$EntityName]/node()[name()='Tarief_48'])"/>
      <xsl:variable name="tarief49" select="sum($results//node()[name()=$EntityName]/node()[name()='Tarief_49'])"/>
      <xsl:if test="not($tarief0=0)">
        <xsl:variable name="DUMMYCOUNT" select="Helper:AddSum('omzetcolumnCounter',1)"/>
      </xsl:if>
      <xsl:if test="not($tarief6=0)">
        <xsl:variable name="DUMMYCOUNT" select="Helper:AddSum('omzetcolumnCounter',1)"/>
      </xsl:if>
      <xsl:if test="not($tarief12=0)">
        <xsl:variable name="DUMMYCOUNT" select="Helper:AddSum('omzetcolumnCounter',1)"/>
      </xsl:if>
      <xsl:if test="not($tarief21=0)">
        <xsl:variable name="DUMMYCOUNT" select="Helper:AddSum('omzetcolumnCounter',1)"/>
      </xsl:if>
      <xsl:if test="not($tariefmedec=0)">
        <xsl:variable name="DUMMYCOUNT" select="Helper:AddSum('omzetcolumnCounter',1)"/>
      </xsl:if>
      <xsl:if test="not($tariefeu=0)">
        <xsl:variable name="DUMMYCOUNT" select="Helper:AddSum('omzetcolumnCounter',1)"/>
      </xsl:if>
      <xsl:if test="not($tariefuitvoer=0)">
        <xsl:variable name="DUMMYCOUNT" select="Helper:AddSum('omzetcolumnCounter',1)"/>
      </xsl:if>
      <xsl:if test="not($tarief44=0)">
        <xsl:variable name="DUMMYCOUNT" select="Helper:AddSum('omzetcolumnCounter',1)"/>
      </xsl:if>
      <xsl:if test="not($tarief48=0)">
        <xsl:variable name="DUMMYCOUNT" select="Helper:AddSum('omzetcolumnCounter',1)"/>
      </xsl:if>
      <xsl:if test="not($tarief49=0)">
        <xsl:variable name="DUMMYCOUNT" select="Helper:AddSum('omzetcolumnCounter',1)"/>
      </xsl:if>
      <xsl:variable name="filledColumns" select="Helper:AddSum('omzetcolumnCounter',0)"/>
      <xsl:variable name="Columns" select="($filledColumns+2)*2-1"/>
      <xsl:variable name="ColumnSizes" select="Helper:GetColumnSizes(100,$Columns)"/>
      <xsl:if test="$filledColumns &lt;= 3">
        <!--non-variable columns -->
        <table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" columns="10" width="100%" align="Center" widths="50;50;5;50;5;50;5;50;5;50;" cellspacing="0" cellpadding="2">
          <row>
            <cell verticalalign="middle" colspan="10" header="true" border="1">
              <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}" fontstyle="bold">
                <xsl:value-of select="Translations:GetTranslation('WPBTW','AansluitingOmzet','AansluitingOmzet')"/>
              </phrase>
            </cell>
          </row>
          <row>
            <cell verticalalign="middle" colspan="10" header="true" border="1">
              <newline/>
              <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}" fontstyle="bold">
                <xsl:value-of select="Translations:GetTranslation('WPBTW',string($EntityName),string($EntityName))"/>
              </phrase>
            </cell>
          </row>
          <row>
            <xsl:choose>
              <xsl:when test="$filledColumns = 3">
                <cell></cell>
              </xsl:when>
              <xsl:when test="$filledColumns = 2">
                <cell></cell>
                <cell></cell>
                <cell></cell>
              </xsl:when>
              <xsl:when test="$filledColumns = 1">
                <cell></cell>
                <cell></cell>
                <cell></cell>
                <cell></cell>
                <cell></cell>
              </xsl:when>
            </xsl:choose>
            <cell verticalalign="middle" horizontalalign="center" header="true" border="1">
              <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}" fontstyle="bold">
                <xsl:value-of select="Translations:GetTranslation($entName,'Y_M','Y_M')"/>
              </phrase>
            </cell>
            <xsl:if test="not($tarief0=0)">
              <cell></cell>
              <cell verticalalign="middle" horizontalalign="right" header="true">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}" fontstyle="bold">
                  <xsl:value-of select="Translations:GetTranslation($entName,'Tarief_0','Tarief_0')"/>
                </phrase>
              </cell>
            </xsl:if>
            <xsl:if test="not($tarief6=0)">
              <cell></cell>
              <cell verticalalign="middle" horizontalalign="right" header="true">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}" fontstyle="bold">
                  <xsl:value-of select="Translations:GetTranslation($entName,'Tarief_6','Tarief_6')"/>
                </phrase>
              </cell>
            </xsl:if>
            <xsl:if test="not($tarief12=0)">
              <cell>12</cell>
              <cell verticalalign="middle" horizontalalign="right" header="true">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}" fontstyle="bold">
                  <xsl:value-of select="Translations:GetTranslation($entName,'Tarief_12','Tarief_12')"/>
                </phrase>
              </cell>
            </xsl:if>
            <xsl:if test="not($tarief21=0)">
              <cell></cell>
              <cell verticalalign="middle" horizontalalign="right" header="true">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}" fontstyle="bold">
                  <xsl:value-of select="Translations:GetTranslation($entName,'Tarief_21','Tarief_21')"/>
                </phrase>
              </cell>
            </xsl:if>
            <xsl:if test="not($tarief44=0)">
              <cell></cell>
              <cell verticalalign="middle" horizontalalign="right" header="true">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}" fontstyle="bold">
                  <xsl:value-of select="Translations:GetTranslation($entName,'Tarief_44','Tarief_44')"/>
                </phrase>
              </cell>
            </xsl:if>
            <xsl:if test="not($tariefmedec=0)">
              <cell></cell>
              <cell verticalalign="middle" horizontalalign="right" header="true">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}" fontstyle="bold">
                  <xsl:value-of select="Translations:GetTranslation($entName,'Tarief_Medec','Tarief_Medec')"/>
                </phrase>
              </cell>
            </xsl:if>
            <xsl:if test="not($tariefeu=0)">
              <cell></cell>
              <cell verticalalign="middle" horizontalalign="right" header="true">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}" fontstyle="bold">
                  <xsl:value-of select="Translations:GetTranslation($entName,'Tarief_EU','Tarief_EU')"/>
                </phrase>
              </cell>
            </xsl:if>
            <xsl:if test="not($tariefuitvoer=0)">
              <cell></cell>
              <cell verticalalign="middle" horizontalalign="right" header="true">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}" fontstyle="bold">
                  <xsl:value-of select="Translations:GetTranslation($entName,'Tarief_Uitvoer','Tarief_Uitvoer')"/>
                </phrase>
              </cell>
            </xsl:if>
            <xsl:if test="not($tarief48=0)">
              <cell></cell>
              <cell verticalalign="middle" horizontalalign="right" header="true">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}" fontstyle="bold">
                  <xsl:value-of select="Translations:GetTranslation($entName,'Tarief_48','Tarief_48')"/>
                </phrase>
              </cell>
            </xsl:if>
            <xsl:if test="not($tarief49=0)">
              <cell></cell>
              <cell verticalalign="middle" horizontalalign="right" header="true">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}" fontstyle="bold">
                  <xsl:value-of select="Translations:GetTranslation($entName,'Tarief_49','Tarief_49')"/>
                </phrase>
              </cell>
            </xsl:if>
            <cell></cell>
            <cell verticalalign="middle" horizontalalign="right" header="true">
              <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}" fontstyle="bold">
                <xsl:value-of select="Translations:GetTranslation($entName,'Totaal','Totaal')"/>
              </phrase>
            </cell>
          </row>
          <xsl:for-each select="$results//node()[name()=$EntityName and not(number(Totaal)=0)]">
            <xsl:sort select="Y_M" order="ascending"/>
            <row>
              <xsl:choose>
                <xsl:when test="$filledColumns = 3">
                  <cell></cell>
                </xsl:when>
                <xsl:when test="$filledColumns = 2">
                  <cell></cell>
                  <cell></cell>
                  <cell></cell>
                </xsl:when>
                <xsl:when test="$filledColumns = 1">
                  <cell></cell>
                  <cell></cell>
                  <cell></cell>
                  <cell></cell>
                  <cell></cell>
                </xsl:when>
              </xsl:choose>
              <cell verticalalign="middle" horizontalalign="center">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
                  <xsl:value-of select="./node()[name()='Y_M']"/>
                </phrase>
              </cell>
              <xsl:if test="not($tarief0=0)">
                <cell></cell>
                <cell verticalalign="middle" horizontalalign="right">
                  <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
                    <xsl:value-of select="format-number(./node()[name()='Tarief_0'],$NumberFormatting,'euro')"/>€
                  </phrase>
                </cell>
              </xsl:if>
              <xsl:if test="not($tarief6=0)">
                <cell></cell>
                <cell verticalalign="middle" horizontalalign="right">
                  <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
                    <xsl:value-of select="format-number(./node()[name()='Tarief_6'],$NumberFormatting,'euro')"/>€
                  </phrase>
                </cell>
              </xsl:if>
              <xsl:if test="not($tarief12=0)">
                <cell></cell>
                <cell verticalalign="middle" horizontalalign="right">
                  <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
                    <xsl:value-of select="format-number(./node()[name()='Tarief_12'],$NumberFormatting,'euro')"/>€
                  </phrase>
                </cell>
              </xsl:if>
              <xsl:if test="not($tarief21=0)">
                <cell></cell>
                <cell verticalalign="middle" horizontalalign="right">
                  <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
                    <xsl:value-of select="format-number(./node()[name()='Tarief_21'],$NumberFormatting,'euro')"/>€
                  </phrase>
                </cell>
              </xsl:if>
              <xsl:if test="not($tarief44=0)">
                <cell></cell>
                <cell verticalalign="middle" horizontalalign="right">
                  <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
                    <xsl:value-of select="format-number(./node()[name()='Tarief_44'],$NumberFormatting,'euro')"/>€
                  </phrase>
                </cell>
              </xsl:if>
              <xsl:if test="not($tariefmedec=0)">
                <cell></cell>
                <cell verticalalign="middle" horizontalalign="right">
                  <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
                    <xsl:value-of select="format-number(./node()[name()='Tarief_Medec'],$NumberFormatting,'euro')"/>€
                  </phrase>
                </cell>
              </xsl:if>
              <xsl:if test="not($tariefeu=0)">
                <cell></cell>
                <cell verticalalign="middle" horizontalalign="right">
                  <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
                    <xsl:value-of select="format-number(./node()[name()='Tarief_EU'],$NumberFormatting,'euro')"/>€
                  </phrase>
                </cell>
              </xsl:if>
              <xsl:if test="not($tariefuitvoer=0)">
                <cell></cell>
                <cell verticalalign="middle" horizontalalign="right">
                  <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
                    <xsl:value-of select="format-number(./node()[name()='Tarief_Uitvoer'],$NumberFormatting,'euro')"/>€
                  </phrase>
                </cell>
              </xsl:if>
              <xsl:if test="not($tarief48=0)">
                <cell></cell>
                <cell verticalalign="middle" horizontalalign="right">
                  <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
                    <xsl:value-of select="format-number(./node()[name()='Tarief_48'],$NumberFormatting,'euro')"/>€
                  </phrase>
                </cell>
              </xsl:if>
              <xsl:if test="not($tarief49=0)">
                <cell border="1"></cell>
                <cell verticalalign="middle" horizontalalign="right">
                  <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
                    <xsl:value-of select="format-number(./node()[name()='Tarief_49'],$NumberFormatting,'euro')"/>€
                  </phrase>
                </cell>
              </xsl:if>
              <cell></cell>
              <cell verticalalign="middle" horizontalalign="right">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
                  <xsl:value-of select="format-number(./node()[name()='Totaal'],$NumberFormatting,'euro')"/>€
                </phrase>
              </cell>
            </row>
          </xsl:for-each>
          <row>
            <cell colspan="10">
              <phrase lineheight="0.5">&#160;</phrase>
            </cell>
          </row>
          <row>
            <xsl:choose>
              <xsl:when test="$filledColumns = 3">
                <cell></cell>
              </xsl:when>
              <xsl:when test="$filledColumns = 2">
                <cell></cell>
                <cell></cell>
                <cell></cell>
              </xsl:when>
              <xsl:when test="$filledColumns = 1">
                <cell></cell>
                <cell></cell>
                <cell></cell>
                <cell></cell>
                <cell></cell>
              </xsl:when>
            </xsl:choose>
            <cell>
              <phrase lineheight="{$WorksheetSpacing}"/>
            </cell>
            <xsl:if test="not($tarief0=0)">
              <cell></cell>
              <cell verticalalign="middle" horizontalalign="right" borderwidth="0.5" top="true" bordercolor="black">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" fontstyle="bold">
                  <xsl:value-of select="format-number(sum($results//node()[name()=$EntityName]/node()[name()='Tarief_0']),$NumberFormatting,'euro')"/>€
                </phrase>
              </cell>
            </xsl:if>
            <xsl:if test="not($tarief6=0)">
              <cell></cell>
              <cell verticalalign="middle" horizontalalign="right" borderwidth="0.5" top="true" bordercolor="black">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" fontstyle="bold">
                  <xsl:value-of select="format-number(sum($results//node()[name()=$EntityName]/node()[name()='Tarief_6']),$NumberFormatting,'euro')"/>€
                </phrase>
              </cell>
            </xsl:if>
            <xsl:if test="not($tarief12=0)">
              <cell></cell>
              <cell verticalalign="middle" horizontalalign="right" borderwidth="0.5" top="true" bordercolor="black">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" fontstyle="bold">
                  <xsl:value-of select="format-number(sum($results//node()[name()=$EntityName]/node()[name()='Tarief_12']),$NumberFormatting,'euro')"/>€
                </phrase>
              </cell>
            </xsl:if>
            <xsl:if test="not($tarief21=0)">
              <cell></cell>
              <cell verticalalign="middle" horizontalalign="right" borderwidth="0.5" top="true" bordercolor="black">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" fontstyle="bold">
                  <xsl:value-of select="format-number(sum($results//node()[name()=$EntityName]/node()[name()='Tarief_21']),$NumberFormatting,'euro')"/>€
                </phrase>
              </cell>
            </xsl:if>
            <xsl:if test="not($tarief44=0)">
              <cell></cell>
              <cell verticalalign="middle" horizontalalign="right" borderwidth="0.5" top="true" bordercolor="black">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" fontstyle="bold">
                  <xsl:value-of select="format-number(sum($results//node()[name()=$EntityName]/node()[name()='Tarief_44']),$NumberFormatting,'euro')"/>€
                </phrase>
              </cell>
            </xsl:if>
            <xsl:if test="not($tariefmedec=0)">
              <cell></cell>
              <cell verticalalign="middle" horizontalalign="right" borderwidth="0.5" top="true" bordercolor="black">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" fontstyle="bold">
                  <xsl:value-of select="format-number(sum($results//node()[name()=$EntityName]/node()[name()='Tarief_Medec']),$NumberFormatting,'euro')"/>€
                </phrase>
              </cell>
            </xsl:if>
            <xsl:if test="not($tariefeu=0)">
              <cell></cell>
              <cell verticalalign="middle" horizontalalign="right" borderwidth="0.5" top="true" bordercolor="black">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" fontstyle="bold">
                  <xsl:value-of select="format-number(sum($results//node()[name()=$EntityName]/node()[name()='Tarief_EU']),$NumberFormatting,'euro')"/>€
                </phrase>
              </cell>
            </xsl:if>
            <xsl:if test="not($tariefuitvoer=0)">
              <cell></cell>
              <cell verticalalign="middle" horizontalalign="right" borderwidth="0.5" top="true" bordercolor="black">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" fontstyle="bold">
                  <xsl:value-of select="format-number(sum($results//node()[name()=$EntityName]/node()[name()='Tarief_Uitvoer']),$NumberFormatting,'euro')"/>€
                </phrase>
              </cell>
            </xsl:if>
            <xsl:if test="not($tarief48=0)">
              <cell></cell>
              <cell verticalalign="middle" horizontalalign="right" borderwidth="0.5" top="true" bordercolor="black">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" fontstyle="bold">
                  <xsl:value-of select="format-number(sum($results//node()[name()=$EntityName]/node()[name()='Tarief_48']),$NumberFormatting,'euro')"/>€
                </phrase>
              </cell>
            </xsl:if>
            <xsl:if test="not($tarief49=0)">
              <cell></cell>
              <cell verticalalign="middle" horizontalalign="right" borderwidth="0.5" top="true" bordercolor="black">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" fontstyle="bold">
                  <xsl:value-of select="format-number(sum($results//node()[name()=$EntityName]/node()[name()='Tarief_49']),$NumberFormatting,'euro')"/>€
                </phrase>
              </cell>
            </xsl:if>
            <cell></cell>
            <cell verticalalign="middle" horizontalalign="right" borderwidth="0.5" top="true" bordercolor="black">
              <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" fontstyle="bold">
                <xsl:value-of select="format-number(sum($results//node()[name()=$EntityName]/node()[name()='Totaal']),$NumberFormatting,'euro')"/>€
              </phrase>
            </cell>
          </row>
        </table>
      </xsl:if>
      <xsl:if test="$filledColumns &lt;= 6 and $filledColumns &gt; 3">
        <!--non-variable columns -->
        <table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" columns="15" width="100%" align="Center" widths="35;5;35;5;35;5;35;5;35;5;35;5;35;5;35" cellspacing="0" cellpadding="2">
          <row>
            <cell verticalalign="middle" colspan="15" header="true" border="1">
              <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}" fontstyle="bold">
                <xsl:value-of select="Translations:GetTranslation('WPBTW','AansluitingOmzet','AansluitingOmzet')"/>
              </phrase>
            </cell>
          </row>
          <row>
            <cell verticalalign="middle" colspan="15" header="true" border="1">
              <newline/>
              <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}" fontstyle="bold">
                <xsl:value-of select="Translations:GetTranslation('WPBTW',string($EntityName),string($EntityName))"/>
              </phrase>
            </cell>
          </row>
          <row>
            <xsl:choose>
              <xsl:when test="$filledColumns = 5">
                <cell></cell>
                <cell></cell>
              </xsl:when>
              <xsl:when test="$filledColumns = 4">
                <cell></cell>
                <cell></cell>
                <cell></cell>
                <cell></cell>
              </xsl:when>
            </xsl:choose>
            <cell verticalalign="middle" horizontalalign="center" header="true" border="1">
              <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" fontstyle="bold">
                <xsl:value-of select="Translations:GetTranslation($entName,'Y_M','Y_M')"/>
              </phrase>
            </cell>
            <xsl:if test="not($tarief0=0)">
              <cell></cell>
              <cell verticalalign="middle" horizontalalign="right" header="true">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" fontstyle="bold">
                  <xsl:value-of select="Translations:GetTranslation($entName,'Tarief_0','Tarief_0')"/>
                </phrase>
              </cell>
            </xsl:if>
            <xsl:if test="not($tarief6=0)">
              <cell></cell>
              <cell verticalalign="middle" horizontalalign="right" header="true">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" fontstyle="bold">
                  <xsl:value-of select="Translations:GetTranslation($entName,'Tarief_6','Tarief_6')"/>
                </phrase>
              </cell>
            </xsl:if>
            <xsl:if test="not($tarief12=0)">
              <cell></cell>
              <cell verticalalign="middle" horizontalalign="right" header="true">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" fontstyle="bold">
                  <xsl:value-of select="Translations:GetTranslation($entName,'Tarief_12','Tarief_12')"/>
                </phrase>
              </cell>
            </xsl:if>
            <xsl:if test="not($tarief21=0)">
              <cell></cell>
              <cell verticalalign="middle" horizontalalign="right" header="true">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" fontstyle="bold">
                  <xsl:value-of select="Translations:GetTranslation($entName,'Tarief_21','Tarief_21')"/>
                </phrase>
              </cell>
            </xsl:if>
            <xsl:if test="not($tarief44=0)">
              <cell></cell>
              <cell verticalalign="middle" horizontalalign="right" header="true">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" fontstyle="bold">
                  <xsl:value-of select="Translations:GetTranslation($entName,'Tarief_44','Tarief_44')"/>
                </phrase>
              </cell>
            </xsl:if>
            <xsl:if test="not($tariefmedec=0)">
              <cell></cell>
              <cell verticalalign="middle" horizontalalign="right" header="true">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" fontstyle="bold">
                  <xsl:value-of select="Translations:GetTranslation($entName,'Tarief_Medec','Tarief_Medec')"/>
                </phrase>
              </cell>
            </xsl:if>
            <xsl:if test="not($tariefeu=0)">
              <cell></cell>
              <cell verticalalign="middle" horizontalalign="right" header="true">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" fontstyle="bold">
                  <xsl:value-of select="Translations:GetTranslation($entName,'Tarief_EU','Tarief_EU')"/>
                </phrase>
              </cell>
            </xsl:if>
            <xsl:if test="not($tariefuitvoer=0)">
              <cell></cell>
              <cell verticalalign="middle" horizontalalign="right" header="true">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" fontstyle="bold">
                  <xsl:value-of select="Translations:GetTranslation($entName,'Tarief_Uitvoer','Tarief_Uitvoer')"/>
                </phrase>
              </cell>
            </xsl:if>
            <xsl:if test="not($tarief48=0)">
              <cell></cell>
              <cell verticalalign="middle" horizontalalign="right" header="true">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" fontstyle="bold">
                  <xsl:value-of select="Translations:GetTranslation($entName,'Tarief_48','Tarief_48')"/>
                </phrase>
              </cell>
            </xsl:if>
            <xsl:if test="not($tarief49=0)">
              <cell></cell>
              <cell verticalalign="middle" horizontalalign="right" header="true">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" fontstyle="bold">
                  <xsl:value-of select="Translations:GetTranslation($entName,'Tarief_49','Tarief_49')"/>
                </phrase>
              </cell>
            </xsl:if>
            <cell></cell>
            <cell verticalalign="middle" horizontalalign="right" header="true">
              <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" fontstyle="bold">
                <xsl:value-of select="Translations:GetTranslation($entName,'Totaal','Totaal')"/>
              </phrase>
            </cell>
          </row>
          <xsl:for-each select="$results//node()[name()=$EntityName]">
            <xsl:sort select="Y_M" order="ascending"/>
            <row>
              <xsl:choose>
                <xsl:when test="$filledColumns = 5">
                  <cell></cell>
                  <cell></cell>
                </xsl:when>
                <xsl:when test="$filledColumns = 4">
                  <cell></cell>
                  <cell></cell>
                  <cell></cell>
                  <cell></cell>
                </xsl:when>
              </xsl:choose>
              <cell verticalalign="middle" horizontalalign="center">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
                  <xsl:value-of select="./node()[name()='Y_M']"/>
                </phrase>
              </cell>
              <xsl:if test="not($tarief0=0)">
                <cell></cell>
                <cell verticalalign="middle" horizontalalign="right">
                  <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
                    <xsl:value-of select="format-number(./node()[name()='Tarief_0'],$NumberFormatting,'euro')"/>€
                  </phrase>
                </cell>
              </xsl:if>
              <xsl:if test="not($tarief6=0)">
                <cell></cell>
                <cell verticalalign="middle" horizontalalign="right">
                  <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
                    <xsl:value-of select="format-number(./node()[name()='Tarief_6'],$NumberFormatting,'euro')"/>€
                  </phrase>
                </cell>
              </xsl:if>
              <xsl:if test="not($tarief12=0)">
                <cell></cell>
                <cell verticalalign="middle" horizontalalign="right">
                  <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
                    <xsl:value-of select="format-number(./node()[name()='Tarief_12'],$NumberFormatting,'euro')"/>€
                  </phrase>
                </cell>
              </xsl:if>
              <xsl:if test="not($tarief21=0)">
                <cell></cell>
                <cell verticalalign="middle" horizontalalign="right">
                  <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
                    <xsl:value-of select="format-number(./node()[name()='Tarief_21'],$NumberFormatting,'euro')"/>€
                  </phrase>
                </cell>
              </xsl:if>
              <xsl:if test="not($tarief44=0)">
                <cell></cell>
                <cell verticalalign="middle" horizontalalign="right">
                  <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
                    <xsl:value-of select="format-number(./node()[name()='Tarief_44'],$NumberFormatting,'euro')"/>€
                  </phrase>
                </cell>
              </xsl:if>
              <xsl:if test="not($tariefmedec=0)">
                <cell></cell>
                <cell verticalalign="middle" horizontalalign="right">
                  <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
                    <xsl:value-of select="format-number(./node()[name()='Tarief_Medec'],$NumberFormatting,'euro')"/>€
                  </phrase>
                </cell>
              </xsl:if>
              <xsl:if test="not($tariefeu=0)">
                <cell></cell>
                <cell verticalalign="middle" horizontalalign="right">
                  <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
                    <xsl:value-of select="format-number(./node()[name()='Tarief_EU'],$NumberFormatting,'euro')"/>€
                  </phrase>
                </cell>
              </xsl:if>
              <xsl:if test="not($tariefuitvoer=0)">
                <cell></cell>
                <cell verticalalign="middle" horizontalalign="right">
                  <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
                    <xsl:value-of select="format-number(./node()[name()='Tarief_Uitvoer'],$NumberFormatting,'euro')"/>€
                  </phrase>
                </cell>
              </xsl:if>
              <xsl:if test="not($tarief48=0)">
                <cell></cell>
                <cell verticalalign="middle" horizontalalign="right">
                  <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
                    <xsl:value-of select="format-number(./node()[name()='Tarief_48'],$NumberFormatting,'euro')"/>€
                  </phrase>
                </cell>
              </xsl:if>
              <xsl:if test="not($tarief49=0)">
                <cell border="1"></cell>
                <cell verticalalign="middle" horizontalalign="right">
                  <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
                    <xsl:value-of select="format-number(./node()[name()='Tarief_49'],$NumberFormatting,'euro')"/>€
                  </phrase>
                </cell>
              </xsl:if>
              <cell></cell>
              <cell verticalalign="middle" horizontalalign="right">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
                  <xsl:value-of select="format-number(./node()[name()='Totaal'],$NumberFormatting,'euro')"/>€
                </phrase>
              </cell>
            </row>
          </xsl:for-each>
          <row>
            <cell colspan="15">
              <phrase lineheight="0.5">&#160;</phrase>
            </cell>
          </row>
          <row>
            <xsl:choose>
              <xsl:when test="$filledColumns = 5">
                <cell></cell>
                <cell></cell>
              </xsl:when>
              <xsl:when test="$filledColumns = 4">
                <cell></cell>
                <cell></cell>
                <cell></cell>
                <cell></cell>
              </xsl:when>
            </xsl:choose>
            <cell>
              <phrase lineheight="{$WorksheetSpacing}"/>
            </cell>
            <xsl:if test="not($tarief0=0)">
              <cell></cell>
              <cell verticalalign="middle" horizontalalign="right" borderwidth="0.5" top="true" bordercolor="black">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" fontstyle="bold">
                  <xsl:value-of select="format-number(sum($results//node()[name()=$EntityName]/node()[name()='Tarief_0']),$NumberFormatting,'euro')"/>€
                </phrase>
              </cell>
            </xsl:if>
            <xsl:if test="not($tarief6=0)">
              <cell></cell>
              <cell verticalalign="middle" horizontalalign="right" borderwidth="0.5" top="true" bordercolor="black">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" fontstyle="bold">
                  <xsl:value-of select="format-number(sum($results//node()[name()=$EntityName]/node()[name()='Tarief_6']),$NumberFormatting,'euro')"/>€
                </phrase>
              </cell>
            </xsl:if>
            <xsl:if test="not($tarief12=0)">
              <cell></cell>
              <cell verticalalign="middle" horizontalalign="right" borderwidth="0.5" top="true" bordercolor="black">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" fontstyle="bold">
                  <xsl:value-of select="format-number(sum($results//node()[name()=$EntityName]/node()[name()='Tarief_12']),$NumberFormatting,'euro')"/>€
                </phrase>
              </cell>
            </xsl:if>
            <xsl:if test="not($tarief21=0)">
              <cell></cell>
              <cell verticalalign="middle" horizontalalign="right" borderwidth="0.5" top="true" bordercolor="black">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" fontstyle="bold">
                  <xsl:value-of select="format-number(sum($results//node()[name()=$EntityName]/node()[name()='Tarief_21']),$NumberFormatting,'euro')"/>€
                </phrase>
              </cell>
            </xsl:if>
            <xsl:if test="not($tarief44=0)">
              <cell></cell>
              <cell verticalalign="middle" horizontalalign="right" borderwidth="0.5" top="true" bordercolor="black">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" fontstyle="bold">
                  <xsl:value-of select="format-number(sum($results//node()[name()=$EntityName]/node()[name()='Tarief_44']),$NumberFormatting,'euro')"/>€
                </phrase>
              </cell>
            </xsl:if>
            <xsl:if test="not($tariefmedec=0)">
              <cell></cell>
              <cell verticalalign="middle" horizontalalign="right" borderwidth="0.5" top="true" bordercolor="black">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" fontstyle="bold">
                  <xsl:value-of select="format-number(sum($results//node()[name()=$EntityName]/node()[name()='Tarief_Medec']),$NumberFormatting,'euro')"/>€
                </phrase>
              </cell>
            </xsl:if>
            <xsl:if test="not($tariefeu=0)">
              <cell></cell>
              <cell verticalalign="middle" horizontalalign="right" borderwidth="0.5" top="true" bordercolor="black">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" fontstyle="bold">
                  <xsl:value-of select="format-number(sum($results//node()[name()=$EntityName]/node()[name()='Tarief_EU']),$NumberFormatting,'euro')"/>€
                </phrase>
              </cell>
            </xsl:if>
            <xsl:if test="not($tariefuitvoer=0)">
              <cell></cell>
              <cell verticalalign="middle" horizontalalign="right" borderwidth="0.5" top="true" bordercolor="black">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" fontstyle="bold">
                  <xsl:value-of select="format-number(sum($results//node()[name()=$EntityName]/node()[name()='Tarief_Uitvoer']),$NumberFormatting,'euro')"/>€
                </phrase>
              </cell>
            </xsl:if>
            <xsl:if test="not($tarief48=0)">
              <cell></cell>
              <cell verticalalign="middle" horizontalalign="right" borderwidth="0.5" top="true" bordercolor="black">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" fontstyle="bold">
                  <xsl:value-of select="format-number(sum($results//node()[name()=$EntityName]/node()[name()='Tarief_48']),$NumberFormatting,'euro')"/>€
                </phrase>
              </cell>
            </xsl:if>
            <xsl:if test="not($tarief49=0)">
              <cell></cell>
              <cell verticalalign="middle" horizontalalign="right" borderwidth="0.5" top="true" bordercolor="black">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" fontstyle="bold">
                  <xsl:value-of select="format-number(sum($results//node()[name()=$EntityName]/node()[name()='Tarief_49']),$NumberFormatting,'euro')"/>€
                </phrase>
              </cell>
            </xsl:if>
            <cell></cell>
            <cell verticalalign="middle" horizontalalign="right" borderwidth="0.5" top="true" bordercolor="black">
              <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" fontstyle="bold">
                <xsl:value-of select="format-number(sum($results//node()[name()=$EntityName]/node()[name()='Totaal']),$NumberFormatting,'euro')"/>€
              </phrase>
            </cell>
          </row>
        </table>
      </xsl:if>
      <xsl:if test="$filledColumns &gt; 6">
        <!-- calculate the columns so that they fit on the page / zoom the fontsize-->
        <xsl:variable name="fontSize" select="Helper:GetFontSize($filledColumns)"/>
        <table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" columns="{$Columns}" width="100%" align="Center" widths="{$ColumnSizes}" cellspacing="0" cellpadding="1">
          <row>
            <cell verticalalign="middle" colspan="{$Columns}" header="true" border="1">
              <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}" fontstyle="bold">
                <xsl:value-of select="Translations:GetTranslation('WPBTW','AansluitingOmzet','AansluitingOmzet')"/>
              </phrase>
            </cell>
          </row>
          <row>
            <cell verticalalign="middle" colspan="{$Columns}" header="true" border="1">
              <newline/>
              <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}" fontstyle="bold">
                <xsl:value-of select="Translations:GetTranslation('WPBTW',string($EntityName),string($EntityName))"/>
              </phrase>
            </cell>
          </row>
          <row>
            <cell verticalalign="middle" horizontalalign="center" header="true" border="1">
              <phrase lineheight="{$WorksheetSpacing}" size="{$fontSize}" fontstyle="bold">
                <xsl:value-of select="Translations:GetTranslation($entName,'Y_M','Y_M')"/>
              </phrase>
            </cell>
            <xsl:if test="not($tarief0=0)">
              <cell></cell>
              <cell verticalalign="middle" horizontalalign="right" header="true">
                <phrase lineheight="{$WorksheetSpacing}" size="{$fontSize}" fontstyle="bold">
                  <xsl:value-of select="Translations:GetTranslation($entName,'Tarief_0','Tarief_0')"/>
                </phrase>
              </cell>
            </xsl:if>
            <xsl:if test="not($tarief6=0)">
              <cell></cell>
              <cell verticalalign="middle" horizontalalign="right" header="true">
                <phrase lineheight="{$WorksheetSpacing}" size="{$fontSize}" fontstyle="bold">
                  <xsl:value-of select="Translations:GetTranslation($entName,'Tarief_6','Tarief_6')"/>
                </phrase>
              </cell>
            </xsl:if>
            <xsl:if test="not($tarief12=0)">
              <cell></cell>
              <cell verticalalign="middle" horizontalalign="right" header="true">
                <phrase lineheight="{$WorksheetSpacing}" size="{$fontSize}" fontstyle="bold">
                  <xsl:value-of select="Translations:GetTranslation($entName,'Tarief_12','Tarief_12')"/>
                </phrase>
              </cell>
            </xsl:if>
            <xsl:if test="not($tarief21=0)">
              <cell></cell>
              <cell verticalalign="middle" horizontalalign="right" header="true">
                <phrase lineheight="{$WorksheetSpacing}" size="{$fontSize}" fontstyle="bold">
                  <xsl:value-of select="Translations:GetTranslation($entName,'Tarief_21','Tarief_21')"/>
                </phrase>
              </cell>
            </xsl:if>
            <xsl:if test="not($tarief44=0)">
              <cell></cell>
              <cell verticalalign="middle" horizontalalign="right" header="true">
                <phrase lineheight="{$WorksheetSpacing}" size="{$fontSize}" fontstyle="bold">
                  <xsl:value-of select="Translations:GetTranslation($entName,'Tarief_44','Tarief_44')"/>
                </phrase>
              </cell>
            </xsl:if>
            <xsl:if test="not($tariefmedec=0)">
              <cell></cell>
              <cell verticalalign="middle" horizontalalign="right" header="true">
                <phrase lineheight="{$WorksheetSpacing}" size="{$fontSize}" fontstyle="bold">
                  <xsl:value-of select="Translations:GetTranslation($entName,'Tarief_Medec','Tarief_Medec')"/>
                </phrase>
              </cell>
            </xsl:if>
            <xsl:if test="not($tariefeu=0)">
              <cell></cell>
              <cell verticalalign="middle" horizontalalign="right" header="true">
                <phrase lineheight="{$WorksheetSpacing}" size="{$fontSize}" fontstyle="bold">
                  <xsl:value-of select="Translations:GetTranslation($entName,'Tarief_EU','Tarief_EU')"/>
                </phrase>
              </cell>
            </xsl:if>
            <xsl:if test="not($tariefuitvoer=0)">
              <cell></cell>
              <cell verticalalign="middle" horizontalalign="right" header="true">
                <phrase lineheight="{$WorksheetSpacing}" size="{$fontSize}" fontstyle="bold">
                  <xsl:value-of select="Translations:GetTranslation($entName,'Tarief_Uitvoer','Tarief_Uitvoer')"/>
                </phrase>
              </cell>
            </xsl:if>
            <xsl:if test="not($tarief48=0)">
              <cell></cell>
              <cell verticalalign="middle" horizontalalign="right" header="true">
                <phrase lineheight="{$WorksheetSpacing}" size="{$fontSize}" fontstyle="bold">
                  <xsl:value-of select="Translations:GetTranslation($entName,'Tarief_48','Tarief_48')"/>
                </phrase>
              </cell>
            </xsl:if>
            <xsl:if test="not($tarief49=0)">
              <cell></cell>
              <cell verticalalign="middle" horizontalalign="right" header="true">
                <phrase lineheight="{$WorksheetSpacing}" size="{$fontSize}" fontstyle="bold">
                  <xsl:value-of select="Translations:GetTranslation($entName,'Tarief_49','Tarief_49')"/>
                </phrase>
              </cell>
            </xsl:if>
            <cell></cell>
            <cell verticalalign="middle" horizontalalign="right" header="true">
              <phrase lineheight="{$WorksheetSpacing}" size="{$fontSize}" fontstyle="bold">
                <xsl:value-of select="Translations:GetTranslation($entName,'Totaal','Totaal')"/>
              </phrase>
            </cell>
          </row>
          <xsl:for-each select="$results//node()[name()=$EntityName]">
            <xsl:sort select="Y_M" order="ascending"/>
            <row>
              <cell verticalalign="middle" horizontalalign="center">
                <phrase lineheight="{$WorksheetSpacing}" size="{$fontSize}">
                  <xsl:value-of select="./node()[name()='Y_M']"/>
                </phrase>
              </cell>
              <xsl:if test="not($tarief0=0)">
                <cell></cell>
                <cell verticalalign="middle" horizontalalign="right">
                  <phrase lineheight="{$WorksheetSpacing}" size="{$fontSize}">
                    <xsl:value-of select="format-number(./node()[name()='Tarief_0'],$NumberFormatting,'euro')"/>€
                  </phrase>
                </cell>
              </xsl:if>
              <xsl:if test="not($tarief6=0)">
                <cell></cell>
                <cell verticalalign="middle" horizontalalign="right">
                  <phrase lineheight="{$WorksheetSpacing}" size="{$fontSize}">
                    <xsl:value-of select="format-number(./node()[name()='Tarief_6'],$NumberFormatting,'euro')"/>€
                  </phrase>
                </cell>
              </xsl:if>
              <xsl:if test="not($tarief12=0)">
                <cell></cell>
                <cell verticalalign="middle" horizontalalign="right">
                  <phrase lineheight="{$WorksheetSpacing}" size="{$fontSize}">
                    <xsl:value-of select="format-number(./node()[name()='Tarief_12'],$NumberFormatting,'euro')"/>€
                  </phrase>
                </cell>
              </xsl:if>
              <xsl:if test="not($tarief21=0)">
                <cell></cell>
                <cell verticalalign="middle" horizontalalign="right">
                  <phrase lineheight="{$WorksheetSpacing}" size="{$fontSize}">
                    <xsl:value-of select="format-number(./node()[name()='Tarief_21'],$NumberFormatting,'euro')"/>€
                  </phrase>
                </cell>
              </xsl:if>
              <xsl:if test="not($tarief44=0)">
                <cell></cell>
                <cell verticalalign="middle" horizontalalign="right">
                  <phrase lineheight="{$WorksheetSpacing}" size="{$fontSize}">
                    <xsl:value-of select="format-number(./node()[name()='Tarief_44'],$NumberFormatting,'euro')"/>€
                  </phrase>
                </cell>
              </xsl:if>
              <xsl:if test="not($tariefmedec=0)">
                <cell></cell>
                <cell verticalalign="middle" horizontalalign="right">
                  <phrase lineheight="{$WorksheetSpacing}" size="{$fontSize}">
                    <xsl:value-of select="format-number(./node()[name()='Tarief_Medec'],$NumberFormatting,'euro')"/>€
                  </phrase>
                </cell>
              </xsl:if>
              <xsl:if test="not($tariefeu=0)">
                <cell></cell>
                <cell verticalalign="middle" horizontalalign="right">
                  <phrase lineheight="{$WorksheetSpacing}" size="{$fontSize}">
                    <xsl:value-of select="format-number(./node()[name()='Tarief_EU'],$NumberFormatting,'euro')"/>€
                  </phrase>
                </cell>
              </xsl:if>
              <xsl:if test="not($tariefuitvoer=0)">
                <cell></cell>
                <cell verticalalign="middle" horizontalalign="right">
                  <phrase lineheight="{$WorksheetSpacing}" size="{$fontSize}">
                    <xsl:value-of select="format-number(./node()[name()='Tarief_Uitvoer'],$NumberFormatting,'euro')"/>€
                  </phrase>
                </cell>
              </xsl:if>
              <xsl:if test="not($tarief48=0)">
                <cell></cell>
                <cell verticalalign="middle" horizontalalign="right">
                  <phrase lineheight="{$WorksheetSpacing}" size="{$fontSize}">
                    <xsl:value-of select="format-number(./node()[name()='Tarief_48'],$NumberFormatting,'euro')"/>€
                  </phrase>
                </cell>
              </xsl:if>
              <xsl:if test="not($tarief49=0)">
                <cell border="1"></cell>
                <cell verticalalign="middle" horizontalalign="right">
                  <phrase lineheight="{$WorksheetSpacing}" size="{$fontSize}">
                    <xsl:value-of select="format-number(./node()[name()='Tarief_49'],$NumberFormatting,'euro')"/>€
                  </phrase>
                </cell>
              </xsl:if>
              <cell></cell>
              <cell verticalalign="middle" horizontalalign="right">
                <phrase lineheight="{$WorksheetSpacing}" size="{$fontSize}">
                  <xsl:value-of select="format-number(./node()[name()='Totaal'],$NumberFormatting,'euro')"/>€
                </phrase>
              </cell>
            </row>
          </xsl:for-each>
          <row>
            <cell colspan="{$Columns}">
              <phrase lineheight="0.5">&#160;</phrase>
            </cell>
          </row>
          <row>
            <cell>
              <phrase lineheight="{$WorksheetSpacing}"/>
            </cell>
            <xsl:if test="not($tarief0=0)">
              <cell></cell>
              <cell verticalalign="middle" horizontalalign="right" borderwidth="0.5" top="true" bordercolor="black">
                <phrase lineheight="{$WorksheetSpacing}" size="{$fontSize}" fontstyle="bold">
                  <xsl:value-of select="format-number(sum($results//node()[name()=$EntityName]/node()[name()='Tarief_0']),$NumberFormatting,'euro')"/>€
                </phrase>
              </cell>
            </xsl:if>
            <xsl:if test="not($tarief6=0)">
              <cell></cell>
              <cell verticalalign="middle" horizontalalign="right" borderwidth="0.5" top="true" bordercolor="black">
                <phrase lineheight="{$WorksheetSpacing}" size="{$fontSize}" fontstyle="bold">
                  <xsl:value-of select="format-number(sum($results//node()[name()=$EntityName]/node()[name()='Tarief_6']),$NumberFormatting,'euro')"/>€
                </phrase>
              </cell>
            </xsl:if>
            <xsl:if test="not($tarief21=0)">
              <cell></cell>
              <cell verticalalign="middle" horizontalalign="right" borderwidth="0.5" top="true" bordercolor="black">
                <phrase lineheight="{$WorksheetSpacing}" size="{$fontSize}" fontstyle="bold">
                  <xsl:value-of select="format-number(sum($results//node()[name()=$EntityName]/node()[name()='Tarief_21']),$NumberFormatting,'euro')"/>€
                </phrase>
              </cell>
            </xsl:if>
            <xsl:if test="not($tarief44=0)">
              <cell></cell>
              <cell verticalalign="middle" horizontalalign="right" borderwidth="0.5" top="true" bordercolor="black">
                <phrase lineheight="{$WorksheetSpacing}" size="{$fontSize}" fontstyle="bold">
                  <xsl:value-of select="format-number(sum($results//node()[name()=$EntityName]/node()[name()='Tarief_44']),$NumberFormatting,'euro')"/>€
                </phrase>
              </cell>
            </xsl:if>
            <xsl:if test="not($tariefmedec=0)">
              <cell></cell>
              <cell verticalalign="middle" horizontalalign="right" borderwidth="0.5" top="true" bordercolor="black">
                <phrase lineheight="{$WorksheetSpacing}" size="{$fontSize}" fontstyle="bold">
                  <xsl:value-of select="format-number(sum($results//node()[name()=$EntityName]/node()[name()='Tarief_Medec']),$NumberFormatting,'euro')"/>€
                </phrase>
              </cell>
            </xsl:if>
            <xsl:if test="not($tariefeu=0)">
              <cell></cell>
              <cell verticalalign="middle" horizontalalign="right" borderwidth="0.5" top="true" bordercolor="black">
                <phrase lineheight="{$WorksheetSpacing}" size="{$fontSize}" fontstyle="bold">
                  <xsl:value-of select="format-number(sum($results//node()[name()=$EntityName]/node()[name()='Tarief_EU']),$NumberFormatting,'euro')"/>€
                </phrase>
              </cell>
            </xsl:if>
            <xsl:if test="not($tariefuitvoer=0)">
              <cell></cell>
              <cell verticalalign="middle" horizontalalign="right" borderwidth="0.5" top="true" bordercolor="black">
                <phrase lineheight="{$WorksheetSpacing}" size="{$fontSize}" fontstyle="bold">
                  <xsl:value-of select="format-number(sum($results//node()[name()=$EntityName]/node()[name()='Tarief_Uitvoer']),$NumberFormatting,'euro')"/>€
                </phrase>
              </cell>
            </xsl:if>
            <xsl:if test="not($tarief48=0)">
              <cell></cell>
              <cell verticalalign="middle" horizontalalign="right" borderwidth="0.5" top="true" bordercolor="black">
                <phrase lineheight="{$WorksheetSpacing}" size="{$fontSize}" fontstyle="bold">
                  <xsl:value-of select="format-number(sum($results//node()[name()=$EntityName]/node()[name()='Tarief_48']),$NumberFormatting,'euro')"/>€
                </phrase>
              </cell>
            </xsl:if>
            <xsl:if test="not($tarief49=0)">
              <cell></cell>
              <cell verticalalign="middle" horizontalalign="right" borderwidth="0.5" top="true" bordercolor="black">
                <phrase lineheight="{$WorksheetSpacing}" size="{$fontSize}" fontstyle="bold">
                  <xsl:value-of select="format-number(sum($results//node()[name()=$EntityName]/node()[name()='Tarief_49']),$NumberFormatting,'euro')"/>€
                </phrase>
              </cell>
            </xsl:if>
            <cell></cell>
            <cell verticalalign="middle" horizontalalign="right" borderwidth="0.5" top="true" bordercolor="black">
              <phrase lineheight="{$WorksheetSpacing}" size="{$fontSize}" fontstyle="bold">
                <xsl:value-of select="format-number(sum($results//node()[name()=$EntityName]/node()[name()='Totaal']),$NumberFormatting,'euro')"/>€
              </phrase>
            </cell>
          </row>
        </table>
      </xsl:if>
    </xsl:if>
    <xsl:if test="$EntityName = 'OmzetAangiftes' and not($results//OmzetAangiftes) and count($results//OmzettenGeboekt) > 0">
      <table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" columns="3" width="100%" align="Center" widths="60;15;15" cellspacing="0" cellpadding="1">
        <row>
          <cell verticalalign="middle" colspan="2" header="true">
            <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}" fontstyle="bold">
              <xsl:value-of select="Translations:GetTranslation('WPBTW',string($EntityName),string($EntityName))"/>
            </phrase>
          </cell>
          <cell verticalalign="middle" header="true" horizontalalign="right">
            <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" fontstyle="bold">
              <xsl:value-of select="format-number(0,$NumberFormatting,'euro')"/>€
            </phrase>
          </cell>
        </row>
      </table>
    </xsl:if>
    <xsl:if test="$EntityName = 'RCAangiftes' and not($results//RCAangiftes)and count($results//RCenGeboekt) > 0">
      <table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" columns="3" width="100%" align="Center" widths="60;15;15" cellspacing="0" cellpadding="1">
        <row>
          <cell verticalalign="middle" colspan="2" header="true">
            <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}" fontstyle="bold">
              <xsl:value-of select="Translations:GetTranslation('WPBTW',string($EntityName),string($EntityName))"/>
            </phrase>
          </cell>
          <cell verticalalign="middle" header="true" horizontalalign="right">
            <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" fontstyle="bold">
              <xsl:value-of select="format-number(0,$NumberFormatting,'euro')"/>€
            </phrase>
          </cell>
        </row>
      </table>
    </xsl:if>
    <xsl:if test="$EntityName = 'RCAangiftes' and not(not($results//RCAangiftes))">
      <newline/>
      <xsl:variable name="endmonth" select="File:GetValue('EndDate','MM/yyyy')"/>
      <xsl:variable name="endmonth2" select="File:GetValue('EndDate','yyyy/MM')"/>
      <xsl:variable name="Post54" select="sum($results//node()[name()=$EntityName]/node()[name()='Post54'])"/>
      <xsl:variable name="Post55" select="sum($results//node()[name()=$EntityName]/node()[name()='Post55'])"/>
      <xsl:variable name="Post56" select="sum($results//node()[name()=$EntityName]/node()[name()='Post56'])"/>
      <xsl:variable name="Post57" select="sum($results//node()[name()=$EntityName]/node()[name()='Post57'])"/>
      <xsl:variable name="Post59" select="sum($results//node()[name()=$EntityName]/node()[name()='Post59'])"/>
      <xsl:variable name="Post61" select="sum($results//node()[name()=$EntityName]/node()[name()='Post61'])"/>
      <xsl:variable name="Post62" select="sum($results//node()[name()=$EntityName]/node()[name()='Post62'])"/>
      <xsl:variable name="Post63" select="sum($results//node()[name()=$EntityName]/node()[name()='Post63'])"/>
      <xsl:variable name="Post64" select="sum($results//node()[name()=$EntityName]/node()[name()='Post64'])"/>
      <xsl:if test="not($Post54=0)">
        <xsl:variable name="DUMMYCOUNT" select="Helper:AddSum('RCaangiftesColumnCount',1)"/>
      </xsl:if>
      <xsl:if test="not($Post55=0)">
        <xsl:variable name="DUMMYCOUNT" select="Helper:AddSum('RCaangiftesColumnCount',1)"/>
      </xsl:if>
      <xsl:if test="not($Post56=0)">
        <xsl:variable name="DUMMYCOUNT" select="Helper:AddSum('RCaangiftesColumnCount',1)"/>
      </xsl:if>
      <xsl:if test="not($Post57=0)">
        <xsl:variable name="DUMMYCOUNT" select="Helper:AddSum('RCaangiftesColumnCount',1)"/>
      </xsl:if>
      <xsl:if test="not($Post59=0)">
        <xsl:variable name="DUMMYCOUNT" select="Helper:AddSum('RCaangiftesColumnCount',1)"/>
      </xsl:if>
      <xsl:if test="not($Post61=0)">
        <xsl:variable name="DUMMYCOUNT" select="Helper:AddSum('RCaangiftesColumnCount',1)"/>
      </xsl:if>
      <xsl:if test="not($Post62=0)">
        <xsl:variable name="DUMMYCOUNT" select="Helper:AddSum('RCaangiftesColumnCount',1)"/>
      </xsl:if>
      <xsl:if test="not($Post63=0)">
        <xsl:variable name="DUMMYCOUNT" select="Helper:AddSum('RCaangiftesColumnCount',1)"/>
      </xsl:if>
      <xsl:if test="not($Post64=0)">
        <xsl:variable name="DUMMYCOUNT" select="Helper:AddSum('RCaangiftesColumnCount',1)"/>
      </xsl:if>
      <xsl:variable name="filledColumns" select="Helper:AddSum('RCaangiftesColumnCount',0)"/>
      <xsl:variable name="Columns" select="($filledColumns+2)*2-1"/>
      <xsl:variable name="ColumnSizes" select="Helper:GetColumnSizes(100,$Columns)"/>
      <xsl:if test="$filledColumns &lt;= 6">
        <table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" columns="15" width="100%" align="Center" widths="35;5;35;5;35;5;35;5;35;5;35;5;35;5;35" cellspacing="0" cellpadding="0">
          <row>
            <cell verticalalign="middle" colspan="15" header="true">
              <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}" fontstyle="bold">
                <xsl:value-of select="Translations:GetTranslation('WPBTW',string($EntityName),string($EntityName))"/>
              </phrase>
            </cell>
          </row>
          <row>
            <xsl:choose>
              <xsl:when test="$filledColumns = 5">
                <cell></cell>
                <cell></cell>
              </xsl:when>
              <xsl:when test="$filledColumns = 4">
                <cell></cell>
                <cell></cell>
                <cell></cell>
                <cell></cell>
              </xsl:when>
              <xsl:when test="$filledColumns = 3">
                <cell></cell>
                <cell></cell>
                <cell></cell>
                <cell></cell>
                <cell></cell>
                <cell></cell>
              </xsl:when>
              <xsl:when test="$filledColumns = 2">
                <cell></cell>
                <cell></cell>
                <cell></cell>
                <cell></cell>
                <cell></cell>
                <cell></cell>
                <cell></cell>
                <cell></cell>
              </xsl:when>
              <xsl:when test="$filledColumns = 1">
                <cell></cell>
                <cell></cell>
                <cell></cell>
                <cell></cell>
                <cell></cell>
                <cell></cell>
                <cell></cell>
                <cell></cell>
                <cell></cell>
                <cell></cell>
              </xsl:when>
            </xsl:choose>
            <cell verticalalign="middle" horizontalalign="center" header="true">
              <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}">
                <xsl:value-of select="Translations:GetTranslation($entName,'Y_M','Y_M')"/>
              </phrase>
            </cell>
            <xsl:if test="not($Post54=0)">
              <cell></cell>
              <cell verticalalign="middle" horizontalalign="right" header="true">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}">
                  <xsl:value-of select="Translations:GetTranslation($entName,'Post54','Post54')"/>
                </phrase>
              </cell>
            </xsl:if>
            <xsl:if test="not($Post55=0)">
              <cell></cell>
              <cell verticalalign="middle" horizontalalign="right" header="true">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}">
                  <xsl:value-of select="Translations:GetTranslation($entName,'Post55','Post55')"/>
                </phrase>
              </cell>
            </xsl:if>
            <xsl:if test="not($Post56=0)">
              <cell></cell>
              <cell verticalalign="middle" horizontalalign="right" header="true">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}">
                  <xsl:value-of select="Translations:GetTranslation($entName,'Post56','Post56')"/>
                </phrase>
              </cell>
            </xsl:if>
            <xsl:if test="not($Post57=0)">
              <cell></cell>
              <cell verticalalign="middle" horizontalalign="right" header="true">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}">
                  <xsl:value-of select="Translations:GetTranslation($entName,'Post57','Post57')"/>
                </phrase>
              </cell>
            </xsl:if>
            <xsl:if test="not($Post59=0)">
              <cell></cell>
              <cell verticalalign="middle" horizontalalign="right" header="true">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}">
                  <xsl:value-of select="Translations:GetTranslation($entName,'Post59','Post59')"/>
                </phrase>
              </cell>
            </xsl:if>
            <xsl:if test="not($Post61=0)">
              <cell></cell>
              <cell verticalalign="middle" horizontalalign="right" header="true">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}">
                  <xsl:value-of select="Translations:GetTranslation($entName,'Post61','Post61')"/>
                </phrase>
              </cell>
            </xsl:if>
            <xsl:if test="not($Post62=0)">
              <cell></cell>
              <cell verticalalign="middle" horizontalalign="right" header="true">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}">
                  <xsl:value-of select="Translations:GetTranslation($entName,'Post62','Post62')"/>
                </phrase>
              </cell>
            </xsl:if>
            <xsl:if test="not($Post63=0)">
              <cell></cell>
              <cell verticalalign="middle" horizontalalign="right" header="true">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}">
                  <xsl:value-of select="Translations:GetTranslation($entName,'Post63','Post63')"/>
                </phrase>
              </cell>
            </xsl:if>
            <xsl:if test="not($Post64=0)">
              <cell></cell>
              <cell verticalalign="middle" horizontalalign="right" header="true">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}">
                  <xsl:value-of select="Translations:GetTranslation($entName,'Post64','Post64')"/>
                </phrase>
              </cell>
            </xsl:if>
            <cell></cell>
            <cell verticalalign="middle" horizontalalign="right" header="true">
              <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}" style="bold">
                <xsl:value-of select="Translations:GetTranslation($entName,'Totaal','Totaal')"/>
              </phrase>
            </cell>
          </row>
          <row>
            <cell colspan="15">
              <phrase lineheight="1.5">&#160;</phrase>
            </cell>
          </row>
          <xsl:for-each select="$results//RCAangiftes[Y_M/text()=$endmonth or Y_M/text()=$endmonth2]">
            <row>
              <xsl:choose>
                <xsl:when test="$filledColumns = 5">
                  <cell></cell>
                  <cell></cell>
                </xsl:when>
                <xsl:when test="$filledColumns = 4">
                  <cell></cell>
                  <cell></cell>
                  <cell></cell>
                  <cell></cell>
                </xsl:when>
                <xsl:when test="$filledColumns = 3">
                  <cell></cell>
                  <cell></cell>
                  <cell></cell>
                  <cell></cell>
                  <cell></cell>
                  <cell></cell>
                </xsl:when>
                <xsl:when test="$filledColumns = 2">
                  <cell></cell>
                  <cell></cell>
                  <cell></cell>
                  <cell></cell>
                  <cell></cell>
                  <cell></cell>
                  <cell></cell>
                  <cell></cell>
                </xsl:when>
                <xsl:when test="$filledColumns = 1">
                  <cell></cell>
                  <cell></cell>
                  <cell></cell>
                  <cell></cell>
                  <cell></cell>
                  <cell></cell>
                  <cell></cell>
                  <cell></cell>
                  <cell></cell>
                  <cell></cell>
                </xsl:when>
              </xsl:choose>
              <cell verticalalign="middle" horizontalalign="center">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
                  <xsl:value-of select="./node()[name()='Y_M']"/>
                </phrase>
              </cell>
              <xsl:if test="not($Post54=0)">
                <cell></cell>
                <cell verticalalign="middle" horizontalalign="right">
                  <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
                    <xsl:value-of select="format-number(./node()[name()='Post54'],$NumberFormatting,'euro')"/>€
                  </phrase>
                </cell>
              </xsl:if>
              <xsl:if test="not($Post55=0)">
                <cell></cell>
                <cell verticalalign="middle" horizontalalign="right">
                  <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
                    <xsl:value-of select="format-number(./node()[name()='Post55'],$NumberFormatting,'euro')"/>€
                  </phrase>
                </cell>
              </xsl:if>
              <xsl:if test="not($Post56=0)">
                <cell></cell>
                <cell verticalalign="middle" horizontalalign="right">
                  <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
                    <xsl:value-of select="format-number(./node()[name()='Post56'],$NumberFormatting,'euro')"/>€
                  </phrase>
                </cell>
              </xsl:if>
              <xsl:if test="not($Post57=0)">
                <cell></cell>
                <cell verticalalign="middle" horizontalalign="right">
                  <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
                    <xsl:value-of select="format-number(./node()[name()='Post57'],$NumberFormatting,'euro')"/>€
                  </phrase>
                </cell>
              </xsl:if>
              <xsl:if test="not($Post59=0)">
                <cell></cell>
                <cell verticalalign="middle" horizontalalign="right">
                  <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
                    <xsl:value-of select="format-number(./node()[name()='Post59'],$NumberFormatting,'euro')"/>€
                  </phrase>
                </cell>
              </xsl:if>
              <xsl:if test="not($Post61=0)">
                <cell></cell>
                <cell verticalalign="middle" horizontalalign="right">
                  <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
                    <xsl:value-of select="format-number(./node()[name()='Post61'],$NumberFormatting,'euro')"/>€
                  </phrase>
                </cell>
              </xsl:if>
              <xsl:if test="not($Post62=0)">
                <cell></cell>
                <cell verticalalign="middle" horizontalalign="right">
                  <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
                    <xsl:value-of select="format-number(./node()[name()='Post62'],$NumberFormatting,'euro')"/>€
                  </phrase>
                </cell>
              </xsl:if>
              <xsl:if test="not($Post63=0)">
                <cell></cell>
                <cell verticalalign="middle" horizontalalign="right">
                  <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
                    <xsl:value-of select="format-number(./node()[name()='Post63'],$NumberFormatting,'euro')"/>€
                  </phrase>
                </cell>
              </xsl:if>
              <xsl:if test="not($Post64=0)">
                <cell></cell>
                <cell verticalalign="middle" horizontalalign="right">
                  <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
                    <xsl:value-of select="format-number(./node()[name()='Post64'],$NumberFormatting,'euro')"/>€
                  </phrase>
                </cell>
              </xsl:if>
              <cell></cell>
              <cell verticalalign="middle" horizontalalign="right">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" style="bold">
                  <xsl:value-of select="format-number(./node()[name()='Totaal'],$NumberFormatting,'euro')"/>€
                </phrase>
              </cell>
            </row>
          </xsl:for-each>
        </table>
      </xsl:if>
      <xsl:if test="$filledColumns &gt; 6">
        <xsl:variable name="fontSize" select="Helper:GetFontSize($filledColumns)"/>
        <table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" columns="{$Columns}" width="100%" align="Center" widths="{$ColumnSizes}" cellspacing="0" cellpadding="1">
          <row>
            <cell verticalalign="middle" colspan="{$Columns}" header="true">
              <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}" fontstyle="bold">
                <xsl:value-of select="Translations:GetTranslation('WPBTW',string($EntityName),string($EntityName))"/>
              </phrase>
            </cell>
          </row>
          <row>
            <cell verticalalign="middle" horizontalalign="center" header="true">
              <phrase lineheight="{$WorksheetSpacing}" size="{$fontSize}" fontstyle="bold">
                <xsl:value-of select="Translations:GetTranslation($entName,'Y_M','Y_M')"/>
              </phrase>
            </cell>
            <xsl:if test="not($Post54=0)">
              <cell></cell>
              <cell verticalalign="middle" horizontalalign="right" header="true">
                <phrase lineheight="{$WorksheetSpacing}" size="{$fontSize}" fontstyle="bold">
                  <xsl:value-of select="Translations:GetTranslation($entName,'Post54','Post54')"/>
                </phrase>
              </cell>
            </xsl:if>
            <xsl:if test="not($Post55=0)">
              <cell></cell>
              <cell verticalalign="middle" horizontalalign="right" header="true">
                <phrase lineheight="{$WorksheetSpacing}" size="{$fontSize}" fontstyle="bold">
                  <xsl:value-of select="Translations:GetTranslation($entName,'Post55','Post55')"/>
                </phrase>
              </cell>
            </xsl:if>
            <xsl:if test="not($Post56=0)">
              <cell></cell>
              <cell verticalalign="middle" horizontalalign="right" header="true">
                <phrase lineheight="{$WorksheetSpacing}" size="{$fontSize}" fontstyle="bold">
                  <xsl:value-of select="Translations:GetTranslation($entName,'Post56','Post56')"/>
                </phrase>
              </cell>
            </xsl:if>
            <xsl:if test="not($Post57=0)">
              <cell></cell>
              <cell verticalalign="middle" horizontalalign="right" header="true">
                <phrase lineheight="{$WorksheetSpacing}" size="{$fontSize}" fontstyle="bold">
                  <xsl:value-of select="Translations:GetTranslation($entName,'Post57','Post57')"/>
                </phrase>
              </cell>
            </xsl:if>
            <xsl:if test="not($Post59=0)">
              <cell></cell>
              <cell verticalalign="middle" horizontalalign="right" header="true">
                <phrase lineheight="{$WorksheetSpacing}" size="{$fontSize}" fontstyle="bold">
                  <xsl:value-of select="Translations:GetTranslation($entName,'Post59','Post59')"/>
                </phrase>
              </cell>
            </xsl:if>
            <xsl:if test="not($Post61=0)">
              <cell></cell>
              <cell verticalalign="middle" horizontalalign="right" header="true">
                <phrase lineheight="{$WorksheetSpacing}" size="{$fontSize}" fontstyle="bold">
                  <xsl:value-of select="Translations:GetTranslation($entName,'Post61','Post61')"/>
                </phrase>
              </cell>
            </xsl:if>
            <xsl:if test="not($Post62=0)">
              <cell></cell>
              <cell verticalalign="middle" horizontalalign="right" header="true">
                <phrase lineheight="{$WorksheetSpacing}" size="{$fontSize}" fontstyle="bold">
                  <xsl:value-of select="Translations:GetTranslation($entName,'Post62','Post62')"/>
                </phrase>
              </cell>
            </xsl:if>
            <xsl:if test="not($Post63=0)">
              <cell></cell>
              <cell verticalalign="middle" horizontalalign="right" header="true">
                <phrase lineheight="{$WorksheetSpacing}" size="{$fontSize}" fontstyle="bold">
                  <xsl:value-of select="Translations:GetTranslation($entName,'Post63','Post63')"/>
                </phrase>
              </cell>
            </xsl:if>
            <xsl:if test="not($Post64=0)">
              <cell></cell>
              <cell verticalalign="middle" horizontalalign="right" header="true">
                <phrase lineheight="{$WorksheetSpacing}" size="{$fontSize}" fontstyle="bold">
                  <xsl:value-of select="Translations:GetTranslation($entName,'Post64','Post64')"/>
                </phrase>
              </cell>
            </xsl:if>
            <cell></cell>
            <cell verticalalign="middle" horizontalalign="right" header="true">
              <phrase lineheight="{$WorksheetSpacing}" size="{$fontSize}" fontstyle="bold">
                <xsl:value-of select="Translations:GetTranslation($entName,'Totaal','Totaal')"/>
              </phrase>
            </cell>
          </row>
          <row>
            <cell colspan="{$Columns}">
              <phrase lineheight="1.5">&#160;</phrase>
            </cell>
          </row>
          <xsl:for-each select="$results//RCAangiftes[Y_M/text()=$endmonth or Y_M/text()=$endmonth2]">
            <row>
              <cell verticalalign="middle" horizontalalign="center">
                <phrase lineheight="{$WorksheetSpacing}" size="{$fontSize}">
                  <xsl:value-of select="./node()[name()='Y_M']"/>
                </phrase>
              </cell>
              <xsl:if test="not($Post54=0)">
                <cell></cell>
                <cell verticalalign="middle" horizontalalign="right">
                  <phrase lineheight="{$WorksheetSpacing}" size="{$fontSize}">
                    <xsl:value-of select="format-number(Helper:CheckNaN(./node()[name()='Post54']),$NumberFormatting,'euro')"/>€
                  </phrase>
                </cell>
              </xsl:if>
              <xsl:if test="not($Post55=0)">
                <cell></cell>
                <cell verticalalign="middle" horizontalalign="right">
                  <phrase lineheight="{$WorksheetSpacing}" size="{$fontSize}">
                    <xsl:value-of select="format-number(Helper:CheckNaN(./node()[name()='Post55']),$NumberFormatting,'euro')"/>€
                  </phrase>
                </cell>
              </xsl:if>
              <xsl:if test="not($Post56=0)">
                <cell></cell>
                <cell verticalalign="middle" horizontalalign="right">
                  <phrase lineheight="{$WorksheetSpacing}" size="{$fontSize}">
                    <xsl:value-of select="format-number(Helper:CheckNaN(./node()[name()='Post56']),$NumberFormatting,'euro')"/>€
                  </phrase>
                </cell>
              </xsl:if>
              <xsl:if test="not($Post57=0)">
                <cell></cell>
                <cell verticalalign="middle" horizontalalign="right">
                  <phrase lineheight="{$WorksheetSpacing}" size="{$fontSize}">
                    <xsl:value-of select="format-number(Helper:CheckNaN(./node()[name()='Post57']),$NumberFormatting,'euro')"/>€
                  </phrase>
                </cell>
              </xsl:if>
              <xsl:if test="not($Post59=0)">
                <cell></cell>
                <cell verticalalign="middle" horizontalalign="right">
                  <phrase lineheight="{$WorksheetSpacing}" size="{$fontSize}">
                    <xsl:value-of select="format-number(Helper:CheckNaN(./node()[name()='Post59']),$NumberFormatting,'euro')"/>€
                  </phrase>
                </cell>
              </xsl:if>
              <xsl:if test="not($Post61=0)">
                <cell></cell>
                <cell verticalalign="middle" horizontalalign="right">
                  <phrase lineheight="{$WorksheetSpacing}" size="{$fontSize}">
                    <xsl:value-of select="format-number(Helper:CheckNaN(./node()[name()='Post61']),$NumberFormatting,'euro')"/>€
                  </phrase>
                </cell>
              </xsl:if>
              <xsl:if test="not($Post62=0)">
                <cell></cell>
                <cell verticalalign="middle" horizontalalign="right">
                  <phrase lineheight="{$WorksheetSpacing}" size="{$fontSize}">
                    <xsl:value-of select="format-number(Helper:CheckNaN(./node()[name()='Post62']),$NumberFormatting,'euro')"/>€
                  </phrase>
                </cell>
              </xsl:if>
              <xsl:if test="not($Post63=0)">
                <cell></cell>
                <cell verticalalign="middle" horizontalalign="right">
                  <phrase lineheight="{$WorksheetSpacing}" size="{$fontSize}">
                    <xsl:value-of select="format-number(Helper:CheckNaN(./node()[name()='Post63']),$NumberFormatting,'euro')"/>€
                  </phrase>
                </cell>
              </xsl:if>
              <xsl:if test="not($Post64=0)">
                <cell></cell>
                <cell verticalalign="middle" horizontalalign="right">
                  <phrase lineheight="{$WorksheetSpacing}" size="{$fontSize}">
                    <xsl:value-of select="format-number(Helper:CheckNaN(./node()[name()='Post64']),$NumberFormatting,'euro')"/>€
                  </phrase>
                </cell>
              </xsl:if>
              <cell></cell>
              <cell verticalalign="middle" horizontalalign="right">
                <phrase lineheight="{$WorksheetSpacing}" size="{$fontSize}" style="bold">
                  <xsl:value-of select="format-number(./node()[name()='Totaal'],$NumberFormatting,'euro')"/>€
                </phrase>
              </cell>
            </row>
          </xsl:for-each>
        </table>
      </xsl:if>
    </xsl:if>
    <xsl:if test="$EntityName = 'RCenGeboekt' and not($results//RCenGeboekt)">
      <table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" columns="3" width="100%" align="Center" widths="60;15;15" cellspacing="0" cellpadding="1">
        <row>
          <cell verticalalign="middle" colspan="2" header="true">
            <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}" fontstyle="bold">
              <xsl:value-of select="Translations:GetTranslation('WPBTW',string($EntityName),string($EntityName))"/>
            </phrase>
          </cell>
          <cell verticalalign="middle" header="true" horizontalalign="right">
            <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" fontstyle="bold">
              <xsl:value-of select="format-number(0,$NumberFormatting,'euro')"/>€
            </phrase>
          </cell>
        </row>
        <row>
          <cell colspan="3">
            <phrase>&#160;</phrase>
            <newline/>
          </cell>
        </row>
        <row>
          <cell verticalalign="middle" colspan="2">
            <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}" fontstyle="bold">
              <xsl:value-of select="Translations:GetTranslation('','TeVerklarenVerschil','Te verklaren verschil')"/>
            </phrase>
          </cell>
          <cell verticalalign="middle" horizontalalign="right">
            <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" fontstyle="bold">
              <xsl:value-of select="format-number($results//RCVerklaringen/Bedrag[../AutoKey = 'TVV' and ../order/text()='0'],$NumberFormatting,'euro')"/>€
            </phrase>
          </cell>
        </row>
      </table>
    </xsl:if>
    <xsl:if test="$EntityName = 'OmzettenGeboekt' and not($results//OmzettenGeboekt)">
      <table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" columns="3" width="100%" align="Center" widths="60;15;15" cellspacing="0" cellpadding="1">
        <row>
          <cell verticalalign="middle" colspan="2" header="true">
            <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}" fontstyle="bold">
              <xsl:value-of select="Translations:GetTranslation('WPBTW','OmzetBoekhouding','OmzetBoekhouding')"/>
            </phrase>
          </cell>
          <cell verticalalign="middle" header="true" horizontalalign="right">
            <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" fontstyle="bold">
              <xsl:value-of select="format-number(0,$NumberFormatting,'euro')"/>€
            </phrase>
          </cell>
        </row>
        <row>
          <cell colspan="3">
            <phrase>&#160;</phrase>
            <newline/>
          </cell>
        </row>
        <row>
          <cell verticalalign="middle" colspan="2">
            <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}" fontstyle="bold">
              <xsl:value-of select="Translations:GetTranslation('','TeVerklarenVerschil','Te verklaren verschil')"/>
            </phrase>
          </cell>
          <cell verticalalign="middle" horizontalalign="right">
            <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" fontstyle="bold">
              <xsl:variable name="result">
                <xsl:call-template name="abs">
                  <xsl:with-param name="n" select="(sum($results//node()[name()='OmzetAangiftes']/node()[name()='Totaal']))-(sum($results//node()[name()='OmzettenGeboekt']/node()[name()='Saldo']))"/>
                </xsl:call-template>
              </xsl:variable><xsl:value-of select="format-number($result,$NumberFormatting,'euro')"/>€
            </phrase>
          </cell>
        </row>
      </table>
    </xsl:if>
    <xsl:if test="$EntityName = 'RCenGeboekt' and count($results//RCenGeboekt) > 0">
      <table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" columns="3" width="100%" align="Center" widths="60;15;15" cellspacing="0" cellpadding="1">
        <row>
          <cell verticalalign="middle" colspan="3" header="true">
            <newline/>
            <newline/>
            <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}" fontstyle="bold">
              <xsl:value-of select="Translations:GetTranslation('WPBTW',string($EntityName),string($EntityName))"/>
            </phrase>
          </cell>
        </row>
        <xsl:for-each select="$results//node()[name()=$EntityName]">
          <row>
            <cell verticalalign="middle" horizontalalign="left" colspan="2">
              <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}">
                <xsl:value-of select="./node()[name()='NaamRekening']"/>
              </phrase>
            </cell>
            <cell verticalalign="middle" horizontalalign="right">
              <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
                <xsl:value-of select="format-number(./node()[name()='Saldo'],$NumberFormatting,'euro')"/>€
              </phrase>
            </cell>
          </row>
        </xsl:for-each>
        <row>
          <cell>
            <phrase lineheight="{$WorksheetSpacing}"/>
          </cell>
          <cell verticalalign="middle" horizontalalign="center">
            <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}" fontstyle="bold">
              <xsl:value-of select="Translations:GetTranslation('WPBTW','Totaal','Totaal')"/>
            </phrase>
          </cell>
          <cell verticalalign="middle" horizontalalign="right">
            <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" fontstyle="bold">
              <xsl:variable name="DummyToAddAmountBTW" select="Helper:AddSum('BTW',sum($results//node()[name()=$EntityName]/node()[name()='Saldo']))"/><xsl:value-of select="format-number($DummyToAddAmountBTW,$NumberFormatting,'euro')"/>€
            </phrase>
          </cell>
        </row>
        <row>
          <cell colspan="3">
            <phrase>&#160;</phrase>
            <newline/>
          </cell>
        </row>
        <row>
          <cell verticalalign="middle" colspan="2">
            <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}" fontstyle="bold">
              <xsl:value-of select="Translations:GetTranslation('','TeVerklarenVerschil','Te verklaren verschil')"/>
            </phrase>
          </cell>
          <cell verticalalign="middle" horizontalalign="right">
            <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" fontstyle="bold">
              <xsl:value-of select="format-number($results//RCVerklaringen/Bedrag[../AutoKey = 'TVV' and ../order/text()='0'],$NumberFormatting,'euro')"/>€
            </phrase>
          </cell>
        </row>
      </table>
    </xsl:if>
    <xsl:if test="$EntityName = 'OmzettenGeboekt' and count($results//OmzettenGeboekt) > 0">
      <xsl:if test="$EntityName = 'OmzettenGeboekt'">
        <table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" columns="3" width="100%" align="Center" widths="60;15;15" cellspacing="0" cellpadding="1">
          <row>
            <cell verticalalign="middle" colspan="3" header="true">
              <newline/>
              <newline/>
              <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}" fontstyle="bold">
                <xsl:value-of select="Translations:GetTranslation('WPBTW',string($EntityName),string($EntityName))"/>
              </phrase>
            </cell>
          </row>
          <xsl:for-each select="$results//node()[name()=$EntityName]">
            <row>
              <cell verticalalign="middle" horizontalalign="left" colspan="2">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}">
                  <xsl:value-of select="./node()[name()='NaamRekening']"/>
                </phrase>
              </cell>
              <cell verticalalign="middle" horizontalalign="right">
                <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
                  <xsl:value-of select="format-number(./node()[name()='Saldo'],$NumberFormatting,'euro')"/>€
                </phrase>
              </cell>
            </row>
          </xsl:for-each>
          <!--xsl:if test="$results//node()[name()='OmzetVerklaringen']/AutoKey = 'TVV'"><row><cell verticalalign="middle" horizontalalign="left"><phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}"><xsl:value-of select="$results//node()[name()='OmzetVerklaringen']/Omschrijving"/></phrase></cell><cell verticalalign="middle" horizontalalign="right"><phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetFontsize}"><xsl:value-of select="format-number($results//node()[name()='OmzetVerklaringen']/Bedrag/text(),$NumberFormatting,'euro')"/></phrase></cell></row></xsl:if-->
          <row>
            <cell>
              <phrase lineheight="{$WorksheetSpacing}"/>
            </cell>
            <cell verticalalign="middle" horizontalalign="center">
              <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}" fontstyle="bold">
                <xsl:value-of select="Translations:GetTranslation('WPBTW','Totaal','Totaal')"/>
              </phrase>
            </cell>
            <cell verticalalign="middle" horizontalalign="right">
              <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" fontstyle="bold">
                <xsl:variable name="DummyToAddAmountBTW" select="sum($results//node()[name()=$EntityName]/node()[name()='Saldo'])"/><xsl:value-of select="format-number($DummyToAddAmountBTW,$NumberFormatting,'euro')"/>€
              </phrase>
            </cell>
          </row>
          <row>
            <cell colspan="3">
              <phrase>&#160;</phrase>
              <newline/>
            </cell>
          </row>
          <row>
            <cell verticalalign="middle" colspan="2">
              <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}" fontstyle="bold">
                <xsl:value-of select="Translations:GetTranslation('','TeVerklarenVerschil','Te verklaren verschil')"/>
              </phrase>
            </cell>
            <cell verticalalign="middle" horizontalalign="right">
              <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" fontstyle="bold">
                <xsl:variable name="result">
                  <xsl:call-template name="abs">
                    <xsl:with-param name="n" select="(sum($results//node()[name()='OmzetAangiftes']/node()[name()='Totaal']))-(sum($results//node()[name()='OmzettenGeboekt']/node()[name()='Saldo']))"/>
                  </xsl:call-template>
                </xsl:variable><xsl:value-of select="format-number($result,$NumberFormatting,'euro')"/>€
              </phrase>
            </cell>
          </row>
        </table>
      </xsl:if>
    </xsl:if>
    <xsl:if test="(($EntityName = 'OmzetVerklaringen' and number($omzetverschil)>0 ) or ($EntityName = 'RCVerklaringen' and number($rcverschil)>0))">
      <table tablefitspage="true" convert2pdfp="true" cellsfitpage="false" columns="3" width="100%" align="Center" widths="60;15;15" cellspacing="0" cellpadding="1">
        <row>
          <cell colspan="3">
            <phrase lineheight="{$WorksheetSpacing}"></phrase>
            <newline/>
            <phrase lineheight="{$WorksheetSpacing}"></phrase>
          </cell>
        </row>
        <row>
          <cell verticalalign="middle" colspan="3" header="true">
            <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}" fontstyle="bold">
              <xsl:value-of select="Translations:GetTranslation('','Verklaring','Verklaring')"/>
            </phrase>
          </cell>
        </row>
        <!--row><cell verticalalign="middle" colspan="3" header="true"><phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}" fontstyle="bold"><xsl:value-of select="Translations:GetTranslation('WPBTW',string($EntityName),string($EntityName))"/></phrase></cell></row-->
        <row>
          <cell verticalalign="middle" horizontalalign="left" header="true">
            <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" fontstyle="bold">
              <!--xsl:apply-templates select="$EntityDefinition/Fields/FieldDef[@Name = 'Omschrijving']" mode="getLabel"/-->
            </phrase>
          </cell>
          <cell verticalalign="middle" horizontalalign="center" header="true">
            <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}" fontstyle="bold">
              <xsl:value-of select="Translations:GetTranslation($entName,'Jaar','Jaar')"/>
            </phrase>
          </cell>
          <cell verticalalign="middle" horizontalalign="center" header="true">
            <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}" fontstyle="bold">
              <xsl:value-of select="Translations:GetTranslation($entName,'Bedrag','Bedrag')"/>
            </phrase>
          </cell>
        </row>
        <xsl:for-each select="$results//node()[name()=$EntityName][AutoKey != 'TVV']">
          <row>
            <cell verticalalign="middle" horizontalalign="left">
              <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}">
                <xsl:value-of select="./node()[name()='Omschrijving']"/>
              </phrase>
            </cell>
            <cell verticalalign="middle" horizontalalign="center">
              <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
                <xsl:value-of select="./node()[name()='Jaar']"/>
              </phrase>
            </cell>
            <cell verticalalign="middle" horizontalalign="right">
              <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}">
                <xsl:value-of select="format-number(./node()[name()='Bedrag'],$NumberFormatting,'euro')"/>€
              </phrase>
            </cell>
          </row>
        </xsl:for-each>
        <row>
          <cell>
            <phrase lineheight="{$WorksheetSpacing}"/>
          </cell>
          <cell verticalalign="middle" horizontalalign="center">
            <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}" fontstyle="bold">
              <xsl:value-of select="Translations:GetTranslation('WPBTW','Totaal','Totaal')"/>
            </phrase>
          </cell>
          <cell verticalalign="middle" horizontalalign="right">
            <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" fontstyle="bold">
              <xsl:variable name="TotalExpl" select="sum($results//node()[name()=$EntityName][AutoKey != 'TVV']/Bedrag)"/><xsl:value-of select="format-number($TotalExpl,$NumberFormatting,'euro')"/>€
            </phrase>
          </cell>
        </row>
        <row>
          <cell colspan="3">
            <phrase lineheight="{$WorksheetSpacing}"/>
          </cell>
        </row>
        <row>
          <cell colspan="3">
            <phrase lineheight="{$WorksheetSpacing}"/>
            <newline/>
          </cell>
        </row>
        <row>
          <cell colspan="2">
            <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetLabelFontsize}" fontstyle="bold">
              <xsl:value-of select="Translations:GetTranslation('WPBTW','ResterendVerschil','ResterendVerschil')"/>
            </phrase>
          </cell>
          <xsl:if test="$EntityName = 'OmzetVerklaringen'">
            <cell verticalalign="middle" horizontalalign="right" borderwidth="0" top="false" bordercolor="black">
              <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" fontstyle="bold">
                <xsl:variable name="TotalOmzetTvv" select="(sum($results//node()[name()='OmzetAangiftes']/node()[name()='Totaal']))-(sum($results//node()[name()='OmzettenGeboekt']/node()[name()='Saldo']))"/><xsl:variable name="TotalOmzetVerklaringen" select="sum($results//node()[name()='OmzetVerklaringen'][AutoKey = '']/Bedrag/text())"/><xsl:variable name="result">
                  <xsl:call-template name="abs">
                    <xsl:with-param name="n" select="($TotalOmzetTvv) - ($TotalOmzetVerklaringen)"/>
                  </xsl:call-template>
                </xsl:variable><xsl:value-of select="format-number($result,$NumberFormatting,'euro')"/>€
              </phrase>
            </cell>
          </xsl:if>
          <xsl:if test="$EntityName = 'RCVerklaringen'">
            <cell verticalalign="middle" horizontalalign="right" borderwidth="0" top="false" bordercolor="black">
              <phrase lineheight="{$WorksheetSpacing}" size="{$WorksheetNumbersize}" fontstyle="bold">
                <xsl:value-of select="format-number($results//RCVerklaringen/Bedrag[../AutoKey = 'TVV' and ../order/text()='999'],$NumberFormatting,'euro')"/>€
              </phrase>
            </cell>
          </xsl:if>
        </row>
      </table>
    </xsl:if>
  </xsl:template>
  <xsl:template name="abs">
    <xsl:param name="n" />
    <xsl:choose>
      <xsl:when test="$n &gt;= 0">
        <xsl:value-of select="$n" />
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="0 - $n" />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
</xsl:stylesheet>