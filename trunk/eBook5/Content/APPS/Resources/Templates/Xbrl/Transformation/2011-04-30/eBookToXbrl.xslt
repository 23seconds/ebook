<xsl:stylesheet version="1.0"
                xmlns="http://www.xbrl.org/2003/instance"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:msxsl="urn:schemas-microsoft-com:xslt"
                xmlns:pfs-gcd="http://www.nbb.be/be/fr/pfs/ci/gcd/2011-04-01"
                xmlns:pfs-vl="http://www.nbb.be/be/fr/pfs/ci/vl/2011-04-01"
                xmlns:d-br="http://www.minfin.fgov.be/be/tax/d/br/2011-04-30"
                xmlns:d-depm="http://www.minfin.fgov.be/be/tax/d/depm/2011-04-30"
                xmlns:d-ec="http://www.minfin.fgov.be/be/tax/d/ec/2011-04-30"
                xmlns:d-expt="http://www.minfin.fgov.be/be/tax/d/expt/2011-04-30"
                xmlns:d-origin="http://www.minfin.fgov.be/be/tax/d/origin/2011-04-30"
                xmlns:d-asst="http://www.minfin.fgov.be/be/tax/d/asst/2011-04-30"
                xmlns:d-incc="http://www.minfin.fgov.be/be/tax/d/incc/2011-04-30"
                xmlns:d-invc="http://www.minfin.fgov.be/be/tax/d/invc/2011-04-30"
                xmlns:d-per="http://www.minfin.fgov.be/be/tax/d/per/2011-04-30"
                xmlns:d-empreg="http://www.minfin.fgov.be/be/tax/d/empreg/2011-04-30"
                xmlns:d-empstat="http://www.minfin.fgov.be/be/tax/d/empstat/2011-04-30"
                xmlns:d-tu="http://www.minfin.fgov.be/be/tax/d/tu/2011-04-30"
                xmlns:d-empf="http://www.minfin.fgov.be/be/tax/d/empf/2011-04-30"
                xmlns:d-empper="http://www.minfin.fgov.be/be/tax/d/empper/2011-04-30"
                xmlns:d-ty="http://www.minfin.fgov.be/be/tax/d/ty/2011-04-30"
                xmlns:iso4217="http://www.xbrl.org/2003/iso4217"
                xmlns:link="http://www.xbrl.org/2003/linkbase"
                xmlns:tax-inc="http://www.minfin.fgov.be/be/tax/inc/2011-04-30"
                xmlns:tax-inc-rcorp="http://www.minfin.fgov.be/be/tax/inc/rcorp/2011-04-30"
                xmlns:xbrldi="http://xbrl.org/2006/xbrldi"
                xmlns:xbrli="http://www.xbrl.org/2003/instance"
                xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                xmlns:helper="urn:helper"
                exclude-result-prefixes="msxsl helper xbrli">
  <xsl:output method="xml" indent="yes" omit-xml-declaration="yes"/>

  <xsl:variable name="TaxReturnType">Vennootschapsbelasting</xsl:variable>

  <xsl:template match="XbrlInput">
    <xbrl xsi:schemaLocation="http://xbrl.org/2006/xbrldi http://www.xbrl.org/2006/xbrldi-2006.xsd">
      <link:schemaRef xlink:href="be-tax-inc-rcorp-2011-04-30.xsd" xlink:type="simple" />
      <xsl:apply-templates select="." mode="CONTEXTS"/>
      <xsl:apply-templates select="." mode="UNITS"/>
      <xsl:apply-templates select="." mode="ENTITY_INFORMATION"/>
      <xsl:apply-templates select="." mode="DOCUMENT_INFORMATION"/>
      <xsl:apply-templates select="." mode="TAXINC"/>
      <xsl:choose>
        <xsl:when test="Manual='true'">
          <xsl:apply-templates select="//DetailVoorziening" mode="VoorzieningenManualDetail"/>
          <xsl:apply-templates select="//DetailDubDeb" mode="DubieuzeDebiteurenManualDetail"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:apply-templates select="Voorzieningen" mode="VoorzieningenDetail"/>
          <xsl:apply-templates select="DubieuzeDebiteuren" mode="DubieuzeDebiteurenDetail"/>
        </xsl:otherwise>
      </xsl:choose>
      <!--<xsl:apply-templates select="Fiche" mode="_275C"/>-->
    </xbrl>
  </xsl:template>

  <xsl:template match="XbrlInput" mode="CONTEXTS">
    <context id="D">
      <entity>
        <identifier scheme="http://www.fgov.be">
             <xsl:value-of select="Client/XbrlClientDataContract/cenr"/>
        </identifier>
      </entity>
      <period>
        <startDate>
          <xsl:value-of select="StartDate"/>
        </startDate>
        <endDate>
          <xsl:value-of select="EndDate"/>
        </endDate>
      </period>
    </context>
    <context id="I-Start">
      <entity>
        <identifier scheme="http://www.fgov.be">
          <xsl:value-of select="Client/XbrlClientDataContract/cenr"/>
        </identifier>
      </entity>
      <period>
        <instant>
          <xsl:value-of select="PreviousEndDate"/>
        </instant>
      </period>
    </context>
    <context id="I-End">
      <entity>
        <identifier scheme="http://www.fgov.be">
          <xsl:value-of select="Client/XbrlClientDataContract/cenr"/>
        </identifier>
      </entity>
      <period>
        <instant>
          <xsl:value-of select="EndDate"/>
        </instant>
      </period>
    </context>

    <context id="D__ForeignBranchMember">
      <entity>
        <identifier scheme="http://www.fgov.be">
          <xsl:value-of select="Client/XbrlClientDataContract/cenr"/>
        </identifier>
      </entity>
      <period>
        <startDate>
          <xsl:value-of select="StartDate"/>
        </startDate>
        <endDate>
          <xsl:value-of select="EndDate"/>
        </endDate>
      </period>
      <scenario>
        <xbrldi:explicitMember dimension="d-br:BranchDimension">d-br:ForeignBranchMember</xbrldi:explicitMember>
      </scenario>
    </context>
    <context id="D__BelgianBranchMember">
      <entity>
        <identifier scheme="http://www.fgov.be">
          <xsl:value-of select="Client/XbrlClientDataContract/cenr"/>
        </identifier>
      </entity>
      <period>
        <startDate>
          <xsl:value-of select="StartDate"/>
        </startDate>
        <endDate>
          <xsl:value-of select="EndDate"/>
        </endDate>
      </period>
      <scenario>
        <xbrldi:explicitMember dimension="d-br:BranchDimension">d-br:BelgianBranchMember</xbrldi:explicitMember>
      </scenario>
    </context>

    <context id="D__TaxTreatyMember">
      <entity>
        <identifier scheme="http://www.fgov.be">
          <xsl:value-of select="Client/XbrlClientDataContract/cenr"/>
        </identifier>
      </entity>
      <period>
        <startDate>
          <xsl:value-of select="StartDate"/>
        </startDate>
        <endDate>
          <xsl:value-of select="EndDate"/>
        </endDate>
      </period>
      <scenario>
        <xbrldi:explicitMember dimension="d-origin:OriginDimension">d-origin:TaxTreatyMember</xbrldi:explicitMember>
      </scenario>
    </context>
    <context id="D__NoTaxTreatyMember">
      <entity>
        <identifier scheme="http://www.fgov.be">
          <xsl:value-of select="Client/XbrlClientDataContract/cenr"/>
        </identifier>
      </entity>
      <period>
        <startDate>
          <xsl:value-of select="StartDate"/>
        </startDate>
        <endDate>
          <xsl:value-of select="EndDate"/>
        </endDate>
      </period>
      <scenario>
        <xbrldi:explicitMember dimension="d-origin:OriginDimension">d-origin:NoTaxTreatyMember</xbrldi:explicitMember>
      </scenario>
    </context>
    <context id="D__BelgiumMember">
      <entity>
        <identifier scheme="http://www.fgov.be">
          <xsl:value-of select="Client/XbrlClientDataContract/cenr"/>
        </identifier>
      </entity>
      <period>
        <startDate>
          <xsl:value-of select="StartDate"/>
        </startDate>
        <endDate>
          <xsl:value-of select="EndDate"/>
        </endDate>
      </period>
      <scenario>
        <xbrldi:explicitMember dimension="d-origin:OriginDimension">d-origin:BelgiumMember</xbrldi:explicitMember>
      </scenario>
    </context>

    <context id="I-Start__id__anderereserves3">
      <entity>
        <identifier scheme="http://www.fgov.be">
          <xsl:value-of select="Client/XbrlClientDataContract/cenr"/>
        </identifier>
      </entity>
      <period>
        <instant>
          <xsl:value-of select="PreviousEndDate"/>
        </instant>
      </period>
      <scenario>
        <xbrldi:typedMember dimension="d-ty:DescriptionTypedDimension">
          <d-ty:DescriptionTypedID>anderereserves3</d-ty:DescriptionTypedID>
        </xbrldi:typedMember>
      </scenario>
    </context>
    <context id="I-End__id__anderereserves2">
      <entity>
        <identifier scheme="http://www.fgov.be">
          <xsl:value-of select="Client/XbrlClientDataContract/cenr"/>
        </identifier>
      </entity>
      <period>
        <instant>
          <xsl:value-of select="EndDate"/>
        </instant>
      </period>
      <scenario>
        <xbrldi:typedMember dimension="d-ty:DescriptionTypedDimension">
          <d-ty:DescriptionTypedID>anderereserves2</d-ty:DescriptionTypedID>
        </xbrldi:typedMember>
      </scenario>
    </context>
    <context id="I-Start__id__anderereserves2">
      <entity>
        <identifier scheme="http://www.fgov.be">
          <xsl:value-of select="Client/XbrlClientDataContract/cenr"/>
        </identifier>
      </entity>
      <period>
        <instant>
          <xsl:value-of select="PreviousEndDate"/>
        </instant>
      </period>
      <scenario>
        <xbrldi:typedMember dimension="d-ty:DescriptionTypedDimension">
          <d-ty:DescriptionTypedID>anderereserves2</d-ty:DescriptionTypedID>
        </xbrldi:typedMember>
      </scenario>
    </context>
    <context id="I-End__id__anderereserves1">
      <entity>
        <identifier scheme="http://www.fgov.be">
          <xsl:value-of select="Client/XbrlClientDataContract/cenr"/>
        </identifier>
      </entity>
      <period>
        <instant>
          <xsl:value-of select="EndDate"/>
        </instant>
      </period>
      <scenario>
        <xbrldi:typedMember dimension="d-ty:DescriptionTypedDimension">
          <d-ty:DescriptionTypedID>anderereserves1</d-ty:DescriptionTypedID>
        </xbrldi:typedMember>
      </scenario>
    </context>
    <context id="I-Start__id__anderereserves1">
      <entity>
        <identifier scheme="http://www.fgov.be">
          <xsl:value-of select="Client/XbrlClientDataContract/cenr"/>
        </identifier>
      </entity>
      <period>
        <instant>
          <xsl:value-of select="PreviousEndDate"/>
        </instant>
      </period>
      <scenario>
        <xbrldi:typedMember dimension="d-ty:DescriptionTypedDimension">
          <d-ty:DescriptionTypedID>anderereserves1</d-ty:DescriptionTypedID>
        </xbrldi:typedMember>
      </scenario>
    </context>
  </xsl:template>

  <xsl:template match="XbrlInput" mode="UNITS">
    <unit id="U-Pure">
      <measure>pure</measure>
    </unit>
    <unit id="EUR">
      <measure>iso4217:EUR</measure>
    </unit>
  </xsl:template>

  <xsl:template match="XbrlInput" mode="ENTITY_INFORMATION">
    <pfs-gcd:EntityInformation>
      <pfs-gcd:EntityName>
        <pfs-gcd:EntityCurrentLegalName contextRef="D">
          <xsl:value-of select="Client/XbrlClientDataContract/n"/>
        </pfs-gcd:EntityCurrentLegalName>
      </pfs-gcd:EntityName>
      <pfs-gcd:EntityIdentifier>
        <pfs-gcd:IdentifierName contextRef="D">Ondernemingsnummer</pfs-gcd:IdentifierName>
        <pfs-gcd:IdentifierValue contextRef="D">
          <xsl:value-of select="Client/XbrlClientDataContract/enr"/>
        </pfs-gcd:IdentifierValue>
      </pfs-gcd:EntityIdentifier>
      <pfs-gcd:EntityForm>
        <xsl:call-template name="ValuedNode2">
          <xsl:with-param name="BaseNodeName" select="concat('XCode_LegalFormCode_',Client/XbrlClientDataContract/lti)"/>
          <xsl:with-param name="Value" select="Client/XbrlClientDataContract/lti" />
        </xsl:call-template>
      </pfs-gcd:EntityForm>
      <pfs-gcd:EntityAddress>
        <pfs-gcd:AddressType>
          <xsl:call-template name="ValuedNode2">
            <xsl:with-param name="BaseNodeName" select="Client/XbrlClientDataContract/foda/at/id"/>
            <xsl:with-param name="Value" select="Client/XbrlClientDataContract/foda/at/v" />
          </xsl:call-template>
        </pfs-gcd:AddressType>
        <pfs-gcd:Street contextRef="D">
          <xsl:value-of select="Client/XbrlClientDataContract/foda/s"/>
        </pfs-gcd:Street>
        <pfs-gcd:Number contextRef="D">
          <xsl:value-of select="Client/XbrlClientDataContract/foda/n"/>
        </pfs-gcd:Number>
        <pfs-gcd:PostalCodeCity>
          <!-- -->
          <xsl:call-template name="ValuedNode2">
            <xsl:with-param name="BaseNodeName" select="Client/XbrlClientDataContract/foda/z/id"/>
            <xsl:with-param name="Value" select="Client/XbrlClientDataContract/foda/z/v" />
          </xsl:call-template>
        </pfs-gcd:PostalCodeCity>
        <pfs-gcd:CountryCode>
          <xsl:call-template name="ValuedNode2">
            <xsl:with-param name="BaseNodeName" select="Client/XbrlClientDataContract/foda/co/id"/>
            <xsl:with-param name="Value" select="Client/XbrlClientDataContract/foda/co/v" />
          </xsl:call-template>
        </pfs-gcd:CountryCode>
      </pfs-gcd:EntityAddress>
      <xsl:if test="Client/XbrlClientDataContract/b/Iban and Client/XbrlClientDataContract/b/Bic and not(Client/XbrlClientDataContract/b/Iban='' and Client/XbrlClientDataContract/b/Bic='')">
      <pfs-gcd:BankInformation>
        <pfs-gcd:BankIdentifierCode contextRef="D">
        <xsl:value-of select="Client/XbrlClientDataContract/b/Bic"/>
      </pfs-gcd:BankIdentifierCode>
        <pfs-gcd:InternationalBankAccountNumber contextRef="D">
        <xsl:value-of select="Client/XbrlClientDataContract/b/Iban"/>
      </pfs-gcd:InternationalBankAccountNumber>
      </pfs-gcd:BankInformation>
      </xsl:if>
    </pfs-gcd:EntityInformation>
  </xsl:template>


  <xsl:template name="ValuedNode2">
    <xsl:param name="BaseNodeName"/>
    <xsl:param name="Value"/>
    <xsl:variable name="element" select="concat('pfs-vl:',$BaseNodeName)"/>
    <xsl:element name="{$element}">
      <xsl:attribute name="contextRef">D</xsl:attribute>
      <xsl:value-of select="$Value"/>
    </xsl:element>
  </xsl:template>
  
  
  <xsl:template match="XbrlInput" mode="DOCUMENT_INFORMATION">
    <pfs-gcd:DocumentInformation>
      <pfs-gcd:DocumentIdentifier contextRef="D">Aangifte</pfs-gcd:DocumentIdentifier>
      <pfs-gcd:DocumentLanguage>
        <xsl:call-template name="ValuedNode">
          <xsl:with-param name="BaseNodeName" select="'pfs-vl:XCode_LanguageCode_'"/>
          <xsl:with-param name="Value" select="Language" />
        </xsl:call-template>
      </pfs-gcd:DocumentLanguage>
      
      <!--  Auteur invullen? -->
      <!--<pfs-gcd:DocumentAuthor></pfs-gcd:DocumentAuthor>-->
      <!-- CONTACT TYPE NOG TE IMPLEMENTEREN IN BIZTAX WIZARD-->
      <xsl:if test="Contact/FodContactDataContract">
        <pfs-gcd:DocumentContact>


          <pfs-gcd:DocumentContactType>
            <!--
            AangifteVenB/Bank/CPHoedanigheid
            Gegevens van de jaarrekening		              XCode_DocumentContactCode_CA
            Gegevens van de sociale balans		            XCode_DocumentContactCode_BS
            Gegevens van de aangifte inkomstenbelasting		XCode_DocumentContactCode_CT

          
          <xsl:call-template name="ValuedNode">
            <xsl:with-param name="BaseNodeName" select="'pfs-vl:XCode_DocumentContactCode_'"/>
            <xsl:with-param name="Value" select="'CT'" />
          </xsl:call-template>
          -->
            <xsl:call-template name="ValuedNode2">
              <xsl:with-param name="BaseNodeName" select="Contact/FodContactDataContract/ct/id"/>
              <xsl:with-param name="Value" select="Contact/FodContactDataContract/ct/v" />
            </xsl:call-template>
          </pfs-gcd:DocumentContactType>
          <pfs-gcd:ContactName contextRef="D">
            <xsl:value-of select="Contact/FodContactDataContract/n"/>
          </pfs-gcd:ContactName>
          <pfs-gcd:ContactFirstName contextRef="D">
            <xsl:value-of select="Contact/FodContactDataContract/fn"/>
          </pfs-gcd:ContactFirstName>
          <pfs-gcd:ContactTitlePosition contextRef="D">
            <xsl:value-of select="Contact/FodContactDataContract/f"/>
          </pfs-gcd:ContactTitlePosition>
          <xsl:if test="Contact/FodContactDataContract/a">
            <pfs-gcd:ContactAddress>
              <!--
              Adres Maatschappelijke Zetel of Domicilie	        XCode_AddressTypeCode_001
              Adres Vestigingseenheid	                          XCode_AddressTypeCode_002
              Bijhuis in België voor buitenlandse onderneming		XCode_AddressTypeCode_003
              Fiscale woonplaats	                              XCode_AddressTypeCode_004
              Correspondentieadres	                            XCode_AddressTypeCode_005

        -->

              <pfs-gcd:AddressType>
                <xsl:call-template name="ValuedNode2">
                  <xsl:with-param name="BaseNodeName" select="Contact/FodContactDataContract/a/at/id"/>
                  <xsl:with-param name="Value" select="Contact/FodContactDataContract/a/at/v" />
                </xsl:call-template>
              </pfs-gcd:AddressType>
              <pfs-gcd:Street contextRef="D">
                <xsl:value-of select="Contact/FodContactDataContract/a/s"/>
              </pfs-gcd:Street>
              <pfs-gcd:Number contextRef="D">
                <xsl:value-of select="Contact/FodContactDataContract/a/n"/>
              </pfs-gcd:Number>
              <xsl:if test="not(Contact/FodContactDataContract/a/b='')">
                <pfs-gcd:Box contextRef="D">
                  <xsl:value-of select="Contact/FodContactDataContract/a/b"/>
                </pfs-gcd:Box>
              </xsl:if>
              <pfs-gcd:PostalCodeCity>
                <xsl:call-template name="ValuedNode2">
                  <xsl:with-param name="BaseNodeName" select="Contact/FodContactDataContract/a/z/id"/>
                  <xsl:with-param name="Value" select="Contact/FodContactDataContract/a/z/v" />
                </xsl:call-template>
                <!-- 
          <xsl:if test="AangifteVenB/Bank/CPPostcodeGemeente and not(AangifteVenB/Bank/CPPostcodeGemeente='')">
            <xsl:call-template name="ValuedNode">
              <xsl:with-param name="BaseNodeName" select="'pfs-vl:XCode_PostalCode_'"/>
              <xsl:with-param name="Value" select="AangifteVenB/Bank/CPPostcodeGemeente" />
            </xsl:call-template>
          </xsl:if>
          -->
              </pfs-gcd:PostalCodeCity>
              <pfs-gcd:CountryCode>
                <xsl:call-template name="ValuedNode2">
                  <xsl:with-param name="BaseNodeName" select="Contact/FodContactDataContract/a/co/id"/>
                  <xsl:with-param name="Value" select="Contact/FodContactDataContract/a/co/v" />
                </xsl:call-template>
              </pfs-gcd:CountryCode>
            </pfs-gcd:ContactAddress>
          </xsl:if>
          <xsl:if test="Contact/FodContactDataContract/p and Contact/FodContactDataContract/p/act='true'">
            <pfs-gcd:ContactPhoneFaxNumber>
              <pfs-gcd:PhoneFaxNumber contextRef="D">
                phone
              </pfs-gcd:PhoneFaxNumber>
              <pfs-gcd:CountryRegion contextRef="D">
                <xsl:value-of select="Contact/FodContactDataContract/p/cc"/>
              </pfs-gcd:CountryRegion>
              <pfs-gcd:CityAreaCode contextRef="D">
                <xsl:value-of select="Contact/FodContactDataContract/p/z"/>
              </pfs-gcd:CityAreaCode>
              <pfs-gcd:LocalPhoneNumber contextRef="D">
                <xsl:value-of select="Contact/FodContactDataContract/p/l"/>
              </pfs-gcd:LocalPhoneNumber>
            </pfs-gcd:ContactPhoneFaxNumber>
          </xsl:if>


          <pfs-gcd:ContactEmail>
            <pfs-gcd:EmailAddress contextRef="D">
              <xsl:value-of select="Contact/FodContactDataContract/e"/>
            </pfs-gcd:EmailAddress>
          </pfs-gcd:ContactEmail>


        </pfs-gcd:DocumentContact>
      </xsl:if>
      <pfs-gcd:SoftwareVendor contextRef="D">Ernst &amp; Young</pfs-gcd:SoftwareVendor>
      
    </pfs-gcd:DocumentInformation>
  </xsl:template>

  <xsl:template match="XbrlInput" mode="TAXINC">
    
    <tax-inc:PeriodStartDate contextRef="I-Start">
      <xsl:value-of select="StartDate"/>
    </tax-inc:PeriodStartDate>
    <tax-inc:PeriodEndDate contextRef="I-End">
      <xsl:value-of select="EndDate"/>
    </tax-inc:PeriodEndDate>
    <tax-inc:AssessmentYear contextRef="D">
      <xsl:value-of select="Assessment"/>
    </tax-inc:AssessmentYear>
    <tax-inc:TaxReturnType contextRef="D">
      <xsl:value-of select="$TaxReturnType"/>
    </tax-inc:TaxReturnType>
    <xsl:apply-templates select="AangifteVenB" mode="TAXINC"/>
    <xsl:apply-templates select="Bijlagen" mode="BIJLAGEN"/>
    
    <xsl:choose>
      <xsl:when test="Manual='true'">
        <xsl:apply-templates select="//Form275C" mode="_275C"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:apply-templates select="Fiche" mode="_275C"/>
      </xsl:otherwise>
    </xsl:choose>
            
  </xsl:template>

  <xsl:template match="AangifteVenB" mode="TAXINC">

    <!--___________________________________-->
    <!--I. - RESERVES                      -->
    <!--___________________________________-->
    <!--A. BELASTBARE GERESERVEERDE WINST-->
    <tax-inc:TaxableReservesCapitalSharePremiums contextRef="I-Start" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(Reserves/AKapitaalBT)"/>
    </tax-inc:TaxableReservesCapitalSharePremiums>
    <tax-inc:TaxableReservesCapitalSharePremiums contextRef="I-End" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(Reserves/AKapitaalET)"/>
    </tax-inc:TaxableReservesCapitalSharePremiums>
    <tax-inc:TaxablePortionRevaluationSurpluses contextRef="I-Start" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(Reserves/AHerwaarderingBT)"/>
    </tax-inc:TaxablePortionRevaluationSurpluses>
    <tax-inc:TaxablePortionRevaluationSurpluses contextRef="I-End" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(Reserves/AHerwaarderingET)"/>
    </tax-inc:TaxablePortionRevaluationSurpluses>
    <tax-inc:LegalReserve contextRef="I-Start" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(Reserves/AWetReserveBT)"/>
    </tax-inc:LegalReserve>
    <tax-inc:LegalReserve contextRef="I-End" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(Reserves/AWetReserveET)"/>
    </tax-inc:LegalReserve>
    <tax-inc:UnavailableReserves contextRef="I-Start" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(Reserves/AOnbeschikReserveBT)"/>
    </tax-inc:UnavailableReserves>
    <tax-inc:UnavailableReserves contextRef="I-End" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(Reserves/AOnbeschikReserveET)"/>
    </tax-inc:UnavailableReserves>
    <tax-inc:AvailableReserves contextRef="I-Start" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(Reserves/ABeschikReserveBT)"/>
    </tax-inc:AvailableReserves>
    <tax-inc:AvailableReserves contextRef="I-End" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(Reserves/ABeschikReserveET)"/>
    </tax-inc:AvailableReserves>
    <tax-inc:AccumulatedProfitsLosses contextRef="I-Start" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(Reserves/AOvergedragenResWinstVerliesBT)"/>
    </tax-inc:AccumulatedProfitsLosses>
    <tax-inc:AccumulatedProfitsLosses contextRef="I-End" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(Reserves/AOvergedragenResWinstVerliesET)"/>
    </tax-inc:AccumulatedProfitsLosses>
    <tax-inc:TaxableProvisions contextRef="I-Start" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(Reserves/ABelastbareVoorzieningenBT)"/>
    </tax-inc:TaxableProvisions>
    <tax-inc:TaxableProvisions contextRef="I-End" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(Reserves/ABelastbareVoorzieningenET)"/>
    </tax-inc:TaxableProvisions>
    <!--This is the section 1-h) of the vensocB-->
    <!-- other reserves, none needed if not at hand or only one needed or all three nodes needed? -->
    <tax-inc:OtherReserves contextRef="I-Start__id__anderereserves1" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(Reserves/AIBalansBT)"/>
    </tax-inc:OtherReserves>
    <tax-inc:OtherReserves contextRef="I-End__id__anderereserves1" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(Reserves/AIBalansET)"/>
    </tax-inc:OtherReserves>
    <tax-inc:OtherReserves contextRef="I-Start__id__anderereserves2" decimals="INF" unitRef="EUR">0</tax-inc:OtherReserves>
    <tax-inc:OtherReserves contextRef="I-End__id__anderereserves2" decimals="INF" unitRef="EUR">0</tax-inc:OtherReserves>
    <tax-inc:OtherReserves contextRef="I-Start__id__anderereserves3" decimals="INF" unitRef="EUR">0</tax-inc:OtherReserves>
    
    <tax-inc:OtherTaxableReserves contextRef="I-Start__id__anderereserves1" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(Reserves/AIBReserveBT)"/>
    </tax-inc:OtherTaxableReserves>
    <tax-inc:OtherTaxableReserves contextRef="I-End__id__anderereserves1" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(Reserves/AIBReserveET)"/>
    </tax-inc:OtherTaxableReserves>
    <!-- other taxable reserves, none needed if not at hand or only one needed or all three nodes needed? -->
    <tax-inc:OtherTaxableReserves contextRef="I-Start__id__anderereserves2" decimals="INF" unitRef="EUR">0</tax-inc:OtherTaxableReserves>
    <tax-inc:OtherTaxableReserves contextRef="I-End__id__anderereserves2" decimals="INF" unitRef="EUR">0</tax-inc:OtherTaxableReserves>
    <tax-inc:OtherTaxableReserves contextRef="I-Start__id__anderereserves3" decimals="INF" unitRef="EUR">0</tax-inc:OtherTaxableReserves>
    
    <tax-inc:TaxableWriteDownsUndisclosedReserve contextRef="I-Start" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(Reserves/AORBelastbareWVBT)"/>
    </tax-inc:TaxableWriteDownsUndisclosedReserve>
    <tax-inc:TaxableWriteDownsUndisclosedReserve contextRef="I-End" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(Reserves/AORBelastbareWVET)"/>
    </tax-inc:TaxableWriteDownsUndisclosedReserve>
    <tax-inc:ExaggeratedDepreciationsUndisclosedReserve contextRef="I-Start" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(Reserves/AOROverdrevenAfschrBT)"/>
    </tax-inc:ExaggeratedDepreciationsUndisclosedReserve>
    <tax-inc:ExaggeratedDepreciationsUndisclosedReserve contextRef="I-End" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(Reserves/AOROverdrevenAfschrET)"/>
    </tax-inc:ExaggeratedDepreciationsUndisclosedReserve>
    <tax-inc:OtherUnderestimationsAssetsUndisclosedReserve contextRef="I-Start" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(Reserves/AORAndereActBT)"/>
    </tax-inc:OtherUnderestimationsAssetsUndisclosedReserve>
    <tax-inc:OtherUnderestimationsAssetsUndisclosedReserve contextRef="I-End" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(Reserves/AORAndereActET)"/>
    </tax-inc:OtherUnderestimationsAssetsUndisclosedReserve>
    <tax-inc:OtherOverestimationsLiabilitiesUndisclosedReserve contextRef="I-Start" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(Reserves/AORAnderePassBT)"/>
    </tax-inc:OtherOverestimationsLiabilitiesUndisclosedReserve>
    <tax-inc:OtherOverestimationsLiabilitiesUndisclosedReserve contextRef="I-End" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(Reserves/AORAnderePassET)"/>
    </tax-inc:OtherOverestimationsLiabilitiesUndisclosedReserve>
    <tax-inc:TaxableReserves contextRef="I-Start" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(Reserves/AORSubTotaalBT)"/>
    </tax-inc:TaxableReserves>
    <tax-inc:TaxableReserves contextRef="I-End" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(Reserves/AORSubTotaalET)"/>
    </tax-inc:TaxableReserves>
    <tax-inc:CapitalGainsShares contextRef="I-Start" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(Reserves/AAMRBTRMeerwaardeAandelenBT)"/>
    </tax-inc:CapitalGainsShares>
    <tax-inc:CapitalGainsSharesReversalPreviousExemptionsAdjustmentReservesPlus contextRef="I-Start" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(Reserves/AAMRTerugnemingVUAandelen)"/>
    </tax-inc:CapitalGainsSharesReversalPreviousExemptionsAdjustmentReservesPlus>
    <tax-inc:DefinitiveExemptionTaxShelterAuthorisedAudiovisualWorksAdjustmentReservesPlus contextRef="I-Start" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(Reserves/AAMRTaxShelterBT)"/>
    </tax-inc:DefinitiveExemptionTaxShelterAuthorisedAudiovisualWorksAdjustmentReservesPlus>
    <tax-inc:ExemptionRegionalGrantsCapitalSubsidiesInterestSubsidiesAdjustmentReservesPlus contextRef="I-Start" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(Reserves/AAMRGewestPremiesBT)"/>
    </tax-inc:ExemptionRegionalGrantsCapitalSubsidiesInterestSubsidiesAdjustmentReservesPlus>
    <tax-inc:FinalExemptionProfitHomologationReorganizationPlanAmicableSettlement contextRef="I-Start" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(Reserves/AAMRDefVrijstellingWinst)"/>
    </tax-inc:FinalExemptionProfitHomologationReorganizationPlanAmicableSettlement>
    <tax-inc:OtherAdjustmentsReservesPlus contextRef="I-Start" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(Reserves/AAMRAndereBT)"/>
    </tax-inc:OtherAdjustmentsReservesPlus>
    <tax-inc:AdjustmentsReservesMinus contextRef="I-Start" decimals="INF" unitRef="EUR">
     <xsl:value-of select="helper:ZeroOrValue(Reserves/AAanpassingenMinBTReservesBT)"/>
    </tax-inc:AdjustmentsReservesMinus>
    <tax-inc:TaxableReservesAfterAdjustments contextRef="I-Start" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(Reserves/ASubTotaalNaAanpassingBT)"/>
    </tax-inc:TaxableReservesAfterAdjustments>
    <tax-inc:TaxableReservedProfit contextRef="D" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(Reserves/ATotaalET)"/>
    </tax-inc:TaxableReservedProfit>
    <!--___________________________________-->
    <!--B. VRIJGESTELDE GERESERVEERDE WINST-->
    <!--___________________________________-->
    <tax-inc:ExemptWriteDownDebtClaim contextRef="I-Start" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(VrijgesteldeReserve/BWaardevHandelsvBT)"/>
    </tax-inc:ExemptWriteDownDebtClaim>
    <tax-inc:ExemptWriteDownDebtClaim contextRef="I-End" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(VrijgesteldeReserve/BWaardevHandelsvET)"/>
    </tax-inc:ExemptWriteDownDebtClaim>
    <tax-inc:ExemptProvisionRisksExpenses contextRef="I-Start" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(VrijgesteldeReserve/BVoorzieningenBT)"/>
    </tax-inc:ExemptProvisionRisksExpenses>
    <tax-inc:ExemptProvisionRisksExpenses contextRef="I-End" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(VrijgesteldeReserve/BVoorzieningenET)"/>
    </tax-inc:ExemptProvisionRisksExpenses>
    <tax-inc:UnrealisedExpressedCapitalGainsExemptReserve contextRef="I-Start" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(VrijgesteldeReserve/BNietVerwezMeerwBT)"/>
    </tax-inc:UnrealisedExpressedCapitalGainsExemptReserve>
    <tax-inc:UnrealisedExpressedCapitalGainsExemptReserve contextRef="I-End" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(VrijgesteldeReserve/BNietVerwezMeerwET)"/>
    </tax-inc:UnrealisedExpressedCapitalGainsExemptReserve>
    <!-- Code 305 320 in de aangifte ven B, die komen samen in hetzelfde vak, 
      op te splitsen in eBook-->
          <tax-inc:CapitalGainsSpecificSecuritiesExemptReserve contextRef="I-Start" decimals="INF" unitRef="EUR">
            <xsl:value-of select="helper:ZeroOrValue(VrijgesteldeReserve/BGespreidVerwezMwBT)"/>
          </tax-inc:CapitalGainsSpecificSecuritiesExemptReserve>
          <tax-inc:CapitalGainsSpecificSecuritiesExemptReserve contextRef="I-End" decimals="INF" unitRef="EUR">
            <xsl:value-of select="helper:ZeroOrValue(VrijgesteldeReserve/BGespreidVerwezMwET)"/>
          </tax-inc:CapitalGainsSpecificSecuritiesExemptReserve>
    <!--))
          <tax-inc:CapitalGainsTangibleIntangibleFixedAssetsExemptReserve contextRef="I-Start" decimals="INF" unitRef="EUR">
            <xsl:value-of select="helper:ZeroOrValue(VrijgesteldeReserve/BGespreidVerwezMwBT)"/>
          </tax-inc:CapitalGainsTangibleIntangibleFixedAssetsExemptReserve>
          <tax-inc:CapitalGainsTangibleIntangibleFixedAssetsExemptReserve contextRef="I-End" decimals="INF" unitRef="EUR">
            <xsl:value-of select="helper:ZeroOrValue(VrijgesteldeReserve/BGespreidVerwezMwET)"/>
          </tax-inc:CapitalGainsTangibleIntangibleFixedAssetsExemptReserve>-->
    <!--_____________________________________________________________________________________________________________-->


    <tax-inc:OtherRealisedCapitalGainsExemptReserve contextRef="I-Start" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(VrijgesteldeReserve/BVerwezMeerwBT)"/>
    </tax-inc:OtherRealisedCapitalGainsExemptReserve>
    <tax-inc:OtherRealisedCapitalGainsExemptReserve contextRef="I-End" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(VrijgesteldeReserve/BVerwezMeerwET)"/>
    </tax-inc:OtherRealisedCapitalGainsExemptReserve>
    <tax-inc:CapitalGainsCorporateVehiclesExemptReserve contextRef="I-Start" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(VrijgesteldeReserve/BBedrijfsVoertuigenBT)"/>
    </tax-inc:CapitalGainsCorporateVehiclesExemptReserve>
    <tax-inc:CapitalGainsCorporateVehiclesExemptReserve contextRef="I-End" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(VrijgesteldeReserve/BBedrijfsVoertuigenET)"/>
    </tax-inc:CapitalGainsCorporateVehiclesExemptReserve>
    <tax-inc:CapitalGainsRiverVesselExemptReserve contextRef="I-Start" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(VrijgesteldeReserve/BBinnenschepenBT)"/>
    </tax-inc:CapitalGainsRiverVesselExemptReserve>
    <tax-inc:CapitalGainsRiverVesselExemptReserve contextRef="I-End" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(VrijgesteldeReserve/BBinnenschepenET)"/>
    </tax-inc:CapitalGainsRiverVesselExemptReserve>
    <tax-inc:CapitalGainsSeaVesselExemptReserve contextRef="I-Start" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(VrijgesteldeReserve/BZeeschepenBT)"/>
    </tax-inc:CapitalGainsSeaVesselExemptReserve>
    <tax-inc:CapitalGainsSeaVesselExemptReserve contextRef="I-End" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(VrijgesteldeReserve/BZeeschepenET)"/>
    </tax-inc:CapitalGainsSeaVesselExemptReserve>
   
    <tax-inc:ExemptInvestmentReserve contextRef="I-Start" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(VrijgesteldeReserve/BInvestReserveBT)"/>
    </tax-inc:ExemptInvestmentReserve>
    <tax-inc:ExemptInvestmentReserve contextRef="I-End" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(VrijgesteldeReserve/BInvestReserveET)"/>
    </tax-inc:ExemptInvestmentReserve>
    <tax-inc:TaxShelterAuthorisedAudiovisualWorkExemptReserve contextRef="I-Start" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(VrijgesteldeReserve/BTaxShelterBT)"/>
    </tax-inc:TaxShelterAuthorisedAudiovisualWorkExemptReserve>
    <tax-inc:TaxShelterAuthorisedAudiovisualWorkExemptReserve contextRef="I-End" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(VrijgesteldeReserve/BTaxShelterET)"/>
    </tax-inc:TaxShelterAuthorisedAudiovisualWorkExemptReserve>
    <tax-inc:ProfitHomologationReorganizationPlanAmicableSettlementExemptReserve contextRef="I-Start" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(VrijgesteldeReserve/BWinstHomologatieBT)"/>
    </tax-inc:ProfitHomologationReorganizationPlanAmicableSettlementExemptReserve>
    <tax-inc:ProfitHomologationReorganizationPlanAmicableSettlementExemptReserve contextRef="I-End" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(VrijgesteldeReserve/BWinstHomologatieET)"/>
    </tax-inc:ProfitHomologationReorganizationPlanAmicableSettlementExemptReserve>
    <tax-inc:OtherExemptReserves contextRef="I-Start" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(VrijgesteldeReserve/BAndereBT)"/>
    </tax-inc:OtherExemptReserves>
    <tax-inc:OtherExemptReserves contextRef="I-End" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(VrijgesteldeReserve/BAndereET)"/>
    </tax-inc:OtherExemptReserves>
    <tax-inc:ExemptReservedProfit contextRef="I-Start" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(VrijgesteldeReserve/BTotalenBT)"/>
    </tax-inc:ExemptReservedProfit>
    <tax-inc:ExemptReservedProfit contextRef="I-End" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(VrijgesteldeReserve/BTotalenET)"/>
    </tax-inc:ExemptReservedProfit>
    <!--___________________________________-->
    <!-- II. - VERWORPEN UITGAVEN          -->
    <!--___________________________________-->
    <tax-inc:NonDeductibleTaxes contextRef="D" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(VerworpenUitgaven/VUNietAftrekbareBelasting)"/>
    </tax-inc:NonDeductibleTaxes>
    <tax-inc:NonDeductibleRegionalTaxesDutiesRetributions contextRef="D" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(VerworpenUitgaven/VUGewestBelast)"/>
    </tax-inc:NonDeductibleRegionalTaxesDutiesRetributions>
    <tax-inc:NonDeductibleFinesConfiscationsPenaltiesAllKind contextRef="D" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(VerworpenUitgaven/VUBoetes)"/>
    </tax-inc:NonDeductibleFinesConfiscationsPenaltiesAllKind>
    <tax-inc:NonDeductiblePensionsCapitalEmployerContributionsEmployerPremiums contextRef="D" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(VerworpenUitgaven/VUPensioenen)"/>
    </tax-inc:NonDeductiblePensionsCapitalEmployerContributionsEmployerPremiums>
    <tax-inc:NonDeductibleCarExpensesLossValuesCars contextRef="D" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(VerworpenUitgaven/VUAutokosten)"/>
    </tax-inc:NonDeductibleCarExpensesLossValuesCars>
    <tax-inc:NonDeductibleReceptionBusinessGiftsExpenses contextRef="D" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(VerworpenUitgaven/VUReceptie)"/>
    </tax-inc:NonDeductibleReceptionBusinessGiftsExpenses>
    <tax-inc:NonDeductibleRestaurantExpenses contextRef="D" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(VerworpenUitgaven/VURestaurant)"/>
    </tax-inc:NonDeductibleRestaurantExpenses>
    <tax-inc:NonDeductibleNonSpecificProfessionalClothsExpenses contextRef="D" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(VerworpenUitgaven/VUKledij)"/>
    </tax-inc:NonDeductibleNonSpecificProfessionalClothsExpenses>
    <tax-inc:ExaggeratedInterests contextRef="D" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(VerworpenUitgaven/VUOverdrevenIntresten)"/>
    </tax-inc:ExaggeratedInterests>
    <tax-inc:NonDeductibleParticularPortionInterestsLoans contextRef="D" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(VerworpenUitgaven/VUBepaaldeLeningen)"/>
    </tax-inc:NonDeductibleParticularPortionInterestsLoans>
    <tax-inc:AbnormalBenevolentAdvantages contextRef="D" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(VerworpenUitgaven/VUAbnormaleGoedgVoordelen)"/>
    </tax-inc:AbnormalBenevolentAdvantages>
    <tax-inc:NonDeductibleSocialAdvantages contextRef="D" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(VerworpenUitgaven/VUSocialeVoordelen)"/>
    </tax-inc:NonDeductibleSocialAdvantages>
    <tax-inc:NonDeductibleAdvantagesLuncheonSportsCultureEcoVouchers contextRef="D" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(VerworpenUitgaven/VUMaaltijdSportCultuurEco)"/>
    </tax-inc:NonDeductibleAdvantagesLuncheonSportsCultureEcoVouchers>
    <tax-inc:Liberalities contextRef="D" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(VerworpenUitgaven/VULiberaliteiten)"/>
    </tax-inc:Liberalities>
    <tax-inc:WriteDownsLossValuesShares contextRef="D" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(VerworpenUitgaven/VUAandelen)"/>
    </tax-inc:WriteDownsLossValuesShares>
    <tax-inc:ReversalPreviousExemptions contextRef="D" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(VerworpenUitgaven/VUTerugnemingVrijstellingen)"/>
    </tax-inc:ReversalPreviousExemptions>
    <tax-inc:EmployeeParticipation contextRef="D" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(VerworpenUitgaven/VUWerknemerParticipatie)"/>
    </tax-inc:EmployeeParticipation>
    <tax-inc:IndemnityMissingCoupon contextRef="D" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(VerworpenUitgaven/VUCoupon)"/>
    </tax-inc:IndemnityMissingCoupon>
    <tax-inc:ExpensesTaxShelterAuthorisedAudiovisualWork contextRef="D" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(VerworpenUitgaven/VUTaxShelter)"/>
    </tax-inc:ExpensesTaxShelterAuthorisedAudiovisualWork>
    <tax-inc:RegionalPremiumCapitalSubsidiesInterestSubsidies contextRef="D" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(VerworpenUitgaven/VUGewestPremies)"/>
    </tax-inc:RegionalPremiumCapitalSubsidiesInterestSubsidies>
    <tax-inc:NonDeductiblePaymentsCertainStates contextRef="D" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(VerworpenUitgaven/VUBepaaldeStaten)"/>
    </tax-inc:NonDeductiblePaymentsCertainStates>
    <tax-inc:OtherDisallowedExpenses contextRef="D" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(VerworpenUitgaven/VUAndere)"/>
    </tax-inc:OtherDisallowedExpenses>
    <tax-inc:DisallowedExpenses contextRef="D" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(VerworpenUitgaven/VUTotaal)"/>
    </tax-inc:DisallowedExpenses>
    <!--___________________________________-->
    <!-- III. - UITGEKEERDE DIVIDENDEN     -->
    <!--___________________________________-->
    <tax-inc:OrdinaryDividends contextRef="D" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(UitgekeerdeDividenden/UDGewone)"/>
    </tax-inc:OrdinaryDividends>
    <tax-inc:AcquisitionOwnShares contextRef="D" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(UitgekeerdeDividenden/UDAandelen)"/>
    </tax-inc:AcquisitionOwnShares>
    <tax-inc:DeceaseDepartureExclusionPartner contextRef="D" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(UitgekeerdeDividenden/UDOverlijden)"/>
    </tax-inc:DeceaseDepartureExclusionPartner>
    <tax-inc:DistributionCompanyAssets contextRef="D" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(UitgekeerdeDividenden/UDMaatschappelijk)"/>
    </tax-inc:DistributionCompanyAssets>
    <tax-inc:TaxableDividendsPaid contextRef="D" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(UitgekeerdeDividenden/UDTotaal)"/>
    </tax-inc:TaxableDividendsPaid>
    <!--___________________________________-->
    <!-- IV. - UITEENZETTING VAN DE WINST  -->
    <!--___________________________________-->
    <tax-inc:FiscalResult contextRef="D" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(UiteenzettingWinst/WIResultaat)"/>
    </tax-inc:FiscalResult>
    <tax-inc:ShippingResultTonnageBased contextRef="D" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(UiteenzettingWinst/WIZeeschip)"/>
    </tax-inc:ShippingResultTonnageBased>
    <tax-inc:ShippingResultNotTonnageBased contextRef="D" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(UiteenzettingWinst/WIActiviteiten)"/>
    </tax-inc:ShippingResultNotTonnageBased>
    <tax-inc:DeductionLimitElementsFiscalResult contextRef="D" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(UiteenzettingWinst/WIBestanddelenAftrekBepTotaal)"/>
    </tax-inc:DeductionLimitElementsFiscalResult>
    <tax-inc:BenevolentAbnormalFinancialAdvantagesBenefitsAllKind contextRef="D" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(UiteenzettingWinst/WIBABAbnormaalGoedg)"/>
    </tax-inc:BenevolentAbnormalFinancialAdvantagesBenefitsAllKind>
    <tax-inc:ViolationInvestmentCommitmentIntangibilityConditionInvestmentReserve contextRef="D" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(UiteenzettingWinst/WIBABInvestVerplicht)"/>
    </tax-inc:ViolationInvestmentCommitmentIntangibilityConditionInvestmentReserve>

    <tax-inc:CapitalSubsidiesInterestSubsidiesAgriculturalSupport contextRef="D" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(UiteenzettingWinst/WIBABKapitaal)"/>
    </tax-inc:CapitalSubsidiesInterestSubsidiesAgriculturalSupport>
    <tax-inc:RemainingFiscalResultBeforeOriginDistribution contextRef="D" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(UiteenzettingWinst/WIBResterendResultaat)"/>
    </tax-inc:RemainingFiscalResultBeforeOriginDistribution>
    

    <tax-inc:RemainingFiscalResult contextRef="D__TaxTreatyMember" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(UiteenzettingWinst/WVVOResterendResultaatVV)"/>
    </tax-inc:RemainingFiscalResult>
    <tax-inc:RemainingFiscalResult contextRef="D__NoTaxTreatyMember" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(UiteenzettingWinst/WVVOResterendResultaatNVV)"/>
    </tax-inc:RemainingFiscalResult>
    <tax-inc:RemainingFiscalResult contextRef="D__BelgiumMember" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(UiteenzettingWinst/WVVOResterendResultaatBelg)"/>
    </tax-inc:RemainingFiscalResult>

    <tax-inc:MiscellaneousExemptions contextRef="D__NoTaxTreatyMember" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(UiteenzettingWinst/WARWNietBelastbareBestandNVV)"/>
    </tax-inc:MiscellaneousExemptions>
    <tax-inc:MiscellaneousExemptions contextRef="D__BelgiumMember" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(UiteenzettingWinst/WARWNietBelastbareBestandBelg)"/>
    </tax-inc:MiscellaneousExemptions>
    <tax-inc:PEExemptIncomeMovableAssets contextRef="D__NoTaxTreatyMember" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(UiteenzettingWinst/WARWDefinitiefBelasteInkomstenNVV)"/>
    </tax-inc:PEExemptIncomeMovableAssets>
    <tax-inc:PEExemptIncomeMovableAssets contextRef="D__BelgiumMember" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(UiteenzettingWinst/WARWDefinitiefBelasteInkomstenBelg)"/>
    </tax-inc:PEExemptIncomeMovableAssets>
    <tax-inc:DeductionPatentsIncome contextRef="D__NoTaxTreatyMember" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(UiteenzettingWinst/WARWAftrekOctrooiNVV)"/>
    </tax-inc:DeductionPatentsIncome>
    <tax-inc:DeductionPatentsIncome contextRef="D__BelgiumMember" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(UiteenzettingWinst/WARWAftrekOctrooiBelg)"/>
    </tax-inc:DeductionPatentsIncome>
    <tax-inc:AllowanceCorporateEquity contextRef="D__NoTaxTreatyMember" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(UiteenzettingWinst/WARWAftrekRisicoNVV)"/>
    </tax-inc:AllowanceCorporateEquity>
    <tax-inc:AllowanceCorporateEquity contextRef="D__BelgiumMember" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(UiteenzettingWinst/WARWAftrekRisicoBelg)"/>
    </tax-inc:AllowanceCorporateEquity>
    <tax-inc:CompensatedTaxLosses contextRef="D__NoTaxTreatyMember" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(UiteenzettingWinst/WARWVorigeVerliezenNVV)"/>
    </tax-inc:CompensatedTaxLosses>
    <tax-inc:CompensatedTaxLosses contextRef="D__BelgiumMember" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(UiteenzettingWinst/WARWVorigeVerliezenBelg)"/>
    </tax-inc:CompensatedTaxLosses>
    <tax-inc:AllowanceInvestmentDeduction contextRef="D__BelgiumMember" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(UiteenzettingWinst/WARWInvesteringsAftrekBelg)"/>
    </tax-inc:AllowanceInvestmentDeduction>
    
    <tax-inc:RemainingFiscalProfitCommonRate contextRef="D__NoTaxTreatyMember" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(UiteenzettingWinst/WARWTotaalNVV)"/>
    </tax-inc:RemainingFiscalProfitCommonRate>
    <tax-inc:RemainingFiscalProfitCommonRate contextRef="D__BelgiumMember" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(UiteenzettingWinst/WARWTotaalBelg)"/>
    </tax-inc:RemainingFiscalProfitCommonRate>

    <tax-inc:BasicTaxableAmountCommonRate contextRef="D" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(UiteenzettingWinst/WIBGGewoonTarief)"/>
    </tax-inc:BasicTaxableAmountCommonRate>
    <tax-inc:RemainingFiscalProfitCommonRate contextRef="D" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(UiteenzettingWinst/WIBGResterendWinst)"/>
    </tax-inc:RemainingFiscalProfitCommonRate>
    <tax-inc:ShippingProfitTonnageBased contextRef="D" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(UiteenzettingWinst/WIBGZeescheepvaarTonnage)"/>
    </tax-inc:ShippingProfitTonnageBased>
    <tax-inc:CapitalSubsidiesInterestSubsidiesAgriculturalSupportRate500 contextRef="D" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(UiteenzettingWinst/WIBGKapitaalInterestLandbouw)"/>
    </tax-inc:CapitalSubsidiesInterestSubsidiesAgriculturalSupportRate500>
    
    
    <!--___________________________________-->
    <!--  V. - AFZONDERLIJKE AANSLAGEN     -->
    <!--___________________________________-->
    <tax-inc:UnjustifiedExpensesHiddenSurplusProfitsFinancialAdvantagesBenefitsAllKind contextRef="D" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(AfzonderlijkeAanslagen/AAVoordelenAlleAard)"/>
    </tax-inc:UnjustifiedExpensesHiddenSurplusProfitsFinancialAdvantagesBenefitsAllKind>
    <tax-inc:SeparateAssessmentTaxedReservesCreditAssociationsInstitutionsOtherAuthorisedInstitutions contextRef="D" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(AfzonderlijkeAanslagen/AABelasteReserves)"/>
    </tax-inc:SeparateAssessmentTaxedReservesCreditAssociationsInstitutionsOtherAuthorisedInstitutions>
    <tax-inc:SeparateAssessmentDividendsPaidCreditCorporationTradeEquipmentHousingCorporation contextRef="D" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(AfzonderlijkeAanslagen/AAUitgekeerdeDividenden)"/>
    </tax-inc:SeparateAssessmentDividendsPaidCreditCorporationTradeEquipmentHousingCorporation>
    <tax-inc:SpecialAssessmentPre1990FullPartialDistributionCompanyAssetsRate3300 contextRef="D" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(AfzonderlijkeAanslagen/AABAMaatschVermogen33)"/>
    </tax-inc:SpecialAssessmentPre1990FullPartialDistributionCompanyAssetsRate3300>
    <tax-inc:SpecialAssessmentPre1990FullPartialDistributionCompanyAssetsRate1650 contextRef="D" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(AfzonderlijkeAanslagen/AABAMaatschVermogen165)"/>
    </tax-inc:SpecialAssessmentPre1990FullPartialDistributionCompanyAssetsRate1650>
    <tax-inc:SpecialAssessmentPre1990BenefitsAllKindCompanyLiquidation contextRef="D" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(AfzonderlijkeAanslagen/AABAVoordelenAlleAard)"/>
    </tax-inc:SpecialAssessmentPre1990BenefitsAllKindCompanyLiquidation>
    <!--_________________________________________________________-->
    <!--  VI.  AANVULLENDE HEFFING ERKENDE DIAMANTHANDELAARS     -->
    <!--       EN TERUGBETALING VAN VOORHEEN VERLEEND            -->
    <!--  BELASTINGKREDIET VOOR ONDERZOEK EN ONTWIKKELING        -->
    <!--_________________________________________________________-->
    <tax-inc:AdditionalDutiesDiamondTraders contextRef="D" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(Diamant/DIAanvullingErkend)"/>
    </tax-inc:AdditionalDutiesDiamondTraders>
    <tax-inc:RetributionTaxCreditResearchDevelopment contextRef="D" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(Diamant/DIOnderzoekOntwikkeling)"/>
    </tax-inc:RetributionTaxCreditResearchDevelopment>
    <!--_________________________________________________________-->
    <!--           NIET BELASTBARE BESTANDDELE                   -->
    <!--_________________________________________________________-->
    <xsl:if test="not(not(NietBelastbareBestanddelen/NTEBECharity)) and not(number(NietBelastbareBestanddelen/NTEBECharity)=0)">
    <tax-inc:ExemptGifts contextRef="D" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(NietBelastbareBestanddelen/NTEBECharity)"/>
    </tax-inc:ExemptGifts>
    </xsl:if>
    <tax-inc:ExemptionAdditionalPersonnelMiscellaneousExemptions contextRef="D" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(NietBelastbareBestanddelen/NTEBEPersonnel)"/>
    </tax-inc:ExemptionAdditionalPersonnelMiscellaneousExemptions>
    <tax-inc:ExemptionAdditionalPersonnelSMEs contextRef="D" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(NietBelastbareBestanddelen/NTEBEPersonnelSME)"/>
    </tax-inc:ExemptionAdditionalPersonnelSMEs>
    <tax-inc:ExemptionTrainingPeriodBonus contextRef="D" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(NietBelastbareBestanddelen/NTEBEInternalshipPremium)"/>
    </tax-inc:ExemptionTrainingPeriodBonus>
    <tax-inc:OtherMiscellaneousExemptions contextRef="D" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(NietBelastbareBestanddelen/NTEBEOthers)"/>
    </tax-inc:OtherMiscellaneousExemptions>
    <!--_________________________________________________________-->
    <!--  VII.  DEFINITIEF BELASTE INKOMSTEN EN VRIJGESTELDE     -->
    <!--              ROERENDE INKOMSTEN                         -->
    <!--_________________________________________________________-->
    <tax-inc:NetBelgianIncomeSharesPEExemptIncomeMovableAssets contextRef="D__BelgianBranchMember" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(DBI/DBIAbeNettoBEI)"/>
    </tax-inc:NetBelgianIncomeSharesPEExemptIncomeMovableAssets>
    <tax-inc:NetBelgianIncomeSharesPEExemptIncomeMovableAssets contextRef="D__ForeignBranchMember" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(DBI/DBIAbeNettoBUI)"/>
    </tax-inc:NetBelgianIncomeSharesPEExemptIncomeMovableAssets>
    <tax-inc:WithholdingTaxBelgianIncomeSharesPEExemptIncomeMovableAssets contextRef="D__BelgianBranchMember" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(DBI/DBIAbeRoerendBEI)"/>
    </tax-inc:WithholdingTaxBelgianIncomeSharesPEExemptIncomeMovableAssets>
    <tax-inc:WithholdingTaxBelgianIncomeSharesPEExemptIncomeMovableAssets contextRef="D__ForeignBranchMember" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(DBI/DBIAbeRoerendBUI)"/>
    </tax-inc:WithholdingTaxBelgianIncomeSharesPEExemptIncomeMovableAssets>
    <tax-inc:NetForeignIncomeSharesPEExemptIncomeMovableAssets contextRef="D__BelgianBranchMember" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(DBI/DBIAbuNettoBEI)"/>
    </tax-inc:NetForeignIncomeSharesPEExemptIncomeMovableAssets>
    <tax-inc:NetForeignIncomeSharesPEExemptIncomeMovableAssets contextRef="D__ForeignBranchMember" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(DBI/DBIAbuNettoBUI)"/>
    </tax-inc:NetForeignIncomeSharesPEExemptIncomeMovableAssets>
    <tax-inc:WithholdingTaxForeignIncomeSharesPEExemptIncomeMovableAssets contextRef="D__BelgianBranchMember" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(DBI/DBIAbuRoerendBEI)"/>
    </tax-inc:WithholdingTaxForeignIncomeSharesPEExemptIncomeMovableAssets>
    <tax-inc:WithholdingTaxForeignIncomeSharesPEExemptIncomeMovableAssets contextRef="D__ForeignBranchMember" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(DBI/DBIAbuRoerendBUI)"/>
    </tax-inc:WithholdingTaxForeignIncomeSharesPEExemptIncomeMovableAssets>
    <tax-inc:NetOtherBelgianIncomeSharesPEExemptIncomeMovableAssets contextRef="D__BelgianBranchMember" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(DBI/DBIAAbeNettoBEI)"/>
    </tax-inc:NetOtherBelgianIncomeSharesPEExemptIncomeMovableAssets>
    <tax-inc:NetOtherBelgianIncomeSharesPEExemptIncomeMovableAssets contextRef="D__ForeignBranchMember" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(DBI/DBIAAbeNettoBUI)"/>
    </tax-inc:NetOtherBelgianIncomeSharesPEExemptIncomeMovableAssets>
    <tax-inc:WithholdingTaxOtherBelgianIncomeSharesPEExemptIncomeMovableAssets contextRef="D__BelgianBranchMember" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(DBI/DBIAAbeRoerendBEI)"/>
    </tax-inc:WithholdingTaxOtherBelgianIncomeSharesPEExemptIncomeMovableAssets>
    <tax-inc:WithholdingTaxOtherBelgianIncomeSharesPEExemptIncomeMovableAssets contextRef="D__ForeignBranchMember" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(DBI/DBIAAbeRoerendBUI)"/>
    </tax-inc:WithholdingTaxOtherBelgianIncomeSharesPEExemptIncomeMovableAssets>
    <tax-inc:NetOtherForeignIncomeSharesPEExemptIncomeMovableAssets contextRef="D__BelgianBranchMember" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(DBI/DBIAAbuNettoBEI)"/>
    </tax-inc:NetOtherForeignIncomeSharesPEExemptIncomeMovableAssets>
    <tax-inc:NetOtherForeignIncomeSharesPEExemptIncomeMovableAssets contextRef="D__ForeignBranchMember" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(DBI/DBIAAbuNettoBUI)"/>
    </tax-inc:NetOtherForeignIncomeSharesPEExemptIncomeMovableAssets>
    <tax-inc:WithholdingTaxOtherForeignIncomeSharesPEExemptIncomeMovableAssets contextRef="D__BelgianBranchMember" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(DBI/DBIAAbuRoerendBEI)"/>
    </tax-inc:WithholdingTaxOtherForeignIncomeSharesPEExemptIncomeMovableAssets>
    <tax-inc:WithholdingTaxOtherForeignIncomeSharesPEExemptIncomeMovableAssets contextRef="D__ForeignBranchMember" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(DBI/DBIAAbuRoerendBUI)"/>
    </tax-inc:WithholdingTaxOtherForeignIncomeSharesPEExemptIncomeMovableAssets>
    <tax-inc:OtherExemptIncomeMovableAssets contextRef="D__BelgianBranchMember" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(DBI/DBIVrijgestelRoerendBEI)"/>
    </tax-inc:OtherExemptIncomeMovableAssets>
    <tax-inc:OtherExemptIncomeMovableAssets contextRef="D__ForeignBranchMember" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(DBI/DBIVrijgestelRoerendBUI)"/>
    </tax-inc:OtherExemptIncomeMovableAssets>
    <tax-inc:GrossPEExemptIncomeMovableAssets contextRef="D__BelgianBranchMember" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(DBI/DBISubtotaalBEI)"/>
    </tax-inc:GrossPEExemptIncomeMovableAssets>
    <tax-inc:GrossPEExemptIncomeMovableAssets contextRef="D__ForeignBranchMember" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(DBI/DBISubtotaalBUI)"/>
    </tax-inc:GrossPEExemptIncomeMovableAssets>
    <tax-inc:ExpensesSharesPEExemptIncomeMovableAssets contextRef="D__BelgianBranchMember" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(DBI/DBIKostenBEI)"/>
    </tax-inc:ExpensesSharesPEExemptIncomeMovableAssets>
    <tax-inc:ExpensesSharesPEExemptIncomeMovableAssets contextRef="D__ForeignBranchMember" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(DBI/DBIKostenBUI)"/>
    </tax-inc:ExpensesSharesPEExemptIncomeMovableAssets>
    <tax-inc:NetPEExemptIncomeMovableAssets contextRef="D__BelgianBranchMember" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(DBI/DBIVerschilBEI)"/>
    </tax-inc:NetPEExemptIncomeMovableAssets>
    <tax-inc:NetPEExemptIncomeMovableAssets contextRef="D__ForeignBranchMember" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(DBI/DBIVerschilBUI)"/>
    </tax-inc:NetPEExemptIncomeMovableAssets>
    <tax-inc:IncomeMergerAcquisitionRetainedSharesSimilarRegulationOtherEUMemberState contextRef="D__BelgianBranchMember" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(DBI/DBILidEuBEI)"/>
    </tax-inc:IncomeMergerAcquisitionRetainedSharesSimilarRegulationOtherEUMemberState>
    <tax-inc:IncomeMergerAcquisitionRetainedSharesSimilarRegulationOtherEUMemberState contextRef="D__ForeignBranchMember" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(DBI/DBILidEuBUI)"/>
    </tax-inc:IncomeMergerAcquisitionRetainedSharesSimilarRegulationOtherEUMemberState>
    <tax-inc:ExemptIncomeMovableAssetsRefinancingLoans contextRef="D__BelgianBranchMember" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(DBI/DBIHerfinancBEI)"/>
    </tax-inc:ExemptIncomeMovableAssetsRefinancingLoans>
    <tax-inc:ExemptIncomeMovableAssetsRefinancingLoans contextRef="D__ForeignBranchMember" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(DBI/DBIHerfinancBUI)"/>
    </tax-inc:ExemptIncomeMovableAssetsRefinancingLoans>
    <tax-inc:DeductiblePEExemptIncomeMovableAssets contextRef="D__BelgianBranchMember" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(DBI/DBITotaalBEI)"/>
    </tax-inc:DeductiblePEExemptIncomeMovableAssets>
    <tax-inc:DeductiblePEExemptIncomeMovableAssets contextRef="D__ForeignBranchMember" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(DBI/DBITotaalBUI)"/>
    </tax-inc:DeductiblePEExemptIncomeMovableAssets>
    <!--_________________________________________________________-->
    <!--   OVERDRACHT AFTREK DEFINITIEVE BELASTE INKOMSTEN       -->
    <!--_________________________________________________________-->
    <tax-inc:AccumulatedPEExemptIncomeMovableAssets contextRef="D" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(OverdrachtDBIAftrek/ODASaldoOverdraagbaarVolgend)"/>
    </tax-inc:AccumulatedPEExemptIncomeMovableAssets>
    <tax-inc:CarryOverNextTaxPeriodInsufficientProfitsPEExemptIncomeMovableAssetsCurrentTaxPeriod contextRef="D" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(OverdrachtDBIAftrek/ODABedragHuidigNaarVolgend)"/>
    </tax-inc:CarryOverNextTaxPeriodInsufficientProfitsPEExemptIncomeMovableAssetsCurrentTaxPeriod>
    <tax-inc:DeductionPEExemptIncomeMovableAssetsPreviousTaxPeriod contextRef="D" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(OverdrachtDBIAftrek/ODABedragSub1Werkelijk)"/>
    </tax-inc:DeductionPEExemptIncomeMovableAssetsPreviousTaxPeriod>
    <tax-inc:CarryOverNextTaxPeriodPEExemptIncomeMovableAssets contextRef="D" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(OverdrachtDBIAftrek/ODASaldoOvergedragen)"/>
    </tax-inc:CarryOverNextTaxPeriodPEExemptIncomeMovableAssets>
    
    <!--_________________________________________________________-->
    <!--   OVERDRACHT AFTREK VOOR RISICOKAPITAAL                 -->
    <!--_________________________________________________________-->
    <tax-inc:AccumulatedAllowanceCorporateEquity contextRef="D" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(OverdrachtNID/ONIDSaldoOvergedragen)"/>
    </tax-inc:AccumulatedAllowanceCorporateEquity>
    <tax-inc:CarryOverNextTaxPeriodInsufficientProfitsAllowanceCorporateEquity contextRef="D" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(OverdrachtNID/ONIDOverdraagbaarVolgend)"/>
    </tax-inc:CarryOverNextTaxPeriodInsufficientProfitsAllowanceCorporateEquity>
    <!--_________________________________________________________-->
    <!--  X. - COMPENSEERBARE VERLIEZEN                          -->
    <!--_________________________________________________________-->
    <tax-inc:CompensableTaxLosses contextRef="D" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(CompenseerbareVerliezen/CVSaldoVorigeVerliezen)"/>
    </tax-inc:CompensableTaxLosses>
    <tax-inc:CompensatedTaxLosses contextRef="D" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(CompenseerbareVerliezen/CVGecompenseerdeVerliezen)"/>
    </tax-inc:CompensatedTaxLosses>
    <tax-inc:LossCurrentTaxPeriod contextRef="D" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(CompenseerbareVerliezen/CVVerliesBelastbaarTijdperk)"/>
    </tax-inc:LossCurrentTaxPeriod>
    <tax-inc:CarryOverTaxLosses contextRef="D" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(CompenseerbareVerliezen/CVOverTeBrengen)"/>
    </tax-inc:CarryOverTaxLosses>
    <!--_________________________________________________________-->
    <!--             TARIEF VAN DE BELASTINGEN                   -->
    <!--_________________________________________________________-->
    <tax-inc:ExclusionReducedRate contextRef="D">
      <xsl:value-of select="TariefBelasting/TARUitgeslotenVerminderd"/>
    </tax-inc:ExclusionReducedRate>
    <tax-inc:ExitTaxRate contextRef="D">
      <xsl:value-of select="TariefBelasting/TAROnderworpen"/>
    </tax-inc:ExitTaxRate>
    <!--_________________________________________________________-->
    <!--             XII. - VOORAFBETALINGEN                     -->
    <!--_________________________________________________________-->
    <tax-inc:FirstThreeAccountingYearsSmallCompanyCorporationCode contextRef="D">
      <xsl:value-of select="VoorafBetalingen/VBJongVennootschap"/>
    </tax-inc:FirstThreeAccountingYearsSmallCompanyCorporationCode>
    <tax-inc:Prepayments contextRef="D" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(VoorafBetalingen/VBTotaal)"/>
    </tax-inc:Prepayments>
    <tax-inc:PrepaymentFirstQuarter contextRef="D" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(VoorafBetalingen/VBEersteKwartaal)"/>
    </tax-inc:PrepaymentFirstQuarter>
    <tax-inc:PrepaymentSecondQuarter contextRef="D" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(VoorafBetalingen/VBTweedeKwartaal)"/>
    </tax-inc:PrepaymentSecondQuarter>
    <tax-inc:PrepaymentThirdQuarter contextRef="D" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(VoorafBetalingen/VBDerdeKwartaal)"/>
    </tax-inc:PrepaymentThirdQuarter>
    <tax-inc:PrepaymentFourthQuarter contextRef="D" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(VoorafBetalingen/VBVierdeKwartaal)"/>
    </tax-inc:PrepaymentFourthQuarter>
    <xsl:if test="helper:ZeroOrValue(VoorafBetalingen/VBEersteKwartaal) > 0 and VoorafBetalingen/VBEersteKwartaalReferentie">
      <tax-inc:PrepaymentReferenceNumberNotEntityIdentifierFirstOccurrence contextRef="D" decimals="INF" unitRef="U-Pure">
        <xsl:value-of select="VoorafBetalingen/VBEersteKwartaalReferentie"/>
      </tax-inc:PrepaymentReferenceNumberNotEntityIdentifierFirstOccurrence>
    </xsl:if>
    <xsl:if test="helper:ZeroOrValue(VoorafBetalingen/VBTweedeKwartaal) > 0 and VoorafBetalingen/VBTweedeKwartaalReferentie">
      <tax-inc:PrepaymentReferenceNumberNotEntityIdentifierSecondOccurrence contextRef="D" decimals="INF" unitRef="U-Pure">
        <xsl:value-of select="VoorafBetalingen/VBTweedeKwartaalReferentie"/>
      </tax-inc:PrepaymentReferenceNumberNotEntityIdentifierSecondOccurrence>
    </xsl:if>
    <xsl:if test="helper:ZeroOrValue(VoorafBetalingen/VBDerdeKwartaal) > 0 and VoorafBetalingen/VBDerdeKwartaalReferentie">
      <tax-inc:PrepaymentReferenceNumberNotEntityIdentifierThirdOccurrence contextRef="D" decimals="INF" unitRef="U-Pure">
        <xsl:value-of select="VoorafBetalingen/VBDerdeKwartaalReferentie"/>
      </tax-inc:PrepaymentReferenceNumberNotEntityIdentifierThirdOccurrence>
    </xsl:if>
    <xsl:if test="helper:ZeroOrValue(VoorafBetalingen/VBVierdeKwartaal) > 0 and VoorafBetalingen/VBVierdeKwartaalReferentie">
      <tax-inc:PrepaymentReferenceNumberNotEntityIdentifierFourthOccurrence contextRef="D" decimals="INF" unitRef="U-Pure">
        <xsl:value-of select="VoorafBetalingen/VBVierdeKwartaalReferentie"/>
      </tax-inc:PrepaymentReferenceNumberNotEntityIdentifierFourthOccurrence>
    </xsl:if>
    <!--_________________________________________________________-->
    <!--             VERREKENBAREVOORHEFFING                     -->
    <!--_________________________________________________________-->
    <tax-inc:NonRepayableAdvanceLevies contextRef="D" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(VerrekenbareVoorheffing/VVNTTotaal)"/>
    </tax-inc:NonRepayableAdvanceLevies>
    <tax-inc:NonRepayableFictiousWitholdingTax contextRef="D" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(VerrekenbareVoorheffing/VVNTFictieveRoerendeVoorheffing)"/>
    </tax-inc:NonRepayableFictiousWitholdingTax>
    <tax-inc:NonRepayableLumpSumForeignTaxes contextRef="D" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(VerrekenbareVoorheffing/VVNTForfaitairBuitenlands)"/>
    </tax-inc:NonRepayableLumpSumForeignTaxes>
    <tax-inc:TaxCreditResearchDevelopment contextRef="D" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(VerrekenbareVoorheffing/VVNTBelastingKredietOnderzoekOntw)"/>
    </tax-inc:TaxCreditResearchDevelopment>
    <tax-inc:RepayableAdvanceLevies contextRef="D" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(VerrekenbareVoorheffing/VVTTotaal)"/>
    </tax-inc:RepayableAdvanceLevies>
    <tax-inc:RepayableRealFictitiousWithholdingTaxOtherSharesPEExemptIncomeMovableAssetsBelgium contextRef="D" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(VerrekenbareVoorheffing/VVTBelgisch)"/>
    </tax-inc:RepayableRealFictitiousWithholdingTaxOtherSharesPEExemptIncomeMovableAssetsBelgium>
    <tax-inc:RepayableWithholdingTaxPEExemptionLiquidationBonusesBonusesAcquisitionOwnSharesBelgiumForeign contextRef="D" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(VerrekenbareVoorheffing/VVTEigenBuitenlands)"/>
    </tax-inc:RepayableWithholdingTaxPEExemptionLiquidationBonusesBonusesAcquisitionOwnSharesBelgiumForeign>
    <tax-inc:RepayableWithholdingTaxOtherPEForeign contextRef="D" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(VerrekenbareVoorheffing/VVTAndereBuitenlands)"/>
    </tax-inc:RepayableWithholdingTaxOtherPEForeign>
    <tax-inc:RepayableWithholdingTaxOtherLiquidationBonusesBonusesAcquisitionOwnShares contextRef="D" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(VerrekenbareVoorheffing/VVTAndereEigen)"/>
    </tax-inc:RepayableWithholdingTaxOtherLiquidationBonusesBonusesAcquisitionOwnShares>
    <tax-inc:RepayableWithholdingTaxOtherDividends contextRef="D" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(VerrekenbareVoorheffing/VVTAndereRoerendeVoorheffing)"/>
    </tax-inc:RepayableWithholdingTaxOtherDividends>
    <tax-inc:OtherRepayableWithholdingTaxes contextRef="D" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(VerrekenbareVoorheffing/VVTAndere)"/>
    </tax-inc:OtherRepayableWithholdingTaxes>
    <tax-inc:TaxCreditResearchDevelopmentClearablePreviousTaxPeriodsRepayableCurrentAssessmentYear contextRef="D" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(VerrekenbareVoorheffing/VBelastingkredietHuiidTijdperk)"/>
    </tax-inc:TaxCreditResearchDevelopmentClearablePreviousTaxPeriodsRepayableCurrentAssessmentYear>
    <!--_________________________________________________________-->
    <!--                   TAX SHELTER                           -->
    <!--_________________________________________________________-->
    <tax-inc:BelgianCorporationAudiovisualWorksTaxShelterAgreement contextRef="D">
      <xsl:value-of select="TaxShelter/TASTaxShelter"/>
    </tax-inc:BelgianCorporationAudiovisualWorksTaxShelterAgreement>
    <!--_________________________________________________________-->
    <!--                   GROOTE VAN DE VENNOOTSCHAP IN DE      -->
    <!--            ZIN VAN HET WETBOEK VAN DE VENNOOTSCHAPPEN   -->
    <!--  !! VERPLICHT !!                                        -->
    <!--_________________________________________________________-->
    <tax-inc:AssociatedCompanyCorporationCodeCurrentTaxPeriod contextRef="D">
      <xsl:value-of select="helper:ZeroOrValue(GrootteVennootschap/GVBTVerbonden)"/>
    </tax-inc:AssociatedCompanyCorporationCodeCurrentTaxPeriod>
    <tax-inc:AnnualWorkForceAverageCorporationCodeCurrentTaxPeriod contextRef="D" decimals="INF" unitRef="U-Pure">
      <xsl:value-of select="helper:NullOrValue(GrootteVennootschap/GVBTJaargemiddelde)"/>
    </tax-inc:AnnualWorkForceAverageCorporationCodeCurrentTaxPeriod>
    <tax-inc:AnnualTurnoverExcludingVATCorporationCodeCurrentTaxPeriod contextRef="D" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(GrootteVennootschap/GVBTJaarOmzet)"/>
    </tax-inc:AnnualTurnoverExcludingVATCorporationCodeCurrentTaxPeriod>
    <tax-inc:BalanceSheetTotalCorporationCodeCurrentTaxPeriod contextRef="D" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(GrootteVennootschap/GVBTBalansTotaal)"/>
    </tax-inc:BalanceSheetTotalCorporationCodeCurrentTaxPeriod>
    <tax-inc:ComplianceSmallCompanyCorporationCodePreviousTaxPeriod contextRef="D">
      <xsl:value-of select="GrootteVennootschap/GVCGLaatsteTijdperk"/>
    </tax-inc:ComplianceSmallCompanyCorporationCodePreviousTaxPeriod>
    <tax-inc:ComplianceSmallCompanyCorporationCodeLastButOneTaxPeriod contextRef="D">
      <xsl:value-of select="GrootteVennootschap/GVCGVoorLaatsteTijdperk"/>
    </tax-inc:ComplianceSmallCompanyCorporationCodeLastButOneTaxPeriod>


    
   
  </xsl:template>

  <xsl:template match="Bijlagen" mode="BIJLAGEN">
    <!--_________________________________________________________-->
    <!--              DIVERSE BESCHEIDEN EN OPGAVEN              -->
    <!--_________________________________________________________-->
    <!--Jaarekening-->
    <xsl:if test="nbb and not(nbb='')">
      <tax-inc:StatutoryAccounts contextRef="D">
        <xsl:value-of select="nbb"/>
      </tax-inc:StatutoryAccounts>
    </xsl:if>
    <!--Verslagen en besluiten-->
    <xsl:choose>
      <xsl:when test="rep and not(rep='')">
        <tax-inc:GeneralMeetingMinutesDecisions contextRef="D">
          <xsl:value-of select="rep"/>
        </tax-inc:GeneralMeetingMinutesDecisions>
      </xsl:when>
      <xsl:otherwise>
        <tax-inc:GeneralMeetingMinutesDecisions contextRef="D">null</tax-inc:GeneralMeetingMinutesDecisions>
      </xsl:otherwise>
    </xsl:choose>
    
    <!--vrijstelling van de winst voortvloeiend uit de homologatie van een reorganisatieplan en uit de vaststelling van een minnelijk akkoord-->
    <xsl:if test="hom and not(hom='')">
      <tax-inc:ExemptionProfitHomologationReorganizationPlanAmicableSettlementDocuments contextRef="D">
        <xsl:value-of select="hom"/>
      </tax-inc:ExemptionProfitHomologationReorganizationPlanAmicableSettlementDocuments>
    </xsl:if>
    <!--Interne Jaarekening-->
    <xsl:if test="intacc and not(intacc='')">
      <tax-inc:InternalStatutoryAccounts contextRef="D">
        <xsl:value-of select="intacc"/>
      </tax-inc:InternalStatutoryAccounts>
    </xsl:if>
    <!--Afschrijvingstabel niet gestandaardiseerd-->
    <xsl:if test="afshr and not(afshr='')">
      <tax-inc:DepreciationTableNonStructured contextRef="D">
        <xsl:value-of select="afshr"/>
      </tax-inc:DepreciationTableNonStructured>
  </xsl:if>
    <!-- andere bijlagen -->
    <xsl:if test="other and not(other='')">
      <tax-inc:OtherDocuments contextRef="D">
        <xsl:value-of select="other"/>
      </tax-inc:OtherDocuments>
    </xsl:if>
    <!--_________________________________________________________-->
  </xsl:template>
  
  
  <xsl:template name="ValuedNode">
    <xsl:param name="BaseNodeName"/>
    <xsl:param name="Value"/>
    <xsl:variable name="element" select="concat($BaseNodeName,$Value)"/>
    <xsl:element name="{$element}">
      <xsl:attribute name="contextRef">D</xsl:attribute>
      <xsl:value-of select="$Value"/>
    </xsl:element>
  </xsl:template>

  <xsl:template match="Fiche|Form275C" mode="_275C">
    <tax-inc:Equity contextRef="I-Start" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(EigenVermogen)"/>
    </tax-inc:Equity>
    <tax-inc:DeductionsEquityAllowanceCorporateEquity contextRef="I-Start" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(Totaal)"/>
    </tax-inc:DeductionsEquityAllowanceCorporateEquity>
    <tax-inc:OwnSharesFiscalValue contextRef="I-Start" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(EigenAandelen)"/>
      </tax-inc:OwnSharesFiscalValue>
    <tax-inc:FinancialFixedAssetsParticipationsOtherShares contextRef="I-Start" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(FinancVasteActiva)"/>
      </tax-inc:FinancialFixedAssetsParticipationsOtherShares>
    <tax-inc:SharesInvestmentCorporations contextRef="I-Start" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(AandelenBelegging)"/>
    </tax-inc:SharesInvestmentCorporations>
    <tax-inc:BranchesCountryTaxTreaty contextRef="I-Start" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(InrichtingVerdrag)"/>
    </tax-inc:BranchesCountryTaxTreaty>
    <tax-inc:ImmovablePropertyCountryTaxTreaty contextRef="I-Start" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(OnroerendVerdrag)"/>
    </tax-inc:ImmovablePropertyCountryTaxTreaty>
    <tax-inc:TangibleFixedAssetsUnreasonableRelatedCosts contextRef="I-Start" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(MvaOnredelijk)"/>
    </tax-inc:TangibleFixedAssetsUnreasonableRelatedCosts>
    <tax-inc:InvestmentsNoPeriodicalIncome contextRef="I-Start" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(BestanddelenBelegging)"/>
    </tax-inc:InvestmentsNoPeriodicalIncome>
    <tax-inc:ImmovablePropertyUseManager contextRef="I-Start" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(OnroerendeBedrijfsleiders)"/>
    </tax-inc:ImmovablePropertyUseManager>
    <tax-inc:UnrealisedExpressedCapitalGains contextRef="I-Start" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(UitgedrukteMeerwaarden)"/>
      </tax-inc:UnrealisedExpressedCapitalGains>
    <tax-inc:TaxCreditResearchDevelopmentDeductionEquityAllowanceCorporateEquity contextRef="I-Start" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(Belastingkrediet)"/>
    </tax-inc:TaxCreditResearchDevelopmentDeductionEquityAllowanceCorporateEquity>
    <tax-inc:InvestmentGrants contextRef="I-Start" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(KapitaalSubsidies)"/>
    </tax-inc:InvestmentGrants>
    <tax-inc:ActualisationStockRecognisedDiamondTraders contextRef="I-Start" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(Voorraadactualisering)"/>
    </tax-inc:ActualisationStockRecognisedDiamondTraders>
   <!-- Hoofdzetel ??? <xsl:value-of select="helper:ZeroOrValue(Hoofdzetel)"/>-->
    <tax-inc:BorrowedFundsMainEstablishmentInterestChargedTaxableResultBelgianBranch contextRef="I-Start" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(BelgischeInrichting)"/>
    </tax-inc:BorrowedFundsMainEstablishmentInterestChargedTaxableResultBelgianBranch>


    <tax-inc:MovementEquityAfterDeductionsAllowanceCorporateEquity contextRef="D" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(PositieveWijzigingen)+helper:ZeroOrValue(NegatieveWijzigingen)"/>
      </tax-inc:MovementEquityAfterDeductionsAllowanceCorporateEquity>
    <tax-inc:AllowanceCorporateEquityCurrentTaxPeriod contextRef="D" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(RisicoKapitaalBelastbaar)"/>
    </tax-inc:AllowanceCorporateEquityCurrentTaxPeriod>
    <tax-inc:DeductibleAllowanceCorporateEquityCurrentAssessmentYear contextRef="D" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(Aftrekbaar)"/>
    </tax-inc:DeductibleAllowanceCorporateEquityCurrentAssessmentYear>
    <tax-inc:DeductionAllowanceCorporateEquityCurrentAssessmentYear contextRef="D" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(WerkelijkAfgetrokken)"/>
    </tax-inc:DeductionAllowanceCorporateEquityCurrentAssessmentYear>
    <tax-inc:CarryOverCurrentAssessmentYearInsufficientProfitsAllowanceCorporateEquity contextRef="D" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(OverdraagbaarSaldo)"/>
    </tax-inc:CarryOverCurrentAssessmentYearInsufficientProfitsAllowanceCorporateEquity>

    <tax-inc:AllowanceCorporateEquityPreviousAssessmentYears contextRef="D" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(SaldoVorigeAJGevormd)"/>
    </tax-inc:AllowanceCorporateEquityPreviousAssessmentYears>
    <tax-inc:DeductionAllowanceCorporateEquityPreviousAssessmentYears contextRef="D" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(WerkelijkVorigAJ)"/>
    </tax-inc:DeductionAllowanceCorporateEquityPreviousAssessmentYears>
    <tax-inc:AllowanceCorporateEquity contextRef="D" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(TotaalAftrekAJ)"/>
    </tax-inc:AllowanceCorporateEquity>
    <tax-inc:CarryOverInsufficientProfitsAllowanceCorporateEquity contextRef="D" decimals="INF" unitRef="EUR">
      <xsl:value-of select="helper:ZeroOrValue(SaldoRisicoOverdraagbaarForm)"/>
    </tax-inc:CarryOverInsufficientProfitsAllowanceCorporateEquity>
   
</xsl:template>

  <!-- 
  DETAIL VERWORPEN UITGAVEN
  =========================
  -->
  <xsl:template name="GetVerworpenUitgavenId">
    <xsl:param name="item"/>
    
  </xsl:template>
  
  <xsl:template match="*" mode="CONTEXT_DETAIL_VERWORPEN_UITGAVEN">
    
    <context id="D__id__123000__id__Omschrijving1">
      <entity>
        <identifier scheme="http://www.fgov.be">BE0123456789</identifier> <!-- BTW nummer aangifte?-->
      </entity>
      <period>
        <startDate>2010-01-01</startDate> <!-- Periode aanslag?-->
        <endDate>2010-12-31</endDate> <!-- Periode aanslag?-->
      </period>
      <scenario>
        <xbrldi:typedMember dimension="d-ty:NatureTypedDimension">
          <d-ty:NatureTypedID>123000</d-ty:NatureTypedID>
        </xbrldi:typedMember>
        <xbrldi:typedMember dimension="d-ty:DescriptionTypedDimension">
          <d-ty:DescriptionTypedID>Omschrijving1</d-ty:DescriptionTypedID>
        </xbrldi:typedMember>
      </scenario>
    </context>
  </xsl:template>

  <!-- 
  Dubieuze debiteuren
  ====================
  types:
     Ongewijzigd tijdens het boekjaar: d-ec:UnalteredBalanceWriteDownDebtClaimMember
     Verhoogd tijdens het boekjaar: d-ec:RemainingBalanceAfterIncreaseWriteDownDebtClaimMember
     Verminderd tijdens het boekjaar: d-ec:RemainingBalanceAfterDecreaseWriteDownDebtClaimMember
     Nieuw tijdens het boekjaar: d-ec:NewWriteDownDebtClaimMember
     
     DubieuzeDebiteuren/ToestandEindHuidigBoekjaar
     
     AlgemeneProvisie
     Klant
     FactuurJaar
     
     isNew
     
     
     BelastbaarOfVrijgesteld
     
     
     ChangedID
     
     RedenTotVermindering
     
     
     
     Aanslagjaar+volgnummer : (FactuurJaar)+volgnr
     Naam schuldenaar       : (businessrelation)
     Adres schuldenaar      :(businessrelation)
     Begin boekjaar         : VorderingStart
     Einde boekjaar         : VorderingEinde
     
     
     
     WaardeverminderingTotaal
     
    -->

  <xsl:template name="GetDubieuzeDebiteurType"><xsl:param name="item"/><xsl:choose><xsl:when test="$item/isNew='true'">NewWriteDownDebtClaimMember</xsl:when><xsl:when test="$item/ChangedID and not($item/ChangedID/text()='')"><xsl:choose><xsl:when test="Waardevermindering and number(Waardevermindering)&lt;0">RemainingBalanceAfterDecreaseWriteDownDebtClaimMember</xsl:when><xsl:otherwise>RemainingBalanceAfterIncreaseWriteDownDebtClaimMember</xsl:otherwise></xsl:choose></xsl:when><xsl:otherwise>UnalteredBalanceWriteDownDebtClaimMember</xsl:otherwise></xsl:choose></xsl:template>


  <xsl:template name="GetDubieuzeDebiteurID"><xsl:param name="item"/><xsl:param name="prequelType"/><xsl:param name="year"/><xsl:param name="nr"/><xsl:param name="ExemptionCategoryDimension"/><xsl:choose><xsl:when test="$prequelType='START'">I-Start</xsl:when><xsl:when test="$prequelType='END'">I-End</xsl:when><xsl:otherwise>D</xsl:otherwise></xsl:choose>__id__<xsl:value-of select="$year"/>-<xsl:value-of select="$nr"/>__<xsl:value-of select="$ExemptionCategoryDimension"/></xsl:template>



  <xsl:template match="DetailDubDeb" mode="DubieuzeDebiteurenManualDetail">
    <xsl:variable name="ExemptionCategoryDimension">
      <xsl:call-template name="GetDubieuzeDebiteurType">
        <xsl:with-param name="item" select="."/>
      </xsl:call-template>
    </xsl:variable>

    <xsl:variable name="year" select="Aanslagjaar"/>

    <xsl:variable name ="yearPos">
      <xsl:value-of select="$year"/>-<xsl:value-of select="VolgNr"/>
    </xsl:variable>

    <xsl:variable name="startid">
      <xsl:call-template name="GetDubieuzeDebiteurID">
        <xsl:with-param name="item" select="."/>
        <xsl:with-param name="prequelType" select="'START'"/>
        <xsl:with-param name="year" select="$year"/>
        <xsl:with-param name="nr" select="VolgNr"/>
        <xsl:with-param name="ExemptionCategoryDimension" select="Categorie"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="endid">
      <xsl:call-template name="GetDubieuzeDebiteurID">
        <xsl:with-param name="item" select="."/>
        <xsl:with-param name="prequelType" select="'END'"/>
        <xsl:with-param name="year" select="$year"/>
        <xsl:with-param name="nr" select="VolgNr"/>
        <xsl:with-param name="ExemptionCategoryDimension" select="Categorie"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="did">
      <xsl:call-template name="GetDubieuzeDebiteurID">
        <xsl:with-param name="item" select="."/>
        <xsl:with-param name="prequelType" select="'D'"/>
        <xsl:with-param name="year" select="$year"/>
        <xsl:with-param name="nr" select="VolgNr"/>
        <xsl:with-param name="ExemptionCategoryDimension" select="Categorie"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:call-template name="CreateContextDubDeb">
      <xsl:with-param name="xbrlParent" select="//XbrlInput"/>
      <xsl:with-param name="Id" select="$did"/>
      <xsl:with-param name="yearPos" select="$yearPos"/>
      <xsl:with-param name="ExemptionCategoryDimension" select="Categorie"/>
    </xsl:call-template>
    <xsl:call-template name="CreateContextDubDeb">
      <xsl:with-param name="xbrlParent" select="//XbrlInput"/>
      <xsl:with-param name="Id" select="$endid"/>
      <xsl:with-param name="yearPos" select="$yearPos"/>
      <xsl:with-param name="ExemptionCategoryDimension" select="Categorie"/>
    </xsl:call-template>
    <xsl:if test="not(Categorie='NewWriteDownDebtClaimMember')">
      <xsl:call-template name="CreateContextDubDeb">
        <xsl:with-param name="xbrlParent" select="//XbrlInput"/>
        <xsl:with-param name="Id" select="$startid"/>
        <xsl:with-param name="yearPos" select="$yearPos"/>
        <xsl:with-param name="ExemptionCategoryDimension" select="Categorie"/>
      </xsl:call-template>
    </xsl:if>

    <tax-inc:IdentityTradeDebtor contextRef="{$did}"><xsl:value-of select="Naam"/></tax-inc:IdentityTradeDebtor>
    <tax-inc:AddressTradeDebtor contextRef="{$did}"><xsl:value-of select="Adres"/></tax-inc:AddressTradeDebtor>

    <xsl:if test="not(Categorie='NewWriteDownDebtClaimMember')">
      <tax-inc:DebtClaim unitRef="EUR" decimals="INF" contextRef="{$startid}"><xsl:value-of select="SchuldVorderingStart"/></tax-inc:DebtClaim>
      <tax-inc:ExemptWriteDownDebtClaim unitRef="EUR" decimals="INF" contextRef="{$startid}"><xsl:value-of select="WaardeverminderingStart"/></tax-inc:ExemptWriteDownDebtClaim>
    </xsl:if>
    <tax-inc:DebtClaim unitRef="EUR" decimals="INF" contextRef="{$endid}"><xsl:value-of select="SchuldVorderingEinde"/></tax-inc:DebtClaim>
    <tax-inc:ExemptWriteDownDebtClaim unitRef="EUR" decimals="INF" contextRef="{$endid}"><xsl:value-of select="WaardeverminderingEinde"/></tax-inc:ExemptWriteDownDebtClaim>

    <xsl:if test="not(Categorie='NewProvisionRisksExpensesMember')">
      <xsl:if test="WVDefinitief and not(helper:ZeroOrValue(WVDefinitief)=0)">
        <tax-inc:DecreaseExemptWriteDownDebtClaimFinalWriteDownCurrentTaxPeriod unitRef="EUR" decimals="INF" contextRef="{$did}"><xsl:value-of select="WVDefinitief"/></tax-inc:DecreaseExemptWriteDownDebtClaimFinalWriteDownCurrentTaxPeriod>
      </xsl:if>
      <xsl:if test="WVInning and not(helper:ZeroOrValue(WVInning)=0)">
         <tax-inc:DecreaseExemptWriteDownDebtClaimFullPartialCollectionCurrentTaxPeriod unitRef="EUR" decimals="INF" contextRef="{$did}"><xsl:value-of select="WVInning"/></tax-inc:DecreaseExemptWriteDownDebtClaimFullPartialCollectionCurrentTaxPeriod>
      </xsl:if>
      <xsl:if test="WVNieuwSchatting and not(helper:ZeroOrValue(WVNieuwSchatting)=0)">
        <tax-inc:DecreaseExemptWriteDownDebtClaimNewEstimationProbableLoss unitRef="EUR" decimals="INF" contextRef="{$did}"><xsl:value-of select="WVNieuwSchatting"/></tax-inc:DecreaseExemptWriteDownDebtClaimNewEstimationProbableLoss>
      </xsl:if>

      <xsl:if test="WVVerhoging and not(helper:ZeroOrValue(WVVerhoging)=0)">
        <tax-inc:IncreaseExemptWriteDownDebtClaim unitRef="EUR" decimals="INF" contextRef="{$did}"><xsl:value-of select="WVVerhoging"/></tax-inc:IncreaseExemptWriteDownDebtClaim>
      </xsl:if>
    </xsl:if>
    <xsl:if test="Verantwoording and not(Verantwoording='')">
      <tax-inc:JustificationExemptWriteDown contextRef="{$did}">
        <xsl:value-of select="Verantwoording"/>
      </tax-inc:JustificationExemptWriteDown>
    </xsl:if>
  </xsl:template>
  
  <xsl:template match="DubieuzeDebiteuren" mode="DubieuzeDebiteurenDetail">
    <xsl:variable name="xbrlParent" select="//XbrlInput"/>
    <xsl:if test="count(ToestandEindHuidigBoekjaar[BelastbaarOfVrijgesteld='Vrijgesteld'])>0">
      <xsl:for-each select="ToestandEindHuidigBoekjaar[BelastbaarOfVrijgesteld='Vrijgesteld']">
        <xsl:variable name="ExemptionCategoryDimension">
          <xsl:call-template name="GetDubieuzeDebiteurType">
            <xsl:with-param name="item" select="."/>
          </xsl:call-template>
        </xsl:variable>

        <xsl:variable name="year">
          <xsl:choose>
            <xsl:when test="isNew='true'">
              <xsl:value-of select="$xbrlParent/Assessment"/>
            </xsl:when>
            <xsl:when test="FactuurJaar and not(FactuurJaar='')">
              <xsl:choose>
                <xsl:when test="number(FactuurJaar)='NaN' or (number(FactuurJaar)+1)=number($xbrlParent/Assessment) or number(FactuurJaar)=0">
                  <xsl:value-of select="number($xbrlParent/Assessment)-1"/>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:value-of select="number(FactuurJaar)+1"/>
                </xsl:otherwise>
              </xsl:choose>
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="number($xbrlParent/Assessment)-1"/>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:variable>
        <xsl:variable name ="yearPos"><xsl:value-of select="$year"/>-<xsl:value-of select="position()"/></xsl:variable>
        
        <xsl:variable name="startid">
          <xsl:call-template name="GetDubieuzeDebiteurID">
            <xsl:with-param name="item" select="."/>
            <xsl:with-param name="prequelType" select="'START'"/>
            <xsl:with-param name="year" select="$year"/>
            <xsl:with-param name="nr" select="position()"/>
            <xsl:with-param name="ExemptionCategoryDimension" select="$ExemptionCategoryDimension"/>
          </xsl:call-template>
        </xsl:variable>
        <xsl:variable name="endid">
          <xsl:call-template name="GetDubieuzeDebiteurID">
            <xsl:with-param name="item" select="."/>
            <xsl:with-param name="prequelType" select="'END'"/>
            <xsl:with-param name="year" select="$year"/>
            <xsl:with-param name="nr" select="position()"/>
            <xsl:with-param name="ExemptionCategoryDimension" select="$ExemptionCategoryDimension"/>
          </xsl:call-template>
        </xsl:variable>
        <xsl:variable name="did">
          <xsl:call-template name="GetDubieuzeDebiteurID">
            <xsl:with-param name="item" select="."/>
            <xsl:with-param name="prequelType" select="'D'"/>
            <xsl:with-param name="year" select="$year"/>
            <xsl:with-param name="nr" select="position()"/>
            <xsl:with-param name="ExemptionCategoryDimension" select="$ExemptionCategoryDimension"/>
          </xsl:call-template>
        </xsl:variable>
        <xsl:call-template name="CreateContextDubDeb">
          <xsl:with-param name="xbrlParent" select="$xbrlParent"/>
          <xsl:with-param name="Id" select="$did"/>
          <xsl:with-param name="yearPos" select="$yearPos"/>
          <xsl:with-param name="ExemptionCategoryDimension" select="$ExemptionCategoryDimension"/>
        </xsl:call-template>
        <xsl:call-template name="CreateContextDubDeb">
          <xsl:with-param name="xbrlParent" select="$xbrlParent"/>
          <xsl:with-param name="Id" select="$endid"/>
          <xsl:with-param name="yearPos" select="$yearPos"/>
          <xsl:with-param name="ExemptionCategoryDimension" select="$ExemptionCategoryDimension"/>
        </xsl:call-template>
        <xsl:if test="not($ExemptionCategoryDimension='NewWriteDownDebtClaimMember')">
          <xsl:call-template name="CreateContextDubDeb">
            <xsl:with-param name="xbrlParent" select="$xbrlParent"/>
            <xsl:with-param name="Id" select="$startid"/>
            <xsl:with-param name="yearPos" select="$yearPos"/>
            <xsl:with-param name="ExemptionCategoryDimension" select="$ExemptionCategoryDimension"/>
          </xsl:call-template>   
        </xsl:if>

        <xsl:variable name="klantId" select="Klant"/>
        <xsl:variable name="br" select="xbrlParent//BusinessRelationDataContract[Id=$klantId]"/>
        
        
        
        <tax-inc:IdentityTradeDebtor contextRef="{$did}"><xsl:choose><xsl:when test="AlgemeneProvisie='true'"><xsl:value-of select="label"/></xsl:when><xsl:when test="$br"><xsl:value-of select="$br/FullName"/></xsl:when><xsl:otherwise>nvt</xsl:otherwise></xsl:choose></tax-inc:IdentityTradeDebtor>
        <tax-inc:AddressTradeDebtor contextRef="{$did}"><xsl:choose><xsl:when test="$br"><xsl:value-of select="$br/Address"/> <xsl:value-of select="$br/ZipCode"/> <xsl:value-of select="$br/City"/></xsl:when><xsl:otherwise>nvt</xsl:otherwise></xsl:choose></tax-inc:AddressTradeDebtor>

        <xsl:if test="not($ExemptionCategoryDimension='NewWriteDownDebtClaimMember')">
          <tax-inc:DebtClaim unitRef="EUR" decimals="INF" contextRef="{$startid}"><xsl:value-of select="VorderingStart"/></tax-inc:DebtClaim>
          <tax-inc:ExemptWriteDownDebtClaim unitRef="EUR" decimals="INF" contextRef="{$startid}"><xsl:value-of select="WaardeverminderingVorig"/></tax-inc:ExemptWriteDownDebtClaim>
        </xsl:if>
        <xsl:choose>
          <xsl:when test="number(VorderingEinde) &lt; number(WaardeverminderingTotaal)">
            <tax-inc:DebtClaim unitRef="EUR" decimals="INF" contextRef="{$endid}"><xsl:value-of select="WaardeverminderingTotaal"/></tax-inc:DebtClaim>
          </xsl:when>
          <xsl:otherwise>
          <tax-inc:DebtClaim unitRef="EUR" decimals="INF" contextRef="{$endid}"><xsl:value-of select="VorderingEinde"/></tax-inc:DebtClaim>
          </xsl:otherwise>
        </xsl:choose>

        
        <tax-inc:ExemptWriteDownDebtClaim unitRef="EUR" decimals="INF" contextRef="{$endid}"><xsl:value-of select="WaardeverminderingTotaal"/></tax-inc:ExemptWriteDownDebtClaim>

        <xsl:if test="not($ExemptionCategoryDimension='NewProvisionRisksExpensesMember')">
          <xsl:if test="Waardevermindering and number(Waardevermindering)&lt;0">
            <xsl:variable name="wv" select="0-number(Waardevermindering)"/>
            <xsl:choose>
              <xsl:when test="RedenTotVermindering='Verlies'">
                <tax-inc:DecreaseExemptWriteDownDebtClaimFinalWriteDownCurrentTaxPeriod unitRef="EUR" decimals="INF" contextRef="{$did}"><xsl:value-of select="$wv"/></tax-inc:DecreaseExemptWriteDownDebtClaimFinalWriteDownCurrentTaxPeriod>
              </xsl:when>
              <xsl:when test="RedenTotVermindering='Inning'">
                <tax-inc:DecreaseExemptWriteDownDebtClaimFullPartialCollectionCurrentTaxPeriod unitRef="EUR" decimals="INF" contextRef="{$did}"><xsl:value-of select="$wv"/></tax-inc:DecreaseExemptWriteDownDebtClaimFullPartialCollectionCurrentTaxPeriod>
              </xsl:when>
              <xsl:when test="RedenTotVermindering='NieuwGeschat'">
                <tax-inc:DecreaseExemptWriteDownDebtClaimNewEstimationProbableLoss unitRef="EUR" decimals="INF" contextRef="{$did}"><xsl:value-of select="$wv"/></tax-inc:DecreaseExemptWriteDownDebtClaimNewEstimationProbableLoss>
              </xsl:when>
            </xsl:choose>
          </xsl:if>
          <xsl:if test="Waardevermindering and number(Waardevermindering)&gt;0">
            <tax-inc:IncreaseExemptWriteDownDebtClaim unitRef="EUR" decimals="INF" contextRef="{$did}"><xsl:value-of select="number(Waardevermindering)"/></tax-inc:IncreaseExemptWriteDownDebtClaim>
          </xsl:if>
        </xsl:if>
        
        <xsl:variable name="changedId" select="ChangedID"/>
        <xsl:if test="$changedId and count(../WijzigingBestaandeTijdensBoekjaar[ID=$changedId])>0">
          <xsl:variable name="wijz" select="../WijzigingBestaandeTijdensBoekjaar[ID=$changedId]"/>
          <xsl:variable name="comm" select="$wijz/Commentaar"/>
          <xsl:if test="$comm and not($comm='')">
            <tax-inc:JustificationExemptWriteDown contextRef="{$did}"><xsl:value-of select="$comm"/></tax-inc:JustificationExemptWriteDown>
          </xsl:if>
        </xsl:if>
        
      </xsl:for-each>
    </xsl:if>
  </xsl:template>
  
  <xsl:template name="CreateContextDubDeb">
    <xsl:param name="xbrlParent"/>
    <xsl:param name="Id"/>
    <xsl:param name="yearPos"/>
    <xsl:param name="ExemptionCategoryDimension"/>

    <context id="{$Id}">
      <entity>
        <identifier scheme="http://www.fgov.be"><xsl:value-of select="$xbrlParent/Client/XbrlClientDataContract/cenr"/></identifier>
      </entity>
      <xsl:choose>
        <xsl:when test="contains($Id,'I-Start')">
          <period>
            <instant><xsl:value-of select="$xbrlParent/PreviousEndDate"/></instant>
          </period>
        </xsl:when>
        <xsl:when test="contains($Id,'I-End')">
          <period>
            <instant><xsl:value-of select="$xbrlParent/EndDate"/></instant>
          </period>
        </xsl:when>
        <xsl:otherwise>
          <period>
            <startDate><xsl:value-of select="$xbrlParent/StartDate"/></startDate>
            <endDate><xsl:value-of select="$xbrlParent/EndDate"/></endDate>
          </period>
        </xsl:otherwise>
      </xsl:choose>
      <scenario>
        <xbrldi:explicitMember dimension="d-ec:ExemptionCategoryDimension">d-ec:<xsl:value-of select="$ExemptionCategoryDimension"/></xbrldi:explicitMember>
        <xbrldi:typedMember dimension="d-ty:WriteDownDebtClaimTypedDimension">
          <d-ty:WritedownDebtClaimTypedID><xsl:value-of select="$yearPos"/></d-ty:WritedownDebtClaimTypedID>
        </xbrldi:typedMember>
      </scenario>
    </context>
  </xsl:template>


  
  <!-- ===========//=========== -->

  <!-- 
  Voorzieningen - IMPLEMENTED & TESTED
  ====================
  types:
     Ongewijzigd tijdens het boekjaar: d-ec:UnalteredBalanceProvisionRisksExpensesMember
     Verminderd tijdens het boekjaar: d-ec:RemainingBalanceAfterDecreaseProvisionRisksExpensesMember
     Verhoogd tijdens het boekjaar: d-ec:RemainingBalanceAfterIncreaseProvisionRisksExpensesMember
     Nieuw tijdens het boekjaar: d-ec:NewProvisionRisksExpensesMember
  
  aard kosten:
    Verkregen vergoeding: d-expt:ExpensesCompensationCoveredCurrentTaxPeriodMember
    Grote herstelling: d-expt:ExpensesOverhaulImportantRepairsMaximum10YearsMember
    Beroepswerkzaamheid: d-expt:ProfessionalExpensesCurrentTaxPeriodMember
    -->

  
  <xsl:template match="DetailVoorziening" mode="VoorzieningenManualDetail">
      <xsl:variable name="ExemptionCategoryDimension" select="Categorie"/>
      <xsl:variable name="ExpenseTypeDimension" select="AardKosten"/>
      <xsl:variable name="yearpos"><xsl:value-of select="Aanslagjaar"/>-<xsl:value-of select="VolgNr"/></xsl:variable>
      <xsl:variable name="xbrlParent" select="//XbrlInput"/>

      <xsl:variable name="startid">
          <xsl:call-template name="GetVoorzieningID">
            <xsl:with-param name="item" select="."/>
            <xsl:with-param name="prequelType" select="'START'"/>
            <xsl:with-param name="year" select="Aanslagjaar"/>
            <xsl:with-param name="nr" select="VolgNr"/>
            <xsl:with-param name="ExemptionCategoryDimension" select="$ExemptionCategoryDimension"/>
            <xsl:with-param name="ExpenseTypeDimension" select="$ExpenseTypeDimension"/>
          </xsl:call-template>
        </xsl:variable>
        <xsl:variable name="endid">
          <xsl:call-template name="GetVoorzieningID">
            <xsl:with-param name="item" select="."/>
            <xsl:with-param name="prequelType" select="'END'"/>
            <xsl:with-param name="year" select="Aanslagjaar"/>
            <xsl:with-param name="nr" select="VolgNr"/>
            <xsl:with-param name="ExemptionCategoryDimension" select="$ExemptionCategoryDimension"/>
            <xsl:with-param name="ExpenseTypeDimension" select="$ExpenseTypeDimension"/>
          </xsl:call-template>
        </xsl:variable>
        <xsl:variable name="did">
          <xsl:call-template name="GetVoorzieningID">
            <xsl:with-param name="item" select="."/>
            <xsl:with-param name="prequelType" select="'D'"/>
            <xsl:with-param name="year" select="Aanslagjaar"/>
            <xsl:with-param name="nr" select="VolgNr"/>
            <xsl:with-param name="ExemptionCategoryDimension" select="$ExemptionCategoryDimension"/>
            <xsl:with-param name="ExpenseTypeDimension" select="$ExpenseTypeDimension"/>
          </xsl:call-template>
        </xsl:variable>
        <xsl:call-template name="CreateContextVoorziening">
          <xsl:with-param name="xbrlParent" select="$xbrlParent"/>
          <xsl:with-param name="Id" select="$endid"/>
          <xsl:with-param name="yearPos" select="$yearpos"/>
          <xsl:with-param name="ExemptionCategoryDimension" select="$ExemptionCategoryDimension"/>
          <xsl:with-param name="ExpenseTypeDimension" select="$ExpenseTypeDimension"/>
        </xsl:call-template>
        <xsl:if test="not($ExemptionCategoryDimension='NewProvisionRisksExpensesMember')">
          <xsl:call-template name="CreateContextVoorziening">
            <xsl:with-param name="xbrlParent" select="$xbrlParent"/>
            <xsl:with-param name="Id" select="$startid"/>
            <xsl:with-param name="yearPos" select="$yearpos"/>
            <xsl:with-param name="ExemptionCategoryDimension" select="$ExemptionCategoryDimension"/>
            <xsl:with-param name="ExpenseTypeDimension" select="$ExpenseTypeDimension"/>
          </xsl:call-template>

          <xsl:call-template name="CreateContextVoorziening">
            <xsl:with-param name="xbrlParent" select="$xbrlParent"/>
            <xsl:with-param name="Id" select="$did"/>
            <xsl:with-param name="yearPos" select="$yearpos"/>
            <xsl:with-param name="ExemptionCategoryDimension" select="$ExemptionCategoryDimension"/>
            <xsl:with-param name="ExpenseTypeDimension" select="$ExpenseTypeDimension"/>
          </xsl:call-template>
        </xsl:if>

        <tax-inc:ProbableCost contextRef="{$endid}" decimals="INF" unitRef="EUR">
          <xsl:value-of select="helper:ZeroOrValue(WaarschKostEinde)"/>
        </tax-inc:ProbableCost>
        <xsl:if test="not($ExemptionCategoryDimension='NewProvisionRisksExpensesMember')">
          <tax-inc:ExemptProvisionRisksExpenses contextRef="{$startid}" decimals="INF" unitRef="EUR">
            <xsl:value-of select="helper:ZeroOrValue(VoorzieningStart)"/>
          </tax-inc:ExemptProvisionRisksExpenses>
        </xsl:if>
        <tax-inc:ExemptProvisionRisksExpenses contextRef="{$endid}" decimals="INF" unitRef="EUR">
          <xsl:value-of select="helper:ZeroOrValue(VoorzieningEinde)"/>
        </tax-inc:ExemptProvisionRisksExpenses>
        <xsl:if test="not($ExemptionCategoryDimension='NewProvisionRisksExpensesMember')">
              <xsl:if test="VermGedragen and not(helper:ZeroOrValue(VermGedragen)=0)">
                <tax-inc:DecreaseExemptProvisionRisksExpensesBorneExpensesCurrentTaxPeriod contextRef="{$did}" decimals="INF" unitRef="EUR"><xsl:value-of select="helper:ZeroOrValue(VermGedragen)"/></tax-inc:DecreaseExemptProvisionRisksExpensesBorneExpensesCurrentTaxPeriod>
              </xsl:if>
              <xsl:if test="VermNieuweInschatting and not(helper:ZeroOrValue(VermNieuweInschatting)=0)">
                <tax-inc:DecreaseExemptProvisionRisksExpensesNewEstimationProbableExpenses contextRef="{$did}" decimals="INF" unitRef="EUR"><xsl:value-of select="helper:ZeroOrValue(VermNieuweInschatting)"/></tax-inc:DecreaseExemptProvisionRisksExpensesNewEstimationProbableExpenses>
              </xsl:if>
          <xsl:if test="VermNieuweInschatting and not(helper:ZeroOrValue(VermNieuweInschatting)=0)">
            <tax-inc:IncreaseExemptProvisionRisksExpenses contextRef="{$did}" decimals="INF" unitRef="EUR"><xsl:value-of select="helper:ZeroOrValue(VermNieuweInschatting)"/></tax-inc:IncreaseExemptProvisionRisksExpenses>
          </xsl:if>
        </xsl:if>
        <xsl:if test="Verantwoording and not(Verantwoording='')"> 
        <tax-inc:JustificationExemptProvision contextRef="{$did}"><xsl:value-of select="Verantwoording"/></tax-inc:JustificationExemptProvision>
        </xsl:if>
    
  </xsl:template>
  
  <xsl:template match="Voorzieningen" mode="VoorzieningenDetail">
    <xsl:variable name="xbrlParent" select="//XbrlInput"/>
    <xsl:if test="count(ToestandEindHuidigBoekjaar)>0">
      <xsl:for-each select="ToestandEindHuidigBoekjaar[Belastbaar='Vrijgesteld']">
        <xsl:variable name="ExemptionCategoryDimension">
          <xsl:call-template name="GetVoorzieningType">
            <xsl:with-param name="item" select="."/>
          </xsl:call-template>
        </xsl:variable>
        <xsl:variable name="ExpenseTypeDimension">
          <xsl:call-template name="GetVoorzieningAardKosten">
            <xsl:with-param name="item" select="."/>
          </xsl:call-template>
        </xsl:variable>
        
        <xsl:variable name="startid">
          <xsl:call-template name="GetVoorzieningID">
            <xsl:with-param name="item" select="."/>
            <xsl:with-param name="prequelType" select="'START'"/>
            <xsl:with-param name="year" select="2009"/>
            <xsl:with-param name="nr" select="position()"/>
            <xsl:with-param name="ExemptionCategoryDimension" select="$ExemptionCategoryDimension"/>
            <xsl:with-param name="ExpenseTypeDimension" select="$ExpenseTypeDimension"/>
          </xsl:call-template>
        </xsl:variable>
        <xsl:variable name="endid">
          <xsl:call-template name="GetVoorzieningID">
            <xsl:with-param name="item" select="."/>
            <xsl:with-param name="prequelType" select="'END'"/>
            <xsl:with-param name="year" select="2009"/>
            <xsl:with-param name="nr" select="position()"/>
            <xsl:with-param name="ExemptionCategoryDimension" select="$ExemptionCategoryDimension"/>
            <xsl:with-param name="ExpenseTypeDimension" select="$ExpenseTypeDimension"/>
          </xsl:call-template>
        </xsl:variable>
        <xsl:variable name="did">
          <xsl:call-template name="GetVoorzieningID">
            <xsl:with-param name="item" select="."/>
            <xsl:with-param name="prequelType" select="'D'"/>
            <xsl:with-param name="year" select="2009"/>
            <xsl:with-param name="nr" select="position()"/>
            <xsl:with-param name="ExemptionCategoryDimension" select="$ExemptionCategoryDimension"/>
            <xsl:with-param name="ExpenseTypeDimension" select="$ExpenseTypeDimension"/>
          </xsl:call-template>
        </xsl:variable>
        <xsl:call-template name="CreateContextVoorziening">
          <xsl:with-param name="xbrlParent" select="$xbrlParent"/>
          <xsl:with-param name="Id" select="$endid"/>
          <xsl:with-param name="yearPos">2009-<xsl:value-of select="position()"/>
          </xsl:with-param>
          <xsl:with-param name="ExemptionCategoryDimension" select="$ExemptionCategoryDimension"/>
          <xsl:with-param name="ExpenseTypeDimension" select="$ExpenseTypeDimension"/>
        </xsl:call-template>
        <xsl:if test="not($ExemptionCategoryDimension='NewProvisionRisksExpensesMember')">
          <xsl:call-template name="CreateContextVoorziening">
            <xsl:with-param name="xbrlParent" select="$xbrlParent"/>
            <xsl:with-param name="Id" select="$startid"/>
            <xsl:with-param name="yearPos">2009-<xsl:value-of select="position()"/></xsl:with-param>
            <xsl:with-param name="ExemptionCategoryDimension" select="$ExemptionCategoryDimension"/>
            <xsl:with-param name="ExpenseTypeDimension" select="$ExpenseTypeDimension"/>
          </xsl:call-template>
         
          <xsl:call-template name="CreateContextVoorziening">
            <xsl:with-param name="xbrlParent" select="$xbrlParent"/>
            <xsl:with-param name="Id" select="$did"/>
            <xsl:with-param name="yearPos">2009-<xsl:value-of select="position()"/></xsl:with-param>
            <xsl:with-param name="ExemptionCategoryDimension" select="$ExemptionCategoryDimension"/>
            <xsl:with-param name="ExpenseTypeDimension" select="$ExpenseTypeDimension"/>
          </xsl:call-template>
        </xsl:if>

        <tax-inc:ProbableCost contextRef="{$endid}" decimals="INF" unitRef="EUR">
          <xsl:value-of select="helper:ZeroOrValue(VoorzieningEindeBJ)"/>
        </tax-inc:ProbableCost>
        <xsl:if test="not($ExemptionCategoryDimension='NewProvisionRisksExpensesMember')">
          <tax-inc:ExemptProvisionRisksExpenses contextRef="{$startid}" decimals="INF" unitRef="EUR">
            <xsl:value-of select="helper:ZeroOrValue(VoorzieningBeginBj)"/>
          </tax-inc:ExemptProvisionRisksExpenses>
        </xsl:if>
        <tax-inc:ExemptProvisionRisksExpenses contextRef="{$endid}" decimals="INF" unitRef="EUR">
          <xsl:value-of select="helper:ZeroOrValue(VoorzieningEindeBJ)"/>
        </tax-inc:ExemptProvisionRisksExpenses>
        <xsl:if test="not($ExemptionCategoryDimension='NewProvisionRisksExpensesMember')">
          <xsl:if test="not(number(VerminderingTijdensBJ)=0)">
            <xsl:choose>
              <xsl:when test="RedenTotVermindering='AANWENDING'">
                <tax-inc:DecreaseExemptProvisionRisksExpensesBorneExpensesCurrentTaxPeriod contextRef="{$did}" decimals="INF" unitRef="EUR">
                  <xsl:value-of select="helper:ZeroOrValue(VerminderingTijdensBJ)"/>
                </tax-inc:DecreaseExemptProvisionRisksExpensesBorneExpensesCurrentTaxPeriod>
              </xsl:when>
              <xsl:when test="RedenTotVermindering='NIEUWEINSCHATTING'">
                <tax-inc:DecreaseExemptProvisionRisksExpensesNewEstimationProbableExpenses contextRef="{$did}" decimals="INF" unitRef="EUR">
                  <xsl:value-of select="helper:ZeroOrValue(VerminderingTijdensBJ)"/>
                </tax-inc:DecreaseExemptProvisionRisksExpensesNewEstimationProbableExpenses>
              </xsl:when>
            </xsl:choose>
          </xsl:if>
          <xsl:if test="not(number(VerhogingTijdensBJ)=0)">
            <tax-inc:IncreaseExemptProvisionRisksExpenses contextRef="{$did}" decimals="INF" unitRef="EUR">
              <xsl:value-of select="helper:ZeroOrValue(VerhogingTijdensBJ)"/>
            </tax-inc:IncreaseExemptProvisionRisksExpenses>
          </xsl:if>
          <tax-inc:JustificationExemptProvision contextRef="{$did}">
            <xsl:value-of select="OmschrijvingProvisie"/>
          </tax-inc:JustificationExemptProvision>
        </xsl:if>
      </xsl:for-each>
    </xsl:if>
  </xsl:template>
  
  <xsl:template name="GetVoorzieningType">
    <xsl:param name="item"/>
    <xsl:choose>
      <xsl:when test="$item/isNew='true'">NewProvisionRisksExpensesMember</xsl:when>
      <xsl:when test="not(number($item/VerhogingTijdensBJ)=0)">RemainingBalanceAfterIncreaseProvisionRisksExpensesMember</xsl:when>
      <xsl:when test="not(number($item/VerminderingTijdensBJ)=0)">RemainingBalanceAfterDecreaseProvisionRisksExpensesMember</xsl:when>
      <xsl:otherwise>UnalteredBalanceProvisionRisksExpensesMember</xsl:otherwise>
    </xsl:choose>
</xsl:template>
  <xsl:template name="GetVoorzieningAardKosten">
    <xsl:param name="item"/>
    <xsl:choose>
      <xsl:when test="$item/AardVanDeKosten">
        <xsl:value-of select="$item/AardVanDeKosten"/>
      </xsl:when>
      <xsl:otherwise>ProfessionalExpensesCurrentTaxPeriodMember</xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template name="GetVoorzieningID">
    <xsl:param name="item"/><xsl:param name="prequelType"/><xsl:param name="year"/><xsl:param name="nr"/><xsl:param name="ExemptionCategoryDimension"/><xsl:param name="ExpenseTypeDimension"/>
    <xsl:choose>
      <xsl:when test="$prequelType='START'">I-Start</xsl:when>
      <xsl:when test="$prequelType='END'">I-End</xsl:when>
      <xsl:otherwise>D</xsl:otherwise>
    </xsl:choose>__id__<xsl:value-of select="$year"/>-<xsl:value-of select="$nr"/>__<xsl:value-of select="$ExemptionCategoryDimension"/>__<xsl:value-of select="$ExpenseTypeDimension"/>
  </xsl:template>

  <xsl:template name="CreateContextVoorziening">
    <xsl:param name="xbrlParent"/>
    <xsl:param name="Id"/>
    <xsl:param name="yearPos"/>
    <xsl:param name="ExemptionCategoryDimension"/>
    <xsl:param name="ExpenseTypeDimension"/>
    <context id="{$Id}">
      <entity>
        <identifier scheme="http://www.fgov.be">
          <xsl:value-of select="$xbrlParent/Client/XbrlClientDataContract/cenr"/>
        </identifier>
      </entity>
      <xsl:choose>
        <xsl:when test="contains($Id,'I-Start')">
          <period>
            <instant>
              <xsl:value-of select="$xbrlParent/PreviousEndDate"/>
            </instant>
          </period>
        </xsl:when>
        <xsl:when test="contains($Id,'I-End')">
          <period>
            <instant>
              <xsl:value-of select="$xbrlParent/EndDate"/>
            </instant>
          </period>
        </xsl:when>
        <xsl:otherwise>
          <period>
            <startDate>
              <xsl:value-of select="$xbrlParent/StartDate"/>
            </startDate>
            <endDate>
              <xsl:value-of select="$xbrlParent/EndDate"/>
            </endDate>
          </period>
        </xsl:otherwise>
      </xsl:choose>
      <scenario>
        <xbrldi:explicitMember dimension="d-ec:ExemptionCategoryDimension">d-ec:<xsl:value-of select="$ExemptionCategoryDimension"/></xbrldi:explicitMember>
        <xbrldi:explicitMember dimension="d-expt:ExpenseTypeDimension">d-expt:<xsl:value-of select="$ExpenseTypeDimension"/></xbrldi:explicitMember>
        <xbrldi:typedMember dimension="d-ty:ProvisionRiskExpenseTypedDimension">
          <d-ty:ProvisionRiskExpenseTypedID>
            <xsl:value-of select="$yearPos"/>
          </d-ty:ProvisionRiskExpenseTypedID>
        </xbrldi:typedMember>
      </scenario>
    </context>
  </xsl:template>

  
 
</xsl:stylesheet>