<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" exclude-result-prefixes="Helper Translations File Client" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:Helper="urn:Helper" xmlns:Translations="urn:Translations" xmlns:File="urn:File" xmlns:Client="urn:Client">
	<xsl:decimal-format name="NonNumber" NaN="0" zero-digit="0"/>
	<xsl:variable name="WorksheetLabelFontsize" select="string('10')"/>
	<xsl:variable name="WorksheetFontsize" select="string('8')"/>
	<xsl:variable name="WorksheetNumbersize" select="string('6')"/>
	<xsl:variable name="WorksheetSmallFontsize" select="string('8')"/>
	<xsl:variable name="WorksheetTotalsFontsize" select="string('10')"/>
	<xsl:variable name="WorksheetSpacing" select="1.3"/>
	<xsl:decimal-format name="euro" decimal-separator="," grouping-separator="."/>
	<xsl:variable name="NumberFormatting" select="string('#.##0,00')"/>
	<xsl:variable name="PercFormatting" select="string('#.##0,000')"/>
	<xsl:variable name="NumberFormattingNoDigit" select="string('#.##0')"/>
	<xsl:variable name="FONT" select="string('C:\WINDOWS\Fonts\EYInterstate-Light.ttf')"/>
	<xsl:variable name="up" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZ'"/>
	<xsl:variable name="lo" select="'abcdefghijklmnopqrstuvwxyz'"/>
	<xsl:param name="RuleAppDef"/>
	<xsl:param name="Culture"/>
	<xsl:template match="VolmachtEnAkkoordverklaring">
		<Root>
			<table tablefitspage="true" convert2pdfp="true" cellsfitpage="true" width="100%" align="Center" cellspacing="0" columns="4" widths="64;12;12;12">
				<!--xsl:if test="sum(BerekeningOverzicht/StatAccountingResult[number()=number()])!=0"-->
				<row>
					<cell colspan="3">
						<paragraph lineheight="1.3" size="{$WorksheetFontsize}" fontstyle="bold">
							<xsl:value-of select="translate(Translations:GetTranslation('General','StatAccountingResult','StatAccountingResult'),$lo,$up)"/>
						</paragraph>
					</cell>
					<cell horizontalalign="right" colspan="1">
						<paragraph lineheight="1.3" size="{$WorksheetNumbersize}" fontstyle="bold">
							<xsl:value-of select="format-number(sum(BerekeningOverzicht/StatAccountingResult[number()=number()]),$NumberFormatting,'euro')"/>€ </paragraph>
					</cell>
				</row>
				<!--/xsl:if-->
			</table>
		</Root>
	</xsl:template>
</xsl:stylesheet>