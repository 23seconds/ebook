﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Data.Linq;
using System.Linq.Dynamic;
using System.Text;
using EY.com.eBook.Core;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.Core.EF;
using EY.com.eBook.API.Contracts.Data.Logic;
using EY.com.eBook.Core.EF.Read;
using System.Data.Objects;
using EY.com.eBook.Core.EntityTranslation;
using EY.com.eBook.API.BL.Translators;
using System.ServiceModel;
using EY.com.eBook.Core.Data;
using EY.com.eBook.API.Contracts.Proxies;
using EY.com.eBook.API.Contracts.Data.Cache;
using System.Text.RegularExpressions;
using System.ServiceProcess;
using System.Xml;
using System.Xml.Linq;
using EY.com.eBook.API.Contracts.Services;

namespace EY.com.eBook.API.BL.Read
{


    public class AdministrationRepository : eBookRepository
    {

        public bool ReloadMainCache()
        {
            ICacheService proxy;
            return TryFetchICacheService(out proxy)
                ? proxy.ReloadMainCache() : false;
        }

        public List<CoefficientGroupDataContract> GetCoefficientGroups(CriteriaCultureDataContract ccdc)
        {
            var groups = eBookReadManager.Context.CoefficientGroupSet.Include("Names");    
            
            

            return Translator.TranslateList<CoefficientGroupDataContract,CoefficientGroup>(groups.ToList(),ccdc.Culture);
        }

        public List<CoefficientKeyDataContract> GetCoefficientKeysInGroup(CriteriaIdAndCultureDataContract cicdc)
        {
            CoefficientGroup group = eBookReadManager.Context.CoefficientGroupSet.Include("Coefficients").FirstOrDefault(g => g.Id == cicdc.Id);
            var keys = group.Coefficients.Select(c=>c.Key);

            List<CoefficientKey> coefKeys = eBookReadManager.Context.CoefficientKeys.Include("Names").WhereIn(e => e.Key, keys).ToList();
            return Translator.TranslateList<CoefficientKeyDataContract, CoefficientKey>(coefKeys.ToList(), cicdc.Culture);
        }

        public List<CoefficientDataContract> GetCoefficientsInKeys(CriteriaStringDataContract csdc)
        {
            var coefficients = eBookReadManager.Context.CoefficientsSet.Where(c => c.Key == csdc.String).OrderBy(c=>c.StartDate).ThenBy(c=>c.AssessmentYear);

            return Translator.TranslateList<CoefficientDataContract, Coefficients>(coefficients.ToList());
        }

        public Boolean RestartCachingService() {
            ServiceController service = new ServiceController("eBook Caching Service");
            if (service.Status == ServiceControllerStatus.Running)
            {
                service.Stop();
                service.WaitForStatus(ServiceControllerStatus.Stopped);
                service.Start();
            }
            else {
                service.Start();
            }
            return true;
        }


        public List<ClientEngagementLetterDataContract> GetClientEngagementLetterReport(CriteriaClientEngagementLetterDataContract cceldc) {
            // Convert date GMT
            DateTime startDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(cceldc.StartDate, "W. Europe Standard Time");
            DateTime endDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(cceldc.EndDate, "W. Europe Standard Time");

            string statId = cceldc.StatusId != null ? cceldc.StatusId.ToString() : "%";
            string templateId = !string.IsNullOrEmpty(cceldc.TemplateId) ? "%" + cceldc.TemplateId + "%" : "%";

            var cels = eBookReadManager.Context.test(statId, startDate, endDate, templateId);

            List<ClientEngagementLetterDataContract> lceldc = (from el in cels
                                                               select new ClientEngagementLetterDataContract
                                                               {
                                                                   ClientName = el.clientName,
                                                                   status = el.statusId,
                                                                   template= el.templateId,
                                                                   StartDate = el.itemStartDate,
                                                                   EndDate = el.itemEndDate,
                                                                   Office = el.officeName
                                                               }).ToList();

            
            return lceldc;                                                     
        }

        public List<CodaPoaDataContract> GetCodaPoa(CriteriaCodaPoaDataContract ccpdc)
        {
            List<CodaPoaDataContract> lcpdc = null;
            /*
            //TODO
            // Convert date GMT
            DateTime startDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(cceldc.StartDate, "W. Europe Standard Time");
            DateTime endDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(cceldc.EndDate, "W. Europe Standard Time");

            string statId = cceldc.StatusId != null ? cceldc.StatusId.ToString() : "%";
            string templateId = !string.IsNullOrEmpty(cceldc.TemplateId) ? "%" + cceldc.TemplateId + "%" : "%";

            
            
            var cels = eBookReadManager.Context.test(statId, startDate, endDate, templateId);

            List<CodaPoaDataContract> lcpdc = (from el in cels
                                                               select new ClientEngagementLetterDataContract
                                                               {
                                                                   ClientName = el.clientName,
                                                                   status = el.statusId,
                                                                   template = el.templateId,
                                                                   StartDate = el.itemStartDate,
                                                                   EndDate = el.itemEndDate,
                                                                   Office = el.officeName
                                                               }).ToList();

            */
            return lcpdc;
        }
        /*
        public List<PersonEngagementLetterDataContract> GetPersonEngagementLetterReport(CriteriaPersonEngagementLetterDataContract cpeldc)
        {
            // Convert date GMT
            DateTime startDateTo = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(cpeldc.StartDateTo, "W. Europe Standard Time");
            DateTime startDateFrom = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(cpeldc.StartDateFrom, "W. Europe Standard Time");
            DateTime endDateTo = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(cpeldc.EndDateTo, "W. Europe Standard Time");            
            DateTime endDateFrom = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(cpeldc.EndDateFrom, "W. Europe Standard Time");

            string statId = cpeldc.StatusId != null ? cpeldc.StatusId.ToString() : "%";
            string templateId = !string.IsNullOrEmpty(cpeldc.TemplateId) ? "%" + cpeldc.TemplateId + "%" : "%";

            var cels = eBookReadManager.Context.test(statId, startDate, endDate, templateId);

            List<ClientEngagementLetterDataContract> lceldc = (from el in cels
                                                               select new ClientEngagementLetterDataContract
                                                               {
                                                                   ClientName = el.clientName,
                                                                   status = el.statusId,
                                                                   template = el.templateId,
                                                                   StartDate = el.itemStartDate,
                                                                   EndDate = el.itemEndDate,
                                                                   Office = el.officeName
                                                               }).ToList();


            return lceldc;
        }
        */
        public List<WorksheetDataContract> GetWorksheets() {
            var worksheets = eBookReadManager.Context.WorksheetTypes;
            List<WorksheetDataContract> lwdc = new List<WorksheetDataContract>();
            foreach (var w in worksheets) {
                WorksheetDataContract wdc = new WorksheetDataContract
                {
                    TypeId = w.Id,
                    Data = w.Description
                };
                lwdc.Add(wdc);
            }
            return lwdc;
        }

        public List<WorksheetHelpDataContract> GetWorksheetHelp(CriteriaBaseIdDataContract cidc) {
            var help = eBookReadManager.Context.WorksheetHelpNewSet.Where(w => w.WorksheetTypeID == cidc.Id && w.Culture=="nl-BE").OrderBy(t=>t.Order);
            List<WorksheetHelpDataContract> lwhdc = new List<WorksheetHelpDataContract>();
            
            foreach (var h in help) {
                Guid g = System.Guid.NewGuid();
                WorksheetHelpDataContract whdc = new WorksheetHelpDataContract
                {
                    Id = g,
                    WorksheetHelpId = h.WorksheetHelpID,
                    WorksheetTypeId = h.WorksheetTypeID,
                    Culture = h.Culture,
                    Subject = h.Subject,
                    Body = h.Body,
                    Order = h.Order
                };
                lwhdc.Add(whdc);
            }
            
            //lwhdc.OrderBy(w => w.Order).ToList() ;
            return lwhdc;
        }

        public List<WorksheetHelpDataContract> GetWorksheetHelpById(CriteriaBaseIdDataContract cidc) {
            var help = eBookReadManager.Context.WorksheetHelpNewSet.Where(w => w.WorksheetTypeID == cidc.Id).OrderBy(t => t.Order); 
            List<WorksheetHelpDataContract> lwhdc = new List<WorksheetHelpDataContract>();
            foreach (var h in help)
            {
                Guid g = System.Guid.NewGuid();
                WorksheetHelpDataContract whdc = new WorksheetHelpDataContract
                {
                    Id = g,
                    WorksheetHelpId = h.WorksheetHelpID,
                    WorksheetTypeId = h.WorksheetTypeID,
                    Culture = h.Culture,
                    Subject = h.Subject,
                    Body = h.Body,
                    Order = h.Order
                };
                lwhdc.Add(whdc);
            }
            lwhdc.OrderBy(w => w.Order).ToList();
            return lwhdc;
        }
        

        

        protected override void RegisterTranslators()
        {
            Translator.RegisterEntityTranslator(new CoefficientGroupTranslator());
            Translator.RegisterEntityTranslator(new CoefficientKeyTranslator());
            Translator.RegisterEntityTranslator(new CoefficientTranslator());
        }
    }
}
