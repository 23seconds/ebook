﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using EY.com.eBook.Core.EF;
using EY.com.eBook.Core.EF.Read;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.API.Contracts.Data.Logic;
using System.ServiceModel;
using EY.com.eBook.Core.Data;
using System.Data.Objects;

namespace EY.com.eBook.API.BL.Read
{
    public class FileServiceRepository : eBookRepository
    {
        //Services
        private Guid fsYearEnd = new Guid("C3B8C8DB-3822-4263-9098-541FAE897D02");
        private Guid fsAnnualAccounts = new Guid("1ABF0836-7D7B-48E7-9006-F03DB97AC28B");

        /// <summary>
        /// Gets status of file service
        /// </summary>
        /// <param name="cfsdc">file service ID's</param>
        /// <returns>Returns file service</returns>
        public FileServiceStatusDataContract GetFileServiceStatus(CriteriaFileServiceDataContract cfsdc)
        {
            FileService fileService = QueryFileService(cfsdc);
            Int16? statusId = fileService.Status;

            //in the future, once all services have statuses, this shouldn't be nullable link between services and service_statuses
            try
            {
                ServiceStatuses fileServiceStatus = eBookReadManager.Context.ServiceStatuses
                                .AsQueryable()
                                .FirstOrDefault(f => f.Id == statusId && f.ServiceId == fileService.ServiceId);

                //Custom property changes to follow up actions

                //Check if file was already added for the upload follow up action, then add added_cabinet iconCls property
                //string followUpActions = "[{type: "upload",text: "Upload Statutory Accounts",location: "08258E0E-99E3-402F-84C2-148383D5255E",filename: "Statutory Accounts",statusId:"FCF7A560-3D74-4341-A32B-310C8FA336CF",reviewNotesRequired:false,followUpStatus: 10,activation:"Manager,Director,Partner,OfficeValidator"},{type: "upload",text: "Upload Minutes General Assemble",location: "D9BB468C-9886-463D-AC22-3D0C75F9F2DB",filename: "Minutes General Assemble",statusId:"FCF7A560-3D74-4341-A32B-310C8FA336CF",reviewNotesRequired:false,followUpStatus: 10,activation:"Manager,Director,Partner,OfficeValidator"},{type: "upload",text: "Upload Representation Letter",location: "F4888591-D65C-4A36-8A9E-CA9273C79CB6",filename: "Representation Letter",statusId:"FCF7A560-3D74-4341-A32B-310C8FA336CF",reviewNotesRequired:false,followUpStatus: 10,activation:"Manager,Director,Partner,OfficeValidator"}]";
                if(fileServiceStatus.FollowUpActions.Contains("Upload"))
                {
                    String followUpActions = fileServiceStatus.FollowUpActions;
                    followUpActions = followUpActions.TrimStart('[').TrimEnd(']').Replace("},{","};{");
                    string[] arrFollowUpActions = followUpActions.Split(';');
                    fileServiceStatus.FollowUpActions = String.Empty;

                    EY.com.eBook.API.BL.Read.Repository repository = new EY.com.eBook.API.BL.Read.Repository();

                    foreach(string followUpAction in arrFollowUpActions)
                    {
                        if (followUpAction.Contains("Upload"))
                        {
                            //Get location and repositoryStatus in the string by finding the value between quaotes after their property name
                            Guid location = new Guid(followUpAction.Substring(followUpAction.IndexOf("location")).Split('"', '"')[1]);
                            Guid? repositoryStatusId = null;
                            if(followUpAction.Contains("repositoryStatusId")) repositoryStatusId = new Guid(followUpAction.Substring(followUpAction.IndexOf("repositoryStatusId")).Split('"', '"')[1]);
                            //Guid repositoryStatusId = new Guid(Regex.Match(followUpAction.Substring(followUpAction.IndexOf("repositoryStatusId" + 20)), @"\""([^""]*)\""").ToString());

                            CriteriaQueryFilesDataContract cqfdc = new CriteriaQueryFilesDataContract
                            {
                                ClientId = cfsdc.ClientId,
                                Status = repositoryStatusId,
                                StructureId = location
                            };

                            List<RepositoryNodeDataContract> fileList = repository.QueryFiles(cqfdc);

                            if (fileList.Count > 0)
                            {
                                //Add iconCls property
                                fileServiceStatus.FollowUpActions += followUpAction.Replace("eBook-repository-add-ico-24", "eBook-repository-added-ico-24") + ",";
                            }
                            else
                            {
                                //no change
                                fileServiceStatus.FollowUpActions += followUpAction + ",";
                            }
                        }
                        else
                        {
                            fileServiceStatus.FollowUpActions += followUpAction + ",";
                        }
                    }

                    fileServiceStatus.FollowUpActions = "[" + fileServiceStatus.FollowUpActions.TrimEnd(',') + "]";
                }
                //}



                

                FileServiceStatusDataContract fileServiceDataContract = new FileServiceStatusDataContract
                {
                    StatusId = fileServiceStatus.Id,
                    Status = fileServiceStatus.Status,
                    Rank = fileServiceStatus.Rank,
                    Activation = fileServiceStatus.Activation,
                    FollowUpStatuses = fileServiceStatus.FollowUpStatuses,
                    FollowUpActions = fileServiceStatus.FollowUpActions
                };

                return fileServiceDataContract;
            }
            catch (Exception ex)
            {
                throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { Message = "Could not determine status of file & service." });
            }
        }

        /// <summary>
        /// Gets log of file service
        /// </summary>
        /// <param name="cfsdc">file service ID's</param>
        /// <returns>returns a log list</returns>
        public List<FileServiceLogDataContract> GetFileServiceLog(CriteriaFileServiceDataContract cfsdc)
        {
            var fileServiceLogs = QueryFileServiceLog(cfsdc);

            List<FileServiceLogDataContract> fileServiceLogList = fileServiceLogs.Select(f => new FileServiceLogDataContract
            {
                Action = f.Action,
                Comment = f.Comment,
                Status = eBookReadManager.Context.ServiceStatuses.FirstOrDefault(c => c.Id == f.StatusId && c.ServiceId == f.ServiceId).Status,
                Timestamp = f.Timestamp,
                DocumentUploaded = f.RepositoryItemId.HasValue,
                PdfAttachment = f.PdfAttachmentId,
                PersonName = f.PersonFirstName + " " + f.PersonLastName
            }).ToList();

            if (cfsdc.Order == null || cfsdc.Order == "ASC")
            {
                return fileServiceLogList.OrderBy(f => f.Timestamp).ToList();
            }
            else
            {
                return fileServiceLogList.OrderByDescending(f => f.Timestamp).ToList();
            }
        }


        /// <summary>
        /// Queries the file services table using the file and service ID
        /// </summary>
        /// <param name="cfsdc"></param>
        /// <returns>Iqueryable</returns>
        public FileService QueryFileService(CriteriaFileServiceDataContract cfsdc)
        {
            var fileService = eBookReadManager.Context.FileServices
                            .AsQueryable()
                            .FirstOrDefault(f => f.FileId == cfsdc.FileId && f.ServiceId == cfsdc.ServiceId);

            return fileService;
        }

        /// <summary>
        /// Queries the bundle log table using the file and service ID
        /// </summary>
        /// <param name="cfsdc"></param>
        /// <returns>Iqueryable</returns>
        public IQueryable<FileServiceLog> QueryFileServiceLog(CriteriaFileServiceDataContract cfsdc)
        {
            var fileServiceLogs = eBookReadManager.Context.FileServiceLogs
                            .AsQueryable()
                            .Where(f => f.FileId == cfsdc.FileId && f.ServiceId == cfsdc.ServiceId);

            return fileServiceLogs;
        }
        
        /// <summary>
        /// Gets all possible File Service statuses
        /// </summary>
        /// <returns>List of CODA POA statuses</returns>
        public List<StatusDataContract> GetServiceStatusesList(CriteriaServiceCultureDataContract cscdc)
        {
            return eBookReadManager.Context.ServiceStatuses
                .Where(s => s.ServiceId == cscdc.ServiceId)
                .OrderBy(s => s.Rank).ThenBy(s => s.Status)
                .Select(s => new StatusDataContract
                {
                    Id = s.Id,
                    StatusName = s.Status,
                    Rank = s.Rank
                })
                .ToList();
        }

        /// <summary>
        /// Gets all File Service statuses that can be manually set by a user in the FS report. User must have activation rights for that particular status
        /// and can only set a status 
        /// </summary>
        /// <returns>List of CODA POA statuses</returns>
        public List<StatusDataContract> GetUserSetStatusesList(CriteriaFileServicePersonDataContract cfspdc)
        {
            bool allowed = false;

            if (cfspdc.ActivePerson == null) cfspdc.ActivePerson = new BusinessHelper().DecodePerson(cfspdc.EncodedActivePerson);

            if (cfspdc.Status.HasValue && eBookReadManager.Context.ServiceStatuses.Count(s => s.Activation.Contains(cfspdc.ActivePerson.ActiveRole) && s.Id == (short)cfspdc.Status) == 1) //user can only change a status that allows activeRole activation
            {
                if (cfspdc.ServiceId == fsAnnualAccounts && cfspdc.ActivePerson.ActiveRole == "NBB Admin" && cfspdc.Status.HasValue)
                {
                    allowed = true;
                }
                else if (cfspdc.ServiceId == fsYearEnd && cfspdc.ActivePerson.ActiveRole == "YearEnd Admin" && cfspdc.Status.HasValue) //does not yet exist. Not required
                {
                    allowed = true;
                }
            }

            if(allowed)
            {
                return eBookReadManager.Context.ServiceStatuses
                .Where(s => s.ServiceId == cfspdc.ServiceId && s.Activation.Contains(cfspdc.ActivePerson.ActiveRole)) //user can only a status to role activation allowed statuses
                .OrderBy(s => s.Rank).ThenBy(s => s.Status)
                .Select(s => new StatusDataContract
                {
                    Id = s.Id,
                    StatusName = s.Status,
                    Rank = s.Rank
                })
                .ToList();
            }
            else{
                return null;
            }
        }

        /// <summary>
        /// Returns a list containing fileservice status info
        /// </summary>
        /// <param name="ccpfsdc">CriteriaClientPersonFileServiceDataContract</param>
        /// <returns></returns>
        public List<ClientPersonFileServiceReport> QueryFileServiceReport(CriteriaClientPersonFileServiceDataContract ccpfsdc,out ObjectParameter recCount)
        {
            //Check what filters to be used on the data
            if (string.IsNullOrEmpty(ccpfsdc.Department)) ccpfsdc.Department = "%";
            if (string.IsNullOrEmpty(ccpfsdc.ClientName)) ccpfsdc.ClientName = "%";
            if (string.IsNullOrEmpty(ccpfsdc.Office)) ccpfsdc.Office = "%";
            if (string.IsNullOrEmpty(ccpfsdc.StatusIds)) ccpfsdc.StatusIds = "%";
            if (!ccpfsdc.Start.HasValue) ccpfsdc.Start = 0;
            if (!ccpfsdc.NewestFile.HasValue) ccpfsdc.NewestFile = false;
            if (!ccpfsdc.Owned.HasValue) ccpfsdc.Owned = false;
            string lastModifiedBy = ccpfsdc.LastModifiedBy.HasValue ? ccpfsdc.LastModifiedBy.ToString() : "%";
            DateTime? now = DateTime.Now;
            recCount = new ObjectParameter("count", typeof(int));

            //utilize a stored procedure to retrieve the data
            var data = eBookReadManager.Context.GetClientPersonFileServiceReport(ccpfsdc.PersonId.HasValue ? ccpfsdc.PersonId.ToString() : null, ccpfsdc.StatusIds, ccpfsdc.EndDate, ccpfsdc.DateModified, ccpfsdc.ClientName, ccpfsdc.Office, ccpfsdc.Start, ccpfsdc.Limit, ccpfsdc.Service.ToString(), ccpfsdc.Department, ccpfsdc.NewestFile, ccpfsdc.Owned, lastModifiedBy, recCount).ToList();
            //order data based on given parameters
            if (!string.IsNullOrEmpty(ccpfsdc.SortField) && !string.IsNullOrEmpty(ccpfsdc.SortOrder))
            {
                //sort the list using the given sortstate
                ccpfsdc.SortField = ccpfsdc.SortField.First().ToString().ToUpper() + ccpfsdc.SortField.Substring(1);
                //
                Type clientPersonFileServiceReport = typeof(ClientPersonFileServiceReport);
                //Create a dynamic lambda clientPersonEngagementLetterStat1Type parameter
                ParameterExpression param = Expression.Parameter(clientPersonFileServiceReport, "x");
                Func<ClientPersonFileServiceReport, dynamic> sortBy;
                Expression paramExpression = null;

                //Match the given order column with the lambda property
                switch (ccpfsdc.SortField)
                {
                    case "Partner":
                        paramExpression = Expression.Property(param, "ppartner");
                        break;
                    case "Manager":
                        paramExpression = Expression.Property(param, "pmanager");
                        break;
                    case "Office":
                        paramExpression = Expression.Property(param, "office");
                        break;
                    case "DateModified":
                        paramExpression = Expression.Property(param, "datemodified");
                        break;
                    case "EndDate":
                        paramExpression = Expression.Property(param, "enddate");
                        break;
                    case "StatusId":
                        paramExpression = Expression.Property(param, "statusId");
                        break;
                    case "ClientName":
                        paramExpression = Expression.Property(param, "clientName");
                        break;
                    case "Person":
                        paramExpression = Expression.Property(param, "person");
                        break;
                    default:
                        paramExpression = Expression.Property(param, ccpfsdc.SortField);
                        break;
                }

                //Set the property to whatever order field has been given
                try
                {
                    //Create lambda
                    if (paramExpression != null)
                    {
                        sortBy = Expression.Lambda<Func<ClientPersonFileServiceReport, dynamic>>(Expression.TypeAs(paramExpression, typeof(object)), param).Compile();

                        if (sortBy != null)
                        {
                            if (ccpfsdc.SortOrder == "ASC")
                            {
                                data = data.OrderBy(sortBy).ToList();
                            }
                            else if (ccpfsdc.SortOrder == "DESC")
                            {
                                data = data.OrderByDescending(sortBy).ToList();
                            }
                        }
                    }
                }
                catch (ArgumentException a)
                {
                    throw;
                }
            }

            return data.ToList();
        }

        /// <summary>
        /// Gets a file service report based on criteria given
        /// </summary>
        /// <param name="cpeldc"></param>
        /// <returns>list of ClientPersonFileService records</returns>
        public ResponseDataContract<List<ClientPersonFileServiceDataContract>> GetFileServiceReport(CriteriaClientPersonFileServiceDataContract ccpfsdc)
        {
           ObjectParameter recCount;
           List<ClientPersonFileServiceReport> data = QueryFileServiceReport(ccpfsdc, out recCount);
           List<ClientPersonFileServiceDataContract> lcpfsdc = new List<ClientPersonFileServiceDataContract>();

            lcpfsdc = (from el in data
                        select new ClientPersonFileServiceDataContract
                        {
                            DateModified = el.dateModified,
                            DaysOnStatus = el.daysOnStatus,
                            ClientGfis = string.IsNullOrEmpty(el.clientGFIS) ? "" : el.clientGFIS,
                            ClientName = string.IsNullOrEmpty(el.clientName) ? "" : el.clientName,
                            ClientId = string.IsNullOrEmpty(el.clientId.ToString()) ? "" : el.clientId.ToString(),
                            Status = el.status,
                            StatusId = el.statusId,
                            EndDate = el.endDate,
                            StartDate = el.startDate,
                            NextAnnualMeeting = el.nextAnnualMeeting,
                            Manager = string.IsNullOrEmpty(el.pmanager) ? "" : el.pmanager,
                            Partner = string.IsNullOrEmpty(el.ppartner) ? "" : el.ppartner,
                            Office = string.IsNullOrEmpty(el.officeName) ? "" : el.officeName,
                            FileId = string.IsNullOrEmpty(el.fileId.ToString()) ? "" : el.fileId.ToString(),
                            Person = string.IsNullOrEmpty(el.person) ? "" : el.person,
                            FileName = string.IsNullOrEmpty(el.fileName) ? "" : el.fileName
                        }).ToList();

            ResponseDataContract<List<ClientPersonFileServiceDataContract>> response = 
            new ResponseDataContract<List<ClientPersonFileServiceDataContract>> {
                Data = lcpfsdc.ToList(),
                Total = (int)recCount.Value
            };

            return response;
        }


        protected override void RegisterTranslators()
        {

        }
    }
}
