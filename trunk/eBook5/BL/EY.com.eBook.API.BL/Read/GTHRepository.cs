﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Data.Linq;
using System.Linq.Dynamic;
using System.Text;
using EY.com.eBook.Core;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.Core.EF;
using EY.com.eBook.API.Contracts.Data.Logic;
using EY.com.eBook.Core.EF.Read;
using System.Data.Objects;
using EY.com.eBook.Core.EntityTranslation;
using EY.com.eBook.API.BL.Translators;
using System.ServiceModel;
using EY.com.eBook.Core.Data;
using EY.com.eBook.API.Contracts.Proxies;
using EY.com.eBook.API.Contracts.Data.Cache;

namespace EY.com.eBook.API.BL.Read
{
    

    public class GTHRepository : RepositoryBase
    {
        
        public GTHClientStatusDataContract GetGTHClient(Guid id)
        {
            
            GTHTeamClient gtc = eBookReadManager.Context.GTHTeamClients.FirstOrDefault(t => t.Id==id);
            return Translator.Translate<GTHClientStatusDataContract>(gtc);
        }


        public ResponseDataContract<List<GTHClientStatusDataContract>> GetAllClients(CriteriaGTHSearchClientTeamDataContract cstdc)
        {
            IQueryable<GTHTeamClient> oQry = eBookReadManager.Context.GTHTeamClients
                                                .Where(t => t.TeamId == cstdc.TeamId);

            if (!string.IsNullOrEmpty(cstdc.Query))
            {
                oQry = oQry.Where(t => (t.ClientName != null && t.ClientName.ToLower().Contains(cstdc.Query))
                                    || (t.ClientEnterprise != null && t.ClientEnterprise.Contains(cstdc.Query)));
            }



            return new ResponseDataContract<List<GTHClientStatusDataContract>>
            {
                Data = (cstdc.Start.HasValue && cstdc.Limit.HasValue)
                            ? Translator.TranslateList<GTHClientStatusDataContract, GTHTeamClient>(oQry.ToList().Skip(cstdc.Start.Value).Take(cstdc.Limit.Value).ToList(), TranslateLevel.FirstLevel)
                            : Translator.TranslateList<GTHClientStatusDataContract, GTHTeamClient>(oQry.ToList(), TranslateLevel.FirstLevel)
                ,
                Total = oQry.Count()
            };
        }


        public ResponseDataContract<List<GTHClientStatusDataContract>> GetClients(CriteriaGTHSearchClientTeamDataContract cstdc)
        {
            IQueryable<GTHTeamClient> oQry = eBookReadManager.Context.GTHTeamClients
                                                .Where(t => t.TeamId == cstdc.TeamId && (t.Status=="INPROGRESS" && t.PersonId==cstdc.PersonId) );

            if (!string.IsNullOrEmpty(cstdc.Query))
            {
                oQry = oQry.Where(t => (t.ClientName != null && t.ClientName.ToLower().Contains(cstdc.Query))
                                    || (t.ClientEnterprise != null && t.ClientEnterprise.Contains(cstdc.Query)));
            }
            
            

            return new ResponseDataContract<List<GTHClientStatusDataContract>>
            {
                Data = (cstdc.Start.HasValue && cstdc.Limit.HasValue)
                            ? Translator.TranslateList<GTHClientStatusDataContract, GTHTeamClient>(oQry.ToList().Skip(cstdc.Start.Value).Take(cstdc.Limit.Value).ToList(), TranslateLevel.FirstLevel)
                            : Translator.TranslateList<GTHClientStatusDataContract, GTHTeamClient>(oQry.ToList(), TranslateLevel.FirstLevel)
                ,
                Total = oQry.Count()
            };
        }

        public GTHClientStartDataContract GetClientStartList(CriteriaGTHTeamDataContract ctdc)
        {
            GTHClientStartDataContract ret = new GTHClientStartDataContract();
            if (ctdc.TeamId != 5)
            {
                //update to translators
                ret.TeamId = ctdc.TeamId;
                ret.Todo = Translator.TranslateList<GTHClientStatusDataContract, GTHTeamClient>(eBookReadManager.Context.GTHTeamClients.Where(c => c.TeamId == ctdc.TeamId && c.Status == "TODO").OrderBy(t => t.LastUpdate).ThenBy(t => t.ClientName).ToList());
                ret.Progress = Translator.TranslateList<GTHClientStatusDataContract, GTHTeamClient>(eBookReadManager.Context.GTHTeamClients.Where(c => c.TeamId == ctdc.TeamId && c.Status != "TODO" && c.Status != "FINAL").OrderBy(t => t.LastUpdate).ThenBy(t => t.ClientName).ToList());
                ret.Final = Translator.TranslateList<GTHClientStatusDataContract, GTHTeamClient>(eBookReadManager.Context.GTHTeamClients.Where(c => c.TeamId == ctdc.TeamId && c.Status == "FINAL").OrderBy(t => t.LastUpdate).ThenBy(t => t.ClientName).ToList());
            }
            else
            { //5 = Filing Annual Accounts NBB
                Guid fsAnnualAccounts = new Guid("1ABF0836-7D7B-48E7-9006-F03DB97AC28B");
                ObjectParameter recCount;
                Int16[] statusesTodo = { 5, 10, 13 }; //Approved by partner, Returned to pool by GTH, Refiling Requested
                Int16[] statusesInProgress = { 7, 9 }; //Picked up by GTH, Filed by GTH
                Int16[] statusesDone = { 11 }; //Invoice uploaded by GTH
                List<GTHClientStatusDataContract> todo = new List<GTHClientStatusDataContract>();
                List<GTHClientStatusDataContract> inProgress = new List<GTHClientStatusDataContract>();
                List<GTHClientStatusDataContract> done = new List<GTHClientStatusDataContract>();
                FileServiceRepository fsr = new FileServiceRepository();
                List<ClientPersonFileServiceReport> lcpfsr = new List<ClientPersonFileServiceReport>();
                CriteriaClientPersonFileServiceDataContract ccpfsdc = new CriteriaClientPersonFileServiceDataContract()
                {
                    Start = 0,
                    Limit = 500,
                    Service = fsAnnualAccounts,
                    StatusIds = "5,10,13,7,9,11"
                };

                //Get all todo, in progress and done NBB filing records
                lcpfsr = fsr.QueryFileServiceReport(ccpfsdc, out recCount);

                foreach (ClientPersonFileServiceReport cpfsr in lcpfsr)
                {
                    Int16 statusId = (Int16)cpfsr.statusId;

                    GTHClientStatusDataContract gcsdc = new GTHClientStatusDataContract()
                    {
                        Id = cpfsr.fileId,
                        ClientName = cpfsr.clientName,
                        ClientId = (Guid)cpfsr.clientId,
                        LastUpdate = (DateTime)cpfsr.dateModified,
                        PersonName = cpfsr.person,
                        ServiceStatus = cpfsr.status,
                        ServiceStatusId = statusId,
                        TeamId = 5,
                        StartDate = cpfsr.startDate,
                        EndDate = cpfsr.endDate
                    };

                    if (statusesTodo.Contains(statusId))
                    {
                        gcsdc.Status = "TODO";
                        todo.Add(gcsdc);
                    }
                    else if(statusesInProgress.Contains(statusId))
                    {
                        gcsdc.Status = "INPROGRESS";
                        inProgress.Add(gcsdc);
                    }
                    else if (statusesDone.Contains(statusId))
                    {
                        gcsdc.Status = "DONE";
                        done.Add(gcsdc);
                    }
                }

                fsr.GetFileServiceReport(ccpfsdc);
                ret.TeamId = ctdc.TeamId;
                ret.Todo = todo;
                ret.Progress = inProgress;
                ret.Final = done;
            }

            return ret;
        }

        public List<PersonBaseDataContract> getPersons() {
            
                var persons = eBookReadManager.Context.PersonSet.Where(p => p.Gpn.ToLower().Contains("in")).OrderBy(p => p.FirstName);
                return Translator.TranslateList<PersonBaseDataContract, Person>(persons.ToList());
            
            
        }

        public List<GTHPersonClientDataContract> GetPersonClients(CriteriaStringDataContract csdc)
        {
            var persons = eBookReadManager.Context.PersonSet.Include("Roles").Where(p => p.Gpn.ToLower().Contains("IN"));
            List<GTHPersonClientDataContract> lgpcdc = new List<GTHPersonClientDataContract>();
            foreach (var person in persons)
            {

                foreach (PersonRole prole in person.Roles)
                {

                    if (prole.Role == csdc.String)
                    {
                        int team = Convert.ToInt32(csdc.String.Substring(csdc.String.Length - 1, 1));
                        var gthClients = eBookReadManager.Context.GTHTeamClients.Where(t => t.PersonId == person.Id && t.Status == "INPROGRESS" && t.TeamId == team).Count();
                        GTHPersonClientDataContract gpcdc = new GTHPersonClientDataContract
                        {
                            PersonId = person.Id,
                            FirstName = person.FirstName,
                            LastName = person.LastName,
                            ActiveClientCount = gthClients
                            
                        };
                        lgpcdc.Add(gpcdc);
                    }
                }
            }
            return lgpcdc;
        }
        

        public List<PersonRolesDataContract> GetPersonRoles(CriteriaBaseIdDataContract cbidc)
        {


            var roles = eBookReadManager.Context.PersonRoleSet.Where(p => p.Role.ToLower().StartsWith("GTH")).Select(p => p.Role).Distinct().ToList();
            roles.Sort();
            var personRoles = eBookReadManager.Context.PersonRoleSet.Where(p => p.PersonId == cbidc.Id).Select(p => p.Role).ToList();
            List<PersonRolesDataContract> lprdc = new List<PersonRolesDataContract>();
            foreach (var role in roles) {
                PersonRolesDataContract prdc = new PersonRolesDataContract { 
                    role=role,
                    check=personRoles.Contains(role)
                };
                lprdc.Add(prdc);
            }
            return lprdc;
            //return new PersonRolesDataContract { 
            //    Personid = cbidc.Id,
            //    roles = eBookReadManager.Context.PersonRoleSet.Where(p => p.PersonId == cbidc.Id).Select(pr => pr.Role).ToList()
            //};
        }

        public List<GTHRolesDataContract> GetRoles() {
            var roles = eBookReadManager.Context.PersonRoleSet.Where(r => r.Role.Contains("GTH Team")).Select(r => r.Role).Distinct().ToList();
            roles.Sort();
            List<GTHRolesDataContract> lrdc = new List<GTHRolesDataContract>();
            foreach (string role in roles){
                GTHRolesDataContract rdc = new GTHRolesDataContract{
                    role = role
                };
                lrdc.Add(rdc);
            }
            return lrdc;            
        }

        public void Team5NotifyTeam(CriteriaIdAndCultureDataContract ciacdc) {
            ClientRepository cr = new ClientRepository();
            List<TeamMemberDataContract> teammembers = cr.GetClientTeam(ciacdc);
            Client cl = eBookReadManager.Context.ClientSet.FirstOrDefault(x => x.Id == ciacdc.Id);
            string clientName = "";
            
            if (cl != null)
            {
                clientName = cl.Name;
            }
            else {
                clientName = "CLIENT NOT FOUND";
            }
            string period = ciacdc.Culture.Replace("\n", " ").Substring(2);
            string subject = "Filing annual accounts – " + clientName + " – " + period + " – multiple files available in eBook – ACTION REQUIRED";
            string[] teamMembersAcr = teammembers.Where(x => x.Department == "ACR").Select(y => y.Mail).ToArray();
            StringBuilder body = new StringBuilder("<html><head></head><body><br/><br/>");
            
            // nl-BE
            body.Append("<p>Beste team,</p>");
            body.Append("<p>Tijdens de verificatie van de neerlegging van de jaarrekening voor bovenvermelde klant en periode werd vastgesteld dat meerdere dossiers voor dezelfde periode aanwezig zijn in eBook.</p>");
            body.Append("<p>Gelieve de overbodige dossiers zo snel mogelijk te willen verwijderen in eBook zodat enkel het finale dossier voor de desbetreffende periode beschikbaar is.</p>");
            body.Append("<p>De neerlegging van de jaarrekening kan niet gebeuren zolang er meerdere dossiers beschikbaar zijn in eBook. </p>");
            body.Append("<p>U ontvangt een dagelijkse notificatie van GTH totdat bovenstaande situatie in orde is gebracht.</p></br>");
            body.Append("<p><i>Bij vragen of bemerkingen, gelieve een mail te sturen naar acrcentralteam@be.ey.com met als referentie GTH_neerlegging JR_klantennaam_klantnummer</i></p><br/><br/><br/>");

            // fr-FR
            body.Append("<p>Cher tous,</p>");
            body.Append("<p>Lors des vérifications effectuées en vue du dépôt des comptes annuels du dossier, dont le nom et la période sont mentionnés ci-dessus, nous avons constaté qu’il existait plusieurs dossiers relatifs </p><p>à la même période dans eBook.</p>");
            body.Append("<p>Nous vous demandons de bien vouloir supprimer les dossiers superflus dès que possible afin que seul le dossier final reste accessible.</p>");
            body.Append("<p>Les comptes annuels ne peuvent être déposés tant qu’il existe plusieurs dossiers dans eBook.</p>");
            body.Append("<p>Vous recevrez quotidiennement une notification du GTH jusqu’au moment où le problème sera résolu.</p></br>");
            body.Append("<p><i>Pour toutes question sou remarques, veuillez adresser votre email à l’adresse acrcentralteam@be.ey.com avec la référence GTH_dépôt Année_Nomduclient_n°duclient</i></p>");

            body.Append("</body></html>");
            CoreHelper.SendMailGTH(teamMembersAcr, new string[] { System.Configuration.ConfigurationManager.AppSettings["Mailing.AcrCentralTeam"] }, body.ToString(), subject, true);
            
        }

        public void Team5MissingFiles(CriteriaIdAndCultureDataContract ciacdc)
        {
            ClientRepository cr = new ClientRepository();
            List<TeamMemberDataContract> teammembers = cr.GetClientTeam(ciacdc);
            Client cl = eBookReadManager.Context.ClientSet.FirstOrDefault(x => x.Id == ciacdc.Id);
            string clientName = "";
            if (cl != null)
            {
                clientName = cl.Name;
            }
            else
            {
                clientName = "CLIENT NOT FOUND";
            }
            string period = ciacdc.Culture.Replace("\n", " ").Substring(2);
            string subject = "Filing annual accounts – " + clientName + " – " + period + " – mandatory document(s) not available in eBook – ACTION";
            string[] teamMembersAcr = teammembers.Where(x => x.Department == "ACR").Select(y => y.Mail).ToArray();
            StringBuilder body = new StringBuilder("<html><head></head><body><br/><br/>");

            // nl-BE
            body.Append("<p>Beste team,</p>");
            body.Append("<p>Tijdens de verificatie van de neerlegging van de jaarrekening voor bovenvermelde klant en periode werd vastgesteld dat één of meerdere van onderstaande verplichte documenten ontbreken in eBook: </p><ul>");
            body.Append("<li>Getekende Engagement Agreement / prime sub memo of ontwerp van een vernieuwde Engagement Agreement (ondertekend door EY partner/director) (<i>Folder Client Permanent – 2. Engagement Letter / Engagement Agreement</i>);</li>");
            body.Append("<li>Getekende representatiebrief (<i>Folder Final Deliverables - A. Statutory Accounts – Representation Letter</i>);</li>");
            body.Append("<li>Getekende Accounting Memorandum (<i>Folder Final Deliverables - A. Statutory Accounts – Accounting Memorandum</i>).</li></ul>");
            body.Append("<p>De neerlegging van de jaarrekening kan <u>niet</u> gebeuren zolang alle vereiste documenten niet beschikbaar zijn in eBook. </p>");
            body.Append("<p>U ontvangt een dagelijkse notificatie van GTH totdat bovenstaande situatie in orde is gebracht.</p></br>");
            body.Append("<p><i>Bij vragen of bemerkingen, gelieve een mail te sturen naar acrcentralteam@be.ey.com met als referentie GTH_neerlegging JR_klantennaam_klantnummer</i></p></br></br></br>");

            // fr-FR
            body.Append("<p>Cher tous,</p>");
            body.Append("<p>Lors des vérifications effectuées en vue du dépôt des comptes annuels du dossier, dont le nom et la période sont mentionnés ci-dessus, nous avons constaté que les documents suivants n’étaient pas repris dans eBook alors que ceux-ci sont obligatoires :</p><ul>");
            body.Append("<li>Lettre d’engagement signée / prime sub memo ou projet de nouvelle lettre d’engagement (signée par un partner / director EY) (<i>Folder Client Permanent – 2. Engagement Letter / Engagement Agreement</i>);</li>");
            body.Append("<li>Lettre de représentation signée (<i>Folder Final Deliverables - A. Statutory Accounts – Representation Letter</i>);</li>");
            body.Append("<li>Mémorandum comptable signé (<i>Folder Final Deliverables - A. Statutory Accounts – Accounting Memorandum</i>).</li></ul>");
            body.Append("<p>Les comptes annuels ne peuvent être déposés tant que les documents précités ne sont pas disponibles en eBook.</p>");
            body.Append("<p>Vous recevrez quotidiennement une notification du GTH jusqu’au moment où le problème sera résolu.</p></br>");
            body.Append("<p><i>Pour toutes question sou remarques, veuillez adresser votre email à l’adresse acrcentralteam@be.ey.com avec la référence GTH_dépôt Année_Nomduclient_n°duclient</i></p>");
            
            body.Append("</body></html>");
            CoreHelper.SendMailGTH(teamMembersAcr, new string[] { System.Configuration.ConfigurationManager.AppSettings["Mailing.AcrCentralTeam"] }, body.ToString(), subject, true);

        }


        protected override void RegisterTranslators()
        {
            Translator.RegisterEntityTranslator(new GTHClientStatusTranslator());
            Translator.RegisterEntityTranslator(new PersonBaseTranslator());
            

            
        }

       
    }
}
