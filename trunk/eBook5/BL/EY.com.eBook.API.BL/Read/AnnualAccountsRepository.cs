﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using EY.com.eBook.Core;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.Core.EF;
using EY.com.eBook.API.Contracts.Data.Logic;
using System.Data.Objects;
using EY.com.eBook.Core.EntityTranslation;
using EY.com.eBook.API.BL.Translators;
using System.ServiceModel;
using EY.com.eBook.Core.Data;
using EY.com.eBook.API.Contracts.Proxies;
using EY.com.eBook.API.Contracts.Data.Cache;
using EY.com.eBook.Core.EF.Read;
using System.Data;
using System.Transactions;
using System.Data.Entity;

namespace EY.com.eBook.API.BL.Read
{

    public class AnnualAccountsRepository : RepositoryBase
    {

        /// <summary>
        /// Returns an array of all annual account form related data so it can be shown in a grid like view
        /// The point is to have a GetFormOverview for all pontential future classes that require data to be shown.
        /// </summary>
        /// <param name="cidc">cidc</param>
        public List<FormDataContract> GetFormOverview(CriteriaIdDataContract cidc)
        {
            if (cidc.Id == null)
            {
                throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { Message = "Couldn't retrieve the Annual Account data. Please contact an administrator" });
            }

            //Instantiate
            EY.com.eBook.API.BL.Read.ClientRepository clientRep = new EY.com.eBook.API.BL.Read.ClientRepository();
            EY.com.eBook.API.BL.Read.Repository rep = new EY.com.eBook.API.BL.Read.Repository();

            //Lists
            List<FormDataContract> lfdc = new List<FormDataContract>();
            List<string[]> lAnnualAccount = new List<string[]>();

            //Data to retrieve from other tables
            string offices = string.Empty;
            string invoiceFile = string.Empty;
            string serviceLine = string.Empty;
            string serviceLineDescription = string.Empty;


            //Get annual accounts record
            FileAnnualAccount faa = eBookReadManager.Context.FileAnnualAccounts.Where(a => a.File.Id == cidc.Id).FirstOrDefault();
            
            //Get file record
            File file = eBookReadManager.Context.Files.Include("Client").Where(f => f.Id == cidc.Id).FirstOrDefault();

            if (file == null || faa == null)
            {
                throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { Message = "Couldn't retrieve the Annual Account data. Please contact an administrator" });
            }

            //Get teammember list
            CriteriaIdAndCultureDataContract cicdc = new CriteriaIdAndCultureDataContract()
            {
                Id = faa.File.Client.Id
            };

            List<TeamMemberDataContract> ltmdc = clientRep.GetClientTeam(cicdc);

            //Get Offices
            ClientDepartment clientDepartment = eBookReadManager.Context.ClientDepartments.Include("Offices").Where(d => d.ClientId == file.Client.Id && d.DepartmentId == "ACR").FirstOrDefault();

            if (clientDepartment != null && clientDepartment.Offices != null)
            {
                offices = string.Join(",", clientDepartment.Offices.Select(o => o.Name).ToArray());
            }

            //Get additional engagment info
            if (!string.IsNullOrEmpty(faa.EngagementCode))
            {
                EngagementCodePerCustomer ecpc = eBookReadManager.Context.EngagementCodePerCustomers.Where(e => e.EngagementId == faa.EngagementCode).FirstOrDefault();
                serviceLine = ecpc.EngagementServiceLine;
                serviceLineDescription = ecpc.EngagementServiceLineDescription;
            }

            //Get invoice fileName
            if (faa.InvoiceRepositoryItemId.HasValue)
            {
                invoiceFile = rep.GetFileName((Guid)faa.InvoiceRepositoryItemId);
                if (String.IsNullOrEmpty(invoiceFile))
                {
                    invoiceFile = "File upload logged, but missing in repository?";
                }
            }

            lfdc.Add(new FormDataContract { Field = "Client", Value = file.Client.Name });
            lfdc.Add(new FormDataContract { Field = "Company type", Value = faa.CompanyType });
            lfdc.Add(new FormDataContract { Field = "Company model", Value = file.Name });
            lfdc.Add(new FormDataContract { Field = "Bookyear", Value = faa.Model });
            lfdc.Add(new FormDataContract { Field = "Engagement", Value = faa.EngagementCode });
            lfdc.Add(new FormDataContract { Field = "Engagement manager", Value = string.Join(",", ltmdc.Where(t => t.Role == "Manager").Select(t => t.Name).ToArray()) });
            lfdc.Add(new FormDataContract { Field = "Engagement partner", Value = string.Join(",", ltmdc.Where(t => t.Role == "Partner").Select(t => t.Name).ToArray()) });
            lfdc.Add(new FormDataContract { Field = "Offices", Value = offices });
            lfdc.Add(new FormDataContract { Field = "Service line", Value = serviceLine });
            lfdc.Add(new FormDataContract { Field = "Service line Description", Value = serviceLineDescription });
            lfdc.Add(new FormDataContract { Field = "Staffing", Value = string.Join(",", ltmdc.Select(t => t.Name).ToArray()) });
            lfdc.Add(new FormDataContract { Field = "Time limit", Value = faa.TimeLimit });
            lfdc.Add(new FormDataContract { Field = "Definitive general assembly", Value = faa.DefinitiveGeneralAssembly.ToString() });
            lfdc.Add(new FormDataContract { Field = "Changes after general assembly", Value = faa.ChangesAfterGeneralAssembly.HasValue ? ((bool)faa.ChangesAfterGeneralAssembly ? "Yes" : "No") : ""});
            lfdc.Add(new FormDataContract { Field = "Invoice language", Value = faa.InvoiceLanguage });
            lfdc.Add(new FormDataContract { Field = "Invoice recharge filing expense", Value = faa.InvoiceRechargeFilingExpense });
            lfdc.Add(new FormDataContract { Field = "Invoice amount", Value = faa.InvoiceAmount.ToString() });
            lfdc.Add(new FormDataContract { Field = "Invoice structured message", Value = faa.InvoiceStructuredMessage });
            lfdc.Add(new FormDataContract { Field = "Invoice file", Value = invoiceFile });

            return lfdc;
        }

        /// <summary>
        /// Returns an array of all annual account form related data so it can be shown in a grid like view
        /// </summary>
        /// <param name="cidc">cidc</param>
        public AnnualAccountDataContract GetAnnualAccounts(CriteriaIdDataContract cidc)
        {

            FileAnnualAccount faa = eBookReadManager.Context.FileAnnualAccounts.Include("File").Where(a => a.File.Id == cidc.Id).FirstOrDefault();

            AnnualAccountDataContract annualAccount = new AnnualAccountDataContract()
            {
                ChangesAfterGeneralAssembly = faa.ChangesAfterGeneralAssembly,
                ChangesAfterGeneralAssemblyDescription = faa.ChangesAfterGeneralAssemblyDescription,
                CompanyType = faa.CompanyType,
                DefinitiveGeneralAssembly = faa.DefinitiveGeneralAssembly,
                Model = faa.Model,
                RepositoryItemId = faa.RepositoryItemId,
                InvoiceLanguage = faa.InvoiceLanguage,
                InvoiceRechargeFilingExpense = faa.InvoiceRechargeFilingExpense,
                InvoiceAmount = faa.InvoiceAmount,
                InvoiceStructuredMessage = faa.InvoiceStructuredMessage,
                InvoiceRepositoryItemId = faa.InvoiceRepositoryItemId,
                TimeLimit = faa.TimeLimit,
                EngagementCode = faa.EngagementCode,
                FileId = faa.File.Id
            };

            return annualAccount;
        }

        /// <summary>
        /// Get list of active engagementCodes ordered by newest
        /// </summary>
        /// <param name="cidc"></param>
        /// <returns>List engagement active codes ordered by newest.</returns>
        public List<EngagementCodeDataContract> GetEngagementCodes(CriteriaIdDataContract cidc)
        {
            string gfisCode = eBookReadManager.Context.ClientSet.Where(c => c.Id == cidc.Id).Select(c => c.GFISCode).FirstOrDefault();
            List<EngagementCodeDataContract> lEngagementCodeDataContract = new List<EngagementCodeDataContract>();
            List<EngagementCodePerCustomer> engagementCodes = eBookReadManager.Context.EngagementCodePerCustomers.Where(e => e.CustomerId == gfisCode).OrderByDescending(e => e.EngagementId).ToList();

            foreach (EngagementCodePerCustomer engagementCode in engagementCodes)
            {
                EngagementCodeDataContract engagementCodeDataContract = new EngagementCodeDataContract()
                {
                    Id = engagementCode.EngagementId,
                    CustomerId = engagementCode.CustomerId,
                    Description = engagementCode.EngagementDescription,
                    Status = engagementCode.EngagementStatus,
                    ServiceLine = engagementCode.EngagementServiceLine,
                    ServiceLineDescription = engagementCode.EngagementServiceLineDescription,
                    BU = engagementCode.EngagementBU
                };

                lEngagementCodeDataContract.Add(engagementCodeDataContract);
            }

            return lEngagementCodeDataContract;
        }

        /// <summary>
        /// Get Engagement Code record by id
        /// </summary>
        /// <param name="cidc"></param>
        /// <returns>List engagement active codes ordered by newest.</returns>
        public EngagementCodeDataContract GetEngagementCode(String id)
        {
            EngagementCodePerCustomer engagementCode = eBookReadManager.Context.EngagementCodePerCustomers.Where(e => e.EngagementId == id).FirstOrDefault();

            if (engagementCode != null)
            {
                EngagementCodeDataContract engagementCodeDataContract = new EngagementCodeDataContract()
                {
                    Id = engagementCode.EngagementId,
                    CustomerId = engagementCode.CustomerId,
                    Description = engagementCode.EngagementDescription,
                    Status = engagementCode.EngagementStatus,
                    ServiceLine = engagementCode.EngagementServiceLine,
                    ServiceLineDescription = engagementCode.EngagementServiceLineDescription,
                    BU = engagementCode.EngagementBU
                };


                return engagementCodeDataContract;
            }
            else
            {
                return null;
            }
        }


        protected override void RegisterTranslators()
        {

        }
    }
}
