﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EY.com.eBook.Core;
using EY.com.eBook.API.Contracts.Data.Cache;
using EY.com.eBook.Core.Data;
using System.ServiceModel;
using EY.com.eBook.Core.EF.Read;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.Core.EF;
using EY.com.eBook.Core.EntityTranslation;
using EY.com.eBook.API.Contracts.Data.Meta;
using EY.com.eBook.API.BL.Translators;
using EY.com.eBook.API.Contracts.Proxies;
using EY.com.eBook.API.Contracts.Services;

namespace EY.com.eBook.API.BL.Read
{
    public class FileReadRepository : eBookRepository
    {
        
        // get file info (for shortcut perhaps)

        // get files list (open, closed, markdeleted, deleted)

        public FileInfoDataContract GetFileInfo(Guid id)
        {
            var file = eBookReadManager.Context.Files
                    .Include("Client")
                    .Include("Services")
                    .FirstOrDefault(f => f.Id == id);
            if (file == null) return null;
            return Translator.Translate<FileInfoDataContract>(file, TranslateLevel.NoChildren);
        }

        public List<FileInfoDataContract> GetFileInfos(CriteriaFilesDataContract cfdc)
        {
            if (cfdc == null)
                throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { Message = "Criteria cannot be null!" });

            IQueryable<File> files;
            if (cfdc.Closed.HasValue)
            {
                files = eBookReadManager.Context.Files
                    .Include("Client")
                    .Include("Services")
                  //  .Include("Messages")
                    .Where(f =>
                        f.Client.Id == cfdc.ClientId
                        && f.MarkDelete == cfdc.MarkedForDeletion
                        && f.Deleted == cfdc.Deleted
                        && f.ToMigrate==false
                        && (((bool)cfdc.Closed && f.CloseDate.HasValue)
   || (!(bool)cfdc.Closed && !f.CloseDate.HasValue))
                );
                
                if (cfdc.StartDate.HasValue)
                {
                    DateTime end = cfdc.StartDate.Value.AddDays(-1);
                    files = files.Where(f => f.EndDate.Year == end.Year && f.EndDate.Month == end.Month && f.EndDate.Day == end.Day);
                }
            }
            else
            {
                files = eBookReadManager.Context.Files
                    .Include("Client")
                    .Include("Services")
                  //  .Include("Messages")
                    .Where(f =>
                        f.Client.Id == cfdc.ClientId
                        && f.MarkDelete == cfdc.MarkedForDeletion
                        && f.Deleted == cfdc.Deleted
                        && f.ToMigrate == false
                );
            }

            //var files = 
            return (files == null) ? new List<FileInfoDataContract>() : Translator.TranslateList<FileInfoDataContract, File>(files.OrderByDescending(r=>r.EndDate).ThenByDescending(r=>r.LastUpdatedDate).ToList(), TranslateLevel.NoChildren);
        }
        /// <summary>
        /// Gets a count of distinct list of files for a customer
        /// </summary>
        /// <param name="csdc">GFIS as string</param>
        /// <returns>distinct file countlist of a customer</returns>
        public List<DistinctCounterFileDataContract> GetDistinctFilesWithCounter(CriteriaStringDataContract csdc) {  
            if (csdc == null)
                throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { Message = "Criteria cannot be null!" });
            List<DistinctCounterFileDataContract> ldcfdc = new List<DistinctCounterFileDataContract>();
            Client client = eBookReadManager.Context.ClientSet.FirstOrDefault(y => y.GFISCode == csdc.String);
            if (client != null)
            {
                var files = eBookReadManager.Context.Files.Include("Client").Where(x => x.Client.GFISCode == csdc.String && x.MarkDelete == false && x.Deleted == false)
                                                                .GroupBy(z => new { z.StartDate, z.EndDate, z.Client.Name })
                                                                .Select(gr => new { Key = gr.Key, Count = gr.Count() });
                
                foreach (var t in files)
                {
                    Guid fileId = new Guid();
                    if (t.Count == 1) {
                         fileId = eBookReadManager.Context.Files.Include("Client").FirstOrDefault(x => x.Client.Id == client.Id && x.StartDate == t.Key.StartDate && x.EndDate == t.Key.EndDate && x.MarkDelete == false && x.Deleted == false).Id;                        
                    }
                    DistinctCounterFileDataContract dcfc = new DistinctCounterFileDataContract()
                    {
                        Counter = t.Count,
                        StartDate = t.Key.StartDate,
                        EndDate = t.Key.EndDate,
                        ClientName = t.Key.Name,
                        ClientId = client.Id,
                        FileId = fileId
                    };
                    ldcfdc.Add(dcfc);
                }
            }
            return ldcfdc.OrderByDescending(x => x.StartDate).ToList();
            
                                                            
        }
        
        public List<FileDataContract> GetFiles(CriteriaFilesDataContract cfdc)
        {
            if (cfdc == null)
                throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { Message = "Criteria cannot be null!" });

            IQueryable<File> files;
            if (cfdc.Closed.HasValue)
            {
                files = eBookReadManager.Context.Files
                    .Include("Client")
                  //  .Include("Messages")
                    .Where(f =>
                        f.Client.Id == cfdc.ClientId
                        && f.MarkDelete == cfdc.MarkedForDeletion
                        && f.Deleted == cfdc.Deleted
                        && f.ToMigrate == false
                        && (((bool)cfdc.Closed && f.CloseDate.HasValue)
   || (!(bool)cfdc.Closed && !f.CloseDate.HasValue))
                );
            }
            else
            {
                files = eBookReadManager.Context.Files
                    .Include("Client")
                  //  .Include("Messages")
                    .Where(f =>
                        f.Client.Id == cfdc.ClientId
                        && f.MarkDelete == cfdc.MarkedForDeletion
                        && f.Deleted == cfdc.Deleted
                        && f.ToMigrate == false
                );
            }
            //var files = 
            return (files == null) ? new List<FileDataContract>() : Translator.TranslateList<FileDataContract, File>(files.ToList(), TranslateLevel.NoChildren);
        }

        public FileDataContract GetFile(Guid id)
        {
            var file = eBookReadManager.Context.Files.Include("Client").FirstOrDefault(f => f.Id == id);
            if (file == null)
                throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { Message = String.Format("No file found for id {0}", id) });

            return Translator.Translate<FileDataContract>(file, TranslateLevel.Infinite);
        }


        public List<FileMessageDataContract> GetMessages(CriteriaFileDataContract cfdc)
        {
            ICacheService proxy;
            return TryFetchICacheService(out proxy)
                       ? proxy.GetMessages(cfdc)
                       : Translator.TranslateList<FileMessageDataContract, Message>(
  eBookReadManager.Context.Messages
                    .Where(m => m.Culture == cfdc.Culture &&
       m.FileId == cfdc.FileId).ToList());
        }

        public List<FileMessageDataContract> GetMessagesWithoutNotes(CriteriaFileDataContract cfdc)
        {
            ICacheService proxy;
            return TryFetchICacheService(out proxy)
                       ? proxy.GetMessagesWithoutNotes(cfdc)
                       : Translator.TranslateList<FileMessageDataContract, Message>(
  eBookReadManager.Context.Messages
                    .Where(m => m.Culture == cfdc.Culture &&
       m.FileId == cfdc.FileId &&
       m.ConnectionType != "Note").ToList());
        }

        public List<FileMessageDataContract> GetNotes(CriteriaFileDataContract cfdc)
        {
            ICacheService proxy;
            return TryFetchICacheService(out proxy)
                       ? proxy.GetMessagesWithoutNotes(cfdc)
                       : Translator.TranslateList<FileMessageDataContract, Message>(
  eBookReadManager.Context.Messages
                    .Where(m => m.Culture == cfdc.Culture &&
       m.FileId == cfdc.FileId &&
       m.ConnectionType == "Note").ToList());
        }

        public FileActionDataContract GetFileAction(Guid fileId, string culture, string action)
        {
            FileAction fa = eBookReadManager.Context.FileActionSet
                       .Include("Person")
                       .Where(f => f.FileId == fileId && f.Action == action.ToUpper())
                       .OrderByDescending(f => f.DateTime)
                       .FirstOrDefault();
            if (fa != null)
            {
                return Translator.Translate<FileActionDataContract>(fa, culture);
            }
            return null;
        }

        public List<FileActionDataContract> GetFileActions(Guid fileId, string culture)
        {
            var fas = eBookReadManager.Context.FileActionSet
                       .Include("Person")
                       .Where(f => f.FileId == fileId)
                       .OrderByDescending(f => f.DateTime);
            return Translator.TranslateList<FileActionDataContract, FileAction>(fas.ToList(), culture);
        }

        

        public FileMetaDataContract GetMeta(Guid fileId)
        {
            FileMeta fm = eBookReadManager.Context.FileMetaSet.FirstOrDefault(f => f.FileId == fileId);
            if (fm == null)
            {
                return new FileMetaDataContract
                {
                    FileId = fileId,
                    Data = new FileMetaDataDataContract
                    {
                        Managers = new List<MetaPersonDataContract>()
                        ,
                        ShareHolders = new List<ShareHolderDataContract>()
                        ,
                        PeerGroup = new List<PeerGroupItemDataContract>()
                    }
                };
            }

            return CoreHelper.DeserializeFromString<FileMetaDataContract>(fm.Data);
        }

        public List<FileMetaDataContract> GetMetaByClientId(Guid clientId) {
            List<File> lf = eBookReadManager.Context.Files.Where(f => f.Client.Id == clientId).ToList();
            List<FileMetaDataContract> lfmdc = new List<FileMetaDataContract>();
            foreach (File fi in lf) {
                FileMeta fm = eBookReadManager.Context.FileMetaSet.FirstOrDefault(f => f.FileId == fi.Id);
                if (fm == null)
                {
                    FileMetaDataContract fmdc = new FileMetaDataContract
                    {
                        FileId = fi.Id,
                        Data = new FileMetaDataDataContract
                        {
                            Managers = new List<MetaPersonDataContract>()
                            ,
                            ShareHolders = new List<ShareHolderDataContract>()
                            ,
                            PeerGroup = new List<PeerGroupItemDataContract>()
                        }
                    };
                    lfmdc.Add(fmdc);

                }else{
                    lfmdc.Add(CoreHelper.DeserializeFromString<FileMetaDataContract>(fm.Data));
                }                
            }
            return lfmdc;
        }

        

        protected override void RegisterTranslators()
        {
            Translator.RegisterEntityTranslator(new FileTranslator());
            Translator.RegisterEntityTranslator(new FileInfoTranslator());
            Translator.RegisterEntityTranslator(new FileActionTranslator());
            Translator.RegisterEntityTranslator(new FileMessageTranslator());
            Translator.RegisterEntityTranslator(new FileServiceTranslator());
            //Translator.RegisterEntityTranslator(new FileReportListTranslator());
          
        }

        public FileDataContract GetFile(CriteriaIdAndCultureDataContract cidc)
        {
            var file = eBookReadManager.Context.Files
                    .Include("Client")
                  //  .Include("Messages")
                    .First(f =>f.Id==cidc.Id);
            //var files = 
            return Translator.Translate<FileDataContract>(file, TranslateLevel.NoChildren);
        }


        public FileSettingsInfoDataContract GetFileSettings(CriteriaIdDataContract cidc)
        {
            File file = eBookReadManager.Context.Files.FirstOrDefault(f => f.Id == cidc.Id);
            if (file != null)
            {
                return new FileSettingsInfoDataContract
                {
                    FileId = cidc.Id
                    ,
                    Culture = file.Culture
                    ,
                    StartDate = file.StartDate
                    ,
                    EndDate = file.EndDate
                    ,
                    PreviousStartDate = file.PrevStartDate
                    ,
                    PreviousEndDate = file.PrevEndDate
                    ,
                    Settings = file.Settings == null ? new FileSettingsDataContract { FileId = cidc.Id } : CoreHelper.DeserializeFromString<FileSettingsDataContract>(file.Settings)
                };
            }
            return null;
        }

        public List<ExactAdminDataContract> GetExactAdmins(CriteriaIdDataContract cidc) {
            var client = eBookReadManager.Context.ClientSet.FirstOrDefault(f => f.Id == cidc.Id);
            var exactAdmin = eBookReadManager.Context.ExactAdmins.Where(ea => ea.ExactAdminVat == "BE"+client.VatNumber || ea.ExactAdminVat == "BE"+client.EnterpriseNumber);
            List<ExactAdminDataContract> leadc = new List<ExactAdminDataContract>();
            if (exactAdmin != null) {                
                foreach (var e in exactAdmin) {
                    ExactAdminDataContract eadc = new ExactAdminDataContract
                    {
                        ExactAdminCode = e.ExactAdminCode,
                        ExactAdminNr = e.ExactAdminNr,
                        ExactAdminName = e.ExactAdminName,
                        ExactAdminAddresLine1 = e.ExactAdminAddresLine1,
                        ExactAdminAddresLine2 = e.ExactAdminAddresLine2,
                        ExactAdminAddresLine3 = e.ExactAdminAddresLine3,
                        ExactAdminZipCode = e.ExactAdminZipCode,
                        ExactAdminCity = e.ExactAdminCity,
                        ExactAdminState = e.ExactAdminState,
                        ExactAdminCountry = e.ExactAdminCountry,
                        ExactAdminCurrency = e.ExactAdminCurrency,
                        ExactAdminVat = e.ExactAdminVat,
                        ExactAdminClusterTermId = e.ExactAdminClusterTermId,
                        ExactAdminClusterCode = e.ExactAdminClusterCode,
                        ExactAdminMainPostbox = e.ExactAdminMainPostbox,
                        ExactAdminDisplay = e.ExactAdminNr + " - " + e.ExactAdminName,
                        ExactAdminHasFinancialPeriods = e.ExactAdminHasFinancialPeriods
                        
                    };
                     leadc.Add(eadc);
                }
            }
            return leadc;
        }

        public List<ExactFinYearsDataContract> GetExactFinYears(CriteriaStringDataContract csdc) {
            var exactFinYears = eBookReadManager.Context.ExactFinYears.Where(fy => fy.ExactFinYearAdminCode == csdc.String);
            List<ExactFinYearsDataContract> lefydc = new List<ExactFinYearsDataContract>();
            if (exactFinYears != null) {
                int i = 1;
                var cult = System.Globalization.CultureInfo.CreateSpecificCulture("nl-BE");
                foreach (var efy in exactFinYears) {
                    ExactFinYearsDataContract efydc = new ExactFinYearsDataContract { 
                        ExactFinYearId = i,
                        ExactFinYearAdminCode = efy.ExactFinYearAdminCode,
                        ExactFinYearNumber = efy.ExactFinYearNumber,
                        ExactFinYearStartDate = efy.ExactFinYearStartDate,
                        ExactFinYearEndDate = efy.ExactFinYearEndDate,
                        ExactFinYearDisplay = efy.ExactFinYearStartDate.Date.ToString("dd/MM/yyyy", cult.DateTimeFormat) + " - " + efy.ExactFinYearEndDate.Date.ToString("dd/MM/yyyy", cult.DateTimeFormat)
                    };
                    lefydc.Add(efydc);
                    i++;
                }
            }
            return lefydc;
        }

        
    }
}
