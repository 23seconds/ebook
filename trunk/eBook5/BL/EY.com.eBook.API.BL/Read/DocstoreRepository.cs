﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Data.Linq;
using System.Linq.Dynamic;
using System.Text;
using EY.com.eBook.Core;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.Core.EF;
using EY.com.eBook.API.Contracts.Data.Logic;
using EY.com.eBook.Core.EF.Read;
using System.Data.Objects;
using EY.com.eBook.Core.EntityTranslation;
using EY.com.eBook.API.BL.Translators;
using System.ServiceModel;
using EY.com.eBook.Core.Data;
using EY.com.eBook.API.Contracts.Proxies;
using EY.com.eBook.API.Contracts.Data.Cache;
using System.Text.RegularExpressions;
using System.ServiceProcess;
using System.Xml;
using System.Xml.Linq;
using EY.com.eBook.API.Contracts.Services;
using Newtonsoft.Json;
using System.Net;
using System.IO;
using System.Web.Configuration;

namespace EY.com.eBook.API.BL.Read
{


    public class DocstoreRepository : eBookRepository
    {
        public string _url = @WebConfigurationManager.AppSettings["eBook.DocstoreAPI"].ToString();//"https://test-docstore.digitalchap.be/EYInterface/api/";
        public static string _token = "";

        public bool ReloadMainCache()
        {
            ICacheService proxy;
            return TryFetchICacheService(out proxy)
                ? proxy.ReloadMainCache() : false;
        }

        public RepositoryItemDataContract GetDocumentInfo(CriteriaIdDataContract cidc)
        {
            var doc = eBookReadManager.Context.RepositoryItems.FirstOrDefault(x => x.ItemId == cidc.Id);
            RepositoryItemDataContract ridc = new RepositoryItemDataContract()
            {
                ClientId = doc.ClientId,
                ContentType = doc.Contenttype,
                DocstoreAccess = eBookReadManager.Context.RepositoryStructureSet.FirstOrDefault(y=>y.Id == doc.StructureId).DocstoreAccess,
                Extension = doc.Extension,
                FileName = doc.Name,
                Id = doc.ItemId,
                //Meta = new List<RepositoryMetaItemDataContract>() {  },
                PeriodEnd = doc.EndDate,
                PeriodStart = doc.StartDate,
                PhysicalFileId = doc.PhysicalFileId,
                Prequel = doc.Prequel,
                Status = doc.Status,
                StructureId = doc.StructureId
            };

            return ridc;
        }


        public byte[] GetFlexedTree(criteriaFlexedTreeDataContract cfdc)
        {

            System.Net.WebClient webclient = getWebClient();
            if (string.IsNullOrEmpty(_token))
            {
                _token = Login();
            }
            authenticationTokenDataContract token = JsonConvert.DeserializeObject<authenticationTokenDataContract>(_token);
            webclient.Headers.Add("AuthenticationToken", token.AuthenticationToken);
            byte[] flexedTree = null;
            try
            {
                //DisableSecureSSL();
                flexedTree = webclient.UploadData(_url + "Document/Tree", "POST", Encoding.Default.GetBytes(JsonConvert.SerializeObject(cfdc)));
            }
            catch (Exception ex)
            {
                var wex = GetNestedException<WebException>(ex);

                // If there is no nested WebException, re-throw the exception.
                if (wex == null) { throw; }

                // Get the response object.
                var response = wex.Response as HttpWebResponse;

                // If it's not an HTTP response or is not error 403, re-throw.
                if (response == null || response.StatusCode != HttpStatusCode.Forbidden)
                {
                    throw;
                }

                // The error is 403.  Handle it here.
                _token = null;
                GetFlexedTree(cfdc);
            }
            return flexedTree;
        }



        public byte[] GetFlexedTrees()
        {
            System.Net.WebClient webclient = getWebClient();
            if (string.IsNullOrEmpty(_token))
            {
                _token = Login();
            }
            authenticationTokenDataContract token = JsonConvert.DeserializeObject<authenticationTokenDataContract>(_token);
            webclient.Headers.Add("AuthenticationToken", token.AuthenticationToken);
            byte[] flexedTrees = null;
            try
            {
                //DisableSecureSSL();
                flexedTrees = webclient.DownloadData(_url + "Document/TreeNames");
            }
            catch (Exception ex)
            {
                var wex = GetNestedException<WebException>(ex);

                // If there is no nested WebException, re-throw the exception.
                if (wex == null) { throw; }

                // Get the response object.
                var response = wex.Response as HttpWebResponse;

                // If it's not an HTTP response or is not error 403, re-throw.
                if (response == null || response.StatusCode != HttpStatusCode.Forbidden)
                {
                    throw;
                }

                // The error is 403.  Handle it here.
                _token = null;
                GetFlexedTrees();
            }
            return flexedTrees;
        }

        public byte[] GetTeamMembersByClient(CriteriaIdDataContract cidc) {
            byte[] teamMembers = null;
            using (System.IO.StreamWriter file = new System.IO.StreamWriter(@WebConfigurationManager.AppSettings["eBook.Resources"].ToString() + "\\DocstoreInterface_log1.txt", true))
            {
                file.WriteLine(DateTime.Now.ToString("HH:mm:ss.fff") + " :: START>>");
                var client = eBookReadManager.Context.ClientSet.FirstOrDefault(x => x.Id == cidc.Id);
                file.WriteLine(DateTime.Now.ToString("HH:mm:ss.fff") + " :: getClient -> " + client.Name);
                
                if (client != null)
                {
                    criteriaTeamMemberByClientDataContract ctmbcdc = new criteriaTeamMemberByClientDataContract()
                    {
                        Code = client.GFISCode,
                        OndernemingsNr = client.EnterpriseNumber
                    };
                    file.WriteLine(DateTime.Now.ToString("HH:mm:ss.fff") + " :: start getWebClient()");
                    WebClient webclient = getWebClient();
                    file.WriteLine(DateTime.Now.ToString("HH:mm:ss.fff") + " :: done getWebClient()");
                    file.WriteLine(DateTime.Now.ToString("HH:mm:ss.fff") + " :: _token: " + _token);
                    if (string.IsNullOrEmpty(_token))
                    {
                        file.WriteLine(DateTime.Now.ToString("HH:mm:ss.fff") + " :: start Login()");
                        _token = Login();
                        file.WriteLine(DateTime.Now.ToString("HH:mm:ss.fff") + " :: done Login()");
                        file.WriteLine(DateTime.Now.ToString("HH:mm:ss.fff") + " :: _token: " + _token);
                    }

                    authenticationTokenDataContract token = JsonConvert.DeserializeObject<authenticationTokenDataContract>(_token);
                    file.WriteLine(DateTime.Now.ToString("HH:mm:ss.fff") + " :: start addheader");
                    webclient.Headers.Add("AuthenticationToken", token.AuthenticationToken);
                    file.WriteLine(DateTime.Now.ToString("HH:mm:ss.fff") + " :: done addheader");
                    try
                    {
                        //DisableSecureSSL();
                        file.WriteLine(DateTime.Now.ToString("HH:mm:ss.fff") + " :: start getTeamMembers");
                        teamMembers = webclient.UploadData(_url + "TeamMember/All", "POST", Encoding.Default.GetBytes(JsonConvert.SerializeObject(ctmbcdc)));
                        //teamMembers = webclient.DownloadData("https://test-docstore-interface.digitalchap.be/api/Teammember/Test");
                        file.WriteLine(DateTime.Now.ToString("HH:mm:ss.fff") + " :: done getTeamMembers");
                    }
                    catch (Exception ex)
                    {
                        file.WriteLine(DateTime.Now.ToString("HH:mm:ss.fff") + " :: Exception:" + ex.Message.ToString());
                        var wex = GetNestedException<WebException>(ex);

                        // If there is no nested WebException, re-throw the exception.
                        if (wex == null) { throw; }

                        // Get the response object.
                        var response = wex.Response as HttpWebResponse;

                        // If it's not an HTTP response or is not error 403, re-throw.
                        if (response == null || response.StatusCode != HttpStatusCode.Forbidden)
                        {
                            throw;
                        }

                        // The error is 403.  Handle it here.
                        _token = null;
                        GetTeamMembersByClient(cidc);
                    }
                }
                file.WriteLine(DateTime.Now.ToString("HH:mm:ss.fff") + " :: END");
            }
            
            return teamMembers;
        }

        public string UploadDocument(criteriaDocstoreDocumentDataContract cdddc)
        {
            if (cdddc.DocumentId != null && cdddc.DocumentId != Guid.Empty)
            {
                var repoItem = eBookReadManager.Context.RepositoryItems.FirstOrDefault(y => y.ItemId == cdddc.DocumentId);
                if (repoItem != null)
                {
                    cdddc.ClientId = repoItem.ClientId;
                    cdddc.ContentType = repoItem.Contenttype;
                    cdddc.Author = null;
                    var templist = new List<docstoreTeamMemberDataContract> ();
                    templist.AddRange(cdddc.TeamMembers.Where(t=>t.memberRole=="Supplier").ToList());
                    templist.AddRange(cdddc.TeamMembers.Where(t => t.memberRole != "Supplier").ToList());
                    if (templist.Count(c => c.memberRole == "Supplier") == 0 || templist.Count(c => c.memberRole != "Supplier") == 0)
                    {
                        throw new Exception("Minimal 1 supplier and 1 Client is required");
                    }
                    cdddc.TeamMembers = templist;

                    var client = eBookReadManager.Context.ClientSet.FirstOrDefault(x => x.Id == cdddc.ClientId);
                    string documentId = null;
                    if (client != null)
                    {
                        cdddc.ClientCode = client.GFISCode;
                        System.Net.WebClient webclient = getWebClient();
                        if (string.IsNullOrEmpty(_token))
                        {
                            _token = Login();
                        }
                        authenticationTokenDataContract token = JsonConvert.DeserializeObject<authenticationTokenDataContract>(_token);
                        webclient.Headers.Add("AuthenticationToken", token.AuthenticationToken);

                        try
                        {
                            //DisableSecureSSL();
                            documentId = Encoding.ASCII.GetString(webclient.UploadData(_url + "Document/Add", "POST", Encoding.Default.GetBytes(JsonConvert.SerializeObject(cdddc))));
                        }
                        catch (Exception ex)
                        {
                            var wex = GetNestedException<WebException>(ex);

                            // If there is no nested WebException, re-throw the exception.
                            if (wex == null) { throw; }

                            // Get the response object.
                            var response = wex.Response as HttpWebResponse;

                            // If it's not an HTTP response or is not error 403, re-throw.
                            if (response == null || response.StatusCode != HttpStatusCode.Forbidden)
                            {
                                string result ="";
                                using (Stream stream = response.GetResponseStream())
                                {
                                    StreamReader reader = new StreamReader(stream, Encoding.UTF8);
                                    result = reader.ReadToEnd();
                                }
                                throw new Exception(result);
                            }

                            // The error is 403.  Handle it here.
                            _token = null;
                            UploadDocument(cdddc);
                        }
                    }
                    string filePath = new Repository().GetFilePath(cdddc.DocumentId);
                    if (@WebConfigurationManager.AppSettings["eBook.Resources"].ToString().StartsWith("C:")) // Check if LOCALHOST
                    {
                        filePath = @"C:\temp\test.pdf";
                    }
                    documentResponseDataContract drdc = JsonConvert.DeserializeObject<documentResponseDataContract>(documentId);
                    bool resp = HttpUploadFile("Document/Send", filePath, cdddc.Filename, cdddc.ContentType, drdc.documentId.ToString());
                    if (resp)
                    {
                        CriteriaLinkRepositoryItemToDocstoreDataContract clritddc = new CriteriaLinkRepositoryItemToDocstoreDataContract()
                        {
                            DocstoreItemId = drdc.documentId, //docstoreId
                            RepositoryItemId = cdddc.DocumentId // ebook
                        };
                        new BL.Read.Repository().LinkToDocstore(clritddc);
                        return "Success";
                    }
                    else {
                        return "Something went wrong";
                    }
                    
                }
                else {
                    return "Could not retrieve document";
                }
            }
            else {
                return "No documentId found";
            }
        }        
        
        public bool HttpUploadFile(string url, string file, string paramName, string contentType, string documentId)
        {

            
            //DisableSecureSSL();
            // make a boundary, does not really matter how it looks, this one is pretty random though
            string boundary = "---------------------------" + DateTime.Now.Ticks.ToString("x");
            byte[] boundarybytes = System.Text.Encoding.ASCII.GetBytes("\r\n--" + boundary + "\r\n");

            // Create a webrequest to our webAPI service
            HttpWebRequest wr = (HttpWebRequest)WebRequest.Create(_url + url);

            // We use contenttype form-data because of reasons...
            wr.ContentType = "multipart/form-data; boundary=" + boundary;
            wr.Method = "POST";
            wr.MediaType = "multipart/form-data";

            // proxy
            WebProxy p = new WebProxy("defrceprx01.ey.net:8080");
            p.Credentials = new NetworkCredential("appeBook", "EYext123", "EURW");
            wr.Proxy = p;

            if (string.IsNullOrEmpty(_token))
            {
                _token = Login();
            }
            authenticationTokenDataContract token = JsonConvert.DeserializeObject<authenticationTokenDataContract>(_token);
            //webclient.Headers.Add("AuthenticationToken", token.AuthenticationToken);

            // TODO here we add our authorization-token, make this less obvious
            wr.Headers.Add("AuthenticationToken", token.AuthenticationToken);  // BIJ JOU IS DIT AuthorizationToken
            wr.KeepAlive = true;

            // the stream so we can send the file
            Stream rs = wr.GetRequestStream();

            

            // Make the requestBody we need: filename, filehash, content-type and name. name is not that important...
            string headerTemplate = "Content-Disposition: form-data; name=\"fromcode\"; filename=\"{0}\"; id=\"{3}\"\r\nContent-Type: {2}\r\n\r\n";
            string header = string.Format(headerTemplate, paramName, file, contentType, documentId); // documentId komt van de eerste request (api/Document/Add)
            byte[] headerbytes = System.Text.Encoding.UTF8.GetBytes(header);

            // write our header
            rs.Write(boundarybytes, 0, boundarybytes.Length);
            rs.Write(headerbytes, 0, headerbytes.Length);

            // write our file
            FileStream fileStream = new FileStream(file, FileMode.Open, FileAccess.Read); // hier dus uw file!
            byte[] buffer = new byte[4096];
            int bytesRead = 0;
            while ((bytesRead = fileStream.Read(buffer, 0, buffer.Length)) != 0)
            {
                rs.Write(buffer, 0, bytesRead);
            }
            fileStream.Close();

            // end the request with our boundary
            byte[] trailer = System.Text.Encoding.ASCII.GetBytes("\r\n--" + boundary + "--\r\n");
            rs.Write(trailer, 0, trailer.Length);
            rs.Close();

            WebResponse wresp = null;

            try
            {
                wresp = wr.GetResponse();
                Stream stream2 = wresp.GetResponseStream();
                StreamReader reader2 = new StreamReader(stream2);
                Console.WriteLine(string.Format("Server response: {0}", reader2.ReadToEnd()));
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                if (wresp != null)
                {
                    wresp.Close();
                    wresp = null;
                }
                return false;
            }
            finally
            {
                wr = null;
            }

        }

        static T GetNestedException<T>(Exception ex) where T : Exception
        {
            if (ex == null) { return null; }

            var tEx = ex as T;
            if (tEx != null) { return tEx; }

            return GetNestedException<T>(ex.InnerException);
        }

        public string Login()
        {
            WebClient client = getWebClient();
            DisableSecureSSL();
            string token = "";
            if (string.IsNullOrEmpty(token))
            {
                token = Encoding.ASCII.GetString(client.UploadData(_url + "Account/Login", "POST", Encoding.Default.GetBytes("{\"Username\": \"Ebook\",\"Password\": \"t3st1ngInfo\"}")));

            }
            return token;
        }

        public WebClient getWebClient()
        {
            System.Net.WebClient client = new System.Net.WebClient();

            //WebProxy p = new WebProxy("http://emweb-pac.ey.net/wpad-be.dat");

            WebProxy p = new WebProxy("defrceprx01.ey.net:8080");
            p.Credentials = new NetworkCredential("appeBook", "EYext123", "EURW");

            

            client.Proxy = p;
            client.Headers.Add("content-type", "application/json");
            return client;
        }

        private static void DisableSecureSSL()
        {
            // TODO DELETE THESE LINES WHEN IN PRODUCTION
            //Trust all certificates VERY VERY VERY NOT SAFE, but oh well for dev it's ok...
            System.Net.ServicePointManager.ServerCertificateValidationCallback =
                ((sender, certificate, chain, sslPolicyErrors) => true);
            // END TODO
        }
        
        

       
        protected override void RegisterTranslators()
        {
        }
        
    }
}
