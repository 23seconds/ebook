﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using EY.com.eBook.Core;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.Core.EF;
using EY.com.eBook.API.Contracts.Data.Logic;
using EY.com.eBook.Core.EF.Read;
using System.Data.Objects;
using EY.com.eBook.Core.EntityTranslation;
using EY.com.eBook.API.BL.Translators;
using System.ServiceModel;
using EY.com.eBook.Core.Data;

namespace EY.com.eBook.API.BL.Read
{
    public class BusinessRelationRepository : RepositoryBase
    {

        public List<SupplierDataContract> GetSuppliers(Guid clientId)
        {
            var brs = eBookReadManager.Context.BusinessRelations
                            .Include("Client")
                            .Where(b => b.Client.Id == clientId && b.Type == "S");
            return Translator.TranslateList<SupplierDataContract,BusinessRelation>(brs.ToList());
        }

        public List<CustomerDataContract> GetCustomers(Guid clientId)
        {
            var brs = eBookReadManager.Context.BusinessRelations
                            .Include("Client")
                            .Where(b => b.Client.Id == clientId && b.Type == "C");
            return Translator.TranslateList<CustomerDataContract, BusinessRelation>(brs.ToList());
        }


        public ResponseDataContract<List<SupplierDataContract>> GetSuppliersRange(Guid clientId,string query, int start, int limit)
        {
            IQueryable<BusinessRelation> qbiz = eBookReadManager.Context.BusinessRelations
                                                    .Include("Client")
                                                    .Where(b => b.Client.Id == clientId && b.Type == "S");
            if (!string.IsNullOrEmpty(query))
            {
                query = query.ToLower();
                qbiz = qbiz.Where(b => b.Name.ToLower().Contains(query));
            }
            return new ResponseDataContract<List<SupplierDataContract>>
            {
                Data = Translator.TranslateList<SupplierDataContract, BusinessRelation>(qbiz.OrderBy(b => b.Name).Skip(start).Take(limit).ToList())
                ,
                Success = true
                ,
                Total = qbiz.Count()
            };
        }

        public ResponseDataContract<List<CustomerDataContract>> GetCustomersRange(Guid clientId, string query, int start, int limit)
        {
            IQueryable<BusinessRelation> qbiz = eBookReadManager.Context.BusinessRelations
                                                    .Include("Client")
                                                    .Where(b => b.Client.Id == clientId && b.Type=="C");
            if (!string.IsNullOrEmpty(query))
            {
                query = query.ToLower();
                qbiz = qbiz.Where(b => b.Name.ToLower().Contains(query));
            }
            return new ResponseDataContract<List<CustomerDataContract>>
            {
                Data = Translator.TranslateList<CustomerDataContract, BusinessRelation>(qbiz.OrderBy(b=>b.Name).Skip(start).Take(limit).ToList())
                ,
                Success = true
                ,
                Total = qbiz.Count()
            };
        }

        public ResponseDataContract<List<StringListItemDataContract>> GetPagedBusinessRelationList(CriteriaBaseIdDataContract cbidc,string type )
        {
            IQueryable<BusinessRelation> qbiz = eBookReadManager.Context.BusinessRelations
                                                    .Include("Client")
                                                    .Where(b => b.Client.Id == cbidc.Id && b.Type == type);
            if (!string.IsNullOrEmpty(cbidc.Query))
            {
                cbidc.Query = cbidc.Query.ToLower();
                qbiz = qbiz.Where(b => b.Name.ToLower().Contains(cbidc.Query));
            }
            return new ResponseDataContract<List<StringListItemDataContract>>
            {
                Data = Translator.TranslateList<StringListItemDataContract, BusinessRelation>(qbiz.OrderBy(b => b.Name).Skip(cbidc.Start.Value).Take(cbidc.Limit.Value).ToList())
                ,
                Success = true
                ,
                Total = qbiz.Count()
            };
        }

        public BusinessRelationDataContract GetBusinessRelation(Guid businessRelationId)
        {
            var brs = eBookReadManager.Context.BusinessRelations.FirstOrDefault(b => b.Id==businessRelationId);
            if (brs == null) return null;
            BusinessRelationDataContract brdc = new BusinessRelationDataContract
            {
                LastName = brs.Name,
                Address = brs.Address,
                City = brs.City,
                ZipCode = brs.Zip,
                Country = brs.Country,
                Vat = brs.VatNumber,
                FirstName = brs.FirstName,
                FullName = brs.FirstName + " " + brs.Name
            };
            return brdc;
            //return Translator.Translate<BusinessRelationDataContract>(brs);
        }

   

        public List<BusinessRelationListDataContract> GetBusinessRelationList(Guid clientId,string type)
        {
            if (string.IsNullOrEmpty(type))
            {
                
                var brs = eBookReadManager.Context.BusinessRelations
                            .Include("Client")
                            .Where(b => b.Client.Id == clientId).OrderBy(c=>c.Name);
                return Translator.TranslateList<BusinessRelationListDataContract, BusinessRelation>(brs.ToList());
            }
            else
            {
                type = type.ToUpper();
                var brs = eBookReadManager.Context.BusinessRelations
                            .Include("Client")
                            .Where(b => b.Client.Id == clientId && b.Type == type).OrderBy(c => c.Name);
                return Translator.TranslateList<BusinessRelationListDataContract, BusinessRelation>(brs.ToList());
            }
        }

        public ResponseDataContract<List<BusinessRelationListDataContract>> GetBusinessRelationListRange(Guid clientId, string type, string query, int start, int limit)
        {
            IQueryable<BusinessRelation> qbiz = eBookReadManager.Context.BusinessRelations
                                                    .Include("Client")
                                                    .Where(b => b.Client.Id == clientId);
            if (!string.IsNullOrEmpty(type))
            {
                type = type.ToUpper();
                qbiz = qbiz.Where(b => b.Type == type);
            }
            if (!string.IsNullOrEmpty(query))
            {
                query = query.ToLower();
                qbiz = qbiz.Where(b => b.Name.ToLower().Contains(query));
            }
            return new ResponseDataContract<List<BusinessRelationListDataContract>>
            {
                Data = Translator.TranslateList<BusinessRelationListDataContract, BusinessRelation>(qbiz.Skip(start).Take(limit).ToList())
                ,
                Success = true
                ,
                Total = qbiz.Count()
            };
        }


        protected override void RegisterTranslators()
        {
            Translator.RegisterEntityTranslator(new BusinessRelationListTranslator());
            Translator.RegisterEntityTranslator(new BusinessRelationStringListTranslator());
            
            Translator.RegisterEntityTranslator(new BusinessRelationTranslator());
            Translator.RegisterEntityTranslator(new SupplierTranslator());
            Translator.RegisterEntityTranslator(new CustomerTranslator());

        }
    }
}
