﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Linq.Expressions;
using System.Text;
using EY.com.eBook.Core;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.Core.EF;
using EY.com.eBook.API.Contracts.Data.Logic;
using EY.com.eBook.Core.EF.Read;
using System.Data.Objects;
using EY.com.eBook.Core.EntityTranslation;
using EY.com.eBook.API.BL.Translators;
using System.ServiceModel;
using EY.com.eBook.Core.Data;
using System.Data.Entity;

namespace EY.com.eBook.API.BL.Read
{
    public class ClientRepository : RepositoryBase
    {
        private ClientBaseTranslator cbt = new ClientBaseTranslator();

        public ClientDataContract GetClient(Guid id)
        {
            return GetClient(id, true);
        }

        public ClientDataContract GetClient(Guid id, bool overruled)
        {
            ObjectQuery<Client> qClient = eBookReadManager.Context.ClientSet;
            if (overruled) qClient = qClient.Include("Overrule");

            Client client = qClient.FirstOrDefault(c => c.Id == id);
            return Translator.Translate<ClientDataContract>(client);
        }

        public ClientBaseDataContract GetClientBase(Guid id, Guid personId)
        {
            return GetClientBase(id, true, personId);
        }

        public ClientBaseDataContract GetClientBase(Guid id)
        {
            return GetClientBase(id, true,Guid.Empty);
        }

        public ClientBaseDataContract GetClientBase(Guid id, bool overruled, Guid personId)
        {
            ObjectQuery<Client> qClient = eBookReadManager.Context.ClientSet;
            if (overruled) qClient = qClient.Include("Overrule");
            if (personId != Guid.Empty)
            {
                cbt.PersonId = personId;
                qClient = qClient.Include("PersonRoles");
            }

            Client client = qClient.First(e => e.Id == id);         
            var res = Translator.Translate<ClientBaseDataContract>(client);

            // Dit is belangrijk, anders ga je altijd filteren.
            cbt.PersonId = null;
            return res;
            
        }

        public ClientOverruleDataContract GetClientOverrule(Guid id)
        {
            var client = eBookReadManager.Context.ClientOverruleSet.FirstOrDefault(c => c.Id == id);
            if (client == null)
                return new ClientOverruleDataContract { Id = id };

            return Translator.Translate<ClientOverruleDataContract>(client, TranslateLevel.Infinite);
        }


        public string GetChampionsString(string officeId)
        {

            Office office = eBookReadManager.Context.OfficeSet.Include("Champions").FirstOrDefault(o => o.Id == officeId);
            if (office != null)
            {
                List<string> sb = new List<string>();

                foreach (Person champion in office.Champions)
                {
                    sb.Add(string.Format("{0} {1}", champion.FirstName, champion.LastName));
                }
                return string.Join("/", sb.ToArray());
            }


            return "No champions";
        }

        public List<BTWDataContract> GetBTWData(Guid fileId)
        {
            File f = eBookReadManager.Context.Files.Include("Client").FirstOrDefault(fl => fl.Id == fileId);
            if (f == null) return new List<BTWDataContract>();
            IQueryable<BTWData> qry = eBookReadManager.Context.BTWDataSet.Where(b => b.Date >= f.StartDate && b.Date <= f.EndDate && b.ClientId == f.Client.Id);
            return Translator.TranslateList<BTWDataContract, BTWData>(qry.ToList());
        }

        public List<TeamMemberDataContract> GetClientTeamDepartment(CriteriaIdAndDepartmentDataContract ciddc)
        {
            List<TeamMemberDataContract> teamList = new List<TeamMemberDataContract>();
            var team = from t in eBookReadManager.Context.PersonClientRoleSet.Include("Persons")
                       where t.ClientId == ciddc.Id
                       select t;
            foreach (PersonClientRole pcr in team.Where(t => t.Persons.Department==ciddc.Department).ToList())
            {
                teamList.Add(new TeamMemberDataContract
                {

                    Department = pcr.Persons.Department,
                    ClientId = pcr.ClientId
                    ,
                    PersonId = pcr.PersonId
                    ,
                    Role = pcr.Role
                    ,
                    Gfis = pcr.Gfis
                    ,
                    Name = string.Format("{0} {1}", pcr.Persons.FirstName, pcr.Persons.LastName)
                    ,
                    Mail = pcr.Persons.Email
                    ,
                    Level = pcr.Role.ToLower().Contains("partner") ? 1 : pcr.Role.ToLower().Contains("director") ? 2 : pcr.Role.ToLower().Contains("manager") ? 3 : 5
                });
            }
            return teamList.OrderBy(t => t.Level).ToList();
        }

        public List<TeamMemberDataContract> GetClientTeam(CriteriaIdAndCultureDataContract cicdc)
        {

            List<TeamMemberDataContract> teamList = new List<TeamMemberDataContract>();
            var team = from t in eBookReadManager.Context.PersonClientRoleSet.Include("Persons")
                       where t.ClientId == cicdc.Id
                       select t;
            foreach (PersonClientRole pcr in team.Where(t => !string.IsNullOrEmpty(t.Persons.Department)).ToList())
            {
                teamList.Add(new TeamMemberDataContract
                {

                    Department = pcr.Persons.Department,
                    ClientId = pcr.ClientId
                    ,
                    PersonId = pcr.PersonId
                    ,
                    Role = pcr.Role
                    ,
                    Gfis = pcr.Gfis
                    ,
                    Name = string.Format("{0} {1}", pcr.Persons.FirstName, pcr.Persons.LastName)
                    ,
                    Mail = pcr.Persons.Email
                    ,
                    Level = pcr.Role.ToLower().Contains("partner") ? 1 : pcr.Role.ToLower().Contains("director") ? 2 : pcr.Role.ToLower().Contains("manager") ? 3 : 5
                });
            }
            return teamList.OrderBy(t => t.Level).ToList();

        }

        public List<SimpleStringListDataContract> GetClientDepartments(CriteriaIdDataContract cidc)
        {
            var deps = eBookReadManager.Context.PersonClientRoleSet.Include("Persons").Where(pc=>pc.ClientId==cidc.Id).Select(pc => pc.Persons.Department).Distinct();
            return (from d in deps
                    select new SimpleStringListDataContract { Id = d, Description = d }
                    ).OrderBy(l => l.Description).ToList();
                     
        }


        public List<SimpleGuidListDataContract> GetClientDepartmentPartners(CriteriaIdAndDepartmentDataContract ciddc)
        {
            var parts = eBookReadManager.Context.PersonClientRoleSet.Include("Persons").Where(pc => pc.ClientId == ciddc.Id && pc.Persons.Department.ToLower()==ciddc.Department.ToLower() && pc.Role.ToLower().Contains("partner")).Select(pc => pc.Persons).Distinct();
            return (from p in parts.ToList()
                    select new SimpleGuidListDataContract { 
                        Id = p.Id
                        , Description = string.Format("{0} {1}",p.FirstName,p.LastName)
                    }
                    ).OrderBy(l => l.Description).ToList();
        }

        public List<PersonBaseDataContract> GetClientPartnerByDepartment(CriteriaIdAndDepartmentDataContract ciddc)
        {
            var parts = eBookReadManager.Context.PersonClientRoleSet.Include("Persons").Where(pc => pc.ClientId == ciddc.Id && pc.Persons.Department.ToLower() == ciddc.Department.ToLower() && pc.Role.ToLower().Contains("partner")).Select(pc => pc.Persons).Distinct();
            return (from p in parts.ToList()
                    select new PersonBaseDataContract
                    {
                        GPN = p.Gpn,
                        FirstName = p.FirstName,
                        LastName = p.LastName
                    }
                    ).ToList();
        }

        public RepositoryMetaConfigDataContract GetClientExactBanks(CriteriaIdKeyDataContract cidc)
        {
            RepositoryMetaConfigDataContract rmcdc = new RepositoryMetaConfigDataContract();
          
            if (cidc.Key!=null){
                string en = "BE" + cidc.Key;
                

                var ac = eBookReadManager.Context.ExactAdmins.Where(x => x.ExactAdminVat == en).Select(x => x.ExactAdminCode);
                var clientBanksLst = eBookReadManager.Context.ClientBankRepo.Where(c => c.ClientId == cidc.Id).ToList();
                var repoCheck = eBookReadManager.Context.RepositoryItems.Where(r=>r.ClientId==cidc.Id && !r.Deleted).Select(r => r.StructureId).ToList();
                var clientBanks = clientBanksLst.Where(c => repoCheck.Contains(c.Id)).Select(b => b.BankAccount).ToList();
                var bics = eBookReadManager.Context.BankTranslations.ToDictionary(b => b.Bic);
                
                string adminCode = ac.FirstOrDefault();
                if (adminCode != null){
                    var adminBanks = eBookReadManager.Context.ExactBanks.Where(y => y.ExactBankAdminCode == adminCode).GroupBy(x=>x.ExactBankBIC).ToDictionary(x=>x.Key,x=>x.ToList());
                   
                    if (adminBanks != null) {

                        rmcdc.Fields = new List<RepositoryMetaFieldConfigDataContract>();
                        int c = 0;

                        foreach (var bic in adminBanks) {
                            c++;
                            var myFields = new List<RepositoryMetaFieldConfigDataContract>();
                            ExactBankBicDataContract ebbdc = new ExactBankBicDataContract();
                           
                           
                          
                            foreach (var rec in bic.Value)
                            {
                                if (!clientBanks.Contains(rec.ExactBankIBAN))
                                {

                                    myFields.Add(new RepositoryMetaFieldConfigDataContract
                                    {
                                        FieldType = "Checkbox",
                                        Id = rec.ExactBankIBAN,

                                        Names = new RepositoryMetaTranslationDataContract
                                        {
                                            En = rec.ExactBankIBAN,
                                            Fr = rec.ExactBankIBAN,
                                            Nl = rec.ExactBankIBAN
                                        },
                                        Required = true,
                                        Attributes = new List<RepositoryMetaFieldAttribute>
                                        {
                                            new RepositoryMetaFieldAttribute {
                                                Key="checked",
                                                Value="true"
                                            }
                                        }
                                    });
                                }
                             }
                            if (myFields.Count > 0)
                            {
                                var bankName = new RepositoryMetaTranslationDataContract
                                {
                                    En = bic.Key,
                                    Fr = bic.Key,
                                    Nl = bic.Key
                                };
                                if (bics.ContainsKey(bic.Key))
                                {
                                    var bicName = bics[bic.Key];
                                    bankName.Nl = bicName.Nl;
                                    bankName.Fr = bicName.Fr;
                                    bankName.En = bicName.En;
                                }
                                rmcdc.Fields.Add(new RepositoryMetaFieldConfigDataContract
                                {
                                    Id = c.ToString(),
                                    FieldType = "Label",
                                    Names = bankName,
                                    
                                        Attributes= new List<RepositoryMetaFieldAttribute>()
                                });
                                rmcdc.Fields.AddRange(myFields);
                            }
                            //+ "_" + n +
                        } 
                    }
                  
                }
            }
            //rmcdc = !string.IsNullOrEmpty(xml) ? CoreHelper.DeserializeFromString<RepositoryMetaConfigDataContract>(xml) : null;
            return rmcdc;
        }

        /* OLD
        public List<ClientEngagementLetterDataContract> GetClientEngagementLettersByPerson(CriteriaIdDataContract cidc) { 
            //var letters = eBookReadManager.Context.PersonClientRoleSet.Include("clients").Include("RepositoryStatisticItem").Where(x=>x.PersonId == cidc).Select(y=>y
            var team = from t in eBookReadManager.Context.PersonClientRoleSet.Include("Persons").Include("Clients")
                       where t.PersonId == cidc.Id
                       select t;
            var letters = from ea in eBookReadManager.Context.RepositoryStatisticItemSet
                     where ea.StructureId == new Guid("32F0391E-E0BD-41F9-A9C1-C04EAC3C8CB1")
                     select ea;
            List<ClientEngagementLetterDataContract> lceldc = new List<ClientEngagementLetterDataContract>();
            foreach (PersonClientRole pcr in team){
                foreach (RepositoryStatisticItem rsi in letters.Where(p => p.ClientId == pcr.ClientId)) {
                    List<RepositoryMetaItemDataContract> lst = CoreHelper.DeserializeFromString<List<RepositoryMetaItemDataContract>>(rsi.Meta).ToList();

                    var status = eBookReadManager.Context.RepositoryStatusItems.FirstOrDefault(x => x.Id == rsi.Status && x.Culture == "en-US").Description;

                    lceldc.Add(new ClientEngagementLetterDataContract() { 
                        ClausePeriod = lst.FirstOrDefault(x=>x.Id == "ClausePeriod"),
                        ClausePeriodOther = lst.FirstOrDefault(x=>x.Id == "ClausePeriodOther"),
                        TermsConditions = lst.FirstOrDefault(x=>x.Id == "TermsConditions"),
                        DateELEA = lst.FirstOrDefault(x=>x.Id == "DateELEA"),
                        EAAmount = lst.FirstOrDefault(x=>x.Id == "EAAmount"),
                        GTHValidated = lst.FirstOrDefault(x=>x.Id == "GTHValidated"),
                        SignerFirstName = lst.FirstOrDefault(x=>x.Id == "SignerFirstName"),
                        SignerLastName = lst.FirstOrDefault(x=>x.Id == "SignerLastName"),
                        SignerFunction = lst.FirstOrDefault(x=>x.Id == "SignerFunction"),
                        SignerOtherFunction = lst.FirstOrDefault(x=>x.Id == "SignerOtherFunction"),
                        SignerIdentify = lst.FirstOrDefault(x=>x.Id=="SignerIdentity"),
                        ClientGfis = pcr.Clients.GFISCode,
                        ClientName = pcr.Clients.Name,
                        FileName = rsi.Name,
                        status = string.IsNullOrEmpty(status) ? "Could not retrieve status" : status,
                        StartDate = rsi.StartDate,
                        EndDate = rsi.EndDate,
                        template = lst.FirstOrDefault(x=>x.Id =="TemplateType").Value
                    });
                }
            }

            return lceldc;
        }
        */

        /// <summary>
        /// Gets the engagement letters of all clients for the active person
        /// </summary>
        /// <param name="cpeldc"></param>
        /// <returns>list of engagement letters</returns>
        public ResponseDataContract<List<ClientEngagementLetterDataContract>> GetClientEngagementLettersByPerson(CriteriaPersonEngagementLetterDataContract cpeldc)
        {
            //Check what filters to be used on the data
            if (string.IsNullOrEmpty(cpeldc.StructureId)) cpeldc.StructureId = "32F0391E-E0BD-41F9-A9C1-C04EAC3C8CB1";
            if (!cpeldc.StartDateFrom.HasValue) cpeldc.StartDateFrom = new DateTime(1900, 01, 01);
            if (!cpeldc.StartDateTo.HasValue) cpeldc.StartDateTo = new DateTime(2050, 01, 01);
            if (!cpeldc.EndDateFrom.HasValue) cpeldc.EndDateFrom = new DateTime(1900, 01, 01);
            if (!cpeldc.EndDateTo.HasValue) cpeldc.EndDateTo = new DateTime(2050, 01, 01);
            if (string.IsNullOrEmpty(cpeldc.ClientName)) cpeldc.ClientName = "%";
            if (!cpeldc.Start.HasValue) cpeldc.Start = 0;
            if (!cpeldc.Limit.HasValue) cpeldc.Limit = 25;
            if (string.IsNullOrEmpty(cpeldc.Expiration)) cpeldc.Expiration = "0";
            string status = !cpeldc.StatusId.HasValue ? "%" : cpeldc.StatusId.GetValueOrDefault().ToString();
            string template = !cpeldc.TemplateId.HasValue ? "%" : '%' + cpeldc.TemplateId.GetValueOrDefault().ToString() + "%";

            //utilize a stored procedure to retrieve the data
            var recCount = new ObjectParameter("count", typeof(int));
            eBookReadManager.Context.CommandTimeout = 180;
            var data = eBookReadManager.Context.testfred(status, template, cpeldc.PersonId.ToString(), cpeldc.StartDateTo, cpeldc.StartDateFrom, cpeldc.EndDateTo, cpeldc.EndDateFrom, cpeldc.Gth, "%" + cpeldc.ClientName + "%", Int32.Parse(cpeldc.Expiration), "nl-BE", cpeldc.Start, cpeldc.Limit,cpeldc.activeClient, recCount).ToList();
           //.OrderBy(x => x.statusId).ThenBy(y => y.itemStartDate).ThenBy(p => p.statusId,null,null);
            
            //order data based on given parameters
            if (!string.IsNullOrEmpty(cpeldc.SortField) && !string.IsNullOrEmpty(cpeldc.SortOrder))
            {
                //sort the list using the given sortstate
                cpeldc.SortField = cpeldc.SortField.First().ToString().ToUpper() + cpeldc.SortField.Substring(1);
                Type clientPersonEngagementLetterStat1Type = typeof(ClientPersonEngagementLetterStat1);
                //Create a dynamic lambda clientPersonEngagementLetterStat1Type parameter
                ParameterExpression param = Expression.Parameter(clientPersonEngagementLetterStat1Type, "x");
                Func<ClientPersonEngagementLetterStat1, dynamic> sortBy;
                Expression paramExpression = null;

                //Match the given order column with the lambda property
                switch (cpeldc.SortField)
                {
                    case "Partner":
                        paramExpression = Expression.Property(param, "ppartner");
                        break;
                    case "Manager":
                        paramExpression = Expression.Property(param, "pmanager");
                        break;
                    case "Office":
                        paramExpression = Expression.Property(param, "officeName");
                        break;
                    case "StartDate":
                        paramExpression = Expression.Property(param, "itemStartDate");
                        break;
                    case "EndDate":
                        paramExpression = Expression.Property(param, "itemEndDate");
                        break;
                    case "Status":
                        paramExpression = Expression.Property(param, "statusId");
                        break;
                    case "Template":
                        paramExpression = Expression.Property(param, "templateId");
                        break;
                    default:
                        paramExpression = Expression.Property(param, cpeldc.SortField);
                        break;
                }

                //Set the property to whatever order field has been given
                try
                {
                    //Create lambda
                    if (paramExpression != null)
                    {
                        sortBy = Expression.Lambda<Func<ClientPersonEngagementLetterStat1, dynamic>>(Expression.TypeAs(paramExpression, typeof(object)), param).Compile();

                        if (sortBy != null)
                        {
                            if (cpeldc.SortOrder == "ASC")
                            {
                                data = data.OrderBy(sortBy).ToList();
                            }
                            else if (cpeldc.SortOrder == "DESC")
                            {
                                data = data.OrderByDescending(sortBy).ToList();
                            }
                        }
                    }
                }
                catch (ArgumentException a)
                {
                    throw;
                }
            }


            List<ClientEngagementLetterDataContract> lceldc = new List<ClientEngagementLetterDataContract>();

            if (cpeldc.RenewalOverview.GetValueOrDefault())
            {
                foreach (var el in data)
                {
                    //Check if list already contains an EA or Prime Sub
                    var clientRecord = lceldc.FirstOrDefault(p => new Guid(p.ClientId) == el.clientId);

                    //If the already added EA or Prime Sub is older, then remove from list
                    if (clientRecord != null && clientRecord.StartDate <= el.itemStartDate)
                    {
                        lceldc.Remove(clientRecord);
                        recCount.Value = (int)recCount.Value - 1;
                        clientRecord = null;
                    }
                    
                    //Only add a new record if no client record exists within the list
                    if(clientRecord == null)
                    {
                        lceldc.Add(new ClientEngagementLetterDataContract
                        {
                            GTHValidated = string.IsNullOrEmpty(el.validatedByGTH) ? "" : el.validatedByGTH,
                            ClientGfis = string.IsNullOrEmpty(el.clientGFIS) ? "" : el.clientGFIS,
                            ClientName = string.IsNullOrEmpty(el.clientName) ? "" : el.clientName,
                            FileName = string.IsNullOrEmpty(el.itemName) ? "" : el.itemName,
                            status = string.IsNullOrEmpty(el.statusId) ? "No EA available" : el.statusId,
                            StartDate = el.itemStartDate,
                            EndDate = el.itemEndDate,
                            template = string.IsNullOrEmpty(el.templateId) ? "" : el.templateId,
                            Manager = string.IsNullOrEmpty(el.pmanager) ? "" : el.pmanager,//eBookReadManager.Context.PersonClientRoleSet.Include("Persons").Where(pc => pc.ClientId == el.clientId && pc.Role == "Manager").Select(pc => pc.Persons.LastName + " " + pc.Persons.FirstName).FirstOrDefault(),
                            Partner = string.IsNullOrEmpty(el.ppartner) ? "" : el.ppartner,//eBookReadManager.Context.PersonClientRoleSet.Include("Persons").Where(pc => pc.ClientId == el.clientId && pc.Role == "Partner").Select(pc => pc.Persons.LastName + " " + pc.Persons.FirstName).FirstOrDefault(),
                            DateELEA = string.IsNullOrEmpty(el.dateELEA) ? "" : el.dateELEA,
                            Expiration = el.expiration,
                            Office = string.IsNullOrEmpty(el.officeName) ? "" : el.officeName,
                            ItemId = string.IsNullOrEmpty(el.itemId.ToString()) ? "" : el.itemId.ToString(),
                            ClientId = string.IsNullOrEmpty(el.clientId.ToString()) ? "" : el.clientId.ToString()
                        });
                    }
                
                }
            }
            else
            {

                lceldc = (from el in data
                          select new ClientEngagementLetterDataContract
                          {
                              GTHValidated = string.IsNullOrEmpty(el.validatedByGTH) ? "" : el.validatedByGTH,
                              ClientGfis = string.IsNullOrEmpty(el.clientGFIS) ? "" : el.clientGFIS,
                              ClientName = string.IsNullOrEmpty(el.clientName) ? "" : el.clientName,
                              FileName = string.IsNullOrEmpty(el.itemName) ? "" : el.itemName,
                              status = string.IsNullOrEmpty(el.statusId) ? "No EA available" : el.statusId,
                              StartDate = el.itemStartDate,
                              EndDate = el.itemEndDate,
                              template = string.IsNullOrEmpty(el.templateId) ? "" : el.templateId,
                              Manager = string.IsNullOrEmpty(el.pmanager) ? "" : el.pmanager,//eBookReadManager.Context.PersonClientRoleSet.Include("Persons").Where(pc => pc.ClientId == el.clientId && pc.Role == "Manager").Select(pc => pc.Persons.LastName + " " + pc.Persons.FirstName).FirstOrDefault(),
                              Partner = string.IsNullOrEmpty(el.ppartner) ? "" : el.ppartner,//eBookReadManager.Context.PersonClientRoleSet.Include("Persons").Where(pc => pc.ClientId == el.clientId && pc.Role == "Partner").Select(pc => pc.Persons.LastName + " " + pc.Persons.FirstName).FirstOrDefault(),
                              DateELEA = string.IsNullOrEmpty(el.dateELEA) ? "" : el.dateELEA,
                              Expiration = el.expiration,
                              Office = string.IsNullOrEmpty(el.officeName) ? "" : el.officeName,
                              ItemId = string.IsNullOrEmpty(el.itemId.ToString()) ? "" : el.itemId.ToString(),
                              ClientId = string.IsNullOrEmpty(el.clientId.ToString()) ? "" : el.clientId.ToString()

                          }).ToList();
            }

            return new ResponseDataContract<List<ClientEngagementLetterDataContract>>
            {
                Data =  lceldc.ToList(),
                Total = (int)recCount.Value
            };
        }

        public bool? GetPrintPreference(CriteriaIdDataContract cidc)
        {
            return eBookWriteManager.Context.ClientSet.FirstOrDefault(c => c.Id == cidc.Id).PrintPreference;
        }


        protected override void RegisterTranslators()
        {
            Translator.RegisterEntityTranslator(cbt);
            Translator.RegisterEntityTranslator(new ClientTranslator());
            Translator.RegisterEntityTranslator(new ClientOverruleTranslator());
        }
    }
}
