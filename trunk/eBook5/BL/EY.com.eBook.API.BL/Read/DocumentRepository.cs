﻿using System;
using System.Collections.Generic;
using System.Linq;
//using System.Linq.Dynamic;
using System.Text;
using EY.com.eBook.Core;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.Core.EF;
using EY.com.eBook.API.Contracts.Data.Logic;
using EY.com.eBook.Core.EF.Read;
using System.Data.Objects;
using EY.com.eBook.Core.EntityTranslation;
using EY.com.eBook.API.BL.Translators;
using System.ServiceModel;
using EY.com.eBook.Core.Data;
using System.Linq.Expressions;
using System.Xml.Xsl;
using EY.com.eBook.API.BL.Xslt;
using System.Globalization;
using System.Xml.XPath;
using System.Xml;
using EY.com.eBook.API.Contracts.Data.Cache;
using EY.com.eBook.API.Contracts.Data.Meta;

namespace EY.com.eBook.API.BL.Read
{
    public class DocumentRepository : RepositoryBase
    {
        public List<DocumentListDataContract> GetDocuments(CriteriaFileDataContract cfdc)
        {
            if (cfdc == null)
                throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { Message = "Criteria cannot be null!" });

            var docs = eBookReadManager.Context.Documents
                                                .Include("File")
                                                .Include("DocumentType")
                                                .Where(d => d.File.Id == cfdc.FileId)
                                                .OrderBy(d => d.Name);

            return Translator.TranslateList<DocumentListDataContract, Document>(docs.ToList());
        }

        public DocumentDataContract GetDocument(CriteriaIdAndCultureDataContract cicdc)
        {
            if (cicdc == null)
                throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { Message = "Criteria cannot be null!" });

            var document = eBookReadManager.Context.Documents
                                        .Include("File")
                                        .Include("DocumentType")
                                        .Include("Fields")
                                        .FirstOrDefault(d => d.Id == cicdc.Id);
            if (document == null)
                throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { Message = String.Format("No document found for Id {0}", cicdc.Id) });

            return Translator.Translate<DocumentDataContract>(document, TranslateLevel.Infinite);
        }

        public DocumentDataContract GetDocumentByType(CriteriaIdAndCultureDataContract cicdc)
        {
            if (cicdc == null)
                throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { Message = "Criteria cannot be null!" });

            var document = eBookReadManager.Context.Documents
                                        .Include("File")
                                        .Include("DocumentType")
                                        .Include("Fields")
                                        .FirstOrDefault(d => d.DocumentType.Id == cicdc.Id);
            if (document == null)
                return null;

            return Translator.Translate<DocumentDataContract>(document, TranslateLevel.Infinite);
        }

        
        public DocumentTypeDataContract GetDocumentType(CriteriaDocumentDataContract cddc)
        {
           
            FileDataContract baseFile = new FileReadRepository().GetFile(cddc.FileId);

            FileMetaDataContract fileMeta = new FileReadRepository().GetMeta(cddc.FileId);

            ClientDataContract client = new ClientRepository().GetClient(baseFile.ClientId);

            CriteriaIdAndCultureDataContract cicdc = new CriteriaIdAndCultureDataContract
            {
                Culture = cddc.Culture
                ,
                Id = cddc.DocumentTypeId
            };

            return GetDocumentTypeFullHtml(cicdc, baseFile, fileMeta, client);
        }

        public DocumentTypeDataContract GetDocumentTypeFullHtml(CriteriaIdAndCultureDataContract cicdc, FileDataContract file, FileMetaDataContract fileMeta, ClientDataContract client)
        {
            if (cicdc == null)
                throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { Message = "Criteria cannot be null!" });

            var documentType = eBookReadManager.Context.DocumentTypes
                                .Include("Names")
                                .Include("Templates")
                                .Include("BlockTypes")
                                .Include("BlockTypes.Names")
                                .Include("BlockTypes.BlockTypeData")
                                .FirstOrDefault(d => d.Id == cicdc.Id && d.IsActive);
            if (documentType == null)
                throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { Message = String.Format("No active documentType found for Id {0}, Culture {1}", cicdc.Id, cicdc.Culture) });

            DocumentTypeDataContract dtdc = Translator.Translate<DocumentTypeDataContract>(documentType, TranslateLevel.Infinite, cicdc.Culture);
            if (dtdc.BlockTypes != null && dtdc.BlockTypes.Count > 0)
            {
                dtdc.BlockTypes = dtdc.BlockTypes.OrderBy(b => b.Name).ToList();
            }
            if (!string.IsNullOrEmpty(dtdc.Template))
            {
                XslCompiledTransform xct = new XslCompiledTransform(Config.DebugXslt());
                xct.Load(typeof(DocumentHtmlRenderer));
                DocumentHelper dh = new DocumentHelper
                {
                    Client = client
                    ,
                    File = file
                    ,
                    FileMeta = fileMeta
                    ,
                    Culture = CultureInfo.CreateSpecificCulture(cicdc.Culture)
                };
                XsltArgumentList arguments = new XsltArgumentList();
                arguments.AddExtensionObject("urn:DocumentHelper", dh);

                StringBuilder sb = new StringBuilder();
                XPathDocument source = new XPathDocument(new System.IO.StringReader(dtdc.Template));

                XmlWriterSettings settings = new XmlWriterSettings();
                settings.Encoding = Encoding.UTF8;
                settings.Indent = true;
                settings.OmitXmlDeclaration = true;
                XmlWriter xwr = XmlWriter.Create(sb, settings);

                xct.Transform(source, arguments, xwr);
                xwr.Close();
                dtdc.Template = sb.ToString();
            }
            return dtdc;
        }


        public List<DocumentTypeBaseDataContract> GetDocumentTypes(CriteriaIdAndCultureDataContract cicdc)
        {
            var documentTypes = eBookReadManager.Context.DocumentTypes
                                    .Include("Names")
                                    .Include("Group")
                                    .Where(a => a.IsActive);
            if (cicdc.Id!=Guid.Empty) documentTypes = documentTypes.Where(a => a.Group.Id == cicdc.Id);
            List<DocumentTypeBaseDataContract> documentTypeList = (documentTypes.Count() == 0) ? new List<DocumentTypeBaseDataContract>() : Translator.TranslateList<DocumentTypeBaseDataContract, DocumentType>(documentTypes.ToList(), TranslateLevel.NoChildren, cicdc.Culture);
            return documentTypeList.OrderBy(dt => dt.Name).ToList();
        }

        public List<DocumentGroupDataContract> GetDocumentGroups(CriteriaCultureDataContract ccdc)
        {
            var docGroups = eBookReadManager.Context.DocumentGroupSet
                                .Include("Names")
                                .Include("DocumentTypes")
                                .ToList();
            List<DocumentGroupDataContract> dgs = new List<DocumentGroupDataContract>();
            foreach (DocumentGroup dg in docGroups)
            {
                Names nm = dg.Names.FirstOrDefault(n => n.Culture == ccdc.Culture);
                if (nm != null && dgs.Count(g => g.Id == dg.Id) == 0 && dg.DocumentTypes.Count > 0)
                {
                    dgs.Add(new DocumentGroupDataContract
                    {
                        Id = dg.Id
                        ,
                        Name = nm.Description
                    });
                }
            }
            return dgs.OrderBy(d => d.Name).ToList();
        }

        public BlockTypeDataContract GetBlockType(CriteriaIdAndCultureDataContract cicdc)
        {
            throw new NotImplementedException();
        }

       

        protected override void RegisterTranslators()
        {

            Translator.RegisterEntityTranslator(new DocumentTranslator());
            Translator.RegisterEntityTranslator(new DocumentListTranslator());
            Translator.RegisterEntityTranslator(new DocumentFieldTranslator());

            Translator.RegisterEntityTranslator(new BlockTypeDataTranslator());
            Translator.RegisterEntityTranslator(new BlockTypeTranslator());
            Translator.RegisterEntityTranslator(new DocumentTypeBaseTranslator());
            Translator.RegisterEntityTranslator(new DocumentTypeTranslator());

            
        }
    }
}
