﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EY.com.eBook.Core;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.Core.EF;
using EY.com.eBook.API.BL.Translators;
using EY.com.eBook.Core.EF.Read;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using EY.com.eBook.API.Contracts.Proxies;
using EY.com.eBook.API.Contracts.Data.Cache;
using EY.com.eBook.Core.Data;
using System.ServiceModel;



namespace EY.com.eBook.API.BL.Read
{
    public class Repository : RepositoryBase
    {

        public List<RepositoryNodeDataContract> QueryFiles(CriteriaQueryFilesDataContract cqfdc)
        {
            IQueryable<RepositoryItem> qryItems = eBookReadManager.Context.RepositoryItems.Where(r => r.ClientId == cqfdc.ClientId && r.Deleted==false);
            if (cqfdc.StructureId.HasValue) qryItems = qryItems.Where(r => r.StructureId == cqfdc.StructureId.Value);
            if (cqfdc.PeriodStart.HasValue && cqfdc.PeriodEnd.HasValue) qryItems = qryItems.Where(r => r.StartDate <= cqfdc.PeriodEnd.Value && r.EndDate >= cqfdc.PeriodStart.Value);
            if (cqfdc.Status.HasValue) qryItems = qryItems.Where(r => r.Status == cqfdc.Status);

            return Translator.TranslateList<RepositoryNodeDataContract, RepositoryItem>(qryItems.ToList()).OrderByDescending(r => r.Text).ToList();
        }

        public List<RepositoryItemDataContract> QueryRepositoryItems(CriteriaQueryFilesDataContract cqfdc)
        {
            IQueryable<RepositoryItem> qryItems = eBookReadManager.Context.RepositoryItems.Where(r => r.ClientId == cqfdc.ClientId && r.Deleted == false);
            if (cqfdc.StructureId.HasValue) qryItems = qryItems.Where(r => r.StructureId == cqfdc.StructureId.Value);
            if (cqfdc.PeriodStart.HasValue && cqfdc.PeriodEnd.HasValue) qryItems = qryItems.Where(r => r.StartDate <= cqfdc.PeriodEnd.Value && r.EndDate >= cqfdc.PeriodStart.Value);
            if (cqfdc.Status.HasValue) qryItems = qryItems.Where(r => r.Status == cqfdc.Status);
            if (!String.IsNullOrEmpty(cqfdc.FileNamePartial)) qryItems = qryItems.Where(r => r.Name.Contains(cqfdc.FileNamePartial));


            List<RepositoryItemDataContract> lridc = new List<RepositoryItemDataContract>();
            var statusList = eBookReadManager.Context.RepositoryStatusItems.Where(r => r.Culture == cqfdc.Culture).GroupBy(r => r.Id).ToDictionary(r => r.Key, r => r.First());

            foreach (RepositoryItem ri in qryItems.ToList())
            {
                Guid? status = null;
                if (ri.Status.HasValue) { status = ri.Status; }

                RepositoryItemDataContract ridc = new RepositoryItemDataContract()
                {
                    Id = ri.ItemId,
                    ClientId = ri.ClientId,
                    FileName = ri.Name,
                    ContentType = ri.Contenttype,
                    DocstoreId = ri.DocstoreId,
                    PhysicalFileId = ri.PhysicalFileId,
                    Extension = ri.Extension,
                    PeriodStart = ri.StartDate,
                    PeriodEnd = ri.EndDate,
                    Status = status,
                    StatusText = status.HasValue && statusList.ContainsKey((Guid)status) ? statusList[(Guid)status].Description : null,
                    IconCls = status.HasValue ? string.Format("eb-rep-node-{0}", status.ToString().ToUpper()) : "eb-rep-node-pdf",
                    FilePath = GetFilePath(ri.ItemId)
                };

                lridc.Add(ridc);
            }

            return lridc;
        }

        public List<RepositoryItem> GetSignedFilesInStructure(Guid clientId, DateTime start, DateTime end)
        {
            Guid signedByClient = new Guid("FCF7A560-3D74-4341-A32B-310C8FA336CF");
            Guid signedByEY = new Guid("0BC2D5BF-C35B-491A-916E-5418F98B9E1F");
            return eBookReadManager.Context.RepositoryItems.Where(r =>
                r.ClientId == clientId
                && r.Deleted == false
                && (r.Status == signedByClient || r.Status == signedByEY)
            ).ToList();
        }

        public List<RepositoryItem> GetSignedFilesInPeriod(Guid clientId, DateTime start, DateTime end)
        {
            Guid signedByClient = new Guid("FCF7A560-3D74-4341-A32B-310C8FA336CF");
            Guid signedByEY = new Guid("0BC2D5BF-C35B-491A-916E-5418F98B9E1F");
            return eBookReadManager.Context.RepositoryItems.Where(r => 
                r.ClientId == clientId 
                && r.Deleted == false 
                && r.StartDate <= end 
                && r.EndDate >= start
                && (r.Status == signedByClient || r.Status == signedByEY)
            ).ToList();
        }

        public RepositoryStructuresDataContract GetStructureTree(string culture)
        {
            RepositoryStructuresDataContract structure = new RepositoryStructuresDataContract
            {
                PeriodStructure = new List<RepositoryNodeDataContract>()
                ,
                PermanentStructure = new List<RepositoryNodeDataContract>()
            };
            var folders = eBookReadManager.Context.RepositoryStructureSet.Include("Translations").ToList();


            foreach (RepositoryStructure struc in folders.Where(v => !v.ParentId.HasValue).OrderBy(v=>v.Order))
            {
                RepositoryNodeDataContract node = GetSubStructure(folders, struc, culture,string.Empty);
                if (struc.StructuralType == "PERMANENT")
                {
                    structure.PermanentStructure.Add(node);
                }
                else
                {
                    structure.PeriodStructure.Add(node);
                }
            }

            return structure;
        }

        public RepositoryNodeDataContract GetSubStructure(List<RepositoryStructure> folders, RepositoryStructure item, string culture, string path)
        {
            string pth = "";
            RepositoryNodeDataContract node = Translator.Translate<RepositoryNodeDataContract>(item, culture);
            node.Children = new List<RepositoryNodeDataContract>();
            node.Path = string.Format(@"{0}\{1}", path, node.Text);


            foreach (RepositoryStructure sub in folders.Where(f => f.ParentId.HasValue && f.ParentId == item.Id).OrderBy(f => f.Order))
            {
                node.Children.Add(GetSubStructure(folders,sub,culture,node.Path));
            }
            if (node.Children.Count == 0) node.Children = null;

            return node;
            
        }

        public bool DeleteFile(Guid id)
        {
            RepositoryItem item = eBookReadManager.Context.RepositoryItems.FirstOrDefault(r => r.ItemId == id);
            if (item != null)
            {
                item.Deleted = true;
                eBookReadManager.Context.SaveChanges();
                eBookReadManager.Context.RepositoryLinks.Where(r => r.ItemId == id).MarkAllObjectsForDeletion();
                eBookReadManager.Context.SaveChanges();
                return true;
            }
            return false;
        }

        public bool DeleteLink(RepositoryListItemDataContract rlidc)
        {
            RepositoryLink lnk = eBookReadManager.Context.RepositoryLinks
                                    .FirstOrDefault(r => r.ClienId == rlidc.ClientId
                                                    && r.ConnectionAccount == rlidc.ConnectionAccount
                                                    && r.ConnectionGuid == rlidc.ConnectionGuid
                                                    && r.FileId == rlidc.FileId
                                                    && r.ItemId == rlidc.ItemId);
            if (lnk != null)
            {
                eBookReadManager.Context.DeleteObject(lnk);
                eBookReadManager.Context.SaveChanges();
                return true;
            }
            return false;
        }

        public List<RepositoryListItemDataContract> GetFileLinks(CriteriaIdAndCultureDataContract cicdc)
        {
            return GetFileLinks(cicdc, false);
        }

        public List<RepositoryListItemDataContract> GetFileLinks(CriteriaIdAndCultureDataContract cicdc,bool enrichTextualPath)
        {
            EY.com.eBook.Core.EF.Write.File fle = eBookWriteManager.Context.Files.First(f=>f.Id==cicdc.Id);
            Guid clientId= fle.ClientId;
            Guid empty = Guid.Empty;
            List<RepositoryListItemDataContract> list = new List<RepositoryListItemDataContract>();
            List<RepositoryItem> items = eBookReadManager.Context.RepositoryItems.Where(r => r.ClientId == clientId && !r.Deleted &&
                                                                         (r.StartDate <= fle.EndDate && r.EndDate >= fle.StartDate)
                                                                    ).ToList();


            List<RepositoryStructure> repstruct = null;

            if (enrichTextualPath)
            {
                repstruct = eBookReadManager.Context.RepositoryStructureSet.Include("Translations").ToList();
            }
           
            

            List<Guid> ids = items.Select(r => r.ItemId).Distinct().ToList();


            foreach (RepositoryLink rl in eBookReadManager.Context.RepositoryLinks.Where(r => r.ClienId==clientId && (r.FileId==empty || r.FileId==cicdc.Id)))
            {
                if (ids.Contains(rl.ItemId))
                {
                    RepositoryItem item = items.First(r => r.ItemId == rl.ItemId);
                    RepositoryListItemDataContract rlidc = new RepositoryListItemDataContract
                    {
                        ClientId = rl.ClienId
                        ,
                        FileId = rl.FileId
                        ,
                        ItemId = rl.ItemId
                        ,
                        ConnectionType = rl.ConnectionType
                        ,
                        ConnectionAccount = rl.ConnectionAccount
                        ,
                        ConnectionDetailedPath = rl.ConnectionDetailedPath
                        ,
                        ConnectionGuid = rl.ConnectionGuid
                        ,
                        Description = item.Name
                        ,
                        ItemPath = string.Format("{0}/{1}{2}{3}", Config.RepositoryVirtualDir, clientId.ToPath("/"), item.ItemId.ToString(), item.Extension)
                    };
                    if (enrichTextualPath)
                    {
                        rlidc.ItemTextualPath = GetTextualPath(item.StructureId, rl.ClienId, rl.FileId, repstruct, cicdc.Culture);
                        if (rlidc.FileId != null)
                        {
                            rlidc.ItemTextualPath = string.Format("Files > {0} > {1}", fle.Name, rlidc.ItemTextualPath);
                        }

                        //rlidc.ItemTextualPath = string.Format("{0}/{1}", rlidc.ItemTextualPath, item.Name);
                    }
                    list.Add(rlidc);
                }
            }
            
            return list.OrderBy(l=>l.Description).ToList();
        
        }


        private string GetTextualPath(Guid structureId, Guid clientId, Guid fileId, List<RepositoryStructure> repstruct, string culture)
        {
            return GetTextualPath(structureId, clientId, fileId, repstruct, culture, null);
        }

        private string GetTextualPath(Guid structureId, Guid clientId, Guid fileId, List<RepositoryStructure> repstruct, string culture, string path)
        {
            if (string.IsNullOrEmpty(path)) path = "";

            RepositoryStructure struc = repstruct.FirstOrDefault(r => r.Id == structureId);
            RepositoryStructureTranslation trans = struc.Translations.FirstOrDefault(r=>r.Culture==culture);
            RepositoryStructureTranslation entrans = struc.Translations.FirstOrDefault(r=>r.Culture=="en-US");
            string txt = trans != null ? trans.Description : entrans != null ? entrans.Description : "KEY:" + struc.Key;

            path = string.Format("{0} > {1}", txt, path);

            if (struc.ParentId.HasValue)
            {
                path = GetTextualPath(struc.ParentId.Value, clientId, fileId, repstruct, culture, path);
            }

            return path;
        }


        
        public string GetFileLinks(CriteriaIdDataContract cidc)
        {
            Core.EF.Write.File file = eBookWriteManager.Context.Files.First(f => f.Id == cidc.Id);

            List<RepositoryItem> items = eBookReadManager.Context.RepositoryItems.Where(r => r.ClientId == file.ClientId && !r.Deleted
                                        && (r.StartDate <= file.EndDate && r.EndDate >= file.StartDate)).ToList();

            List<RepositoryLink> links = eBookReadManager.Context.RepositoryLinks.Where(l => l.ClienId == file.ClientId && (l.FileId == Guid.Empty || l.FileId == file.Id)).ToList();

            JObject resLinks = new JObject();
            JObject resItems = new JObject();


            foreach (RepositoryItem ri in items)
            {
                string name = !string.IsNullOrEmpty(ri.Prequel) ? string.Format("{0} {1}", ri.Prequel, ri.Name):ri.Name;

                resItems.Add("R"+ri.ItemId.ToString(), name);

            }
            foreach (RepositoryLink rl in links)
            {
                
                string conId = "L" + (rl.ConnectionGuid!=Guid.Empty ? rl.ConnectionGuid.ToString() : rl.ConnectionAccount);
                JProperty prop = resLinks.Property(conId);

                if (prop == null)
                {
                    resLinks.Add(conId, new JArray());
                }
                ((JArray)resLinks.Property(conId).Value).Add(rl.ItemId.ToString());
            }

            JObject obj = new JObject(
                new JProperty("items", resItems)
                , new JProperty("links", resLinks)
                );

            StringBuilder sb = new StringBuilder();
            System.IO.StringWriter sw = new System.IO.StringWriter(sb);
            
            JsonSerializer js = new JsonSerializer();
            js.Serialize(sw,obj);
            sw.Flush();
            return sb.ToString();
        }

        public List<RepositoryListItemDataContract> GetItemLinks(CriteriaItemLinksDataContract cildc)
        {
            List<RepositoryListItemDataContract> list = new List<RepositoryListItemDataContract>();
            if (!cildc.FileId.HasValue) return list;
            foreach (RepositoryLink rl in eBookReadManager.Context.RepositoryLinks.Where(r => r.ItemId == cildc.ItemId))
            {
                RepositoryListItemDataContract rlidc = new RepositoryListItemDataContract
                {
                    ClientId = rl.ClienId
                    ,
                    FileId = rl.FileId
                    ,
                    ItemId = rl.ItemId
                    ,
                    ConnectionType = rl.ConnectionType
                    ,
                    ConnectionAccount = rl.ConnectionAccount
                    ,
                    ConnectionDetailedPath = rl.ConnectionDetailedPath
                    ,
                    ConnectionGuid = rl.ConnectionGuid
                };
                Guid fileId = rl.FileId == Guid.Empty ? cildc.FileId.Value : rl.FileId;
                if (rl.ConnectionType == "ACCOUNT")
                {
                    AccountDescription ac = eBookReadManager.Context.AccountDescriptions.FirstOrDefault(a => a.InternalNr == rl.ConnectionAccount && a.FileId == cildc.FileId && a.Culture == cildc.Culture);
                    if (ac != null)
                    {
                        rlidc.Description = string.Format("{0} {1}", ac.InternalNr.Substring(0, 6), ac.Description);
                    }
                    else
                    {
                        rlidc.Description = rlidc.ConnectionAccount.Substring(0, 6);
                    }
                }
                else
                {
                    rlidc.Description = rl.ConnectionDetailedPath;
                }
                list.Add(rlidc);
            }
            return list;
        }

        public bool AddLink(RepositoryLinkDataContract rldc)
        {
            RepositoryLink rl = eBookReadManager.Context.RepositoryLinks.FirstOrDefault(r => r.ClienId == rldc.ClientId && r.FileId == rldc.FileId
                && r.ItemId == rldc.ItemId && r.ConnectionType == rldc.ConnectionType
                && r.ConnectionGuid == rldc.ConnectionGuid && r.ConnectionAccount == rldc.ConnectionAccount
                && r.ConnectionDetailedPath == rldc.ConnectionDetailedPath);

            if (!string.IsNullOrEmpty(rldc.ConnectionAccount))
            {
                if (rldc.ConnectionAccount.Contains(' '))
                {
                    rldc.ConnectionAccount = rldc.ConnectionAccount.Split(new char[] { ' ' })[0];
                }
                rldc.ConnectionAccount = rldc.ConnectionAccount.PadRight(12, '0');
            }
            if (rl == null)
            {
                rl = new RepositoryLink
                {
                    ClienId = rldc.ClientId
                    ,
                    FileId = rldc.FileId
                    ,
                    ItemId = rldc.ItemId
                    ,
                    ConnectionType = rldc.ConnectionType
                    ,
                    ConnectionGuid = rldc.ConnectionGuid
                    ,
                    ConnectionAccount = rldc.ConnectionAccount
                    ,
                    ConnectionDetailedPath = rldc.ConnectionDetailedPath
                };
                eBookReadManager.Context.AddToRepositoryLinks(rl);
                eBookReadManager.Context.SaveChanges();
            }
            return true;
        }

        public bool ReplaceRepositoryItem(CriteriaReplaceRepositoryItemDataContract crridc)
        {
            if (crridc == null)
            {
                throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { Message = "Network package is empty." });
            }
            RepositoryItem ri = eBookReadManager.Context.RepositoryItems.FirstOrDefault(r => r.ItemId == crridc.ItemId);
            if (ri == null)
            {
                return false;
            }

            try
            {
                string fname = string.Format("{0}{1}", crridc.NewFileId.ToString(), crridc.Extension);
                string orgPath = System.IO.Path.Combine(EY.com.eBook.Core.Config.UploadFolder, fname);
                string newPath = System.IO.Path.Combine(EY.com.eBook.Core.Config.RepositoryLocal, ri.ClientId.ToPath());

                string newFullPath = System.IO.Path.Combine(newPath, string.Format("{0}{1}", crridc.ItemId.ToString(), crridc.Extension));

                if (System.IO.File.Exists(newFullPath))
                    System.IO.File.Delete(newFullPath);

                if (!System.IO.File.Exists(orgPath)) throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { Message = "Original uploaded file could not be found. Adding cancelled" });
                if (!System.IO.Directory.Exists(newPath)) System.IO.Directory.CreateDirectory(newPath);

                System.IO.File.Move(orgPath, newFullPath);

                if (!String.IsNullOrEmpty(crridc.NewFileName))
                {
                    ri.Name = crridc.NewFileName;
                }
                ri.Contenttype = crridc.Contenttype;
                ri.Extension = crridc.Extension;
                eBookReadManager.Context.SaveChanges();

            }
            catch (Exception e)
            {
                return false;
            }

            return true;
        }

        public void UpdateFilePathRepositoryItem(Guid id, Guid physicalFileId)
        {
            RepositoryItem ri = eBookReadManager.Context.RepositoryItems.FirstOrDefault(r => r.ItemId == id);

            ri.PhysicalFileId = physicalFileId;
            eBookReadManager.Context.SaveChanges();
        }

        public Guid SaveRepositoryItem(CriteriaSaveRepositoryItemDataContract csridc)
        {
            RepositoryItem ri = eBookReadManager.Context.RepositoryItems.FirstOrDefault(r => r.ItemId == csridc.Item.Id);

            if (csridc.IsNew || (csridc.IsNew && ri != null && ri.PhysicalFileId != csridc.Item.PhysicalFileId))
            {
                // file moving
                string fname = string.Format("{0}{1}", csridc.Item.PhysicalFileId.ToString(), csridc.Item.Extension);
                string orgPath = System.IO.Path.Combine(EY.com.eBook.Core.Config.UploadFolder, fname);
                string newPath = System.IO.Path.Combine(EY.com.eBook.Core.Config.RepositoryLocal, csridc.Item.ClientId.ToPath());


                //if (csridc.Item.FileId.HasValue)
                //    newPath = System.IO.Path.Combine(newPath, csridc.Item.FileId.Value.ToString());
                string newFullPath = System.IO.Path.Combine(newPath, fname);

                if (!System.IO.File.Exists(newFullPath)) // if it exists, creation is in fact a copy.
                {
                    if (!System.IO.File.Exists(orgPath)) throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { Message = "Original uploaded file could not be found. Adding cancelled" });
                    if (!System.IO.Directory.Exists(newPath)) System.IO.Directory.CreateDirectory(newPath);

                    System.IO.File.Move(orgPath, newFullPath);
                }
            }
            /*
            if (csridc.Item.StructureId == new Guid("e611d384-4488-4371-aa2d-22224667aa8e"))
            {
                string clPath = System.IO.Path.Combine(EY.com.eBook.Core.Config.RepositoryLocal, csridc.Item.ClientId.ToPath());
                // add bank folder if none existing / What gives if all are already shown.
                int cnt = 0;
                var cbrepos = eBookReadManager.Context.ClientBankRepo.Where(c => c.ClientId == csridc.Item.ClientId).ToList();
                foreach (var metaVal in csridc.Item.Meta.Where(m => m.Value.ToLower() == "true"))
                {
                    if (cnt > 0)
                    {
                        // copy file ? 
                        var newFileId = Guid.NewGuid();
                        string fnewname = string.Format("{0}{1}", newFileId.ToString(), csridc.Item.Extension);
                        string fname = string.Format("{0}{1}", csridc.Item.PhysicalFileId.ToString(), csridc.Item.Extension);
                        System.IO.File.Copy(System.IO.Path.Combine(clPath, fname), System.IO.Path.Combine(clPath, fnewname));
                        csridc.Item.PhysicalFileId = newFileId;
                    }
                    ClientBankRepo cbrep = cbrepos.FirstOrDefault(c => c.BankAccount == metaVal.Id);
                    if (cbrep==null)
                    {
                        cbrep = new ClientBankRepo
                        {
                            Id=Guid.NewGuid(),
                            ClientId=csridc.Item.ClientId,
                            BankAccount = metaVal.Id,
                            DateCreated  = DateTime.Now
                        };
                        eBookReadManager.Context.AddToClientBankRepo(cbrep);
                        eBookReadManager.Context.SaveChanges();
                    }
                    ri = new RepositoryItem
                    {
                        ItemId = Guid.NewGuid()
                        ,
                        ClientId = csridc.Item.ClientId
                        ,
                        PhysicalFileId = csridc.Item.PhysicalFileId
                        ,
                        StructureId = cbrep.Id
                    };
                    csridc.Item.StructureId = cbrep.Id;
                    UpdateContents(ref ri, csridc);
                    eBookReadManager.Context.AddToRepositoryItems(ri);
                    eBookReadManager.Context.SaveChanges();
                    cnt++;
                }
                // returns last ri for now

                return ri.ItemId;
            }
            else
            {
             * */
                if (csridc.IsNew || (csridc.IsNew && ri != null && ri.PhysicalFileId != csridc.Item.PhysicalFileId))
                {
                    if ((ri != null && ri.PhysicalFileId != csridc.Item.PhysicalFileId))
                    {
                        csridc.Item.Id = Guid.NewGuid();
                    }
                    // copy file from temp folder to client repos folder
                  

                    ri = new RepositoryItem
                    {
                        ItemId = csridc.Item.Id
                        ,
                        ClientId = csridc.Item.ClientId
                        ,
                        PhysicalFileId = csridc.Item.PhysicalFileId
                        ,
                        Name = csridc.Item.FileName
                    };
                    eBookReadManager.Context.AddToRepositoryItems(ri);



                }
                else
                {
                    ri = eBookReadManager.Context.RepositoryItems.First(r => r.ItemId == csridc.Item.Id);
                }
                UpdateContents(ref ri, csridc);
                eBookReadManager.Context.SaveChanges();
                return ri.ItemId;
            //}
            
           // if (ri.EntityState == System.Data.EntityState.Detached)
               
            
        }

        private void UpdateContents(ref RepositoryItem ri, CriteriaSaveRepositoryItemDataContract csridc)
        {
            csridc.Item.PeriodEnd = csridc.Item.PeriodEnd.Value.AddMinutes((-csridc.Item.TimeZoneOffset) + 10);
            csridc.Item.PeriodStart = csridc.Item.PeriodStart.Value.AddMinutes((-csridc.Item.TimeZoneOffset) + 10);

            var end = new DateTime(csridc.Item.PeriodEnd.Value.Year, csridc.Item.PeriodEnd.Value.Month, csridc.Item.PeriodEnd.Value.Day);
            var start = new DateTime(csridc.Item.PeriodStart.Value.Year, csridc.Item.PeriodStart.Value.Month, csridc.Item.PeriodStart.Value.Day);


            // check if structure id is Coda Volmacht


            ri.StructureId = csridc.Item.StructureId;
            ri.Meta = CoreHelper.SerializeToString(csridc.Item.Meta);
            ri.EndDate = end;
            ri.StartDate = start;
            //  ri.StartDate = csridc.Item.PeriodStart.Value;
            ri.Name = csridc.Item.FileName;
            ri.Prequel = csridc.Item.Prequel;
            ri.Status = csridc.Item.Status;
            ri.Extension = csridc.Item.Extension;
            ri.Contenttype = csridc.Item.ContentType;
        }

        public List<RepositoryNodeDataContract> GetChildren(CriteriaRepositoryChildrenDataContract crcdc)
        {
            // CHANGE: ONLY FILES USED, STRUCTURE MOVED TO BULK LOAD (?)

            if (crcdc.PeriodEnd.HasValue && !crcdc.PeriodStart.HasValue) crcdc.PeriodStart = DateTime.MinValue;
            if (!crcdc.PeriodEnd.HasValue && crcdc.PeriodStart.HasValue) crcdc.PeriodEnd = DateTime.MaxValue;


            //if(crcdc.PeriodStart.HasValue) crcdc.PeriodStart= new DateTime(crcdc.PeriodStart.Value.Year ,crcdc.PeriodStart.Value.Month, crcdc.PeriodStart.Value.Day);
            //if (crcdc.PeriodEnd.HasValue) crcdc.PeriodEnd = new DateTime(crcdc.PeriodEnd.Value.Year, crcdc.PeriodEnd.Value.Month, crcdc.PeriodEnd.Value.Day);

            List<RepositoryNodeDataContract> lst = new List<RepositoryNodeDataContract>();
            if (!crcdc.StructureId.HasValue)
            {
                // ROOT NODES, GET PERMANENT ROOT FOLDERS
                // NOT USED ANY LONGER > BULK LOAD STRUCTURES
                var structs = eBookReadManager.Context.RepositoryStructureSet
                                    .Include("Translations")
                                    .Where(r => r.ParentId == null && r.StructuralType == "PERMANENT");
                lst = Translator.TranslateList<RepositoryNodeDataContract, RepositoryStructure>(structs.ToList(), crcdc.Culture).OrderBy(r=>r.Order).ThenBy(r => r.Text).ToList();

            }
            else
            {
                // OTHERS STILL USED?
                switch (crcdc.Key)
                {
                    case "PERIODCONTAINER":
                        if (crcdc.PeriodStart.HasValue && crcdc.PeriodEnd.HasValue)
                        {
                            lst = new List<RepositoryNodeDataContract> {
                                new RepositoryNodeDataContract
                                   {
                                       AllowChildren = false
                                       ,
                                       AllowDrag = false
                                       ,
                                       Class = "eBook-Repository-node-period-folder"
                                       ,
                                       Id = string.Format("{0}-{1}", crcdc.PeriodStart.Value.ToShortDateString(), crcdc.PeriodEnd.Value.ToShortDateString()).DeterministicGuid()
                                       ,
                                       Leaf = false
                                       ,
                                       Text = string.Format("{0}-{1}", crcdc.PeriodStart.Value.ToShortDateString(), crcdc.PeriodEnd.Value.ToShortDateString())
                                       ,
                                       Key = "PERIOD_ITEM"
                                       ,
                                       DateType = null
                                       ,
                                       StatusType = null
                                       ,
                                       StructuralType = "PERIOD_ITEM"
                                       ,
                                       Files = false
                                       ,
                                       PeriodStart = crcdc.PeriodStart.Value
                                       ,
                                       PeriodEnd =  crcdc.PeriodEnd.Value
                                   }
                            };
                        }
                        else
                        {
                            var periods = (from r in eBookReadManager.Context.RepositoryItems
                                           where r.ClientId == crcdc.ClientId && !r.Deleted
                                                && eBookReadManager.Context.RepositoryStructureSet.Count(rs => rs.StructuralType == "PERIOD" && rs.Id == r.StructureId) == 1
                                           orderby r.StartDate descending
                                           select new { r.StartDate, r.EndDate }
                                           ).Distinct();

                            lst = (from p in periods.ToList()
                                   select new RepositoryNodeDataContract
                                   {
                                       AllowChildren = false
                                       ,
                                       AllowDrag = false
                                       ,
                                       Class = "eBook-Repository-node-period-folder"
                                       ,
                                       Id = string.Format("{0}-{1}", p.StartDate.ToShortDateString(), p.EndDate.ToShortDateString()).DeterministicGuid()
                                       ,
                                       Leaf = false
                                       ,
                                       Text = string.Format("{0}-{1}", p.StartDate.ToShortDateString(), p.EndDate.ToShortDateString())
                                       ,
                                       Key = "PERIOD_ITEM"
                                       ,
                                       DateType = null
                                       ,
                                       StatusType = null
                                       ,
                                       StructuralType = "PERIOD_ITEM"
                                       ,
                                       Files = false
                                       ,
                                       PeriodStart = p.StartDate
                                       ,
                                       PeriodEnd = p.EndDate
                                   }).ToList();
                        }
                        break;
                                      
                                        
                    case "FILECONTAINER":

                        var fcStructs = eBookReadManager.Context.Files
                                        .Include("Client")
                                        .Where(r => r.Client.Id == crcdc.ClientId && !r.Deleted && !r.MarkDelete);

                        if (crcdc.FileId.HasValue)
                        {
                            fcStructs = fcStructs.Where(f => f.Id==crcdc.FileId.Value);
                        }
                        lst = Translator.TranslateList<RepositoryNodeDataContract, File>(fcStructs.ToList(), crcdc.Culture).OrderBy(r => r.Text).ToList();
                        break;
                
                    case "PERIOD_ITEM":
                        var structs = eBookReadManager.Context.RepositoryStructureSet
                                     .Include("Translations")
                                     .Where(r => r.ParentId == null && r.StructuralType == "PERIOD");
                        lst = Translator.TranslateList<RepositoryNodeDataContract, RepositoryStructure>(structs.ToList(), crcdc.Culture).OrderBy(r => r.Order).ThenBy(r => r.Text).ToList();
                        break;
                    case "CODA_PROCURATION":
                        // Subfo
                        // APARTE TABEL KLANT BANKREKENINGEN - CODA VOLMACHT
                        // ClientId, IBAN, BIC, Status, ...
                        // LINKED TABLE voor die commentaren

                        // Query op tabel CodaVolmacht met clientId en status X
                        // omzetten naar RepositoryNode

                        // var structs = eBookReadManager.Context.ClientBank.Where(c=>c.ClientId=clientId && c.Status ="").ToList();
                        //lst = Translator.TranslateList<RepositoryNodeDataContract, ClientBank>(structs.ToList()).OrderBy(r => r.Text).ToList();
                        // RepositoryNodeDataContract => KEY == CODAPROC_ + bankrekening nummer
                        // META Config => heeft verwijzing naar comment feature


                        lst = eBookReadManager.Context.ClientBankRepo.Where(c => c.ClientId == crcdc.ClientId).ToList().Select(c => new RepositoryNodeDataContract
                        {
                            AllowChildren = false
                            ,
                            AllowDrag = false
                            ,
                            Class = "eBook-Repository-node-CODABANK"
                            ,
                            Id = c.Id
                            ,
                            Leaf = false
                            ,
                            Text = c.BankAccount
                            ,
                            Key = "CODABANK_" + c.BankAccount
                            ,
                            DateType = "INFINITEPERIOD"
                            ,
                            StatusType = "CODABANK"
                            ,
                            StructuralType = "PERMANENT"
                            ,
                            Files = true
                            ,
                            AllowLinks = true
                            , // TODO == COMMENTS
                            MetaConfig = new RepositoryMetaConfigDataContract
                            {
                                FeatureType = "CODA_BANK_COMMENT",
                                Fields = new List<RepositoryMetaFieldConfigDataContract>
                                {
                                   new RepositoryMetaFieldConfigDataContract {
                                       Id="BankComments",
                                       FieldType="Comments",
                                       Attributes = new List<RepositoryMetaFieldAttribute>(),
                                       Names= new RepositoryMetaTranslationDataContract {
                                           Nl="Opmerkingen",
                                           Fr="Commentaires",
                                           En="Comments"
                                       }, Required =false
                                   }
                                }
                              
                            }
                            ,
                            Order = 0
                            ,
                            DocstoreAccess = false
                            ,
                            Dynamic=true
                        }).ToList();
                        crcdc.ShowFiles = true;
                        break;
                    default:
                        var defstructs = eBookReadManager.Context.RepositoryStructureSet
                                        .Include("Translations")
                                        .Where(r => r.ParentId == crcdc.StructureId.Value);
                        lst = Translator.TranslateList<RepositoryNodeDataContract, RepositoryStructure>(defstructs.ToList(), crcdc.Culture).OrderBy(r => r.Order).ThenBy(r => r.Text).ToList();
                        break;
                }

                if (crcdc.ShowFiles)
                {
                   // Coda volmachten met ID van ClientBank als StructureId
                        var fls = eBookReadManager.Context.RepositoryItems.Where(r => r.ClientId == crcdc.ClientId && r.StructureId == crcdc.StructureId && !r.Deleted);
                        if (crcdc.PeriodStart.HasValue && crcdc.PeriodEnd.HasValue)
                        {
                            fls = fls.Where(r => r.StartDate <= crcdc.PeriodEnd.Value && r.EndDate >= crcdc.PeriodStart.Value);
                        }
                        List<RepositoryNodeDataContract> files = Translator.TranslateList<RepositoryNodeDataContract, RepositoryItem>(fls.ToList(), crcdc.Culture).OrderBy(r => r.Text).ToList(); ;
                        lst.AddRange(files);

                    // TODO: Cache statuslist per culture ?
                        var statusList = eBookReadManager.Context.RepositoryStatusItems.Where(r => r.Culture == crcdc.Culture).GroupBy(r => r.Id).ToDictionary(r => r.Key, r => r.First());
                        lst.ForEach(f =>
                        {
                            if (f.Item != null && f.Item.Status.HasValue && statusList.ContainsKey(f.Item.Status.Value))
                            {
                                var statusText = statusList[f.Item.Status.Value].Description;
                                f.Text = string.Format("{0} ({1})", f.Text, statusText);
                                f.Item.StatusText = statusText;
                            }
                        });
                }
            }


            return lst;
        }

        public RepositoryNodeDataContract GetFileNode(CriteriaIdAndCultureDataContract cicdc)
        {
            RepositoryItem ri = eBookReadManager.Context.RepositoryItems.FirstOrDefault(r => r.ItemId == cicdc.Id);
            if (ri != null)
            {
                return Translator.Translate<RepositoryNodeDataContract>(ri);
            }
            return null;
        }

        public string GetFilePath(Guid id)
        {
            RepositoryItem ri = eBookReadManager.Context.RepositoryItems.FirstOrDefault(r => r.ItemId == id);
            if (ri != null)
            {
                string path = System.IO.Path.Combine(Config.RepositoryLocal, ri.ClientId.ToPath());
                
                path = System.IO.Path.Combine(path, string.Format("{0}{1}", ri.ItemId.ToString(), ri.Extension));
                return path;
            }
            return null;
        }

        public string GetFileName(Guid id)
        {
            RepositoryItem ri = eBookReadManager.Context.RepositoryItems.FirstOrDefault(r => r.ItemId == id);
            if (ri != null)
            {
                return ri.Name;
            }
            return null;
        }

        public string GetFileExtension(Guid id)
        {
            RepositoryItem ri = eBookReadManager.Context.RepositoryItems.FirstOrDefault(r => r.ItemId == id);
            if (ri != null)
            {
                return ri.Extension;
            }
            return null;
        }

        public List<GuidListItemDataContract> GetStatusList(CriteriaStatusDataContract csdc)
        {
            List<GuidListItemDataContract> list = new List<GuidListItemDataContract>(); 
            foreach (RepositoryStatusType rst in eBookReadManager.Context.RepositoryStatusTypes.Where(r => r.Id == csdc.Id).OrderBy(r=>r.Order))
            {
                var item = new GuidListItemDataContract { Id = rst.ItemId };
                foreach (RepositoryStatusItem rsi in eBookReadManager.Context.RepositoryStatusItems.Where(r => r.Id == rst.ItemId))
                {
                    switch (rsi.Culture)
                    {
                        case "nl-BE":
                            item.NL = rsi.Description;
                            break;
                        case "fr-FR":
                            item.FR = rsi.Description;
                            break;
                        case "en-US":
                            item.EN = rsi.Description;
                            break;
                    }
                }
                list.Add(item);
            }
            return list;
        }

        public Boolean GthCheckEngagementLetters(CriteriaRepositoryChildrenDataContract crcdc) {
            List<RepositoryNodeDataContract> lrndc = GetChildren(crcdc);

            if (lrndc != null) {
                foreach (RepositoryNodeDataContract rndc in lrndc) {
                    var metaGth = rndc.Item.Meta.FirstOrDefault(x => x.Id == "GTHValidated");
                }
            }
            return true;
        }

        public Boolean PersonHasGTHrole(CriteriaIdDataContract cidc) {
            var roles = eBookReadManager.Context.PersonRoleSet.Where(x => x.PersonId == cidc.Id && x.Role.Contains("GTH")).ToList();
            if (roles.Count > 0)
            {
                return true;
            }
            else {
                return false;
            }
        }

        public void LinkToDocstore(CriteriaLinkRepositoryItemToDocstoreDataContract clritddc) {
            RepositoryItem r = eBookReadManager.Context.RepositoryItems.FirstOrDefault(x => x.ItemId == clritddc.RepositoryItemId);
            if (r != null) {
                r.DocstoreId = clritddc.DocstoreItemId;
                r.Status = new Guid("c0009211-8936-49f0-a2b4-e0f70a0a2a96");
                eBookReadManager.Context.SaveChanges();
            }
        }

        protected override void RegisterTranslators()
        {
            Translator.RegisterEntityTranslator(new RepositoryStructureNodeTranslator());
            Translator.RegisterEntityTranslator(new RepositoryStructureFileNodeTranslator());
            Translator.RegisterEntityTranslator(new RepositoryItemNodeTranslator());
            //nothing yet
        }
    }
}
