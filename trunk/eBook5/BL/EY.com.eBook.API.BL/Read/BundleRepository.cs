﻿using System;
using System.Collections.Generic;
using System.Linq;
//using System.Linq.Dynamic;
using System.Text;
using EY.com.eBook.Core;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.API.Contracts.Data.Cache;
using EY.com.eBook.Core.EF;
using EY.com.eBook.API.Contracts.Data.Logic;
using EY.com.eBook.Core.EF.Read;
using System.Data.Objects;
using EY.com.eBook.Core.EntityTranslation;
using EY.com.eBook.API.BL.Translators;
using System.ServiceModel;
using EY.com.eBook.Core.Data;
using System.Linq.Expressions;
using System.Xml.Linq;

namespace EY.com.eBook.API.BL.Read
{
    public class BundleRepository : RepositoryBase
    {

        public List<BundleListDataContract> GetBundles(CriteriaIdAndCultureDataContract cicdc)
        {
            return (from b in eBookReadManager.Context.Bundles.Where(b => b.FileId == cicdc.Id && !b.Deleted).ToList()
                    select new BundleListDataContract
                    {
                        Id = b.Id
                        ,
                        Culture = b.Culture
                        ,
                        Name = b.Name
                    }).ToList();
        }

        /// <summary>
        /// Retrieves all bundles that, through their template, aren't linked to a service
        /// </summary>
        /// <param name="cicdc">CriteriaIdAndCultureDataContract</param>
        /// <returns>List<BundleListDataContract> </returns>
        public List<BundleListDataContract> GetGenericBundles(CriteriaIdAndCultureDataContract cicdc)
        {
            List<Bundle> filebundles = eBookReadManager.Context.Bundles.Where(b => b.FileId == cicdc.Id && !b.Deleted).ToList();
            List<Guid> serviceBundleTemplates = eBookReadManager.Context.ServiceBundleTemplates.Select(s => s.TemplateId).Distinct().ToList();

            //get bundles that have no record in the service template link table
            filebundles = filebundles.Where(b => !serviceBundleTemplates.Contains(b.TemplateId)).ToList(); ;

            return (from b in filebundles
                    select new BundleListDataContract
                    {
                        Id = b.Id
                        ,
                        Culture = b.Culture
                        ,
                        Name = b.Name
                    }).ToList();
        }

        public List<BundleListDataContract> GetServiceBundles(CriteriaFileServiceDataContract cfsdc)
        {
            List<Bundle> filebundles = eBookReadManager.Context.Bundles.Where(b => b.FileId == cfsdc.FileId && !b.Deleted).ToList();
            List<ServiceBundleTemplate> serviceBundleTemplates = eBookReadManager.Context.ServiceBundleTemplates.ToList();

            if (cfsdc.ServiceId == new Guid("BD7C2EAE-8500-4103-8984-0131E73D07FA") || cfsdc.ServiceId == new Guid("60CA9DF9-C549-4A61-A2AD-3FDB1705CF55")) //biztax, get both auto and manual 
            {
                serviceBundleTemplates = serviceBundleTemplates.Where(b => b.ServiceId == new Guid("BD7C2EAE-8500-4103-8984-0131E73D07FA") || b.ServiceId == new Guid("60CA9DF9-C549-4A61-A2AD-3FDB1705CF55")).ToList();
            }
            else
            {
                serviceBundleTemplates = serviceBundleTemplates.Where(b => b.ServiceId == cfsdc.ServiceId).ToList();
            }

            filebundles = filebundles.Where(b => serviceBundleTemplates.Select(s => s.TemplateId).Distinct().Contains(b.TemplateId)).ToList();

            return (from b in filebundles
                    join s in serviceBundleTemplates on b.TemplateId equals s.TemplateId
                    select new BundleListDataContract
                    {
                        Id = b.Id
                        ,
                        Culture = b.Culture
                        ,
                        Name = b.Name
                        ,
                        Service = s.ServiceId
                    }).ToList();
        }

        public List<BundleTemplateListDataContract> GetBundleTemplates(CriteriaCultureDataContract ccdc)
        {
            var lst = from bn in eBookReadManager.Context.BundleTemplates.Where(b => b.Culture == ccdc.Culture)
                      select new BundleTemplateListDataContract
                      {
                          Id = bn.Id
                          ,
                          Culture = bn.Culture
                          ,
                          Name = bn.Name
                      };
            return lst.ToList();
        }

        public List<BundleTemplateListDataContract> GetGenericBundleTemplates(CriteriaCultureDataContract ccdc)
        {
            List<BundleTemplate> bundleTemplates = eBookReadManager.Context.BundleTemplates.Where(b => b.Culture == ccdc.Culture).ToList();
            List<Guid> serviceBundleTemplates = eBookReadManager.Context.ServiceBundleTemplates.Select(s => s.TemplateId).Distinct().ToList();


            bundleTemplates = bundleTemplates.Where(b => !serviceBundleTemplates.Contains(b.Id)).ToList();

            var lst = from bt in bundleTemplates
                      select new BundleTemplateListDataContract
                      {
                          Id = bt.Id
                          ,
                          Culture = bt.Culture
                          ,
                          Name = bt.Name
                      };
            return lst.ToList();
        }

        public List<BundleTemplateListDataContract> GetServiceBundleTemplates(CriteriaServiceCultureDataContract cscdc)
        {
            var serviceBndls = eBookReadManager.Context.ServiceBundleTemplates.Where(b => b.ServiceId == cscdc.ServiceId).Select(s=>s.TemplateId).Distinct().ToList();
            var bndls = eBookReadManager.Context.BundleTemplates.Where(b => b.Culture == cscdc.Culture).ToList();
            bndls = bndls.Where(b => serviceBndls.Contains(b.Id)).ToList();
            var lst = from bn in bndls
                      select new BundleTemplateListDataContract
                      {
                          Id = bn.Id
                          ,
                          Culture = bn.Culture
                          ,
                          Name = bn.Name
                      };
            return lst.ToList();
        }

        public BundleDataContract CreateBundle(CriteriaCreateBundleDataContract ccbdc)
        {
            BundleTemplate bt = eBookReadManager.Context.BundleTemplates.FirstOrDefault(b => b.Id == ccbdc.TemplateId);
            if (bt == null) return null;

            List<IndexItemBaseDataContract> lst = CoreHelper.DeserializeFromString<List<IndexItemBaseDataContract>>(bt.Contents);

            lst = BundleFy(lst,ccbdc.FileId,ccbdc.Culture);


            Bundle bundle = new Bundle {
                  Id = Guid.NewGuid()
                , Name = ccbdc.Name
                , Deleted = false
                , Draft= true
                , RectoVerso= true
                , Contents= CoreHelper.SerializeToString(lst)
                , Culture = ccbdc.Culture
                , FileId= ccbdc.FileId
                , HeaderFooterType = bt.Type
                , TemplateId = ccbdc.TemplateId
            };
            eBookReadManager.Context.AddToBundles(bundle);
            eBookReadManager.Context.SaveChanges();

            return new BundleDataContract
            {
                Id = bundle.Id
                ,
                Name = ccbdc.Name
                ,
                Deleted = false
                ,
                Draft = true
                ,
                RectoVerso = true
                ,
                Contents = lst
                ,
                Culture = ccbdc.Culture
                ,
                FileId = ccbdc.FileId
                ,
                HeaderFooterType = bundle.HeaderFooterType
                ,
                TemplateId = ccbdc.TemplateId
            };
        }

        /// <summary>
        /// Create an annual accounts deliverable, which can only be created if the 5, to be added, pdf's exist.
        /// </summary>
        /// <param name="ciapdc">fileId</param>
        /// <returns></returns>
        public BundleDataContract CreateAnnualAccountsDeliverable(CriteriaCreateBundlePersonDataContract ccbpdc)
        {
            var oService = new { id = new Guid("1ABF0836-7D7B-48E7-9006-F03DB97AC28B"), name = "Annual Accounts" };
            AnnualAccountRequiredFilesDataContract aarfdc = GetAnnualAccountsRequiredFiles(ccbpdc.FileId);

            //output
            if (aarfdc.FileTypesMissing.Count > 0 || aarfdc.FileTypesDuplicates.Count > 0) //If any files are missing, output reason
            {
                string message = "<p><b>Cannot create an Annual Accounts bundle.<b></p>";
                if (aarfdc.FileTypesMissing.Count > 0)
                {
                    message += "<p><br>Please make sure the following files are available within their respective folders.</p><ul>";
                    foreach (string fileTypeMissing in aarfdc.FileTypesMissing)
                    {
                        message += "<li>" + fileTypeMissing + "</li>";
                    }
                    message += "</ul>";
                }
                if (aarfdc.FileTypesDuplicates.Count > 0)
                {
                    message += "<p><br>Please make sure the following folder does not contain <u>Signed</u> duplicates.</p><ul>";
                    foreach (string fileTypeDuplicates in aarfdc.FileTypesDuplicates)
                    {
                        message += "<li>" + fileTypeDuplicates + "</li>";
                    }
                    message += "</ul>";
                }

                throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { Message = message });
            }
            else //Generate bundle
            {
                BundleTemplate bt = eBookReadManager.Context.BundleTemplates.FirstOrDefault(b => b.Id == ccbpdc.TemplateId);
                if (bt == null) return null;

                List<IndexItemBaseDataContract> lst = CoreHelper.DeserializeFromString<List<IndexItemBaseDataContract>>(bt.Contents);

                foreach (RepositoryItemDataContract fileFound in aarfdc.FilesFound)
                {

                    PDFDataContract pdc = new PDFDataContract
                    {
                        Id = fileFound.Id,
                        EndsAt = 0,
                        FooterConfig = null,
                        HeaderConfig = null,
                        NoDraft = true,
                        StartsAt = 0,
                        iTextTemplate = null,
                        IconCls = null,
                        ShowInIndex = true,
                        Delete = false,
                        Locked = false,
                        Title = fileFound.FileName,
                        Type = "PDFPAGE",
                        FromPage = null,
                        ToPage = null,
                        Origin = null,
                        Outlines = null,
                        Pages = 0,
                        PDFLocation = null,
                        ItemId = fileFound.Id,
                        IsTempRender = false
                    };

                    lst.Add(pdc);
                }

                lst = BundleFy(lst, ccbpdc.FileId, ccbpdc.Culture);


                Bundle bundle = new Bundle
                {
                    Id = Guid.NewGuid()
                    ,
                    Name = "Annual Accounts"
                    ,
                    Deleted = false
                    ,
                    Draft = true
                    ,
                    RectoVerso = true
                    ,
                    Contents = CoreHelper.SerializeToString(lst)
                    ,
                    Culture = ccbpdc.Culture
                    ,
                    FileId = ccbpdc.FileId
                    ,
                    HeaderFooterType = bt.Type
                    ,
                    TemplateId = ccbpdc.TemplateId
                };
                eBookReadManager.Context.AddToBundles(bundle);
                eBookReadManager.Context.SaveChanges();

                //create log entry
                UpdateFileServiceDataContract ufsdc = new UpdateFileServiceDataContract
                {
                    ActivePerson = ccbpdc.ActivePerson,
                    Status = 1,
                    EncodedActivePerson = ccbpdc.EncodedActivePerson,
                    FileId = ccbpdc.FileId,
                    ServiceId = oService.id,
                    Actions = "status,finalGenerated"
                };

                EY.com.eBook.API.BL.Write.FileServiceRepository fileServiceRepository = new EY.com.eBook.API.BL.Write.FileServiceRepository();
                fileServiceRepository.LogFileServiceActions(ufsdc);

                //Create Annual Accounts record
                CriteriaFileAnnualAcountsDataContract cfaadc = new CriteriaFileAnnualAcountsDataContract{
                    FileId = ufsdc.FileId
                };

                BL.Write.AnnualAccountsRepository aar = new BL.Write.AnnualAccountsRepository();
                aar.CreateAnnualAccounts(cfaadc);

                return new BundleDataContract
                {
                    Id = bundle.Id
                    ,
                    Name = bundle.Name
                    ,
                    Deleted = false
                    ,
                    Draft = false
                    ,
                    RectoVerso = true
                    ,
                    Contents = lst
                    ,
                    Culture = bundle.Culture
                    ,
                    FileId = bundle.FileId
                    ,
                    HeaderFooterType = bundle.HeaderFooterType
                    ,
                    TemplateId = bundle.TemplateId
                };
            }
        }

        public AnnualAccountRequiredFilesDataContract GetAnnualAccountsRequiredFiles(Guid fileId)
        {
            //vars
            var oService = new { id = new Guid("1ABF0836-7D7B-48E7-9006-F03DB97AC28B"), name = "Annual Accounts" };
            var oSignedByClient = new { id = new Guid("FCF7A560-3D74-4341-A32B-310C8FA336CF"), status = "Signed by Client" };
            var oSignedByEY = new { id = new Guid("0BC2D5BF-C35B-491A-916E-5418F98B9E1F"), status = "Signed by EY" };
            var oSignedAccMemo = new { id = new Guid("B1362A5D-924A-41E1-B505-92D984EE40C9"), filetype = "Accounting Memorandum" };
            var oSignedAccTaxMemo = new { id = new Guid("B9069EA7-6A32-41AF-B0B2-F66DB656760B"), filetype = "Accounting/Tax Memorandum" };
            var oSignedRepLetter = new { id = new Guid("d9bb468c-9886-463d-ac22-3d0c75f9f2db"), filetype = "Representation Letter" };
            var oSignedStatutoryAccounts = new { id = new Guid("08258e0e-99e3-402f-84c2-148383d5255e"), filetype = "Statutory Accounts" };
            var oSignedMinutesGeneralAssemble = new { id = new Guid("f18165cb-748f-430a-b2b7-88df26fdd8ba"), filetype = "Minutes General Assemble" };
            var oSignedEngagementAgreement = new { id = new Guid("32F0391E-E0BD-41F9-A9C1-C04EAC3C8CB1"), filetype = "Engagement Agreement" };
            var filesNeeded = new[] { oSignedRepLetter, oSignedStatutoryAccounts, oSignedMinutesGeneralAssemble, oSignedEngagementAgreement }; //apart from memo's
            List<string> fileTypesMissing = new List<string>();
            List<string> fileTypesDuplicates = new List<string>();
            List<RepositoryItem> memosFound = new List<RepositoryItem>();
            List<RepositoryItem> filesFoundWithoutPeriod = new List<RepositoryItem>();
            List<RepositoryItem> filesFoundStructureType = new List<RepositoryItem>();
            List<RepositoryItem> filesFound = new List<RepositoryItem>();

            //Get File service
            EY.com.eBook.Core.EF.Write.FileService fs = eBookWriteManager.Context.FileServices.FirstOrDefault(f => f.FileId == fileId && f.ServiceId == oService.id);
            //Get file
            File file = eBookReadManager.Context.Files.Include("Client").Where(f => f.Id == fileId).FirstOrDefault();

            //retrieval: "Signed by EY" Accountancy Memorandum OR signed by EY Accountancy/Tax Memorandum file (as one of them files is enough)
            memosFound = eBookReadManager.Context.RepositoryItems.Where(r => r.ClientId == file.Client.Id && r.StartDate == file.StartDate && r.EndDate == file.EndDate && r.Deleted == false && (r.StructureId == oSignedAccMemo.id || r.StructureId == oSignedAccTaxMemo.id) && r.Contenttype == "application/pdf" && r.Status == oSignedByEY.id).ToList();

            //validation
            if (memosFound.Count == 0) //none found
            {
                fileTypesMissing.Add(oSignedAccMemo.filetype + " or " + oSignedAccTaxMemo.filetype);
            }
            else if (memosFound.Count == 1) //one found
            {
                filesFound.Add(memosFound.FirstOrDefault());
            }
            else //multiple found
            {
                fileTypesDuplicates.Add(oSignedAccMemo.filetype + " or " + oSignedAccTaxMemo.filetype);
            }

            //retrieval: Go through the list of required "Signed by Client" files (without memo's)
            foreach (var fileNeeded in filesNeeded)
            {
                //retrieval
                filesFoundWithoutPeriod = eBookReadManager.Context.RepositoryItems.Where(r => r.ClientId == file.Client.Id && r.Deleted == false && r.StructureId == fileNeeded.id && r.Contenttype == "application/pdf" && r.Status == oSignedByClient.id).ToList();

                if (fileNeeded.filetype == "Engagement Agreement")
                {
                    filesFoundStructureType = filesFoundWithoutPeriod.Where(r => r.StartDate == filesFoundWithoutPeriod.Max(s => s.StartDate)).ToList(); //Newest EA based on startdate
                }
                else
                {
                    filesFoundStructureType = filesFoundWithoutPeriod.Where(r => r.StartDate == file.StartDate && file.EndDate == file.EndDate).ToList(); //check period matches current file
                }

                //validation
                if (filesFoundStructureType.Count == 0) //none found
                {
                    fileTypesMissing.Add(fileNeeded.filetype);
                }
                else if (filesFoundStructureType.Count == 1) //one found
                {
                    filesFound.Add(filesFoundStructureType.FirstOrDefault());
                }
                else //multiple found
                {
                    fileTypesDuplicates.Add(fileNeeded.filetype);
                }
            }

            List<RepositoryItemDataContract> lridc = new List<RepositoryItemDataContract>();
            foreach(RepositoryItem fileFound in filesFound)
            {
                RepositoryItemDataContract ridc = new RepositoryItemDataContract()
                {
                    ClientId = fileFound.ClientId,
                    ContentType = fileFound.Contenttype,
                    DocstoreAccess = eBookReadManager.Context.RepositoryStructureSet.FirstOrDefault(y=>y.Id == fileFound.StructureId).DocstoreAccess,
                    Extension = fileFound.Extension,
                    FileName = fileFound.Name,
                    Id = fileFound.ItemId,
                    //Meta = new List<RepositoryMetaItemDataContract>() {  },
                    PeriodEnd = fileFound.EndDate,
                    PeriodStart = fileFound.StartDate,
                    PhysicalFileId = fileFound.PhysicalFileId,
                    Prequel = fileFound.Prequel,
                    Status = fileFound.Status,
                    StructureId = fileFound.StructureId
                };
                lridc.Add(ridc);
            }

            return new AnnualAccountRequiredFilesDataContract()
            {
                FileTypesDuplicates = fileTypesDuplicates,
                FileTypesMissing = fileTypesMissing,
                FilesFound = lridc
            };
        }

        private List<IndexItemBaseDataContract> BundleFy(List<IndexItemBaseDataContract> lst, Guid fileId, string culture)
        {
            for (int i = 0; i < lst.Count; i++)
            {
                IndexItemBaseDataContract item = lst[i];
                item.Id = Guid.NewGuid();
                switch (item.GetType().Name)
                {
                    case "ChapterDataContract":
                        ChapterDataContract cdc = (ChapterDataContract)item;
                        cdc.Items = BundleFy(cdc.Items, fileId,culture);
                        lst[i] = cdc;
                        break;
                    case "WorksheetItemDataContract":
                        WorksheetItemDataContract wdc = (WorksheetItemDataContract)item;
                        wdc.FileId = fileId;
                        lst[i] = wdc;
                        break;
                    case "StatementsDataContract":
                        StatementsDataContract sdc = (StatementsDataContract)item;
                        sdc.FileId = fileId;
                        lst[i] = sdc;
                        break;
                    case "DocumentItemDataContract":
                        DocumentItemDataContract didc = (DocumentItemDataContract)item;
                        Document doc = eBookReadManager.Context.Documents.Include("DocumentType").Include("File").FirstOrDefault(d => d.File.Id == fileId && d.DocumentType.Id == didc.DocumentTypeId);
                        if (doc != null)
                        {
                            didc.DocumentId = doc.Id;
                            lst[i] = didc;
                        }
                        else
                        {
                            Names nme = eBookReadManager.Context.NamesSet.First(n => n.ObjectId == didc.DocumentTypeId && n.Culture == culture);
                            string tpName = nme !=null? nme.Description : didc.Title;
                            //lst[i] = new TextItemDataContract
                        }

                        break;
                }
            }
            return lst;
        }


        public BundleDataContract GetBundle(Guid id)
        {
            
            Bundle bundle = eBookReadManager.Context.Bundles.FirstOrDefault(b => b.Id == id);
            if (bundle == null) return null;
            string path = string.Empty;
            string virtualPath = string.Empty;

            FileDataContract fdc = new BL.Read.FileReadRepository().GetFile(bundle.FileId);
            ClientDataContract cdc = new BL.Read.ClientRepository().GetClient(fdc.ClientId);

            path = System.IO.Path.Combine(Config.RepositoryLocal, cdc.Id.ToPath());
            //if (temp) path = Config.PickupLocal;
            path = System.IO.Path.Combine(path, string.Format("{0}.pdf", bundle.Id));

            if (System.IO.File.Exists(path))
            {
                virtualPath = path.Replace(Config.RepositoryLocal, Config.RepositoryVirtualDir);
                virtualPath = virtualPath.Replace(@"\", "/");
            }

            return new BundleDataContract
            {
                Id = bundle.Id
                ,
                Name = bundle.Name
                ,
                Deleted = bundle.Deleted
                ,
                Draft = bundle.Draft
                ,
                RectoVerso = bundle.RectoVerso
                ,
                Contents = BundleFy(CoreHelper.DeserializeFromString<List<IndexItemBaseDataContract>>(bundle.Contents),bundle.FileId,bundle.Culture)
                ,
                Culture = bundle.Culture
                ,
                FileId = bundle.FileId
                ,
                HeaderFooterType = bundle.HeaderFooterType
                ,
                TemplateId = bundle.TemplateId
                , 
                Locked = bundle.Locked
                ,
                Filepath = virtualPath
            };
        }

        public BundleDataContract GetBundleByTemplate(CriteriaFileBundleTemplateDataContract cfbtdc)
        {
            Bundle bundle = eBookReadManager.Context.Bundles.FirstOrDefault(b => b.FileId==cfbtdc.FileId && b.TemplateId==cfbtdc.TemplateId);
            if (bundle == null) return null;


            return new BundleDataContract
            {
                Id = bundle.Id
                ,
                Name = bundle.Name
                ,
                Deleted = bundle.Deleted
                ,
                Draft = bundle.Draft
                ,
                RectoVerso = bundle.RectoVerso
                ,
                Contents = BundleFy(CoreHelper.DeserializeFromString<List<IndexItemBaseDataContract>>(bundle.Contents),bundle.FileId,bundle.Culture)
                ,
                Culture = bundle.Culture
                ,
                FileId = bundle.FileId
                ,
                HeaderFooterType = bundle.HeaderFooterType
                ,
                TemplateId = bundle.TemplateId
                 ,
                Locked = bundle.Locked
            };


        }

        public bool DeleteBundle(Guid id)
        {
            bool ret = false;
            Bundle bundle = eBookReadManager.Context.Bundles.FirstOrDefault(b => b.Id == id);
            if (bundle != null)
            {
                bundle.Deleted = true;
                eBookReadManager.Context.SaveChanges();
                ret = true;
            }
            RepositoryItem ri = eBookReadManager.Context.RepositoryItems.FirstOrDefault(r => r.ItemId == id);
            if (ri != null)
            {
                ri.Deleted = true;
                eBookReadManager.Context.SaveChanges();
                if (!ret) ret = true;
            }
            return ret;
        }

        public BundleDataContract SaveBundle(BundleDataContract bdc)
        {
            if (bdc.Id == Guid.Empty) bdc.Id = Guid.NewGuid();
            bool locked = eBookReadManager.Context.Bundles.Where(b => b.Id == bdc.Id).FirstOrDefault().Locked;
            if (locked)
            {
                throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { Message = "Cannot save. Bundle is locked. Please reopen the window as another user updated the deliverable." });
            }
            else
            {
                Bundle bundle = eBookReadManager.Context.Bundles.FirstOrDefault(b => b.Id == bdc.Id);
                if (bundle == null)
                {
                    bundle = new Bundle
                    {
                        Id = bdc.Id
                        ,
                        TemplateId = bdc.TemplateId
                        ,
                        Culture = bdc.Culture
                        ,
                        FileId = bdc.FileId
                    };
                }

                bundle.Name = bdc.Name;
                bundle.HeaderFooterType = bdc.HeaderFooterType;
                bundle.Deleted = bdc.Deleted;
                bundle.Draft = bdc.Draft;
                bundle.RectoVerso = bdc.RectoVerso;
                bundle.Contents = CoreHelper.SerializeToString(bdc.Contents);
                if (bundle.EntityState == System.Data.EntityState.Detached)
                    eBookReadManager.Context.AddToBundles(bundle);
                eBookReadManager.Context.SaveChanges();
                return bdc;
            }
        }

        /// <summary>
        /// Validates a bundle with linked service
        /// </summary>
        /// <param name="bdc">BundleDataContract</param>
        /// <returns>validation object</returns>
        public CheckBundleDataContract ValidateBundle(Guid id) {
            CheckBundleDataContract cbdc = new CheckBundleDataContract(){
                Valid = false
            };

            if (id == Guid.Empty)
                throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { Message = "Bundle ID missing" });

            //Get bundle
            Bundle bundle = eBookReadManager.Context.Bundles.FirstOrDefault(b => b.Id == id);

            if (bundle == null)
            {
                throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { Message = "Bundle parameters invalid" });
            }

            //Define default services
            Guid fsYearEnd = new Guid("C3B8C8DB-3822-4263-9098-541FAE897D02");
            Guid fsAnnualAccounts = new Guid("1ABF0836-7D7B-48E7-9006-F03DB97AC28B");

            //Get ServiceBundleTemplate
            ServiceBundleTemplate sbt = eBookReadManager.Context.ServiceBundleTemplates.FirstOrDefault(s => s.TemplateId == bundle.TemplateId);
            //File service
            FileService fs = eBookReadManager.Context.FileServices.FirstOrDefault(f => f.FileId == bundle.FileId && f.ServiceId == sbt.ServiceId);
            //Get file
            File  file = eBookReadManager.Context.Files.Include("Client").Where(f => f.Id == bundle.FileId).FirstOrDefault();

            //If Bundle is linked to year end service
            if (fs.ServiceId == fsYearEnd)
            {
                Int16 iMemoInBundle = 0, iMemoOnStatusInBundle = 0;
                var oMemoStatusDraft = new { id = new Guid("D036D79A-B2AF-4A5F-A75F-F04BA59AFCC6"), status = "Draft" };
                var oMemoStatusSigned = new { id = new Guid("0BC2D5BF-C35B-491A-916E-5418F98B9E1F"), status = "Signed by EY" };
                cbdc.BundleId = bundle.Id;
                cbdc.AccountingMemorandum = false;
                    
                //Check Accountancy Memorandum and Accountancy/Tax Memorandum folder for files
                IEnumerable<RepositoryItem> allMemos = eBookReadManager.Context.RepositoryItems.Where(r => r.ClientId == file.Client.Id && r.Deleted == false && (r.StructureId == new Guid("B1362A5D-924A-41E1-B505-92D984EE40C9") || r.StructureId == new Guid("B9069EA7-6A32-41AF-B0B2-F66DB656760B")) && r.Contenttype == "application/pdf");
                //Check if these files are included within the bundle
                foreach (RepositoryItem memo in allMemos)
                {
                    if (bundle.Contents.Contains(memo.ItemId.ToString()) == true)
                    {
                        iMemoInBundle++;
                        //Expecting signed memo beyond approval partner
                        if(oMemoStatusSigned.id == memo.Status || ((fs.Status < 5 || fs.Status == 6 || fs.Status == 9) && oMemoStatusDraft.id == memo.Status))
                        {
                            iMemoOnStatusInBundle++;
                        }
                    }
                }

                if (iMemoInBundle == 1 && iMemoOnStatusInBundle == 1)
                {
                    cbdc.AccountingMemorandum = true;
                    cbdc.Valid = true;
                }
                else
                {
                    int iItems = allMemos.Count();

                    if (iItems == 0)
                    {
                        cbdc.Fault = "The Accounting Memorandum (" + (fs.Status < 5 ? oMemoStatusDraft.status : oMemoStatusSigned.status) + ") has not been added to the repository.";
                    }
                    else if (iMemoOnStatusInBundle == 0)
                    {
                        cbdc.Fault = "The Accounting Memorandum (" + (fs.Status < 5 ? oMemoStatusDraft.status : oMemoStatusSigned.status) + ") isn't included within the bundle.";
                    }
                    else if (iMemoInBundle > 1)
                    {
                        cbdc.Fault = "Multiple Memorandum documents are included within the bundle.";
                    }
                }

                if (fs.Status == 8)
                {
                    //Signed Statutory Account
                    if (eBookReadManager.Context.RepositoryItems.Where(r => r.ClientId == file.Client.Id && file.StartDate == r.StartDate && file.EndDate == r.EndDate && r.Deleted == false && r.Status == new Guid("FCF7A560-3D74-4341-A32B-310C8FA336CF") && r.StructureId == new Guid("08258e0e-99e3-402f-84c2-148383d5255e")).Count() == 0)
                    {
                        cbdc.Valid = false;
                        cbdc.SignedStatutoryAccounts = false;
                    }
                    else
                    {
                        cbdc.SignedStatutoryAccounts = true;
                    }
                    //Signed Minutes General Assemble
                    if (eBookReadManager.Context.RepositoryItems.Where(r => r.ClientId == file.Client.Id && file.StartDate == r.StartDate && file.EndDate == r.EndDate && r.Deleted == false && r.Status == new Guid("FCF7A560-3D74-4341-A32B-310C8FA336CF") && r.StructureId == new Guid("f18165cb-748f-430a-b2b7-88df26fdd8ba")).Count() == 0)
                    {
                        cbdc.Valid = false;
                        cbdc.SignedMinutesGeneralAssemble = false;
                    }
                    else
                    {
                        cbdc.SignedMinutesGeneralAssemble = true;
                    }
                    //Signed Representation Letter
                    if (eBookReadManager.Context.RepositoryItems.Where(r => r.ClientId == file.Client.Id && file.StartDate == r.StartDate && file.EndDate == r.EndDate && r.Deleted == false && r.Status == new Guid("FCF7A560-3D74-4341-A32B-310C8FA336CF") && r.StructureId == new Guid("d9bb468c-9886-463d-ac22-3d0c75f9f2db")).Count() == 0)
                    {
                        cbdc.Valid = false;
                        cbdc.SignedRepresentationLetter = false;
                    }
                    else
                    {
                        cbdc.SignedRepresentationLetter = true;
                    }
                }
                

                /*
                if (b != null) {
                    var content = CoreHelper.DeserializeFromString<List<IndexItemBaseDataContract>>(b.Contents);
                    if (fs != null && fs.Completed.HasValue && fs.Status.HasValue && fs.Status == 0)
                    {
                        cbdc.AccountingMemorandum = true;
                        cbdc.RepresentationLetter = true;
                    }
                    else
                    {
                        cbdc.AccountingMemorandum = CheckForDoc(new Guid("012C591F-A798-4BA2-A51F-6AB63F23C4A0"), content);
                        cbdc.RepresentationLetter = CheckForDoc(new Guid("2C6CDDD5-B8F0-41A3-B2B8-73112C334B26"), content);
                    }
                }
                 **/
            }
            else if (fs.ServiceId == fsAnnualAccounts)
            {
                /*/vars
                var oService = new { id = new Guid("1ABF0836-7D7B-48E7-9006-F03DB97AC28B"), name = "Annual Accounts" };
                var oSignedByClient = new { id = new Guid("FCF7A560-3D74-4341-A32B-310C8FA336CF"), status = "Signed by Client" };
                var oSignedByEY = new { id = new Guid("0BC2D5BF-C35B-491A-916E-5418F98B9E1F"), status = "Signed by EY" };
                var oSignedAccMemo = new { id = new Guid("B1362A5D-924A-41E1-B505-92D984EE40C9"), filetype = "Accountancy Memorandum" };
                var oSignedAccTaxMemo = new { id = new Guid("B9069EA7-6A32-41AF-B0B2-F66DB656760B"), filetype = "Accountancy/Tax Memorandum" };
                var oSignedRepLetter = new { id = new Guid("d9bb468c-9886-463d-ac22-3d0c75f9f2db"), filetype = "Representation Letter" };
                var oSignedStatutoryAccounts = new { id = new Guid("08258e0e-99e3-402f-84c2-148383d5255e"), filetype = "Statutory Accounts" };
                var oSignedMinutesGeneralAssemble = new { id = new Guid("f18165cb-748f-430a-b2b7-88df26fdd8ba"), filetype = "Minutes General Assemble" };
                var oSignedEngagementAgreement = new { id = new Guid("32F0391E-E0BD-41F9-A9C1-C04EAC3C8CB1"), filetype = "Engagement Agreement" };
                
                var filesNeeded = new[] { oSignedRepLetter, oSignedStatutoryAccounts, oSignedMinutesGeneralAssemble, oSignedEngagementAgreement }; //apart from memo's
                List<string> filesMissing = new List<string>();
                List<RepositoryItem> filesFound = new List<RepositoryItem>();

                //Check if bundle contains all 5 documents (nth)

                //Check for a signed by EY Accountancy Memorandum or signed by EY Accountancy/Tax Memorandum file (as one of them files is enough)
                IEnumerable<RepositoryItem> allMemos = eBookReadManager.Context.RepositoryItems.Where(r => r.ClientId == file.Client.Id && r.Deleted == false && (r.StructureId == oSignedAccMemo.id || r.StructureId == oSignedAccTaxMemo.id) && r.Contenttype == "application/pdf" && r.Status == oSignedByEY.id);

                if (allMemos.Count() == 0)
                {
                    filesMissing.Add(oSignedAccMemo.filetype + " or " + oSignedAccTaxMemo.filetype);
                }
                else
                {
                    filesFound.FirstOrDefault();
                }

                //Go through the list of required Signed by Client files (without memo's)
                foreach (var fileNeeded in filesNeeded)
                {
                    RepositoryItem fileFound = eBookReadManager.Context.RepositoryItems.Where(r => r.ClientId == file.Client.Id && r.Deleted == false && r.StructureId == fileNeeded.id && r.Contenttype == "application/pdf" && r.Status == oSignedByClient.id).FirstOrDefault();

                    if (fileFound == null)
                    {
                        filesMissing.Add(fileNeeded.filetype);
                    }
                    else
                    {
                        filesFound.Add(fileFound);
                    }
                }
        
                /*if(filesMissing.Count == 0) VALIDATE BUNDLE
                {
                    //Check if these files are included within the bundle, else add to fileMissing
                    XDocument xmlDoc = XDocument.Parse(bundle.Contents);
                    List<Guid> itemIds;

                    var elements = from d in xmlDoc.Descendants("IndexItemBaseDataContract")
                                      select d.Element("ItemId");

                    foreach (string itemId in elements)
                    {
                        if(filesFound.Where(f => f.ItemId = itemId).Count() > 0)
                        {
                        
                        }

                    foreach (RepositoryItem fileFound in filesFound)
                    {
                        if (documentIds.Contains(fileFound.ItemId.ToString()) != true)
                        {
                            filesMissing.Add(fileFound.Name);
                        }
                    }
                }*/
                var oDepositNBB = new { id = new Guid("4E5BEE8A-0F06-42CD-8809-4A642092704C"), filetype = "Deposit NBB" };
                AnnualAccountRequiredFilesDataContract aarfdc = GetAnnualAccountsRequiredFiles(file.Id);


                if (aarfdc.FileTypesMissing.Count() == 0 && aarfdc.FileTypesDuplicates.Count() == 0)
                {
                    cbdc.Valid = true;
                }
                else
                {
                    cbdc.Valid = false;
                    cbdc.Fault = string.Empty;
                    if (aarfdc.FileTypesMissing.Count() > 0)
                    {
                        cbdc.Fault = "The following files are missing: " + string.Join(",", aarfdc.FileTypesMissing.ToArray());
                    }
                    if (aarfdc.FileTypesDuplicates.Count() > 0)
                    {
                        cbdc.Fault = "The following folder contains signed duplicates: " + string.Join(",", aarfdc.FileTypesDuplicates.ToArray());
                    }

                    return cbdc;
                }

                //Step validation
                if (fs.Status >= 1) //Staff step
                {
                    //Check if the First generated XBRL or pdf has been uploaded
                    int depositNBB = eBookReadManager.Context.RepositoryItems.Where(r => r.ClientId == file.Client.Id && r.StartDate == file.StartDate && r.EndDate == file.EndDate && r.Deleted == false && r.StructureId == oDepositNBB.id && (r.Extension == ".pdf" || r.Extension == ".xbrl")).Count();

                    if (depositNBB == 0)
                    {
                        cbdc.Valid = false;
                        cbdc.DepositNBB = false;
                        cbdc.Fault = "Please upload the xbrl or pdf file, which will be uploaded to NBB.";

                        return cbdc;
                    }
                }
            }
            else
            {
                cbdc.Valid = true;
            }

            return cbdc;
        }

        public bool CheckForDoc(Guid templateId,List<IndexItemBaseDataContract> items)
        {
            if (items.Count == 0) return false;
            bool res = items.Count(i => i.GetType().Name == typeof(DocumentItemDataContract).Name
                && ((DocumentItemDataContract)i).DocumentTypeId == templateId
                && ((DocumentItemDataContract)i).DocumentId != Guid.Empty) == 1;
            if (!res)
            {
                items.Where(i => i.GetType().Name == typeof(ChapterDataContract).Name).ToList().ForEach(i=> {
                    res = CheckForDoc(templateId,((ChapterDataContract)i).Items);
                    if(res) return;
                });
            }
            return res;
        }

        protected override void RegisterTranslators()
        {
          
            
        }

        internal static List<Guid> YearEndBundleTemplateIds = new List<Guid>
        {
            new Guid("43C27DBE-AE64-4208-BD57-060553D41EC8"),
            new Guid("C29A94BF-BB06-466D-9054-5B2C632E1D97")
        };

        #region Lock bundle
        internal void LockBundle(Guid fileId, Guid bundleId)
        {
            // Closing the bundle will render pdf for each document and place in repo
            // Replace in bundle with pdf

            ITextRepository ir = new ITextRepository();
            var bundle = eBookReadManager.Context.Bundles.Where(b=>b.FileId==fileId  && b.Id == bundleId && !b.Deleted).FirstOrDefault();
            var cts = CoreHelper.DeserializeFromString<List<IndexItemBaseDataContract>>(bundle.Contents);

            EY.com.eBook.API.Contracts.Data.Cache.FileDataContract fdc = new BL.Read.FileReadRepository().GetFile(fileId);
            ClientDataContract cdc = new BL.Read.ClientRepository().GetClient(fdc.ClientId);

            //Lock bundle (only editing through repo is possible, cfr. signed versions of docs)
            bundle.Locked = true;

            cts = ReplaceDocsWithRepo(ir, cts, cdc, fdc);
            bundle.Contents = CoreHelper.SerializeToString(cts);
            eBookReadManager.Context.SaveChanges();
        }

        internal List<IndexItemBaseDataContract> ReplaceDocsWithRepo(ITextRepository ir, List<IndexItemBaseDataContract> items, 
            ClientDataContract client,EY.com.eBook.API.Contracts.Data.Cache.FileDataContract file)
        {
            var itDocs = items.Where(i => i.GetType().Name == typeof(DocumentItemDataContract).Name || i.GetType().Name == typeof(ChapterDataContract).Name);
            if(itDocs.Count()==0) return items;

            foreach (IndexItemBaseDataContract it in itDocs.ToList())
            {
                int i = 0;
                switch(it.GetType().Name)
                {
                    case "DocumentItemDataContract":
                        var dic = (DocumentItemDataContract)it;
                        i = items.IndexOf(it);
                        items[i] = ir.GetDocumentPrintAndPlaceInRepo(client, file, dic, GetDocTemplateRepo(dic.DocumentTypeId));
                        break;
                    case "ChapterDataContract":
                        var chap = (ChapterDataContract)it;
                        i = items.IndexOf(it);//added
                        chap.Items = ReplaceDocsWithRepo(ir, chap.Items,client,file);
                        items[i] = chap;
                        break;
                }
            }
            return items;

        }

        #endregion

        #region OpenBundle
        /// <summary>
        /// Bundle unlocked,ReplaceRepoWithDocs draft true
        /// </summary>
        /// <param name="fileId"></param>
        internal void UnlockBundle(Guid fileId, Guid bundleId)
        {
            //var bundle = eBookReadManager.Context.Bundles.Where(b => b.FileId == fileId && !b.Deleted).ToList().FirstOrDefault(b => YearEndBundleTemplateIds.Contains(b.TemplateId));
            var bundle = eBookReadManager.Context.Bundles.Where(b => b.FileId == fileId && b.Id == bundleId && !b.Deleted).ToList().FirstOrDefault(b => YearEndBundleTemplateIds.Contains(b.TemplateId));
            if (bundle != null)
            {
                bundle.Locked = false;
                var cts = CoreHelper.DeserializeFromString<List<IndexItemBaseDataContract>>(bundle.Contents);
                cts = ReplaceRepoWithDocs(cts, fileId);
                bundle.Contents = CoreHelper.SerializeToString(cts);
                eBookReadManager.Context.SaveChanges();
            }
        }

        internal void setDraft(Guid id, bool draft)
        {
            Bundle bundle = eBookReadManager.Context.Bundles.FirstOrDefault(b => b.Id == id);
            if (bundle != null)
            {
                bundle.Draft = draft;
                eBookReadManager.Context.SaveChanges();
            }
        }

        private List<IndexItemBaseDataContract> ReplaceRepoWithDocs(List<IndexItemBaseDataContract> items, Guid fileId)
        {
            var itDocs = items.Where(i => i.GetType().Name == typeof(PDFDataContract).Name || i.GetType().Name == typeof(ChapterDataContract).Name);
            if (itDocs.Count() == 0) return items;

            foreach (IndexItemBaseDataContract it in itDocs.ToList())
            {
                int i = 0;
                switch (it.GetType().Name)
                {
                    case "PDFDataContract":
                        var pic = (PDFDataContract)it;
                        i = items.IndexOf(it);
                        if (pic.Origin != null && pic.Origin.StartsWith("DOC::"))
                        {
                            string[] docDets = pic.Origin.Split(new string[] { "::" }, StringSplitOptions.None);
                            Guid docTemplId = new Guid(docDets[1]);
                            Guid docId = new Guid(docDets[2]);
                            string docCult = docDets[3];
                            DocumentItemDataContract dic = new DocumentItemDataContract
                            {
                                Culture = docCult
                                ,
                                DocumentId = docId
                                ,
                                DocumentTypeId = docTemplId
                                ,
                                FileId = fileId
                              ,
                                ShowInIndex = pic.ShowInIndex
                              ,
                                Title = pic.Title
                              ,
                                HeaderConfig = new HeaderConfigDataContract { Enabled = false, ShowTitle = false }
                              ,
                                FooterConfig = new FooterConfigDataContract { Enabled = false, ShowPageNr = false, ShowFooterNote = false }
                              ,
                                IconCls = "eBook-bundle-tree-document"
                            };
                            items[i] = dic;
                        }

                        break;
                    case "ChapterDataContract":
                        var chap = (ChapterDataContract)it;
                        i = items.IndexOf(it);
                        chap.Items = ReplaceRepoWithDocs(chap.Items, fileId);
                        items[i] = chap;
                        break;
                }
            }
            return items;
        }

        #endregion


        private Guid GetDocTemplateRepo(Guid docTypeId)
        {
            // INSERT RETURN OF REPO STRUCTURE LOCATION BASED ON DOCUMENT TYPE ID
            switch (docTypeId.ToString().ToUpper()) { 
                case "2C6CDDD5-B8F0-41A3-B2B8-73112C334B26": //Representation letter
                    return new Guid("D9BB468C-9886-463D-AC22-3D0C75F9F2DB");
                    break;
                case "012C591F-A798-4BA2-A51F-6AB63F23C4A0": //Accounting_memorandum
                    return new Guid("B1362A5D-924A-41E1-B505-92D984EE40C9");
                    break;
            }
            return Guid.Empty;
        }

        public List<string> CheckSignedRepos(CriteriaIdDataContract cidc)
        {
            var file = eBookWriteManager.Context.Files.First(f => f.Id == cidc.Id);
            var bundle = eBookReadManager.Context.Bundles.Where(b => b.FileId == cidc.Id && !b.Deleted).ToList().FirstOrDefault(b => YearEndBundleTemplateIds.Contains(b.TemplateId));
            var repos = new Repository().GetSignedFilesInPeriod(file.ClientId,file.StartDate,file.EndDate);

            var cts = CoreHelper.DeserializeFromString<List<IndexItemBaseDataContract>>(bundle.Contents);
            List<string> result = new List<string>();
            CheckSigned(cts, ref result, false, repos);

            return result;
        }

        public void CheckSigned(List<IndexItemBaseDataContract> items, ref List<string> result, bool requireAll,List<RepositoryItem> repos)
        {
            var itDocs = items.Where(i => i.GetType().Name == typeof(PDFDataContract).Name || i.GetType().Name == typeof(ChapterDataContract).Name);
            if (itDocs.Count() == 0) return;

            foreach (IndexItemBaseDataContract it in itDocs.ToList())
            {
                int i = 0;
                switch (it.GetType().Name)
                {
                    case "PDFDataContract":
                        var dic = (PDFDataContract)it;
                        if (requireAll || (!string.IsNullOrEmpty(dic.Origin) && dic.Origin.StartsWith("DOC::")))
                        {
                            if (repos.Count(r => r.ItemId == dic.ItemId) == 0)
                                result.Add(dic.Title);
                        }
                        break;
                    case "ChapterDataContract":
                        var chap = (ChapterDataContract)it;
                        if (chap.RequiredSignedRepo)
                        {
                            CheckSigned(chap.Items, ref result, true,repos);
                        }
                        break;
                }
            }
          
        }
    }
}
