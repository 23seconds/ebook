﻿using System;
using System.Collections.Generic;
using System.Linq;
//using System.Linq.Dynamic;
using System.Text;
using EY.com.eBook.Core;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.Core.EF;
using EY.com.eBook.API.Contracts.Data.Logic;
using EY.com.eBook.Core.EF.Read;
using System.Data.Objects;
using EY.com.eBook.Core.EntityTranslation;
using EY.com.eBook.API.BL.Translators;
using System.ServiceModel;
using EY.com.eBook.Core.Data;
using System.Linq.Expressions;

namespace EY.com.eBook.API.BL.Read
{
    public class HomeRepository : RepositoryBase
    {
        /// <summary>
        /// Gets file and client info regarding the last 10 recently updated records based on the given person ID.
        /// </summary>
        /// <param name="cidc"></param>
        /// <returns>List of maximum 10 records containing file and client info.</returns>
        public List<RecentFileDataContract> GetRecentFiles(CriteriaClientDataContract ccdc)
        {
            ccdc.ActivePerson = new BusinessHelper().DecodePerson(ccdc.EncodedActivePerson);

            var recentFiles = (from p in eBookReadManager.Context.RecentFileSet
                    .Where(pp => pp.PersonId == ccdc.ActivePerson.Id)
                    .OrderByDescending(pp => pp.LastUpdated)
                    .Take(10)
                               select p
                    ).ToList();

            return (from p in recentFiles
                    select new RecentFileDataContract
                    {
                        Client = new ClientRepository().GetClientBase(p.ClientId, ccdc.ActivePerson.Id)
                        ,
                        File = new FileReadRepository().GetFileInfo(p.FileId)
                        ,
                        LastAccessed = p.LastUpdated
                    }).ToList();
            /*
            bool filterPersonRoles = false;
            IOrderedQueryable<Client> clientsQry = null;
            ccdc.ActivePerson = new BusinessHelper().DecodePerson(ccdc.EncodedActivePerson);
            List<RecentFile> recentFiles = (from p in eBookReadManager.Context.RecentFileSet
                                .Where(pp => pp.PersonId == ccdc.ActivePerson.Id)
                                .OrderByDescending(pp => pp.LastUpdated)
                                .Take(10)
                                select p
                                ).ToList();
            Client client = null;


            List<RecentFileDataContract> lrfdc = new List<RecentFileDataContract>();

            foreach(RecentFile recentFile in recentFiles)
            {
                //This is a hasty fix in order to be able to release. the recent file function wasn't supplying the correct role.
                //Needs to be rewritten in a proper manner. The easy fix was creattion a GetPersonClients copy function that only supplies the given client.
                ClientBaseDataContract cbdc = new ClientBaseDataContract();
                clientsQry = new HomeRepository().GetPersonClient(ccdc, recentFile.ClientId, ref filterPersonRoles);

                if (clientsQry == null)
                {
                    //No client with that given role was found, hence we utilize the active role
                    cbdc = new ClientRepository().GetClientBase(recentFile.ClientId, ccdc.ActivePerson.Id);
                    cbdc.Role = ccdc.ActivePerson.ActiveRole; //Add the currently set person role if there is no person client role
                }
                else
                {
                    cbdc = Translator.Translate<ClientBaseDataContract>(clientsQry.FirstOrDefault());

                    if (filterPersonRoles)
                    {
                        Translator.RemoveEntityTranslatorByType(typeof(ClientBaseTranslator));
                        Translator.RegisterEntityTranslator(new ClientBaseTranslator { PersonId = ccdc.ActivePerson.Id });
                    }
                }

                RecentFileDataContract rfdc = new RecentFileDataContract
                {
                    Client = cbdc,
                    File = new FileReadRepository().GetFileInfo(recentFile.FileId),
                    LastAccessed = recentFile.LastUpdated
                };

                lrfdc.Add(rfdc);
            }

            return lrfdc;

            /*
            bool filterPersonRoles = false;

            ccdc.ActivePerson = new BusinessHelper().DecodePerson(ccdc.EncodedActivePerson);

            if (filterPersonRoles)
            {
                Translator.RemoveEntityTranslatorByType(typeof(ClientBaseTranslator));
                Translator.RegisterEntityTranslator(new ClientBaseTranslator { PersonId = ccdc.ActivePerson.Id });
            }

            var recentFiles = (from p in eBookReadManager.Context.RecentFileSet
                                .Where(pp => pp.PersonId == ccdc.ActivePerson.Id)
                                .OrderByDescending(pp => pp.LastUpdated)
                                .Take(10)
                                select p
                                ).ToList();


            return (from p in recentFiles
                    select new RecentFileDataContract
                    {
                        Client = new HomeRepository().GetPersonClient(ccdc, p.ClientId, ref filterPersonRoles)
                        ,
                        File = new FileReadRepository().GetFileInfo(p.FileId)
                        ,
                        LastAccessed = p.LastUpdated
                    }).ToList();*/
        }

        /// <summary>
        /// Gets the requested range of clients based on given active person info.
        /// </summary>
        /// <param name="ccdc"></param>
        /// <returns>List of clients.</returns>
        public ResponseDataContract<List<ClientBaseDataContract>> GetClients(CriteriaClientDataContract ccdc)
        {
            IOrderedQueryable<Client> clientsQry = null;
            if (!string.IsNullOrEmpty(ccdc.EncodedActivePerson))
            {
                ccdc.ActivePerson = new BusinessHelper().DecodePerson(ccdc.EncodedActivePerson);
                bool filterPersonRoles = false;
                clientsQry = GetPersonClients(ccdc, ref filterPersonRoles);

                //if (filterPersonRoles)
                //{
                    Translator.RemoveEntityTranslatorByType(typeof(ClientBaseTranslator));
                    Translator.RegisterEntityTranslator(new ClientBaseTranslator { PersonId = ccdc.ActivePerson.Id, ActiveRole = ccdc.ActivePerson.ActiveRole });
                //}
            }
            else {
                // FAIL SAFE ?
                ObjectQuery<Client> clients = eBookReadManager.Context.ClientSet;
                clientsQry = clients.Where(c => c.Name.Contains(ccdc.Query) || c.GFISCode.Contains(ccdc.Query) || c.EnterpriseNumber.Contains(ccdc.Query)).OrderBy(c=>c.Name);
            }

            return new ResponseDataContract<List<ClientBaseDataContract>>
            {
                //If the datacontract contains a start and limit value, only return the number of client elements specified by both values.
                Data = (ccdc.Start.HasValue && ccdc.Limit.HasValue)
                            ? Translator.TranslateList<ClientBaseDataContract, Client>(clientsQry.Skip(ccdc.Start.Value).Take(ccdc.Limit.Value).ToList(), TranslateLevel.FirstLevel)
                            : Translator.TranslateList<ClientBaseDataContract, Client>(clientsQry.ToList(), TranslateLevel.FirstLevel)
                ,
                Total = clientsQry.Count()
            };
        }

        /// <summary>
        /// Gets the requested range of persons whereas the given string occurs within the person's name;
        /// </summary>
        /// <param name="cbdc"></param>
        /// <returns>List of persons.</returns>
        public ResponseDataContract<List<PersonDataContract>> GetPersons(CriteriaBaseDataContract cbdc)
        {
            IOrderedQueryable<Person> personsQry = null;
            
            ObjectQuery<Person> persons = eBookReadManager.Context.PersonSet;
            personsQry = persons.Where(p => p.FirstName.Contains(cbdc.Query) || p.LastName.Contains(cbdc.Query)).OrderBy(p=>p.FirstName);
               // (c => c.Name.Contains(ccdc.Query) || c.GFISCode.Contains(ccdc.Query) || c.EnterpriseNumber.Contains(ccdc.Query)).OrderBy(c => c.Name);
            

            return new ResponseDataContract<List<PersonDataContract>>
            {
                Data = (cbdc.Start.HasValue && cbdc.Limit.HasValue)
                            ? Translator.TranslateList<PersonDataContract, Person>(personsQry.Skip(cbdc.Start.Value).Take(cbdc.Limit.Value).ToList(), TranslateLevel.FirstLevel)
                            : Translator.TranslateList<PersonDataContract, Person>(personsQry.ToList(), TranslateLevel.FirstLevel)
                ,
                Total = personsQry.Count()
            };
        }




        /// <summary>
        /// Gets all offices.
        /// </summary>
        /// <returns>List of offices.</returns>
        public List<OfficeDataContract> GetAllOffices() 
        {
            var offices = eBookReadManager.Context.OfficeSet.Distinct();
            List<OfficeDataContract> lodc = new List<OfficeDataContract>();
            foreach (var office in offices)
            {
                OfficeDataContract odc = new OfficeDataContract
                {
                    Id = office.Id,
                    Name = office.Name
                };
                lodc.Add(odc);
            }
            return lodc;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ccdc"></param>
        /// <param name="filterPersonRoles"></param>
        /// <returns>List of ordered clients, bool filterPersonRoles.</returns>
        internal IOrderedQueryable<Client> GetPersonClient(CriteriaClientDataContract ccdc, Guid clientId, ref bool filterPersonRoles)
        {
            if (ccdc.ActivePerson == null && string.IsNullOrEmpty(ccdc.EncodedActivePerson)) return null;
            if (ccdc.ActivePerson == null) ccdc.ActivePerson = new BusinessHelper().DecodePerson(ccdc.EncodedActivePerson);

            List<object> pars = new List<object>();
            IQueryable<Client> oQry = eBookReadManager.Context.ClientSet.Include("PersonRoles").Include("Overrule").Where(c => c.Id == clientId);

            StringBuilder qry = new StringBuilder();

            switch (ccdc.ActivePerson.ActiveRole.ToUpper())
            {
                case "CHAMPION":
                    Person champ = eBookReadManager.Context.PersonSet.Include("ChampionOffices").First(p => p.Id == ccdc.ActivePerson.Id);
                    //var test = eBookReadManager.Context.ClientDepartments.Count(cd=>cd.Offices.Count()>0)>0);

                    var cQry = eBookReadManager.Context.ClientDepartments
                        .Include("Offices")
                        .Include("Client")
                        .Include("Client.PersonRoles")
                        .Include("Client.Overrule")
                        .Where(cd => cd.DepartmentId == champ.Department);

                    int cdCount = 0;
                    StringBuilder cWhere = new StringBuilder();
                    List<string> offices = champ.ChampionOffices.Select(o => o.Id).Distinct().ToList();
                    if (!offices.Contains("00176"))
                    {
                        foreach (string officeId in offices)
                        {
                            if (cWhere.Length > 3) cWhere.Append(" or ");
                            cWhere.Append("Offices.Count(Id = @").Append(pars.Count).Append(")>0");
                            cdCount++;
                            pars.Add(officeId);

                        }

                        Expression<Func<ClientDepartment, bool>> exprc = System.Linq.Dynamic.DynamicExpression.ParseLambda<ClientDepartment, bool>(cWhere.ToString(), pars.ToArray());
                        oQry = cQry.Where(exprc).Select(cd => cd.Client).Distinct();
                    }
                    else
                    {
                        oQry = cQry.Select(cd => cd.Client).Distinct();
                    }

                    break;
                case "ADMIN":
                case "DEVELOPER":
                    // nothing to add (all clients)
                    break;
                default:

                    //qry.Append("PersonRoles.Count(PersonId.Equals(@").Append(pars.Count).Append(")) > 0");
                    qry.Append("PersonRoles.Count(PersonId.Equals(@").Append(pars.Count).Append(")) > 0");
                    pars.Add(ccdc.ActivePerson.Id);
                    filterPersonRoles = true;
                    break;
            }

            if (ccdc.Fields != null && !string.IsNullOrEmpty(ccdc.Query))
            {

                if (qry.Length > 0) qry.Append(" and ");
                qry.Append("(");
                int cnt = 0;
                foreach (string field in ccdc.Fields)
                {
                    if (cnt > 0) qry.Append(" or ");
                    qry.Append(field)
                        .Append(".Contains(@")
                        .Append(pars.Count)
                        .Append(")");
                    cnt++;
                }
                qry.Append(")");
                pars.Add(ccdc.Query);
            }

            IOrderedQueryable<Client> clientQry = null;
            if (qry.Length > 0)
            {
                Expression<Func<Client, bool>> expr = System.Linq.Dynamic.DynamicExpression.ParseLambda<Client, bool>(qry.ToString(), pars.ToArray());

                clientQry = oQry.Where(expr).OrderBy(c => c.Name);
            }
            else
            {
                clientQry = oQry.OrderBy(c => c.Name);
            }
            return clientQry;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ccdc"></param>
        /// <param name="filterPersonRoles"></param>
        /// <returns>List of ordered clients, bool filterPersonRoles.</returns>
        internal IOrderedQueryable<Client> GetPersonClients(CriteriaClientDataContract ccdc, ref bool filterPersonRoles)
        {
            if(ccdc.ActivePerson==null && string.IsNullOrEmpty(ccdc.EncodedActivePerson)) return null;
            if(ccdc.ActivePerson == null) ccdc.ActivePerson = new BusinessHelper().DecodePerson(ccdc.EncodedActivePerson);

            List<object> pars = new List<object>();
            IQueryable<Client> oQry = eBookReadManager.Context.ClientSet.Include("PersonRoles").Include("Overrule");

            StringBuilder qry = new StringBuilder();

            switch (ccdc.ActivePerson.ActiveRole.ToUpper())
            {
                case "CHAMPION":
                    Person champ = eBookReadManager.Context.PersonSet.Include("ChampionOffices").First(p => p.Id == ccdc.ActivePerson.Id);
                    //var test = eBookReadManager.Context.ClientDepartments.Count(cd=>cd.Offices.Count()>0)>0);

                   var cQry = eBookReadManager.Context.ClientDepartments
                       .Include("Offices")
                       .Include("Client")
                       .Include("Client.PersonRoles")
                       .Include("Client.Overrule")
                       .Where(cd=>cd.DepartmentId==champ.Department);
                  
                   int cdCount = 0;
                   StringBuilder cWhere = new StringBuilder();
                   List<string> offices = champ.ChampionOffices.Select(o => o.Id).Distinct().ToList();
                   if (!offices.Contains("00176"))
                   {
                       foreach (string officeId in offices)
                       {
                           if (cWhere.Length > 3) cWhere.Append(" or ");
                           cWhere.Append("Offices.Count(Id = @").Append(pars.Count).Append(")>0");
                           cdCount++;
                           pars.Add(officeId);

                       }

                       Expression<Func<ClientDepartment, bool>> exprc = System.Linq.Dynamic.DynamicExpression.ParseLambda<ClientDepartment, bool>(cWhere.ToString(), pars.ToArray());
                       oQry = cQry.Where(exprc).Select(cd => cd.Client).Distinct();
                   }
                   else
                   {
                       oQry = cQry.Select(cd => cd.Client).Distinct();
                   }

                    break;
                case "ADMIN":
                case "DEVELOPER":
                    // nothing to add (all clients)
                    break;
                default:
                    
                    //qry.Append("PersonRoles.Count(PersonId.Equals(@").Append(pars.Count).Append(")) > 0");
                    qry.Append("PersonRoles.Count(PersonId.Equals(@").Append(pars.Count).Append(")) > 0");
                    pars.Add(ccdc.ActivePerson.Id);
                    filterPersonRoles = true;
                    break;
            }
            if (ccdc.Fields != null && !string.IsNullOrEmpty(ccdc.Query))
            {

                if (qry.Length > 0) qry.Append(" and ");
                qry.Append("(");
                int cnt = 0;
                foreach (string field in ccdc.Fields)
                {
                    if (cnt > 0) qry.Append(" or ");
                    qry.Append(field)
                        .Append(".Contains(@")
                        .Append(pars.Count)
                        .Append(")");
                    cnt++;
                }
                qry.Append(")");
                pars.Add(ccdc.Query);
            }

            IOrderedQueryable<Client> clientQry = null;
            if (qry.Length > 0)
            {
                Expression<Func<Client, bool>> expr = System.Linq.Dynamic.DynamicExpression.ParseLambda<Client, bool>(qry.ToString(), pars.ToArray());

                clientQry = oQry.Where(expr).OrderBy(c => c.Name);
            }
            else
            {
                clientQry = oQry.OrderBy(c => c.Name);
            }
            return clientQry;
        }

        public PersonDataContract Login(CriteriaWinAccountDataContract cwadc)
        {

            Person person = eBookReadManager.Context.PersonSet
                                .Include("Roles")
                                .Include("ChampionOffices")
                                .FirstOrDefault(p => p.WindowsAccount.Replace(@"\", "").ToUpper() == cwadc.WindowsAccount.Replace(@"\", "").ToUpper());
                                //.FirstOrDefault(p => p.WindowsAccount.ToUpper() == cwadc.WindowsAccount.ToUpper());
            if (person == null)
            {
                // first logon, lookup email
                string mail = new ActiveDirectoryRepository().FindEmail(cwadc.WindowsAccount);
                if (!string.IsNullOrEmpty(mail))
                {
                    // import person, get from cdb by email and log windows account for future use
                    EY.com.eBook.Core.EF.Write.Person wperson = eBookWriteManager.Context.PersonSet
                                                                   .FirstOrDefault(p => p.Email.ToUpper() == mail.ToUpper());
                    if (wperson != null)
                    {
                        wperson.WindowsAccount = cwadc.WindowsAccount;
                        eBookWriteManager.Context.SaveChanges();
                        eBookReadManager.Context.Refresh(RefreshMode.StoreWins, eBookReadManager.Context.PersonSet);
                        person = eBookReadManager.Context.PersonSet
                                .Include("Roles")
                                .Include("ChampionOffices")
                                .FirstOrDefault(p => p.WindowsAccount.Replace(@"\", "").ToUpper() == cwadc.WindowsAccount.Replace(@"\", "").ToUpper());

                    }
                    else
                    {
                        throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { Message = String.Format("Contact your local eBook champion: User [{0}] email address is incorrect/missing in either GFIS or Active directory.'", cwadc.WindowsAccount) });
                    }
                }
                else
                {
                    throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { Message = String.Format("Contact E&Y helpdesk with following information: 'user {0} has no email address in Active Directory.'", cwadc.WindowsAccount) });
                    //throw error
                }
            }
            PersonDataContract pdc =  Translator.Translate<PersonDataContract>(person);
            if (pdc.DefaultOffice != null)
            {
                pdc.Champions = new ClientRepository().GetChampionsString(pdc.DefaultOffice.Id);
            }
            return pdc;

        }

        public List<ChampionDataContract> GetAllChampions()
        {
            List<ChampionDataContract> champions = new List<ChampionDataContract>();

            IQueryable<Office> offices = eBookReadManager.Context.OfficeSet.Include("Champions").Include("Champions.Roles").Where(o => o.Champions.Count > 0);
           
            foreach (Office office in offices)
            {
                var sort = office.Champions.OrderBy(c => c.Department);
                champions.AddRange(from pers in sort                                   
                                   where pers.Roles.Where(r => r.Role.ToUpper().Contains("PARTNER")).Count() == 0                                   
                                   select new ChampionDataContract
                                   {
                                       Office = office.Name
                                       ,
                                       OfficeId = office.Id
                                       ,
                                       Id = pers.Id
                                       ,
                                       Email = pers.Email
                                       ,
                                       FirstName = pers.FirstName
                                       ,
                                       LastName = pers.LastName
                                       ,
                                       Department = pers.Department
                                   });
            }
            return champions;
        }

        public PersonDataContract GetPerson(Guid id)
        {
            Person person = eBookReadManager.Context.PersonSet.Include("Roles").Include("ChampionOffices").FirstOrDefault(p => p.Id == id);
            if (person == null)
                throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { Message = String.Format("Person {0} not found!", id) });
            return Translator.Translate<PersonDataContract>(person, TranslateLevel.FirstLevel);

        }

        public PersonDataContract GetPersonByEmail(string email)
        {
            Person person = eBookReadManager.Context.PersonSet
                            .Include("Roles")
                            .Include("ChampionOffices")
                            .FirstOrDefault(p => p.Email == email);
            if (person == null)
                throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { Message = String.Format("Person {0} not found!", email) });

            return Translator.Translate<PersonDataContract>(person, TranslateLevel.SecondLevel);
        }

        public bool IsAdmin(Guid personId)
        {
            Person pers = eBookReadManager.Context.PersonSet.Include("Roles").FirstOrDefault(p => p.Id == personId);
            return pers.Roles.Count(r => r.Role == "ADMIN") > 0;
        }

        public bool IsChampion(Guid personId)
        {
            Person pers = eBookReadManager.Context.PersonSet.Include("ChampionOffices").FirstOrDefault(p => p.Id == personId);
            return pers.ChampionOffices.Count > 0;
        }

        public bool IsOfficeChampion(Guid personId, string officeId)
        {
            Person pers = eBookReadManager.Context.PersonSet.Include("ChampionOffices").FirstOrDefault(p => p.Id == personId);
            return pers.ChampionOffices.Where(o => o.Id == officeId
                                                || o.Id == "00000"
                                                || o.Id == "00011"
                                                || o.Id == "00176").Count() > 0;
        }

        public bool IsClientChampion(Guid personId, Guid clientId)
        {

            Person pers = eBookReadManager.Context.PersonSet.Include("ChampionOffices").Include("ChampionOffices.Clients").FirstOrDefault(p => p.Id == personId);
            if (pers.ChampionOffices.Count == 0) return false;
            
            return pers.ChampionOffices.Where(o => o.ClientDepartments.Where(d=>d.ClientId == clientId).Count() > 0).Count() > 0;
        }

        protected override void RegisterTranslators()
        {
            Translator.RegisterEntityTranslator(new ClientBaseTranslator());
            Translator.RegisterEntityTranslator(new PersonTranslator());
            Translator.RegisterEntityTranslator(new OfficeTranslator());
            Translator.RegisterEntityTranslator(new PersonRoleTranslator());
            Translator.RegisterEntityTranslator(new PersonClientRoleTranslator());
            
        }
    }
}
