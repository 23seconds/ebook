﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Data.Linq;
using System.Linq.Dynamic;
using System.Text;
using EY.com.eBook.Core;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.Core.EF;
using EY.com.eBook.API.Contracts.Data.Logic;
using EY.com.eBook.Core.EF.Read;
using System.Data.Objects;
using EY.com.eBook.Core.EntityTranslation;
using EY.com.eBook.API.BL.Translators;
using System.ServiceModel;
using EY.com.eBook.Core.Data;
using EY.com.eBook.API.Contracts.Proxies;
using EY.com.eBook.API.Contracts.Data.Cache;
using System.Text.RegularExpressions;
using EY.com.eBook.API.Contracts.Services;

namespace EY.com.eBook.API.BL.Read
{
    

    public class AccountSchemaRepository : eBookRepository
    {

        private static Regex ReNumeric = new Regex(@"^\d+?$");

        public List<AccountDataContract> GetAccountsMappingView(CriteriaAccountMappingViewDataContract cicdc)
        {

            ICacheService cp;
            return TryFetchICacheService(out cp)
                ? cp.GetAccountsAdjustedTree(cicdc.FileId, false, true, cicdc.Culture, cicdc.IncludeZeroes,
                                                    cicdc.MappedOnly, cicdc.Query)
                : LoadAccountsMappingView(cicdc.FileId, cicdc.Culture, cicdc.IncludeZeroes.GetValueOrDefault(),
                                                 cicdc.MappedOnly.GetValueOrDefault(), cicdc.Query);

        }

        private List<AccountDataContract> LoadAccountsMappingView(Guid fileId, string culture, bool includeZeroes,
                                                                  bool mappedOnly, string query)
        {
            var accounts = eBookReadManager
                .Context.Accounts.Include("Mappings").Include("Descriptions")
                .Where(a => a.FileId == fileId);

            if (!string.IsNullOrEmpty(query))
            {
                query = query.Trim().ToLowerInvariant();
                accounts = accounts
                    .Where(a => a.InternalNr.StartsWith(query) ||
                                a.Descriptions.Any(d => d.Culture == culture && d.Description.Contains(query)));
            }
            if (!includeZeroes)
            {
                accounts = accounts.Where(a => a.Saldo != 0);
            }
            if (mappedOnly)
            {
                accounts = accounts.Where(a => a.Mappings.Any());
            }

            return ConvertToAccountDataContracts(accounts, culture, fileId, false, true);
        }

        private List<AccountDataContract> ConvertToAccountDataContracts(IQueryable<Account> accounts, string culture,
                                                                      Guid fileId, bool includeDescriptions,
                                                                      bool includeMappings)
        {
            var returnList = accounts
                .ToList()
                .Select(a => new AccountDataContract
                {
                    Culture = culture,
                    DefaultDescription =
                        (a.Descriptions.FirstOrDefault(d => d.Culture == culture) ??
                         new AccountDescription { Description = "NO TRANSLATION" })
                             .Description,
                    ExistedLastYear = a.ExistedLastYear,
                    FileId = fileId,
                    HasTaxAdjustments = a.HasTaxAdjustments,
                    ImportedId = a.ImportedID,
                    InternalNr = a.InternalNr,
                    IsTaxAdjusment = a.IsTaxAdjustment,
                    LastChanged = a.LastChanged,
                    Mappings = includeMappings
                                   ? a.Mappings.Select(m => new AccountMappingDataContract
                                   {
                                       FileId = fileId,
                                       InternalNr = m.InternalNr,
                                       MappingItemId = m.MappingItemId,
                                       MappingKey = m.MappingKey,
                                       MappingMeta = m.MappingMeta,
                                       PreviousMappingItemId = m.PreviousMappingItemId
                                   }).ToList()
                                   : new List<AccountMappingDataContract>(),
                    PreviousSaldo = a.PreviousSaldo,
                    Saldo = a.Saldo,
                    StartSaldo = a.StartSaldo,
                    VisualNr = a.VisualNr,
                    Descriptions = includeDescriptions
                                       ? a.Descriptions.Select(
                                           d =>
                                           new AccountDescriptionDataContract
                                           {
                                               changed = false,
                                               Culture = d.Culture,
                                               Description = d.Description,
                                               FileId = fileId,
                                               InternalNr = a.InternalNr
                                           }).ToList()
                                       : new List<AccountDescriptionDataContract>()
                })
                .ToList();
            var taxAdjustments = returnList
                .Where(x => x.IsTaxAdjusment)
                .GroupBy(x => x.InternalNr.Split('.')[0], x => x)
                .ToDictionary(x => x.Key, x => x.ToList());
            foreach (var returnItem in returnList.Where(x => x.HasTaxAdjustments))
            {
                List<AccountDataContract> adjustments;
                if (taxAdjustments.TryGetValue(returnItem.InternalNr, out adjustments))
                    returnItem.Adjustments = adjustments;
            }

            return returnList;
        }
      

        public List<AccountDescriptionDataContract> GetAccountTranslations(CriteriaAccountDataContract cadc)
        {
            ICacheService proxy;
            return TryFetchICacheService(out proxy)
                       ? proxy.GetAccountTranslations(cadc.FileId, cadc.InternalNr)
                       : LoadAccountTranslations(cadc.FileId, cadc.InternalNr);
        }

        private List<AccountDescriptionDataContract> LoadAccountTranslations(Guid fileId, string internalNr)
        {
            var fixedUpNumber = internalNr.PadRight(12, '0');
            var account = eBookReadManager
                .Context.Accounts.Include("Descriptions")
                .SingleOrDefault(a => a.FileId == fileId && a.InternalNr == fixedUpNumber);
            if (account == null || account.Descriptions == null)
                return new List<AccountDescriptionDataContract>();

            return account
                .Descriptions
                .Select(d => new AccountDescriptionDataContract
                {
                    changed = false,
                    Culture = d.Culture,
                    Description = d.Description,
                    FileId = fileId,
                    InternalNr = account.InternalNr
                })
                .ToList();
        }


        public AccountDataContract GetAccount(CriteriaAccountDataContract cadc)
        {
            cadc.InternalNr = cadc.InternalNr.PadRight(12, '0');
            ICacheService proxy;
            return TryFetchICacheService(out proxy)
                       ? proxy.GetAccount(cadc.FileId, cadc.InternalNr,
                                          cadc.IncludeAllDescriptions.GetValueOrDefault(),
                                          cadc.IncludeMappingDetails.GetValueOrDefault(),
                                          cadc.Culture)
                       : LoadAccount(cadc.FileId, cadc.InternalNr,
                                     cadc.IncludeAllDescriptions.GetValueOrDefault(),
                                     cadc.IncludeMappingDetails.GetValueOrDefault(),
                                     cadc.Culture);
        }

        private AccountDataContract LoadAccount(Guid fileId, string internalNr, bool includeAllDescriptions,
                                                 bool includeMappingDetails,
                                                 string culture)
        {
            var accounts = eBookReadManager
                .Context.Accounts.Include("Mappings");
            if (includeAllDescriptions)
                accounts = accounts.Include("Descriptions");


            return
                ConvertToAccountDataContracts(accounts.Where(a => a.FileId == fileId && a.InternalNr == internalNr),
                                              culture, fileId, includeAllDescriptions, includeMappingDetails)
                    .FirstOrDefault();
        }



        public ResponseDataContract<List<StringListItemDataContract>> GetAccountsListByRanges(CriteriaAccountsByRangesDataContract cardc)
        {
            ICacheService proxy;
            var accounts = TryFetchICacheService(out proxy)
                               ? proxy.GetAccountsByRanges(cardc.FileId, cardc.Ranges.ToList(), true, false,
                                                           cardc.Culture)
                               : LoadAccountsByRanges(cardc.FileId, cardc.Ranges, true, false, cardc.Culture);


            IQueryable<AccountDataContract> qaccounts = accounts.AsQueryable();
            if (cardc.IncludeZeroes.HasValue)
            {
                if (!cardc.IncludeZeroes.Value) qaccounts = qaccounts.Where(a => a.Saldo != 0 || a.PreviousSaldo != 0);
            }
            if (!string.IsNullOrEmpty(cardc.Query))
            {
                qaccounts = qaccounts.Where(a => a.InternalNr.StartsWith(cardc.Query) || a.DefaultDescription.Contains(cardc.Query));
            }
            IOrderedQueryable<AccountDataContract> qoaccounts = qaccounts.OrderBy(q => q.InternalNr);
            List<StringListItemDataContract> accountList = (from a in (cardc.Start.HasValue && cardc.Limit.HasValue) ? qoaccounts.Skip(cardc.Start.Value).Take(cardc.Limit.Value) : qoaccounts
                                                            select a.GetListItem()).ToList();
            return new ResponseDataContract<List<StringListItemDataContract>>
            {
                Total = qoaccounts.Count()
                ,
                Data = accountList
                ,
                Success = true
            };
            //return cp.GetAccountTranslations(cadc.FileId, cadc.InternalNr);

        }

        private List<AccountDataContract> LoadAccountsByRanges(Guid fileId, string[] ranges, bool includeDescriptions,
                                                               bool includeMappings,
                                                               string culture)
        {
            if (ranges.Length == 0)
                return new List<AccountDataContract>();

            var accounts = eBookReadManager
                .Context.Accounts.Include("Mappings");
            if (includeDescriptions)
                accounts = accounts.Include("Descriptions");

            var query = accounts.Where(a => a.FileId == fileId);
            const string rangeQueryFormat = "InternalNr.StartsWith(@{0})";
            var dynamicQuery = ranges
                .Aggregate(new { QueryString = new List<string>(), Parameters = new List<object>() },
                           (s, x) =>
                           {
                               s.Parameters.Add(x);
                               s.QueryString.Add(string.Format(rangeQueryFormat, (s.Parameters.Count - 1)));
                               return s;
                           });

            query = query.Where(string.Join(" || ", dynamicQuery.QueryString.ToArray()),
                                dynamicQuery.Parameters.ToArray());

            return ConvertToAccountDataContracts(query, culture, fileId,
                                                 includeDescriptions, includeMappings);
        }

        public List<AccountDataContract> GetAllAccounts(Guid fileId, string culture)
        {
            return GetAllAccounts(fileId, culture, false,false);
        }

        public List<AccountDataContract> GetAllAccounts(Guid fileId, string culture,bool includeDescs, bool includeMappings)
        {
            ICacheService proxy;
            return TryFetchICacheService(out proxy)
                       ? proxy.GetAllAccounts(fileId, includeDescs, includeMappings, culture)
                       : LoadAllAccounts(fileId, includeDescs, includeMappings, culture);
        }

        private List<AccountDataContract> LoadAllAccounts(Guid fileId, bool includeDescs, bool includeMappings,
                                                          string culture)
        {
            var accounts = eBookReadManager
                .Context.Accounts.Include("Mappings");
            if (includeDescs)
                accounts = accounts.Include("Descriptions");

            return ConvertToAccountDataContracts(accounts.Where(a => a.FileId == fileId), culture, fileId,
                                                 includeDescs, includeMappings);
        }

        public ResponseDataContract<List<AccountDataContract>> GetAccountsDeep(CriteriaAccountsDataContract cardc)
        {
            ICacheService proxy;
            var qaccounts = (TryFetchICacheService(out proxy)
                                ? proxy.GetAllAccounts(cardc.FileId, true, true, cardc.Culture)
                                : LoadAllAccounts(cardc.FileId, true, true, cardc.Culture))
                .AsQueryable();



            if (!string.IsNullOrEmpty(cardc.Query))
            {
                qaccounts = qaccounts.Where(a => a.DefaultDescription.Contains(cardc.Query)
                                                || a.InternalNr.StartsWith(cardc.Query));
            }
            if (cardc.IncludeZeroes.HasValue)
            {
                if (!cardc.IncludeZeroes.Value) qaccounts = qaccounts.Where(a => a.Saldo != 0 || a.PreviousSaldo != 0);
            }

            return new ResponseDataContract<List<AccountDataContract>>
            {
                Total = qaccounts.Count()
                ,
                Data = (cardc.Start.HasValue && cardc.Limit.HasValue)
                        ? qaccounts.Skip(cardc.Start.Value).Take(cardc.Limit.Value).ToList()
                        : qaccounts.ToList()
                ,
                Success = true
            };

            //return cp.GetAccountTranslations(cadc.FileId, cadc.InternalNr);


        }

        public ResponseDataContract<List<StringListItemDataContract>> GetAccountsListByMapping(CriteriaAccountFilteredByMappingDataContract cafmdc)
        {
            var searchMeta = !string.IsNullOrEmpty(cafmdc.Meta);
            ICacheService proxy;
            var qaccounts = (TryFetchICacheService(out proxy)
                                ? proxy.GetAccountsOfMapping(cafmdc.FileId, cafmdc.Key, true, searchMeta, cafmdc.Culture)
                                : LoadAccountsOfMapping(cafmdc.FileId, cafmdc.Key, true, searchMeta, cafmdc.Culture))
                .AsQueryable();



            if (searchMeta)
            {
                qaccounts = qaccounts
                    .Where(a => a.Mappings.Any(m => m.MappingKey.ToUpper() == cafmdc.Key.ToUpper() &&
                                                    m.MappingMeta != null &&
                                                    m.MappingMeta.ToUpper() == cafmdc.Meta.ToUpper()));
            }


            if (!string.IsNullOrEmpty(cafmdc.Query))
            {
                qaccounts = qaccounts.Where(a => a.InternalNr.StartsWith(cafmdc.Query) || a.DefaultDescription.Contains(cafmdc.Query));
            }

            var accountList = (((cafmdc.Start.HasValue && cafmdc.Limit.HasValue)
                            ? qaccounts.Skip(cafmdc.Start.Value).Take(cafmdc.Limit.Value)
                            : qaccounts).Select(a => a.GetListItem())).ToList();
            return new ResponseDataContract<List<StringListItemDataContract>>
            {
                Total = qaccounts.Count()
                ,
                Data = accountList
                ,
                Success = true
            };
            //return cp.GetAccountTranslations(cadc.FileId, cadc.InternalNr);


        }

        private List<AccountDataContract> LoadAccountsOfMapping(Guid fileId, string mappingKey, bool includeDescriptions,
                                                                bool includeMappings, string culture)
        {
            throw new NotImplementedException();
        }

        public ResponseDataContract<List<StringListItemDataContract>> GetAccountsList(CriteriaAccountsDataContract cadc)
        {
            ICacheService cp;
            if (!TryFetchICacheService(out cp))
            {
                return new ResponseDataContract<List<StringListItemDataContract>>
                {
                    Data = new List<StringListItemDataContract>(),
                    Total = 0,
                    Success = false
                };
            }

            var textQuery = false;
            List<AccountDataContract> accounts;
            if (!string.IsNullOrEmpty(cadc.Query))
            {
                cadc.Query = cadc.Query.Trim();
                if (ReNumeric.IsMatch(cadc.Query))
                {
                    accounts = cp.GetAccountsByRanges(cadc.FileId, new List<string> { cadc.Query }, true, false,
                                                      cadc.Culture);
                }
                else
                {
                    accounts = cp.GetAllAccounts(cadc.FileId, true, false, cadc.Culture);
                    textQuery = true;
                }
            }
            else
            {
                accounts = cp.GetAllAccounts(cadc.FileId, true, false, cadc.Culture);
            }
            var qaccounts = accounts.AsQueryable();
            if (textQuery)
            {
                qaccounts = qaccounts
                    .Where(a => a.InternalNr.StartsWith(cadc.Query) ||
                                a.DefaultDescription.ToLower().Contains(cadc.Query.ToLower()));
            }


            return new ResponseDataContract<List<StringListItemDataContract>>
            {
                Total = qaccounts.Count(),
                Data = (qaccounts.OrderBy(a => a.InternalNr)
                                 .Skip(cadc.Start.GetValueOrDefault())
                                 .Take(cadc.Limit.GetValueOrDefault(qaccounts.Count()))
                                 .Select(a => a.GetListItem())).ToList(),
                Success = true
            };
        }


        public List<StringListItemDataContract> GetQueriedAccountsList(CriteriaAccountsDataContract cadc)
        {
            ICacheService cp;
            return TryFetchICacheService(out cp) && cp.FileInCache(cadc.FileId)
                       ? GetAccountsList(cp, cadc)
                       : LoadAccountsList(cadc);
        }

        /// <summary>
        ///     Queries the accounts list directly of the database.
        /// </summary>
        /// <param name="cadc">The cadc.</param>
        /// <returns></returns>
        /// <remarks>
        ///     database fallback (if caching not available).
        ///     Will occur frequently, since this can be called from the client view as well.
        /// </remarks>
        private List<StringListItemDataContract> LoadAccountsList(CriteriaAccountsDataContract cadc)
        {
            var dbaccounts = eBookReadManager
                .Context.Accounts.Include("Descriptions")
                .Where(a => a.FileId == cadc.FileId);

            if (!string.IsNullOrEmpty(cadc.Query))
            {
                cadc.Query = cadc.Query.Trim();
                if (ReNumeric.IsMatch(cadc.Query))
                {
                    dbaccounts = dbaccounts
                        .Where(a => a.InternalNr.StartsWith(cadc.Query));
                }
                else
                {
                    dbaccounts = dbaccounts
                        .Where(a => a.InternalNr.StartsWith(cadc.Query) ||
                                    a.Descriptions.Any(d => d.Culture == cadc.Culture &&
                                                            d.Description.Contains(cadc.Query)));
                }
            }

            return Translator.TranslateList<StringListItemDataContract, Account>(dbaccounts
                                                                                     .OrderBy(a => a.InternalNr)
                                                                                     .Take(20).ToList());
        }

        private static List<StringListItemDataContract> GetAccountsList(ICacheService cp,
                                                                        CriteriaAccountsDataContract cadc)
        {
            var textQuery = false;
            List<AccountDataContract> accounts;
            if (!string.IsNullOrEmpty(cadc.Query))
            {
                cadc.Query = cadc.Query.Trim();
                if (ReNumeric.IsMatch(cadc.Query))
                {
                    accounts = cp.GetAccountsByRanges(cadc.FileId, new List<string> { cadc.Query }, true,
                                                      false, cadc.Culture);
                }
                else
                {
                    accounts = cp.GetAllAccounts(cadc.FileId, true, false, cadc.Culture);
                    textQuery = true;
                }
            }
            else
            {
                accounts = cp.GetAllAccounts(cadc.FileId, true, false, cadc.Culture);
            }
            var qaccounts = accounts.AsQueryable();
            if (textQuery)
            {
                var lowerQuery = cadc.Query.ToLower();
                qaccounts = qaccounts
                    .Where(a => a.InternalNr.StartsWith(cadc.Query) ||
                                a.DefaultDescription.ToLower().Contains(lowerQuery));
            }

            return qaccounts.OrderBy(a => a.InternalNr).Take(20).Select(a => a.GetListItem()).ToList();
        }


        public List<AccountMappingDataContract> GetAccountMappings(Guid fileId, string accountNr)
        {
            ICacheService cp;
            return TryFetchICacheService(out cp)
                       ? cp.GetAccountMappings(fileId, accountNr)
                       : new List<AccountMappingDataContract>();
        }

        public decimal GetProfitLoss(Guid fileId)
        {
            ICacheService proxy;
            var accounts = TryFetchICacheService(out proxy)
                               ? proxy.GetAllAccounts(fileId, false, false, "nl-BE")
                               : LoadAllAccounts(fileId, false, false, "nl-BE");

            var tot6 = accounts
                .Where(a => a.InternalNr.StartsWith("6") &&
                            !a.InternalNr.StartsWith("69"))
                .Sum(a => a.Saldo);

            var tot7 = accounts
                .Where(a => a.InternalNr.StartsWith("7") &&
                            !a.InternalNr.StartsWith("79"))
                .Sum(a => a.Saldo);

            return tot7 + tot6;
        }

        public List<LeadSheetLineDataContract> GetLeadSheetAccounts(CriteriaIdAndCultureDataContract cicdc)
        {
            ICacheService proxy;
            var accounts = TryFetchICacheService(out proxy)
                               ? proxy.GetAllAccounts(cicdc.Id, false, false, cicdc.Culture)
                               : LoadAllAccounts(cicdc.Id, false, false, cicdc.Culture);

            return accounts
                .Where(a => a.Saldo != 0 || a.PreviousSaldo != 0)
                .Select(a => new LeadSheetLineDataContract
                    {
                        InternalNr = a.InternalNr,
                        StartBalance = a.StartSaldo,
                        Adjustments = a.Saldo - a.StartSaldo,
                        EndBalance = a.Saldo,
                        PreviousBalance = a.PreviousSaldo,
                        VisualNr = a.VisualNr,
                        Description = a.DefaultDescription
                    })
                .ToList();
        }

        

        #region Bookings 
        public List<BookingSingleDataContract> GetAutomaticBookings(CriteriaFileDataContract cbdc)
        {
            ICacheService cp;
            return GetGroupedBookings(TryFetchICacheService(out cp)
                                          ? cp.GetAutomaticBookings(cbdc.FileId)
                                          : LoadAutomaticBookings(cbdc.FileId));
        }

        public List<BookingSingleDataContract> GetSchemaBooking(CriteriaIdDataContract cidc)
        {
            var bookings = eBookReadManager.Context.Bookings
                                             .Include("BookingLines")
                                             .Include("File")
                                             .Include("BookingLines.Account")
                                             .Include("BookingLines.Account.Descriptions")
                //.Include("BusinessRelation")
                                             .Where(b => b.Id == cidc.Id
                                                         && (!b.WorksheetTypeId.HasValue
                                                             || (b.WorksheetTypeId.HasValue
                                                                 && b.WorksheetTypeId.Value == Guid.Empty)))
                                             .OrderBy(b => b.CreationDate)
                                             .ToList();


            return GetGroupedBookings(Translator.TranslateList<BookingDataContract, Booking>(bookings, TranslateLevel.Infinite)); 
        }

        public List<BookingSingleDataContract> GetGroupedBookings(List<BookingDataContract> myBookings)
        {
            List<BookingSingleDataContract> Bookings = new List<BookingSingleDataContract>();
            int nr = 1;
            foreach (BookingDataContract b in myBookings)
            {
                if (b.BookingLines != null)
                {
                    
                       
                   
                    Bookings.AddRange((from bl in b.BookingLines.OrderBy(l => l.AccountNr.Id)
                                      
                                       select new BookingSingleDataContract
                                       {
                                           Account = bl.AccountNr
                                           ,
                                           BookingDescription = b.Description
                                           ,
                                           BookingId = b.Id
                                           ,
                                           GroupNr = nr
                                           ,
                                           BookingCreationDate = b.CreationDate
                                           ,
                                           Credit = bl.Amount < 0 ? Math.Abs(bl.Amount) : (decimal?)null
                                           ,
                                           Debet = bl.Amount > 0 ? bl.Amount : (decimal?)null
                                           ,
                                           ExportCount = b.ExportCount
                                           ,
                                           Hidden = b.Hidden
                                           ,
                                           Id = bl.Id
                                           ,
                                           WorksheetBookingKey = b.WorksheetBookingKey
                                           ,
                                           WorksheetCollection = b.WorksheetCollection
                                           ,
                                           WorksheetRowId = b.WorksheetRowId
                                           ,
                                           WorksheetTypeId = b.WorksheetTypeId
                                           , 
                                           ClientSupplierId = bl.BusinessRelationId
                                           ,
                                           ClientSupplierName = bl.BusinessRelationName
                                           ,
                                           Comment = bl.Comment
                                       }).ToList());
                    nr++;


                    
                }
            }
            return Bookings;
        }

        public List<BookingSingleDataContract> GetAllbookings(CriteriaFileDataContract cfdc)
        {
            ICacheService cp;
            return GetGroupedBookings(TryFetchICacheService(out cp)
                                          ? cp.GetAllBookings(cfdc.FileId)
                                          : LoadManualBookings(cfdc.FileId)
                                                .Union(LoadAutomaticBookings(cfdc.FileId))
                                                .ToList());
        }


        public List<BookingSingleDataContract> GetManualBookings(CriteriaFileDataContract cfdc)
        {
            ICacheService cp;
            return GetGroupedBookings(TryFetchICacheService(out cp)
                                          ? cp.GetManualBookings(cfdc.FileId)
                                          : LoadManualBookings(cfdc.FileId));
        }



        public List<BookingSingleDataContract> GetWorksheetBookings(CriteriaWorksheetDataContract cwdc)
        {
            ICacheService cp;
            return GetGroupedBookings(TryFetchICacheService(out cp)
                                          ? cp.GetWorksheetBookings(cwdc.FileId, cwdc.TypeId)
                                          : LoadWorksheetBookings(cwdc));
        }

        public List<BookingDataContract> LoadWorksheetBookings(CriteriaWorksheetDataContract cwdc)
        {
            var bookings = eBookReadManager.Context.Bookings
                                             .Include("File")
                                             .Include("BookingLines")
                                             .Include("BookingLines.Account")
                                             .Include("BookingLines.Account.Descriptions")
                                             .Where(b => b.File.Id == cwdc.FileId
                                                         && (b.WorksheetTypeId.HasValue
                                                             && b.WorksheetTypeId.Value == cwdc.TypeId))
                                             .OrderBy(b => b.CreationDate)
                                             .ToList();


            return Translator.TranslateList<BookingDataContract, Booking>(bookings, TranslateLevel.Infinite);
        }


        public List<BookingDataContract> LoadManualBookings(Guid fileId)
        {
            var bookings = eBookReadManager.Context.Bookings
                                             .Include("BookingLines")
                                             .Include("File")
                                             .Include("BookingLines.Account")
                                             .Include("BookingLines.Account.Descriptions")
                                             //.Include("BusinessRelation")
                                             .Where(b => b.File.Id == fileId
                                                         && (!b.WorksheetTypeId.HasValue
                                                             || (b.WorksheetTypeId.HasValue
                                                                 && b.WorksheetTypeId.Value == Guid.Empty)))
                                             .OrderBy(b => b.CreationDate)
                                             .ToList();


            return Translator.TranslateList<BookingDataContract, Booking>(bookings, TranslateLevel.Infinite);
        }

        public List<BookingDataContract> LoadAutomaticBookings(Guid fileId)
        {
            var bookings = eBookReadManager.Context.Bookings
                                             .Include("BookingLines")
                                             .Include("File")
                                             .Include("BookingLines.Account")
                                             .Include("BookingLines.Account.Descriptions")
                                             .Where(b => b.File.Id == fileId
                                                         && (b.WorksheetTypeId.HasValue
                                                             && b.WorksheetTypeId.Value != Guid.Empty))
                                             .OrderBy(b => b.CreationDate)
                                             .ToList();


            return Translator.TranslateList<BookingDataContract, Booking>(bookings, TranslateLevel.Infinite);
        }

        /// <summary>
        /// Gets the openings booking.
        /// </summary>
        /// <param name="cbdc">The cbdc.</param>
        /// <returns></returns>
        public List<AccountStateDataContract> GetState(Guid fileId, string culture,FileSchemaState fileSchemaState )
        {
            List<AccountDataContract> accounts = GetAllAccounts(fileId, culture);
            List<AccountStateDataContract> states = new List<AccountStateDataContract>();

            if (fileSchemaState == FileSchemaState.Start)
            {
                states = (from a in accounts
                          where a.StartSaldo != 0
                          select new AccountStateDataContract
                          {
                              FileId = a.FileId
                              ,
                              InternalNr = a.InternalNr
                              ,
                              VisualNr = a.VisualNr
                              ,
                              Description = a.DefaultDescription
                              ,
                              Debet = a.StartSaldo > 0 ? a.StartSaldo : 0
                              ,
                              Credit = a.StartSaldo < 0 ? Math.Abs(a.StartSaldo) : 0
                          }).ToList();
            }
            else
            {
                states = (from a in accounts
                          where a.StartSaldo != 0
                          select new AccountStateDataContract
                          {
                              FileId = a.FileId
                              ,
                              InternalNr = a.InternalNr
                              ,
                              VisualNr = a.VisualNr
                              ,
                              Description = a.DefaultDescription
                              ,
                              Debet = a.PreviousSaldo > 0 ? a.PreviousSaldo : 0
                              ,
                              Credit = a.PreviousSaldo < 0 ? Math.Abs(a.PreviousSaldo) : 0
                          }).ToList();
            }
            return states;
        }


        public List<FinalTrialBalanceLineDataContract> GetFinalTrialBalance(CriteriaIdAndCultureDataContract cicdc)
        {
            List<AccountDataContract> accounts = GetAllAccounts(cicdc.Id, cicdc.Culture,true,false);

            var fts = from ac in accounts.Where(a=>!a.IsTaxAdjusment)
                      where ac.PreviousSaldo != 0 || ac.Saldo != 0
                            && !ac.IsTaxAdjusment && !ac.InternalNr.Contains('.')
                      select new FinalTrialBalanceLineDataContract
                      {
                          Account = ac.GetListItem()
                          ,
                          AdjustmentsCredit = (ac.Saldo - ac.PreviousSaldo) < 0 ? (ac.Saldo - ac.PreviousSaldo) : 0
                          ,
                          AdjustmentsDebet = (ac.Saldo - ac.PreviousSaldo) > 0 ? (ac.Saldo - ac.PreviousSaldo) : 0
                          ,
                          Description = ac.DefaultDescription
                          ,
                          EndBalance = ac.Saldo
                          ,
                          StartBalance = ac.PreviousSaldo
                          ,
                          InternalAccountNr = ac.InternalNr
                          ,
                          VisualNr = ac.VisualNr
                      };
            return fts.ToList();
        }

        /// <summary>
        /// Gets the booking.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <returns></returns>
        public BookingDataContract GetBooking(Guid id)
        {
            var booking = eBookReadManager.Context.Bookings.FirstOrDefault(b => b.Id == id);
            if (booking == null)
                throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { Message = String.Format("No booking found for id {0}}", id) });

            return Translator.Translate<BookingDataContract>(booking, TranslateLevel.Infinite);
        }

        public List<BookingDataContract> GetProAccExportBookings(Guid fileId)
        {
            var bookings = eBookReadManager.Context.Bookings
                            .Include("BookingLines")
                            .Include("BookingLines.Account")
                            .Include("BookingType")
                            .Include("File")
                            .Include("JournalType")
                            .Where(b => b.File.Id == fileId)
                            .OrderBy(b => b.CreationDate);
            return Translator.TranslateList<BookingDataContract, Booking>(bookings.ToList(), TranslateLevel.Infinite);
        }

        public List<BookingDataContract> GetBookings(CriteriaIdAndCultureDataContract cicdc)
        {
            var bookings = eBookReadManager.Context.Bookings
                            .Include("BookingLines")
                            .Include("BookingLines.Account")
                            .Include("BookingLines.Account.Descriptions")
                            .Include("BookingType")
                            .Include("File")
                            .Include("JournalType")
                            .Where(b => b.File.Id == cicdc.Id)
                            .OrderBy(b => b.CreationDate);
            return Translator.TranslateList<BookingDataContract, Booking>(bookings.ToList(), TranslateLevel.Infinite, cicdc.Culture);
        }

        public List<BookingSingleDataContract> GetBookingLinesByAccountNr(CriteriaBookingLineDataContract cbldc) 
        {
            var bookingLines = eBookReadManager.Context.BookingLines
                            .Include("Booking")
                            .Include("Account")
                            .Include("Account.Descriptions")
                            .Include("Booking.File")
                            .Where(b => b.Booking.File.Id == cbldc.FileId && b.Account.InternalNr == cbldc.AccountNr)
                            ;

            
            
            if (bookingLines != null)
            {
                CriteriaFileDataContract cfdc = new CriteriaFileDataContract{
                    FileId = cbldc.FileId
                };
                List<BookingSingleDataContract> groupedBookings = GetAllbookings(cfdc);

                List<BookingSingleDataContract> lbldc = new List<BookingSingleDataContract>();
                foreach (var bl in bookingLines)
                {
                    BookingSingleDataContract gb = groupedBookings.FirstOrDefault(x=>x.Id == bl.Id);
                    var br = eBookReadManager.Context.BusinessRelations.FirstOrDefault(b => b.Id == bl.BusinessRelationId);
                    lbldc.Add(new BookingSingleDataContract{
                        AccountNr = bl.Account.VisualNr                        
                        ,
                        BookingDescription = bl.Booking.Description
                        ,
                        BookingId = bl.Booking.Id
                        ,
                        GroupNr = gb != null ? gb.GroupNr : 0
                        ,
                        BookingCreationDate = bl.Booking.CreationDate
                        ,
                        Credit = bl.Amount < 0 ? Math.Abs(bl.Amount) : (decimal?)null
                        ,
                        Debet = bl.Amount > 0 ? bl.Amount : (decimal?)null
                        ,
                        ExportCount = bl.Booking.ExportCount
                        ,
                        Hidden = bl.Booking.HiddenFlag
                        ,
                        Id = bl.Id
                        ,
                        WorksheetBookingKey = bl.Booking.WorksheetBookingKey
                        ,
                        WorksheetCollection = bl.Booking.WorksheetCollection
                        ,
                        WorksheetRowId = bl.Booking.WorksheetRowId
                        ,
                        WorksheetTypeId = bl.Booking.WorksheetTypeId
                        , 
                        ClientSupplierId = bl.BusinessRelationId
                        ,
                        ClientSupplierName = br !=null ?  br.Name : null
                        ,
                        Comment = bl.Comment
                    });
                }
                return lbldc.OrderBy(x=>x.GroupNr).ToList();
            }
            else {
                return null;
            }
        }

        #endregion


        protected override void RegisterTranslators()
        {
            Translator.RegisterEntityTranslator(new BookingTranslator());
            Translator.RegisterEntityTranslator(new FinalTrialBalanceLineTranslator());
            Translator.RegisterEntityTranslator(new BookingLineTranslator());
            Translator.RegisterEntityTranslator(new BookingSingleTranslator());
            Translator.RegisterEntityTranslator(new AccountStringListItemTranslator());

            
        }

       
    }
}
