﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EY.com.eBook.Core;
using EY.com.eBook.Core.EF;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.API.BL.Translators;
using System.ServiceModel;
using EY.com.eBook.Core.Data;
using EY.com.eBook.Core.EF.Read;

namespace EY.com.eBook.API.BL.Read
{

    public class ListRepository : RepositoryBase
    {
        public List<FodListDataContract> GetFodListItems(CriteriaFodListDataContract cfldc)
        {
            IQueryable<FodList> fodList = eBookReadManager.Context.FodLists.Where(f => f.ListId == cfldc.ListId && f.Culture == cfldc.Culture);
            if (!string.IsNullOrEmpty(cfldc.Query))
            {
                fodList = fodList.Where(f => f.Description.StartsWith(cfldc.Query));
            }
            return Translator.TranslateList<FodListDataContract, FodList>(fodList.ToList());
        }

        public FodListDataContract GetFodListItem(CriteriaFodListDataContract cfldc, string itemId)
        {
            FodList fodList = eBookReadManager.Context.FodLists.FirstOrDefault(f => f.ListId == cfldc.ListId
                                                            && f.Culture == cfldc.Culture
                                                            && f.ItemId == itemId);
            if (fodList == null) return null;
            return Translator.Translate<FodListDataContract, FodList>(fodList);
        }

        public string GetFodListItemIdByDescription(CriteriaFodListDataContract cfldc, string description)
        {
            FodList fodList = eBookReadManager.Context.FodLists.FirstOrDefault(f => f.ListId == cfldc.ListId
                                                            && f.Description.ToLower() == description.ToLower());
            if (fodList == null) return null;
            return fodList.ItemId;
        }

        public List<LegalTypeDataContract> GetLegalTypes(CriteriaCultureDataContract ccdc)
        {
            DateTime now = DateTime.Now;
            List<LegalTypeTranslation> translations = eBookReadManager.Context.LegalTypeTranslationSet
                                                        .Include("LegalType")
                                                        .Where(l => (!l.LegalType.Start.HasValue || l.LegalType.Start.Value < now)
                                                                    && (!l.LegalType.End.HasValue || l.LegalType.End.Value > now)
                                                                    && l.Culture == ccdc.Culture)
                                                        .ToList();

            return Translator.TranslateList<LegalTypeDataContract, LegalTypeTranslation>(translations);
        }

        public GuidListItemDataContract GetListItem(Guid id)
        {
            ListItem lit = eBookReadManager.Context.ListItemSet.Include("List").Include("Names").FirstOrDefault(li => li.Id == id);
            if (lit == null) return null;
            return Translator.Translate<GuidListItemDataContract, ListItem>(lit);
        }

        public List<StringListItemDataContract> GetFodGlobalList(CriteriaFodListDataContract cfldc)
        {
            List<FodList> fodList = eBookReadManager.Context.FodLists.Where(f => f.ListId == cfldc.ListId).ToList();

            List<string> ids = fodList.Select(f => f.ItemId).Distinct().ToList();
            List<StringListItemDataContract> list = new List<StringListItemDataContract>();

            foreach (string id in ids)
            {
                StringListItemDataContract slidc = new StringListItemDataContract { Id = id };
                foreach (FodList fl in fodList.Where(f => f.ItemId == id))
                {
                    switch (fl.Culture.ToUpper())
                    {
                        case "NL-BE":
                            slidc.NL = fl.Description;
                            break;
                        case "FR-FR":
                            slidc.FR = fl.Description;
                            break;
                        case "EN-US":
                            slidc.EN = fl.Description;
                            break;
                    }
                }
                list.Add(slidc);
            }
            list = list.OrderBy(l => l.NL).ToList();
            return list;
        }

        public List<GuidListItemDataContract> GetList(CriteriaListDataContract cldc) 
        {
            IQueryable<ListItem> listItems;

            if (cldc.ListId.HasValue)
            {


                listItems = eBookReadManager.Context.ListItemSet
                                                      .Include("List")
                                                      .Include("Names")
                                                      .Where(l => l.List.Id == cldc.ListId);
            }
            else if (!string.IsNullOrEmpty(cldc.ListKey))
            {
                cldc.ListKey = cldc.ListKey.ToUpper();
                listItems = eBookReadManager.Context.ListItemSet
                                                      .Include("List")
                                                      .Include("Names")
                                                      .Where(l => l.List.Name.ToUpper() == cldc.ListKey);
            }
            else
            {
                return new List<GuidListItemDataContract>();
                // throw exception
            }

            if (cldc.AssessmentYear.HasValue && (!cldc.StartDate.HasValue || !cldc.EndDate.HasValue))
            {
                 // calculate minimum startdate bookyear for assessment = end 31/12 previous year - 24 months
                cldc.StartDate = new DateTime(cldc.AssessmentYear.Value - 1, 12, 31).AddMonths(-24);
                cldc.StartDate = new DateTime(cldc.StartDate.Value.Year, cldc.StartDate.Value.Month, 1);

                // calculate maximum enddate bookyear = end 30/12/assessmentyear
                cldc.EndDate = new DateTime(cldc.AssessmentYear.Value, 12, 30);
                
            }

            if (!cldc.AssessmentYear.HasValue && cldc.StartDate.HasValue && cldc.EndDate.HasValue)
            {
                //listItems = listItems.Where(l => ((l.StartDate.HasValue && l.StartDate.Value >= cldc.StartDate.Value)
                //                                && (l.EndDate.HasValue && l.EndDate.Value <= cldc.EndDate))
                //                                || (!l.AssessmentYear.HasValue && !l.StartDate.HasValue && !l.EndDate.HasValue));
                 listItems = listItems.Where(l => (l.StartDate.HasValue && l.StartDate.Value <=cldc.EndDate
                                                    && (!l.EndDate.HasValue ||  l.EndDate.Value > cldc.StartDate)
                                                )
                                                || (!l.AssessmentYear.HasValue && !l.StartDate.HasValue && !l.EndDate.HasValue));
            }
            else if (cldc.AssessmentYear.HasValue && cldc.StartDate.HasValue && cldc.EndDate.HasValue)
            {


                listItems = listItems.Where(l => (l.AssessmentYear.HasValue && l.AssessmentYear.Value == cldc.AssessmentYear.Value
                                                || (!l.StartDate.HasValue && !l.EndDate.HasValue)
                                                || (l.StartDate.HasValue && l.StartDate.Value <= cldc.EndDate
                                                    && (!l.EndDate.HasValue || l.EndDate.Value > cldc.StartDate)
                                                )));
            }

            //.OrderBy(l => l.Name)
            return Translator.TranslateList<GuidListItemDataContract, ListItem>(listItems.OrderByDescending(x => x.Order).ToList(), cldc.Culture).ToList();

        }

        public List<StringListItemDataContract> GetYears(CriteriaYearsDataContract cydc)
        {
            List<StringListItemDataContract> years = new List<StringListItemDataContract>();
            File file = eBookReadManager.Context.Files.FirstOrDefault(f => f.Id == cydc.FileId);
            if (file == null) throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { Message = "File not found!" });

            int offsetBase = cydc.Base.ToLower() == "end" ? file.EndDate.Year : file.EndDate.AddDays(1).Year;
            int year = offsetBase + cydc.From;
            if (year > (offsetBase + cydc.To)) throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { Message = "From is larger then To!" });

            while (year <= (offsetBase + cydc.To))
            {
                if (cydc.Min.HasValue && year < cydc.Min.Value) { year++; continue; }
                if (cydc.Max.HasValue && year > cydc.Max.Value) { year++; continue; }
                years.Add(new StringListItemDataContract { Id = year.ToString(), NL = year.ToString(), FR = year.ToString(), EN = year.ToString(), Value = year });
                year++;
            }
            return years;

        }

        public List<StringListItemDataContract> GetBookyearMonths(CriteriaMonthListDataContract cmldc)
        {
            List<StringListItemDataContract> months = new List<StringListItemDataContract>();
            
            DateTime sDate = cmldc.From;

            while (sDate < cmldc.To.AddYears(1))
            {
                string m = sDate.Month.ToString().PadLeft(2, '0');
                string id=string.Format("{0}/{1}", sDate.Year, m);
                months.Add(new StringListItemDataContract { Id = id,NL=id,FR=id, EN=id });
                sDate = sDate.AddMonths(1);
            }
            return months;

        }

        



        protected override void RegisterTranslators()
        {
            Translator.RegisterEntityTranslator(new FodListTranslator());
            Translator.RegisterEntityTranslator(new LegalTypeTranslator());
            Translator.RegisterEntityTranslator(new ListItemTranslator());
            Translator.RegisterEntityTranslator(new GuidListItemTranslator());
            Translator.RegisterEntityTranslator(new CoefficientTranslator());
        }

        
    }
}
