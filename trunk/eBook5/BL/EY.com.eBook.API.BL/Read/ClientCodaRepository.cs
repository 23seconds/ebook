﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Linq.Expressions;
using System.Data.Entity;
using System.Text;
using System.Data.Objects;
using EY.com.eBook.Core.EF;
using EY.com.eBook.Core.EF.Read;
using EY.com.eBook.Core.EntityTranslation;
using EY.com.eBook.API.BL.Translators;
using System.ServiceModel;
using EY.com.eBook.Core.Data;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.API.Contracts.Data.Logic;
using EY.com.eBook.Core;

namespace EY.com.eBook.API.BL.Read
{
    public class ClientCodaRepository : eBookRepository
    {
        /// <summary>
        /// Gets only utilized banks for CODA POA
        /// </summary>
        /// <returns>List of banks</returns>
        public List<BankDataContract> GetBanks()
        {
            var lBics = eBookReadManager.Context.ClientCodaPoaSet.Include("BankInfo").Select(c => c.BankInfo.Bic).Distinct().ToList();
            var lBanks = eBookReadManager.Context.BankTranslations.OrderBy(b => b.En).ToList();
            var banks = lBanks.Where(b => lBics.Contains(b.Bic)).GroupBy(b => b.En).ToDictionary(b => b.Key, b => b.Select(s => s.Bic).ToArray());

            return banks.Keys.Select(b => new BankDataContract
            {
                Id = string.Join("_", banks[b]),
                Name = b
            }).ToList();
        }

        /// <summary>
        /// Gets all possible CODA POA statuses allowed to be set by users
        /// </summary>
        /// <returns>List of CODA POA statuses</returns>
        public List<StatusDataContract> GetCodaPoaStatusList(CriteriaIdBoolPersonDataContract cibpdc)
        {
            if (cibpdc.ActivePerson == null) cibpdc.ActivePerson = new BusinessHelper().DecodePerson(cibpdc.EncodedActivePerson);

            IQueryable<ClientCodaPoaStatuses> ccpss = eBookReadManager.Context.ClientCodaPoaStatusesSet
                .Where(p => String.IsNullOrEmpty(p.OnlyVisibleTo) || p.OnlyVisibleTo.Contains(cibpdc.ActivePerson.ActiveRole)) 
                .AsQueryable();

            if (cibpdc.Boolean) //get only statuses that can be activated by user
            {
                ccpss = ccpss.Where(s => s.Activation == "User").OrderBy(s => s.Rank);
            }

            return ccpss.Select(s => new StatusDataContract
            {
                Id = s.Id,
                StatusName = s.Status,
                Rank = s.Rank
            }).ToList();
        }

        public List<ClientCodaPoa> QueryCodaPoa(CriteriaCodaPoaDataContract ccpdc)
        {
            var poas = eBookReadManager.Context.ClientCodaPoaSet
                            .Include("Status")
                            .Include("Client")
                            .Include("Client.ClientDepartments")
                            .Include("BankInfo")
                            .Where(p => String.IsNullOrEmpty(p.Status.OnlyVisibleTo) || p.Status.OnlyVisibleTo.Contains(ccpdc.ActiveRole)) //If onlyVisibleTo has a value it has to match the active role
                            .AsQueryable();

            if (ccpdc.ActiveRole != "CODA Admin" && ccpdc.ActiveRole != "Admin" && ccpdc.ActiveRole != "DEVELOPER") //CODA Admin & Admin can see all
            {
                poas = poas.Where(p => p.Client.PersonRoles.Count(r => r.PersonId == ccpdc.PersonId) > 0);
            }
            if (ccpdc.activeClientACR.HasValue)
            {
                poas = poas.Where(p => p.Client.ClientDepartments.Count(d => d.DepartmentId == "ACR" && d.EndDate.HasValue != ccpdc.activeClientACR) > 0); //if ACR department end date, then not active   
            }
            if (!string.IsNullOrEmpty(ccpdc.BankAccount))
            {
                poas = poas.Where(p => p.BancAccountNr.Contains(ccpdc.BankAccount));
            }
            if (!string.IsNullOrEmpty(ccpdc.BankId))
            {
                poas = poas.Where(p => ccpdc.BankId.Contains(p.BankInfo.Bic));
            }
            if (!string.IsNullOrEmpty(ccpdc.ClientName))
            {
                poas = poas.Where(p => p.Client.Name.Contains(ccpdc.ClientName) || p.Client.GFISCode.Contains(ccpdc.ClientName));
            }
            if (ccpdc.LastModified.HasValue)
            {
                DateTime start = ccpdc.LastModified.Value.Date;
                DateTime end = start.AddDays(1);
                poas = poas.Where(p => p.LastModified >= start && p.LastModified < end);
            }
            if (ccpdc.StatusModified.HasValue)
            {
                DateTime start = ccpdc.StatusModified.Value.Date;
                DateTime end = start.AddDays(1);
                poas = poas.Where(p => p.StatusModified >= start && p.StatusModified < end);
            }
            if (ccpdc.StatusId.HasValue)
            {
                poas = poas.Where(p => p.Status.Id == ccpdc.StatusId.Value);
            }
            if (ccpdc.AllowCoda.HasValue)
            {
                poas = poas.Where(p => p.PmtAllowCoda == ccpdc.AllowCoda);
            }
            if (ccpdc.EOL_EYLicense.HasValue)
            {
                poas = poas.Where(p => p.Client.EOL_EYLicense == ccpdc.EOL_EYLicense);
            }
            if (ccpdc.EOL_DigMailbox.HasValue)
            {
                poas = poas.Where(p => p.Client.EOL_DigMailbox == ccpdc.EOL_DigMailbox);
            }
            if (!string.IsNullOrEmpty(ccpdc.Bic))
            {
                poas = poas.Where(p => p.BankInfo.Bic.Contains(ccpdc.Bic));
            }
            if (!string.IsNullOrEmpty(ccpdc.SortField) && !string.IsNullOrEmpty(ccpdc.SortOrder))
            {
                //sort the list using the given sortstate
                ccpdc.SortField = ccpdc.SortField.First().ToString().ToUpper() + ccpdc.SortField.Substring(1);
                Type clientCodaPoaType = typeof(ClientCodaPoa);
                //Create a dynamic lambda ClientCodaPoa parameter
                ParameterExpression param = Expression.Parameter(clientCodaPoaType, "x");
                Func<ClientCodaPoa, dynamic> sortBy;
                Expression paramExpression = null;
                Expression clientExpression;
                Expression bankExpression;
                Expression clientDepartmentsExpression;
                Expression statusExpression;
                Type clientType = typeof(Client);
                Type bankInfoType = typeof(BankTranslation);
                Type codaPoaStatusType = typeof(ClientCodaPoaStatuses);

                try
                {
                    switch(ccpdc.SortField)
                    {
                        case "ClientGfis":
                            clientExpression = Expression.Property(param, "Client");
                            paramExpression = Expression.Property(clientExpression, "GFISCode");
                            break;
                        case "ClientName":
                            clientExpression = Expression.Property(param, "Client");
                            paramExpression = Expression.Property(clientExpression, "Name");
                            break;
                        case "EOL_EYLicense":
                            clientExpression = Expression.Property(param, "Client");
                            paramExpression = Expression.Property(clientExpression, "EOL_EYLicense");
                            break;
                        case "EOL_DigMailbox":
                            clientExpression = Expression.Property(param, "Client");
                            paramExpression = Expression.Property(clientExpression, "EOL_DigMailbox");
                            break;
                        case "EOL_Division":
                            clientExpression = Expression.Property(param, "Client");
                            paramExpression = Expression.Property(clientExpression, "EOL_Division");
                            break;
                        /*case "ActiveClientACR":
                            clientDepartmentsExpression = Expression.Property(param, "Client.ClientDepartments");
                            paramExpression = Expression.Property(clientDepartmentsExpression, "EndDate");
                            break;*/
                        case "Bic":
                            bankExpression = Expression.Property(param, "BankInfo");
                            paramExpression = Expression.Property(bankExpression, "Bic");
                            break;
                        case "BankName":
                            bankExpression = Expression.Property(param, "BankInfo");
                            paramExpression = Expression.Property(bankExpression, "En");
                            break;
                        case "Status":
                            statusExpression = Expression.Property(param, "Status");
                            paramExpression = Expression.Property(statusExpression, "status");
                            break;
                        case "BankAccount":
                            paramExpression = Expression.Property(param, "BancAccountNr");
                            break;
                        case "AllowCoda":
                            paramExpression = Expression.Property(param, "PmtAllowCoda");
                            break;
                        default:
                            paramExpression = Expression.Property(param, ccpdc.SortField);
                            break;
                    } 
                
                    //Set the property to whatever order field has been given
                
                    //Create lambda
                    if (paramExpression != null)
                    {
                        sortBy = Expression.Lambda<Func<ClientCodaPoa, dynamic>>(Expression.TypeAs(paramExpression, typeof(object)), param).Compile();

                        if (sortBy != null)
                        {
                            if (ccpdc.SortOrder == "ASC")
                            {
                                return poas.OrderBy(sortBy).ToList();
                            }
                            else if (ccpdc.SortOrder == "DESC")
                            {
                                return poas.OrderByDescending(sortBy).ToList();
                            }
                        }
                    }
                }
                catch(ArgumentException a)
                {
                    throw;
                }
            }

            return poas.ToList();
        }
       

        /// <summary>
        /// Gets all clients their CODA Powers of Attorney along with status, bank and client information based on criteria given
        /// </summary>
        /// <param name="cccpdc"></param>
        /// <returns>List of CODA POA's</returns>
        public ResponseDataContract<List<CodaPoaDataContract>> GetCodaPoa(CriteriaCodaPoaDataContract ccpdc)
        {
            var poas = QueryCodaPoa(ccpdc);
            int cnt = poas.Count();

            List<CodaPoaDataContract> rpoas = poas.Select(p=>new CodaPoaDataContract {
                BankAccount = p.BancAccountNr,
                BankName = p.BankInfo.En,
                Bic = p.BankInfo.Bic,
                ClientId = p.Client.Id,
                ClientName = p.Client.Name,
                ClientGfis = p.Client.GFISCode,
                DocStoreId = p.DocStoreId,
                RepositoryItemId = p.RepositoryItemId,
                StatusId = p.Status.Id,
                StatusName = p.Status.Status,
                LastModified = p.LastModified,
                EOL_EYLicense = p.Client.EOL_EYLicense,
                EOL_DigMailbox = p.Client.EOL_DigMailbox,
                EOL_division = p.Client.EOL_Division,
                AllowCoda = p.PmtAllowCoda,
                StatusModified = p.StatusModified,
                activeClientACR = p.Client.ClientDepartments.Any(d => d.DepartmentId == "ACR" && !d.EndDate.HasValue)
            }).ToList();

            var rdc = new ResponseDataContract<List<CodaPoaDataContract>>
            {
                Data = ccpdc.Start.HasValue && ccpdc.Limit.HasValue ? rpoas.Skip(ccpdc.Start.Value).Take(ccpdc.Limit.Value).ToList() : rpoas.ToList(),
                Total = cnt
            };

            return rdc;
        }


        /// <summary>
        /// Update the Coda Poa
        /// </summary>
        /// <param name="cisdc">UpdateCodaPoaDataContract</param>
        public bool UpdateCodaPoa(UpdateCodaPoaDataContract ucpdc)
        {
            List<string> changes = new List<string>();
            string comment = String.Empty;

            //retrieve record
            ClientCodaPoa ccp = eBookReadManager.Context.ClientCodaPoaSet.FirstOrDefault(d => d.BancAccountNr == ucpdc.Id);

            //If order is important as action will be set depending on change
            if (!string.IsNullOrEmpty(ucpdc.Comment))
            {
                //optional comment
                changes.Add("comment");
                comment = ucpdc.Comment;
            }

            if (ucpdc.Status != null)
            {
                //status change
                changes.Add("status");
                ClientCodaPoaStatuses status = eBookReadManager.Context.ClientCodaPoaStatusesSet.FirstOrDefault(d => d.Id == ucpdc.Status);
                ccp.Status = status;
                ccp.StatusModified = DateTime.Now;
            }

            if (ucpdc.RepositoryItem != null)
            {
                //repositoryItemId change
                changes.Add("file");
                ccp.RepositoryItemId = ucpdc.RepositoryItem;
            }

            ccp.LastModified = DateTime.Now;

            //Person and action required
            if ((ucpdc.ActivePerson != null || !string.IsNullOrEmpty(ucpdc.EncodedActivePerson)) && changes.Count > 0)
            {
                //save
                eBookReadManager.Context.SaveChanges();
                if (ucpdc.ActivePerson == null) ucpdc.ActivePerson = new BusinessHelper().DecodePerson(ucpdc.EncodedActivePerson);

                string seperator = ",";
                string actions = String.Join(seperator, changes.ToArray());
                //create log entry
                CreateCodaPoaLog(ccp, actions, comment, ucpdc.ActivePerson.Id);

                return true;
            }

            return false;
        }

        /// <summary>
        /// Create a log entry with the purpose of recording Coda Poa changes
        /// </summary>
        /// <param name="ccp">coda poa record</param>
        /// <param name="action">description of the taken action</param>
        /// <param name="comment">user comment</param>
        /// <param name="personId">active person</param>
        public void CreateCodaPoaLog(ClientCodaPoa ccp, string actions, string comment, Guid personId)
        {
            Person person = eBookReadManager.Context.PersonSet.FirstOrDefault(p => p.Id == personId);
            
            //Create log record
            ClientCodaPoaLog ccpl = new ClientCodaPoaLog
            {
                Id = Guid.NewGuid(),
                BankAccountNr = ccp.BancAccountNr,
                StatusId = ccp.Status != null ? ccp.Status.Id : (int?)null,
                Action = actions,
                Comment = comment,
                Timestamp = ccp.LastModified != null ? ccp.LastModified : DateTime.Now,
                RepositoryItemId = ccp.RepositoryItemId,
                PersonId = person.Id,
                PersonFirstName = person.FirstName,
                PersonLastName = person.LastName
            };
            eBookReadManager.Context.AddToClientCodaPoaLogSet(ccpl);
            eBookReadManager.Context.SaveChanges();
        }

        /// <summary>
        /// Queries the coda poa log table using a bankAccount ID
        /// </summary>
        /// <param name="ccpdc"></param>
        /// <returns>Iqueryable</returns>
        public IQueryable<ClientCodaPoaLog> QueryCodaPoaLog(CriteriaCodaPoaLogDataContract ccpldc)
        {
            var poaLogs = eBookReadManager.Context.ClientCodaPoaLogSet
                            .AsQueryable()
                            .Where(p => p.BankAccountNr.Equals(ccpldc.BankAccount));

            return poaLogs;
        }


        /// <summary>
        /// Gets all clients their CODA Powers of Attorney along with status, bank and client information based on criteria given
        /// </summary>
        /// <param name="cccpdc"></param>
        /// <returns>List of CODA POA's</returns>
        public List<CodaPoaLogDataContract> GetCodaPoaLog(CriteriaCodaPoaLogDataContract ccpldc)
        {
            var poaLogs = QueryCodaPoaLog(ccpldc);

            List<CodaPoaLogDataContract> poaLogList = poaLogs.Select(p => new CodaPoaLogDataContract
            {
                BankAccount = p.BankAccountNr,
                Action = p.Action,
                Comment = p.Comment,
                Status = eBookReadManager.Context.ClientCodaPoaStatusesSet.FirstOrDefault(c => c.Id == p.StatusId).Status,
                Timestamp = p.Timestamp,
                DocumentUploaded = p.RepositoryItemId.HasValue,
                PersonName = p.PersonFirstName + " " + p.PersonLastName,
            }).ToList();

            return poaLogList.OrderByDescending(p => p.Timestamp).ToList();
        }

        protected override void RegisterTranslators()
        {
            
        }
    }
}
