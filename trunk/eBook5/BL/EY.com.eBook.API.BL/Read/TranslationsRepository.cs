﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EY.com.eBook.Core;
using EY.com.eBook.Core.EF;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.API.BL.Translators;
using System.ServiceModel;
using EY.com.eBook.Core.Data;
using EY.com.eBook.Core.EF.Read;

namespace EY.com.eBook.API.BL.Read
{

    public class TranslationsRepository : RepositoryBase
    {
        
        public List<WorksheetTypeTranslationDataContract> GetWorksheetTypeTranslations(Guid typeId, string culture)
        {
            return new List<WorksheetTypeTranslationDataContract>();
        }

        public List<WorksheetTypeTranslationDataContract> GetAllWorksheetTypeTranslationsFromDb()
        {
            var list = from t in eBookReadManager.Context.WorksheetTypeTranslations
                       select new WorksheetTypeTranslationDataContract
                       {
                           Culture = t.Culture
                           ,
                           EntityOrCollection = t.EntityOrCollection
                           ,
                           FieldName = t.FieldName
                           ,
                           OutputType = t.OutputType
                           ,
                           Translation = t.Translation
                           , WorksheetTypeId = t.WorksheetTypeId
                       };
            return list.ToList();
        }

        public List<MessageTranslationDataContract> GetAllMessageTranslationsFromDb()
        {
            var list = from t in eBookReadManager.Context.MessageTranslationSet
                       select new MessageTranslationDataContract
                       {
                           Id = t.Id
                           ,Culture = t.Culture
                           , Message = t.Description
                       };
            return list.ToList();
        }

        public List<WorksheetTypeMessageTranslationDataContract> GetAllWorksheetTypeMessageTranslationsFromDb()
        {
            var msgs = from m in eBookReadManager.Context.WorksheetTypeMessageTranslations.ToList()
                       select new WorksheetTypeMessageTranslationDataContract
                       {
                           WorksheetTypeId = m.WorksheetTypeId
                           ,
                           MessageId = m.MessageId
                           ,
                           Collection = m.CollectionName
                           ,
                           FieldName = m.FieldName
                           ,
                           Key = m.Key
                           ,
                           Translations = new List<MessageTranslationDataContract>()
                       };
            return msgs.ToList();


        }

        protected override void RegisterTranslators()
        {
            Translator.RegisterEntityTranslator(new FodListTranslator());
            Translator.RegisterEntityTranslator(new LegalTypeTranslator());
            Translator.RegisterEntityTranslator(new ListItemTranslator());
            Translator.RegisterEntityTranslator(new ListItemTranslator());
            Translator.RegisterEntityTranslator(new CoefficientTranslator());
        }

        
    }
}
