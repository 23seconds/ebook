﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Data.Linq;
using System.Linq.Dynamic;
using System.Text;
using EY.com.eBook.Core;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.Core.EF;
using EY.com.eBook.API.Contracts.Data.Logic;
using EY.com.eBook.Core.EF.Read;
using System.Data.Objects;
using EY.com.eBook.Core.EntityTranslation;
using EY.com.eBook.API.BL.Translators;
using System.ServiceModel;
using EY.com.eBook.Core.Data;
using EY.com.eBook.API.Contracts.Proxies;
using EY.com.eBook.API.Contracts.Data.Cache;

namespace EY.com.eBook.API.BL.Read
{
    

    public class GlobalRepository : RepositoryBase
    {
        
        public List<WorksheetTypeDataContract> GetWorksheetTypes(CriteriaWorksheetTypesDataContract cwtdc)
        {
            List<WorksheetType> worksheetTypes;
            if (string.IsNullOrEmpty(cwtdc.Type))
            {
                var wtQuery = from wt in eBookReadManager.Context.WorksheetTypes
                                                                       .Include("Names")
                              where wt.IsActive
                              orderby wt.Type, wt.Order
                              select wt;
                worksheetTypes = wtQuery.ToList();
            }
            else
            {
                worksheetTypes = eBookReadManager.Context.WorksheetTypes
                                        .Include("Names")
                                        .Where(a => a.IsActive && a.Type == cwtdc.Type)
                                        .OrderBy(a => a.Order)
                                        .ToList();
            }
            return (worksheetTypes.Count == 0) ? new List<WorksheetTypeDataContract>() : Translator.TranslateList<WorksheetTypeDataContract, WorksheetType>(worksheetTypes, TranslateLevel.NoChildren, cwtdc.Culture);
        }

        public List<WorksheetHelpDataContract> GetWorksheetHelpByID(CriteriaIdAndCultureDataContract cicdc)
        {
            var helpBlocks = eBookReadManager.Context.WorksheetHelpNewSet.Where(w => w.WorksheetTypeID == cicdc.Id && w.Culture == cicdc.Culture).OrderBy(wo=>wo.Order);
            List<WorksheetHelpDataContract> lwhdc = new List<WorksheetHelpDataContract>();
            foreach (var hb in helpBlocks) {
                WorksheetHelpDataContract whdc = new WorksheetHelpDataContract { 
                    Id = hb.WorksheetHelpID,
                    WorksheetTypeId = hb.WorksheetTypeID,
                    Culture = hb.Culture,
                    Subject = hb.Subject,
                    Body = hb.Body,
                    Order = (int)hb.Order
                };
                lwhdc.Add(whdc);
            }
            //lwhdc.OrderBy(w => w.Order);
            return lwhdc;
        }

        protected override void RegisterTranslators()
        {
            Translator.RegisterEntityTranslator(new WorksheetTypeTranslator());
            

            
        }

     
    }
}
