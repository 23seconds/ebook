﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EY.com.eBook.Core;
using EY.com.eBook.Core.EF;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.API.BL.Translators;
using System.ServiceModel;
using EY.com.eBook.Core.Data;
using EY.com.eBook.Core.EF.Read;
using System.Data.Objects;

namespace EY.com.eBook.API.BL.Read
{

    public class MappingRepository : RepositoryBase
    {

        public List<StringListItemDataContract> GetMappingGroupItems(CriteriaMappingDataContract cmdc)
        {
            IQueryable<MappingItem> mappings = eBookReadManager.Context.MappingItems
                                        .Include("Mapping")
                                        .Include("Translations")
                                        .Where(m=>m.Mapping.Key==cmdc.Key);

            if (!string.IsNullOrEmpty(cmdc.Meta))
            {
                mappings = mappings.Where(m => m.Meta == cmdc.Meta);
            }

            List<StringListItemDataContract> slis = (from m in mappings.ToList()
                                                     select new StringListItemDataContract
                                                     {
                                                         Id = m.Id.ToString()
                                                         ,
                                                         NL = m.Translations.First(l => l.Culture == "nl-BE").Description
                                                         ,
                                                         FR = m.Translations.First(l => l.Culture == "fr-FR").Description
                                                         ,
                                                         EN = m.Translations.First(l => l.Culture == "en-US").Description
                                                     }).ToList();
            switch (cmdc.Culture)
            {
                case "nl-BE":
                    slis = slis.OrderBy(m => m.NL).ToList();
                    break;
                case "fr-FR":
                    slis = slis.OrderBy(m => m.FR).ToList();
                    break;
                case "en-US":
                    slis = slis.OrderBy(m => m.EN).ToList();
                    break;
            }

            return slis;
        
        }

        public MappingItemDataContract GetMappingItem(Guid id)
        {
            MappingItem mi = eBookReadManager.Context.MappingItems
                                        .Include("Translations")
                                        .Include("Mapping")
                                        .FirstOrDefault(m => m.Id == id);
            return new MappingItemDataContract
                                 {
                                     Id = mi.Id
                                     ,
                                     Description = mi.Translations.First().Description
                                     ,
                                     From = mi.From
                                     ,
                                     To = mi.To
                                     ,
                                     Key = mi.Mapping.Key
                                     ,
                                     Meta = mi.Meta
                                     ,
                                     ListObject = new GuidListItemDataContract
                                     {
                                         Id = mi.Id
                                         ,
                                         NL = mi.Translations.First(t => t.Culture == "nl-BE").Description
                                         ,
                                         EN = mi.Translations.First(t => t.Culture == "en-US").Description
                                         ,
                                         FR = mi.Translations.First(t => t.Culture == "fr-FR").Description
                                     }
                                 };
        }

        // all mappings to display, including menu (selection) tree
        public List<MappingDataContract> GetMappings(string culture)
        {
            List<Mapping> mappings = eBookReadManager.Context.Mappings
                                        .Include("Translations")
                                        .Include("MappingItems")
                                        .Include("MappingItems.Translations")
                                        .OrderBy(m => m.Order)
                                        .ToList();
            List<MappingMeta> metas = eBookReadManager.Context.MappingMetaSet.ToList();


            return (from m in mappings
                      select new MappingDataContract
                      {
                          Key = m.Key
                          ,
                          Description =m.Key  // m.Translations.First(t => t.Culture == culture).Description
                          ,
                          Order = m.Order
                          ,
                          Items = GetMappingList(metas, m.MappingItems.ToList(), m.Key,culture)
                      }).ToList();

        }


        public List<MappingListItemDataContract> GetMappingList(List<MappingMeta> metas, List<MappingItem> items, string key,string culture)
        {
            List<MappingListItemDataContract> mlidc = new List<MappingListItemDataContract>();
            foreach (string meta in items.Where(m => !string.IsNullOrEmpty(m.Meta)).Select(m=>m.Meta).Distinct())
            {
                MappingMeta mm = metas.FirstOrDefault(m=>m.Meta==meta && m.Culture==culture);
                if (mm != null)
                {
                    if (items.Count(i => i.Meta == meta) == 1)
                    {
                        MappingItem midc = items.First(i => i.Meta == meta);
                        mlidc.Add(new MappingItemDataContract
                        {
                            Id = midc.Id
                            ,
                            Description = midc.Translations.First(t => t.Culture == culture).Description
                            ,
                            From = midc.From
                            ,
                            Key = key
                            ,
                            Meta = midc.Meta
                            ,
                            To = midc.To
                            , ListObject = new GuidListItemDataContract
                                         {
                                             Id = midc.Id
                                             ,
                                             NL = midc.Translations.First(t => t.Culture == "nl-BE").Description
                                             ,
                                             EN = midc.Translations.First(t => t.Culture == "en-US").Description
                                             ,
                                             FR = midc.Translations.First(t => t.Culture == "fr-FR").Description
                                         }
                        });
                    }
                    else
                    {
                        mlidc.Add(new MappingMetaDataContract
                        {
                            Meta = mm.Meta
                            ,
                            Description = mm.Description
                            ,
                            Items = (from mi in items.Where(i => i.Meta == meta)
                                     select new MappingItemDataContract
                                     {
                                         Id = mi.Id
                                         ,
                                         Description = mi.Translations.First(t => t.Culture == culture).Description
                                         ,
                                         From = mi.From
                                         ,
                                         To = mi.To
                                         ,
                                         Key = key
                                         ,
                                         Meta = meta
                                         ,
                                         ListObject = new GuidListItemDataContract
                                         {
                                             Id = mi.Id
                                             ,
                                             NL = mi.Translations.First(t => t.Culture == "nl-BE").Description
                                             ,
                                             EN = mi.Translations.First(t => t.Culture == "en-US").Description
                                             ,
                                             FR = mi.Translations.First(t => t.Culture == "fr-FR").Description
                                         }
                                     }).OrderBy(o => o.Description).ToList()

                        });
                    }
                }
            }

            // remaining meta-less items
            foreach (MappingItem mi in items.Where(m => string.IsNullOrEmpty(m.Meta)))
            {
                mlidc.Add(new MappingItemDataContract
                {
                    Id = mi.Id
                    ,
                    Description = mi.Translations.First(t => t.Culture == culture).Description
                    ,
                    From = mi.From
                    ,
                    Key = key
                    ,
                    Meta = mi.Meta
                    ,
                    To = mi.To
                    ,
                    ListObject = new GuidListItemDataContract
                    {
                        Id = mi.Id
                        ,
                        NL = mi.Translations.First(t => t.Culture == "nl-BE").Description
                        ,
                        EN = mi.Translations.First(t => t.Culture == "en-US").Description
                        ,
                        FR = mi.Translations.First(t => t.Culture == "fr-FR").Description
                    }
                });
            }
            return mlidc.OrderBy(o => o.Description).ToList();
        }

        public List<MappingItemDataContract> GetAllMappingItems()
        {
            List<MappingItem> mappings = eBookReadManager.Context.MappingItems
                                       .Include("Translations")
                                       .Include("Mapping")
                                       .ToList();
            return (from m in mappings
                    select new MappingItemDataContract
                    {
                        Id = m.Id
                        ,
                        Description = ""
                        ,
                        From = m.From
                        ,
                        Key = m.Mapping.Key
                        ,
                        Meta = m.Meta
                        ,
                        To = m.To
                        ,
                        ListObject = new GuidListItemDataContract
                        {
                            Id = m.Id
                            ,
                            NL = m.Translations.First(t => t.Culture == "nl-BE").Description
                            ,
                            EN = m.Translations.First(t => t.Culture == "en-US").Description
                            ,
                            FR = m.Translations.First(t => t.Culture == "fr-FR").Description
                        }
                    }).ToList();

        }

        protected override void RegisterTranslators()
        {
            
        }
    }
}
