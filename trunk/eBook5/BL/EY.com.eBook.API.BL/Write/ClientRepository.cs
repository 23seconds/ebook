﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using EY.com.eBook.Core;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.Core.EF;
using EY.com.eBook.API.Contracts.Data.Logic;
using System.Data.Objects;
using EY.com.eBook.Core.EntityTranslation;
using EY.com.eBook.API.BL.Translators;
using System.ServiceModel;
using EY.com.eBook.Core.Data;
using EY.com.eBook.API.Contracts.Proxies;
using EY.com.eBook.API.Contracts.Data.Cache;
using EY.com.eBook.Core.EF.Write;
using System.Data;
using System.Transactions;

namespace EY.com.eBook.API.BL.Write
{
   
    public class ClientRepository : RepositoryBase
    {

        public bool AddBTW(BTWDataContract bdc)
        {
            Client client = eBookWriteManager.Context.ClientSet.FirstOrDefault(c => c.GFISCode == bdc.GfisCode);
            if (client != null)
            {
                
                BTWData btwd = new BTWData
                {
                    ClientId = client.Id
                    , Data = bdc.Data.ToString()
                    , Date = bdc.Date
                };

                if (btwd.EntityState == EntityState.Detached)
                    eBookWriteManager.Context.AddToBTWDataSet(btwd);
                eBookWriteManager.Context.SaveChanges();
                return true;
            }
            return false;
        }

        public bool SetPrintPreference(CriteriaIdAndBoolDataContract ciabdc)
        {
            Client client = eBookWriteManager.Context.ClientSet.FirstOrDefault(c => c.Id == ciabdc.Id);
            if (client != null)
            {
                client.PrintPreference = ciabdc.Boolean;
                eBookWriteManager.Context.SaveChanges();
                return true;
            }
            return false;
        }

        public bool SaveClientOverrule(ClientOverruleDataContract cdc)
        {
            if (cdc == null)
                throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { Message = "Criteria cannot be null!" });

            ClientOverrule co = eBookWriteManager.Context.ClientOverruleSet.FirstOrDefault(c => c.Id == cdc.Id);
            if (co != null)
            {

               // co.Active = cdc.IsActive.HasValue && cdc.IsActive.Value ? "Y" : "N";
                co.Address = cdc.Address;
                co.City = cdc.City;
                co.CLIENT_LEGAL_TYPE_ID = cdc.LegalTypeId;
                co.Country = cdc.Country;
                co.Director = cdc.DirectorName;
                co.Email = cdc.Email;
                co.EnterpriseNumber = cdc.EnterpriseNumber;
                co.Fax = cdc.Fax;
                //  co.FodAddress = cdc.FodAddress != null ? CoreHelper.SerializeToString(cdc.FodAddress) : null;
                co.LegalStructure = cdc.LegalStructure;
                co.Name = cdc.Name;
                co.Rpr = cdc.Rpr;
                //co.SubjectToVAT = cdc.IsVat.HasValue && cdc.IsVat.Value ? "Y" : "N";
                co.Tel = cdc.Phone;
                co.VatNumber = cdc.Vat;
                co.ZipCode = cdc.ZipCode;
                eBookWriteManager.Context.SaveChanges();
            }
            else
            {
                co = new ClientOverrule
                {
                    Id = cdc.Id
                    ,
                    Active = cdc.IsActive.HasValue && cdc.IsActive.Value ? "Y" : "N"
                    ,
                    Address = cdc.Address
                    ,
                    City = cdc.City
                    ,
                    CLIENT_LEGAL_TYPE_ID = cdc.LegalTypeId
                    ,
                    Country = cdc.Country
                    ,
                    Director = cdc.DirectorName
                    ,
                    Email = cdc.Email
                    ,
                    EnterpriseNumber = cdc.EnterpriseNumber
                    ,
                    Fax = cdc.Fax
                        // ,
                        //  FodAddress = cdc.FodAddress != null ? CoreHelper.SerializeToString(cdc.FodAddress) : null
                    ,
                    LegalStructure = cdc.LegalStructure
                    ,
                    Name = cdc.Name
                    ,
                    Rpr = cdc.Rpr
                    ,
                    SubjectToVAT = cdc.IsVat.HasValue && cdc.IsVat.Value ? "Y" : "N"
                    ,
                    Tel = cdc.Phone
                    ,
                    VatNumber = cdc.Vat
                    ,
                    ZipCode = cdc.ZipCode
                };
                if (co.EntityState == EntityState.Detached)
                    eBookWriteManager.Context.AddToClientOverruleSet(co);
                eBookWriteManager.Context.SaveChanges();

            }
            
            return true;
        }

        

        

        protected override void RegisterTranslators()
        {
            
        }
    }
}
