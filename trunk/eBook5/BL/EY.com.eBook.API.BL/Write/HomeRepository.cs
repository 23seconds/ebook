﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using EY.com.eBook.Core;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.Core.EF;
using EY.com.eBook.API.Contracts.Data.Logic;
using System.Data.Objects;
using EY.com.eBook.Core.EntityTranslation;
using EY.com.eBook.API.BL.Translators;
using System.ServiceModel;
using EY.com.eBook.Core.Data;
using EY.com.eBook.API.Contracts.Proxies;
using EY.com.eBook.API.Contracts.Data.Cache;
using EY.com.eBook.Core.EF.Write;
using System.Data;
using System.Transactions;

namespace EY.com.eBook.API.BL.Write
{
   
    public class HomeRepository : RepositoryBase
    {


        /// <summary>
        /// Set Last Used role of user.
        /// </summary>
        /// <param name="cprdc"></param>
        /// <returns></returns>
        public void SetLastUsedRole(CriteriaPersonRolesDataContract cprdc)
        {
            var activeRole = eBookWriteManager.Context.PersonRoleSet.FirstOrDefault(r => r.PersonId == cprdc.PersonId && r.Role == cprdc.Role);
            var previousActiveRole = eBookWriteManager.Context.PersonRoleSet.FirstOrDefault(r => r.PersonId == cprdc.PersonId && r.LastUsed == true);

            if (activeRole == null)
                throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { Message = String.Format("Person and role not found for  {0}/{1}", cprdc.PersonId, cprdc.Role) });

            if (previousActiveRole != null)
            {
                previousActiveRole.LastUsed = false;
            }
            activeRole.LastUsed = true;

            eBookWriteManager.Context.SaveChanges();
        }

        protected override void RegisterTranslators()
        {
            
        }
    }
}
