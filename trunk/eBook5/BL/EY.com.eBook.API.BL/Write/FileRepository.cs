﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using EY.com.eBook.Core;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.Core.EF;
using EY.com.eBook.API.Contracts.Data.Logic;
using System.Data.Objects;
using EY.com.eBook.Core.EntityTranslation;
using EY.com.eBook.API.BL.Translators;
using System.ServiceModel;
using EY.com.eBook.Core.Data;
using EY.com.eBook.API.Contracts.Proxies;
using EY.com.eBook.API.Contracts.Data.Cache;
using EY.com.eBook.Core.EF.Write;
using System.Data;
using System.Transactions;
using EY.com.eBook.API.Contracts.Data.Meta;
using EY.com.eBook.ProAcc.API.Contracts.Data;
using System.Xml.Linq;
using EY.com.eBook.Excel.BL;
using EY.com.eBook.API.Contracts.Services;


namespace EY.com.eBook.API.BL.Write
{
   
    public class FileRepository : eBookRepository
    {
        //Services
        private Guid fsYearEnd = new Guid("C3B8C8DB-3822-4263-9098-541FAE897D02");
        private Guid fsAnnualAccounts = new Guid("1ABF0836-7D7B-48E7-9006-F03DB97AC28B");

        public Guid CreateFile(CriteriaCreateFileDataContract ccfdc)
        {
            if (ccfdc == null)
                throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { Message = "Criteria cannot be null!" });

            ActivePersonDataContract activePerson = new BusinessHelper().DecodePerson(ccfdc.EncodedActivePerson);

            Person person = eBookWriteManager.Context.PersonSet.Where(p => p.Id == activePerson.Id).FirstOrDefault();

            int assessment = ccfdc.EndDate.AddDays(1).Year;
            Guid fileId = Guid.NewGuid();

            ccfdc.Settings.FileId = fileId;

            File file = new File
            {
                Id = fileId
                ,
                StartDate = ccfdc.StartDate
                ,
                EndDate = ccfdc.EndDate
                ,
                Name = string.Format("{0} {1} - {2}",assessment,ccfdc.StartDate.ToShortDateString(),ccfdc.EndDate.ToShortDateString())
                ,
                PrevStartDate = ccfdc.PreviousStartDate
                ,
                PrevEndDate = ccfdc.PreviousEndDate
                ,
                PreviousFileId = ccfdc.PreviousFileId
                ,
                Culture = ccfdc.Culture
                ,
                ClientId = ccfdc.ClientId
                ,
                AccountLength = 6
                ,
                CreationDate = DateTime.Now
                ,
                CreatedBy = person.Id.ToString()
                , Settings = CoreHelper.SerializeToString(ccfdc.Settings)
                , ImportType = ccfdc.ImportType

            };

            if (file.EntityState == EntityState.Detached)
                eBookWriteManager.Context.AddToFiles(file);

            eBookWriteManager.Context.SaveChanges();


            foreach (Guid serviceId in ccfdc.ActiveServices)
            {
                FileService fs = new FileService
                {
                    FileId = file.Id
                    ,
                    ServiceId = serviceId
                    ,
                    Completed = null
                    ,
                    Status = 1
                };
                if (fs.EntityState == EntityState.Detached)
                    eBookWriteManager.Context.AddToFileServices(fs);

                
            }
            eBookWriteManager.Context.SaveChanges();
            return file.Id;
        }


        public bool ImportAccountseBook(ImportAccountseBookDataContract iaedc)
        {
            return eBookWriteManager.Context.ImportPreviousAccounts(iaedc.FileId, iaedc.IncludePreviousSaldi);
        }

        public bool ImportMappingseBook(CriteriaFileDataContract cfdc)
        {
            return eBookWriteManager.Context.ImportPreviousMappings(cfdc.FileId);
        }

        public DateTime? ImportAccountsProAcc(ImportAccountseBookDataContract iaedc)
        {
            Guid fileId = iaedc.FileId;
            bool includePreviousSaldi = iaedc.IncludePreviousSaldi;
            File file = eBookWriteManager.Context.Files.First(f => f.Id == fileId);
            Client client = eBookWriteManager.Context.ClientSet.First(c => c.Id == file.ClientId);

            if (string.IsNullOrEmpty(client.ProAccDatabase) || string.IsNullOrEmpty(client.ProAccServer))
                return file.ImportDate; // throw exception instead.

            List<Account> accounts = eBookWriteManager.Context.Accounts.Where(a => a.FileId == fileId).ToList();
            List<AccountDescription> accountdescs = eBookWriteManager.Context.AccountDescriptions.Where(a => a.FileId == fileId).ToList();

            List<string> nrs = accounts.Select(a=>a.InternalNr).ToList();

            ProAccServiceProxy psp = new ProAccServiceProxy();
            psp.Open();
            List<ProAccAccountDataContract> pacs = psp.ProAccGetAccounts(new CriteriaProAccStartAndEndDatesDataContract
            {
                Server = client.ProAccServer
                ,
                Database = client.ProAccDatabase
                ,
                End = file.EndDate
                ,
                Start = file.StartDate
                ,
                StartPrevious = includePreviousSaldi ? file.PrevStartDate : null
                ,
                EndPrevious = includePreviousSaldi ? file.PrevEndDate : null
            });
            psp.Close();

            List<string> pnrs = pacs.Select(a=>a.InternalNr).ToList();

            // update existing
            foreach (Account account in accounts.Where(a => pnrs.Contains(a.InternalNr)))
            {
                ProAccAccountDataContract pac = pacs.First(a=>a.InternalNr==account.InternalNr);
                account.ImportedID = pac.VisualNr;
                account.VisualNr= pac.VisualNr;
                account.StartSaldo = pac.CurrentSaldo;
                account.LastChanged = DateTime.Now;
                if (includePreviousSaldi) account.PreviousSaldo = pac.PreviousSaldo;
                
                eBookWriteManager.Context.SaveChanges();

                foreach (AccountTranslationDataContract atdc in pac.Translations)
                {
                    AccountDescription ad = accountdescs.FirstOrDefault(d => d.InternalNr == account.InternalNr && d.Culture == atdc.Culture);
                    if (ad==null)
                    {
                        AccountDescription ads = new AccountDescription
                        {
                            Culture = atdc.Culture
                            ,
                            Description = atdc.Description
                            ,
                            FileId = fileId
                            ,
                            InternalNr = account.InternalNr
                        };
                        if (ads.EntityState == EntityState.Detached)
                            eBookWriteManager.Context.AddToAccountDescriptions(ads);

                        eBookWriteManager.Context.SaveChanges();
                    }
                    else if(!ad.HasChanged)
                    {
                        ad.Description = atdc.Description;
                        eBookWriteManager.Context.SaveChanges();
                    }
                }
            }

            // Add new items
            foreach (ProAccAccountDataContract pac in pacs.Where(p => !nrs.Contains(p.InternalNr)))
            {
                Account account = new Account
                {
                    FileId = fileId
                    ,
                    InternalNr = pac.InternalNr
                    ,
                    VisualNr = pac.VisualNr
                    ,
                    ImportedID = pac.VisualNr
                    ,
                    ExistedLastYear = false
                    ,
                    HasTaxAdjustments = false
                    ,
                    IsTaxAdjustment = false
                    ,
                    StartSaldo = pac.CurrentSaldo
                    ,
                    PreviousSaldo = includePreviousSaldi ? pac.PreviousSaldo : 0
                    ,
                    Saldo = pac.CurrentSaldo
                    , LastChanged = DateTime.Now
                };
                if (account.EntityState == EntityState.Detached)
                    eBookWriteManager.Context.AddToAccounts(account);
                eBookWriteManager.Context.SaveChanges();

                foreach (AccountTranslationDataContract atdc in pac.Translations)
                {
                    double num;
                    bool isNum = double.TryParse(atdc.Description, out num);
                    if (!isNum)
                    {
                        AccountDescription ads = new AccountDescription
                        {
                            Culture = atdc.Culture
                            ,
                            Description = atdc.Description
                            ,
                            FileId = fileId
                            ,
                            InternalNr = account.InternalNr
                        };
                        if (ads.EntityState == EntityState.Detached)
                            eBookWriteManager.Context.AddToAccountDescriptions(ads);

                        eBookWriteManager.Context.SaveChanges();
                    }
                }

            }

            // update saldi in database?
            eBookWriteManager.Context.UpdateFileAllAccountsSaldi(fileId);
            File fle = eBookWriteManager.Context.Files.First(f => f.Id == iaedc.FileId);
            fle.ImportDate = DateTime.Now;
            eBookWriteManager.Context.SaveChanges();

            SetFileInCache(fle.Id, true);

            return fle.ImportDate;
        }

        public DateTime? ImportAccountsExcel(CriteriaExcelSheetMappedDataContract cesmdc)
        {
            if (cesmdc.FileId == Guid.Empty) throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { Message = "File id not found" });
            Guid fileId = cesmdc.FileId;
            var er = new ExcelRepository();
            string path = System.IO.Path.Combine(Config.UploadFolder,cesmdc.FileName);
            List<ExcelAccountImportDataContract> impAccounts = er.GetAccountSchema(path, cesmdc.SheetName, cesmdc.Mapping, false);

            List<Account> accounts = eBookWriteManager.Context.Accounts.Where(a => a.FileId == fileId).ToList();
            List<AccountDescription> accountdescs = eBookWriteManager.Context.AccountDescriptions.Where(a => a.FileId == fileId).ToList();

            List<string> nrs = accounts.Select(a => a.InternalNr).ToList();
            List<string> pnrs = impAccounts.Select(a => a.InternalNr).ToList();

            // update existing
            foreach (Account account in accounts.Where(a => pnrs.Contains(a.InternalNr)))
            {
                ExcelAccountImportDataContract pac = impAccounts.First(a => a.InternalNr == account.InternalNr);
                account.ImportedID = pac.AccountNr;
                account.VisualNr = pac.AccountNr;
                account.LastChanged = DateTime.Now;

                eBookWriteManager.Context.SaveChanges();


                    AccountDescription ad = accountdescs.FirstOrDefault(d => d.InternalNr == account.InternalNr && d.Culture == cesmdc.Culture);
                    if (ad == null)
                    {
                        AccountDescription ads = new AccountDescription
                        {
                            Culture = cesmdc.Culture
                            ,
                            Description = pac.AccountDescription
                            ,
                            FileId = fileId
                            ,
                            InternalNr = account.InternalNr
                        };
                        if (ads.EntityState == EntityState.Detached)
                            eBookWriteManager.Context.AddToAccountDescriptions(ads);

                        eBookWriteManager.Context.SaveChanges();
                    }
                    else if (!ad.HasChanged)
                    {
                        ad.Description = pac.AccountDescription;
                        eBookWriteManager.Context.SaveChanges();
                    }
                
            }

            // Add new items
            foreach (ExcelAccountImportDataContract pac in impAccounts.Where(p => !nrs.Contains(p.InternalNr)))
            {
                Account account = new Account
                {
                    FileId = fileId
                    ,
                    InternalNr = pac.InternalNr
                    ,
                    VisualNr = pac.AccountNr
                    ,
                    ImportedID = pac.AccountNr
                    ,
                    ExistedLastYear = false
                    ,
                    HasTaxAdjustments = false
                    ,
                    IsTaxAdjustment = false
                    ,
                    StartSaldo = 0
                    ,
                    PreviousSaldo = 0
                    ,
                    Saldo = 0
                    ,
                    LastChanged = DateTime.Now
                };
                if (account.EntityState == EntityState.Detached)
                    eBookWriteManager.Context.AddToAccounts(account);
                eBookWriteManager.Context.SaveChanges();

                AccountDescription ads = new AccountDescription
                {
                    Culture = cesmdc.Culture
                    ,
                    Description = pac.AccountDescription
                    ,
                    FileId = fileId
                    ,
                    InternalNr = account.InternalNr
                };
                if (ads.EntityState == EntityState.Detached)
                    eBookWriteManager.Context.AddToAccountDescriptions(ads);

                eBookWriteManager.Context.SaveChanges();

            }

            // update saldi in database?
            eBookWriteManager.Context.UpdateFileAllAccountsSaldi(fileId);
            File fle = eBookWriteManager.Context.Files.First(f => f.Id == fileId);
            fle.ImportDate = DateTime.Now;
            eBookWriteManager.Context.SaveChanges();
            SetFileInCache(fle.Id, true);
            return fle.ImportDate;
        }

        public DateTime? ImportAccountsTxt(CriteriaExcelSheetMappedDataContract cesmdc)
        {
            if (cesmdc.FileId == Guid.Empty) throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { Message = "File id not found" });
            Guid fileId = cesmdc.FileId;
            //List<ExcelBalansImportDataContract> impAccounts = er.GetBalans(path, cesmdc.SheetName, cesmdc.Mapping, false);
            string line;

            // Read the file and display it line by line.
            string path = System.IO.Path.Combine(Config.UploadFolder, cesmdc.FileName);
            List<string> sepList = new List<string>();
            List<ExactBalansImportDataContract> impAccounts = new List<ExactBalansImportDataContract>();
            using (System.IO.StreamReader fe = new System.IO.StreamReader(@path))
            {
                while ((line = fe.ReadLine()) != null)
                {

                    char[] delimiters = new char[] { '\t' };
                    string[] parts = line.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                    decimal result1;
                    decimal result2;
                    decimal.TryParse(parts[2], out result1);
                    decimal.TryParse(parts[3], out result2);
                    ExactBalansImportDataContract ebidc = new ExactBalansImportDataContract
                    {
                        AccountNr = parts[0],
                        AccountDescription = parts[1],
                        Saldo = result1 - result2
                    };
                    impAccounts.Add(ebidc);
                }

                fe.Close();
            }
            List<Account> accounts = eBookWriteManager.Context.Accounts.Where(a => a.FileId == fileId).ToList();
            List<AccountDescription> accountdescs = eBookWriteManager.Context.AccountDescriptions.Where(a => a.FileId == fileId).ToList();

            List<string> nrs = accounts.Select(a => a.InternalNr).ToList();
            List<string> pnrs = impAccounts.Select(a => a.InternalNr).ToList();

            // update existing
            foreach (Account account in accounts.Where(a => pnrs.Contains(a.InternalNr)))
            {
                ExactBalansImportDataContract pac = impAccounts.First(a => a.InternalNr == account.InternalNr);
                account.ImportedID = pac.AccountNr;
                account.VisualNr = pac.AccountNr;
                account.LastChanged = DateTime.Now;

                eBookWriteManager.Context.SaveChanges();


                AccountDescription ad = accountdescs.FirstOrDefault(d => d.InternalNr == account.InternalNr && d.Culture == cesmdc.Culture);
                if (ad == null)
                {
                    AccountDescription ads = new AccountDescription
                    {
                        Culture = cesmdc.Culture
                        ,
                        Description = pac.AccountDescription
                        ,
                        FileId = fileId
                        ,
                        InternalNr = account.InternalNr
                    };
                    if (ads.EntityState == EntityState.Detached)
                        eBookWriteManager.Context.AddToAccountDescriptions(ads);

                    eBookWriteManager.Context.SaveChanges();
                }
                else if (!ad.HasChanged)
                {
                    ad.Description = pac.AccountDescription;
                    eBookWriteManager.Context.SaveChanges();
                }

            }

            // Add new items
            foreach (ExactBalansImportDataContract pac in impAccounts.Where(p => !nrs.Contains(p.InternalNr)))
            {
                Account account = new Account
                {
                    FileId = fileId
                    ,
                    InternalNr = pac.InternalNr
                    ,
                    VisualNr = pac.AccountNr
                    ,
                    ImportedID = pac.AccountNr
                    ,
                    ExistedLastYear = false
                    ,
                    HasTaxAdjustments = false
                    ,
                    IsTaxAdjustment = false
                    ,
                    StartSaldo = 0
                    ,
                    PreviousSaldo = 0
                    ,
                    Saldo = 0
                    ,
                    LastChanged = DateTime.Now
                };
                if (account.EntityState == EntityState.Detached)
                    eBookWriteManager.Context.AddToAccounts(account);
                eBookWriteManager.Context.SaveChanges();

                AccountDescription ads = new AccountDescription
                {
                    Culture = cesmdc.Culture
                    ,
                    Description = pac.AccountDescription
                    ,
                    FileId = fileId
                    ,
                    InternalNr = account.InternalNr
                };
                if (ads.EntityState == EntityState.Detached)
                    eBookWriteManager.Context.AddToAccountDescriptions(ads);

                eBookWriteManager.Context.SaveChanges();

            }

            // update saldi in database?
            eBookWriteManager.Context.UpdateFileAllAccountsSaldi(fileId);
            File fle = eBookWriteManager.Context.Files.First(f => f.Id == fileId);
            fle.ImportDate = DateTime.Now;
            eBookWriteManager.Context.SaveChanges();
            SetFileInCache(fle.Id, true);
            return fle.ImportDate;
        }

        public DateTime? ImportAccountsBalansTxt(CriteriaExcelSheetMappedDataContract cesmdc, bool previous)
        {      
            if (cesmdc.FileId == Guid.Empty) throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { Message = "File id not found" });
            Guid fileId = cesmdc.FileId;
            
            //List<ExcelBalansImportDataContract> impAccounts = er.GetBalans(path, cesmdc.SheetName, cesmdc.Mapping, false);
            string line;

            // Read the file and display it line by line.
            string path = System.IO.Path.Combine(Config.UploadFolder, cesmdc.FileName);
            List<string> sepList = new List<string>();
            List<ExactBalansImportDataContract> impAccounts = new List<ExactBalansImportDataContract>();
            var cult = System.Globalization.CultureInfo.CreateSpecificCulture("nl-BE");
            using (System.IO.StreamReader fe = new System.IO.StreamReader(@path))
            {
                while ((line = fe.ReadLine()) != null)
                {

                    char[] delimiters = new char[] { '\t' };
                    string[] parts = line.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                    decimal result1;
                    decimal result2;
                    decimal.TryParse(parts[2], System.Globalization.NumberStyles.AllowDecimalPoint , cult, out result1);
                    decimal.TryParse(parts[3], System.Globalization.NumberStyles.AllowDecimalPoint , cult, out result2);
                    ExactBalansImportDataContract ebidc = new ExactBalansImportDataContract
                    {
                        AccountNr = parts[0],
                        AccountDescription = parts[1],
                        Saldo = Math.Abs(result1) - Math.Abs(result2)
                    };
                    impAccounts.Add(ebidc);
                }

                fe.Close();
            }

            List<Account> accounts = eBookWriteManager.Context.Accounts.Where(a => a.FileId == fileId).ToList();
            List<AccountDescription> accountdescs = eBookWriteManager.Context.AccountDescriptions.Where(a => a.FileId == fileId).ToList();

            List<string> nrs = accounts.Select(a => a.InternalNr).ToList();
            List<string> pnrs = impAccounts.Select(a => a.InternalNr).ToList();

            // update existing
            foreach (Account account in accounts.Where(a => pnrs.Contains(a.InternalNr)))
            {
                ExactBalansImportDataContract pac = impAccounts.First(a => a.InternalNr == account.InternalNr);
                account.ImportedID = pac.AccountNr;
                account.VisualNr = pac.AccountNr;
                account.LastChanged = DateTime.Now;
                if (previous)
                {
                    account.PreviousSaldo = pac.Saldo;
                }
                else
                {
                    account.StartSaldo = pac.Saldo;
                }
                eBookWriteManager.Context.SaveChanges();
            }

            // Add new items
            foreach (ExactBalansImportDataContract pac in impAccounts.Where(p => !nrs.Contains(p.InternalNr)))
            {
                Account account = new Account
                {
                    FileId = fileId
                    ,
                    InternalNr = pac.InternalNr
                    ,
                    VisualNr = pac.AccountNr
                    ,
                    ImportedID = pac.AccountNr
                    ,
                    ExistedLastYear = false
                    ,
                    HasTaxAdjustments = false
                    ,
                    IsTaxAdjustment = false
                    ,
                    StartSaldo = previous ? 0 : pac.Saldo
                    ,
                    PreviousSaldo = !previous ? 0 : pac.Saldo
                    ,
                    Saldo = previous ? 0 : pac.Saldo
                    ,
                    LastChanged = DateTime.Now
                };
                if (account.EntityState == EntityState.Detached)
                    eBookWriteManager.Context.AddToAccounts(account);
                eBookWriteManager.Context.SaveChanges();

            }

            // update saldi in database?
            eBookWriteManager.Context.UpdateFileAllAccountsSaldi(fileId);
            File fle = eBookWriteManager.Context.Files.First(f => f.Id == fileId);
            fle.ImportDate = DateTime.Now;
            eBookWriteManager.Context.SaveChanges();
            SetFileInCache(fle.Id, true);
            return fle.ImportDate;
        }


        public DateTime? ImportAccountsBalansExcel(CriteriaExcelSheetMappedDataContract cesmdc, bool previous)
        {
            if (cesmdc.FileId == Guid.Empty) throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { Message = "File id not found" });
            Guid fileId = cesmdc.FileId;
            var er = new ExcelRepository();
            string path = System.IO.Path.Combine(Config.UploadFolder, cesmdc.FileName);
            List<ExcelBalansImportDataContract> impAccounts = er.GetBalans(path, cesmdc.SheetName, cesmdc.Mapping, false);

            List<Account> accounts = eBookWriteManager.Context.Accounts.Where(a => a.FileId == fileId).ToList();
            List<AccountDescription> accountdescs = eBookWriteManager.Context.AccountDescriptions.Where(a => a.FileId == fileId).ToList();

            List<string> nrs = accounts.Select(a => a.InternalNr).ToList();
            List<string> pnrs = impAccounts.Select(a => a.InternalNr).ToList();

            // update existing
            foreach (Account account in accounts.Where(a => pnrs.Contains(a.InternalNr)))
            {
                ExcelBalansImportDataContract pac = impAccounts.First(a => a.InternalNr == account.InternalNr);
                account.ImportedID = pac.AccountNr;
                account.VisualNr = pac.AccountNr;
                account.LastChanged = DateTime.Now;
                if (previous)
                {
                    account.PreviousSaldo = pac.Saldo;
                }
                else
                {
                    account.StartSaldo = pac.Saldo;
                }
                eBookWriteManager.Context.SaveChanges();
            }

            // Add new items
            foreach (ExcelBalansImportDataContract pac in impAccounts.Where(p => !nrs.Contains(p.InternalNr)))
            {
                Account account = new Account
                {
                    FileId = fileId
                    ,
                    InternalNr = pac.InternalNr
                    ,
                    VisualNr = pac.AccountNr
                    ,
                    ImportedID = pac.AccountNr
                    ,
                    ExistedLastYear = false
                    ,
                    HasTaxAdjustments = false
                    ,
                    IsTaxAdjustment = false
                    ,
                    StartSaldo = previous ? 0 : pac.Saldo
                    ,
                    PreviousSaldo = !previous ? 0 : pac.Saldo
                    ,
                    Saldo = previous ? 0 : pac.Saldo
                    ,
                    LastChanged = DateTime.Now
                };
                if (account.EntityState == EntityState.Detached)
                    eBookWriteManager.Context.AddToAccounts(account);
                eBookWriteManager.Context.SaveChanges();

            }

            // update saldi in database?
            eBookWriteManager.Context.UpdateFileAllAccountsSaldi(fileId);
            File fle = eBookWriteManager.Context.Files.First(f => f.Id == fileId);
            fle.ImportDate = DateTime.Now;
            eBookWriteManager.Context.SaveChanges();
            SetFileInCache(fle.Id, true);
            return fle.ImportDate;
        }


        public bool SetFileInCache(Guid fileId, bool updateSaldi)
        {
            if (updateSaldi) eBookWriteManager.Context.UpdateFileAllAccountsSaldi(fileId);

            ICacheService proxy;
            if (TryFetchICacheService(out proxy))
            {
                proxy.ReloadOrAddFileInCacheForce(fileId, true);
                return true;
            }
            return false;
        }

        public bool UndoCreateFile(Guid fileId)
        {
            return true;
        }

        public void DeleteFile(Guid id)
        {
            var file = eBookWriteManager.Context.Files.FirstOrDefault(f => f.Id == id);
            if (file == null)
                throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { Message = String.Format("No file found for id {0}", id) });

            file.Deleted = true;
            eBookWriteManager.Context.SaveChanges();
        }

        public void MarkFileForDeletion(Guid id)
        {
            var file = eBookWriteManager.Context.Files.FirstOrDefault(f => f.Id == id);
            if (file == null)
                throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { Message = String.Format("No file found for id {0}", id) });

            file.MarkDelete = true;
            eBookWriteManager.Context.SaveChanges();
        }

        public void UnMarkFileForDeletion(Guid id)
        {
            var file = eBookWriteManager.Context.Files.FirstOrDefault(f => f.Id == id);
            if (file == null)
                throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { Message = String.Format("No file found for id {0}", id) });

            file.MarkDelete = false;
            eBookWriteManager.Context.SaveChanges();
        }


        public void SaveMeta(CriteriaFileMetaDataContract cfmdc)
        {
            if (string.IsNullOrEmpty(cfmdc.EncodedActivePerson)) throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { Message = "User not found. Please make sure you are logged on." });
            if (cfmdc.ActivePerson == null)
            {
                cfmdc.ActivePerson = new BusinessHelper().DecodePerson(cfmdc.EncodedActivePerson);
            }

            FileMetaDataContract metadata = cfmdc.FileMeta;
            if (metadata.Data.ShareHolders != null)
            {
                foreach (ShareHolderDataContract shdc in metadata.Data.ShareHolders)
                {
                    if (shdc.RepresentedBy == null) shdc.RepresentedBy = new MetaPersonDataContract();
                }
            }
            FileMeta fm = eBookWriteManager.Context.FileMetaSet.FirstOrDefault(f => f.FileId == metadata.FileId);
            if (fm == null)
            {
                fm = new FileMeta
                {
                    FileId = metadata.FileId
                };
            }
            fm.Data = CoreHelper.SerializeToString(metadata);
            if (fm.EntityState == EntityState.Detached)
                eBookWriteManager.Context.AddToFileMetaSet(fm);

            eBookWriteManager.Context.SaveChanges();

            eBookWriteManager.Context.LogFileAction(cfmdc.ActivePerson.Id, fm.FileId, "UPDATED FILE META", fm.FileId.ToString());

        }

        public bool UnCloseFile(CriteriaIdAndCultureDataContract cicdc)
        {
            if (string.IsNullOrEmpty(cicdc.EncodedActivePerson)) throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { Message = "User not found. Please make sure you are logged on." });
            if (cicdc.ActivePerson == null)
            {
                cicdc.ActivePerson = new BusinessHelper().DecodePerson(cicdc.EncodedActivePerson);
            }
            PersonDataContract person = new EY.com.eBook.API.BL.Read.HomeRepository().GetPerson(cicdc.ActivePerson.Id);

            if (person.IsSysAdmin || person.IsChampion)
            {
                File file = eBookWriteManager.Context.Files.FirstOrDefault(f => f.Id == cicdc.Id);
                if (file != null)
                {
                    file.CloseDate = null;
                    file.ClosedBy = null;
                    eBookWriteManager.Context.SaveChanges();
                    
                    return true;
                }
            }
            else
            {
                throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { Message = "Only administrators or champions can unclose a file." });
            }
            //cicdc.ActivePerson.Id
            return false;
        }

        public bool UnCloseFileNumbers(CriteriaIdAndCultureDataContract cicdc)
        {
            if (string.IsNullOrEmpty(cicdc.EncodedActivePerson)) throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { Message = "User not found. Please make sure you are logged on." });
            if (cicdc.ActivePerson == null)
            {
                cicdc.ActivePerson = new BusinessHelper().DecodePerson(cicdc.EncodedActivePerson);
            }
            PersonDataContract person = new EY.com.eBook.API.BL.Read.HomeRepository().GetPerson(cicdc.ActivePerson.Id);

            if (person.IsSysAdmin || person.IsChampion)
            {
                File file = eBookWriteManager.Context.Files.FirstOrDefault(f => f.Id == cicdc.Id);
                if (file != null)
                {
                    file.NumbersLocked = false;
                    eBookWriteManager.Context.SaveChanges();
                    SetFileInCache(file.Id,false);
                    return true;
                }
            }
            else
            {
                throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { Message = "Only administrators or champions can unclose a file." });
            }
            //cicdc.ActivePerson.Id
            return false;
        }

        public bool UnCloseWorksheet(CriteriaWorksheetDataContract cwsdc)
        {
            if (string.IsNullOrEmpty(cwsdc.EncodedActivePerson)) throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { Message = "User not found. Please make sure you are logged on." });
            if (cwsdc.ActivePerson == null)
            {
                cwsdc.ActivePerson = new BusinessHelper().DecodePerson(cwsdc.EncodedActivePerson);
            }

            Worksheet worksheet = eBookWriteManager.Context.Worksheets.FirstOrDefault(w => w.FileId == cwsdc.FileId && w.TypeId == cwsdc.TypeId);
            if (worksheet != null)
            {
                worksheet.Closed = false;
                worksheet.ClosedBy = null;
                worksheet.ClosedDate = null;
                eBookWriteManager.Context.SaveChanges();
                SetFileInCache(cwsdc.FileId, false);
                eBookWriteManager.Context.LogFileAction(cwsdc.ActivePerson.Id, cwsdc.FileId, "UNCLOSED WORKSHEET ", cwsdc.TypeId.ToString());
            }
            return true;
        }

        public bool UnCloseAllWorksheets(CriteriaIdAndCultureDataContract cicdc)
        {
            if (string.IsNullOrEmpty(cicdc.EncodedActivePerson)) throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { Message = "User not found. Please make sure you are logged on." });
            if (cicdc.ActivePerson == null)
            {
                cicdc.ActivePerson = new BusinessHelper().DecodePerson(cicdc.EncodedActivePerson);
            }

            foreach (Worksheet worksheet in eBookWriteManager.Context.Worksheets.Where(w => w.FileId == cicdc.Id))
            {
                worksheet.Closed = false;
                worksheet.ClosedBy = null;
                worksheet.ClosedDate = null;
            }
            eBookWriteManager.Context.SaveChanges();
            SetFileInCache(cicdc.Id, false);
            eBookWriteManager.Context.LogFileAction(cicdc.ActivePerson.Id, cicdc.Id, "UNCLOSED  ALL WORKSHEETS", "");
            return true;
        }

        public bool UnCloseBundle(CriteriaIdAndCultureDataContract cicdc)
        {
            if (string.IsNullOrEmpty(cicdc.EncodedActivePerson)) throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { Message = "User not found. Please make sure you are logged on." });
            if (cicdc.ActivePerson == null)
            {
                cicdc.ActivePerson = new BusinessHelper().DecodePerson(cicdc.EncodedActivePerson);
            }
            PersonDataContract person = new EY.com.eBook.API.BL.Read.HomeRepository().GetPerson(cicdc.ActivePerson.Id);

            if (person.IsSysAdmin || person.IsChampion)
            {
                FileReportLayout fr = eBookWriteManager.Context.FileReportLayouts.FirstOrDefault(f => f.Id == cicdc.Id);
                if (fr != null)
                {
                    fr.Closed = false;
                    fr.ClosedBy = null;
                    fr.ClosedDate = null;
                    eBookWriteManager.Context.SaveChanges();
                    SetFileInCache(fr.FileId, false);
                    return true;
                }
            }
            else
            {
                throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { Message = "Only administrators or champions can unclose a delivarable." });
            }
            return false;
        }

        public XElement GetWorksheetData(Guid fileId, Guid typeId)
        {
            Worksheet ws = eBookWriteManager.Context.Worksheets.FirstOrDefault(w => w.FileId == fileId && w.TypeId == typeId);
            if (ws == null) return XElement.Parse("<root/>");
            if (string.IsNullOrEmpty(ws.Data)) return XElement.Parse("<root/>");
            return XElement.Parse(ws.Data).StripNamespaces();
        }


        public bool SaveFileSettings(CriteriaSaveFileSettingsDataContract csfsdc)
        {
            FileSettingsInfoDataContract fsidc = csfsdc.Settings;
            File file = eBookWriteManager.Context.Files.FirstOrDefault(f => f.Id == fsidc.FileId);
            bool datechange = false;
            if (file != null)
            {

                if (file.StartDate != fsidc.StartDate) { 
                    file.StartDate = fsidc.StartDate;
                    datechange = true;
                }
                if (file.EndDate != fsidc.EndDate)
                {
                    file.EndDate = fsidc.EndDate;
                    datechange = true;
                }
                if (file.PrevStartDate != fsidc.PreviousStartDate) file.PrevStartDate = fsidc.PreviousStartDate;
                if (file.PrevEndDate != fsidc.PreviousEndDate) file.PrevEndDate = fsidc.PreviousEndDate;
                if (file.Culture != fsidc.Culture) file.Culture = fsidc.Culture;
                fsidc.Settings.FileId = file.Id;
                file.Settings = CoreHelper.SerializeToString(fsidc.Settings);
                eBookWriteManager.Context.SaveChanges();
                if (datechange)
                {
                    CleanInvalidDates(file);
                }
                ICacheService proxy;
                if (TryFetchICacheService(out proxy))
                {
                    proxy.ReloadOrAddFileInCacheForce(file.Id, true);
                }
                csfsdc.ActivePerson = new BusinessHelper().DecodePerson(csfsdc.EncodedActivePerson);
                eBookWriteManager.Context.LogFileAction(csfsdc.ActivePerson.Id, fsidc.FileId, "CHANGED FILE SETTINGS", "");

                eBookWriteManager.Context.MarkFileAccountLastChanged(fsidc.FileId, DateTime.Now.AddSeconds(-1));
                
                return true;
            }

            return false;
        }

        public static string ToIso8601DateOnly(DateTime dte)
        {

            return dte.ToString("yyyy-MM-dd");
        }

        public void CleanInvalidDates(File file)
        {
            FileXbrl fx = eBookWriteManager.Context.FileXbrlSet.FirstOrDefault(f => f.FileId == file.Id);
            string startDate = ToIso8601DateOnly(file.StartDate);
            string endDate = ToIso8601DateOnly(file.EndDate);
            string iStart = ToIso8601DateOnly(file.StartDate.AddDays(-1));
            //     FileXbrl fx = eBookWriteManager.Context.FileXbrlSet.FirstOrDefault(f => f.FileId == file.Id);
            if (fx != null)
            {
                EY.com.eBook.BizTax.Contracts.BizTaxDataContract bzt = CoreHelper.DeserializeFromString<EY.com.eBook.BizTax.Contracts.BizTaxDataContract>(fx.XbrlData);
                List<EY.com.eBook.BizTax.ContextElementDataContract> cedcs = bzt.Contexts.Where(c => !string.IsNullOrEmpty(c.Period.StartDate) && c.Period.StartDate != startDate).ToList();
                List<EY.com.eBook.BizTax.ContextElementDataContract> cedce = bzt.Contexts.Where(c => !string.IsNullOrEmpty(c.Period.EndDate) && c.Period.EndDate != endDate).ToList();
                List<EY.com.eBook.BizTax.ContextElementDataContract> cedcis = bzt.Contexts.Where(c => c.Id.StartsWith("I-Start") && c.Period.Instant != iStart).ToList();
                List<EY.com.eBook.BizTax.ContextElementDataContract> cedcie = bzt.Contexts.Where(c => c.Id.StartsWith("I-End") && c.Period.Instant != endDate).ToList();

                bool save = false;
                if (cedcs.Count > 0)
                {
                    cedcs.ForEach(c => { c.Period.StartDate = startDate; });
                    save = true;
                }
                if (cedce.Count > 0)
                {
                    cedce.ForEach(c => { c.Period.EndDate = endDate; });

                    save = true;
                }
                if (cedcis.Count > 0)
                {
                    cedcis.ForEach(c => { c.Period.Instant = iStart; });

                    save = true;
                }
                if (cedcie.Count > 0)
                {
                    cedcie.ForEach(c => { c.Period.Instant = endDate; });

                    save = true;
                }
                var xe = bzt.Elements.FirstOrDefault(x => x.Name == "PeriodStartDate");
                if (xe != null && xe.Value != startDate)
                {
                    xe.Value = startDate;
                    save = true;
                }
                xe = bzt.Elements.FirstOrDefault(x => x.Name == "PeriodEndDate");
                if (xe != null && xe.Value != endDate)
                {
                    xe.Value = endDate;
                    save = true;
                }


                /*
                cedcs = bzt.Contexts.Where(c => c.Scenario!=null && c.Scenario.Count(s=>s.Type=="d-ty:date")>0).ToList();
                if (cedcs.Count > 0)
                {
                    cedcs.ForEach(c => { 
                        c.Scenario.Where(s=>s.Type=="d-ty:date").ToList().ForEach(s=> { s.Type="d-ty:DateTypedID"; });
                    });
                    fx.XbrlData = CoreHelper.SerializeToString(bzt);
                    Console.Write("...cleaned d-ty:date to DateTypedID");
                    save = true;
                } * */

                if (save)
                {
                    fx.XbrlData = CoreHelper.SerializeToString(bzt);
                    eBookWriteManager.Context.SaveChanges();
                }


            }
            

        }

        public void ReopenFile(List<CriteriaIdAndBoolDataContract> lcibdc) {
            foreach (CriteriaIdAndBoolDataContract cibdc in lcibdc)
            {
                File file = eBookWriteManager.Context.Files.FirstOrDefault(f => f.Id == cibdc.Id);
                file.CloseDate = null;
                file.ClosedBy = null;
                if (cibdc.Boolean) {
                    file.NumbersLocked = false;
                    FileService fs = eBookWriteManager.Context.FileServices.FirstOrDefault(f => f.FileId == cibdc.Id && f.ServiceId == fsYearEnd);
                    if (fs != null)
                    {
                        fs.Completed = null;
                        fs.CompletedBy = null;
                        fs.Status = null;
                    }
                }
                eBookWriteManager.Context.SaveChanges();
            }            
        }



        public void CloseFile(List<CriteriaIdDataContract> lcidc)
        {
            foreach (CriteriaIdDataContract cidc in lcidc)
            {
                File file = eBookWriteManager.Context.Files.FirstOrDefault(f => f.Id == cidc.Id);
                file.CloseDate = DateTime.Now;
                file.ClosedBy = "AdminTool";
                file.NumbersLocked = true;
                FileService fs = eBookWriteManager.Context.FileServices.FirstOrDefault(f => f.FileId == cidc.Id && f.ServiceId == fsYearEnd);
                if (fs != null && fs.Completed == null && fs.CompletedBy == null) {
                    fs.Completed = DateTime.Now;
                    fs.CompletedBy = "AdminTool";
                }
                eBookWriteManager.Context.SaveChanges();
            }
        }

        protected override void RegisterTranslators()
        {
            
        }
    }
}
