﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using EY.com.eBook.Core;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.Core.EF;
using EY.com.eBook.API.Contracts.Data.Logic;
using System.Data.Objects;
using EY.com.eBook.Core.EntityTranslation;
using EY.com.eBook.API.BL.Translators;
using System.ServiceModel;
using EY.com.eBook.Core.Data;
using EY.com.eBook.API.Contracts.Proxies;
using EY.com.eBook.API.Contracts.Data.Cache;
using EY.com.eBook.Core.EF.Write;
using System.Data;
using System.Transactions;
using EY.com.eBook.API.Contracts.Data.Meta;
using EY.com.eBook.ProAcc.API.Contracts.Data;
using System.Xml.Linq;
using EY.com.eBook.Excel.BL;
using EY.com.eBook.API.Contracts.Services;
using EY.com.eBook.Core.IText.Render;
using System.Configuration;

namespace EY.com.eBook.API.BL.Write
{

    public class FileServiceRepository : eBookRepository
    {
        //Services
        private Guid fsYearEnd = new Guid("C3B8C8DB-3822-4263-9098-541FAE897D02");
        private Guid fsAnnualAccounts = new Guid("1ABF0836-7D7B-48E7-9006-F03DB97AC28B");

        /// <summary>
        /// reopens a service for the given file
        /// </summary>
        /// <param name="cfspdc">CriteriaFileServicePersonDataContract</param>
        public void ReOpenService(CriteriaFileServicePersonDataContract cfspdc)
        {
            if (cfspdc == null)
                throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { Message = "Criteria cannot be null!" });

            if (cfspdc.FileId == Guid.Empty)
                throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { Message = "File ID is missing!" });

            if (string.IsNullOrEmpty(cfspdc.EncodedActivePerson))
                throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { Message = "Encoded Person is missing!" });

            //Only Year End and  service can be reopened
            if (cfspdc.ServiceId != fsYearEnd && cfspdc.ServiceId != fsAnnualAccounts)
                throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { Message = "Currently only the Year End and Annual Accounts service can be reopened!" });

            ActivePersonDataContract activePerson = null;
            try
            {
                activePerson = new BusinessHelper().DecodePerson(cfspdc.EncodedActivePerson);
            }
            catch (Exception e)
            {
                throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { Message = "Failed converting encoded person." + e.Message });
            }

            //Get services of file
            List<FileService> fileservices = eBookWriteManager.Context.FileServices.Where(s => s.FileId == cfspdc.FileId).ToList();
            //Get service
            FileService fs = fileservices.FirstOrDefault(s => s.ServiceId == cfspdc.ServiceId);
            if (fs == null)
                throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { Message = "Service not found!" });
            //Get person
            Person pers = eBookWriteManager.Context.PersonSet.FirstOrDefault(p => p.Id == activePerson.Id);
            if (pers == null)
                throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { Message = "Person not found!" });

            //Save service being opened
            fs.Status = cfspdc.Status;
            fs.Completed = null;
            fs.CompletedBy = null;
            eBookWriteManager.Context.SaveChanges();

            //Save file being opened
            File fle = eBookWriteManager.Context.Files.First(f => f.Id == cfspdc.FileId);
            fle.NumbersLocked = false;
            fle.CloseDate = null;
            fle.ClosedBy = null;

            eBookWriteManager.Context.SaveChanges();

            //Save worksheets being opened
            var wsTypes = eBookReadManager.Context.ServiceWorksheetTypes.Where(s => s.ServiceId == cfspdc.ServiceId).Select(s => s.WorksheetTypeId).ToList();
            var ws = eBookWriteManager.Context.Worksheets.Where(w => w.FileId == cfspdc.FileId).ToList().Where(w => wsTypes.Contains(w.TypeId)).ToList();
            ws.ForEach(w => { w.Closed = false; w.ClosedBy = null; w.ClosedDate = null; });
            eBookWriteManager.Context.SaveChanges();

            //Opens bundle, replaces repository
            new EY.com.eBook.API.BL.Read.BundleRepository().UnlockBundle(cfspdc.FileId, cfspdc.BundleId);

            //Log file action
            eBookWriteManager.Context.LogFileAction(activePerson.Id, cfspdc.FileId, "REOPENED FILE SERVICE", cfspdc.ServiceId.ToString());
        }

        public void CloseService(CriteriaFileServicePersonDataContract cfspdc)
        {
            if (cfspdc == null)
                throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { Message = "Criteria cannot be null!" });

            if (cfspdc.FileId == Guid.Empty)
                throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { Message = "File ID is missing!" });

            if (string.IsNullOrEmpty(cfspdc.EncodedActivePerson))
                throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { Message = "Encoded Person is missing!" });

            ActivePersonDataContract activePerson = null;
            try
            {
                activePerson = new BusinessHelper().DecodePerson(cfspdc.EncodedActivePerson);
            }
            catch (Exception e)
            {
                throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { Message = "Failed converting encoded person" });
            }

            //Get all service records of file
            List<FileService> fileservices = eBookWriteManager.Context.FileServices.Where(s => s.FileId == cfspdc.FileId).ToList();
            //Get service
            FileService fs = fileservices.FirstOrDefault(s => s.ServiceId == cfspdc.ServiceId);
            if (fs == null)
                throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { Message = "Service not found!" });
            //Get person
            Person pers = eBookWriteManager.Context.PersonSet.FirstOrDefault(p => p.Id == activePerson.Id);
            if (pers == null)
                throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { Message = "Person not found!" });

            //Save completion of file service
            fs.Completed = DateTime.Now;
            fs.CompletedBy = string.Format("{0} {1}", pers.FirstName, pers.LastName).Trim();
            short? previousStatus = fs.Status;
            fs.Status = cfspdc.Status;
            eBookWriteManager.Context.SaveChanges();

            //Get file
            File fle = eBookWriteManager.Context.Files.First(f => f.Id == cfspdc.FileId);

            if (cfspdc.Status.HasValue)
            {
                //if ((previousStatus == null || previousStatus == 1 || previousStatus == 2 || previousStatus == 4 || previousStatus == 7)) // Year end bundle  flow either rejected or in progress, other flows status null
                //{
                //Get all worksheettypes linked to the service ID
                var wsTypes = eBookReadManager.Context.ServiceWorksheetTypes.Where(s => s.ServiceId == cfspdc.ServiceId).Select(s => s.WorksheetTypeId).ToList();
                //Get all worksheets linked to the retrieved worksheet types and the file
                var ws = eBookWriteManager.Context.Worksheets.Where(w => w.FileId == cfspdc.FileId).ToList().Where(w => wsTypes.Contains(w.TypeId)).ToList();
                //Save closing of worksheets
                ws.ForEach(w => { w.Closed = true; w.ClosedBy = pers.Id; w.ClosedDate = DateTime.Now; });
                eBookWriteManager.Context.SaveChanges();
                //}
            }

            // CLOSE FILE IF ALL SERVICES ARE CLOSED.
            if (fileservices.Count(s => !s.Completed.HasValue) == 0)
            {
                fle.CloseDate = DateTime.Now;
                fle.ClosedBy = fs.CompletedBy;
            }

            fle.NumbersLocked = true;

            eBookWriteManager.Context.SaveChanges();


            if (cfspdc.ServiceId == fsYearEnd)
            {
                //Locks bundle, replaces repository
                EY.com.eBook.API.BL.Read.BundleRepository bundleRep = new EY.com.eBook.API.BL.Read.BundleRepository();
                bundleRep.LockBundle(cfspdc.FileId, cfspdc.BundleId);
                //bundleRep.setDraft(cfspdc.BundleId, true);
            }

            eBookWriteManager.Context.LogFileAction(activePerson.Id, cfspdc.FileId, (cfspdc.Status == 0 ? "PRE" : "") + "CLOSED FILE SERVICE", cfspdc.ServiceId.ToString());
            FileRepository fileRepository = new FileRepository();
            fileRepository.SetFileInCache(cfspdc.FileId, false);
        }

        /// <summary>
        /// Create a log entry with the purpose of recording file service changes (such as year end bundle flow)
        /// </summary>
        /// <param name="fs">file service record</param>
        /// <param name="actions">action list</param>
        /// <param name="comment">user comment</param>
        /// <param name="personId">person</param>
        public void CreateFileServiceLogEntry(FileService fs, string actions, string comment, Guid personId, Guid? repositoryitemId, Guid? pdfAttachment)
        {
            Person person = eBookWriteManager.Context.PersonSet.FirstOrDefault(p => p.Id == personId);

            //Create log record
            FileServiceLog fsl = new FileServiceLog
            {
                Id = Guid.NewGuid(),
                ServiceId = fs.ServiceId,
                StatusId = fs.Status != null ? fs.Status : (short?)null,
                Action = actions,
                Comment = comment,
                PdfAttachmentId = pdfAttachment,
                Timestamp = DateTime.Now,
                RepositoryItemId = repositoryitemId,
                PersonId = person.Id,
                FileId = fs.FileId,
                PersonFirstName = person.FirstName,
                PersonLastName = person.LastName
            };
            eBookWriteManager.Context.AddToFileServiceLogs(fsl);
            eBookWriteManager.Context.SaveChanges();
        }

        /// <summary>
        /// Reset the service flow
        /// </summary>
        /// <param name="cisdc">UpdateFileServiceDataContract</param>
        public void ResetServiceFlow(UpdateFileServiceDataContract ufsdc)
        {
            //Unlock bundle
            eBookReadManager.Context.Bundles.Where(b => b.FileId == ufsdc.FileId && b.Id == ufsdc.BundleId).ToList().ForEach(b => { b.Locked = false; });
            /*
            //Set file service status to 1
            FileService fs = eBookWriteManager.Context.FileServices.FirstOrDefault(f => f.FileId == ufsdc.FileId && f.ServiceId == ufsdc.ServiceId);
            if (ufsdc.ActivePerson == null) ufsdc.ActivePerson = new BusinessHelper().DecodePerson(ufsdc.EncodedActivePerson);
            ServiceStatuses status = eBookWriteManager.Context.ServiceStatuses.FirstOrDefault(d => d.Id == 1);
            fs.Status = status.Id;
            */
            //

            eBookReadManager.Context.SaveChanges();

            if (ufsdc.BundleId != null)
            {
                CriteriaFileServicePersonDataContract cfspdc = new CriteriaFileServicePersonDataContract
                {
                    FileId = ufsdc.FileId,
                    ServiceId = ufsdc.ServiceId,
                    BundleId = (Guid)ufsdc.BundleId,
                    ActivePerson = ufsdc.ActivePerson,
                    EncodedActivePerson = ufsdc.EncodedActivePerson,
                    Status = 1
                };

                //Ppen service
                ReOpenService(cfspdc);
            }
        }

        /// <summary>
        /// Adds a service to the file
        /// </summary>
        /// <param name="cfspdc">cfspdc</param>
        public FileServiceDataContract AddService(CriteriaFileServicePersonDataContract cfspdc)
        {
            //An improvement would be adding the service table to EF and verifying the service GUID
            if (cfspdc == null)
                throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { Message = "Criteria cannot be null!" });

            if (cfspdc.FileId == Guid.Empty)
                throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { Message = "File ID is missing!" });

            if (cfspdc.ServiceId == Guid.Empty)
                throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { Message = "Service ID is missing!" });

            if (string.IsNullOrEmpty(cfspdc.EncodedActivePerson))
                throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { Message = "Encoded Person is missing!" });

            ActivePersonDataContract activePerson = null;
            try
            {
                activePerson = new BusinessHelper().DecodePerson(cfspdc.EncodedActivePerson);
            }
            catch (Exception e)
            {
                throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { Message = "Failed converting encoded person" });
            }

            //Get file
            File file = eBookWriteManager.Context.Files.FirstOrDefault(f => f.Id == cfspdc.FileId);

            //Get order number of last service linked to the file
            int order = eBookWriteManager.Context.FileServices.Where(s => s.FileId == cfspdc.FileId && s.ServiceId == cfspdc.ServiceId).OrderByDescending(s => s.Order).Select(s => s.Order).FirstOrDefault();

            //Get person
            Person pers = eBookWriteManager.Context.PersonSet.FirstOrDefault(p => p.Id == activePerson.Id);
            if (pers == null)
                throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { Message = "Person not found!" });

            if (file != null)
            {

                FileService fileService = new FileService
                {
                    FileId = cfspdc.FileId,
                    ServiceId = cfspdc.ServiceId,
                    Completed = null,
                    CompletedBy = null,
                    Order = order + 1,
                    Status = 1
                };

                if (fileService.EntityState == EntityState.Detached)
                    eBookWriteManager.Context.AddToFileServices(fileService);
                eBookWriteManager.Context.SaveChanges();

                //Log file action
                eBookWriteManager.Context.LogFileAction(activePerson.Id, cfspdc.FileId, "CREATED FILE SERVICE", cfspdc.ServiceId.ToString());

                //return
                return new FileServiceDataContract()
                {
                    Completed = fileService.Completed,
                    CompletedBy = fileService.CompletedBy,
                    FileId = fileService.FileId,
                    ServiceId = fileService.ServiceId,
                    Order = fileService.Order,
                    Status = fileService.Status
                };
            }
            return null;
        }

        /// <summary>
        /// The UpdateFileService function does exactly that. It contains specific flows per file service.
        /// </summary>
        /// <param name="cisdc">UpdateCodaPoaDataContract</param>
        public FileServiceStatusDataContract UpdateFileService(UpdateFileServiceDataContract ufsdc)
        {

            //Classes
            EY.com.eBook.API.BL.Read.FileServiceRepository fileServiceRep = new EY.com.eBook.API.BL.Read.FileServiceRepository();
            ITextRepository iTextRep = new ITextRepository();
            //Retrieval of records
            FileService fs = eBookWriteManager.Context.FileServices.FirstOrDefault(f => f.FileId == ufsdc.FileId && f.ServiceId == ufsdc.ServiceId);
            File file = eBookWriteManager.Context.Files.Where(f => f.Id == fs.FileId).FirstOrDefault();
            Client client = eBookWriteManager.Context.ClientSet.Where(c => c.Id == file.ClientId).FirstOrDefault();
            //Decode person
            if (ufsdc.ActivePerson == null) ufsdc.ActivePerson = new BusinessHelper().DecodePerson(ufsdc.EncodedActivePerson);
            //Lists
            List<string> changes = new List<string>();
            List<string> rolesToNotify = new List<string>();
            List<string> rolesToCC = new List<string>();
            //Strings
            string comment = String.Empty;
            string filepath = String.Empty;
            string errorMessage = String.Empty;
            string fileEnddate = file.EndDate.ToString("dd/MM/yyyy");
            string mailSubject = String.Empty;
            string mailBody = String.Empty;
            string mailAttachmentFilePath = String.Empty;
            string invoiceFilePath = String.Empty;
            string header = "<html> <header> <style TYPE=\"text/css\"> body { background: #eee; color:black; font-family: Arial; font-size: 13px; } h4 { margin-top:2px;} ul { list-style-type:square; padding-left:15px} </style> </header>";
            string body = String.Empty;
            string sNl = String.Empty;
            string sFr = String.Empty;
            string footer = String.Empty;
            string reviewNotes = String.Empty;
            //Datacontracts
            CriteriaIdAndCultureDataContract ciacdc = new CriteriaIdAndCultureDataContract
            {
                Id = client.Id
            };

            //Status is optional, if supplied it triggers that step of the file service
            if (ufsdc.Status.HasValue)
            {
                int? FollowUpStatus = ufsdc.Status;
                List<string> teamMemberRoles = new List<string>();
                List<TeamMemberDataContract> teamMembers = new EY.com.eBook.API.BL.Read.ClientRepository().GetClientTeam(ciacdc);
                foreach (TeamMemberDataContract teamMember in teamMembers)
                {
                    teamMemberRoles.Add(teamMember.Role);
                }

                EY.com.eBook.API.BL.Read.BundleRepository bundleRep = new EY.com.eBook.API.BL.Read.BundleRepository();
                BundleDataContract bundle = null;
                CriteriaFileServicePersonDataContract cfspdc = null;
                if (ufsdc.BundleId != null)
                {

                    cfspdc = new CriteriaFileServicePersonDataContract
                    {
                        FileId = ufsdc.FileId,
                        ServiceId = ufsdc.ServiceId,
                        BundleId = (Guid)ufsdc.BundleId,
                        ActivePerson = ufsdc.ActivePerson,
                        EncodedActivePerson = ufsdc.EncodedActivePerson,
                        Status = (Int16)ufsdc.Status
                    };
                }

                if (ufsdc.ServiceId == fsYearEnd)
                {
                    switch (ufsdc.Status)
                    {
                        case 1: //In progress
                            break;
                        case 2: //Draft awaiting executive approval (consultant committed)
                            rolesToNotify.Add("Manager");
                            rolesToNotify.Add("Director");
                            //rolesToNotify.Add("Partner");
                            mailSubject = client.Name + ": review EM required of YEC deliverable per " + fileEnddate;
                            sNl = "De afsluitingsbundel per " + fileEnddate + " van " + client.Name + " dient nagekeken en gevalideerd te worden.<br>Deze bundel is beschikbaar in eBook onder Year-end deliverables.<br>Indien akkoord – klik in eBook op “Approve”<br>Indien niet akkoord – klik in eBook op “Reject” + add review notes";
                            sFr = "La liasse de cl&ocirc;ture des comptes cl&ocirc;tur&eacute;s au " + fileEnddate + " de " + client.Name + " doit &ecirc;tre revue et valid&eacute;e.<br>La liasse est disponible en eBook sous Year-end deliverables.<br>Si d&rsquo;accord &ndash; cliquez dans eBook sur &ldquo;Approve&rdquo;<br>Si pas d&rsquo;accord &ndash; cliquez dans eBook sur &ldquo;Reject&rdquo; + add review notes";
                            //Close service, Generate draft pdf & save in repository
                            CloseService(cfspdc);

                            //Lock bundle
                            //eBookReadManager.Context.Bundles.Where(b => b.FileId == ufsdc.FileId).ToList().ForEach(b => { b.Locked = true; });
                            //eBookReadManager.Context.SaveChanges();

                            //Lock bundle which generates a draft bundle
                            //bundleRep.LockBundle(ufsdc.FileId,ufsdc.BundleId, true);
                            //Save in repository
                            bundle = bundleRep.GetBundle(cfspdc.BundleId);
                            bundle.RectoVerso = true;
                            filepath = iTextRep.GenerateBundle(cfspdc.BundleId);

                            changes.Add("draftGenerated");
                            changes.Add("bundleLocked");

                            //Notify Executive

                            break;
                        case 3: //Approved by Executive (em)
                            rolesToNotify.Add("OfficeValidator");
                            rolesToCC.Add("Staff");
                            rolesToCC.Add("Partner");
                            mailSubject = client.Name + ": YEC deliverable per " + fileEnddate + " approved by EM";
                            sNl = "De afsluitingsbundel per " + fileEnddate + " van " + client.Name + " werd gevalideerd door de Engagement Manager.<br>De afsluitingsbundel is beschikbaar in eBook en dient afgeprint en bezorgd te worden aan de EP ter validatie.";
                            sFr = "La liasse de cl&ocirc;ture des comptes cl&ocirc;tur&eacute;s au " + fileEnddate + " de " + client.Name + " a &eacute;t&eacute; valid&eacute;e par l&rsquo;Engagement Manager.<br>La liasse est disponible en eBook sous Year-end deliverables et doit &ecirc;tre imprim&eacute;e et transmise &agrave; l&rsquo;EP pour validation.";

                            //Generate non-draft contents
                            bundleRep.LockBundle(ufsdc.FileId, cfspdc.BundleId);
                            bundleRep.setDraft(cfspdc.BundleId, false);
                            //Save in repository
                            bundle = bundleRep.GetBundle(cfspdc.BundleId);
                            bundle.RectoVerso = true;
                            filepath = iTextRep.GenerateBundle(bundle);

                            changes.Add("finalGenerated");
                            break;
                        case 4: //Rejected by Executive (Executive rejected)
                            rolesToNotify.Add("Staff");
                            //Reopen service
                            ReOpenService(cfspdc);
                            mailSubject = client.Name + ": YEC deliverable rejected by EM";
                            sNl = "De afsluitingsbundel per " + fileEnddate + " van " + client.Name + " dient aangepast te worden conform de review notes van de Engagement Manager.<br>De afsluitingsbundel en de review notes zijn beschikbaar in eBook onder Year-end deliverables.";
                            sFr = "La liasse de cl&ocirc;ture des comptes cl&ocirc;tur&eacute;s au " + fileEnddate + " de " + client.Name + " doit &ecirc;tre adapt&eacute;e sur base des review notes de l&rsquo;Engagement Manager.<br>La liasse et les review notes sont disponibles en eBook sous Year-end deliverables.";
                            //Reopen bundle
                            //eBookReadManager.Context.Bundles.Where(b => b.FileId == ufsdc.FileId).ToList().ForEach(b => { b.Locked = false; });
                            //eBookReadManager.Context.SaveChanges();

                            changes.Add("bundleOpen");
                            break;

                        case 5: //Approved by partner
                            rolesToNotify.Add("OfficeValidator");
                            rolesToCC.Add("Staff");
                            rolesToCC.Add("Manager");
                            rolesToCC.Add("Director");
                            mailSubject = client.Name + ": YEC deliverable per " + fileEnddate + " approved by EP";
                            sNl = "De afsluitingsbundel per " + fileEnddate + " van " + client.Name + " werd gevalideerd door de Engagement Partner.<br>De afsluitingsbundel is beschikbaar in eBook en dient afgeprint te worden ter verzending aan de klant. De accounting memorandum dient afgeprint te worden op briefpapier EY. De bundel dient bezorgd te worden aan de EP ter ondertekening.";
                            sFr = "La liasse de cl&ocirc;ture des comptes cl&ocirc;tur&eacute;s au " + fileEnddate + " de " + client.Name + " a &eacute;t&eacute; valid&eacute;e par l&rsquo;Engagement Partner.<br>La liasse est disponible en eBook sous Year-end deliverables et doit &ecirc;tre imprim&eacute;e pour envoi au client. Le memorandum comptable doit &ecirc;tre imprim&eacute; sur du papier &agrave; lettre EY. La liasse client doit &ecirc;tre transmise &agrave; l&rsquo;EP (et/ou EM) pour signature.";
                            break;
                        case 6: //Rejected by partner;

                            //Lock Bundle, Generate draft pdf & save in repository
                            bundleRep.LockBundle(ufsdc.FileId, cfspdc.BundleId);
                            bundleRep.setDraft(cfspdc.BundleId, true);

                            //Save in repository
                            bundle = bundleRep.GetBundle(cfspdc.BundleId);
                            bundle.RectoVerso = true;
                            filepath = iTextRep.GenerateBundle(cfspdc.BundleId);

                            rolesToNotify.Add("Manager");
                            rolesToNotify.Add("Director");
                            rolesToCC.Add("Staff");

                            mailSubject = client.Name + ": YEC deliverable per " + fileEnddate + "  rejected by EP";
                            sNl = "De afsluitingsbundel per " + fileEnddate + " van " + client.Name + " dient aangepast te worden conform de review notes van de Engagement Partner.<br>De afsluitingsbundel en de review notes zijn beschikbaar in eBook onder Year-end deliverables";
                            sFr = "La liasse de cl&ocirc;ture des comptes cl&ocirc;tur&eacute;s au " + fileEnddate + " de " + client.Name + " doit &ecirc;tre adapt&eacute;e sur base des review notes de l&rsquo;Engagement Partner.<br>Les liasses et les review notes sont disponibles en eBook sous Year-end deliverables.";
                            break;
                        case 7: //Sent to client for approval (Office Validator or auto-committed)
                            rolesToCC.Add("Staff");
                            rolesToCC.Add("Pertner");
                            rolesToNotify.Add("Manager");
                            rolesToNotify.Add("Director");
                            mailSubject = client.Name + ": YEC deliverable per " + fileEnddate + " signed by EP and sent to client for approval";
                            sNl = "De afsluitingsbundel per " + fileEnddate + " van " + client.Name + " werd uitgestuurd aan de klant ter ondertekening.";
                            sFr = "La liasse de cl&ocirc;ture des comptes cl&ocirc;tur&eacute;s au " + fileEnddate + " de " + client.Name + " a &eacute;t&eacute; envoy&eacute;e au client pour signature.";

                            break;
                        case 8: //Approved by client (Executive approves by client's request)
                            rolesToNotify.Add("Manager");
                            rolesToNotify.Add("Director");
                            rolesToCC.Add("Staff");
                            rolesToCC.Add("Partner");
                            mailSubject = client.Name + ": YEC deliverable per " + fileEnddate + " accepted by client";
                            body = "De statutaire documenten per " + fileEnddate + " van " + client.Name + " werden ondertekend ontvangen van de klant.<br>De getekende bevestigingsbrief, getekende jaarrekening en getekende vergaderstukken dienen elk apart opgeladen te worden in eBook onder de desbetreffende folder in Final deliverables.";
                            sFr = "Les documents statutaires de " + client.Name + " pour l&rsquo;exercice cl&ocirc;tur&eacute; le " + fileEnddate + " ont &eacute;t&eacute; re&ccedil;us sign&eacute;s du client.<br>La lettre de repr&eacute;sentation, les comptes annuels et les proc&egrave;s-verbaux sign&eacute;s doivent &ecirc;tre upload&eacute;s s&eacute;part&eacute;ment dans eBook (repository) dans les sections appropri&eacute;es de l&rsquo;onglet Final deliverables.";

                            break;
                        case 9: //Rejected by client (Executive/OV/Partner rejected by client's request)
                            rolesToNotify.Add("Manager");
                            rolesToNotify.Add("Director");
                            rolesToCC.Add("Staff");
                            rolesToCC.Add("Partner");
                            mailSubject = client.Name + ": YEC deliverable per " + fileEnddate + " rejected by client";
                            sNl = "De afsluitingsbundel per " + fileEnddate + " van " + client.Name + " dient aangepast te worden conform de bemerkingen van de klant.";
                            sFr = "La liasse annuel relative aux comptes cl&ocirc;tur&eacute;s au " + fileEnddate + " de " + client.Name + " doit &ecirc;tre adapt&eacute;e sur base des remarques formul&eacute;es par le client.";
                            //Store bundle as rejected + date
                            bundle = bundleRep.GetBundle(cfspdc.BundleId);
                            bundle.Id = Guid.NewGuid();
                            bundle.Name = bundle.Name + " Rejected " + DateTime.Now.ToString("yyyyMMdd");
                            bundle.RectoVerso = true;
                            iTextRep.GenerateBundle(bundle, true);
                            //Set draft
                            bundleRep.setDraft(cfspdc.BundleId, true);
                            changes.Add("rejectedGenerated");
                            break;
                        case 10: //Scanned and signed by client (Office Validator scans client's request)
                            rolesToNotify.Add("Staff");
                            rolesToNotify.Add("Manager");
                            rolesToNotify.Add("Director");
                            rolesToNotify.Add("Partner");
                            rolesToNotify.Add("OfficeValidator");
                            mailSubject = client.Name + ": Documents per " + fileEnddate + " signed by client and scanned in eBook";
                            sNl = "De getekende statutaire documenten per " + fileEnddate + " van " + client.Name + " werden opgeladen in eBook.<br>De getekende bevestigingsbrief, getekende jaarrekening en getekende vergaderstukken vind je in eBook terug onder de desbetreffende folders onder Final Deliverables.";
                            sFr = "Les documents statutaires sign&eacute;s de " + client.Name + " pour l&rsquo;exercice cl&ocirc;tur&eacute; le " + fileEnddate + " ont &eacute;t&eacute; upload&eacute;s dans eBook.<br>La lettre de repr&eacute;sentation, les comptes annuels et les proc&egrave;s-verbaux sign&eacute;s sont repris dans eBook (depository) dans les sections appropri&eacute;es de l&rsquo;onglet Final deliverables.";

                            changes.Add("bundleScanned");
                            break;
                        case 11: //Filed NBB
                            rolesToNotify.Add("Staff");
                            rolesToNotify.Add("Manager");
                            rolesToNotify.Add("Director");
                            rolesToNotify.Add("Partner");
                            rolesToNotify.Add("OfficeValidator");
                            mailSubject = client.Name + ": Statutory accounts per " + fileEnddate + " filed with NBB";
                            sNl = "De jaarrekening per " + fileEnddate + " van " + client.Name + " werd neergelegd bij de Nationale Bank van België.";
                            sFr = "Les comptes annuels cl&ocirc;tur&eacute;s au " + fileEnddate + " de " + client.Name + " ont &eacute;t&eacute; d&eacute;pos&eacute;s &agrave; la Banque Nationale de Belgique.";

                            break;
                        default:
                            //do nothing
                            break;
                    }
                }
                else if (ufsdc.ServiceId == fsAnnualAccounts) //NBB FILING
                {
                    //Class initilization
                    eBook.API.BL.Read.AnnualAccountsRepository annualAccountRepository = new eBook.API.BL.Read.AnnualAccountsRepository();
                    eBook.API.BL.Read.Repository repository = new eBook.API.BL.Read.Repository();
                    AnnualAccountDataContract aadc = new AnnualAccountDataContract();
                    EngagementCodeDataContract ecdc = new EngagementCodeDataContract();
                    CriteriaIdDataContract cidc = new CriteriaIdDataContract() { Id = ufsdc.FileId };
                    string sourceFilePath = string.Empty;
                    string sourceFolderPath = string.Empty;
                    string sourceClientFolderPath = string.Empty;
                    string sourceFileFolderPath = string.Empty;
                    string destinationClientFolderPath = string.Empty;
                    string destinationFileFolderPath = string.Empty;
                    string destinationFilePath = string.Empty;                
                    //Strings to be used for foldernames
                    string clientName = new string(client.Name.Where(c => char.IsLetterOrDigit(c) || c == '-').ToArray());
                    string fileName = new string(file.Name.Substring(4).Where(c => char.IsLetterOrDigit(c) || c == '-').ToArray()); //remove year from name

                    switch (ufsdc.Status)
                    {
                        case 1: //Created
                            break;
                        case 2: //Draft awaiting executive approval (consultant committed)
                            rolesToNotify.Add("Manager");
                            rolesToNotify.Add("Director");
                            //rolesToNotify.Add("Partner");
                            mailSubject = client.Name + ": review EM required of NBB bundle per " + fileEnddate;
                            sNl = "De NBB bundel per " + fileEnddate + " van " + client.Name + " dient nagekeken en gevalideerd te worden.<br>Deze bundel is beschikbaar in eBook onder File NBB.<br>Indien akkoord – klik in eBook op “Approve”<br>Indien niet akkoord – klik in eBook op “Reject” + add review notes";
                            sFr = "La liasse de NBB au " + fileEnddate + " de " + client.Name + " doit &ecirc;tre revue et valid&eacute;e.<br>La liasse est disponible en eBook sous File NBB.<br>Si d&rsquo;accord &ndash; cliquez dans eBook sur &ldquo;Approve&rdquo;<br>Si pas d&rsquo;accord &ndash; cliquez dans eBook sur &ldquo;Reject&rdquo; + add review notes";
                            //Close service, Generate draft pdf & save in repository
                            //CloseService(cfspdc);
                            bundleRep.LockBundle(ufsdc.FileId, cfspdc.BundleId);
                            filepath = iTextRep.GenerateBundle(cfspdc.BundleId);
                            changes.Add("bundleLocked");

                            if (!string.IsNullOrEmpty(ufsdc.Comment)) //Whenever changes were made after the General assembly meeting, the user has to fill in a note.
                            {
                                changes.Add("changesAfterGAD");
                                comment = ufsdc.Comment;
                            }

                            break;
                        case 3: //Approved by Executive (em)
                            rolesToNotify.Add("OfficeValidator");
                            rolesToCC.Add("Staff");
                            rolesToCC.Add("Partner");
                            mailSubject = client.Name + ": NBB bundle per " + fileEnddate + " approved by EM";
                            sNl = "De NBB bundel per " + fileEnddate + " van " + client.Name + " werd gevalideerd door de Engagement Manager.<br>De NBB bundel is beschikbaar in eBook onder File NBB.<br>Indien akkoord – klik in eBook op “Approve”<br>Indien niet akkoord – klik in eBook op “Reject” + add review notes";
                            sFr = "La liasse de NBB au " + fileEnddate + " de " + client.Name + " a &eacute;t&eacute; valid&eacute;e par l&rsquo;Engagement Manager.<br>La liasse est disponible en eBook sous File NBB.<br>Si d&rsquo;accord &ndash; cliquez dans eBook sur &ldquo;Approve&rdquo;<br>Si pas d&rsquo;accord &ndash; cliquez dans eBook sur &ldquo;Reject&rdquo; + add review notes";

                            //Generate non-draft contents
                            //bundleRep.setDraft(cfspdc.BundleId, false);
                            //Save in repository
                            bundle = bundleRep.GetBundle(cfspdc.BundleId);
                            bundle.RectoVerso = true;
                            filepath = iTextRep.GenerateBundle(bundle);
                            break;
                        case 4: //Rejected by Executive (Executive rejected)
                            rolesToNotify.Add("Staff");
                            //Reopen service
                            ReOpenService(cfspdc);
                            mailSubject = client.Name + ": NBB bundle rejected by EM";
                            sNl = "De NBB bundel per " + fileEnddate + " van " + client.Name + " dient aangepast te worden conform de review notes van de Engagement Manager.<br>De NBB bundel en de review notes zijn beschikbaar in eBook onder File NBB.";
                            sFr = "La liasse de NBB au " + fileEnddate + " de " + client.Name + " doit &ecirc;tre adapt&eacute;e sur base des review notes de l&rsquo;Engagement Manager.<br>La liasse et les review notes sont disponibles en eBook sous File NBB.";
                            //Reopen bundle
                            //eBookReadManager.Context.Bundles.Where(b => b.FileId == ufsdc.FileId).ToList().ForEach(b => { b.Locked = false; });
                            //eBookReadManager.Context.SaveChanges();

                            changes.Add("bundleOpen");
                            break;

                        case 5: //Approved by partner
                            rolesToNotify.Add("OfficeValidator");
                            rolesToCC.Add("Staff");
                            rolesToCC.Add("Manager");
                            rolesToCC.Add("Director");
                            mailSubject = client.Name + ": NBB bundle per " + fileEnddate + " approved by EP";
                            sNl = "De NBB bundel per " + fileEnddate + " van " + client.Name + " werd gevalideerd door de Engagement Partner en wacht op indiening door GTH.";
                            sFr = "La liasse de NBB au " + fileEnddate + " de " + client.Name + " a &eacute;t&eacute; valid&eacute;e par l&rsquo;Engagement Partner et attend la soumettre par GTH.";

                            //Get filepath based on repositoryItemId stored in annual accounts table
                            aadc = annualAccountRepository.GetAnnualAccounts(cidc);
                            sourceFilePath = repository.GetFilePath((Guid)aadc.RepositoryItemId); //Source filepath

                            if (!aadc.RepositoryItemId.HasValue)
                            {
                                throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { Message = "Repository item not found. Please reject and verify if the First file is present within the folder." });
                            }

                            /*using (UNCAccessWithCredentials unc = new UNCAccessWithCredentials())
                            {
                                if (unc.NetUseWithCredentials(Config.NBBTodoDir, Config.UserName, Config.AppuserDomain, Config.AppuserPwd))
                                {*/
                            using (new Impersonator(Config.AppuserUsername, Config.AppuserDomain, Config.AppuserPwd))
                            {
                                    //Destination filepath (NBB Todo folder/Client/file)
                                    destinationClientFolderPath = System.IO.Path.Combine(Config.NBBTodoDir, clientName + "-" + client.Id);  //first subdir in TODO (client)
                                    destinationFileFolderPath = System.IO.Path.Combine(destinationClientFolderPath, fileName + "-" + file.Id); //second subdier in TODO (client/file)

                                    //Create new destination folder within GTH folder on server
                                    System.IO.Directory.CreateDirectory(destinationFileFolderPath);

                                    //Add filename to destination path
                                    destinationFilePath = System.IO.Path.Combine(destinationFileFolderPath, System.IO.Path.GetFileName(sourceFilePath));

                                    //Copy file
                                    System.IO.File.Copy(sourceFilePath, destinationFilePath, true);
                                /*}
                                else
                                {
                                    // The connection has failed. Use the LastError to get the system error code
                                    throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { Message = "Could not find GTH NBB Todo folder. " + unc.LastError.ToString() + "." }); 
                                }*/
                            }

                            break;
                        case 6: //Rejected by partner;

                            //Lock Bundle, Generate draft pdf & save in repository
                            bundleRep.LockBundle(ufsdc.FileId, cfspdc.BundleId);
                            bundleRep.setDraft(cfspdc.BundleId, true);

                            //Save in repository
                            bundle = bundleRep.GetBundle(cfspdc.BundleId);
                            bundle.RectoVerso = true;
                            filepath = iTextRep.GenerateBundle(cfspdc.BundleId);

                            rolesToNotify.Add("Manager");
                            rolesToNotify.Add("Director");
                            rolesToCC.Add("Staff");

                            mailSubject = client.Name + ": NBB bundle per " + fileEnddate + "  rejected by EP";
                            sNl = "De NBB bundel per " + fileEnddate + " van " + client.Name + " dient aangepast te worden conform de review notes van de Engagement Partner.<br>De NBB bundel en de review notes zijn beschikbaar in eBook onder File NBB";
                            sFr = "La liasse de NBB au " + fileEnddate + " de " + client.Name + " doit &ecirc;tre adapt&eacute;e sur base des review notes de l&rsquo;Engagement Partner.<br>Les liasses et les review notes sont disponibles en eBook sous File NBB.";
                            break;
                        case 7: //Picked up by GTH
                            break;
                        case 8: //Rejected by GTH
                            rolesToNotify.Add("Staff");
                            rolesToNotify.Add("Manager");
                            rolesToNotify.Add("Director");
                            rolesToNotify.Add("Partner");
                            mailSubject = client.Name + ": Statutory accounts per " + fileEnddate + " rejected by GTH";
                            sNl = "De neerlegging van de NBB bundel per " + fileEnddate + " van " + client.Name + " werd geweigerd door GTH. Zie de nota in eBook review notes voor een eventuele verklaring.";
                            sFr = "La liasse de NBB au " + fileEnddate + " de " + client.Name + " a été refusée par GTH. Voir la note en eBook review notes pour une explication possible.";
                            break;
                        case 9: //Filed by GTH
                            rolesToNotify.Add("Staff");
                            rolesToNotify.Add("Manager");
                            rolesToNotify.Add("Director");
                            rolesToNotify.Add("Partner");
                            mailSubject = client.Name + ": Statutory accounts per " + fileEnddate + " filed with NBB";
                            sNl = "De jaarrekening per " + fileEnddate + " van " + client.Name + " werd neergelegd bij de Nationale Bank van België.";
                            sFr = "Les comptes annuels cl&ocirc;tur&eacute;s au " + fileEnddate + " de " + client.Name + " ont &eacute;t&eacute; d&eacute;pos&eacute;s &agrave; la Banque Nationale de Belgique.";

                            using (new Impersonator(Config.AppuserUsername, Config.AppuserDomain, Config.AppuserPwd))
                            {
                                sourceClientFolderPath = System.IO.Path.Combine(Config.NBBTodoDir, clientName + "-" + client.Id); //first subdir in TODO (client)
                                sourceFileFolderPath = System.IO.Path.Combine(sourceClientFolderPath, fileName + "-" + file.Id); //second subdier in TODO (client/file)
                                destinationClientFolderPath = System.IO.Path.Combine(Config.NBBDoneDir, clientName + "-" + client.Id);

                                if (System.IO.Directory.Exists(sourceFileFolderPath))
                                {
                                    System.IO.DirectoryInfo sourceFileFolderDir = new System.IO.DirectoryInfo(sourceFileFolderPath);

                                    if (!System.IO.Directory.Exists(destinationClientFolderPath))
                                    {
                                        System.IO.Directory.CreateDirectory(destinationClientFolderPath); // create client dir in DONE
                                    }

                                    sourceFileFolderDir.MoveTo(destinationClientFolderPath); //move file folder from TODO to DONE
                                }
                            }

                            break;
                        case 10: //Returned to pool by GTH
                            break;
                        case 11: //Invoice Uploaded by GTH
                            rolesToNotify.Add("Staff");
                            rolesToNotify.Add("Manager");
                            rolesToNotify.Add("Director");
                            rolesToNotify.Add("Partner");
                            mailSubject = client.Name + ": Statutory accounts invoice per " + fileEnddate + " uploaded by GTH";
                            sNl = "De factuur voor jaarrekening per " + fileEnddate + " van " + client.Name + " werd door GTH opgehaald en toegevoegd in eBook. Deze factuur wordt door eBook automatisch verstuurd naar finance.";
                            sFr = "La facture pour les compte annuels cl&ocirc;tur&eacute;s au " + fileEnddate + " de " + client.Name + " a &eacute;t&eacute; repris par GTH et a ajout&eacute; dans eBook. Cette facture est envoy&eacute;e par eBook automatiquement &agrave; financer.";

                            cidc.Id = ufsdc.FileId;
                            aadc = annualAccountRepository.GetAnnualAccounts(cidc); //Get annual account record based on file id
                            ecdc = annualAccountRepository.GetEngagementCode(aadc.EngagementCode); //Get Engagement code info based on client id

                            if (ecdc == null)
                            {
                                throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { Message = "Could not retrieve Engagement code info. Engagement code filled in and active?" });
                            }

                            if (!aadc.InvoiceRepositoryItemId.HasValue)
                            {
                                throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { Message = "Invoice repository item not found. Please reject and verify if the Invoice file is present within the folder." });
                            }

                            invoiceFilePath = repository.GetFilePath((Guid)aadc.InvoiceRepositoryItemId); //Get filepath of NBB invoice
                            
                            if (!System.IO.File.Exists(invoiceFilePath))
                            {
                                throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { Message = "Invoice PDF not found at specified path. Please reject and verify if the Invoice file is present within the folder." });
                            }

                            Guid mergedFilePhysicalFileId = Guid.NewGuid();
                            string mergedFilePhysicalFileName = String.Concat(mergedFilePhysicalFileId.ToString() + System.IO.Path.GetExtension(invoiceFilePath));
                            string mergedFilePhysicalFilePath = System.IO.Path.Combine(System.IO.Path.GetDirectoryName(invoiceFilePath), mergedFilePhysicalFileName);
                            string invoiceId = String.Format("{0}-{1}", DateTime.Now.ToString("yyyyMMdd"), new String(aadc.InvoiceStructuredMessage.ToCharArray().Where(c => Char.IsDigit(c)).ToArray())); //date + structured message
                            string nbbInvoiceCoverFilePath = iTextRep.GetNbbInfoPage(invoiceId, aadc, ecdc); //Generate a new NBB Invoice info page (Requested by finance, to be addded to invoice pdf)
                            
                            PdfMerge.MergeFiles(mergedFilePhysicalFilePath,new string[]{nbbInvoiceCoverFilePath.Replace(Config.PickupVirtualDir, Config.PickupLocal),invoiceFilePath},new string[]{"infoPage","invoice"}); //Merge info and invoice and save it to invoice location

                            if (System.IO.File.Exists(mergedFilePhysicalFilePath)) //if merged file exists
                            {
                                repository.UpdateFilePathRepositoryItem((Guid)aadc.InvoiceRepositoryItemId, mergedFilePhysicalFileId); //replace physical id
                                System.IO.File.Delete(invoiceFilePath); //remove previous file

                                using (new Impersonator(Config.AppuserUsername, Config.AppuserDomain, Config.AppuserPwd))
                                {
                                    var financeToFileName = System.IO.Path.Combine(Config.NBBFinanceTo, String.Concat(invoiceId,System.IO.Path.GetExtension(mergedFilePhysicalFilePath)));
                                    System.IO.File.Copy(mergedFilePhysicalFilePath, financeToFileName,true); //Copy file to finance "to" pool and use the generated invoiceId as filename

                                    if (!System.IO.File.Exists(financeToFileName)) //verify finance to pool contains file
                                    {
                                        throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { Message = "File Could not be written to Finance pool." });
                                    }
                                }
                            }
                            //CreateNew bundle using the info provided

                            //Send an e-mail to finance, including the invoice as attachment
                            //CoreHelper.SendMail(null, new string[] { "timothy.haerinck@23seconds.be" }, null, "NBB Filing invoice Attachment for customer " + client.Name + " per " + fileEnddate, "URGENT: NBB Filing invoice " + client.Name + " per " + fileEnddate, true, mailAttachmentFilePath);

                            //Get filepath
                            //string invoiceFilePath = repository.GetFilePath((Guid)aadc.InvoiceRepositoryItemId);

                            break;
                        case 12: //Invoice paid by Finance
                            break;
                        case 13: //Refiling requested by NBB Admin
                            break;
                        default:
                            //do nothing
                            break;
                    }
                }
            }

            //The changes list defines the actions and is here built in a certain order
            if (!string.IsNullOrEmpty(ufsdc.Comment))
            {
                changes.Add("comment");
                comment = ufsdc.Comment;
            }

            if (ufsdc.PdfAttachment.HasValue)
            {
                changes.Add("pdfAttachment");
            }

            if (ufsdc.Status != null)
            {
                changes.Add("status");
                ServiceStatuses status = eBookWriteManager.Context.ServiceStatuses.FirstOrDefault(d => d.Id == ufsdc.Status);
                fs.Status = status.Id;
            }

            if (ufsdc.RepositoryItem != null)
            {
                changes.Add("file");
            }

            if (ufsdc.Locked == true)
            {
                changes.Add("bundleLocked");
            }
            else if (ufsdc.Locked == false)
            {
                changes.Add("bundleOpen");
            }

            //Person and action required
            if ((ufsdc.ActivePerson != null || !string.IsNullOrEmpty(ufsdc.EncodedActivePerson)) && changes.Count > 0)
            {
                //save
                eBookWriteManager.Context.SaveChanges();

                string seperator = ",";
                string actions = String.Join(seperator, changes.ToArray());
                //create log entry
                CreateFileServiceLogEntry(fs, actions, comment, ufsdc.ActivePerson.Id, ufsdc.RepositoryItem, ufsdc.PdfAttachment);

                CriteriaFileServiceDataContract cfsdc = new CriteriaFileServiceDataContract
                {
                    FileId = ufsdc.FileId,
                    ServiceId = ufsdc.ServiceId,
                    ClientId = client.Id
                };

                FileServiceStatusDataContract fssdc = fileServiceRep.GetFileServiceStatus(cfsdc);

                //If the flow step has a message defined and  there are roles to notify, send mail
                if (rolesToNotify.Count > 0)
                {
                    //Gather persons and contact people
                    List<string> toMailAdresses = eBookReadManager.Context.PersonClientRoleSet.Include("Persons").Where(p => p.ClientId == file.ClientId && p.Persons.Department == "ACR").ToList().Where(p => rolesToNotify.Contains(p.Role)).Select(p => p.Persons.Email).Distinct<string>().ToList<string>();
                    List<string> toCCAdresses = eBookReadManager.Context.PersonClientRoleSet.Include("Persons").Where(p => p.ClientId == file.ClientId && p.Persons.Department == "ACR").ToList().Where(p => rolesToCC.Contains(p.Role)).Select(p => p.Persons.Email).Distinct<string>().ToList<string>();
                    //arrToMailAdresses[0] = "timothy.haerinck@23seconds.be";
                    //arrToMailAdresses = toMailAdresses.ToArray<string>();

                    if (!String.IsNullOrEmpty(fssdc.Status) && !String.IsNullOrEmpty(ufsdc.ActivePerson.Name))
                        footer = "<ul><li>Status changed to <b>" + fssdc.Status + "</b> by: <b>" + System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(ufsdc.ActivePerson.Name.ToLower()) + "</b>.</li></ul>";

                    if (!String.IsNullOrEmpty(comment))
                        reviewNotes = "<hr><h4>Review Notes:</h4>" + comment + "<hr>";

                    string[] mailItems = new string[4] { sNl, sFr, footer, reviewNotes };

                    foreach (string mailItem in mailItems)
                    {
                        if (!String.IsNullOrEmpty(mailItem))
                        {
                            body += "<p>" + mailItem + "</p>";
                        }
                    }

                    body = "<body>" + body + "</body>";
                    mailBody = "<html>" + header + body + "</html>";

                    CoreHelper.SendMail(null, toMailAdresses.ToArray<string>(), toCCAdresses.ToArray<string>(), mailBody, mailSubject, true, mailAttachmentFilePath);
                }

                return fssdc;
            }

            throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { Message = "Update of file service failed!" });
        }

        /// <summary>
        /// Reverts the File
        /// </summary>
        /// <param name="ufsdc"></param>
        public void RevertFileServiceStatus(UpdateFileServiceDataContract ufsdc)
        {

        }

        /// <summary>
        /// Add actions to the service log
        /// </summary>
        /// <param name="ufsdc">UpdateFileServiceDataContract</param>
        public void LogFileServiceActions(UpdateFileServiceDataContract ufsdc)
        {
            FileService fs = eBookWriteManager.Context.FileServices.FirstOrDefault(f => f.FileId == ufsdc.FileId && f.ServiceId == ufsdc.ServiceId);
            if (ufsdc.ActivePerson == null) ufsdc.ActivePerson = new BusinessHelper().DecodePerson(ufsdc.EncodedActivePerson);
            CreateFileServiceLogEntry(fs, ufsdc.Actions, null, ufsdc.ActivePerson.Id, ufsdc.RepositoryItem, ufsdc.PdfAttachment);
        }

        protected override void RegisterTranslators()
        {
        }
    }
}