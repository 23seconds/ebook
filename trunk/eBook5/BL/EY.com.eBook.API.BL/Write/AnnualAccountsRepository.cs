﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using EY.com.eBook.Core;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.Core.EF;
using EY.com.eBook.API.Contracts.Data.Logic;
using System.Data.Objects;
using EY.com.eBook.Core.EntityTranslation;
using EY.com.eBook.API.BL.Translators;
using System.ServiceModel;
using EY.com.eBook.Core.Data;
using EY.com.eBook.API.Contracts.Proxies;
using EY.com.eBook.API.Contracts.Data.Cache;
using EY.com.eBook.Core.EF.Write;
using System.Data;
using System.Transactions;

namespace EY.com.eBook.API.BL.Write
{
   
    public class AnnualAccountsRepository : RepositoryBase
    {

        /// <summary>
        /// Create an annual accounts record
        /// </summary>
        /// <param name="cfaadc">CriteriaFileAnnualAcountsDataContract</param>
        public bool CreateAnnualAccounts(CriteriaFileAnnualAcountsDataContract cfaadc)
        {
            FileAnnualAccount faa = new FileAnnualAccount
            {
                Id = Guid.NewGuid(),
                FileId = cfaadc.FileId
            };

            //Retrieve date AGM (not possible at the moment. PMT only holds future estimate as date. Although an XML specifies occurence)
                //eBookWriteManager.Context.AddToFileAnnualAccounts(faa);
                eBookWriteManager.Context.AddToFileAnnualAccounts(faa);
                eBookWriteManager.Context.SaveChanges();
            return true;
        }

        /// <summary>
        /// Updates the Annual Accounts data
        /// </summary>
        /// <param name="fs">cfaadc</param>
        public bool UpdateAnnualAccounts(CriteriaFileAnnualAcountsDataContract cfaadc)
        {
            FileAnnualAccount faa = eBookWriteManager.Context.FileAnnualAccounts.Where(a => a.FileId == cfaadc.FileId).FirstOrDefault();
            Int16? status = eBookReadManager.Context.FileServices.Where(s => s.FileId == faa.FileId && s.ServiceId == new Guid("1abf0836-7d7b-48e7-9006-f03db97ac28b")).FirstOrDefault().Status;
            Int16[] statusesTimeLimitUpdate = {1,2,3,4,5,6,7,8,10,13}; //This should be done differently, but no time

            if(cfaadc.DefinitiveGeneralAssembly.HasValue) faa.DefinitiveGeneralAssembly = cfaadc.DefinitiveGeneralAssembly;
            if (!String.IsNullOrEmpty(cfaadc.CompanyType)) faa.CompanyType = cfaadc.CompanyType.GetValueOrDefault();
            if (!String.IsNullOrEmpty(cfaadc.Model)) faa.Model = cfaadc.Model.GetValueOrDefault();
            if (cfaadc.ChangesAfterGeneralAssembly.HasValue) faa.ChangesAfterGeneralAssembly = (bool)cfaadc.ChangesAfterGeneralAssembly;
            if (!String.IsNullOrEmpty(cfaadc.ChangesAfterGeneralAssemblyDescription)) faa.ChangesAfterGeneralAssemblyDescription = cfaadc.ChangesAfterGeneralAssemblyDescription.GetValueOrDefault();
            if (!String.IsNullOrEmpty(cfaadc.EngagementCode)) faa.EngagementCode = cfaadc.EngagementCode.GetValueOrDefault();
            if (!String.IsNullOrEmpty(cfaadc.InvoiceLanguage)) faa.InvoiceLanguage = cfaadc.InvoiceLanguage.GetValueOrDefault();
            if (!String.IsNullOrEmpty(cfaadc.InvoiceRechargeFilingExpense)) faa.InvoiceRechargeFilingExpense = cfaadc.InvoiceRechargeFilingExpense;
            if (cfaadc.InvoiceAmount.HasValue) faa.InvoiceAmount = cfaadc.InvoiceAmount.GetValueOrDefault();
            if (!String.IsNullOrEmpty(cfaadc.InvoiceStructuredMessage)) faa.InvoiceStructuredMessage = cfaadc.InvoiceStructuredMessage;
            if (cfaadc.InvoiceRepositoryItemId.HasValue) faa.InvoiceRepositoryItemId = cfaadc.InvoiceRepositoryItemId;


            //Calculate Time Limit
            if (status != null)
            {
                if (faa.DefinitiveGeneralAssembly != null && statusesTimeLimitUpdate.Contains((Int16)status))
                {
                    DateTime fileEndDate = eBookWriteManager.Context.Files.Where(f => f.Id == cfaadc.FileId).Select(e => e.EndDate).FirstOrDefault();
                    DateTime now = DateTime.Now;
                    DateTime GAM = (DateTime)faa.DefinitiveGeneralAssembly;
                    string timeLimit = null;

                    if (fileEndDate != null && now != null)
                    {
                        if (now <= GAM.AddMonths(1) && now <= fileEndDate.AddMonths(7))
                        {
                            timeLimit = "Filing on time";
                        }
                        else if (now > GAM.AddMonths(1) || (now > fileEndDate.AddMonths(7) && now <= fileEndDate.AddMonths(8)))
                        {
                            timeLimit = "Filing before 9th month";
                        }
                        else if (now > fileEndDate.AddMonths(8) && now <= fileEndDate.AddMonths(9))
                        {
                            timeLimit = "Filing during 9th month following the end of the financial year";
                        }
                        else if (now > fileEndDate.AddMonths(9) && now <= fileEndDate.AddMonths(12))
                        {
                            timeLimit = "Filing between 10th and 12th month following the end of the financial year";
                        }
                        else if (now > fileEndDate.AddMonths(12))
                        {
                            timeLimit = "Filing as from 13th month following the end of the financial year";
                        }
                    }

                    faa.TimeLimit = timeLimit;
                }
            }

            //An active engagement code is set by the partner, but might become inactive in the process afterwards
            //If so, set new engagement code and log the change
            if (!string.IsNullOrEmpty(faa.EngagementCode))
            {
                if (eBookReadManager.Context.EngagementCodePerCustomers.Count(e => e.EngagementId == faa.EngagementCode) == 0) //still to be found in view
                {
                    //Update
                    var clientId = eBookWriteManager.Context.Files.Where(f => f.Id == cfaadc.FileId).Select(f => f.ClientId).FirstOrDefault();
                    var gfisCode = eBookWriteManager.Context.ClientSet.Where(c => c.Id == clientId).Select(c => c.GFISCode).FirstOrDefault();
                    faa.EngagementCode = eBookReadManager.Context.EngagementCodePerCustomers.Where(e => e.CustomerId == gfisCode).OrderByDescending(e => e.EngagementId).Select(e => e.EngagementId).FirstOrDefault();
                    //Log
                    FileServiceRepository fileServiceRepository = new FileServiceRepository();

                    ActivePersonDataContract activePersonDataContract = new ActivePersonDataContract()
                    {
                        Id = new Guid("00000000-0000-0000-0000-000000000000") //System
                    };

                    UpdateFileServiceDataContract ufsdc = new UpdateFileServiceDataContract()
                    {
                        FileId = faa.FileId,
                        ServiceId = new Guid("1abf0836-7d7b-48e7-9006-f03db97ac28b"),
                        Actions = "engagementCodeUpdate"
                    };
                    fileServiceRepository.LogFileServiceActions(ufsdc); //Log system action
                }
            }

            eBookWriteManager.Context.SaveChanges();
            return true;
        }

        /// <summary>
        /// Update repositoryItemId
        /// </summary>
        /// <param name="cfaadc"></param>
        /// <returns>cfaadc</returns>
        public bool UpdateRepositoryItem(CriteriaFileAnnualAcountsDataContract cfaadc)
        {
            FileAnnualAccount faa = eBookWriteManager.Context.FileAnnualAccounts.Where(a => a.FileId == cfaadc.FileId).FirstOrDefault();

            if (cfaadc.RepositoryItemId != null)
            {
                faa.RepositoryItemId = cfaadc.RepositoryItemId;
            }

            eBookWriteManager.Context.SaveChanges();
            return true;
        }

        protected override void RegisterTranslators()
        {

        }
    }
}
