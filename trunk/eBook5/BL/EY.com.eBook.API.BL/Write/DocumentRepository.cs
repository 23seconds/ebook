﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using EY.com.eBook.Core;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.Core.EF;
using EY.com.eBook.API.Contracts.Data.Logic;
using System.Data.Objects;
using EY.com.eBook.Core.EntityTranslation;
using EY.com.eBook.API.BL.Translators;
using System.ServiceModel;
using EY.com.eBook.Core.Data;
using EY.com.eBook.API.Contracts.Proxies;
using EY.com.eBook.API.Contracts.Data.Cache;
using EY.com.eBook.Core.EF.Write;
using System.Data;
using System.Transactions;

namespace EY.com.eBook.API.BL.Write
{
   
    public class DocumentRepository : RepositoryBase
    {
       
        public void SaveDocument(DocumentDataContract ddc)
        {
            if (ddc == null)
                throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { Message = "Criteria cannot be null!" });

            Document document = eBookWriteManager.Context.Documents.FirstOrDefault(d => d.Id == ddc.Id);
            if (document == null)
            {
                document = new Document
                    {
                        Id = ddc.Id,
                        FileId = ddc.FileId
                        , Culture = ddc.Culture
                        ,
                        TypeId = ddc.DocumentTypeId
                    };
            }
            document.Name = ddc.Name;
            document.BlockContainerData = null;

            if (ddc.BlockContainerData != null)
            {
                if (ddc.BlockContainerData.Count > 0)
                {
                    document.BlockContainerData = Core.CoreHelper.SerializeToString(ddc.BlockContainerData);
                }
            }

            if (document.EntityState == EntityState.Detached)
            {
                eBookWriteManager.Context.AddToDocuments(document);

            }
            else
            {
                eBookWriteManager.Context.DocumentFieldSet.Where(d => d.DocumentId == ddc.Id).MarkAllObjectsForDeletion();
            }
            eBookWriteManager.Context.SaveChanges();



            foreach (DocumentFieldDataContract dfdc in ddc.Fields)
            {
                DocumentField df = new DocumentField
                {
                    DocumentId = ddc.Id
                    ,
                    DataType = dfdc.DataType
                    ,
                    Name = dfdc.Name

                };
                SetDocumentField(ref df, dfdc);
                eBookWriteManager.Context.AddToDocumentFieldSet(df);
                eBookWriteManager.Context.SaveChanges();
            }

            ddc.ActivePerson = new BusinessHelper().DecodePerson(ddc.EncodedActivePerson);
            eBookWriteManager.Context.LogFileAction(ddc.ActivePerson.Id, ddc.FileId, "SAVED DOCUMENT", string.Format("{0} {1}", ddc.Culture, ddc.Name));

        }

        public void SetDocumentField(ref DocumentField df, DocumentFieldDataContract value)
        {
            if (value.Value != null)
            {
                switch (value.DataType.ToUpper())
                {
                    case "DATETIME":
                        value.Value = DateTime.Parse(value.Value.ToString());
                        break;
                    case "INT":
                    case "INT32":
                        value.Value = int.Parse(value.Value.ToString());
                        break;
                    case "FLOAT":
                        value.Value = float.Parse(value.Value.ToString());
                        break;
                    case "DECIMAL":
                        value.Value = decimal.Parse(value.Value.ToString());
                        break;
                    case "BOOL":
                        value.Value = bool.Parse(value.Value.ToString());
                        break;
                    default:
                        value.Value = value.Value.ToString();
                        break;
                }

                df.DataType = value.Value.GetType().Name;
                df.Value = Core.CoreHelper.SerializeToString(value.Value);
            }
        }

        public void DeleteDocument(CriteriaIdDataContract cidc)
        {
            Document doc = eBookWriteManager.Context.Documents.FirstOrDefault(d => d.Id == cidc.Id);
            if (doc!=null)
            {
                eBookWriteManager.Context.DeleteObject(doc);
                eBookWriteManager.Context.SaveChanges();
            }
        }

        protected override void RegisterTranslators()
        {
            
        }
    }
}
