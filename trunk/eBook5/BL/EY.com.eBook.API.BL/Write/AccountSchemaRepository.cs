﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using EY.com.eBook.Core;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.Core.EF;
using EY.com.eBook.API.Contracts.Data.Logic;
using System.Data.Objects;
using EY.com.eBook.Core.EntityTranslation;
using EY.com.eBook.API.BL.Translators;
using System.ServiceModel;
using EY.com.eBook.Core.Data;
using EY.com.eBook.API.Contracts.Proxies;
using EY.com.eBook.API.Contracts.Data.Cache;
using EY.com.eBook.Core.EF.Write;
using System.Data;
using System.Transactions;
using EY.com.eBook.API.Contracts.Services;

namespace EY.com.eBook.API.BL.Write
{
    public enum ImportPriority
    {
        SourceWins,
        TargetWins
    }

    public enum ImportSaldi
    {
        NoImport,
        AsStateAtStartCurrentPeriod,
        AsStateAtEndLastPeriod
    }

    public class AccountSchemaRepository : eBookRepository
    {
       


        // public void SaveTaxAdjustments(Guid fileId, string parentInternalNr, List<AdjustmentAccountDataContract> adjustments)
        // private void CopyMappings(Account sa, string targetAccountInternalNr)
        // public void CopyDescriptions(Account parent, string targetInternalNr)

        #region Accounts

        public bool SaveNewAccount(AccountDataContract adc)
        {
            Account account = eBookWriteManager.Context.Accounts.FirstOrDefault(a => a.FileId == adc.FileId && a.InternalNr == adc.InternalNr);

            if (account != null)
            {
                throw new FaultException<DefaultFaultContract>(new DefaultFaultContract
                {
                    Message = "Account already exists",
                    ErrorCode = 1200
                });
            }
            
                account = new Account
                {
                    FileId = adc.FileId
                    ,
                    InternalNr = adc.InternalNr
                    ,
                    ExistedLastYear = false
                    ,
                    ImportedID = null
                    ,
                    HasTaxAdjustments = false
                    ,
                    IsTaxAdjustment = false
                    ,
                    Saldo = 0
                    ,
                    PreviousSaldo = 0
                    ,
                    StartSaldo = 0
                    ,
                    VisualNr = adc.VisualNr
                    ,
                    LastChanged = DateTime.Now
                };
                if (account.EntityState == System.Data.EntityState.Detached)
                    eBookWriteManager.Context.AddToAccounts(account);
                eBookWriteManager.Context.SaveChanges();

                if (adc.Descriptions == null) adc.Descriptions = new List<AccountDescriptionDataContract>();
                if (adc.Descriptions.Count == 0 && !string.IsNullOrEmpty(adc.DefaultDescription))
                {
                    adc.Descriptions.Add(new AccountDescriptionDataContract
                    {
                        changed = true
                        ,
                        Culture = adc.Culture
                        ,
                        FileId = adc.FileId
                        ,
                        InternalNr = adc.InternalNr
                        ,
                        Description = adc.DefaultDescription
                    });
                }
                if (adc.Mappings == null) adc.Mappings = new List<AccountMappingDataContract>();

                // save descriptions
                foreach (AccountDescriptionDataContract addc in adc.Descriptions)
                {
                    AccountDescription ad = new AccountDescription
                    {
                        FileId = adc.FileId
                        ,
                        InternalNr = adc.InternalNr
                        ,
                        Culture = addc.Culture
                        ,
                        HasChanged = true
                        ,
                        Description = addc.Description
                    };
                    if (ad.EntityState == System.Data.EntityState.Detached)
                        eBookWriteManager.Context.AddToAccountDescriptions(ad);
                    eBookWriteManager.Context.SaveChanges();

                }

                ICacheService proxy;
                if (TryFetchICacheService(out proxy))
                {
                    proxy.AddAccounts(new List<AccountDataContract> { adc });
                }
                return true;
        }

        private bool InternalSaveAccount(AccountDataContract adc)
        {
            bool isnew = false;
            Account account = eBookWriteManager.Context.Accounts.FirstOrDefault(a => a.FileId == adc.FileId && a.InternalNr == adc.InternalNr);
            if(account == null && adc.IsTaxAdjusment) {
                account = new Account {
                    FileId = adc.FileId
                    , ExistedLastYear=false
                    , ImportedID=null
                    , HasTaxAdjustments = false
                    , InternalNr = adc.InternalNr
                    , IsTaxAdjustment =true
                    , LastChanged=DateTime.Now
                };
                isnew = true;
            }
            if (account == null)
            {
                throw new FaultException<DefaultFaultContract>(new DefaultFaultContract
                {
                    Message = "Account doesn't exists",
                    ErrorCode = 1200
                });
            }
            
                if (adc.Adjustments == null) adc.Adjustments = new List<AccountDataContract>();
                account.HasTaxAdjustments = adc.Adjustments.Count > 0;
                account.VisualNr = adc.VisualNr;
                account.LastChanged = DateTime.Now;
                if(adc.IsTaxAdjusment) {
                    account.PreviousSaldo = adc.PreviousSaldo;
                    account.StartSaldo = 0;
                    account.Saldo = adc.Saldo;
                   
                }
                if (account.EntityState == System.Data.EntityState.Detached)
                    eBookWriteManager.Context.AddToAccounts(account);
                eBookWriteManager.Context.SaveChanges();

                if (isnew && adc.IsTaxAdjusment)
                {
                    string parent = adc.InternalNr.Split(new char[] { '.' })[0];
                    foreach (AccountMapping mapping in eBookWriteManager.Context.AccountMappings.Where(a => a.InternalNr == parent && a.FileId == adc.FileId).ToList())
                    {
                        AccountMapping newmap = new AccountMapping
                        {
                            FileId = adc.FileId
                            ,
                            InternalNr = adc.InternalNr
                            ,
                            Key = mapping.Key
                            ,
                            MappingItemId = mapping.MappingItemId
                            ,
                            Meta = mapping.Meta
                            ,
                            PreviousMappingItemId = null
                        };
                        if (newmap.EntityState == System.Data.EntityState.Detached)
                            eBookWriteManager.Context.AddToAccountMappings(newmap);
                        eBookWriteManager.Context.SaveChanges();
                    }
                }

                List<string> cultures = new List<string>();
                foreach (AccountDescription ad in eBookWriteManager.Context.AccountDescriptions.Where(ad => ad.InternalNr == adc.InternalNr && ad.FileId == adc.FileId).ToList())
                {
                    cultures.Add(ad.Culture);
                    AccountDescriptionDataContract addc = adc.Descriptions.FirstOrDefault(d => d.Culture == ad.Culture);
                    if (addc != null)
                    {
                        if (ad.Description != addc.Description)
                        {
                            ad.Description = addc.Description;
                            ad.HasChanged = true;
                            eBookWriteManager.Context.SaveChanges();
                        }
                    }
                    else
                    {
                        eBookWriteManager.Context.DeleteObject(ad);
                        eBookWriteManager.Context.SaveChanges();
                    }

                }
                foreach (AccountDescriptionDataContract addc in adc.Descriptions.Where(d => !cultures.Contains(d.Culture)).ToList())
                {
                    // NEW
                    AccountDescription ad = new AccountDescription
                    {
                        FileId = adc.FileId
                        ,
                        Culture = addc.Culture
                        ,
                        Description = addc.Description
                        ,
                        HasChanged = true
                        ,
                        InternalNr = adc.InternalNr
                    };
                    if (ad.EntityState == System.Data.EntityState.Detached)
                        eBookWriteManager.Context.AddToAccountDescriptions(ad);
                    eBookWriteManager.Context.SaveChanges();

                }
            return true;
        }

        public bool SaveAccount(AccountDataContract adc)
        {
            
                bool result = InternalSaveAccount(adc);
                if (result)
                {
                    List<string> adjusts = adc.Adjustments.Select(a => a.InternalNr).ToList();
                    string searchNr = adc.InternalNr + ".";

                    // CLEANUP ALL EXISTING ADJUSTMENTS THAT NO LONGER EXIST.
                    foreach (Account existingAdj in eBookWriteManager.Context.Accounts.Where(a => a.FileId == adc.FileId && a.InternalNr.StartsWith(searchNr)).ToList())
                    {
                        if (adjusts.Contains(existingAdj.InternalNr)) continue;
                        var adj = existingAdj;
                        eBookWriteManager.Context.AccountDescriptions
                                       .Where(d => d.FileId == adc.FileId &&
                                                   d.InternalNr == adj.InternalNr)
                                       .MarkAllObjectsForDeletion();
                        eBookWriteManager.Context.AccountMappings
                                      .Where(d => d.FileId == adc.FileId &&
                                                  d.InternalNr == adj.InternalNr)
                                      .MarkAllObjectsForDeletion();
                        eBookWriteManager.Context.DeleteObject(existingAdj);
                    } 
                    eBookWriteManager.Context.SaveChanges();

                    // ADD/UPDATE ALL ADJUSTMENTS
                    foreach (AccountDataContract adcj in adc.Adjustments)
                    {
                        adcj.IsTaxAdjusment = true;
                        result = result && InternalSaveAccount(adcj);
                    }
                }

                ICacheService proxy;
                if (TryFetchICacheService(out proxy))
                {
                    proxy.ReloadOrAddFileInCacheForce(adc.FileId, true);
                }
                return result;
        }

        public AccountDataContract AddAccount(AccountDataContract adc)
        {
            if (eBookWriteManager.Context.Accounts
                                   .Any(a => a.FileId == adc.FileId &&
                                             a.InternalNr == adc.InternalNr))
            {
                return null;
            }
            var account = new Account
                {
                    FileId = adc.FileId
                    ,
                    InternalNr = adc.InternalNr
                    ,
                    ExistedLastYear = adc.ExistedLastYear
                    ,
                    ImportedID = adc.ImportedId
                    ,
                    HasTaxAdjustments = adc.HasTaxAdjustments
                    ,
                    IsTaxAdjustment = adc.IsTaxAdjusment
                    ,
                    Saldo = 0
                    ,
                    PreviousSaldo = 0
                    , StartSaldo =0
                    ,
                    VisualNr = adc.VisualNr
                    , LastChanged = DateTime.Now
                };
                eBookWriteManager.Context.AddToAccounts(account);

                if (adc.Descriptions == null) adc.Descriptions = new List<AccountDescriptionDataContract>();
                if (adc.Descriptions.Count == 0 && !string.IsNullOrEmpty(adc.DefaultDescription))
                {
                    adc.Descriptions.Add(new AccountDescriptionDataContract
                    {
                        changed = true
                        ,
                        Culture = adc.Culture
                        ,
                        FileId = adc.FileId
                        ,
                        InternalNr = adc.InternalNr
                        ,
                        Description = adc.DefaultDescription
                    });
                }
                if (adc.Mappings == null) adc.Mappings = new List<AccountMappingDataContract>();

                // save descriptions
                 foreach (var ad in adc.Descriptions.Select(addc => new AccountDescription
                {
                    FileId = adc.FileId,
                    InternalNr = adc.InternalNr,
                    Culture = addc.Culture,
                    HasChanged = true,
                    Description = addc.Description
                }))
            {
                eBookWriteManager.Context.AddToAccountDescriptions(ad);
            }

                foreach (var am in adc.Mappings.Select(amdc => new AccountMapping
                {
                    FileId = adc.FileId,
                    InternalNr = adc.InternalNr,
                    Key = amdc.MappingKey,
                    Meta = amdc.MappingMeta,
                    MappingItemId = amdc.MappingItemId,
                    PreviousMappingItemId = null
                }))
            {
                eBookWriteManager.Context.AddToAccountMappings(am);
            }

            eBookWriteManager.Context.SaveChanges();
            ICacheService proxy;
            if (TryFetchICacheService(out proxy))
            {
                proxy.AddAccounts(new List<AccountDataContract> { adc });
            }
            return adc;
        }

        public AccountDescriptionDataContract SaveAccountDescription(AccountDescriptionDataContract adc)
        {
            List<AccountDescriptionDataContract> adcs = new List<AccountDescriptionDataContract>();

            adc.changed = true;
            adcs.Add(adc);

            AccountDescription ad = eBookWriteManager.Context.AccountDescriptions
                                        .FirstOrDefault(a => a.FileId == adc.FileId
                                                            && a.InternalNr == adc.InternalNr
                                                            && a.Culture == adc.Culture);

            if (ad == null)
            {
                ad = new AccountDescription
                {
                    FileId = adc.FileId
                    ,
                    InternalNr = adc.InternalNr
                    ,
                    Culture = adc.Culture
                    ,
                    HasChanged = true
                    ,
                    Description = adc.Description
                };
                if (ad.EntityState == System.Data.EntityState.Detached)
                    eBookWriteManager.Context.AddToAccountDescriptions(ad);
                eBookWriteManager.Context.SaveChanges();

                ICacheService proxy;
                if (TryFetchICacheService(out proxy))
                {
                    proxy.AddAccountDescriptions(adcs);
                }
            }
            else
            {
                ad.Description = adc.Description;
                ad.HasChanged = true;
                eBookWriteManager.Context.SaveChanges();
                ICacheService proxy;
                if (TryFetchICacheService(out proxy))
                {
                    proxy.UpdateAccountDescriptions(adcs);
                }
            }
            eBookWriteManager.Context.Accounts.First(a => a.FileId == adc.FileId && a.InternalNr == adc.InternalNr).LastChanged = DateTime.Now;
            eBookWriteManager.Context.SaveChanges();
            
            return adc;
            
        }

        public bool DeleteAccountDescription(AccountDescriptionDataContract adc)
        {
            AccountDescription ad = eBookWriteManager.Context.AccountDescriptions
                                        .FirstOrDefault(a => a.FileId == adc.FileId
                                                            && a.InternalNr == adc.InternalNr
                                                            && a.Culture == adc.Culture);
            if (ad == null) return false;
            eBookWriteManager.Context.DeleteObject(ad);
            eBookWriteManager.Context.SaveChanges();
            ICacheService proxy;
            if (TryFetchICacheService(out proxy))
            {
                proxy.RemoveAccountDescriptions(new List<AccountDescriptionDataContract> { adc });
            }
            eBookWriteManager.Context.Accounts.First(a => a.FileId == adc.FileId && a.InternalNr == adc.InternalNr).LastChanged = DateTime.Now;
            eBookWriteManager.Context.SaveChanges();
            return true;

        }

        // public AccountDataContract SaveAccount(AccountDataContract adc) NOT USED

        // public void DeleteAccount(CriteriaAccountDataContract cadc) NOT USED

        public void SaveTaxAdjustments(AccountDataContract account)
        {
            ICacheService proxy;
            if (account.Adjustments != null && account.Adjustments.Count > 0)
            {
                if (TryFetchICacheService(out proxy))
                {
                    account = proxy.GetAccount(account.FileId, account.InternalNr, true, true, account.Culture);
                }
                else
                {
                    account.Descriptions = new List<AccountDescriptionDataContract>();
                    account.Mappings = new List<AccountMappingDataContract>();
                    account.Descriptions.AddRange(eBookWriteManager
                                                      .Context.AccountDescriptions
                                                      .Where(d => d.InternalNr == account.InternalNr &&
                                                                  d.FileId == account.FileId)
                                                      .Select(ad => new AccountDescriptionDataContract
                                                      {
                                                          FileId = ad.FileId,
                                                          changed = ad.HasChanged,
                                                          Culture = ad.Culture,
                                                          Description = ad.Description,
                                                          InternalNr = ad.InternalNr
                                                      }));
                    account.Mappings.AddRange(eBookWriteManager
                                                  .Context.AccountMappings
                                                  .Where(d => d.InternalNr == account.InternalNr &&
                                                              d.FileId == account.FileId)
                                                  .Select(ad => new AccountMappingDataContract
                                                  {
                                                      FileId = ad.FileId,
                                                      InternalNr = ad.InternalNr,
                                                      MappingItemId = ad.MappingItemId,
                                                      MappingKey = ad.Key,
                                                      MappingMeta = ad.Meta,
                                                      PreviousMappingItemId = ad.PreviousMappingItemId
                                                  }));
                }
                var leftOver = account.Adjustments.FirstOrDefault(a => a.InternalNr.EndsWith(".999")) ??
                               new AccountDataContract
                               {
                                   Adjustments = new List<AccountDataContract>(),
                                   Culture = account.Culture,
                                   Descriptions = account.Descriptions
                                                         .Select(d => new AccountDescriptionDataContract
                                                         {
                                                             changed = true,
                                                             Culture = d.Culture,
                                                             Description = d.Description + " ADJ 999",
                                                             FileId = d.FileId,
                                                             InternalNr = d.InternalNr + ".999"
                                                         })
                                                         .ToList(),
                                   ExistedLastYear = false,
                                   FileId = account.FileId,
                                   ImportedId = null,
                                   HasTaxAdjustments = false,
                                   Mappings = account.Mappings
                                                     .Select(m => new AccountMappingDataContract
                                                     {
                                                         MappingItemId = m.MappingItemId,
                                                         MappingKey = m.MappingKey,
                                                         MappingMeta = m.MappingMeta,
                                                         PreviousMappingItemId = m.PreviousMappingItemId,
                                                         FileId = m.FileId,
                                                         InternalNr = m.InternalNr + ".999"
                                                     }).ToList(),
                                   PreviousSaldo = 0,
                                   Saldo = 0,
                                   StartSaldo = 0
                               };

                decimal prevRemaining = 0;
                decimal curRemaining = 0;
                int nr = 1;
                foreach (AccountDataContract adj in account.Adjustments.Where(a => !a.InternalNr.EndsWith(".999")))
                {
                    adj.IsTaxAdjusment = true;
                    if (!adj.InternalNr.Contains("."))
                    {
                        adj.InternalNr = account.InternalNr + nr.ToString().PadLeft(3, '0');
                        adj.VisualNr = account.VisualNr + nr.ToString().PadLeft(3, '0');
                    }
                    adj.LastChanged = DateTime.Now;
                    prevRemaining += adj.PreviousSaldo;
                    curRemaining += adj.Saldo;
                    nr++;
                }
                leftOver.PreviousSaldo = account.PreviousSaldo - prevRemaining;
                leftOver.Saldo = account.Saldo - curRemaining;
                leftOver.InternalNr = account.InternalNr + ".999";
                leftOver.VisualNr = account.VisualNr + ".999";
                leftOver.IsTaxAdjusment = true;
                leftOver.LastChanged = DateTime.Now;

                //SAVE TO DATABASE
                foreach (var adj in account.Adjustments)
                {
                    var wadj = eBookWriteManager
                        .Context.Accounts
                        .FirstOrDefault(d => d.InternalNr == adj.InternalNr &&
                                             d.FileId == adj.FileId);
                    if (wadj == null)
                    {
                        wadj = new Account { FileId = adj.FileId, InternalNr = adj.InternalNr };
                    }
                    wadj.ExistedLastYear = false;
                    wadj.HasTaxAdjustments = false;
                    wadj.ImportedID = null;
                    wadj.InternalNr = adj.InternalNr;
                    wadj.IsTaxAdjustment = true;
                    wadj.LastChanged = DateTime.Now;
                    wadj.PreviousSaldo = adj.PreviousSaldo;
                    wadj.StartSaldo = 0;
                    wadj.Saldo = adj.Saldo;
                    wadj.VisualNr = adj.VisualNr;
                    if (wadj.EntityState == EntityState.Detached)
                        eBookWriteManager.Context.AddToAccounts(wadj);

                    foreach (var addc in adj.Descriptions)
                    {
                        var wadd = eBookWriteManager
                                       .Context.AccountDescriptions
                                       .FirstOrDefault(d => d.FileId == wadj.FileId &&
                                                            d.InternalNr == wadj.InternalNr &&
                                                            d.Culture == addc.Culture) ??
                                   new AccountDescription
                                   {
                                       Culture = addc.Culture,
                                       FileId = wadj.FileId,
                                       InternalNr = wadj.InternalNr
                                   };
                        wadd.HasChanged = true;
                        wadd.Description = addc.Description;

                        if (wadd.EntityState == EntityState.Detached)
                            eBookWriteManager.Context.AddToAccountDescriptions(wadd);
                    }

                    foreach (var amdc in adj.Mappings)
                    {
                        var wam = eBookWriteManager
                                      .Context.AccountMappings
                                      .FirstOrDefault(d => d.FileId == wadj.FileId &&
                                                           d.InternalNr == wadj.InternalNr &&
                                                           d.Key == amdc.MappingKey) ??
                                  new AccountMapping
                                  {
                                      FileId = wadj.FileId,
                                      InternalNr = wadj.InternalNr,
                                      Key = amdc.MappingKey,
                                      Meta = amdc.MappingMeta,
                                      MappingItemId = amdc.MappingItemId,
                                      PreviousMappingItemId = null
                                  };
                        wam.MappingItemId = amdc.MappingItemId;
                        wam.Meta = amdc.MappingMeta;

                        if (wam.EntityState == EntityState.Detached)
                            eBookWriteManager.Context.AddToAccountMappings(wam);
                    }
                }

            }
            Account parent = eBookWriteManager.Context.Accounts.FirstOrDefault(p => p.InternalNr == account.InternalNr && p.FileId == account.FileId);
            if (parent != null)
            {
                parent.HasTaxAdjustments = account.Adjustments!= null && account.Adjustments.Count>0;
                
            }

            eBookWriteManager.Context.SaveChanges();

            if (TryFetchICacheService(out proxy))
            {
                proxy.ReloadOrAddFileInCacheForce(account.FileId, true);
            }
           
        }

        public AccountMappingDataContract SaveAccountMapping(AccountMappingDataContract amdc)
        {
            var adcs = new List<AccountMappingDataContract> { amdc };
            amdc.ActivePerson = new BusinessHelper().DecodePerson(amdc.EncodedActivePerson);
            var file = eBookWriteManager.Context.Files.First(x => x.Id == amdc.FileId);

            var mapping = eBookWriteManager
                              .Context.AccountMappings
                              .FirstOrDefault(a => a.FileId == amdc.FileId &&
                                                   a.InternalNr == amdc.InternalNr &&
                                                   a.Key == amdc.MappingKey) ??
                          new AccountMapping
                          {
                              FileId = amdc.FileId,
                              InternalNr = amdc.InternalNr,
                              Key = amdc.MappingKey,
                              PreviousMappingItemId = null
                          };
            
            mapping.Meta = amdc.MappingMeta;
            mapping.MappingItemId = amdc.MappingItemId;



            if (mapping.EntityState == EntityState.Detached)
            {
                eBookWriteManager.Context.AddToAccountMappings(mapping);
                eBookWriteManager.Context.LogFileAction(amdc.ActivePerson.Id, amdc.FileId, "ADDED ACCOUNT MAPPING",
                                                          string.Format("{0} - {1}", amdc.InternalNr, amdc.MappingItemId));
            }
            else
            {
                eBookWriteManager.Context.LogFileAction(amdc.ActivePerson.Id, amdc.FileId, "CHANGED ACCOUNT MAPPING",
                                                          string.Format("{0} - {1}", amdc.InternalNr, amdc.MappingItemId));
            }
            eBookWriteManager.Context.Accounts.First(a => a.FileId == amdc.FileId && a.InternalNr == amdc.InternalNr)
                               .LastChanged = DateTime.Now;
            eBookWriteManager.Context.SaveChanges();

            ICacheService proxy;
            if (TryFetchICacheService(out proxy))
            {
                proxy.AddAccountMappings(adcs);
            }

            return amdc;
        }

        public void DeleteAccountMapping(CriteriaAccountMappingDataContract camdc)
        {
            DeleteAccountMappingInternal(camdc);

            eBookWriteManager.Context.Accounts.First(a => a.FileId == camdc.FileId && a.InternalNr == camdc.InternalNr)
                               .LastChanged = DateTime.Now;

            camdc.ActivePerson = new BusinessHelper().DecodePerson(camdc.EncodedActivePerson);
            eBookWriteManager.Context.LogFileAction(camdc.ActivePerson.Id, camdc.FileId, "DELETED ACCOUNT MAPPING",
                                                      string.Format("{0} - {1}", camdc.InternalNr, camdc.MappingKey));
            eBookWriteManager.Context.SaveChanges();
        }

        private void DeleteAccountMappingInternal(CriteriaAccountMappingDataContract camdc)
        {
            var am = eBookWriteManager.Context.AccountMappings
                                        .FirstOrDefault(a => a.FileId == camdc.FileId
                                                             && a.InternalNr == camdc.InternalNr
                                                             && a.Key == camdc.MappingKey);

            if (am == null) return;
            


            var amdc = new AccountMappingDataContract
            {
                FileId = am.FileId,
                InternalNr = am.InternalNr,
                MappingKey = am.Key,
                MappingMeta = am.Meta,
                MappingItemId = am.MappingItemId,
                PreviousMappingItemId = am.PreviousMappingItemId
            };

            if (am.PreviousMappingItemId.HasValue)
            {
                am.Meta = null;
                am.MappingItemId = null;
                ICacheService proxy;
                if (TryFetchICacheService(out proxy))
                {
                    amdc.MappingMeta = am.Meta;
                    amdc.MappingItemId = am.MappingItemId;

                    var adcs = new List<AccountMappingDataContract> { amdc };
                    proxy.UpdateAccountMappings(adcs);
                }
            }
            else
            {
                eBookWriteManager.Context.DeleteObject(am);
                ICacheService proxy;
                if (TryFetchICacheService(out proxy))
                {
                    proxy.RemoveAccountMappings(new List<AccountMappingDataContract> { amdc });
                }
            }

            
        }

        #endregion

        #region Mass updates
        /// <summary>
        /// Import accountschema from different (previous) eBook file into current eBook file
        /// <para>!! DESTRUCTIVE !! This will delete all accounts, mappings, descriptions and bookings in the CURRENT (target) eBook file</para>
        /// <para>!! Only to be used upon file creation.</para>
        /// </summary>
        /// <param name="currentTargetFileId">Target eBook File ID</param>
        /// <param name="sourceFileId">Source eBook File ID</param>
        /// <param name="includePreviousSaldi">Import saldi as previous saldi or not</param>
        public void ImportPreviousFile(Guid currentTargetFileId, Guid sourceFileId, bool includePreviousSaldi)
        {

            // pure database action
            // ADD PROCEDURE usp_ImportPreviousYear

            ICacheService proxy;
            if (TryFetchICacheService(out proxy))
            {
                // only reloads if already cached before, shouldn't be the case.
                proxy.ReloadFileInCache(currentTargetFileId);
            }
        }

        /// <summary>
        /// Imports a list of accounts 'with saldo' as state at eBook start (start saldi)
        /// <para>Doesn't perform refresh in Cache automatically, to be used in combined import method.</para>
        /// <para>Forces recalculation of current saldi in database.</para>
        /// </summary>
        private void ImportThirdPartyAccountsWithSaldi(Guid targetFileId, List<AccountImportDataContract> accounts, bool startState, bool previousState )
        {
            // reset AccountStartSaldi
            if(startState) eBookWriteManager.Context.ExecuteSql(string.Format("UPDATE TPBOOK_FILE_ACCOUNTS SET Account_StartSaldo=0 WHERE Account_File_ID = '{0}'",targetFileId.ToString()));
            if (previousState) eBookWriteManager.Context.ExecuteSql(string.Format("UPDATE TPBOOK_FILE_ACCOUNTS SET Account_PreviousSaldo=0 WHERE Account_File_ID = '{0}'", targetFileId.ToString()));

            List<Account> existingAccounts = eBookWriteManager.Context.Accounts.Where(a => a.FileId == targetFileId).ToList();
            
            foreach (AccountImportDataContract aidc in accounts.Where(a=>a.Amount!=0))
            {
                string internalNr = aidc.AccountNr.Trim().PadRight(12, '0');
                Account account = existingAccounts.FirstOrDefault(a=>a.InternalNr==internalNr);
                
                if (account!=null)
                {
                    if(startState) account.StartSaldo = aidc.Amount;
                    if (previousState) account.PreviousSaldo = aidc.PreviousAmount;
                    if (startState || previousState) account.LastChanged = DateTime.Now;
                }
                else
                {
                    account = new Account
                    {
                        FileId = targetFileId
                        ,
                        InternalNr = internalNr
                        ,
                        ExistedLastYear = false
                        ,
                        HasTaxAdjustments = false
                        ,
                        IsTaxAdjustment = false
                        ,
                        ImportedID = aidc.AccountNr
                        ,
                        PreviousSaldo = previousState ? aidc.PreviousAmount : 0
                        ,
                        StartSaldo = startState ? aidc.Amount : 0
                        ,
                        Saldo = aidc.Amount
                        ,
                        VisualNr = aidc.AccountNr.Trim()
                        , LastChanged = DateTime.Now
                    };
                    if (account.EntityState == EntityState.Detached)
                        eBookWriteManager.Context.AddToAccounts(account);
                    
                    

                }
                
            }
            eBookWriteManager.Context.SaveChanges();
            // Update all current saldi if start state changed
            if(startState) eBookWriteManager.Context.UpdateFileAllAccountsSaldi(targetFileId);

        }

       //HERE
        private void ImportThirdPartyAccountDescriptions(Guid targetFileId, List<AccountDescriptionImportDataContract> accountDescriptions, bool sourcePriority)
        {
            List<string> existingDescriptionIds = eBookWriteManager.Context.AccountDescriptions
                                                        .Where(a => a.FileId == targetFileId)
                                                        .Select(a => a.InternalNr + a.Culture)
                                                        .ToList();
            if (!sourcePriority)
            {

                foreach (AccountDescriptionImportDataContract adidc in accountDescriptions.Where(a => existingDescriptionIds.Contains((a.AccountNr.Trim().PadRight(12, '0') + a.Culture))))
                {
                    AccountDescription ad = new AccountDescription
                    {
                        FileId = targetFileId
                        ,
                        InternalNr = adidc.AccountNr.Trim().PadRight(12, '0')
                        ,
                        Culture = adidc.Culture
                        ,
                        Description = adidc.Description
                        ,
                        HasChanged = false
                    };
                    if (ad.EntityState == EntityState.Detached)
                        eBookWriteManager.Context.AddToAccountDescriptions(ad);
                   
                }
            }
            else
            {
                foreach (AccountDescriptionImportDataContract adidc in accountDescriptions)
                {
                    AccountDescription ad = eBookWriteManager.Context.AccountDescriptions
                                                .FirstOrDefault(a => a.FileId == targetFileId
                                                                && a.InternalNr == adidc.AccountNr.Trim().PadRight(12, '0')
                                                                && a.Culture == adidc.Culture
                                                                );
                    if (ad == null)
                    {
                        ad = new AccountDescription
                        {
                            FileId = targetFileId
                            ,
                            InternalNr = adidc.AccountNr.Trim().PadRight(12, '0')
                            ,
                            Culture = adidc.Culture
                            ,
                            Description = adidc.Description
                            ,
                            HasChanged = false
                        };
                        if (ad.EntityState == EntityState.Detached)
                            eBookWriteManager.Context.AddToAccountDescriptions(ad);
                        
                    }
                    else
                    {
                        ad.Description = adidc.Description;
                       
                    }
                }
            }
            eBookWriteManager.Context.SaveChanges();
            
        }

        public void ImportThirdPartyAccounts(Guid targetFileId, List<AccountImportDataContract> accounts,List<AccountDescriptionImportDataContract> accountDescriptions,bool startState,bool previousState)
        {
            ImportThirdPartyAccountsWithSaldi(targetFileId, accounts,startState,previousState);

            ImportThirdPartyAccountDescriptions(targetFileId, accountDescriptions, false);
            
            //reload cache
            ICacheService proxy;
            if (TryFetchICacheService(out proxy))
            {
                // only reloads if already cached before, shouldn't be the case.
                proxy.ReloadFileInCache(targetFileId);
            }
        }

        #endregion

        #region Bookings - TO REVIEW

        public BookingDataContract SaveBooking(CriteriaSaveBookingDataContract csbdc)
        {
            if (csbdc == null)
                throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { Message = "Criteria cannot be null!" });

            if (csbdc.Lines.Count < 2)
                throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { Message = "At least 2 bookinglines are expected." });

            decimal debet = csbdc.Lines.Where(bl => bl.Debet.HasValue).Sum(bl => bl.Debet.Value);
            decimal credit = csbdc.Lines.Where(bl => bl.Credit.HasValue).Sum(bl => bl.Credit.Value);
            if (debet - credit != 0)
                throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { Message = string.Format("Debet {0} and credit {1} are not equal.", debet, credit) });

            IQueryable<Booking> bookings = eBookWriteManager.Context.Bookings.Where(b => b.FileId == csbdc.FileId);

            BookingSingleDataContract defaultBooking = csbdc.Lines.First();
            DateTime creationDate = DateTime.Now;
            if (defaultBooking.BookingId.HasValue && defaultBooking.BookingId.Value != Guid.Empty)
            {
                creationDate = defaultBooking.BookingCreationDate;
                DeleteBooking(defaultBooking.BookingId.Value);
            }
            else
            {
                defaultBooking.BookingId = Guid.NewGuid();
                
            }
            defaultBooking.BookingCreationDate = creationDate;
            if (!defaultBooking.Hidden.HasValue) defaultBooking.Hidden = false;

            BookingDataContract booking = new BookingDataContract
            {
                Id = defaultBooking.BookingId.Value
                ,
                FileId = csbdc.FileId
                ,
                Hidden = defaultBooking.Hidden.Value
                ,
                Description = defaultBooking.BookingDescription
                ,
                Deleted = false
                ,
                CreationDate = creationDate
                ,
                ExportCount = defaultBooking.ExportCount
                    ,
                WorksheetCollection = null
                    ,
                WorksheetRowId =null
                , WorksheetBookingKey = null
                , WorksheetTypeId = null
                ,
                BookingLines = new List<BookingLineDataContract>()
            };


            foreach (BookingSingleDataContract bsdc in csbdc.Lines)
            {
                booking.BookingLines.Add(new BookingLineDataContract
                {
                    Id = Guid.NewGuid()
                    ,
                    AccountNr = bsdc.Account
                    ,
                    BookingId = booking.Id
                    ,
                    Amount = (bsdc.Debet.HasValue ? bsdc.Debet.Value : 0) - (bsdc.Credit.HasValue ? bsdc.Credit.Value : 0)
                    , 
                    BusinessRelationId = bsdc.ClientSupplierId
                    ,
                    Comment = bsdc.Comment
                });
            }
            BookingDataContract bdc = SaveBooking(booking);
            csbdc.ActivePerson = new BusinessHelper().DecodePerson(csbdc.EncodedActivePerson);
            eBookWriteManager.Context.LogFileAction(csbdc.ActivePerson.Id, csbdc.FileId, "SAVED MANUAL BOOKING", "");
            return bdc;
        }

        /* SaveBookingLastYear TO BE REMOVED --> AKA Tax adjustments
        public BookingDataContract SaveBookingLastYear(CriteriaSaveBookingDataContract csbdc)
        {
            // TO BE REMOVED --> AKA Tax adjustments

            if (csbdc == null)
                throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { Message = "Criteria cannot be null!" });

            if (csbdc.Lines.Count < 2)
                throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { Message = "At least 2 bookinglines are expected." });

            decimal debet = csbdc.Lines.Where(bl => bl.Debet.HasValue).Sum(bl => bl.Debet.Value);
            decimal credit = csbdc.Lines.Where(bl => bl.Credit.HasValue).Sum(bl => bl.Credit.Value);
            if (debet - credit != 0)
                throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { Message = string.Format("Debet {0} and credit {1} are not equal.", debet, credit) });

            
            IQueryable<Booking> bookings = eBookWriteManager.Context.Bookings
                                              .Where(b=>b.FileId == csbdc.FileId
                                                        && b.JournalTypeId==-1);

            BookingSingleDataContract defaultBooking = csbdc.Lines.First();
            if (defaultBooking.BookingId.HasValue && defaultBooking.BookingId.Value != Guid.Empty)
            {
                DeleteBooking(defaultBooking.BookingId.Value);
            }
            else
            {
                defaultBooking.BookingNr = bookings.Min(b => b.Nr) - 1;
                defaultBooking.BookingId = Guid.NewGuid();
            }

            defaultBooking.BookingHidden = true;

            BookingDataContract booking = new BookingDataContract
            {
                Id = defaultBooking.BookingId.Value
                ,
                FileId = csbdc.FileId
                ,
                Hidden = defaultBooking.BookingHidden.Value
                ,
                Description = defaultBooking.BookingDescription
                ,
                JournalType = -1
                ,
                Deleted = false
                ,
                Imported = false
                ,
                Nr = defaultBooking.BookingNr.Value
                ,
                BookingTypeId = OPENING_BOOKING
                , WorksheetBookingKey =null
                , WorksheetTypeId=null
            };

            foreach (BookingSingleDataContract bsdc in csbdc.Lines)
            {

                booking.BookingLines.Add(new BookingLineDataContract
                {
                    Id = Guid.NewGuid()
                    ,
                    AccountNr = bsdc.InternalAccountNr.PadRight(12, '0')
                    ,
                    BookingId = booking.Id
                    ,
                    Amount = (bsdc.Debet.HasValue ? bsdc.Debet.Value : 0) - (bsdc.Credit.HasValue ? bsdc.Credit.Value : 0)
                });
            }
            BookingDataContract bdc = SaveBooking(booking);
            csbdc.ActivePerson = new BusinessHelper().DecodePerson(csbdc.EncodedActivePerson);
            eBookWriteManager.Context.LogFileAction(csbdc.ActivePerson.Id, csbdc.FileId, "SAVED MANUAL BOOKING LAST YEAR", "");
            return bdc;
        }
        */
        /// <summary>
        /// Saves the booking.
        /// </summary>
        /// <param name="bdc"></param>
        /// <returns></returns>
        public BookingDataContract SaveBooking(BookingDataContract bdc)
        {
            if (bdc == null)
                throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { Message = "Criteria cannot be null!" });

            if (bdc.Id == Guid.Empty) bdc.Id = Guid.NewGuid();

            Booking booking = eBookWriteManager.Context.Bookings.FirstOrDefault(b => b.Id == bdc.Id);

            List<string> accountNrsToUpdate = new List<string>();
            if (booking != null)
            {
                IQueryable<BookingLine> bls = eBookWriteManager.Context.BookingLines.Where(bl => bl.BookingId == bdc.Id);
                accountNrsToUpdate.AddRange(bls.Select(bl => bl.AccountNr).Distinct().ToList());
                bls.ToList().MarkAllObjectsForDeletion();
                eBookWriteManager.Context.SaveChanges();
            }
            else
            {
                booking = new Booking
                {
                    Id = bdc.Id
                    ,
                    FileId = bdc.FileId
                    ,
                    DelFlag = bdc.Deleted
                    ,
                    Description = bdc.Description
                    ,
                    HiddenFlag = bdc.Hidden
                    ,
                    CreationDate = bdc.CreationDate
                    ,
                    WorksheetBookingKey = bdc.WorksheetBookingKey
                    ,
                    WorksheetTypeId = bdc.WorksheetTypeId
                    , ExportCount = bdc.ExportCount
                    , WorksheetCollection = bdc.WorksheetCollection
                    , WorksheetRowId = bdc.WorksheetRowId
                };
               
            }

            if (booking.EntityState == EntityState.Detached)
                eBookWriteManager.Context.AddToBookings(booking);
            eBookWriteManager.Context.SaveChanges();

            if (bdc.BookingLines != null)
            {
                accountNrsToUpdate.AddRange(bdc.BookingLines.Where(bl => !accountNrsToUpdate.Contains(bl.AccountNr.Id)).Select(b => b.AccountNr.Id).Distinct().ToList());
                foreach (var bldc in bdc.BookingLines)
                {
                    bldc.BookingId = booking.Id;
                    bldc.Id = Guid.NewGuid();
                    BookingLine bookline = new BookingLine
                    {
                        Id = bldc.Id
                        ,
                        AccountNr = bldc.AccountNr.Id
                        ,
                        BookingId = booking.Id
                        ,
                        Amount = bldc.Amount
                        ,
                        BusinessRelationId = bldc.BusinessRelationId
                        ,
                        FileId = booking.FileId
                        , 
                        Comment = bldc.Comment
                    };
                    if (bookline.EntityState == EntityState.Detached)
                        eBookWriteManager.Context.AddToBookingLines(bookline);
                    

                }
                eBookWriteManager.Context.SaveChanges();
            }
            //   tx.Complete();
            //}

            eBookWriteManager.Context.UpdateFileAccountListSaldi(bdc.FileId,accountNrsToUpdate);
            eBookWriteManager.Context.AcceptAllChanges();
            ICacheService proxy;
            if (TryFetchICacheService(out proxy))
            {
                var asus = eBookWriteManager
                    .Context.Accounts
                    .Where(a => a.FileId == bdc.FileId)
                    .WhereIn(a => a.InternalNr, accountNrsToUpdate)
                    .Select(ac => new AccountSaldiUpdate
                    {
                        AccountNr = ac.InternalNr,
                        Saldo = ac.Saldo,
                        StartSaldo = ac.StartSaldo,
                        PreviousSaldo = ac.PreviousSaldo
                    })
                    .ToList();

                proxy.ResetAccountsSaldi(bdc.FileId, asus);
                proxy.UpdateBookings(bdc.FileId);
            }

            return bdc;
        }


       
        /// <summary>
        /// Deletes the booking.
        /// </summary>
        /// <param name="id">The id.</param>
        public void DeleteBooking(Guid id)
        {
            var booking = eBookWriteManager.Context.Bookings.FirstOrDefault(b => b.Id == id);
            if (booking == null)
                throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { Message = "Criteria cannot be null!" });

            Guid fileId = booking.FileId;
            IQueryable<BookingLine> qBookings = eBookWriteManager.Context.BookingLines.Where(b => b.BookingId == id);

            List<string> accountNrs = qBookings.Select(b => b.AccountNr).Distinct().ToList();


            using (var tx = new TransactionScope())
            {
                qBookings.MarkAllObjectsForDeletion();
                eBookWriteManager.Context.SaveChanges();

                eBookWriteManager.Context.DeleteObject(booking);
                eBookWriteManager.Context.SaveChanges();

                tx.Complete();
            }

            eBookWriteManager.Context.UpdateFileAccountListSaldi(fileId, accountNrs);
            eBookWriteManager.Context.AcceptAllChanges();
            ICacheService proxy;
            if (TryFetchICacheService(out proxy))
            {
                var asus = eBookWriteManager
                    .Context.Accounts
                    .Where(a => a.FileId == fileId)
                    .WhereIn(a => a.InternalNr, accountNrs)
                    .Select(ac => new AccountSaldiUpdate
                    {
                        AccountNr = ac.InternalNr,
                        Saldo = ac.Saldo,
                        StartSaldo = ac.StartSaldo,
                        PreviousSaldo = ac.PreviousSaldo
                    })
                    .ToList();

                proxy.ResetAccountsSaldi(fileId, asus);
                proxy.UpdateBookings(fileId);
            }
        }

        // public BookingDataContract SaveBooking(CriteriaSaveBookingDataContract csbdc)
        // ?? public BookingDataContract SaveBookingLastYear(CriteriaSaveBookingDataContract csbdc)
        // ?? public BookingDataContract SaveBooking(BookingDataContract bdc)
        // public void DeleteBooking(Guid id)
        //public void ImportAccounts(List<Account> accountsToImport)

        #endregion

        
        protected override void RegisterTranslators()
        {
            
        }
    }
}
