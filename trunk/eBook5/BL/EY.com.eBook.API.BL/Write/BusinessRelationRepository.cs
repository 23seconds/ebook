﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using EY.com.eBook.Core;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.Core.EF;
using EY.com.eBook.API.Contracts.Data.Logic;
using System.Data.Objects;
using EY.com.eBook.Core.EntityTranslation;
using EY.com.eBook.API.BL.Translators;
using System.ServiceModel;
using EY.com.eBook.Core.Data;
using EY.com.eBook.API.Contracts.Proxies;
using EY.com.eBook.API.Contracts.Data.Cache;
using EY.com.eBook.Core.EF.Write;
using System.Data;
using System.Transactions;

namespace EY.com.eBook.API.BL.Write
{

    public class BusinessRelationRepository : RepositoryBase
    {

        public BusinessRelationDataContract SaveBusinessRelationDataContract(BusinessRelationDataContract brdc)
        {
            brdc.Id = brdc.Id == Guid.Empty ? Guid.NewGuid() : brdc.Id;
            BusinessRelation br = eBookWriteManager.Context.BusinessRelations
                                    .FirstOrDefault(b => b.Id == brdc.Id);

            if (br == null)
            {
                br = new BusinessRelation
                {
                    Id = brdc.Id
                    ,
                    Address = brdc.Address
                    ,
                    City = brdc.City
                    ,
                    ClientId = brdc.ClientId
                    ,
                    CompanyNr = brdc.CompanyNr
                    ,
                    Country = brdc.Country
                    
                    ,
                    FirstName = brdc.FirstName
                    ,
                    ImportedId = brdc.ImportedId
                    ,
                    Name = brdc.LastName
                    ,
                    Type = brdc.BusinessRelationType.ToString()
                    ,
                    VatNumber = brdc.Vat
                    ,
                    Zip = brdc.ZipCode
                };
                if (br.EntityState == EntityState.Detached)
                    eBookWriteManager.Context.AddToBusinessRelations(br);
                eBookWriteManager.Context.SaveChanges();
            }
            else
            {
                br.Address = brdc.Address;
                
                br.City = brdc.City;
                br.ClientId = brdc.ClientId;
                br.CompanyNr = brdc.CompanyNr;
                br.Country = brdc.Country;
                
                br.FirstName = brdc.FirstName;
                br.ImportedId = brdc.ImportedId;
                br.Name = brdc.LastName;

                br.Type = brdc.BusinessRelationType.ToString();
                br.VatNumber = brdc.Vat;
                br.Zip = brdc.ZipCode;
                eBookWriteManager.Context.SaveChanges();
            }
            return brdc;
        }

        public void DeleteBusinessRelation(Guid businessRelationId)
        {
            BusinessRelation br = eBookWriteManager.Context.BusinessRelations.FirstOrDefault(b => b.Id == businessRelationId);
            if (br != null)
            {
                eBookWriteManager.Context.DeleteObject(br);
                eBookWriteManager.Context.SaveChanges();
            }
        }


        //public void ImportFromProacc(List<BusinessRelationDataContract> brs, Guid clientId, string type)
        //{
        //    type = type.ToUpper();
        //    eBookWriteManager.Context.ExecuteSql(string.Format("DELETE FROM TPBOOK_CLIENT_BUSINESS_RELATIONS where BUSINESS_RELATION_CLIENT_ID='{0}' and BUSINESS_RELATION_IMPORTED_ID is not null and BUSINESS_RELATION_IMPORTED_ID <> '' and BUSINESS_RELATION_TYPE='{1}'", clientId,type));
        //    foreach (BusinessRelationDataContract br in brs)
        //    {
        //        br.Id = Guid.NewGuid();
        //        br.ClientId = clientId;
        //        SaveBusinessRelationDataContract(br);
        //    }

        //}

        private void ImportCustomersProAcc(ProAccServiceProxy psp, Guid clientId, string server, string database)
        {
            List<BusinessRelationDataContract> custs = psp.ProAccGetCustomers(new EY.com.eBook.ProAcc.API.Contracts.Data.CriteriaProAccClientDataContract
            {
                ClientId = clientId
                ,
                Database = database
                ,
                Server = server
            });
            List<string> impIds = custs.Select(c => c.ImportedId).ToList();

            List<BusinessRelation> brs = eBookWriteManager.Context.BusinessRelations.Where(b => b.ClientId == clientId && b.Type == "C").ToList();

            brs.Where(b => !string.IsNullOrEmpty(b.ImportedId) && !impIds.Contains(b.ImportedId)).MarkAllObjectsForDeletion();
            eBookWriteManager.Context.SaveChanges();

            foreach (BusinessRelationDataContract brp in custs)
            {
                BusinessRelation br = brs.FirstOrDefault(b => b.ImportedId == brp.ImportedId);
                if (br == null)
                {
                    br = new BusinessRelation
                    {
                        ClientId = clientId
                        ,
                        Id = Guid.NewGuid()
                        ,
                        ImportedId = brp.ImportedId
                        ,
                        Type = "C"
                        ,
                        StartDate = DateTime.Now
                    };
                }
                br.Address = brp.Address;
                br.City = brp.City;
                br.CompanyNr = brp.CompanyNr;
                br.Country = brp.Country;
                br.FirstName = brp.FirstName;
                br.Name = brp.LastName;
                br.VatNumber = brp.Vat;
                br.Zip = brp.ZipCode;
                if (br.EntityState == EntityState.Detached)
                    eBookWriteManager.Context.AddToBusinessRelations(br);
                eBookWriteManager.Context.SaveChanges();
            }
        }

        private void ImportSuppliersProAcc(ProAccServiceProxy psp, Guid clientId, string server, string database)
        {
            List<BusinessRelationDataContract> custs = psp.ProAccGetSuppliers(new EY.com.eBook.ProAcc.API.Contracts.Data.CriteriaProAccClientDataContract
            {
                ClientId = clientId
                ,
                Database = database
                ,
                Server = server
            });
            List<string> impIds = custs.Select(c => c.ImportedId).ToList();

            List<BusinessRelation> brs = eBookWriteManager.Context.BusinessRelations.Where(b => b.ClientId == clientId && b.Type == "S").ToList();

            brs.Where(b => !string.IsNullOrEmpty(b.ImportedId) && !impIds.Contains(b.ImportedId)).MarkAllObjectsForDeletion();
            eBookWriteManager.Context.SaveChanges();

            foreach (BusinessRelationDataContract brp in custs)
            {
                BusinessRelation br = brs.FirstOrDefault(b => b.ImportedId == brp.ImportedId);
                if (br == null)
                {
                    br = new BusinessRelation
                    {
                        ClientId = clientId
                        ,
                        Id = Guid.NewGuid()
                        ,
                        ImportedId = brp.ImportedId
                        ,
                        Type = "S"
                        ,
                        StartDate = DateTime.Now
                    };
                }
                br.Address = brp.Address;
                br.City = brp.City;
                br.CompanyNr = brp.CompanyNr;
                br.Country = brp.Country;
                br.FirstName = brp.FirstName;
                br.Name = brp.LastName;
                br.VatNumber = brp.Vat;
                br.Zip = brp.ZipCode;
                if (br.EntityState == EntityState.Detached)
                    eBookWriteManager.Context.AddToBusinessRelations(br);
                eBookWriteManager.Context.SaveChanges();
            }
        }


        public void ImportBusinessRelationsProAcc(CriteriaIdAndCultureDataContract cidc)
        {
            Client client = eBookWriteManager.Context.ClientSet.First(c => c.Id == cidc.Id);
            if (string.IsNullOrEmpty(client.ProAccServer) || string.IsNullOrEmpty(client.ProAccDatabase))
            {
                throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { ErrorCode = -101, Message = "CLIENT HAS NO KNOWN PROACC CONNECTION." });
            }
            ProAccServiceProxy psp = new ProAccServiceProxy();
            psp.Open();
            ImportCustomersProAcc(psp, cidc.Id, client.ProAccServer, client.ProAccDatabase);
            ImportSuppliersProAcc(psp, cidc.Id, client.ProAccServer, client.ProAccDatabase);
            psp.Close();
        }

        public void ImportCustomersProAcc(CriteriaIdAndCultureDataContract cidc)
        {
            Client client = eBookWriteManager.Context.ClientSet.First(c => c.Id == cidc.Id);
            if (string.IsNullOrEmpty(client.ProAccServer) || string.IsNullOrEmpty(client.ProAccDatabase))
            {
                throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { ErrorCode = -101, Message = "CLIENT HAS NO KNOWN PROACC CONNECTION." });
            }
            ProAccServiceProxy psp = new ProAccServiceProxy();
            psp.Open();
            ImportCustomersProAcc(psp, cidc.Id, client.ProAccServer, client.ProAccDatabase);
            psp.Close();
        }

        public void ImportSuppliersProAcc(CriteriaIdAndCultureDataContract cidc)
        {
            Client client = eBookWriteManager.Context.ClientSet.First(c => c.Id == cidc.Id);
            if (string.IsNullOrEmpty(client.ProAccServer) || string.IsNullOrEmpty(client.ProAccDatabase))
            {
                throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { ErrorCode = -101, Message = "CLIENT HAS NO KNOWN PROACC CONNECTION." });
            }
            ProAccServiceProxy psp = new ProAccServiceProxy();
            psp.Open();
            ImportSuppliersProAcc(psp, cidc.Id, client.ProAccServer, client.ProAccDatabase);
            psp.Close();
        }


        protected override void RegisterTranslators()
        {
            
        }
    }
}
