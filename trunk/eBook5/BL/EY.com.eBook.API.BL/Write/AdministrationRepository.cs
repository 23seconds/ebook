﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using EY.com.eBook.Core;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.Core.EF;
using EY.com.eBook.API.Contracts.Data.Logic;
using System.Data.Objects;
using EY.com.eBook.Core.EntityTranslation;
using EY.com.eBook.API.BL.Translators;
using System.ServiceModel;
using EY.com.eBook.Core.Data;
using EY.com.eBook.API.Contracts.Proxies;
using EY.com.eBook.API.Contracts.Data.Cache;
using EY.com.eBook.Core.EF.Write;
using System.Data;
using System.Transactions;

namespace EY.com.eBook.API.BL.Write
{    
    public class AdministrationRepository : RepositoryBase
    {
        public Guid CreateCoefficient(CriteriaCoefficientDataContract ccdc)
        {
            if (ccdc == null)
                throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { Message = "Please select a Group/Key" });

            Guid coeffId = Guid.NewGuid();
            // Convert date GMT    
            
            DateTime? startDate = null;
            DateTime? endDate = null;

            if (ccdc.StartDate != null && ccdc.EndDate != null){
                startDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId((DateTime)ccdc.StartDate, "W. Europe Standard Time");
                endDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId((DateTime)ccdc.EndDate, "W. Europe Standard Time");
            }

            Coefficients coeff = new Coefficients
            {
                Id = coeffId,
                GroupId = ccdc.GroupId,
                Key = ccdc.Key,
                Description = ccdc.Description,
                AssessmentYear = ccdc.AssessmentYear,
                StartDate = startDate,
                EndDate = endDate,
                Value = ccdc.Value
            };
            eBookWriteManager.Context.AddToCoefficientsSet(coeff);
            eBookWriteManager.Context.SaveChanges();
            return coeff.Id;
        }

        public void DeleteCoefficient(CriteriaBaseIdDataContract cbidc ) 
        {
            var coefficient = eBookWriteManager.Context.CoefficientsSet.FirstOrDefault(c => c.Id == cbidc.Id);
            if (coefficient == null)
                throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { Message = String.Format("No coefficient found for id {0}", cbidc.Id) });

            eBookWriteManager.Context.DeleteObject(coefficient);
            eBookWriteManager.Context.SaveChanges();
        }

        public void UpdateCoefficient(CriteriaCoefficientDataContract ccdc)
        {
            if (ccdc == null)
                throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { Message = "Please select a Group/Key" });

            var coefficient = eBookWriteManager.Context.CoefficientsSet.FirstOrDefault(c => c.Id == ccdc.Id);

            DateTime? startDate = null;
            DateTime? endDate = null;

            if (ccdc.StartDate != null && ccdc.EndDate != null)
            {
                startDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId((DateTime)ccdc.StartDate, "W. Europe Standard Time");
                endDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId((DateTime)ccdc.EndDate, "W. Europe Standard Time");
            }

            coefficient.Description = ccdc.Description;
            // Convert date GMT  
            coefficient.StartDate = startDate;
            coefficient.EndDate = endDate;
            coefficient.AssessmentYear = ccdc.AssessmentYear;
            coefficient.Value = Convert.ToDecimal(ccdc.Value);
            coefficient.Value = Math.Round(coefficient.Value, 6);
            eBookWriteManager.Context.SaveChanges();            
        }

        public void InsertNewChampion(CriteriaChampionDataContract ccdc)
        {
            Champions c = new Champions { 
                ChampionPersonId = ccdc.PersonId,
                ChampionOfficeId = ccdc.OfficeId
            };
            eBookWriteManager.Context.AddToChampionsSet(c);            

            PersonRole pr = new PersonRole { 
                PersonId = ccdc.PersonId,
                Role = "CHAMPION",
                Manual = true
            };
            eBookWriteManager.Context.AddToPersonRoleSet(pr);

            eBookWriteManager.Context.SaveChanges();
        }

        public void DeleteChampion(CriteriaChampionDataContract ccdc)
        {
            Champions champ = eBookWriteManager.Context.ChampionsSet.FirstOrDefault(c => c.ChampionPersonId == ccdc.PersonId && c.ChampionOfficeId == ccdc.OfficeId);
            eBookWriteManager.Context.DeleteObject(champ);

            PersonRole personRole = eBookWriteManager.Context.PersonRoleSet.FirstOrDefault(pr => pr.PersonId == ccdc.PersonId && pr.Role == "CHAMPION");
            if (personRole != null)
            {
                eBookWriteManager.Context.DeleteObject(personRole);
            }

            eBookWriteManager.Context.SaveChanges();
        }

        public void InsertHelp(List<CriteriaWorksheetHelpDataContract> lcwhdc) {
            Guid guid = System.Guid.NewGuid();
            foreach (CriteriaWorksheetHelpDataContract cwhdc in lcwhdc) {
                WorksheetHelpNew w = new WorksheetHelpNew
                {
                    WorksheetHelpID = guid,
                    WorksheetTypeID = cwhdc.WorksheetTypeId,
                    Subject = cwhdc.Subject,
                    Body = cwhdc.Body,
                    Culture = cwhdc.Culture,
                    Order = cwhdc.Order
                };
                eBookWriteManager.Context.AddToWorksheetHelpNewSet(w);
            }
            eBookWriteManager.Context.SaveChanges();
        }

        public void UpdateHelp(List<CriteriaWorksheetHelpDataContract> lcwhdc) {
            foreach (CriteriaWorksheetHelpDataContract cwhdc in lcwhdc) {
                WorksheetHelpNew w = eBookWriteManager.Context.WorksheetHelpNewSet.FirstOrDefault(wo => wo.WorksheetHelpID == cwhdc.WorksheetHelpId && wo.WorksheetTypeID == cwhdc.WorksheetTypeId && wo.Culture == cwhdc.Culture);
                w.Subject = cwhdc.Subject;
                w.Body = cwhdc.Body;
                w.Order = cwhdc.Order;
                eBookWriteManager.Context.SaveChanges();
            }
        }

        public void DeleteHelp(CriteriaBaseIdDataContract cidc) {
            List<WorksheetHelpNew> lw = eBookWriteManager.Context.WorksheetHelpNewSet.Where(wo => wo.WorksheetHelpID == cidc.Id).ToList();
            foreach (WorksheetHelpNew w in lw)
            {
                eBookWriteManager.Context.DeleteObject(w);
            }
            eBookWriteManager.Context.SaveChanges();
        }

        public void UpdateHelpOrder(List<CriteriaIdOrderDataContract> lciodc) {
            foreach (CriteriaIdOrderDataContract ciodc in lciodc) {
                List<WorksheetHelpNew> lw = eBookWriteManager.Context.WorksheetHelpNewSet.Where(wo => wo.WorksheetHelpID == ciodc.Id).ToList();
                foreach (WorksheetHelpNew w in lw) {
                    w.Order = ciodc.Order;
                    eBookWriteManager.Context.SaveChanges();
                }
            }
        }

        protected override void RegisterTranslators()
        {

        }
    }
}
