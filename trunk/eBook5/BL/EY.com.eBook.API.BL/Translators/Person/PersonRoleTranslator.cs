﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EY.com.eBook.Core.EntityTranslation;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.Core.EF;
using EY.com.eBook.Core.EF.Read;

namespace EY.com.eBook.API.BL.Translators
{
    /// <summary>
    /// FileTranslator
    /// </summary>
    public class PersonRoleTranslator : EntityMapperTranslator<PersonRole, PersonRoleDataContract>
    {
        /// <summary>
        /// Translates the business object to the service object.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <param name="value">The value.</param>
        /// <param name="levelsToTranslate">The levels to translate.</param>
        /// <returns></returns>
        protected override PersonRoleDataContract BusinessToService(IEntityTranslatorService service, PersonRole value, TranslateLevel levelsToTranslate)
        {
            using (new InternalTranslators(service, new List<IEntityTranslator> { new  PersonTranslator()}))
            {
                var prdc = new PersonRoleDataContract
                {
                    PersonId = value.PersonId
                    ,Role = value.Role
                    ,Manual = value.Manual
                    ,LastUsed = value.LastUsed
                };
                
                return prdc;
            }
        }

        /// <summary>
        /// Translates the service object to the business object.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <param name="value">The value.</param>
        /// <param name="levelsToTranslate">The levels to translate.</param>
        /// <returns></returns>
        protected override PersonRole ServiceToBusiness(IEntityTranslatorService service, PersonRoleDataContract value, TranslateLevel levelsToTranslate)
        {
            using (new InternalTranslators(service, new List<IEntityTranslator> { }))
            {
                PersonRole pr = eBookReadManager.Context.PersonRoleSet.FirstOrDefault(ft => ft.PersonId == value.PersonId && ft.Role == value.Role);

                if (pr == null) pr = new PersonRole { PersonId = value.PersonId, Role = value.Role};
                pr.Manual = value.Manual;
                pr.LastUsed = value.LastUsed;
                return pr;
            }
        }
    }

    public class PersonClientRoleTranslator : EntityMapperTranslator<PersonClientRole, PersonRoleDataContract>
    {
        /// <summary>
        /// Translates the business object to the service object.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <param name="value">The value.</param>
        /// <param name="levelsToTranslate">The levels to translate.</param>
        /// <returns></returns>
        protected override PersonRoleDataContract BusinessToService(IEntityTranslatorService service, PersonClientRole value, TranslateLevel levelsToTranslate)
        {
            using (new InternalTranslators(service, new List<IEntityTranslator> { new PersonTranslator() }))
            {
                var prdc = new PersonRoleDataContract
                {
                    PersonId = value.PersonId
                    ,
                    Role = value.Role
                    ,
                    Manual = false
                };
                if (value.Persons != null && levelsToTranslate.TranslateRelations) prdc.Person = service.Translate<PersonDataContract>(value.Persons);

                return prdc;
            }
        }

        /// <summary>
        /// Translates the service object to the business object.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <param name="value">The value.</param>
        /// <param name="levelsToTranslate">The levels to translate.</param>
        /// <returns></returns>
        protected override PersonClientRole ServiceToBusiness(IEntityTranslatorService service, PersonRoleDataContract value, TranslateLevel levelsToTranslate)
        {
            throw new NotImplementedException();
        }
    }
}