﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EY.com.eBook.Core.EntityTranslation;
using EY.com.eBook.Core.EF;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.Core.EF.Read;

namespace EY.com.eBook.API.BL.Translators
{
    /// <summary>
    /// PersonTranslator
    /// </summary>
    public class PersonTranslator : EntityMapperTranslator<Person, PersonDataContract>
    {
        private const string CHAMPIONROLE = "CHAMPION";
        private const string ADMINROLE = "ADMIN";

        protected override PersonDataContract BusinessToService(IEntityTranslatorService service, Person value, TranslateLevel levelsToTranslate)
        {
            using (new InternalTranslators(service, new List<IEntityTranslator> { new OfficeTranslator() }))
            {
                var cdc = new PersonDataContract
                {
                     Id = value.Id
                     ,FirstName = value.FirstName
                     ,LastName = value.LastName
                     ,Email = value.Email
                     ,WindowsAccount = value.WindowsAccount
                     ,LastLogon = value.LastLogon
                     ,IsSysAdmin = value.Roles.Count(r=>r.Role.ToUpper()==ADMINROLE)>0
                     ,Department = value.Department
                     ,GPN = value.Gpn
                };

                if (!string.IsNullOrEmpty(value.DefaultOffice))
                {
                    Office defO = eBookReadManager.Context.OfficeSet.FirstOrDefault(o => o.Id == value.DefaultOffice);
                    if (defO != null) cdc.DefaultOffice = service.Translate<OfficeDataContract>(defO);
                }

                if (levelsToTranslate.TranslateRelations)
                {
                    if (value.Roles!=null && value.Roles.Count>0) cdc.Roles = service.TranslateList<PersonRoleDataContract, PersonRole>(value.Roles.ToList());
                    if (value.ChampionOffices != null && value.ChampionOffices.Count > 0) cdc.Offices = service.TranslateList<OfficeDataContract, Office>(value.ChampionOffices.OrderBy(c => c.Name).ToList());
                    if (cdc.Offices != null) cdc.IsChampion = cdc.Offices.Count > 0;
                }

                return cdc;
            }
        }

        protected override Person ServiceToBusiness(IEntityTranslatorService service, PersonDataContract value, TranslateLevel levelsToTranslate)
        {
            // No need to implement, Person is a view in the DB
            throw new NotImplementedException();
        }
    }
}
