﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EY.com.eBook.Core.EntityTranslation;
using EY.com.eBook.Core.EF;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.Core.EF.Read;

namespace EY.com.eBook.API.BL.Translators
{
    /// <summary>
    /// PersonBaseTranslator
    /// </summary>
    public class PersonBaseTranslator : EntityMapperTranslator<Person, PersonBaseDataContract>
    {
        private const string CHAMPIONROLE = "CHAMPION";
        private const string ADMINROLE = "ADMIN";

        protected override PersonBaseDataContract BusinessToService(IEntityTranslatorService service, Person value, TranslateLevel levelsToTranslate)
        {
            using (new InternalTranslators(service, new List<IEntityTranslator> { }))
            {
                var cdc = new PersonBaseDataContract
                {
                     Id = value.Id
                     ,FirstName = value.FirstName
                     ,LastName = value.LastName
                     ,Email = value.Email
                     ,WindowsAccount = value.WindowsAccount
                     ,LastLogon = value.LastLogon
                     ,Department = value.Department
                     , DefaultOfficeId = value.DefaultOffice
                     , GPN = value.Gpn
                };



                return cdc;
            }
        }

        protected override Person ServiceToBusiness(IEntityTranslatorService service, PersonBaseDataContract value, TranslateLevel levelsToTranslate)
        {
            // No need to implement, Person is a view in the DB
            throw new NotImplementedException();
        }
    }
}
