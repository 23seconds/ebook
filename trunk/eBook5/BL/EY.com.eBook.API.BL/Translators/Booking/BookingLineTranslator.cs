﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EY.com.eBook.Core.EntityTranslation;
using EY.com.eBook.Core.EF;
using EY.com.eBook.API.Contracts.Data;
using System.Diagnostics;
using EY.com.eBook.Core.EF.Read;

namespace EY.com.eBook.API.BL.Translators
{
    /// <summary>
    /// BookingLineTranslator
    /// </summary>
    public class BookingLineTranslator : EntityMapperTranslator<BookingLine, BookingLineDataContract>
    {
        /// <summary>
        /// Translates the business object to the service object.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <param name="value">The value.</param>
        /// <param name="levelsToTranslate">The levels to translate.</param>
        /// <returns></returns>
        protected override BookingLineDataContract BusinessToService(IEntityTranslatorService service, BookingLine value, TranslateLevel levelsToTranslate)
        {
            using (new InternalTranslators(service, new List<IEntityTranslator> { }))
            {
                var br = eBookReadManager.Context.BusinessRelations.FirstOrDefault(b => b.Id == value.BusinessRelationId);
                var bldc = new BookingLineDataContract
                {
                    Id = value.Id,
                    BusinessRelationId = value.BusinessRelationId,
                    Amount = value.Amount,
                    AccountNr = new StringListItemDataContract { Id = value.Account.InternalNr },
                    VisibleNr = string.IsNullOrEmpty(value.Account.ImportedID) ? value.Account.VisualNr : value.Account.ImportedID,
                    Comment = value.Comment,
                    BusinessRelationName = br !=null ?  br.Name : null
                };

                if (levelsToTranslate.TranslateRelations)
                {
                    string visual = value.Account.VisualNr;
                    visual = string.IsNullOrEmpty(visual) ? value.Account.InternalNr.Substring(0, 6) : visual;
                    value.BookingReference.LoadIfNotLoaded();
                    bldc.BookingId = value.Booking.Id;
                    if (value.Account.Descriptions != null)
                    {
                        AccountDescription ad = value.Account.Descriptions.FirstOrDefault(d => d.Culture == "nl-BE");
                        if (ad != null)
                        {
                            bldc.AccountNr.NL = string.Format("{0} {1}", visual, ad.Description);
                        }
                        else
                        {
                            bldc.AccountNr.NL = visual;
                        }

                        ad = value.Account.Descriptions.FirstOrDefault(d => d.Culture == "fr-FR");
                        if (ad != null)
                        {
                            bldc.AccountNr.FR = string.Format("{0} {1}", visual, ad.Description);
                        }
                        else
                        {
                            bldc.AccountNr.FR = visual;
                        }

                        ad = value.Account.Descriptions.FirstOrDefault(d => d.Culture == "en-US");
                        if (ad != null)
                        {
                            bldc.AccountNr.EN = string.Format("{0} {1}", visual, ad.Description);
                        }
                        else
                        {
                            bldc.AccountNr.EN = visual;
                        }


                    }
                }
               
                return bldc;
            }
        }
        
        /// <summary>
        /// Translates the service object to the business object.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <param name="value">The value.</param>
        /// <param name="levelsToTranslate">The levels to translate.</param>
        /// <returns></returns>
        protected override BookingLine ServiceToBusiness(IEntityTranslatorService service, BookingLineDataContract value, TranslateLevel levelsToTranslate)
        {
            //using (new InternalTranslators(service, new List<IEntityTranslator> { }))
            //{
            //    BookingLine bookingLine = EFContextManager.Context.BookingLines.FirstOrDefault(b => b.Id == value.Id);
            //    if (bookingLine == null)
            //        bookingLine = new BookingLine { Id = value.Id };

            //    value.AccountNr=value.AccountNr.PadRight(12, '0');
            //    bookingLine.BusinessRelationId = value.BusinessRelationId;
            //    bookingLine.Amount = value.Amount;
            //    bookingLine.Booking = EFContextManager.Context.Bookings.Include("File").First(b => b.Id == value.BookingId);
                
            //    bookingLine.Account = EFContextManager.Context.Accounts.FirstOrDefault(a=>a.FileId==bookingLine.Booking.File.Id && a.InternalNr==value.AccountNr);
            //    if (bookingLine.Account == null) throw new Exception(string.Format("Bookingline file id [{0}], booking id [{1}], account {2}", bookingLine.Booking.File.Id, value.BookingId, value.AccountNr));


            //    return bookingLine;
            //}
            throw new NotImplementedException();
        }

    }
}
