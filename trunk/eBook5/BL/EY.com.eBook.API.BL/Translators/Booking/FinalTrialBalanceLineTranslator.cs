﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EY.com.eBook.Core.EntityTranslation;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.Core.EF;
using EY.com.eBook.Core.EF.Read;

namespace EY.com.eBook.API.BL.Translators
{
    /// <summary>
    /// FinalTrialBalanceLineTranslator
    /// </summary>
    public class FinalTrialBalanceLineTranslator : EntityMapperTranslator<Account, FinalTrialBalanceLineDataContract>
    {



        /// <summary>
        /// Translates the business object to the service object.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <param name="value">The value.</param>
        /// <param name="levelsToTranslate">The levels to translate.</param>
        /// <returns></returns>
        protected override FinalTrialBalanceLineDataContract BusinessToService(IEntityTranslatorService service, Account value, TranslateLevel levelsToTranslate)
        {
            using (new InternalTranslators(service, new List<IEntityTranslator> {  }))
            {
                value.Descriptions.LoadIfNotLoaded();

                var ftbLine = new FinalTrialBalanceLineDataContract
                {
                    InternalAccountNr = value.InternalNr
                    ,
                    Account = new StringListItemDataContract { Id = value.InternalNr }
                    ,
                    StartBalance = value.PreviousSaldo
                    ,
                    AdjustmentsCredit = (value.Saldo - value.PreviousSaldo) < 0 ? (value.Saldo - value.PreviousSaldo) : 0
                    ,
                    AdjustmentsDebet = (value.Saldo - value.PreviousSaldo) > 0 ? (value.Saldo - value.PreviousSaldo) : 0
                    ,
                    EndBalance = value.Saldo
                    ,
                    VisualNr = value.VisualNr
                    ,
                    Description = ""
                };

                if (value.Descriptions != null)
                {
                    string visual = value.VisualNr;
                    visual = string.IsNullOrEmpty(visual) ? value.InternalNr.Substring(0, 6) : visual;
                    AccountDescription ad = value.Descriptions.FirstOrDefault(d => d.Culture == "nl-BE");
                    if (ad != null)
                    {
                        ftbLine.Account.NL = string.Format("{0} {1}", visual, ad.Description);
                    }
                    else
                    {
                        ftbLine.Account.NL = visual;
                    }

                    ad = value.Descriptions.FirstOrDefault(d => d.Culture == "fr-FR");
                    if (ad != null)
                    {
                        ftbLine.Account.FR = string.Format("{0} {1}", visual, ad.Description);
                    }
                    else
                    {
                        ftbLine.Account.FR = visual;
                    }

                    ad = value.Descriptions.FirstOrDefault(d => d.Culture == "en-US");
                    if (ad != null)
                    {
                        ftbLine.Account.EN = string.Format("{0} {1}", visual, ad.Description);
                    }
                    else
                    {
                        ftbLine.Account.EN = visual;
                    }


                }
                

                return ftbLine;
            }
        }

        /// <summary>
        /// Translates the service object to the business object.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <param name="value">The value.</param>
        /// <param name="levelsToTranslate">The levels to translate.</param>
        /// <returns></returns>
        protected override Account ServiceToBusiness(IEntityTranslatorService service, FinalTrialBalanceLineDataContract value, TranslateLevel levelsToTranslate)
        {
            throw new NotImplementedException();
        }
    }
}
