﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EY.com.eBook.Core.EntityTranslation;
using EY.com.eBook.Core.EF;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.Core.EF.Read;

namespace EY.com.eBook.API.BL.Translators
{
    /// <summary>
    /// BookingTranslator
    /// </summary>
    public class BookingTranslator : EntityMapperTranslator<Booking, BookingDataContract>
    {
        /// <summary>
        /// Translates the business object to the service object.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <param name="value">The value.</param>
        /// <param name="levelsToTranslate">The levels to translate.</param>
        /// <returns></returns>
        protected override BookingDataContract BusinessToService(IEntityTranslatorService service, Booking value, TranslateLevel levelsToTranslate)
        {
            using (new InternalTranslators(service, new List<IEntityTranslator> { new BookingLineTranslator() }))
            {
                var bdc = new BookingDataContract
                {
                    Id = value.Id,
                    WorksheetTypeId = value.WorksheetTypeId,
                    Deleted = value.DelFlag,
                    Description = value.Description,
                    WorksheetBookingKey = value.WorksheetBookingKey,
                    CreationDate = value.CreationDate
                    , WorksheetRowId = value.WorksheetRowId
                    , WorksheetCollection = value.WorksheetCollection
                    , ExportCount = value.ExportCount
                    //, FileId = value.File.Id
                    , Hidden = value.HiddenFlag
                };

                if (levelsToTranslate.TranslateRelations)
                {
                    value.FileReference.LoadIfNotLoaded();
                    if (value.File != null)
                        bdc.FileId = value.File.Id;

                    value.BookingLines.LoadIfNotLoaded();
                    if (value.BookingLines != null)
                        bdc.BookingLines = service.TranslateList<BookingLineDataContract, BookingLine>(value.BookingLines.ToList());
                }

                return bdc;
            }
        }

        /// <summary>
        /// Translates the service object to the business object.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <param name="value">The value.</param>
        /// <param name="levelsToTranslate">The levels to translate.</param>
        /// <returns></returns>
        protected override Booking ServiceToBusiness(IEntityTranslatorService service, BookingDataContract value, TranslateLevel levelsToTranslate)
        {
            //using (new InternalTranslators(service, new List<IEntityTranslator> { new BookingLineTranslator() }))
            //{
            //    Booking booking = EFContextManager.Context.Bookings.FirstOrDefault(b => b.Id == value.Id);
            //    if (booking == null)
            //        booking = new Booking { Id = value.Id };
            //    booking.Nr = value.Nr;
            //    booking.WorksheetTypeId = value.WorksheetTypeId;
            //    booking.JournalType = EFContextManager.Context.JournalTypeSet.FirstOrDefault(j=> j.Id == value.JournalType);
            //    booking.ImpFlag = value.Imported;
            //    booking.DelFlag = value.Deleted;
            //    booking.Description = value.Description;
            //    booking.WorksheetBookingKey = value.WorksheetBookingKey;
            //    booking.BookingType = EFContextManager.Context.BookingTypeSet.First(bt => bt.Id == value.BookingTypeId);
            //    booking.File = EFContextManager.Context.Files.First(f => f.Id == value.FileId);
                                           
            //    return booking;
            //}
            throw new NotImplementedException();
        }

    }
}
