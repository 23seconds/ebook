﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EY.com.eBook.Core.EntityTranslation;
using EY.com.eBook.Core.EF;
using EY.com.eBook.API.Contracts.Data;
using System.ServiceModel;
using EY.com.eBook.Core.Data;
using EY.com.eBook.Core.EF.Read;

namespace EY.com.eBook.API.BL.Translators
{
    /// <summary>
    /// BookingSingleTranslator
    /// </summary>
    public class BookingSingleTranslator : EntityMapperTranslator<BookingLine, BookingSingleDataContract>
    {
        /// <summary>
        /// Translates the business object to the service object.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <param name="value">The value.</param>
        /// <param name="levelsToTranslate">The levels to translate.</param>
        /// <returns></returns>
        protected override BookingSingleDataContract BusinessToService(IEntityTranslatorService service, BookingLine value, TranslateLevel levelsToTranslate)
        {
            using (new InternalTranslators(service, new List<IEntityTranslator> { }))
            {
                value.BookingReference.LoadIfNotLoaded();
                value.AccountReference.LoadIfNotLoaded();
                value.Account.Descriptions.LoadIfNotLoaded();

                var bldc = new BookingSingleDataContract
                {
                    // booking
                    BookingId= value.Booking.Id,
                    BookingDescription= value.Booking.Description,
                    WorksheetTypeId= value.Booking.WorksheetTypeId,
                    WorksheetBookingKey = value.Booking.WorksheetBookingKey,
                    Hidden=value.Booking.HiddenFlag,
                    // bookingline
                    Id = value.Id,                  
                    Debet = value.Amount>0 ? value.Amount : 0,
                    Credit = value.Amount < 0 ? Math.Abs(value.Amount) : 0,
                    Account = new StringListItemDataContract { Id = value.Account.InternalNr }
                    , ExportCount = value.Booking.ExportCount
                    , WorksheetCollection = value.Booking.WorksheetCollection
                    , WorksheetRowId = value.Booking.WorksheetRowId
                    ,
                    BookingCreationDate = value.Booking.CreationDate
                };

                if (value.Account.Descriptions != null)
                {
                    string visual = value.Account.VisualNr;
                    visual = string.IsNullOrEmpty(visual) ? value.Account.InternalNr.Substring(0, 6) : visual;
                    AccountDescription ad = value.Account.Descriptions.FirstOrDefault(d => d.Culture == "nl-BE");
                    if (ad != null)
                    {
                        bldc.Account.NL = string.Format("{0} {1}", visual, ad.Description);
                    }
                    else
                    {
                        bldc.Account.NL = visual;
                    }

                    ad = value.Account.Descriptions.FirstOrDefault(d => d.Culture == "fr-FR");
                    if (ad != null)
                    {
                        bldc.Account.FR = string.Format("{0} {1}", visual, ad.Description);
                    }
                    else
                    {
                        bldc.Account.FR = visual;
                    }

                    ad = value.Account.Descriptions.FirstOrDefault(d => d.Culture == "en-US");
                    if (ad != null)
                    {
                        bldc.Account.EN = string.Format("{0} {1}", visual, ad.Description);
                    }
                    else
                    {
                        bldc.Account.EN = visual;
                    }


                }
               
               
                return bldc;
            }
        }
        
        /// <summary>
        /// Translates the service object to the business object.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <param name="value">The value.</param>
        /// <param name="levelsToTranslate">The levels to translate.</param>
        /// <returns></returns>
        protected override BookingLine ServiceToBusiness(IEntityTranslatorService service, BookingSingleDataContract value, TranslateLevel levelsToTranslate)
        {
            /*
            using (new InternalTranslators(service, new List<IEntityTranslator> { }))
            {
                BookingLine bookingLine = EFContextManager.Context.BookingLines.FirstOrDefault(b => b.Id == value.Id);
                if (bookingLine == null)
                    bookingLine = new BookingLine { Id = value.Id };
                               
                bookingLine.CurrencyId = value.CurrencyId;
                bookingLine.BusinessRelationId = value.BusinessRelationId;
                bookingLine.TypeId = (int)value.Type;
                bookingLine.Amount = value.Amount;
                bookingLine.Booking = EFContextManager.Context.Bookings.Include("File").First(b => b.Id == value.BookingId);
                bookingLine.Account = EFContextManager.Context.Accounts.FirstOrDefault(a=>a.FileId==bookingLine.Booking.File.Id && a.InternalNr==value.AccountNr);

                return bookingLine;
            }*/
            throw new NotImplementedException();
        }

    }
}
