﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EY.com.eBook.Core.EntityTranslation;
using EY.com.eBook.Core.EF;
using EY.com.eBook.API.Contracts.Data;
using System.Xml.Linq;
using EY.com.eBook.Core.EF.Read;

namespace EY.com.eBook.API.BL.Translators
{
    /// <summary>
    /// FileActionTranslator
    /// </summary>
    public class FileActionTranslator : EntityMapperTranslator<FileAction, FileActionDataContract>
    {
        /// <summary>
        /// Translates the business object to the service object.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <param name="value">The value.</param>
        /// <param name="levelsToTranslate">The levels to translate.</param>
        /// <returns></returns>
        protected override FileActionDataContract BusinessToService(IEntityTranslatorService service, FileAction value, TranslateLevel levelsToTranslate)
        {
            using (new InternalTranslators(service, new List<IEntityTranslator> { new PersonBaseTranslator() }))
            {
                var bdc = new FileActionDataContract
                {
                   Action = value.Action
                   , Date = value.DateTime
                   , FileId = value.FileId
                   , Meta= value.Meta
                   , Person =  value.PersonReference.IsLoaded ?  service.Translate<PersonBaseDataContract>(value.Person) : null
                     
                };

               
                return bdc;
            }
        }

        /// <summary>
        /// Translates the service object to the business object.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <param name="value">The value.</param>
        /// <param name="levelsToTranslate">The levels to translate.</param>
        /// <returns></returns>
        protected override FileAction ServiceToBusiness(IEntityTranslatorService service, FileActionDataContract value, TranslateLevel levelsToTranslate)
        {
            throw new NotImplementedException("USE PROCEDURE");
        }

    }
}
