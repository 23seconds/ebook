﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EY.com.eBook.Core.EntityTranslation;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.Core.EF;
using System.Data.Objects.DataClasses;
using System.Globalization;
using EY.com.eBook.Core.EF.Read;
using EY.com.eBook.API.Contracts.Data.Cache;

namespace EY.com.eBook.API.BL.Translators
{
    /// <summary>
    /// FileTranslator
    /// </summary>
    public class FileInfoTranslator : EntityMapperTranslator<File, FileInfoDataContract>
    {
        /// <summary>
        /// Translates the business object to the service object.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <param name="value">The value.</param>
        /// <param name="levelsToTranslate">The levels to translate.</param>
        /// <returns></returns>
        protected override FileInfoDataContract BusinessToService(IEntityTranslatorService service, File value, TranslateLevel levelsToTranslate)
        {
            using (new InternalTranslators(service, new List<IEntityTranslator> { new FileServiceTranslator()
                                 }))
            {

                CultureInfo ci = CultureInfo.CreateSpecificCulture(Culture);
                var fdc = new FileInfoDataContract
                {
                    Id = value.Id,
                    ClientId = value.Client != null ? value.Client.Id : Guid.Empty,
                    Name = value.Name,
                    Culture = value.Culture,
                    StartDate = value.StartDate,
                    EndDate = value.EndDate,
                    PreviousStartDate = value.PrevStartDate,
                    PreviousEndDate = value.PrevEndDate,
                    NumbersLocked = value.NumbersLocked,
                    Display = string.Format("{0} [{1} - {2}]",
                                                                value.Name,
                                                                value.StartDate.ToString(ci.DateTimeFormat.ShortDatePattern, ci.DateTimeFormat),
                                                                value.EndDate.ToString(ci.DateTimeFormat.ShortDatePattern, ci.DateTimeFormat)) 
                    , Closed =value.CloseDate.HasValue
                    , Errors=0
                    , Warnings =0
                    , ImportDate = value.ImportDate
                    , ImportType = value.ImportType

                };

                if (value.Services != null)
                {
                    fdc.Services = service.TranslateList<FileServiceDataContract, FileService>(value.Services.ToList());
                }

                //if (value.Messages != null)
                //{
                //    fdc.Errors = value.Messages.Where(f => f.Type == 99).Count();
                //    fdc.Warnings = value.Messages.Where(f => f.Type == 50).Count();
                //}

                return fdc;
            }
        }

        /// <summary>
        /// Translates the service object to the business object.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <param name="value">The value.</param>
        /// <param name="levelsToTranslate">The levels to translate.</param>
        /// <returns></returns>
        protected override File ServiceToBusiness(IEntityTranslatorService service, FileInfoDataContract value, TranslateLevel levelsToTranslate)
        {
            // No need to implement, FileBase is only for viewing (lists)
            throw new NotImplementedException();
        }
    }
}


