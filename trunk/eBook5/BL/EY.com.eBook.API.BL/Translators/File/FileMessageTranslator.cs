﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EY.com.eBook.Core.EntityTranslation;
using EY.com.eBook.Core.EF;
using EY.com.eBook.API.Contracts.Data;
using System.Xml.Linq;
using EY.com.eBook.Core.EF.Read;
using EY.com.eBook.Core;

namespace EY.com.eBook.API.BL.Translators
{
    /// <summary>
    /// FileMessageTranslator
    /// </summary>
    public class FileMessageTranslator : EntityMapperTranslator<Message, FileMessageDataContract>
    {
        /// <summary>
        /// Translates the business object to the service object.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <param name="value">The value.</param>
        /// <param name="levelsToTranslate">The levels to translate.</param>
        /// <returns></returns>
        protected override FileMessageDataContract BusinessToService(IEntityTranslatorService service, Message value, TranslateLevel levelsToTranslate)
        {
            using (new InternalTranslators(service, new List<IEntityTranslator> { }))
            {
                var bdc = new FileMessageDataContract
                {
                    FileId = value.FileId
                               ,
                    MessageId = value.MessageId
                               ,
                    Culture = value.Culture
                               ,
                    Type = CoreHelper.ToEnum<MessageTypeDataContract>(value.Type)
                               ,
                    Text = value.Text
                               ,
                    ConnectionType = CoreHelper.ToEnum<FileMessageConnectionTypeDataContract>(value.ConnectionType)
                               ,
                    ConnectionAccount = value.ConnectionAccount
                               ,
                    ConnectionGuid = value.ConnectionGuid
                               ,
                    WorksheetTypeCollection = value.WorksheetTypeCollection
                               ,
                    WorksheetTypeRowId = value.WorksheetTypeRowId
                               ,
                    WorksheetTypeField = value.WorksheetTypeField

                };

               
                return bdc;
            }
        }

        /// <summary>
        /// Translates the service object to the business object.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <param name="value">The value.</param>
        /// <param name="levelsToTranslate">The levels to translate.</param>
        /// <returns></returns>
        protected override Message ServiceToBusiness(IEntityTranslatorService service, FileMessageDataContract value, TranslateLevel levelsToTranslate)
        {
            throw new NotImplementedException("USE PROCEDURE");
        }

    }
}
