﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EY.com.eBook.Core.EntityTranslation;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.Core.EF;
using System.Data.Objects.DataClasses;
using System.Globalization;
using EY.com.eBook.Core.EF.Read;
using EY.com.eBook.API.Contracts.Data.Cache;

namespace EY.com.eBook.API.BL.Translators
{
    /// <summary>
    /// FileServiceTranslator
    /// </summary>
    public class FileServiceTranslator : EntityMapperTranslator<FileService, FileServiceDataContract>
    {
        /// <summary>
        /// Translates the business object to the service object.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <param name="value">The value.</param>
        /// <param name="levelsToTranslate">The levels to translate.</param>
        /// <returns></returns>
        protected override FileServiceDataContract BusinessToService(IEntityTranslatorService service, FileService value, TranslateLevel levelsToTranslate)
        {
            using (new InternalTranslators(service, new List<IEntityTranslator> { 
                                 }))
            {

                FileServiceDataContract fdc = new FileServiceDataContract
                {
                    FileId = value.FileId
                    , ServiceId = value.ServiceId
                    , Completed = value.Completed
                    , CompletedBy = value.CompletedBy
                    , Order = value.Order
                    , Status = value.Status
                };
                return fdc;
            }
        }

        /// <summary>
        /// Translates the service object to the business object.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <param name="value">The value.</param>
        /// <param name="levelsToTranslate">The levels to translate.</param>
        /// <returns></returns>
        protected override FileService ServiceToBusiness(IEntityTranslatorService service, FileServiceDataContract value, TranslateLevel levelsToTranslate)
        {
            // No need to implement, FileBase is only for viewing (lists)
            throw new NotImplementedException();
        }
    }
}


