﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EY.com.eBook.Core.EF;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.Core.EntityTranslation;
using EY.com.eBook.Core.EF.Read;

namespace EY.com.eBook.API.BL.Translators
{
    public class WorksheetTypeTranslator : EntityMapperTranslator<WorksheetType, WorksheetTypeDataContract>
    {
        /// <summary>
        /// Translates the business object to the service object.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <param name="value">The value.</param>
        /// <param name="levelsToTranslate">The levels to translate.</param>
        /// <returns></returns>
        protected override WorksheetTypeDataContract BusinessToService(IEntityTranslatorService service, WorksheetType value, TranslateLevel levelsToTranslate)
        {
            using (new InternalTranslators(service, new List<IEntityTranslator> {  }))
            {
                var wstdc = new WorksheetTypeDataContract
                {                    
                    Id = value.Id,
                    IsActive = value.IsActive,
                    IsFiche = value.IsFiche,
                    Culture= Culture,
                    Order = value.Order,
                    RuleApp = value.RuleApp,
                    Type = value.Type,
                    ClosingDepartment = value.ClosingDepartment
                };
                Names name = value.Names.FirstOrDefault(n => n.Culture == Culture);
                if (name != null)
                {
                    wstdc.Name = name.Description;
                }
                else
                {
                    wstdc.Name = "NO TRANSLATION FOUND FOR " + Culture;
                }


                return wstdc;
            }
        }

        /// <summary>
        /// Translates the service object to the business object.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <param name="value">The value.</param>
        /// <param name="levelsToTranslate">The levels to translate.</param>
        /// <returns></returns>
        protected override WorksheetType ServiceToBusiness(IEntityTranslatorService service, WorksheetTypeDataContract value, TranslateLevel levelsToTranslate)
        {
            // No need to implement, it's a view in the database
            throw new NotImplementedException();
        }
    }
}
