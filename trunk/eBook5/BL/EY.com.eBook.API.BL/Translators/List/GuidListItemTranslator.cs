﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EY.com.eBook.Core.EntityTranslation;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.Core.EF;
using EY.com.eBook.Core.EF.Read;

namespace EY.com.eBook.API.BL.Translators
{
    /// <summary>
    /// GuidListItemTranslator
    /// </summary>
    public class GuidListItemTranslator : EntityMapperTranslator<ListItem, GuidListItemDataContract>
    {

        /// <summary>
        /// Translates the business object to the service object.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <param name="value">The value.</param>
        /// <param name="levelsToTranslate">The levels to translate.</param>
        /// <returns></returns>
        protected override GuidListItemDataContract BusinessToService(IEntityTranslatorService service, ListItem value, TranslateLevel levelsToTranslate)
        {
            value.Names.LoadIfNotLoaded();
            return new GuidListItemDataContract
            {
                Id=value.Id
                ,
                NL= value.Names.Count(n => n.Culture == "nl-BE") > 0 ? value.Names.First(n => n.Culture == "nl-BE").Description : value.Id.ToString()
                ,
                FR = value.Names.Count(n => n.Culture == "fr-FR") > 0 ? value.Names.First(n => n.Culture == "fr-FR").Description : value.Id.ToString()
                ,
                EN = value.Names.Count(n => n.Culture == "en-US") > 0 ? value.Names.First(n => n.Culture == "en-US").Description : value.Id.ToString()
                //, Value = value.Value
            };


        }

        /// <summary>
        /// Translates the service object to the business object.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <param name="value">The value.</param>
        /// <param name="levelsToTranslate">The levels to translate.</param>
        /// <returns></returns>
        protected override ListItem ServiceToBusiness(IEntityTranslatorService service, GuidListItemDataContract value, TranslateLevel levelsToTranslate)
        {
            throw new NotImplementedException();
            
        }
        
    }
}
