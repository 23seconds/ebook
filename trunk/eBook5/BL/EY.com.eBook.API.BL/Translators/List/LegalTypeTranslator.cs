﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EY.com.eBook.Core.EntityTranslation;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.Core.EF;
using EY.com.eBook.Core.EF.Read;

namespace EY.com.eBook.API.BL.Translators
{

    public class LegalTypeTranslator : EntityMapperTranslator<LegalTypeTranslation, LegalTypeDataContract>
    {
        protected override LegalTypeDataContract BusinessToService(IEntityTranslatorService service, LegalTypeTranslation value, TranslateLevel levelsToTranslate)
        {
            return new LegalTypeDataContract
            {
                Id = value.LegalType.Id
                ,
                Code = value.Code
                ,
                ListDescription = string.Format("{0} - {1}", value.Code, value.Description)
                ,
                Name = value.Description
            };
        }

        protected override LegalTypeTranslation ServiceToBusiness(IEntityTranslatorService service, LegalTypeDataContract value, TranslateLevel levelsToTranslate)
        {
            throw new NotImplementedException();
        }
    }
}
