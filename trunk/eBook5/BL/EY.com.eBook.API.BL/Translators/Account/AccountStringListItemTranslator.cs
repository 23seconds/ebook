﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EY.com.eBook.Core.EntityTranslation;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.Core.EF;
using EY.com.eBook.Core.EF.Read;

namespace EY.com.eBook.API.BL.Translators
{
    /// <summary>
    /// AccountStringListItemTranslator
    /// </summary>
    public class AccountStringListItemTranslator : EntityMapperTranslator<Account, StringListItemDataContract>
    {

        /// <summary>
        /// Translates the business object to the service object.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <param name="value">The value.</param>
        /// <param name="levelsToTranslate">The levels to translate.</param>
        /// <returns></returns>
        protected override StringListItemDataContract BusinessToService(IEntityTranslatorService service, Account value, TranslateLevel levelsToTranslate)
        {
            
            StringListItemDataContract sli = new StringListItemDataContract
            {
                Id=value.InternalNr.ToString()
                //, Value = value.Value
            };

            value.Descriptions.LoadIfNotLoaded();
            foreach (AccountDescription ad in value.Descriptions)
            {
                switch (ad.Culture)
                {
                    case "nl-BE":
                        sli.NL = string.Format("{0} {1}", value.VisualNr, ad.Description);
                        break;
                    case "fr-FR":
                        sli.FR = string.Format("{0} {1}", value.VisualNr, ad.Description);
                        break;
                    case "en-US":
                        sli.EN = string.Format("{0} {1}", value.VisualNr, ad.Description);
                        break;

                }
            }
            return sli;
        }

        /// <summary>
        /// Translates the service object to the business object.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <param name="value">The value.</param>
        /// <param name="levelsToTranslate">The levels to translate.</param>
        /// <returns></returns>
        protected override Account ServiceToBusiness(IEntityTranslatorService service, StringListItemDataContract value, TranslateLevel levelsToTranslate)
        {
            throw new NotImplementedException();
            
        }
        
    }
}
