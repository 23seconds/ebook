﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EY.com.eBook.Core.EntityTranslation;
using EY.com.eBook.Core.EF;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.Core.EF.Read;

namespace EY.com.eBook.API.BL.Translators
{
    /// <summary>
    /// ClientOverruleTranslator
    /// </summary>
    public class ClientOverruleTranslator : EntityMapperTranslator<ClientOverrule, ClientOverruleDataContract>
    {
        protected override ClientOverruleDataContract BusinessToService(IEntityTranslatorService service, ClientOverrule value, TranslateLevel levelsToTranslate)
        {
            using (new InternalTranslators(service, new List<IEntityTranslator> {  }))
            {
                var cdc = new ClientOverruleDataContract
                {
                     // ClientBaseDataContract properties 
                     // TODO: refactor => call ClientBaseTranslator
                     Id = value.Id,
                     Name = value.Name,
                     Address = value.Address,
                     ZipCode = value.ZipCode,
                     City = value.City,
                     Country = value.Country,
                     Vat = value.VatNumber,
                     EnterpriseNumber = value.EnterpriseNumber,

                     // ClientDataContract properties
                     DirectorName = value.Director,
                     Email = value.Email,
                     Fax = value.Fax,
                     IsVat = string.IsNullOrEmpty(value.SubjectToVAT) ? null : ((value.SubjectToVAT.ToUpper() == "Y") ? (bool?)true : (bool?)false),
                     LegalStructure = value.LegalStructure,
                     Phone = value.Tel,
                     Rpr = value.Rpr,                     
                    // LegalTypeId = value.LegalTypeId,
                     IsActive = string.IsNullOrEmpty(value.Active) ? null : ((value.Active.ToUpper() == "Y") ? (bool?)true : (bool?)false),
                     FodAddress = string.IsNullOrEmpty(value.FodAddress) ? null : Core.CoreHelper.DeserializeFromString<FodAddressDataContract>(value.FodAddress)
                };


                return cdc;
            }
        }

        protected override ClientOverrule ServiceToBusiness(IEntityTranslatorService service, ClientOverruleDataContract value, TranslateLevel levelsToTranslate)
        {

            throw new NotImplementedException();

        }

      
    }
}
