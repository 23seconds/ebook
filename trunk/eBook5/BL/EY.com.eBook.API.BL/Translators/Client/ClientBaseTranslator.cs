﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EY.com.eBook.Core.EntityTranslation;
using EY.com.eBook.Core.EF;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.Core.EF.Read;

namespace EY.com.eBook.API.BL.Translators
{
    public class ClientBaseTranslator : EntityMapperTranslator<Client, ClientBaseDataContract>
    {
        public Guid? PersonId { get; set; }
        public string ActiveRole { get; set; }

        protected override ClientBaseDataContract BusinessToService(IEntityTranslatorService service, Client value, TranslateLevel levelsToTranslate)
        {
            using (new InternalTranslators(service, new List<IEntityTranslator> { }))
            {
                var cdc = new ClientBaseDataContract
                {                    
                    Id = value.Id,
                    Name = value.Name,
                    GfisCode = value.GFISCode,
                    Address = value.Address,
                    ZipCode = value.ZipCode,
                    City = value.City,
                    Country = value.Country,
                    Vat = value.VatNumber,
                    EnterpriseNumber = value.EnterpriseNumber,
                    ProAccServer = value.ProAccServer,
                    ProAccDatabase = value.ProAccDatabase,
                    ProAccLinkUpdated = value.ProAccLinkUpdated,
                    Shared = false,
                    BizTaxFolder = value.BizTaxFolder,
                    LegalTypeId = value.CLIENT_LEGAL_TYPE_ID,
                    BNI = value.BNI
                };
                if (value.ClientDepartments != null)
                {
                    cdc.Shared = value.ClientDepartments.Count() > 1;
                }
                
                if (value.PersonRoles != null)
                {
                    IQueryable<PersonClientRole> qpcr = value.PersonRoles.AsQueryable();

                    if (PersonId.HasValue)
                    {
                        qpcr = qpcr.Where(p => p.PersonId == PersonId.Value);
                        cdc.Roles = qpcr.Select(r => r.Role).ToArray();
                    }
                    
                    if(cdc.Roles == null && !String.IsNullOrEmpty(ActiveRole))
                    {
                        //if no roles can be retrieved, use the active role
                        //TODO: It would be better to improve security on frontend instead
                        cdc.Roles[0] = ActiveRole;
                    }
                }
               
                return Overrule(cdc, value);
            }
        }

        protected override Client ServiceToBusiness(IEntityTranslatorService service, ClientBaseDataContract value, TranslateLevel levelsToTranslate)
        {
            // No need to implement, client is a view in the DB
            throw new NotImplementedException();
        }

        private ClientBaseDataContract Overrule(ClientBaseDataContract cbdc,Client value)
        {
            if (value.Overrule != null)
            {
                cbdc.Name=!string.IsNullOrEmpty(value.Overrule.Name) ? value.Overrule.Name : cbdc.Name;
                cbdc.Address = !string.IsNullOrEmpty(value.Overrule.Address) ? value.Overrule.Address : cbdc.Address;
                cbdc.City = !string.IsNullOrEmpty(value.Overrule.City) ? value.Overrule.City : cbdc.City;
                cbdc.ZipCode = !string.IsNullOrEmpty(value.Overrule.ZipCode) ? value.Overrule.ZipCode : cbdc.ZipCode;
                cbdc.Country = !string.IsNullOrEmpty(value.Overrule.Country) ? value.Overrule.Country : cbdc.Country;
                cbdc.EnterpriseNumber = !string.IsNullOrEmpty(value.Overrule.EnterpriseNumber) ? value.Overrule.EnterpriseNumber : cbdc.EnterpriseNumber;
                cbdc.Vat = !string.IsNullOrEmpty(value.Overrule.VatNumber) ? value.Overrule.VatNumber : cbdc.Vat;
                cbdc.BizTaxFolder = cbdc.BizTaxFolder;
                cbdc.BNI = cbdc.BNI;
                if (string.IsNullOrEmpty(value.Overrule.FodAddress))
                {
                    cbdc.FodAddress = null;
                }
                else
                {

                    cbdc.FodAddress = Core.CoreHelper.DeserializeFromString<FodAddressDataContract>(value.Overrule.FodAddress);
                }
            }
            return cbdc;
        }

    }
}
