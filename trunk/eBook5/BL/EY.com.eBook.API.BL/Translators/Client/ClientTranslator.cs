﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EY.com.eBook.Core.EntityTranslation;
using EY.com.eBook.Core.EF;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.Core.EF.Read;

namespace EY.com.eBook.API.BL.Translators
{
    /// <summary>
    /// ClientTranslator
    /// </summary>
    public class ClientTranslator : EntityMapperTranslator<Client, ClientDataContract>
    {
        protected override ClientDataContract BusinessToService(IEntityTranslatorService service, Client value, TranslateLevel levelsToTranslate)
        {
            using (new InternalTranslators(service, new List<IEntityTranslator> { 
                new CustomerTranslator(), new SupplierTranslator() }))
            {
                var cdc = new ClientDataContract
                {
                     // ClientBaseDataContract properties 
                     // TODO: refactor => call ClientBaseTranslator
                     Id = value.Id,
                     Name = value.Name,
                     GfisCode = value.GFISCode,
                     Address = value.Address,
                     ZipCode = value.ZipCode,
                     City = value.City,
                     Country = value.Country,
                     Vat = value.VatNumber,
                     EnterpriseNumber = value.EnterpriseNumber,
                     ProAccServer = value.ProAccServer,
                    ProAccDatabase = value.ProAccDatabase,
                    ProAccLinkUpdated = value.ProAccLinkUpdated,
                     LegalTypeId= value.CLIENT_LEGAL_TYPE_ID,
                     // ClientDataContract properties
                     DirectorName = value.Director,
                     Email = value.Email,
                     Fax = value.Fax,
                     IsVat = (value.SubjectToVAT.ToUpper() == "Y") ? true : false,
                     LegalStructure = value.LegalStructure,
                     Phone = value.Tel,
                     Rpr = value.Rpr,                     
                    // LegalTypeId = value.LegalTypeId,
                     IsActive = (value.Active.ToUpper() == "Y") ? true : false,
                     BizTaxFolder = value.BizTaxFolder     ,
                     BNI = value.BNI
                };
                
                if (levelsToTranslate.TranslateRelations)
                {
                    //if (value.BusinessRelations != null)
                    //{
                    //    cdc.Customers = service.TranslateList<CustomerDataContract, BusinessRelation>(value.BusinessRelations.Where(b=>b.Type=="C").ToList());
                    //    cdc.Suppliers = service.TranslateList<SupplierDataContract, BusinessRelation>(value.BusinessRelations.Where(b => b.Type == "S").ToList());
                    //}
                }

                return Overrule(cdc,value);
            }
        }

        protected override Client ServiceToBusiness(IEntityTranslatorService service, ClientDataContract value, TranslateLevel levelsToTranslate)
        {
            // No need to implement, client is a view in the DB
            throw new NotImplementedException();
        }

        private ClientDataContract Overrule(ClientDataContract cbdc, Client value)
        {
            if (value.Overrule != null)
            {
                return new ClientDataContract
                {
                    Id = cbdc.Id
                    ,
                    GfisCode = cbdc.GfisCode
                    ,
                    Name = !string.IsNullOrEmpty(value.Overrule.Name) ? value.Overrule.Name : cbdc.Name
                    ,
                    Address = !string.IsNullOrEmpty(value.Overrule.Address) ? value.Overrule.Address : cbdc.Address
                    ,
                    City = !string.IsNullOrEmpty(value.Overrule.City) ? value.Overrule.City : cbdc.City
                    ,
                    ZipCode = !string.IsNullOrEmpty(value.Overrule.ZipCode) ? value.Overrule.ZipCode : cbdc.ZipCode
                    ,
                    Country = !string.IsNullOrEmpty(value.Overrule.Country) ? value.Overrule.Country : cbdc.Country
                    ,
                    EnterpriseNumber = !string.IsNullOrEmpty(value.Overrule.EnterpriseNumber) ? value.Overrule.EnterpriseNumber : cbdc.EnterpriseNumber
                    ,
                    Vat = !string.IsNullOrEmpty(value.Overrule.VatNumber) ? value.Overrule.VatNumber : cbdc.Vat

                    ,
                    DirectorName = !string.IsNullOrEmpty(value.Overrule.Director) ? value.Overrule.Director : cbdc.DirectorName
                    ,
                    Email = !string.IsNullOrEmpty(value.Overrule.Email) ? value.Overrule.Email : cbdc.Email
                    ,
                    Fax = !string.IsNullOrEmpty(value.Overrule.Fax) ? value.Overrule.Fax : cbdc.Fax
                    ,
                    IsVat = !string.IsNullOrEmpty(value.Overrule.SubjectToVAT) ? ((value.Overrule.SubjectToVAT.ToUpper() == "Y") ? true : false) : cbdc.IsVat
                    ,
                    LegalStructure = !string.IsNullOrEmpty(value.Overrule.LegalStructure) ? value.Overrule.LegalStructure : cbdc.LegalStructure
                    ,
                    Phone = !string.IsNullOrEmpty(value.Overrule.Tel) ? value.Overrule.Tel : cbdc.Phone
                    ,
                    Rpr = !string.IsNullOrEmpty(value.Overrule.Rpr) ? value.Overrule.Rpr : cbdc.Rpr
                    ,
                    LegalTypeId = !string.IsNullOrEmpty(value.Overrule.CLIENT_LEGAL_TYPE_ID) ? value.Overrule.CLIENT_LEGAL_TYPE_ID : cbdc.LegalTypeId
                    ,
                    IsActive = !string.IsNullOrEmpty(value.Overrule.Active) ? ((value.Overrule.Active.ToUpper() == "Y") ? true : false) : cbdc.IsActive
                    //,
                    //Customers = cbdc.Customers
                    //,
                    //Suppliers = cbdc.Suppliers
                    ,
                    SecUlt = cbdc.SecUlt
                    ,
                    BizTaxFolder = cbdc.BizTaxFolder
                    ,
                    FodAddress = string.IsNullOrEmpty(value.Overrule.FodAddress) ? null : Core.CoreHelper.DeserializeFromString<FodAddressDataContract>(value.Overrule.FodAddress)
                    ,
                    BNI = cbdc.BNI
                };
            }
            return cbdc;
        }
    }
}
