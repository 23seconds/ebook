﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EY.com.eBook.Core.EntityTranslation;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.Core.EF;
using EY.com.eBook.Core.EF.Write;
using EY.com.eBook.Core;

namespace EY.com.eBook.API.BL.Translators
{
    /// <summary>
    /// XbrlTranslator
    /// </summary>
    public class XbrlTranslator : EntityMapperTranslator<FileXbrl, XbrlDataContract>
    {
        /// <summary>
        /// Translates the business object to the service object.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <param name="value">The value.</param>
        /// <param name="levelsToTranslate">The levels to translate.</param>
        /// <returns></returns>
        protected override XbrlDataContract BusinessToService(IEntityTranslatorService service, FileXbrl value, TranslateLevel levelsToTranslate)
        {
            using (new InternalTranslators(service, new List<IEntityTranslator> {  }))
            {
                var odc = new XbrlDataContract
                {
                    Calculation = null
                    , Data = CoreHelper.DeserializeFromString<BizTaxDataContract>(value.XbrlData)
                    , FileId = value.FileId
                    , Locked = value.Locked
                    , Status = value.StatusId
                    , Type = value.XbrlType
                    , Validated = value.Validated
                };
               
                return odc;
            }
        }

        /// <summary>
        /// Translates the service object to the business object.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <param name="value">The value.</param>
        /// <param name="levelsToTranslate">The levels to translate.</param>
        /// <returns></returns>
        protected override FileXbrl ServiceToBusiness(IEntityTranslatorService service, XbrlDataContract value, TranslateLevel levelsToTranslate)
        {
            throw new NotImplementedException();
        }
    }
}
