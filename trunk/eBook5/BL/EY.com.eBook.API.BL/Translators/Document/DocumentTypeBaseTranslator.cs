﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EY.com.eBook.Core.EF;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.Core.EntityTranslation;
using EY.com.eBook.Core.EF.Read;

namespace EY.com.eBook.API.BL.Translators
{
    public class DocumentTypeBaseTranslator : EntityMapperTranslator<DocumentType, DocumentTypeBaseDataContract>
    {
        /// <summary>
        /// Translates the business object to the service object.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <param name="value">The value.</param>
        /// <param name="levelsToTranslate">The levels to translate.</param>
        /// <returns></returns>
        protected override DocumentTypeBaseDataContract BusinessToService(IEntityTranslatorService service, DocumentType value, TranslateLevel levelsToTranslate)
        {
            using (new InternalTranslators(service, new List<IEntityTranslator> { }))
            {
                var dtdc = new DocumentTypeBaseDataContract
                {
                    Id = value.Id,
                    IsActive = value.IsActive,
                    IsFullyEditable = value.IsFullyEditable,
                    Key = value.Key,
                };
                Names name = value.Names.FirstOrDefault(n => n.Culture == Culture);
                if (name != null)
                {
                    dtdc.Name = name.Description;
                }
                else
                {
                    dtdc.Name = "NO TRANSLATION FOUND FOR " + Culture;
                }
                return dtdc;
            }
        }

        /// <summary>
        /// Translates the service object to the business object.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <param name="value">The value.</param>
        /// <param name="levelsToTranslate">The levels to translate.</param>
        /// <returns></returns>
        protected override DocumentType ServiceToBusiness(IEntityTranslatorService service, DocumentTypeBaseDataContract value, TranslateLevel levelsToTranslate)
        {
            // No need to implement, it's a view in the database
            throw new NotImplementedException();
        }
    }
}
