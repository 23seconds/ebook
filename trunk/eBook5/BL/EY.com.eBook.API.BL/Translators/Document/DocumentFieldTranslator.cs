﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EY.com.eBook.Core.EntityTranslation;
using EY.com.eBook.Core.EF;
using EY.com.eBook.API.Contracts.Data;
using System.Runtime.Serialization;
using System.Xml;
using EY.com.eBook.Core.EF.Read;

namespace EY.com.eBook.API.BL.Translators
{
    /// <summary>
    /// DocumentFieldTranslator
    /// </summary>
    public class DocumentFieldTranslator : EntityMapperTranslator<DocumentField, DocumentFieldDataContract>
    {
        /// <summary>
        /// Translates the business object to the service object.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <param name="value">The value.</param>
        /// <param name="levelsToTranslate">The levels to translate.</param>
        /// <returns></returns>
        protected override DocumentFieldDataContract BusinessToService(IEntityTranslatorService service, DocumentField value, TranslateLevel levelsToTranslate)
        {
            using (new InternalTranslators(service, new List<IEntityTranslator> { }))
            {
                var documentField = new DocumentFieldDataContract
                {
                    DocumentId = value.Document.Id
                    , Name = value.Name
                    ,DataType = value.DataType
                };

                if (!string.IsNullOrEmpty(value.Value))
                {
                    switch(value.DataType.ToLower()) {
                        case "datetime":
                            documentField.Value = Core.CoreHelper.DeserializeFromString<DateTime>(value.Value);
                            break;
                        case "int":
                        case "int32":
                            documentField.Value = Core.CoreHelper.DeserializeFromString<int>(value.Value);
                            break;
                        case "decimal":
                            documentField.Value = Core.CoreHelper.DeserializeFromString<decimal>(value.Value);
                            break;
                        case "float":
                            documentField.Value = Core.CoreHelper.DeserializeFromString<float>(value.Value);
                            break;
                        case "bool":
                        case "boolean":
                            documentField.Value = Core.CoreHelper.DeserializeFromString<bool>(value.Value);
                            break;
                        default:
                            documentField.Value = Core.CoreHelper.DeserializeFromString<string>(value.Value);
                            break;
                    }
                    
                }
                else
                {
                    documentField.Value = null;
                }
                return documentField;
            }
        }

        /// <summary>
        /// Translates the service object to the business object.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <param name="value">The value.</param>
        /// <param name="levelsToTranslate">The levels to translate.</param>
        /// <returns></returns>
        protected override DocumentField ServiceToBusiness(IEntityTranslatorService service, DocumentFieldDataContract value, TranslateLevel levelsToTranslate)
        {
            throw new NotImplementedException();
            //    using (new InternalTranslators(service, new List<IEntityTranslator> { }))
            //    {
            //        DocumentField documentField = EFContextManager.Context.DocumentFieldSet
            //                                                            .Include("Document")
            //                                                            .FirstOrDefault(d => d.Document.Id == value.DocumentId && d.Name==value.Name );

            //        Document doc = EFContextManager.Context.Documents.Where(d => d.Id == value.DocumentId).FirstOrDefault();
            //        if (documentField == null)
            //        {
            //            documentField = new DocumentField
            //            {
            //                Name=value.Name
            //                , Document = EFContextManager.Context.Documents.Where(d=>d.Id==value.DocumentId).First()
            //                , DataType = typeof(string).Name
            //                , Value=null
            //            };
            //        }

            //        if (value.Value != null)
            //        {
            //            switch (value.DataType.ToUpper())
            //            {
            //                case "DATETIME":
            //                    value.Value = DateTime.Parse(value.Value.ToString());
            //                    break;
            //                case "INT":
            //                case "INT32":
            //                    value.Value = int.Parse(value.Value.ToString());
            //                    break;
            //                case "FLOAT":
            //                    value.Value = float.Parse(value.Value.ToString());
            //                    break;
            //                case "DECIMAL":
            //                    value.Value = decimal.Parse(value.Value.ToString());
            //                    break;
            //                case "BOOL":
            //                    value.Value = bool.Parse(value.Value.ToString());
            //                    break;
            //                default:
            //                    value.Value = value.Value.ToString();
            //                    break;
            //            }

            //            documentField.DataType = value.Value.GetType().Name;

            //            documentField.Value = Core.CoreHelper.SerializeToString(value.Value);
            //        }

            //        return documentField;
            //    }
        }
    }
}


