﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.Core.EF;
using EY.com.eBook.Core.EntityTranslation;
using EY.com.eBook.Core.EF.Read;

namespace EY.com.eBook.API.BL.Translators
{
    /// <summary>
    /// 
    /// </summary>
    public class BlockTypeDataTranslator : EntityMapperTranslator<BlockTypeData, BlockTypeDataDataContract>
    {
        /// <summary>
        /// Translates the business object to the service object.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <param name="value">The value.</param>
        /// <param name="levelsToTranslate">The levels to translate.</param>
        /// <returns></returns>
        protected override BlockTypeDataDataContract BusinessToService(IEntityTranslatorService service, BlockTypeData value, TranslateLevel levelsToTranslate)
        {
            using (new InternalTranslators(service, new List<IEntityTranslator> { }))
            {
                value.BlockTypeReference.LoadIfNotLoaded();

                var btddc = new BlockTypeDataDataContract
                {
                    Id = value.Id,                  
                    EditOnStart = value.EditOnStart,
                    Culture = value.Culture,
                    Order = value.Order,
                    Text = value.Text,
                    Title = value.Title,
                    BlockTypeID = value.BlockType.Id
                };

                if (levelsToTranslate.TranslateRelations)
                { 
                }

                return btddc;
            }
        }

        /// <summary>
        /// Translates the service object to the business object.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <param name="value">The value.</param>
        /// <param name="levelsToTranslate">The levels to translate.</param>
        /// <returns></returns>
        protected override BlockTypeData ServiceToBusiness(IEntityTranslatorService service, BlockTypeDataDataContract value, TranslateLevel levelsToTranslate)
        {
            // Not implemented, no need to save BlockTypeData
            throw new NotImplementedException();
        }
    }


}
