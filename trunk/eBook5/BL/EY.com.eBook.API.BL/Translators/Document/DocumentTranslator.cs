﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EY.com.eBook.Core.EntityTranslation;
using EY.com.eBook.Core.EF;
using EY.com.eBook.API.Contracts.Data;
using System.Runtime.Serialization;
using System.Xml;
using EY.com.eBook.Core.EF.Read;

namespace EY.com.eBook.API.BL.Translators
{
    /// <summary>
    /// DocumentTranslator
    /// </summary>
    public class DocumentTranslator : EntityMapperTranslator<Document, DocumentDataContract>
    {
        /// <summary>
        /// Translates the business object to the service object.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <param name="value">The value.</param>
        /// <param name="levelsToTranslate">The levels to translate.</param>
        /// <returns></returns>
        protected override DocumentDataContract BusinessToService(IEntityTranslatorService service, Document value, TranslateLevel levelsToTranslate)
        {
            using (new InternalTranslators(service, new List<IEntityTranslator> { new DocumentFieldTranslator() }))
            {
                var document = new DocumentDataContract
                {
                    Id=value.Id,
                    FileId = value.File.Id,
                    DocumentTypeId = value.DocumentType.Id,
                    Culture=value.Culture,
                    Name=value.Name
                };

                if (!string.IsNullOrEmpty(value.BlockContainerData))
                {
                    document.BlockContainerData = Core.CoreHelper.DeserializeFromString<List<BlockContainerDataContract>>(value.BlockContainerData);
                }
                else
                {
                    document.BlockContainerData = new List<BlockContainerDataContract>();
                }
                if (value.Fields != null)
                {
                    document.Fields = service.TranslateList<DocumentFieldDataContract, DocumentField>(value.Fields.ToList());
                }

                return document;
            }
        }

        /// <summary>
        /// Translates the service object to the business object.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <param name="value">The value.</param>
        /// <param name="levelsToTranslate">The levels to translate.</param>
        /// <returns></returns>
        protected override Document ServiceToBusiness(IEntityTranslatorService service, DocumentDataContract value, TranslateLevel levelsToTranslate)
        {
            //using (new InternalTranslators(service, new List<IEntityTranslator> { }))
            //{
            //    Document document = EFContextManager.Context.Documents
            //                                                        .Include("File")
            //                                                        .Include("DocumentType")
            //        //.FirstOrDefault(d => d.File.Id == value.FileId && d.DocumentType.Id == value.DocumentTypeId);
            //                                                        .FirstOrDefault(d => d.Id == value.Id && d.File.Id == value.FileId && d.DocumentType.Id == value.DocumentTypeId);
            //    if (document == null)
            //    {
            //        document = new Document
            //        {
            //            Id = value.Id,
            //            File = EFContextManager.Context.Files.First(f => f.Id == value.FileId),
            //            DocumentType = EFContextManager.Context.DocumentTypes.First(d => d.Id == value.DocumentTypeId),
            //            Culture=value.Culture
            //        };
            //    }
            //    document.Name = value.Name;
                
                
            //    document.BlockContainerData = null;
            //    if (value.BlockContainerData != null)
            //    {
            //        if (value.BlockContainerData.Count > 0)
            //        {
            //            document.BlockContainerData = Core.CoreHelper.SerializeToString(value.BlockContainerData);
            //        }
            //    }
                
            //    return document;
            //}
            throw new NotImplementedException();
        }
    }
}