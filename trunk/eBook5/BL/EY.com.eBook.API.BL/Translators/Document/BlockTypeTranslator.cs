﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EY.com.eBook.Core.EF;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.Core.EntityTranslation;
using EY.com.eBook.Core.EF.Read;

namespace EY.com.eBook.API.BL.Translators
{
    /// <summary>
    /// BlockTypeViewTranslator
    /// </summary>
    public class BlockTypeTranslator : EntityMapperTranslator<BlockType, BlockTypeDataContract>
    {
        /// <summary>
        /// Translates the business object to the service object.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <param name="value">The value.</param>
        /// <param name="levelsToTranslate">The levels to translate.</param>
        /// <returns></returns>
        protected override BlockTypeDataContract BusinessToService(IEntityTranslatorService service, BlockType value, TranslateLevel levelsToTranslate)
        {
            using (new InternalTranslators(service, new List<IEntityTranslator> { new BlockTypeDataTranslator() }))
            {
                var btdc = new BlockTypeDataContract
                {
                    Id = value.Id,
                    IsEditable = value.IsEditable,
                    Key = value.Key,
                    Culture = Culture
                };
                Names name = value.Names.FirstOrDefault(n => n.Culture == Culture);
                if (name != null)
                {
                    btdc.Name = name.Description;
                }
                else
                {
                    btdc.Name = "NO TRANSLATION FOUND FOR " + Culture;
                }
                btdc.ShortName = btdc.Name.Length>30 ? btdc.Name.Substring(0, 30) + "..." : btdc.Name;
                if (value.BlockTypeData !=null)
                {

                    btdc.BlockTypeData = service.TranslateList<BlockTypeDataDataContract, BlockTypeData>(value.BlockTypeData.Where(btd => btd.Culture == Culture).OrderBy(btd=>btd.Title).ToList(), Culture);
                }
                
                return btdc;
            }
        }

        /// <summary>
        /// Translates the service object to the business object.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <param name="value">The value.</param>
        /// <param name="levelsToTranslate">The levels to translate.</param>
        /// <returns></returns>
        protected override BlockType ServiceToBusiness(IEntityTranslatorService service, BlockTypeDataContract value, TranslateLevel levelsToTranslate)
        {
            // No need to implement, it's a view in the database
            throw new NotImplementedException();
        }
    }
}
