﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EY.com.eBook.Core.EntityTranslation;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.Core.EF;
using EY.com.eBook.Core.EF.Read;

namespace EY.com.eBook.API.BL.Translators
{
    /// <summary>
    /// FileTranslator
    /// </summary>
    public class OfficeTranslator : EntityMapperTranslator<Office, OfficeDataContract>
    {
        /// <summary>
        /// Translates the business object to the service object.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <param name="value">The value.</param>
        /// <param name="levelsToTranslate">The levels to translate.</param>
        /// <returns></returns>
        protected override OfficeDataContract BusinessToService(IEntityTranslatorService service, Office value, TranslateLevel levelsToTranslate)
        {
            using (new InternalTranslators(service, new List<IEntityTranslator> { }))
            {
                var odc = new OfficeDataContract
                {
                    Id = value.Id,
                    Name = value.Name
                };
               
                return odc;
            }
        }

        /// <summary>
        /// Translates the service object to the business object.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <param name="value">The value.</param>
        /// <param name="levelsToTranslate">The levels to translate.</param>
        /// <returns></returns>
        protected override Office ServiceToBusiness(IEntityTranslatorService service, OfficeDataContract value, TranslateLevel levelsToTranslate)
        {
            throw new NotImplementedException();
        }
    }
}
