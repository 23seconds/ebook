﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EY.com.eBook.Core.EntityTranslation;
using EY.com.eBook.Core.EF;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.Core.EF.Read;
using EY.com.eBook.Core;

namespace EY.com.eBook.API.BL.Translators
{
    /// <summary>
    /// RepositoryItemNodeTranslator
    /// </summary>
    public class RepositoryItemNodeTranslator : EntityMapperTranslator<RepositoryItem, RepositoryNodeDataContract>
    {

        protected override RepositoryNodeDataContract BusinessToService(IEntityTranslatorService service, RepositoryItem value, TranslateLevel levelsToTranslate)
        {
            using (new InternalTranslators(service, new List<IEntityTranslator> { }))
            {
                string partnerGfis = null;
                if (eBookReadManager.Context.PersonClientRoleSet.Include("Persons").FirstOrDefault(pc => pc.ClientId == value.ClientId && pc.Persons.Department.ToLower() == "acr" && pc.Role.ToLower().Contains("partner") && pc.Gfis == true) != null){
                    partnerGfis = eBookReadManager.Context.PersonClientRoleSet.Include("Persons").FirstOrDefault(pc => pc.ClientId == value.ClientId && pc.Persons.Department.ToLower() == "acr" && pc.Role.ToLower().Contains("partner") && pc.Gfis == true).Persons.Gpn;
                }
                else if (eBookReadManager.Context.PersonClientRoleSet.Include("Persons").FirstOrDefault(pc => pc.ClientId == value.ClientId && pc.Persons.Department.ToLower() == "acr" && pc.Role.ToLower().Contains("partner")) != null)
                {
                    partnerGfis = eBookReadManager.Context.PersonClientRoleSet.Include("Persons").FirstOrDefault(pc => pc.ClientId == value.ClientId && pc.Persons.Department.ToLower() == "acr" && pc.Role.ToLower().Contains("partner")).Persons.Gpn;
                }
               
                var rndc = new RepositoryNodeDataContract
                {
                    AllowChildren = false
                    ,
                    AllowDrag = true
                    ,
                    Class = "eBook-Repository-node-file"
                    ,
                    Id = value.ItemId
                    ,
                    Leaf = true
                    ,
                    Text = !string.IsNullOrEmpty(value.Prequel) ? string.Format("{0} {1}", value.Prequel, value.Name) : value.Name
                    ,
                    Key = "PDFFILE"
                    ,
                    DateType = null
                    ,
                    StatusType = null
                    ,
                    StructuralType = null
                    ,
                    Files = false
                    ,
                    MetaConfig = null
                    ,
                    PeriodStart = value.StartDate
                    ,
                    PeriodEnd = value.EndDate
                    ,
                    IconCls = value.Status != null ? (value.Status.HasValue ? string.Format("eb-rep-node-{0}", value.Status.ToString().ToUpper()) : "eb-rep-node-pdf") : "eb-rep-node-pdf"
                    //IconCls = value.Status.HasValue ? string.Format(iconCls+"{0}", value.Status.ToString().ToUpper()) : iconCls+"pdf"
                    ,
                    Item = new RepositoryItemDataContract { 
                        ClientId = value.ClientId
                        , FileName = value.Name
                        , Id = value.ItemId
                        , PhysicalFileId = value.PhysicalFileId
                        , ContentType = value.Contenttype
                        , Extension = value.Extension
                        , Meta = !string.IsNullOrEmpty(value.Meta) ? CoreHelper.DeserializeFromString<List<RepositoryMetaItemDataContract>>(value.Meta) : null
                        , PeriodEnd = value.EndDate
                        , PeriodStart =value.StartDate
                        , Prequel = value.Prequel
                        , Status = value.Status
                        , StructureId = value.StructureId
                        , DocstoreAccess = eBookReadManager.Context.RepositoryStructureSet.Any(x=>x.Id == value.StructureId) ? eBookReadManager.Context.RepositoryStructureSet.FirstOrDefault(x=>x.Id == value.StructureId).DocstoreAccess : false
                        , DocstoreId = value.DocstoreId
                        , PartnerGpnAcr = partnerGfis
                        /*, PartnerGpnAcr = eBookReadManager.Context.PersonClientRoleSet.Include("Persons").FirstOrDefault(pc => pc.ClientId == value.ClientId && pc.Persons.Department.ToLower() == "acr" && pc.Role.ToLower().Contains("partner")) != null 
                        ? eBookReadManager.Context.PersonClientRoleSet.Include("Persons").FirstOrDefault(pc => pc.ClientId == value.ClientId && pc.Persons.Department.ToLower() == "acr"
                            && pc.Role.ToLower().Contains("partner")).Persons.Gpn : null*/
                    }
                   
                };
                
                return rndc;
            }
        }

        protected override RepositoryItem ServiceToBusiness(IEntityTranslatorService service, RepositoryNodeDataContract value, TranslateLevel levelsToTranslate)
        {
            // No need to implement, Person is a view in the DB
            throw new NotImplementedException();
        }
    }
}
