﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EY.com.eBook.Core.EntityTranslation;
using EY.com.eBook.Core.EF;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.Core.EF.Read;

namespace EY.com.eBook.API.BL.Translators
{
    /// <summary>
    /// RepositoryStructureFileNodeTranslator
    /// </summary>
    public class RepositoryStructureFileNodeTranslator : EntityMapperTranslator<File, RepositoryNodeDataContract>
    {

        protected override RepositoryNodeDataContract BusinessToService(IEntityTranslatorService service, File value, TranslateLevel levelsToTranslate)
        {
            using (new InternalTranslators(service, new List<IEntityTranslator> { }))
            {
                var rndc = new RepositoryNodeDataContract
                {
                    AllowChildren = false
                    ,
                    AllowDrag = false
                    ,
                    Class = "eBook-Repository-node-file-folder"
                    ,
                    Id = value.Id
                    ,
                    Leaf = false
                    ,
                    Text = value.Name
                    ,
                    Key = "PERIOD_ITEM"
                    , DateType =null
                    , StatusType=null
                    ,
                    StructuralType = "PERIOD_ITEM"
                    ,
                    Files = false
                    , PeriodStart = value.StartDate
                    ,PeriodEnd = value.EndDate
                    ,DocstoreAccess = eBookReadManager.Context.RepositoryStructureSet.FirstOrDefault(x=>x.Id == value.Id).DocstoreAccess
                };

                
                return rndc;
            }
        }

        protected override File ServiceToBusiness(IEntityTranslatorService service, RepositoryNodeDataContract value, TranslateLevel levelsToTranslate)
        {
            // No need to implement, Person is a view in the DB
            throw new NotImplementedException();
        }
    }
}
