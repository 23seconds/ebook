﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EY.com.eBook.Core.EntityTranslation;
using EY.com.eBook.Core.EF;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.Core.EF.Read;
using EY.com.eBook.Core;

namespace EY.com.eBook.API.BL.Translators
{
    /// <summary>
    /// RepositoryLinkTranslator
    /// </summary>
    public class RepositoryLinkTranslator : EntityMapperTranslator<RepositoryLink, RepositoryLinkDataContract>
    {

        protected override RepositoryLinkDataContract BusinessToService(IEntityTranslatorService service, RepositoryLink value, TranslateLevel levelsToTranslate)
        {
            using (new InternalTranslators(service, new List<IEntityTranslator> { }))
            {
               
            }
            return null;
        }

        protected override RepositoryLink ServiceToBusiness(IEntityTranslatorService service, RepositoryLinkDataContract value, TranslateLevel levelsToTranslate)
        {
            // No need to implement, Person is a view in the DB
            throw new NotImplementedException();
        }
    }
}
