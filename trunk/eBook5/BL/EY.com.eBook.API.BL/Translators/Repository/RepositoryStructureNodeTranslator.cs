﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EY.com.eBook.Core.EntityTranslation;
using EY.com.eBook.Core.EF;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.Core.EF.Read;
using EY.com.eBook.Core;

namespace EY.com.eBook.API.BL.Translators
{
    /// <summary>
    /// RepositoryStructureNodeTranslator
    /// </summary>
    public class RepositoryStructureNodeTranslator : EntityMapperTranslator<RepositoryStructure, RepositoryNodeDataContract>
    {

        protected override RepositoryNodeDataContract BusinessToService(IEntityTranslatorService service, RepositoryStructure value, TranslateLevel levelsToTranslate)
        {
            using (new InternalTranslators(service, new List<IEntityTranslator> { }))
            {
                var rndc = new RepositoryNodeDataContract
                {
                    AllowChildren = value.Folders && value.Files
                    ,
                    AllowDrag = false
                    ,
                    Class = "eBook-Repository-node-" + value.Key
                    ,
                    Id = value.Id
                    ,
                    Leaf = false
                    ,
                    Text = value.Key
                    , Key = value.Key
                    , DateType = value.DateType
                    , DateInterval = value.DateInterval
                    , StatusType = value.StatusType
                    , StructuralType = value.StructuralType
                    , Files = value.Files
                    , AllowLinks = value.AllowLinks
                    , MetaConfig = !string.IsNullOrEmpty(value.MetaConfig) ? CoreHelper.DeserializeFromString<RepositoryMetaConfigDataContract>(value.MetaConfig) : null
                    , Order=value.Order
                    , DocstoreAccess = eBookReadManager.Context.RepositoryStructureSet.FirstOrDefault(x => x.Id == value.Id).DocstoreAccess
                    ,
                    Dynamic = value.Id == new Guid("e611d384-4488-4371-aa2d-22224667aa8e") // just for coda for now = quick fix
                };

                if (value.Translations.IsLoaded)
                {
                    if (!string.IsNullOrEmpty(Culture))
                    {
                        RepositoryStructureTranslation rst = value.Translations.FirstOrDefault(r => r.Culture == Culture);
                        if (rst != null)
                        {
                            rndc.Text = rst.Description;
                        }
                    }
                }
                return rndc;
            }
        }

        protected override RepositoryStructure ServiceToBusiness(IEntityTranslatorService service, RepositoryNodeDataContract value, TranslateLevel levelsToTranslate)
        {
            // No need to implement, Person is a view in the DB
            throw new NotImplementedException();
        }
    }
}
