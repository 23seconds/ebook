﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EY.com.eBook.Core.EntityTranslation;
using EY.com.eBook.Core.EF;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.Core.EF.Read;

namespace EY.com.eBook.API.BL
{
    public class SupplierTranslator : EntityMapperTranslator<BusinessRelation, SupplierDataContract>
    {
        protected override SupplierDataContract BusinessToService(IEntityTranslatorService service, BusinessRelation value, TranslateLevel levelsToTranslate)
        {
            using (new InternalTranslators(service, new List<IEntityTranslator> { }))
            {
                var sdc = new SupplierDataContract
                {
                    ZipCode = !string.IsNullOrEmpty(value.Zip) ? value.Zip.Trim() : null,
                    Vat = value.VatNumber,
                    LastName = value.Name,
                    FirstName = value.FirstName,
                    ImportedId = value.ImportedId,
                    Id = value.Id,
                    CompanyNr = value.CompanyNr,
                    Country = value.Country,
                    ClientId = value.Client.Id,
                    City = value.City,
                    BusinessRelationType = value.Type[0],
                    Address = value.Address
                    //Birthdate = value.Birthdate,
                    //CurrencyId = value.CurrencyId,
                    //StartDate = value.StartDate,
                    //EndDate = value.EndDate
                };

                if (levelsToTranslate.TranslateRelations)
                {
                }

                return sdc;
            }
        }

        protected override BusinessRelation ServiceToBusiness(IEntityTranslatorService service, SupplierDataContract value, TranslateLevel levelsToTranslate)
        {
            throw new NotImplementedException();
        }
    }
}
