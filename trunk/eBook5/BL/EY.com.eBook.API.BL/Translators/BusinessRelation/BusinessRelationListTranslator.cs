﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EY.com.eBook.Core.EntityTranslation;
using EY.com.eBook.Core.EF;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.Core.EF.Read;

namespace EY.com.eBook.API.BL
{
    public class BusinessRelationListTranslator : EntityMapperTranslator<BusinessRelation, BusinessRelationListDataContract>
    {
        protected override BusinessRelationListDataContract BusinessToService(IEntityTranslatorService service, BusinessRelation value, TranslateLevel levelsToTranslate)
        {
            using (new InternalTranslators(service, new List<IEntityTranslator> { }))
            {
                var brdc = new BusinessRelationListDataContract
                {
                   Name = string.IsNullOrEmpty(value.FirstName) ? value.Name : string.IsNullOrEmpty(value.Name) ? value.FirstName : string.Format("{0} {1}", value.FirstName, value.Name)
                   , Id = value.Id             
                };

                if (levelsToTranslate.TranslateRelations)
                {
                }

                return brdc;
            }
        }

        protected override BusinessRelation ServiceToBusiness(IEntityTranslatorService service, BusinessRelationListDataContract value, TranslateLevel levelsToTranslate)
        {
            throw new NotImplementedException();
        }
    }


    public class CustomerListTranslator : EntityMapperTranslator<BusinessRelation, CustomerListDataContract>
    {
        protected override CustomerListDataContract BusinessToService(IEntityTranslatorService service, BusinessRelation value, TranslateLevel levelsToTranslate)
        {
            using (new InternalTranslators(service, new List<IEntityTranslator> { }))
            {
                var brdc = new CustomerListDataContract
                {
                   Name = string.IsNullOrEmpty(value.FirstName) ? value.Name : string.IsNullOrEmpty(value.Name) ? value.FirstName : string.Format("{0} {1}", value.FirstName, value.Name)
                   , Id = value.Id             
                };

                if (levelsToTranslate.TranslateRelations)
                {
                }

                return brdc;
            }
        }

        protected override BusinessRelation ServiceToBusiness(IEntityTranslatorService service, CustomerListDataContract value, TranslateLevel levelsToTranslate)
        {
            throw new NotImplementedException();
        }
    }

    public class SupplierListTranslator : EntityMapperTranslator<BusinessRelation, SupplierListDataContract>
    {
        protected override SupplierListDataContract BusinessToService(IEntityTranslatorService service, BusinessRelation value, TranslateLevel levelsToTranslate)
        {
            using (new InternalTranslators(service, new List<IEntityTranslator> { }))
            {
                var brdc = new SupplierListDataContract
                {
                   Name = string.IsNullOrEmpty(value.FirstName) ? value.Name : string.IsNullOrEmpty(value.Name) ? value.FirstName : string.Format("{0} {1}", value.FirstName, value.Name)
                   , Id = value.Id             
                };

                if (levelsToTranslate.TranslateRelations)
                {
                }

                return brdc;
            }
        }

        protected override BusinessRelation ServiceToBusiness(IEntityTranslatorService service, SupplierListDataContract value, TranslateLevel levelsToTranslate)
        {
            throw new NotImplementedException();
        }
    }
}
