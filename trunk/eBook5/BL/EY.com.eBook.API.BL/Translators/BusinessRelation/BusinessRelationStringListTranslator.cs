﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EY.com.eBook.Core.EntityTranslation;
using EY.com.eBook.Core.EF;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.Core.EF.Read;

namespace EY.com.eBook.API.BL
{
    public class BusinessRelationStringListTranslator : EntityMapperTranslator<BusinessRelation, StringListItemDataContract>
    {
        protected override StringListItemDataContract BusinessToService(IEntityTranslatorService service, BusinessRelation value, TranslateLevel levelsToTranslate)
        {
            using (new InternalTranslators(service, new List<IEntityTranslator> { }))
            {
                var brdc = new StringListItemDataContract
                {
                   NL = string.IsNullOrEmpty(value.FirstName) ? value.Name : string.IsNullOrEmpty(value.Name) ? value.FirstName : string.Format("{0} {1}", value.FirstName, value.Name)
                   , Id = value.Id.ToString()         
                };
                brdc.FR = brdc.NL;
                brdc.EN = brdc.NL;

                if (levelsToTranslate.TranslateRelations)
                {
                }

                return brdc;
            }
        }

        protected override BusinessRelation ServiceToBusiness(IEntityTranslatorService service, StringListItemDataContract value, TranslateLevel levelsToTranslate)
        {
            throw new NotImplementedException();
        }
    }

}
