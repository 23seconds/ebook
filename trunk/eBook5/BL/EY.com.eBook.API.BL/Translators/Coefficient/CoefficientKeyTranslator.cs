﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EY.com.eBook.Core.EntityTranslation;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.Core.EF;
using EY.com.eBook.Core.EF.Read;
using EY.com.eBook.Core;

namespace EY.com.eBook.API.BL.Translators
{
    /// <summary>
    /// CoefficienGroupTranslator
    /// </summary>
    public class CoefficientKeyTranslator : EntityMapperTranslator<CoefficientKey, CoefficientKeyDataContract>
    {
        /// <summary>
        /// Translates the business object to the service object.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <param name="value">The value.</param>
        /// <param name="levelsToTranslate">The levels to translate.</param>
        /// <returns></returns>
        protected override CoefficientKeyDataContract BusinessToService(IEntityTranslatorService service, CoefficientKey value, TranslateLevel levelsToTranslate)
        {
            using (new InternalTranslators(service, new List<IEntityTranslator> { }))
            {
                Names nme = value.Names.FirstOrDefault(n => n.Culture == Culture);
                CoefficientKeySettingsDataContract cksdc = CoreHelper.DeserializeFromString<CoefficientKeySettingsDataContract>(value.Settings);

                return new CoefficientKeyDataContract
                {
                    Id = value.Id,
                    Key = value.Key,
                    Description = nme != null ? nme.Description : value.Key,
                    Settings = cksdc

                };

            }
        }

        /// <summary>
        /// Translates the service object to the business object.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <param name="value">The value.</param>
        /// <param name="levelsToTranslate">The levels to translate.</param>
        /// <returns></returns>
        protected override CoefficientKey ServiceToBusiness(IEntityTranslatorService service, CoefficientKeyDataContract value, TranslateLevel levelsToTranslate)
        {
            throw new NotImplementedException();
        }
    }
}
