﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EY.com.eBook.Core.EntityTranslation;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.Core.EF;
using EY.com.eBook.Core.EF.Read;

namespace EY.com.eBook.API.BL.Translators
{
    /// <summary>
    /// CoefficientTranslator
    /// </summary>
    public class CoefficientTranslator : EntityMapperTranslator<Coefficients, CoefficientDataContract>
    {
        /// <summary>
        /// Translates the business object to the service object.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <param name="value">The value.</param>
        /// <param name="levelsToTranslate">The levels to translate.</param>
        /// <returns></returns>
        protected override CoefficientDataContract BusinessToService(IEntityTranslatorService service, Coefficients value, TranslateLevel levelsToTranslate)
        {
            using (new InternalTranslators(service, new List<IEntityTranslator> { }))
            {
                return new CoefficientDataContract
                {
                   Key = value.Key
                   , Value = value.Value
                   , AssessmentYear = value.AssessmentYear
                   , EndDate = value.EndDate
                   , StartDate = value.StartDate
                   , Description = value.Description
                   , Id = value.Id
                };
            }
        }

        /// <summary>
        /// Translates the service object to the business object.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <param name="value">The value.</param>
        /// <param name="levelsToTranslate">The levels to translate.</param>
        /// <returns></returns>
        protected override Coefficients ServiceToBusiness(IEntityTranslatorService service, CoefficientDataContract value, TranslateLevel levelsToTranslate)
        {
            throw new NotImplementedException();
        }
    }
}
