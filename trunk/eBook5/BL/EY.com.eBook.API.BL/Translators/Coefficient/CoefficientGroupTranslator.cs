﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EY.com.eBook.Core.EntityTranslation;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.Core.EF;
using EY.com.eBook.Core.EF.Read;

namespace EY.com.eBook.API.BL.Translators
{
    /// <summary>
    /// CoefficienGroupTranslator
    /// </summary>
    public class CoefficientGroupTranslator : EntityMapperTranslator<CoefficientGroup, CoefficientGroupDataContract>
    {
        /// <summary>
        /// Translates the business object to the service object.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <param name="value">The value.</param>
        /// <param name="levelsToTranslate">The levels to translate.</param>
        /// <returns></returns>
        protected override CoefficientGroupDataContract BusinessToService(IEntityTranslatorService service, CoefficientGroup value, TranslateLevel levelsToTranslate)
        {
            using (new InternalTranslators(service, new List<IEntityTranslator> { }))
            {
                Names nme = value.Names.FirstOrDefault(n => n.Culture == Culture);
                return  new CoefficientGroupDataContract
                {
                    Id = value.Id,
                    Key = value.Key,
                    Description = nme!=null ? nme.Description : value.Key
                };
                
            }
        }

        /// <summary>
        /// Translates the service object to the business object.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <param name="value">The value.</param>
        /// <param name="levelsToTranslate">The levels to translate.</param>
        /// <returns></returns>
        protected override CoefficientGroup ServiceToBusiness(IEntityTranslatorService service, CoefficientGroupDataContract value, TranslateLevel levelsToTranslate)
        {
            throw new NotImplementedException();
        }
    }
}
