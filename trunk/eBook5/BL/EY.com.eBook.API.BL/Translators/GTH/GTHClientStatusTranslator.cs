﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EY.com.eBook.Core.EntityTranslation;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.Core.EF;
using EY.com.eBook.Core.EF.Read;

namespace EY.com.eBook.API.BL.Translators
{
    /// <summary>
    /// GTHClientStatusTranslator
    /// </summary>
    public class GTHClientStatusTranslator : EntityMapperTranslator<GTHTeamClient, GTHClientStatusDataContract>
    {
        /// <summary>
        /// Translates the business object to the service object.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <param name="value">The value.</param>
        /// <param name="levelsToTranslate">The levels to translate.</param>
        /// <returns></returns>
        protected override GTHClientStatusDataContract BusinessToService(IEntityTranslatorService service, GTHTeamClient value, TranslateLevel levelsToTranslate)
        {
            using (new InternalTranslators(service, new List<IEntityTranslator> { }))
            {
                var odc = new GTHClientStatusDataContract
                {
                    ClientEnterprise = !string.IsNullOrEmpty(value.ClientEnterprise) ? value.ClientEnterprise : value.ClientVat
                    ,
                    ClientId = value.ClientId
                    ,
                    ClientName = value.ClientName
                    , ClientGfis= value.ClientGfis
                    ,
                    Id = value.Id
                    ,
                    LastUpdate = value.LastUpdate
                    ,
                    PersonId = value.PersonId
                    ,
                    PersonName = string.Format("{0} {1}", value.PersonFirst, value.PersonLast)
                    ,
                    Status = value.Status
                    ,
                    StartDate = value.PeriodStart
                    ,
                    EndDate = value.PeriodEnd
                    ,
                    TeamId = value.TeamId
                };
               
                return odc;
            }
        }

        /// <summary>
        /// Translates the service object to the business object.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <param name="value">The value.</param>
        /// <param name="levelsToTranslate">The levels to translate.</param>
        /// <returns></returns>
        protected override GTHTeamClient ServiceToBusiness(IEntityTranslatorService service, GTHClientStatusDataContract value, TranslateLevel levelsToTranslate)
        {
            throw new NotImplementedException();
        }
    }
}
