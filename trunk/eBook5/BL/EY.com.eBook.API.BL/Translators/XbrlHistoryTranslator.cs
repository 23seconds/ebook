﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EY.com.eBook.Core.EntityTranslation;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.Core.EF;
using EY.com.eBook.Core.EF.Read;

namespace EY.com.eBook.API.BL.Translators
{
    /// <summary>
    /// XbrlHistoryTranslator
    /// </summary>
    public class XbrlHistoryTranslator : EntityMapperTranslator<FileXbrlHistory, XbrlHistoryDataContract>
    {
        /// <summary>
        /// Translates the business object to the service object.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <param name="value">The value.</param>
        /// <param name="levelsToTranslate">The levels to translate.</param>
        /// <returns></returns>
        protected override XbrlHistoryDataContract BusinessToService(IEntityTranslatorService service, FileXbrlHistory value, TranslateLevel levelsToTranslate)
        {
            using (new InternalTranslators(service, new List<IEntityTranslator> { }))
            {
                var odc = new XbrlHistoryDataContract
                {
                    FileId = value.FileId
                    , ActionId = value.ActionId
                    , ActionText = value.ActionText
                    , TimeStamp = value.Date
                    , PersonFullName = value.PersonFullName
                    , PersonId = value.PersonId
                   // , Type = value.t
                };
               
                return odc;
            }
        }

        /// <summary>
        /// Translates the service object to the business object.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <param name="value">The value.</param>
        /// <param name="levelsToTranslate">The levels to translate.</param>
        /// <returns></returns>
        protected override FileXbrlHistory ServiceToBusiness(IEntityTranslatorService service, XbrlHistoryDataContract value, TranslateLevel levelsToTranslate)
        {
            throw new NotImplementedException();
        }
    }
}
