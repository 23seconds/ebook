﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Collections;
using EY.com.eBook.API.Contracts.Data.Cache;
using EY.com.eBook.Core.EF;
using EY.com.eBook.Core.EF.Read;
using System.Threading;
using EY.com.eBook.API.Contracts.Data;
using System.Data.Objects;
using System.Runtime.Serialization;

namespace EY.com.eBook.Caching
{
    

    public static class ListExtensionMethods
    {
        public static List<S> IndexedList<T, S>(this Dictionary<T, S> dic, List<T> keyList)
        {
            List<S> valueList = new List<S>();
            foreach (T key in keyList)
            {
                if (dic.ContainsKey(key))
                {
                    valueList.Add(dic[key]);
                }
            }
            return valueList;
        }

        public static List<S> IndexedList<T, S>(this Hashtable dic, List<T> keyList)
        {
            List<S> valueList = new List<S>();
            foreach (T key in keyList)
            {
                valueList.Add((S)dic[key]);
            }
            return valueList;
        }

        public static List<T> ElementsAt<T>(this List<T> list, List<int> indexes) where T : class
        {
            if(indexes == null) return new List<T>();
            if (indexes.Count == 0) return new List<T>();
            List<T> elements = new List<T>();
            indexes.ForEach(i => elements.Add(list[i]));
            return elements;
        }

        public static void RemoveAt<T>(this List<T> list, List<int> indexes) where T : class
        {
            if (indexes == null) return;
            if (indexes.Count == 0) return;
            
            foreach (int idx in indexes.OrderByDescending(i=>i))
            {
                list.RemoveAt(idx);
            }
        }

    }

    [DataContract]
    public class SchemaCacheItem : SchemaQuery
    {

        public SchemaCacheItem()
        {
            // overkill ?

            Schema = new AccountSchemaWorkSet();

            // overkill ?

          
        }

        [DataMember]
        public Guid Id { get; set; }
        
        //public FileTypeDataContract FileType { get; set; }

        [DataMember]
        public FileDataContract DataContract { get; set; }

        [DataMember]
        public FileSettingsDataContract Settings { get; set; }

        [DataMember]
        public ClientBaseDataContract Client { get; set; }

        [DataMember]
        public List<FileMessageDataContract> Messages { get; set; }

        [DataMember]
        public List<BookingDataContract> ManualBookings { get; set; }

        [DataMember]
        public List<BookingDataContract> AutomaticBookings { get; set; }

        // by account public nr
        [DataMember]
        public override AccountSchemaWorkSet Schema { get; set; }

        /*
        #region Schema Query

        private void EnrichAccountData(ref List<AccountDataContract> accounts, bool includeDescriptions, bool includeMappings, string culture)
        {
            List<AccountDescriptionDataContract> addcs;
            AccountDescriptionDataContract addc;


            foreach (AccountDataContract account in accounts)
            {
                addcs = GetAccountDescriptionsPrivate(account.InternalNr);
                if (includeMappings) account.Mappings = GetMappingsPrivate(account.InternalNr);
                if (includeDescriptions) account.Descriptions = addcs;
                addc = addcs.FirstOrDefault(d => d.Culture == culture);
                account.DefaultDescription = addc != null ? addc.Description : "NO TRANSLATION";
            }

        }

        private void EnrichAccountData(ref AccountDataContract account, bool includeDescriptions, bool includeMappings, string culture)
        {
            List<AccountDescriptionDataContract> addcs;
            AccountDescriptionDataContract addc;

            addcs = GetAccountDescriptionsPrivate(account.InternalNr);
            if (includeMappings) account.Mappings = GetMappingsPrivate(account.InternalNr);
            if (includeDescriptions) account.Descriptions = addcs;
            addc = addcs.FirstOrDefault(d => d.Culture == culture);
            account.DefaultDescription = addc != null ? addc.Description : "NO TRANSLATION";

        }



        public AccountDataContract GetAccount(string accountNr, bool includeDescriptions, bool includeMappings, string culture)
        {
            AccountDataContract account = null;

            if (Schema.Accounts.ContainsKey(accountNr))
            {
                account = Schema.Accounts[accountNr];
            }
            EnrichAccountData(ref account, includeDescriptions, includeMappings, culture);



            return account;
        }

        public List<AccountDataContract> GetAccounts(bool includeDescriptions, bool includeMappings, string culture)
        {
            return GetAccounts(true, true, includeDescriptions, includeMappings, culture);
        }

        public List<AccountDataContract> GetAccounts(bool zeroSaldi, bool withSaldi, bool includeDescriptions, bool includeMappings, string culture)
        {
            List<AccountDataContract> accounts = new List<AccountDataContract>();
            if (!zeroSaldi && !withSaldi) return accounts;


            if (zeroSaldi && withSaldi)
            {

                accounts = Schema.Accounts.Values.ToList();

            }
            else if ((!zeroSaldi && withSaldi) || (zeroSaldi && !withSaldi))
            {

                if ((!zeroSaldi && withSaldi)) accounts = Schema.Accounts.IndexedList(Schema.SaldiAccounts);
                if ((zeroSaldi && !withSaldi)) accounts = Schema.Accounts.IndexedList(Schema.ZeroAccounts);


            }

            EnrichAccountData(ref accounts, includeDescriptions, includeMappings, culture);

            return accounts;
        }

        public List<AccountDataContract> GetAccountsInRanges(List<string> ranges, bool includeDescriptions, bool includeMappings, string culture)
        {
            return GetAccountsInRanges(ranges, true, true, includeDescriptions, includeMappings, culture);
        }

        public List<AccountDataContract> GetAccountsInRanges(List<string> ranges, bool zeroSaldi, bool withSaldi, bool includeDescriptions, bool includeMappings, string culture)
        {
            if (!zeroSaldi && !withSaldi) return new List<AccountDataContract>();
            // build keys query
            string rangeQueryFormat = "StartsWith(@{0})";
            string query = "{0}";
            StringBuilder rangeBuilder = new StringBuilder();
            List<object> variables = new List<object>();

            foreach (var range in ranges)
            {
                if (rangeBuilder.Length > 0) rangeBuilder.Append(" || ");
                variables.Add(range);
                rangeBuilder.Append(string.Format(rangeQueryFormat, (variables.Count - 1)));

            }

            query = string.Format(query, rangeBuilder.ToString());

            List<string> keys = new List<string>();
            List<AccountDataContract> accounts = new List<AccountDataContract>();


            if (zeroSaldi) keys.AddRange(Schema.ZeroAccounts);
            if (withSaldi) keys.AddRange(Schema.SaldiAccounts);


            keys = keys.AsQueryable().Where(query, variables.ToArray()).Distinct().ToList();

            accounts = Schema.Accounts.IndexedList(keys);

            EnrichAccountData(ref accounts, includeDescriptions, includeMappings, culture);



            return accounts;
        }

        public List<AccountDataContract> GetAccountsOfMapping(string key, bool includeEmpties, bool includeDescriptions, bool includeMappings, string culture)
        {
            return GetAccountsOfMappingMeta(null, key, includeEmpties, includeDescriptions, includeMappings, culture);
        }

        public List<AccountDataContract> GetAccountsOfMapping(string key, bool includeEmpties, bool zeroSaldi, bool withSaldi, bool includeDescriptions, bool includeMappings, string culture)
        {
            return GetAccountsOfMappingMeta(null, key, includeEmpties, zeroSaldi, withSaldi, includeDescriptions, includeMappings, culture);
        }

        // key is the old value ?
        public List<AccountDataContract> GetAccountsOfMappingMeta(string meta, string key, bool includeEmpties, bool includeDescriptions, bool includeMappings, string culture)
        {

            return GetAccountsOfMappingMeta(meta, key, includeEmpties, true, true, includeDescriptions, includeMappings, culture);
        }

        public List<AccountDataContract> GetAccountsOfMappingMeta(string meta, string key, bool includeEmpties, bool zeroSaldi, bool withSaldi, bool includeDescriptions, bool includeMappings, string culture)
        {
            List<int> mapIdxs = new List<int>();
            List<string> accountKeys = new List<string>();
            List<AccountDataContract> accounts = new List<AccountDataContract>();
            if (!zeroSaldi && !withSaldi) return accounts;


            mapIdxs = Schema.AccountMappingMappedKeyIndex[key];
            if (!string.IsNullOrEmpty(meta))
            {
                if (includeEmpties)
                {
                    accountKeys = Schema.AccountMappings.ElementsAt(mapIdxs).Where(m => m.MappingMeta == meta).Select(m => m.InternalNr).Distinct().ToList();
                }
                else
                {
                    accountKeys = Schema.AccountMappings.ElementsAt(mapIdxs).Where(m => m.MappingMeta == meta && m.MappingItemId.HasValue).Select(m => m.InternalNr).Distinct().ToList();
                }
            }
            else
            {
                if (includeEmpties)
                {
                    accountKeys = Schema.AccountMappings.ElementsAt(mapIdxs).Select(m => m.InternalNr).Distinct().ToList();
                }
                else
                {
                    accountKeys = Schema.AccountMappings.ElementsAt(mapIdxs).Where(a => a.MappingItemId.HasValue).Select(m => m.InternalNr).Distinct().ToList();
                }
            }

            if (!zeroSaldi) accountKeys.RemoveAll(a => Schema.ZeroAccounts.Contains(a));
            if (!withSaldi) accountKeys.RemoveAll(a => Schema.SaldiAccounts.Contains(a));

            accounts = Schema.Accounts.IndexedList(accountKeys);

            EnrichAccountData(ref accounts, includeDescriptions, includeMappings, culture);




            return accounts;
        }


        public List<AccountDescriptionDataContract> GetAccountDescriptions(string accountNr)
        {
            List<AccountDescriptionDataContract> ads = new List<AccountDescriptionDataContract>();


            if (Schema.AccountDescriptionsIndex.ContainsKey(accountNr))
            {
                ads = Schema.AccountDescriptions.ElementsAt(Schema.AccountDescriptionsIndex[accountNr]);
            }

            return ads;
        }


        public List<AccountMappingDataContract> GetAccountMappings(string accountNr)
        {
            List<AccountMappingDataContract> ams = new List<AccountMappingDataContract>();

            if (Schema.AccountMappingsAccountIndex.ContainsKey(accountNr))
            {
                ams = Schema.AccountMappings.ElementsAt(Schema.AccountMappingsAccountIndex[accountNr]);
            }


            return ams;
        }

        private List<AccountMappingDataContract> GetMappingsPrivate(string accountNr)
        {
            if (!Schema.AccountMappingsAccountIndex.ContainsKey(accountNr)) return new List<AccountMappingDataContract>();
            return Schema.AccountMappings.ElementsAt(Schema.AccountMappingsAccountIndex[accountNr]);
        }

        private List<AccountDescriptionDataContract> GetAccountDescriptionsPrivate(string accountNr)
        {
            if (!Schema.AccountDescriptionsIndex.ContainsKey(accountNr)) return new List<AccountDescriptionDataContract>();
            return Schema.AccountDescriptions.ElementsAt(Schema.AccountDescriptionsIndex[accountNr]);
        }

        public List<AccountDataContract> GetAccountsAdjustedTree(bool includeDescriptions, bool includeMappings, string culture)
        {
            List<string> haveAdjustments = new List<string>();
            List<string> parentkeys = new List<string>();
            List<string> adjusted = new List<string>();
            List<AccountDataContract> adjustments = new List<AccountDataContract>();
            List<AccountDataContract> parentAccounts = new List<AccountDataContract>();


            haveAdjustments = Schema.ParentAdjustedIndex.Keys.ToList();
            parentkeys = Schema.SaldiAccounts.Where(a => Schema.ParentAccounts.Contains(a) || haveAdjustments.Contains(a)).ToList();
            adjusted = Schema.AdjustedAccounts;

            adjustments = Schema.Accounts.IndexedList(adjusted);
            parentAccounts = Schema.Accounts.IndexedList(parentkeys);

            EnrichAccountData(ref adjustments, includeDescriptions, includeMappings, culture);
            EnrichAccountData(ref parentAccounts, includeDescriptions, includeMappings, culture);



            foreach (AccountDataContract adc in parentAccounts.Where(a => haveAdjustments.Contains(a.InternalNr)))
            {
                adc.Adjustments = adjustments.Where(a => a.InternalNr.StartsWith(adc.InternalNr)).OrderBy(a => a.InternalNr).ToList();
            }
            return parentAccounts;
        }



        #endregion

        */
    }
}
