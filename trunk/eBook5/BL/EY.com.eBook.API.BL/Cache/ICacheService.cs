﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using EY.com.eBook.Core;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.Core.Data;
using EY.com.eBook.API.Contracts.Data.Cache;
using EY.com.eBook.Caching;

namespace EY.com.eBook.API.Contracts.Services
{
    [ServiceContract]
    public interface ICacheService
    {

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        bool Initialize();

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        void TestException(int id);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        void TestCommunicatedException(int id);
        
        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        void TestSave(EY.com.eBook.RuleEngine.Core.Interfaces.IWorksheet fac);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        bool ReloadMainCache();

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        bool FileInCache(Guid fileId);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        void RemoveFileFromCache(Guid fileId);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        bool ReloadFileInCache(Guid fileId);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        bool ReloadOrAddFileInCache(Guid fileId);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        bool ReloadOrAddFileInCacheForce(Guid fileId, bool force);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        EY.com.eBook.Caching.WorksheetWorkSetDataContract GetWorksheetSet(Guid fileId);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<WorksheetTypeTranslationDataContract> GetWorksheetTypeTranslations(Guid typeId, string culture);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        void SaveWorksheetSetToCache(WorksheetLibrary wsb, Guid fileId);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> GetMessages(CriteriaFileDataContract cfdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> GetMessagesWithoutNotes(CriteriaFileDataContract cfdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<FileMessageDataContract> GetNotes(CriteriaFileDataContract cfdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<BookingDataContract> GetManualBookings(Guid fileId);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<BookingDataContract> GetAutomaticBookings(Guid fileId);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<BookingDataContract> GetAllBookings(Guid fileId);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        void UpdateBookings(Guid fileId);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<BookingDataContract> GetWorksheetBookings(Guid fileId, Guid typeId);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<AccountDataContract> GetAllAccounts(Guid fileId, bool includeDescriptions, bool includeMappings, string culture);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        AccountDataContract GetAccount(Guid fileId, string accountNr, bool includeDescriptions, bool includeMappings, string culture);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<AccountDataContract> GetAccountsByRanges(Guid fileId, List<string> ranges, bool includeDescriptions, bool includeMappings, string culture);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<AccountDataContract> GetAccountsOfMapping(Guid fileId, string mappingKey, bool includeDescriptions, bool includeMappings, string culture);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<AccountDescriptionDataContract> GetAccountTranslations(Guid fileId, string accountNr);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<AccountMappingDataContract> GetAccountMappings(Guid fileId, string accountNr);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<AccountDataContract> GetAccountsAdjustedTree(Guid fileId, bool includeDescriptions, bool includeMappings, string culture, bool? includezeroes, bool? mappedOnly, string query);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<MappingItemDataContract> GetMappingItemsOfKey(string key);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<CoefficientDataContract> GetMappingCoefficients(Guid id);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        List<MappingItemDataContract> GetMappingItemsOfKeyMeta(string key, string meta);


        #region Updates 
        
        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        void AddAccounts(List<AccountDataContract> adcs);

        //[OperationContract]
        //[FaultContract(typeof(DefaultFaultContract))]
        //void UpdateAccounts(List<AccountDataContract> adcs);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        void RemoveAccounts(Guid fileId, List<string> accountNrs);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        void ResetAccountsSaldi(Guid fileId, List<AccountSaldiUpdate> asus);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        void AddAccountDescriptions(List<AccountDescriptionDataContract> addcs);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        void UpdateAccountDescriptions(List<AccountDescriptionDataContract> addcs);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        void RemoveAccountDescriptions(List<AccountDescriptionDataContract> addcs);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        void AddAccountMappings(List<AccountMappingDataContract> amdcs);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        void UpdateAccountMappings(List<AccountMappingDataContract> amdcs);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        void RemoveAccountMappings(List<AccountMappingDataContract> amdcs);


        #endregion

    }
}
