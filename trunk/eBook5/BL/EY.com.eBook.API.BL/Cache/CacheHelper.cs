﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.ServiceModel.Configuration;
using EY.com.eBook.API.Contracts.Services;
using EY.com.eBook.API.Contracts.Proxies;
using System.ServiceModel;
using System.Web.Configuration;
using System.Web;

namespace EY.com.eBook.API.BL
{
    public class CacheHelper : IDisposable
    {
        private string _configName;
        private bool _cacheServiceConfigured;
        public bool CacheRunning {get;set;}
        private CacheProxy _cacheProxy;

        public CacheHelper()
        {
            Configuration config;
            if (HttpContext.Current != null)
                config = WebConfigurationManager.OpenWebConfiguration("~");
            else
                config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

            ServiceModelSectionGroup smsg = ServiceModelSectionGroup.GetSectionGroup(config);

            if (smsg.Client == null) return;

            foreach (ChannelEndpointElement cee in smsg.Client.Endpoints)
            {
                if (cee.Contract == typeof(ICacheService).FullName)
                {
                    _configName = cee.Name;
                    _cacheServiceConfigured = true;
                    break;
                }
            }
        }

        public bool CacheServiceConfigured
        {
            get
            {
                return _cacheServiceConfigured;
            }
        }

        public void CreateCacheProxy()
        {
            if (_cacheServiceConfigured)
            {
                _cacheProxy = new CacheProxy();
                try
                {
                    _cacheProxy.Open();
                    CacheRunning = true;
                    
                }
                catch
                {
                    CacheRunning = false;
                }
            }
        }

        public CacheProxy GetCacheProxy()
        {
            if (_cacheProxy != null && _cacheServiceConfigured)
            {
                if (_cacheProxy.State == CommunicationState.Opened)
                {
                    return _cacheProxy;
                }
                else
                {
                    try
                    {
                        if (_cacheProxy.State == CommunicationState.Opened)
                            _cacheProxy.Close();
                        else if (_cacheProxy.State == CommunicationState.Faulted)
                            _cacheProxy.Abort();
                    }
                    catch
                    {
                        try
                        {
                            _cacheProxy.Abort();
                        }
                        catch { }
                    }
                    CreateCacheProxy();
                    return _cacheProxy;
                }
            }
            else
            {
                if (_cacheServiceConfigured)
                {
                    CreateCacheProxy();
                    return _cacheProxy;
                }
            }
            return null;
        }
    
        #region IDisposable Members

        public void Dispose()
        {
            if (_cacheProxy != null)
            {
                try
                {
                    if (_cacheProxy.State == CommunicationState.Opened)
                        _cacheProxy.Close();
                    else if (_cacheProxy.State == CommunicationState.Faulted)
                        _cacheProxy.Abort();
                }
                catch
                {
                    try
                    {
                        _cacheProxy.Abort();
                    }
                    catch { }
                }
            }
            _cacheProxy = null;

        }

        #endregion
}
}
