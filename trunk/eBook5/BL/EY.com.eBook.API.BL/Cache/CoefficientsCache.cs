﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Collections;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.Core.EF.Read;
using EY.com.eBook.Core.EF;

namespace EY.com.eBook.Caching
{
    public class CoefficientsCache
    {
        private static CoefficientsCache _current = new CoefficientsCache();

        private ReaderWriterLockSlim _itemsLock = new ReaderWriterLockSlim();
        private ReaderWriterLockSlim _indexLock = new ReaderWriterLockSlim();

        private static Hashtable _items = new Hashtable();

        private static Dictionary<Guid, List<Guid>> _listItemIdx = new Dictionary<Guid,List<Guid>>();
        private static Dictionary<string, List<Guid>> _keyedIdx = new Dictionary<string,List<Guid>>();
        private static Dictionary<string, List<Guid>> _groupedIdx = new Dictionary<string,List<Guid>>();
        private static Dictionary<int, List<Guid>> _assessmentIdx = new Dictionary<int,List<Guid>>();

        public int CachedItemsNumber
        {
            get
            {
                _itemsLock.EnterReadLock();
                try
                {
                    return _items.Count;
                }
                finally
                {
                    _itemsLock.ExitReadLock();
                }
            }
        }

        private CoefficientsCache()
        {
           
        }

        public static CoefficientsCache Current
        {
            get
            {
                return _current;
            }
        }

        public object this[object key]
        {
            get
            {
                CoefficientDataContract res = null;
                _itemsLock.EnterReadLock();
                try
                {
                    res = (CoefficientDataContract)_items[key];
                }
                finally
                {
                    _itemsLock.ExitReadLock();
                }
                return res ;
            }
            set
            {
                _itemsLock.EnterWriteLock();
                try
                {
                   
                    _items[key] = new CacheItem(value);
                }
                finally
                {
                    _itemsLock.ExitWriteLock();
                }
            }
        }


        public void Update(Hashtable items, Dictionary<Guid, List<Guid>> listitemIdx
            , Dictionary<string, List<Guid>> keyedIdx, Dictionary<string, List<Guid>> groupedIdx
            , Dictionary<int, List<Guid>> assessmentIndex)
        {
            bool lck = _itemsLock.TryEnterWriteLock(6000);
            if (lck)
            {
                try
                {
                    _items = items;
                }
                finally
                {
                    _itemsLock.ExitWriteLock();
                }
            }

            bool lckIdx = _indexLock.TryEnterWriteLock(6000);
            if (lckIdx)
            {
                try
                {
                    _keyedIdx = keyedIdx;
                    _groupedIdx = groupedIdx;
                    _listItemIdx = listitemIdx;
                    _assessmentIdx = assessmentIndex;
                }
                finally
                {
                    _indexLock.ExitWriteLock();
                }
            }
        }

        public bool InBetween(DateTime periodStart1, DateTime periodEnd1, DateTime periodStart2, DateTime periodEnd2)
        {
            if (periodStart1 <= periodStart2)
            {
                return periodEnd2 > periodStart1;
            }
            else
            {
                return periodStart1 < periodEnd2;
            }
        }

        public CoefficientsWorkingCopy GetWorkingCopy(int assessmentyear, DateTime? startDate, DateTime? endDate)
        {
            CoefficientsWorkingCopy cwc = new CoefficientsWorkingCopy();

            List<Guid> assCoefIds = new List<Guid>();
            _indexLock.EnterReadLock();
            try
            {
                assCoefIds = _assessmentIdx[assessmentyear];
                cwc.ListItemsIdx = _listItemIdx;
                cwc.KeysIdx = _keyedIdx;
                cwc.GroupsIdx = _groupedIdx;
            }
            finally
            {
                _indexLock.ExitReadLock();
            }
            _itemsLock.EnterReadLock();
            cwc.Items = new IndexedCoefficientDataContract();
            try
            {
                foreach (Guid id in assCoefIds)
                {
                    CoefficientDataContract cdc = (CoefficientDataContract)_items[id];
                    if (startDate.HasValue && endDate.HasValue && cdc.StartDate.HasValue && cdc.EndDate.HasValue)
                    {
                        if (InBetween(cdc.StartDate.Value, cdc.EndDate.Value, startDate.Value, endDate.Value))
                            cwc.Items.Add(id, cdc);
                    }
                    else
                    {
                        cwc.Items.Add(id, cdc);
                    }     
                }
            }
            finally
            {
                _itemsLock.ExitReadLock();
            }
            return cwc;
        }


        public List<CoefficientDataContract> GetListItemCoeffs(Guid id)
        {
            List<CoefficientDataContract> cofs = new List<CoefficientDataContract>();
            if (!_listItemIdx.ContainsKey(id)) return cofs;
            foreach (Guid mid in _listItemIdx[id])
            {
                cofs.Add((CoefficientDataContract)this[mid]);
            }
            return cofs;
        }
    }
}
