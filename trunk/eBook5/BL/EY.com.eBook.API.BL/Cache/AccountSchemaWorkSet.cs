﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using EY.com.eBook.API.Contracts;
using EY.com.eBook.API.Contracts.Data.Cache;
using EY.com.eBook.Core;

namespace EY.com.eBook.Caching
{
    [DataContract]
    public class AccountSchemaWorkSet
    {
        public AccountSchemaWorkSet()
        {
            Accounts = new IndexedAccountsDataContract();
            ZeroAccounts = new List<string>();
            SaldiAccounts = new List<string>();
            AdjustedAccounts = new List<string>();
            ParentAccounts = new List<string>();
            ParentAdjustedIndex = new IndexedKeysDataContract();
            AccountDescriptions = new List<AccountDescriptionDataContract>();
            AccountDescriptionsIndex = new IndexListDataContract();
            AccountMappings = new List<AccountMappingDataContract>();
            AccountMappingMappedKeyIndex = new IndexListDataContract();
            AccountMappingsAccountIndex = new IndexListDataContract();
        }

        [DataMember(Order=1)]
        public IndexedAccountsDataContract Accounts { get; set; }

        [DataMember(Order = 2)]
        public List<string> ZeroAccounts { get; set; }

        [DataMember(Order = 3)]
        public List<string> SaldiAccounts { get; set; }

        [DataMember(Order = 4)]
        public List<string> AdjustedAccounts { get; set; }

        [DataMember(Order = 5)]
        public List<string> ParentAccounts { get; set; }

        [DataMember(Order = 6)]
        public IndexedKeysDataContract ParentAdjustedIndex { get; set; }

        [DataMember(Order = 7)]
        public List<AccountDescriptionDataContract> AccountDescriptions { get; set; }

        [DataMember(Order = 8)]
        public IndexListDataContract AccountDescriptionsIndex { get; set; }

        [DataMember(Order = 9)]
        public List<AccountMappingDataContract> AccountMappings { get; set; }
        [DataMember(Order = 10)]
        public IndexListDataContract AccountMappingsAccountIndex { get; set; }
        [DataMember(Order = 11)]
        public IndexListDataContract AccountMappingMappedKeyIndex { get; set; }

        
    }
}
