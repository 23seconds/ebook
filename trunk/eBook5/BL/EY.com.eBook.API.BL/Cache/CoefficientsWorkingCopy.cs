﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Runtime.Serialization;

namespace EY.com.eBook.Caching
{
    [DataContract]
    public class CoefficientsWorkingCopy
    {
        [DataMember(Order=1)]
        public int AssessmentYear { get; set; }

        [DataMember(Order = 2)]
        public IndexedCoefficientDataContract Items { get; set; }

        [DataMember(Order = 3)]
        public Dictionary<Guid, List<Guid>> ListItemsIdx { get; set; }
        [DataMember(Order = 4)]
        public Dictionary<string, List<Guid>> KeysIdx { get; set; }
        [DataMember(Order = 5)]
        public Dictionary<string, List<Guid>> GroupsIdx { get; set; }
        
    }
}
