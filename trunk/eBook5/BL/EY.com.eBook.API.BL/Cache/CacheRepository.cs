﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.API.BL;
using EY.com.eBook.Core.EF;
using EY.com.eBook.Core.EF.Read;
using System.Collections;
using EY.com.eBook.RuleEngine.Data;
using EY.com.eBook.Core;
using System.Xml.Linq;
using System.ServiceModel;
using EY.com.eBook.Core.Data;
using EY.com.eBook.API.Contracts.Data.Cache;

namespace EY.com.eBook.Caching
{
    public class CacheRepository
    {
        public bool ReloadFileInCache(Guid fileId, bool createIfNonExisting)
        {
            bool ok = true;
            try
            {
                
                if (FileInCache(fileId))
                {
                    LoadFileInCache(fileId);
                }
                else
                {
                    if (createIfNonExisting) LoadFileInCache(fileId);
                    // or automatic load in cache?
                }
            }
            catch
            {
                ok = false;
            }
            return ok;
        }

        public bool FileInCache(Guid fileId)
        {
            return FileCache.Current[FileCache.SchemaKey + fileId.ToString()] != null;
        }

        public void RemoveFileFromCache(Guid fileId)
        {
            FileCache.Current.Remove(FileCache.SchemaKey + fileId.ToString());
            FileCache.Current.Remove(FileCache.WorksheetskKey + fileId.ToString());
        }

        public void LoadFileInCache(Guid fileId)
        {
            if (FileInCache(fileId))
                FileCache.Current.Remove(FileCache.SchemaKey + fileId.ToString());

            SchemaCacheItem fc = new SchemaCacheItem
            {
                Id = fileId
            };
            
            FileCache.Current.Insert(FileCache.SchemaKey +  fileId.ToString(), fc, new TimeSpan(0, 5, 0), true);
            FileCache.Current.ReloadFileInCache(fileId);
        }


        public List<FileMessageDataContract> GetMessages(CriteriaFileDataContract cfdc)
        {
            SchemaCacheItem sc = GetCachedFileOrThrow(cfdc.FileId);
            return sc.Messages.Where(m=>m.Culture==cfdc.Culture).ToList();
        }

        public List<FileMessageDataContract> GetMessagesWithoutNotes(CriteriaFileDataContract cfdc)
        {
            SchemaCacheItem sc = GetCachedFileOrThrow(cfdc.FileId);
            return sc.Messages.Where(m => m.Culture == cfdc.Culture && m.Type != MessageTypeDataContract.Note).ToList();
        }

        public List<FileMessageDataContract> GetNotes(CriteriaFileDataContract cfdc)
        {
            SchemaCacheItem sc = GetCachedFileOrThrow(cfdc.FileId);
            return sc.Messages.Where(m =>m.Culture==cfdc.Culture && m.Type == MessageTypeDataContract.Note).ToList();
        }

        public List<BookingDataContract> GetAutomaticBookings(Guid fileId)
        {
            SchemaCacheItem sc = GetCachedFileOrThrow(fileId);
            return sc.AutomaticBookings;
        }

        public List<BookingDataContract> GetManualBookings(Guid fileId)
        {
            SchemaCacheItem sc = GetCachedFileOrThrow(fileId);
            return sc.ManualBookings;
        }

        public List<BookingDataContract> GetAllBookings(Guid fileId)
        {
            SchemaCacheItem sc = GetCachedFileOrThrow(fileId);
            List<BookingDataContract> bookings = new List<BookingDataContract>();
            if (sc.AutomaticBookings!=null) bookings.AddRange(sc.AutomaticBookings);
            if (sc.ManualBookings != null) bookings.AddRange(sc.ManualBookings);
            return bookings.OrderBy(b => b.CreationDate).ToList();
        }

        public List<BookingDataContract> GetWorksheetBookings(Guid fileId, Guid typeId)
        {
            SchemaCacheItem sc = GetCachedFileOrThrow(fileId);
            return sc.AutomaticBookings.Where(b=>b.WorksheetTypeId.HasValue && b.WorksheetTypeId.Value==typeId).ToList();
        }

        public AccountSchemaWorkSet GetAccountSchemaWorkSet(Guid fileId)
        {
            SchemaCacheItem sci = (SchemaCacheItem)FileCache.Current[FileCache.SchemaKey + fileId.ToString()];
            if (sci == null) return new AccountSchemaWorkSet();
            return CoreHelper.DeepCopy<AccountSchemaWorkSet>(sci.Schema);
        }

        public WorksheetWorkSetDataContract GetWorksheetSet(Guid fileId)
        {
            string key = FileCache.WorksheetskKey + fileId.ToString();
            WorksheetLibrary wl = (WorksheetLibrary)FileCache.Current[key];
            if (wl == null)
            {
                wl = new WorksheetLibrary();
            }
            WorksheetWorkSetDataContract wwsdc = new WorksheetWorkSetDataContract();
            wwsdc.Worksheets = wl;
            SchemaCacheItem sci = (SchemaCacheItem)FileCache.Current[FileCache.SchemaKey + fileId.ToString()];
            wwsdc.DataContract = sci.DataContract;
            wwsdc.Client = sci.Client;
            wwsdc.Settings = sci.Settings;
            wwsdc.Schema = sci.Schema;
            wwsdc.Messages = sci.Messages;
            wwsdc.AutoBookings = sci.AutomaticBookings;
            wwsdc.Coeffs = CoefficientsCache.Current.GetWorkingCopy(sci.DataContract.AssessmentYear, sci.DataContract.StartDate, sci.DataContract.EndDate);


            //return wwsdc;
            return CoreHelper.DeepCopy<WorksheetWorkSetDataContract>(wwsdc);
        }

        public void SaveWorksheetSetToCache(WorksheetLibrary wsb, Guid fileId)
        {
            //wsb.setHelper.Coeffs = null;
            //wsb.setHelper.FileCache = null;
            
            FileCache.Current.Insert(FileCache.WorksheetskKey + fileId.ToString(), wsb, new TimeSpan(0, 20, 0), true);
        }

        public bool LoadStaticDataToCache() 
        {
            try
            {

                int cnt = 0;
                Logger.Info("CACHE Start loading static data");
                LoadCoeffsFromDB();
                Logger.Info("CACHE Coefficients loaded " + CoefficientsCache.Current.CachedItemsNumber);
                cnt = LoadWorksheetTypeTranslations();
                Logger.Info("CACHE Worksheet type translations loaded " + cnt);
                cnt = LoadMessageTranslations();
                Logger.Info("CACHE Message translations loaded " + cnt);
                cnt = LoadMappings();
                Logger.Info("CACHE Mappings loaded " + cnt);
                Logger.Info("CACHE static data load Finished");
                return true;
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                throw;
            }
            
        }

        public void LoadCoeffsFromDB()
        {

            List<Coefficients> coeffs = eBookReadManager.Context.CoefficientsSet.Include("Group").Include("ListItem").ToList();

            List<MappingItemCoefficient> micoeffs = eBookReadManager.Context.MappingItemCoefficients.ToList();


            Dictionary<Guid, List<Guid>> listItemIndex = new Dictionary<Guid, List<Guid>>();
            Dictionary<string, List<Guid>> keyIndex = new Dictionary<string, List<Guid>>();
            Dictionary<string, List<Guid>> groupIndex = new Dictionary<string, List<Guid>>();
            Dictionary<int, List<Guid>> assessmentIndex = new Dictionary<int, List<Guid>>();

            foreach (Guid mid in micoeffs.Select(m=>m.MappingItemId).Distinct())
            {
                listItemIndex.Add(mid, micoeffs.Where(m=>m.MappingItemId==mid).Select(m=>m.CoeffId).Distinct().ToList());
            }

            Hashtable ht = new Hashtable();
            foreach (Coefficients coef in coeffs)
            {
                ht.Add(coef.Id, new CoefficientDataContract
                {
                    AssessmentYear = coef.AssessmentYear
                    ,
                    EndDate = coef.EndDate
                    ,
                    GroupKey = coef.Group.Key
                    ,
                    Key = coef.Key
                    ,
                    StartDate = coef.StartDate
                    ,
                    Value = coef.Value
                });

                if (!keyIndex.ContainsKey(coef.Key)) keyIndex.Add(coef.Key, new List<Guid>());
                if (!groupIndex.ContainsKey(coef.Group.Key)) groupIndex.Add(coef.Group.Key, new List<Guid>());

                keyIndex[coef.Key].Add(coef.Id);
                groupIndex[coef.Group.Key].Add(coef.Id);

                if (coef.AssessmentYear.HasValue)
                {
                    if (!assessmentIndex.ContainsKey(coef.AssessmentYear.Value)) assessmentIndex.Add(coef.AssessmentYear.Value, new List<Guid>());
                    assessmentIndex[coef.AssessmentYear.Value].Add(coef.Id);
                }
                else
                {
                    foreach (int ay in GetAssessmentsFromDates(coef.StartDate, coef.EndDate))
                    {
                        if (!assessmentIndex.ContainsKey(ay)) assessmentIndex.Add(ay, new List<Guid>());
                        assessmentIndex[ay].Add(coef.Id);
                    }
                }


                foreach (ListItem li in coef.ListItem)
                {
                    if (!listItemIndex.ContainsKey(li.Id))
                    {
                        listItemIndex.Add(li.Id, new List<Guid>());

                    }
                    listItemIndex[li.Id].Add(coef.Id);
                }

            }

            CoefficientsCache.Current.Update(ht, listItemIndex, keyIndex, groupIndex, assessmentIndex);

        }

        public List<CoefficientDataContract> GetMappingCoefficients(Guid id)
        {
            return CoefficientsCache.Current.GetListItemCoeffs(id);

        }

        public List<int> GetAssessmentsFromDates(DateTime? start, DateTime? end)
        {
            if (!start.HasValue) start = new DateTime(DateTime.Now.AddYears(-4).Year, 1, 1);
            if (!end.HasValue) end = new DateTime(DateTime.Now.AddYears(2).Year, 12, 30);
            List<int> ays = new List<int>();
            int s = start.Value.Year;
            for (int i = start.Value.Year; i <= (end.Value.AddDays(1).Year+1); i++)
            {
                ays.Add(i);
            }
            return ays;

        }

        public SchemaCacheItem GetCachedFile(Guid fileId)
        {
            return GetCachedFileOrThrow(fileId);
        }

        public SchemaCacheItem GetCachedFileOrThrow(Guid fileId)
        {
            SchemaCacheItem sci = (SchemaCacheItem)FileCache.Current[FileCache.SchemaKey + fileId.ToString()];
            if (sci == null)
            {
                throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { ErrorCode = -99, Message = "CACHE-MISS", FileId = fileId });
            }
            if (sci.AutomaticBookings == null) sci.AutomaticBookings = new List<BookingDataContract>();
            if (sci.ManualBookings == null) sci.ManualBookings = new List<BookingDataContract>();
            if (sci.Messages == null) sci.Messages = new List<FileMessageDataContract>();
            return sci;
        }

        public int LoadIWorksheetClasses(Dictionary<string, List<Type>> lst)
        {
            List<EY.com.eBook.Core.EF.Write.WorksheetType> worksheetTypes = EY.com.eBook.Core.EF.eBookWriteManager.Context.WorksheetTypes.ToList();
            List<WorksheetTypeMetaDataContract> wtTypeMeta = new List<WorksheetTypeMetaDataContract>();

            foreach (string ruleAppKey in lst.Keys)
            {
                wtTypeMeta.Add(new WorksheetTypeMetaDataContract {
                    WorksheetTypeId = worksheetTypes.First(wt=>wt.RuleApp ==ruleAppKey).Id
                    , IWorksheetClasses = lst[ruleAppKey].Select(t=>t.Name).Distinct().ToList()
                    , RuleAppName = ruleAppKey
                });
            }
            GlobalCache.Current.Insert(GlobalCacheKey.WorksheetTypeMeta.Value, wtTypeMeta);
            return wtTypeMeta.Count;

        }

        public int LoadWorksheetTypeTranslations()
        {
            List<WorksheetTypeTranslationDataContract> typeTranslations = new EY.com.eBook.API.BL.Read.TranslationsRepository().GetAllWorksheetTypeTranslationsFromDb();

            List<Guid> distinctIds = typeTranslations.Select(i => i.WorksheetTypeId).Distinct().ToList();

            foreach (Guid typeId in distinctIds)
            {
                GlobalCache.Current.Insert(GlobalCacheKey.WorksheetTypeTranslation.Value + typeId.ToString(), typeTranslations.Where(t => t.WorksheetTypeId == typeId).ToList());
            }
            return distinctIds.Count;
        }



        public string GetWorksheetTypeTabTranslation(Guid typeId, string tab,string type, string culture)
        {
            List<WorksheetTypeMetaDataContract> wtMetaList = (List<WorksheetTypeMetaDataContract>)GlobalCache.Current[GlobalCacheKey.WorksheetTypeMeta.Value];
            List<WorksheetTypeTranslationDataContract> wttdcs = (List<WorksheetTypeTranslationDataContract>)GlobalCache.Current[GlobalCacheKey.WorksheetTypeTranslation.Value + typeId.ToString()];

            WorksheetTypeMetaDataContract wtMeta = wtMetaList.First(m => m.WorksheetTypeId == typeId);

            if (wttdcs == null) return tab;

            WorksheetTypeTranslationDataContract wttdc = wttdcs.FirstOrDefault(w => w.Culture == culture && w.EntityOrCollection == wtMeta.IWorksheetClasses.First()
                                                                                && w.FieldName == tab && w.OutputType == type);

            if (wttdc != null) return wttdc.Translation;
            return tab;

        }


        public string GetWorksheetTypeTranslation(Guid typeId, string collection, string field, string key, string type,string culture)
        {
            if (string.IsNullOrEmpty(field) && !string.IsNullOrEmpty(key)) field = key;
            List<WorksheetTypeTranslationDataContract> wttdcs = (List<WorksheetTypeTranslationDataContract>)GlobalCache.Current[GlobalCacheKey.WorksheetTypeTranslation.Value + typeId.ToString()];
            if (wttdcs == null) return string.Empty;

            WorksheetTypeTranslationDataContract wttdc = wttdcs.FirstOrDefault(w => w.Culture == culture && w.EntityOrCollection == collection
                                                                                && w.FieldName == field && w.OutputType == type);

            if (wttdc == null && type.ToLower() != "label")
            {
                wttdc = wttdcs.FirstOrDefault(w => w.Culture == culture && w.EntityOrCollection == collection
                                                                                && w.FieldName == field && w.OutputType == "label");
                if (wttdc == null) return "no label";
            }
            
            if (wttdc == null) return string.Empty;
            
            return wttdc.Translation;
            
        }

        public List<WorksheetTypeTranslationDataContract> GetWorksheetTypeTranslations(Guid typeId, string culture)
        {
            List<WorksheetTypeTranslationDataContract> wttdcs = (List<WorksheetTypeTranslationDataContract>)GlobalCache.Current[GlobalCacheKey.WorksheetTypeTranslation.Value + typeId.ToString()];
            if (wttdcs == null) return new List<WorksheetTypeTranslationDataContract>();

            return wttdcs.Where(w => w.Culture == culture).ToList();
        }

        public int LoadMessageTranslations()
        {
            EY.com.eBook.API.BL.Read.TranslationsRepository tr = new EY.com.eBook.API.BL.Read.TranslationsRepository();
            List<MessageTranslationDataContract> translations = tr.GetAllMessageTranslationsFromDb();
            foreach (MessageTranslationDataContract translation in translations)
            {
                GlobalCache.Current.Insert(GlobalCacheKey.MessageTranslation.Value + translation.Id.ToString(), translations.Where(m => m.Id == translation.Id).ToList());
            }

            List<WorksheetTypeMessageTranslationDataContract> wtTranlsations = tr.GetAllWorksheetTypeMessageTranslationsFromDb();

            foreach (Guid wtId in wtTranlsations.Select(w=>w.WorksheetTypeId).Distinct())
            {
                GlobalCache.Current.Insert(GlobalCacheKey.WorksheetTypeMessageTranslation + wtId.ToString()
                    , (from m in wtTranlsations.Where(w => w.WorksheetTypeId == wtId)
                       select new WorksheetTypeMessageTranslationDataContract
                       {
                           WorksheetTypeId = m.WorksheetTypeId
                           ,
                           MessageId = m.MessageId
                           ,
                           Collection = m.Collection
                           ,
                           FieldName = m.FieldName
                           ,
                           Key = m.Key
                           ,
                           Translations = translations.Where(mt => mt.Id == m.MessageId).ToList()
                       }).ToList());
            }
            return translations.Count;

        }

        public WorksheetTypeMessageTranslationDataContract GetWorksheetTypeMessageTranslations(Guid worksheetTypeId, string messageKey, string worksheetTab, string fieldName)
        {
            List<WorksheetTypeMessageTranslationDataContract> msgTrans = (List<WorksheetTypeMessageTranslationDataContract>)GlobalCache.Current[GlobalCacheKey.WorksheetTypeMessageTranslation + worksheetTypeId.ToString()];
            WorksheetTypeMessageTranslationDataContract wtmtdc = null;
            if (msgTrans != null)
            {
                wtmtdc = msgTrans.Where(m => m.Key == messageKey && m.Collection == worksheetTab && m.FieldName == fieldName).FirstOrDefault();
            }
            return wtmtdc;
        }

        public Guid? GetWorksheetTypeMessageId(Guid worksheetTypeId, string messageKey, string worksheetTab, string fieldName)
        {
            List<WorksheetTypeMessageTranslationDataContract> msgTrans = (List<WorksheetTypeMessageTranslationDataContract>)GlobalCache.Current[GlobalCacheKey.WorksheetTypeMessageTranslation + worksheetTypeId.ToString()];
            WorksheetTypeMessageTranslationDataContract wtmtdc = null;
            if (msgTrans != null)
            {
                wtmtdc = msgTrans.Where(m => m.Key == messageKey && m.Collection == worksheetTab && m.FieldName == fieldName).FirstOrDefault();
                if (wtmtdc != null) return wtmtdc.MessageId;
            }
            return null;
        }

        public void SaveWorksheetsFileMessages(List<FileMessageDataContract> messages, Guid fileId)
        {
            if (messages == null) return;
            //if (messages.Count == 0) return;

            string key = FileCache.SchemaKey + fileId.ToString();
            SchemaCacheItem sci = (SchemaCacheItem)FileCache.Current[key];
            List<FileMessageDataContract> worksheetMsgs = sci.Messages.Where(m => m.ConnectionType == FileMessageConnectionTypeDataContract.WORKSHEET).ToList();

            List<FileMessageDataContract> adds = messages.Except(worksheetMsgs, new FileMessageComparer(true)).ToList(); ;
            List<FileMessageDataContract> removes = worksheetMsgs.Except(messages, new FileMessageComparer(true)).ToList(); ;
            List<FileMessageDataContract> updates = messages.Except(worksheetMsgs, new FileMessageComparer(false)).ToList(); ;

            updates = updates.Except(adds).ToList();

            // update cache
            if (adds.Count > 0)
            {                
                sci.Messages.AddRange(adds);
            }
            if (removes.Count > 0)
            {
                foreach (FileMessageDataContract fmdc in removes)
                {
                    sci.Messages.Remove(fmdc);
                }
            }

            if (updates.Count > 0)
            {
                foreach (FileMessageDataContract fmdc in updates)
                {
                    FileMessageDataContract exist = sci.Messages.FirstOrDefault(m => m.FileId == fmdc.FileId
                                                      && m.MessageId == fmdc.MessageId && m.Type == fmdc.Type
                                                      && m.Culture == fmdc.Culture && m.ConnectionType == fmdc.ConnectionType
                                                      && m.ConnectionAccount == fmdc.ConnectionAccount
                                                      && m.ConnectionGuid == fmdc.ConnectionGuid
                                                      && m.WorksheetTypeCollection == fmdc.WorksheetTypeCollection
                                                      && m.WorksheetTypeField == fmdc.WorksheetTypeField
                                                      && m.WorksheetTypeRowId == fmdc.WorksheetTypeRowId
                                                      && m.WorksheetTypeCollectionText == fmdc.WorksheetTypeCollectionText
                                                      && m.WorksheetTypeFieldText == fmdc.WorksheetTypeFieldText);
                    if (exist == null)
                    {
                        sci.Messages.Add(fmdc);
                    }
                    else
                    {
                        exist.Text = fmdc.Text;
                    }
                }
                
            }

            // UPDATE DATABASE - MASS
            // IMPLEMENT STORED PROCEDURE FOR MASS UPDATE
            //string toSqlXml = XElement.Parse(CoreHelper.SerializeToString(messages)).StripNamespaces().ToString();

        }

        public void SaveWorksheetBookings(List<Guid> BookingsToAdd, List<Guid> BookingsToUpdate, List<Guid> BookingsToDelete, List<BookingDataContract> AutoBookings, Guid fileID)
        {
            if (BookingsToAdd.Count == 0 && BookingsToUpdate.Count == 0 && BookingsToDelete.Count == 0) return;

            List<AccountSaldiUpdate> accounts = new List<AccountSaldiUpdate>();
            List<string> nrs = AutoBookings.Where(b => BookingsToAdd.Contains(b.Id)
                || BookingsToUpdate.Contains(b.Id)
                || BookingsToDelete.Contains(b.Id)
                ).SelectMany(b => b.BookingLines)
                .Select(bl => bl.AccountNr.Id)
                .Distinct()
                .ToList();

            

            SchemaCacheItem sci = GetCachedFile(fileID);
            
            List<string> deleteNrs = sci.AutomaticBookings.Where(b => BookingsToDelete.Contains(b.Id)
                ).SelectMany(b => b.BookingLines)
                .Select(bl => bl.AccountNr.Id)
                .Distinct()
                .ToList();
            nrs.AddRange(deleteNrs);

            sci.AutomaticBookings = AutoBookings;

            foreach (string account in nrs)
            {
                AccountDataContract adc = sci.Schema.Accounts[account];
                decimal? auto = AutoBookings.SelectMany(b => b.BookingLines).Where(bl => bl.AccountNr.Id == account).Sum(bl => bl.Amount);
                decimal? manual = sci.ManualBookings.SelectMany(b => b.BookingLines).Where(bl => bl.AccountNr.Id == account).Sum(bl => bl.Amount);
                decimal bookedSaldo = (auto.HasValue ? auto.Value:0) + (manual.HasValue ? manual.Value:0);


                accounts.Add(new AccountSaldiUpdate
                {
                    AccountNr = account
                    ,
                    Saldo = adc.StartSaldo+bookedSaldo
                    , PreviousSaldo = adc.PreviousSaldo
                    , StartSaldo = adc.StartSaldo
                });
            }

            FileCache.Current.ResetAccountsSaldi(accounts, fileID);
            
            // UPDATE DATABASE
            API.BL.Write.AccountSchemaRepository asr = new EY.com.eBook.API.BL.Write.AccountSchemaRepository();
            foreach (BookingDataContract bdc in AutoBookings.Where(b => BookingsToAdd.Contains(b.Id) || BookingsToUpdate.Contains(b.Id)))
            {
                asr.SaveBooking(bdc);
            }
            foreach (Guid id in BookingsToDelete)
            {
                asr.DeleteBooking(id);
            }


        }

        public int LoadMappings()
        {
            EY.com.eBook.API.BL.Read.MappingRepository mr = new EY.com.eBook.API.BL.Read.MappingRepository();
            List<MappingItemDataContract> mappingItems = mr.GetAllMappingItems();

            Dictionary<string, List<string>> mappingKeyIdx = new Dictionary<string, List<string>>();
            Dictionary<string, List<string>> mappingKeyMetaIdx = new Dictionary<string, List<string>>();

            foreach (MappingItemDataContract mi in mappingItems)
            {
                string key =GlobalCacheKey.MappingItem.Value + mi.Id.ToString();
                if (GlobalCache.Current[key] != null) GlobalCache.Current.Remove(key);
                GlobalCache.Current.Insert(key, mi);
                if (!mappingKeyIdx.ContainsKey(mi.Key)) mappingKeyIdx.Add(mi.Key, new List<string>());
                if (!mappingKeyIdx[mi.Key].Contains(key)) mappingKeyIdx[mi.Key].Add(key);
                if(!string.IsNullOrEmpty(mi.Meta)) {
                    string metaKey = mi.Key+mi.Meta;
                    if (!mappingKeyMetaIdx.ContainsKey(metaKey)) mappingKeyMetaIdx.Add(metaKey, new List<string>());
                    if (!mappingKeyMetaIdx[metaKey].Contains(key)) mappingKeyMetaIdx[metaKey].Add(key);
                }
            }
            
            if (GlobalCache.Current["MappingKeyIdx"] != null) GlobalCache.Current.Remove("MappingKeyIdx");
            GlobalCache.Current.Insert("MappingKeyIdx", mappingKeyIdx);

            if (GlobalCache.Current["MappingKeyMetaIdx"] != null) GlobalCache.Current.Remove("MappingKeyMetaIdx");
            GlobalCache.Current.Insert("MappingKeyMetaIdx", mappingKeyMetaIdx);
            return mappingItems.Count;

        }

        public List<MappingItemDataContract> GetMappingItemsOfKey(string key)
        {
            List<MappingItemDataContract> mis = new List<MappingItemDataContract>();
            Dictionary<string, List<string>> mappingKeyIdx = (Dictionary<string, List<string>>)GlobalCache.Current["MappingKeyIdx"];
            if (mappingKeyIdx == null) return mis;
            if (!mappingKeyIdx.ContainsKey(key)) return mis;
            foreach (string cacheId in mappingKeyIdx[key])
            {
                MappingItemDataContract midc = (MappingItemDataContract)GlobalCache.Current[cacheId];
                if (midc != null) mis.Add(midc);
            }
            return mis;
        }

        public List<MappingItemDataContract> GetMappingItemsOfKeyMeta(string key, string meta)
        {
            List<MappingItemDataContract> mis = new List<MappingItemDataContract>();
            Dictionary<string, List<string>> mappingKeyMetaIdx = (Dictionary<string, List<string>>)GlobalCache.Current["MappingKeyMetaIdx"];
            if (mappingKeyMetaIdx == null) return mis;
            if (!mappingKeyMetaIdx.ContainsKey(key+meta)) return mis;
            foreach (string cacheId in mappingKeyMetaIdx[key + meta])
            {
                MappingItemDataContract midc = (MappingItemDataContract)GlobalCache.Current[cacheId];
                if (midc != null) mis.Add(midc);
            }
            return mis;
        }

        internal void UpdateBookings(Guid fileId)
        {
            FileCache.Current.ReloadBookingsFromDatabase(fileId);
        }
    }

    public class FileMessageComparer : IEqualityComparer<FileMessageDataContract>
    {
        private bool _onlyNew;
        public FileMessageComparer(bool onlyNew)
        {
            _onlyNew = onlyNew;
        }

        #region IEqualityComparer<FileMessageDataContract> Members

        public bool Equals(FileMessageDataContract x, FileMessageDataContract y)
        {
            bool result = x.FileId == y.FileId && x.MessageId == y.MessageId
                && x.Type == y.Type && x.Culture == y.Culture
                && x.ConnectionAccount.Equals(y.ConnectionAccount)
                && x.ConnectionGuid == y.ConnectionGuid
                && x.ConnectionType == y.ConnectionType
                && x.WorksheetTypeCollection.Equals(y.WorksheetTypeCollection)
                && x.WorksheetTypeField.Equals(y.WorksheetTypeField)
                && x.WorksheetTypeRowId == y.WorksheetTypeRowId;
            if (!_onlyNew) result = result && x.Text.Equals(y.Text,StringComparison.OrdinalIgnoreCase);
            return result;
        }

        public int GetHashCode(FileMessageDataContract obj)
        {
            //Check whether the object is null
            if (Object.ReferenceEquals(obj, null)) return 0;

            int hash = obj.FileId == null ? 0 : obj.FileId.GetHashCode();
            hash ^= obj.MessageId == null ? 0 : obj.MessageId.GetHashCode();
            hash ^= obj.Type == null ? 0 : obj.Type.GetHashCode();
            hash ^= obj.Culture == null ? 0 : obj.Culture.GetHashCode();
            hash ^= obj.ConnectionAccount == null ? 0 : obj.ConnectionAccount.GetHashCode();
            hash ^= obj.ConnectionGuid == null ? 0 : obj.ConnectionGuid.GetHashCode();
            hash ^= obj.ConnectionType == null ? 0 : obj.ConnectionType.GetHashCode();
            hash ^= obj.WorksheetTypeCollection == null ? 0 : obj.WorksheetTypeCollection.GetHashCode();
            hash ^= obj.WorksheetTypeField == null ? 0 : obj.WorksheetTypeField.GetHashCode();
            hash ^= obj.WorksheetTypeRowId == null ? 0 : obj.WorksheetTypeRowId.GetHashCode();

            if (!_onlyNew) hash ^= obj.Text == null ? 0 : obj.Text.GetHashCode();

            return hash;
        }

        #endregion
    }

}
