﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using EY.com.eBook.API.Contracts.Services;
using EY.com.eBook.Caching;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.Core.Data;

namespace EY.com.eBook.API.Contracts.Proxies
{
    public class CacheProxy : ClientBase<ICacheService>, ICacheService
    {
        #region ICacheService Members

        public bool Initialize()
        {
            return Channel.Initialize();
        }

        public void TestException(int id)
        {
            Channel.TestException(id);
            
        }

        public void TestCommunicatedException(int id)
        {
            Channel.TestCommunicatedException(id);
        }

        public void TestSave(EY.com.eBook.RuleEngine.Core.Interfaces.IWorksheet fac)
        {
            Channel.TestSave(fac);
        }

        public bool ReloadMainCache()
        {
            return Channel.ReloadMainCache();
        }

        public bool FileInCache(Guid fileId)
        {
            
                return Channel.FileInCache(fileId);
            
        }

        public void RemoveFileFromCache(Guid fileId)
        {
            Channel.RemoveFileFromCache(fileId);
        }

        public bool ReloadFileInCache(Guid fileId)
        {
            return Channel.ReloadFileInCache(fileId);
        }

        public bool ReloadOrAddFileInCache(Guid fileId)
        {
            return Channel.ReloadOrAddFileInCache(fileId);
        }

        public bool ReloadOrAddFileInCacheForce(Guid fileId, bool force)
        {
            return Channel.ReloadOrAddFileInCacheForce(fileId, force);
        }

        public WorksheetWorkSetDataContract GetWorksheetSet(Guid fileId)
        {
            return Channel.GetWorksheetSet(fileId);
        }
        public List<WorksheetTypeTranslationDataContract> GetWorksheetTypeTranslations(Guid typeId, string culture)
        {
            return Channel.GetWorksheetTypeTranslations(typeId, culture);
        }

        public void SaveWorksheetSetToCache(WorksheetLibrary wsb, Guid fileId)
        {
            Channel.SaveWorksheetSetToCache(wsb,fileId);
        }

        public List<FileMessageDataContract> GetMessages(CriteriaFileDataContract cfdc)
        {
            return Channel.GetMessages(cfdc);
        }

        public List<FileMessageDataContract> GetMessagesWithoutNotes(CriteriaFileDataContract cfdc)
        {
            return Channel.GetMessagesWithoutNotes(cfdc);
        }

        public List<FileMessageDataContract> GetNotes(CriteriaFileDataContract cfdc)
        {
            return Channel.GetNotes(cfdc); ;
        }

        public List<BookingDataContract> GetManualBookings(Guid fileId)
        {
            return Channel.GetManualBookings(fileId);
        }

        public List<BookingDataContract> GetAutomaticBookings(Guid fileId)
        {
            return Channel.GetAutomaticBookings(fileId);
        }

        public List<BookingDataContract> GetAllBookings(Guid fileId)
        {
            return Channel.GetAllBookings(fileId);
        }

        public void UpdateBookings(Guid fileId)
        {
            Channel.UpdateBookings(fileId);
        }

        public List<BookingDataContract> GetWorksheetBookings(Guid fileId, Guid typeId)
        {
            return Channel.GetWorksheetBookings(fileId,typeId);
        }


        public List<EY.com.eBook.API.Contracts.Data.Cache.AccountDataContract> GetAllAccounts(Guid fileId, bool includeDescriptions, bool includeMappings, string culture)
        {
            return Channel.GetAllAccounts(fileId, includeDescriptions, includeMappings, culture);
        }

        public EY.com.eBook.API.Contracts.Data.Cache.AccountDataContract GetAccount(Guid fileId, string accountNr, bool includeDescriptions, bool includeMappings, string culture)
        {
            return Channel.GetAccount(fileId, accountNr, includeDescriptions, includeMappings, culture);
        }

        public List<EY.com.eBook.API.Contracts.Data.Cache.AccountDataContract> GetAccountsByRanges(Guid fileId, List<string> ranges, bool includeDescriptions, bool includeMappings, string culture)
        {
            return Channel.GetAccountsByRanges(fileId,ranges, includeDescriptions, includeMappings, culture);
        }

        public List<EY.com.eBook.API.Contracts.Data.Cache.AccountDataContract> GetAccountsOfMapping(Guid fileId, string mappingKey, bool includeDescriptions, bool includeMappings, string culture)
        {
            return Channel.GetAccountsOfMapping(fileId, mappingKey, includeDescriptions, includeMappings, culture);
        }

        public List<EY.com.eBook.API.Contracts.Data.Cache.AccountDescriptionDataContract> GetAccountTranslations(Guid fileId, string accountNr)
        {
            return Channel.GetAccountTranslations(fileId, accountNr);
        }

        public List<EY.com.eBook.API.Contracts.Data.Cache.AccountMappingDataContract> GetAccountMappings(Guid fileId, string accountNr)
        {
            return Channel.GetAccountMappings(fileId, accountNr);
        }

        public List<EY.com.eBook.API.Contracts.Data.Cache.AccountDataContract> GetAccountsAdjustedTree(Guid fileId, bool includeDescriptions, bool includeMappings, string culture, bool? includezeroes, bool? mappedOnly, string query)
        {
            return Channel.GetAccountsAdjustedTree(fileId, includeDescriptions, includeMappings, culture, includezeroes,mappedOnly, query);
        }

        public List<MappingItemDataContract> GetMappingItemsOfKey(string key)
        {
            return Channel.GetMappingItemsOfKey(key);
        }

        public List<CoefficientDataContract> GetMappingCoefficients(Guid id)
        {
            return Channel.GetMappingCoefficients(id);
        }

        public List<MappingItemDataContract> GetMappingItemsOfKeyMeta(string key, string meta)
        {
            return Channel.GetMappingItemsOfKeyMeta(key, meta);
        }
      
        public void AddAccounts(List<EY.com.eBook.API.Contracts.Data.Cache.AccountDataContract> adcs)
        {
            Channel.AddAccounts(adcs);
        }

        public void RemoveAccounts(Guid fileId, List<string> accountNrs)
        {
            Channel.RemoveAccounts(fileId,accountNrs);
        }

        public void ResetAccountsSaldi(Guid fileId, List<EY.com.eBook.API.Contracts.Data.Cache.AccountSaldiUpdate> asus)
        {
            Channel.ResetAccountsSaldi(fileId,asus);
        }

        public void AddAccountDescriptions(List<EY.com.eBook.API.Contracts.Data.Cache.AccountDescriptionDataContract> addcs)
        {
            Channel.AddAccountDescriptions(addcs);
        }

        public void UpdateAccountDescriptions(List<EY.com.eBook.API.Contracts.Data.Cache.AccountDescriptionDataContract> addcs)
        {
            Channel.UpdateAccountDescriptions(addcs);
        }

        public void RemoveAccountDescriptions(List<EY.com.eBook.API.Contracts.Data.Cache.AccountDescriptionDataContract> addcs)
        {
            Channel.RemoveAccountDescriptions(addcs);
        }

        public void AddAccountMappings(List<EY.com.eBook.API.Contracts.Data.Cache.AccountMappingDataContract> amdcs)
        {
            Channel.AddAccountMappings(amdcs);
        }

        public void UpdateAccountMappings(List<EY.com.eBook.API.Contracts.Data.Cache.AccountMappingDataContract> amdcs)
        {
            Channel.UpdateAccountMappings(amdcs);
        }

        public void RemoveAccountMappings(List<EY.com.eBook.API.Contracts.Data.Cache.AccountMappingDataContract> amdcs)
        {
            Channel.RemoveAccountMappings(amdcs);
        }

        #endregion
    }
}
