﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Threading;
using EY.com.eBook.API.Contracts.Data.Cache;
using System.Linq.Dynamic;
using EY.com.eBook.Core.EF;
using EY.com.eBook.Core.EF.Read;
using EY.com.eBook.RuleEngine.Data;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.Core;
using System.ServiceModel;
using EY.com.eBook.Core.Data;

namespace EY.com.eBook.Caching
{
    public class FileCache
    {
        private static FileCache _current = new FileCache();

        private ReaderWriterLockSlim _itemsLock = new ReaderWriterLockSlim();
        private static Hashtable _items = new Hashtable();

        private object _cacheRefreshFrequencyLock = new object();
        private TimeSpan _cacheRefreshFrequency = new TimeSpan(0, 0, 10);

        private Timer _timer = null;

        public TimeSpan CacheRefreshFrequency
        {
            get
            {
                TimeSpan res;

                lock (_cacheRefreshFrequencyLock)
                {
                    res = _cacheRefreshFrequency;
                }

                return res;
            }
            set
            {
                lock (_cacheRefreshFrequencyLock)
                {
                    _cacheRefreshFrequency = value;
                }

                int refreshFrequencyMilliseconds = (int)CacheRefreshFrequency.TotalMilliseconds;
                this._timer.Change(refreshFrequencyMilliseconds, refreshFrequencyMilliseconds);
            }
        }

        public int CachedItemsNumber
        {
            get
            {
                _itemsLock.EnterReadLock();
                try
                {
                    return _items.Count;
                }
                finally
                {
                    _itemsLock.ExitReadLock();
                }
            }
        }

        private FileCache()
        {
            
            int refreshFrequencyMilliseconds = (int)CacheRefreshFrequency.TotalMilliseconds;
            this._timer = new System.Threading.Timer(new
                       TimerCallback(CacherefreshTimerCallback),
                       null, refreshFrequencyMilliseconds, refreshFrequencyMilliseconds);
        }

        public void ClearComplete()
        {
            _itemsLock.EnterWriteLock();
            try
            {
                _items.Clear();
            }
            finally
            {
                _itemsLock.ExitWriteLock();
            }
            
        }

        public static FileCache Current
        {
            get
            {
                return _current;
            }
        }

        public object this[object key]
        {
            get
            {
                _itemsLock.EnterUpgradeableReadLock();
                try
                {
                    CacheItem res = (CacheItem)_items[key];
                    if (res != null)
                    {
                        if (res.SlidingExpirationTime.TotalMilliseconds > 0)
                        {
                            _itemsLock.EnterWriteLock();
                            try
                            {
                                res.LastAccessTime = DateTime.Now;
                            }
                            finally
                            {
                                _itemsLock.ExitWriteLock();
                            }
                        }
                        return res.ItemValue;
                    }
                    else
                    {
                        return null;
                    }
                }
                finally
                {
                    _itemsLock.ExitUpgradeableReadLock();
                }
            }
            set
            {
                _itemsLock.EnterWriteLock();
                try
                {
                    //if (_items.ContainsKey(key))
                    //{
                    //    CacheItem res = (CacheItem)_items[key];
                    //}
                    
                    _items[key] = new CacheItem(value);
                }
                finally
                {
                    _itemsLock.ExitWriteLock();
                }
            }
        }

        public void Insert(object key, object value)
        {
            _itemsLock.EnterWriteLock();
            try
            {
                _items[key] = new CacheItem(value);
            }
            finally
            {
                _itemsLock.ExitWriteLock();
            }
        }

        public void Insert(object key, object value, DateTime expirationDate)
        {
            _itemsLock.EnterWriteLock();
            try
            {
                _items[key] = new CacheItem(value, expirationDate);
            }
            finally
            {
                _itemsLock.ExitWriteLock();
            }
        }

        public void Insert(object key, object value, TimeSpan expirationTime)
        {
            _itemsLock.EnterWriteLock();
            try
            {
                _items[key] = new CacheItem(value, expirationTime);
            }
            finally
            {
                _itemsLock.ExitWriteLock();
            }
        }

        public void Insert(object key, object value, TimeSpan expirationTime, bool slidingExpiration)
        {
            _itemsLock.EnterWriteLock();
            try
            {
                _items[key] = new CacheItem(value, expirationTime, slidingExpiration);
            }
            finally
            {
                _itemsLock.ExitWriteLock();
            }
        }

        public void Insert(object key, object value, DateTime expirationDate, TimeSpan slidingExpirationTime)
        {
            _itemsLock.EnterWriteLock();
            try
            {
                _items[key] = new CacheItem(value, expirationDate, slidingExpirationTime);
            }
            finally
            {
                _itemsLock.ExitWriteLock();
            }
        }

        public void Remove(object key)
        {
            _itemsLock.EnterWriteLock();
            try
            {
                _items.Remove(key);
            }
            finally
            {
                _itemsLock.ExitWriteLock();
            }
        }


        private void CacherefreshTimerCallback(object state)
        {
            _itemsLock.EnterUpgradeableReadLock();
            try
            {
                Dictionary<object, CacheItem> delItems = new Dictionary<object, CacheItem>();
                DateTime dtNow = DateTime.Now;
                foreach (DictionaryEntry de in _items)
                {
                    CacheItem ci = (CacheItem)de.Value;
                    if (ci.ExpirationDate < dtNow)
                    {
                        delItems.Add(de.Key, ci);
                    }
                    else
                    {
                        if (ci.SlidingExpirationTime.TotalMilliseconds > 0)
                        {
                            if (dtNow.Subtract(ci.LastAccessTime).TotalMilliseconds > ci.SlidingExpirationTime.TotalMilliseconds)
                            {
                                delItems.Add(de.Key, ci);
                            }
                        }
                    }
                }

                if (delItems.Count > 0)
                {
                    _itemsLock.EnterWriteLock();
                    try
                    {
                        foreach (KeyValuePair<object, CacheItem> kvp in delItems)
                        {
                            if (_items.ContainsKey(kvp.Key))
                            {
                                _items.Remove(kvp.Key);
                            }
                        }
                    }
                    finally
                    {
                        _itemsLock.ExitWriteLock();
                    }
                }
            }
            finally
            {
                _itemsLock.ExitUpgradeableReadLock();
            }
        }

        public static string SchemaKey = "AS";
        public static string WorksheetskKey = "WS";

       

        #region Cache Updates
        public void AddAccounts(List<EY.com.eBook.API.Contracts.Data.Cache.AccountDataContract> adcs)
        {
            List<AccountDataContract> ads = adcs;
            List<AccountDescriptionDataContract> adds = new List<AccountDescriptionDataContract>();
            List<AccountMappingDataContract> amds = new List<AccountMappingDataContract>();
            Guid fileId = adcs.First().FileId;
            _itemsLock.EnterWriteLock();
                try
                {
                     SchemaCacheItem fci = (SchemaCacheItem)((CacheItem)_items[SchemaKey + fileId.ToString()]).ItemValue;
                    foreach (AccountDataContract ad in ads)
                    {
                        if (ad.Descriptions != null) adds.AddRange(ad.Descriptions);
                        if (ad.Mappings != null) amds.AddRange(ad.Mappings);
                        ad.Descriptions = new List<AccountDescriptionDataContract>();
                        ad.Mappings = new List<AccountMappingDataContract>();
                        fci.Schema.Accounts.Add(ad.InternalNr, ad);
                    }
                    fci.Schema.AccountDescriptions.AddRange(adds);
                    if (amds.Count > 0) fci.Schema.AccountMappings.AddRange(amds);
                }
                finally
                {
                    _itemsLock.ExitWriteLock();
                }
                ReIndexAccounts(fileId);
                ReIndexDescriptions(fileId);
                ReIndexMappings(fileId);
                //UpdateAccountDescriptions(adds, fileId);
                //UpdateAccountMappings(amds, fileId);

            

        }

        // impossible, only saldi needs updating, all others are through direct update methods (descs & mappings)
        //public void UpdateAccounts(List<EY.com.eBook.API.Contracts.Data.Cache.AccountDataContract> adcs)
        //{
        //    throw new NotImplementedException();
        //}

        public void RemoveAccounts(List<string> accountNrs,Guid fileId)
        {
            _itemsLock.EnterWriteLock();
            try
            {
                SchemaCacheItem fci = (SchemaCacheItem)((CacheItem)_items[SchemaKey + fileId.ToString()]).ItemValue;
                foreach (string accountNr in accountNrs)
                {
                    fci.Schema.Accounts.Remove(accountNr);
                }


                foreach (string accountNr in accountNrs)
                {

                    fci.Schema.AccountDescriptions.RemoveAt(fci.Schema.AccountDescriptionsIndex[accountNr]);
                    fci.Schema.AccountDescriptionsIndex.Remove(accountNr); // faster then actually reindexing all
                }



                foreach (string accountNr in accountNrs)
                {
                    fci.Schema.AccountMappings.RemoveAt(fci.Schema.AccountMappingsAccountIndex[accountNr]);
                }



            }
            finally
            {
                _itemsLock.ExitWriteLock();
            }
            
            ReIndexMappings(fileId);
            ReIndexAccounts(fileId);
        }

        public void ResetAccountsSaldi(List<AccountSaldiUpdate> asus, Guid fileId)
        {
            List<AccountDataContract> accounts = new List<AccountDataContract>();

            if (!_items.ContainsKey(SchemaKey + fileId.ToString()) || _items[SchemaKey + fileId.ToString()]==null)
            {
                throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { ErrorCode = -99, Message = "CACHE-MISS", FileId = Guid.Empty });
            }

            _itemsLock.EnterWriteLock();
            try
            {
                SchemaCacheItem fci = (SchemaCacheItem)((CacheItem)_items[SchemaKey + fileId.ToString()]).ItemValue;
                accounts = fci.Schema.Accounts.Values.Where(a => asus.Count(s => s.AccountNr == a.InternalNr) > 0).ToList();


                foreach (AccountDataContract ad in accounts)
                {
                    AccountSaldiUpdate asu = asus.First(a => a.AccountNr == ad.InternalNr);
                    ad.Saldo = asu.Saldo;
                    ad.PreviousSaldo = asu.PreviousSaldo;
                    ad.StartSaldo = asu.StartSaldo;
                    ad.LastChanged = DateTime.Now;
                }

                foreach (AccountDataContract ad in accounts)
                {
                    fci.Schema.Accounts[ad.InternalNr] = ad;
                }


            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                _itemsLock.ExitWriteLock();
            }
            ReIndexAccounts(fileId);

        }

        public void AddAccountDescriptions(List<AccountDescriptionDataContract> addcs, Guid fileId)
        {
            _itemsLock.EnterWriteLock();
            try
            {
                SchemaCacheItem fci = (SchemaCacheItem)((CacheItem)_items[SchemaKey + fileId.ToString()]).ItemValue;
                fci.Schema.AccountDescriptions.AddRange(addcs);
                foreach (string accountNr in addcs.Select(a => a.InternalNr).Distinct())
                {
                    fci.Schema.Accounts[accountNr].LastChanged = DateTime.Now;
                }
            }
            finally
            {
                _itemsLock.ExitWriteLock();
            }
            ReIndexDescriptions(fileId);

        }

        public void UpdateAccountDescriptions(List<AccountDescriptionDataContract> addcs, Guid fileId)
        {
            List<string> accountNrs = addcs.Select(ad => ad.InternalNr).Distinct().ToList();
            _itemsLock.EnterWriteLock();
            try
            {
                SchemaCacheItem fci = (SchemaCacheItem)((CacheItem)_items[SchemaKey + fileId.ToString()]).ItemValue;
                List<int> idxs = new List<int>();
                foreach (string accountNr in accountNrs)
                {
                    fci.Schema.Accounts[accountNr].LastChanged = DateTime.Now;
                    idxs.AddRange(fci.Schema.AccountDescriptionsIndex[accountNr]);
                }

               
                    foreach (int idx in idxs)
                    {
                        AccountDescriptionDataContract addc = fci.Schema.AccountDescriptions[idx];
                        AccountDescriptionDataContract addcU = addcs.FirstOrDefault(ad => ad.Culture == addc.Culture && ad.InternalNr == addc.InternalNr);

                        addc.Description = addcU.Description;
                        addc.changed = addcU.changed;

                    }
               


            }
            finally
            {
                _itemsLock.ExitWriteLock();
            }

            // no reindexing needed.
        }

        public void RemoveAccountDescriptions(List<AccountDescriptionDataContract> addcs, Guid fileId)
        {
           _itemsLock.EnterWriteLock();
           try
           {
               SchemaCacheItem fci = (SchemaCacheItem)((CacheItem)_items[SchemaKey + fileId.ToString()]).ItemValue;
               List<int> indexes = new List<int>();
               indexes = fci.Schema.AccountDescriptions.Where(ad => addcs.Count(a => a.InternalNr == ad.InternalNr && a.Culture == ad.Culture) > 0).Select(a => fci.Schema.AccountDescriptions.IndexOf(a)).ToList();

               fci.Schema.AccountDescriptions.RemoveAt(indexes);
               foreach (string accountNr in addcs.Select(a => a.InternalNr).Distinct())
               {
                   fci.Schema.Accounts[accountNr].LastChanged = DateTime.Now;
               }

           }
           finally
           {
               _itemsLock.ExitWriteLock();
           }
           ReIndexDescriptions(fileId);
        }

        public void AddAccountMappings(List<AccountMappingDataContract> amdcs, Guid fileId)
        {
            _itemsLock.EnterWriteLock();
            try
            {
                SchemaCacheItem fci = (SchemaCacheItem)((CacheItem)_items[SchemaKey + fileId.ToString()]).ItemValue;
                fci.Schema.AccountMappings.AddRange(amdcs);
                foreach (string accountNr in amdcs.Select(a => a.InternalNr).Distinct())
                {
                    fci.Schema.Accounts[accountNr].LastChanged = DateTime.Now;
                }
            }
            finally
            {
                _itemsLock.ExitWriteLock();
            }
            ReIndexMappings(fileId);

        }

        public void UpdateAccountMappings(List<AccountMappingDataContract> amdcs,Guid fileId)
        {
            List<string> accountNrs = amdcs.Select(ad => ad.InternalNr).Distinct().ToList();
            _itemsLock.EnterWriteLock();
            try
            {
                SchemaCacheItem fci = (SchemaCacheItem)((CacheItem)_items[SchemaKey + fileId.ToString()]).ItemValue;
                List<int> idxs = new List<int>();
                foreach (string accountNr in accountNrs)
                {
                    fci.Schema.Accounts[accountNr].LastChanged = DateTime.Now;
                    idxs.AddRange(fci.Schema.AccountMappingsAccountIndex[accountNr]);
                }

                foreach (int idx in idxs)
                {
                    AccountMappingDataContract addc = fci.Schema.AccountMappings[idx];
                    AccountMappingDataContract addcU = amdcs.FirstOrDefault(ad => ad.MappingKey == addc.MappingKey && ad.InternalNr == addc.InternalNr);
                    if (addcU != null)
                    {
                        addc.MappingMeta = addcU.MappingMeta;
                        addc.MappingItemId = addcU.MappingItemId;
                        addc.PreviousMappingItemId = addcU.PreviousMappingItemId;
                    }

                }

            }
            finally
            {
                _itemsLock.ExitWriteLock();
            }
        }

        public void RemoveAccountMappings(List<AccountMappingDataContract> amdcs, Guid fileId)
        {
            _itemsLock.EnterWriteLock();
            try
            {
                SchemaCacheItem fci = (SchemaCacheItem)((CacheItem)_items[SchemaKey + fileId.ToString()]).ItemValue;
                List<int> indexes = new List<int>();

                foreach (string accountNr in amdcs.Select(a => a.InternalNr).Distinct())
                {
                    fci.Schema.Accounts[accountNr].LastChanged = DateTime.Now;
                }

                indexes = fci.Schema.AccountMappings.Where(ad => amdcs.Count(a => a.InternalNr == ad.InternalNr && a.MappingKey == ad.MappingKey) > 0).Select(a => fci.Schema.AccountMappings.IndexOf(a)).ToList();



                fci.Schema.AccountMappings.RemoveAt(indexes);
            }
            finally
            {
                _itemsLock.ExitWriteLock();
            }
            ReIndexMappings(fileId);

        }
        #endregion

        #region Database updates (readonly)

        

        internal void ReloadAccountSadliFromDB(Guid fileId)
        {
            // might be quicker to utilise the stored procedure.


            var books = from b in eBookReadManager.Context.BookingLines
                                            .Include("Account")
                                            .Include("Booking")
                                            .Include("Booking.File")
                                            .Where(bl => bl.Booking.File.Id == fileId)
                        group b by b.Account.InternalNr
                        into bgroup
                        select new AccountSaldiUpdate {
                               AccountNr= bgroup.Key
                               , Saldo=  bgroup.Sum(a => a.Amount)
                        };


            var bookedAccs = eBookReadManager.Context.BookingLines
                    .Include("Account")
                    .Include("Booking")
                    .Include("Booking.File")
                    .Where(bl => bl.Booking.File.Id == fileId)
                    .Select(bl=>bl.Account)
                    .Distinct();

            var acc = eBookReadManager.Context.Accounts.Where(a=>a.StartSaldo!=a.Saldo).ToList();


            List<AccountSaldiUpdate> saldi = (from ac in bookedAccs
                                             where ac.Saldo != (ac.StartSaldo + books.First(b=>b.AccountNr==ac.InternalNr).Saldo)
                                             select new AccountSaldiUpdate {
                                                 AccountNr = ac.InternalNr
                                                 , PreviousSaldo = ac.PreviousSaldo
                                                 , StartSaldo = ac.StartSaldo
                                                 , Saldo = ac.StartSaldo + books.First(b=>b.AccountNr==ac.InternalNr).Saldo
                                             }).ToList();

            List<AccountSaldiUpdate> removedBookings = (from ac in acc.Where(a => saldi.Count(s => s.AccountNr == a.InternalNr) == 0)
                                                        select new AccountSaldiUpdate
                                                        {
                                                            AccountNr = ac.InternalNr
                                                            ,
                                                            Saldo = ac.StartSaldo
                                                            ,
                                                            StartSaldo = ac.StartSaldo
                                                            ,
                                                            PreviousSaldo = ac.PreviousSaldo
                                                        }).ToList();

            if (removedBookings.Count > 0) saldi.AddRange(removedBookings);
                
                //(from abl in 
                //                              group abl by abl.Account.InternalNr
                //                                  into ablGroup
                //                                  select new AccountSaldiUpdate
                //                                  {
                //                                      AccountNr = ablGroup.Key
                //                                      ,
                //                                      Saldo = abl ablGroup.Where(bl => bl.Booking.JournalType.Id > -1 && bl.Booking.JournalType.Id < 2).Sum(a => a.Amount)
                //                                  }).ToList();

            bool al = _itemsLock.TryEnterWriteLock(2000);
            if (al)
            {
                try
                {
                    SchemaCacheItem fci = (SchemaCacheItem)((CacheItem)_items[SchemaKey + fileId.ToString()]).ItemValue;
                    foreach (AccountSaldiUpdate asu in saldi)
                    {
                        fci.Schema.Accounts[asu.AccountNr].Saldo = asu.Saldo;
                        fci.Schema.Accounts[asu.AccountNr].StartSaldo = asu.StartSaldo;
                        fci.Schema.Accounts[asu.AccountNr].PreviousSaldo = asu.PreviousSaldo;
                    }

                    
                }
                finally
                {
                    _itemsLock.ExitWriteLock();
                }
                ReIndexAccounts(fileId);
            }

        }

        internal void ReloadAccounts(Guid fileId)
        {
            IndexedAccountsDataContract tmpAccounts = new IndexedAccountsDataContract();
            foreach (Account ac in eBookReadManager.Context.Accounts.Where(ad => ad.FileId == fileId))
            {
                tmpAccounts.Add(ac.InternalNr, new AccountDataContract
                {
                    DefaultDescription = ""
                    ,
                    Descriptions = new List<AccountDescriptionDataContract>()
                    ,
                    Mappings = new List<AccountMappingDataContract>()
                    ,
                    ExistedLastYear = ac.ExistedLastYear
                    ,
                    FileId = ac.FileId
                    ,
                    HasTaxAdjustments = ac.HasTaxAdjustments
                    ,
                    ImportedId = ac.ImportedID
                    ,
                    InternalNr = ac.InternalNr
                    ,
                    IsTaxAdjusment = ac.IsTaxAdjustment
                    ,
                    VisualNr = ac.VisualNr
                    ,
                    Saldo = ac.Saldo
                    ,
                    StartSaldo = ac.StartSaldo
                    ,
                    PreviousSaldo = ac.PreviousSaldo
                    , LastChanged = ac.LastChanged
                });
            }
            SchemaCacheItem fci = (SchemaCacheItem)((CacheItem)_items[SchemaKey + fileId.ToString()]).ItemValue;
            bool al = _itemsLock.TryEnterWriteLock(2000);
            if (al)
            {
                try
                {

                    fci.Schema.Accounts = tmpAccounts;

                    tmpAccounts = null;
                    
                }
                finally
                {
                    _itemsLock.ExitWriteLock();
                }
                ReIndexAccounts(fileId);
            }
        }

        internal void ReloadMappings(Guid fileId)
        {
            List<AccountMappingDataContract> amdc = (from am in eBookReadManager.Context.AccountMappings.Where(am => am.FileId == fileId)
                                                     select new AccountMappingDataContract
                                                     {
                                                         FileId = am.FileId
                                                         ,
                                                         InternalNr = am.InternalNr
                                                         ,
                                                         MappingKey = am.MappingKey
                                                         , MappingMeta = am.MappingMeta
                                                         ,
                                                         MappingItemId = am.MappingItemId
                                                         ,
                                                         PreviousMappingItemId = am.PreviousMappingItemId
                                                     }).ToList();

            SchemaCacheItem fci = (SchemaCacheItem)((CacheItem)_items[SchemaKey + fileId.ToString()]).ItemValue;
            bool al = _itemsLock.TryEnterWriteLock(2000);
            if (al)
            {
                try
                {

                    fci.Schema.AccountMappings = amdc;

                    amdc = null;
                    
                }
                finally
                {
                    _itemsLock.ExitWriteLock();
                }
                ReIndexMappings(fileId);
            }
        }

        internal void ReloadDescriptions(Guid fileId)
        {
            List<AccountDescriptionDataContract> amdc = (from am in eBookReadManager.Context.AccountDescriptions.Where(am => am.FileId == fileId)
                                                         select new AccountDescriptionDataContract
                                                         {
                                                             FileId = am.FileId
                                                             ,
                                                             InternalNr = am.InternalNr
                                                             ,
                                                             changed = am.HasChanged
                                                             ,
                                                             Culture = am.Culture
                                                             ,
                                                             Description = am.Description
                                                         }).ToList();

            SchemaCacheItem fci = (SchemaCacheItem)((CacheItem)_items[SchemaKey + fileId.ToString()]).ItemValue;
            bool al = _itemsLock.TryEnterWriteLock(2000);
            if (al)
            {
                try
                {

                    fci.Schema.AccountDescriptions = amdc;

                    amdc = null;
                    
                }
                finally
                {
                    _itemsLock.ExitWriteLock();
                }
                ReIndexDescriptions(fileId);
            }
            
        }

        internal void ReloadFileInCache(Guid fileId)
        {
            ReloadSchemaFromDatabase(fileId);
            ReloadBookingsFromDatabase(fileId);
            ReloadMessagesFromDatabase(fileId);
        }

        internal void ReloadMessagesFromDatabase(Guid fileId)
        {
            var messages = from m in eBookReadManager.Context.Messages.Where(m=>m.FileId==fileId).ToList()
                           select new FileMessageDataContract
                           {
                               FileId = m.FileId
                               ,
                               MessageId = m.MessageId
                               ,
                               Culture = m.Culture
                               ,
                               Type = CoreHelper.ToEnum<MessageTypeDataContract>(m.Type)
                               ,
                               Text = m.Text
                               ,
                               ConnectionType = CoreHelper.ToEnum<FileMessageConnectionTypeDataContract>(m.ConnectionType)
                               ,
                               ConnectionAccount = m.ConnectionAccount
                               ,
                               ConnectionGuid = m.ConnectionGuid
                               ,
                               WorksheetTypeCollection = m.WorksheetTypeCollection
                               ,
                               WorksheetTypeRowId = m.WorksheetTypeRowId
                               ,
                               WorksheetTypeField = m.WorksheetTypeField
                               ,
                               WorksheetTypeCollectionText = m.WorksheetTypeCollectionText
                               ,
                               WorksheetTypeFieldText = m.WorksheetTypeFieldText
                           };

            List<FileMessageDataContract> msgs = messages.ToList();
            if (msgs == null) msgs = new List<FileMessageDataContract>();
            SchemaCacheItem fci = (SchemaCacheItem)((CacheItem)_items[SchemaKey + fileId.ToString()]).ItemValue;
            bool al = _itemsLock.TryEnterWriteLock(2000);
            if (al)
            {
                try
                {
                    fci.Messages = msgs;
                }
                finally
                {
                    _itemsLock.ExitWriteLock();
                }
            }

        }

        internal void ReloadBookingsFromDatabase(Guid fileId)
        {
            EY.com.eBook.API.BL.Read.AccountSchemaRepository asr = new EY.com.eBook.API.BL.Read.AccountSchemaRepository();
            var manuals = asr.LoadManualBookings(fileId);
            var auto = asr.LoadAutomaticBookings(fileId);

            SchemaCacheItem fci = (SchemaCacheItem)((CacheItem)_items[SchemaKey + fileId.ToString()]).ItemValue;
            bool al = _itemsLock.TryEnterWriteLock(2000);
            if (al)
            {
                try
                {
                    fci.ManualBookings = manuals;
                    fci.AutomaticBookings = auto;   
                }
                finally
                {
                    _itemsLock.ExitWriteLock();
                }
            }
        }

        internal void ReloadSchemaFromDatabase(Guid fileId)
        {
            EY.com.eBook.Core.EF.Write.File fle = eBookWriteManager.Context.Files.First(f => f.Id == fileId);
            FileDataContract fdc = new FileDataContract
            {
                ClientId = fle.ClientId
                ,
                Closed = fle.CloseDate.HasValue
                ,
                ClosedBy = fle.ClosedBy
                ,
                ClosedDate = fle.CloseDate
                ,
                CreatedBy = fle.CreatedBy
                ,
                CreationDate = fle.CreationDate.Value
                ,
                Culture = fle.Culture
                ,
                Deleted = fle.Deleted
                ,
                Display = fle.Name
                ,
                EndDate = fle.EndDate
                ,
                Id = fle.Id
                ,
                ImportDate = fle.ImportDate
                ,
                MarkedForDeletion = fle.MarkDelete
                ,
                Name = fle.Name
                ,
                NumbersLocked = fle.NumbersLocked
                ,
                PreviousEndDate = fle.PrevEndDate
                ,
                PreviousFileId = fle.PreviousFileId
                ,
                PreviousStartDate = fle.PrevStartDate
                ,
                StartDate = fle.StartDate
            };
            FileSettingsDataContract settings = new FileSettingsDataContract { };
            if (!string.IsNullOrEmpty(fle.Settings))
            {
                settings = CoreHelper.DeserializeFromString<FileSettingsDataContract>(fle.Settings);
            }
            else
            {
                settings = new FileSettingsDataContract { FileId = fle.Id };
            }

            List<EY.com.eBook.Core.EF.Write.FileService> fs = eBookWriteManager.Context.FileServices.Where(f => f.FileId == fle.Id).OrderBy(s => s.Order).ToList();
            if (fdc == null || fs == null || fs.Count == 0) fs = new List<EY.com.eBook.Core.EF.Write.FileService>();
            if (fs == null || fs.Count == 0)
            {
                fdc.Services = new List<FileServiceDataContract>();
            }
            else
            {
                fdc.Services = (from s in fs
                                select new FileServiceDataContract
                                {
                                    FileId = s.FileId
                                    ,
                                    Completed = s.Completed
                                    ,
                                    CompletedBy = s.CompletedBy
                                    ,
                                    ServiceId = s.ServiceId
                                    ,
                                    Order = s.Order
                                    ,
                                    Status = s.Status
                                }).ToList();
            }
            EY.com.eBook.Core.EF.Write.Client cle = eBookWriteManager.Context.ClientSet.First(c => c.Id == fle.ClientId);
            ClientBaseDataContract cdc = new ClientBaseDataContract
            {
                Id = cle.Id
                ,
                Address = cle.Address
                ,
                BizTaxFolder = null
                ,
                City = cle.City
                ,
                Country = cle.Country
                ,
                EnterpriseNumber = cle.EnterpriseNumber
                ,
                FodAddress = null
                ,
                GfisCode = cle.GFISCode
                ,
                LegalTypeStructure = cle.LegalStructure
                ,
                LegalTypeId = cle.CLIENT_LEGAL_TYPE_ID
                ,
                Name = cle.Name
                ,
                ProAccDatabase = cle.ProAccDatabase
                ,
                ProAccLinkUpdated = cle.ProAccLinkUpdated
                ,
                ProAccServer = cle.ProAccServer
                ,
                Shared = false
                ,
                Vat = cle.VatNumber
                ,
                ZipCode = cle.ZipCode
                ,
                BNI = cle.BNI
            };

            
            _itemsLock.EnterWriteLock();
            try
            {
                ((SchemaCacheItem)((CacheItem)_items[SchemaKey + fileId.ToString()]).ItemValue).DataContract = fdc;
                ((SchemaCacheItem)((CacheItem)_items[SchemaKey + fileId.ToString()]).ItemValue).Settings = settings;
                ((SchemaCacheItem)((CacheItem)_items[SchemaKey + fileId.ToString()]).ItemValue).Client = cdc;
            }
            finally
            {
                _itemsLock.ExitWriteLock();
            }
            ReloadAccounts(fileId);
            ReloadMappings(fileId);
            ReloadDescriptions(fileId);
        }

        private void ReIndexMappings(Guid fileId)
        {

            IndexListDataContract amk = new IndexListDataContract();
            IndexListDataContract ama = new IndexListDataContract();

            _itemsLock.EnterWriteLock();
            try
            {
                SchemaCacheItem fci = (SchemaCacheItem)((CacheItem)_items[SchemaKey + fileId.ToString()]).ItemValue;
                foreach (string key in fci.Schema.AccountMappings.Select(am => am.MappingKey).Distinct())
                {
                    List<int> idxs = fci.Schema.AccountMappings.Where(am => am.MappingKey == key).Select(am => fci.Schema.AccountMappings.IndexOf(am)).ToList();
                    amk.Add(key, idxs);
                }


                foreach (string key in fci.Schema.AccountMappings.Select(am => am.InternalNr).Distinct())
                {
                    List<int> idxs = fci.Schema.AccountMappings.Where(am => am.InternalNr == key).Select(am => fci.Schema.AccountMappings.IndexOf(am)).ToList();
                    ama.Add(key, idxs);
                }


                fci.Schema.AccountMappingMappedKeyIndex = amk;
                fci.Schema.AccountMappingsAccountIndex = ama;
                
            }
            finally
            {
                _itemsLock.ExitWriteLock();
            }

            ama = null;
            amk = null;
        }

        private void ReIndexDescriptions(Guid fileId)
        {
            IndexListDataContract adi = new IndexListDataContract();
            _itemsLock.EnterWriteLock();
            try
            {
                SchemaCacheItem fci = (SchemaCacheItem)((CacheItem)_items[SchemaKey + fileId.ToString()]).ItemValue;
                foreach (string key in fci.Schema.AccountDescriptions.Select(ad => ad.InternalNr).Distinct())
                {
                    List<int> idxs = fci.Schema.AccountDescriptions.Where(ad => ad.InternalNr == key).Select(ad => fci.Schema.AccountDescriptions.IndexOf(ad)).ToList();
                    adi.Add(key, idxs);
                }


                fci.Schema.AccountDescriptionsIndex = adi;
               
            }
            finally
            {
                _itemsLock.ExitWriteLock();
            }

            adi = null;
        }

        private void ReIndexAccounts(Guid fileId)
        {
            _itemsLock.EnterWriteLock();
            try
            {
                SchemaCacheItem fci = (SchemaCacheItem)((CacheItem)_items[SchemaKey + fileId.ToString()]).ItemValue;
                List<string> zero = fci.Schema.Accounts.Values.Where(a => a.PreviousSaldo == 0 && a.Saldo == 0).Select(a => a.InternalNr).ToList();
                List<string> saldi = fci.Schema.Accounts.Values.Where(a => a.PreviousSaldo != 0 || a.Saldo != 0).Select(a => a.InternalNr).ToList();
                List<string> adjusted = fci.Schema.Accounts.Values.Where(a => a.IsTaxAdjusment).Select(a => a.InternalNr).ToList();
                List<string> hasAdjustments = adjusted.Select(a => a.Split('.')[0]).Distinct().ToList();
                List<string> parentAccounts = fci.Schema.Accounts.Values.Where(a => !a.IsTaxAdjusment).Select(a => a.InternalNr).ToList();

                IndexedKeysDataContract parentClients = new IndexedKeysDataContract();
                foreach (string nr in hasAdjustments)
                {
                    parentClients.Add(nr, adjusted.Where(a => a.StartsWith(nr)).ToList());
                }


                fci.Schema.ZeroAccounts = zero;
                fci.Schema.SaldiAccounts = saldi;
                fci.Schema.AdjustedAccounts = adjusted;
                fci.Schema.ParentAdjustedIndex = parentClients;
                fci.Schema.ParentAccounts = parentAccounts;

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                _itemsLock.ExitWriteLock();
            }

        }

        #endregion


       

        


    }
}

