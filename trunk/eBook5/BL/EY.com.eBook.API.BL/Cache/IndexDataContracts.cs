﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Threading;
using EY.com.eBook.API.Contracts.Data.Cache;
using System.Linq.Dynamic;
using EY.com.eBook.Core.EF;
using EY.com.eBook.Core.EF.Read;
using EY.com.eBook.RuleEngine.Data;
using System.Runtime.Serialization;
using EY.com.eBook.API.Contracts.Data;

namespace EY.com.eBook.Caching
{
    [CollectionDataContract]
    public class IndexedAccountsDataContract : Dictionary<string, AccountDataContract>
    {
    }

    [CollectionDataContract]
    public class IndexedCoefficientDataContract : Dictionary<Guid, CoefficientDataContract>
    {
    }

    [CollectionDataContract]
    public class IndexListDataContract : Dictionary<string, List<int>>
    {
    }

    [CollectionDataContract]
    public class IndexedKeysDataContract : Dictionary<string, List<string>>
    {
    }
}