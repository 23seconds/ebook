﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EY.com.eBook.Core.EF;
using EY.com.eBook.API.Contracts.Services;
using EY.com.eBook.API.Contracts.Data;
using System.Data;
using EY.com.eBook.Core;
using System.ServiceModel;
using System.Xml.Linq;
using EY.com.eBook.Caching;
using EY.com.eBook.Core.Data;


namespace EY.com.eBook.API.Implementation
{
    /// <summary>
    /// CachingService
    /// </summary>
    [EntityFrameworkServiceBehavior]
    [ServiceBehavior(IncludeExceptionDetailInFaults = true, ConcurrencyMode = ConcurrencyMode.Multiple, InstanceContextMode = InstanceContextMode.PerCall)]
    public class CacheService : ICacheService
    {
        #region ICacheService Members
        // INIT OF WCF (for test purposes, timing)
        public bool Initialize() { return true; }

        public void TestException(int id)
        {
            string t = "this is a test";
        }

        public void TestCommunicatedException(int id)
        {
            throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { ErrorCode = -99, Message = "CACHE-MISS", FileId = Guid.Empty});
        }

        public void TestSave(EY.com.eBook.RuleEngine.Core.Interfaces.IWorksheet fac)
        {
            Console.WriteLine(fac.ToString());
        }

        public bool ReloadMainCache()
        {
            return new CacheRepository().LoadStaticDataToCache();
        }

        public bool FileInCache(Guid fileId)
        {
            return new CacheRepository().FileInCache(fileId);
        }

        public void RemoveFileFromCache(Guid fileId)
        {
            new CacheRepository().RemoveFileFromCache(fileId);
        }

        public bool ReloadFileInCache(Guid fileId)
        {
            return ReloadFileInCache(fileId, false, false);
        }

        public bool ReloadOrAddFileInCache(Guid fileId)
        {
            return ReloadFileInCache(fileId, true,false);
        }

        public bool ReloadOrAddFileInCacheForce(Guid fileId, bool force)
        {
            return ReloadFileInCache(fileId, true,force);
        }

        private bool ReloadFileInCache(Guid fileId, bool createIfNonExisting, bool force)
        {
            bool ok = true;
            try
            {
                CacheRepository cr = new CacheRepository();
                if (cr.FileInCache(fileId))
                {
                    if (force)
                    {
                        cr.LoadFileInCache(fileId);
                    }
                }
                else
                {
                    if (createIfNonExisting) cr.LoadFileInCache(fileId);
                    // or automatic load in cache?
                }
            }
            catch (Exception ex)
            {
                
                ok = false;
            }
            return ok;
        }

        public List<EY.com.eBook.API.Contracts.Data.Cache.WorksheetDataContract> GetWorksheets(Guid fileId)
        {
            throw new NotImplementedException();
        }

        public string GetWorksheetData(Guid fileId, Guid typeId)
        {
            return "check";
        }

        public List<FileMessageDataContract> GetMessages(CriteriaFileDataContract cfdc)
        {
            CacheRepository rp = new CacheRepository();
            SchemaCacheItem fc = rp.GetCachedFile(cfdc.FileId);
            if (fc == null)
            {
                throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { ErrorCode = -99, Message = "CACHE-MISS", FileId = cfdc.FileId });
                // throw exception
            }
            return rp.GetMessages(cfdc);
        }

        public List<FileMessageDataContract> GetMessagesWithoutNotes(CriteriaFileDataContract cfdc)
        {
            CacheRepository rp = new CacheRepository();
            SchemaCacheItem fc = rp.GetCachedFile(cfdc.FileId);
            if (fc == null)
            {
                throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { ErrorCode = -99, Message = "CACHE-MISS", FileId = cfdc.FileId });
                // throw exception
            }
            return rp.GetMessagesWithoutNotes(cfdc);
        }

        public List<FileMessageDataContract> GetNotes(CriteriaFileDataContract cfdc)
        {
            CacheRepository rp = new CacheRepository();
            SchemaCacheItem fc = rp.GetCachedFile(cfdc.FileId);
            if (fc == null)
            {
                throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { ErrorCode = -99, Message = "CACHE-MISS", FileId = cfdc.FileId });
                // throw exception
            }
            return rp.GetNotes(cfdc);
        }

        public List<BookingDataContract> GetManualBookings(Guid fileId)
        {
            CacheRepository rp = new CacheRepository();
            SchemaCacheItem fc = rp.GetCachedFile(fileId);
            if (fc == null)
            {
                throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { ErrorCode = -99, Message = "CACHE-MISS", FileId = fileId });
                // throw exception
            }
            return rp.GetManualBookings(fileId);
        }

        public List<BookingDataContract> GetAutomaticBookings(Guid fileId)
        {
            CacheRepository rp = new CacheRepository();
            SchemaCacheItem fc = rp.GetCachedFile(fileId);
            if (fc == null)
            {
                throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { ErrorCode = -99, Message = "CACHE-MISS", FileId = fileId });
                // throw exception
            }
            return rp.GetAutomaticBookings(fileId);
        }


        public List<BookingDataContract> GetAllBookings(Guid fileId)
        {
            CacheRepository rp = new CacheRepository();
            SchemaCacheItem fc = rp.GetCachedFile(fileId);
            if (fc == null)
            {
                throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { ErrorCode = -99, Message = "CACHE-MISS", FileId = fileId });
                // throw exception
            }
            return rp.GetAllBookings(fileId);
        }

        public void UpdateBookings(Guid fileId)
        {
            CacheRepository rp = new CacheRepository();
            SchemaCacheItem fc = rp.GetCachedFile(fileId);
            if (fc == null)
            {
                throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { ErrorCode = -99, Message = "CACHE-MISS", FileId = fileId });
                // throw exception
            }
            rp.UpdateBookings(fileId);
        }

        public List<BookingDataContract> GetWorksheetBookings(Guid fileId, Guid typeId)
        {
            CacheRepository rp = new CacheRepository();
            SchemaCacheItem fc = rp.GetCachedFile(fileId);
            if (fc == null)
            {
                throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { ErrorCode = -99, Message = "CACHE-MISS", FileId = fileId });
                // throw exception
            }
            return rp.GetWorksheetBookings(fileId,typeId);
        }

        public List<EY.com.eBook.API.Contracts.Data.Cache.AccountDataContract> GetAllAccounts(Guid fileId, bool includeDescriptions, bool includeMappings, string culture)
        {
            CacheRepository cr = new CacheRepository();
            SchemaCacheItem fc = cr.GetCachedFile(fileId);
            if (fc == null)
            {
                throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { ErrorCode = -99, Message = "CACHE-MISS", FileId = fileId });
            }

            return fc.GetAccounts(includeDescriptions,includeMappings,culture);
        }

        public EY.com.eBook.API.Contracts.Data.Cache.AccountDataContract GetAccount(Guid fileId, string accountNr, bool includeDescriptions, bool includeMappings, string culture)
        {
            CacheRepository cr = new CacheRepository();
            SchemaCacheItem fc = cr.GetCachedFile(fileId);
            if (fc == null)
            {
                throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { ErrorCode = -99, Message = "CACHE-MISS", FileId = fileId });
                // throw exception
            }

            return fc.GetAccount(accountNr, includeDescriptions, includeMappings, culture);
        }

        public List<EY.com.eBook.API.Contracts.Data.Cache.AccountDataContract> GetAccountsAdjustedTree(Guid fileId, bool includeDescriptions, bool includeMappings, string culture, bool? includezeroes, bool? mappedOnly, string query)
        {
            CacheRepository cr = new CacheRepository();
            SchemaCacheItem fc = cr.GetCachedFile(fileId);
            if (fc == null)
            {
                throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { ErrorCode = -99, Message = "CACHE-MISS", FileId = fileId });
                // throw exception
            }
            return fc.GetAccountsAdjustedTree(includeDescriptions, includeMappings, culture,includezeroes, mappedOnly,query);
        }

        public List<EY.com.eBook.API.Contracts.Data.Cache.AccountDataContract> GetAccountsByRanges(Guid fileId, List<string> ranges, bool includeDescriptions, bool includeMappings, string culture)
        {
            CacheRepository cr = new CacheRepository();
            SchemaCacheItem fc = cr.GetCachedFile(fileId);
            if (fc == null)
            {
                throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { ErrorCode = -99, Message = "CACHE-MISS", FileId = fileId });
                // throw exception
            }

            return fc.GetAccountsInRanges(ranges,includeDescriptions, includeMappings, culture);
        }

        public List<EY.com.eBook.API.Contracts.Data.Cache.AccountDataContract> GetAccountsOfMapping(Guid fileId, string mappingKey, bool includeDescriptions, bool includeMappings, string culture)
        {
            CacheRepository cr = new CacheRepository();
            SchemaCacheItem fc = cr.GetCachedFile(fileId);
            if (fc == null)
            {
                throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { ErrorCode = -99, Message = "CACHE-MISS", FileId = fileId });
                // throw exception
            }

            return fc.GetAccountsOfMapping(mappingKey,true, includeDescriptions, includeMappings, culture);
        }

        public List<EY.com.eBook.API.Contracts.Data.Cache.AccountDescriptionDataContract> GetAccountTranslations(Guid fileId, string accountNr)
        {
            CacheRepository cr = new CacheRepository();
            SchemaCacheItem fc = cr.GetCachedFile(fileId);
            if (fc == null)
            {
                throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { ErrorCode = -99, Message = "CACHE-MISS", FileId = fileId });
                // throw exception
            }

            return fc.GetAccountDescriptions(accountNr);
        }

        public List<EY.com.eBook.API.Contracts.Data.Cache.AccountMappingDataContract> GetAccountMappings(Guid fileId, string accountNr)
        {
            CacheRepository cr = new CacheRepository();
            SchemaCacheItem fc = cr.GetCachedFile(fileId);
            if (fc == null)
            {
                throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { ErrorCode = -99, Message = "CACHE-MISS", FileId = fileId });
                // throw exception
            }

            return fc.GetAccountMappings(accountNr);
        }


        public List<MappingItemDataContract> GetMappingItemsOfKey(string key)
        {
            return new CacheRepository().GetMappingItemsOfKey(key);
        }

        public List<MappingItemDataContract> GetMappingItemsOfKeyMeta(string key,string meta)
        {
            return new CacheRepository().GetMappingItemsOfKeyMeta(key,meta);
        }

        public List<CoefficientDataContract> GetMappingCoefficients(Guid id)
        {
            return new CacheRepository().GetMappingCoefficients(id);
        }


        public void AddAccounts(List<EY.com.eBook.API.Contracts.Data.Cache.AccountDataContract> adcs)
        {
            CacheRepository cr = new CacheRepository();
            List<Guid> fileIds = adcs.Select(a => a.FileId).Distinct().ToList();

            foreach (Guid fileId in fileIds)
            {
                SchemaCacheItem fc = cr.GetCachedFile(fileId);
                if (fc == null)
                {
                    throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { ErrorCode = -99, Message = "CACHE-MISS", FileId = fileId });
                    // throw exception
                }
                FileCache.Current.AddAccounts(adcs.Where(a => a.FileId == fileId).ToList());
            }

        }

        //public void UpdateAccounts(List<EY.com.eBook.API.Contracts.Data.Cache.AccountDataContract> adcs)
        //{
        //    CacheRepository cr = new CacheRepository();
        //    List<Guid> fileIds = adcs.Select(a => a.FileId).Distinct().ToList();

        //    foreach (Guid fileId in fileIds)
        //    {
        //        FileCache fc = cr.GetCachedFile(fileId);
        //        if (fc != null)
        //        {
        //            fc.UpdateAccounts(adcs.Where(a => a.FileId == fileId).ToList());
        //        }
        //    }
        //}

        public void RemoveAccounts(Guid fileId, List<string> accountNrs)
        {
            CacheRepository cr = new CacheRepository();
            SchemaCacheItem fc = cr.GetCachedFile(fileId);
            if (fc == null)
            {
                throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { ErrorCode = -99, Message = "CACHE-MISS", FileId = fileId });
                // throw exception
            }
            FileCache.Current.RemoveAccounts(accountNrs, fileId);
        }

        public void ResetAccountsSaldi(Guid fileId, List<EY.com.eBook.API.Contracts.Data.Cache.AccountSaldiUpdate> asus)
        {
            CacheRepository cr = new CacheRepository();
            SchemaCacheItem fc = cr.GetCachedFile(fileId);
            if (fc == null)
            {
                throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { ErrorCode = -99, Message = "CACHE-MISS", FileId = fileId });
                // throw exception
            }
            FileCache.Current.ResetAccountsSaldi(asus,fileId);
            
        }

        public void AddAccountDescriptions(List<EY.com.eBook.API.Contracts.Data.Cache.AccountDescriptionDataContract> addcs)
        {
            CacheRepository cr = new CacheRepository();
            
            List<Guid> fileIds = addcs.Select(a => a.FileId).Distinct().ToList();

            foreach (Guid fileId in fileIds)
            {
                SchemaCacheItem fc = cr.GetCachedFile(fileId);
                if (fc == null)
                {
                    throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { ErrorCode = -99, Message = "CACHE-MISS", FileId = fileId });
                    // throw exception
                }
                FileCache.Current.AddAccountDescriptions(addcs.Where(a => a.FileId == fileId).ToList(),fileId);
            }
        }

        public void UpdateAccountDescriptions(List<EY.com.eBook.API.Contracts.Data.Cache.AccountDescriptionDataContract> addcs)
        {
            CacheRepository cr = new CacheRepository();

            List<Guid> fileIds = addcs.Select(a => a.FileId).Distinct().ToList();

            foreach (Guid fileId in fileIds)
            {
                SchemaCacheItem fc = cr.GetCachedFile(fileId);
                if (fc == null)
                {
                    throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { ErrorCode = -99, Message = "CACHE-MISS", FileId = fileId });
                    // throw exception
                }
                FileCache.Current.UpdateAccountDescriptions(addcs.Where(a => a.FileId == fileId).ToList(),fileId);
                
            }
        }

        public void RemoveAccountDescriptions(List<EY.com.eBook.API.Contracts.Data.Cache.AccountDescriptionDataContract> addcs)
        {
            CacheRepository cr = new CacheRepository();
            List<Guid> fileIds = addcs.Select(a => a.FileId).Distinct().ToList();

            foreach (Guid fileId in fileIds)
            {
                SchemaCacheItem fc = cr.GetCachedFile(fileId);
                if (fc == null)
                {
                    throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { ErrorCode = -99, Message = "CACHE-MISS", FileId = fileId });
                    // throw exception
                }
                FileCache.Current.RemoveAccountDescriptions(addcs.Where(a => a.FileId == fileId).ToList(),fileId);
            }
        }

        public void AddAccountMappings(List<EY.com.eBook.API.Contracts.Data.Cache.AccountMappingDataContract> amdcs)
        {
            CacheRepository cr = new CacheRepository();
            List<Guid> fileIds = amdcs.Select(a => a.FileId).Distinct().ToList();

            foreach (Guid fileId in fileIds)
            {
                SchemaCacheItem fc = cr.GetCachedFile(fileId);
                if (fc == null)
                {
                    throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { ErrorCode = -99, Message = "CACHE-MISS", FileId = fileId });
                    // throw exception
                }
                FileCache.Current.AddAccountMappings(amdcs.Where(a => a.FileId == fileId).ToList(),fileId);
                
            }
        }

        public void UpdateAccountMappings(List<EY.com.eBook.API.Contracts.Data.Cache.AccountMappingDataContract> amdcs)
        {
            CacheRepository cr = new CacheRepository();
            List<Guid> fileIds = amdcs.Select(a => a.FileId).Distinct().ToList();

            foreach (Guid fileId in fileIds)
            {
                SchemaCacheItem fc = cr.GetCachedFile(fileId);
                if (fc == null)
                {
                    throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { ErrorCode = -99, Message = "CACHE-MISS", FileId = fileId });
                    // throw exception
                }
                FileCache.Current.UpdateAccountMappings(amdcs.Where(a => a.FileId == fileId).ToList(),fileId);
                
            }
        }

        public void RemoveAccountMappings(List<EY.com.eBook.API.Contracts.Data.Cache.AccountMappingDataContract> amdcs)
        {
            CacheRepository cr = new CacheRepository();
            List<Guid> fileIds = amdcs.Select(a => a.FileId).Distinct().ToList();

            foreach (Guid fileId in fileIds)
            {
                SchemaCacheItem fc = cr.GetCachedFile(fileId);
                if (fc == null)
                {
                    throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { ErrorCode = -99, Message = "CACHE-MISS", FileId = fileId });
                    // throw exception
                }
                FileCache.Current.RemoveAccountMappings(amdcs.Where(a => a.FileId == fileId).ToList(),fileId);
                
            }
        }

        #endregion

        #region ICacheService Members


        public WorksheetWorkSetDataContract GetWorksheetSet(Guid fileId)
        {
            return new CacheRepository().GetWorksheetSet(fileId);
        }

        public List<WorksheetTypeTranslationDataContract> GetWorksheetTypeTranslations(Guid typeId, string culture)
        {
            return new CacheRepository().GetWorksheetTypeTranslations(typeId, culture);
        }


        public void SaveWorksheetSetToCache(WorksheetLibrary wsb, Guid fileId)
        {
            new CacheRepository().SaveWorksheetSetToCache(wsb, fileId);
        }

        #endregion
    }
}
