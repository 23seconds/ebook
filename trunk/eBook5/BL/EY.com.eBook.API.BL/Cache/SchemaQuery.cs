﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using EY.com.eBook.Caching;
using EY.com.eBook.API.Contracts.Data.Cache;

namespace EY.com.eBook.Caching
{
    public abstract class SchemaQuery
    {
        public abstract AccountSchemaWorkSet Schema { get; set; }

        #region Schema Query

        private void EnrichAccountData(ref List<AccountDataContract> accounts, bool includeDescriptions, bool includeMappings, string culture)
        {
            List<AccountDescriptionDataContract> addcs;
            AccountDescriptionDataContract addc;


            foreach (AccountDataContract account in accounts)
            {
                addcs = GetAccountDescriptionsPrivate(account.InternalNr);
                if (includeMappings) account.Mappings = GetMappingsPrivate(account.InternalNr);
                if (includeDescriptions) account.Descriptions = addcs;
                addc = addcs.FirstOrDefault(d => d.Culture == culture);
                account.DefaultDescription = addc != null ? addc.Description : "NO TRANSLATION";
            }

        }

        private void EnrichAccountData(ref AccountDataContract account, bool includeDescriptions, bool includeMappings, string culture)
        {
            List<AccountDescriptionDataContract> addcs;
            AccountDescriptionDataContract addc;

            addcs = GetAccountDescriptionsPrivate(account.InternalNr);
            if (includeMappings) account.Mappings = GetMappingsPrivate(account.InternalNr);
            if (includeDescriptions) account.Descriptions = addcs;
            addc = addcs.FirstOrDefault(d => d.Culture == culture);
            account.DefaultDescription = addc != null ? addc.Description : "NO TRANSLATION";

        }



        public AccountDataContract GetAccount(string accountNr, bool includeDescriptions, bool includeMappings, string culture)
        {
            AccountDataContract account = null;

            if (Schema.Accounts.ContainsKey(accountNr))
            {
                account = Schema.Accounts[accountNr];

                EnrichAccountData(ref account, includeDescriptions, includeMappings, culture);

                if (account.HasTaxAdjustments)
                {
                    List<string> adjustmentkeys = Schema.AdjustedAccounts.Where(a => a.StartsWith(account.InternalNr)).ToList();
                    List<AccountDataContract> adjs = Schema.Accounts.IndexedList(adjustmentkeys);
                    EnrichAccountData(ref adjs, includeDescriptions, includeMappings, culture);
                    account.Adjustments = adjs;
                }
            }

            return account;
        }

        public List<AccountDataContract> GetAccounts(bool includeDescriptions, bool includeMappings, string culture)
        {
            return GetAccounts(true, true, includeDescriptions, includeMappings, culture);
        }

        public List<AccountDataContract> GetAccounts(bool zeroSaldi, bool withSaldi, bool includeDescriptions, bool includeMappings, string culture)
        {
            List<AccountDataContract> accounts = new List<AccountDataContract>();
            if (!zeroSaldi && !withSaldi) return accounts;


            if (zeroSaldi && withSaldi)
            {

                accounts = Schema.Accounts.Values.ToList();

            }
            else if ((!zeroSaldi && withSaldi) || (zeroSaldi && !withSaldi))
            {

                if ((!zeroSaldi && withSaldi)) accounts = Schema.Accounts.IndexedList(Schema.SaldiAccounts);
                if ((zeroSaldi && !withSaldi)) accounts = Schema.Accounts.IndexedList(Schema.ZeroAccounts);


            }

            EnrichAccountData(ref accounts, includeDescriptions, includeMappings, culture);

            return accounts;
        }

        public List<AccountDataContract> GetAccountsInRanges(List<string> ranges, bool includeDescriptions, bool includeMappings, string culture)
        {
            return GetAccountsInRanges(ranges, true, true, includeDescriptions, includeMappings, culture);
        }

        public List<AccountDataContract> GetAccountsInRanges(List<string> ranges, bool zeroSaldi, bool withSaldi, bool includeDescriptions, bool includeMappings, string culture)
        {
            if (!zeroSaldi && !withSaldi) return new List<AccountDataContract>();
            // build keys query
            string rangeQueryFormat = "StartsWith(@{0})";
            string query = "{0}";
            StringBuilder rangeBuilder = new StringBuilder();
            List<object> variables = new List<object>();

            foreach (var range in ranges)
            {
                if (rangeBuilder.Length > 0) rangeBuilder.Append(" || ");
                variables.Add(range);
                rangeBuilder.Append(string.Format(rangeQueryFormat, (variables.Count - 1)));

            }

            query = string.Format(query, rangeBuilder.ToString());

            List<string> keys = new List<string>();
            List<AccountDataContract> accounts = new List<AccountDataContract>();


            if (zeroSaldi && Schema.ZeroAccounts != null) keys.AddRange(Schema.ZeroAccounts);
            if (withSaldi && Schema.SaldiAccounts!=null) keys.AddRange(Schema.SaldiAccounts);


            keys = keys.AsQueryable().Where(query, variables.ToArray()).Distinct().ToList();

            accounts = Schema.Accounts.IndexedList(keys);

            EnrichAccountData(ref accounts, includeDescriptions, includeMappings, culture);



            return accounts;
        }

        public List<AccountDataContract> GetAccountsOfMapping(Guid itemId, bool includeEmpties, bool includeDescriptions, bool includeMappings, string culture)
        {
            return GetAccountsOfMapping(itemId, includeEmpties, !includeEmpties,true, includeDescriptions, includeMappings, culture);
        }

        public List<AccountDataContract> GetAccountsOfMapping(Guid itemId, bool includeEmpties, bool zeroSaldi, bool withSaldi, bool includeDescriptions, bool includeMappings, string culture)
        {
            List<AccountDataContract> accounts = new List<AccountDataContract>();
            if (Schema.AccountMappings == null) return accounts;
            List<string> accountKeys = Schema.AccountMappings.Where(a => a.MappingItemId == itemId).Select(a => a.InternalNr).ToList();

            if (!zeroSaldi) accountKeys.RemoveAll(a => Schema.ZeroAccounts.Contains(a));
            if (!withSaldi) accountKeys.RemoveAll(a => Schema.SaldiAccounts.Contains(a));

            accounts = Schema.Accounts.IndexedList(accountKeys);
            accounts.RemoveAll(a => a.HasTaxAdjustments);
            EnrichAccountData(ref accounts, includeDescriptions, includeMappings, culture);
            return accounts;
        }

        public List<AccountDataContract> GetAccountsOfMapping(string key, bool includeEmpties, bool includeDescriptions, bool includeMappings, string culture)
        {
            return GetAccountsOfMappingMeta(null, key, includeEmpties, includeEmpties, true, includeDescriptions, includeMappings, culture);
        }

        public List<AccountDataContract> GetAccountsOfMapping(string key, bool includeEmpties, bool zeroSaldi, bool withSaldi, bool includeDescriptions, bool includeMappings, string culture)
        {
            return GetAccountsOfMappingMeta(null, key, includeEmpties, zeroSaldi, withSaldi, includeDescriptions, includeMappings, culture);
        }

        // key is the old value ?
        public List<AccountDataContract> GetAccountsOfMappingMeta(string meta, string key, bool includeEmpties, bool includeDescriptions, bool includeMappings, string culture)
        {

            return GetAccountsOfMappingMeta(meta, key, includeEmpties, true, true, includeDescriptions, includeMappings, culture);
        }

        public List<AccountDataContract> GetAccountsOfMappingMeta(string meta, string key, bool includeEmpties, bool zeroSaldi, bool withSaldi, bool includeDescriptions, bool includeMappings, string culture)
        {
            List<int> mapIdxs = new List<int>();
            List<string> accountKeys = new List<string>();
            List<AccountDataContract> accounts = new List<AccountDataContract>();
            if (!zeroSaldi && !withSaldi) return accounts;
            if (Schema.AccountMappingMappedKeyIndex == null) return accounts;
            if (!Schema.AccountMappingMappedKeyIndex.ContainsKey(key)) return new List<AccountDataContract>();

            mapIdxs = Schema.AccountMappingMappedKeyIndex[key];
            if (!string.IsNullOrEmpty(meta))
            {
                if (includeEmpties)
                {
                    accountKeys = Schema.AccountMappings.ElementsAt(mapIdxs).Where(m => m.MappingMeta == meta).Select(m => m.InternalNr).Distinct().ToList();
                }
                else
                {
                    accountKeys = Schema.AccountMappings.ElementsAt(mapIdxs).Where(m => m.MappingMeta == meta && m.MappingItemId.HasValue).Select(m => m.InternalNr).Distinct().ToList();
                }
            }
            else
            {
                if (includeEmpties)
                {
                    accountKeys = Schema.AccountMappings.ElementsAt(mapIdxs).Select(m => m.InternalNr).Distinct().ToList();
                }
                else
                {
                    accountKeys = Schema.AccountMappings.ElementsAt(mapIdxs).Where(a => a.MappingItemId.HasValue).Select(m => m.InternalNr).Distinct().ToList();
                }
            }

            if (!zeroSaldi) accountKeys.RemoveAll(a => Schema.ZeroAccounts.Contains(a));
            if (!withSaldi) accountKeys.RemoveAll(a => Schema.SaldiAccounts.Contains(a));

            accounts = Schema.Accounts.IndexedList(accountKeys);
            accounts.RemoveAll(a => a.HasTaxAdjustments);
            EnrichAccountData(ref accounts, includeDescriptions, includeMappings, culture);




            return accounts;
        }


        public List<AccountDescriptionDataContract> GetAccountDescriptions(string accountNr)
        {
            List<AccountDescriptionDataContract> ads = new List<AccountDescriptionDataContract>();


            if (Schema.AccountDescriptionsIndex.ContainsKey(accountNr))
            {
                ads = Schema.AccountDescriptions.ElementsAt(Schema.AccountDescriptionsIndex[accountNr]);
            }

            return ads;
        }


        public List<AccountMappingDataContract> GetAccountMappings(string accountNr)
        {
            List<AccountMappingDataContract> ams = new List<AccountMappingDataContract>();

            if (Schema.AccountMappingsAccountIndex.ContainsKey(accountNr))
            {
                ams = Schema.AccountMappings.ElementsAt(Schema.AccountMappingsAccountIndex[accountNr]);
            }


            return ams;
        }

        private List<AccountMappingDataContract> GetMappingsPrivate(string accountNr)
        {
            if (Schema.AccountMappingsAccountIndex == null || Schema.AccountMappings == null) return new List<AccountMappingDataContract>();
            if (!Schema.AccountMappingsAccountIndex.ContainsKey(accountNr)) return new List<AccountMappingDataContract>();
            return Schema.AccountMappings.ElementsAt(Schema.AccountMappingsAccountIndex[accountNr]);
        }

        private List<AccountDescriptionDataContract> GetAccountDescriptionsPrivate(string accountNr)
        {
            if (Schema.AccountDescriptionsIndex == null) return new List<AccountDescriptionDataContract>();
            if (!Schema.AccountDescriptionsIndex.ContainsKey(accountNr)) return new List<AccountDescriptionDataContract>();
            return Schema.AccountDescriptions.ElementsAt(Schema.AccountDescriptionsIndex[accountNr]);
        }

        public List<AccountDataContract> GetAccountsAdjustedTree(bool includeDescriptions, bool includeMappings, string culture, bool? includezeroes, bool? mappedOnly, string query)
        {
            List<string> haveAdjustments = new List<string>();
            List<string> parentkeys = new List<string>();
            List<string> adjusted = new List<string>();
            List<AccountDataContract> adjustments = new List<AccountDataContract>();
            List<AccountDataContract> parentAccounts = new List<AccountDataContract>();

            if (!includezeroes.HasValue) includezeroes = false;
            if (!mappedOnly.HasValue) mappedOnly = false;

            haveAdjustments = Schema.ParentAdjustedIndex.Keys.ToList();
            parentkeys = Schema.SaldiAccounts.Where(a => Schema.ParentAccounts.Contains(a) || haveAdjustments.Contains(a)).ToList();

            if (includezeroes==true)
            {
                parentkeys.AddRange(Schema.ZeroAccounts.Where(a => Schema.ParentAccounts.Contains(a) || haveAdjustments.Contains(a)).ToList());
                parentkeys = parentkeys.OrderBy(q => q).Distinct().ToList();
            }

            adjusted = Schema.AdjustedAccounts;

            adjustments = Schema.Accounts.IndexedList(adjusted);
            parentAccounts = Schema.Accounts.IndexedList(parentkeys);

            if (mappedOnly==true)
            {
                parentAccounts=  parentAccounts.Where(a => Schema.AccountMappingsAccountIndex.ContainsKey(a.InternalNr)).ToList();
            }

            if (!string.IsNullOrEmpty(query))
            {
                query = query.Trim();
                parentAccounts = parentAccounts.Where(a => a.InternalNr.StartsWith(query) || a.DefaultDescription.ToLower().Contains(query)).ToList();
            }

            EnrichAccountData(ref adjustments, includeDescriptions, includeMappings, culture);
            EnrichAccountData(ref parentAccounts, includeDescriptions, includeMappings, culture);



            foreach (AccountDataContract adc in parentAccounts.Where(a => haveAdjustments.Contains(a.InternalNr)))
            {
                adc.Adjustments = adjustments.Where(a => a.InternalNr.StartsWith(adc.InternalNr)).OrderBy(a => a.InternalNr).ToList();
            }
            return parentAccounts;
        }



        #endregion
    }
}
