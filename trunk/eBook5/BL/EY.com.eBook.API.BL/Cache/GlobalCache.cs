﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Collections;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.Core.EF.Read;
using EY.com.eBook.Core.EF;

namespace EY.com.eBook.Caching
{
    public class GlobalCache
    {
        private static GlobalCache _current = new GlobalCache();

        private ReaderWriterLockSlim _itemsLock = new ReaderWriterLockSlim();
        private ReaderWriterLockSlim _indexLock = new ReaderWriterLockSlim();

        private static Hashtable _items = new Hashtable();

        //private static Dictionary<Guid, List<Guid>> _listItemIdx = new Dictionary<Guid,List<Guid>>();
        //private static Dictionary<string, List<Guid>> _keyedIdx = new Dictionary<string,List<Guid>>();
        //private static Dictionary<string, List<Guid>> _groupedIdx = new Dictionary<string,List<Guid>>();
        //private static Dictionary<int, List<Guid>> _assessmentIdx = new Dictionary<int,List<Guid>>();

        public int CachedItemsNumber
        {
            get
            {
                _itemsLock.EnterReadLock();
                try
                {
                    return _items.Count;
                }
                finally
                {
                    _itemsLock.ExitReadLock();
                }
            }
        }

        private GlobalCache()
        {
           
        }

        public static GlobalCache Current
        {
            get
            {
                return _current;
            }
        }

        public object this[object key]
        {
            get
            {
                CacheItem res = (CacheItem)_items[key];
                if (res != null)
                {
                    if (res.SlidingExpirationTime.TotalMilliseconds > 0)
                    {
                        _itemsLock.EnterWriteLock();
                        try
                        {
                            res.LastAccessTime = DateTime.Now;
                        }
                        finally
                        {
                            _itemsLock.ExitWriteLock();
                        }
                    }
                    return res.ItemValue;
                }
                else
                {
                    return null;
                }
            }
            set
            {
                _itemsLock.EnterWriteLock();
                try
                {
                   
                    _items[key] = new CacheItem(value);
                }
                finally
                {
                    _itemsLock.ExitWriteLock();
                }
            }
        }

        public void Insert(object key, object value)
        {
            _itemsLock.EnterWriteLock();
            try
            {
                _items[key] = new CacheItem(value);
            }
            finally
            {
                _itemsLock.ExitWriteLock();
            }
        }

        public void Insert(object key, object value, DateTime expirationDate)
        {
            _itemsLock.EnterWriteLock();
            try
            {
                _items[key] = new CacheItem(value, expirationDate);
            }
            finally
            {
                _itemsLock.ExitWriteLock();
            }
        }

        public void Insert(object key, object value, TimeSpan expirationTime)
        {
            _itemsLock.EnterWriteLock();
            try
            {
                _items[key] = new CacheItem(value, expirationTime);
            }
            finally
            {
                _itemsLock.ExitWriteLock();
            }
        }

        public void Insert(object key, object value, TimeSpan expirationTime, bool slidingExpiration)
        {
            _itemsLock.EnterWriteLock();
            try
            {
                _items[key] = new CacheItem(value, expirationTime, slidingExpiration);
            }
            finally
            {
                _itemsLock.ExitWriteLock();
            }
        }

        public void Insert(object key, object value, DateTime expirationDate, TimeSpan slidingExpirationTime)
        {
            _itemsLock.EnterWriteLock();
            try
            {
                _items[key] = new CacheItem(value, expirationDate, slidingExpirationTime);
            }
            finally
            {
                _itemsLock.ExitWriteLock();
            }
        }

        public void Remove(object key)
        {
            _itemsLock.EnterWriteLock();
            try
            {
                _items.Remove(key);
            }
            finally
            {
                _itemsLock.ExitWriteLock();
            }
        }


        private void CacherefreshTimerCallback(object state)
        {
            _itemsLock.EnterUpgradeableReadLock();
            try
            {
                Dictionary<object, CacheItem> delItems = new Dictionary<object, CacheItem>();
                DateTime dtNow = DateTime.Now;
                foreach (DictionaryEntry de in _items)
                {
                    CacheItem ci = (CacheItem)de.Value;
                    if (ci.ExpirationDate < dtNow)
                    {
                        delItems.Add(de.Key, ci);
                    }
                    else
                    {
                        if (ci.SlidingExpirationTime.TotalMilliseconds > 0)
                        {
                            if (dtNow.Subtract(ci.LastAccessTime).TotalMilliseconds > ci.SlidingExpirationTime.TotalMilliseconds)
                            {
                                delItems.Add(de.Key, ci);
                            }
                        }
                    }
                }

                if (delItems.Count > 0)
                {
                    _itemsLock.EnterWriteLock();
                    try
                    {
                        foreach (KeyValuePair<object, CacheItem> kvp in delItems)
                        {
                            if (_items.ContainsKey(kvp.Key))
                            {
                                _items.Remove(kvp.Key);
                            }
                        }
                    }
                    finally
                    {
                        _itemsLock.ExitWriteLock();
                    }
                }
            }
            finally
            {
                _itemsLock.ExitUpgradeableReadLock();
            }
        }

        
   

        public List<T> GetAllOfKey<T>(GlobalCacheKey keyCode)
        {
            List<string> keys = new List<string>();
            foreach (string key in _items.Keys)
            {
                if (key.StartsWith(keyCode.Value)) keys.Add(key);
            }
            return _items.IndexedList<string, CacheItem>(keys)
                            .Select(c => c.ItemValue)
                            .Cast<T>()
                            .ToList() ;
        }
    }
    public sealed class GlobalCacheKey
    {
        private readonly string name;
        private readonly string value;
        private readonly Type type;

        public static readonly GlobalCacheKey MappingItem = new GlobalCacheKey("MPL", "MappingListItemDataContract", typeof(MappingListItemDataContract));
        public static readonly GlobalCacheKey WorksheetTypeMeta= new GlobalCacheKey("WTT", "WorksheetTypeMeta", typeof(WorksheetTypeMetaDataContract));
        public static readonly GlobalCacheKey WorksheetTypeTranslation = new GlobalCacheKey("WTT", "WorksheetTypeTranslation", typeof(WorksheetTypeTranslationDataContract));
        public static readonly GlobalCacheKey WorksheetTypeTabTranslation = new GlobalCacheKey("WTT", "WorksheetTypeTabTranslation", typeof(WorksheetTypeTranslationDataContract));
        public static readonly GlobalCacheKey MessageTranslation = new GlobalCacheKey("MT", "MessageTranslation", typeof(MessageTranslation));
        public static readonly GlobalCacheKey WorksheetTypeMessageTranslation = new GlobalCacheKey("WT-MT", "MessageTranslation", typeof(MessageTranslation));

        private GlobalCacheKey(string value, string name, Type type)
        {
            this.name = name;
            this.value = value;
            this.type = type;
        }

        public override string ToString()
        {
            return name;
        }

        public string Value { get { return value; } }

        
    }
}
