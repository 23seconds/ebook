﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Data.Linq;
using System.Linq.Dynamic;
using System.Text;
using EY.com.eBook.Core;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.Core.EF;
using EY.com.eBook.API.Contracts.Data.Logic;
using EY.com.eBook.Core.EF.Write;
using System.Data.Objects;
using EY.com.eBook.Core.EntityTranslation;
using EY.com.eBook.API.BL.Translators;
using System.ServiceModel;
using EY.com.eBook.Core.Data;
using EY.com.eBook.API.Contracts.Proxies;
using EY.com.eBook.API.Contracts.Data.Cache;
using EY.com.eBook.API.Contracts;

namespace EY.com.eBook.API.BL.Write
{
    

    public class GTHRepository : RepositoryBase
    {
        private bool _cachingActive=false;
        private CacheHelper _cacheHelper;



        public GTHRepository()
        {
            //_cacheHelper = new CacheHelper();
            //if (_cacheHelper.CacheServiceConfigured)
            //{
            //    _cacheHelper.CreateCacheProxy();
            //    _cachingActive = _cacheHelper.CacheRunning;
            //}
        }

        private void CopyToNewClientTeam(GTHTeamClient gth, int teamId)
        {
            CopyToNewClientTeam(gth, teamId, null, null);
        }

        private void CopyToNewClientTeam(GTHTeamClient gth,int teamId, DateTime? start, DateTime? end)
        {

            if (eBookWriteManager.Context.GTHTeamClients.Count(g =>g.TeamId==teamId &&  g.ClientId == gth.ClientId && g.PeriodStart == start && g.PeriodEnd == end) == 0)
            {
                GTHTeamClient nw = new GTHTeamClient
                {
                    Id = Guid.NewGuid()
                    ,
                    ClientId = gth.ClientId
                    ,
                    LastUpdate = DateTime.Now
                    ,
                    PersonId = gth.PersonId
                    ,
                    Status = "TODO"
                    ,
                    TeamId = teamId
                    ,
                    PeriodStart = start
                    ,
                    PeriodEnd = end
                };
                eBookWriteManager.Context.AddToGTHTeamClients(nw);
                eBookWriteManager.Context.SaveChanges();
            }
        }

        public bool ProcessNBBFiles(CriteriaProcessFilesDataContract cpfdc)
        {
            GTHTeamClient gtc = eBookWriteManager.Context.GTHTeamClients.First(t => t.Id == cpfdc.GTHClientId);
            BL.Read.Repository rep = new EY.com.eBook.API.BL.Read.Repository();

            List<GTHPeriodDataContract> periods = new List<GTHPeriodDataContract>();



            foreach (UploadedFileDataContract file in cpfdc.Files)
            {

                GTHPeriodDataContract period = new GTHPeriodDataContract { Start = file.Start.ToDateOnly(), End = file.End.ToDateOnly() };

                if (periods.Count(p => p.Start == period.Start && p.End == period.End) == 0) periods.Add(period);
                
                // COPY IS DONE IN REPOSITORY BL
                //string path = System.IO.Path.Combine(EY.com.eBook.Core.Config.UploadFolder, file.FileName);
                //if (!System.IO.File.Exists(path)) throw new Exception("File not found");
                //string newPath = System.IO.Path.Combine(EY.com.eBook.Core.Config.RepositoryLocal, gtc.ClientId.ToPath());
                //newPath = System.IO.Path.Combine(newPath, file.FileName);
                //System.IO.File.Move(path, newPath);
                rep.SaveRepositoryItem(new CriteriaSaveRepositoryItemDataContract
                {
                    IsNew = true
                    ,
                    Item = new RepositoryItemDataContract
                    {
                        ClientId = gtc.ClientId
                        ,
                        ContentType = file.ContentType
                        ,
                        Extension = file.Extension
                        ,
                        FileName = System.IO.Path.GetFileNameWithoutExtension(file.Name)
                        ,
                        Id = file.Id
                        , PhysicalFileId=file.Id
                        ,
                        Meta = new List<RepositoryMetaItemDataContract>()
                        ,
                        PeriodEnd = file.End
                        ,
                        PeriodStart = file.Start
                        ,
                        Prequel = string.Empty
                        ,
                        Status = null
                        ,
                        StructureId = cpfdc.StructureId
                    }
                });
            }
            gtc.DueDate = null;
            gtc.SubStatus = null;
            gtc.PeriodStart = null;
            gtc.PeriodEnd = null;
            gtc.Status = cpfdc.Status;
            gtc.PersonId = cpfdc.PersonId;
            gtc.LastUpdate = DateTime.Now;
            eBookWriteManager.Context.SaveChanges();

            DateTime dte = new DateTime(2010,12,31).AddMinutes(-1);

            foreach (GTHPeriodDataContract period in periods.Where(p => p.End > dte))
            {
                CopyToNewClientTeam(gtc, 2, period.Start, period.End);
                CopyToNewClientTeam(gtc, 3, period.Start, period.End);
                CopyToNewClientTeam(gtc, 4, period.Start, period.End);
            }

            return true;
        }

        public bool ProcessTeam2Files(CriteriaProcessFilesDataContract cpfdc)
        {
            GTHTeamClient gtc = eBookWriteManager.Context.GTHTeamClients.First(t => t.Id == cpfdc.GTHClientId);
            BL.Read.Repository rep = new EY.com.eBook.API.BL.Read.Repository();

           
            foreach (UploadedFileDataContract file in cpfdc.Files)
            {

                
                // COPY IS DONE IN REPOSITORY BL
                //string path = System.IO.Path.Combine(EY.com.eBook.Core.Config.UploadFolder, file.FileName);
                //if (!System.IO.File.Exists(path)) throw new Exception("File not found");
                //string newPath = System.IO.Path.Combine(EY.com.eBook.Core.Config.RepositoryLocal, gtc.ClientId.ToPath());
                //newPath = System.IO.Path.Combine(newPath, file.FileName);
                //System.IO.File.Move(path, newPath);
                rep.SaveRepositoryItem(new CriteriaSaveRepositoryItemDataContract
                {
                    IsNew = true
                    ,
                    Item = new RepositoryItemDataContract
                    {
                        ClientId = gtc.ClientId
                        ,
                        ContentType = file.ContentType
                        ,
                        Extension = file.Extension
                        ,
                        FileName = string.Concat(System.IO.Path.GetFileNameWithoutExtension(file.Name),!string.IsNullOrEmpty(file.FileNameParentheticalRemark) ? " (" + file.FileNameParentheticalRemark + ")" : "")
                        ,
                        Id = file.Id
                        ,
                        PhysicalFileId = file.Id
                        ,
                        Meta = new List<RepositoryMetaItemDataContract>()
                        ,
                        PeriodEnd = file.End
                        ,
                        PeriodStart = file.Start
                        ,
                        Prequel = string.Empty
                        ,
                        Status = null
                        ,
                        StructureId = cpfdc.StructureId
                    }
                });
            }
            gtc.DueDate = null;
            gtc.SubStatus = null;
            //gtc.PeriodStart = gtc.per;
            //gtc.PeriodEnd = null;
            gtc.Status = cpfdc.Status;
            gtc.PersonId = cpfdc.PersonId;
            gtc.LastUpdate = DateTime.Now;
            eBookWriteManager.Context.SaveChanges();

            return true;
        }
               
        public bool ProcessTeam6(CriteriaProcessFilesDataContract cpfdc)
        {
            GTHTeamClient gtc = eBookWriteManager.Context.GTHTeamClients.First(t => t.ClientId == cpfdc.GTHClientId && t.TeamId == cpfdc.TeamId);
                        
            gtc.DueDate = null;
            gtc.SubStatus = null;
            //gtc.PeriodStart = gtc.per;
            //gtc.PeriodEnd = null;
            gtc.Status = cpfdc.Status;
            gtc.PersonId = cpfdc.PersonId;
            gtc.LastUpdate = DateTime.Now;
            eBookWriteManager.Context.SaveChanges();

            return true;
        }

        public Guid SelectClient(CriteriaClientTeamDataContract cctdc)
        {
            GTHTeamClient gtc;
            if (cctdc.GTHId.HasValue)
            {
                gtc = eBookWriteManager.Context.GTHTeamClients.FirstOrDefault(t => t.Id==cctdc.GTHId.Value);
                if (gtc.Status == "TODO" || (gtc.Status!="TODO" && gtc.Status!="FINAL" && gtc.PersonId==cctdc.PersonId))
                {
                    return gtc.Id;
                } else {
                    Client cl = eBookWriteManager.Context.ClientSet.First(c => c.Id == gtc.ClientId);
                    Person pr = eBookWriteManager.Context.PersonSet.First(p => p.Id == gtc.PersonId);
                    throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { ErrorCode = -1, Message =string.Format("The client {0} is already at status {1} by {2} {3}. Please select another client.",cl.Name,gtc.Status,pr.FirstName,pr.LastName) ,  FileId = Guid.Empty });
                }
                
            }
            else
            {
                gtc = eBookWriteManager.Context.GTHTeamClients.FirstOrDefault(t => t.TeamId == cctdc.TeamId && t.Status == "TODO");
                if (gtc != null)
                {
                    gtc.Status = cctdc.Status;
                    gtc.PersonId = cctdc.PersonId;
                    gtc.LastUpdate = DateTime.Now;
                    eBookWriteManager.Context.SaveChanges();
                    return gtc.Id;
                }
                else
                {
                    return Guid.Empty;
                }
            }
            
        }

        public Guid SelectTeam2Work(Guid PersonId)
        {
            DateTime now= DateTime.Now;

            // check if remaining work item exist for this person (aka. crash recovery)
            GTHTeamClient gtc = eBookWriteManager.Context.GTHTeamClients.FirstOrDefault(g =>
                                   g.TeamId == 2 && g.PickedUp && g.PersonId == PersonId);

            if (gtc != null)
            {
                return gtc.Id;
            }
            else
            {
                // Check if an overdue exists that needs to be handled first.
                gtc = eBookWriteManager.Context.GTHTeamClients.FirstOrDefault(g =>
                                        g.TeamId == 2 && g.DueDate != null
                                        && g.DueDate < now && g.Status != "FINAL"
                                        && g.SubStatus != "FURTHERFOLLOWUP_EYB"
                                        && !g.PickedUp
                                        );
                if (gtc != null)
                {
                    gtc.PickedUp = true;
                    gtc.PersonId = PersonId;
                    gtc.LastUpdate = DateTime.Now;
                    eBookWriteManager.Context.SaveChanges();
                    return gtc.Id;
                }
                else
                {
                    // Last remaining: pick first free Todo/gth status work item (no ordering/random pick)
                    gtc = eBookWriteManager.Context.GTHTeamClients.FirstOrDefault(g =>
                                        g.TeamId == 2 && (g.Status == "TODO" || g.Status.Contains("GTH"))
                                        && !g.PickedUp
                                        );
                    if (gtc != null)
                    {
                        gtc.PickedUp = true;
                        gtc.PersonId = PersonId;
                        gtc.LastUpdate = DateTime.Now;
                        eBookWriteManager.Context.SaveChanges();
                        return gtc.Id;
                    }

                }
            }
            return Guid.Empty;

        }

        public void SavePerson(CriteriaPersonDataContract cpdc)
        {
            Person existingPerson = eBookWriteManager.Context.PersonSet.FirstOrDefault(p => p.Gpn == cpdc.GPN);
            if (existingPerson == null)
            {
                Guid personId = Guid.NewGuid();
                Person person = new Person
                {
                    Id = personId,
                    Gpn = cpdc.GPN,
                    FirstName = cpdc.FirstName,
                    LastName = cpdc.LastName,
                    Email = cpdc.Email,
                    DefaultOffice = "00473",
                    WindowsAccount = "MEA\\" + cpdc.FirstName + "." + cpdc.LastName,
                    LastLogon = null,
                    DataUpdated = DateTime.Now,
                    EndedAt = null,
                    Manual = true,
                    Department = "ACR"

                };
                eBookWriteManager.Context.AddToPersonSet(person);                
            }
            else {
                existingPerson.FirstName = cpdc.FirstName;
                existingPerson.LastName = cpdc.LastName;
                existingPerson.Email = cpdc.Email;

            }
            eBookWriteManager.Context.SaveChanges();
            
        }

        public void SavePersonRoles(List<CriteriaPersonRolesDataContract> lcprdc) {
            foreach (CriteriaPersonRolesDataContract cprdc in lcprdc) {
                if (cprdc.Check)
                {
                    PersonRole prdc = new PersonRole
                    {
                        PersonId = cprdc.PersonId,
                        Role = cprdc.Role,
                        Manual = true
                    };
                    eBookWriteManager.Context.AddToPersonRoleSet(prdc);
                    eBookWriteManager.Context.SaveChanges();
                }
                else {
                    PersonRole prdc = eBookWriteManager.Context.PersonRoleSet.FirstOrDefault(p => p.PersonId == cprdc.PersonId && p.Role == cprdc.Role);
                    eBookWriteManager.Context.DeleteObject(prdc);
                    eBookWriteManager.Context.SaveChanges();
                }                
            }
        }

        public void ReplacePersons(CriteriaReplacePersonsDataContract crpdc) { 
            List<GTHTeamClient> lgtc = eBookWriteManager.Context.GTHTeamClients.Where(t => t.PersonId == crpdc.personFrom && t.Status == "INPROGRESS" && t.TeamId == crpdc.team).ToList();
            foreach (GTHTeamClient gtc in lgtc)
            {
                gtc.PersonId = crpdc.personTo;
                eBookWriteManager.Context.SaveChanges();
            }
        }

        protected override void RegisterTranslators()
        {
          

            
        }

       
    }
}
