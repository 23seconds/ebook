﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EY.com.eBook.API.Contracts.Data;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Reflection;
using System.Xml.XPath;
using System.Xml.Linq;
using System.Xml;
using EY.com.eBook.API.Contracts.Data.Meta;
using System.Collections;
using EY.com.eBook.API.Contracts.Data.Cache;

namespace EY.com.eBook.API.BL.Xslt
{
    public class DocumentHelper
    {
        private string[] _xmls = new string[] { "&#8204;", "&#8205;", "&#918;", "&#950;", "&#255;", "&#376;", "&#165;", "&#221;", "&#253;", "&#926;", "&#958;", "&#8472;", "&#220;", "&#252;", "&#933;", "&#965;", "&#978;", "&#168;", "&#217;", "&#249;", "&#219;", "&#251;", "&#8593;", "&#8657;", "&#218;", "&#250;", "&#8482;", "&#215;", "&#732;", "&#222;", "&#254;", "&#8201;", "&#977;", "&#920;", "&#952;", "&#8756;", "&#932;", "&#964;", "&#223;", "&#8839;", "&#179;", "&#178;", "&#185;", "&#8835;", "&#8721;", "&#8838;", "&#8834;", "&#9824;", "&#8764;", "&#962;", "&#931;", "&#963;", "&#173;", "&#167;", "&#8901;", "&#352;", "&#353;", "&#8218;", "&#8217;", "&#8250;", "&#8207;", "&#929;", "&#961;", "&#8971;", "&#174;", "&#8476;", "&#8221;", "&#8969;", "&#8594;", "&#8658;", "&#187;", "&#10217;", "&#8730;", "&#34;", "&#936;", "&#968;", "&#8733;", "&#8719;", "&#8242;", "&#8243;", "&#163;", "&#177;", "&#982;", "&#928;", "&#960;", "&#934;", "&#966;", "&#8869;", "&#8240;", "&#8706;", "&#182;", "&#214;", "&#246;", "&#8855;", "&#213;", "&#245;", "&#216;", "&#248;", "&#186;", "&#170;", "&#8744;", "&#8853;", "&#927;", "&#959;", "&#937;", "&#969;", "&#8254;", "&#210;", "&#242;", "&#338;", "&#339;", "&#212;", "&#244;", "&#211;", "&#243;", "&#925;", "&#957;", "&#209;", "&#241;", "&#8836;", "&#8713;", "&#172;", "&#8715;", "&#8800;", "&#8211;", "&#160;", "&#8711;", "&#924;", "&#956;", "&#8722;", "&#183;", "&#181;", "&#8212;", "&#175;", "&#60;", "&#8216;", "&#8249;", "&#8206;", "&#9674;", "&#8727;", "&#8970;", "&#8804;", "&#8220;", "&#8968;", "&#8592;", "&#8656;", "&#171;", "&#10216;", "&#923;", "&#955;", "&#922;", "&#954;", "&#207;", "&#239;", "&#8712;", "&#191;", "&#921;", "&#953;", "&#8747;", "&#8734;", "&#8465;", "&#204;", "&#236;", "&#161;", "&#206;", "&#238;", "&#205;", "&#237;", "&#8230;", "&#9829;", "&#8596;", "&#8660;", "&#62;", "&#8805;", "&#915;", "&#947;", "&#8260;", "&#190;", "&#188;", "&#189;", "&#8704;", "&#402;", "&#8707;", "&#8364;", "&#203;", "&#235;", "&#208;", "&#240;", "&#919;", "&#951;", "&#8801;", "&#917;", "&#949;", "&#8194;", "&#8195;", "&#8709;", "&#200;", "&#232;", "&#202;", "&#234;", "&#201;", "&#233;", "&#247;", "&#9830;", "&#916;", "&#948;", "&#176;", "&#8595;", "&#8659;", "&#8224;", "&#8225;", "&#164;", "&#8746;", "&#8629;", "&#169;", "&#8773;", "&#9827;", "&#710;", "&#935;", "&#967;", "&#162;", "&#184;", "&#199;", "&#231;", "&#8745;", "&#8226;", "&#166;", "&#914;", "&#946;", "&#8222;", "&#196;", "&#228;", "&#195;", "&#227;", "&#8776;", "&#197;", "&#229;", "&#39;", "&#8736;", "&#8743;", "&#38;", "&#913;", "&#945;", "&#8501;", "&#192;", "&#224;", "&#198;", "&#230;", "&#180;", "&#194;", "&#226;", "&#193;", "&#225;", "&#160;", "&#161;", "&#162;", "&#163;", "&#164;", "&#165;", "&#166;", "&#167;", "&#168;", "&#169;", "&#170;", "&#171;", "&#172;", "&#173;", "&#174;", "&#175;", "&#176;", "&#177;", "&#178;", "&#179;", "&#180;", "&#181;", "&#182;", "&#183;", "&#184;", "&#185;", "&#186;", "&#187;", "&#188;", "&#189;", "&#190;", "&#191;", "&#215;", "&#247;" };
        private string[] _htmls = new string[] { "&zwnj;", "&zwj;", "&Zeta;", "&zeta;", "&yuml;", "&Yuml;", "&yen;", "&Yacute;", "&yacute;", "&Xi;", "&xi;", "&weierp;", "&Uuml;", "&uuml;", "&Upsilon;", "&upsilon;", "&upsih;", "&uml;", "&Ugrave;", "&ugrave;", "&Ucirc;", "&ucirc;", "&uarr;", "&uArr;", "&Uacute;", "&uacute;", "&trade;", "&times;", "&tilde;", "&THORN;", "&thorn;", "&thinsp;", "&thetasym;", "&Theta;", "&theta;", "&there4;", "&Tau;", "&tau;", "&szlig;", "&supe;", "&sup3;", "&sup2;", "&sup1;", "&sup;", "&sum;", "&sube;", "&sub;", "&spades;", "&sim;", "&sigmaf;", "&Sigma;", "&sigma;", "&shy;", "&sect;", "&sdot;", "&Scaron;", "&scaron;", "&sbquo;", "&rsquo;", "&rsaquo;", "&rlm;", "&Rho;", "&rho;", "&rfloor;", "&reg;", "&real;", "&rdquo;", "&rceil;", "&rarr;", "&rArr;", "&raquo;", "&rang;", "&radic;", "&quot;", "&Psi;", "&psi;", "&prop;", "&prod;", "&prime;", "&Prime;", "&pound;", "&plusmn;", "&piv;", "&Pi;", "&pi;", "&Phi;", "&phi;", "&perp;", "&permil;", "&part;", "&para;", "&Ouml;", "&ouml;", "&otimes;", "&Otilde;", "&otilde;", "&Oslash;", "&oslash;", "&ordm;", "&ordf;", "&or;", "&oplus;", "&Omicron;", "&omicron;", "&Omega;", "&omega;", "&oline;", "&Ograve;", "&ograve;", "&OElig;", "&oelig;", "&Ocirc;", "&ocirc;", "&Oacute;", "&oacute;", "&Nu;", "&nu;", "&Ntilde;", "&ntilde;", "&nsub;", "&notin;", "&not;", "&ni;", "&ne;", "&ndash;", "&nbsp;", "&nabla;", "&Mu;", "&mu;", "&minus;", "&middot;", "&micro;", "&mdash;", "&macr;", "&lt;", "&lsquo;", "&lsaquo;", "&lrm;", "&loz;", "&lowast;", "&lfloor;", "&le;", "&ldquo;", "&lceil;", "&larr;", "&lArr;", "&laquo;", "&lang;", "&Lambda;", "&lambda;", "&Kappa;", "&kappa;", "&Iuml;", "&iuml;", "&isin;", "&iquest;", "&Iota;", "&iota;", "&int;", "&infin;", "&image;", "&Igrave;", "&igrave;", "&iexcl;", "&Icirc;", "&icirc;", "&Iacute;", "&iacute;", "&hellip;", "&hearts;", "&harr;", "&hArr;", "&gt;", "&ge;", "&Gamma;", "&gamma;", "&frasl;", "&frac34;", "&frac14;", "&frac12;", "&forall;", "&fnof;", "&exist;", "&euro;", "&Euml;", "&euml;", "&ETH;", "&eth;", "&Eta;", "&eta;", "&equiv;", "&Epsilon;", "&epsilon;", "&ensp;", "&emsp;", "&empty;", "&Egrave;", "&egrave;", "&Ecirc;", "&ecirc;", "&Eacute;", "&eacute;", "&divide;", "&diams;", "&Delta;", "&delta;", "&deg;", "&darr;", "&dArr;", "&dagger;", "&Dagger;", "&curren;", "&cup;", "&crarr;", "&copy;", "&cong;", "&clubs;", "&circ;", "&Chi;", "&chi;", "&cent;", "&cedil;", "&Ccedil;", "&ccedil;", "&cap;", "&bull;", "&brvbar;", "&Beta;", "&beta;", "&bdquo;", "&Auml;", "&auml;", "&Atilde;", "&atilde;", "&asymp;", "&Aring;", "&aring;", "&apos;", "&ang;", "&and;", "&amp;", "&Alpha;", "&alpha;", "&alefsym;", "&Agrave;", "&agrave;", "&AElig;", "&aelig;", "&acute;", "&Acirc;", "&acirc;", "&Aacute;", "&aacute;", "&nbsp;", "&iexcl;", "&cent;", "&pound;", "&curren;", "&yen;", "&brvbar;", "&sect;", "&uml;", "&copy;", "&ordf;", "&laquo;", "&not;", "&shy;", "&reg;", "&macr;", "&deg;", "&plusmn;", "&sup2;", "&sup3;", "&acute;", "&micro;", "&para;", "&middot;", "&cedil;", "&sup1;", "&ordm;", "&raquo;", "&frac14;", "&frac12;", "&frac34;", "&iquest;", "&times;", "&divide;" };

        public FileDataContract File { get; set; }
        public FileMetaDataContract FileMeta { get; set; }
        public ClientDataContract Client { get; set; }
        public DocumentDataContract Document { get; set; }
        public CultureInfo Culture { get; set; }

        private string _color = "#000000";

        private Regex reTestExpression = new Regex(@"\'{0,1}(?<LeftFirst>(\w|\/){1,})\'{0,1}((?<LeftSeperator>(\:\:))(?<LeftSecond>\w{1,})){0,1}(?<Comparison>(\=|\<|\>|!)\={0,1})\'{0,1}(?<RightFirst>(\w|\/){1,})\'{0,1}((?<RightSeperator>(\:\:))(?<RightSecond>\w{1,})){0,1}");

        public bool ProcessTest(string test)
        {
            Match maCondition = reTestExpression.Match(test);

            string sideValue1 = maCondition.Groups["LeftFirst"].Value;
            if (maCondition.Groups["LeftSeperator"].Success)
            {
                sideValue1 = GetValue(maCondition.Groups["LeftFirst"].Value, maCondition.Groups["LeftSecond"].Value, null);
            }
            string sideValue2 = maCondition.Groups["RightFirst"].Value;
            if (maCondition.Groups["RightSeperator"].Success)
            {
                sideValue1 = GetValue(maCondition.Groups["RightFirst"].Value, maCondition.Groups["RightSecond"].Value, null);
            }
            switch (maCondition.Groups["Comparison"].Value)
            {
                case "==":
                    Console.WriteLine("condition: {0} = {1}", test, (sideValue1.Equals(sideValue2, StringComparison.OrdinalIgnoreCase)));
                    return sideValue1.Equals(sideValue2, StringComparison.OrdinalIgnoreCase);
                case "!=":
                    Console.WriteLine("condition: {0} = {1}", test, (!sideValue1.Equals(sideValue2, StringComparison.OrdinalIgnoreCase)));
                    return !sideValue1.Equals(sideValue2, StringComparison.OrdinalIgnoreCase);
                /*
                 * Future change: datatype sensitive comparison
                 * 
                case ">=":
                   // return sideValue1 >= sideValue2;
                case "<=":
                    //return sideValue1 <= sideValue2;
                case ">":
                    //return sideValue1 > sideValue2;
                case "<":
                    //return sideValue1 < sideValue2;
                 * */
            }
            return false;
        }

        public string GetColumnWidth(string widths, int pos)
        {
            string[] tablewidths = widths.Split(new char[] { ';' });
            if (tablewidths.Length > pos) return tablewidths[pos];
            return string.Empty;
        }

        public decimal GetMetaListSum(string context, string subcontext,string sum)
        {
            decimal sm = 0;
            object instance = GetMetaListInstance(context, subcontext);
            if (instance == null) return 0;

            IList list = (IList)instance;
            if(list.Count==0) return 0;

            

            for (int i = 0; i < list.Count; i++)
            {
                
                object val = GetInstanceValue(list[i], sum);
                if (val == null)
                {
                    if (!string.IsNullOrEmpty(subcontext))
                    {
                        val = GetInstanceValue(list[i], subcontext);
                        if (val == null) continue;
                        val = GetInstanceValue(val, sum);
                    }
                }
                decimal dval = 0;
                decimal.TryParse(val.ToString(), out dval);
                sm += dval;
            }
            
            
            return sm;
        }

        public XPathNavigator GetMetaList(string context, string subcontext)
        {
            XElement root = new XElement("metas");
            object instance = GetMetaListInstance(context, subcontext);
            if (instance == null) return root.CreateNavigator();

            PropertyInfo prop = instance.GetType().GetProperty("Count");
            if (prop == null) return root.CreateNavigator();

            int cnt = (int)prop.GetValue(instance, null);
            for (int i = 0; i < cnt; i++)
            {
                root.Add(new XElement("meta", i));
            }
            return root.CreateNavigator();
        }

        public string GetMetaListItemValue(int idx, string context, string subcontext, string property, string format)
        {
            object instance = GetMetaListInstance(context, subcontext);
            if (instance == null) return string.Empty;
            IList list = (IList)instance;
            if (list.Count < (idx + 1)) return string.Empty;
            object val = null;
            if (!string.IsNullOrEmpty(subcontext))
            {
                val = GetInstanceValue(list[idx], subcontext);
                if (val == null) return string.Empty;
                val = GetInstanceValue(val, property);
            }
            else
            {
                val = GetInstanceValue(list[idx], property);
            }

            if (val == null) return string.Empty;

            return GetObjectValue(val, format);

        }

        private object GetMetaListInstance(string context, string subcontext)
        {
            object instance = GetMetaInstance(context, null);
            Type type = instance.GetType();
            if (!type.IsGenericType && type.GetGenericTypeDefinition() != typeof(List<>))
            {
                if (string.IsNullOrEmpty(subcontext)) return null;
                instance = GetMetaInstance(context, subcontext);
                type = instance.GetType();
                if (!type.IsGenericType && type.GetGenericTypeDefinition() != typeof(List<>)) return null;
            }
            return instance;
        }

        public string GetMetaValue(string context, string subcontext, string property, string format)
        {

            if (string.IsNullOrEmpty(property)) return string.Empty;
 
            object cInstance =GetMetaInstance(context,subcontext);
            if (cInstance == null) return string.Empty;
            
            object val = GetInstanceValue(cInstance, property);
            if (val == null) return string.Empty;

            return GetObjectValue(val, format);
        }

        private object GetMetaInstance(string context, string subcontext)
        {
            if (FileMeta == null) return null;
            object instance = FileMeta.Data;
            object cInstance = GetInstanceValue(instance, context);
            if (cInstance == null) return null;
            if (!string.IsNullOrEmpty(subcontext) && cInstance.GetType().IsClass)
            {
                cInstance = GetInstanceValue(cInstance, subcontext);
                if (cInstance == null) return null;
            }
            return cInstance;
        }

        //public string GetMetaValue(string context, string subcontext, string property, string format)

        private object GetInstanceValue(object instance, string property)
        {
            return GetInstanceValue(instance, property, null);
        }

        private object GetInstanceValue(object instance, string property,int? idx)
        {
            PropertyInfo propInfo = instance.GetType().GetProperty(property);
            if (propInfo == null) return null;
            if (idx.HasValue)
            {
                object[] indexArgs = { idx };
                return propInfo.GetValue(instance, indexArgs);
            }
            return propInfo.GetValue(instance, null);
        }

        public string GetValue(string type, string property, string format)
        {
            object instance = null;
            switch (type.ToUpper())
            {
                case "CLIENT":
                    instance = Client;
                    break;
                case "FILE":
                    instance = File;
                    break;
            }

            PropertyInfo propInfo = instance.GetType().GetProperty(property);
            if (propInfo != null)
            {
                object val = propInfo.GetValue(instance, null);
                if (val == null) return string.Empty;
                return GetObjectValue(val,format);
            }
            return string.Empty;

        }

        private string GetObjectValue(object val, string format)
        {
            switch (val.GetType().Name.ToLower())
            {
                case "datetime":
                    if (!string.IsNullOrEmpty(format))
                    {
                        return ((DateTime)val).ToString(format, Culture.DateTimeFormat);
                    }
                    return ((DateTime)val).ToString(CultureInfo.InvariantCulture.DateTimeFormat.ShortDatePattern, CultureInfo.InvariantCulture.DateTimeFormat);

                case "float":
                    if (!string.IsNullOrEmpty(format))
                    {
                        return ((float)val).ToString(format, Culture.NumberFormat);
                    }
                    return ((float)val).ToString();

                case "decimal":
                    if (!string.IsNullOrEmpty(format))
                    {
                        return ((decimal)val).ToString(format, Culture.NumberFormat);
                    }
                    return ((decimal)val).ToString();
                default:
                    return val.ToString();

            }
            //return string.Empty;
        }

        public string GetColor()
        {
            return _color;
        }

        public void SetColor(string color)
        {
            _color = color;
        }

        public XPathNavigator GetBlockContainerData(string name)
        {
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.NewLineHandling = NewLineHandling.None;
            settings.OmitXmlDeclaration = true;

            BlockContainerDataContract bcdc = Document.BlockContainerData.Where(b => b.Name == name).FirstOrDefault();

            if (bcdc == null) bcdc = new BlockContainerDataContract { Name = name, Blocks = new List<BlockDataDataContract>() };
            string xml = Core.CoreHelper.SerializeToString(bcdc, settings);
            xml = xml.Replace("xmlns:i=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns=\"http://schemas.datacontract.org/2004/07/EY.com.eBook.API.Contracts.Data\"", "");
            XElement block = XElement.Parse(xml, LoadOptions.None);
            return block.CreateNavigator();
        }

        public string ReplaceAll(string source, string[] oldValues, string[] newValues)
        {
            // error checking etc removed for brevity

            string pattern = string.Join("|", oldValues.Select(r=>Regex.Escape(r)).ToArray());

            return Regex.Replace(source, pattern, m =>
            {
                int index = Array.IndexOf(oldValues, m.Value);
                return newValues[index];
            });
        }

        public XPathNavigator ToXml(string text)
        {
            XElement t = XElement.Parse("<phrase>text missing or parsing failed.</phrase>");
            try
            {
                if (!(text.StartsWith("&lt") || text.StartsWith("<"))) text = string.Format("<paragraph>{0}</paragraph>", text);
                text = Regex.Replace(text, @"(\&lt\;){2,}", "[-", RegexOptions.IgnoreCase);
                text = Regex.Replace(text, @"(\&gt\;){2,}", "-]", RegexOptions.IgnoreCase);
                text = Regex.Replace(text, @"\&lt\;", "<", RegexOptions.IgnoreCase);
                text = Regex.Replace(text, @"\&gt\;", ">", RegexOptions.IgnoreCase);
                text = ReplaceAll(text, _htmls, _xmls);
                text = Regex.Replace(text, @"\<br\>", "<br/>", RegexOptions.IgnoreCase);
                t = XElement.Parse(string.Format("<data>{0}</data>", text));
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return t.CreateNavigator();
        }


        public string GetFieldValue(string name)
        {
            return GetFieldValue(name, null);
        }

        public string GetFieldValue(string name, string format, string list)
        {
            return GetFieldValue(name, format);
        }

        public string GetFieldValue(string name, string format)
        {
            DocumentFieldDataContract dfdc = Document.Fields.Where(f => f.Name == name).FirstOrDefault();
            if (dfdc == null) return string.Empty;

            return GetObjectValue(dfdc.Value, format);
        }

        public int GetFontSize(int defSize, int size)
        {
            return defSize + (size - 2);
        }
    }
}
