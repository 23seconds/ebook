﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.DirectoryServices;

namespace EY.com.eBook.API.BL
{
    public class ActiveDirectoryRepository
    {
        public ActiveDirectoryRepository() { }

        /// <summary>
        /// Find email in active directory for windows account (needed to get linked person from cdb)
        /// </summary>
        /// <param name="windowsAccount">The windows accountname</param>
        /// <returns>string email</returns>
        public string FindEmail(string windowsAccount)
        {
            try
            {
                string domain = "EURW";
                //if (windowsAccount.Contains("\\"))
                 //   windowsAccount = windowsAccount.Substring(windowsAccount.IndexOf("\\") + 1);
                if (windowsAccount.Contains("\\"))
                {
                    
                    string[] splittedUser = windowsAccount.Split(new char[] {@"\"[0]}, StringSplitOptions.RemoveEmptyEntries);
                    windowsAccount = splittedUser[1];
                    domain = splittedUser[0];
                }

                string ldapRootUri = string.Format("LDAP://DC={0},DC=ey,DC=net", domain);
                    //= ConfigurationManager.AppSettings["LDAP_ROOTURI"];
                string ldapUsername = ConfigurationManager.AppSettings["LDAP_USER"];
                string ldapPassword = ConfigurationManager.AppSettings["LDAP_PASSWORD"];

                if (ldapRootUri != string.Empty && ldapRootUri != null)
                {
                    DirectoryEntry de;
                    if (ldapUsername != string.Empty && ldapUsername != null && ldapPassword != string.Empty && ldapPassword != null)
                    {
                        de = new DirectoryEntry(ldapRootUri, ldapUsername, ldapPassword);
                    }
                    else
                    {
                        de = new DirectoryEntry(ldapRootUri);
                    }
                    DirectorySearcher deSearch = new DirectorySearcher();
                    deSearch.SearchRoot = de;

                    deSearch.Filter = "(&(objectClass=user)(SAMAccountName=" + windowsAccount + "))";
                    deSearch.SearchScope = SearchScope.Subtree;
                    SearchResult results = deSearch.FindOne();
                    if (results != null)
                    {
                        if (results.Properties["mail"] != null && results.Properties["mail"].Count > 0)
                        {
                            return results.Properties["mail"][0].ToString();
                        }
                    }
                    else
                    {
                        return null;
                    }

                }
                else
                {
                    return null;
                }
            }
            catch
            {
                return null;
            }
            return null;
        }
    }
}
