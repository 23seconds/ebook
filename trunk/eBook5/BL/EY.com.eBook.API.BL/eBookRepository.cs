﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EY.com.eBook.Core;
using EY.com.eBook.API.Contracts.Services;

namespace EY.com.eBook.API.BL
{
    public abstract class eBookRepository : RepositoryBase
    {
        static eBookRepository()
        {
            SetCacheHelper();
        }

         #region CacheHelper
        private static bool _cachingActive;
        private static ICacheService _cache;

        private static void SetCacheHelper()
        {
            using (var cacheHelper = new CacheHelper())
            {
                if (!cacheHelper.CacheServiceConfigured) return;
                cacheHelper.CreateCacheProxy();
                _cachingActive = cacheHelper.CacheRunning;
                if (_cachingActive)
                {
                    _cache = (ICacheService) EY.com.eBook.Utils.Services
                                                 .ProxyFactory.CreateInstance(
                                                     typeof (ICacheService).AssemblyQualifiedName,
                                                     "EY.com.eBook.CachingService.CacheService");
                }
            }
        }

        protected static bool TryFetchICacheService(out ICacheService proxy)
        {
            proxy = null;
            return _cachingActive && (proxy = _cache) != null;
        }

        #endregion
    
    }
    
}
