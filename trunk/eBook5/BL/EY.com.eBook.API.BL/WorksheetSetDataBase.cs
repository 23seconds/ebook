﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EY.com.eBook.Caching;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.Core.EF.Write;
using EY.com.eBook.Core.EF;
using System.Runtime.Serialization;
using EY.com.eBook.API.Contracts.Data.Cache;
using EY.com.eBook.RuleEngine.Core.Interfaces;
using EY.com.eBook.RuleEngine.Core;

namespace EY.com.eBook.RuleEngine.Data
{
    
    
    public class WorksheetSetBase
    {
        // add mappingitems assessmentyear
        // add coefficients assessmentyear
        // add listitem-coefficients index assessmentyear
        // add mappingitem-coefficients index assessmentyear
        public WorksheetSetBase()
        {
        }


    }

 
    public class WorksheetSetDataBase : SchemaQuery
    {
        internal CacheRepository _cacheRep = new CacheRepository();

        public virtual int AssessmentYear { get { return 0; } }

        public WorksheetSetDataBase PreviousSet { get; set; }

        public void SaveBookings()
        {
            SaveBookings(true);
        }

        public void SaveBookings(bool reloadAccounts)
        {

            if (BookingsToAdd.Count > 0 || BookingsToUpdate.Count > 0 || BookingsToDelete.Count > 0)
            {
                CacheRepository cp = new CacheRepository();
                cp.SaveWorksheetBookings(BookingsToAdd, BookingsToUpdate, BookingsToDelete, AutoBookings, DataContract.Id);
                BookingsToAdd = new List<Guid>();
                BookingsToUpdate = new List<Guid>();
                BookingsToDelete = new List<Guid>();
                if(reloadAccounts) Schema = cp.GetAccountSchemaWorkSet(DataContract.Id);
            }
        }

        public bool HasAlteredBookings()
        {
            return BookingsToAdd.Count > 0 || BookingsToDelete.Count > 0 || BookingsToUpdate.Count > 0;
        }

        public bool IsEmpty;

        public override AccountSchemaWorkSet Schema { get; set; }

        public FileDataContract DataContract { get; set; }

        public ClientBaseDataContract Client { get; set; }

        public FileSettingsDataContract Settings { get; set; }

        public List<BookingDataContract> AutoBookings { get; set; }

        public List<Guid> BookingsToDelete = new List<Guid>();

        public List<Guid> BookingsToUpdate = new List<Guid>();

        public List<Guid> BookingsToAdd = new List<Guid>();

        public List<FileMessageDataContract> Messages { get; set; }

        public List<FileMessageDataContract> MessagesToAdd = new List<FileMessageDataContract>();
        
        public Dictionary<Guid, string> GetWorksheetDataFromDB()
        {
            Dictionary<Guid, string> worksheets = new Dictionary<Guid, string>();

            foreach (Worksheet worksheet in eBookWriteManager.Context.Worksheets.Where(w => w.FileId == DataContract.Id))
            {
                worksheets.Add(worksheet.TypeId, worksheet.Data);
            }
            return worksheets;
            
        }

        public CoefficientsWorkingCopy Coeffs { get; set; }

        // add mappingitems assessmentyear
        // add coefficients assessmentyear
        // add listitem-coefficients index assessmentyear
        // add mappingitem-coefficients index assessmentyear

        // methods
        // getcoefficient(s)
        // getmapping


        public StringListItemDataContract GetListAccount(string nr)
        {
            nr = nr.PadRight(12, '0');
            AccountDataContract adc = GetAccount(nr, true, false, "nl-BE");

            if (adc == null) return new StringListItemDataContract
            {
                Id = nr
                ,
                NL = string.Format("{0} REKENING BESTAAT NIET!", nr.Substring(0, 6))
                ,
                EN = string.Format("{0} ACCOUNT DOES NOT EXIST!", nr.Substring(0, 6))
                ,
                FR = string.Format("{0} COMPTE N'EXISTE PAS!", nr.Substring(0, 6))
            };
            return GetListAccount(adc);
        }

        public StringListItemDataContract GetListAccount(AccountDataContract adc)
        {
            return adc.GetListItem();
        }

        public void DeleteBooking(Guid typeId, string collection, Guid? rowId, string key)
        {
            IQueryable<BookingDataContract> qryBookings = AutoBookings.AsQueryable().Where(b => b.WorksheetTypeId == typeId && b.WorksheetRowId == rowId);
            if (!string.IsNullOrEmpty(collection)) qryBookings = qryBookings.Where(b => b.WorksheetCollection == collection);
            if (!string.IsNullOrEmpty(key)) qryBookings = qryBookings.Where(b => b.WorksheetBookingKey == key);
            BookingsToDelete.AddRange(qryBookings.Select(b => b.Id).ToList());
            qryBookings.ToList().ForEach(bl => AutoBookings.Remove(bl));
        }

        public void UpdateBooking(Guid typeId, string collection, Guid? rowId, string key, List<BookingLineDataContract> lines)
        {
            BookingDataContract bdc = AutoBookings.FirstOrDefault(b => b.WorksheetTypeId == typeId && b.WorksheetCollection == collection
                && b.WorksheetRowId == rowId && b.WorksheetBookingKey == key);
            if (bdc == null)
            {
                AddBooking(typeId, collection, rowId, key, lines);
            }
            else
            {
                
                List<string> accounts = lines.Select(l=>l.AccountNr.Id).ToList();
                bdc.BookingLines.Clear();
                foreach (BookingLineDataContract line in lines)
                {
                    line.Id = Guid.NewGuid();
                    bdc.BookingLines.Add(line);
                    
                }
                BookingsToUpdate.Add(bdc.Id);

            }
        }

        public void AddBooking(Guid typeId, string collection, Guid? rowId, string key, List<BookingLineDataContract> lines)
        {
            BookingDataContract bdc = new BookingDataContract
            {
                Id = Guid.NewGuid()
                ,
                Deleted = false
                ,
                BookingLines = new List<BookingLineDataContract>()
                ,
                Description = "AUTO"
                ,
                ExportCount = 0
                ,
                FileId = DataContract.Id
                ,
                Hidden = false
                ,
                CreationDate = DateTime.Now
                ,
                WorksheetBookingKey = key
                ,
                WorksheetCollection = collection
                ,
                WorksheetRowId = rowId
                ,
                WorksheetTypeId = typeId
            };
            foreach (BookingLineDataContract line in lines)
            {
                line.Id = Guid.NewGuid();
                line.BookingId = bdc.Id;
                line.AccountDescription = null;
                line.BusinessRelationId = null;
                line.VisibleNr = null;
                if (line.AccountNr.Id.Contains(".")) line.AccountNr.Id = line.AccountNr.Id.Split(new char[] { '.' })[0];
                bdc.BookingLines.Add(line);
            }
            AutoBookings.Add(bdc);
            BookingsToAdd.Add(bdc.Id);

        }

        public string GetTranslation(Guid typeId, string collection, string field)
        {
            return GetTranslation(typeId, collection, field, null, "label");
        }

        public string GetTranslation(Guid typeId, string collection, string field, string type)
        {
            return GetTranslation(typeId, collection, field, null, type);
        }

        public string GetTranslation(Guid typeId, string collection, string field, string key, string type)
        {
            return GetTranslation(typeId, collection, field, key, type, DataContract.Culture);
         
        }

        public string GetTranslation(Guid typeId, string collection, string field, string key, string type,string culture)
        {
            CacheRepository cr = new CacheRepository();
            string result = cr.GetWorksheetTypeTranslation(typeId, collection, field, key, type, culture);
            return result;
        }



        public string GetTabTranslation(Guid typeId, string tab,string culture)
        {
            return GetTabTranslation(typeId, tab, "label",culture);
        }

        public string GetTabTranslation(Guid typeId, string tab, string type, string culture)
        {
            CacheRepository cr = new CacheRepository();
            string result = cr.GetWorksheetTypeTabTranslation(typeId, tab, type, culture);
            return result;
        }

        public ClientDataContract GetClient()
        {
            EY.com.eBook.API.BL.Read.ClientRepository cr = new EY.com.eBook.API.BL.Read.ClientRepository();
            return cr.GetClient(DataContract.ClientId);
            //return new ClientDataContract { };
        }

        public ClientBaseDataContract GetClientBase()
        {
            EY.com.eBook.API.BL.Read.ClientRepository cr = new EY.com.eBook.API.BL.Read.ClientRepository();
            return cr.GetClientBase(DataContract.ClientId);
            //return new ClientDataContract { };
        }

        public CoefficientDataContract GetCoefficient(string key, int assessmentyear)
        {
            List<Guid> ids = Coeffs.KeysIdx[key];
            if (ids.Count == 0) return null;

            DateTime end = new DateTime(assessmentyear,12,30);
            DateTime start = new DateTime(assessmentyear-1,12,31).AddMonths(-24).AddDays(1);

            return Coeffs.Items.IndexedList(ids).FirstOrDefault(c => (c.AssessmentYear.HasValue && c.AssessmentYear == assessmentyear)
                                                    || ((c.StartDate.HasValue && c.EndDate.HasValue)
                                                          && (c.StartDate.Value < end && end <= c.EndDate.Value
                                                                        && start < c.EndDate.Value)
                                                        )
                                                );
        }

        public CoefficientDataContract GetCoefficientFullPeriod(string key, DateTime start, DateTime end)
        {
            return GetCoefficientFullPeriod(null, key, start, end);
        }

        public CoefficientDataContract GetCoefficientFullPeriod(string group, string key, DateTime start, DateTime end)
        {

            List<Guid> ids = Coeffs.Items.Keys.ToList();

            if (!string.IsNullOrEmpty(group))
            {
                ids = ids.Intersect(Coeffs.GroupsIdx[group]).ToList();
            }
            if (!string.IsNullOrEmpty(key))
            {
                ids = ids.Intersect(Coeffs.KeysIdx[key]).ToList(); ;
            }

            if (ids.Count == 0) return null;

            return Coeffs.Items.IndexedList(ids).FirstOrDefault(c => c.StartDate.HasValue && c.EndDate.HasValue
                                                                && start >= c.StartDate && end <= c.EndDate);
                                                               // || (c.StartDate.Value <= start)
                                                               // || (c.EndDate.Value >= end));

        }

        public CoefficientDataContract GetCoefficientFromListFullPeriod(List<CoefficientDataContract> listCoeffs, string key, DateTime start, DateTime end)
        {
            return listCoeffs.FirstOrDefault(c => c.Key == key &&  c.StartDate.HasValue && c.EndDate.HasValue
                                                                && start >= c.StartDate && end <= c.EndDate);
        }

        public CoefficientDataContract GetCoefficient(string key, DateTime start, DateTime end)
        {
            return GetCoefficient(null, key, start, end);
        }

        public CoefficientDataContract GetCoefficient(string group, string key, DateTime start, DateTime end)
        {
           
            List<Guid> ids = Coeffs.Items.Keys.ToList();

            if (!string.IsNullOrEmpty(group))
            {
                ids = ids.Intersect(Coeffs.GroupsIdx[group]).ToList();
            }
            if (!string.IsNullOrEmpty(key))
            {
                ids = ids.Intersect(Coeffs.KeysIdx[key]).ToList(); ;
            }

            if (ids.Count == 0) return null;

            return Coeffs.Items.IndexedList(ids).FirstOrDefault(c => !c.StartDate.HasValue && !c.EndDate.HasValue
                                                                ||  (c.StartDate.Value <= start)
                                                                || (c.EndDate.Value >= end));

        }

        public GuidListItemDataContract GetListItem(Guid id)
        {
            EY.com.eBook.API.BL.Read.ListRepository lr = new EY.com.eBook.API.BL.Read.ListRepository();
            return lr.GetListItem(id);

           // return new GuidListItemDataContract { };
        }

        public MappingItemDataContract GetMappingItem(Guid id)
        {
            EY.com.eBook.API.BL.Read.MappingRepository mr = new EY.com.eBook.API.BL.Read.MappingRepository();
            return mr.GetMappingItem(id);
            
        }

        public CoefficientDataContract GetMappingCoefficient(Guid mappingItemId, DateTime start, DateTime end)
        {
            List<Guid> ids = Coeffs.ListItemsIdx[mappingItemId];
            if (ids == null) return null;
            if (ids.Count == 0) return null;
            return Coeffs.Items[ids.First()];
        }

        public List<CoefficientDataContract> GetMappingCoefficients(Guid mappingItemId, DateTime start, DateTime end)
        {
            List<Guid> ids = Coeffs.ListItemsIdx[mappingItemId];
            if (ids == null) return null;
            if (ids.Count == 0) return null;
            return Coeffs.Items.IndexedList(ids);
        }

        public List<CoefficientDataContract> GetListItemCoefficients(Guid listItemId, string key, DateTime start, DateTime end)
        {
            return GetListItemCoefficients(listItemId, null, key, start, end);
        }

        public List<CoefficientDataContract> GetListItemCoefficients(Guid listItemId, string group, string key, DateTime start, DateTime end)
        {
            List<Guid> ids = Coeffs.ListItemsIdx[listItemId];
            if (!string.IsNullOrEmpty(group)) ids = ids.Intersect(Coeffs.GroupsIdx[group]).ToList();
            if (!string.IsNullOrEmpty(key)) ids = ids.Intersect(Coeffs.KeysIdx[key]).ToList();

            if (ids.Count == 0) return new List<CoefficientDataContract>();
            return Coeffs.Items.IndexedList(ids).Where(c => !c.StartDate.HasValue && !c.EndDate.HasValue
                                                                || (c.StartDate.Value < end 
                                                                        && start < c.EndDate.Value)
                                                               )
                                                 .OrderBy(c => c.StartDate).ToList();
        }

        public CoefficientDataContract GetListItemCoefficientFullPeriod(Guid listItemId, string group, string key, DateTime start, DateTime end)
        {
            List<Guid> ids = Coeffs.ListItemsIdx[listItemId];
            if (!string.IsNullOrEmpty(group)) ids = ids.Intersect(Coeffs.GroupsIdx[group]).ToList();
            if (!string.IsNullOrEmpty(key)) ids = ids.Intersect(Coeffs.KeysIdx[key]).ToList();

            if (ids.Count == 0) return null;
            return Coeffs.Items.IndexedList(ids).FirstOrDefault(c => c.StartDate.HasValue && c.EndDate.HasValue
                                                                && start >= c.StartDate && end <= c.EndDate);
        }

        
        public List<CoefficientDataContract> GetCoefficients(string key, DateTime start, DateTime end)
        {
            return GetCoefficients(null, key, start, end);
        }

        public List<CoefficientDataContract> GetCoefficients(string group, string key, DateTime start, DateTime end)
        {
            List<Guid> ids = Coeffs.Items.Keys.ToList();
            if (!string.IsNullOrEmpty(group)) ids = ids.Intersect(Coeffs.GroupsIdx[group]).ToList();
            if (!string.IsNullOrEmpty(key)) ids = ids.Intersect(Coeffs.KeysIdx[key]).ToList();

            if (ids.Count == 0) return new List<CoefficientDataContract>();  //(end <= c.EndDate.Value && end.Year==c.EndDate.Value.Year)
            return Coeffs.Items.IndexedList(ids).Where(c => !c.StartDate.HasValue && !c.EndDate.HasValue
                                                                || (c.StartDate.Value < end
                                                                    && (  (end <= c.EndDate.Value && end.Year<=c.EndDate.Value.Year)
                                                                        || ( end.Year> c.EndDate.Value.Year)
                                                                        )
                                                                    && start < c.EndDate.Value))
                                                 .Distinct()
                                                 .OrderBy(c => c.StartDate).ToList();
        }

        public BusinessRelationDataContract GetBusinessRelation(string importedId)
        {
            // to implement
            BusinessRelation br = eBookWriteManager.Context.BusinessRelations.FirstOrDefault(b =>b.ClientId==DataContract.ClientId &&  b.ImportedId == importedId);
            if (br == null) return new BusinessRelationDataContract { Id = Guid.Empty, FirstName = "NOT FOUND" };
            return new BusinessRelationDataContract
            {
                Address = br.Address
                ,
                BusinessRelationType = br.Type[0]
                ,
                City = br.City
                ,
                ClientId = br.ClientId
                ,
                CompanyNr = br.CompanyNr
                ,
                Country = br.Country
                ,
                FirstName = br.FirstName
                ,
                Id = br.Id
                ,
                ImportedId = br.ImportedId
                ,
                LastName = br.Name
                ,
                Vat = br.VatNumber
                ,
                ZipCode = br.Zip
            };
        }

        public BusinessRelationDataContract GetBusinessRelation(Guid id)
        {
            // to implement
            BusinessRelation br = eBookWriteManager.Context.BusinessRelations.FirstOrDefault(b => b.Id == id);
            if (br == null) return new BusinessRelationDataContract { Id = id, FirstName = "NOT FOUND" };
            return new BusinessRelationDataContract
            {
                Address = br.Address
                ,
                BusinessRelationType = br.Type[0]
                ,
                City = br.City
                ,
                ClientId = br.ClientId
                ,
                CompanyNr = br.CompanyNr
                ,
                Country = br.Country
                ,
                FirstName = br.FirstName
                ,
                Id = br.Id
                ,
                ImportedId = br.ImportedId
                ,
                LastName = br.Name
                ,
                Vat = br.VatNumber
                ,
                ZipCode = br.Zip
            };
        }

        public void SetMessage(Guid worksheetTypeId, MessageTypeDataContract messageType, string messageKey)
        {
            SetMessage(worksheetTypeId, messageType, messageKey, string.Empty);
        }

        public void SetMessage(Guid worksheetTypeId, MessageTypeDataContract messageType, string messageKey, string worksheetTab)
        {
            SetMessage(worksheetTypeId, messageType, messageKey, worksheetTab, null);
        }

        public void SetMessage(Guid worksheetTypeId, MessageTypeDataContract messageType, string messageKey, string worksheetTab, Guid? entityId)
        {
            SetMessage(worksheetTypeId, messageType, messageKey, worksheetTab, entityId, string.Empty);
        }

        public void SetMessage(Guid worksheetTypeId, MessageTypeDataContract messageType, string messageKey, string worksheetTab, Guid? entityId, string fieldName)
        {
            SetMessage(worksheetTypeId, messageType, messageKey, worksheetTab, entityId, fieldName, new object[] { });
        }

        public void SetMessage(Guid worksheetTypeId, MessageTypeDataContract messageType, string messageKey, string worksheetTab, Guid? entityId, string fieldName, object[] values)
        {
            // get message translations
            // format each with supplied variables (if send)
            //
            

            List<FileMessageDataContract> messages = new List<FileMessageDataContract>();

            WorksheetTypeMessageTranslationDataContract wtmtdc = _cacheRep.GetWorksheetTypeMessageTranslations(worksheetTypeId, messageKey, worksheetTab, fieldName);
            if (wtmtdc != null)
            {
               
                foreach (MessageTranslationDataContract mtdc in wtmtdc.Translations)
                {
                    var field = GetTranslation(worksheetTypeId, worksheetTab, fieldName, null, "label", mtdc.Culture);
                    var tab = GetTabTranslation(worksheetTypeId, worksheetTab,mtdc.Culture);
                    
                    FileMessageDataContract fmdc = new FileMessageDataContract
                    {
                        FileId = DataContract.Id
                        ,
                        MessageId = mtdc.Id
                        ,
                        ConnectionAccount = ""
                        ,
                        ConnectionGuid = worksheetTypeId
                        ,
                        ConnectionType = FileMessageConnectionTypeDataContract.WORKSHEET
                        ,
                        Culture = mtdc.Culture
                        ,
                        Text = mtdc.Message
                        ,
                        Type = messageType
                        ,
                        WorksheetTypeCollection = worksheetTab
                        ,
                        WorksheetTypeCollectionText = tab
                        ,
                        WorksheetTypeField = fieldName
                        ,
                        WorksheetTypeRowId = entityId.HasValue ? entityId.Value : Guid.Empty
                        ,
                        WorksheetTypeFieldText = field
                    };
                    
                    //GetTranslation(worksheetTypeId,worksheetTab,fieldName)
                      //  GetTranslation(worksheetTypeId,"Voorzieningen",worksheetTab)
                    try
                    {
                        if (values != null && values.Length > 0)
                        {
                            fmdc.Text = string.Format(fmdc.Text, values);
                        }
                    }
                    catch { }
                    messages.Add(fmdc);
                }
                Messages.AddRange(messages);
                MessagesToAdd.AddRange(messages);
                
            }
            else
            {
                MissingWorksheettypeTranslation wtmt = new MissingWorksheettypeTranslation
                {
                    LogId = Guid.NewGuid()
                    ,
                    Date = DateTime.Now
                    ,
                    Collection = worksheetTab
                    ,
                    FieldName = fieldName
                    ,
                    Key = messageKey
                    ,
                    TypeId = worksheetTypeId
                };
                eBookWriteManager.Context.AddToMissingWorksheettypeTranslations(wtmt);
                eBookWriteManager.Context.SaveChanges();
                    
            }

        }

        public void ClearMessages(Guid worksheetTypeId)
        {
            Messages.RemoveAll(m => m.ConnectionGuid == worksheetTypeId && m.FileId == DataContract.Id);
            // Direct DB update
            eBookWriteManager.Context.ExecuteSql(string.Format("DELETE FROM TPBOOK_FILE_MESSAGES WHERE FILE_MESSAGE_CONNECTION_GUID='{0}' and FILE_MESSAGE_FILE_ID='{1}'", worksheetTypeId.ToString(), DataContract.Id));
        }

        public void ClearMessagesCollection(Guid worksheetTypeId,string collection)
        {
            Messages.RemoveAll(m => m.ConnectionGuid == worksheetTypeId && m.WorksheetTypeCollection == collection && m.FileId == DataContract.Id);
            // Direct DB update
            eBookWriteManager.Context.ExecuteSql(string.Format("DELETE FROM TPBOOK_FILE_MESSAGES WHERE FILE_MESSAGE_CONNECTION_GUID='{0}' and FILE_MESSAGE_WT_COLLECTION = '{1}' and FILE_MESSAGE_FILE_ID='{2}'", worksheetTypeId.ToString(), collection, DataContract.Id));
        }

        public void ClearMessages(Guid worksheetTypeId, string messageKey)
        {
            Guid? msgId = _cacheRep.GetWorksheetTypeMessageId(worksheetTypeId, messageKey, null, null);
            if (msgId.HasValue)
            {
                Messages.RemoveAll(m => m.ConnectionGuid == worksheetTypeId && m.MessageId == msgId.Value && m.FileId == DataContract.Id);
                // Direct DB update
                eBookWriteManager.Context.ExecuteSql(string.Format("DELETE FROM TPBOOK_FILE_MESSAGES WHERE FILE_MESSAGE_CONNECTION_GUID='{0}' and FILE_MESSAGE_ID='{1}' and FILE_MESSAGE_FILE_ID='{2}'", worksheetTypeId.ToString(), msgId.Value.ToString(), DataContract.Id));
            }
        }

        public void ClearMessages(Guid worksheetTypeId, Guid entityId)
        {
            Messages.RemoveAll(m => m.ConnectionGuid == worksheetTypeId && m.WorksheetTypeRowId.HasValue && m.WorksheetTypeRowId.Value == entityId && m.FileId == DataContract.Id);
            eBookWriteManager.Context.ExecuteSql(string.Format("DELETE FROM TPBOOK_FILE_MESSAGES WHERE FILE_MESSAGE_CONNECTION_GUID='{0}' and FILE_MESSAGE_WT_ROWID='{1}' and FILE_MESSAGE_FILE_ID='{2}'", worksheetTypeId.ToString(), entityId.ToString(), DataContract.Id));
        }

        //public void ClearMessages(Guid worksheetTypeId, string messageKey, string worksheetTab, Guid? entityId, string fieldName)
        //{
        //    Guid? msgId = _cacheRep.GetWorksheetTypeMessageId(worksheetTypeId, messageKey, worksheetTab, fieldName);
        //    if (msgId.HasValue)
        //    {
        //        Messages.RemoveAll(m => m.ConnectionGuid == worksheetTypeId && m.MessageId == msgId.Value);
        //    }
        //}

        //public void ClearMessagesNoKey(Guid worksheetTypeId, string worksheetTab)
        //{
        //    Messages.RemoveAll(m => m.ConnectionGuid == worksheetTypeId && m.w);
        //}

        //public void ClearMessagesNoKey(Guid worksheetTypeId, string messageKey, string worksheetTab, Guid? entityId)
        //{
        //    Guid? msgId = _cacheRep.GetWorksheetTypeMessageId(worksheetTypeId, messageKey, worksheetTab, null);
        //    if (msgId.HasValue)
        //    {
        //        Messages.RemoveAll(m => m.ConnectionGuid == worksheetTypeId && m.MessageId == msgId.Value);
        //    }
        //}

        //public void ClearMessagesNoKey(Guid worksheetTypeId, string messageKey, string worksheetTab, Guid? entityId, string fieldName)
        //{
        //    Guid? msgId = _cacheRep.GetWorksheetTypeMessageId(worksheetTypeId, messageKey, worksheetTab, fieldName);
        //    if (msgId.HasValue)
        //    {
        //        Messages.RemoveAll(m => m.ConnectionGuid == worksheetTypeId && m.MessageId == msgId.Value);
        //    }
        //}

        
        
    }
}
