﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.API.Contracts.Data.Cache;
using EY.com.eBook.API.Contracts.Data.IText;
using System.Xml.Linq;
using System.Configuration;
using EY.com.eBook.Core;
using EY.com.eBook.Caching;
using EY.com.eBook.API.BL.Translators;
using EY.com.eBook.Core.IText.Render;
using EY.com.eBook.API.BL.Read;
using EY.com.eBook.Core.EF.Read;
using EY.com.eBook.Core.EF;
using EY.com.eBook.API.Contracts.Data.Meta;
using System.ServiceModel;
using EY.com.eBook.Core.Data;
using System.Xml.Xsl;
using System.IO;
using System.Xml;
using System.Xml.XPath;

namespace EY.com.eBook.API.BL
{
	public class ITextRepository : RepositoryBase
	{

		 public FooterConfigDataContract fcdc = new FooterConfigDataContract
			{
				Enabled = true
				,
				ShowFooterNote = true
				,
				ShowPageNr = true
			};

		  public  HeaderConfigDataContract hcdc = new HeaderConfigDataContract
			{
				Enabled = true
				,
				ShowTitle = true
			};

		  public  FooterConfigDataContract nofooter = new FooterConfigDataContract
			{
				Enabled = false
				,
				ShowFooterNote = false
				,
				ShowPageNr = false
			};

		  public  HeaderConfigDataContract noheader = new HeaderConfigDataContract
			{
				Enabled = false
				,
				ShowTitle = false
			};

		public List<LibraryTreeNodeDataContract> GetItems(string parentNode, string culture, Guid fileId)
		{
		   
			string title = "";

			List<LibraryTreeNodeDataContract> list = new List<LibraryTreeNodeDataContract>();
			IndexItemBaseDataContract indexItem = null;
			if (parentNode.IsGuid())
			{
			}
			else
			{
				List<Core.EF.Read.InterfaceTranslation> trans = EY.com.eBook.Core.EF.eBookReadManager.Context.InterfaceTranslations.Where(i => i.Key == "BundleLibrary.Bundle" && i.Culture == culture).ToList();
				switch (parentNode.ToLower())
				{
					case "rootbiztaxbundle":
					case "rootmanualbiztaxbundle":
					case "rootbiztax":
					case "root":
						if (parentNode.ToLower() != "rootbiztax")
						{
							title = trans.Count(i => i.Interface == "GENERAL") > 0 ? trans.First(i => i.Interface == "GENERAL").Translation : "General items";
							list.Add(new LibraryTreeNodeDataContract
							{
								Id = "BUNDLE",
								Text = title
									,
								AllowDrag = false
									,
								AllowChildren = false
									,
								Class = ""
									,
								IconCls = "eb-library-general"
									,
								IndexItem = null
									,
								Leaf = false
							});
						}

						if (parentNode.ToLower() != "rootmanualbiztaxbundle")
						{
							title = trans.Count(i => i.Interface == "STATEMENTS") > 0 ? trans.First(i => i.Interface == "STATEMENTS").Translation : "Statements";
							list.Add(new LibraryTreeNodeDataContract
							{
								Id = "STATEMENTS",
								Text = title
									,
								AllowDrag = false
									,
								AllowChildren = false
									,
								Class = ""
									,
								IconCls = "eb-library-statements"
									,
								IndexItem = null
									,
								Leaf = false
							});

							title = trans.Count(i => i.Interface == "DOCUMENTS") > 0 ? trans.First(i => i.Interface == "DOCUMENTS").Translation : "Documents";
							list.Add(new LibraryTreeNodeDataContract
							{
								Id = "DOCUMENTS",
								Text = title
									,
								AllowDrag = false
									,
								AllowChildren = false
									,
								Class = ""
									,
								IconCls = "eb-library-documents"
									,
								IndexItem = null
									,
								Leaf = false
							});

							title = trans.Count(i => i.Interface == "WORKSHEETS") > 0 ? trans.First(i => i.Interface == "WORKSHEETS").Translation : "Worksheets";
							list.Add(new LibraryTreeNodeDataContract
							{
								Id = "WORKSHEETS",
								Text = title
									,
								AllowDrag = false
									,
								AllowChildren = false
									,
								Class = ""
									,
								IconCls = "eb-library-worksheets"
									,
								IndexItem = null
									,
								Leaf = false
							});
						}

						bool bni = eBookReadManager.Context.Files.Include("Client").First(f => f.Id == fileId).Client.BNI;

						//title = trans.Count(i => i.Interface == "BIZTAX") > 0 ? trans.First(i => i.Interface == "WORKSHEETS").Translation : "Worksheets";
						list.Add(new LibraryTreeNodeDataContract
						{
							Id = bni ? "biztax-bni" : "biztax",
							Text = "BizTax"
								,
							AllowDrag = false
								,
							AllowChildren = false
								,
							Class = ""
								,
							IconCls = "eb-library-biztax"
								,
							IndexItem = null
								,
							Leaf = false
						});

						title = trans.Count(i => i.Interface == "REPOSITORY") > 0 ? trans.First(i => i.Interface == "REPOSITORY").Translation : "Repository";
						list.Add(new LibraryTreeNodeDataContract
						{
							Id = "REPOSITORY",
							Text = title
								,
							AllowDrag = false
								,
							AllowChildren = false
								,
							Class = ""
								,
							IconCls = "eb-library-repository"
								,
							IndexItem = null
								,
							Leaf = false
						});
						break;
					case "biztax-bni":
						title = culture == "nl-BE" ? "Automatische Biztax bundel" : culture == "fr-FR" ? "Liasse de BizTax - automatiquement" : "Automatic Biztax bundle";
						list.Add(new LibraryTreeNodeDataContract
						{
							Id = "auto-biztax"
							,
							Text = title
							,
							AllowDrag = true
							,
							AllowChildren = false
							,
							Class = ""
							,
							IconCls = "eBook-bundle-tree-biztax"
							,
							IndexItem = new AutoBizTaxDataContract
								{
									FooterConfig = nofooter
									,
									HeaderConfig = noheader
									,
									Id = "BIZTAXBNI".DeterministicGuid()
									,
									ShowInIndex = true
									,
									Title = title
									,
									IconCls = "eBook-bundle-tree-biztax"
			
								}
							,
							Leaf = true
						});
						title = culture == "nl-BE" ? "Biztax Belastingberekening" : culture == "fr-FR" ? "BizTax - Calculation Tax" : "Biztax Tax Calculation";
						
						list.Add(new LibraryTreeNodeDataContract
						{
							Id = "biztax-calc"
							,
							Text = title
							,
							AllowDrag = true
							,
							AllowChildren = false
							,
							Class = ""
							,
							IconCls = "eBook-bundle-tree-biztax"
							,
							IndexItem = new BizTaxTaxCalcDataContract
							{
								FooterConfig = nofooter
									,
								HeaderConfig = noheader
									,
								Id = "BIZTAXCALC".DeterministicGuid()
									,
								ShowInIndex = true
									,
								Title = title
									,
								IconCls = "eBook-bundle-tree-biztax"

							}
						});

						break;
					case "biztax":
						title = culture == "nl-BE" ? "Automatische Biztax bundel" : culture == "fr-FR" ? "Liasse de BizTax - automatiquement" : "Automatic Biztax bundle";
						list.Add(new LibraryTreeNodeDataContract
						{
							Id = "auto-biztax"
							,
							Text = title
							,
							AllowDrag = true
							,
							AllowChildren = false
							,
							Class = ""
							,
							IconCls = "eBook-bundle-tree-biztax"
							,
							IndexItem = new AutoBizTaxDataContract
								{
									FooterConfig = nofooter
									,
									HeaderConfig = noheader
									,
									Id = "BIZTAX".DeterministicGuid()
									,
									ShowInIndex = true
									,
									Title = title
									,
									IconCls = "eBook-bundle-tree-biztax"
								   
								}
							,
							Leaf = true
						});
						title = culture == "nl-BE" ? "Biztax Belastingberekening" : culture == "fr-FR" ? "BizTax - Calculation Tax" : "Biztax Tax Calculation";
						
						list.Add(new LibraryTreeNodeDataContract
						{
							Id = "biztax-calc"
							,
							Text = title
							,
							AllowDrag = true
							,
							AllowChildren = false
							,
							Class = ""
							,
							IconCls = "eBook-bundle-tree-biztax"
							,
							IndexItem = new BizTaxTaxCalcDataContract
							{
								FooterConfig = nofooter
									,
								HeaderConfig = noheader
									,
								Id = "BIZTAXCALC".DeterministicGuid()
									,
								ShowInIndex = true
									,
								Title = title
									,
								IconCls = "eBook-bundle-tree-biztax"

							}
						});
					   
						break;
					case "bundle":
						title = trans.Count(i => i.Interface == "COVER") > 0 ? trans.First(i => i.Interface == "COVER").Translation : "COVERPAGE";
						indexItem = new CoverpageDataContract
						{
							FooterConfig = nofooter
							,
							HeaderConfig = noheader
							,
							Id = "BUNDLE_COVERPAGE".DeterministicGuid()
							,
							ShowInIndex = true
							,
							Title = title
							,
							IconCls = "eBook-bundle-tree-cover"
						};
						list.Add(new LibraryTreeNodeDataContract
							{
								Id = "Coverpage"
								,
								Text = title
								,
								AllowDrag = true
								,
								AllowChildren = false
								,
								Class = ""
								,
								IconCls = "eBook-bundle-tree-cover"
								,
								IndexItem = indexItem
								,
								Leaf = true
							});

						title = trans.Count(i => i.Interface == "INDEX") > 0 ? trans.First(i => i.Interface == "INDEX").Translation : "INDEXPAGE";
						indexItem = new IndexPageDataContract
						{
							FooterConfig = nofooter
							,
							HeaderConfig = noheader
							,
							Id = "BUNDLE_INDEXPAGE".DeterministicGuid()
							,
							ShowInIndex = true
							,
							Title = title
							,
							IconCls = "eBook-bundle-tree-index"
						};
						list.Add(new LibraryTreeNodeDataContract
						{
							Id = "Indexpage",
							Text = title
							,
							AllowDrag = true
							,
							AllowChildren = false
							,
							Class = ""
							,
							IconCls = "eBook-bundle-tree-index"
							,
							IndexItem = indexItem
							,
							Leaf = true
						});

						break;
					case "statements":
						title = trans.Count(i => i.Interface == "ACTIVA") > 0 ? trans.First(i => i.Interface == "ACTIVA").Translation : "ACTIVA";
						indexItem = new StatementsDataContract
						{
							Detailed = true
							,
							Culture = culture
							,
							FileId = fileId
							,
							FooterConfig = fcdc
							,
							HeaderConfig = hcdc
							,
							Layout = "ACTIVA"
							,
							ShowInIndex = true
							,
							Id = Guid.NewGuid()
							,
							Title = title
							,
							IconCls = "eBook-bundle-tree-statement"
						};
						list.Add(new LibraryTreeNodeDataContract
						{
							Id = "activa"
							,
							Text = title
							,
							AllowDrag = true
							,
							AllowChildren = false
							,
							Class = ""
							,
							IconCls = "eBook-bundle-tree-statement"
							,
							IndexItem = indexItem
							,
							Leaf = true
						});
						title = trans.Count(i => i.Interface == "PASSIVA") > 0 ? trans.First(i => i.Interface == "PASSIVA").Translation : "PASSIVA";
						indexItem = new StatementsDataContract
						{
							Detailed = true
							,
							Culture = culture
							,
							FileId = fileId
							,
							FooterConfig = fcdc
							,
							HeaderConfig = hcdc
							,
							Layout = "PASSIVA"
							,
							ShowInIndex = true
							,
							Id = Guid.NewGuid()
							,
							Title = title
							,
							IconCls = "eBook-bundle-tree-statement"
						};
						list.Add(new LibraryTreeNodeDataContract
						{
							Id = "passiva"
							,
							Text = title
							,
							AllowDrag = true
							,
							AllowChildren = false
							,
							Class = ""
							,
							IconCls = "eBook-bundle-tree-statement"
							,
							IndexItem = indexItem
							,
							Leaf = true
						});
						title = trans.Count(i => i.Interface == "RESULTATENREKENING") > 0 ? trans.First(i => i.Interface == "RESULTATENREKENING").Translation : "RESULTATENREKENING";
						indexItem = new StatementsDataContract
						{
							Detailed = true
							,
							Culture = culture
							,
							FileId = fileId
							,
							FooterConfig = fcdc
							,
							HeaderConfig = hcdc
							,
							Layout = "RESULTATENREKENING"
							,
							ShowInIndex = true
							,
							Id = Guid.NewGuid()
							,
							Title = title
							,
							IconCls = "eBook-bundle-tree-statement"
						};
						list.Add(new LibraryTreeNodeDataContract
						{
							Id = "resultatenrekening"
							,
							Text = title
							,
							AllowDrag = true
							,
							AllowChildren = false
							,
							Class = ""
							,
							IconCls = "eBook-bundle-tree-statement"
							,
							IndexItem = indexItem
							,
							Leaf = true
						});
						title = trans.Count(i => i.Interface == "RESULTAATVERWERKING") > 0 ? trans.First(i => i.Interface == "RESULTAATVERWERKING").Translation : "RESULTAATVERWERKING";
						indexItem = new StatementsDataContract
						{
							Detailed = true
							,
							Culture = culture
							,
							FileId = fileId
							,
							FooterConfig = fcdc
							,
							HeaderConfig = hcdc
							,
							Layout = "RESULTAATVERWERKING"
							,
							ShowInIndex = true
							,
							Id = Guid.NewGuid()
							,
							Title = title
							,
							IconCls = "eBook-bundle-tree-statement"
						};
						list.Add(new LibraryTreeNodeDataContract
						{
							Id = "resultatenverwerking"
							,
							Text = title
							,
							AllowDrag = true
							,
							AllowChildren = false
							,
							Class = ""
							,
							IconCls = "eBook-bundle-tree-statement"
							,
							IndexItem = indexItem
							,
							Leaf = true
						});
						break;
					case "worksheets":

						//list = Translator.
						list.AddRange(GetWorksheetTypesForLib(culture, fileId));
						break;
					case "documents":
						list.AddRange(GetDocumentsForLib(culture, fileId));
						break;
					case "repository":
						// client side resolved
						break;
				}
			}
			return list;
		}

		private List<LibraryTreeNodeDataContract> GetDocumentsForLib(string culture, Guid fileId)
		{
			List<EY.com.eBook.Core.EF.Read.Document> documents = EY.com.eBook.Core.EF.eBookReadManager.Context.Documents
													.Include("File").Include("DocumentType")
													.Where(d => d.File.Id == fileId)
													.OrderBy(d => d.Name)
													.ToList();
			var its = from doc in documents
					  select new LibraryTreeNodeDataContract
					  {
						  IndexItem = new DocumentItemDataContract
						  {
							  DocumentId = doc.Id
							  ,
							  Culture = doc.Culture
							  ,
							  DocumentTypeId = doc.DocumentType.Id
							  ,
							  FileId = doc.File.Id
							  ,
							  ShowInIndex = true
							  ,
							  Title = doc.Name
							  ,
							  HeaderConfig = new HeaderConfigDataContract { Enabled = false, ShowTitle = false }
							  ,
							  FooterConfig = new FooterConfigDataContract { Enabled = false, ShowPageNr = false, ShowFooterNote = false }
							  ,
							  IconCls = "eBook-bundle-tree-document"
						  }
						  ,
						  Id = doc.Id.ToString()
						  ,
						  AllowChildren = false
						  ,
						  AllowDrag = true
						  ,
						  Leaf = true
						  ,
						  Text = doc.Name
						  ,
						  IconCls = "eBook-bundle-tree-document"
					  };
			return its.ToList();
		}

		private List<LibraryTreeNodeDataContract> GetWorksheetTypesForLib(string culture, Guid fileId)
		{
			 List<EY.com.eBook.Core.EF.Read.WorksheetType> worksheettypes = EY.com.eBook.Core.EF.eBookReadManager.Context.WorksheetTypes
													.Include("Names")
													.Where(w =>  !string.IsNullOrEmpty(w.RuleApp)
																&& w.IsActive
																&& (w.Type=="FI" || w.Type=="AL"))
													.OrderBy(w=>w.Type).ThenBy(w => w.Order).ToList();
			 var its = from w in worksheettypes
					   select new LibraryTreeNodeDataContract
					   {
						   IndexItem = new WorksheetItemDataContract
				 {
					 Culture = culture
					 ,
					 FileId = fileId
					 ,
					 FooterConfig = new FooterConfigDataContract
						 {
							 Enabled = false
							 ,
							 ShowFooterNote = false
							 ,
							 ShowPageNr = false
						 }
					 ,
					 HeaderConfig = new HeaderConfigDataContract
						 {
							 Enabled = true
							 ,
							 ShowTitle = true
						 }
					 ,
					 PrintLayout = "default"
					 ,
					 RuleApp = w.RuleApp
					 ,
					 Id = Guid.NewGuid()
					 ,
					 ShowInIndex = true
					 ,
					 Title = w.Names.Where(n => n.Culture == culture).Count() > 0 ? w.Names.First(n => n.Culture == culture).Description : w.RuleApp
					 ,
					 WorksheetType = w.Id
					 ,
					 IconCls = "eBook-bundle-tree-worksheet"
				 }
						   ,
						   Id = w.Id.ToString()
						   ,
						   AllowChildren = false
						   ,
						   AllowDrag = true
						   ,
						   Leaf = true
						   ,
						   Text = w.Names.Where(n => n.Culture == culture).Count() > 0 ? w.Names.First(n => n.Culture == culture).Description : w.RuleApp
						   ,
						   IconCls = "eBook-bundle-tree-worksheet"
					   };
			 return its.ToList();
		}

		private T GetDefaultGenerationContract<T>(ClientDataContract client, FileDataContract file) where T: ITextGenerationDataContract, new()
		{
			return new T
			{
				Client = client
				,
				File = file
				,
				FontLocation = new FontConfigDataContract { Path = Config.StandardFont, Alias = "EYInterstate-Light" }
				,
				GenerationId = Guid.NewGuid()
				,
				Item = null
				,
				Settings = new GenerationSettings
				{
					GenerationPath = Config.PdfWorkingFolder
					,
					ResultPath = Config.PickupLocal
				}
			};
		}

		//public string GenerateLeadsheets(CriteriaIdAndCultureDataContract cidc)
		public PDFDataContract GenerateLeadsheets(CriteriaIdAndCultureDataContract cidc)
		{
			FileDataContract fdc = new BL.Read.FileReadRepository().GetFile(cidc);
			ClientDataContract cdc = new BL.Read.ClientRepository().GetClient(fdc.ClientId);
			List<AccountDataContract> adcs = new BL.Read.AccountSchemaRepository().GetAllAccounts(cidc.Id, cidc.Culture, false, false);
			List<RepositoryListItemDataContract> fls = new Repository().GetFileLinks(cidc,true);
			List<LeadSheetLineDataContract> lines = new List<LeadSheetLineDataContract>();
			if (adcs == null || adcs.Count(a=>a.Saldo != 0 || a.PreviousSaldo != 0) == 0)
			{
				lines = new List<LeadSheetLineDataContract>();
			}
			else
			{
				lines = (from a in adcs
							 where a.Saldo != 0 || a.PreviousSaldo != 0
							 select new LeadSheetLineDataContract
							 {
								 InternalNr = a.InternalNr
								 ,
								 Description = a.DefaultDescription
								 ,
								 VisualNr = a.VisualNr
								 ,
								 StartBalance = a.StartSaldo
								 ,
								 EndBalance = a.Saldo
								 ,
								 PreviousBalance = a.PreviousSaldo
								 ,
								 Adjustments = a.Saldo - a.StartSaldo
							 }).ToList();
			}
			string result = GenerateLeadsheets(cdc, fdc, lines,fls);
		   
			//return System.IO.Path.GetFileName(result);
			/* new fred */
			PDFDataContract pdc = new PDFDataContract
			{
				PDFLocation = result,
				Title = "Leadsheets"
			};
			return pdc;
		}

		public string GenerateLeadsheets(ClientDataContract client, FileDataContract file, List<LeadSheetLineDataContract> Lines, List<RepositoryListItemDataContract> fls)
		{
			LeadSheetGenerationDataContract generationContract = GetDefaultGenerationContract<LeadSheetGenerationDataContract>(client,file);
			generationContract.Lines = Lines;
			generationContract.LinkedFiles = fls;
			string rPath = System.IO.Path.Combine(Config.SchemaTemplates, "Ranges.xml");
			generationContract.Ranges = XDocument.Load(rPath);
			
			Core.IText.LeadSheets.Renderer renderer = new EY.com.eBook.Core.IText.LeadSheets.Renderer();
			string result = renderer.Render(generationContract);
			return result;
		}

		public string GetDocumentPrint(DocumentItemDataContract didc)
		{
			FileDataContract fdc = new BL.Read.FileReadRepository().GetFile(didc.FileId);
			ClientDataContract cdc = new BL.Read.ClientRepository().GetClient(fdc.ClientId);

			PDFDataContract pdc = GetDocumentPrint(cdc, fdc, didc);
			string pickup = System.IO.Path.Combine(Config.PickupLocal, string.Format("{0}.pdf", Guid.NewGuid().ToString()));
			System.IO.File.Move(pdc.PDFLocation, pickup);

			
			string virtualPath = pickup.Replace(Config.PickupLocal, Config.PickupVirtualDir);

			return virtualPath.Replace(@"\", "/");
		   
		}

		public PDFDataContract GetDocumentPrint(ClientDataContract client, FileDataContract file,DocumentItemDataContract didc)
		{
			return new DocumentRenderer(file, client, didc).CreatePdf();
		}

		public PDFDataContract GetDocumentPrintAndPlaceInRepo(ClientDataContract client, FileDataContract file, DocumentItemDataContract didc, Guid repoStructureId)
		{
			PDFDataContract pdoc = GetDocumentPrint(client, file, didc);
			pdoc.ItemId = didc.DocumentId;
			pdoc.Origin = string.Format("DOC::{0}::{1}::{2}", didc.DocumentTypeId.ToString(), didc.DocumentId.ToString(),didc.Culture);
			string path = System.IO.Path.Combine(Config.RepositoryLocal, client.Id.ToPath());
			path = System.IO.Path.Combine(path, string.Format("{0}.pdf", didc.DocumentId));
			if (!System.IO.Directory.Exists(System.IO.Path.GetDirectoryName(path)))
				System.IO.Directory.CreateDirectory(System.IO.Path.GetDirectoryName(path));
			if (System.IO.File.Exists(path)) System.IO.File.Delete(path);
			if(pdoc.PDFLocation != path) System.IO.File.Move(pdoc.PDFLocation, path);

			RepositoryItem ri = eBookReadManager.Context.RepositoryItems.FirstOrDefault(r => r.ItemId == didc.DocumentId);
			Boolean fileExists = false;
			if (ri == null)
			{
				ri = new RepositoryItem
				{
					ItemId = didc.DocumentId
					,
					ClientId = client.Id
					,
					Status = new Guid("D036D79A-B2AF-4A5F-A75F-F04BA59AFCC6") // DRAFT
					,
					PhysicalFileId = didc.DocumentId
				};


				ri.Meta = CoreHelper.SerializeToString(new List<RepositoryMetaItemDataContract>());
				// ri.Status = bdc.Draft ? new Guid("D036D79A-B2AF-4A5F-A75F-F04BA59AFCC6") : new Guid("58BC0DB9-F28F-4C8A-B363-7FCB9276A50F");
			}
			else {
				fileExists = true;
			}
			ri.StructureId = repoStructureId;
			ri.EndDate = file.EndDate;
			ri.StartDate = file.StartDate;
			ri.Name = didc.Title;
			ri.Prequel = "";
			ri.PhysicalFileId = didc.DocumentId;
			
			ri.Extension = ".pdf";
			ri.Contenttype = "application/pdf";
			if (ri.EntityState == System.Data.EntityState.Detached && !fileExists)
			{
				eBookReadManager.Context.AddToRepositoryItems(ri);
			}
			eBookReadManager.Context.SaveChanges();
			pdoc.Delete = false;
			pdoc.IsTempRender = false;
			return pdoc;
		}

		public string GetWorksheetPrint(WorksheetItemDataContract widc)
		{
			FileDataContract fdc = new BL.Read.FileReadRepository().GetFile(widc.FileId);
			ClientDataContract cdc = new BL.Read.ClientRepository().GetClient(fdc.ClientId);
			PDFDataContract pdc = GetWorksheetPrint(cdc, fdc, widc, true);

			  // pickup path (temp render)
			string virtualPath = PostProcessHeader(pdc, widc.Culture, widc.Title, cdc, fdc);
				virtualPath = virtualPath.Replace(Config.PickupLocal, Config.PickupVirtualDir);

				return virtualPath.Replace(@"\", "/");
		}

		public PDFDataContract GetWorksheetPrint(ClientDataContract client, FileDataContract file, WorksheetItemDataContract widc, bool addheader)
		{
			return GetWorksheetPrint(client, file, widc, addheader, null,false);
		}


		public PDFDataContract GetWorksheetPrint(ClientDataContract client, FileDataContract file, WorksheetItemDataContract widc, bool addheader, XElement source, bool biztaxdata)
		{
			
			CacheHelper cacheHelper = new CacheHelper();
			if (cacheHelper.CacheServiceConfigured)
			{
				cacheHelper.CreateCacheProxy();
				if (cacheHelper.CacheRunning)
				{
					//WorksheetWorkSetDataContract wwsdc = cacheHelper.GetCacheProxy().GetWorksheetSet(widc.FileId);
					PDFDataContract pdc = new WorksheetRenderer(file, client, widc, source, biztaxdata).CreatePdf();
					pdc.Title = widc.Title;
					return pdc;
				   // string virtualPath = pdc.PDFLocation.Replace(Config.RepositoryLocal, Config.RepositoryVirtualDir);

				}
			}
			return null;

		  //  PDFDataContract pdc = new WorksheetRenderer(
		}

		private string FormatVAT(string vat)
		{

			return string.IsNullOrEmpty(vat.Trim()) ? string.Empty : vat.Substring(0, 4) + "." + vat.Substring(4, 3) + "." + vat.Substring(7);
		}

		private string PostProcessHeader(PDFDataContract pdc, string culture, string title, ClientDataContract client, FileDataContract file)
		{

			string assessment = "{0}{1}";
			switch (culture)
			{
				case "nl-BE":
					assessment = "Aanslagjaar {0}{1}";
					break;
				case "fr-FR":
					assessment = "Exercice d'imposition {0}{1}";
					break;
				case "en-US":
					assessment = "Tax year {0}{1}";
					break;
			}

			XElement element = new XElement("data",
				new XElement("block",
					new XAttribute("type", "TOPLEFT"),
					new XElement("phrase", " "),
					new XElement("phrase",
						new XAttribute("font", Config.StandardFont),
						new XAttribute("fontstyle", "bold"),
						new XAttribute("size", "10"),
						new XCData(string.Format("          {0}", client.Name))
						),
					new XElement("phrase",
						new XAttribute("font", Config.StandardFont),
						new XAttribute("fontstyle", "bold"),
						new XAttribute("size", "10"),
						new XCData(string.Format("          {0}", string.IsNullOrEmpty(client.EnterpriseNumber) ? "" : FormatVAT(client.EnterpriseNumber)))
						)
				),
				new XElement("block",
					new XAttribute("type", "TOPRIGHT"),
					new XElement("phrase", " "),
					new XElement("phrase",
						new XAttribute("font", Config.StandardFont),
						new XAttribute("size", "10"),
						new XCData(string.Format("{0} - {1}", file.StartDate.ToString("dd/MM/yyyy"), file.EndDate.ToString("dd/MM/yyyy")))
						),
					new XElement("phrase",
						new XAttribute("font", Config.StandardFont),
						new XAttribute("size", "10"),
						new XCData(string.Format(assessment, file.AssessmentYear, ""))
						)
				)
			);

			if (!string.IsNullOrEmpty(title))
			{
				element.Add(new XElement("block",
					new XAttribute("type", "TOPTITLE"),
					new XElement("phrase",
						new XAttribute("font", Config.StandardFont),
						new XAttribute("size", "10"),
						new XAttribute("fontstyle", "bold underline"),
						new XCData(title)
						)
					)
				);
			}



			EY.com.eBook.Core.IText.Render.ITextRenderer ir = new EY.com.eBook.Core.IText.Render.ITextRenderer();
			string fname = string.Format("{0}.pdf", Guid.NewGuid());
			string path = System.IO.Path.Combine(Config.PickupLocal, fname);
			ir.PostProcessHeaderFooter(pdc, element, path);
			//public string Generate
			return path;
		}


		public string GenerateBundle(Guid id)
		{
			var bndl = new BundleRepository().GetBundle(id);
			return GenerateBundle(bndl, false);
		}

		public string GenerateBundle(BundleDataContract bdc)
		{
			return GenerateBundle(bdc, false);
		}

		private bool CheckPaging(List<IndexItemBaseDataContract> list)
		{
			bool paging = false;
			list.ForEach(l => { 
				if (l.FooterConfig != null) {
					paging = paging || (l.FooterConfig.ShowPageNr && l.FooterConfig.Enabled);
				} 
				if (l.GetType().Name == typeof(ChapterDataContract).Name && paging ==false)
				{
					paging = paging || CheckPaging(((ChapterDataContract)l).Items);
				}
			
			});

			return paging;
		}

		public string GenerateBundle(BundleDataContract bdc, bool temp)
		{
			if(!temp && !bdc.Locked) new BundleRepository().SaveBundle(bdc);
			FileDataContract fdc = new BL.Read.FileReadRepository().GetFile(bdc.FileId);
			ClientDataContract cdc = new BL.Read.ClientRepository().GetClient(fdc.ClientId);

			FooterConfigDataContract fcdc = new FooterConfigDataContract
			{
				Enabled = true
				,
				ShowPageNr = CheckPaging(bdc.Contents)
			};

			// STEP 1: GENERATE ITEMS THAT NEED GENERATION
			bdc.Contents = GenerateBundleItems(bdc.Culture, fdc,cdc, bdc.Contents, bdc.HeaderFooterType);
			
			// default footer:
		   
			switch (bdc.Culture)
			{
				case "nl-BE":
					fcdc.FootNote = "Deze cijfers werden niet onderworpen aan een audit of beperkt nazicht door EY";
					break;
				case "fr-FR":
					fcdc.FootNote = "Ces chiffres n'ont pas fait l'objet ni d'un controle révisoral, ni d'une révision limitée de la part d'EY";
					break;
			}

			string path = System.IO.Path.Combine(Config.RepositoryLocal, cdc.Id.ToPath());
			if (temp) path = Config.PickupLocal;
			path = System.IO.Path.Combine(path,string.Format("{0}.pdf",bdc.Id));

			// STEP 2: GENERATE BUNDLE
			PDFDataContract pdc = new BundleRenderer(bdc, null, fcdc, GetBundleHeader(bdc.Culture, cdc, fdc)).CreatePdf();

			if (!System.IO.Directory.Exists(System.IO.Path.GetDirectoryName(path)))
				System.IO.Directory.CreateDirectory(System.IO.Path.GetDirectoryName(path));
			if (System.IO.File.Exists(path)) System.IO.File.Delete(path);
			if(pdc.PDFLocation != path) System.IO.File.Move(pdc.PDFLocation, path);

			RepositoryItem ri = eBookReadManager.Context.RepositoryItems.FirstOrDefault(r => r.ItemId == bdc.Id);
			if (ri == null)
			{
				ri = new RepositoryItem
				{
					ItemId = bdc.Id
					,
					ClientId = cdc.Id
					,
					PhysicalFileId = bdc.Id
				};
			}
			ri.StructureId = new Guid("4587B8AC-7AC1-44A4-8106-CC7CD7DFCDEF");
			ri.Meta = CoreHelper.SerializeToString(new List<RepositoryMetaItemDataContract>());
			ri.EndDate = fdc.EndDate;
			ri.StartDate = fdc.StartDate;
			ri.Name = bdc.Name;
			ri.Prequel ="";
			ri.Status = bdc.Draft ? new Guid("D036D79A-B2AF-4A5F-A75F-F04BA59AFCC6") : new Guid("58BC0DB9-F28F-4C8A-B363-7FCB9276A50F");
			ri.Extension = ".pdf";
			ri.Contenttype = "application/pdf";
			if (ri.EntityState == System.Data.EntityState.Detached)
				eBookReadManager.Context.AddToRepositoryItems(ri);
			eBookReadManager.Context.SaveChanges();


			CleanUpTemporaryBundleItems(bdc.Contents);

			
			string virtualPath = temp ? path.Replace(Config.PickupLocal,Config.PickupVirtualDir) 
										: path.Replace(Config.RepositoryLocal, Config.RepositoryVirtualDir);
		   

			return virtualPath.Replace(@"\", "/");
		}

		private void CleanUpTemporaryBundleItems(List<IndexItemBaseDataContract> items)
		{
			for (int i = 0; i < items.Count; i++)
			{
				IndexItemBaseDataContract item = items[i];
				switch (item.GetType().Name)
				{
					case "ChapterDataContract":
						ChapterDataContract chdc = (ChapterDataContract)item;
						CleanUpTemporaryBundleItems(chdc.Items);
						break;
					case "PDFDataContract":
						PDFDataContract pdc = (PDFDataContract)item;
						if (pdc.Delete)
						{
							if (System.IO.File.Exists(pdc.PDFLocation))
							{
								System.IO.File.Delete(pdc.PDFLocation);
							}
						}
						break;

				}
			}
		}

		public XElement BizTaxData { get; set; }

		private EY.com.eBook.BizTax.Contracts.BizTaxDataContract BizTax { get; set; }

	   
		public List<IndexItemBaseDataContract> GenerateBundleItems(string culture,FileDataContract fdc, ClientDataContract cdc, List<IndexItemBaseDataContract> items,string hfType)
		{
			int itemscount = items.Count;
            XslCompiledTransform xslTransformer;
            XsltArgumentList arguments;
            XsltSettings settings;
            StringWriter stringWriter;
            XmlWriterSettings wSettings;
            XmlWriter xmlTextWriter;
            PDFDataContract bfpdc;

			// PRE PREP AUTO BIZTAX
			for (int i = 0; i < itemscount; i++)
			{
				IndexItemBaseDataContract item = items[i];

				if (item.GetType().Name == "AutoBizTaxDataContract")
				{
					
					ChapterDataContract acdc = new ChapterDataContract
					{
						Id = Guid.NewGuid()
						 ,
						Title = "Biztax"
						 ,
						Items = new List<IndexItemBaseDataContract>()
						,
						ShowInIndex = true
					};
					if (BizTaxData == null)
					{
						Core.EF.Write.FileXbrl fx = Core.EF.eBookWriteManager.Context.FileXbrlSet.FirstOrDefault(f => f.FileId == fdc.Id);
						if (fx != null)
						{
							BizTaxData = XElement.Parse(fx.XbrlData);
							BizTaxData = BizTaxData.StripNamespaces();
							BizTax = CoreHelper.DeserializeFromString<EY.com.eBook.BizTax.Contracts.BizTaxDataContract>(fx.XbrlData);
						}
						else
						{
							BizTaxData = new XElement("BizTaxDataContract");
							BizTax = new BizTax.Contracts.BizTaxDataContract();
						}
					}
					if (BizTax != null && BizTax.Fiches != null)
					{
						int itemsAdded = 0;
						items[i] = new CoverpageDataContract();//GetBiztaxFicheContract(culture, "Cover");
						itemsAdded++;
						items.Insert(i + itemsAdded, new IndexPageDataContract());
						itemsAdded++;
						items.Insert(i + itemsAdded, acdc);
						itemsAdded++;

						foreach (var ficheitem in BizTax.Fiches)
						{
							items.Insert(i + itemsAdded, GetBiztaxFicheContract(culture, ficheitem.FicheId));
							itemsAdded++;
						}
					}
				}
			}


			for (int i = 0; i < items.Count; i++)
			{
				IndexItemBaseDataContract item = items[i];
				switch (item.GetType().Name)
				{
					case "BizTaxTaxCalcDataContract":
						BizTaxTaxCalcDataContract btcdc = (BizTaxTaxCalcDataContract)item;
						if (BizTaxData == null)
						{
							Core.EF.Write.FileXbrl fx = Core.EF.eBookWriteManager.Context.FileXbrlSet.FirstOrDefault(f => f.FileId == fdc.Id);
							if (fx != null)
							{
								BizTaxData = XElement.Parse(fx.XbrlData);
								BizTaxData = BizTaxData.StripNamespaces();
								BizTax = CoreHelper.DeserializeFromString<EY.com.eBook.BizTax.Contracts.BizTaxDataContract>(fx.XbrlData);
							}
							else
							{
								BizTaxData = new XElement("BizTaxDataContract");
								BizTax = new BizTax.Contracts.BizTaxDataContract();
							}
						}

						WorksheetItemDataContract btwdc = new WorksheetItemDataContract
						{
							WorksheetType = new Guid("803D6A06-352E-4E28-ADE5-214DEAAC76CA")
							, Culture = culture
							, FileId = fdc.Id
							, PrintLayout = null
							, NoDraft = true
							, Title = btcdc.Title
							,
							RuleApp = "BerekeningVenBApp"
							
						};
                        PDFDataContract bttcdc = GetWorksheetPrint(cdc, fdc, btwdc, false, BizTax.TaxCalcDetail != null ? CoreHelper.SerializeToXElement(BizTax.TaxCalcDetail, true) : null, true);
                        items[i] = bttcdc;

						break;
					case "BizTaxFicheDataContract":
						BizTaxFicheDataContract bfdc = (BizTaxFicheDataContract)item;
						
						if (BizTaxData == null)
						{
							Core.EF.Write.FileXbrl fx= Core.EF.eBookWriteManager.Context.FileXbrlSet.FirstOrDefault(f=>f.FileId==fdc.Id);
							if(fx!=null) {
								BizTaxData = XElement.Parse(fx.XbrlData);
								BizTaxData = BizTaxData.StripNamespaces();
								BizTax = CoreHelper.DeserializeFromString<EY.com.eBook.BizTax.Contracts.BizTaxDataContract>(fx.XbrlData);
							} else {
								BizTaxData=new XElement("BizTaxDataContract");
								BizTax = new BizTax.Contracts.BizTaxDataContract();
							}
						}
							

						xslTransformer = new XslCompiledTransform(Config.DebugXslt());

						settings = new XsltSettings
						{
							EnableDocumentFunction = true
							,
							EnableScript = true
						};

						xslTransformer.Load(Config.GetBiztaxFicheTemplate(fdc.AssessmentYear,cdc.BNI ? "nrcorp":"rcorp",bfdc.Fiche) , settings, null);
						arguments = new XsltArgumentList();
						arguments.AddParam("Culture", "", fdc.Culture);

						stringWriter = new StringWriter();
						wSettings = new XmlWriterSettings();
						wSettings.NewLineHandling = NewLineHandling.None;
						wSettings.NewLineOnAttributes = false;
						xmlTextWriter = XmlTextWriter.Create(stringWriter, wSettings);

						xslTransformer.Transform(BizTaxData.CreateNavigator(), arguments, xmlTextWriter);

						bfdc.iTextTemplate = stringWriter.GetStringBuilder().ToString();


						// CLEANUP
						xslTransformer = null;
						stringWriter.Close();
						stringWriter.Dispose();
						stringWriter = null;
						wSettings = null;
						xmlTextWriter = null;

						bfpdc = new ITextRenderer(bfdc.iTextTemplate, bfdc).CreatePdf();
						items[i] = bfpdc;

						
						if (bfdc.Fiche == "275.1.B")
						{
							ChapterDataContract bfcdc = new ChapterDataContract
							{
								Id = Guid.NewGuid()
								 ,
								Title = bfpdc.Title
								 ,
								Items = new List<IndexItemBaseDataContract>()
								,
								ShowInIndex = true
							};

							//

							BizTax.XbrlElementDataContract xedc = BizTax.Elements.FirstOrDefault(x => x.Name == "StatutoryAccounts");
							if (xedc != null && xedc.BinaryValue != null && xedc.BinaryValue.Count > 0)
							{
								bfcdc.Items.AddRange(xedc.BinaryValue);
							}
						 
							xedc = BizTax.Elements.FirstOrDefault(x => x.Name == "GeneralMeetingMinutesDecisions");
							if (xedc != null && xedc.BinaryValue != null && xedc.BinaryValue.Count > 0)
							{
								bfcdc.Items.AddRange(xedc.BinaryValue);
							}
							xedc = BizTax.Elements.FirstOrDefault(x => x.Name == "ExemptionProfitHomologationReorganizationPlanAmicableSettlementDocuments");
							if (xedc != null && xedc.BinaryValue != null && xedc.BinaryValue.Count > 0)
							{
								bfcdc.Items.AddRange(xedc.BinaryValue);
							}

							xedc = BizTax.Elements.FirstOrDefault(x => x.Name == "InternalStatutoryAccounts");
							if (xedc != null && xedc.BinaryValue != null && xedc.BinaryValue.Count > 0)
							{
								bfcdc.Items.AddRange(xedc.BinaryValue);
							}

							xedc = BizTax.Elements.FirstOrDefault(x => x.Name == "DepreciationTableNonStructured");
							if (xedc != null && xedc.BinaryValue != null && xedc.BinaryValue.Count > 0)
							{
								bfcdc.Items.AddRange(xedc.BinaryValue);
							}

							xedc = BizTax.Elements.FirstOrDefault(x => x.Name == "OtherDocuments");
							if (xedc != null && xedc.BinaryValue != null && xedc.BinaryValue.Count > 0)
							{
								bfcdc.Items.AddRange(xedc.BinaryValue);
							}

							if (bfcdc.Items.Count>0)
							{
								bfcdc.Items.ForEach(bit => { bit.FooterConfig = nofooter; });
								bfcdc.Items = GenerateBundleItems(culture, fdc, cdc, bfcdc.Items, hfType);
								bfcdc.Items.Insert(0, bfpdc);
								items[i] = bfcdc;
							}
						}
						else if (bfdc.Fiche == "275.2.B.1")
						{
							ChapterDataContract bfcdc = new ChapterDataContract
							{
								Id = Guid.NewGuid()
								 ,
								Title = bfpdc.Title
								 ,
								Items = new List<IndexItemBaseDataContract>()
								,
								ShowInIndex = true
							};

							//

							BizTax.XbrlElementDataContract xedc = BizTax.Elements.FirstOrDefault(x => x.Name == "BalanceSheetStatutorySeatStatutoryAccountsBelgianBranch");
							if (xedc != null && xedc.BinaryValue != null && xedc.BinaryValue.Count > 0)
							{
								bfcdc.Items.AddRange(xedc.BinaryValue);
							}

							xedc = BizTax.Elements.FirstOrDefault(x => x.Name == "ExemptionProfitHomologationReorganizationPlanAmicableSettlementDocuments");
							if (xedc != null && xedc.BinaryValue != null && xedc.BinaryValue.Count > 0)
							{
								bfcdc.Items.AddRange(xedc.BinaryValue);
							}
							xedc = BizTax.Elements.FirstOrDefault(x => x.Name == "InternalStatutoryAccounts");
							if (xedc != null && xedc.BinaryValue != null && xedc.BinaryValue.Count > 0)
							{
								bfcdc.Items.AddRange(xedc.BinaryValue);
							}

							xedc = BizTax.Elements.FirstOrDefault(x => x.Name == "DepreciationTableNonStructured");
							if (xedc != null && xedc.BinaryValue != null && xedc.BinaryValue.Count > 0)
							{
								bfcdc.Items.AddRange(xedc.BinaryValue);
							}

							xedc = BizTax.Elements.FirstOrDefault(x => x.Name == "OtherDocuments");
							if (xedc != null && xedc.BinaryValue != null && xedc.BinaryValue.Count > 0)
							{
								bfcdc.Items.AddRange(xedc.BinaryValue);
							}

							if (bfcdc.Items.Count > 0)
							{
								bfcdc.Items.ForEach(bit => { bit.FooterConfig = nofooter; });
								bfcdc.Items = GenerateBundleItems(culture, fdc, cdc, bfcdc.Items, hfType);
								bfcdc.Items.Insert(0, bfpdc);
								items[i] = bfcdc;
							}
						} else if (bfdc.Fiche == "275.2.B.1")
						{
							ChapterDataContract bfcdc = new ChapterDataContract
							{
								Id = Guid.NewGuid()
								 ,
								Title = bfpdc.Title
								 ,
								Items = new List<IndexItemBaseDataContract>()
								,
								ShowInIndex = true
							};

							//

							BizTax.XbrlElementDataContract xedc = BizTax.Elements.FirstOrDefault(x => x.Name == "BalanceSheetStatutorySeatStatutoryAccountsBelgianBranchLegalEntity");
							if (xedc != null && xedc.BinaryValue != null && xedc.BinaryValue.Count > 0)
							{
								bfcdc.Items.AddRange(xedc.BinaryValue);
							}

							xedc = BizTax.Elements.FirstOrDefault(x => x.Name == "AccountsAssessmentLiabilityTaxRegimeLegalEntityIncomeTaxTaxableIncome");
							if (xedc != null && xedc.BinaryValue != null && xedc.BinaryValue.Count > 0)
							{
								bfcdc.Items.AddRange(xedc.BinaryValue);
							}
							xedc = BizTax.Elements.FirstOrDefault(x => x.Name == "OtherDocumentsLegalEntity");
							if (xedc != null && xedc.BinaryValue != null && xedc.BinaryValue.Count > 0)
							{
								bfcdc.Items.AddRange(xedc.BinaryValue);
							}

							if (bfcdc.Items.Count > 0)
							{
								bfcdc.Items.ForEach(bit => { bit.FooterConfig = nofooter; });
								bfcdc.Items = GenerateBundleItems(culture, fdc, cdc, bfcdc.Items, hfType);
								bfcdc.Items.Insert(0, bfpdc);
								items[i] = bfcdc;
							}
						}


						
						
						
						break;
				  /*  case "BizTaxBundleDataContract":
						BizTaxBundleDataContract btpdc = (BizTaxBundleDataContract)item;
						BizTaxStandardRenderer btr = new BizTaxStandardRenderer();
						
						BizTaxDataContract btdc = btr.LoadFromDb(fdc.Id);
						btr = null;


						List<IndexItemBaseDataContract> lst = GenerateBizTaxAttachments(culture, fdc, cdc, btdc, hfType);

						btpdc.Title = culture == "nl-BE" ? "BizTax aangifte" : culture == "fr-FR" ? "BizTax déclaration" : "BizTax Declaration";
						//btpdc.Culture = culture;
						PDFDataContract bpdc = new BizTaxBundleRenderer(btpdc, fdc, cdc, btdc,lst).CreatePdf();
						bpdc.ShowInIndex = true;
						bpdc.Title = btpdc.Title;
						//List<IndexItemBaseDataContract> lst = new List<IndexItemBaseDataContract>();
					   
						if (bpdc.FooterConfig == null)bpdc.FooterConfig = new FooterConfigDataContract();
						bpdc.FooterConfig.Enabled = true;
						
						items[i] = bpdc;
						//items[i].Title = "COVERPAGE";
						break;*/
					case "PDFDataContract":
						PDFDataContract pdc = (PDFDataContract)item;
						if (pdc.ItemId == Guid.Empty) pdc.ItemId = pdc.Id;
						string fname = string.Format("{0}.pdf", pdc.ItemId.ToString());
						pdc.PDFLocation = System.IO.Path.Combine(Config.RepositoryLocal, cdc.Id.ToPath());
						pdc.PDFLocation = System.IO.Path.Combine(pdc.PDFLocation, fname);
						if (!System.IO.File.Exists(pdc.PDFLocation))
						{
							throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { ErrorCode = -1, Message = string.Format("Pdf file not found: {0}: {1}", pdc.Title, pdc.PDFLocation) });
						}
						pdc.Delete = false;
						items[i] = pdc;
						break;
					case "CoverpageDataContract":
						CoverpageDataContract cpdc = (CoverpageDataContract)item;
						cpdc.Culture = culture;
						if (string.IsNullOrEmpty(cpdc.CoverType))
						{
							cpdc.CoverType = string.IsNullOrEmpty(hfType) ? "ACR" : hfType;
						}
						items[i] = new CoverpageRenderer(cpdc, fdc, cdc).CreatePdf();
						if (items[i].FooterConfig == null) items[i].FooterConfig = new FooterConfigDataContract();
						items[i].FooterConfig.Enabled = true;
						items[i].Title = "COVERPAGE";
						break;
                    case "TablePageDataContract":
                        TablePageDataContract tpdc = (TablePageDataContract)item;
                        /*tpdc.Culture = culture;
                        if (string.IsNullOrEmpty(tpdc.CoverType))
						{
                            tpdc.CoverType = string.IsNullOrEmpty(hfType) ? "ACR" : hfType;
						}*/
                        items[i] = new TablePageRenderer(tpdc).CreatePdf();
						if (items[i].FooterConfig == null) items[i].FooterConfig = new FooterConfigDataContract();
						items[i].FooterConfig.Enabled = true;
						items[i].Title = "COVERPAGE";
						break;
					case "ChapterDataContract":
						ChapterDataContract chdc = (ChapterDataContract)item;
						chdc.FooterConfig = null;
						chdc.Items = GenerateBundleItems(culture, fdc, cdc, chdc.Items,hfType);
						items[i] = chdc;
						break;
					case "WorksheetItemDataContract":
						WorksheetItemDataContract wdc = (WorksheetItemDataContract)item;
						PDFDataContract wpdc = GetWorksheetPrint(cdc,fdc,wdc,false);
						items[i] = wpdc;
						break;
					case "StatementsDataContract":
						// ADD vergelijkend flag aan data contract DONE
						// pas renderer aan om vergelijkende versie te genereren
						StatementsDataContract sdc = (StatementsDataContract)item;
						PDFDataContract spdc = RenderStatements(fdc, sdc);

						items[i] = spdc;
						break;
					case "DocumentItemDataContract":
						DocumentItemDataContract didc = (DocumentItemDataContract)item;
						/*BundleTextDataContract btdc = new BundleTextDataContract
						{
							Culture = didc.Culture
							,
							Title = didc.Title
							,
							Text = "document " + didc.Title + " not found. Create the document first."
						};
						PDFDataContract tpdc = new TextRenderer(btdc).CreatePdf();*/
						PDFDataContract dpdc = new DocumentRenderer(fdc, cdc, didc).CreatePdf();
                        dpdc.Delete = true;
                        items[i] = dpdc;
						break;
					case "LeadsheetsDataContract":
						LeadsheetsDataContract ldc = (LeadsheetsDataContract)item;
						 CriteriaIdAndCultureDataContract cidc = new CriteriaIdAndCultureDataContract{
							Id = ldc.FileId,
							Culture = ldc.Culture
						};
						 PDFDataContract lpdc =  GenerateLeadsheets(cidc);
						 items[i] = lpdc;
						break;
                    case "ConfirmationOfApprovalCitrDataContract":
                        ConfirmationOfApprovalCitrDataContract coacdc = (ConfirmationOfApprovalCitrDataContract)item;

                        if (BizTaxData == null)
                        {
                            Core.EF.Write.FileXbrl fx = Core.EF.eBookWriteManager.Context.FileXbrlSet.FirstOrDefault(f => f.FileId == fdc.Id);
                            if (fx != null)
                            {
                                BizTaxData = XElement.Parse(fx.XbrlData);
                                BizTaxData = BizTaxData.StripNamespaces();
                                BizTax = CoreHelper.DeserializeFromString<EY.com.eBook.BizTax.Contracts.BizTaxDataContract>(fx.XbrlData);
                            }
                            else
                            {
                                BizTaxData = new XElement("BizTaxDataContract");
                                BizTax = new BizTax.Contracts.BizTaxDataContract();
                            }
                        }

                        xslTransformer = new XslCompiledTransform(Config.DebugXslt());

						settings = new XsltSettings
						{
							EnableDocumentFunction = true
							,
							EnableScript = true
						};

                        //Load and compile XSLT
						xslTransformer.Load(Config.GetPdfTemplate(coacdc.RuleApp,fdc.AssessmentYear,coacdc.PrintLayout) , settings, null);

                        //Add arguments
						arguments = new XsltArgumentList();
						arguments.AddParam("Culture", "", coacdc.Culture);
                        arguments.AddParam("CVBA", "", coacdc.CVBA);
                        arguments.AddParam("Title", "", coacdc.Title);
                        arguments.AddParam("BNI", "", coacdc.BNI);

						stringWriter = new StringWriter();
						wSettings = new XmlWriterSettings();
						wSettings.NewLineHandling = NewLineHandling.None;
						wSettings.NewLineOnAttributes = false;
						xmlTextWriter = XmlTextWriter.Create(stringWriter, wSettings);

                        //Transform using Biztax and arguments given
						xslTransformer.Transform(BizTaxData.CreateNavigator(), arguments, xmlTextWriter);

                        coacdc.iTextTemplate = stringWriter.GetStringBuilder().ToString();

                        bfpdc = new ITextRenderer(coacdc.iTextTemplate, coacdc).CreatePdf();
						items[i] = bfpdc;
                        //PDFDataContract coacpdc = RenderConfirmationOfApprovalCitr(fdc, cdc, coacdc, BizTax, coacdc.Culture);
                        break;
					case "CorrectionBookingDataContract":
						CorrectionBookingDataContract cbdc = (CorrectionBookingDataContract)item;
						PDFDataContract bipdc = RenderBookings(fdc, cbdc, cbdc.culture);
						items[i]=bipdc;
						break;
					case "CorrectionBookingSchemaDataContract":
						CorrectionBookingSchemaDataContract cbsdc = (CorrectionBookingSchemaDataContract)item;
						PDFDataContract bispdc = RenderBookingsSchema(fdc, cbsdc, cbsdc.culture);
						items[i]=bispdc;
						break;

				}
			}
			return items;
		}

		private IndexItemBaseDataContract GetBiztaxFicheContract(string culture, string ficheId)
		{
			string title = "";
			switch (ficheId.ToUpper())
			{
				case "ID":
					title = culture == "nl-BE" ? "Ondernemingsgegevens" : culture == "fr-FR" ? "Signalétique de l'entreprise" : "Company Information Form";
					break;
				case "275.1.A":
					title = culture == "nl-BE" ? "275.1.A - Aangifte in de vennootschapsbelasting" : culture == "fr-FR" ? "275.1.A - Déclaration à l’impôt des sociétés" : "275.1.A - Tax Return Resident Corporate Tax Form";
					break;
				case "275.1.B":
					title = culture == "nl-BE" ? "275.1.B - Diverse bescheiden en opgaven" : culture == "fr-FR" ? "275.1.B - Documents et relevés divers" : "275.1.B - Documents Statements Form";
					break;
				case "204.3":
					title = culture == "nl-BE" ? "204.3 - Waardeverminderingen voor waarschijnlijke verliezen en voorzieningen voor risico's en kosten" : culture == "fr-FR" ? "204.3 - Réductions de valeur pour pertes probables et provisions pour risques et charges" : "204.3 - Write Downs Debt Claims Provisions Expenses Risks Form";
					break;
                case "275A":
                    title = culture == "nl-BE" ? "275A - Liquidatiereserve" : culture == "fr-FR" ? "275A - Réserve de liquidation" : "275A - Liquidation reserve";
                    break;
				case "275K":
					title = culture == "nl-BE" ? "275K - Gespreide belasting van meerwaarden op bepaalde effecten" : culture == "fr-FR" ? "275K - Taxation étalée des plus-values sur certains titres" : "275K - Spread Taxable Capital Gains Specific Securities Form";
					break;
				case "276K":
					title = culture == "nl-BE" ? "276K - Gespreid te belasten meerwaarden op materiële en immateriële vaste activa" : culture == "fr-FR" ? "276K - Taxation étalée des plus-values sur immobilisations corporelles et incorporelles" : "276K - Spread Taxable Capital Gains Tangible Intangible Fixed Assets Form";
					break;
				case "276N":
					title = culture == "nl-BE" ? "276N - Vrijstelling van meerwaarden op bedrijfsvoertuigen" : culture == "fr-FR" ? "276N - Exonération des plus-values sur véhicules d’entreprise" : "276N - Exemption Capital Gains Corporate Vehicles Form";
					break;
				case "276P":
					title = culture == "nl-BE" ? "276P - Vrijstelling van meerwaarden op binnenschepen voor de commerciële vaart" : culture == "fr-FR" ? "276P - Exonération des plus-values sur bateaux de navigation intérieure destinés à la navigation commerciale" : "276P - Exemption Capital Gains River Vessels Commercial Trade Form";
					break;
				case "275C":
					title = culture == "nl-BE" ? "275C - Aftrek voor risicokapitaal" : culture == "fr-FR" ? "275C - Déduction pour capital à risque" : "275C - Allowance Corporate Equity Form";
					break;
				case "275F":
					title = culture == "nl-BE" ? "275F - Aangifte van gedane betalingen naar staten die niet effectief en substantieel de OESO-standaard op het gebied van uitwisseling van inlichtingen toepassen of naar staten zonder of met een lage belasting" : culture == "fr-FR" ? "275F - Déclaration des paiements effectués vers des états qui n'appliquent pas substantiellement et effectivement le standard de l'OCDE d'échange d'informations ou vers des états à fiscalité inexistante ou peu élevée" : "275F - Payments Certain States Form";
					break;
				case "275P":
					title = culture == "nl-BE" ? "275P - Aftrek voor octrooi-inkomsten" : culture == "fr-FR" ? "275P - Déduction pour revenus de brevets" : "275P - Deduction Patents Income Form";
					break;
				case "275R":
					title = culture == "nl-BE" ? "275R - Investeringsreserve" : culture == "fr-FR" ? "275R - Réserve d'investissement" : "275R - Investment Reserve Form";
					break;
				case "275U":
					title = culture == "nl-BE" ? "275U - Investeringsaftrek" : culture == "fr-FR" ? "275U - Déduction pour investissement" : "275U - Investment Deduction Form";
					break;
				case "275W":
					title = culture == "nl-BE" ? "275W - Belastingkrediet voor onderzoek en ontwikkeling" : culture == "fr-FR" ? "275W - Crédit d'impôt pour recherche et développement" : "275W - Tax Credit Research Development Form";
					break;
				case "276T":
					title = culture == "nl-BE" ? "276T - Vrijstelling bijkomend personeel" : culture == "fr-FR" ? "276T - Exonération pour personnel supplémentaire" : "276T - Exemption Additional Personnel Form";
					break;
				case "276W1":
					title = culture == "nl-BE" ? "276W1 - Vrijstelling voor bijkomend personeel tewerkgesteld voor wetenschappelijk onderzoek" : culture == "fr-FR" ? "276W1 - Exonération pour personnel supplémentaire affecté à la recherche scientifique" : "276W1 - Exemption Additional Personnel Scientific Research Form";
					break;
				case "276W2":
					title = culture == "nl-BE" ? "276W2 - Vrijstelling voor bijkomend personeel tewerkgesteld voor de uitbouw van het technologisch potentieel van de onderneming" : culture == "fr-FR" ? "276W2 - Exonération pour personnel supplémentaire affecté au développement du potentiel technologique de l'entreprise" : "276W2 - Exemption Additional Personnel Development Technological Potential Form";
					break;
				case "276W3":
					title = culture == "nl-BE" ? "276W3 - Vrijstelling voor bijkomend personeel tewerkgesteld als diensthoofd voor de uitvoer" : culture == "fr-FR" ? "276W3 - Exonération pour personnel supplémentaire affecté comme chef de service des exportations" : "276W3 - Exemption Additional Personnel Head Export Department Form";
					break;
				case "276W4":
					title = culture == "nl-BE" ? "276W4 - Vrijstelling voor bijkomend personeel als diensthoofd van de afdeling Integrale kwaliteitszorg" : culture == "fr-FR" ? "276W4 - Exonération pour personnel supplémentaire affecté comme chef de service de la section Gestion intégrale de la qualité" : "276W4 - Exemption Additional Personnel Head T Q M Department Form";
					break;
				case "328K":
					title = culture == "nl-BE" ? "328K - Degressief af te schrijven vaste activa" : culture == "fr-FR" ? "328K - Immobilisations à amortir de manière dégressive" : "328K - Degressive Depreciation Fixed Assets Form";
					break;
				case "328L":
					title = culture == "nl-BE" ? "328L - Vaste activa voorheen in het stelsel van degressieve afschrijvingen opgenomen" : culture == "fr-FR" ? "328L - Immobilisations amorties antérieurement de manière dégressive" : "328L - Revocation Degressive Depreciation Fixed Assets Form";
					break;
				case "275.2.A.1":
					title = culture == "nl-BE" ? "275.2.A.1 - Aangifte in de belasting van niet-inwoners - Buitenlandse vennootschappen, verenigingen, instellingen of lichamen die een onderneming exploiteren of zich met verrichtingen van winstgevende aard bezighouden" : culture == "fr-FR" ? "275.2.A.1 - Déclaration à l’impôt des non-résidents - Sociétés, associations, établissements ou organismes quelconques étrangers qui se livrent à une exploitation ou à des opérations de caractère lucratif" : "275.2.A.1 - Tax Return Non Resident Corporate Tax Form";
					break;
				case "275.2.B.1":
					title = culture == "nl-BE" ? "275.2.B.1 - Diverse bescheiden en opgaven" : culture == "fr-FR" ? "275.2.B.1 - Documents et relevés divers" : "275.2.B.1 - Documents Statements Foreign Companies Associations Establishments Institutions Engaged In Business Profit Making Activity Form";
					break;
				case "275.2.A.2":
					title = culture == "nl-BE" ? "275.2.A.2 - Aangifte in de belasting van niet-inwoners - Rechtspersonen die geen onderneming exploiteren of zich niet met verrichtingen van winstgevende aard bezighouden" : culture == "fr-FR" ? "275.2.A.2 - Déclaration à l’impôt des non-résidents - Personnes morales qui ne se livrent pas à une exploitation ou à des opérations de caractère lucratif" : "275.2.A.2 - Tax Return Legal Person Not Engaged In Business Profit Making Activity Form";
					break;
				case "275.2.B.2":
					title = culture == "nl-BE" ? "275.2.B.2 - Diverse bescheiden en opgaven" : culture == "fr-FR" ? "275.2.B.2 - Documents et relevés divers" : "275.2.B.2 - Documents Statements Legal Person Not Engaged In Business Profit Making Activity Form";
					break;
			}
			return new BizTaxFicheDataContract
				{
					FooterConfig = nofooter
					,
					HeaderConfig = noheader
					,
					Id = ("BIZTAX_FICHE_" + ficheId).DeterministicGuid()
					,
					ShowInIndex = true
					,
					Title = title
					,
					IconCls = "eBook-bundle-tree-biztax"
					,
					Fiche = ficheId
				};
		}

		public string GenerateBundleItemsBase64(List<IndexItemBaseDataContract> items,Guid fileId)
		{
			FileDataContract fdc = new BL.Read.FileReadRepository().GetFile(fileId);
			ClientDataContract cdc = new BL.Read.ClientRepository().GetClient(fdc.ClientId);
			items = GenerateBundleItems(fdc.Culture, fdc, cdc, items, string.Empty);
			string base64 = new Base64BundleRenderer(items).CreateBase64();
			return base64;
		}

		private List<IndexItemBaseDataContract> GenerateBizTaxAttachments(string culture, FileDataContract fdc, ClientDataContract cdc, BizTaxDataContract btdc, string hfType)
		{
			List<IndexItemBaseDataContract> items = new List<IndexItemBaseDataContract>();
			foreach (XbrlElementDataContract xedc in btdc.Elements.Where(e => e.BinaryValue != null && e.BinaryValue.Count > 0))
			{
				items.Add(new ChapterDataContract
				{
					Title = xedc.Id
					,
					Items = xedc.BinaryValue
				});
			}
			if (items.Count == 0) return items;
			//bpdc.Title = culture == "nl-BE" ? "BizTax formulieren" : culture == "fr-FR" ? "BizTax formulaires" : "BizTax Forms";
			return GenerateBundleItems(culture, fdc, cdc, items, hfType);
		} 

		public PDFDataContract RenderStatements(FileDataContract fdc, StatementsDataContract sdc)
		{
			CacheHelper cacheHelper = new CacheHelper();
			if (cacheHelper.CacheServiceConfigured)
			{
				cacheHelper.CreateCacheProxy();
				if (cacheHelper.CacheRunning)
				{
					List<AccountDataContract> accounts = cacheHelper.GetCacheProxy().GetAllAccounts(sdc.FileId, true, false, sdc.Culture);
					PDFDataContract pdc =  new StatementRenderer(fdc,sdc,accounts).CreatePdf();
					return pdc;
				}
			}
			return null;
		}

        public PDFDataContract RenderConfirmationOfApprovalCitr(FileDataContract fdc, ClientDataContract cdc, ConfirmationOfApprovalCitrDataContract coacdc, EY.com.eBook.BizTax.Contracts.BizTaxDataContract btdc, string culture)
        {
            CacheHelper cacheHelper = new CacheHelper();

            if (cacheHelper.CacheServiceConfigured)
            {
                cacheHelper.CreateCacheProxy();
                if (cacheHelper.CacheRunning)
                {
                    PDFDataContract pdc = new ConfirmationOfApprovalCitrRenderer(fdc, cdc, btdc, coacdc).CreatePdf();
                    pdc.Title = coacdc.Title;
                    return pdc;
                }
            }
            return null;
        }

		public PDFDataContract RenderBookings(FileDataContract fdc, CorrectionBookingDataContract cbdc, string culture)
		{
			string title = null;
			switch (culture) { 
				case "nl-BE":
					title = "Bijkomende diverse boekingen";
					break;
				case "fr-FR":
					title = "Opérations diverses additionnelles";
					break;
				case "en-US":
					title = "Additional postings";
					break;
			}
			CacheHelper cacheHelper = new CacheHelper();
			if (cacheHelper.CacheServiceConfigured)
			{
				cacheHelper.CreateCacheProxy();
				if (cacheHelper.CacheRunning)
				{
					List<BookingDataContract> bookings = cacheHelper.GetCacheProxy().GetAllBookings(fdc.Id);
					PDFDataContract pdc = new BookingRenderer(bookings, cbdc).CreatePdf();
					pdc.Title = title ;
					//pdc.HeaderConfig.Enabled = true;
					//PDFDataContract pdc = new StatementRenderer(fdc, sdc, accounts).CreatePdf();
					return pdc;
				}
			}
			return null;
		}

		public PDFDataContract RenderBookingsSchema(FileDataContract fdc, CorrectionBookingSchemaDataContract cbsdc, string culture)
		{
			string title = null;
			switch (culture)
			{
				case "nl-BE":
					title = "Bijkomende diverse boekingen";
					break;
				case "fr-FR":
					title = "Opérations diverses additionnelles";
					break;
				case "en-US":
					title = "Additional postings";
					break;
			}
			CacheHelper cacheHelper = new CacheHelper();
			if (cacheHelper.CacheServiceConfigured)
			{
				cacheHelper.CreateCacheProxy();
				if (cacheHelper.CacheRunning)
				{
					//List<BookingDataContract> bookings = cacheHelper.GetCacheProxy().GetAllBookings(fdc.Id);
					eBook.API.BL.Read.AccountSchemaRepository a = new AccountSchemaRepository();
					CriteriaBookingLineDataContract cbldc = new CriteriaBookingLineDataContract{
						AccountNr = cbsdc.AccountNr,
						FileId = fdc.Id
					};
					List<BookingSingleDataContract> bookings = a.GetBookingLinesByAccountNr(cbldc);
					PDFDataContract pdc = new BookingSchemaRenderer(bookings, cbsdc).CreatePdf();
					pdc.Title = title;
					//pdc.HeaderConfig.Enabled = true;
					//PDFDataContract pdc = new StatementRenderer(fdc, sdc, accounts).CreatePdf();
					return pdc;
				}
			}
			return null;
		}



		private XElement GetBundleHeader( string culture, ClientDataContract client, FileDataContract file)
		{

			string assessment = "{0}{1}";
			switch (culture)
			{
				case "nl-BE":
					assessment = "Aanslagjaar {0}{1}";
					break;
				case "fr-FR":
					assessment = "Exercice d'imposition {0}{1}";
					break;
				case "en-US":
					assessment = "Tax year {0}{1}";
					break;
			}

			XElement element = new XElement("data",
				new XElement("block",
					new XAttribute("type", "TOPLEFT"),
					new XElement("phrase", " "),
					new XElement("phrase",
						new XAttribute("font", Config.StandardFont),
						new XAttribute("fontstyle", "bold"),
						new XAttribute("size", "10"),
						new XCData(string.Format("          {0}", client.Name))
						),
					new XElement("phrase",
						new XAttribute("font", Config.StandardFont),
						new XAttribute("fontstyle", "bold"),
						new XAttribute("size", "10"),
						new XCData(string.Format("          {0}", string.IsNullOrEmpty(client.EnterpriseNumber) ? "" : FormatVAT(client.EnterpriseNumber)))
						)
				),
				new XElement("block",
					new XAttribute("type", "TOPRIGHT"),
					new XElement("phrase", " "),
					new XElement("phrase",
						new XAttribute("font", Config.StandardFont),
						new XAttribute("size", "10"),
						new XCData(string.Format("{0} - {1}", file.StartDate.ToString("dd/MM/yyyy"), file.EndDate.ToString("dd/MM/yyyy")))
						),
					new XElement("phrase",
						new XAttribute("font", Config.StandardFont),
						new XAttribute("size", "10"),
						new XCData(string.Format(assessment, file.AssessmentYear, ""))
						)
				)
			);

			
				element.Add(new XElement("block",
					new XAttribute("type", "TOPTITLE"),
					new XElement("phrase",
						new XAttribute("font", Config.StandardFont),
						new XAttribute("size", "10"),
						new XAttribute("fontstyle", "bold underline"),
						new XCData("[VAR::TITLE]")
						)
					)
				);
		   

			return element;
		}

		public string GetAnnualAccountsBundle(CriteriaIdAndCultureDataContract cicdc)
		{
			FileDataContract fdc = new BL.Read.FileReadRepository().GetFile(cicdc.Id);
			ClientDataContract cdc = new BL.Read.ClientRepository().GetClient(fdc.ClientId);

			BundleDataContract bdc = new BundleDataContract
			{
				Culture = cicdc.Culture
				,
				Draft = false
				,
				FileId = cicdc.Id
				,
				Id = Guid.NewGuid()
				,
				HeaderFooterType = "ACR"
				,
				Name = "Annual Accounts"
				,
				RectoVerso = false
				,
				TemplateId = Guid.Empty
				,
				Contents = GetItems("statements", cicdc.Culture, cicdc.Id).Select(it => it.IndexItem).ToList()
			};
			bdc.Contents = GenerateBundleItems(bdc.Culture, fdc, cdc, bdc.Contents,"ACR");
			
			string path = System.IO.Path.Combine(Config.PickupLocal,string.Format("{0}.pdf",bdc.Id.ToString()));
			
			PDFDataContract pdc = new BundleRenderer(bdc, null, new FooterConfigDataContract { Enabled = false }, GetBundleHeader(bdc.Culture, cdc, fdc)).CreatePdf();

			if (System.IO.File.Exists(path)) System.IO.File.Delete(path);
			System.IO.File.Move(pdc.PDFLocation, path);

			string virtualPath = path.Replace(Config.PickupLocal, Config.PickupVirtualDir);

			return virtualPath.Replace(@"\", "/");
		}

		public string GenerateBiztaxFichePreview(CriteriaBiztaxFichePreviewDataContract cbfpdc)
		{
			

			

			BundleDataContract bdc = new BundleDataContract
			{
				Culture = cbfpdc.Culture,
				Deleted = false,
				Draft = false,
				FileId = cbfpdc.FileId,
				HeaderFooterType = "TAX",
				Id = Guid.NewGuid(),
				Name = "Biztax fiche preview",
				RectoVerso = false,
				TemplateId = new Guid("da5b2eed-d7a5-4a3e-bffb-5493cc8cd2d1"),
				Contents = new List<IndexItemBaseDataContract>
				{
					new BizTaxFicheDataContract
			{
				FooterConfig = new FooterConfigDataContract
					{
						Enabled = false,
						FootNote = null,
						ShowFooterNote = true,
						ShowPageNr = false,
						Style = null
					},
				HeaderConfig = new HeaderConfigDataContract
				{
					Enabled = true,
					ShowTitle = true,
					Style = null
				}
				,
				Id = ("BIZTAX_FICHE_" + cbfpdc.FicheId).DeterministicGuid()
				,
				ShowInIndex = true
				,
				Title = cbfpdc.FicheTitle
				,
				IconCls = "eBook-bundle-tree-biztax"
				,
				Fiche = cbfpdc.FicheId
			}
					
				}
			};

			
			return this.GenerateBundle(bdc, true);
		   
		}

		public string GenerateQuickprint(CriteriaQuickprintDataContract cqdc) {

			BundleDataContract bdc = new BundleDataContract
			{
				Culture = cqdc.Culture,
				Deleted = false,
				Draft = false,
				FileId = cqdc.FileId,
				HeaderFooterType = "ACR",
				Id = new Guid("5039b425-1f42-4798-8dce-f9f46adcc2a4"),
				Name = "Quickprint TEST",
				RectoVerso = false,
				TemplateId = new Guid("da5b2eed-d7a5-4a3e-bffb-5493cc8cd2d1"),
				Contents = new List<IndexItemBaseDataContract>()
			};            
			string returnValue = null;
			foreach (string option in cqdc.Options) {
				bool comparable = option.EndsWith("_v");
				IndexItemBaseDataContract defaultItem = null;
				if (option.StartsWith("jaarrekening_"))
				{
					defaultItem = new StatementsDataContract
					{
						EndsAt = 0,
						FooterConfig = new FooterConfigDataContract
						{
							Enabled = true,
							FootNote = null,
							ShowFooterNote = true,
							ShowPageNr = false,
							Style = null
						},
						HeaderConfig = new HeaderConfigDataContract
						{
							Enabled = true,
							ShowTitle = true,
							Style = null
						},
						NoDraft = false,
						StartsAt = 0,
						iTextTemplate = null,
						IconCls = "eBook-bundle-tree-statement",
						Id = Guid.NewGuid(),
						Locked = false,
						Title = "Activa",
						Culture = cqdc.Culture,
						Detailed = false,
						FileId = cqdc.FileId,
						Layout = "ACTIVA",
						Type = "STATEMENT",
						Comparable = comparable
					};
				}

				switch (option) { 
					case "leadsheets":
						//returnValue += option + "/";
						LeadsheetsDataContract ldc = new LeadsheetsDataContract
						{
							FileId = cqdc.FileId,
							Culture = cqdc.Culture
						};
					   
						bdc.Contents.Add(ldc);
						break;
					case "jaarrekening_activa_v":
					case "jaarrekening_activa":
						string titleA = "";
						switch (cqdc.Culture) { 
							case "nl-BE":
								titleA = "Activa";
								break;
							case "fr-FR":
								titleA = "Actif";
								break;
							case "en-US":
								titleA = "Assets";
								break;
						}
						StatementsDataContract sdc = (StatementsDataContract)defaultItem;
						sdc.Title = titleA;
						sdc.Layout = "ACTIVA";
						sdc.Detailed = true;
						bdc.Contents.Add(sdc);
					   
						break;
					case "jaarrekening_passiva_v":
					case "jaarrekening_passiva":
						string titleP = "";
						switch (cqdc.Culture) { 
							case "nl-BE":
								titleP = "Passiva";
								break;
							case "fr-FR":
								titleP = "Passif";
								break;
							case "en-US":
								titleP = "Liabilities";
								break;
						}
						StatementsDataContract sdcp = (StatementsDataContract)defaultItem;
						sdcp.Title = titleP;
						sdcp.Layout = "PASSIVA";
						sdcp.Detailed = true;
						bdc.Contents.Add(sdcp);

						break;

					case "jaarrekening_resultatenrekening_v":
					case "jaarrekening_resultatenrekening":
						string titleRR = "";
						switch (cqdc.Culture) { 
							case "nl-BE":
								titleRR = "Resultatenrekening";
								break;
							case "fr-FR":
                                titleRR = "Compte de résultat";
								break;
							case "en-US":
								titleRR = "Income statement";
								break;
						}
						StatementsDataContract sdcrr = (StatementsDataContract)defaultItem;
						sdcrr.Title = titleRR;
						sdcrr.Layout = "RESULTATENREKENING";
						//sdcrr.Detailed = false;
						sdcrr.Detailed = true; /* TICKET #58 */
						bdc.Contents.Add(sdcrr);
						
						break;

					case "jaarrekening_resultaatverwerking_v":
					case "jaarrekening_resultaatverwerking":
						string titleRV = "";
						switch (cqdc.Culture) { 
							case "nl-BE":
								titleRV = "Resultaatverwerking";
								break;
							case "fr-FR":
                                titleRV = "Affectation du résultat";
								break;
							case "en-US":
								titleRV = "Appropriation account";
								break;
						}
						StatementsDataContract sdcrv = (StatementsDataContract)defaultItem;
						sdcrv.Title = titleRV;
						sdcrv.Layout = "RESULTAATVERWERKING";
						sdcrv.Detailed = false;
						bdc.Contents.Add(sdcrv);
						
						break;


					case "correctieboekingen":
						CorrectionBookingDataContract bodc = new CorrectionBookingDataContract
						{
							culture = cqdc.Culture,
							Title="etestsetsetset",
							HeaderConfig = new HeaderConfigDataContract { 
								Enabled = true,
								ShowTitle = true
							}
						};
						bdc.Contents.Add(bodc);
						break;

					case "correctieboekingenSchema":
						CorrectionBookingSchemaDataContract bosdc = new CorrectionBookingSchemaDataContract
						{
							culture = cqdc.Culture,
							Title="etestsetsetset",
							HeaderConfig = new HeaderConfigDataContract { 
								Enabled = true,
								ShowTitle = true
							},
							AccountNr = cqdc.AccountNr,
							StartSaldo = cqdc.StartSaldo
						};
						bdc.Contents.Add(bosdc);
						break;

				}

			   

				
			}

			if (bdc.Contents.Count > 0)
			{
				returnValue = this.GenerateBundle(bdc, true);
			}

			return returnValue;
		}
	  


		protected override void RegisterTranslators()
		{
			
		}

		public string GenerateBiztaxTaxDetail(CriteriaIdAndCultureDataContract cidc)
		{

			string title = cidc.Culture == "nl-BE" ? "Biztax Belastingberekening" : cidc.Culture == "fr-FR" ? "BizTax - Calculation Tax" : "Biztax Tax Calculation";
						

			BundleDataContract bdc = new BundleDataContract
			{
				Culture = cidc.Culture,
				Deleted = false,
				Draft = false,
				FileId = cidc.Id,
				HeaderFooterType = "TAX",
				Id = Guid.NewGuid(),
				Name = "Biztax Tax Calculation",
				RectoVerso = false,
				TemplateId = new Guid("da5b2eed-d7a5-4a3e-bffb-5493cc8cd2d1"),
				Contents = new List<IndexItemBaseDataContract>
				{
					new BizTaxTaxCalcDataContract
							{
								FooterConfig = nofooter
									,
								HeaderConfig = noheader
									,
								Id = "BIZTAXCALC".DeterministicGuid()
									,
								ShowInIndex = true
									,
								Title = title
									,
								IconCls = "eBook-bundle-tree-biztax"

							}
					
				}
			};


			return this.GenerateBundle(bdc, true);
		}

        /// <summary>
        /// This will generate a page showing a corporate income tax return summary which requires a signature from the client.
        /// XSLT contains if blocks allowing the page to have dynamic content.
        /// </summary>
        /// <param name="cidc"></param>
        /// <returns></returns>
        public string GetConfirmationOfApprovalCITR(CriteriaIdAndDepartmentDataContract ciaddc)
        {
            bool bni = eBookReadManager.Context.Files.Include("Client").First(f => f.Id == ciaddc.Id).Client.BNI;
            string title;
            if (bni)
            {
                title = ciaddc.Culture == "nl-BE" ? "Akkoordverklaring aangifte in de belasting van niet-inwoners (vennootschappen)" : ciaddc.Culture == "fr-FR" ? "Déclaration d'accord - impôt des non-résidents (sociétés)" : "Confirmation of approval non-resident corporate income tax return";
            }
            else
            {
                title = ciaddc.Culture == "nl-BE" ? "Akkoordverklaring aangifte vennootschapsbelasting" : ciaddc.Culture == "fr-FR" ? "Déclaration d'accord impôt des sociétés" : "Confirmation of approval corporate income tax return";
            }
            
            BundleDataContract bdc = new BundleDataContract
            {
                Culture = ciaddc.Culture,
                Deleted = false,
                Draft = false,
                FileId = ciaddc.Id,
                HeaderFooterType = "TAX",
                Id = Guid.NewGuid(),
                Name = "Confirmation Of Approval CITR",
                RectoVerso = false,
                TemplateId = new Guid("da5b2eed-d7a5-4a3e-bffb-5493cc8cd2d1"),
                Contents = new List<IndexItemBaseDataContract>
				{
					new ConfirmationOfApprovalCitrDataContract
							{
								FooterConfig = nofooter
									,
								HeaderConfig = noheader
									,
								Id = "CONFIRMATIONOFAPPROVALCITR".DeterministicGuid()
									,
								ShowInIndex = false
									,
								Title = title
									,
								IconCls = "eBook-bundle-tree-biztax"
                                    ,
                                CVBA = ciaddc.Department
                                    ,
                                Culture = ciaddc.Culture
                                    ,
                                RuleApp = "VolmachtEnAkkoordverklaring"
                                    ,
                                PrintLayout = "default"
                                    ,
                                BNI = bni
							}	
				}
            };

            return this.GenerateBundle(bdc, true);
        }

        /// <summary>
        /// This will generate a page showing a NBB invoice summary using annual accounts and engagement code data.
        /// </summary>
        /// <param name="cidc"></param>
        /// <returns></returns>
        public string GetNbbInfoPage(string invoiceId, AnnualAccountDataContract aadc,EngagementCodeDataContract ecdc)
        {
            Client client = eBookReadManager.Context.Files.Include("Client").Where(f => f.Id == aadc.FileId).FirstOrDefault().Client;
            string title = "NBB Invoice";
            string[,] table = new string[,]
	        {
	            {"Entiteit:", ecdc.BU},
	            {"Engagement Code:", aadc.EngagementCode},
                {"Invoice ID:", invoiceId}, //date + structured message
                {"Client Name:", client.Name},
                {"Client ID:", client.GFISCode}
	        };
            
            BundleDataContract bdc = new BundleDataContract
            {
                Culture = "nl-BE",
                Deleted = false,
                Draft = false,
                FileId = aadc.FileId,
                HeaderFooterType = "TAX",
                Id = Guid.NewGuid(),
                Name = "InvoiceNBBCoverPage",
                RectoVerso = false,
                TemplateId = Guid.Empty,
                Contents = new List<IndexItemBaseDataContract>
				{
                   new TablePageDataContract
						{
							FooterConfig = nofooter
							,
							HeaderConfig = noheader
							,
							Id = "NBBINVOICE_COVERPAGE".DeterministicGuid()
							,
							ShowInIndex = true
							,
							Title = title
							,
							IconCls = "eBook-bundle-tree-cover"
                            ,
                            Table = table
						}
				}
            };

            return this.GenerateBundle(bdc, true);
        }
	}
}
