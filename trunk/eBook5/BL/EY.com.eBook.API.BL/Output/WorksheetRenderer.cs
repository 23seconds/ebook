﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Xml.Xsl;
using System.Globalization;
using System.IO;
using System.Xml;
using EY.com.eBook.RuleEngine.Data;
using EY.com.eBook.Core.EF;
using EY.com.eBook.API.BL.Write;
using System.Xml.XPath;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.Core;
using EY.com.eBook.Core.IText;
using EY.com.eBook.Core.IText.Render;
using EY.com.eBook.Caching;
using EY.com.eBook.API.Contracts.Data.Cache;
using EY.com.eBook.API.Contracts.Proxies;
using EY.com.eBook.API.Contracts;

namespace EY.com.eBook.API.BL
{
    public class WorksheetRenderer : ITextRenderer
    {
        private WorksheetSetDataBase _worksheetset;
        private Guid _typeId;
        private CultureInfo _cultureInfo;
        private string _xslPath;
        private string _ruleApp;
        private WorksheetItemDataContract _widc;
        private bool _render = true;
        public WorksheetTypeTranslationHelper _translationHelper;
        private FileDataContract _fdc;
        private ClientDataContract _cdc;


        public WorksheetRenderer(FileDataContract fdc, ClientDataContract cdc, WorksheetItemDataContract widc) 
            : this(fdc, cdc, widc, null, false)
        {
            
        }

        public WorksheetRenderer(FileDataContract fdc, ClientDataContract cdc, WorksheetItemDataContract widc, XElement source, bool biztaxdata)
        {
            _fdc = fdc;
            _cdc = cdc;
            if(string.IsNullOrEmpty(widc.PrintLayout)) widc.PrintLayout="default";
            string renderId = widc.FileId.ToString() + widc.WorksheetType.ToString() + widc.PrintLayout + widc.Culture;
            if (biztaxdata) renderId += "BIZTAXDATA";
            widc.Id = renderId.DeterministicGuid();

            
            _typeId = widc.WorksheetType;
            _ruleApp = widc.RuleApp;
            _widc = widc;
            _cultureInfo = CultureInfo.CreateSpecificCulture(widc.Culture);

            string path = Path.Combine(Config.RepositoryLocal, fdc.DataDirectory);
            path = Path.Combine(path, "Generation");
            if (!System.IO.Directory.Exists(path)) System.IO.Directory.CreateDirectory(path);

            path = Path.Combine(path, string.Format("{0}.pdf", widc.Id.ToString()));

            if (widc.LastChanged.HasValue && !biztaxdata)
            {
                
                if (System.IO.File.Exists(path))
                {
                    DateTime lw = System.IO.File.GetLastWriteTime(path);
                    if (lw >= widc.LastChanged.Value)
                    {
                        _render = false;
                    }
                }
            }

            if (_render)
            {

                _xslPath = Config.GetWorksheetTemplate(_ruleApp, fdc.AssessmentYear, widc.PrintLayout);
                GetPdfTemplate(source);
                Init(_widc.iTextTemplate, _widc);

            }
            else
            {
                _pdfDataContract.Delete = _widc.Delete;
                _pdfDataContract.HeaderConfig = _widc.HeaderConfig;
                _pdfDataContract.FooterConfig = _widc.FooterConfig;
                //pdc.Id = iibdc.Id != Guid.Empty ? iibdc.Id : Guid.NewGuid();
                _pdfDataContract.ShowInIndex = _widc.ShowInIndex;
                _pdfDataContract.Title = _widc.Title;
                _pdfDataContract.NoDraft = _widc.NoDraft;
            }
            _pdfDataContract.PDFLocation = path;
        }

       

        public void GetPdfTemplate(XElement source)
        {
            FileRepository fr = new FileRepository();
            if (source == null)
            {
                source = fr.GetWorksheetData(_widc.FileId, _typeId);
            }

            if (string.IsNullOrEmpty(_xslPath))
            {
                _widc.iTextTemplate = (new XElement("Root", new XElement("Paragraph", string.Format("XSLT - PRINTLAYOUT NOT FOUND, CONTACT SUPPORT WITH CLIENT AND EBOOK FILE NAME AS WELL AS 'WORKSHEET:{0} - LAYOUT:{1} - ASSESSMENTYEAR:{2}'", _widc.RuleApp, _widc.PrintLayout, _fdc.AssessmentYear.ToString())))).ToString();
            }
            else
            {
                XslCompiledTransform xslTransformer = new XslCompiledTransform(Config.DebugXslt());

                XsltSettings settings = new XsltSettings
                {
                    EnableDocumentFunction = true
                    ,
                    EnableScript = false
                };

                xslTransformer.Load(_xslPath, settings, null);



                XsltHelperClass xhc = new XsltHelperClass { Culture = _cultureInfo };

                if (_translationHelper == null)
                {
                    CacheProxy cp = new CacheProxy();
                    _translationHelper = new WorksheetTypeTranslationHelper
                    {
                        Translations = cp.GetWorksheetTypeTranslations(_widc.WorksheetType, _widc.Culture)
                    };
                    cp.Close();
                }

                XsltArgumentList arguments = new XsltArgumentList();
                arguments.AddExtensionObject("urn:Helper", xhc);
                arguments.AddExtensionObject("urn:Translations", _translationHelper);
                arguments.AddExtensionObject("urn:File", new DataContractReader<FileDataContract>(_fdc,_cultureInfo));
                arguments.AddExtensionObject("urn:Client", new ClientDataContractReader(_cdc, _cultureInfo));
                arguments.AddParam("Culture", "", _fdc.Culture);


                StringWriter stringWriter = new StringWriter();
                XmlWriterSettings wSettings = new XmlWriterSettings();
                wSettings.NewLineHandling = NewLineHandling.None;
                wSettings.NewLineOnAttributes = false;
                XmlWriter xmlTextWriter = XmlTextWriter.Create(stringWriter, wSettings);

                xslTransformer.Transform(source.CreateNavigator(), arguments, xmlTextWriter);

                _widc.iTextTemplate = stringWriter.GetStringBuilder().ToString();


                // CLEANUP
                xslTransformer = null;
                xhc = null;
                stringWriter.Close();
                stringWriter.Dispose();
                stringWriter = null;
                wSettings = null;
                xmlTextWriter = null;
            }

        }

        public override PDFDataContract CreatePdf()
        {
            if (_render)
            {
                return base.CreatePdf();
            }
            return _pdfDataContract;
        }
    }
}
