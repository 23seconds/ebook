﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.API.Contracts.Data.Cache;
using EY.com.eBook.API.Contracts.Data.IText;
using System.Xml.Linq;
using System.Configuration;
using EY.com.eBook.Core;
using EY.com.eBook.Caching;
using System.Xml.XPath;
using EY.com.eBook.API.Contracts.Proxies;
using EY.com.eBook.API.BL.Read;

namespace EY.com.eBook.API.BL
{
    public class WorksheetTypeTranslationHelper
    {
        private string defaultOutput = "print";

        public List<WorksheetTypeTranslationDataContract> Translations { get; set; }

        public string GetTranslation(string collection, string field, string defaultName)
        {
            return GetTranslation(collection, field, defaultOutput, defaultName);
        }

        public string GetTranslation(string collection, string field, string outputType, string defaultName)
        {
            if (defaultOutput.ToLower() != "print")
            {
                defaultName = defaultName.Replace("'", @"\'");
            }
            if (Translations == null) return defaultName;
            if (Translations.Count == 0) return defaultName;

            WorksheetTypeTranslationDataContract wTrans = Translations.FirstOrDefault(wtt => wtt.EntityOrCollection.ToLower() == collection.ToLower()
                                                                            && wtt.FieldName.ToLower() == field.ToLower()
                                                                            && wtt.OutputType.ToLower() == outputType.ToLower());
            if (wTrans == null)
            {
                //LogMissingTranslation(collection, field, outputType);
                if (defaultOutput != "label")
                {
                    wTrans = Translations.FirstOrDefault(wtt => wtt.EntityOrCollection.ToLower() == collection.ToLower()
                                                                            && wtt.FieldName.ToLower() == field.ToLower()
                                                                            && wtt.OutputType.ToLower() == "label");
                    if (wTrans != null)
                    {
                        if (defaultOutput.ToLower() != "print")
                        {
                            return wTrans.Translation.Replace("'", @"\'");
                        }
                        return wTrans.Translation.Replace(@"\'", @"'");

                    }
                    
                }

                return defaultName;
            }
            if (string.IsNullOrEmpty(wTrans.Translation))
            {
                return defaultName;
            }

            if (defaultOutput.ToLower() != "print")
            {
                return wTrans.Translation.Replace("'", @"\'");
            }
            return wTrans.Translation.Replace(@"\'", @"'");

        }

        public XPathNavigator GetMappings(string key, string meta)
        {
            CacheProxy cp = new CacheProxy();
            cp.Open();
            List<MappingItemDataContract> mappings = cp.GetMappingItemsOfKeyMeta(key, meta);
            cp.Close();
            XElement xe = CoreHelper.SerializeToXElement(mappings, true);
            return xe.CreateNavigator();
        }

        public XPathNavigator GetMappingCoeffs(string mappingId)
        {
            if (string.IsNullOrEmpty(mappingId)) return null;
            if (!mappingId.IsGuid()) return null;
            Guid id = new Guid(mappingId);
            CacheProxy cp = new CacheProxy();
            cp.Open();
            List<CoefficientDataContract> coefs = cp.GetMappingCoefficients(id);
            cp.Close();
            XElement xe = CoreHelper.SerializeToXElement(coefs, true);
            return xe.CreateNavigator();
        }
    }
}

