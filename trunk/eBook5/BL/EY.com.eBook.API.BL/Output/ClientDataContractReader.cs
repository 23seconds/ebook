﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EY.com.eBook.Core;
using EY.com.eBook.API.Contracts.Data;
using System.Globalization;
using EY.com.eBook.API.Contracts;

namespace EY.com.eBook.API.BL
{
    public class ClientDataContractReader : DataContractReader<ClientDataContract>
    {
        public ClientDataContractReader(ClientDataContract cdc, CultureInfo culture) : base(cdc, culture) { }

        private string GetSupplier(BusinessRelationDataContract sdc, string property, string format)
        {
            DataContractReader<BusinessRelationDataContract> reader = new DataContractReader<BusinessRelationDataContract>(sdc, Culture);
            return reader.GetValue(property, format);
        }

        public string GetSupplier(string id, string property, string format)
        {
            try
            {
                if (string.IsNullOrEmpty(id)) return string.Empty;
                if (!id.IsGuid()) return id;
                EY.com.eBook.API.BL.Read.BusinessRelationRepository br = new EY.com.eBook.API.BL.Read.BusinessRelationRepository();

                BusinessRelationDataContract sdc = br.GetBusinessRelation(new Guid(id));
                if (sdc == null) return string.Empty; //not found
                return GetSupplier(sdc, property, format);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message + ":: " + id);
                return string.Empty;
            }
        }

        private string GetCustomer(BusinessRelationDataContract cdc, string property, string format)
        {
            DataContractReader<BusinessRelationDataContract> reader = new DataContractReader<BusinessRelationDataContract>(cdc, Culture);
            return reader.GetValue(property, format);
        }

        public string GetCustomer(string id, string property, string format)
        {
            if (string.IsNullOrEmpty(id)) return string.Empty;
            EY.com.eBook.API.BL.Read.BusinessRelationRepository br = new EY.com.eBook.API.BL.Read.BusinessRelationRepository();

            BusinessRelationDataContract cdc = br.GetBusinessRelation(new Guid(id));
            if (cdc == null) return string.Empty; //not found
            return GetCustomer(cdc, property, format);
        }

        public string GetBusinessRelation(string id, string property, string format)
        {
            try
            {
                if (!id.IsGuid()) return string.Empty;
                return GetSupplier(id, property, format);
            }
            catch
            {
                return string.Empty;
            }
        }
    }
}
