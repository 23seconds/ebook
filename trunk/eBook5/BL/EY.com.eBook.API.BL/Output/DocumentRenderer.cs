﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Xml.Xsl;
using System.Globalization;
using System.IO;
using System.Xml;
using EY.com.eBook.RuleEngine.Data;
using EY.com.eBook.Core.EF;
using EY.com.eBook.API.BL.Read;
using System.Xml.XPath;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.Core;
using EY.com.eBook.Core.IText;
using EY.com.eBook.Core.IText.Render;
using EY.com.eBook.Caching;
using EY.com.eBook.API.Contracts.Data.Cache;
using EY.com.eBook.API.Contracts.Proxies;
using EY.com.eBook.API.Contracts;
using EY.com.eBook.API.Contracts.Data.Meta;
using EY.com.eBook.API.BL.Xslt;

namespace EY.com.eBook.API.BL
{
    public class DocumentRenderer : ITextRenderer
    {
        
        private CultureInfo _cultureInfo;
        
        private bool _render = true;
        private DocumentDataContract _ddc;
        private DocumentItemDataContract _didc;
        private FileDataContract _fdc;
        private ClientDataContract _cdc;

        public DocumentRenderer(FileDataContract fdc, ClientDataContract cdc, DocumentItemDataContract didc)
        {
            _fdc = fdc;
            _cdc = cdc;
            _didc = didc;
            if (_didc.DocumentId == Guid.Empty)
            {
                EY.com.eBook.Core.EF.Write.Document doc= eBookWriteManager.Context.Documents.FirstOrDefault(d => d.FileId == fdc.Id && d.Culture == didc.Culture && d.TypeId == didc.DocumentTypeId);
                if (doc != null) _didc.DocumentId = doc.Id;
            } 

            if (didc.DocumentId == Guid.Empty)
            {
                if (eBookWriteManager.Context.Documents.Count(d => d.TypeId == didc.DocumentTypeId && d.FileId==fdc.Id && d.Culture==didc.Culture) != 0)
                {
                    _ddc = new DocumentRepository().GetDocument(new CriteriaIdAndCultureDataContract { Id = didc.DocumentTypeId, Culture = didc.Culture });
                }
            }
            else
            {
                if (eBookWriteManager.Context.Documents.Count(d => d.Id == didc.DocumentId) != 0)
                {

                    _ddc = new DocumentRepository().GetDocument(new CriteriaIdAndCultureDataContract { Id = didc.DocumentId, Culture = didc.Culture });
                }
            }
            

            _didc.iTextTemplate = string.Format("<phrase>YOUR FILE CONTAINS NO EBOOK DOCUMENT '{0}'</phrase>",_didc.Title);
            if (_ddc != null)
            {
                EY.com.eBook.Core.EF.Read.DocumentType dt = eBookReadManager.Context.DocumentTypes
                                .Include("Names")
                                .Include("Templates")
                                .Where(dty => dty.Id == _ddc.DocumentTypeId).FirstOrDefault();
                if (dt != null)
                {
                    EY.com.eBook.Core.EF.Read.DocumentTypeTemplate dtt = dt.Templates.Where(t => t.Culture == didc.Culture).FirstOrDefault();
                    if (dtt == null)
                    {
                        _didc.iTextTemplate = "<phrase>DOCUMENT NOT FOUND</phrase>";

                    }
                    else
                    {
                        string tpl = dtt.Template;

                        FileMetaDataContract fileMeta = new FileReadRepository().GetMeta(_fdc.Id);

                        XslCompiledTransform xct = new XslCompiledTransform(Config.DebugXslt());
                        xct.Load(typeof(DocumentITextRenderer));
                        //xct.Load(@"C:\Projects_TFS\EY\Ebook\Main-v3.5\Xslt\DocumentItextRender.xslt");

                        DocumentHelper dh = new DocumentHelper
                        {
                            Client = cdc
                            ,
                            File = fdc
                            ,
                            FileMeta = fileMeta
                            ,
                            Culture = CultureInfo.CreateSpecificCulture(didc.Culture)
                            ,
                            Document = _ddc
                        };
                        XsltArgumentList arguments = new XsltArgumentList();
                        arguments.AddExtensionObject("urn:DocumentHelper", dh);

                        StringBuilder sb = new StringBuilder();
                        XPathDocument source = new XPathDocument(new System.IO.StringReader(tpl));

                        XmlWriterSettings settings = new XmlWriterSettings();
                        settings.Encoding = Encoding.ASCII;
                        settings.Indent = true;
                        settings.OmitXmlDeclaration = true;
                        XmlWriter xwr = XmlWriter.Create(sb, settings);

                        xct.Transform(source, arguments, xwr);
                        xwr.Close();
                        _didc.iTextTemplate = sb.ToString();
                        xct = null;
                        xwr = null;
                        arguments = null;
                        fileMeta = null;
                    }
                }
            }
            Init(_didc.iTextTemplate, _didc);
        }

    }
}
