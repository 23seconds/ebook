﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.API.Contracts.Data.Cache;
using System.Xml;
using System.Xml.Xsl;
using System.Xml.XPath;
using System.IO;
using EY.com.eBook.Core.IText.Render;
using EY.com.eBook.Core;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace EY.com.eBook.API.BL
{
    public class BizTaxBundleRenderer : ITextRenderer
    {
        private BizTaxBundleDataContract _cpdc;
        private BizTaxDataContract _btdc;
        private FileDataContract _fdc;
        private ClientDataContract _cdc;
        private List<IndexItemBaseDataContract> _binaryFields;
        private string _xslPath;

        public BizTaxBundleRenderer(BizTaxBundleDataContract cpdc, FileDataContract fdc, ClientDataContract cdc, BizTaxDataContract btdc, List<IndexItemBaseDataContract> binaryFields)
            : base(null,cpdc)
        {
            this.utiliseChapter = false; 
            _cpdc = cpdc;
            _btdc = btdc;
            _fdc = fdc;
            _cdc = cdc;
            _binaryFields = binaryFields;
            _pdfDataContract.Delete = true;
            _pdfDataContract.ShowInIndex = false;
           // string _xslFle = _cdc.BNI ? "BNI-" : "";

            
            if (_cdc.BNI)
            {
                _xslPath = System.IO.Path.Combine(Config.TemplatesRoot, "BizTax-" + fdc.AssessmentYear + "-BNI-print-" + fdc.Culture + ".xslt");
            }
            else
            {
                _xslPath = System.IO.Path.Combine(Config.TemplatesRoot, "BizTax-" + fdc.AssessmentYear + "-print-" + fdc.Culture + ".xslt");
            }
           // System.Diagnostics.EventLog.WriteEntry("eBook", _xslPath, System.Diagnostics.EventLogEntryType.Warning);
           // _xslPath = null;
            GetPdfTemplate();
            Init(_cpdc.iTextTemplate, _cpdc);
        }


        public override void HandleDocument()
        {

            PdfPTable table = new PdfPTable(1);
            table.SetTotalWidth(new float[]
                                 {
                                     _document.PageSize.Width - _document.LeftMargin - _document.BottomMargin
                                 });
            table.LockedWidth = true;

            Font font = new Font(bf, 14, Font.BOLD);
            Chunk ph = new Chunk(_cpdc.Title, font);
            //ph.SetLocalDestination("PG_" + writer.CurrentPageNumber);


            PdfPCell cell = new PdfPCell(new Phrase(ph));

            cell.FixedHeight = _document.PageSize.Height - _document.TopMargin - _document.BottomMargin;
            cell.Padding = 0;
            cell.PaddingBottom = 160;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.Border = 0;
            //cell.Width = table.TotalWidth;
            //  cell.Height = table.TotalHeight;


            table.AddCell(cell);

            _document.Add(table);
            
            _document.NewPage();

            base.HandleDocument();
        }
       

        public void GetPdfTemplate()
        {
            
            


            if (string.IsNullOrEmpty(_xslPath))
            {
                _cpdc.iTextTemplate = (new XElement("Root", new XElement("Paragraph", string.Format("XSLT - PRINTLAYOUT NOT FOUND, CONTACT SUPPORT WITH CLIENT AND EBOOK FILE NAME ", _fdc.AssessmentYear.ToString())))).ToString();
            }
            else
            {
                
                if (_btdc == null)
                {
                    _cpdc.iTextTemplate = (new XElement("Root", new XElement("Paragraph", string.Format("THIS FILE HAS NO BIZTAX DECLARATION PREPARED AT THIS POINT IN TIME. ", _fdc.AssessmentYear.ToString())))).ToString();
                }
                else
                {
                    try {
                    XslCompiledTransform xslTransformer = new XslCompiledTransform(Config.DebugXslt());

                    XsltSettings settings = new XsltSettings
                    {
                        EnableDocumentFunction = true
                        ,
                        EnableScript = false
                    };

                    xslTransformer.Load(_xslPath, settings, null);

                    XElement source = CoreHelper.SerializeToXElement(_btdc, true);

                    XsltHelperClass xhc = new XsltHelperClass { Culture = System.Globalization.CultureInfo.CreateSpecificCulture(_fdc.Culture) };



                    XsltArgumentList arguments = new XsltArgumentList();
                    
                    arguments.AddExtensionObject("urn:Helper", xhc);
                    /*
                    arguments.AddExtensionObject("urn:Translations", _translationHelper);
                    arguments.AddExtensionObject("urn:File", new DataContractReader<FileDataContract>(_fdc, _cultureInfo));
                    arguments.AddExtensionObject("urn:Client", new ClientDataContractReader(_cdc, _cultureInfo));
                     * */
                    arguments.AddParam("Culture", "", _fdc.Culture);
                    arguments.AddParam("Binaries", "", CoreHelper.SerializeToXElement(_binaryFields,true).CreateNavigator());


                    StringWriter stringWriter = new StringWriter();
                    XmlWriterSettings wSettings = new XmlWriterSettings();
                    wSettings.NewLineHandling = NewLineHandling.None;
                    wSettings.NewLineOnAttributes = false;
                    XmlWriter xmlTextWriter = XmlTextWriter.Create(stringWriter, wSettings);

                    xslTransformer.Transform(source.CreateNavigator(), arguments, xmlTextWriter);

                    _cpdc.iTextTemplate = stringWriter.GetStringBuilder().ToString();

                    //XElement.Parse(_cpdc.iTextTemplate).Save(@"c:\test_bniprint.xml");


                    // CLEANUP
                    xslTransformer = null;
                    stringWriter.Close();
                    stringWriter.Dispose();
                    stringWriter = null;
                    wSettings = null;
                    xmlTextWriter = null;
                    }
                    catch (Exception e)
                    {
                        System.Diagnostics.EventLog.WriteEntry("eBook", e.Message, System.Diagnostics.EventLogEntryType.Error);

                        }
                }
            }

        }
    }
}
