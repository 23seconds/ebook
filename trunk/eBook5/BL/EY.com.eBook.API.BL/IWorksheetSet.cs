﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EY.com.eBook.RuleEngine.Data;
using EY.com.eBook.Caching;

namespace EY.com.eBook.RuleEngine
{
    public interface IWorksheetSet
    {
        int AssessmentYear { get;}
        WorksheetSetDataBase DataContainer { get; set; }
         decimal? GetTaxCalculation();
         void Initialize(WorksheetLibrary wl);
         void Save();
         void Calculate(string appName);
         void ForceRecalculation(string appName);
         void ImportPrevious();
    }
}