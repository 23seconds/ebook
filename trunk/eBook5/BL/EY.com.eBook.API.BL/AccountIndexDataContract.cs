﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EY.com.eBook.API.Contracts.Data.Cache;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts
{
    [DataContract]
    public class AccountIndexDataContract
    {
        [DataMember]
        public string Key { get; set; }

        [DataMember]
        public List<string> Values { get; set; }
    }
}
