﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.Core;
using System.IO;
using System.Runtime.Serialization.Json;

namespace EY.com.eBook.API.BL
{
    public class BusinessHelper
    {
        public ActivePersonDataContract DecodePerson(string base64Json)
        {
            string decodedJson = CoreHelper.DecodeFromBase64(base64Json);
            ActivePersonDataContract activePerson = new ActivePersonDataContract();
            MemoryStream ms = new MemoryStream(Encoding.Unicode.GetBytes(decodedJson));
            DataContractJsonSerializer serializer = new DataContractJsonSerializer(activePerson.GetType());
            activePerson = serializer.ReadObject(ms) as ActivePersonDataContract;
            ms.Close();
            return activePerson;
        }

        

    }
}
