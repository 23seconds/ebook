﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using EY.com.eBook.API.Contracts.Data.Cache;
using EY.com.eBook.API.Contracts;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.Core;

namespace EY.com.eBook.Caching
{
    [DataContract]
    public class WorksheetWorkSetDataContract
    {
        
        [DataMember(Order=1)]
        public FileDataContract DataContract { get; set; }

        [DataMember(Order = 2)]
        public AccountSchemaWorkSet Schema { get; set; }

        [DataMember(Order = 3)]
        public CoefficientsWorkingCopy Coeffs { get; set; }

        [DataMember(Order = 4)]
        public WorksheetLibrary Worksheets { get; set; }

        [DataMember(Order = 5)]
        public List<BookingDataContract> AutoBookings { get; set; }

        [DataMember(Order = 6)]
        public List<FileMessageDataContract> Messages { get; set; }

        [DataMember(Order = 7)]
        public FileSettingsDataContract Settings { get; set; }

        [DataMember(Order = 8)]
        public ClientBaseDataContract Client { get; set; }

        public WorksheetWorkSetDataContract Copy()
        {
            string copy = CoreHelper.SerializeToString(this);
            return CoreHelper.DeserializeFromString<WorksheetWorkSetDataContract>(copy);
        } 
        
    }
}
