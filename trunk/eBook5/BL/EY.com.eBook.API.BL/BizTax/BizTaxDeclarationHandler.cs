﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EY.com.eBook.API.BL.Write;
using EY.com.eBook.Core;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.Core.EF.Write;
using EY.com.eBook.Core.EF;
using System.Data.Objects;
using System.ServiceModel;
using EY.com.eBook.Core.Data;

namespace EY.com.eBook.API.BL
{
    /* INFO
     * STATUS: 
     *      A status of a biztax declaration is a numeric representation 
     *      consisting of a decade (tiental) and a unit (eenheid) (10,15,19,20,25,29,30,35,39,99,-99)
     *      
     *      The decade states the step:
     *          1 = Upload
     *          2 = Validation
     *          3 = Finalization
     *          9 = Successfull declaration
     *          -9 = Failed declaration (send back to user)
     *          
     *      The unit states the substatus of that step.
     *          0 = Step is todo
     *          5 = Item is currently on hold
     *          9 = Item is in progress (by), except for decades +/-9 (= final feedback)
     *          
     *      99 = Successfull declaration
     *      -99 = failed declaration
     *          
     */

    public class BizTaxDeclarationHandler : RepositoryBase
    {
     
        public BizTaxPersonDataContract Login(CriteriaWinAccountDataContract cwadc)
        {
            Person person = eBookWriteManager.Context.PersonSet.FirstOrDefault(p => p.WindowsAccount.Replace(@"\", "").ToUpper() == cwadc.WindowsAccount.Replace(@"\", "").ToUpper());
            
            if (person == null)
            {
                // first logon, lookup email
                string mail = new ActiveDirectoryRepository().FindEmail(cwadc.WindowsAccount);
                if (!string.IsNullOrEmpty(mail))
                {
                    // import person, get from cdb by email and log windows account for future use
                    EY.com.eBook.Core.EF.Write.Person wperson = eBookWriteManager.Context.PersonSet
                                                                   .FirstOrDefault(p => p.Email.ToUpper() == mail.ToUpper());
                    if (wperson != null)
                    {
                        wperson.WindowsAccount = cwadc.WindowsAccount;
                        eBookWriteManager.Context.SaveChanges();
                        eBookReadManager.Context.Refresh(RefreshMode.StoreWins, eBookReadManager.Context.PersonSet);
                        person = wperson;

                    }
                    else
                    {
                        throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { Message = String.Format("Contact your local eBook champion: User [{0}] email address is incorrect/missing in either GFIS or Active directory.'", cwadc.WindowsAccount) });
                    }
                }
                else
                {
                    throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { Message = String.Format("Contact E&Y helpdesk with following information: 'user {0} has no email address in Active Directory.'", cwadc.WindowsAccount) });
                    //throw error
                }
            }

            BizTaxDeclarer declarer = eBookWriteManager.Context.BizTaxDeclarers.FirstOrDefault(b => b.PersonId == person.Id);
           /* if (declarer != null)
            {
            }*/

            BizTaxPersonDataContract pdc = new BizTaxPersonDataContract {
                Champions=null
                , DeclaringDepartment= declarer!=null ? declarer.Department : person.Department
                , ReadOnly = declarer==null
                , DeclaringPartnerId = declarer!=null ? declarer.PartnerId : Guid.Empty
                ,
                Department = person.Department
                ,
                Email = person.Email
                ,
                FirstName = person.FirstName
                ,
                Id = person.Id
                , IsChampion = false
                , IsSysAdmin = false
                , LastLogon = DateTime.Now
                ,
                LastName = person.LastName
                , Offices= null
                , Roles = null
                ,
                WindowsAccount = person.WindowsAccount
            };
                
            return pdc;

        }

        public List<BizTaxDeclarationDataContract> GetDeclarationsByClient(CriteriaBaseIdDataContract cbidc)
        { 
            return (from b in eBookWriteManager.Context.BizTaxDeclarationSet
                    where (b.ClientId == cbidc.Id)
                    orderby b.LastChanged
                    select new BizTaxDeclarationDataContract
                    {
                        Id = b.Id
                        ,
                        AssessmentYear = b.AssessmentYear
                        ,
                        BizTaxTaxCalculation = b.BiztaxCalc
                        ,
                        ClientEnterpriseNumber = b.ClientEnterprise
                        ,
                        ClientName = b.ClientName
                        ,
                        CurrentPath = b.CurrentPath
                        ,
                        Department = b.Department
                        ,
                        EBookTaxCalculation = b.eBookCalc
                        ,
                        Errors = b.Errors
                        ,
                        InProgressBy = b.InProgressBy
                        ,
                        PartnerId = b.PartnerId
                        ,
                        PartnerName = b.PartnerName
                        ,
                        Proxy = b.Proxy
                        ,
                        SendToDeclareBy = b.SenderName
                        ,
                        SendToDeclareDate = b.SenderDate
                        ,
                        Status = b.Status
                        ,
                        Type = b.FileType
                    }).ToList();                    
        }

        public List<BizTaxDeclarationDataContract> GetDeclarations(CriteriaDepartmentPartnerMainstateDataContract cdpmdc)
        {
            cdpmdc.ActivePerson = new BusinessHelper().DecodePerson(cdpmdc.EncodedActivePerson);
            //BizTaxStatuses bs = GetStatuses(cdpmdc.LoadType);
            int mainState = cdpmdc.MainStatus * 10;
            return (from b in eBookWriteManager.Context.BizTaxDeclarationSet
                    where (
                            (
                                b.Status >= mainState           // all in main status x (=decade)
                                && b.Status < (mainState+10)
                            )
                            && b.Department == cdpmdc.Department
                            && b.PartnerId == cdpmdc.Partner
                    )
                    orderby b.LastChanged
                    select new BizTaxDeclarationDataContract
                    {
                        Id = b.Id
                        ,
                        AssessmentYear = b.AssessmentYear
                        ,
                        BizTaxTaxCalculation = b.BiztaxCalc
                        ,
                        ClientEnterpriseNumber = b.ClientEnterprise
                        ,
                        ClientName = b.ClientName
                        ,
                        CurrentPath = b.CurrentPath
                        ,
                        Department = b.Department
                        ,
                        EBookTaxCalculation = b.eBookCalc
                        ,
                        Errors = b.Errors
                        ,
                        InProgressBy = b.InProgressBy
                        ,
                        PartnerId = b.PartnerId
                        ,
                        PartnerName = b.PartnerName
                        ,
                        Proxy = b.Proxy
                        ,
                        SendToDeclareBy = b.SenderName
                        ,
                        SendToDeclareDate = b.SenderDate
                        ,
                        Status = b.Status
                        ,
                        Type = b.FileType
                    }).ToList();
                        
        }

        public void ChangeStatus(List<CriteriaIdStatusContract> lcisdc)
        {
            BizTaxDeclaration bd = new BizTaxDeclaration();
            FileXbrl fx = new FileXbrl();
            foreach (CriteriaIdStatusContract cisdc in lcisdc)
            {
                bd = eBookWriteManager.Context.BizTaxDeclarationSet.FirstOrDefault(d => d.Id == cisdc.Id);
                bd.Status = cisdc.Status;
                fx = eBookWriteManager.Context.FileXbrlSet.FirstOrDefault(f => f.FileId == cisdc.Id);
                fx.StatusId = cisdc.Status;
                eBookWriteManager.Context.SaveChanges();
            }
        }




        public BizTaxDeclarationDataContract GetFirstAvailable(CriteriaDepartmentPartnerMainstateDataContract cdpmdc)
        {
            cdpmdc.ActivePerson = new BusinessHelper().DecodePerson(cdpmdc.EncodedActivePerson);
            int mainState = cdpmdc.MainStatus * 10;

           

            BizTaxDeclaration bzt = eBookWriteManager.Context.BizTaxDeclarationSet
                                    .Where(b =>
                                            (b.Status == mainState  // In todo state (not yet in progress)
                                                || (b.Status > mainState  // or in progress or on hold by current person
                                                    && b.Status < (mainState + 10)
                                                    && b.InProgressBy == cdpmdc.ActivePerson.Name)
                                            )
                                            && b.Department == cdpmdc.Department    // within person granted department
                                            && b.PartnerId == cdpmdc.Partner        // within person granted department partner 
                                    )
                                    .OrderBy(b => b.LastChanged)
                                    .FirstOrDefault();
            if (bzt != null)
            {
                bzt.Status = mainState+9;  // 9 = in progress
                bzt.InProgressBy = cdpmdc.ActivePerson.Name;
                bzt.LastChanged = DateTime.Now;
                bzt.CurrentPath = MoveDir(bzt.CurrentPath, bzt.Status);
                eBookWriteManager.Context.SaveChanges();
                return new BizTaxDeclarationDataContract
                    {
                        Id = bzt.Id
                        ,
                        AssessmentYear = bzt.AssessmentYear
                        ,
                        BizTaxTaxCalculation = bzt.BiztaxCalc
                        ,
                        ClientEnterpriseNumber = bzt.ClientEnterprise
                        ,
                        ClientName = bzt.ClientName
                        ,
                        CurrentPath = bzt.CurrentPath
                        ,
                        Department = bzt.Department
                        ,
                        EBookTaxCalculation = bzt.eBookCalc
                        ,
                        Errors = bzt.Errors
                        ,
                        InProgressBy = bzt.InProgressBy
                        ,
                        PartnerId = bzt.PartnerId
                        ,
                        PartnerName = bzt.PartnerName
                        ,
                        Proxy = bzt.Proxy
                        ,
                        SendToDeclareBy = bzt.SenderName
                        ,
                        SendToDeclareDate = bzt.SenderDate
                        ,
                        Status = bzt.Status
                        ,
                        Type = bzt.FileType
                    };
            }
            return null;
            
        }

        /*
        public BizTaxDeclarationDataContract GetSpecific(CriteriaIdLoadTypeDataContract cildc)
        {
            

            BizTaxDeclaration bzt = eBookWriteManager.Context.BizTaxDeclarationSet.FirstOrDefault(b => (b.Status == bs.Open)
                            && b.Id==cildc.Id);
            if (bzt != null)
            {
                bzt.Status = bs.Open + 9;
                bzt.InProgressBy = cildc.ActivePerson.Name;
                eBookWriteManager.Context.SaveChanges();
                return new BizTaxDeclarationDataContract
                {
                    Id = bzt.Id
                    ,
                    AssessmentYear = bzt.AssessmentYear
                    ,
                    BizTaxTaxCalculation = bzt.BiztaxCalc
                    ,
                    ClientEnterpriseNumber = bzt.ClientEnterprise
                    ,
                    ClientName = bzt.ClientName
                    ,
                    CurrentPath = bzt.CurrentPath
                    ,
                    Department = bzt.Department
                    ,
                    EBookTaxCalculation = bzt.eBookCalc
                    ,
                    Errors = bzt.Errors
                    ,
                    InProgressBy = bzt.InProgressBy
                    ,
                    PartnerId = bzt.PartnerId
                    ,
                    PartnerName = bzt.PartnerName
                    ,
                    Proxy = bzt.Proxy
                    ,
                    SendToDeclareBy = bzt.SenderName
                    ,
                    SendToDeclareDate = bzt.SenderDate
                    ,
                    Status = bzt.Status
                    ,
                    Type = bzt.FileType
                };
            }
            return null;

        }

        */


        private void LogAction(CriteriaIdKeyDataContract cikdc,int severity)
        {
            if(cikdc.ActivePerson==null) {
                cikdc.ActivePerson = new BusinessHelper().DecodePerson(cikdc.EncodedActivePerson);
            }
            FileXbrlHistory fxh = new FileXbrlHistory
            {
                ActionId = severity
                ,
                ActionText = GetKeyedMessage(cikdc.Key)
                ,
                Date = DateTime.Now
                ,
                FileId = cikdc.Id
                ,
                PersonFullName = cikdc.ActivePerson.Name
                ,
                PersonId = cikdc.ActivePerson.Id
                ,
                XbrlType = "BIZTAX"
            };
        }

        private string GetKeyedMessage(string key)
        {
            // TO MOVE TO DB or CACHE WITH TRANSLATIONS
            return key;
        }

        protected override void RegisterTranslators()
        {
            
        }

        public void UpdateStatus(CriteriaIdStatusContract cisdc)
        {
            cisdc.ActivePerson = new BusinessHelper().DecodePerson(cisdc.EncodedActivePerson);
            BizTaxDeclaration bzt = eBookWriteManager.Context.BizTaxDeclarationSet.FirstOrDefault(b => b.Id == cisdc.Id);
            if (bzt != null)
            {
                bzt.Status = cisdc.Status;
                bzt.InProgressBy = null;
                bzt.LastChanged = DateTime.Now;
                bzt.CurrentPath = MoveDir(bzt.CurrentPath, cisdc.Status);
                eBookWriteManager.Context.SaveChanges();
            }
            LogAction(new CriteriaIdKeyDataContract { Id = cisdc.Id, ActivePerson = cisdc.ActivePerson, Key = "STATECHANGE_" + cisdc.Status }, 1);

            if (bzt.Status == 99)
            {
                // import resulting files in repository
                foreach (FileService serv in eBookWriteManager.Context.FileServices.Where(fs => fs.FileId == cisdc.Id && !fs.Completed.HasValue).ToList())
                {
                    serv.Completed = DateTime.Now;
                    serv.CompletedBy = bzt.SenderName;
                    eBookWriteManager.Context.SaveChanges();
                }
                File file = eBookWriteManager.Context.Files.FirstOrDefault(f => f.Id == cisdc.Id);
                if (file != null)
                {
                    file.NumbersLocked = true;
                    file.CloseDate = DateTime.Now;
                    file.ClosedBy = bzt.SenderName;
                    eBookWriteManager.Context.SaveChanges();
                }
            }
        }

        public void RegisterCalculation(CriteriaIdSimulationDataContract cisdc)
        {
            cisdc.ActivePerson = new BusinessHelper().DecodePerson(cisdc.EncodedActivePerson);
            BizTaxDeclaration bzt = eBookWriteManager.Context.BizTaxDeclarationSet.FirstOrDefault(b => b.Id == cisdc.Id);
            if (bzt != null)
            {
                bzt.BiztaxCalc = cisdc.Simulation;
                bzt.LastChanged = DateTime.Now;                
                eBookWriteManager.Context.SaveChanges();
            }
        }

        public void RegisterSuccess(CriteriaIdKeyDataContract cikdc)
        {
            LogAction(cikdc,1);
        }

        public void RegisterFailure(CriteriaIdKeyDataContract cikdc)
        {
            LogAction(cikdc,-99);
            BizTaxDeclaration bzt = eBookWriteManager.Context.BizTaxDeclarationSet.FirstOrDefault(b => b.Id == cikdc.Id);
            if (bzt != null)
            {
                bzt.Status = -99;
                bzt.InProgressBy = null;
                bzt.LastChanged = DateTime.Now;
                bzt.CurrentPath = MoveDir(bzt.CurrentPath,-99);
                if (string.IsNullOrEmpty(cikdc.Errors)) { bzt.Errors = cikdc.Errors; }
                eBookWriteManager.Context.SaveChanges();
            }
        }

     

        private string MoveDir(string originalpath, int status) // returns new full path
        {
            System.IO.DirectoryInfo rootFolder = new System.IO.DirectoryInfo (originalpath);
            System.IO.DirectoryInfo fileFolder = rootFolder;
            System.IO.DirectoryInfo clientFolder = fileFolder.Parent;
            System.IO.DirectoryInfo partnerFolder = clientFolder.Parent;

            string finalPath = originalpath;
            
            string fleFolders = string.Format("{0}\\{1}\\{2}", rootFolder.Parent.Parent.Name, rootFolder.Parent.Name, rootFolder.Name);
            while (rootFolder.Name.IndexOf("_BizTax") == -1)
            {
                rootFolder = rootFolder.Parent;
            }

            rootFolder = rootFolder.Parent;
            string moveFolder = rootFolder.FullName;

            using (new Impersonator(Config.AppuserUsername, Config.AppuserDomain, Config.AppuserPwd))
            {
            //string btFolder = "1_BizTax In";
            //string subStateFolder = "";
            if (status < 99 && status > -99)
            {
                moveFolder = System.IO.Path.Combine(moveFolder, "2_BizTax Progress");
                if (!System.IO.Directory.Exists(moveFolder)) System.IO.Directory.CreateDirectory(moveFolder);
                if (status > 10 && status < 20)
                {
                    moveFolder = System.IO.Path.Combine(moveFolder, "1 - Upload");
                }
                else if (status >= 20 && status < 30)
                {
                    moveFolder = System.IO.Path.Combine(moveFolder,"2 - Check validation");
                }
                else
                {
                    moveFolder = System.IO.Path.Combine(moveFolder, "3 - Finalize");
                }
                if (!System.IO.Directory.Exists(moveFolder)) System.IO.Directory.CreateDirectory(moveFolder);
            }
            else
            {
                moveFolder = System.IO.Path.Combine(moveFolder, "3_BizTax Out");
                if (status == 99)
                {
                    moveFolder = System.IO.Path.Combine(moveFolder, "Success");
                }
                else
                {
                     moveFolder = System.IO.Path.Combine(moveFolder, "Failed");
                }
                if (!System.IO.Directory.Exists(moveFolder)) System.IO.Directory.CreateDirectory(moveFolder);
            }
            moveFolder = System.IO.Path.Combine(moveFolder, partnerFolder.Name);
            if (!System.IO.Directory.Exists(moveFolder)) System.IO.Directory.CreateDirectory(moveFolder);

            moveFolder = System.IO.Path.Combine(moveFolder, clientFolder.Name);
            finalPath = System.IO.Path.Combine(moveFolder, fileFolder.Name);


            if (finalPath == originalpath) return finalPath;
           
                try
                {
                    System.IO.Directory.Move(clientFolder.FullName, moveFolder);
                }
                catch
                {
                    finalPath = null;
                }
            }
            return finalPath;

        }
    }
}
