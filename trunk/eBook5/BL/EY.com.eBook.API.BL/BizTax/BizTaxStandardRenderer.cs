﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.Caching;
using EY.com.eBook.Core;
using System.Xml.Linq;
using System.Text.RegularExpressions;
using System.Xml;
using EY.com.eBook.API.Contracts.Data.Cache;
using EY.com.eBook.Core.EF.Write;
using EY.com.eBook.Core.EF;
using EY.com.eBook.Core.Data;
using System.ServiceModel;
namespace EY.com.eBook.API.BL
{

    public class BizTaxStandardRenderer
    {
        #region Schema Info

        /*     */
        public virtual string SchemaRef { get { return "";  } }

        public virtual List<XmlQualifiedName> NameSpaces { get { return new List<XmlQualifiedName>(); } }

        #endregion

        public BizTaxDataContract CreateDefault(ClientBaseDataContract cdc, FileDataContract fdc)
        {


            /*
            CacheRepository cp = new CacheRepository();
            BizTaxDataContract btdc = cp.GetDefaultBizTax(2012);
            */
            if (string.IsNullOrEmpty(cdc.EnterpriseNumber))
            {
                throw new FaultException<DefaultFaultContract>(new DefaultFaultContract { ErrorCode = -101, Message = "CLIENT HAS NO ENTERPRISE NUMBER. RECTIFY IN GFIS !" });
            }
            BizTaxDataContract btdc = new BizTaxDataContract
            {
                AssessmentYear = fdc.AssessmentYear.ToString()
                ,
                Contexts = new List<ContextElementDataContract>()
                ,
                Elements = new List<XbrlElementDataContract>()
                ,
                EntityIdentifier = new ContextEntityDataContract
                {
                    IdentifierScheme = "http://www.fgov.be"
                    ,
                    IdentifierValue = cdc.EnterpriseNumber.ToBelgianEnterprise()
                }
                ,
                Units = new List<UnitElementDataContract>
                {
                    new UnitElementDataContract { Measure="pure",Id="U-Pure"}
                    ,new UnitElementDataContract { Measure="iso4217:EUR",Id="EUR"}
                }
            };
            string startDate = fdc.StartDate.ToIso8601DateOnly();
            string endDate = fdc.EndDate.ToIso8601DateOnly();
            string previousEndDate = fdc.StartDate.AddDays(-1).ToIso8601DateOnly();

            ContextPeriodDataContract periodD = new ContextPeriodDataContract { StartDate = startDate, EndDate = endDate };
            ContextPeriodDataContract periodIstart = new ContextPeriodDataContract { Instant = previousEndDate };
            ContextPeriodDataContract periodIend = new ContextPeriodDataContract { Instant = endDate };
            // add default contexts
            // GENERAL CONTEXTS
            btdc.Contexts.Add(new ContextElementDataContract
            {
                Entity = btdc.EntityIdentifier
                ,
                Id = "D"
                ,
                Period = periodD
                ,
                Scenario = new List<ContextScenarioDataContract>()
            });
            btdc.Contexts.Add(new ContextElementDataContract
            {
                Entity = btdc.EntityIdentifier
                ,
                Id = "I-Start"
                ,
                Period = periodIstart
                ,
                Scenario = new List<ContextScenarioDataContract>()
            });
            btdc.Contexts.Add(new ContextElementDataContract
            {
                Entity = btdc.EntityIdentifier
                ,
                Id = "I-End"
                ,
                Period = periodIend
                ,
                Scenario = new List<ContextScenarioDataContract>()
            });

            // EXPLICIT CONTEXTS
            btdc.Contexts.Add(new ContextElementDataContract
            {
                Entity = btdc.EntityIdentifier
                ,
                Id = "D__ForeignBranchMember"
                ,
                Period = periodD
                ,
                Scenario = new List<ContextScenarioDataContract>()
                {
                    new ContextScenarioExplicitDataContract {
                        Dimension="d-br:BranchDimension"
                        ,Value="d-br:ForeignBranchMember"
                    }
                }
            });
            btdc.Contexts.Add(new ContextElementDataContract
            {
                Entity = btdc.EntityIdentifier
                ,
                Id = "D__BelgianBranchMember"
                ,
                Period = periodD
                ,
                Scenario = new List<ContextScenarioDataContract>()
                {
                    new ContextScenarioExplicitDataContract {
                        Dimension="d-br:BranchDimension"
                        ,Value="d-br:BelgianBranchMember"
                    }
                }
            });
            btdc.Contexts.Add(new ContextElementDataContract
            {
                Entity = btdc.EntityIdentifier
                ,
                Id = "I-End__NoTaxTreatyMember"
                ,
                Period = periodIend
                ,
                Scenario = new List<ContextScenarioDataContract>()
                {
                    new ContextScenarioExplicitDataContract {
                        Dimension="d-origin:OriginDimension"
                        ,Value="d-origin:NoTaxTreatyMember"
                    }
                }
            });
            btdc.Contexts.Add(new ContextElementDataContract
            {
                Entity = btdc.EntityIdentifier
                ,
                Id = "I-End__BelgiumMember"
                ,
                Period = periodIend
                ,
                Scenario = new List<ContextScenarioDataContract>()
                {
                    new ContextScenarioExplicitDataContract {
                        Dimension="d-origin:OriginDimension"
                        ,Value="d-origin:BelgiumMember"
                    }
                }
            });
            btdc.Contexts.Add(new ContextElementDataContract
            {
                Entity = btdc.EntityIdentifier
                ,
                Id = "D__TaxTreatyMember"
                ,
                Period = periodD
                ,
                Scenario = new List<ContextScenarioDataContract>()
                {
                    new ContextScenarioExplicitDataContract {
                        Dimension="d-origin:OriginDimension"
                        ,Value="d-origin:TaxTreatyMember"
                    }
                }
            });
            btdc.Contexts.Add(new ContextElementDataContract
            {
                Entity = btdc.EntityIdentifier
                ,
                Id = "D__NoTaxTreatyMember"
                ,
                Period = periodD
                ,
                Scenario = new List<ContextScenarioDataContract>()
                {
                    new ContextScenarioExplicitDataContract {
                        Dimension="d-origin:OriginDimension"
                        ,Value="d-origin:NoTaxTreatyMember"
                    }
                }
            });
            btdc.Contexts.Add(new ContextElementDataContract
            {
                Entity = btdc.EntityIdentifier
                ,
                Id = "D__BelgiumMember"
                ,
                Period = periodD
                ,
                Scenario = new List<ContextScenarioDataContract>()
                {
                    new ContextScenarioExplicitDataContract {
                        Dimension="d-origin:OriginDimension"
                        ,Value="d-origin:BelgiumMember"
                    }
                }
            });

            CreateOtherReservesContext(ref btdc, fdc);

            CreateOtherTaxableReservesContext(ref btdc, fdc);

            


            return btdc;
        }

        public void CreateOtherTaxableReservesContext(ref BizTaxDataContract btdc, FileDataContract fdc)
        {
            string startDate = fdc.StartDate.ToIso8601DateOnly();
            string endDate = fdc.EndDate.ToIso8601DateOnly();
            string previousEndDate = fdc.StartDate.AddDays(-1).ToIso8601DateOnly();
            ContextPeriodDataContract periodD = new ContextPeriodDataContract { StartDate = startDate, EndDate = endDate };
            ContextPeriodDataContract periodIstart = new ContextPeriodDataContract { Instant = previousEndDate };
            ContextPeriodDataContract periodIend = new ContextPeriodDataContract { Instant = endDate };

            if (btdc.Contexts.Count(c => c.Id == "D__id__othertaxablereserves") == 0)
            {
                btdc.Contexts.Add(new ContextElementDataContract
                {
                    Entity = btdc.EntityIdentifier
                    ,
                    Id = "D__id__othertaxablereserves"
                    ,
                    Period = periodD
                    ,
                    Scenario = new List<ContextScenarioDataContract>()
                {
                    new ContextScenarioTypeDataContract {
                        Dimension="d-ty:DescriptionTypedDimension"
                        , Type="d-ty:DescriptionTypedID"
                        ,Value="other taxable reserves"
                    }
                }
                });
            }
            if (btdc.Contexts.Count(c => c.Id == "I-Start__id__othertaxablereserves") == 0)
            {
                btdc.Contexts.Add(new ContextElementDataContract
                {
                    Entity = btdc.EntityIdentifier
                    ,
                    Id = "I-Start__id__othertaxablereserves"
                    ,
                    Period = periodIstart
                    ,
                    Scenario = new List<ContextScenarioDataContract>()
                {
                    new ContextScenarioTypeDataContract {
                        Dimension="d-ty:DescriptionTypedDimension"
                        , Type="d-ty:DescriptionTypedID"
                        ,Value="other taxable reserves"
                    }
                }
                });
            }
            if (btdc.Contexts.Count(c => c.Id == "I-End__id__othertaxablereserves") == 0)
            {
                btdc.Contexts.Add(new ContextElementDataContract
                {
                    Entity = btdc.EntityIdentifier
                    ,
                    Id = "I-End__id__othertaxablereserves"
                    ,
                    Period = periodIend
                    ,
                    Scenario = new List<ContextScenarioDataContract>()
                 {
                    new ContextScenarioTypeDataContract {
                        Dimension="d-ty:DescriptionTypedDimension"
                        , Type="d-ty:DescriptionTypedID"
                        ,Value="other taxable reserves"
                    }
                }
                });
            }
        }

        public void CreateOtherReservesContext(ref BizTaxDataContract btdc, FileDataContract fdc)
        {
            string startDate = fdc.StartDate.ToIso8601DateOnly();
            string endDate = fdc.EndDate.ToIso8601DateOnly();
            string previousEndDate = fdc.StartDate.AddDays(-1).ToIso8601DateOnly();
            ContextPeriodDataContract periodD = new ContextPeriodDataContract { StartDate = startDate, EndDate = endDate };
            ContextPeriodDataContract periodIstart = new ContextPeriodDataContract { Instant = previousEndDate };
            ContextPeriodDataContract periodIend = new ContextPeriodDataContract { Instant = endDate };

            if (btdc.Contexts.Count(c => c.Id == "D__id__otherreserves") == 0)
            {
                btdc.Contexts.Add(new ContextElementDataContract
                {
                    Entity = btdc.EntityIdentifier
                    ,
                    Id = "D__id__otherreserves"
                    ,
                    Period = periodD
                    ,
                    Scenario = new List<ContextScenarioDataContract>()
                {
                    new ContextScenarioTypeDataContract {
                        Dimension="d-ty:DescriptionTypedDimension"
                        , Type="d-ty:DescriptionTypedID"
                        ,Value="other reserves"
                    }
                }
                });
            }
            if (btdc.Contexts.Count(c => c.Id == "I-Start__id__otherreserves") == 0)
            {
                btdc.Contexts.Add(new ContextElementDataContract
                {
                    Entity = btdc.EntityIdentifier
                    ,
                    Id = "I-Start__id__otherreserves"
                    ,
                    Period = periodIstart
                    ,
                    Scenario = new List<ContextScenarioDataContract>()
                {
                    new ContextScenarioTypeDataContract {
                        Dimension="d-ty:DescriptionTypedDimension"
                        , Type="d-ty:DescriptionTypedID"
                        ,Value="other reserves"
                    }
                }
                });
            }
            if (btdc.Contexts.Count(c => c.Id == "I-End__id__otherreserves") == 0)
            {
                btdc.Contexts.Add(new ContextElementDataContract
                {
                    Entity = btdc.EntityIdentifier
                    ,
                    Id = "I-End__id__otherreserves"
                    ,
                    Period = periodIend
                    ,
                    Scenario = new List<ContextScenarioDataContract>()
                 {
                    new ContextScenarioTypeDataContract {
                        Dimension="d-ty:DescriptionTypedDimension"
                        , Type="d-ty:DescriptionTypedID"
                        ,Value="other reserves"
                    }
                }
                });
            }
        }

        public XDocument RenderBizTaxFile(BizTaxDataContract btdc, Guid fileId)
        {


            Dictionary<string, XNamespace> ns = NameSpaces.ToDictionary(k => k.Name, v => XNamespace.Get(v.Namespace));

            XNamespace defaultns = ns[""];

            XElement biztax = new XElement("biztax");

            XElement xbrl = new XElement(defaultns + "xbrl");
            foreach (XmlQualifiedName qname in NameSpaces)
            {
                if (!string.IsNullOrEmpty(qname.Name))
                {
                    xbrl.Add(new XAttribute(XNamespace.Xmlns + qname.Name, qname.Namespace));
                }
            }
            //xbrl.Add(new XAttribute("xmlns", "http://www.xbrl.org/2003/instance"));
            biztax.Add(xbrl);
            XDocument doc = XDocument.Parse(biztax.ToString());
            xbrl = doc.Root.Element(defaultns + "xbrl");


            xbrl.Add(new XElement(ns["link"] + "schemaRef"
                        , new XAttribute(ns["xlink"] + "href", SchemaRef)
                        , new XAttribute(ns["xlink"] + "type", "simple")
                        )
            );

            foreach (ContextElementDataContract ctedc in btdc.Contexts)
            {
                XElement ctx = new XElement(defaultns + "context", new XAttribute("id", ctedc.Id));
                XElement ent = new XElement(defaultns + "entity", new XElement(defaultns + "identifier", new XAttribute("scheme", ctedc.Entity.IdentifierScheme), ctedc.Entity.IdentifierValue));
                XElement period = new XElement(defaultns + "period");
                if (!string.IsNullOrEmpty(ctedc.Period.Instant))
                {
                    period.Add(new XElement(defaultns + "instant", ctedc.Period.Instant));
                }
                else
                {
                    period.Add(new XElement(defaultns + "startDate", ctedc.Period.StartDate), new XElement(defaultns + "endDate", ctedc.Period.EndDate));
                }
                ctx.Add(ent, period);
                if (ctedc.Scenario != null && ctedc.Scenario.Count > 0)
                {
                    XElement scen = new XElement(defaultns + "scenario");
                    foreach (ContextScenarioDataContract csdc in ctedc.Scenario)
                    {
                        XElement cts = null;
                        if (csdc.GetType() == typeof(ContextScenarioExplicitDataContract))
                        {
                            ContextScenarioExplicitDataContract csedc = (ContextScenarioExplicitDataContract)csdc;
                            cts = new XElement(ns["xbrldi"] + "explicitMember", new XAttribute("dimension", csedc.Dimension), csedc.Value);
                        }

                        else if (csdc.GetType() == typeof(ContextScenarioTypeDataContract))
                        {
                            /*
                              <xbrldi:typedMember dimension="d-ty:DescriptionTypedDimension">
                                    <d-ty:DescriptionTypedID>anderereserves3</d-ty:DescriptionTypedID>
                               </xbrldi:typedMember>
                             */
                            ContextScenarioTypeDataContract cstdc = (ContextScenarioTypeDataContract)csdc;
                            cts = new XElement(ns["xbrldi"] + "typedMember", new XAttribute("dimension", cstdc.Dimension));
                            string[] csName = cstdc.Type.Split(new char[] { ':' });
                            cts.Add(new XElement(ns[csName[0]] + csName[1], cstdc.Value));
                        }
                        else
                        {
                            if (csdc.Name == "explicitMember")
                            {
                                cts = new XElement(ns["xbrldi"] + "explicitMember", new XAttribute("dimension", csdc.Dimension), csdc.Value);
                            }
                            else if (csdc.Name == "typedMember")
                            {
                                cts = new XElement(ns["xbrldi"] + "typedMember", new XAttribute("dimension", csdc.Dimension));
                                string[] csName = csdc.Type.Split(new char[] { ':' });
                                cts.Add(new XElement(ns[csName[0]] + csName[1], csdc.Value));
                            }
                        }
                        if (cts != null)
                        {
                            scen.Add(cts);
                        }
                    }
                    ctx.Add(scen);
                }
                xbrl.Add(ctx);
            }

            foreach (UnitElementDataContract unit in btdc.Units)
            {
                xbrl.Add(new XElement(defaultns + "unit", new XAttribute("id", unit.Id), new XElement(defaultns + "measure", unit.Measure)));
            }



            foreach (XbrlElementDataContract xedc in btdc.Elements)
            {
                xbrl.Add(XbrlElementToXml(xedc, ns, fileId));
            }


            //doc.Save(@"C:\Projects_TFS\EY\Ebook\Main-v5.0\DATA\BIZTAX_" + Guid.NewGuid().ToString() + ".xml");

            return doc;
        }

        public XElement XbrlElementToXml(XbrlElementDataContract xedc, Dictionary<string, XNamespace> ns,Guid fileid)
        {
            XElement xed = new XElement(ns[xedc.Prefix] + xedc.Name);
            if (!string.IsNullOrEmpty(xedc.ContextRef)) xed.Add(new XAttribute("contextRef", xedc.ContextRef));
            if (!string.IsNullOrEmpty(xedc.Decimals)) xed.Add(new XAttribute("decimals", xedc.Decimals));
            if (!string.IsNullOrEmpty(xedc.UnitRef)) xed.Add(new XAttribute("unitRef", xedc.UnitRef));

            if (xedc.BinaryValue != null && xedc.BinaryValue.Count > 0)
            {
                ITextRepository it = new ITextRepository();
                string base64 = it.GenerateBundleItemsBase64(xedc.BinaryValue, fileid);
                xed.Add(base64);
                // GET BASE 64 ENCODED BUNDLE
            }
            else
            {
                xed.Add(xedc.Value);

                if (xedc.Children != null && xedc.Children.Count > 0)
                {
                    foreach (XbrlElementDataContract xedcChild in xedc.Children)
                    {
                        XElement xedChild = XbrlElementToXml(xedcChild, ns,fileid);
                        if (xedChild != null && (!string.IsNullOrEmpty(xedChild.Value) || xedChild.Elements().Count() > 0))
                        {
                            xed.Add(xedChild);
                        }
                    }
                }
            }

            return xed;
        }

        public List<ContextElementDataContract> CreateContextAllPeriods(ClientBaseDataContract cdc, FileDataContract fdc, List<ContextScenarioDataContract> scenarios, bool autoRendered)
        {
            return new List<ContextElementDataContract>
            {
                CreateContext(cdc,fdc,"D",scenarios,autoRendered)
                ,CreateContext(cdc,fdc,"I-Start",scenarios,autoRendered)
                ,CreateContext(cdc,fdc,"I-End",scenarios,autoRendered)
            };

        }

        public ContextElementDataContract CreateContext(ClientBaseDataContract cdc, FileDataContract fdc, string period, List<ContextScenarioDataContract> scenarios, bool autoRendered)
        {
            string startDate = fdc.StartDate.ToIso8601DateOnly();
            string endDate = fdc.EndDate.ToIso8601DateOnly();
            string previousEndDate = fdc.StartDate.AddDays(-1).ToIso8601DateOnly();
            ContextPeriodDataContract periodD = new ContextPeriodDataContract { StartDate = startDate, EndDate = endDate };
            ContextPeriodDataContract periodIstart = new ContextPeriodDataContract { Instant = previousEndDate };
            ContextPeriodDataContract periodIend = new ContextPeriodDataContract { Instant = endDate };


            ContextElementDataContract cte = new ContextElementDataContract
            {
                AutoRendered = autoRendered
                ,
                Entity = new ContextEntityDataContract
                {
                    IdentifierScheme = "http://www.fgov.be"
                    ,
                    IdentifierValue = cdc.EnterpriseNumber.ToBelgianEnterprise()
                }
                ,
                Id = string.Empty
                ,
                Period = period == "D" ? periodD : period == "I-Start" ? periodIstart : periodIend
                ,
                Scenario = scenarios
            };

            // create id
            StringBuilder sb = new StringBuilder(period);
            foreach (ContextScenarioDataContract scenario in scenarios)
            {
                string scenId = scenario.Value;
                if (scenario.GetType() == typeof(ContextScenarioExplicitDataContract))
                {
                    string[] splitted = scenId.Split(new char[] { ':' });
                    scenId = splitted[splitted.Length - 1];
                }
                else
                {
                    scenId = scenId.CleanXbrlId();
                }
                sb.Append("__id__").Append(scenId);
            }

            cte.Id = sb.ToString();
            return cte;
        }


        public ContextScenarioDataContract CreateScenario(string dimensiontype, string dimension, string value, string type)
        {
            switch (dimensiontype.ToLower())
            {
                case "explicit":
                    return new ContextScenarioExplicitDataContract
                    {
                        Dimension = dimension
                        ,
                        Value = value
                    };
                    break;
                case "type":
                    return new ContextScenarioTypeDataContract
                    {
                        Dimension = dimension
                        ,
                        Type = type
                        ,
                        Value = value.Replace(" ","")
                    };
                    break;
            }
            return null;
        }

        public void AddTab(ref BizTaxDataContract btdc)
        {
        }

        public BizTaxDataContract FindProxy(BizTaxDataContract btdc, Guid fileId)
        {
            File file = eBookWriteManager.Context.Files.First(f=>f.Id==fileId);
            EY.com.eBook.API.BL.Read.Repository rep = new EY.com.eBook.API.BL.Read.Repository();
            List<RepositoryNodeDataContract> files = rep.QueryFiles(new CriteriaQueryFilesDataContract
            {
                ClientId = file.ClientId
                ,
                PeriodStart = file.StartDate
                ,
                PeriodEnd = file.EndDate
                ,
                StructureId = new Guid("D33CE2B5-FBEB-4D58-9C47-2A4731804173")
            });

            if (files.Count > 0)
            {
                Guid signedStatus = new Guid("FCF7A560-3D74-4341-A32B-310C8FA336CF");
                RepositoryNodeDataContract signedFile = files.FirstOrDefault(f => f.Item.Status == signedStatus);
                if (signedFile != null)
                {
                    btdc.ProxyId = signedFile.Item.Id;
                    btdc.ProxyTitle = signedFile.Item.FileName;
                    return btdc;
                }
            }
            btdc.ProxyId = null;
            btdc.ProxyTitle = null;
            return btdc;
        }

        public BizTaxDataContract Clean(BizTaxDataContract btdc)
        {
            // remove empty nodes.
            btdc.Elements.RemoveAll(e => string.IsNullOrEmpty(e.Value) && (e.BinaryValue == null || e.BinaryValue.Count == 0)
                                        && (e.Children == null || e.Children.Count == 0));

            // !!DO NOT!! remove unusedContexts !!!
            //List<string> contexts = btdc.Elements.Select(e => e.ContextRef).ToList();
            //btdc.Contexts.RemoveAll(c => !contexts.Contains(c.Id));
            return btdc;
        }

        public void CheckChildren(ref XbrlElementDataContract existing, List<XbrlElementDataContract> children)
        {
            if (existing.Children == null) existing.Children = new List<XbrlElementDataContract>();
            foreach (XbrlElementDataContract xedc in children)
            {
                XbrlElementDataContract exist = existing.Children.FirstOrDefault(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix);
                if (xedc.Name.StartsWith("XCode")) // IS VALUE LIST VALUE
                {
                    if (exist==null)
                    {
                        existing.Children.Clear();
                        existing.Children.Add(xedc);
                    }
                    // else nothing todo, value (list value) is equal.
                }
                else
                {
                    
                    if (exist!=null)
                    {
                        if (xedc.Children != null && xedc.Children.Count > 0)
                        {
                            CheckChildren(ref exist, xedc.Children);
                        }
                        else
                        {
                            if (exist.AutoRendered || !string.IsNullOrEmpty(xedc.Value))
                            {
                                
                                exist.Value = xedc.Value;
                            }
                        }
                    }
                    else
                    {
                        existing.Children.Add(xedc);
                    }
                }
            }
        }

        public void UpdateBizTaxMetaData(XbrlDataContract xdc)
        {
            FileXbrl fx = eBookWriteManager.Context.FileXbrlSet.FirstOrDefault(x => x.FileId == xdc.FileId && x.XbrlType == "BIZTAX");//xdc.Type); 
            if (fx != null)
            {
                if (xdc.Status == 10 && fx.StatusId !=10)
                {
                    fx.Validated = xdc.Validated;
                    fx.Locked = xdc.Locked;
                    fx.DeclaringDepartment = xdc.DeclaringDepartment;
                    fx.DeclaringPartnerId = xdc.DeclaringPartnerId;
                    fx.DeclaringPartnerName = xdc.DeclaringPartnerName;
                    eBookWriteManager.Context.SaveChanges();
                    Declare(xdc.FileId,xdc.EncodedActivePerson);
                }
                fx.StatusId = xdc.Status;
                fx.Validated = xdc.Validated;
                fx.Locked = xdc.Locked;
                fx.DeclaringDepartment = xdc.DeclaringDepartment;
                fx.DeclaringPartnerId = xdc.DeclaringPartnerId;
                fx.DeclaringPartnerName = xdc.DeclaringPartnerName;
                eBookWriteManager.Context.SaveChanges();
            }
            else
            {

            }
        }

        public void SaveBizTax(CriteriaXbrlDataContract cxdc)
        {
            cxdc.Xbrl.Data = Clean(cxdc.Xbrl.Data); // CLEAN UNREFERENCED CONTEXTS AND EMPTY NODES
            cxdc.Xbrl.Data.LastSaved = DateTime.Now;
            FileXbrl fx = eBookWriteManager.Context.FileXbrlSet.FirstOrDefault(x => x.FileId == cxdc.FileId && x.XbrlType == "BIZTAX");
            if (fx == null)
            {
                fx = new FileXbrl
                {
                    FileId = cxdc.Xbrl.FileId
                    ,
                    XbrlType = "BIZTAX"
                    ,
                    Calculation = cxdc.Xbrl.Calculation
                    ,
                    StatusId = cxdc.Xbrl.Status
                    ,
                    Validated = cxdc.Xbrl.Validated
                    ,
                    XbrlData = CoreHelper.SerializeToString(cxdc.Xbrl.Data)
                    , Locked = cxdc.Xbrl.Locked
                };
                eBookWriteManager.Context.AddToFileXbrlSet(fx);
            }
            else
            {
                fx.StatusId = cxdc.Xbrl.Status;
                fx.Validated = cxdc.Xbrl.Validated;
                fx.Locked = cxdc.Xbrl.Locked;
                fx.XbrlData = CoreHelper.SerializeToString(cxdc.Xbrl.Data);
            }
            eBookWriteManager.Context.SaveChanges();
        }

        public BizTaxDataContract LoadFromDb(Guid fileId)
        {
            FileXbrl fx = eBookWriteManager.Context.FileXbrlSet.FirstOrDefault(x => x.FileId == fileId && x.XbrlType == "BIZTAX");
            if (fx == null) return null;
            return CoreHelper.DeserializeFromString<BizTaxDataContract>(fx.XbrlData);
        }

        public XbrlDataContract LoadFromDbXbrl(Guid fileId, string xbrlType)
        {
            FileXbrl value = eBookWriteManager.Context.FileXbrlSet.FirstOrDefault(x => x.FileId == fileId && x.XbrlType == "BIZTAX");
            if (value == null)
            {
                return new XbrlDataContract
                {
                    Calculation = null,
                    Data = null,
                    FileId = fileId
                    ,
                    Validated = false
                    ,
                    Locked = false
                    ,
                    Status = 0
                    ,
                    Type = xbrlType

                };
            }
            return new XbrlDataContract
                {
                    Calculation = null
                    ,
                    Data = CoreHelper.DeserializeFromString<BizTaxDataContract>(value.XbrlData)
                    ,
                    FileId = value.FileId
                    ,
                    Locked = value.Locked
                    ,
                    Status = value.StatusId
                    ,
                    Type = value.XbrlType
                    ,
                    Validated = value.Validated
                    , DeclaringPartnerName = value.DeclaringPartnerName
                    , DeclaringDepartment = value.DeclaringDepartment
                    , DeclaringPartnerId = value.DeclaringPartnerId
                };
        }

        public void SentForPartnerValidation(CriteriaXbrlDataContract cxdc)
        {

        }

        public void UnLock(CriteriaXbrlDataContract cxdc)
        {

        }

        public virtual decimal? GetTaxCalculation(Guid fileId)
        {
            return null;
        }

        public bool Declare(Guid id, string encodedPerson)
        {
            
            ActivePersonDataContract apdc = null;
            if (!string.IsNullOrEmpty(encodedPerson))
            {
                 apdc = new BusinessHelper().DecodePerson(encodedPerson);
            }

            XbrlDataContract xdc = LoadFromDbXbrl(id, "BIZTAX");
            //xdc.Status = 10;
           
            using (new Impersonator(Config.AppuserUsername, Config.AppuserDomain, Config.AppuserPwd))
            {
                BizTaxDeclaration btdExists = eBookWriteManager.Context.BizTaxDeclarationSet.FirstOrDefault(d => d.Id == id);
                if (btdExists != null)
                {

                    if (System.IO.Directory.Exists(btdExists.CurrentPath))
                    {
                        System.IO.Directory.Delete(btdExists.CurrentPath,true);
                    }
                   
                    eBookWriteManager.Context.DeleteObject(btdExists);
                    eBookWriteManager.Context.SaveChanges();
                }

                string basefolder = Config.GetBiztaxPublishFolder(xdc.DeclaringDepartment,xdc.Data.AssessmentYear);
                basefolder = System.IO.Path.Combine(basefolder, xdc.DeclaringPartnerName.Replace(" ", "."));

                if (!System.IO.Directory.Exists(basefolder))
                {
                    System.IO.Directory.CreateDirectory(basefolder);
                }

                EY.com.eBook.Core.EF.Read.File file = eBookReadManager.Context.Files.Include("Client").First(f => f.Id == id);
                basefolder = System.IO.Path.Combine(basefolder, file.Client.Name.CleanXbrlId());
                string filebasefolder = System.IO.Path.Combine(basefolder, file.EndDate.ToString("dd-MM-yyyy"));
                if (System.IO.Directory.Exists(filebasefolder))
                {
                    filebasefolder = System.IO.Path.Combine(basefolder, string.Format("{0}_{1}", file.EndDate.ToString("dd-MM-yyyy"), file.Id.ToString().CleanXbrlId()));
                }
                string eBookFile = System.IO.Path.Combine(filebasefolder, string.Format("{0}.eBook", id.ToString()));
                if (!System.IO.Directory.Exists(filebasefolder))
                {
                    System.IO.Directory.CreateDirectory(filebasefolder);

                }

                if (!System.IO.File.Exists(eBookFile))
                {
                    System.IO.StreamWriter sw = System.IO.File.CreateText(eBookFile);
                    sw.WriteLine(string.Format("Created: {0} - ClientId:{1}", DateTime.Now.ToString(), file.Client.Id.ToString()));
                    sw.Close();
                }


                XDocument xbrl = RenderBizTaxFile(xdc.Data, xdc.FileId);
                xbrl.Save(System.IO.Path.Combine(filebasefolder, @"biztax.biztax"));
                xdc.Data = FindProxy(xdc.Data, id);
                if (xdc.Data.ProxyId.HasValue)
                {
                    string path = new EY.com.eBook.API.BL.Read.Repository().GetFilePath(xdc.Data.ProxyId.Value);
                    if (System.IO.File.Exists(System.IO.Path.Combine(filebasefolder, "proxy.pdf")))
                    {
                        System.IO.File.Delete(System.IO.Path.Combine(filebasefolder, "proxy.pdf"));
                    }
                    if (System.IO.File.Exists(path))
                    {
                        System.IO.File.Copy(path, System.IO.Path.Combine(filebasefolder, "proxy.pdf"));
                    }
                }

                decimal? taxCalc = GetTaxCalculation(id);
                BizTaxDeclaration btd = new BizTaxDeclaration
                {
                    Id = file.Id
                    ,
                    AssessmentYear = file.EndDate.AddDays(1).Year
                    ,
                    BiztaxCalc = null
                    ,
                    ClientEnterprise = file.Client.EnterpriseNumber
                    ,
                    ClientId = file.Client.Id
                    ,
                    ClientName = file.Client.Name
                    ,
                    CurrentPath = filebasefolder
                    ,
                    Department = xdc.DeclaringDepartment
                    ,
                    eBookCalc = taxCalc
                    ,
                    Errors = null
                    ,
                    FileType = taxCalc.HasValue ? "M" : "F"
                    ,
                    InProgressBy = null
                    ,
                    LastChanged = DateTime.Now
                    ,
                    PartnerId = xdc.DeclaringPartnerId.Value
                    ,
                    PartnerName = xdc.DeclaringPartnerName
                    ,
                    Proxy = xdc.Data.ProxyId.HasValue
                    ,
                    SenderDate = DateTime.Now
                    ,
                    SenderName = apdc !=null? apdc.Name : null
                    ,
                    Status = 10//xdc.Status
                };
                

                eBookWriteManager.Context.AddToBizTaxDeclarationSet(btd);
                eBookWriteManager.Context.SaveChanges();
            }
            //if (xdc.Data.ProxyId.HasValue)
            //{

            //}


            return true;
           // UpdateBizTaxMetaData(xdc);
        }
    }

    public static class BizTaxExtensions
    {
        public static string CleanXbrlId(this string val)
        {
            Regex rgx = new Regex("[^a-zA-Z0-9 \\-]");
            val = rgx.Replace(val, "");
            return val;
        }

        public static string ToXbrlValue(this DateTime dte)
        {
            return dte.ToIso8601DateOnly();
        }

        public static string ToXbrlValue(this DateTime? dte)
        {
            if (!dte.HasValue) return null;
            return dte.Value.ToIso8601DateOnly();
        }

        public static string ToXbrlValue(this decimal nr)
        {
          //  if (nr == 0) return null;
            return nr.ToString(System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
        }

        public static string ToXbrlValue(this int nr)
        {
           // if (nr == 0) return null;
            return nr.ToString(System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
        }

        public static string ToXbrlValue(this bool val)
        {
            return val.ToString().ToLower();
        }

        public static string ToXbrlValue(this string val)
        {
            return val.GetValueOrDefault();
        }

        public static string ToXbrlValue(this decimal? nr)
        {
            return nr.HasValue ? nr.Value.ToXbrlValue() : null;
        }

        public static string ToXbrlValue(this int? nr)
        {
            return nr.HasValue ? nr.Value.ToXbrlValue() : null;
        }

        public static string ToXbrlValue(this bool? val)
        {
            return val.HasValue ? val.Value.ToXbrlValue() : false.ToXbrlValue();
        }
    }
}
