﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EY.com.eBook.Core.EF;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.BL;
using Newtonsoft.Json;
using EY.com.eBook.Core.EF.Read;


namespace EY.com.eBook.BL.Export
{
    public class ExportCodaPoa : BaseExport<CodaPoaDataContract>
    {
        private CriteriaCodaPoaDataContract _ccpdc;
        private CriteriaCodaPoaLogDataContract _ccpldc;
        //private ClientRepository _clientRep;
        private List<ClientCodaPoa> _lccp;
        private List<ClientCodaPoaLog> _lccpl;

        public ExportCodaPoa(CriteriaCodaPoaDataContract ccpdc)
        {
            _ccpdc = ccpdc;
            _ccpldc  = new CriteriaCodaPoaLogDataContract(){BankAccount = ccpdc.BankAccount};

            LoadData();
            CreateWorkbook();
            SetCurrentWorksheet(1, "Clients");
            CreateHeaderCurrentWorksheet();
            FillMyData(_lccp);

            Id = SaveWorkbook();
        }

        protected override void LoadData()
        {
            API.BL.Read.ClientCodaRepository a = new EY.com.eBook.API.BL.Read.ClientCodaRepository();
            _lccp = a.QueryCodaPoa(_ccpdc);
        }

        protected override void CreateHeaderCurrentWorksheet()
        {
            int columns = 0; //I got tired of changing numbers

            _worksheet.Cells[1, ++columns].Value = "Client Name";
            _worksheet.Cells[1, ++columns].Value = "Client GFIS";
            _worksheet.Cells[1, ++columns].Value = "Bank Account";
            _worksheet.Cells[1, ++columns].Value = "BIC";
            _worksheet.Cells[1, ++columns].Value = "Bank";
            _worksheet.Cells[1, ++columns].Value = "PMT Allow Coda";
            _worksheet.Cells[1, ++columns].Value = "Active Client ACR";
            _worksheet.Cells[1, ++columns].Value = "Exact EY License";
            _worksheet.Cells[1, ++columns].Value = "Exact Dig Mailbox";
            _worksheet.Cells[1, ++columns].Value = "Exact Division";
            _worksheet.Cells[1, ++columns].Value = "Status";
            _worksheet.Cells[1, ++columns].Value = "Status Modified";
            _worksheet.Cells[1, ++columns].Value = "Document In Repository";
            _worksheet.Cells[1, ++columns].Value = "Last Modified";
            _worksheet.Cells[1, ++columns].Value = "Last log Entry";
            _worksheet.Cells[1, ++columns].Value = "Log Entry Modified";


            for (int column = 1; column <= columns; ++column)
            {
                _worksheet.Cells[1, column].Style.Font.Bold = true;
            }    
        }

        protected override void FillData(List<CodaPoaDataContract> list)
        {
            FillMyData(_lccp);
        }

        protected void FillMyData(List<ClientCodaPoa> lccp)
        {
            int row = 2; //start row

            foreach (var cpdc in lccp)
            {
                int column = 0;
                ClientCodaPoaLog ccpl = eBookReadManager.Context.ClientCodaPoaLogSet.AsQueryable().Where(p => p.BankAccountNr == cpdc.BancAccountNr && p.Action.Contains("Comment")).OrderByDescending(p => p.Timestamp).FirstOrDefault(); //get last comment

                AddCell(row, ++column, cpdc.Client.Name);
                AddCell(row, ++column, cpdc.Client.GFISCode);
                AddCell(row, ++column, cpdc.BancAccountNr);
                AddCell(row, ++column, cpdc.BankInfo.Bic);
                AddCell(row, ++column, cpdc.BankInfo.En);
                AddCell(row, ++column, cpdc.PmtAllowCoda);
                AddCell(row, ++column, cpdc.Client.ClientDepartments.Any(d => d.DepartmentId == "ACR" && !d.EndDate.HasValue));
                AddCell(row, ++column, cpdc.Client.EOL_EYLicense);
                AddCell(row, ++column, cpdc.Client.EOL_DigMailbox);
                AddCell(row, ++column, cpdc.Client.EOL_Division);  
                AddCell(row, ++column, cpdc.Status.Status);
                AddCell(row, ++column, cpdc.StatusModified);
                AddCell(row, ++column, cpdc.RepositoryItemId.HasValue);
                AddCell(row, ++column, cpdc.LastModified);
                if (ccpl != null)
                {
                    AddCell(row, ++column, ccpl.Comment);
                    AddCell(row, ++column, ccpl.Timestamp);
                }
        
                row++;
            }
        }



        private void AddCell(int row, int cell, object value)
        {

            var range = _worksheet.Cells[row, cell];
            if (value == null) value = "";
            if (value.GetType().Name == typeof(DateTime).Name)
            {
                DateTime dte = (DateTime)value;
               // range.Style.Numberformat.Format = "dd/mm/jjjj uu:mm:ss";
                //range.Formula =string.Format("=DATE({0};{1};{2}) + TIME({3};{4};{5})",dte.Year,dte.Month,dte.Day,dte.Hour,dte.Minute,dte.Second);
                range.Value = dte.ToString("dd/MM/yyyy HH:mm:ss");
              //  range.Value = string.Format("=DATE({0};{1};{2}) + TIME({3};{4};{5})",dte.Year,dte.Month,dte.Day,dte.Hour,dte.Minute,dte.Second);
                setColumnWidth(_worksheet, row, cell, value.ToString());

            }
            else
            {
                range.Value = value;
            }
            if (value.GetType().Name == typeof(string).Name)
            {
                
                setColumnWidth(_worksheet, row, cell, value.ToString());
            }
        }
    }
}
