﻿using System.Text;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using DocumentFormat.OpenXml;
using System.Globalization;
using EY.com.eBook.API.Contracts.Data;
using System.Configuration;
using System;
using System.Linq;
using System.Collections.Generic;
using ICSharpCode.SharpZipLib.Zip;
using EY.com.eBook.Core;
using EY.com.eBook.Core.EF.Read;
using EY.com.eBook.Core.EF;
using EY.com.eBook.API.Contracts.Proxies;
using EY.com.eBook.ProAcc.API.Contracts.Data;

namespace EY.com.eBook.BL.Export
{
    
    // Creates a txt file ready for import in ProAcc.
    public class ExportExactBookings
    {


        // Creates a SpreadsheetDocument.
        public string CreatePackage(CriteriaExportBookingsDataContract cebdc)
        {

            

            Guid id = Guid.NewGuid();
            string workingFolder = Config.ExcelWorkingFolder;
            string zPath = System.IO.Path.Combine(workingFolder, string.Format("{0}.zip", id.ToString()));
            workingFolder = System.IO.Path.Combine(workingFolder, id.ToString());
            

            File file = eBookReadManager.Context.Files
                            .Include("Client")
                            .Include("Bookings")
                            .Include("Bookings.BookingLines")
                            .Include("Bookings.BookingLines.Account")
                            .First(f => f.Id == cebdc.FileId);
            /*
            if(string.IsNullOrEmpty(file.Client.ProAccDatabase) || string.IsNullOrEmpty(file.Client.ProAccServer)) 
                throw new Exception("No proacc connection");

            ProAccServiceProxy psp = new ProAccServiceProxy();
            psp.Open();
            ProAccImportDetailsDataContract piddc = psp.GetImportDetails(new CriteriaProAccStartAndEndDatesDataContract
            {
                Database = file.Client.ProAccDatabase
                ,
                Server = file.Client.ProAccServer
                ,
                Start = file.StartDate
                ,
                End = file.EndDate
            });
            psp.Close();
            */
            System.IO.Directory.CreateDirectory(workingFolder);

            string pickupPath = System.IO.Path.Combine(Config.PickupLocal, string.Format("{0}.zip", id.ToString()));


            ZipFile zipFile = ZipFile.Create(zPath);
            

            List<Booking> bookings = file.Bookings.Where(b => cebdc.BookingIds.Contains(b.Id)).ToList();

            List<Account> newAccounts = eBookReadManager.Context.Accounts
                                            .Include("Descriptions")
                                            .Where(a => a.FileId == cebdc.FileId && string.IsNullOrEmpty(a.ImportedID)
                                                        && (a.Saldo!=0 || a.StartSaldo!=0)
                                            )
                                            .ToList();

            List<Account> changedAccounts = eBookReadManager.Context.Accounts
                                            .Include("Descriptions")
                                            .Where(a => a.FileId == cebdc.FileId && !string.IsNullOrEmpty(a.ImportedID)
                                                        && a.Descriptions.Count(d=>d.HasChanged)>0)
                                            .ToList();

            zipFile.BeginUpdate();


            // create first set: new accounts
            string filePath = System.IO.Path.Combine(workingFolder, "1-New-Accounts.txt");
            if (newAccounts.Count > 0)
            {
                RenderAccounts(filePath, newAccounts);
                zipFile.Add(filePath,System.IO.Path.GetFileName(filePath));
            }


            filePath = System.IO.Path.Combine(workingFolder, "2-Changed-Accounts.txt");
            if (changedAccounts.Count>0)
            {
                RenderAccounts(filePath, changedAccounts);
                zipFile.Add(filePath, System.IO.Path.GetFileName(filePath));
            }
            filePath = System.IO.Path.Combine(workingFolder, "3-Bookings.txt");
            RenderBookings(filePath, bookings, file.EndDate);
            zipFile.Add(filePath, System.IO.Path.GetFileName(filePath));
            zipFile.CommitUpdate();
            zipFile.Close();

            System.IO.File.Move(zPath, pickupPath);
            System.IO.Directory.Delete(workingFolder, true);


            CacheProxy cp = new CacheProxy();
            cp.Open();
            cp.UpdateBookings(cebdc.FileId);
            cp.Close();


            return string.Format("{0}.zip",id.ToString());
            
        }

        private void RenderAccounts(string filePath, List<Account> accounts)
        {
            //StreamWriter sw = System.IO.File.CreateText(filePath);
            System.IO.StreamWriter sw = new System.IO.StreamWriter(filePath, false, Encoding.GetEncoding("ISO-8859-1"));
            foreach (Account account in accounts)
            {
                string visnr = account.InternalNr;
                if (!string.IsNullOrEmpty(account.VisualNr))
                {
                    visnr = account.VisualNr;
                }
                visnr = visnr.Substring(0, 6);

                string descrNL = "";
                string descrFR = "";
                string descrEN = "";

                foreach (AccountDescription description in account.Descriptions)
                {
                     switch (description.Culture)
                    {
                        case "fr-FR":
                            descrFR = description.Description;
                            break;
                        case "en-US":
                            descrEN = description.Description;
                            break;
                        case "de-DE":
                            continue;
                        default:
                            descrNL = description.Description;
                            break;
                    }
                }

                StringBuilder sb = new StringBuilder();
                // COL A
                sb.Append(visnr).Append("\t");
                // COL B
                sb.Append(descrNL).Append("\t");
                // COL C
                sb.Append(descrFR).Append("\t");
                // COL D
                sb.Append(descrEN).Append("\t");
                
                /*
                int lineCounter = 0;
                foreach (AccountDescription description in account.Descriptions)
                {
                    if (description.HasChanged)
                    {
                        description.HasChanged = false;
                        eBookReadManager.Context.SaveChanges();
                    }
                    string lang = "N";
                    switch (description.Culture)
                    {
                        case "fr-FR":
                            lang = "F";
                            break;
                        case "en-US":
                            lang = "E";
                            break;
                        case "de-DE":
                            continue;
                        default:
                            lang = "N";
                            break;
                    }
                    
                    StringBuilder sb = new StringBuilder();
                    /*
                    // COL A : Linecode, 1=new booking, 2=bookingline, 99=end
                    sb.Append(lineCounter == 0 ? "1" : "2").Append("\t");
                    // COL B
                    sb.Append(lineCounter == 0 ? visnr : "").Append("\t");
                    // COL C
                    sb.Append(lang).Append("\t");
                    // COL D

                    sb.Append(description.Description.Length > 60 ? description.Description.Substring(0, 60) : description.Description).Append("\t");
                     * *//*
                    // COL A
                    sb.Append(visnr).Append("\t");
                    // COL B
                    sb.Append(
                    // COL C

                    // COL D

                    // COL E
                    sb.Append("").Append("\t");
                    // COL F
                    sb.Append("").Append("\t");
                    // COL G
                    sb.Append("").Append("\t");
                    // COL H
                    sb.Append("").Append("\t");
                    // COL I
                    sb.Append("").Append("\t");
                    // COL J
                    sb.Append("").Append("\t");
                    // COL K
                    sb.Append("").Append("\t");
                    // COL L
                    sb.Append("").Append("\t");
                    // COL M
                    sb.Append("").Append("\t");
                    // COL N
                    sb.Append("").Append("\t");
                    // COL O
                    sb.Append("").Append("\t");
                    // COL O
                    sb.Append("").Append("\t");
                    // COL O
                    sb.Append("").Append("\t");
                    // COL O
                    sb.Append("").Append("\t");
                    // COL O
                    sb.Append("").Append("\t");
                    // COL O
                    sb.Append("").Append("\t");
                    // COL O
                    sb.Append("").Append("\t");
                    // COL O
                    sb.Append("").Append("\t");
                    // COL W
                    sb.Append("").Append("\t");
                    sw.WriteLine(sb.ToString());
                    lineCounter++;
                }
            */
            }
        

            // END LINE
            /*
            StringBuilder sbEnd = new StringBuilder();
            // COL A : Linecode, 1=new booking, 2=bookingline, 99=end
            sbEnd.Append("99").Append("\t");
            // COL B
            sbEnd.Append("").Append("\t");
            // COL C
            sbEnd.Append("").Append("\t");
            // COL D
            sbEnd.Append("").Append("\t");
            // COL E
            sbEnd.Append("").Append("\t");
            // COL F
            sbEnd.Append("").Append("\t");
            // COL G
            sbEnd.Append("").Append("\t");
            // COL H
            sbEnd.Append("").Append("\t");
            // COL I
            sbEnd.Append("").Append("\t");
            // COL J
            sbEnd.Append("").Append("\t");
            // COL K
            sbEnd.Append("").Append("\t");
            // COL L
            sbEnd.Append("").Append("\t");
            // COL M
            sbEnd.Append("").Append("\t");
            // COL N
            sbEnd.Append("").Append("\t");
            // COL O
            sbEnd.Append("").Append("\t");
            // COL O
            sbEnd.Append("").Append("\t");
            // COL O
            sbEnd.Append("").Append("\t");
            // COL O
            sbEnd.Append("").Append("\t");
            // COL O
            sbEnd.Append("").Append("\t");
            // COL O
            sbEnd.Append("").Append("\t");
            // COL O
            sbEnd.Append("").Append("\t");
            // COL O
            sbEnd.Append("").Append("\t");
            // COL W
            sbEnd.Append("").Append("\t");
            sw.WriteLine(sbEnd.ToString());
             * */
            sw.Close();
        }

        private void RenderBookings(string filePath, List<Booking> bookings, DateTime fileEndDate)
        {
            // windows latin 1 encoding - cfr ProAcc
            System.IO.StreamWriter sw = new System.IO.StreamWriter(filePath, false, Encoding.GetEncoding("ISO-8859-1"));
                
                //System.IO.File.CreateText(filePath);


            //int bookingNr = piddc.LastBookingNr + 1;

            int index = 1;

            foreach (Booking booking in bookings)
            {
                // New booking, create booking row
                int lineCounter = 0;
                if (!booking.HiddenFlag)
                {
                    booking.ExportCount++;
                    eBookReadManager.Context.SaveChanges();
                    foreach (BookingLine bookingLine in booking.BookingLines)
                    {
                        StringBuilder sb = new StringBuilder();
                        // COL A : Linecode, 1=new booking, 2=bookingline, 99=end
                        sb.Append(lineCounter == 0 ? "1" : "0").Append("\t");
                        // COL B : journalnr, must exist in ProAcc
                        //sb.Append(lineCounter == 0 ? piddc.DiversJournalId.ToString(): "0").Append("\t");
                        // COL C
                        //sb.Append(lineCounter == 0 ? bookingNr.ToString() : "0").Append("\t");
                        // COL D
                        sb.Append(fileEndDate.ToString("dd/MM/yyyy")).Append("\t");
                        // COL E
                        //sb.Append(piddc.LastPeriodId).Append("\t");
                        // COL F
                        //sb.Append("").Append("\t");
                        // COL G
                        //sb.Append("3").Append("\t");
                        // COL H
                        sb.Append(bookingLine.Account.VisualNr).Append("\t");
                        // COL I: analytisch
                        sb.Append(bookingLine.Amount.ToString()).Append("\t");
                        // COL J: bedrag
                        //sb.Append(bookingLine.Amount < 0 ? Math.Abs(bookingLine.Amount).ToString(CultureInfo.InvariantCulture.NumberFormat) : "D").Append("\t");
                        // COL K
                        //sb.Append(bookingLine.Amount < 0 ? "C" : "D").Append("\t");
                        // COL L
                        sb.Append(booking.Description).Append("\t");
                        // COL M
                        //sb.Append("0").Append("\t");
                        // COL N
                        //sb.Append("0").Append("\t");
                        // COL O
                        //sb.Append("EUR").Append("\t");
                        // COL P
                        //sb.Append("1").Append("\t");
                        // COL Q
                        //sb.Append(Math.Abs(bookingLine.Amount).ToString(CultureInfo.InvariantCulture.NumberFormat)).Append("\t");
                        // COL R
                        //sb.Append("0").Append("\t");
                        // COL S
                        //sb.Append("0").Append("\t");
                        // COL T
                        //sb.Append("0").Append("\t");
                        // COL U
                        //sb.Append("").Append("\t");
                        sw.WriteLine(sb.ToString());
                        index++;
                        lineCounter++;

                    }

                    //bookingNr++;
                }
            }
            /*
            // Add end of bookings.
            StringBuilder sbEnd = new StringBuilder();
            // COL A : Linecode, 1=new booking, 2=bookingline, 99=end
            sbEnd.Append("99").Append("\t");
            // COL B : journalnr, must exist in ProAcc
            sbEnd.Append("0").Append("\t");
            // COL C
            sbEnd.Append("0").Append("\t");
            // COL D
            sbEnd.Append("").Append("\t");
            // COL E
            sbEnd.Append("").Append("\t");
            // COL F
            sbEnd.Append("").Append("\t");
            // COL G
            sbEnd.Append("").Append("\t");
            // COL H
            sbEnd.Append("").Append("\t");
            // COL I: analytisch
            sbEnd.Append("").Append("\t");
            // COL J: bedrag
            sbEnd.Append("0").Append("\t");
            // COL K
            sbEnd.Append("").Append("\t");
            // COL L
            sbEnd.Append("").Append("\t");
            // COL M
            sbEnd.Append("0").Append("\t");
            // COL N
            sbEnd.Append("0").Append("\t");
            // COL O
            sbEnd.Append("").Append("\t");
            // COL P
            sbEnd.Append("0").Append("\t");
            // COL Q
            sbEnd.Append("0").Append("\t");
            // COL R
            sbEnd.Append("0").Append("\t");
            // COL S
            sbEnd.Append("0").Append("\t");
            // COL T
            sbEnd.Append("0").Append("\t");
            // COL U
            sbEnd.Append("").Append("\t");
            sw.WriteLine(sbEnd.ToString());
            */
            sw.Close();
        }

    }
}
