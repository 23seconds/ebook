﻿using System.Text;
using System.IO;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using DocumentFormat.OpenXml;
using System.Globalization;
using EY.com.eBook.API.Contracts.Data;
using System.Configuration;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Xml.Linq;
using System.Xml.XPath;
using EY.com.eBook.Core;
using EY.com.eBook.Core.EF;
using System.Data;
using System.Xml;

namespace EY.com.eBook.BL.Export
{
    
    public class ExportErelonen
    {

        
        private char[] letters = "ABCDEFGHIJKLMNO".ToCharArray();
        

        // Creates a SpreadsheetDocument.
        public string CreatePackage(Guid fileId)
        {
            Guid erelonenId = new Guid("3E9BB601-EA23-4072-BD49-E5DC61A6B5A8");
            Core.EF.Write.File file = eBookWriteManager.Context.Files.First(f => f.Id == fileId);
            Core.EF.Write.Client client = eBookWriteManager.Context.ClientSet.First(c => c.Id == file.ClientId);
            Core.EF.Write.Worksheet mws = eBookWriteManager.Context.Worksheets.FirstOrDefault(w => w.FileId == file.Id && w.TypeId == erelonenId);

            string templateFolder = Config.ExcelTemplates;
            string workingFolder = Config.ExcelWorkingFolder;
            string templateFileName = ConfigurationManager.AppSettings["eBook.Export.Templates.Erelonen"];
            string templatePath = Path.Combine(templateFolder,templateFileName);
            string filePath = Path.Combine(workingFolder,string.Format("{0}{1}",Guid.NewGuid().ToString(),Path.GetExtension(templateFileName)));
            System.IO.File.Copy(templatePath, filePath, true);

            XElement ws = XElement.Parse(mws.Data);
            
            XNamespace xns = ws.GetDefaultNamespace();
            XmlNamespaceManager xnsMg = new XmlNamespaceManager(new NameTable());
            xnsMg.AddNamespace(string.Empty,xns.NamespaceName);

            List<XElement> fiches = ws.Element(xns + "Fiches281_50").Elements(xns + "Fiche281_50").ToList();
             //= ws.XPathSelectElements("//Fiche281_50",xnsMg).ToList();

            //Open the copied template workbook. 
            using (SpreadsheetDocument myWorkbook = SpreadsheetDocument.Open(filePath, true))
            {
                //Access the main Workbook part, which contains all references.
                WorkbookPart workbookPart = myWorkbook.WorkbookPart;
                
                //Get the first worksheet. 
                WorksheetPart worksheetPart = GetWorksheetPartByName(myWorkbook,"fiche 281 50");
                // The SheetData object will contain all the data.
                


                //SetHeader(worksheetPart.Worksheet, title, client.Name, string.Format("{0} [{1}]",file.Name,file.EndDate.ToString("dd/MM/yyyy")));

                //SetColumnsHeaders(sheetData, fixedHeaders);

                uint index = 11;

                foreach (XElement item in fiches)
                {
                    bool run = true;
                    XElement ficheItem = item.Element(xns+"Fiche");
                    if (ficheItem != null)
                    {
                       run = (ficheItem.Value.ToLower() == "true");
                    }

                    if (run)
                    {
                        Row row = GetRow(worksheetPart.Worksheet, index);
                        FillInRow(index, item, ref row,xns);
                        index++;
                    }

                    //Append new row to sheet data
                    //sheetData.AppendChild(contentRow);
                }

                worksheetPart = GetWorksheetPartByName(myWorkbook, "aangifte");
                if (worksheetPart != null)
                {
                    FillInClientRows(client, ref worksheetPart);
                }
                // delete template rows
               // DeleteRow(worksheetPart.Worksheet, 2); 
                //DeleteRow(worksheetPart.Worksheet, 2);
                
                workbookPart.Workbook.CalculationProperties.ForceFullCalculation = true;
                workbookPart.Workbook.CalculationProperties.FullCalculationOnLoad = true;
                worksheetPart.Worksheet.Save();
            }
            System.IO.File.Move(filePath, Path.Combine(Config.PickupLocal, Path.GetFileName(filePath)));
            return Path.GetFileName(filePath);
        }

        private void FillInClientRows(Core.EF.Write.Client client, ref WorksheetPart worksheetPart)
        {
            Row row = GetRow(worksheetPart.Worksheet, 7);
            Cell cell = GetCell(row, 7, "B");
            cell.CellValue = new CellValue(client.EnterpriseNumber);
            cell.DataType = new EnumValue<CellValues>(CellValues.String);

            row = GetRow(worksheetPart.Worksheet, 10);
            cell = GetCell(row, 10, "B");
            cell.CellValue = new CellValue(client.Name);
            cell.DataType = new EnumValue<CellValues>(CellValues.String);

            row = GetRow(worksheetPart.Worksheet, 12);
            cell = GetCell(row, 12, "B");
            cell.CellValue = new CellValue(client.Address);
            cell.DataType = new EnumValue<CellValues>(CellValues.String);

            row = GetRow(worksheetPart.Worksheet, 13);
            cell = GetCell(row, 13, "B");
            cell.CellValue = new CellValue(client.ZipCode);
            cell.DataType = new EnumValue<CellValues>(CellValues.String);

            row = GetRow(worksheetPart.Worksheet, 14);
            cell = GetCell(row, 14, "B");
            cell.CellValue = new CellValue(client.City);
            cell.DataType = new EnumValue<CellValues>(CellValues.String);

        }

        private void FillInRow(uint index, XElement item,ref Row row, XNamespace xns)
        {
            XElement valueItem;
            Cell cell;
            // naam
            valueItem = item.Element(xns+"NaamLeverancier");
            cell = GetCell(row, index, "B");
            cell.CellValue = new CellValue(valueItem!=null ? valueItem.Value : "");
            cell.DataType =new EnumValue<CellValues>(CellValues.String);

            // voornaam
            valueItem = item.Element(xns+"VoornaamLeverancier");
            cell = GetCell(row, index, "C");
            cell.CellValue = new CellValue(valueItem != null ? valueItem.Value : "");
            cell.DataType = new EnumValue<CellValues>(CellValues.String);

            // adres
            valueItem = item.Element(xns+"AdresLeverancier");
            cell = GetCell(row, index, "D");
            cell.CellValue = new CellValue(valueItem!=null ? valueItem.Value : "");
            cell.DataType = new EnumValue<CellValues>(CellValues.String);

            bool belg = true;
            // postnummer 
            valueItem = item.Element(xns+"LandLeverancier");
            if (valueItem != null)
            {
                if (valueItem.Value.ToLower().StartsWith("belg"))
                {
                    cell = GetCell(row, index, "E");
                }
                else
                {
                    belg = false;
                    cell = GetCell(row, index, "G");
                }
            }
            else
            {
                cell = GetCell(row, index, "E");
            }
            valueItem = item.Element(xns+"PostcodeLeverancier");
            cell.CellValue = new CellValue(valueItem!=null ? valueItem.Value : "");
            cell.DataType = new EnumValue<CellValues>(CellValues.String);

            // woonplaats
            valueItem = item.Element(xns+"WoonplaatsLeverancier");
            cell = GetCell(row, index, "F");
            cell.CellValue = new CellValue(valueItem!=null ? valueItem.Value : "");
            cell.DataType = new EnumValue<CellValues>(CellValues.String);

            
            // Landcode & land
            valueItem = item.Element(xns+"LandLeverancier");
            cell = GetCell(row, index, "H"); // landcode
            cell.CellValue = new CellValue("0");
            cell.DataType = new EnumValue<CellValues>(CellValues.Number);
            cell = GetCell(row, index, "I"); // land
            cell.CellValue = new CellValue(valueItem != null ? valueItem.Value : "");
            cell.DataType = new EnumValue<CellValues>(CellValues.String);

            // Ondernemingsnummer
            valueItem = item.Element(xns+"AardVerkrijger");
            if (valueItem != null)
            {
                if (valueItem.Value == "1")
                {
                    cell = GetCell(row, index, "K");
                }
                else
                {
                    cell = GetCell(row, index, "J");
                }
            }
            else
            {
                cell = GetCell(row, index, "J");
            }
            if (!belg)
            {
                cell = GetCell(row, index, "L");
            }

            valueItem = item.Element(xns+"Ondernemingsnummer");
            cell.CellValue = new CellValue(valueItem != null ? valueItem.Value : "");
            cell.DataType = new EnumValue<CellValues>(CellValues.String);

            // aard verkijger
            valueItem = item.Element(xns+"AardVerkrijger");
            cell = GetCell(row, index, "N");
            if (valueItem != null && valueItem.Element("id").Value.ToLower() == "26dee86a-4648-474a-8050-1261d431404d")
            {
                cell.CellValue = new CellValue("1");
            }
            else if (valueItem != null && valueItem.Element("id").Value.ToLower() == "8fe420b3-32a2-4deb-858e-ef961175de15")
            {
                cell.CellValue = new CellValue("2");
            }
            cell.DataType = new EnumValue<CellValues>(CellValues.Number);

           //  UitgeoefendBeroep
            valueItem = item.Element(xns+"UitgeoefendBeroep");
            cell = GetCell(row, index, "AB");
            cell.CellValue = new CellValue(valueItem != null ? valueItem.Value : "");
            cell.DataType = new EnumValue<CellValues>(CellValues.String);

            // cijfers
            // Commissies
            valueItem = item.Element(xns+"Commissies");
            cell = GetCell(row, index, "AC");
            cell.CellValue = new CellValue(valueItem != null ? valueItem.Value : "");
            cell.DataType = new EnumValue<CellValues>(CellValues.Number);

            // erelonen
            valueItem = item.Element(xns+"Erelonen");
            cell = GetCell(row, index, "AD");
            cell.CellValue = new CellValue(valueItem != null ? valueItem.Value : "");
            cell.DataType = new EnumValue<CellValues>(CellValues.Number);

            // voordelen
            valueItem = item.Element(xns+"Voordelen");
            cell = GetCell(row, index, "AE");
            cell.CellValue = new CellValue(valueItem != null ? valueItem.Value : "");
            cell.DataType = new EnumValue<CellValues>(CellValues.Number);

            // voordelen oms
            valueItem = item.Element(xns+"VoordelenAlleAardOmschr");
            cell = GetCell(row, index, "AF");
            cell.CellValue = new CellValue(valueItem != null ? valueItem.Value : "");
            cell.DataType = new EnumValue<CellValues>(CellValues.String);

            // kosten
            valueItem = item.Element(xns+"Kosten");
            cell = GetCell(row, index, "AG");
            cell.CellValue = new CellValue(valueItem != null ? valueItem.Value : "");
            cell.DataType = new EnumValue<CellValues>(CellValues.Number);

            // werkelijk betaald
            valueItem = item.Element(xns+"Uitbetaald");
            if (valueItem != null)
            {
                switch (valueItem.Value.ToLower())
                {
                    case "nvt":
                        cell = GetCell(row, index, "AJ");
                        cell.CellValue = new CellValue("0");
                        cell.DataType = new EnumValue<CellValues>(CellValues.Number);
                        break;
                    case "nihil":
                        cell = GetCell(row, index, "AJ");
                        cell.CellValue = new CellValue("1");
                        cell.DataType = new EnumValue<CellValues>(CellValues.Number);
                        break;
                    case "0":
                        cell = GetCell(row, index, "AJ");
                        cell.CellValue = new CellValue("1");
                        cell.DataType = new EnumValue<CellValues>(CellValues.Number);
                        break;
                    default:
                        valueItem = item.Element(xns+"Uitbetaling");
                        cell = GetCell(row, index, "AI");
                        cell.CellValue = new CellValue(valueItem != null ? valueItem.Value : "0");
                        cell.DataType = new EnumValue<CellValues>(CellValues.Number);
                        break;
                }
            }
        }

        private Row GetRow(Worksheet worksheet, uint rowIndex)
        {
            return worksheet.GetFirstChild<SheetData>().
              Elements<Row>().Where(r => r.RowIndex == rowIndex).First();
        }

        private Cell GetCell(Row row, uint rowIndex, string columnName)
        {
            return row.Elements<Cell>().Where(c => string.Compare
                   (c.CellReference.Value, columnName +
                   rowIndex, true) == 0).First();
        }

        private WorksheetPart GetWorksheetPartByName(SpreadsheetDocument document,string sheetName)
        {
            IEnumerable<Sheet> sheets =
               document.WorkbookPart.Workbook.GetFirstChild<Sheets>().
               Elements<Sheet>().Where(s => s.Name == sheetName);

            if (sheets.Count() == 0)
            {
                // The specified worksheet does not exist.

                return null;
            }

            string relationshipId = sheets.First().Id.Value;
            WorksheetPart worksheetPart = (WorksheetPart)
                 document.WorkbookPart.GetPartById(relationshipId);
            return worksheetPart;

        }

    }
    
}
