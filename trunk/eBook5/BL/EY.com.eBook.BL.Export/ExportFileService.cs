﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EY.com.eBook.Core.EF;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.BL;
using Newtonsoft.Json;
using EY.com.eBook.Core.EF.Read;
using System.Data.Objects;


namespace EY.com.eBook.BL.Export
{
    public class ExportFileService : BaseExport<ClientPersonFileServiceDataContract>
    {
        private CriteriaClientPersonFileServiceDataContract _ccpfsdc;
        //private ClientRepository _clientRep;
        private List<ClientPersonFileServiceReport> _cpfsr;
        ObjectParameter recCount;

        public ExportFileService(CriteriaClientPersonFileServiceDataContract ccpfsdc)
        {
            if (!ccpfsdc.Limit.HasValue) ccpfsdc.Limit = 10000000;
            _ccpfsdc = ccpfsdc;
            LoadData();
            CreateWorkbook();
            SetCurrentWorksheet(1, "Clients");
            CreateHeaderCurrentWorksheet();
            FillMyData(_cpfsr);

            Id = SaveWorkbook();
        }

        protected override void LoadData()
        {
            API.BL.Read.FileServiceRepository fsr = new EY.com.eBook.API.BL.Read.FileServiceRepository();
            _cpfsr = fsr.QueryFileServiceReport(_ccpfsdc,out recCount);
            //_template = _ccpdc.TemplateName;
        }

        protected override void CreateHeaderCurrentWorksheet()
        {
            _worksheet.Cells[1, 1].Value = "Client Name";
            _worksheet.Cells[1, 2].Value = "Client GFIS";
            _worksheet.Cells[1, 3].Value = "End Date";
            _worksheet.Cells[1, 3].Value = "Next Annual Meeting";
            _worksheet.Cells[1, 4].Value = "Partner";
            _worksheet.Cells[1, 5].Value = "Manager";
            _worksheet.Cells[1, 6].Value = "Office";
            _worksheet.Cells[1, 7].Value = "Status";
            _worksheet.Cells[1, 8].Value = "Person";
            _worksheet.Cells[1, 9].Value = "Date Modified";
            _worksheet.Cells[1, 10].Value = "Days on Status";

            for (int i = 1; i < 11; i++)
            {
                _worksheet.Cells[1, i].Style.Font.Bold = true;
            }

            
        }

        protected override void FillData(List<ClientPersonFileServiceDataContract> list)
        {
            FillMyData(_cpfsr);
        }

        protected void FillMyData(List<ClientPersonFileServiceReport> lcpfsr)
        {
            int row = 2;
            foreach (var cpfsr in lcpfsr)
            {
                AddCell(row, 1, cpfsr.clientName);
                AddCell(row, 2, cpfsr.clientGFIS);
                AddCell(row, 3, cpfsr.endDate);
                AddCell(row, 3, cpfsr.nextAnnualMeeting);
                AddCell(row, 4, cpfsr.ppartner);
                AddCell(row, 5, cpfsr.pmanager);
                AddCell(row, 6, cpfsr.officeName);
                AddCell(row, 7, cpfsr.status);
                AddCell(row, 8, cpfsr.person);
                AddCell(row, 9, cpfsr.dateModified);
                AddCell(row, 10, cpfsr.daysOnStatus);
                row++;
            }
        }



        private void AddCell(int row, int cell, object value)
        {

            var range = _worksheet.Cells[row, cell];
            if (value == null)
            {
                value = String.Empty;
            }

            if (value.GetType().Name == typeof(DateTime).Name)
            {
                DateTime dte = (DateTime)value;
               // range.Style.Numberformat.Format = "dd/mm/jjjj uu:mm:ss";
                //range.Formula =string.Format("=DATE({0};{1};{2}) + TIME({3};{4};{5})",dte.Year,dte.Month,dte.Day,dte.Hour,dte.Minute,dte.Second);
                range.Value = dte.ToString("dd/MM/yyyy HH:mm:ss");
              //  range.Value = string.Format("=DATE({0};{1};{2}) + TIME({3};{4};{5})",dte.Year,dte.Month,dte.Day,dte.Hour,dte.Minute,dte.Second);
                setColumnWidth(_worksheet, row, cell, value.ToString());

            }
            else
            {
                range.Value = value;
            }
            if (value.GetType().Name == typeof(string).Name)
            {
                
                setColumnWidth(_worksheet, row, cell, value.ToString());
            }
        }
    }
}
