﻿using System.Text;
using System.IO;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using DocumentFormat.OpenXml;
using System.Globalization;
using EY.com.eBook.API.Contracts.Data;
using System.Configuration;
using System;
using System.Linq;
using System.Collections.Generic;

namespace EY.com.eBook.BL.Export
{
    /*
    public class ExportStatisticDetails
    {

        
        private char[] letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".ToCharArray();
        private CultureInfo dteCult = CultureInfo.CreateSpecificCulture("nl-BE");

        // Creates a SpreadsheetDocument.
        public string CreatePackage(List<StatisticDetailDataContract> stats,string[] fixedHeaders,string officeId)
        {
            
            string templateFolder = ConfigurationManager.AppSettings["eBook.Export.TemplateFolder"];
            string workingFolder = ConfigurationManager.AppSettings["eBook.Export.WorkingFolder"];
            string templateFileName = ConfigurationManager.AppSettings["eBook.Export.Templates.Statistics"];
            string templatePath = Path.Combine(templateFolder,templateFileName);
            string filePath = Path.Combine(workingFolder,string.Format("{0}{1}",Guid.NewGuid().ToString(),Path.GetExtension(templateFileName)));
            File.Copy(templatePath, filePath, true);

            List<string> headers = new List<string>();
            headers.AddRange(fixedHeaders); 

            //Open the copied template workbook. 
            using (SpreadsheetDocument myWorkbook = SpreadsheetDocument.Open(filePath, true))
            {
                //Access the main Workbook part, which contains all references.
                WorkbookPart workbookPart = myWorkbook.WorkbookPart;
                //Get the first worksheet. 
                WorksheetPart worksheetPart = workbookPart.WorksheetParts.First();
                // The SheetData object will contain all the data.
                SheetData sheetData = worksheetPart.Worksheet.GetFirstChild<SheetData>();


                //SetHeader(worksheetPart.Worksheet, title, client.Name, string.Format("{0} [{1}]",file.Name,file.EndDate.ToString("dd/MM/yyyy")));

                SetColumnsHeaders(sheetData, headers.ToArray());

                int index = 4;

                foreach(StatisticDetailDataContract stat in stats) {

                    foreach (OfficeDataContract office in stat.Offices)
                    {
                        if (string.IsNullOrEmpty(officeId) || office.Id == officeId)
                        {
                            Row contentRow = CreateContentRow(index, stat, office.Name, (Row)sheetData.ChildElements[(index % 2 == 0 ? 1 : 2)].Clone());
                            index++;

                            //Append new row to sheet data
                            sheetData.AppendChild(contentRow);
                        }
                    }
                }
                // delete template rows
                DeleteRow(worksheetPart.Worksheet, 2); 
                DeleteRow(worksheetPart.Worksheet, 2);

                worksheetPart.Worksheet.Save();
            }
            return filePath;
        }

        public void SetHeader(Worksheet worksheet, string itemTitle, string clientName, string fileName)
        {
            //string header = worksheet.Descendants<HeaderFooter>().FirstOrDefault().OddHeader.Text;
            //header = string.Format(header, itemTitle, clientName, fileName);
            //worksheet.Descendants<HeaderFooter>().FirstOrDefault().OddHeader.Text = header;
        }

        public void SetColumnsHeaders(SheetData sheetData, string[] titles)
        {
            Row r = sheetData.Elements<Row>().First();
            for (int i = 0; i < titles.Length; i++)
            {
                AddText(titles[i], r.Elements<Cell>().Skip(i).First(), letters[i].ToString(), 1);
            }
        }

        public void AddText(string text, Cell cell, string header, int index)
        {
            cell.DataType = CellValues.InlineString;
            cell.CellReference = header + index;
            cell.CellValue = new CellValue(text);
            cell.DataType = new EnumValue<CellValues>(CellValues.String);
        }

        public void AddNumber(decimal number, Cell cell, string header, int index)
        {

            cell.CellReference = header + index;
            cell.CellValue = new CellValue(number.ToString(CultureInfo.InvariantCulture));
            cell.DataType = new EnumValue<CellValues>(CellValues.Number);
        }

        public void DeleteRow(Worksheet worksheet, int rowIndex)
        {

            var deleteRow = worksheet.Descendants<Row>().Where(r => r.RowIndex == rowIndex).FirstOrDefault();
            deleteRow.Remove();

            foreach (Row row in worksheet.Descendants<Row>().
                                    Where(r => r.RowIndex.Value > rowIndex))
            {

                uint newRowIndex = System.Convert.ToUInt32(row.RowIndex.Value - 1);

                foreach (Cell cell in row.Elements<Cell>())
                {
                    // Update the references for reserved cells.
                    string cellReference = cell.CellReference.Value;
                    cell.CellReference = new StringValue(cellReference.Replace(
                      row.RowIndex.Value.ToString(), newRowIndex.ToString()));
                }

                // Update the row index.
                row.RowIndex = new UInt32Value(newRowIndex);
            }
        }

        public Row CreateContentRow(int index,StatisticDetailDataContract stat, string officeName, Row r)
        {
            //Create the new row.

            PersonRoleDataContract partner = stat.PersonRoles.Where(p => p.Role.ToLower().Contains("partner") && p.Person.DefaultOffice!=null && p.Person.DefaultOffice.Name == officeName && p.Person.Department=="ACR").FirstOrDefault();
            PersonRoleDataContract manager1 = stat.PersonRoles.Where(p => p.Role.ToLower().Contains("manager 1") && p.Person.DefaultOffice != null && p.Person.DefaultOffice.Name == officeName && p.Person.Department == "ACR").FirstOrDefault();
            PersonRoleDataContract manager2 = stat.PersonRoles.Where(p => p.Role.ToLower().Contains("manager 2") && p.Person.DefaultOffice != null && p.Person.DefaultOffice.Name == officeName && p.Person.Department == "ACR").FirstOrDefault();

            if(partner==null) partner = stat.PersonRoles.Where(p => p.Role.ToLower().Contains("partner") && p.Person.DefaultOffice != null && p.Person.DefaultOffice.Name == officeName).FirstOrDefault();
            if (manager1 == null) manager1 = stat.PersonRoles.Where(p => p.Role.ToLower().Contains("manager 1") && p.Person.DefaultOffice != null && p.Person.DefaultOffice.Name == officeName).FirstOrDefault();
            if (manager2 == null) manager2 = stat.PersonRoles.Where(p => p.Role.ToLower().Contains("manager 2") && p.Person.DefaultOffice != null && p.Person.DefaultOffice.Name == officeName).FirstOrDefault();

            r.RowIndex = (UInt32)index;
            
            AddText(stat.ClientName, r.Elements<Cell>().First(), letters[0].ToString(), index);
            AddText(stat.FileName, r.Elements<Cell>().Skip(1).First(), letters[1].ToString(), index);
            AddText(stat.StartDate.ToString("dd/MM/yyyy", dteCult), r.Elements<Cell>().Skip(2).First(), letters[2].ToString(), index);
            AddText(stat.EndDate.ToString("dd/MM/yyyy", dteCult), r.Elements<Cell>().Skip(3).First(), letters[3].ToString(), index);
            AddText(stat.CreationDate.ToString("dd/MM/yyyy HH:mm:ss", dteCult), r.Elements<Cell>().Skip(4).First(), letters[4].ToString(), index);
            AddText(stat.LastUpdatedDate.HasValue ? stat.LastUpdatedDate.Value.ToString("dd/MM/yyyy HH:mm:ss", dteCult) : "-", r.Elements<Cell>().Skip(5).First(), letters[5].ToString(), index);
            AddText(stat.ClosedDate.HasValue ? stat.ClosedDate.Value.ToString("dd/MM/yyyy HH:mm:ss", dteCult) : "-", r.Elements<Cell>().Skip(6).First(), letters[6].ToString(), index);
            AddText(officeName, r.Elements<Cell>().Skip(7).First(), letters[7].ToString(), index);
            AddText(partner != null ? string.Format("{0} {1}", partner.Person.FirstName, partner.Person.LastName) : "", r.Elements<Cell>().Skip(8).First(), letters[8].ToString(), index);
            AddText(manager1 != null ? string.Format("{0} {1}", manager1.Person.FirstName, manager1.Person.LastName) : "", r.Elements<Cell>().Skip(9).First(), letters[9].ToString(), index);
            AddText(manager2 != null ? string.Format("{0} {1}", manager2.Person.FirstName, manager2.Person.LastName) : "", r.Elements<Cell>().Skip(10).First(), letters[10].ToString(), index);
            return r;
        }

    }
    */
}
