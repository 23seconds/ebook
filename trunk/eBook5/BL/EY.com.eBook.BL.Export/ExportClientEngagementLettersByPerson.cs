﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EY.com.eBook.Core.EF;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.BL;
using Newtonsoft.Json;


namespace EY.com.eBook.BL.Export
{
    public class ExportClientEngagementLettersByPerson : BaseExport<ClientEngagementLetterDataContract>
    {
        private CriteriaPersonEngagementLetterDataContract _cpeldc;
        //private ClientRepository _clientRep;
        private List<ClientEngagementLetterDataContract> _lceldc;
        private string _template;
        private int i = 2;

        public ExportClientEngagementLettersByPerson(CriteriaPersonEngagementLetterDataContract cpeldc)
        {
            _cpeldc = cpeldc;
            LoadData();
            CreateWorkbook();
            SetCurrentWorksheet(1, "Clients");
            CreateHeaderCurrentWorksheet();
            FillData(_lceldc);

            Id = SaveWorkbook();
        }

        protected override void LoadData()
        {
            API.BL.Read.ClientRepository cr = new EY.com.eBook.API.BL.Read.ClientRepository();
            
            _lceldc = cr.GetClientEngagementLettersByPerson(_cpeldc).Data;
            //_template = _cceldc.TemplateName;            
        }

        protected override void CreateHeaderCurrentWorksheet()
        {
            if (_cpeldc.StartDateFrom == null) _cpeldc.StartDateFrom = new DateTime(1900, 01, 01);
            if (_cpeldc.StartDateTo == null) _cpeldc.StartDateTo = new DateTime(2050, 01, 01);
            if (_cpeldc.EndDateFrom == null) _cpeldc.EndDateFrom = new DateTime(1900, 01, 01);
            if (_cpeldc.EndDateTo == null) _cpeldc.EndDateTo = new DateTime(2050, 01, 01);

            //string startDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(_cpeldc.StartDateFrom, "W. Europe Standard Time").ToShortDateString();
            //string endDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(_cceldc.EndDate, "W. Europe Standard Time").ToShortDateString();

            _worksheet.Cells[1, 1].Value = "Startdate range: ";
            _worksheet.Cells[1, 2].Value = _cpeldc.StartDateFrom + " - " + _cpeldc.StartDateTo;
            _worksheet.Cells[2, 1].Value = "Enddate range: ";
            _worksheet.Cells[2, 2].Value = _cpeldc.EndDateFrom + " - " + _cpeldc.EndDateTo;
            
            if (_cpeldc.StatusId != null)
            {
                i++;

                Core.EF.Read.RepositoryStatusItem repstatitem = Core.EF.eBookReadManager.Context.RepositoryStatusItems.FirstOrDefault(r => r.Id == _cpeldc.StatusId && r.Culture == "en-US");
                string status = repstatitem.Description;

                _worksheet.Cells[i, 1].Value = "Status: ";
                _worksheet.Cells[i, 2].Value = status;
            }

            if (_cpeldc.TemplateId != null)
            {
                i++;

                Core.EF.Read.Names nameItem = Core.EF.eBookReadManager.Context.NamesSet.FirstOrDefault(r => r.ObjectId == _cpeldc.TemplateId && r.Culture == "en-US");
                string template = nameItem.Description;

                _worksheet.Cells[i, 1].Value = "Template: ";
                _worksheet.Cells[i, 2].Value = template;
            }

            if (_cpeldc.Gth != null && _cpeldc.Gth != "%") {
                i++;

                _worksheet.Cells[i, 1].Value = "GTH validated: ";
                _worksheet.Cells[i, 2].Value = _cpeldc.Gth;
            }

            if (!string.IsNullOrEmpty(_cpeldc.ClientName) && _cpeldc.ClientName != "%") {
                i++;

                _worksheet.Cells[i, 1].Value = "Clientname: ";
                _worksheet.Cells[i, 2].Value = _cpeldc.ClientName;                  
            }

            if (!string.IsNullOrEmpty(_cpeldc.Expiration) && _cpeldc.Expiration != "0")
            {
                i++;
                switch (_cpeldc.Expiration){
                    case "30":
                        _cpeldc.Expiration = "1 month";
                        break;
                    case "60":
                        _cpeldc.Expiration = "2 month";
                        break;
                    case "90":
                        _cpeldc.Expiration = "3 month";
                        break;
                    case "120":
                        _cpeldc.Expiration = "4 month";
                        break;
                    case "150":
                        _cpeldc.Expiration = "5 month";
                        break;
                    case "180":
                        _cpeldc.Expiration = "6 month";
                        break;
                    case "356":
                        _cpeldc.Expiration = "1 year";
                        break;
                    case "712":
                        _cpeldc.Expiration = "2 year";
                        break;
                    case "1068":
                        _cpeldc.Expiration = "3 year";
                        break;
                    case "1424":
                        _cpeldc.Expiration = "4 year";
                        break;
                    case "1780":
                        _cpeldc.Expiration = "5 year";
                        break;

                }
                

                _worksheet.Cells[i, 1].Value = "Expiration: ";
                _worksheet.Cells[i, 2].Value = _cpeldc.Expiration;   
            }

            if (_cpeldc.RenewalOverview.GetValueOrDefault()) {
                i++;

                _worksheet.Cells[i, 1].Value = "Renewal: ";
                _worksheet.Cells[i, 2].Value = _cpeldc.RenewalOverview.ToString();  
            }

            _worksheet.Cells[1, 1, i, 1].Style.Font.Bold = true;

            // whiteline
            i++;


            // headers
            i++;

            int j = 1;
            _worksheet.Cells[i, j].Value = "Client name";
            setColumnWidth(_worksheet, 1, j, _worksheet.Cells[i, j].Value.ToString());
            j++;

            _worksheet.Cells[i,j].Value = "Client GFIS";
            setColumnWidth(_worksheet, 1, j, _worksheet.Cells[i, j].Value.ToString());
            j++;

            _worksheet.Cells[i, j].Value = "Partner";
            setColumnWidth(_worksheet, 1, j, _worksheet.Cells[i, j].Value.ToString());
            j++;

            _worksheet.Cells[i, j].Value = "Manager";
            setColumnWidth(_worksheet, 1, j, _worksheet.Cells[i, j].Value.ToString());
            j++;

            _worksheet.Cells[i, j].Value = "Office";
            setColumnWidth(_worksheet, 1, j, _worksheet.Cells[i, j].Value.ToString());
            j++;

            _worksheet.Cells[i, j].Value = "Date of the EL/EA";
            setColumnWidth(_worksheet, 1, j, _worksheet.Cells[i, j].Value.ToString());
            j++;

            _worksheet.Cells[i, j].Value = "Startdate";
            setColumnWidth(_worksheet, 1, j, _worksheet.Cells[i, j].Value.ToString());
            j++;

            _worksheet.Cells[i, j].Value = "Enddate";
            setColumnWidth(_worksheet, 1, j, _worksheet.Cells[i, j].Value.ToString());
            j++;

            //days to expiration

            //to be renewed

            _worksheet.Cells[i, j].Value = "Template";
            setColumnWidth(_worksheet, 1, j, _worksheet.Cells[i, j].Value.ToString());
            j++;

            _worksheet.Cells[i, j].Value = "Status";
            setColumnWidth(_worksheet, 1, j, _worksheet.Cells[i, j].Value.ToString());
            j++;

            _worksheet.Cells[i, j].Value = "Filename";
            setColumnWidth(_worksheet, 1, j, _worksheet.Cells[i, j].Value.ToString());
            j++;

            _worksheet.Cells[i, j].Value = "Validated by GTH";
            setColumnWidth(_worksheet, 1, j, _worksheet.Cells[i, j].Value.ToString());
            j++;

            _worksheet.Cells[i, 1, i, j].Style.Font.Bold = true;
            _worksheet.Cells[i, 1, i, j].AutoFilter = true;
        }

        protected override void FillData(List<ClientEngagementLetterDataContract> lceldc)
        {
            
            
            foreach (var celdc in lceldc)
            {
                //string startDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(celdc.StartDate, "W. Europe Standard Time").ToShortDateString();
                //string endDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(celdc.EndDate, "W. Europe Standard Time").ToShortDateString();

                i++;
                ListItemDataContract lidc =  new ListItemDataContract();
                if (!string.IsNullOrEmpty(celdc.template))
                {
                    var objTemplate = JsonConvert.DeserializeObject(celdc.template);
                    lidc = JsonConvert.DeserializeObject<ListItemDataContract>(celdc.template);
                }

                

                _worksheet.Cells[i, 1].Value = celdc.ClientName != null ? celdc.ClientName : "";
                _worksheet.Cells[i, 2].Value = celdc.ClientGfis != null ? celdc.ClientGfis : "";
                _worksheet.Cells[i, 3].Value = celdc.Partner != null ? celdc.Partner : "";
                _worksheet.Cells[i, 4].Value = celdc.Manager != null ? celdc.Manager : "";
                _worksheet.Cells[i, 5].Value = celdc.Office != null ? celdc.Office : "";
                _worksheet.Cells[i, 6].Value = celdc.DateELEA != null ? celdc.DateELEA : "";
                _worksheet.Cells[i, 7].Value = celdc.StartDate != null ? celdc.StartDate.Value.ToString("dd-MM-yyyy") : "";
                _worksheet.Cells[i, 8].Value = celdc.EndDate != null ? celdc.EndDate.Value.ToString("dd-MM-yyyy") : "";                
                //expiration
                //renewal
                _worksheet.Cells[i, 9].Value = lidc.NL != null ? lidc.NL : "";
                _worksheet.Cells[i, 10].Value = celdc.status != null ? celdc.status : "";
                _worksheet.Cells[i, 11].Value = celdc.FileName != null ? celdc.FileName : "";
                _worksheet.Cells[i, 12].Value = celdc.GTHValidated != null ? celdc.GTHValidated : "";
            }
            

        }

        
    }
}
