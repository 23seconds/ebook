﻿using System.Text;
using System.IO;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using DocumentFormat.OpenXml;
using System.Globalization;
using EY.com.eBook.API.Contracts.Data;
using System.Configuration;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Security.Principal;
using EY.com.eBook.Core;

namespace EY.com.eBook.BL.Export
{
    /*
    public class ExportAllBookings
    {
        
        
        private char[] letters = "ABCDEFGH".ToCharArray();
        

        // Creates a SpreadsheetDocument.
        public string CreatePackage(FileBaseDataContract file,ClientBaseDataContract client,
                                    List<BookingDataContract> bookings,
                                    List<ProAccBookingDataContract> proaccBookings,
                                    string title, string[] fixedHeaders, string culture)
        {
            
            string templateFolder = ConfigurationManager.AppSettings["eBook.Export.TemplateFolder"];
            string workingFolder = ConfigurationManager.AppSettings["eBook.Export.WorkingFolder"];
            string templateFileName = ConfigurationManager.AppSettings["eBook.Export.Templates.AllBookings"];
            string templatePath = Path.Combine(templateFolder,templateFileName);
            string filePath = Path.Combine(workingFolder,string.Format("{0}{1}",Guid.NewGuid().ToString(),Path.GetExtension(templateFileName)));
            File.Copy(templatePath, filePath, true);

            using (new Impersonator("ebookid", "eurw", "gu5raYApHefuCh"))
            {


                //Open the copied template workbook. 
                using (SpreadsheetDocument myWorkbook = SpreadsheetDocument.Open(filePath, true))
                {
                    //Access the main Workbook part, which contains all references.
                    WorkbookPart workbookPart = myWorkbook.WorkbookPart;
                    //Get the first worksheet. 
                    WorksheetPart worksheetPart = workbookPart.WorksheetParts.First();
                    // The SheetData object will contain all the data.
                    SheetData sheetData = worksheetPart.Worksheet.GetFirstChild<SheetData>();


                    SetHeader(worksheetPart.Worksheet, title, client.Name, string.Format("{0} [{1}]", file.Name, file.EndDate.ToString("dd/MM/yyyy")));

                    SetColumnsHeaders(sheetData, fixedHeaders);

                    int index = 4;

                    // PROACC FIRST
                    foreach (ProAccBookingDataContract pBooking in proaccBookings)
                    {

                        foreach (AccountHistoryLineDataContract pLine in pBooking.BookingLines)
                        {
                            Row contentRow = CreateContentRow(index, pBooking, pLine, culture, (Row)sheetData.ChildElements[(index % 2 == 0 ? 1 : 2)].Clone());
                            index++;
                            //Append new row to sheet data
                            sheetData.AppendChild(contentRow);
                        }
                    }

                    // THEN eBook
                    foreach (BookingDataContract booking in bookings)
                    {
                        foreach (BookingLineDataContract line in booking.BookingLines)
                        {
                            Row contentRow = CreateContentRow(index, booking, line, file, (Row)sheetData.ChildElements[(index % 2 == 0 ? 1 : 2)].Clone());
                            index++;
                            //Append new row to sheet data
                            sheetData.AppendChild(contentRow);
                        }
                    }
                    // delete template rows
                    DeleteRow(worksheetPart.Worksheet, 2);
                    DeleteRow(worksheetPart.Worksheet, 2);

                    worksheetPart.Worksheet.Save();
                }
            }
            return filePath;
        }

        private Row CreateContentRow(int index, BookingDataContract booking, BookingLineDataContract line, FileBaseDataContract file, Row r)
        {
            r.RowIndex = (UInt32)index;

            AddText("EBOOK", r.Elements<Cell>().First(), letters[0].ToString(), index);
            AddNumber(booking.Nr, r.Elements<Cell>().Skip(1).First(), letters[1].ToString(), index);
            AddText(file.EndDate.ToShortDateString(), r.Elements<Cell>().Skip(2).First(), letters[2].ToString(), index);
            AddText(booking.Description, r.Elements<Cell>().Skip(3).First(), letters[3].ToString(), index);
            AddText(line.AccountNr.Substring(0, 8), r.Elements<Cell>().Skip(4).First(), letters[4].ToString(), index);
            AddText(line.AccountDescription, r.Elements<Cell>().Skip(5).First(), letters[5].ToString(), index);
           

            if (line.Amount >=0)
            {
                AddNumber(line.Amount, r.Elements<Cell>().Skip(6).First(), letters[6].ToString(), index);
                AddText(" ", r.Elements<Cell>().Skip(7).First(), letters[7].ToString(), index);
            }else {
                AddText(" ", r.Elements<Cell>().Skip(6).First(), letters[6].ToString(), index);
                AddNumber(Math.Abs(line.Amount), r.Elements<Cell>().Skip(7).First(), letters[7].ToString(), index);
            }
            return r;
        }

        private Row CreateContentRow(int index, ProAccBookingDataContract pBooking, AccountHistoryLineDataContract pLine, string culture, Row r)
        {
            r.RowIndex = (UInt32)index;

            AddText("PROACC", r.Elements<Cell>().First(), letters[0].ToString(), index);
            AddNumber(pBooking.BookingNr, r.Elements<Cell>().Skip(1).First(), letters[1].ToString(), index);

            if (pBooking.TransactionDate.HasValue)
            {
                AddText(pBooking.TransactionDate.Value.ToShortDateString(), r.Elements<Cell>().Skip(2).First(), letters[2].ToString(), index);
            }
            else
            {
                AddText(" ", r.Elements<Cell>().Skip(2).First(), letters[2].ToString(), index);
            }
            AddText(pBooking.BookingDescription, r.Elements<Cell>().Skip(3).First(), letters[3].ToString(), index);
            AddText(pLine.AccountNr.Substring(0,8), r.Elements<Cell>().Skip(4).First(), letters[4].ToString(), index);
            AccountTranslationDataContract atdc = pLine.Translations.FirstOrDefault(t => t.Culture == culture);
            if (atdc != null)
            {
                AddText(atdc.Description, r.Elements<Cell>().Skip(5).First(), letters[5].ToString(), index);
            }
            else
            {
                atdc = pLine.Translations.FirstOrDefault();
                if (atdc != null)
                {
                    AddText(atdc.Description, r.Elements<Cell>().Skip(5).First(), letters[5].ToString(), index);
                }
                else
                {
                    AddText(" - ", r.Elements<Cell>().Skip(5).First(), letters[5].ToString(), index);
                }
            }

            if (pLine.Debet != 0)
            {
                AddNumber(pLine.Debet, r.Elements<Cell>().Skip(6).First(), letters[6].ToString(), index);
            }
            else
            {
                AddText(" ", r.Elements<Cell>().Skip(6).First(), letters[6].ToString(), index);
            }
            if (pLine.Credit != 0)
            {
                AddNumber(pLine.Credit, r.Elements<Cell>().Skip(7).First(), letters[7].ToString(), index);
            }
            else
            {
                AddText(" ", r.Elements<Cell>().Skip(7).First(), letters[7].ToString(), index);
            }
            return r;
        }

        public void SetHeader(Worksheet worksheet, string itemTitle, string clientName, string fileName)
        {
            string header = worksheet.Descendants<HeaderFooter>().FirstOrDefault().OddHeader.Text;
            header = string.Format(header, itemTitle, clientName, fileName);
            worksheet.Descendants<HeaderFooter>().FirstOrDefault().OddHeader.Text = header;
        }

        public void SetColumnsHeaders(SheetData sheetData, string[] titles)
        {
            Row r = sheetData.Elements<Row>().First();
            for (int i = 0; i < titles.Length; i++)
            {
                AddText(titles[i], r.Elements<Cell>().Skip(i).First(), letters[i].ToString(), 1);
            }
        }

        public void AddText(string text, Cell cell, string header, int index)
        {
            cell.DataType = CellValues.InlineString;
            cell.CellReference = header + index;
            cell.CellValue = new CellValue(text);
            cell.DataType = new EnumValue<CellValues>(CellValues.String);
        }

        public void AddNumber(decimal number, Cell cell, string header, int index)
        {

            cell.CellReference = header + index;
            cell.CellValue = new CellValue(number.ToString(CultureInfo.InvariantCulture));
            cell.DataType = new EnumValue<CellValues>(CellValues.Number);
        }

        public void AddDate(DateTime dte, Cell cell, string header, int index)
        {
            
            cell.CellReference = header + index;
            cell.DataType = new EnumValue<CellValues>(CellValues.Date);
            cell.CellValue = new CellValue(ToJulianDate(dte).ToString(CultureInfo.InvariantCulture));
        }

        public void DeleteRow(Worksheet worksheet, int rowIndex)
        {

            var deleteRow = worksheet.Descendants<Row>().Where(r => r.RowIndex == rowIndex).FirstOrDefault();
            deleteRow.Remove();

            foreach (Row row in worksheet.Descendants<Row>().
                                    Where(r => r.RowIndex.Value > rowIndex))
            {

                uint newRowIndex = System.Convert.ToUInt32(row.RowIndex.Value - 1);

                foreach (Cell cell in row.Elements<Cell>())
                {
                    // Update the references for reserved cells.
                    string cellReference = cell.CellReference.Value;
                    cell.CellReference = new StringValue(cellReference.Replace(
                      row.RowIndex.Value.ToString(), newRowIndex.ToString()));
                }

                // Update the row index.
                row.RowIndex = new UInt32Value(newRowIndex);
            }
        }

        public static double ToJulianDate(DateTime date)
        {
            return date.ToOADate() + 2415018.5;
        }
        

    }
    */
}
