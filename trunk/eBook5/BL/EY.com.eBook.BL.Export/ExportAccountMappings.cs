﻿using System.Text;
using System.IO;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using DocumentFormat.OpenXml;
using System.Globalization;
using EY.com.eBook.API.Contracts.Data;
using System.Configuration;
using System;
using System.Linq;
using System.Collections.Generic;
using EY.com.eBook.Core;
using EY.com.eBook.API.Contracts.Proxies;
using EY.com.eBook.API.Contracts.Data.Cache;

namespace EY.com.eBook.BL.Export
{
    public class ExportAccountMappings
    {

        
        private char[] letters = "ABCDEFGHIJKLMNOP".ToCharArray();
        private bool _detailed;
        private List<WorksheetTypeDataContract> _worksheetTypes;
        private List<Core.EF.Write.Mapping> _mappings;
        private List<Core.EF.Write.MappingItemTranslation> _itemTranslations;

        // Creates a SpreadsheetDocument.
        public string CreatePackage(Guid fileId, string culture)
        {

            Core.EF.Write.File file = Core.EF.eBookWriteManager.Context.Files.First(f => f.Id == fileId);
            Core.EF.Write.Client client = Core.EF.eBookWriteManager.Context.ClientSet.First(c => c.Id == file.ClientId);


            string title = GetTranslation("MAPPING.TITLE", culture, "mappings");

            string[] fixedColumnHeaders = new string[] { "", "", "" };
            fixedColumnHeaders[0] = GetTranslation("MAPPING.ACCOUNTNR", culture, "Account Nr");
            fixedColumnHeaders[1] = GetTranslation("MAPPING.ACCOUNTDESCRIPTION", culture, "Account description");
            fixedColumnHeaders[2] = GetTranslation("MAPPING.SALDO", culture, "Balans");


            _mappings = EY.com.eBook.Core.EF.eBookWriteManager.Context.Mappings.OrderBy(m => m.Order).ToList();
            List<Core.EF.Write.MappingTranslation> mappingTranslations = EY.com.eBook.Core.EF.eBookWriteManager.Context.MappingTranslations.Where(m => m.Culture == culture).ToList();

            _itemTranslations = Core.EF.eBookWriteManager.Context.MappingItemTranslations.Where(c => c.Culture == culture).ToList();

            CacheProxy cp = new CacheProxy();
            List<AccountDataContract> accounts = cp.GetAllAccounts(fileId, true, true, culture);
            cp.Close();

            accounts = accounts.Where(a =>a.Mappings!=null && a.Mappings.Count(m => m.MappingItemId.HasValue) > 0).ToList();


            string templateFolder = Config.ExcelTemplates;
            string workingFolder = Config.ExcelWorkingFolder;
            string templateFileName = ConfigurationManager.AppSettings["eBook.Export.Templates.Mappings"];
            string templatePath = Path.Combine(templateFolder,templateFileName);
            string filePath = Path.Combine(workingFolder,string.Format("{0}{1}",Guid.NewGuid().ToString(),Path.GetExtension(templateFileName)));
            File.Copy(templatePath, filePath, true);

            List<string> headers = new List<string>();
            headers.AddRange(fixedColumnHeaders);
            foreach (Core.EF.Write.Mapping mapping in _mappings)
            {
                Core.EF.Write.MappingTranslation mt = mappingTranslations.FirstOrDefault(m => m.Key == mapping.Key);
                headers.Add(mt!=null?mt.Description : mapping.Key);
            }

            //Open the copied template workbook. 
            using (SpreadsheetDocument myWorkbook = SpreadsheetDocument.Open(filePath, true))
            {
                //Access the main Workbook part, which contains all references.
                WorkbookPart workbookPart = myWorkbook.WorkbookPart;
                //Get the first worksheet. 
                WorksheetPart worksheetPart = workbookPart.WorksheetParts.First();
                // The SheetData object will contain all the data.
                SheetData sheetData = worksheetPart.Worksheet.GetFirstChild<SheetData>();


                SetHeader(worksheetPart.Worksheet, title, client.Name, string.Format("{0} [{1}]",file.Name,file.EndDate.ToString("dd/MM/yyyy")));

                SetColumnsHeaders(sheetData, headers.ToArray());

                int index = 4;

                foreach(AccountDataContract adc in accounts) {
                    int count = sheetData.ChildElements.Count();
                    Row contentRow = CreateContentRow(index,adc, (Row)sheetData.ChildElements[(index % 2 == 0 ? 1 : 2)].Clone(),culture);
                    index++;

                    //Append new row to sheet data
                    sheetData.AppendChild(contentRow);
                }
                // delete template rows
                DeleteRow(worksheetPart.Worksheet, 2); 
                DeleteRow(worksheetPart.Worksheet, 2);

                worksheetPart.Worksheet.Save();
            }

            System.IO.File.Move(filePath, Path.Combine(Config.PickupLocal, Path.GetFileName(filePath)));
            return Path.GetFileName(filePath);
        }

        public void SetHeader(Worksheet worksheet, string itemTitle, string clientName, string fileName)
        {
            string header = worksheet.Descendants<HeaderFooter>().FirstOrDefault().OddHeader.Text;
            header = string.Format(header, itemTitle, clientName, fileName);
            worksheet.Descendants<HeaderFooter>().FirstOrDefault().OddHeader.Text = header;
        }

        public void SetColumnsHeaders(SheetData sheetData, string[] titles)
        {
            Row r = sheetData.Elements<Row>().First();
            var iCellCount = r.Elements<Cell>().Count();

            for (int i = 0; i < titles.Length; i++)
            {
                Cell cell = null;

                if ((r.Elements<Cell>().Skip(i).Count() == 0)) //Issue where template count is lower than given titles, then create cell
                {
                    cell = new Cell();
                }
                else{
                    cell = r.Elements<Cell>().Skip(i).First();
                }

                AddText(titles[i], cell, letters[i].ToString(), 1);
            }
        }

        public void AddText(string text, Cell cell, string header, int index)
        {
            cell.DataType = CellValues.InlineString;
            cell.CellReference = header + index;
            cell.CellValue = new CellValue(text);
            cell.DataType = new EnumValue<CellValues>(CellValues.String);
        }

        public void AddNumber(decimal number, Cell cell, string header, int index)
        {

            cell.CellReference = header + index;
            cell.CellValue = new CellValue(number.ToString(CultureInfo.InvariantCulture));
            cell.DataType = new EnumValue<CellValues>(CellValues.Number);
        }

        public void DeleteRow(Worksheet worksheet, int rowIndex)
        {

            var deleteRow = worksheet.Descendants<Row>().Where(r => r.RowIndex == rowIndex).FirstOrDefault();
            deleteRow.Remove();

            foreach (Row row in worksheet.Descendants<Row>().
                                    Where(r => r.RowIndex.Value > rowIndex))
            {

                uint newRowIndex = System.Convert.ToUInt32(row.RowIndex.Value - 1);

                foreach (Cell cell in row.Elements<Cell>())
                {
                    // Update the references for reserved cells.
                    string cellReference = cell.CellReference.Value;
                    cell.CellReference = new StringValue(cellReference.Replace(
                      row.RowIndex.Value.ToString(), newRowIndex.ToString()));
                }

                // Update the row index.
                row.RowIndex = new UInt32Value(newRowIndex);
            }
        }

        public Row CreateContentRow(int index,AccountDataContract adc, Row r, string culture)
        {
            //Create the new row.

            AccountDescriptionDataContract desc = adc.Descriptions.FirstOrDefault(d => d.Culture == culture);
            if (desc==null && adc.Descriptions.Count>0) desc = adc.Descriptions.First();
            string sd = desc == null ? string.Empty : desc.Description;

            r.RowIndex = (UInt32)index;
            
            AddText(adc.VisualNr, r.Elements<Cell>().First(), letters[0].ToString(), index);
            AddText(sd, r.Elements<Cell>().Skip(1).First(), letters[1].ToString(), index);
            AddNumber(adc.Saldo, r.Elements<Cell>().Skip(2).First(), letters[2].ToString(), index);

            int i = 3;
            foreach(Core.EF.Write.Mapping map in _mappings) {
                AccountMappingDataContract amdc = adc.Mappings.Where(m => m.MappingKey == map.Key).FirstOrDefault();

                Cell cell = null;

                if (r.Elements<Cell>().Skip(i).Count() == 0) //Issue where no cell is available, then create cell
                {
                    cell = new Cell();
                }
                else
                {
                    cell = r.Elements<Cell>().Skip(i).First();
                }

                if (amdc != null)
                {
                    if (amdc.MappingItemId.HasValue)
                    {
                       Core.EF.Write.MappingItemTranslation mit = _itemTranslations.FirstOrDefault(m => m.Id == amdc.MappingItemId.Value);
                       string name = mit != null ? mit.Description : "X";
                       AddText(name, cell, letters[i].ToString(), index);
                    }
                    else
                    {
                        AddText("", cell, letters[i].ToString(), index);
                    }
                }
                else
                {
                    AddText("", cell, letters[i].ToString(), index);
                }
                i++;
            }

            return r;
        }

        private string GetTranslation(string key, string culture, string defaultText)
        {
            string trans = GetTranslation(key, culture);
            if (string.IsNullOrEmpty(trans)) trans = defaultText;
            return trans;
        }

        private string GetTranslation(string key, string culture)
        {
            Core.EF.Read.InterfaceTranslation it = EY.com.eBook.Core.EF.eBookReadManager.Context.InterfaceTranslations
                                                                .Where(i => i.Interface.ToUpper() == "EXPORT" &&
                                                                    i.Key.ToUpper() == key.ToUpper() &&
                                                                    i.Culture.ToUpper() == culture.ToUpper())
                                                                .FirstOrDefault();

            if (it == null) return null;
            return it.Translation;
        }

    }
  
}
