﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EY.com.eBook.Core.EF;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.BL;
using Newtonsoft.Json;


namespace EY.com.eBook.BL.Export
{
    public class ExportClientEngagementLetterReport : BaseExport<ClientEngagementLetterDataContract>
    {
        private CriteriaClientEngagementLetterDataContract _cceldc;
        //private ClientRepository _clientRep;
        private List<ClientEngagementLetterDataContract> _lceldc;
        private string _template;

        public ExportClientEngagementLetterReport(CriteriaClientEngagementLetterDataContract cceldc)
        {
            _cceldc = cceldc;
            LoadData();
            CreateWorkbook();
            SetCurrentWorksheet(1, "Clients");
            CreateHeaderCurrentWorksheet();
            FillData(_lceldc);

            Id = SaveWorkbook();
        }

        protected override void LoadData()
        {
            API.BL.Read.AdministrationRepository a = new EY.com.eBook.API.BL.Read.AdministrationRepository();
            _lceldc = a.GetClientEngagementLetterReport(_cceldc);
            _template = _cceldc.TemplateName;            
        }

        protected override void CreateHeaderCurrentWorksheet()
        {
            string status = null;
            Guid g = new Guid();
            if (_cceldc.StatusId != null) {
                g = new Guid(_cceldc.StatusId);
                Core.EF.Read.RepositoryStatusItem repstatitem = Core.EF.eBookReadManager.Context.RepositoryStatusItems.FirstOrDefault(r => r.Id == g && r.Culture == "en-US");
                status = repstatitem.Description;
            }            
            
            string startDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(_cceldc.StartDate, "W. Europe Standard Time").ToShortDateString();
            string endDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(_cceldc.EndDate, "W. Europe Standard Time").ToShortDateString();
            
            _worksheet.Cells[1,1].Value = "Date range: ";
            _worksheet.Cells[1, 2].Value = startDate + " - " + endDate; 
            _worksheet.Cells[2,1].Value = "Status: ";
            _worksheet.Cells[2, 2].Value = status;
            _worksheet.Cells[3,1].Value = "Template: ";
            _worksheet.Cells[3, 2].Value = _template;
            _worksheet.Cells[1, 1, 3, 1].Style.Font.Bold = true;

            _worksheet.Cells[5, 1].Value = "Client name";
            _worksheet.Cells[5, 1].Style.Font.Bold = true;
            _worksheet.Cells[5, 2].Value = "Status";
            _worksheet.Cells[5, 2].Style.Font.Bold = true;
            _worksheet.Cells[5, 3].Value = "Template";
            _worksheet.Cells[5, 3].Style.Font.Bold = true;
        }

        protected override void FillData(List<ClientEngagementLetterDataContract> lceldc)
        {
            int row = 6;
            foreach (var celdc in lceldc)
            {
                var objTemplate = JsonConvert.DeserializeObject(celdc.template);
                ListItemDataContract lidc = JsonConvert.DeserializeObject<ListItemDataContract>(celdc.template);
                
                _worksheet.Cells[row, 1].Value = celdc.ClientName;
                setColumnWidth(_worksheet, row, 1, celdc.ClientName);
                _worksheet.Cells[row, 2].Value = celdc.status;
                setColumnWidth(_worksheet, row, 2, celdc.status);
                _worksheet.Cells[row, 3].Value = lidc.NL;
                setColumnWidth(_worksheet, row, 3, lidc.NL);
                row++;
            }

        }  
    }
}
