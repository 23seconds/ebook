﻿using System.Text;
using System.IO;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using DocumentFormat.OpenXml;
using System.Globalization;
using EY.com.eBook.API.Contracts.Data;
using System.Configuration;
using System;
using System.Linq;
using System.Collections.Generic;
using EY.com.eBook.API.Contracts.Data.Cache;
using EY.com.eBook.Core;
using EY.com.eBook.API.Contracts.Proxies;

namespace EY.com.eBook.BL.Export
{
    
    public class ExportFinalTrial
    {

        
        private char[] letters = "ABCDE".ToCharArray();
        

        // Creates a SpreadsheetDocument.
        public string CreatePackage(Guid fileId, string culture)
        {

            Core.EF.Write.File file = Core.EF.eBookWriteManager.Context.Files.First(f => f.Id == fileId);
            Core.EF.Write.Client client = Core.EF.eBookWriteManager.Context.ClientSet.First(c => c.Id == file.ClientId);


            string title = GetTranslation("FINALTRIAL.TITLE", culture, "Final trial balance");

            string[] fixedColumnHeaders = new string[] { "", "", "", "", "" };
            fixedColumnHeaders[0] = GetTranslation("FINALTRIAL.ACCOUNT", culture, "Account");
            fixedColumnHeaders[1] = GetTranslation("FINALTRIAL.STARTBALANCE", culture, "Before adjustments");
            fixedColumnHeaders[2] = GetTranslation("FINALTRIAL.ACCOUNT", culture, "Debet adjustments");
            fixedColumnHeaders[3] = GetTranslation("FINALTRIAL.ACCOUNT", culture, "Credit adjustments");
            fixedColumnHeaders[4] = GetTranslation("FINALTRIAL.ENDBALANCE", culture, "After adjustments");

            string templateFolder = Config.ExcelTemplates;
            string workingFolder = Config.ExcelWorkingFolder;
            string templateFileName = ConfigurationManager.AppSettings["eBook.Export.Templates.FinalTrial"];
            string templatePath = Path.Combine(templateFolder,templateFileName);
            string filePath = Path.Combine(workingFolder,string.Format("{0}{1}",Guid.NewGuid().ToString(),Path.GetExtension(templateFileName)));
            File.Copy(templatePath, filePath, true);


            CacheProxy cp = new CacheProxy();
            List<AccountDataContract> accounts = cp.GetAllAccounts(fileId, true, false, culture);
            cp.Close();

            

            //Open the copied template workbook. 
            using (SpreadsheetDocument myWorkbook = SpreadsheetDocument.Open(filePath, true))
            {
                //Access the main Workbook part, which contains all references.
                WorkbookPart workbookPart = myWorkbook.WorkbookPart;
                //Get the first worksheet. 
                WorksheetPart worksheetPart = workbookPart.WorksheetParts.First();
                // The SheetData object will contain all the data.
                SheetData sheetData = worksheetPart.Worksheet.GetFirstChild<SheetData>();


                SetHeader(worksheetPart.Worksheet, title, client.Name, string.Format("{0} [{1}]",file.Name,file.EndDate.ToString("dd/MM/yyyy")));

                SetColumnsHeaders(sheetData, fixedColumnHeaders);

                int index = 4;

                foreach (AccountDataContract adc in accounts.Where(a => (!a.IsTaxAdjusment) && (a.Saldo != 0 || a.PreviousSaldo != 0 || a.StartSaldo != 0)))
                {

                    Row contentRow = CreateContentRow(index, adc, (Row)sheetData.ChildElements[(index % 2 == 0 ? 1 : 2)].Clone(),culture);
                    index++;

                    //Append new row to sheet data
                    sheetData.AppendChild(contentRow);
                }
                // delete template rows
                DeleteRow(worksheetPart.Worksheet, 2); 
                DeleteRow(worksheetPart.Worksheet, 2);

                worksheetPart.Worksheet.Save();
            }
            System.IO.File.Move(filePath, Path.Combine(Config.PickupLocal, Path.GetFileName(filePath)));
            return Path.GetFileName(filePath);
        }

        public void SetHeader(Worksheet worksheet, string itemTitle, string clientName, string fileName)
        {
            string header = worksheet.Descendants<HeaderFooter>().FirstOrDefault().OddHeader.Text;
            header = string.Format(header, itemTitle, clientName, fileName);
            worksheet.Descendants<HeaderFooter>().FirstOrDefault().OddHeader.Text = header;
        }

        public void SetColumnsHeaders(SheetData sheetData, string[] titles)
        {
            Row r = sheetData.Elements<Row>().First();
            var iCellCount = r.Elements<Cell>().Count();

            for (int i = 0; i < titles.Length; i++)
            {
                Cell cell = null;

                if ((r.Elements<Cell>().Skip(i).Count() == 0)) //Issue where template count is lower than given titles, then create cell
                {
                    cell = new Cell();
                }
                else
                {
                    cell = r.Elements<Cell>().Skip(i).First();
                }

                AddText(titles[i], cell, letters[i].ToString(), 1);
            }
        }

        public void AddText(string text, Cell cell, string header, int index)
        {
            cell.DataType = CellValues.InlineString;
            cell.CellReference = header + index;
            cell.CellValue = new CellValue(text);
            cell.DataType = new EnumValue<CellValues>(CellValues.String);
        }

        public void AddNumber(decimal number, Cell cell, string header, int index)
        {

            cell.CellReference = header + index;
            cell.CellValue = new CellValue(number.ToString(CultureInfo.InvariantCulture));
            cell.DataType = new EnumValue<CellValues>(CellValues.Number);
        }

        public void DeleteRow(Worksheet worksheet, int rowIndex)
        {

            var deleteRow = worksheet.Descendants<Row>().Where(r => r.RowIndex == rowIndex).FirstOrDefault();
            deleteRow.Remove();

            foreach (Row row in worksheet.Descendants<Row>().
                                    Where(r => r.RowIndex.Value > rowIndex))
            {

                uint newRowIndex = System.Convert.ToUInt32(row.RowIndex.Value - 1);

                foreach (Cell cell in row.Elements<Cell>())
                {
                    // Update the references for reserved cells.
                    string cellReference = cell.CellReference.Value;
                    cell.CellReference = new StringValue(cellReference.Replace(
                      row.RowIndex.Value.ToString(), newRowIndex.ToString()));
                }

                // Update the row index.
                row.RowIndex = new UInt32Value(newRowIndex);
            }
        }

        public Row CreateContentRow(int index,AccountDataContract adc, Row r,string culture)
        {
            //Create the new row.
            r.RowIndex = (UInt32)index;
            AddText(adc.VisualNr + " " + adc.DefaultDescription, r.Elements<Cell>().First(), letters[0].ToString(), index);            
            AddNumber(adc.PreviousSaldo, r.Elements<Cell>().Skip(1).First(), letters[1].ToString(), index);
            AddNumber((adc.Saldo - adc.PreviousSaldo) > 0 ? (adc.Saldo - adc.PreviousSaldo) : 0, r.Elements<Cell>().Skip(2).First(), letters[2].ToString(), index);
            AddNumber((adc.Saldo - adc.PreviousSaldo) < 0 ? (adc.Saldo - adc.PreviousSaldo) : 0, r.Elements<Cell>().Skip(3).First(), letters[3].ToString(), index);
            AddNumber(adc.Saldo, r.Elements<Cell>().Skip(4).First(), letters[4].ToString(), index);
           

            return r;
        }

        private string GetTranslation(string key, string culture, string defaultText)
        {
            string trans = GetTranslation(key, culture);
            if (string.IsNullOrEmpty(trans)) trans = defaultText;
            return trans;
        }

        private string GetTranslation(string key, string culture)
        {
            Core.EF.Read.InterfaceTranslation it = EY.com.eBook.Core.EF.eBookReadManager.Context.InterfaceTranslations
                                                                .Where(i => i.Interface.ToUpper() == "EXPORT" &&
                                                                    i.Key.ToUpper() == key.ToUpper() &&
                                                                    i.Culture.ToUpper() == culture.ToUpper())
                                                                .FirstOrDefault();

            if (it == null) return null;
            return it.Translation;
        }

    }
    
}
