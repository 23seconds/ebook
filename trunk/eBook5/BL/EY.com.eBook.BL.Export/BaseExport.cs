﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OfficeOpenXml;
using System.IO;
using EY.com.eBook.Core;

namespace EY.com.eBook.BL.Export
{
    public abstract class BaseExport<T>
    {
        protected ExcelPackage p = null;
        protected ExcelWorkbook _workbook = null;
        protected ExcelWorksheet _worksheet = null;        
        public Guid Id { get; set; }

        public BaseExport()
        {

        }

        protected abstract void LoadData();
        protected abstract void CreateHeaderCurrentWorksheet();
        protected abstract void FillData(List<T> list);

        protected void CreateWorkbook()
        {
            p = new ExcelPackage();
            _workbook = p.Workbook;
        }

        protected Guid SaveWorkbook()
        {
            var fileId = Guid.NewGuid();
            //var fileName = string.Format(@"C:\\Projects_TFS\\EY\\Ebook\\Main-v5.0\\EY.com.eBook.Admintool\\Temporary\\ExcelGeneration\\{0}.xlsx", fileId.ToString());
            var fileName = Path.Combine(Config.ExcelWorkingFolder,fileId.ToString() + ".xlsx");
            Byte[] bin = p.GetAsByteArray();
            File.WriteAllBytes(fileName, bin);

            System.IO.File.Move(fileName, Path.Combine(Config.PickupLocal, Path.GetFileName(fileName)));
            return fileId;
        }

        protected void SetCurrentWorksheet(int number, string name)
        {
            if (_workbook.Worksheets.Count < number)
                _worksheet = _workbook.Worksheets.Add(name);// .Add(Type.Missing, Type.Missing, Type.Missing, Type.Missing);
            else
                _worksheet = _workbook.Worksheets[number];

            _worksheet.Name = name;
        }

        protected void setColumnWidth(ExcelWorksheet ws, int dataRows, int dataColumns, string textToAdd)
        {
            double cellSize = ws.Cells[dataRows, dataColumns].Worksheet.Column(dataColumns).Width;
            double proposedCellSize = textToAdd.Trim().Length * 3;
            if (cellSize <= proposedCellSize)
            {
                ws.Cells[dataRows, dataColumns].Worksheet.Column(dataColumns).Width = proposedCellSize;
            }
        }


    }
}
