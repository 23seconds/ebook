﻿using System.Text;
using System.IO;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using DocumentFormat.OpenXml;
using System.Globalization;
using EY.com.eBook.API.Contracts.Data;
using System.Configuration;
using System;
using System.Linq;
using System.Collections.Generic;

namespace EY.com.eBook.BL.Export
{
    /*
    public class ExportBookings
    {

        
        private char[] letters = "ABCDE".ToCharArray();
        

        // Creates a SpreadsheetDocument.
        public string CreatePackage(FileBaseDataContract file,ClientBaseDataContract client,
                                    List<FinalTrialBalanceLineDataContract> finalTrialLines,
                                    string title, string[] fixedHeaders)
        {
            
            string templateFolder = ConfigurationManager.AppSettings["eBook.Export.TemplateFolder"];
            string workingFolder = ConfigurationManager.AppSettings["eBook.Export.WorkingFolder"];
            string templateFileName = ConfigurationManager.AppSettings["eBook.Export.Templates.Bookings"];
            string templatePath = Path.Combine(templateFolder,templateFileName);
            string filePath = Path.Combine(workingFolder,string.Format("{0}{1}",Guid.NewGuid().ToString(),Path.GetExtension(templateFileName)));
            File.Copy(templatePath, filePath, true);

            

            //Open the copied template workbook. 
            using (SpreadsheetDocument myWorkbook = SpreadsheetDocument.Open(filePath, true))
            {
                //Access the main Workbook part, which contains all references.
                WorkbookPart workbookPart = myWorkbook.WorkbookPart;
                //Get the first worksheet. 
                WorksheetPart worksheetPart = workbookPart.WorksheetParts.First();
                // The SheetData object will contain all the data.
                SheetData sheetData = worksheetPart.Worksheet.GetFirstChild<SheetData>();


                SetHeader(worksheetPart.Worksheet, title, client.Name, string.Format("{0} [{1}]",file.Name,file.EndDate.ToString("dd/MM/yyyy")));

                SetColumnsHeaders(sheetData, fixedHeaders);

                int index = 4;

                foreach (FinalTrialBalanceLineDataContract ftbldc in finalTrialLines)
                {

                    Row contentRow = CreateContentRow(index, ftbldc, (Row)sheetData.ChildElements[(index % 2 == 0 ? 1 : 2)].Clone());
                    index++;

                    //Append new row to sheet data
                    sheetData.AppendChild(contentRow);
                }
                // delete template rows
                DeleteRow(worksheetPart.Worksheet, 2); 
                DeleteRow(worksheetPart.Worksheet, 2);

                worksheetPart.Worksheet.Save();
            }
            return filePath;
        }

        public void SetHeader(Worksheet worksheet, string itemTitle, string clientName, string fileName)
        {
            string header = worksheet.Descendants<HeaderFooter>().FirstOrDefault().OddHeader.Text;
            header = string.Format(header, itemTitle, clientName, fileName);
            worksheet.Descendants<HeaderFooter>().FirstOrDefault().OddHeader.Text = header;
        }

        public void SetColumnsHeaders(SheetData sheetData, string[] titles)
        {
            Row r = sheetData.Elements<Row>().First();
            for (int i = 0; i < titles.Length; i++)
            {
                AddText(titles[i], r.Elements<Cell>().Skip(i).First(), letters[i].ToString(), 1);
            }
        }

        public void AddText(string text, Cell cell, string header, int index)
        {
            cell.DataType = CellValues.InlineString;
            cell.CellReference = header + index;
            cell.CellValue = new CellValue(text);
            cell.DataType = new EnumValue<CellValues>(CellValues.String);
        }

        public void AddNumber(decimal number, Cell cell, string header, int index)
        {

            cell.CellReference = header + index;
            cell.CellValue = new CellValue(number.ToString(CultureInfo.InvariantCulture));
            cell.DataType = new EnumValue<CellValues>(CellValues.Number);
        }

        public void DeleteRow(Worksheet worksheet, int rowIndex)
        {

            var deleteRow = worksheet.Descendants<Row>().Where(r => r.RowIndex == rowIndex).FirstOrDefault();
            deleteRow.Remove();

            foreach (Row row in worksheet.Descendants<Row>().
                                    Where(r => r.RowIndex.Value > rowIndex))
            {

                uint newRowIndex = System.Convert.ToUInt32(row.RowIndex.Value - 1);

                foreach (Cell cell in row.Elements<Cell>())
                {
                    // Update the references for reserved cells.
                    string cellReference = cell.CellReference.Value;
                    cell.CellReference = new StringValue(cellReference.Replace(
                      row.RowIndex.Value.ToString(), newRowIndex.ToString()));
                }

                // Update the row index.
                row.RowIndex = new UInt32Value(newRowIndex);
            }
        }

        public Row CreateContentRow(int index,FinalTrialBalanceLineDataContract ftbldc, Row r)
        {
            //Create the new row.
            r.RowIndex = (UInt32)index;
            decimal adjustSaldo = ftbldc.AdjustmentsDebet - ftbldc.AdjustmentsCredit;
            string adjustDC = adjustSaldo < 0 ? "C" : "D";
            AddText(ftbldc.VisualNr, r.Elements<Cell>().First(), letters[0].ToString(), index);
            AddText(ftbldc.Description, r.Elements<Cell>().Skip(1).First(), letters[1].ToString(), index);
            AddNumber(adjustSaldo, r.Elements<Cell>().Skip(2).First(), letters[2].ToString(), index);
            AddNumber(Math.Abs(adjustSaldo), r.Elements<Cell>().Skip(3).First(), letters[3].ToString(), index);
            AddText(adjustDC, r.Elements<Cell>().Skip(4).First(), letters[4].ToString(), index);

            return r;
        }

    }
    */
}
