﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.ServiceModel;
using EY.com.eBook.API.Implementation;
using EY.com.eBook.Caching;
using ProtoBuf.Meta;
using EY.com.eBook.RuleEngine.Core.Interfaces;
using System.Reflection;
using EY.com.eBook.RuleEngine;
using System.Timers;
using System.Text.RegularExpressions;
using EY.com.eBook.Core;

namespace EY.com.eBook.CachingService
{
    // HOLD CACHE
    // HOLD WORKSHEET RULEEGINE (So has direct cache link)
    

    public partial class eBookCacheService : ServiceBase
    {

        private readonly List<Type> _servicesToHost = new List<Type> { 
            typeof (CacheService),
            typeof(RuleEngine2016),
            typeof (RuleEngine2015),
            typeof (RuleEngine2014),
            typeof (RuleEngine2013),
            typeof (RuleEngine2012),
            typeof (RuleEngine2011),
            typeof (RuleEngine2010),
            typeof (RuleEngine2009),
            typeof (RuleEngine2008),
        };

        private readonly Dictionary<Type, ServiceHost> _hosts = new Dictionary<Type, ServiceHost>();

        private Dictionary<string, List<Type>> _iWorksheetClassTypes = new Dictionary<string, List<Type>>();

        private Timer _wcfStarter;

        bool closing = false;

        public eBookCacheService()
        {
            InitializeComponent();
        }

        internal void DoStart(string[] args)
        {
            OnStart(args);
        }

        protected override void OnStart(string[] args)
        {
            try
            {
                Logger.Debug("Starting Init Cache Service");
                _iWorksheetClassTypes = new EY.com.eBook.RuleEngine.Data.Prepper().PrepareProtobuf();

                foreach (var host in _hosts.Values)
                    host.Close();

                _hosts.Clear();
                
                
                var backgroundWorker = new BackgroundWorker();
                backgroundWorker.DoWork += backgroundWorker_DoWork;
                backgroundWorker.RunWorkerCompleted += backgroundWorker_RunWorkerCompleted;
                backgroundWorker.RunWorkerAsync();

            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                throw;
            }
        }

        /* OLD
        protected override void OnStart(string[] args)
        {
            _lastMem = DateTime.Now;
    
            _iWorksheetClassTypes = new EY.com.eBook.RuleEngine.Data.Prepper().PrepareProtobuf();
            //RuntimeTypeModel.Default.AutoAddMissingTypes = true;
            //RuntimeTypeModel.Default.AutoCompile = true;
            //RuntimeTypeModel.Default.
                //.AddSubType(1, typeof(LogonRequest));

            //_cacheThread = new System.Threading.Thread(new ThreadStart(
            
            if (serviceHost != null)
            {
                serviceHost.Close();
            }
            if (_ruleEngine2013 != null)
            {
                _ruleEngine2013.Close();
            }
            if (_ruleEngine2012 != null)
            {
                _ruleEngine2012.Close();
            }

            if (_ruleEngine2011 != null)
            {
                _ruleEngine2011.Close();
            }
            if (_ruleEngine2010 != null)
            {
                _ruleEngine2010.Close();
            }
            if (_ruleEngine2009 != null)
            {
                _ruleEngine2009.Close();
            }
            if (_ruleEngine2008 != null)
            {
                _ruleEngine2008.Close();
            }

            //serviceHost.Faulted += new EventHandler(serviceHost_Faulted);
            //serviceHost.Closed += new EventHandler(serviceHost_Closed);
            // Create a ServiceHost for the CalculatorService type and 
            // provide the base address.
           
            // Open the ServiceHostBase to create listeners and start 
            // listening for messages.
            BackgroundWorker backgroundWorker = new BackgroundWorker();
            backgroundWorker.DoWork += new DoWorkEventHandler(backgroundWorker_DoWork);
            backgroundWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(backgroundWorker_RunWorkerCompleted);
            backgroundWorker.RunWorkerAsync();

            

           
        }
        */

        void _wcfStarter_Elapsed(object sender, ElapsedEventArgs e)
        {
            try
            {
                foreach (var hostType in _servicesToHost)
                {
                    ServiceHost host;
                    if (_hosts.TryGetValue(hostType, out host))
                    {
                        if (host.State != CommunicationState.Faulted && host.State != CommunicationState.Closed &&
                            host.State != CommunicationState.Closing) continue;
                        host.Abort();
                        _hosts.Remove(hostType);
                    }

                    host = new ServiceHost(hostType);
                    _hosts.Add(hostType, host);
                    Logger.Info("Service {0} restarted", hostType.Name);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                throw;
            }
        }




        private void backgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                var cr = new CacheRepository();
                int cnt = 0;
                Logger.Info("CACHE Start loading static data");
                cr.LoadCoeffsFromDB();
                Logger.Info("CACHE Coefficients loaded " + CoefficientsCache.Current.CachedItemsNumber);
                cnt = cr.LoadIWorksheetClasses(_iWorksheetClassTypes);
                Logger.Info("CACHE Worksheet interfaces loaded" + cnt);
                cnt=cr.LoadWorksheetTypeTranslations();
                Logger.Info("CACHE Worksheet type translations loaded " + cnt);
                cnt=cr.LoadMessageTranslations();
                Logger.Info("CACHE Message translations loaded " + cnt);
                cnt=cr.LoadMappings();
                Logger.Info("CACHE Mappings loaded " +cnt);
                Logger.Info("CACHE Finished");
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                throw;
            }
        }

        private void backgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            Logger.Info("CACHE Finished loading translations");
            //_wcfStarter_Elapsed(null, null);

            Logger.Info("CACHE starting WCF SERVICES");
            foreach (var type in _servicesToHost)
            {
                var host = new ServiceHost(type);
                host.Open();
                _hosts.Add(type, host);
                Logger.Info("Started {0} ", type.Name);
            }

            _wcfStarter = new Timer(2 * 60 * 1000);
            _wcfStarter.Elapsed += _wcfStarter_Elapsed;
            _wcfStarter.Start();
            Logger.Info("CACHE Services are available");
        }

       


        protected override void OnStop()
        {
            _wcfStarter.Stop();
            _wcfStarter = null;
            foreach (var host in _hosts.Values)
            {
                if (host.State == CommunicationState.Opened)
                    host.Close(new TimeSpan(0,0,5));
                host.Abort();
            }
            Logger.Warning("CACHE Service STOPPED");
        }
    }
}
