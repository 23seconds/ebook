﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Linq;
using System.ServiceProcess;
using System.ServiceModel;
using System.Configuration;
using System.Diagnostics;
using System.Reflection;

namespace EY.com.eBook.CachingService
{
    // Provide the ProjectInstaller class which allows 
    // the service to be installed by the Installutil.exe tool
    [RunInstaller(true)]
    public class ProjectInstaller : Installer
    {
        private ServiceProcessInstaller process;
        private ServiceInstaller service;

        public ProjectInstaller()
        {
            process = new ServiceProcessInstaller();
            process.Account = ServiceAccount.LocalSystem;
            service = new ServiceInstaller();
            service.ServiceName = "eBook Caching Service";
            Installers.Add(process);
            Installers.Add(service);

            if(EventLog.Exists("eBook.Caching")) EventLog.Delete("eBook.Caching");
            if (EventLog.Exists("eBook.RuleEngine")) EventLog.Delete("eBook.RuleEngine");

            if (EventLog.SourceExists("eBook.Caching")) EventLog.DeleteEventSource("eBook.Caching");
            if (EventLog.SourceExists("eBook.RuleEngine")) EventLog.DeleteEventSource("eBook.RuleEngine");
            
            EventLogInstaller evtInstaller = FindInstaller(this.Installers);
            Console.WriteLine("> Installing eventlog");
            evtInstaller.Log = "eBook.Caching";
           // evtInstaller.Source = "eBook.Caching";

            EventLogInstaller evtInstaller2 = new EventLogInstaller();
            evtInstaller2.Source = "eBook.RuleEngine";
           
            evtInstaller2.Log = "eBook.RuleEngine";
            Installers.Add(evtInstaller2);
           // evtInstaller.Source = "eBook.RuleEngine";


        }

        private EventLogInstaller FindInstaller(InstallerCollection installers)
        {
            foreach (Installer installer in installers)
            {
                if (installer is EventLogInstaller)
                {
                    return (EventLogInstaller)installer;
                }

                EventLogInstaller eventLogInstaller = FindInstaller(installer.Installers);
                if (eventLogInstaller != null)
                {
                    return eventLogInstaller;
                }
            }
            return null;
        }

       
    }
}
