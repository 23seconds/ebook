﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System;
using System.Xml;
using System.Xml.Linq;
using EY.com.eBook.BizTax.Contracts;
using EY.com.eBook.API.Contracts.Data.BizTax;

namespace EY.com.eBook.BizTax.AY2014.nrcorp
{
    public class BizTaxRenderer : BizTaxRendererGenerated
    {
        public List<XmlQualifiedName> NameSpaces
        {
            get
            {
                return new List<XmlQualifiedName> {
          
            new XmlQualifiedName("xlink","http://www.w3.org/1999/xlink")
              ,
            new XmlQualifiedName("","http://www.xbrl.org/2003/instance")
          ,
            new XmlQualifiedName("iso4217","http://www.xbrl.org/2003/iso4217")
          ,
            new XmlQualifiedName("xsi","http://www.w3.org/2001/XMLSchema-instance")
          ,
            new XmlQualifiedName("link","http://www.xbrl.org/2003/linkbase")
   
          ,
            new XmlQualifiedName("xbrldi","http://xbrl.org/2006/xbrldi")
          ,
            new XmlQualifiedName("xbrldt","http://xbrl.org/2005/xbrldt")
          ,
            new XmlQualifiedName("tax-inc-nrcorp","http://www.minfin.fgov.be/be/tax/inc/nrcorp/2014-04-30")
          ,
            new XmlQualifiedName("xl","http://www.xbrl.org/2003/XLink")
          ,
            new XmlQualifiedName("t-reso","http://www.minfin.fgov.be/be/tax/t/reso/2014-04-30")
          ,
            new XmlQualifiedName("d-ty","http://www.minfin.fgov.be/be/tax/d/ty/2014-04-30")
          ,
            new XmlQualifiedName("tax-inc","http://www.minfin.fgov.be/be/tax/inc/2014-04-30")
          ,
            new XmlQualifiedName("pfs-dt","http://www.nbb.be/be/fr/pfs/ci/dt/2014-04-01")
          ,
            new XmlQualifiedName("ref","http://www.xbrl.org/2006/ref")
          ,
            new XmlQualifiedName("t-drfa","http://www.minfin.fgov.be/be/tax/t/drfa/2014-04-30")
          ,
            new XmlQualifiedName("t-wddc","http://www.minfin.fgov.be/be/tax/t/wddc/2014-04-30")
          ,
            new XmlQualifiedName("d-hh","http://www.minfin.fgov.be/be/tax/d/hh/2014-04-30")
          ,
            new XmlQualifiedName("d-ec","http://www.minfin.fgov.be/be/tax/d/ec/2014-04-30")
          ,
            new XmlQualifiedName("t-prre","http://www.minfin.fgov.be/be/tax/t/prre/2014-04-30")
          ,
            new XmlQualifiedName("d-expt","http://www.minfin.fgov.be/be/tax/d/expt/2014-04-30")
          ,
            new XmlQualifiedName("t-cg","http://www.minfin.fgov.be/be/tax/t/cg/2014-04-30")
          ,
            new XmlQualifiedName("d-asst","http://www.minfin.fgov.be/be/tax/d/asst/2014-04-30")
          ,
            new XmlQualifiedName("t-ricg","http://www.minfin.fgov.be/be/tax/t/ricg/2014-04-30")
          ,
            new XmlQualifiedName("t-stcg","http://www.minfin.fgov.be/be/tax/t/stcg/2014-04-30")
          ,
            new XmlQualifiedName("t-ace","http://www.minfin.fgov.be/be/tax/t/ace/2014-04-30")
          ,
            new XmlQualifiedName("t-pcs","http://www.minfin.fgov.be/be/tax/t/pcs/2014-04-30")
          ,
            new XmlQualifiedName("t-dpi","http://www.minfin.fgov.be/be/tax/t/dpi/2014-04-30")
          ,
            new XmlQualifiedName("t-itifa","http://www.minfin.fgov.be/be/tax/t/itifa/2014-04-30")
          ,
            new XmlQualifiedName("d-per","http://www.minfin.fgov.be/be/tax/d/per/2014-04-30")
          ,
            new XmlQualifiedName("t-idtc","http://www.minfin.fgov.be/be/tax/t/idtc/2014-04-30")
          ,
            new XmlQualifiedName("d-incc","http://www.minfin.fgov.be/be/tax/d/incc/2014-04-30")
          ,
            new XmlQualifiedName("d-invc","http://www.minfin.fgov.be/be/tax/d/invc/2014-04-30")
          ,
            new XmlQualifiedName("t-eap","http://www.minfin.fgov.be/be/tax/t/eap/2014-04-30")
          ,
            new XmlQualifiedName("d-empstat","http://www.minfin.fgov.be/be/tax/d/empstat/2014-04-30")
          ,
            new XmlQualifiedName("d-empreg","http://www.minfin.fgov.be/be/tax/d/empreg/2014-04-30")
          ,
            new XmlQualifiedName("d-tu","http://www.minfin.fgov.be/be/tax/d/tu/2014-04-30")
          ,
            new XmlQualifiedName("t-eapsf","http://www.minfin.fgov.be/be/tax/t/eapsf/2014-04-30")
          ,
            new XmlQualifiedName("d-empf","http://www.minfin.fgov.be/be/tax/d/empf/2014-04-30")
          ,
            new XmlQualifiedName("d-empper","http://www.minfin.fgov.be/be/tax/d/empper/2014-04-30")
          ,
            new XmlQualifiedName("t-dfa","http://www.minfin.fgov.be/be/tax/t/dfa/2014-04-30")
          ,
            new XmlQualifiedName("d-depm","http://www.minfin.fgov.be/be/tax/d/depm/2014-04-30")
          ,
            new XmlQualifiedName("d-br","http://www.minfin.fgov.be/be/tax/d/br/2014-04-30")
          ,
            new XmlQualifiedName("d-origin","http://www.minfin.fgov.be/be/tax/d/origin/2014-04-30")
          ,
            new XmlQualifiedName("pfs-gcd","http://www.nbb.be/be/fr/pfs/ci/gcd/2014-04-01")
          ,
            new XmlQualifiedName("pfs-vl","http://www.nbb.be/be/fr/pfs/ci/vl/2014-04-01")
          
        };
            }

        }
        

        public XDocument RenderBizTaxFile(BizTaxDataContract btdc, Guid fileId)
        {
            return RenderBizTaxFile(btdc, fileId, NameSpaces,TaxonomySchemaRef_nrcorp);
        }

        /*
        internal override void Calc_f_prre_5050_assertion_set()
        {
            List<ContextElementDataContract> contexts = GetContexts(new List<string> { "I-End" },
                new List<string> { "d-ec:ExemptionCategoryDimension::d-ec:RemainingBalanceAfterDecreaseProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ExpensesCompensationCoveredCurrentTaxPeriodMember/d-ty:ProvisionRiskExpenseTypedDimension",
                    "d-ec:ExemptionCategoryDimension::d-ec:RemainingBalanceAfterDecreaseProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ExpensesOverhaulImportantRepairsMaximum10YearsMember/d-ty:ProvisionRiskExpenseTypedDimension",
                    "d-ec:ExemptionCategoryDimension::d-ec:RemainingBalanceAfterDecreaseProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ProfessionalExpensesCurrentTaxPeriodMember/d-ty:ProvisionRiskExpenseTypedDimension",
                   });
            foreach (ContextElementDataContract context in contexts)
            {
                var ExemptProvisionRisksExpensesEnd = FilterElementsByContext(context, new List<string> { "tax-inc:ExemptProvisionRisksExpenses" }, new List<string> { "I-End" }, null, null);
                var ExemptProvisionRisksExpensesStart = FilterElementsByContext(context, new List<string> { "tax-inc:ExemptProvisionRisksExpenses" }, new List<string> { "I-Start" }, null, null);
                var IncreaseExemptProvisionRisksExpenses = FilterElementsByContext(context, new List<string> { "tax-inc:IncreaseExemptProvisionRisksExpenses" }, new List<string> { "D" }, new List<DimensionFilter> { new DimensionFilter { Invert = false, Dimension = "d-ec:ExemptionCategoryDimension" }, new DimensionFilter { Invert = true, Dimension = "d-ec:ExemptionCategoryDimension", Values = new List<string> { "d-ec:NewProvisionRisksExpensesMember" } } }, new List<string> { "d-ec:ExemptionCategoryDimension::d-ec:UnalteredBalanceProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ExpensesCompensationCoveredCurrentTaxPeriodMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:UnalteredBalanceProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ExpensesOverhaulImportantRepairsMaximum10YearsMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:UnalteredBalanceProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ProfessionalExpensesCurrentTaxPeriodMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:RemainingBalanceAfterDecreaseProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ExpensesCompensationCoveredCurrentTaxPeriodMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:RemainingBalanceAfterDecreaseProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ExpensesOverhaulImportantRepairsMaximum10YearsMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:RemainingBalanceAfterDecreaseProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ProfessionalExpensesCurrentTaxPeriodMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:RemainingBalanceAfterIncreaseProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ExpensesCompensationCoveredCurrentTaxPeriodMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:RemainingBalanceAfterIncreaseProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ExpensesOverhaulImportantRepairsMaximum10YearsMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:RemainingBalanceAfterIncreaseProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ProfessionalExpensesCurrentTaxPeriodMember/d-ty:ProvisionRiskExpenseTypedDimension" });
                var DecreaseExemptProvisionRisksExpensesBorneExpensesCurrentTaxPeriod = FilterElementsByContext(context, new List<string> { "tax-inc:DecreaseExemptProvisionRisksExpensesBorneExpensesCurrentTaxPeriod" }, new List<string> { "D" }, new List<DimensionFilter> { new DimensionFilter { Invert = false, Dimension = "d-ec:ExemptionCategoryDimension" }, new DimensionFilter { Invert = true, Dimension = "d-ec:ExemptionCategoryDimension", Values = new List<string> { "d-ec:NewProvisionRisksExpensesMember" } } }, new List<string> { "d-ec:ExemptionCategoryDimension::d-ec:UnalteredBalanceProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ExpensesCompensationCoveredCurrentTaxPeriodMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:UnalteredBalanceProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ExpensesOverhaulImportantRepairsMaximum10YearsMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:UnalteredBalanceProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ProfessionalExpensesCurrentTaxPeriodMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:RemainingBalanceAfterDecreaseProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ExpensesCompensationCoveredCurrentTaxPeriodMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:RemainingBalanceAfterDecreaseProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ExpensesOverhaulImportantRepairsMaximum10YearsMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:RemainingBalanceAfterDecreaseProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ProfessionalExpensesCurrentTaxPeriodMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:RemainingBalanceAfterIncreaseProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ExpensesCompensationCoveredCurrentTaxPeriodMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:RemainingBalanceAfterIncreaseProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ExpensesOverhaulImportantRepairsMaximum10YearsMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:RemainingBalanceAfterIncreaseProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ProfessionalExpensesCurrentTaxPeriodMember/d-ty:ProvisionRiskExpenseTypedDimension" });
                var DecreaseExemptProvisionRisksExpensesNewEstimationProbableExpenses = FilterElementsByContext(context, new List<string> { "tax-inc:DecreaseExemptProvisionRisksExpensesNewEstimationProbableExpenses" }, new List<string> { "D" }, new List<DimensionFilter> { new DimensionFilter { Invert = false, Dimension = "d-ec:ExemptionCategoryDimension" }, new DimensionFilter { Invert = true, Dimension = "d-ec:ExemptionCategoryDimension", Values = new List<string> { "d-ec:NewProvisionRisksExpensesMember" } } }, new List<string> { "d-ec:ExemptionCategoryDimension::d-ec:UnalteredBalanceProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ExpensesCompensationCoveredCurrentTaxPeriodMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:UnalteredBalanceProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ExpensesOverhaulImportantRepairsMaximum10YearsMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:UnalteredBalanceProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ProfessionalExpensesCurrentTaxPeriodMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:RemainingBalanceAfterDecreaseProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ExpensesCompensationCoveredCurrentTaxPeriodMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:RemainingBalanceAfterDecreaseProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ExpensesOverhaulImportantRepairsMaximum10YearsMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:RemainingBalanceAfterDecreaseProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ProfessionalExpensesCurrentTaxPeriodMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:RemainingBalanceAfterIncreaseProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ExpensesCompensationCoveredCurrentTaxPeriodMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:RemainingBalanceAfterIncreaseProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ExpensesOverhaulImportantRepairsMaximum10YearsMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:RemainingBalanceAfterIncreaseProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ProfessionalExpensesCurrentTaxPeriodMember/d-ty:ProvisionRiskExpenseTypedDimension" });
                if (ExemptProvisionRisksExpensesStart.Count > 0 || ExemptProvisionRisksExpensesEnd.Count > 0 || IncreaseExemptProvisionRisksExpenses.Count > 0 || DecreaseExemptProvisionRisksExpensesBorneExpensesCurrentTaxPeriod.Count > 0 || DecreaseExemptProvisionRisksExpensesNewEstimationProbableExpenses.Count > 0)
                {
                    var PeriodStartDate = GetNodeByName("tax-inc", "PeriodStartDate");
                    var PeriodEndDate = GetNodeByName("tax-inc", "PeriodEndDate");
                    var CalculatedAmountExemptProvisionRisksExpenses = Sum(Number(ExemptProvisionRisksExpensesEnd, "0"), -Number(ExemptProvisionRisksExpensesStart, "0"));
                    var CalculatedAmountDecreaseExemptProvisionRisksExpenses = Sum(Number(DecreaseExemptProvisionRisksExpensesBorneExpensesCurrentTaxPeriod, "0"), Number(DecreaseExemptProvisionRisksExpensesNewEstimationProbableExpenses, "0"));
                    //CALCULATION HERE
                    //FORMULA HERE
                    bool test = (Sum(Number(ExemptProvisionRisksExpensesEnd, "0"), -Number(ExemptProvisionRisksExpensesStart, "0")) == Number(IncreaseExemptProvisionRisksExpenses, "0")) || (Sum(Number(ExemptProvisionRisksExpensesEnd, "0"), -Number(ExemptProvisionRisksExpensesStart, "0")) == Sum(-Number(DecreaseExemptProvisionRisksExpensesBorneExpensesCurrentTaxPeriod, "0"), -Number(DecreaseExemptProvisionRisksExpensesNewEstimationProbableExpenses, "0")));
                    if (!test)
                    {
                        // MESSAGES
                        AddMessage(new BizTaxErrorDataContract
                        {
                            FileId = _fileId,
                            Id = "f-prre-5050",
                            Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Het verschil tussen de vrijgestelde voorziening bij het begin van het belastbare tijdperk ({$ExemptProvisionRisksExpensesStart}) en op het einde van het belastbare tijdperk ({$ExemptProvisionRisksExpensesEnd}) 	moet bij een vermindering (= {$CalculatedAmountDecreaseExemptProvisionRisksExpenses}) overeenstemmen met de 	'{Vermindering van de vrijgestelde voorziening ingevolge kosten die tijdens het boekjaar effectief gedragen zijn}' 	en/of 	'{Vermindering van de vrijgestelde voorziening ingevolge een nieuwe schatting van de waarschijnlijke kosten tijdens het boekjaar}', 	OF 	bij een verhoging met het bedrag ingevuld in de kolom 	'{Verhoging van de vrijgestelde voorziening}' ({$IncreaseExemptProvisionRisksExpenses})  (= {$CalculatedAmountExemptProvisionRisksExpenses})."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "La différence entre la provision exonérée au début de l'exercice imposable ({$ExemptProvisionRisksExpensesStart}) et à la fin de l'exercice imposable ({$ExemptProvisionRisksExpensesEnd}) 	doit correspondre en cas de diminution (= {$CalculatedAmountDecreaseExemptProvisionRisksExpenses}) à la 	'{Diminution de la provision exonérée résultant de la prise en charge effective au cours de l'exercice comptable}' 	et/ou la  	'{Diminution de la provision exonérée résultant d'une nouvelle estimation de la charge probable au cours de l'exercice comptable}', 	OU en cas d'augmentation au montant complété dans la colonne 		'{Augmentation de la provision exonérée}' ({$IncreaseExemptProvisionRisksExpenses})  (= {$CalculatedAmountExemptProvisionRisksExpenses})."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Der Unterschied zwischen den steuerfreien Rückstellungen am Anfang ({$ExemptProvisionRisksExpensesStart}) und am Ende ({$ExemptProvisionRisksExpensesEnd}) 	des Besteuerungszeitraums muss im Falle einer Verringerung (= {$CalculatedAmountDecreaseExemptProvisionRisksExpenses}) der Rubrik 	'{Die Verringerung, die sich aus den während des Rechnungsjahres, worauf sich das Verzeichnis bezieht, effektiv getragenen Kosten ergeben}' 	und/oder 	'{Die Verringerung, die sich aus einer Neueinschätzung der wahrscheinlichen Kosten ergeben}', 	entsprechen ODER im Falle einer Erhöhung dem in der Spalte	'{Erhöhung der steuerbefreiten Rückstellung}' ({$IncreaseExemptProvisionRisksExpenses})  (= {$CalculatedAmountExemptProvisionRisksExpenses}) angegebenen Betrag entsprechen."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Het verschil tussen de vrijgestelde voorziening bij het begin van het belastbare tijdperk ({$ExemptProvisionRisksExpensesStart}) en op het einde van het belastbare tijdperk ({$ExemptProvisionRisksExpensesEnd}) 	moet bij een vermindering (= {$CalculatedAmountDecreaseExemptProvisionRisksExpenses}) overeenstemmen met de 	'{Vermindering van de vrijgestelde voorziening ingevolge kosten die tijdens het boekjaar effectief gedragen zijn}' 	en/of 	'{Vermindering van de vrijgestelde voorziening ingevolge een nieuwe schatting van de waarschijnlijke kosten tijdens het boekjaar}', 	OF 	bij een verhoging met het bedrag ingevuld in de kolom 	'{Verhoging van de vrijgestelde voorziening}' ({$IncreaseExemptProvisionRisksExpenses})  (= {$CalculatedAmountExemptProvisionRisksExpenses})."}  
 }
                            ,
                            Fields = new List<string>()
                        }
                        );
                    }
                }
            }

        }
         */

        public override BizTaxDataContract CalculateAndValidate()
        {
            base.CalculateAndValidate();
           
            Extra_275P_PatentsIncomeCheck();
            Extra_275U_TaxCreditResearchDevelopmentCheck();
            Extra_276Ws();
            // EXTRA'S

            return this.Xbrl;
        }
        internal override void Validation_f_corp_1415a_assertion_set()
        {
            string scenarioId = GetScenarioId("");
            string period = "D";
            // SCENARIO: ""
            // PERIOD: "D"
            var Prepayments = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:Prepayments" });
            var FormulaTarget = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:Prepayments" });
            var PrepaymentFirstQuarter = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:PrepaymentFirstQuarter" });
            var PrepaymentSecondQuarter = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:PrepaymentSecondQuarter" });
            var PrepaymentThirdQuarter = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:PrepaymentThirdQuarter" });
            var PrepaymentFourthQuarter = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:PrepaymentFourthQuarter" });
            if (Prepayments.Count > 0 || PrepaymentFirstQuarter.Count > 0 || PrepaymentSecondQuarter.Count > 0 || PrepaymentThirdQuarter.Count > 0 || PrepaymentFourthQuarter.Count > 0)
            {
                var CalculatedAmountPrepayments = Sum(Number(PrepaymentFirstQuarter, "0"), Number(PrepaymentSecondQuarter, "0"), Number(PrepaymentThirdQuarter, "0"), Number(PrepaymentFourthQuarter, "0"));
                //CALCULATION HERE
                //FORMULA HERE
                if (FormulaTarget.Count == 0)
                {
                    FormulaTarget = CreateNewCalculationElement(scenarioId, period, "tax-inc:Prepayments", new string[] { "" }, "EUR", "INF");
                }
                if (FormulaTarget.Count > 1)
                {
                    throw new Exception("More then one target for FormulaTarget in assertion f-corp-1415a");
                }
                FormulaTarget.First().Calculated = true;
                FormulaTarget.First().SetNumber(CalculatedAmountPrepayments);


            }

        }

        internal override void Validation_f_nrcorp_2446_assertion_set()
        {
            // HAS NO UNBOUND SEQUENCES
            var nonNegativeMonetaryElementsNonResidentCorporate = GetElementsByScenDefs(new string[] { "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "d-ty:DescriptionTypedDimension/d-ty:NatureTypedDimension", "", "", "", "", "", "" }, new string[] { "D" }, new string[] { "tax-inc:TaxableReservedProfit", "tax-inc:DisallowedExpenses", "tax-inc:ImmovablePropertyIncome", "tax-inc:IncomePartnerCompaniesJointVenturesAssociationsNoLegalPersonality", "tax-inc:DeductionLimitElementsFiscalResult", "tax-inc:BasicTaxableAmountCommonRate", "tax-inc:BasicTaxableAmountExitTaxRate", "tax-inc:UnjustifiedExpensesHiddenSurplusProfitsFinancialAdvantagesBenefitsAllKind", "tax-inc:AdditionalDutiesDiamondTraders", "tax-inc:RetributionTaxCreditResearchDevelopment", "tax-inc:DeductibleMiscellaneousExemptions", "tax-inc:PEExemptIncomeMovableAssets", "tax-inc:CarryOverNextTaxPeriodPEExemptIncomeMovableAssets", "tax-inc:CarryOverCarryOverExemptionsAllowanceCorporateEquityAccumulatedUpToAndIncludingAssessmentYear2012", "tax-inc:CarryOverNextTaxPeriodInsufficientProfitsAllowanceCorporateEquity", "tax-inc:Prepayments", "tax-inc:NonRepayableAdvanceLevies", "tax-inc:RepayableAdvanceLevies", "tax-inc:TaxCreditResearchDevelopmentClearablePreviousTaxPeriodsRepayableCurrentAssessmentYear", "tax-inc:SeparateAssessmentAdditionalIndividualPensionProvisionsOnEndTaxPeriodClosingDateBefore2012-01-01", "tax-inc:CapitalGainsSharesRate2500", "tax-inc:UnjustifiedExpensesBenefitsAllKindIncludedInAssessmentInNameOfBeneficiary", "tax-inc:TaxableIncomeAtTheExpenseOfBelgianResidentOrBelgianEstablishment", "tax-inc:CapitalGainsSharesRate0040", "tax-inc:FairnessTax", "tax-inc:SeparateAssessmentTransitionalSystemReducedWithholdingTaxPaidTaxedReserves", "tax-inc:WithholdingTaxAtTheExpenseOfBelgianResidentOrBelgianEstablishment" });
            var monetaryElementsNonResidentCorporate = GetElementsByScenDefs(new string[] { "", "" }, new string[] { "D" }, new string[] { "tax-inc:ShippingResultTonnageBased", "tax-inc:RemainingFiscalResultBeforeOriginDistribution" });
            var nonPositiveMonetaryElementsNonResidentCorporate = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:CarryOverTaxLosses" });
            if (nonNegativeMonetaryElementsNonResidentCorporate.Count > 0 || monetaryElementsNonResidentCorporate.Count > 0 || nonPositiveMonetaryElementsNonResidentCorporate.Count > 0)
            {
                if ((Sum(Number(nonNegativeMonetaryElementsNonResidentCorporate, "0")) > (decimal)0) || (Sum(Number(monetaryElementsNonResidentCorporate, "0")) != (decimal)0) || (Sum(Number(nonPositiveMonetaryElementsNonResidentCorporate, "0")) < (decimal)0))
                {
                    //FORMULA HERE
                    bool test = Count(Xbrl.Elements.Where(e => e.Name == "BalanceSheetStatutorySeatStatutoryAccountsBelgianBranch").ToList()) != (decimal)0;
                    if (!test)
                    {
                        // MESSAGES
                        AddMessage(new BizTaxErrorDataContract
                        {
                            FileId = _fileId,
                            Id = "f-nrcorp-2446",
                            Messages = new List<BizTaxErrorMessageDataContract> { 
new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "De bijlage 'Balans opgesteld door de maatschappelijke zetel en jaarrekening van de inrichting (balans, resultatenrekening en eventuele toelichting)' is verplicht bij te voegen in het tabblad 	'Diverse bescheiden en opgaven'."}  
,new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "L'annexe 'Bilan établi par le siège social et les comptes annuels de l'établissement (bilan, compte de résultats et annexe éventuelle)' doit obligatoirement être ajoutée dans l'onglet 	'Documents et relevés divers'."}  
,new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Der Anhang 'Vom Gesellschaftssitz aufgestellte Bilanz und Jahresabschluss der Einrichtung (Bilanz, Ergebnisrechnung und eventuelle Anlage)' muss obligatorisch in die Registerkarte 'Verschiedene Unterlagen und Verzeichnisse' eingegeben werden."}  
,new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "NO TRANSLATION AVAILABLE IN LANGUAGE en-US."}  
 }
                            ,
                            Fields = GetFieldIdsOf(new List<object> { nonNegativeMonetaryElementsNonResidentCorporate, monetaryElementsNonResidentCorporate, nonPositiveMonetaryElementsNonResidentCorporate })
                        }
                        );
                    }
                } // END: hasVarList checkif(nonNegativeMonetaryElementsNonResidentCorporate.Count > 0 || monetaryElementsNonResidentCorporate.Count > 0 || nonPositiveMonetaryElementsNonResidentCorporate.Count > 0)
            } // END: preTest check: if((Sum(Number(nonNegativeMonetaryElementsNonResidentCorporate,"0"))  >  (decimal)0)  ||  (Sum(Number(monetaryElementsNonResidentCorporate,"0"))  !=  (decimal)0)  ||  (Sum(Number(nonPositiveMonetaryElementsNonResidentCorporate,"0"))  <  (decimal)0))

        } // END FUNCTION
        #region Extra checks
       

        internal void Extra_275P_PatentsIncomeCheck()
        {
            var DeductibleDeductionPatentsIncomeBelgium = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:DeductionPatentsIncome" });
           var DeductibleDeductionPatentsIncome275P = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:DeductibleDeductionPatentsIncome" });

            var calced = Sum(Number(DeductibleDeductionPatentsIncomeBelgium, "0"));
            bool test = calced == Number(DeductibleDeductionPatentsIncome275P, "0");

            if (!test)
            {
                // MESSAGES
                AddMessage(new BizTaxErrorDataContract
                {
                    FileId = _fileId,
                    Id = "f-xtra-nrcorp-275p-patincome",
                    Messages = new List<BizTaxErrorMessageDataContract> { 
                        new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = string.Format("Het totaal vermeld in formulier 275P, 'Aftrek voor octrooi-inkomsten' ({0}€) is niet gelijk aan het bedrag ({1}€) in het vak 'Uiteenzetting van de winst'.",Number(DeductibleDeductionPatentsIncome275P,"0").ToString("N",DefaultCulture),Number(DeductibleDeductionPatentsIncomeBelgium,"0").ToString("N",DefaultCulture),Number(calced).ToString("N",DefaultCulture))} , 

                        new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = string.Format("Le total repris en formulaire 275P, 'Déduction pour revenus de brevets' ({0}€) n'est pas égal au montant ({1}€) dans la rubrique 'Détail des bénéfices'.",Number(DeductibleDeductionPatentsIncome275P,"0").ToString("N",DefaultCulture),Number(DeductibleDeductionPatentsIncomeBelgium,"0").ToString("N",DefaultCulture),Number(calced).ToString("N",DefaultCulture))}  ,
                        new BizTaxErrorMessageDataContract { Culture = "en-US", Message = string.Format("Het totaal vermeld in formulier 275P, 'Aftrek voor octrooi-inkomsten' ({0}€) is niet gelijk aan het bedrag ({1}€) in het vak 'Uiteenzetting van de winst'.",Number(DeductibleDeductionPatentsIncome275P,"0").ToString("N",DefaultCulture),Number(DeductibleDeductionPatentsIncomeBelgium,"0").ToString("N",DefaultCulture),Number(calced).ToString("N",DefaultCulture))} , 
                        new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = string.Format("Het totaal vermeld in formulier 275P, 'Aftrek voor octrooi-inkomsten' ({0}€) is niet gelijk aan het bedrag ({1}€) in het vak 'Uiteenzetting van de winst'.",Number(DeductibleDeductionPatentsIncome275P,"0").ToString("N",DefaultCulture),Number(DeductibleDeductionPatentsIncomeBelgium,"0").ToString("N",DefaultCulture),Number(calced).ToString("N",DefaultCulture))} 
                    }
                    ,
                    Fields = new List<string>()
                }
                );
            }
        }

        internal void Extra_275U_TaxCreditResearchDevelopmentCheck()
        {

            //DeductibleAllowanceInvestmentDeductionIncompatibleTaxCreditResearchDevelopment
            var AllowanceInvestmentDeductionBelgium = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:AllowanceInvestmentDeduction" });
            var TaxCreditResearchDevelopment275U_G = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:DeductibleAllowanceInvestmentDeductionIncompatibleTaxCreditResearchDevelopment" });
            var TaxCreditResearchDevelopment275U_E = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:DeductibleAllowanceInvestmentDeductionCompatibleTaxCreditResearchDevelopment" });

            bool test = Number(AllowanceInvestmentDeductionBelgium, "0") == Number(TaxCreditResearchDevelopment275U_G, "0")
                           || Number(AllowanceInvestmentDeductionBelgium, "0") == Number(TaxCreditResearchDevelopment275U_E, "0");

            if (!test)
            {
                // MESSAGES
                AddMessage(new BizTaxErrorDataContract
                {
                    FileId = _fileId,
                    Id = "f-xtra-nrcorp-275u-taxcredit",
                    Messages = new List<BizTaxErrorMessageDataContract> { 
                        new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = string.Format("Het bedrag ({0}€) aangewend in het vak 'Uiteenzetting van de winst' onder 'Investeringsaftrek aftrekbaar voor het belastbare tijdperk' is niet gelijk aan het bedrag in formulier 275U, vak 'Investeringsaftrek aftrekbaar voor het belastbare tijdperk' onder 'Investeringsaftrek voor vennootschappen die niet opteren voor het belastingkrediet voor onderzoek en ontwikkeling' [code 1437.1] ({1}€) noch onder 'Investeringsaftrek voor vennootschappen die opteren voor het belastingkrediet voor onderzoek en ontwikkeling' [code 1437.2] ({2}€).",Number(AllowanceInvestmentDeductionBelgium,"0").ToString("N",DefaultCulture),Number(TaxCreditResearchDevelopment275U_G,"0").ToString("N",DefaultCulture),Number(TaxCreditResearchDevelopment275U_E,"0").ToString("N",DefaultCulture))} , 
                        new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = string.Format("Le montant ({0}€) employée dans la boîte 'Détail des bénéfices',  'Déduction pour investissement' n'est pas égal du montant sous forme 275U 'Déduction pour investissement déductible pour la période imposable', 'Déduction pour investissement pour les sociétés qui n'optent pas pour le crédit d’impôt pour recherche et développement' [code 1437.1] ({1}€) ni 'Déduction pour investissement pour les sociétés qui optent pour le crédit d’impôt pour recherche et développement' [code 1437.2] ({2}€).",Number(AllowanceInvestmentDeductionBelgium,"0").ToString("N",DefaultCulture),Number(TaxCreditResearchDevelopment275U_G,"0").ToString("N",DefaultCulture),Number(TaxCreditResearchDevelopment275U_E,"0").ToString("N",DefaultCulture))} , 
                         new BizTaxErrorMessageDataContract { Culture = "en-US", Message = string.Format("Het bedrag ({0}€) aangewend in het vak 'Uiteenzetting van de winst' onder 'Investeringsaftrek aftrekbaar voor het belastbare tijdperk' is niet gelijk aan het bedrag in formulier 275U, vak 'Investeringsaftrek aftrekbaar voor het belastbare tijdperk' onder 'Investeringsaftrek voor vennootschappen die niet opteren voor het belastingkrediet voor onderzoek en ontwikkeling' [code 1437.1] ({1}€) noch onder 'Investeringsaftrek voor vennootschappen die opteren voor het belastingkrediet voor onderzoek en ontwikkeling' [code 1437.2] ({2}€).",Number(AllowanceInvestmentDeductionBelgium,"0").ToString("N",DefaultCulture),Number(TaxCreditResearchDevelopment275U_G,"0").ToString("N",DefaultCulture),Number(TaxCreditResearchDevelopment275U_E,"0").ToString("N",DefaultCulture))} , 
                         new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = string.Format("Het bedrag ({0}€) aangewend in het vak 'Uiteenzetting van de winst' onder 'Investeringsaftrek aftrekbaar voor het belastbare tijdperk' is niet gelijk aan het bedrag in formulier 275U, vak 'Investeringsaftrek aftrekbaar voor het belastbare tijdperk' onder 'Investeringsaftrek voor vennootschappen die niet opteren voor het belastingkrediet voor onderzoek en ontwikkeling' [code 1437.1] ({1}€) noch onder 'Investeringsaftrek voor vennootschappen die opteren voor het belastingkrediet voor onderzoek en ontwikkeling' [code 1437.2] ({2}€).",Number(AllowanceInvestmentDeductionBelgium,"0").ToString("N",DefaultCulture),Number(TaxCreditResearchDevelopment275U_G,"0").ToString("N",DefaultCulture),Number(TaxCreditResearchDevelopment275U_E,"0").ToString("N",DefaultCulture))} , 
                        
                    }
                    ,
                    Fields = new List<string>()
                }
                );
            }
        }

        internal void Extra_276Ws()
        {
            var elsW1 = GetElementsByScenDefs(new string[] { 
                "d-empf:EmploymentFunctionDimension::d-empf:ResearcherMember/d-empper:EmploymentPeriodDimension::d-empper:EmployedPartTimeMember/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension" ,
                "d-empf:EmploymentFunctionDimension::d-empf:HighlyQualifiedResearcherMember/d-empper:EmploymentPeriodDimension::d-empper:EmployedPartTimeMember/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension"
            }, new string[] { "D" }, new string[] { "tax-inc:ActualExemptionAdditionalPersonnel" });

            var elsW2 = GetElementsByScenDefs(new string[] { 
                "d-empf:EmploymentFunctionDimension::d-empf:IncreaseTechnologicalPotentialPersonnelMember/d-empper:EmploymentPeriodDimension::d-empper:EmployedPartTimeMember/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension"
                }, new string[] { "D" }, new string[] { "tax-inc:ActualExemptionAdditionalPersonnel" });

            var elsW3 = GetElementsByScenDefs(new string[] { 
                "d-empf:EmploymentFunctionDimension::d-empf:DepartmentHeadExportMember/d-empper:EmploymentPeriodDimension::d-empper:EmployedPartTimeMember"
            }, new string[] { "D" }, new string[] { "tax-inc:ActualExemptionAdditionalPersonnel" });

            var elsW4 = GetElementsByScenDefs(new string[] { 
                "d-empf:EmploymentFunctionDimension::d-empf:DepartmentHeadTQMMember/d-empper:EmploymentPeriodDimension::d-empper:EmployedPartTimeMember"
            }, new string[] { "D" }, new string[] { "tax-inc:ActualExemptionAdditionalPersonnel" });


            var ttl = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:ReversalPreviousExemptions" });
            var sumEls = Sum(Number(elsW1, "0"), Number(elsW2, "0"), Number(elsW3, "0"), Number(elsW4, "0"));
            bool test = Number(ttl, "0") == sumEls;

            if (!test)
            {
                AddMessage(new BizTaxErrorDataContract
                {
                    FileId = _fileId,
                    Id = "f-xtra-nrcorp-276W1-4",
                    Messages = new List<BizTaxErrorMessageDataContract> { 
                        new BizTaxErrorMessageDataContract { Culture = "nl-BE", 
                            Message = string.Format(
                                "Het bedrag ({0}€) vemeld in het vak 'Verworpen uitgaven' onder 'Terugnemingen van vroegere vrijstellingen' moet gelijk zijn aan de som ({1}€) bekomen uit <ul>"
                                +"<li> 276W1 'In een vorig belastbaar tijdperk aangeworven personeel waarvan de voltijdse tewerkstelling tijdens het belastbare tijdperk is opgehouden', som van veld 'Effectief vrijgesteld' ({2}€)</li>"
                                +"<li> 276W2 'In een vorig belastbaar tijdperk aangeworven personeel waarvan de voltijdse tewerkstelling tijdens het belastbare tijdperk is opgehouden', som van veld 'Effectief vrijgesteld' ({3}€)</li>"
                                +"<li> 276W3 'Vrijstelling voor bijkomend personeel tewerkgesteld als diensthoofd voor de uitvoer', veld 'Effectief vrijgesteld' onder kolom 'Einde voltijds' ({4}€)</li>"
                                +"<li> 276W4 'Vrijstelling voor bijkomend personeel als diensthoofd van de afdeling Integrale kwaliteitszorg', veld 'Effectief vrijgesteld' onder kolom 'Einde voltijds' ({5}€)</li></ul>"
                                    , Number(ttl,"0").ToString("N",DefaultCulture)
                                    , sumEls.ToString("N",DefaultCulture)
                                    , Number(elsW1,"0").ToString("N",DefaultCulture)
                                    , Number(elsW2,"0").ToString("N",DefaultCulture)
                                    , Number(elsW3,"0").ToString("N",DefaultCulture)
                                    , Number(elsW4,"0").ToString("N",DefaultCulture)
                            )
                        },
                        new BizTaxErrorMessageDataContract { Culture = "fr-FR", 
                            Message = string.Format(
                                "Het bedrag ({0}€) vemeld in het vak 'Verworpen uitgaven' onder 'Terugnemingen van vroegere vrijstellingen' moet gelijk zijn aan de som ({1}€) bekomen uit <ul>"
                                +"<li> 276W1 'In een vorig belastbaar tijdperk aangeworven personeel waarvan de voltijdse tewerkstelling tijdens het belastbare tijdperk is opgehouden', som van veld 'Effectief vrijgesteld' ({2}€)</li>"
                                +"<li> 276W2 'In een vorig belastbaar tijdperk aangeworven personeel waarvan de voltijdse tewerkstelling tijdens het belastbare tijdperk is opgehouden', som van veld 'Effectief vrijgesteld' ({3}€)</li>"
                                +"<li> 276W3 'Vrijstelling voor bijkomend personeel tewerkgesteld als diensthoofd voor de uitvoer', veld 'Effectief vrijgesteld' onder kolom 'Einde voltijds' ({4}€)</li>"
                                +"<li> 276W4 'Vrijstelling voor bijkomend personeel als diensthoofd van de afdeling Integrale kwaliteitszorg', veld 'Effectief vrijgesteld' onder kolom 'Einde voltijds' ({5}€)</li></ul>"
                                    , Number(ttl,"0").ToString("N",DefaultCulture)
                                    , sumEls.ToString("N",DefaultCulture)
                                    , Number(elsW1,"0").ToString("N",DefaultCulture)
                                    , Number(elsW2,"0").ToString("N",DefaultCulture)
                                    , Number(elsW3,"0").ToString("N",DefaultCulture)
                                    , Number(elsW4,"0").ToString("N",DefaultCulture)
                            )
                        },
                        new BizTaxErrorMessageDataContract { Culture = "en-US", 
                            Message = string.Format(
                                "Het bedrag ({0}€) vemeld in het vak 'Verworpen uitgaven' onder 'Terugnemingen van vroegere vrijstellingen' moet gelijk zijn aan de som ({1}€) bekomen uit <ul>"
                                +"<li> 276W1 'In een vorig belastbaar tijdperk aangeworven personeel waarvan de voltijdse tewerkstelling tijdens het belastbare tijdperk is opgehouden', som van veld 'Effectief vrijgesteld' ({2}€)</li>"
                                +"<li> 276W2 'In een vorig belastbaar tijdperk aangeworven personeel waarvan de voltijdse tewerkstelling tijdens het belastbare tijdperk is opgehouden', som van veld 'Effectief vrijgesteld' ({3}€)</li>"
                                +"<li> 276W3 'Vrijstelling voor bijkomend personeel tewerkgesteld als diensthoofd voor de uitvoer', veld 'Effectief vrijgesteld' onder kolom 'Einde voltijds' ({4}€)</li>"
                                +"<li> 276W4 'Vrijstelling voor bijkomend personeel als diensthoofd van de afdeling Integrale kwaliteitszorg', veld 'Effectief vrijgesteld' onder kolom 'Einde voltijds' ({5}€)</li></ul>"
                                    , Number(ttl,"0").ToString("N",DefaultCulture)
                                    , sumEls.ToString("N",DefaultCulture)
                                    , Number(elsW1,"0").ToString("N",DefaultCulture)
                                    , Number(elsW2,"0").ToString("N",DefaultCulture)
                                    , Number(elsW3,"0").ToString("N",DefaultCulture)
                                    , Number(elsW4,"0").ToString("N",DefaultCulture)
                            )
                        },
                       new BizTaxErrorMessageDataContract { Culture = "de-DE", 
                            Message = string.Format(
                                "Het bedrag ({0}€) vemeld in het vak 'Verworpen uitgaven' onder 'Terugnemingen van vroegere vrijstellingen' moet gelijk zijn aan de som ({1}€) bekomen uit <ul>"
                                +"<li> 276W1 'In een vorig belastbaar tijdperk aangeworven personeel waarvan de voltijdse tewerkstelling tijdens het belastbare tijdperk is opgehouden', som van veld 'Effectief vrijgesteld' ({2}€)</li>"
                                +"<li> 276W2 'In een vorig belastbaar tijdperk aangeworven personeel waarvan de voltijdse tewerkstelling tijdens het belastbare tijdperk is opgehouden', som van veld 'Effectief vrijgesteld' ({3}€)</li>"
                                +"<li> 276W3 'Vrijstelling voor bijkomend personeel tewerkgesteld als diensthoofd voor de uitvoer', veld 'Effectief vrijgesteld' onder kolom 'Einde voltijds' ({4}€)</li>"
                                +"<li> 276W4 'Vrijstelling voor bijkomend personeel als diensthoofd van de afdeling Integrale kwaliteitszorg', veld 'Effectief vrijgesteld' onder kolom 'Einde voltijds' ({5}€)</li></ul>"
                                    , Number(ttl,"0").ToString("N",DefaultCulture)
                                    , sumEls.ToString("N",DefaultCulture)
                                    , Number(elsW1,"0").ToString("N",DefaultCulture)
                                    , Number(elsW2,"0").ToString("N",DefaultCulture)
                                    , Number(elsW3,"0").ToString("N",DefaultCulture)
                                    , Number(elsW4,"0").ToString("N",DefaultCulture)
                            )
                        }
                    }
                    ,
                    Fields = new List<string>()
                }
                );
            }
        }

        // ActualExemptionAdditionalPersonnel
        // ActualExemptionAdditionalPersonnel
        // ActualExemptionAdditionalPersonnel


        //      d-empf:EmploymentFunctionDimension/d-empper:EmploymentPeriodDimension/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension
        // NOT: d-empf:EmploymentFunctionDimension/d-empper:EmploymentPeriodDimension/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension
        #endregion

        internal void IndexFiches()
        {
            List<BizTaxFicheInfoDataContract> fiches = new List<BizTaxFicheInfoDataContract>
            {
               
                new BizTaxFicheInfoDataContract { FicheId="Id" }
            };

            if (Xbrl.Elements.Where(x => x.Name == "BasicTaxableAmountCommonRate" && x.GetNumberOrDefault(0) != 0).Count() > 0
                || Xbrl.Elements.Where(x => x.Name == "RemainingFiscalResultBeforeOriginDistribution" && x.GetNumberOrDefault(0) != 0).Count() > 0
                || Xbrl.Elements.Where(x => x.Name == "FiscalResult" && x.GetNumberOrDefault(0) != 0).Count() > 0
                 || Xbrl.Elements.Where(x => x.Name == "AnnualWorkForceAverageCorporationCodeCurrentTaxPeriod" && x.GetNumberOrDefault(0) != 0).Count() > 0)
            {
                fiches.Add(new BizTaxFicheInfoDataContract { FicheId = "275.2.A.1" });
                fiches.Add(new BizTaxFicheInfoDataContract { FicheId = "275.2.B.1", Annexes = Xbrl.Elements.Where(x => 
                    x.BinaryValue != null && x.BinaryValue.Count > 0
                    && (x.Name == "BalanceSheetStatutorySeatStatutoryAccountsBelgianBranch" ||
            x.Name == "ExemptionProfitHomologationReorganizationPlanAmicableSettlementDocuments" ||
            x.Name == "InternalStatutoryAccounts" ||
            x.Name == "DepreciationTableNonStructured" ||
            x.Name == "OtherDocuments")
                    ).ToList() });
            }


            if (Xbrl.Elements.Where(x => x.Name == "LetOutImmovablePropertyIncomeBelgium" && x.GetNumberOrDefault(0) != 0).Count() > 0
                || Xbrl.Elements.Where(x => x.Name == "TaxableImmovablePropertyIncomeBelgium" && x.GetNumberOrDefault(0) != 0).Count() > 0)
            {
                fiches.Add(new BizTaxFicheInfoDataContract { FicheId = "275.2.A.2" });
                fiches.Add(new BizTaxFicheInfoDataContract
                {
                    FicheId = "275.2.B.2",
                    Annexes = Xbrl.Elements.Where(x =>
                        x.BinaryValue != null && x.BinaryValue.Count > 0
                        && (x.Name == "BalanceSheetStatutorySeatStatutoryAccountsBelgianBranchLegalEntity" ||
                        x.Name == "AccountsAssessmentLiabilityTaxRegimeLegalEntityIncomeTaxTaxableIncome" ||
                        x.Name == "OtherDocumentsLegalEntity" )
                        ).ToList()
                });
            }
            

            // 
            //
            //
            if (Xbrl.Elements.Where(x => x.Name == "ExemptWriteDownDebtClaim" && x.GetNumberOrDefault(0) != 0).Count() > 0
                || Xbrl.Elements.Where(x => x.Name == "ExemptProvisionRisksExpenses" && x.GetNumberOrDefault(0) != 0).Count() > 0)
            {
                fiches.Add(new BizTaxFicheInfoDataContract { FicheId = "204.3" });
            }

            List<string> scenarioIds = GetScenarioIds(new string[] { "d-asst:AssetTypeDimension::d-asst:SeaVesselMember" });
            if (Xbrl.Elements.Count(x => scenarioIds.Contains(x.Context) && !string.IsNullOrEmpty(x.Value)) > 0)
            {
                fiches.Add(new BizTaxFicheInfoDataContract { FicheId = "275B" });
            }

            //D__id__SpecificSecurityMember__id__2010-01
            //D__id__SpecificSecurityMember__id__2013-02__id__2010-01
            scenarioIds = GetScenarioIdsFuzzy(new string[] { "d-asst:AssetTypeDimension::d-asst:SpecificSecurityMember" });
            if (Xbrl.Elements.Count(x => scenarioIds.Contains(x.Context) && !string.IsNullOrEmpty(x.Value)) > 0)
            {
                fiches.Add(new BizTaxFicheInfoDataContract { FicheId = "275K" });
            }

            scenarioIds = GetScenarioIdsFuzzy(new string[] { "d-asst:AssetTypeDimension::d-asst:TangibleIntangibleFixedAssetsMember" });
            if (Xbrl.Elements.Count(x => scenarioIds.Contains(x.Context) && !string.IsNullOrEmpty(x.Value)) > 0)
            {
                fiches.Add(new BizTaxFicheInfoDataContract { FicheId = "276K" });
            }

            scenarioIds = GetScenarioIdsFuzzy(new string[] { "d-asst:AssetTypeDimension::d-asst:CorporateVehicleMember" });
            if (Xbrl.Elements.Count(x => scenarioIds.Contains(x.Context) && !string.IsNullOrEmpty(x.Value)) > 0)
            {
                fiches.Add(new BizTaxFicheInfoDataContract { FicheId = "276N" });
            }

            scenarioIds = GetScenarioIdsFuzzy(new string[] { "d-asst:AssetTypeDimension::d-asst:RiverVesselMember" });
            if (Xbrl.Elements.Count(x => scenarioIds.Contains(x.Context) && !string.IsNullOrEmpty(x.Value)) > 0)
            {
                fiches.Add(new BizTaxFicheInfoDataContract { FicheId = "276P" });
            }

            if (Xbrl.Elements.Where(x => x.Name == "AllowanceCorporateEquityCurrentTaxPeriod" && x.GetNumberOrDefault(0) != 0).Count() > 0
                || Xbrl.Elements.Where(x => x.Name.StartsWith("ExemptionsAllowanceCorporateEquityAccumulatedUpToAndIncludingAssessmentYear") && x.GetNumberOrDefault(0) != 0).Count() > 0)
            {
                fiches.Add(new BizTaxFicheInfoDataContract { FicheId = "275C" });
            }

            if (Xbrl.Elements.Where(x => x.Name == "PaymentCertainState" && x.GetNumberOrDefault(0) != 0).Count() > 0)
            {
                fiches.Add(new BizTaxFicheInfoDataContract { FicheId = "275F" });
            }

            if (Xbrl.Elements.Where(x => x.Name == "CalculationBasisDeductionPatentsIncome" && x.GetNumberOrDefault(0) != 0).Count() > 0)
            {
                fiches.Add(new BizTaxFicheInfoDataContract { FicheId = "275P" });
            }


            if (Xbrl.Elements.Where(x => x.Name == "CorrectedIncreaseTaxableReservesInvestmentReserveExcluded" && x.GetNumberOrDefault(0) != 0).Count() > 0
               || Xbrl.Elements.Where(x => x.Name == "ExemptInvestmentReserveOverview" && x.GetNumberOrDefault(0) != 0).Count() > 0
                || Xbrl.Elements.Where(x => x.Name == "InvestBalance" && x.GetNumberOrDefault(0) != 0).Count() > 0)
            {
                fiches.Add(new BizTaxFicheInfoDataContract { FicheId = "275R" });
            }

            // d-incc:IncentiveCategoryDimension/d-invc:InvestmentCategoryDimension (?)
            if (Xbrl.Elements.Where(x => x.Name == "DeductibleAllowanceInvestmentDeductionIncompatibleTaxCreditResearchDevelopment" && x.GetNumberOrDefault(0) != 0).Count() > 0
               || Xbrl.Elements.Where(x => x.Name == "CarryOverInvestmentDeduction" && x.GetNumberOrDefault(0) != 0).Count() > 0
               || Xbrl.Elements.Where(x => x.Name == "DeductibleAllowanceInvestmentDeductionCompatibleTaxCreditResearchDevelopment" && x.GetNumberOrDefault(0) != 0).Count() > 0
               || Xbrl.Elements.Where(x => x.Name == "CarryOverInvestmentDeductionCompatibleTaxCreditResearchDevelopment" && x.GetNumberOrDefault(0) != 0).Count() > 0)
            {
                fiches.Add(new BizTaxFicheInfoDataContract { FicheId = "275U" });
            }


            if (Xbrl.Elements.Where(x => x.Name == "TaxCreditResearchDevelopment" && x.GetNumberOrDefault(0) != 0).Count() > 0
              || Xbrl.Elements.Where(x => x.Name == "ClearableTaxCreditResearchDevelopmentCurrentTaxPeriod" && x.GetNumberOrDefault(0) != 0).Count() > 0
              )
            {
                fiches.Add(new BizTaxFicheInfoDataContract { FicheId = "275W" });
            }
            //
            //
            //d-empreg:EmploymentRegimeDimension/d-empstat:EmploymentStatusDimension/d-per:PeriodDimension/d-tu:TimeUnitDimension/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension


            List<string> crefs = Xbrl.Contexts.Where(c => c.ScenarioDef == "d-empreg:EmploymentRegimeDimension/d-empstat:EmploymentStatusDimension/d-per:PeriodDimension/d-tu:TimeUnitDimension/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension").Select(c => c.Id).ToList();
            if (Xbrl.Elements.Count(x => crefs.Contains(x.ContextRef) && x.GetNumberOrDefault(0) != 0) > 0)
            {
                fiches.Add(new BizTaxFicheInfoDataContract { FicheId = "276T" });
            }

            scenarioIds = GetScenarioIds(new string[] { 
                "d-empf:EmploymentFunctionDimension::d-empf:HighlyQualifiedResearcherMember/d-empper:EmploymentPeriodDimension::d-empper:EmployedFullTimeMember/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension",
                "d-empf:EmploymentFunctionDimension::d-empf:HighlyQualifiedResearcherMember/d-empper:EmploymentPeriodDimension::d-empper:EmployedPartTimeMember/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension",
                "d-empf:EmploymentFunctionDimension::d-empf:ResearcherMember/d-empper:EmploymentPeriodDimension::d-empper:EmployedPartTimeMember/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension",
                "d-empf:EmploymentFunctionDimension::d-empf:ResearcherMember/d-empper:EmploymentPeriodDimension::d-empper:EmployedFullTimeMember/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension"
            });
            if (Xbrl.Elements.Count(x => scenarioIds.Contains(x.Context) && !string.IsNullOrEmpty(x.Value)) > 0)
            {
                fiches.Add(new BizTaxFicheInfoDataContract { FicheId = "276W1" });
            }

            scenarioIds = GetScenarioIds(new string[] { 
                "d-empf:EmploymentFunctionDimension::d-empf:IncreaseTechnologicalPotentialPersonnelMember/d-empper:EmploymentPeriodDimension::d-empper:EmployedFullTimeMember/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension",
                "d-empf:EmploymentFunctionDimension::d-empf:IncreaseTechnologicalPotentialPersonnelMember/d-empper:EmploymentPeriodDimension::d-empper:EmployedPartTimeMember/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension"
            });
            if (Xbrl.Elements.Count(x => scenarioIds.Contains(x.Context) && !string.IsNullOrEmpty(x.Value)) > 0)
            {
                fiches.Add(new BizTaxFicheInfoDataContract { FicheId = "276W2" });
            }

            scenarioIds = GetScenarioIds(new string[] { 
                "d-empf:EmploymentFunctionDimension::d-empf:DepartmentHeadExportMember/d-empper:EmploymentPeriodDimension::d-empper:RecruitmentFullTimeMember",
                "d-empf:EmploymentFunctionDimension::d-empf:DepartmentHeadExportMember/d-empper:EmploymentPeriodDimension::d-empper:EmployedFullTimeTransferTaxPeriodMember",
                "d-empf:EmploymentFunctionDimension::d-empf:DepartmentHeadExportMember/d-empper:EmploymentPeriodDimension::d-empper:RecruitmentFullTimeReplacementMember",
                "d-empf:EmploymentFunctionDimension::d-empf:DepartmentHeadExportMember/d-empper:EmploymentPeriodDimension::d-empper:EmployedFullTimeMember",
                "d-empf:EmploymentFunctionDimension::d-empf:DepartmentHeadExportMember/d-empper:EmploymentPeriodDimension::d-empper:EmployedPartTimeMember"
            });
            if (Xbrl.Elements.Count(x => scenarioIds.Contains(x.Context) && !string.IsNullOrEmpty(x.Value)) > 0)
            {
                fiches.Add(new BizTaxFicheInfoDataContract { FicheId = "276W3" });
            }

            scenarioIds = GetScenarioIds(new string[] { 
                "d-empf:EmploymentFunctionDimension::d-empf:DepartmentHeadTQMMember/d-empper:EmploymentPeriodDimension::d-empper:RecruitmentFullTimeMember",
                "d-empf:EmploymentFunctionDimension::d-empf:DepartmentHeadTQMMember/d-empper:EmploymentPeriodDimension::d-empper:EmployedFullTimeTransferTaxPeriodMember",
                "d-empf:EmploymentFunctionDimension::d-empf:DepartmentHeadTQMMember/d-empper:EmploymentPeriodDimension::d-empper:RecruitmentFullTimeReplacementMember",
                "d-empf:EmploymentFunctionDimension::d-empf:DepartmentHeadTQMMember/d-empper:EmploymentPeriodDimension::d-empper:EmployedFullTimeMember",
                "d-empf:EmploymentFunctionDimension::d-empf:DepartmentHeadTQMMember/d-empper:EmploymentPeriodDimension::d-empper:EmployedPartTimeMember"
            });
            if (Xbrl.Elements.Count(x => scenarioIds.Contains(x.Context) && !string.IsNullOrEmpty(x.Value)) > 0)
            {
                fiches.Add(new BizTaxFicheInfoDataContract { FicheId = "276W3" });
            }

            scenarioIds = GetScenarioIds(new string[] { 
                "d-depm:DepreciationMethodDimension::d-depm:DegressiveDepreciationMethodMember/d-ty:DescriptionTypedDimension/d-ty:NatureTypedDimension"
            });
            if (Xbrl.Elements.Count(x => scenarioIds.Contains(x.Context) && !string.IsNullOrEmpty(x.Value)) > 0)
            {
                fiches.Add(new BizTaxFicheInfoDataContract { FicheId = "328K" });
            }

            scenarioIds = GetScenarioIds(new string[] { 
                "d-depm:DepreciationMethodDimension::d-depm:RenunciationDegressiveDepreciationMethodMember/d-ty:DescriptionTypedDimension/d-ty:NatureTypedDimension"
            });
            if (Xbrl.Elements.Count(x => scenarioIds.Contains(x.Context) && !string.IsNullOrEmpty(x.Value)) > 0)
            {
                fiches.Add(new BizTaxFicheInfoDataContract { FicheId = "328L" });
            }


            this.Xbrl.Fiches = fiches;
        }

        private void _ApplyFieldsToCalc(ref TaxCalc.TaxCalc tx)
        {


            tx.BerekeningOverzicht.AssessmentYear = 2014;

            /* TARIEVEN */
            #region TARIEVEN


            tx.BerekeningOverzicht.CapitalGainsShares0412Perc = (decimal)0.412;
            tx.BerekeningOverzicht.CapitalGainsShares25Perc = (decimal)25.75;
            tx.BerekeningOverzicht.ExitTarifPerc = (decimal)16.995;

            tx.FairnessTax.FairnessTaxPerc = (decimal)5.15;

            tx.BerekeningOverzicht.EenduidigTariefPerc = (decimal)33.99;

            //If CreditCorporationTradeEquipmentHousingCorporationTaxRate (1752) is checked, apply lower tax rate
            tx.BerekeningOverzicht.CreditCorporationTradeEquipmentHousingCorporationTaxRate = this.Bool(this.Xbrl.Elements.FirstOrDefault(x => x.Period == "D" && x.Name == "CreditCorporationTradeEquipmentHousingCorporationTaxRate"));

            if (tx.BerekeningOverzicht.CreditCorporationTradeEquipmentHousingCorporationTaxRate.GetValueOrDefault())
            {
                tx.BerekeningOverzicht.EenduidigTariefPerc = (decimal)5.15;
            }

            tx.BerekeningOverzicht.VerminderdtariefStap1Base = (decimal)25000;

            tx.BerekeningOverzicht.VerminderdtariefStap2Base = (decimal)90000;

            tx.BerekeningOverzicht.VerminderdtariefStap3Base = (decimal)322500;

            tx.BerekeningOverzicht.VerminderdtariefStap1Perc = (decimal)24.9775;

            tx.BerekeningOverzicht.VerminderdtariefStap2Perc = (decimal)31.93;

            tx.BerekeningOverzicht.VerminderdtariefStap3Perc = (decimal)35.535;

            tx.BerekeningOverzicht.AP1Percentage = (decimal)3;

            tx.BerekeningOverzicht.AP2Percentage = (decimal)2.5;

            tx.BerekeningOverzicht.AP3Percentage = (decimal)2;

            tx.BerekeningOverzicht.AP4Percentage = (decimal)1.5;

            tx.BerekeningOverzicht.BaseForIncreasePerc = (tx.BerekeningOverzicht.AP1Percentage.GetValueOrDefault()
                + tx.BerekeningOverzicht.AP2Percentage.GetValueOrDefault() + tx.BerekeningOverzicht.AP3Percentage.GetValueOrDefault()
                + tx.BerekeningOverzicht.AP4Percentage.GetValueOrDefault()) / 4;


            tx.BerekeningOverzicht.KapitaalEnInterestsubsidiesPerc = (decimal)5;

            tx.BerekeningOverzicht.UnjustifiedExpensesHiddenSurplusPerc = (decimal)309.00;

            tx.BerekeningOverzicht.SeparateAssessmentTaxedReserves3400Perc = (decimal)35.02;

            tx.BerekeningOverzicht.SeparateAssessmentTaxedReserves2800Perc = (decimal)28.84;


            tx.BerekeningOverzicht.SeparateAssessmentDividendsPaidPerc = (decimal)28.84;

            tx.BerekeningOverzicht.SeparateAssessmentTaxPaidTaxedReservesPerc = (decimal)15.00;

            tx.BerekeningOverzicht.DistributionCompanyAssets3300Perc = (decimal)33.00;

            tx.BerekeningOverzicht.DistributionCompanyAssets1650Perc = (decimal)16.50;

            tx.BerekeningOverzicht.BenefitsAllKindCompanyLiquidationPerc = (decimal)33.00;

            tx.BerekeningOverzicht.AdditionalDutiesDiamondTradersPerc = (decimal)10.00;

            tx.BerekeningOverzicht.RetributionResearchDevelopmentPerc = (decimal)100;

            #endregion


            /* IMPORT */
            tx.BerekeningOverzicht.StatAccountingResult = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Period == "I-End" && x.Name == "LegalReserve"))
                - this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Period == "I-Start" && x.Name == "LegalReserve"))
                + this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Period == "I-End" && x.Name == "UnavailableReserves"))
                - this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Period == "I-Start" && x.Name == "UnavailableReserves"))
                + this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Period == "I-End" && x.Name == "AvailableReserves"))
                - this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Period == "I-Start" && x.Name == "AvailableReserves"))
                 + this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Period == "I-End" && x.Name == "AccumulatedProfitsLosses"))
                - this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Period == "I-Start" && x.Name == "AccumulatedProfitsLosses"))
                + this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "TaxableDividendsPaid"));


            tx.BelasteReserves.TDKapitaalEnUitgifte = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Period == "I-End" && x.Name == "TaxableReservesCapitalSharePremiums"))
              - this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Period == "I-Start" && x.Name == "TaxableReservesCapitalSharePremiums"));
            tx.BelasteReserves.TDHerwaardering = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Period == "I-End" && x.Name == "TaxablePortionRevaluationSurpluses"))
              - this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Period == "I-Start" && x.Name == "TaxablePortionRevaluationSurpluses"));
            tx.BelasteReserves.TDTaxableProvisions = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Period == "I-End" && x.Name == "TaxableProvisions"))
              - this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Period == "I-Start" && x.Name == "TaxableProvisions"));

            tx.BelasteReserves.TDOtherTaxableReserves = this.Number(this.Xbrl.Elements.Where(x => x.Period == "I-End" && x.Name == "OtherReserves"), "0")
             - this.Number(this.Xbrl.Elements.Where(x => x.Period == "I-Start" && x.Name == "OtherReserves"), "0");

            tx.BelasteReserves.TDOtherNonBalanceTaxableReserves = this.Number(this.Xbrl.Elements.Where(x => x.Period == "I-End" && x.Name == "OtherTaxableReserves"), "0")
             - this.Number(this.Xbrl.Elements.Where(x => x.Period == "I-Start" && x.Name == "OtherTaxableReserves"), "0");

            tx.BelasteReserves.TDTaxableDoubtfullDebtors = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Period == "I-End" && x.Name == "TaxableWriteDownsUndisclosedReserve"))
              - this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Period == "I-Start" && x.Name == "TaxableWriteDownsUndisclosedReserve"));

            tx.BelasteReserves.TDExcessDeprications = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Period == "I-End" && x.Name == "ExaggeratedDepreciationsUndisclosedReserve"))
              - this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Period == "I-Start" && x.Name == "ExaggeratedDepreciationsUndisclosedReserve"));

            tx.BelasteReserves.TDUnderestimationAssetsOverestimationLiabilities = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Period == "I-End" && x.Name == "OtherUnderestimationsAssetsUndisclosedReserve"))
              - this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Period == "I-Start" && x.Name == "OtherUnderestimationsAssetsUndisclosedReserve"));

            tx.BelasteReserves.TDOverestimationLiabilities = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Period == "I-End" && x.Name == "OtherOverestimationsLiabilitiesUndisclosedReserve"))
              - this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Period == "I-Start" && x.Name == "OtherOverestimationsLiabilitiesUndisclosedReserve"));

            tx.BelasteReserves.TDTotal = tx.BelasteReserves.TDTaxableProvisions.GetValueOrDefault()
                                              + tx.BelasteReserves.TDOtherTaxableReserves.GetValueOrDefault()
                                              + tx.BelasteReserves.TDOtherNonBalanceTaxableReserves.GetValueOrDefault()
                                              + tx.BelasteReserves.TDTaxableDoubtfullDebtors.GetValueOrDefault()
                                              + tx.BelasteReserves.TDExcessDeprications.GetValueOrDefault()
                                              + tx.BelasteReserves.TDUnderestimationAssetsOverestimationLiabilities.GetValueOrDefault()
                                              + tx.BelasteReserves.TDKapitaalEnUitgifte.GetValueOrDefault()
                                              + tx.BelasteReserves.TDOverestimationLiabilities.GetValueOrDefault()
                                              + tx.BelasteReserves.TDHerwaardering.GetValueOrDefault();


            tx.BelasteReserves.OACapitalGainsOnShares = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "CapitalGainsShares"));
            //ONTBREEKT IN BEL BEREK:: Terugnemingen van vroegere in verworpen uitgaven opgenomen waardeverminderingen op aandelen > 1052 > CapitalGainsSharesReversalPreviousExemptionsAdjustmentReservesPlus
            tx.BelasteReserves.OACapitalGainsSharesReversal = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "CapitalGainsSharesReversalPreviousExemptionsAdjustmentReservesPlus"));

            tx.BelasteReserves.OADefinitiveExemptionTaxShelter = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "DefinitiveExemptionTaxShelterAuthorisedAudiovisualWorksAdjustmentReservesPlus"));

            tx.BelasteReserves.OAExemptionRegionalPrem = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "ExemptionRegionalGrantsCapitalSubsidiesInterestSubsidiesAdjustmentReservesPlus"));
            //ONTBREEKT IN BEL BEREK:: Definitieve vijstelling winst voortvloeiend uit de homologatie van een reorganisatieplan > 1055 > FinalExemptionProfitHomologationReorganizationPlanAmicableSettlement

            tx.BelasteReserves.OAFinalExemptionProfit = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "FinalExemptionProfitHomologationReorganizationPlanAmicableSettlement"));


            tx.BelasteReserves.OAOthers = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "OtherAdjustmentsReservesPlus"));

            tx.BelasteReserves.OAInTheMin = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "AdjustmentsReservesMinus"));

            tx.BelasteReserves.OATotal = tx.BelasteReserves.OACapitalGainsOnShares.GetValueOrDefault()
                                      + tx.BelasteReserves.OADefinitiveExemptionTaxShelter.GetValueOrDefault()
                                      + tx.BelasteReserves.OAExemptionRegionalPrem.GetValueOrDefault()
                                      + tx.BelasteReserves.OAOthers.GetValueOrDefault()
                                      + tx.BelasteReserves.OACapitalGainsSharesReversal.GetValueOrDefault()
                                      + tx.BelasteReserves.OAFinalExemptionProfit.GetValueOrDefault();

            if (tx.BelasteReserves.OATotal.GetValueOrDefault() == 0 && tx.BelasteReserves.OAInTheMin.GetValueOrDefault() > 0)
            {
                tx.BelasteReserves.OATotal = tx.BelasteReserves.OATotal.GetValueOrDefault() - tx.BelasteReserves.OAInTheMin.GetValueOrDefault();
            }
            else
            {
                tx.BelasteReserves.OAInTheMin = 0;
            }



            tx.VerworpenUitgaven.PDDisallowedCurrentIncomeTaxExpenses = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "NonDeductibleTaxes"));

            tx.VerworpenUitgaven.DERegionalTaxes = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "NonDeductibleRegionalTaxesDutiesRetributions"));
            tx.VerworpenUitgaven.DEPenalties = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "NonDeductibleFinesConfiscationsPenaltiesAllKind"));
            tx.VerworpenUitgaven.DEPensions = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "NonDeductiblePensionsCapitalEmployerContributionsEmployerPremiums"));
            tx.VerworpenUitgaven.DECar = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "NonDeductibleCarExpensesLossValuesCars"));
            tx.VerworpenUitgaven.DECarVAA = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "NonDeductibleCarExpensesPartBenefitsAllKind"));
            tx.VerworpenUitgaven.DEReception = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "NonDeductibleReceptionBusinessGiftsExpenses"));
            tx.VerworpenUitgaven.DERestaurant = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "NonDeductibleRestaurantExpenses"));
            tx.VerworpenUitgaven.DEClothes = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "NonDeductibleNonSpecificProfessionalClothsExpenses"));
            tx.VerworpenUitgaven.DEExcessInterest = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "ExaggeratedInterests"));
            tx.VerworpenUitgaven.DEInterestLoans = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "NonDeductibleParticularPortionInterestsLoans"));
            tx.VerworpenUitgaven.DEAbnormalAdvantages = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "AbnormalBenevolentAdvantages"));
            tx.VerworpenUitgaven.DESocial = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "NonDeductibleSocialAdvantages"));
            tx.VerworpenUitgaven.DEDinerSportCultureEco = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "NonDeductibleAdvantagesLuncheonSportsCultureEcoVouchers"));
            tx.VerworpenUitgaven.DECharity = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "Liberalities"));
            tx.VerworpenUitgaven.DEReductionValueShares = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "WriteDownsLossValuesShares"));
            tx.VerworpenUitgaven.DEReversalsPreviousTax = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "ReversalPreviousExemptions"));
            tx.VerworpenUitgaven.DEEmployeeCapital = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "EmployeeParticipation"));
            tx.VerworpenUitgaven.DEIndemnityMissingCoupon = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "IndemnityMissingCoupon"));
            tx.VerworpenUitgaven.DEExpensesTaxShelter = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "ExpensesTaxShelterAuthorisedAudiovisualWork"));
            tx.VerworpenUitgaven.DERegionalPremiumsCapital = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "RegionalPremiumCapitalSubsidiesInterestSubsidies"));
            tx.VerworpenUitgaven.DEBepaaldeStaten = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "NonDeductiblePaymentsCertainStates"));
            tx.VerworpenUitgaven.DEOther = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "OtherDisallowedExpenses"));
            tx.VerworpenUitgaven.DEUnjustifiedExpenses = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "UnjustifiedExpensesBenefitsAllKindIncludedInAssessmentInNameOfBeneficiary"));

            tx.VerworpenUitgaven.DEOther = tx.VerworpenUitgaven.DEOther.GetValueOrDefault() + this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "ProfitTransferBelgiumAbroad"));
            tx.VerworpenUitgaven.DETotal = tx.VerworpenUitgaven.DEAbnormalAdvantages.GetValueOrDefault()
                                              + tx.VerworpenUitgaven.DEBepaaldeStaten.GetValueOrDefault()
                                              + tx.VerworpenUitgaven.DECar.GetValueOrDefault()
                                              + tx.VerworpenUitgaven.DECarVAA.GetValueOrDefault()
                                              + tx.VerworpenUitgaven.DECharity.GetValueOrDefault()
                                              + tx.VerworpenUitgaven.DEClothes.GetValueOrDefault()
                                              + tx.VerworpenUitgaven.DEDinerSportCultureEco.GetValueOrDefault()
                                              + tx.VerworpenUitgaven.DEEmployeeCapital.GetValueOrDefault()
                                              + tx.VerworpenUitgaven.DEExcessInterest.GetValueOrDefault()
                                              + tx.VerworpenUitgaven.DEExpensesTaxShelter.GetValueOrDefault()
                                              + tx.VerworpenUitgaven.DEIndemnityMissingCoupon.GetValueOrDefault()
                                              + tx.VerworpenUitgaven.DEInterestLoans.GetValueOrDefault()
                                              + tx.VerworpenUitgaven.DEOther.GetValueOrDefault()
                                              + tx.VerworpenUitgaven.DEUnjustifiedExpenses.GetValueOrDefault()
                                              + tx.VerworpenUitgaven.DEPenalties.GetValueOrDefault()
                                              + tx.VerworpenUitgaven.DEPensions.GetValueOrDefault()
                                              + tx.VerworpenUitgaven.DEReception.GetValueOrDefault()
                                              + tx.VerworpenUitgaven.DEReductionValueShares.GetValueOrDefault()
                                              + tx.VerworpenUitgaven.DERegionalPremiumsCapital.GetValueOrDefault()
                                              + tx.VerworpenUitgaven.DERegionalTaxes.GetValueOrDefault()
                                              + tx.VerworpenUitgaven.DERestaurant.GetValueOrDefault()
                                              + tx.VerworpenUitgaven.DEReversalsPreviousTax.GetValueOrDefault()
                                              + tx.VerworpenUitgaven.DESocial.GetValueOrDefault();


            //
            tx.BelastbareBestZonderAftrek.TECapitalGains0412 = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "CapitalGainsSharesRate0040"));
            tx.BelastbareBestZonderAftrek.TEReceivedAbnormal = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "BenevolentAbnormalFinancialAdvantagesBenefitsAllKind"));
            tx.BelastbareBestZonderAftrek.TETaxableInvestmentReserve = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "ViolationInvestmentCommitmentIntangibilityConditionInvestmentReserve"));
            tx.BelastbareBestZonderAftrek.TEVaaCar = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "NonDeductibleCarExpensesPartBenefitsAllKind"));

            tx.BelastbareBestZonderAftrek.TEEmployeeParticipationCapital = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "EmployeeParticipation"));
            tx.BelastbareBestZonderAftrek.TECapitalAgriCulture = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "CapitalSubsidiesInterestSubsidiesAgriculturalSupport"));

            tx.BelastbareBestZonderAftrek.TETotal = tx.BelastbareBestZonderAftrek.TECapitalGains0412.GetValueOrDefault() + tx.BelastbareBestZonderAftrek.TEReceivedAbnormal.GetValueOrDefault() + tx.BelastbareBestZonderAftrek.TETaxableInvestmentReserve.GetValueOrDefault()
              + tx.BelastbareBestZonderAftrek.TEEmployeeParticipationCapital.GetValueOrDefault() + tx.BelastbareBestZonderAftrek.TECapitalAgriCulture.GetValueOrDefault() + tx.BelastbareBestZonderAftrek.TEVaaCar.GetValueOrDefault();


            // OpdelingNaarOorsprong
            tx.OpdelingNaarOorsprong.IRTreatyForeignIncome = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "RemainingFiscalResult" && x.ContextRef == "D__id__TaxTreatyMember"));
            tx.OpdelingNaarOorsprong.IRNonTreatyForeignIncome = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "RemainingFiscalResult" && x.ContextRef == "D__id__NoTaxTreatyMember"));
            tx.OpdelingNaarOorsprong.IRBelgianTaxable = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "RemainingFiscalResult" && x.ContextRef == "D__id__BelgiumMember"));


            tx.FiscaleAftrek.NTEBECharity = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "ExemptGifts"));
            tx.FiscaleAftrek.NTEBEPersonnel = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "ExemptionAdditionalPersonnelMiscellaneousExemptions"));
            tx.FiscaleAftrek.NTEBEPersonnelSME = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "ExemptionAdditionalPersonnelSMEs"));
            tx.FiscaleAftrek.NTEBEInternalshipPremium = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "ExemptionTrainingPeriodBonus"));
            tx.FiscaleAftrek.NTEBEOthers = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "OtherMiscellaneousExemptions"));


            tx.FiscaleAftrek.TotalNonTaxableElements = (((tx.FiscaleAftrek.NTEBECharity.GetValueOrDefault() + tx.FiscaleAftrek.NTEBEPersonnel.GetValueOrDefault())
                                                    + tx.FiscaleAftrek.NTEBEPersonnelSME.GetValueOrDefault()) + tx.FiscaleAftrek.NTEBEInternalshipPremium.GetValueOrDefault())
                                                    + tx.FiscaleAftrek.NTEBEOthers.GetValueOrDefault();

            /* #165
            tx.FiscaleAftrek.PEBelgium = this.Number(this.Xbrl.Elements.Where(x => x.Name == "PEExemptIncomeMovableAssets" && x.ContextRef != "D"), "0");
            tx.FiscaleAftrek.PIDBelgium = this.Number(this.Xbrl.Elements.Where(x => x.Name == "DeductionPatentsIncome" && x.ContextRef != "D"), "0");
            tx.FiscaleAftrek.NIDBelgium = this.Number(this.Xbrl.Elements.Where(x => x.Name == "AllowanceCorporateEquity" && x.ContextRef != "D"), "0");
            tx.FiscaleAftrek.DTLBelgium = this.Number(this.Xbrl.Elements.Where(x => x.Name == "CompensatedTaxLosses" && x.ContextRef != "D"), "0");
            tx.FiscaleAftrek.IDBelgium = this.Number(this.Xbrl.Elements.Where(x => x.Name == "AllowanceInvestmentDeduction" && x.ContextRef != "D"), "0");
            tx.FiscaleAftrek.NIDHistoryBelgium = this.Number(this.Xbrl.Elements.Where(x => x.Name == "DeductionExemptionsAllowanceCorporateEquityAccumulatedUpToAndIncludingAssessmentYear2012" && x.ContextRef != "D"), "0");
             */

            tx.FiscaleAftrek.PEBelgium = this.Number(this.Xbrl.Elements.Where(x => x.Name == "PEExemptIncomeMovableAssets" && x.ContextRef != "D"), "0") != 0 ? this.Number(this.Xbrl.Elements.Where(x => x.Name == "PEExemptIncomeMovableAssets" && x.ContextRef != "D"), "0") : this.Number(this.Xbrl.Elements.Where(x => x.Name == "PEExemptIncomeMovableAssets" && x.ContextRef == "D"), "0");
            tx.FiscaleAftrek.PIDBelgium = this.Number(this.Xbrl.Elements.Where(x => x.Name == "DeductionPatentsIncome" && x.ContextRef != "D"), "0") != 0 ? this.Number(this.Xbrl.Elements.Where(x => x.Name == "DeductionPatentsIncome" && x.ContextRef != "D"), "0") : this.Number(this.Xbrl.Elements.Where(x => x.Name == "DeductionPatentsIncome" && x.ContextRef == "D"), "0");
            tx.FiscaleAftrek.NIDBelgium = this.Number(this.Xbrl.Elements.Where(x => x.Name == "AllowanceCorporateEquity" && x.ContextRef != "D"), "0") != 0 ? this.Number(this.Xbrl.Elements.Where(x => x.Name == "AllowanceCorporateEquity" && x.ContextRef != "D"), "0") : this.Number(this.Xbrl.Elements.Where(x => x.Name == "AllowanceCorporateEquity" && x.ContextRef == "D"), "0");
            tx.FiscaleAftrek.DTLBelgium = this.Number(this.Xbrl.Elements.Where(x => x.Name == "CompensatedTaxLosses" && x.ContextRef != "D"), "0") != 0 ? this.Number(this.Xbrl.Elements.Where(x => x.Name == "CompensatedTaxLosses" && x.ContextRef != "D"), "0") : this.Number(this.Xbrl.Elements.Where(x => x.Name == "CompensatedTaxLosses" && x.ContextRef == "D"), "0");
            tx.FiscaleAftrek.IDBelgium = this.Number(this.Xbrl.Elements.Where(x => x.Name == "AllowanceInvestmentDeduction" && x.ContextRef != "D"), "0") != 0 ? this.Number(this.Xbrl.Elements.Where(x => x.Name == "AllowanceInvestmentDeduction" && x.ContextRef != "D"), "0") : this.Number(this.Xbrl.Elements.Where(x => x.Name == "AllowanceInvestmentDeduction" && x.ContextRef == "D"), "0");
            tx.FiscaleAftrek.NIDHistoryBelgium = this.Number(this.Xbrl.Elements.Where(x => x.Name == "DeductionExemptionsAllowanceCorporateEquityAccumulatedUpToAndIncludingAssessmentYear2012" && x.ContextRef != "D"), "0") != 0 ? this.Number(this.Xbrl.Elements.Where(x => x.Name == "DeductionExemptionsAllowanceCorporateEquityAccumulatedUpToAndIncludingAssessmentYear2012" && x.ContextRef != "D"), "0") : this.Number(this.Xbrl.Elements.Where(x => x.Name == "DeductionExemptionsAllowanceCorporateEquityAccumulatedUpToAndIncludingAssessmentYear2012" && x.ContextRef == "D"), "0");

            tx.FiscaleAftrek.TotalNonTaxableElements = (((tx.FiscaleAftrek.NTEBECharity.GetValueOrDefault() + tx.FiscaleAftrek.NTEBEPersonnel.GetValueOrDefault())
                                                 + tx.FiscaleAftrek.NTEBEPersonnelSME.GetValueOrDefault()) + tx.FiscaleAftrek.NTEBEInternalshipPremium.GetValueOrDefault())
                                                 + tx.FiscaleAftrek.NTEBEOthers.GetValueOrDefault();
            tx.FiscaleAftrek.TotalFiscaleAftrek = ((((tx.FiscaleAftrek.TotalNonTaxableElements + tx.FiscaleAftrek.PEBelgium) + tx.FiscaleAftrek.PIDBelgium) + tx.FiscaleAftrek.NIDBelgium) + tx.FiscaleAftrek.DTLBelgium) + tx.FiscaleAftrek.IDBelgium + tx.FiscaleAftrek.NIDHistoryBelgium.GetValueOrDefault();

            /*
             * MISSING DETAILS IN BEREK.
             * 
              Winst uit zeescheepvaart, vastgesteld op basis van de tonnage		1461
              Verkregen abnormale of goedgunstige voordelen en verkregen financiële voordelen van alle aard		1421
              Niet-naleving investeringsverplichting of onaantastbaarheidsvoorwaarde voor de investeringsreserve		1422
              Autokosten ten belope van een gedeelte van het voordeel van alle aard		1206
              Werknemersparticipatie		1219

             * 
             */

            // Afzonderlijke aanslagen

            tx.VerrekenbareVoorheffing.VVNTFictieveRoerendeVoorheffing = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "NonRepayableFictiousWitholdingTax"));
            tx.VerrekenbareVoorheffing.VVNTForfaitairBuitenlands = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "NonRepayableLumpSumForeignTaxes"));
            tx.VerrekenbareVoorheffing.VVNTBelastingKredietOnderzoekOntw = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "TaxCreditResearchDevelopment"));


            tx.VerrekenbareVoorheffing.VVTBelgisch = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "RepayableRealFictitiousWithholdingTaxOtherSharesPEExemptIncomeMovableAssetsBelgium"));
            tx.VerrekenbareVoorheffing.VVTAndereBuitenlands = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "RepayableWithholdingTaxPEExemptionLiquidationBonusesBonusesAcquisitionOwnSharesBelgiumForeign"));
            tx.VerrekenbareVoorheffing.VVTEigenBuitenlands = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "RepayableWithholdingTaxOtherPEForeign"));
            tx.VerrekenbareVoorheffing.VVTAndereEigen = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "RepayableWithholdingTaxOtherLiquidationBonusesBonusesAcquisitionOwnShares"));
            tx.VerrekenbareVoorheffing.VVTAndereRoerendeVoorheffing = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "RepayableWithholdingTaxOtherDividends"));
            tx.VerrekenbareVoorheffing.VVTAndere = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "OtherRepayableWithholdingTaxes"));

            tx.VerrekenbareVoorheffing.VBelastingkredietHuiidTijdperk = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "TaxCreditResearchDevelopmentClearablePreviousTaxPeriodsRepayableCurrentAssessmentYear"));



            tx.Aanslag.KapitaalEnInterestsubsidies = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "CapitalSubsidiesInterestSubsidiesAgriculturalSupportRate500"));
            tx.Aanslag.NietVerantwoordeKosten = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "UnjustifiedExpensesHiddenSurplusProfitsFinancialAdvantagesBenefitsAllKind"));
            tx.Aanslag.AfzonderlijkeAanslag34 = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "SeparateAssessmentTaxedReservesCreditAssociationsInstitutionsOtherAuthorisedInstitutionsRate3400"));
            tx.Aanslag.AfzonderlijkeAanslag28 = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "SeparateAssessmentTaxedReservesCreditAssociationsInstitutionsOtherAuthorisedInstitutionsRate2800"));
            tx.Aanslag.AfzonderlijkeAanslagDividenden = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "SeparateAssessmentDividendsPaidCreditCorporationTradeEquipmentHousingCorporation"));
            tx.Aanslag.AfzonderlijkeAanslagRV = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "SeparateAssessmentTransitionalSystemReducedWithholdingTaxPaidTaxedReserves"));


            tx.Aanslag.VerdelingVermogen33 = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "SpecialAssessmentPre1990FullPartialDistributionCompanyAssetsRate3300"));
            tx.Aanslag.VerdelingVermogen16 = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "SpecialAssessmentPre1990FullPartialDistributionCompanyAssetsRate1650"));
            tx.Aanslag.VAAVennootschappen = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "SpecialAssessmentPre1990BenefitsAllKindCompanyLiquidation"));

            tx.Aanslag.AanvullendeHeffingDiamant = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "AdditionalDutiesDiamondTraders"));
            tx.Aanslag.TerugbetalingBelastingskrediet = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "RetributionTaxCreditResearchDevelopment"));

            tx.Aanslag.Taxable2015 = !this.Bool(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "NotLiableCorporateIncomeTaxNextTaxPeriodDueToMergerDivisionSimilarTransactions"));
            tx.Aanslag.TotaalBedragVoorzieningen = this.Number(this.Xbrl.Elements.Where(x => x.Name == "SeparateAssessmentAdditionalIndividualPensionProvisionsOnEndTaxPeriodClosingDateBefore2012-01-01"), "0");


            tx.FairnessTax.FairnessTaxBase = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "FairnessTax"));
            tx.FairnessTax.FairnessTaxPerc = (decimal)5.15;


            tx.BerekeningOverzicht.Verminderd = Bool(Xbrl.Elements.FirstOrDefault(x => x.Name == "ExclusionReducedRate"));

            tx.BerekeningOverzicht.CommonRateCapitalGains25 = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "CapitalGainsSharesRate2500"));



            tx.BerekeningOverzicht.CapitalGainsShares0412 = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "CapitalGainsSharesRate0040"));
            tx.BerekeningOverzicht.CapitalGainsShares25 = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "CapitalGainsSharesRate2500"));
            tx.BerekeningOverzicht.ExitTarif = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "BasicTaxableAmountExitTaxRate"));

            //tx.BelastingsBerekening.EenduidigTariefBedrag = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "BasicTaxableAmountCommonRate"));

            /* VERMINDERD / EENDUIDIG */


            /* 
             tx.BelastingsBerekening.NonRefundableWithholdingTaxes_TaxCredit = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "NonRepayableAdvanceLevies"));
             tx.BelastingsBerekening.RefundableTaxeCredit = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "TaxCreditResearchDevelopmentClearablePreviousTaxPeriodsRepayableCurrentAssessmentYear"));
             tx.BelastingsBerekening.RefundableWithholdingTaxes = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "RepayableAdvanceLevies"));
             */
            //FairnessTaxForIncrease
            // move


            tx.BerekeningOverzicht.AP1Base = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "PrepaymentFirstQuarter"));
            tx.BerekeningOverzicht.AP2Base = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "PrepaymentSecondQuarter"));
            tx.BerekeningOverzicht.AP3Base = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "PrepaymentThirdQuarter"));
            tx.BerekeningOverzicht.AP4Base = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "PrepaymentFourthQuarter"));









        }

        private void _CalculateTax(ref TaxCalc.TaxCalc Data)
        {
            bool jongVen = this.Bool(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "FirstThreeAccountingYearsSmallCompanyCorporationCode"));



            if (Data.BerekeningOverzicht == null)
            {
                Data.BerekeningOverzicht = new TaxCalc.BerekeningOverzichtItem { Id = Guid.NewGuid() };
            }



            // Data.BelastingsBerekening.JongVennootschap =
            int assessment = 2014;

            /* VerrekenbareVoorheffing
             */

            Data.VerrekenbareVoorheffing.VVNTBelastingKredietOnderzoekOntw = Data.VerrekenbareVoorheffing.VVNTBelastingKredietOnderzoekOntw.GetValueOrDefault();
            Data.VerrekenbareVoorheffing.VVNTFictieveRoerendeVoorheffing = Data.VerrekenbareVoorheffing.VVNTFictieveRoerendeVoorheffing.GetValueOrDefault();
            Data.VerrekenbareVoorheffing.VVNTForfaitairBuitenlands = Data.VerrekenbareVoorheffing.VVNTForfaitairBuitenlands.GetValueOrDefault();
            Data.VerrekenbareVoorheffing.VVNTBelastingKredietOnderzoekOntw = Data.VerrekenbareVoorheffing.VVNTBelastingKredietOnderzoekOntw.GetValueOrDefault();


            Data.VerrekenbareVoorheffing.VVNTTotaal = (Data.VerrekenbareVoorheffing.VVNTBelastingKredietOnderzoekOntw.GetValueOrDefault() + Data.VerrekenbareVoorheffing.VVNTFictieveRoerendeVoorheffing.GetValueOrDefault()) + Data.VerrekenbareVoorheffing.VVNTForfaitairBuitenlands.GetValueOrDefault();
            Data.VerrekenbareVoorheffing.VVTAndere = Math.Abs(Data.VerrekenbareVoorheffing.VVTAndere.GetValueOrDefault());
            Data.VerrekenbareVoorheffing.VVTAndereBuitenlands = Math.Abs(Data.VerrekenbareVoorheffing.VVTAndereBuitenlands.GetValueOrDefault());
            Data.VerrekenbareVoorheffing.VVTAndereEigen = Math.Abs(Data.VerrekenbareVoorheffing.VVTAndereEigen.GetValueOrDefault());
            Data.VerrekenbareVoorheffing.VVTAndereRoerendeVoorheffing = Math.Abs((Data.VerrekenbareVoorheffing.VVTAndereRoerendeVoorheffing.GetValueOrDefault()));
            Data.VerrekenbareVoorheffing.VVTBelgisch = Math.Abs((Data.VerrekenbareVoorheffing.VVTBelgisch.GetValueOrDefault()));
            Data.VerrekenbareVoorheffing.VVTEigenBuitenlands = Math.Abs((Data.VerrekenbareVoorheffing.VVTEigenBuitenlands.GetValueOrDefault()));
            Data.VerrekenbareVoorheffing.VBelastingkredietHuiidTijdperk = Data.VerrekenbareVoorheffing.VBelastingkredietHuiidTijdperk.GetValueOrDefault();
            Data.VerrekenbareVoorheffing.VVTTotaal = ((((Data.VerrekenbareVoorheffing.VVTAndere.GetValueOrDefault() + Data.VerrekenbareVoorheffing.VVTBelgisch.GetValueOrDefault()) + Data.VerrekenbareVoorheffing.VVTAndereBuitenlands.GetValueOrDefault()) + Data.VerrekenbareVoorheffing.VVTAndereEigen.GetValueOrDefault()) + Data.VerrekenbareVoorheffing.VVTAndereRoerendeVoorheffing.GetValueOrDefault()) + Data.VerrekenbareVoorheffing.VVTEigenBuitenlands.GetValueOrDefault();

            Data.BerekeningOverzicht.NonRepayableAdvanceLevies = Data.VerrekenbareVoorheffing.VVNTTotaal.GetValueOrDefault();
            Data.BerekeningOverzicht.RepayableAdvanceLevies = Data.VerrekenbareVoorheffing.VVTTotaal.GetValueOrDefault();
            Data.BerekeningOverzicht.RepayableResearchDevelopment = Data.VerrekenbareVoorheffing.VBelastingkredietHuiidTijdperk.GetValueOrDefault();



            #region Calc IPT

            Data.Aanslag.IPT2013 = 0;
            Data.Aanslag.IPT2014 = 0;
            Data.Aanslag.IPT2015 = 0;
            Data.BerekeningOverzicht.IPTBase = 0;
            Data.BerekeningOverzicht.IPTTotal = 0;
            Data.BerekeningOverzicht.IPTPerc = (decimal)0.6;
            if (Data.Aanslag.TotaalBedragVoorzieningen.GetValueOrDefault() > 0)
            {

                Data.BerekeningOverzicht.IPTBase = Data.Aanslag.TotaalBedragVoorzieningen.GetValueOrDefault();
                Data.Aanslag.IPT2013 = Math.Round(Data.Aanslag.TotaalBedragVoorzieningen.GetValueOrDefault() * (decimal)0.006, 2, MidpointRounding.AwayFromZero);
                Data.Aanslag.IPT2014 = Math.Round(Data.Aanslag.TotaalBedragVoorzieningen.GetValueOrDefault() * (decimal)0.006, 2, MidpointRounding.AwayFromZero);
                Data.Aanslag.IPT2015 = Math.Round(Data.Aanslag.TotaalBedragVoorzieningen.GetValueOrDefault() * (decimal)0.006, 2, MidpointRounding.AwayFromZero);

                if (!Data.Aanslag.Taxable2015.GetValueOrDefault())
                {
                    Data.Aanslag.IPT2014 += Data.Aanslag.IPT2015.GetValueOrDefault();
                    Data.Aanslag.IPT2015 = 0;
                    Data.BerekeningOverzicht.IPTPerc = (decimal)1.2;
                }
                Data.BerekeningOverzicht.IPTTotal = Data.Aanslag.IPT2014.GetValueOrDefault();

            }
            #endregion





            Data.BerekeningOverzicht.AssessmentYear = assessment;


            Data.BerekeningOverzicht.TotalTaxableReserves = Data.BelasteReserves.TDTotal.GetValueOrDefault();
            Data.BerekeningOverzicht.TotalTaxableReservesPlus = Data.BelasteReserves.OACapitalGainsOnShares.GetValueOrDefault()
                + Data.BelasteReserves.OACapitalGainsSharesReversal.GetValueOrDefault() + Data.BelasteReserves.OADefinitiveExemptionTaxShelter.GetValueOrDefault()
                + Data.BelasteReserves.OAExemptionRegionalPrem.GetValueOrDefault() + Data.BelasteReserves.OAFinalExemptionProfit.GetValueOrDefault()
                + Data.BelasteReserves.OAOthers.GetValueOrDefault();
            Data.BerekeningOverzicht.TotalTaxableReservesMinus = Data.BelasteReserves.OAInTheMin.GetValueOrDefault();

            Data.BerekeningOverzicht.TotalNonDeductibleTaxes = Data.VerworpenUitgaven.PDDisallowedCurrentIncomeTaxExpenses.GetValueOrDefault();
            Data.BerekeningOverzicht.TotalDissalowedExpensesWithoutNonDeductibleTax = Data.VerworpenUitgaven.DETotal.GetValueOrDefault();

            Data.BerekeningOverzicht.TaxableResult = Data.BerekeningOverzicht.StatAccountingResult.GetValueOrDefault()
                + Data.BerekeningOverzicht.TotalTaxableReserves.GetValueOrDefault()
                - Data.BerekeningOverzicht.TotalTaxableReservesPlus.GetValueOrDefault()
                + Data.BerekeningOverzicht.TotalTaxableReservesMinus.GetValueOrDefault()
                + Data.BerekeningOverzicht.TotalNonDeductibleTaxes.GetValueOrDefault()
                + Data.BerekeningOverzicht.TotalDissalowedExpensesWithoutNonDeductibleTax.GetValueOrDefault();

            if (Data.BerekeningOverzicht.TaxableResult.GetValueOrDefault() < 0)
                Data.BerekeningOverzicht.TaxableResult = 0;

            Data.BerekeningOverzicht.TotalDeductionLimit = Data.BelastbareBestZonderAftrek.TETotal.GetValueOrDefault();

            Data.BerekeningOverzicht.RemainingTaxableResult = Data.BerekeningOverzicht.TaxableResult.GetValueOrDefault() - Data.BerekeningOverzicht.TotalDeductionLimit.GetValueOrDefault();
            if (Data.BerekeningOverzicht.RemainingTaxableResult.GetValueOrDefault() < 0)
                Data.BerekeningOverzicht.RemainingTaxableResult = 0;



            Data.BerekeningOverzicht.TotalTaxTreaty = Data.OpdelingNaarOorsprong.IRTreatyForeignIncome.GetValueOrDefault();

            #region Fiscale aftrek herrekening

            decimal resterend = Data.BerekeningOverzicht.RemainingTaxableResult.GetValueOrDefault() - Data.BerekeningOverzicht.TotalTaxTreaty.GetValueOrDefault();

            Data.FiscaleAftrek.TotalNonTaxableElements = Data.FiscaleAftrek.TotalNonTaxableElements.HasValue ? Data.FiscaleAftrek.TotalNonTaxableElements.Value : 0;
            if (resterend < 0) resterend = 0;
            if (Data.FiscaleAftrek.TotalNonTaxableElements.Value > resterend)
            {
            }
            else
            {
                resterend -= Data.FiscaleAftrek.TotalNonTaxableElements.Value;
                Data.FiscaleAftrek.PEBelgium = Data.FiscaleAftrek.PEBelgium.HasValue ? Data.FiscaleAftrek.PEBelgium.Value : 0;

                if ((resterend - Data.FiscaleAftrek.PEBelgium.Value) < 0)
                {
                }
                else
                {

                    resterend -= Data.FiscaleAftrek.PEBelgium.Value;
                    Data.FiscaleAftrek.PIDBelgium = Data.FiscaleAftrek.PIDBelgium.HasValue ? Data.FiscaleAftrek.PIDBelgium.Value : 0;
                    if ((resterend - Data.FiscaleAftrek.PIDBelgium.Value) < 0)
                    {
                    }
                    else
                    {
                        resterend -= Data.FiscaleAftrek.PIDBelgium.Value;
                        Data.FiscaleAftrek.NIDBelgium = Data.FiscaleAftrek.NIDBelgium.HasValue ? Data.FiscaleAftrek.NIDBelgium.Value : 0;
                        if ((resterend - Data.FiscaleAftrek.NIDBelgium.Value) < 0)
                        {
                        }
                        else
                        {
                            resterend -= Data.FiscaleAftrek.NIDBelgium.Value;
                            Data.FiscaleAftrek.DTLBelgium = Data.FiscaleAftrek.DTLBelgium.HasValue ? Data.FiscaleAftrek.DTLBelgium.Value : 0;
                            if ((resterend - Data.FiscaleAftrek.DTLBelgium.Value) < 0)
                            {
                            }
                            else
                            {
                                resterend -= Data.FiscaleAftrek.DTLBelgium.Value;
                                Data.FiscaleAftrek.IDBelgium = Data.FiscaleAftrek.IDBelgium.HasValue ? Data.FiscaleAftrek.IDBelgium.Value : 0;
                                if ((resterend - Data.FiscaleAftrek.IDBelgium.Value) < 0)
                                {
                                }
                                else
                                {
                                    resterend -= Data.FiscaleAftrek.IDBelgium.Value;
                                }
                            }
                        }
                    }
                }

            }
            Data.FiscaleAftrek.TotalFiscaleAftrek = ((((Data.FiscaleAftrek.TotalNonTaxableElements + Data.FiscaleAftrek.PEBelgium) + Data.FiscaleAftrek.PIDBelgium) + Data.FiscaleAftrek.NIDBelgium) + Data.FiscaleAftrek.DTLBelgium) + Data.FiscaleAftrek.IDBelgium + Data.FiscaleAftrek.NIDHistoryBelgium.GetValueOrDefault();



            #endregion


            Data.BerekeningOverzicht.TotalFiscalDeductions = Data.FiscaleAftrek.TotalFiscaleAftrek.GetValueOrDefault();
            /* #166
            Data.BerekeningOverzicht.RemainingFiscalProfit = Data.BerekeningOverzicht.RemainingTaxableResult.GetValueOrDefault()
                    - Data.BerekeningOverzicht.TotalTaxTreaty.GetValueOrDefault()
                    - Data.BerekeningOverzicht.TotalFiscalDeductions.GetValueOrDefault();
             */
            if (Data.BerekeningOverzicht.TotalTaxTreaty.GetValueOrDefault() > 0)
            {
                Data.BerekeningOverzicht.RemainingFiscalProfit = Data.BerekeningOverzicht.RemainingTaxableResult.GetValueOrDefault()
                    - Data.BerekeningOverzicht.TotalTaxTreaty.GetValueOrDefault()
                    - Data.BerekeningOverzicht.TotalFiscalDeductions.GetValueOrDefault();
            }
            else
            {
                Data.BerekeningOverzicht.RemainingFiscalProfit = Data.BerekeningOverzicht.RemainingTaxableResult.GetValueOrDefault()
                    - Data.BerekeningOverzicht.TotalFiscalDeductions.GetValueOrDefault();
            }

            if (Data.BerekeningOverzicht.RemainingFiscalProfit.GetValueOrDefault() < 0)
                Data.BerekeningOverzicht.RemainingFiscalProfit = 0;

            Data.BerekeningOverzicht.CommonRateRemainingFiscalProfit = Data.BerekeningOverzicht.RemainingFiscalProfit;
            Data.BerekeningOverzicht.CommonRateDeductionLimit = Data.BerekeningOverzicht.TotalDeductionLimit.GetValueOrDefault()
                - Data.BelastbareBestZonderAftrek.TECapitalGains0412.GetValueOrDefault()
                - Data.BelastbareBestZonderAftrek.TECapitalAgriCulture.GetValueOrDefault();



            decimal? vaaDeelAuto = Data.BelastbareBestZonderAftrek.TEVaaCar.GetValueOrDefault();


            Data.BerekeningOverzicht.CapitalGainsShares25 = Data.BerekeningOverzicht.CommonRateCapitalGains25.GetValueOrDefault();

            Data.BerekeningOverzicht.CapitalGainsShares25Result = Math.Round((Data.BerekeningOverzicht.CapitalGainsShares25.GetValueOrDefault() / 100) * (Data.BerekeningOverzicht.CapitalGainsShares25Perc.GetValueOrDefault()), 2, MidpointRounding.AwayFromZero);



            Data.BerekeningOverzicht.CommonRateResult = Data.BerekeningOverzicht.CommonRateRemainingFiscalProfit.GetValueOrDefault()
                + Data.BerekeningOverzicht.CommonRateDeductionLimit.GetValueOrDefault()
                - Data.BerekeningOverzicht.CommonRateCapitalGains25.GetValueOrDefault();

            if (Data.BerekeningOverzicht.CommonRateResult.GetValueOrDefault() < 0)
                Data.BerekeningOverzicht.CommonRateResult = 0;

            #region Fairness Tax calc





            Data.FairnessTax.FairnessTaxResult = Math.Round(Data.FairnessTax.FairnessTaxBase.GetValueOrDefault() * (Data.FairnessTax.FairnessTaxPerc.GetValueOrDefault() / 100), 2, MidpointRounding.AwayFromZero);


            Data.BerekeningOverzicht.FairnessTaxBase = Data.FairnessTax.FairnessTaxBase.GetValueOrDefault();
            Data.BerekeningOverzicht.FairnessTaxPerc = Data.FairnessTax.FairnessTaxPerc.GetValueOrDefault();
            Data.BerekeningOverzicht.FairnessTaxTotal = Data.FairnessTax.FairnessTaxResult.GetValueOrDefault();
            Data.BerekeningOverzicht.BaseForIncreaseFairnessTax = Data.FairnessTax.FairnessTaxResult.GetValueOrDefault();

            #endregion


            Data.BerekeningOverzicht.ExitTarifResult = Math.Round(Data.BerekeningOverzicht.ExitTarif.GetValueOrDefault() * (Data.BerekeningOverzicht.ExitTarifPerc.GetValueOrDefault() / 100), 2, MidpointRounding.AwayFromZero);
            Data.BerekeningOverzicht.CapitalGainsShares0412 = Data.BelastbareBestZonderAftrek.TECapitalGains0412.GetValueOrDefault();
            Data.BerekeningOverzicht.CapitalGainsShares0412Result = Math.Round((Data.BerekeningOverzicht.CapitalGainsShares0412.GetValueOrDefault() / 100) * Data.BerekeningOverzicht.CapitalGainsShares0412Perc.GetValueOrDefault(), 2, MidpointRounding.AwayFromZero);

            decimal verminderdBovenGrens = Data.BerekeningOverzicht.VerminderdtariefStap3Base.GetValueOrDefault();


            Data.BerekeningOverzicht.VerminderdtariefStap1 = 0;

            Data.BerekeningOverzicht.VerminderdtariefStap2 = 0;

            Data.BerekeningOverzicht.VerminderdtariefStap3 = 0;
            Data.BerekeningOverzicht.EenduidigTariefBedrag = 0;

            // Added fbo 25082014
            Data.BerekeningOverzicht.EenduidigTariefTot = 0;

            if (Data.BerekeningOverzicht.CommonRateResult.GetValueOrDefault() > 0)
            {


                if (
                    Data.BerekeningOverzicht.CreditCorporationTradeEquipmentHousingCorporationTaxRate.GetValueOrDefault() ||
                    (!Data.BerekeningOverzicht.Verminderd.GetValueOrDefault() || (((Data.Tarief.BezitAandelenMeerDan50Percent == true || Data.Tarief.BezitAandelenDoorVennootschappen == true) || Data.Tarief.UitgekeerdDividendHogerDan13 == true) || Data.Tarief.ToegekendeBezoldiging == true) || (Data.BerekeningOverzicht.CommonRateResult.GetValueOrDefault() > verminderdBovenGrens)))
                {
                    Data.BerekeningOverzicht.VerminderdtariefStap1Base = 0;
                    Data.BerekeningOverzicht.VerminderdtariefStap1 = 0;
                    Data.BerekeningOverzicht.VerminderdtariefStap2Base = 0;
                    Data.BerekeningOverzicht.VerminderdtariefStap2 = 0;
                    Data.BerekeningOverzicht.VerminderdtariefStap3Base = 0;
                    Data.BerekeningOverzicht.VerminderdtariefStap3 = 0;

                    Data.BerekeningOverzicht.EenduidigTariefBedrag = Data.BerekeningOverzicht.CommonRateResult.GetValueOrDefault();

                    Data.BerekeningOverzicht.EenduidigTariefTot = Math.Round(Data.BerekeningOverzicht.EenduidigTariefBedrag.Value * (Data.BerekeningOverzicht.EenduidigTariefPerc.Value / 100), 2, MidpointRounding.AwayFromZero);
                    Data.BerekeningOverzicht.Verminderd = false;
                }
                else
                {
                    Data.BerekeningOverzicht.Verminderd = true;
                    Data.BerekeningOverzicht.EenduidigTariefBedrag = 0;
                    Data.BerekeningOverzicht.EenduidigTariefTot = 0;
                    decimal remainingTaxNormalTarif = Data.BerekeningOverzicht.CommonRateResult.GetValueOrDefault();

                    if (remainingTaxNormalTarif < Data.BerekeningOverzicht.VerminderdtariefStap1Base)
                    {
                        Data.BerekeningOverzicht.VerminderdtariefStap1Base = remainingTaxNormalTarif;
                        Data.BerekeningOverzicht.VerminderdtariefStap1 = Math.Round(Data.BerekeningOverzicht.VerminderdtariefStap1Base.Value * (Data.BerekeningOverzicht.VerminderdtariefStap1Perc.Value / 100), 2, MidpointRounding.AwayFromZero);
                        Data.BerekeningOverzicht.VerminderdtariefStap2 = 0;
                        Data.BerekeningOverzicht.VerminderdtariefStap3 = 0;
                        Data.BerekeningOverzicht.VerminderdtariefStap2Base = 0;
                        Data.BerekeningOverzicht.VerminderdtariefStap3Base = 0;
                    }
                    else
                    {
                        Data.BerekeningOverzicht.VerminderdtariefStap1 = Math.Round(Data.BerekeningOverzicht.VerminderdtariefStap1Base.Value * (Data.BerekeningOverzicht.VerminderdtariefStap1Perc.Value / 100), 2, MidpointRounding.AwayFromZero);
                        if (remainingTaxNormalTarif < Data.BerekeningOverzicht.VerminderdtariefStap2Base)
                        {
                            Data.BerekeningOverzicht.VerminderdtariefStap2Base = remainingTaxNormalTarif - Data.BerekeningOverzicht.VerminderdtariefStap1Base;
                            Data.BerekeningOverzicht.VerminderdtariefStap2 = Math.Round(Data.BerekeningOverzicht.VerminderdtariefStap2Base.Value * (Data.BerekeningOverzicht.VerminderdtariefStap2Perc.Value / 100), 2, MidpointRounding.AwayFromZero);

                            Data.BerekeningOverzicht.VerminderdtariefStap3 = 0;
                            Data.BerekeningOverzicht.VerminderdtariefStap3Base = 0;
                        }
                        else
                        {
                            Data.BerekeningOverzicht.VerminderdtariefStap2Base = Data.BerekeningOverzicht.VerminderdtariefStap2Base - Data.BerekeningOverzicht.VerminderdtariefStap1Base;
                            Data.BerekeningOverzicht.VerminderdtariefStap2 = Math.Round(Data.BerekeningOverzicht.VerminderdtariefStap2Base.Value * (Data.BerekeningOverzicht.VerminderdtariefStap2Perc.Value / 100), 2, MidpointRounding.AwayFromZero);
                            if (remainingTaxNormalTarif < Data.BerekeningOverzicht.VerminderdtariefStap3Base)
                            {
                                Data.BerekeningOverzicht.VerminderdtariefStap3Base = (remainingTaxNormalTarif - Data.BerekeningOverzicht.VerminderdtariefStap2Base) - Data.BerekeningOverzicht.VerminderdtariefStap1Base;
                                Data.BerekeningOverzicht.VerminderdtariefStap3 = Math.Round(Data.BerekeningOverzicht.VerminderdtariefStap3Base.Value * (Data.BerekeningOverzicht.VerminderdtariefStap3Perc.Value / 100), 2, MidpointRounding.AwayFromZero);

                            }
                            else
                            {
                                Data.BerekeningOverzicht.EenduidigTariefBedrag = remainingTaxNormalTarif;
                                Data.BerekeningOverzicht.EenduidigTariefTot = Math.Round(Data.BerekeningOverzicht.EenduidigTariefBedrag.Value * (Data.BerekeningOverzicht.EenduidigTariefPerc.Value / 100), 2, MidpointRounding.AwayFromZero);

                            }
                        }
                    }
                }
            }

            Data.BerekeningOverzicht.CorporateTaxBeforeRepayable = Data.BerekeningOverzicht.EenduidigTariefTot.GetValueOrDefault()
                + Data.BerekeningOverzicht.VerminderdtariefStap1.GetValueOrDefault() + Data.BerekeningOverzicht.VerminderdtariefStap2.GetValueOrDefault()
                + Data.BerekeningOverzicht.VerminderdtariefStap3.GetValueOrDefault() + Data.BerekeningOverzicht.ExitTarifResult.GetValueOrDefault()
                + Data.BerekeningOverzicht.CapitalGainsShares25Result.GetValueOrDefault() + Data.BerekeningOverzicht.CapitalGainsShares0412Result.GetValueOrDefault();


            Data.BerekeningOverzicht.RepayableTotal = 0;
            //If the corporate tax is higher than the positive nonrepayableAdvanceLevies 
            if (Data.BerekeningOverzicht.CorporateTaxBeforeRepayable.GetValueOrDefault() >= Math.Abs(Data.BerekeningOverzicht.NonRepayableAdvanceLevies.GetValueOrDefault()))
            {
                //add the non repayable advance levies as being the lower value as you cannot ever return more from the goverment than the set tax. The rest is lost.
                Data.BerekeningOverzicht.RepayableTotal += Math.Abs(Data.BerekeningOverzicht.NonRepayableAdvanceLevies.GetValueOrDefault());
            }
            else
            {
                //add the corporate tax as being the lower value as you cannot ever return more from the goverment than the set tax. The rest is lost.
                Data.BerekeningOverzicht.RepayableTotal += Data.BerekeningOverzicht.CorporateTaxBeforeRepayable.GetValueOrDefault();
            }

            //The following are always repayable and therefore are always added
            Data.BerekeningOverzicht.RepayableTotal += Data.BerekeningOverzicht.RepayableAdvanceLevies.GetValueOrDefault();
            Data.BerekeningOverzicht.RepayableTotal += Data.BerekeningOverzicht.RepayableResearchDevelopment.GetValueOrDefault();

            /*
            Data.BerekeningOverzicht.RepayableTotal = Data.BerekeningOverzicht.NonRepayableAdvanceLevies.GetValueOrDefault() + Data.BerekeningOverzicht.RepayableAdvanceLevies.GetValueOrDefault()
                + Data.BerekeningOverzicht.RepayableResearchDevelopment.GetValueOrDefault();
            if (Data.BerekeningOverzicht.CorporateTaxBeforeRepayable.GetValueOrDefault() - Data.BerekeningOverzicht.NonRepayableAdvanceLevies.GetValueOrDefault() > 0)
            {
                Data.BerekeningOverzicht.RepayableTotal = Data.BerekeningOverzicht.NonRepayableAdvanceLevies.GetValueOrDefault() + Data.BerekeningOverzicht.RepayableAdvanceLevies.GetValueOrDefault()
                + Data.BerekeningOverzicht.RepayableResearchDevelopment.GetValueOrDefault();
            }
            else
            {
                Data.BerekeningOverzicht.RepayableTotal = Data.BerekeningOverzicht.RepayableAdvanceLevies.GetValueOrDefault() + Data.BerekeningOverzicht.RepayableResearchDevelopment.GetValueOrDefault();
            }
            */

            Data.BerekeningOverzicht.CorporateTaxAfterRepayable = Data.BerekeningOverzicht.CorporateTaxBeforeRepayable.GetValueOrDefault()
                - Data.BerekeningOverzicht.RepayableTotal.GetValueOrDefault();

            Data.BerekeningOverzicht.RetributionResearchDevelopmentBase = 0;
            Data.BerekeningOverzicht.RetributionResearchDevelopmentTotal = 0;
            if (Data.Aanslag.TerugbetalingBelastingskrediet.GetValueOrDefault() > 0)
            {
                Data.BerekeningOverzicht.RetributionResearchDevelopmentBase = Data.Aanslag.TerugbetalingBelastingskrediet.GetValueOrDefault();
                Data.BerekeningOverzicht.RetributionResearchDevelopmentTotal = Data.BerekeningOverzicht.RetributionResearchDevelopmentBase.GetValueOrDefault()
                    * (Data.BerekeningOverzicht.RetributionResearchDevelopmentPerc.GetValueOrDefault() / 100);
            }

            Data.BerekeningOverzicht.BaseForIncrease = Data.BerekeningOverzicht.CorporateTaxAfterRepayable.GetValueOrDefault()
                + Data.BerekeningOverzicht.RetributionResearchDevelopmentTotal.GetValueOrDefault()
                + Data.BerekeningOverzicht.BaseForIncreaseFairnessTax.GetValueOrDefault();

            if (!Data.BerekeningOverzicht.BaseForIncrease.HasValue) Data.BerekeningOverzicht.BaseForIncrease = 0;

            if (Data.BerekeningOverzicht.BaseForIncrease < 0)
            {
                Data.BerekeningOverzicht.BaseForIncrease = 0;
            }


            Data.BerekeningOverzicht.BaseForIncreaseTotal = Math.Round(Data.BerekeningOverzicht.BaseForIncrease.GetValueOrDefault() * (Data.BerekeningOverzicht.BaseForIncreasePerc.GetValueOrDefault() / 100), 2, MidpointRounding.AwayFromZero);


            Data.BerekeningOverzicht.AP1Total = Math.Round(Data.BerekeningOverzicht.AP1Base.Value * (Data.BerekeningOverzicht.AP1Percentage.Value / 100), 2, MidpointRounding.AwayFromZero);
            Data.BerekeningOverzicht.AP2Total = Math.Round(Data.BerekeningOverzicht.AP2Base.Value * (Data.BerekeningOverzicht.AP2Percentage.Value / 100), 2, MidpointRounding.AwayFromZero);
            Data.BerekeningOverzicht.AP3Total = Math.Round(Data.BerekeningOverzicht.AP3Base.Value * (Data.BerekeningOverzicht.AP3Percentage.Value / 100), 2, MidpointRounding.AwayFromZero);
            Data.BerekeningOverzicht.AP4Total = Math.Round(Data.BerekeningOverzicht.AP4Base.Value * (Data.BerekeningOverzicht.AP4Percentage.Value / 100), 2, MidpointRounding.AwayFromZero);

            Data.BerekeningOverzicht.APBaseTotal = ((Data.BerekeningOverzicht.AP1Base.GetValueOrDefault() + Data.BerekeningOverzicht.AP2Base.GetValueOrDefault()) + Data.BerekeningOverzicht.AP3Base.GetValueOrDefault()) + Data.BerekeningOverzicht.AP4Base.GetValueOrDefault();

            Data.BerekeningOverzicht.APTotal = ((Data.BerekeningOverzicht.AP1Total.GetValueOrDefault() + Data.BerekeningOverzicht.AP2Total.GetValueOrDefault()) + Data.BerekeningOverzicht.AP3Total.GetValueOrDefault()) + Data.BerekeningOverzicht.AP4Total.GetValueOrDefault();

            Data.BerekeningOverzicht.AdvancePayments = Data.BerekeningOverzicht.APBaseTotal;

            Data.BerekeningOverzicht.Test1Percent = Data.BerekeningOverzicht.BaseForIncrease.GetValueOrDefault() * (decimal)0.01;

            if (((Data.BerekeningOverzicht.BaseForIncreaseTotal.GetValueOrDefault() - Data.BerekeningOverzicht.APTotal.GetValueOrDefault()) < Data.BerekeningOverzicht.Test1Percent.GetValueOrDefault())
                || ((Data.BerekeningOverzicht.BaseForIncreaseTotal.GetValueOrDefault() - Data.BerekeningOverzicht.APTotal.GetValueOrDefault()) <= 40))
            {
                Data.BerekeningOverzicht.BaseForIncreaseApplied = 0;
            }
            else
            {
                Data.BerekeningOverzicht.BaseForIncreaseApplied = Data.BerekeningOverzicht.BaseForIncreaseTotal.GetValueOrDefault() - Data.BerekeningOverzicht.APTotal.GetValueOrDefault();
            }

            if (jongVen)
                Data.BerekeningOverzicht.BaseForIncreaseApplied = 0;

            Data.BerekeningOverzicht.TaxResult = Data.BerekeningOverzicht.CorporateTaxBeforeRepayable.GetValueOrDefault()
                + Data.BerekeningOverzicht.BaseForIncreaseApplied.GetValueOrDefault()
                - Data.BerekeningOverzicht.AdvancePayments.GetValueOrDefault();

            if (Data.BerekeningOverzicht.CorporateTaxBeforeRepayable.GetValueOrDefault() - Data.BerekeningOverzicht.NonRepayableAdvanceLevies.GetValueOrDefault() > 0)
            {
                Data.BerekeningOverzicht.TaxResult -= Data.BerekeningOverzicht.NonRepayableAdvanceLevies.GetValueOrDefault();
            }
            
            if (Data.BerekeningOverzicht.NonRepayableAdvanceLevies.GetValueOrDefault() > 0 && Data.BerekeningOverzicht.TaxResult.GetValueOrDefault() >0)
            {
                Data.BerekeningOverzicht.TaxResult -= Data.BerekeningOverzicht.NonRepayableAdvanceLevies.GetValueOrDefault();
                if (Data.BerekeningOverzicht.TaxResult.GetValueOrDefault() < 0) Data.BerekeningOverzicht.TaxResult = 0;
            }
             

            Data.BerekeningOverzicht.TaxResult = Data.BerekeningOverzicht.TaxResult.GetValueOrDefault() - Data.BerekeningOverzicht.RepayableAdvanceLevies.GetValueOrDefault() - Data.BerekeningOverzicht.RepayableResearchDevelopment.GetValueOrDefault();



            Data.BerekeningOverzicht.KapitaalEnInterestsubsidiesResult = 0;
            Data.BerekeningOverzicht.KapitaalEnInterestsubsidies = 0;
            if (Data.Aanslag.KapitaalEnInterestsubsidies.GetValueOrDefault() > 0)
            {
                Data.BerekeningOverzicht.KapitaalEnInterestsubsidies = Data.Aanslag.KapitaalEnInterestsubsidies.GetValueOrDefault();
                Data.BerekeningOverzicht.KapitaalEnInterestsubsidiesResult = Data.BerekeningOverzicht.KapitaalEnInterestsubsidies.GetValueOrDefault() * (Data.BerekeningOverzicht.KapitaalEnInterestsubsidiesPerc.GetValueOrDefault() / 100);

            }

            Data.BerekeningOverzicht.TaxResult += Data.BerekeningOverzicht.KapitaalEnInterestsubsidiesResult.GetValueOrDefault();


            // afzonderlijke aanslagen
            //Niet-verantwoorde kosten of voordelen van alle aard, verdoken meerwinsten en financiële voordelen of voordelen van alle aard
            Data.BerekeningOverzicht.UnjustifiedExpensesHiddenSurplusBase = 0;
            Data.BerekeningOverzicht.UnjustifiedExpensesHiddenSurplusTotal = 0;
            if (Data.Aanslag.NietVerantwoordeKosten.GetValueOrDefault() > 0)
            {
                Data.BerekeningOverzicht.UnjustifiedExpensesHiddenSurplusBase = Data.Aanslag.NietVerantwoordeKosten.GetValueOrDefault();
                Data.BerekeningOverzicht.UnjustifiedExpensesHiddenSurplusTotal = Data.BerekeningOverzicht.UnjustifiedExpensesHiddenSurplusBase.GetValueOrDefault()
                    * (Data.BerekeningOverzicht.UnjustifiedExpensesHiddenSurplusPerc.GetValueOrDefault() / 100);

            }
            Data.BerekeningOverzicht.TaxResult += Data.BerekeningOverzicht.UnjustifiedExpensesHiddenSurplusTotal.GetValueOrDefault();

            //Afzonderlijke aanslag van de belaste reserves ten name van erkende kredietinstellingen, tegen het tarief van 34 %
            Data.BerekeningOverzicht.SeparateAssessmentTaxedReserves3400Base = 0;
            Data.BerekeningOverzicht.SeparateAssessmentTaxedReserves3400Total = 0;
            if (Data.Aanslag.AfzonderlijkeAanslag34.GetValueOrDefault() > 0)
            {
                Data.BerekeningOverzicht.SeparateAssessmentTaxedReserves3400Base = Data.Aanslag.AfzonderlijkeAanslag34.GetValueOrDefault();
                Data.BerekeningOverzicht.SeparateAssessmentTaxedReserves3400Total = Data.BerekeningOverzicht.SeparateAssessmentTaxedReserves3400Base.GetValueOrDefault()
                    * (Data.BerekeningOverzicht.SeparateAssessmentTaxedReserves3400Perc.GetValueOrDefault() / 100);

            }
            Data.BerekeningOverzicht.TaxResult += Data.BerekeningOverzicht.SeparateAssessmentTaxedReserves3400Total.GetValueOrDefault();


            //Afzonderlijke aanslag van de belaste reserves ten name van erkende kredietinstellingen, tegen het tarief van 28 %
            Data.BerekeningOverzicht.SeparateAssessmentTaxedReserves2800Base = 0;
            Data.BerekeningOverzicht.SeparateAssessmentTaxedReserves2800Total = 0;
            if (Data.Aanslag.AfzonderlijkeAanslag28.GetValueOrDefault() > 0)
            {
                Data.BerekeningOverzicht.SeparateAssessmentTaxedReserves2800Base = Data.Aanslag.AfzonderlijkeAanslag28.GetValueOrDefault();
                Data.BerekeningOverzicht.SeparateAssessmentTaxedReserves2800Total = Data.BerekeningOverzicht.SeparateAssessmentTaxedReserves2800Base.GetValueOrDefault()
                    * (Data.BerekeningOverzicht.SeparateAssessmentTaxedReserves2800Perc.GetValueOrDefault() / 100);

            }
            Data.BerekeningOverzicht.TaxResult += Data.BerekeningOverzicht.SeparateAssessmentTaxedReserves2800Total.GetValueOrDefault();

            //Afzonderlijke aanslag van de uitgekeerde dividenden ten name van vennootschappen die krediet voor ambachtsoutillage mogen verstrekken en vennootschappen voor huisvesting
            Data.BerekeningOverzicht.SeparateAssessmentDividendsPaidBase = 0;
            Data.BerekeningOverzicht.SeparateAssessmentDividendsPaidTotal = 0;
            if (Data.Aanslag.AfzonderlijkeAanslagDividenden.GetValueOrDefault() > 0)
            {
                Data.BerekeningOverzicht.SeparateAssessmentDividendsPaidBase = Data.Aanslag.AfzonderlijkeAanslagDividenden.GetValueOrDefault();
                Data.BerekeningOverzicht.SeparateAssessmentDividendsPaidTotal = Data.BerekeningOverzicht.SeparateAssessmentDividendsPaidBase.GetValueOrDefault()
                    * (Data.BerekeningOverzicht.SeparateAssessmentDividendsPaidPerc.GetValueOrDefault() / 100);
            }
            Data.BerekeningOverzicht.TaxResult += Data.BerekeningOverzicht.SeparateAssessmentDividendsPaidTotal.GetValueOrDefault();

            // AFZONDERLIJKE AANSLAG FAIRNESS TAX (already calculated, not yet included)
            Data.BerekeningOverzicht.TaxResult += Data.BerekeningOverzicht.FairnessTaxTotal.GetValueOrDefault();

            //Afzonderlijke aanslag in het kader van de overgangsregeling van verlaagde roerende voorheffing op de uitkering van belaste reserves
            Data.BerekeningOverzicht.SeparateAssessmentTaxPaidTaxedReservesBase = 0;
            Data.BerekeningOverzicht.SeparateAssessmentTaxPaidTaxedReservesTotal = 0;
            if (Data.Aanslag.AfzonderlijkeAanslagRV.GetValueOrDefault() > 0)
            {
                Data.BerekeningOverzicht.SeparateAssessmentTaxPaidTaxedReservesBase = Data.Aanslag.AfzonderlijkeAanslagRV.GetValueOrDefault();
                Data.BerekeningOverzicht.SeparateAssessmentTaxPaidTaxedReservesTotal = Data.BerekeningOverzicht.SeparateAssessmentTaxPaidTaxedReservesBase.GetValueOrDefault()
                    * (Data.BerekeningOverzicht.SeparateAssessmentTaxPaidTaxedReservesPerc.GetValueOrDefault() / 100);
            }
            Data.BerekeningOverzicht.TaxResult += Data.BerekeningOverzicht.SeparateAssessmentTaxPaidTaxedReservesTotal.GetValueOrDefault();

            // Bijzondere Aanslagen

            Data.BerekeningOverzicht.DistributionCompanyAssets3300Base = 0;
            Data.BerekeningOverzicht.DistributionCompanyAssets3300Total = 0;
            if (Data.Aanslag.VerdelingVermogen33.GetValueOrDefault() > 0)
            {
                Data.BerekeningOverzicht.DistributionCompanyAssets3300Base = Data.Aanslag.VerdelingVermogen33.GetValueOrDefault();
                Data.BerekeningOverzicht.DistributionCompanyAssets3300Total = Data.BerekeningOverzicht.DistributionCompanyAssets3300Base.GetValueOrDefault()
                    * (Data.BerekeningOverzicht.DistributionCompanyAssets3300Perc.GetValueOrDefault() / 100);

            }
            Data.BerekeningOverzicht.TaxResult += Data.BerekeningOverzicht.DistributionCompanyAssets3300Total.GetValueOrDefault();

            Data.BerekeningOverzicht.DistributionCompanyAssets1650Base = 0;
            Data.BerekeningOverzicht.DistributionCompanyAssets1650Total = 0;
            if (Data.Aanslag.VerdelingVermogen16.GetValueOrDefault() > 0)
            {
                Data.BerekeningOverzicht.DistributionCompanyAssets1650Base = Data.Aanslag.VerdelingVermogen16.GetValueOrDefault();
                Data.BerekeningOverzicht.DistributionCompanyAssets1650Total = Data.BerekeningOverzicht.DistributionCompanyAssets1650Base.GetValueOrDefault()
                    * (Data.BerekeningOverzicht.DistributionCompanyAssets1650Perc.GetValueOrDefault() / 100);
            }
            Data.BerekeningOverzicht.TaxResult += Data.BerekeningOverzicht.DistributionCompanyAssets1650Total.GetValueOrDefault();

            Data.BerekeningOverzicht.BenefitsAllKindCompanyLiquidationBase = 0;
            Data.BerekeningOverzicht.BenefitsAllKindCompanyLiquidationTotal = 0;
            if (Data.Aanslag.VAAVennootschappen.GetValueOrDefault() > 0)
            {
                Data.BerekeningOverzicht.BenefitsAllKindCompanyLiquidationBase = Data.Aanslag.VAAVennootschappen.GetValueOrDefault();
                Data.BerekeningOverzicht.BenefitsAllKindCompanyLiquidationTotal = Data.BerekeningOverzicht.BenefitsAllKindCompanyLiquidationBase.GetValueOrDefault()
                    * (Data.BerekeningOverzicht.BenefitsAllKindCompanyLiquidationPerc.GetValueOrDefault() / 100);

            }
            Data.BerekeningOverzicht.TaxResult += Data.BerekeningOverzicht.BenefitsAllKindCompanyLiquidationTotal.GetValueOrDefault();

            // aanvullend diamant
            Data.BerekeningOverzicht.AdditionalDutiesDiamondTradersBase = 0;
            Data.BerekeningOverzicht.AdditionalDutiesDiamondTradersTotal = 0;
            if (Data.Aanslag.AanvullendeHeffingDiamant.GetValueOrDefault() > 0)
            {
                Data.BerekeningOverzicht.AdditionalDutiesDiamondTradersBase = Data.Aanslag.AanvullendeHeffingDiamant.GetValueOrDefault();
                Data.BerekeningOverzicht.AdditionalDutiesDiamondTradersTotal = Data.BerekeningOverzicht.AdditionalDutiesDiamondTradersBase.GetValueOrDefault()
                    * (Data.BerekeningOverzicht.AdditionalDutiesDiamondTradersPerc.GetValueOrDefault() / 100);
            }
            Data.BerekeningOverzicht.TaxResult += Data.BerekeningOverzicht.AdditionalDutiesDiamondTradersTotal.GetValueOrDefault();


            //Terugbetaling van een gedeelte van het voorheen verleende belastingkrediet voor onderzoek en ontwikkeling

            Data.BerekeningOverzicht.TaxResult += Data.BerekeningOverzicht.RetributionResearchDevelopmentTotal.GetValueOrDefault();


            // IPT ALREADY CALCULATED, JUST ADD TO TAXRESULT
            Data.BerekeningOverzicht.TaxResult += Data.BerekeningOverzicht.IPTTotal.GetValueOrDefault();


        }

        internal decimal? TaxCalculation()
        {

            if (Xbrl.Errors.Count > 0)
            {
                this.Xbrl.TaxCalcDetail = null;
                this.Xbrl.TaxCalc = null;
                return null;
            }

            TaxCalc.TaxCalc txc = new TaxCalc.TaxCalc();
            _ApplyFieldsToCalc(ref txc);
            _CalculateTax(ref txc);

            this.Xbrl.TaxCalcDetail = txc;
            this.Xbrl.TaxCalc = txc.BerekeningOverzicht.TaxResult;
            return txc.BerekeningOverzicht.TaxResult;
            /*
             decimal taxcalc = 0;

             if (Xbrl.Errors.Count > 0)
             {
                 return null;
             }

             bool ExclusionReducedRate = Bool(Xbrl.Elements.FirstOrDefault(x => x.Name == "ExclusionReducedRate"));
             bool CreditCorporationTradeEquipmentHousingCorporationTaxRate = Bool(Xbrl.Elements.FirstOrDefault(x => x.Name == "CreditCorporationTradeEquipmentHousingCorporationTaxRate"));

             // UITEENZETTING VAN DE WINST
             decimal BasicTaxableAmountCommonRate = Number(Xbrl.Elements.Where(x => x.Name == "BasicTaxableAmountCommonRate"), "0");

             if (BasicTaxableAmountCommonRate > 0)
             {

                 if ((!ExclusionReducedRate && !CreditCorporationTradeEquipmentHousingCorporationTaxRate)
                     || (ExclusionReducedRate && BasicTaxableAmountCommonRate > 322500)
                     )
                 {
                     taxcalc += BasicTaxableAmountCommonRate * MaximumRateCorporateTax;
                 }
                 else if (ExclusionReducedRate)
                 {
                     if (BasicTaxableAmountCommonRate <= (decimal)25000)
                     {
                         taxcalc += BasicTaxableAmountCommonRate * (decimal)0.24975;
                     }
                     else
                     {
                         taxcalc += 25000 * (decimal)0.249775;
                         BasicTaxableAmountCommonRate -= 25000;
                         if (BasicTaxableAmountCommonRate <= (decimal)65000)
                         {
                             taxcalc += BasicTaxableAmountCommonRate * (decimal)0.3193;
                         }
                         else
                         {
                             taxcalc += 65000 * (decimal)0.3193;
                             BasicTaxableAmountCommonRate -= 65000;

                             if (BasicTaxableAmountCommonRate <= (decimal)232500)
                             {
                                 taxcalc += BasicTaxableAmountCommonRate * (decimal)0.35535;
                             }
                             else
                             {
                                 taxcalc += 232500 * (decimal)0.35535;
                                 BasicTaxableAmountCommonRate -= 232500;

                             }
                         }
                     }

                 }
                 else if (CreditCorporationTradeEquipmentHousingCorporationTaxRate)
                 {
                     taxcalc += BasicTaxableAmountCommonRate * (decimal)0.0515;
                 }
             }


             decimal CapitalGainsSharesRate2500 = Number(Xbrl.Elements.Where(x => x.Name == "CapitalGainsSharesRate2500"), "0");
             if (CapitalGainsSharesRate2500 > 0)
             {
                 taxcalc += CapitalGainsSharesRate2500 * (decimal)0.2575;
             }

             decimal CapitalGainsSharesRate0040 = Number(Xbrl.Elements.Where(x => x.Name == "CapitalGainsSharesRate0040"), "0");
             if (CapitalGainsSharesRate0040 > 0)
             {
                 taxcalc += CapitalGainsSharesRate0040 * (decimal)0.004;
             }
            

             decimal BasicTaxableAmountExitTaxRate = Number(Xbrl.Elements.Where(x => x.Name == "BasicTaxableAmountExitTaxRate"), "0");
             if (BasicTaxableAmountExitTaxRate > 0)
             {
                 taxcalc += BasicTaxableAmountExitTaxRate * (decimal)0.16995;
             }

             decimal baseForIncrease = 0 + taxcalc;

             decimal CapitalSubsidiesInterestSubsidiesAgriculturalSupportRate500 = Number(Xbrl.Elements.Where(x => x.Name == "CapitalSubsidiesInterestSubsidiesAgriculturalSupportRate500"), "0");
             if (CapitalSubsidiesInterestSubsidiesAgriculturalSupportRate500 > 0)
             {
                 taxcalc += CapitalSubsidiesInterestSubsidiesAgriculturalSupportRate500 * (decimal)0.0515;
             }

             //AFZONDERLIJKE AANSLAGEN
             decimal UnjustifiedExpensesHiddenSurplusProfitsFinancialAdvantagesBenefitsAllKind = Number(Xbrl.Elements.Where(x => x.Name == "UnjustifiedExpensesHiddenSurplusProfitsFinancialAdvantagesBenefitsAllKind"), "0");
             if (UnjustifiedExpensesHiddenSurplusProfitsFinancialAdvantagesBenefitsAllKind > 0)
             {
                 taxcalc += UnjustifiedExpensesHiddenSurplusProfitsFinancialAdvantagesBenefitsAllKind * (decimal)3.09;
             }


             decimal SeparateAssessmentTaxedReservesCreditAssociationsInstitutionsOtherAuthorisedInstitutionsRate3400 = Number(Xbrl.Elements.Where(x => x.Name == "SeparateAssessmentTaxedReservesCreditAssociationsInstitutionsOtherAuthorisedInstitutionsRate3400"), "0");
             if (SeparateAssessmentTaxedReservesCreditAssociationsInstitutionsOtherAuthorisedInstitutionsRate3400 > 0)
             {
                 taxcalc += SeparateAssessmentTaxedReservesCreditAssociationsInstitutionsOtherAuthorisedInstitutionsRate3400 * (decimal)0.3502;
             }

             decimal SeparateAssessmentTaxedReservesCreditAssociationsInstitutionsOtherAuthorisedInstitutionsRate2800 = Number(Xbrl.Elements.Where(x => x.Name == "SeparateAssessmentTaxedReservesCreditAssociationsInstitutionsOtherAuthorisedInstitutionsRate2800"), "0");
             if (SeparateAssessmentTaxedReservesCreditAssociationsInstitutionsOtherAuthorisedInstitutionsRate2800 > 0)
             {
                 taxcalc += SeparateAssessmentTaxedReservesCreditAssociationsInstitutionsOtherAuthorisedInstitutionsRate2800 * (decimal)0.2884;
             }

             decimal SeparateAssessmentDividendsPaidCreditCorporationTradeEquipmentHousingCorporation = Number(Xbrl.Elements.Where(x => x.Name == "SeparateAssessmentDividendsPaidCreditCorporationTradeEquipmentHousingCorporation"), "0");
             if (SeparateAssessmentDividendsPaidCreditCorporationTradeEquipmentHousingCorporation > 0)
             {
                 taxcalc += SeparateAssessmentDividendsPaidCreditCorporationTradeEquipmentHousingCorporation * (decimal)0.3502;
             }

             decimal FairnessTax = Number(Xbrl.Elements.Where(x => x.Name == "FairnessTax"), "0");
             if (FairnessTax > 0)
             {
                 taxcalc += FairnessTax * (decimal)0.0515;
             }
            


             // BIJZONDERE AANSLAGEN
             decimal SpecialAssessmentPre1990FullPartialDistributionCompanyAssetsRate3300 = Number(Xbrl.Elements.Where(x => x.Name == "SpecialAssessmentPre1990FullPartialDistributionCompanyAssetsRate3300"), "0");
             if (SpecialAssessmentPre1990FullPartialDistributionCompanyAssetsRate3300 > 0)
             {
                 taxcalc += SpecialAssessmentPre1990FullPartialDistributionCompanyAssetsRate3300 * (decimal)0.33;
             }

             decimal SpecialAssessmentPre1990FullPartialDistributionCompanyAssetsRate1650 = Number(Xbrl.Elements.Where(x => x.Name == "SpecialAssessmentPre1990FullPartialDistributionCompanyAssetsRate1650"), "0");
             if (SpecialAssessmentPre1990FullPartialDistributionCompanyAssetsRate1650 > 0)
             {
                 taxcalc += SpecialAssessmentPre1990FullPartialDistributionCompanyAssetsRate1650 * (decimal)0.165;
             }

             decimal SpecialAssessmentPre1990BenefitsAllKindCompanyLiquidation = Number(Xbrl.Elements.Where(x => x.Name == "SpecialAssessmentPre1990BenefitsAllKindCompanyLiquidation"), "0");
             if (SpecialAssessmentPre1990BenefitsAllKindCompanyLiquidation > 0)
             {
                 taxcalc += SpecialAssessmentPre1990BenefitsAllKindCompanyLiquidation * (decimal)0.45;
             }


             // AANVULLENDE HEFFING
             decimal AdditionalDutiesDiamondTraders = Number(Xbrl.Elements.Where(x => x.Name == "AdditionalDutiesDiamondTraders"), "0");
             if (AdditionalDutiesDiamondTraders > 0)
             {
                 taxcalc += AdditionalDutiesDiamondTraders * (decimal)0.15;
             }

             // TOCHECK !!!
             decimal RetributionTaxCreditResearchDevelopment = Number(Xbrl.Elements.Where(x => x.Name == "RetributionTaxCreditResearchDevelopment"), "0");
             if (RetributionTaxCreditResearchDevelopment > 0)
             {
                 taxcalc += RetributionTaxCreditResearchDevelopment;
             }
            
             decimal SeparateAssessmentAdditionalIndividualPensionProvisionsOnEndTaxPeriodClosingDateBefore20120101 = Number(Xbrl.Elements.Where(x => x.Name == "SeparateAssessmentAdditionalIndividualPensionProvisionsOnEndTaxPeriodClosingDateBefore2012-01-01"), "0");

             if (SeparateAssessmentAdditionalIndividualPensionProvisionsOnEndTaxPeriodClosingDateBefore20120101 > 0)
             {
                taxcalc += SeparateAssessmentAdditionalIndividualPensionProvisionsOnEndTaxPeriodClosingDateBefore20120101 * (decimal)0.006;
               
                  //   taxcalc += SeparateAssessmentAdditionalIndividualPensionProvisionsOnEndTaxPeriodClosingDateBefore20120101 * (decimal)0.0175;
                
             }



             taxcalc = Math.Round(taxcalc, 2, MidpointRounding.AwayFromZero);

             // VERREKENBARE VOORHEFFINGEN

             decimal RepayableAdvanceLevies = Number(Xbrl.Elements.Where(x => x.Name == "RepayableAdvanceLevies"), "0");
             decimal TaxCreditResearchDevelopmentClearablePreviousTaxPeriodsRepayableCurrentAssessmentYear = Number(Xbrl.Elements.Where(x => x.Name == "TaxCreditResearchDevelopmentClearablePreviousTaxPeriodsRepayableCurrentAssessmentYear"), "0");
             decimal NonRepayableAdvanceLevies = Number(Xbrl.Elements.Where(x => x.Name == "NonRepayableAdvanceLevies"), "0");

             taxcalc -= RepayableAdvanceLevies;
             taxcalc -= TaxCreditResearchDevelopmentClearablePreviousTaxPeriodsRepayableCurrentAssessmentYear;
             taxcalc -= NonRepayableAdvanceLevies;

             baseForIncrease -= RepayableAdvanceLevies;
             baseForIncrease -= TaxCreditResearchDevelopmentClearablePreviousTaxPeriodsRepayableCurrentAssessmentYear;
             baseForIncrease -= NonRepayableAdvanceLevies;


             bool FirstThreeAccountingYearsSmallCompanyCorporationCode = Bool(Xbrl.Elements.FirstOrDefault(x => x.Name == "FirstThreeAccountingYearsSmallCompanyCorporationCode"));

             if (!FirstThreeAccountingYearsSmallCompanyCorporationCode)
             {
                 decimal baseIncrease = baseForIncrease * (decimal)0.0225;

                 decimal vooraf = baseIncrease;
                 decimal PrepaymentFirstQuarter = Number(Xbrl.Elements.Where(x => x.Name == "PrepaymentFirstQuarter"), "0");
                 decimal PrepaymentSecondQuarter = Number(Xbrl.Elements.Where(x => x.Name == "PrepaymentSecondQuarter"), "0");
                 decimal PrepaymentThirdQuarter = Number(Xbrl.Elements.Where(x => x.Name == "PrepaymentThirdQuarter"), "0");
                 decimal PrepaymentFourthQuarter = Number(Xbrl.Elements.Where(x => x.Name == "PrepaymentFourthQuarter"), "0");

                 vooraf -= PrepaymentFirstQuarter * (decimal)0.03;
                 vooraf -= PrepaymentSecondQuarter * (decimal)0.025;
                 vooraf -= PrepaymentThirdQuarter * (decimal)0.02;
                 vooraf -= PrepaymentFourthQuarter * (decimal)0.015;
                 vooraf = Math.Round(vooraf, 2, MidpointRounding.AwayFromZero);
                 if (vooraf < (baseForIncrease * (decimal)0.01))
                 {
                     vooraf = 0;
                 }

                 decimal increase = 0;
                 if (vooraf > 40)
                 {
                     increase = vooraf;
                 }
                 taxcalc += increase;

             }

             decimal Prepayments = Number(Xbrl.Elements.Where(x => x.Name == "Prepayments"), "0");

             taxcalc -= Prepayments;

             return taxcalc;
             */

        }

                internal void ApplyExemptions()
        {
            string scenarioId = GetScenarioId("");
            string period = "D";
            var RemainingFiscalResultBelgium = Number(GetElementsByScenDefs(new string[] { "d-origin:OriginDimension::d-origin:BelgiumMember" }, new string[] { "D" }, new string[] { "tax-inc:RemainingFiscalResult" }));
            var RemainingFiscalResultNoTaxTreaty = Number(GetElementsByScenDefs(new string[] { "d-origin:OriginDimension::d-origin:NoTaxTreatyMember" }, new string[] { "D" }, new string[] { "tax-inc:RemainingFiscalResult" }));

            // SOURCES
            // Niet belastbare bestanddelen
            var MiscExemptionsSource = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Period == "D" && x.Name == "DeductibleMiscellaneousExemptions"));
            // DBI
            var DBISource = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Period == "D" && x.Name == "AccumulatedPEExemptIncomeMovableAssets"));
            // aftrek octrooi inkomsten
            var PatentsIncomeSource = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Period == "D" && x.Name == "DeductibleDeductionPatentsIncome"));
            // NID (Aftrek risicokapitaal)
            var AllowanceCorporateEquitySource = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Period == "D" && x.Name == "DeductionsBranchImmovablePropertyInEEAOutsideEEATreatyAfterDeductions"));
            // Gecompenseerde verliezen / vorige verliezen
            var CompensatedLossesSource = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Period == "D" && x.Name == "CompensableTaxLosses"));
            // InvesteringsAftrek
            var InvestDeductSource1 = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Period == "D" && x.Name == "BasicTaxableInvestmentDeductionCompatibleTaxCreditResearchDevelopment"));
            var InvestDeductSource2 = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Period == "D" && x.Name == "BasicTaxableInvestmentDeduction"));
            // NID History : Aftrek Risicokapitaal historiek
            var AllowanceCorporateEquityHistorySource = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Period == "D" && x.Name == "ExemptionsAllowanceCorporateEquityAccumulatedUpToAndIncludingAssessmentYear2012"));
            CompensatedLossesSource = Math.Abs(CompensatedLossesSource);
            // TARGETS
            
            var MiscExemptionsBelgium = CreateNewCalculationElement(null, new string[] { "D" }, "tax-inc:MiscellaneousExemptions", new string[] { "d-origin:OriginDimension::d-origin:BelgiumMember" }, "EUR", "INF").First(); 
            var MiscExemptionNoTaxTreaty = CreateNewCalculationElement(null, new string[] { "D" }, "tax-inc:MiscellaneousExemptions", new string[] { "d-origin:OriginDimension::d-origin:NoTaxTreatyMember" }, "EUR", "INF").First();

            var DBIBelgium= CreateNewCalculationElement(null, new string[] { "D" }, "tax-inc:PEExemptIncomeMovableAssets", new string[] { "d-origin:OriginDimension::d-origin:BelgiumMember" }, "EUR", "INF").First();
            var DBINoTaxTreaty = CreateNewCalculationElement(null, new string[] { "D" }, "tax-inc:PEExemptIncomeMovableAssets", new string[] { "d-origin:OriginDimension::d-origin:NoTaxTreatyMember" }, "EUR", "INF").First();

            var PatentsIncomeBelgium = CreateNewCalculationElement(null, new string[] { "D" }, "tax-inc:DeductionPatentsIncome", new string[] { "d-origin:OriginDimension::d-origin:BelgiumMember" }, "EUR", "INF").First();
            var PatentsIncomeNoTaxTreaty = CreateNewCalculationElement(null, new string[] { "D" }, "tax-inc:DeductionPatentsIncome", new string[] { "d-origin:OriginDimension::d-origin:NoTaxTreatyMember" }, "EUR", "INF").First();

            var AllowanceCorporateEquityBelgium = CreateNewCalculationElement(null, new string[] { "D" }, "tax-inc:AllowanceCorporateEquity", new string[] { "d-origin:OriginDimension::d-origin:BelgiumMember" }, "EUR", "INF").First();
            var AllowanceCorporateEquityNoTaxTreaty = CreateNewCalculationElement(null, new string[] { "D" }, "tax-inc:AllowanceCorporateEquity", new string[] { "d-origin:OriginDimension::d-origin:NoTaxTreatyMember" }, "EUR", "INF").First();

            var CompensatedLossesBelgium = CreateNewCalculationElement(null, new string[] { "D" }, "tax-inc:CompensatedTaxLosses", new string[] { "d-origin:OriginDimension::d-origin:BelgiumMember" }, "EUR", "INF").First();
            var CompensatedLossesNoTaxTreaty = CreateNewCalculationElement(null, new string[] { "D" }, "tax-inc:CompensatedTaxLosses", new string[] { "d-origin:OriginDimension::d-origin:NoTaxTreatyMember" }, "EUR", "INF").First();

            var InvestDeductBelgium = CreateNewCalculationElement(null, new string[] { "D" }, "tax-inc:AllowanceInvestmentDeduction", new string[] { "d-origin:OriginDimension::d-origin:BelgiumMember" }, "EUR", "INF").First();

            var AllowanceCorporateEquityHistoryBelgium = CreateNewCalculationElement(null, new string[] { "D" }, "tax-inc:DeductionExemptionsAllowanceCorporateEquityAccumulatedUpToAndIncludingAssessmentYear2012", new string[] { "d-origin:OriginDimension::d-origin:BelgiumMember" }, "EUR", "INF").First();
            var AllowanceCorporateEquityHistoryNoTaxTreaty = CreateNewCalculationElement(null, new string[] { "D" }, "tax-inc:DeductionExemptionsAllowanceCorporateEquityAccumulatedUpToAndIncludingAssessmentYear2012", new string[] { "d-origin:OriginDimension::d-origin:NoTaxTreatyMember" }, "EUR", "INF").First();



            // TOTALs
            // NID (Aftrek risicokapitaal)
            var AllowanceCorporateEquityResult = CreateNewCalculationElement(null, new string[] { "D" }, "tax-inc:AllowanceCorporateEquity", new string[] { "" }, "EUR", "INF").First();
            // Gecompenseerde verliezen / vorige verliezen
            var CompensatedLossesResult = CreateNewCalculationElement(null, new string[] { "D" }, "tax-inc:CompensatedTaxLossesIncludingTaxTreaty", new string[] { "" }, "EUR", "INF").First();

            var InvestDeductResult1 = CreateNewCalculationElement(null, new string[] { "D" }, "tax-inc:DeductibleAllowanceInvestmentDeductionCompatibleTaxCreditResearchDevelopment", new string[] { "" }, "EUR", "INF").First();
            var InvestDeductResult2 = CreateNewCalculationElement(null, new string[] { "D" }, "tax-inc:DeductibleAllowanceInvestmentDeductionIncompatibleTaxCreditResearchDevelopment", new string[] { "" }, "EUR", "INF").First();

            var AllowanceCorporateEquityHistoryResult = CreateNewCalculationElement(null, new string[] { "D" }, "tax-inc:DeductionExemptionsAllowanceCorporateEquityAccumulatedUpToAndIncludingAssessmentYear2012", new string[] { "" }, "EUR", "INF").First();


            //if (MiscExemptionsBelgium == null)
            //{
            //    MiscExemptionsBelgium = 
            //}
            //if (MiscExemptionNoTaxTreaty == null)
            //{
            //    MiscExemptionNoTaxTreaty = 
            //}

            // RESET
            
            MiscExemptionsBelgium.SetNumber(0);
            MiscExemptionNoTaxTreaty.SetNumber(0);
            DBIBelgium.SetNumber(0);
            DBINoTaxTreaty.SetNumber(0);
            PatentsIncomeBelgium.SetNumber(0);
            PatentsIncomeNoTaxTreaty.SetNumber(0);
            AllowanceCorporateEquityBelgium.SetNumber(0);
            AllowanceCorporateEquityNoTaxTreaty.SetNumber(0);
            CompensatedLossesBelgium.SetNumber(0);
            CompensatedLossesNoTaxTreaty.SetNumber(0);
            InvestDeductBelgium.SetNumber(0);
            AllowanceCorporateEquityHistoryBelgium.SetNumber(0);
            AllowanceCorporateEquityHistoryNoTaxTreaty.SetNumber(0);

            MiscExemptionsBelgium.Locked = true;
            MiscExemptionNoTaxTreaty.Locked = true;
            DBIBelgium.Locked = true;
            DBINoTaxTreaty.Locked = true;
            PatentsIncomeBelgium.Locked = true;
            PatentsIncomeNoTaxTreaty.Locked = true;
            AllowanceCorporateEquityBelgium.Locked = true;
            AllowanceCorporateEquityNoTaxTreaty.Locked = true;
            CompensatedLossesBelgium.Locked = true;
            CompensatedLossesNoTaxTreaty.Locked = true;
            InvestDeductBelgium.Locked = true;
            AllowanceCorporateEquityHistoryBelgium.Locked = true;
            AllowanceCorporateEquityHistoryNoTaxTreaty.Locked = true;
            
          


            // NIET BELASTBARE BESTANDDELEN
            if (RemainingFiscalResultBelgium > 0 && MiscExemptionsSource>0)
            {
                var exempted = RemainingFiscalResultBelgium > MiscExemptionsSource ? MiscExemptionsSource : RemainingFiscalResultBelgium;
                MiscExemptionsBelgium.SetNumber(exempted);
                MiscExemptionsSource -= exempted;
                RemainingFiscalResultBelgium -= exempted;

            }
            if (RemainingFiscalResultNoTaxTreaty > 0 && MiscExemptionsSource > 0)
            {
                var exempted = RemainingFiscalResultNoTaxTreaty > MiscExemptionsSource ? MiscExemptionsSource : RemainingFiscalResultNoTaxTreaty;
                MiscExemptionNoTaxTreaty.SetNumber(exempted);
                MiscExemptionsSource -= exempted;
                RemainingFiscalResultNoTaxTreaty -= exempted;

            }

            // DBI
            if (RemainingFiscalResultBelgium > 0 && DBISource > 0)
            {
                var exempted = RemainingFiscalResultBelgium > DBISource ? DBISource : RemainingFiscalResultBelgium;
                DBIBelgium.SetNumber(exempted);
                DBISource -= exempted;
                RemainingFiscalResultBelgium -= exempted;

            }
            if (RemainingFiscalResultNoTaxTreaty > 0 && DBISource > 0)
            {
                var exempted = RemainingFiscalResultNoTaxTreaty > DBISource ? DBISource : RemainingFiscalResultNoTaxTreaty;
                DBINoTaxTreaty.SetNumber(exempted);
                DBISource -= exempted;
                RemainingFiscalResultNoTaxTreaty -= exempted;

            }

            // AFTREK OCTROOIINKOMSTEN
            if (RemainingFiscalResultBelgium > 0 && PatentsIncomeSource > 0)
            {
                var exempted = RemainingFiscalResultBelgium > PatentsIncomeSource ? PatentsIncomeSource : RemainingFiscalResultBelgium;
                PatentsIncomeBelgium.SetNumber(exempted);
                PatentsIncomeSource -= exempted;
                RemainingFiscalResultBelgium -= exempted;

            }
            if (RemainingFiscalResultNoTaxTreaty > 0 && PatentsIncomeSource > 0)
            {
                var exempted = RemainingFiscalResultNoTaxTreaty > PatentsIncomeSource ? PatentsIncomeSource : RemainingFiscalResultNoTaxTreaty;
                PatentsIncomeNoTaxTreaty.SetNumber(exempted);
                PatentsIncomeSource -= exempted;
                RemainingFiscalResultNoTaxTreaty -= exempted;

            }

            // NID (aftrek risicokapitaal)
            if (RemainingFiscalResultBelgium > 0 && AllowanceCorporateEquitySource > 0)
            {
                var exempted = RemainingFiscalResultBelgium > AllowanceCorporateEquitySource ? AllowanceCorporateEquitySource : RemainingFiscalResultBelgium;
                AllowanceCorporateEquityBelgium.SetNumber(exempted);
                AllowanceCorporateEquitySource -= exempted;
                RemainingFiscalResultBelgium -= exempted;

            }
            if (RemainingFiscalResultNoTaxTreaty > 0 && AllowanceCorporateEquitySource > 0)
            {
                var exempted = RemainingFiscalResultNoTaxTreaty > AllowanceCorporateEquitySource ? AllowanceCorporateEquitySource : RemainingFiscalResultNoTaxTreaty;
                AllowanceCorporateEquityNoTaxTreaty.SetNumber(exempted);
                AllowanceCorporateEquitySource -= exempted;
                RemainingFiscalResultNoTaxTreaty -= exempted;

            }
            // SET RESULT USED
            AllowanceCorporateEquityResult.SetNumber(AllowanceCorporateEquityBelgium.GetNumberOrDefault(0) + AllowanceCorporateEquityNoTaxTreaty.GetNumberOrDefault(0));

            // Gecompenseerde verliezen
            if (RemainingFiscalResultBelgium > 0 && CompensatedLossesSource > 0)
            {
                var exempted = RemainingFiscalResultBelgium > CompensatedLossesSource ? CompensatedLossesSource : RemainingFiscalResultBelgium;
                CompensatedLossesBelgium.SetNumber(exempted);
                CompensatedLossesSource -= exempted;
                RemainingFiscalResultBelgium -= exempted;

            }
            if (RemainingFiscalResultNoTaxTreaty > 0 && CompensatedLossesSource > 0)
            {
                var exempted = RemainingFiscalResultNoTaxTreaty > CompensatedLossesSource ? CompensatedLossesSource : RemainingFiscalResultNoTaxTreaty;
                CompensatedLossesNoTaxTreaty.SetNumber(exempted);
                CompensatedLossesSource -= exempted;
                RemainingFiscalResultNoTaxTreaty -= exempted;

            }
            // SET RESULT USED
            CompensatedLossesResult.SetNumber(CompensatedLossesBelgium.GetNumberOrDefault(0) + CompensatedLossesNoTaxTreaty.GetNumberOrDefault(0));

            InvestDeductResult1.SetNumber(0);
            InvestDeductResult2.SetNumber(0);
            if (InvestDeductSource1 > 0)
            {
                if (RemainingFiscalResultBelgium > 0 && InvestDeductSource1 > 0)
                {
                    var exempted = RemainingFiscalResultBelgium > InvestDeductSource1 ? InvestDeductSource1 : RemainingFiscalResultBelgium;
                    InvestDeductBelgium.SetNumber(exempted);
                    InvestDeductSource1 -= exempted;
                    RemainingFiscalResultBelgium -= exempted;
                    InvestDeductResult1.SetNumber(exempted);
                }

            }
            else if (InvestDeductSource2 > 0)
            {
                if (RemainingFiscalResultBelgium > 0 && InvestDeductSource2 > 0)
                {
                    var exempted = RemainingFiscalResultBelgium > InvestDeductSource2 ? InvestDeductSource2 : RemainingFiscalResultBelgium;
                    InvestDeductBelgium.SetNumber(exempted);
                    InvestDeductSource2 -= exempted;
                    RemainingFiscalResultBelgium -= exempted;
                    InvestDeductResult2.SetNumber(exempted);

                }

            }


            // NID (aftrek risicokapitaal) HISTORIEK
            if (RemainingFiscalResultBelgium > 0 && AllowanceCorporateEquityHistorySource > 0)
            {
                var exempted = RemainingFiscalResultBelgium > AllowanceCorporateEquityHistorySource ? AllowanceCorporateEquityHistorySource : RemainingFiscalResultBelgium;

                if (exempted > 1000000)
                {
                    exempted -= 1000000;
                    exempted = 1000000 + (exempted * 60 / 100); //If the figure is higher than a million, then set aside the million and add 60% off the remaining figure
                }

                AllowanceCorporateEquityHistoryBelgium.SetNumber(exempted);
                AllowanceCorporateEquityHistorySource -= exempted;
                RemainingFiscalResultBelgium -= exempted;

            }
            if (RemainingFiscalResultNoTaxTreaty > 0 && AllowanceCorporateEquityHistorySource > 0)
            {
                var exempted = RemainingFiscalResultNoTaxTreaty > AllowanceCorporateEquityHistorySource ? AllowanceCorporateEquityHistorySource : RemainingFiscalResultNoTaxTreaty;

                if (exempted > 1000000)
                {
                    exempted -= 1000000;
                    exempted = 1000000 + (exempted * 60 / 100); //If the figure is higher than a million, then set aside the million and add 60% off the remaining figure
                }

                AllowanceCorporateEquityHistoryNoTaxTreaty.SetNumber(exempted);
                AllowanceCorporateEquityHistorySource -= exempted;
                RemainingFiscalResultNoTaxTreaty -= exempted;

            }
            // SET RESULT USED
            AllowanceCorporateEquityHistoryResult.SetNumber(AllowanceCorporateEquityHistoryBelgium.GetNumberOrDefault(0) + AllowanceCorporateEquityHistoryNoTaxTreaty.GetNumberOrDefault(0));
        }
    }
}
