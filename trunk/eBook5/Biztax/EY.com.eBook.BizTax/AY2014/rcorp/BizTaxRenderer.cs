﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System;
using System.Xml;
using System.Xml.Linq;
using EY.com.eBook.BizTax.Contracts;
using EY.com.eBook.API.Contracts.Data.BizTax;

namespace EY.com.eBook.BizTax.AY2014.rcorp
{
    public class BizTaxRenderer : BizTaxRendererGenerated
    {
        public List<XmlQualifiedName> NameSpaces
        {
            get
            {
                return new List<XmlQualifiedName> {
          
            new XmlQualifiedName("xlink","http://www.w3.org/1999/xlink")
              ,
            new XmlQualifiedName("","http://www.xbrl.org/2003/instance")
          ,
            new XmlQualifiedName("iso4217","http://www.xbrl.org/2003/iso4217")
          ,
            new XmlQualifiedName("xsi","http://www.w3.org/2001/XMLSchema-instance")
          ,
            new XmlQualifiedName("link","http://www.xbrl.org/2003/linkbase")
   
          ,
            new XmlQualifiedName("xbrldi","http://xbrl.org/2006/xbrldi")
          ,
            new XmlQualifiedName("xbrldt","http://xbrl.org/2005/xbrldt")
          ,
            new XmlQualifiedName("tax-inc-rcorp","http://www.minfin.fgov.be/be/tax/inc/rcorp/2014-04-30")
          ,
            new XmlQualifiedName("xl","http://www.xbrl.org/2003/XLink")
          ,
            new XmlQualifiedName("t-reso","http://www.minfin.fgov.be/be/tax/t/reso/2014-04-30")
          ,
            new XmlQualifiedName("d-ty","http://www.minfin.fgov.be/be/tax/d/ty/2014-04-30")
          ,
            new XmlQualifiedName("tax-inc","http://www.minfin.fgov.be/be/tax/inc/2014-04-30")
          ,
            new XmlQualifiedName("pfs-dt","http://www.nbb.be/be/fr/pfs/ci/dt/2014-04-01")
          ,
            new XmlQualifiedName("ref","http://www.xbrl.org/2006/ref")
          ,
            new XmlQualifiedName("t-drfa","http://www.minfin.fgov.be/be/tax/t/drfa/2014-04-30")
          ,
            new XmlQualifiedName("t-wddc","http://www.minfin.fgov.be/be/tax/t/wddc/2014-04-30")
          ,
            new XmlQualifiedName("d-hh","http://www.minfin.fgov.be/be/tax/d/hh/2014-04-30")
          ,
            new XmlQualifiedName("d-ec","http://www.minfin.fgov.be/be/tax/d/ec/2014-04-30")
          ,
            new XmlQualifiedName("t-prre","http://www.minfin.fgov.be/be/tax/t/prre/2014-04-30")
          ,
            new XmlQualifiedName("d-expt","http://www.minfin.fgov.be/be/tax/d/expt/2014-04-30")
          ,
            new XmlQualifiedName("t-cg","http://www.minfin.fgov.be/be/tax/t/cg/2014-04-30")
          ,
            new XmlQualifiedName("d-asst","http://www.minfin.fgov.be/be/tax/d/asst/2014-04-30")
          ,
            new XmlQualifiedName("t-ricg","http://www.minfin.fgov.be/be/tax/t/ricg/2014-04-30")
          ,
            new XmlQualifiedName("t-stcg","http://www.minfin.fgov.be/be/tax/t/stcg/2014-04-30")
          ,
            new XmlQualifiedName("t-ace","http://www.minfin.fgov.be/be/tax/t/ace/2014-04-30")
          ,
            new XmlQualifiedName("t-pcs","http://www.minfin.fgov.be/be/tax/t/pcs/2014-04-30")
          ,
            new XmlQualifiedName("t-dpi","http://www.minfin.fgov.be/be/tax/t/dpi/2014-04-30")
          ,
            new XmlQualifiedName("t-itifa","http://www.minfin.fgov.be/be/tax/t/itifa/2014-04-30")
          ,
            new XmlQualifiedName("d-per","http://www.minfin.fgov.be/be/tax/d/per/2014-04-30")
          ,
            new XmlQualifiedName("t-idtc","http://www.minfin.fgov.be/be/tax/t/idtc/2014-04-30")
          ,
            new XmlQualifiedName("d-incc","http://www.minfin.fgov.be/be/tax/d/incc/2014-04-30")
          ,
            new XmlQualifiedName("d-invc","http://www.minfin.fgov.be/be/tax/d/invc/2014-04-30")
          ,
            new XmlQualifiedName("t-eap","http://www.minfin.fgov.be/be/tax/t/eap/2014-04-30")
          ,
            new XmlQualifiedName("d-empstat","http://www.minfin.fgov.be/be/tax/d/empstat/2014-04-30")
          ,
            new XmlQualifiedName("d-empreg","http://www.minfin.fgov.be/be/tax/d/empreg/2014-04-30")
          ,
            new XmlQualifiedName("d-tu","http://www.minfin.fgov.be/be/tax/d/tu/2014-04-30")
          ,
            new XmlQualifiedName("t-eapsf","http://www.minfin.fgov.be/be/tax/t/eapsf/2014-04-30")
          ,
            new XmlQualifiedName("d-empf","http://www.minfin.fgov.be/be/tax/d/empf/2014-04-30")
          ,
            new XmlQualifiedName("d-empper","http://www.minfin.fgov.be/be/tax/d/empper/2014-04-30")
          ,
            new XmlQualifiedName("t-dfa","http://www.minfin.fgov.be/be/tax/t/dfa/2014-04-30")
          ,
            new XmlQualifiedName("d-depm","http://www.minfin.fgov.be/be/tax/d/depm/2014-04-30")
          ,
            new XmlQualifiedName("d-br","http://www.minfin.fgov.be/be/tax/d/br/2014-04-30")
          ,
            new XmlQualifiedName("d-origin","http://www.minfin.fgov.be/be/tax/d/origin/2014-04-30")
          ,
            new XmlQualifiedName("pfs-gcd","http://www.nbb.be/be/fr/pfs/ci/gcd/2014-04-01")
          ,
            new XmlQualifiedName("pfs-vl","http://www.nbb.be/be/fr/pfs/ci/vl/2014-04-01")
          
        };
            }

        }

        public XDocument RenderBizTaxFile(BizTaxDataContract btdc, Guid fileId)
        {
            return RenderBizTaxFile(btdc, fileId, NameSpaces,TaxonomySchemaRef_rcorp);
        }


        public override BizTaxDataContract CalculateAndValidate()
        {
            base.CalculateAndValidate();
            Extra_275C_AllowanceCorporateCheck();
            Extra_275C_AllowanceCorporateHistoryCheck();
            Extra_275P_PatentsIncomeCheck();
            Extra_275U_TaxCreditResearchDevelopmentCheck();
            Extra_276Ws();
            // EXTRA'S

            return this.Xbrl;
        }



        internal override void Validation_f_rcorp_2211_assertion_set()
        {
            // PERIOD: "D"
            var RemainingFiscalResultBelgium = GetElementsByScenDefs(new string[] { "d-origin:OriginDimension::d-origin:BelgiumMember" }, new string[] { "D" }, new string[] { "tax-inc:RemainingFiscalResult" });
            var RemainingFiscalResultTaxTreaty = GetElementsByScenDefs(new string[] { "d-origin:OriginDimension::d-origin:TaxTreatyMember" }, new string[] { "D" }, new string[] { "tax-inc:RemainingFiscalResult" });
            var RemainingFiscalResultNoTaxTreaty = GetElementsByScenDefs(new string[] { "d-origin:OriginDimension::d-origin:NoTaxTreatyMember" }, new string[] { "D" }, new string[] { "tax-inc:RemainingFiscalResult" });
            var RemainingFiscalResultBeforeOriginDistribution = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:RemainingFiscalResultBeforeOriginDistribution" });
            var LossCurrentTaxPeriod = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:LossCurrentTaxPeriod" });
            if (RemainingFiscalResultBeforeOriginDistribution.Count > 0 || LossCurrentTaxPeriod.Count > 0 || RemainingFiscalResultBelgium.Count > 0 || RemainingFiscalResultTaxTreaty.Count > 0 || RemainingFiscalResultNoTaxTreaty.Count > 0)
            {
                var CalculatedAmountRemainingFiscalResultOriginDistributed = Sum(Number(RemainingFiscalResultBelgium, "0"), Number(RemainingFiscalResultTaxTreaty, "0"), Number(RemainingFiscalResultNoTaxTreaty, "0"));
                //CALCULATION HERE
                if ((Number(RemainingFiscalResultBeforeOriginDistribution, "0") < (decimal)0) || (Number(LossCurrentTaxPeriod, "0") < (decimal)0))
                {
                    //FORMULA HERE
                    bool test = ((Sum(Number(RemainingFiscalResultBelgium, "0"), Number(RemainingFiscalResultTaxTreaty, "0"), Number(RemainingFiscalResultNoTaxTreaty, "0")) < (decimal)0) && (Number(RemainingFiscalResultBeforeOriginDistribution, "0") < Sum(Number(RemainingFiscalResultBelgium, "0"), Number(RemainingFiscalResultTaxTreaty, "0"), Number(RemainingFiscalResultNoTaxTreaty, "0")))) ? (Sum(Number(RemainingFiscalResultBelgium, "0"), Number(RemainingFiscalResultTaxTreaty, "0"), Number(RemainingFiscalResultNoTaxTreaty, "0")) == Number(LossCurrentTaxPeriod, "0")) : (((Sum(Number(RemainingFiscalResultBelgium, "0"), Number(RemainingFiscalResultTaxTreaty, "0"), Number(RemainingFiscalResultNoTaxTreaty, "0")) >= (decimal)0) && (Number(RemainingFiscalResultBeforeOriginDistribution, "0") < Sum(Number(RemainingFiscalResultBelgium, "0"), Number(RemainingFiscalResultTaxTreaty, "0"), Number(RemainingFiscalResultNoTaxTreaty, "0")))) ? (Number(LossCurrentTaxPeriod, "0") == (decimal)0) : ((Number(RemainingFiscalResultBeforeOriginDistribution, "0") == Number(LossCurrentTaxPeriod, "0")) && (Sum(Number(RemainingFiscalResultBelgium, "0"), Number(RemainingFiscalResultTaxTreaty, "0"), Number(RemainingFiscalResultNoTaxTreaty, "0")) == Number(RemainingFiscalResultBeforeOriginDistribution, "0"))));
                    if (!test)
                    {
                        // MESSAGES
                        AddMessage(new BizTaxErrorDataContract
                        {
                            FileId = _fileId,
                            Id = "f-rcorp-2211",
                            Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Het bedrag vermeld in de rubriek '{Verlies van het belastbare tijdperk}' ({$LossCurrentTaxPeriod}) moet gelijk zijn aan het negatieve bedrag vermeld in de rubriek '{Resterend resultaat}' ({$RemainingFiscalResultBeforeOriginDistribution}) tenzij het zogenaamde \"recapture-mechanisme\" van toepassing is, d. i. wanneer het tijdens het belastbaar tijdperk geleden verlies in een buitenlandse inrichting niet in aanmerking kan worden genomen voor het vaststellen van de belastbare basis (art. 185, Â§ 3, WIB 92) of wanneer vorige verliezen geleden in een buitenlandse inrichting moeten worden toegevoegd aan het belastbaar inkomen van het belastbaar tijdperk (art. 206, Â§ 1, tweede lid, tweede zin, WIB 92). In voormelde gevallen moet het bedrag vermeld in de rubriek '{Verlies van het belastbare tijdperk}' ({$LossCurrentTaxPeriod}) gelijk zijn aan de som van de bedragen vermeld in de rubriek '{Resterend resultaat volgens oorsprong}' (= {$CalculatedAmountRemainingFiscalResultOriginDistributed})."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Le montant repris dans la rubrique '{Perte de la pÃ©riode imposable}' ({$LossCurrentTaxPeriod}) doit Ãªtre Ã©gal au montant nÃ©gatif mentionnÃ© dans la rubrique '{RÃ©sultat subsistant}' ({$RemainingFiscalResultBeforeOriginDistribution}) sauf si le mÃ©canisme dit de 'recapture' est d'application, c.-Ã -d. quand le montant des pertes de la pÃ©riode imposable Ã©prouvÃ©es dans un Ã©tablissement Ã©tranger n'est pas pris en considÃ©ration pour dÃ©terminer la base imposable (art. 185, Â§ 3, CIR 92) ou quand les pertes antÃ©rieures Ã©prouvÃ©es dans un Ã©tablissement Ã©tranger doivent Ãªtre ajoutÃ©es au bÃ©nÃ©fice imposable de la pÃ©riode imposable (art. 206, Â§ 1, 2Ã¨me alinÃ©a, 2Â°, CIR 92). Dans les cas mentionnÃ©s ci-dessus le montant mentionnÃ© dans la rubrique '{Perte de la pÃ©riode imposable}' ({$LossCurrentTaxPeriod}) doit Ãªtre Ã©gal Ã  la somme des montants repris dans la rubrique '{RÃ©sultat subsistant suivant sa provenance}' (= {$CalculatedAmountRemainingFiscalResultOriginDistributed})."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Der in der Rubrik '{Verlust des Besteuerungszeitraums}' erwÃ¤hnte Betrag ({$LossCurrentTaxPeriod}) muss dem negativen Betrag in der Rubrik '{Verbleibendes Ergebnis}' ({$RemainingFiscalResultBeforeOriginDistribution}) entsprechen, es sei denn, dass der so genannte \"recapture-Mechanismus\" Anwendung findet, d.h. wenn der wÃ¤hrend des Besteuerungszeitraums erlittene Verlust in einer auslÃ¤ndischer Niederlassung  fÃ¼r die Festlegung der steuerpflichtigen Grundlage nicht berÃ¼cksichtigt werden kann (Art. 185, Â§ 3, EStGB 92) oder wenn vorige in einer auslÃ¤ndischen Niederlassung erlittene Verluste zum steuerpflichtigen Einkommen des Besteuerungszeitraums hinzugefÃ¼gt werden mÃ¼ssen (Art. 206, Â§ 1, 2. Absatz, 2. Satz EStGB 92). In den vorerwÃ¤hnten FÃ¤llen muss der in der Rubrik '{Verlust des Besteuerungszeitraums}' angegebene Betrag ({$LossCurrentTaxPeriod}) der Summe der in der Rubrik '{Verbleibendes Ergebnis nach Herkunft}' erwÃ¤hnten BetrÃ¤ge (= {$CalculatedAmountRemainingFiscalResultOriginDistributed}) entsprechen."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Het bedrag vermeld in de rubriek '{Verlies van het belastbare tijdperk}' ({$LossCurrentTaxPeriod}) moet gelijk zijn aan het negatieve bedrag vermeld in de rubriek '{Resterend resultaat}' ({$RemainingFiscalResultBeforeOriginDistribution}) tenzij het zogenaamde \"recapture-mechanisme\" van toepassing is, d. i. wanneer het tijdens het belastbaar tijdperk geleden verlies in een buitenlandse inrichting niet in aanmerking kan worden genomen voor het vaststellen van de belastbare basis (art. 185, Â§ 3, WIB 92) of wanneer vorige verliezen geleden in een buitenlandse inrichting moeten worden toegevoegd aan het belastbaar inkomen van het belastbaar tijdperk (art. 206, Â§ 1, tweede lid, tweede zin, WIB 92). In voormelde gevallen moet het bedrag vermeld in de rubriek '{Verlies van het belastbare tijdperk}' ({$LossCurrentTaxPeriod}) gelijk zijn aan de som van de bedragen vermeld in de rubriek '{Resterend resultaat volgens oorsprong}' (= {$CalculatedAmountRemainingFiscalResultOriginDistributed})."}  
 }
                            ,
                            Fields = new List<string>()
                        }
                        );
                    }
                }
            }

        }


        internal override void Validation_f_idtc_5353_assertion_set()
        {
          
            string scenarioId = GetScenarioId("d-incc:IncentiveCategoryDimension::d-incc:InvestmentDeductionIncompatibleTaxCreditResearchDevelopmentMember/d-invc:InvestmentCategoryDimension::d-invc:ResearchDevelopmentMember");
            string period = "D";
            // SCENARIO: "d-incc:IncentiveCategoryDimension::d-incc:InvestmentDeductionIncompatibleTaxCreditResearchDevelopmentMember/d-invc:InvestmentCategoryDimension::d-invc:ResearchDevelopmentMember"
            // PERIOD: "D"
            var EligibleDepreciations_ResearchDevelopment = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:EligibleDepreciations" });
            var AcquisitionInvestmentValueSpread_ResearchDevelopment = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:AcquisitionInvestmentValueSpread" });
            if (EligibleDepreciations_ResearchDevelopment.Count > 0 || AcquisitionInvestmentValueSpread_ResearchDevelopment.Count > 0)
            {
                if (Number(EligibleDepreciations_ResearchDevelopment, "0") != 0 && Number(AcquisitionInvestmentValueSpread_ResearchDevelopment, "0") != 0)
                {
                    //CALCULATION HERE
                    //FORMULA HERE
                    bool test = Number(EligibleDepreciations_ResearchDevelopment, "0") < Number(AcquisitionInvestmentValueSpread_ResearchDevelopment, "0");
                    if (!test)
                    {
                        // MESSAGES
                        AddMessage(new BizTaxErrorDataContract
                        {
                            FileId = _fileId,
                            Id = "f-idtc-5353",
                            Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Het bedrag vermeld in de rubriek '{Aanneembare afschrijvingen}' moet kleiner zijn dan het bedrag vermeld in de rubriek '{Aanschaffings- of beleggingswaarde, afschrijfbaar}'."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Le montant repris dans la rubrique '{Amortissements admissibles}' doit Ãªtre infÃ©rieure au montant repris dans la rubrique '{Valeur d'acquisition ou d'investissement, amortissable}'."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Der in der Rubrik '{ZulÃ¤ssige Abschreibungen}' angegebene Betrag muss niedriger sein als der in der Rubrik '{Abschreibbarer Investitions- oder Anschaffungswert}' angegebene Betrag."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Het bedrag vermeld in de rubriek '{Aanneembare afschrijvingen}' moet kleiner zijn dan het bedrag vermeld in de rubriek '{Aanschaffings- of beleggingswaarde, afschrijfbaar}'."}  
 }
                            ,
                            Fields = new List<string>()
                        }
                        );
                    }
                }
            }

        
        }

        internal override void Validation_f_rcorp_2231_assertion_set()
        {
            // PERIOD: "D"
            var DeductibleAllowanceInvestmentDeductionIncompatibleTaxCreditResearchDevelopment = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:DeductibleAllowanceInvestmentDeductionIncompatibleTaxCreditResearchDevelopment" });
            var AllowanceInvestmentDeductionBelgium = GetElementsByScenDefs(new string[] { "d-origin:OriginDimension::d-origin:BelgiumMember" }, new string[] { "D" }, new string[] { "tax-inc:AllowanceInvestmentDeduction" });
            var DeductibleAllowanceInvestmentDeductionCompatibleTaxCreditResearchDevelopment = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:DeductibleAllowanceInvestmentDeductionCompatibleTaxCreditResearchDevelopment" });
            if (AllowanceInvestmentDeductionBelgium.Count > 0 || DeductibleAllowanceInvestmentDeductionIncompatibleTaxCreditResearchDevelopment.Count > 0 || DeductibleAllowanceInvestmentDeductionCompatibleTaxCreditResearchDevelopment.Count > 0)
            {
                //CALCULATION HERE
                //FORMULA HERE
                bool test = (Number(DeductibleAllowanceInvestmentDeductionIncompatibleTaxCreditResearchDevelopment, "0") > (decimal)0) ? ((Number(AllowanceInvestmentDeductionBelgium, "0") == Number(DeductibleAllowanceInvestmentDeductionIncompatibleTaxCreditResearchDevelopment, "0")) && (Number(DeductibleAllowanceInvestmentDeductionCompatibleTaxCreditResearchDevelopment, "0") == (decimal)0)) : ((Number(DeductibleAllowanceInvestmentDeductionCompatibleTaxCreditResearchDevelopment, "0") > (decimal)0) ? ((Number(AllowanceInvestmentDeductionBelgium, "0") == Number(DeductibleAllowanceInvestmentDeductionCompatibleTaxCreditResearchDevelopment, "0")) && (Number(DeductibleAllowanceInvestmentDeductionIncompatibleTaxCreditResearchDevelopment, "0") == (decimal)0)) : (Number(AllowanceInvestmentDeductionBelgium, "0") == (decimal)0));
                if (!test)
                {
                    // MESSAGES
                    AddMessage(new BizTaxErrorDataContract
                    {
                        FileId = _fileId,
                        Id = "f-rcorp-2231",
                        Messages = new List<BizTaxErrorMessageDataContract> { 
new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = string.Format("Het bedrag vermeld in de rubriek 'Investeringsaftrek' moet gelijk zijn aan het bedrag ingevuld in de bijlage in de rubriek 'Investeringsaftrek aftrekbaar voor het belastbare tijdperk' ({1} of {0}).",Number(DeductibleAllowanceInvestmentDeductionCompatibleTaxCreditResearchDevelopment).ToString("N",DefaultCulture),Number(DeductibleAllowanceInvestmentDeductionIncompatibleTaxCreditResearchDevelopment).ToString("N",DefaultCulture))}  
,new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = string.Format("Le montant mentionné dans la rubrique 'Déduction pour investissement' doit être égal à celui rempli dans l'annexe dans la rubrique 'Déduction pour investissement déductible pour la période imposable' ({1} of {0}).",Number(DeductibleAllowanceInvestmentDeductionCompatibleTaxCreditResearchDevelopment).ToString("N",DefaultCulture),Number(DeductibleAllowanceInvestmentDeductionIncompatibleTaxCreditResearchDevelopment).ToString("N",DefaultCulture))}  
,new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = string.Format("Der in der Rubrik 'Investitionsabzug' angegebene Betrag muss dem in der Rubrik  'Für den Besteuerungszeitraum abziehbarer Investitionsabzug' ({1} oder {0}) der Anlage ausgefüllten Betrag entsprechen.",Number(DeductibleAllowanceInvestmentDeductionCompatibleTaxCreditResearchDevelopment).ToString("N",DefaultCulture),Number(DeductibleAllowanceInvestmentDeductionIncompatibleTaxCreditResearchDevelopment).ToString("N",DefaultCulture))}  
,new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "NO TRANSLATION AVAILABLE IN LANGUAGE en-US."}  
 }
                        ,
                        Fields = new List<string>()
                    }
                    );
                }
            }

        }

        internal override void Validation_f_rcorp_2209b_assertion_set()
        {
            // PERIOD: "D"
            var RemainingFiscalResultBelgium = GetElementsByScenDefs(new string[] { "d-origin:OriginDimension::d-origin:BelgiumMember" }, new string[] { "D" }, new string[] { "tax-inc:RemainingFiscalResult" });
            var RemainingFiscalResultTaxTreaty = GetElementsByScenDefs(new string[] { "d-origin:OriginDimension::d-origin:TaxTreatyMember" }, new string[] { "D" }, new string[] { "tax-inc:RemainingFiscalResult" });
            var RemainingFiscalResultNoTaxTreaty = GetElementsByScenDefs(new string[] { "d-origin:OriginDimension::d-origin:NoTaxTreatyMember" }, new string[] { "D" }, new string[] { "tax-inc:RemainingFiscalResult" });
            var RemainingFiscalResultBeforeOriginDistribution = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:RemainingFiscalResultBeforeOriginDistribution" });
            if (RemainingFiscalResultBeforeOriginDistribution.Count > 0 || RemainingFiscalResultBelgium.Count > 0 || RemainingFiscalResultTaxTreaty.Count > 0 || RemainingFiscalResultNoTaxTreaty.Count > 0)
            {
                var CalculatedAmountRemainingFiscalResultOriginDistributed = Sum(Number(RemainingFiscalResultBelgium, "0"), Number(RemainingFiscalResultTaxTreaty, "0"), Number(RemainingFiscalResultNoTaxTreaty, "0"));
                //FORMULA HERE
                bool test = Sum(Number(RemainingFiscalResultBelgium, "0"), Number(RemainingFiscalResultTaxTreaty, "0"), Number(RemainingFiscalResultNoTaxTreaty, "0")) == Number(RemainingFiscalResultBeforeOriginDistribution, "0");
                if (!test)
                {
                    // MESSAGES
                    AddMessage(new BizTaxErrorDataContract
                    {
                        FileId = _fileId,
                        Id = "f-rcorp-2209b",
                        Messages = new List<BizTaxErrorMessageDataContract> { 
new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = string.Format("De som van de bedragen vermeld in de rubriek 'Resterend resultaat volgens oorsprong' 	(= {1}) 	gelijk zijn aan het bedrag ingevuld in de rubriek 	'Resterend resultaat' 	({0}).",Number(RemainingFiscalResultBeforeOriginDistribution).ToString(DefaultCulture),Number(CalculatedAmountRemainingFiscalResultOriginDistributed).ToString(DefaultCulture))}  
,new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = string.Format("La somme des montants mentionnés dans la rubrique 'Résultat subsistant suivant sa provenance' 	(= {1}) 		doit être égale au montant repris dans la rubrique 	'Résultat subsistant' 	({0}).",Number(RemainingFiscalResultBeforeOriginDistribution).ToString(DefaultCulture),Number(CalculatedAmountRemainingFiscalResultOriginDistributed).ToString(DefaultCulture))}  
,new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = string.Format("Die Summe der in der Rubrik 'Resterend resultaat volgens oorsprong' 	(= {1}) 	angegebenen Beträge muss sich mindestens auf den in der Rubrik 	'Resterend resultaat' 	({0}) angegebenen Betrag belaufen.",Number(RemainingFiscalResultBeforeOriginDistribution).ToString(DefaultCulture),Number(CalculatedAmountRemainingFiscalResultOriginDistributed).ToString(DefaultCulture))}  
,new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "NO TRANSLATION AVAILABLE IN LANGUAGE en-US."}  
 }
                        ,
                        Fields = GetFieldIdsOf(new List<object> { RemainingFiscalResultBeforeOriginDistribution, RemainingFiscalResultBelgium, RemainingFiscalResultTaxTreaty, RemainingFiscalResultNoTaxTreaty, CalculatedAmountRemainingFiscalResultOriginDistributed })
                    }
                    );
                }
            } // END: hasVarList checkif(RemainingFiscalResultBeforeOriginDistribution.Count > 0 || RemainingFiscalResultBelgium.Count > 0 || RemainingFiscalResultTaxTreaty.Count > 0 || RemainingFiscalResultNoTaxTreaty.Count > 0)

        } // END FUNCTION

        internal override void Validation_f_itifa_5327_assertion_set()
        {
            List<string> scenarioIds = GetScenarioIds(new string[] {"d-asst:AssetTypeDimension::d-asst:TangibleIntangibleFixedAssetsMember/d-per:PeriodDimension::d-per:TaxPeriod-0Member", "d-asst:AssetTypeDimension::d-asst:TangibleIntangibleFixedAssetsMember/d-per:PeriodDimension::d-per:TaxPeriod-1Member", "d-asst:AssetTypeDimension::d-asst:TangibleIntangibleFixedAssetsMember/d-per:PeriodDimension::d-per:TaxPeriod-2Member" });
            foreach (string scenarioId in scenarioIds)
            {
                string period = "D";
                // SCENARIO: "d-asst:AssetTypeDimension::d-asst:TangibleIntangibleFixedAssetsMember/d-per:PeriodDimension::d-per:TaxPeriod-1Member"
                // SCENARIO: "d-asst:AssetTypeDimension::d-asst:TangibleIntangibleFixedAssetsMember/d-per:PeriodDimension::d-per:TaxPeriod-2Member"
                // PERIOD: "D"
                var ExemptInvestmentReserveOverview = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:ExemptInvestmentReserveOverview" });
                var InvestmentsPreviousInvestmentReserveTaxPeriod = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:InvestmentsPreviousInvestmentReserveTaxPeriod" });
              //  var InvestmentCurrentTaxPeriod = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:InvestmentCurrentTaxPeriod" });

                var InvestmentCurrentTaxPeriod = GetElementsByScenDefsFilter(scenarioId,new string[] { "d-asst:AssetTypeDimension::d-asst:TangibleIntangibleFixedAssetsMember/d-per:PeriodDimension::d-per:TaxPeriod-0Member/d-ty:DateTypedDimension/d-ty:DescriptionTypedDimension", "d-asst:AssetTypeDimension::d-asst:TangibleIntangibleFixedAssetsMember/d-per:PeriodDimension::d-per:TaxPeriod-1Member/d-ty:DateTypedDimension/d-ty:DescriptionTypedDimension", "d-asst:AssetTypeDimension::d-asst:TangibleIntangibleFixedAssetsMember/d-per:PeriodDimension::d-per:TaxPeriod-2Member/d-ty:DateTypedDimension/d-ty:DescriptionTypedDimension" }, new string[] { period }, new string[] { "tax-inc:InvestmentCurrentTaxPeriod" });

                //var InvestmentCurrentTaxPeriod = GetElementsByScenDefs(new string[] { "d-asst:AssetTypeDimension::d-asst:TangibleIntangibleFixedAssetsMember/d-per:PeriodDimension::d-per:TaxPeriod-0Member/d-ty:DateTypedDimension/d-ty:DescriptionTypedDimension", "d-asst:AssetTypeDimension::d-asst:TangibleIntangibleFixedAssetsMember/d-per:PeriodDimension::d-per:TaxPeriod-1Member/d-ty:DateTypedDimension/d-ty:DescriptionTypedDimension", "d-asst:AssetTypeDimension::d-asst:TangibleIntangibleFixedAssetsMember/d-per:PeriodDimension::d-per:TaxPeriod-2Member/d-ty:DateTypedDimension/d-ty:DescriptionTypedDimension" }, new string[] { period }, new string[] { "tax-inc:InvestmentCurrentTaxPeriod" });
                var InvestBalance = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:InvestBalance" });
                var FormulaTarget = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:InvestBalance" });
                if (InvestBalance.Count > 0 || ExemptInvestmentReserveOverview.Count > 0 || InvestmentsPreviousInvestmentReserveTaxPeriod.Count > 0 || InvestmentCurrentTaxPeriod.Count > 0 || FormulaTarget.Count > 0)
                {
                    var dimensionValueQName = FactExplicitScenarioDimensionValue(ExemptInvestmentReserveOverview, QName("http://www . minfin . fgov . be/be/tax/d/per/2014-04-30", "d-per:PeriodDimension"));
                    var dimensionValueString = Data(LocalNameFromQname(dimensionValueQName));
                    var dimensionValue = SubstringBefore(dimensionValueString, "Member");
                    //CALCULATION HERE
                    if (FormulaTarget.Count == 0)
                    {
                        FormulaTarget = CreateNewCalculationElement(scenarioId, period, "tax-inc:InvestBalance", new string[] { "d-asst:AssetTypeDimension::d-asst:TangibleIntangibleFixedAssetsMember/d-per:PeriodDimension::d-per:TaxPeriod-1Member", "d-asst:AssetTypeDimension::d-asst:TangibleIntangibleFixedAssetsMember/d-per:PeriodDimension::d-per:TaxPeriod-2Member" }, "EUR", "INF");
                    }
                    if (FormulaTarget.Count > 1)
                    {
                        throw new Exception("More then one target for FormulaTarget in assertion f-itifa-5327");
                    }
                    FormulaTarget.First().Calculated = true;
                    FormulaTarget.First().SetNumber(Max(-Sum(-Number(ExemptInvestmentReserveOverview, "0"), Number(InvestmentsPreviousInvestmentReserveTaxPeriod, "0"), Number(InvestmentCurrentTaxPeriod, "0")), (decimal)0));
                    //FORMULA HERE
                    bool test = Max(-Sum(-Number(ExemptInvestmentReserveOverview, "0"), Number(InvestmentsPreviousInvestmentReserveTaxPeriod, "0"), Number(InvestmentCurrentTaxPeriod, "0")), (decimal)0) == Number(InvestBalance, "0");
                    if (!test)
                    {
                        // MESSAGES
                        AddMessage(new BizTaxErrorDataContract
                        {
                            FileId = _fileId,
                            Id = "f-itifa-5327",
                            Messages = new List<BizTaxErrorMessageDataContract> { 
new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = string.Format("Het totaal vermeld in de rubriek 'Nog te investeren saldo' ({0}) is niet juist (= ).",Number(InvestBalance).ToString("N",DefaultCulture))}  
,new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = string.Format("Le total repris dans la rubrique 'Solde des investissements à effectuer' ({0}) n'est pas correct (= ).",Number(InvestBalance).ToString("N",DefaultCulture))}  
,new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = string.Format("Der in der Rubrik 'Saldo der noch vorzunehmenden Investitionen' angegebene Gesamtbetrag ({0}) ist unzutreffend (= ).",Number(InvestBalance).ToString("N",DefaultCulture))}  
,new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "NO TRANSLATION AVAILABLE IN LANGUAGE en-US."}  
 }
                            ,
                            Fields = new List<string>()
                        }
                        );
                    }
                }
            } // end scenario loop

        }

        internal override void Validation_f_corp_1415a_assertion_set()
        {
            string scenarioId = GetScenarioId("");
            string period = "D";
            // SCENARIO: ""
            // PERIOD: "D"
            var Prepayments = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:Prepayments" });
            var FormulaTarget = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:Prepayments" });
            var PrepaymentFirstQuarter = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:PrepaymentFirstQuarter" });
            var PrepaymentSecondQuarter = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:PrepaymentSecondQuarter" });
            var PrepaymentThirdQuarter = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:PrepaymentThirdQuarter" });
            var PrepaymentFourthQuarter = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:PrepaymentFourthQuarter" });
            if (Prepayments.Count > 0 || PrepaymentFirstQuarter.Count > 0 || PrepaymentSecondQuarter.Count > 0 || PrepaymentThirdQuarter.Count > 0 || PrepaymentFourthQuarter.Count > 0)
            {
                var CalculatedAmountPrepayments = Sum(Number(PrepaymentFirstQuarter, "0"), Number(PrepaymentSecondQuarter, "0"), Number(PrepaymentThirdQuarter, "0"), Number(PrepaymentFourthQuarter, "0"));
                //CALCULATION HERE
                //FORMULA HERE
                if (FormulaTarget.Count == 0)
                {
                    FormulaTarget = CreateNewCalculationElement(scenarioId, period, "tax-inc:Prepayments", new string[] { "" }, "EUR", "INF");
                }
                if (FormulaTarget.Count > 1)
                {
                    throw new Exception("More then one target for FormulaTarget in assertion f-corp-1415a");
                }
                FormulaTarget.First().Calculated = true;
                FormulaTarget.First().SetNumber(CalculatedAmountPrepayments);
                   

            }

        }

        internal override void Validation_f_ace_5311_assertion_set()
        {
            string scenarioId = GetScenarioId("");
            string period = "D";
            // SCENARIO: ""
            // PERIOD: "D"
            var DeductionsBranchImmovablePropertyInEEAOutsideEEATreatyAfterDeductions = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:DeductionsBranchImmovablePropertyInEEAOutsideEEATreatyAfterDeductions" });
            var DeductibleAllowanceCorporateEquityCurrentAssessmentYear = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:DeductibleAllowanceCorporateEquityCurrentAssessmentYear" });
            var DeductionBranchImmovablePropertyInEEATreaty = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:DeductionBranchImmovablePropertyInEEATreaty" });
            var DeductionBranchImmovablePropertyOutsideEEATreaty = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:DeductionBranchImmovablePropertyOutsideEEATreaty" });
            var FormulaTarget = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:DeductionsBranchImmovablePropertyInEEAOutsideEEATreatyAfterDeductions" });
            if (DeductionsBranchImmovablePropertyInEEAOutsideEEATreatyAfterDeductions.Count > 0 || DeductibleAllowanceCorporateEquityCurrentAssessmentYear.Count > 0 || DeductionBranchImmovablePropertyInEEATreaty.Count > 0 || DeductionBranchImmovablePropertyOutsideEEATreaty.Count > 0 || FormulaTarget.Count > 0)
            {
                var CalculatedAmountDeductionsBranchImmovablePropertyInEEAOutsideEEATreatyAfterDeductions = Max(Sum(Number(DeductibleAllowanceCorporateEquityCurrentAssessmentYear, "0"), -Number(DeductionBranchImmovablePropertyInEEATreaty, "0"), Number(DeductionBranchImmovablePropertyOutsideEEATreaty, "0")), (decimal)0);
                //CALCULATION HERE
                if (FormulaTarget.Count == 0)
                {
                    FormulaTarget = CreateNewCalculationElement(scenarioId, period, "tax-inc:DeductionsBranchImmovablePropertyInEEAOutsideEEATreatyAfterDeductions", new string[] { "" }, "EUR", "INF");
                }
                if (FormulaTarget.Count > 1)
                {
                    throw new Exception("More then one target for FormulaTarget in assertion f-ace-5311");
                }
                FormulaTarget.First().Calculated = true;
                FormulaTarget.First().SetNumber(Max(Sum(Number(DeductibleAllowanceCorporateEquityCurrentAssessmentYear, "0"), -Number(DeductionBranchImmovablePropertyInEEATreaty, "0"), -Number(DeductionBranchImmovablePropertyOutsideEEATreaty, "0")), (decimal)0));
            } // END: hasVarList checkif(DeductionsBranchImmovablePropertyInEEAOutsideEEATreatyAfterDeductions.Count > 0 || DeductibleAllowanceCorporateEquityCurrentAssessmentYear.Count > 0 || DeductionBranchImmovablePropertyInEEATreaty.Count > 0 || DeductionBranchImmovablePropertyOutsideEEATreaty.Count > 0 || FormulaTarget.Count > 0)

        } // END FUNCTION

        internal override void Validation_f_corp_1404_assertion_set()
        {
            string scenarioId = GetScenarioId("");
            string period = "D";
            // SCENARIO: ""
            // PERIOD: "D"
            var ExemptGifts = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:ExemptGifts" });
            var ShippingResultNotTonnageBased = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:ShippingResultNotTonnageBased" });
            if (ExemptGifts.Count > 0 || ShippingResultNotTonnageBased.Count > 0)
            {
                var CalculatedAmountLimitExemptGifts = Math.Round(Number(ShippingResultNotTonnageBased, "0") * Number(RateShippingResultNotTonnageBased, "0.05") * (decimal)100, MidpointRounding.AwayFromZero) / (decimal)100;
                //CALCULATION HERE
                if ((Number(ShippingResultNotTonnageBased, "0") >= (decimal)0) && (Number(ExemptGifts, "0") > (decimal)0))
                {
                    //FORMULA HERE
                    bool test = ((Math.Round(Number(ExemptGifts, "0") * (decimal)100, MidpointRounding.AwayFromZero) / (decimal)100) <= (Math.Round(Number(ShippingResultNotTonnageBased, "0") * Number(RateShippingResultNotTonnageBased, "0.05") * (decimal)100, MidpointRounding.AwayFromZero) / (decimal)100)) && (Number(ExemptGifts, "0") <= (decimal)500000);
                    if (!test)
                    {
                        // MESSAGES
                        AddMessage(new BizTaxErrorDataContract
                        {
                            FileId = _fileId,
                            Id = "f-corp-1404",
                            Messages = new List<BizTaxErrorMessageDataContract> { 
new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Het bedrag vermeld in de rubriek 'Vrijgestelde giften' 	moet kleiner zijn dan of gelijk zijn aan 5% van het bedrag vermeld in de rubriek 	'Werkelijk resultaat uit activiteiten waarvoor de winst niet wordt vastgesteld op basis van de tonnage' , met een maximum van 500 000 EUR. "}  
,new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Le montant repris dans la rubrique 'Libéralités exonérées' 	doit être inférieur ou égal à 5% du montant repris dans la rubrique 	'Résultat effectif des activités pour lesquelles le bénéfice n’est pas déterminé sur base du tonnage' , au maximum 500 000 EUR. "}  
,new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Der in der Rubrik 'Steuerfreie unentgeltliche Zuwendungen' 	angegebene Betrag darf sich, mit einem Maximalwert in Höhe von 500 000 EUR, maximal auf 5% des in der Rubrik 	'Effektives Ergebnis aus Aktivitäten, für die der Gewinn nicht anhand der Tonnage bestimmt wird'  angegebenen Betrags belaufen. "}  
,new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "NO TRANSLATION AVAILABLE IN LANGUAGE en-US."}  
 }
                            ,
                            Fields = new List<string>()
                        }
                        );
                    }
                }
            }

        }

        

        #region Extra checks
        internal void Extra_275C_AllowanceCorporateCheck()
        {
            var AllowanceCorporateEquityBelgium = GetElementsByScenDefs(new string[] { "d-origin:OriginDimension::d-origin:BelgiumMember" }, new string[] { "D" }, new string[] { "tax-inc:AllowanceCorporateEquity" });
            var AllowanceCorporateEquityNoTaxTreaty = GetElementsByScenDefs(new string[] { "d-origin:OriginDimension::d-origin:NoTaxTreatyMember" }, new string[] { "D" }, new string[] { "tax-inc:AllowanceCorporateEquity" });
            var AllowanceCorporateEquity275C = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:AllowanceCorporateEquity" });

            var calced = Sum(Number(AllowanceCorporateEquityBelgium, "0"), Number(AllowanceCorporateEquityNoTaxTreaty, "0"));
            bool test = calced == Number(AllowanceCorporateEquity275C, "0");

            if (!test)
            {
                // MESSAGES
                AddMessage(new BizTaxErrorDataContract
                {
                    FileId = _fileId,
                    Id = "f-xtra-rcorp-275c-allowcorp",
                    Messages = new List<BizTaxErrorMessageDataContract> { 
                        new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = string.Format("Het totaal vermeld in formulier 275C, 'Aftrek voor risicokapitaal van het huidig aanslagjaar die werkelijk wordt afgetrokken rubriek' ({0}€) is niet gelijk aan de som ({3}€) van de bedragen vermeld onder 'Belgisch' ({1}€) en 'Niet bij verdrag vrijgesteld' ({2}€) in het vak 'Uiteenzetting van de winst'.",Number(AllowanceCorporateEquity275C,"0").ToString("N",DefaultCulture),Number(AllowanceCorporateEquityBelgium,"0").ToString("N",DefaultCulture),Number(AllowanceCorporateEquityNoTaxTreaty,"0").ToString("N",DefaultCulture),Number(calced).ToString("N",DefaultCulture))} , 

                        new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = string.Format("Le total repris en formulaire 275C, 'Déduction pour capital à risque de l'exercice d'imposition actuel, effectivement déduite' ({0}€) n'est pas égal à la somme ({3}€) des quantités énumérées 'Belge' ({1}€) et 'Non exonéré par convention' ({2}€) dans la rubrique 'Détail des bénéfices'.",Number(AllowanceCorporateEquity275C,"0").ToString("N",DefaultCulture),Number(AllowanceCorporateEquityBelgium,"0").ToString("N",DefaultCulture),Number(AllowanceCorporateEquityNoTaxTreaty,"0").ToString("N",DefaultCulture),Number(calced).ToString("N",DefaultCulture))}  ,
                        new BizTaxErrorMessageDataContract { Culture = "en-US", Message = string.Format("Het totaal vermeld in formulier 275C, 'Aftrek voor risicokapitaal van het huidig aanslagjaar die werkelijk wordt afgetrokken rubriek' ({0}€) is niet gelijk aan de som ({3}€) van de bedragen vermeld onder 'Belgisch' ({1}€) en 'Niet bij verdrag vrijgesteld' ({2}€) in het vak 'Uiteenzetting van de winst'.",Number(AllowanceCorporateEquity275C,"0").ToString("N",DefaultCulture),Number(AllowanceCorporateEquityBelgium,"0").ToString("N",DefaultCulture),Number(AllowanceCorporateEquityNoTaxTreaty,"0").ToString("N",DefaultCulture),Number(calced).ToString("N",DefaultCulture))} , 
                        new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = string.Format("Het totaal vermeld in formulier 275C, 'Aftrek voor risicokapitaal van het huidig aanslagjaar die werkelijk wordt afgetrokken rubriek' ({0}€) is niet gelijk aan de som ({3}€) van de bedragen vermeld onder 'Belgisch' ({1}€) en 'Niet bij verdrag vrijgesteld' ({2}€) in het vak 'Uiteenzetting van de winst'.",Number(AllowanceCorporateEquity275C,"0").ToString("N",DefaultCulture),Number(AllowanceCorporateEquityBelgium,"0").ToString("N",DefaultCulture),Number(AllowanceCorporateEquityNoTaxTreaty,"0").ToString("N",DefaultCulture),Number(calced).ToString("N",DefaultCulture))}  
                    }
                    ,
                    Fields = new List<string>()
                }
                );
            }
        }

        internal void Extra_275C_AllowanceCorporateHistoryCheck() 
        {
            var AllowanceCorporateEquity2012Belgium = GetElementsByScenDefs(new string[] { "d-origin:OriginDimension::d-origin:BelgiumMember" }, new string[] { "D" }, new string[] { "tax-inc:DeductionExemptionsAllowanceCorporateEquityAccumulatedUpToAndIncludingAssessmentYear2012" });
            var AllowanceCorporateEquity2012NoTaxTreaty = GetElementsByScenDefs(new string[] { "d-origin:OriginDimension::d-origin:NoTaxTreatyMember" }, new string[] { "D" }, new string[] { "tax-inc:DeductionExemptionsAllowanceCorporateEquityAccumulatedUpToAndIncludingAssessmentYear2012" });
            var AllowanceCorporateEquity2012275C = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:DeductionExemptionsAllowanceCorporateEquityAccumulatedUpToAndIncludingAssessmentYear2012" });

            var calced = Sum(Number(AllowanceCorporateEquity2012Belgium, "0"), Number(AllowanceCorporateEquity2012NoTaxTreaty, "0"));
            bool test = calced == Number(AllowanceCorporateEquity2012275C, "0");

            if (!test)
            {
                // MESSAGES
                AddMessage(new BizTaxErrorDataContract
                {
                    FileId = _fileId,
                    Id = "f-xtra-rcorp-275c-allowcorphist",
                    Messages = new List<BizTaxErrorMessageDataContract> { 
                        new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = string.Format("Het totaal vermeld in formulier 275C, 'Voorheen en ten laatste tijdens aanslagjaar 2012 gevormde vrijstellingen voor risicokapitaal die werkelijk worden afgetrokken tijdens het huidige aanslagjaar' ({0}€) is niet gelijk aan de som ({3}€) van de bedragen vermeld onder 'Belgisch' ({1}€) en 'Niet bij verdrag vrijgesteld' ({2}€) in het vak 'Uiteenzetting van de winst'.",Number(AllowanceCorporateEquity2012275C,"0").ToString("N",DefaultCulture),Number(AllowanceCorporateEquity2012Belgium,"0").ToString("N",DefaultCulture),Number(AllowanceCorporateEquity2012NoTaxTreaty,"0").ToString("N",DefaultCulture),Number(calced).ToString("N",DefaultCulture))} , 

                        new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = string.Format("Le total repris en formulaire 275C, 'Exonérations pour capital à risque constituées antérieurement et au plus tard au cours de l'exercice d'imposition 2012, effectivement déduites au cours de l'exercice d'imposition actuel' ({0}€) n'est pas égal à la somme ({3}€) des quantités énumérées 'Belge' ({1}€) et 'Non exonéré par convention' ({2}€) dans la rubrique 'Détail des bénéfices'.",Number(AllowanceCorporateEquity2012275C,"0").ToString("N",DefaultCulture),Number(AllowanceCorporateEquity2012Belgium,"0").ToString("N",DefaultCulture),Number(AllowanceCorporateEquity2012NoTaxTreaty,"0").ToString("N",DefaultCulture),Number(calced).ToString("N",DefaultCulture))}  ,
                        new BizTaxErrorMessageDataContract { Culture = "en-US", Message = string.Format("Het totaal vermeld in formulier 275C, 'Voorheen en ten laatste tijdens aanslagjaar 2012 gevormde vrijstellingen voor risicokapitaal die werkelijk worden afgetrokken tijdens het huidige aanslagjaar' ({0}€) is niet gelijk aan de som ({3}€) van de bedragen vermeld onder 'Belgisch' ({1}€) en 'Niet bij verdrag vrijgesteld' ({2}€) in het vak 'Uiteenzetting van de winst'.",Number(AllowanceCorporateEquity2012275C,"0").ToString("N",DefaultCulture),Number(AllowanceCorporateEquity2012Belgium,"0").ToString("N",DefaultCulture),Number(AllowanceCorporateEquity2012NoTaxTreaty,"0").ToString("N",DefaultCulture),Number(calced).ToString("N",DefaultCulture))} , 
                        new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = string.Format("Het totaal vermeld in formulier 275C, 'Voorheen en ten laatste tijdens aanslagjaar 2012 gevormde vrijstellingen voor risicokapitaal die werkelijk worden afgetrokken tijdens het huidige aanslagjaar' ({0}€) is niet gelijk aan de som ({3}€) van de bedragen vermeld onder 'Belgisch' ({1}€) en 'Niet bij verdrag vrijgesteld' ({2}€) in het vak 'Uiteenzetting van de winst'.",Number(AllowanceCorporateEquity2012275C,"0").ToString("N",DefaultCulture),Number(AllowanceCorporateEquity2012Belgium,"0").ToString("N",DefaultCulture),Number(AllowanceCorporateEquity2012NoTaxTreaty,"0").ToString("N",DefaultCulture),Number(calced).ToString("N",DefaultCulture))} 
                    }
                    ,
                    Fields = new List<string>()
                }
                );
            }
        }


        internal void Extra_275P_PatentsIncomeCheck()
        {
            var DeductibleDeductionPatentsIncomeBelgium = GetElementsByScenDefs(new string[] { "d-origin:OriginDimension::d-origin:BelgiumMember" }, new string[] { "D" }, new string[] { "tax-inc:DeductionPatentsIncome" });
            var DeductibleDeductionPatentsIncomeNoTaxTreaty = GetElementsByScenDefs(new string[] { "d-origin:OriginDimension::d-origin:NoTaxTreatyMember" }, new string[] { "D" }, new string[] { "tax-inc:DeductionPatentsIncome" });
            var DeductibleDeductionPatentsIncome275P = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:DeductibleDeductionPatentsIncome" });

            var calced = Sum(Number(DeductibleDeductionPatentsIncomeBelgium, "0"), Number(DeductibleDeductionPatentsIncomeNoTaxTreaty, "0"));
            bool test = calced == Number(DeductibleDeductionPatentsIncome275P, "0");

            if (!test)
            {
                // MESSAGES
                AddMessage(new BizTaxErrorDataContract
                {
                    FileId = _fileId,
                    Id = "f-xtra-rcorp-275p-patincome",
                    Messages = new List<BizTaxErrorMessageDataContract> { 
                        new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = string.Format("Het totaal vermeld in formulier 275P, 'Aftrek voor octrooi-inkomsten' ({0}€) is niet gelijk aan de som ({3}€) van de bedragen vermeld onder 'Belgisch' ({1}€) en 'Niet bij verdrag vrijgesteld' ({2}€) in het vak 'Uiteenzetting van de winst'.",Number(DeductibleDeductionPatentsIncome275P,"0").ToString("N",DefaultCulture),Number(DeductibleDeductionPatentsIncomeBelgium,"0").ToString("N",DefaultCulture),Number(DeductibleDeductionPatentsIncomeNoTaxTreaty,"0").ToString("N",DefaultCulture),Number(calced).ToString("N",DefaultCulture))} , 

                        new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = string.Format("Le total repris en formulaire 275P, 'Déduction pour revenus de brevets' ({0}€) n'est pas égal à la somme ({3}€) des quantités énumérées 'Belge' ({1}€) et 'Non exonéré par convention' ({2}€) dans la rubrique 'Détail des bénéfices'.",Number(DeductibleDeductionPatentsIncome275P,"0").ToString("N",DefaultCulture),Number(DeductibleDeductionPatentsIncomeBelgium,"0").ToString("N",DefaultCulture),Number(DeductibleDeductionPatentsIncomeNoTaxTreaty,"0").ToString("N",DefaultCulture),Number(calced).ToString("N",DefaultCulture))}  ,
                        new BizTaxErrorMessageDataContract { Culture = "en-US", Message = string.Format("Het totaal vermeld in formulier 275P, 'Aftrek voor octrooi-inkomsten' ({0}€) is niet gelijk aan de som ({3}€) van de bedragen vermeld onder 'Belgisch' ({1}€) en 'Niet bij verdrag vrijgesteld' ({2}€) in het vak 'Uiteenzetting van de winst'.",Number(DeductibleDeductionPatentsIncome275P,"0").ToString("N",DefaultCulture),Number(DeductibleDeductionPatentsIncomeBelgium,"0").ToString("N",DefaultCulture),Number(DeductibleDeductionPatentsIncomeNoTaxTreaty,"0").ToString("N",DefaultCulture),Number(calced).ToString("N",DefaultCulture))} , 
                        new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = string.Format("Het totaal vermeld in formulier 275P, 'Aftrek voor octrooi-inkomsten' ({0}€) is niet gelijk aan de som ({3}€) van de bedragen vermeld onder 'Belgisch' ({1}€) en 'Niet bij verdrag vrijgesteld' ({2}€) in het vak 'Uiteenzetting van de winst'.",Number(DeductibleDeductionPatentsIncome275P,"0").ToString("N",DefaultCulture),Number(DeductibleDeductionPatentsIncomeBelgium,"0").ToString("N",DefaultCulture),Number(DeductibleDeductionPatentsIncomeNoTaxTreaty,"0").ToString("N",DefaultCulture),Number(calced).ToString("N",DefaultCulture))}  
                    }
                    ,
                    Fields = new List<string>()
                }
                );
            }
        }

        internal void Extra_275U_TaxCreditResearchDevelopmentCheck()
        {

            //DeductibleAllowanceInvestmentDeductionIncompatibleTaxCreditResearchDevelopment
            var AllowanceInvestmentDeductionBelgium = GetElementsByScenDefs(new string[] { "d-origin:OriginDimension::d-origin:BelgiumMember" }, new string[] { "D" }, new string[] { "tax-inc:AllowanceInvestmentDeduction" });
            var TaxCreditResearchDevelopment275U_G = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:DeductibleAllowanceInvestmentDeductionIncompatibleTaxCreditResearchDevelopment" });
            var TaxCreditResearchDevelopment275U_E = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:DeductibleAllowanceInvestmentDeductionCompatibleTaxCreditResearchDevelopment" });

            bool test = Number(AllowanceInvestmentDeductionBelgium, "0") == Number(TaxCreditResearchDevelopment275U_G, "0")
                           || Number(AllowanceInvestmentDeductionBelgium, "0") == Number(TaxCreditResearchDevelopment275U_E, "0");

            if (!test)
            {
                // MESSAGES
                AddMessage(new BizTaxErrorDataContract
                {
                    FileId = _fileId,
                    Id = "f-xtra-rcorp-275u-taxcredit",
                    Messages = new List<BizTaxErrorMessageDataContract> { 
                        new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = string.Format("Het bedrag ({0}€) aangewend in het vak 'Uiteenzetting van de winst' onder 'Investeringsaftrek aftrekbaar voor het belastbare tijdperk' is niet gelijk aan het bedrag in formulier 275U, vak 'Investeringsaftrek aftrekbaar voor het belastbare tijdperk' onder 'Investeringsaftrek voor vennootschappen die niet opteren voor het belastingkrediet voor onderzoek en ontwikkeling' [code 1437.1] ({1}€) noch onder 'Investeringsaftrek voor vennootschappen die opteren voor het belastingkrediet voor onderzoek en ontwikkeling' [code 1437.2] ({2}€).",Number(AllowanceInvestmentDeductionBelgium,"0").ToString("N",DefaultCulture),Number(TaxCreditResearchDevelopment275U_G,"0").ToString("N",DefaultCulture),Number(TaxCreditResearchDevelopment275U_E,"0").ToString("N",DefaultCulture))} , 
                        new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = string.Format("Le montant ({0}€) employée dans la boîte 'Détail des bénéfices',  'Déduction pour investissement' n'est pas égal du montant sous forme 275U 'Déduction pour investissement déductible pour la période imposable', 'Déduction pour investissement pour les sociétés qui n'optent pas pour le crédit d’impôt pour recherche et développement' [code 1437.1] ({1}€) ni 'Déduction pour investissement pour les sociétés qui optent pour le crédit d’impôt pour recherche et développement' [code 1437.2] ({2}€).",Number(AllowanceInvestmentDeductionBelgium,"0").ToString("N",DefaultCulture),Number(TaxCreditResearchDevelopment275U_G,"0").ToString("N",DefaultCulture),Number(TaxCreditResearchDevelopment275U_E,"0").ToString("N",DefaultCulture))} , 
                         new BizTaxErrorMessageDataContract { Culture = "en-US", Message = string.Format("Het bedrag ({0}€) aangewend in het vak 'Uiteenzetting van de winst' onder 'Investeringsaftrek aftrekbaar voor het belastbare tijdperk' is niet gelijk aan het bedrag in formulier 275U, vak 'Investeringsaftrek aftrekbaar voor het belastbare tijdperk' onder 'Investeringsaftrek voor vennootschappen die niet opteren voor het belastingkrediet voor onderzoek en ontwikkeling' [code 1437.1] ({1}€) noch onder 'Investeringsaftrek voor vennootschappen die opteren voor het belastingkrediet voor onderzoek en ontwikkeling' [code 1437.2] ({2}€).",Number(AllowanceInvestmentDeductionBelgium,"0").ToString("N",DefaultCulture),Number(TaxCreditResearchDevelopment275U_G,"0").ToString("N",DefaultCulture),Number(TaxCreditResearchDevelopment275U_E,"0").ToString("N",DefaultCulture))} , 
                         new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = string.Format("Het bedrag ({0}€) aangewend in het vak 'Uiteenzetting van de winst' onder 'Investeringsaftrek aftrekbaar voor het belastbare tijdperk' is niet gelijk aan het bedrag in formulier 275U, vak 'Investeringsaftrek aftrekbaar voor het belastbare tijdperk' onder 'Investeringsaftrek voor vennootschappen die niet opteren voor het belastingkrediet voor onderzoek en ontwikkeling' [code 1437.1] ({1}€) noch onder 'Investeringsaftrek voor vennootschappen die opteren voor het belastingkrediet voor onderzoek en ontwikkeling' [code 1437.2] ({2}€).",Number(AllowanceInvestmentDeductionBelgium,"0").ToString("N",DefaultCulture),Number(TaxCreditResearchDevelopment275U_G,"0").ToString("N",DefaultCulture),Number(TaxCreditResearchDevelopment275U_E,"0").ToString("N",DefaultCulture))} , 
                        
                    }
                    ,
                    Fields = new List<string>()
                }
                );
            }
        }

        internal void Extra_276Ws()
        {
            var elsW1 = GetElementsByScenDefs(new string[] { 
                "d-empf:EmploymentFunctionDimension::d-empf:ResearcherMember/d-empper:EmploymentPeriodDimension::d-empper:EmployedPartTimeMember/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension" ,
                "d-empf:EmploymentFunctionDimension::d-empf:HighlyQualifiedResearcherMember/d-empper:EmploymentPeriodDimension::d-empper:EmployedPartTimeMember/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension"
            }, new string[] { "D" }, new string[] { "tax-inc:ActualExemptionAdditionalPersonnel" });

            var elsW2 = GetElementsByScenDefs(new string[] { 
                "d-empf:EmploymentFunctionDimension::d-empf:IncreaseTechnologicalPotentialPersonnelMember/d-empper:EmploymentPeriodDimension::d-empper:EmployedPartTimeMember/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension"
                }, new string[] { "D" }, new string[] { "tax-inc:ActualExemptionAdditionalPersonnel" });

            var elsW3 = GetElementsByScenDefs(new string[] { 
                "d-empf:EmploymentFunctionDimension::d-empf:DepartmentHeadExportMember/d-empper:EmploymentPeriodDimension::d-empper:EmployedPartTimeMember"
            }, new string[] { "D" }, new string[] { "tax-inc:ActualExemptionAdditionalPersonnel" });

            var elsW4 = GetElementsByScenDefs(new string[] { 
                "d-empf:EmploymentFunctionDimension::d-empf:DepartmentHeadTQMMember/d-empper:EmploymentPeriodDimension::d-empper:EmployedPartTimeMember"
            }, new string[] { "D" }, new string[] { "tax-inc:ActualExemptionAdditionalPersonnel" });


            var ttl = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:ReversalPreviousExemptions" });
            var sumEls = Sum(Number(elsW1,"0"),Number(elsW2,"0"),Number(elsW3,"0"),Number(elsW4,"0"));
            bool test = Number(ttl, "0") == sumEls;

            if (!test)
            {
                AddMessage(new BizTaxErrorDataContract
                {
                    FileId = _fileId,
                    Id = "f-xtra-rcorp-276W1-4",
                    Messages = new List<BizTaxErrorMessageDataContract> { 
                        new BizTaxErrorMessageDataContract { Culture = "nl-BE", 
                            Message = string.Format(
                                "Het bedrag ({0}€) vemeld in het vak 'Verworpen uitgaven' onder 'Terugnemingen van vroegere vrijstellingen' moet gelijk zijn aan de som ({1}€) bekomen uit <ul>"
                                +"<li> 276W1 'In een vorig belastbaar tijdperk aangeworven personeel waarvan de voltijdse tewerkstelling tijdens het belastbare tijdperk is opgehouden', som van veld 'Effectief vrijgesteld' ({2}€)</li>"
                                +"<li> 276W2 'In een vorig belastbaar tijdperk aangeworven personeel waarvan de voltijdse tewerkstelling tijdens het belastbare tijdperk is opgehouden', som van veld 'Effectief vrijgesteld' ({3}€)</li>"
                                +"<li> 276W3 'Vrijstelling voor bijkomend personeel tewerkgesteld als diensthoofd voor de uitvoer', veld 'Effectief vrijgesteld' onder kolom 'Einde voltijds' ({4}€)</li>"
                                +"<li> 276W4 'Vrijstelling voor bijkomend personeel als diensthoofd van de afdeling Integrale kwaliteitszorg', veld 'Effectief vrijgesteld' onder kolom 'Einde voltijds' ({5}€)</li></ul>"
                                    , Number(ttl,"0").ToString("N",DefaultCulture)
                                    , sumEls.ToString("N",DefaultCulture)
                                    , Number(elsW1,"0").ToString("N",DefaultCulture)
                                    , Number(elsW2,"0").ToString("N",DefaultCulture)
                                    , Number(elsW3,"0").ToString("N",DefaultCulture)
                                    , Number(elsW4,"0").ToString("N",DefaultCulture)
                            )
                        },
                        new BizTaxErrorMessageDataContract { Culture = "fr-FR", 
                            Message = string.Format(
                                "Het bedrag ({0}€) vemeld in het vak 'Verworpen uitgaven' onder 'Terugnemingen van vroegere vrijstellingen' moet gelijk zijn aan de som ({1}€) bekomen uit <ul>"
                                +"<li> 276W1 'In een vorig belastbaar tijdperk aangeworven personeel waarvan de voltijdse tewerkstelling tijdens het belastbare tijdperk is opgehouden', som van veld 'Effectief vrijgesteld' ({2}€)</li>"
                                +"<li> 276W2 'In een vorig belastbaar tijdperk aangeworven personeel waarvan de voltijdse tewerkstelling tijdens het belastbare tijdperk is opgehouden', som van veld 'Effectief vrijgesteld' ({3}€)</li>"
                                +"<li> 276W3 'Vrijstelling voor bijkomend personeel tewerkgesteld als diensthoofd voor de uitvoer', veld 'Effectief vrijgesteld' onder kolom 'Einde voltijds' ({4}€)</li>"
                                +"<li> 276W4 'Vrijstelling voor bijkomend personeel als diensthoofd van de afdeling Integrale kwaliteitszorg', veld 'Effectief vrijgesteld' onder kolom 'Einde voltijds' ({5}€)</li></ul>"
                                    , Number(ttl,"0").ToString("N",DefaultCulture)
                                    , sumEls.ToString("N",DefaultCulture)
                                    , Number(elsW1,"0").ToString("N",DefaultCulture)
                                    , Number(elsW2,"0").ToString("N",DefaultCulture)
                                    , Number(elsW3,"0").ToString("N",DefaultCulture)
                                    , Number(elsW4,"0").ToString("N",DefaultCulture)
                            )
                        },
                        new BizTaxErrorMessageDataContract { Culture = "en-US", 
                            Message = string.Format(
                                "Het bedrag ({0}€) vemeld in het vak 'Verworpen uitgaven' onder 'Terugnemingen van vroegere vrijstellingen' moet gelijk zijn aan de som ({1}€) bekomen uit <ul>"
                                +"<li> 276W1 'In een vorig belastbaar tijdperk aangeworven personeel waarvan de voltijdse tewerkstelling tijdens het belastbare tijdperk is opgehouden', som van veld 'Effectief vrijgesteld' ({2}€)</li>"
                                +"<li> 276W2 'In een vorig belastbaar tijdperk aangeworven personeel waarvan de voltijdse tewerkstelling tijdens het belastbare tijdperk is opgehouden', som van veld 'Effectief vrijgesteld' ({3}€)</li>"
                                +"<li> 276W3 'Vrijstelling voor bijkomend personeel tewerkgesteld als diensthoofd voor de uitvoer', veld 'Effectief vrijgesteld' onder kolom 'Einde voltijds' ({4}€)</li>"
                                +"<li> 276W4 'Vrijstelling voor bijkomend personeel als diensthoofd van de afdeling Integrale kwaliteitszorg', veld 'Effectief vrijgesteld' onder kolom 'Einde voltijds' ({5}€)</li></ul>"
                                    , Number(ttl,"0").ToString("N",DefaultCulture)
                                    , sumEls.ToString("N",DefaultCulture)
                                    , Number(elsW1,"0").ToString("N",DefaultCulture)
                                    , Number(elsW2,"0").ToString("N",DefaultCulture)
                                    , Number(elsW3,"0").ToString("N",DefaultCulture)
                                    , Number(elsW4,"0").ToString("N",DefaultCulture)
                            )
                        },
                       new BizTaxErrorMessageDataContract { Culture = "de-DE", 
                            Message = string.Format(
                                "Het bedrag ({0}€) vemeld in het vak 'Verworpen uitgaven' onder 'Terugnemingen van vroegere vrijstellingen' moet gelijk zijn aan de som ({1}€) bekomen uit <ul>"
                                +"<li> 276W1 'In een vorig belastbaar tijdperk aangeworven personeel waarvan de voltijdse tewerkstelling tijdens het belastbare tijdperk is opgehouden', som van veld 'Effectief vrijgesteld' ({2}€)</li>"
                                +"<li> 276W2 'In een vorig belastbaar tijdperk aangeworven personeel waarvan de voltijdse tewerkstelling tijdens het belastbare tijdperk is opgehouden', som van veld 'Effectief vrijgesteld' ({3}€)</li>"
                                +"<li> 276W3 'Vrijstelling voor bijkomend personeel tewerkgesteld als diensthoofd voor de uitvoer', veld 'Effectief vrijgesteld' onder kolom 'Einde voltijds' ({4}€)</li>"
                                +"<li> 276W4 'Vrijstelling voor bijkomend personeel als diensthoofd van de afdeling Integrale kwaliteitszorg', veld 'Effectief vrijgesteld' onder kolom 'Einde voltijds' ({5}€)</li></ul>"
                                    , Number(ttl,"0").ToString("N",DefaultCulture)
                                    , sumEls.ToString("N",DefaultCulture)
                                    , Number(elsW1,"0").ToString("N",DefaultCulture)
                                    , Number(elsW2,"0").ToString("N",DefaultCulture)
                                    , Number(elsW3,"0").ToString("N",DefaultCulture)
                                    , Number(elsW4,"0").ToString("N",DefaultCulture)
                            )
                        }
                    }
                    ,
                    Fields = new List<string>()
                }
                );
            }
        }

        // ActualExemptionAdditionalPersonnel
        // ActualExemptionAdditionalPersonnel
        // ActualExemptionAdditionalPersonnel


        //      d-empf:EmploymentFunctionDimension/d-empper:EmploymentPeriodDimension/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension
        // NOT: d-empf:EmploymentFunctionDimension/d-empper:EmploymentPeriodDimension/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension
        #endregion

        internal void IndexFiches()
        {
            List<BizTaxFicheInfoDataContract> fiches = new List<BizTaxFicheInfoDataContract>
            {
                //new BizTaxFicheInfoDataContract { FicheId="Cover"},
                new BizTaxFicheInfoDataContract { FicheId="Id" },
                new BizTaxFicheInfoDataContract { FicheId="275.1.A" },
                new BizTaxFicheInfoDataContract { FicheId="275.1.B", Annexes =  Xbrl.Elements.Where(x=>x.BinaryValue!=null && x.BinaryValue.Count>0).ToList()}
            };

            if (Xbrl.Elements.Where(x => x.Name == "ExemptWriteDownDebtClaim" && x.GetNumberOrDefault(0) != 0).Count() > 0
                || Xbrl.Elements.Where(x => x.Name == "ExemptProvisionRisksExpenses" && x.GetNumberOrDefault(0) != 0).Count() > 0)
            {
                fiches.Add(new BizTaxFicheInfoDataContract { FicheId = "204.3" });
            }   

            List<string> scenarioIds = GetScenarioIdsFuzzy(new string[] { "d-asst:AssetTypeDimension::d-asst:SeaVesselMember" });
            if (Xbrl.Elements.Count(x => scenarioIds.Contains(x.Context) && !string.IsNullOrEmpty(x.Value)) > 0)
            {
                fiches.Add(new BizTaxFicheInfoDataContract { FicheId = "275B" });
            }

            //D__id__SpecificSecurityMember__id__2010-01
            //D__id__SpecificSecurityMember__id__2013-02__id__2010-01
            scenarioIds = GetScenarioIdsFuzzy(new string[] { "d-asst:AssetTypeDimension::d-asst:SpecificSecurityMember" });
            if (Xbrl.Elements.Count(x => scenarioIds.Contains(x.Context) && !string.IsNullOrEmpty(x.Value)) > 0)
            {
                fiches.Add(new BizTaxFicheInfoDataContract { FicheId = "275K" });
            }
            scenarioIds = GetScenarioIdsFuzzy(new string[] { "d-asst:AssetTypeDimension::d-asst:TangibleIntangibleFixedAssetsMember" });
            if (Xbrl.Elements.Count(x => scenarioIds.Contains(x.Context) && !string.IsNullOrEmpty(x.Value)) > 0)
            {
                fiches.Add(new BizTaxFicheInfoDataContract { FicheId = "276K" });
            }

            scenarioIds = GetScenarioIdsFuzzy(new string[] { "d-asst:AssetTypeDimension::d-asst:CorporateVehicleMember" });
            if (Xbrl.Elements.Count(x => scenarioIds.Contains(x.Context) && !string.IsNullOrEmpty(x.Value)) > 0)
            {
                fiches.Add(new BizTaxFicheInfoDataContract { FicheId = "276N" });
            }

            scenarioIds = GetScenarioIdsFuzzy(new string[] { "d-asst:AssetTypeDimension::d-asst:RiverVesselMember" });
            if (Xbrl.Elements.Count(x => scenarioIds.Contains(x.Context) && !string.IsNullOrEmpty(x.Value)) > 0)
            {
                fiches.Add(new BizTaxFicheInfoDataContract { FicheId = "276P" });
            }

            if (Xbrl.Elements.Where(x => x.Name == "AllowanceCorporateEquityCurrentTaxPeriod" && x.GetNumberOrDefault(0) != 0).Count() > 0
                || Xbrl.Elements.Where(x => x.Name.StartsWith("ExemptionsAllowanceCorporateEquityAccumulatedUpToAndIncludingAssessmentYear") && x.GetNumberOrDefault(0) != 0).Count() > 0)
            {
                fiches.Add(new BizTaxFicheInfoDataContract { FicheId = "275C" });
            }

            if (Xbrl.Elements.Where(x => x.Name == "PaymentCertainState" && x.GetNumberOrDefault(0) != 0).Count() > 0)
            {
                fiches.Add(new BizTaxFicheInfoDataContract { FicheId = "275F" });
            }

            if (Xbrl.Elements.Where(x => x.Name == "CalculationBasisDeductionPatentsIncome" && x.GetNumberOrDefault(0) != 0).Count() > 0)
            {
                fiches.Add(new BizTaxFicheInfoDataContract { FicheId = "275P" });
            }


            if (Xbrl.Elements.Where(x => x.Name == "CorrectedIncreaseTaxableReservesInvestmentReserveExcluded" && x.GetNumberOrDefault(0) != 0).Count() > 0
               || Xbrl.Elements.Where(x => x.Name =="ExemptInvestmentReserveOverview" && x.GetNumberOrDefault(0) != 0).Count() > 0
                || Xbrl.Elements.Where(x => x.Name == "InvestBalance" && x.GetNumberOrDefault(0) != 0).Count() > 0)
            {
                fiches.Add(new BizTaxFicheInfoDataContract { FicheId = "275R" });
            }

            // d-incc:IncentiveCategoryDimension/d-invc:InvestmentCategoryDimension (?)
            if (Xbrl.Elements.Where(x => x.Name == "DeductibleAllowanceInvestmentDeductionIncompatibleTaxCreditResearchDevelopment" && x.GetNumberOrDefault(0) != 0).Count() > 0
               || Xbrl.Elements.Where(x => x.Name == "CarryOverInvestmentDeduction" && x.GetNumberOrDefault(0) != 0).Count() > 0
               || Xbrl.Elements.Where(x => x.Name == "DeductibleAllowanceInvestmentDeductionCompatibleTaxCreditResearchDevelopment" && x.GetNumberOrDefault(0) != 0).Count() > 0
               || Xbrl.Elements.Where(x => x.Name == "CarryOverInvestmentDeductionCompatibleTaxCreditResearchDevelopment" && x.GetNumberOrDefault(0) != 0).Count() > 0)
            {
                fiches.Add(new BizTaxFicheInfoDataContract { FicheId = "275U" });
            }


            if (Xbrl.Elements.Where(x => x.Name == "TaxCreditResearchDevelopment" && x.GetNumberOrDefault(0) != 0).Count() > 0
              || Xbrl.Elements.Where(x => x.Name == "ClearableTaxCreditResearchDevelopmentCurrentTaxPeriod" && x.GetNumberOrDefault(0) != 0).Count() > 0
              )
            {
                fiches.Add(new BizTaxFicheInfoDataContract { FicheId = "275W" });
            }
            //
            //
            //d-empreg:EmploymentRegimeDimension/d-empstat:EmploymentStatusDimension/d-per:PeriodDimension/d-tu:TimeUnitDimension/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension


            List<string> crefs = Xbrl.Contexts.Where(c => c.ScenarioDef=="d-empreg:EmploymentRegimeDimension/d-empstat:EmploymentStatusDimension/d-per:PeriodDimension/d-tu:TimeUnitDimension/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension").Select(c => c.Id).ToList();
            if (Xbrl.Elements.Count(x => crefs.Contains(x.ContextRef) && x.GetNumberOrDefault(0) != 0)>0)
            {
                fiches.Add(new BizTaxFicheInfoDataContract { FicheId = "276T" });
            }

            scenarioIds = GetScenarioIds(new string[] { 
                "d-empf:EmploymentFunctionDimension::d-empf:HighlyQualifiedResearcherMember/d-empper:EmploymentPeriodDimension::d-empper:EmployedFullTimeMember/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension",
                "d-empf:EmploymentFunctionDimension::d-empf:HighlyQualifiedResearcherMember/d-empper:EmploymentPeriodDimension::d-empper:EmployedPartTimeMember/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension",
                "d-empf:EmploymentFunctionDimension::d-empf:ResearcherMember/d-empper:EmploymentPeriodDimension::d-empper:EmployedPartTimeMember/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension",
                "d-empf:EmploymentFunctionDimension::d-empf:ResearcherMember/d-empper:EmploymentPeriodDimension::d-empper:EmployedFullTimeMember/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension"
            });
            if (Xbrl.Elements.Count(x => scenarioIds.Contains(x.Context) && !string.IsNullOrEmpty(x.Value)) > 0)
            {
                fiches.Add(new BizTaxFicheInfoDataContract { FicheId = "276W1" });
            }

            scenarioIds = GetScenarioIds(new string[] { 
                "d-empf:EmploymentFunctionDimension::d-empf:IncreaseTechnologicalPotentialPersonnelMember/d-empper:EmploymentPeriodDimension::d-empper:EmployedFullTimeMember/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension",
                "d-empf:EmploymentFunctionDimension::d-empf:IncreaseTechnologicalPotentialPersonnelMember/d-empper:EmploymentPeriodDimension::d-empper:EmployedPartTimeMember/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension"
            });
            if (Xbrl.Elements.Count(x => scenarioIds.Contains(x.Context) && !string.IsNullOrEmpty(x.Value)) > 0)
            {
                fiches.Add(new BizTaxFicheInfoDataContract { FicheId = "276W2" });
            }

            scenarioIds = GetScenarioIds(new string[] { 
                "d-empf:EmploymentFunctionDimension::d-empf:DepartmentHeadExportMember/d-empper:EmploymentPeriodDimension::d-empper:RecruitmentFullTimeMember",
                "d-empf:EmploymentFunctionDimension::d-empf:DepartmentHeadExportMember/d-empper:EmploymentPeriodDimension::d-empper:EmployedFullTimeTransferTaxPeriodMember",
                "d-empf:EmploymentFunctionDimension::d-empf:DepartmentHeadExportMember/d-empper:EmploymentPeriodDimension::d-empper:RecruitmentFullTimeReplacementMember",
                "d-empf:EmploymentFunctionDimension::d-empf:DepartmentHeadExportMember/d-empper:EmploymentPeriodDimension::d-empper:EmployedFullTimeMember",
                "d-empf:EmploymentFunctionDimension::d-empf:DepartmentHeadExportMember/d-empper:EmploymentPeriodDimension::d-empper:EmployedPartTimeMember"
            });
            if (Xbrl.Elements.Count(x => scenarioIds.Contains(x.Context) && !string.IsNullOrEmpty(x.Value)) > 0)
            {
                fiches.Add(new BizTaxFicheInfoDataContract { FicheId = "276W3" });
            }

            scenarioIds = GetScenarioIds(new string[] { 
                "d-empf:EmploymentFunctionDimension::d-empf:DepartmentHeadTQMMember/d-empper:EmploymentPeriodDimension::d-empper:RecruitmentFullTimeMember",
                "d-empf:EmploymentFunctionDimension::d-empf:DepartmentHeadTQMMember/d-empper:EmploymentPeriodDimension::d-empper:EmployedFullTimeTransferTaxPeriodMember",
                "d-empf:EmploymentFunctionDimension::d-empf:DepartmentHeadTQMMember/d-empper:EmploymentPeriodDimension::d-empper:RecruitmentFullTimeReplacementMember",
                "d-empf:EmploymentFunctionDimension::d-empf:DepartmentHeadTQMMember/d-empper:EmploymentPeriodDimension::d-empper:EmployedFullTimeMember",
                "d-empf:EmploymentFunctionDimension::d-empf:DepartmentHeadTQMMember/d-empper:EmploymentPeriodDimension::d-empper:EmployedPartTimeMember"
            });
            if (Xbrl.Elements.Count(x => scenarioIds.Contains(x.Context) && !string.IsNullOrEmpty(x.Value)) > 0)
            {
                fiches.Add(new BizTaxFicheInfoDataContract { FicheId = "276W3" });
            }
         
            scenarioIds = GetScenarioIds(new string[] { 
                "d-depm:DepreciationMethodDimension::d-depm:DegressiveDepreciationMethodMember/d-ty:DescriptionTypedDimension/d-ty:NatureTypedDimension"
            });
            if (Xbrl.Elements.Count(x => scenarioIds.Contains(x.Context) && !string.IsNullOrEmpty(x.Value)) > 0)
            {
                fiches.Add(new BizTaxFicheInfoDataContract { FicheId = "328K" });
            }

            scenarioIds = GetScenarioIds(new string[] { 
                "d-depm:DepreciationMethodDimension::d-depm:RenunciationDegressiveDepreciationMethodMember/d-ty:DescriptionTypedDimension/d-ty:NatureTypedDimension"
            });
            if (Xbrl.Elements.Count(x => scenarioIds.Contains(x.Context) && !string.IsNullOrEmpty(x.Value)) > 0)
            {
                fiches.Add(new BizTaxFicheInfoDataContract { FicheId = "328L" });
            }


            this.Xbrl.Fiches = fiches;
        }


        private void _ApplyFieldsToCalc(ref TaxCalc.TaxCalc tx)
        {

            
            tx.BerekeningOverzicht.AssessmentYear = 2014;

            /* TARIEVEN */
            #region TARIEVEN


            tx.BerekeningOverzicht.CapitalGainsShares0412Perc = (decimal)0.412;
            tx.BerekeningOverzicht.CapitalGainsShares25Perc = (decimal)25.75;
            tx.BerekeningOverzicht.ExitTarifPerc = (decimal)16.995;

            tx.FairnessTax.FairnessTaxPerc = (decimal)5.15;

            tx.BerekeningOverzicht.EenduidigTariefPerc = (decimal)33.99;

            //If CreditCorporationTradeEquipmentHousingCorporationTaxRate (1752) is checked, apply lower tax rate
            tx.BerekeningOverzicht.CreditCorporationTradeEquipmentHousingCorporationTaxRate = this.Bool(this.Xbrl.Elements.FirstOrDefault(x => x.Period == "D" && x.Name == "CreditCorporationTradeEquipmentHousingCorporationTaxRate"));

            if (tx.BerekeningOverzicht.CreditCorporationTradeEquipmentHousingCorporationTaxRate.GetValueOrDefault())
            {
                tx.BerekeningOverzicht.EenduidigTariefPerc = (decimal)5.15;
            }

            tx.BerekeningOverzicht.VerminderdtariefStap1Base = (decimal)25000;

            tx.BerekeningOverzicht.VerminderdtariefStap2Base = (decimal)90000;

            tx.BerekeningOverzicht.VerminderdtariefStap3Base = (decimal)322500;

            tx.BerekeningOverzicht.VerminderdtariefStap1Perc = (decimal)24.9775;

            tx.BerekeningOverzicht.VerminderdtariefStap2Perc = (decimal)31.93;

            tx.BerekeningOverzicht.VerminderdtariefStap3Perc = (decimal)35.535;

            tx.BerekeningOverzicht.AP1Percentage = (decimal)3;
            
            tx.BerekeningOverzicht.AP2Percentage = (decimal)2.5;
            
            tx.BerekeningOverzicht.AP3Percentage = (decimal)2;
            
            tx.BerekeningOverzicht.AP4Percentage = (decimal)1.5;

            tx.BerekeningOverzicht.BaseForIncreasePerc = (tx.BerekeningOverzicht.AP1Percentage.GetValueOrDefault()
                + tx.BerekeningOverzicht.AP2Percentage.GetValueOrDefault() + tx.BerekeningOverzicht.AP3Percentage.GetValueOrDefault()
                + tx.BerekeningOverzicht.AP4Percentage.GetValueOrDefault()) / 4;


            tx.BerekeningOverzicht.KapitaalEnInterestsubsidiesPerc = (decimal)5;

            tx.BerekeningOverzicht.UnjustifiedExpensesHiddenSurplusPerc = (decimal)309.00;

            tx.BerekeningOverzicht.SeparateAssessmentTaxedReserves3400Perc = (decimal)35.02;

            tx.BerekeningOverzicht.SeparateAssessmentTaxedReserves2800Perc = (decimal)28.84;


            tx.BerekeningOverzicht.SeparateAssessmentDividendsPaidPerc = (decimal)28.84;

            tx.BerekeningOverzicht.SeparateAssessmentTaxPaidTaxedReservesPerc = (decimal)15.00;

            tx.BerekeningOverzicht.DistributionCompanyAssets3300Perc = (decimal)33.00;

            tx.BerekeningOverzicht.DistributionCompanyAssets1650Perc = (decimal)16.50;

            tx.BerekeningOverzicht.BenefitsAllKindCompanyLiquidationPerc = (decimal)33.00;

            tx.BerekeningOverzicht.AdditionalDutiesDiamondTradersPerc = (decimal)10.00;

            tx.BerekeningOverzicht.RetributionResearchDevelopmentPerc = (decimal)100;

            #endregion


            /* IMPORT */
            tx.BerekeningOverzicht.StatAccountingResult =  this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Period == "I-End" && x.Name == "LegalReserve"))
                - this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Period == "I-Start" && x.Name == "LegalReserve"))
                + this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Period == "I-End" && x.Name == "UnavailableReserves"))
                - this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Period == "I-Start" && x.Name == "UnavailableReserves"))
                + this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Period == "I-End" && x.Name == "AvailableReserves"))
                - this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Period == "I-Start" && x.Name == "AvailableReserves"))
                 + this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Period == "I-End" && x.Name == "AccumulatedProfitsLosses"))
                - this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Period == "I-Start" && x.Name == "AccumulatedProfitsLosses"))
                + this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "TaxableDividendsPaid"));


            tx.BelasteReserves.TDKapitaalEnUitgifte = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Period == "I-End" && x.Name == "TaxableReservesCapitalSharePremiums"))
              - this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Period == "I-Start" && x.Name == "TaxableReservesCapitalSharePremiums"));
            tx.BelasteReserves.TDHerwaardering = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Period == "I-End" && x.Name == "TaxablePortionRevaluationSurpluses"))
              - this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Period == "I-Start" && x.Name == "TaxablePortionRevaluationSurpluses"));
            tx.BelasteReserves.TDTaxableProvisions = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Period == "I-End" && x.Name == "TaxableProvisions"))
              - this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Period == "I-Start" && x.Name == "TaxableProvisions"));

            tx.BelasteReserves.TDOtherTaxableReserves = this.Number(this.Xbrl.Elements.Where(x => x.Period == "I-End" && x.Name == "OtherReserves"), "0")
             - this.Number(this.Xbrl.Elements.Where(x => x.Period == "I-Start" && x.Name == "OtherReserves"), "0");

            tx.BelasteReserves.TDOtherNonBalanceTaxableReserves = this.Number(this.Xbrl.Elements.Where(x => x.Period == "I-End" && x.Name == "OtherTaxableReserves"), "0")
             - this.Number(this.Xbrl.Elements.Where(x => x.Period == "I-Start" && x.Name == "OtherTaxableReserves"), "0");

            tx.BelasteReserves.TDTaxableDoubtfullDebtors = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Period == "I-End" && x.Name == "TaxableWriteDownsUndisclosedReserve"))
              - this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Period == "I-Start" && x.Name == "TaxableWriteDownsUndisclosedReserve"));

            tx.BelasteReserves.TDExcessDeprications = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Period == "I-End" && x.Name == "ExaggeratedDepreciationsUndisclosedReserve"))
              - this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Period == "I-Start" && x.Name == "ExaggeratedDepreciationsUndisclosedReserve"));

            tx.BelasteReserves.TDUnderestimationAssetsOverestimationLiabilities = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Period == "I-End" && x.Name == "OtherUnderestimationsAssetsUndisclosedReserve"))
              - this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Period == "I-Start" && x.Name == "OtherUnderestimationsAssetsUndisclosedReserve"));

            tx.BelasteReserves.TDOverestimationLiabilities = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Period == "I-End" && x.Name == "OtherOverestimationsLiabilitiesUndisclosedReserve"))
              - this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Period == "I-Start" && x.Name == "OtherOverestimationsLiabilitiesUndisclosedReserve"));

            tx.BelasteReserves.TDTotal = tx.BelasteReserves.TDTaxableProvisions.GetValueOrDefault()
                                              + tx.BelasteReserves.TDOtherTaxableReserves.GetValueOrDefault()
                                              + tx.BelasteReserves.TDOtherNonBalanceTaxableReserves.GetValueOrDefault()
                                              + tx.BelasteReserves.TDTaxableDoubtfullDebtors.GetValueOrDefault()
                                              + tx.BelasteReserves.TDExcessDeprications.GetValueOrDefault()
                                              + tx.BelasteReserves.TDUnderestimationAssetsOverestimationLiabilities.GetValueOrDefault()
                                              + tx.BelasteReserves.TDKapitaalEnUitgifte.GetValueOrDefault()
                                              + tx.BelasteReserves.TDOverestimationLiabilities.GetValueOrDefault()
                                              + tx.BelasteReserves.TDHerwaardering.GetValueOrDefault();


            tx.BelasteReserves.OACapitalGainsOnShares = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "CapitalGainsShares"));
            //ONTBREEKT IN BEL BEREK:: Terugnemingen van vroegere in verworpen uitgaven opgenomen waardeverminderingen op aandelen > 1052 > CapitalGainsSharesReversalPreviousExemptionsAdjustmentReservesPlus
            tx.BelasteReserves.OACapitalGainsSharesReversal = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "CapitalGainsSharesReversalPreviousExemptionsAdjustmentReservesPlus"));

            tx.BelasteReserves.OADefinitiveExemptionTaxShelter = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "DefinitiveExemptionTaxShelterAuthorisedAudiovisualWorksAdjustmentReservesPlus"));

            tx.BelasteReserves.OAExemptionRegionalPrem = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "ExemptionRegionalGrantsCapitalSubsidiesInterestSubsidiesAdjustmentReservesPlus"));
            //ONTBREEKT IN BEL BEREK:: Definitieve vijstelling winst voortvloeiend uit de homologatie van een reorganisatieplan > 1055 > FinalExemptionProfitHomologationReorganizationPlanAmicableSettlement

            tx.BelasteReserves.OAFinalExemptionProfit = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "FinalExemptionProfitHomologationReorganizationPlanAmicableSettlement"));


            tx.BelasteReserves.OAOthers = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "OtherAdjustmentsReservesPlus"));

            tx.BelasteReserves.OAInTheMin = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "AdjustmentsReservesMinus"));

            tx.BelasteReserves.OATotal = tx.BelasteReserves.OACapitalGainsOnShares.GetValueOrDefault()
                                      + tx.BelasteReserves.OADefinitiveExemptionTaxShelter.GetValueOrDefault()
                                      + tx.BelasteReserves.OAExemptionRegionalPrem.GetValueOrDefault()
                                      + tx.BelasteReserves.OAOthers.GetValueOrDefault()
                                      + tx.BelasteReserves.OACapitalGainsSharesReversal.GetValueOrDefault()
                                      + tx.BelasteReserves.OAFinalExemptionProfit.GetValueOrDefault();

            if (tx.BelasteReserves.OATotal.GetValueOrDefault() == 0 && tx.BelasteReserves.OAInTheMin.GetValueOrDefault() > 0)
            {
                tx.BelasteReserves.OATotal = tx.BelasteReserves.OATotal.GetValueOrDefault() - tx.BelasteReserves.OAInTheMin.GetValueOrDefault();
            }
            else
            {
                tx.BelasteReserves.OAInTheMin = 0;
            }



            tx.VerworpenUitgaven.PDDisallowedCurrentIncomeTaxExpenses = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "NonDeductibleTaxes"));

            tx.VerworpenUitgaven.DERegionalTaxes = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "NonDeductibleRegionalTaxesDutiesRetributions"));
            tx.VerworpenUitgaven.DEPenalties = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "NonDeductibleFinesConfiscationsPenaltiesAllKind"));
            tx.VerworpenUitgaven.DEPensions = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "NonDeductiblePensionsCapitalEmployerContributionsEmployerPremiums"));
            tx.VerworpenUitgaven.DECar = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "NonDeductibleCarExpensesLossValuesCars"));
            tx.VerworpenUitgaven.DECarVAA = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "NonDeductibleCarExpensesPartBenefitsAllKind"));
            tx.VerworpenUitgaven.DEReception = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "NonDeductibleReceptionBusinessGiftsExpenses"));
            tx.VerworpenUitgaven.DERestaurant = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "NonDeductibleRestaurantExpenses"));
            tx.VerworpenUitgaven.DEClothes = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "NonDeductibleNonSpecificProfessionalClothsExpenses"));
            tx.VerworpenUitgaven.DEExcessInterest = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "ExaggeratedInterests"));
            tx.VerworpenUitgaven.DEInterestLoans = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "NonDeductibleParticularPortionInterestsLoans"));
            tx.VerworpenUitgaven.DEAbnormalAdvantages = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "AbnormalBenevolentAdvantages"));
            tx.VerworpenUitgaven.DESocial = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "NonDeductibleSocialAdvantages"));
            tx.VerworpenUitgaven.DEDinerSportCultureEco = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "NonDeductibleAdvantagesLuncheonSportsCultureEcoVouchers"));
            tx.VerworpenUitgaven.DECharity = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "Liberalities"));
            tx.VerworpenUitgaven.DEReductionValueShares = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "WriteDownsLossValuesShares"));
            tx.VerworpenUitgaven.DEReversalsPreviousTax = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "ReversalPreviousExemptions"));
            tx.VerworpenUitgaven.DEEmployeeCapital = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "EmployeeParticipation"));
            tx.VerworpenUitgaven.DEIndemnityMissingCoupon = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "IndemnityMissingCoupon"));
            tx.VerworpenUitgaven.DEExpensesTaxShelter = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "ExpensesTaxShelterAuthorisedAudiovisualWork"));
            tx.VerworpenUitgaven.DERegionalPremiumsCapital = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "RegionalPremiumCapitalSubsidiesInterestSubsidies"));
            tx.VerworpenUitgaven.DEBepaaldeStaten = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "NonDeductiblePaymentsCertainStates"));
            tx.VerworpenUitgaven.DEOther = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "OtherDisallowedExpenses"));
            tx.VerworpenUitgaven.DEUnjustifiedExpenses = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "UnjustifiedExpensesBenefitsAllKindIncludedInAssessmentInNameOfBeneficiary"));


            tx.VerworpenUitgaven.DETotal = tx.VerworpenUitgaven.DEAbnormalAdvantages.GetValueOrDefault()
                                              + tx.VerworpenUitgaven.DEBepaaldeStaten.GetValueOrDefault()
                                              + tx.VerworpenUitgaven.DECar.GetValueOrDefault()
                                              + tx.VerworpenUitgaven.DECarVAA.GetValueOrDefault()
                                              + tx.VerworpenUitgaven.DECharity.GetValueOrDefault()
                                              + tx.VerworpenUitgaven.DEClothes.GetValueOrDefault()
                                              + tx.VerworpenUitgaven.DEDinerSportCultureEco.GetValueOrDefault()
                                              + tx.VerworpenUitgaven.DEEmployeeCapital.GetValueOrDefault()
                                              + tx.VerworpenUitgaven.DEExcessInterest.GetValueOrDefault()
                                              + tx.VerworpenUitgaven.DEExpensesTaxShelter.GetValueOrDefault()
                                              + tx.VerworpenUitgaven.DEIndemnityMissingCoupon.GetValueOrDefault()
                                              + tx.VerworpenUitgaven.DEInterestLoans.GetValueOrDefault()
                                              + tx.VerworpenUitgaven.DEOther.GetValueOrDefault()
                                              + tx.VerworpenUitgaven.DEUnjustifiedExpenses.GetValueOrDefault()
                                              + tx.VerworpenUitgaven.DEPenalties.GetValueOrDefault()
                                              + tx.VerworpenUitgaven.DEPensions.GetValueOrDefault()
                                              + tx.VerworpenUitgaven.DEReception.GetValueOrDefault()
                                              + tx.VerworpenUitgaven.DEReductionValueShares.GetValueOrDefault()
                                              + tx.VerworpenUitgaven.DERegionalPremiumsCapital.GetValueOrDefault()
                                              + tx.VerworpenUitgaven.DERegionalTaxes.GetValueOrDefault()
                                              + tx.VerworpenUitgaven.DERestaurant.GetValueOrDefault()
                                              + tx.VerworpenUitgaven.DEReversalsPreviousTax.GetValueOrDefault()
                                              + tx.VerworpenUitgaven.DESocial.GetValueOrDefault();


            //
            tx.BelastbareBestZonderAftrek.TECapitalGains0412 = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "CapitalGainsSharesRate0040"));
            tx.BelastbareBestZonderAftrek.TEReceivedAbnormal = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "BenevolentAbnormalFinancialAdvantagesBenefitsAllKind"));
            tx.BelastbareBestZonderAftrek.TETaxableInvestmentReserve = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "ViolationInvestmentCommitmentIntangibilityConditionInvestmentReserve"));
            tx.BelastbareBestZonderAftrek.TEVaaCar = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "NonDeductibleCarExpensesPartBenefitsAllKind"));

            tx.BelastbareBestZonderAftrek.TEEmployeeParticipationCapital = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "EmployeeParticipation"));
            tx.BelastbareBestZonderAftrek.TECapitalAgriCulture = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "CapitalSubsidiesInterestSubsidiesAgriculturalSupport"));

            tx.BelastbareBestZonderAftrek.TETotal = tx.BelastbareBestZonderAftrek.TECapitalGains0412.GetValueOrDefault() + tx.BelastbareBestZonderAftrek.TEReceivedAbnormal.GetValueOrDefault() + tx.BelastbareBestZonderAftrek.TETaxableInvestmentReserve.GetValueOrDefault()
              + tx.BelastbareBestZonderAftrek.TEEmployeeParticipationCapital.GetValueOrDefault() + tx.BelastbareBestZonderAftrek.TECapitalAgriCulture.GetValueOrDefault() + tx.BelastbareBestZonderAftrek.TEVaaCar.GetValueOrDefault();


            // OpdelingNaarOorsprong
            tx.OpdelingNaarOorsprong.IRTreatyForeignIncome = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "RemainingFiscalResult" && x.ContextRef == "D__id__TaxTreatyMember"));
            tx.OpdelingNaarOorsprong.IRNonTreatyForeignIncome = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "RemainingFiscalResult" && x.ContextRef == "D__id__NoTaxTreatyMember"));
            tx.OpdelingNaarOorsprong.IRBelgianTaxable = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "RemainingFiscalResult" && x.ContextRef == "D__id__BelgiumMember"));
          

            tx.FiscaleAftrek.NTEBECharity = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "ExemptGifts"));
            tx.FiscaleAftrek.NTEBEPersonnel = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "ExemptionAdditionalPersonnelMiscellaneousExemptions"));
            tx.FiscaleAftrek.NTEBEPersonnelSME = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "ExemptionAdditionalPersonnelSMEs"));
            tx.FiscaleAftrek.NTEBEInternalshipPremium = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "ExemptionTrainingPeriodBonus"));
            tx.FiscaleAftrek.NTEBEOthers = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "OtherMiscellaneousExemptions"));


            tx.FiscaleAftrek.TotalNonTaxableElements = (((tx.FiscaleAftrek.NTEBECharity.GetValueOrDefault() + tx.FiscaleAftrek.NTEBEPersonnel.GetValueOrDefault())
                                                    + tx.FiscaleAftrek.NTEBEPersonnelSME.GetValueOrDefault()) + tx.FiscaleAftrek.NTEBEInternalshipPremium.GetValueOrDefault())
                                                    + tx.FiscaleAftrek.NTEBEOthers.GetValueOrDefault();

            /* #165
            tx.FiscaleAftrek.PEBelgium = this.Number(this.Xbrl.Elements.Where(x => x.Name == "PEExemptIncomeMovableAssets" && x.ContextRef != "D"), "0");
            tx.FiscaleAftrek.PIDBelgium = this.Number(this.Xbrl.Elements.Where(x => x.Name == "DeductionPatentsIncome" && x.ContextRef != "D"), "0");
            tx.FiscaleAftrek.NIDBelgium = this.Number(this.Xbrl.Elements.Where(x => x.Name == "AllowanceCorporateEquity" && x.ContextRef!="D"), "0");
            tx.FiscaleAftrek.DTLBelgium = this.Number(this.Xbrl.Elements.Where(x => x.Name == "CompensatedTaxLosses" && x.ContextRef != "D"), "0");
            tx.FiscaleAftrek.IDBelgium = this.Number(this.Xbrl.Elements.Where(x => x.Name == "AllowanceInvestmentDeduction" && x.ContextRef!="D"), "0");
            tx.FiscaleAftrek.NIDHistoryBelgium = this.Number(this.Xbrl.Elements.Where(x => x.Name == "DeductionExemptionsAllowanceCorporateEquityAccumulatedUpToAndIncludingAssessmentYear2012" && x.ContextRef!="D"), "0");
            */

            tx.FiscaleAftrek.PEBelgium = this.Number(this.Xbrl.Elements.Where(x => x.Name == "PEExemptIncomeMovableAssets" && x.ContextRef != "D"), "0") != 0 ? this.Number(this.Xbrl.Elements.Where(x => x.Name == "PEExemptIncomeMovableAssets" && x.ContextRef != "D"), "0") : this.Number(this.Xbrl.Elements.Where(x => x.Name == "PEExemptIncomeMovableAssets" && x.ContextRef == "D"), "0");
            tx.FiscaleAftrek.PIDBelgium = this.Number(this.Xbrl.Elements.Where(x => x.Name == "DeductionPatentsIncome" && x.ContextRef != "D"), "0") != 0 ? this.Number(this.Xbrl.Elements.Where(x => x.Name == "DeductionPatentsIncome" && x.ContextRef != "D"), "0") : this.Number(this.Xbrl.Elements.Where(x => x.Name == "DeductionPatentsIncome" && x.ContextRef == "D"), "0");
            tx.FiscaleAftrek.NIDBelgium = this.Number(this.Xbrl.Elements.Where(x => x.Name == "AllowanceCorporateEquity" && x.ContextRef != "D"), "0") != 0 ? this.Number(this.Xbrl.Elements.Where(x => x.Name == "AllowanceCorporateEquity" && x.ContextRef != "D"), "0") : this.Number(this.Xbrl.Elements.Where(x => x.Name == "AllowanceCorporateEquity" && x.ContextRef == "D"), "0");
            tx.FiscaleAftrek.DTLBelgium = this.Number(this.Xbrl.Elements.Where(x => x.Name == "CompensatedTaxLosses" && x.ContextRef != "D"), "0") != 0 ? this.Number(this.Xbrl.Elements.Where(x => x.Name == "CompensatedTaxLosses" && x.ContextRef != "D"), "0") : this.Number(this.Xbrl.Elements.Where(x => x.Name == "CompensatedTaxLosses" && x.ContextRef == "D"), "0");
            tx.FiscaleAftrek.IDBelgium = this.Number(this.Xbrl.Elements.Where(x => x.Name == "AllowanceInvestmentDeduction" && x.ContextRef != "D"), "0") != 0 ? this.Number(this.Xbrl.Elements.Where(x => x.Name == "AllowanceInvestmentDeduction" && x.ContextRef != "D"), "0") : this.Number(this.Xbrl.Elements.Where(x => x.Name == "AllowanceInvestmentDeduction" && x.ContextRef == "D"), "0");
            tx.FiscaleAftrek.NIDHistoryBelgium = this.Number(this.Xbrl.Elements.Where(x => x.Name == "DeductionExemptionsAllowanceCorporateEquityAccumulatedUpToAndIncludingAssessmentYear2012" && x.ContextRef != "D"), "0") != 0 ? this.Number(this.Xbrl.Elements.Where(x => x.Name == "DeductionExemptionsAllowanceCorporateEquityAccumulatedUpToAndIncludingAssessmentYear2012" && x.ContextRef != "D"), "0") : this.Number(this.Xbrl.Elements.Where(x => x.Name == "DeductionExemptionsAllowanceCorporateEquityAccumulatedUpToAndIncludingAssessmentYear2012" && x.ContextRef == "D"), "0");

            tx.FiscaleAftrek.TotalNonTaxableElements = (((tx.FiscaleAftrek.NTEBECharity.GetValueOrDefault() + tx.FiscaleAftrek.NTEBEPersonnel.GetValueOrDefault())
                                                 + tx.FiscaleAftrek.NTEBEPersonnelSME.GetValueOrDefault()) + tx.FiscaleAftrek.NTEBEInternalshipPremium.GetValueOrDefault())
                                                 + tx.FiscaleAftrek.NTEBEOthers.GetValueOrDefault();
            tx.FiscaleAftrek.TotalFiscaleAftrek = ((((tx.FiscaleAftrek.TotalNonTaxableElements + tx.FiscaleAftrek.PEBelgium) + tx.FiscaleAftrek.PIDBelgium) + tx.FiscaleAftrek.NIDBelgium) + tx.FiscaleAftrek.DTLBelgium) + tx.FiscaleAftrek.IDBelgium + tx.FiscaleAftrek.NIDHistoryBelgium.GetValueOrDefault();

            /*
             * MISSING DETAILS IN BEREK.
             * 
              Winst uit zeescheepvaart, vastgesteld op basis van de tonnage		1461
              Verkregen abnormale of goedgunstige voordelen en verkregen financiële voordelen van alle aard		1421
              Niet-naleving investeringsverplichting of onaantastbaarheidsvoorwaarde voor de investeringsreserve		1422
              Autokosten ten belope van een gedeelte van het voordeel van alle aard		1206
              Werknemersparticipatie		1219

             * 
             */

            // Afzonderlijke aanslagen

            tx.VerrekenbareVoorheffing.VVNTFictieveRoerendeVoorheffing = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "NonRepayableFictiousWitholdingTax"));
            tx.VerrekenbareVoorheffing.VVNTForfaitairBuitenlands = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "NonRepayableLumpSumForeignTaxes"));
            tx.VerrekenbareVoorheffing.VVNTBelastingKredietOnderzoekOntw = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "TaxCreditResearchDevelopment"));


            tx.VerrekenbareVoorheffing.VVTBelgisch = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "RepayableRealFictitiousWithholdingTaxOtherSharesPEExemptIncomeMovableAssetsBelgium"));
            tx.VerrekenbareVoorheffing.VVTAndereBuitenlands = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "RepayableWithholdingTaxPEExemptionLiquidationBonusesBonusesAcquisitionOwnSharesBelgiumForeign"));
            tx.VerrekenbareVoorheffing.VVTEigenBuitenlands = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "RepayableWithholdingTaxOtherPEForeign"));
            tx.VerrekenbareVoorheffing.VVTAndereEigen = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "RepayableWithholdingTaxOtherLiquidationBonusesBonusesAcquisitionOwnShares"));
            tx.VerrekenbareVoorheffing.VVTAndereRoerendeVoorheffing = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "RepayableWithholdingTaxOtherDividends"));
            tx.VerrekenbareVoorheffing.VVTAndere = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "OtherRepayableWithholdingTaxes"));

            tx.VerrekenbareVoorheffing.VBelastingkredietHuiidTijdperk = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "TaxCreditResearchDevelopmentClearablePreviousTaxPeriodsRepayableCurrentAssessmentYear"));
                


            tx.Aanslag.KapitaalEnInterestsubsidies = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "CapitalSubsidiesInterestSubsidiesAgriculturalSupportRate500"));
            tx.Aanslag.NietVerantwoordeKosten = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "UnjustifiedExpensesHiddenSurplusProfitsFinancialAdvantagesBenefitsAllKind"));
            tx.Aanslag.AfzonderlijkeAanslag34 = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "SeparateAssessmentTaxedReservesCreditAssociationsInstitutionsOtherAuthorisedInstitutionsRate3400"));
            tx.Aanslag.AfzonderlijkeAanslag28 = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "SeparateAssessmentTaxedReservesCreditAssociationsInstitutionsOtherAuthorisedInstitutionsRate2800"));
            tx.Aanslag.AfzonderlijkeAanslagDividenden = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "SeparateAssessmentDividendsPaidCreditCorporationTradeEquipmentHousingCorporation"));
            tx.Aanslag.AfzonderlijkeAanslagRV = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "SeparateAssessmentTransitionalSystemReducedWithholdingTaxPaidTaxedReserves"));
            
            
            tx.Aanslag.VerdelingVermogen33 = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "SpecialAssessmentPre1990FullPartialDistributionCompanyAssetsRate3300"));
            tx.Aanslag.VerdelingVermogen16 = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "SpecialAssessmentPre1990FullPartialDistributionCompanyAssetsRate1650"));
            tx.Aanslag.VAAVennootschappen = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "SpecialAssessmentPre1990BenefitsAllKindCompanyLiquidation"));

            tx.Aanslag.AanvullendeHeffingDiamant = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "AdditionalDutiesDiamondTraders"));
            tx.Aanslag.TerugbetalingBelastingskrediet = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "RetributionTaxCreditResearchDevelopment"));

            tx.Aanslag.Taxable2015 = !this.Bool(this.Xbrl.Elements.FirstOrDefault(x=>x.Name=="NotLiableCorporateIncomeTaxNextTaxPeriodDueToMergerDivisionSimilarTransactions"));
            tx.Aanslag.TotaalBedragVoorzieningen = this.Number(this.Xbrl.Elements.Where(x => x.Name == "SeparateAssessmentAdditionalIndividualPensionProvisionsOnEndTaxPeriodClosingDateBefore2012-01-01"), "0");
            

            tx.FairnessTax.FairnessTaxBase = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "FairnessTax"));
            tx.FairnessTax.FairnessTaxPerc = (decimal)5.15;


            tx.BerekeningOverzicht.Verminderd = Bool(Xbrl.Elements.FirstOrDefault(x => x.Name == "ExclusionReducedRate"));

            tx.BerekeningOverzicht.CommonRateCapitalGains25 = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "CapitalGainsSharesRate2500"));

      

            tx.BerekeningOverzicht.CapitalGainsShares0412 = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "CapitalGainsSharesRate0040"));
            tx.BerekeningOverzicht.CapitalGainsShares25 = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "CapitalGainsSharesRate2500"));
            tx.BerekeningOverzicht.ExitTarif = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "BasicTaxableAmountExitTaxRate"));

            //tx.BelastingsBerekening.EenduidigTariefBedrag = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "BasicTaxableAmountCommonRate"));

            /* VERMINDERD / EENDUIDIG */


           /*
            tx.BelastingsBerekening.NonRefundableWithholdingTaxes_TaxCredit = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "NonRepayableAdvanceLevies"));
            tx.BelastingsBerekening.RefundableTaxeCredit = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "TaxCreditResearchDevelopmentClearablePreviousTaxPeriodsRepayableCurrentAssessmentYear"));
            tx.BelastingsBerekening.RefundableWithholdingTaxes = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "RepayableAdvanceLevies"));
            */
            //FairnessTaxForIncrease
            // move
            

            tx.BerekeningOverzicht.AP1Base = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "PrepaymentFirstQuarter"));
            tx.BerekeningOverzicht.AP2Base = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "PrepaymentSecondQuarter"));
            tx.BerekeningOverzicht.AP3Base = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "PrepaymentThirdQuarter"));
            tx.BerekeningOverzicht.AP4Base = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "PrepaymentFourthQuarter"));



            

           



        }

        private void _CalculateTax(ref TaxCalc.TaxCalc Data)
        {
            bool jongVen = this.Bool(this.Xbrl.Elements.FirstOrDefault(x => x.Name == "FirstThreeAccountingYearsSmallCompanyCorporationCode"));


           
            if (Data.BerekeningOverzicht == null)
            {
                Data.BerekeningOverzicht = new TaxCalc.BerekeningOverzichtItem { Id = Guid.NewGuid() };
            }

        

            // Data.BelastingsBerekening.JongVennootschap =
            int assessment = 2014;

            /* VerrekenbareVoorheffing
             */

            Data.VerrekenbareVoorheffing.VVNTBelastingKredietOnderzoekOntw = Data.VerrekenbareVoorheffing.VVNTBelastingKredietOnderzoekOntw.GetValueOrDefault();
            Data.VerrekenbareVoorheffing.VVNTFictieveRoerendeVoorheffing = Data.VerrekenbareVoorheffing.VVNTFictieveRoerendeVoorheffing.GetValueOrDefault();
            Data.VerrekenbareVoorheffing.VVNTForfaitairBuitenlands = Data.VerrekenbareVoorheffing.VVNTForfaitairBuitenlands.GetValueOrDefault();
            Data.VerrekenbareVoorheffing.VVNTBelastingKredietOnderzoekOntw = Data.VerrekenbareVoorheffing.VVNTBelastingKredietOnderzoekOntw.GetValueOrDefault();


            Data.VerrekenbareVoorheffing.VVNTTotaal = (Data.VerrekenbareVoorheffing.VVNTBelastingKredietOnderzoekOntw.GetValueOrDefault() + Data.VerrekenbareVoorheffing.VVNTFictieveRoerendeVoorheffing.GetValueOrDefault()) + Data.VerrekenbareVoorheffing.VVNTForfaitairBuitenlands.GetValueOrDefault();
            Data.VerrekenbareVoorheffing.VVTAndere = Math.Abs(Data.VerrekenbareVoorheffing.VVTAndere.GetValueOrDefault());
            Data.VerrekenbareVoorheffing.VVTAndereBuitenlands = Math.Abs(Data.VerrekenbareVoorheffing.VVTAndereBuitenlands.GetValueOrDefault());
            Data.VerrekenbareVoorheffing.VVTAndereEigen = Math.Abs(Data.VerrekenbareVoorheffing.VVTAndereEigen.GetValueOrDefault());
            Data.VerrekenbareVoorheffing.VVTAndereRoerendeVoorheffing = Math.Abs((Data.VerrekenbareVoorheffing.VVTAndereRoerendeVoorheffing.GetValueOrDefault()));
            Data.VerrekenbareVoorheffing.VVTBelgisch = Math.Abs((Data.VerrekenbareVoorheffing.VVTBelgisch.GetValueOrDefault()));
            Data.VerrekenbareVoorheffing.VVTEigenBuitenlands = Math.Abs((Data.VerrekenbareVoorheffing.VVTEigenBuitenlands.GetValueOrDefault()));
            Data.VerrekenbareVoorheffing.VBelastingkredietHuiidTijdperk = Data.VerrekenbareVoorheffing.VBelastingkredietHuiidTijdperk.GetValueOrDefault();
            Data.VerrekenbareVoorheffing.VVTTotaal = ((((Data.VerrekenbareVoorheffing.VVTAndere.GetValueOrDefault() + Data.VerrekenbareVoorheffing.VVTBelgisch.GetValueOrDefault()) + Data.VerrekenbareVoorheffing.VVTAndereBuitenlands.GetValueOrDefault()) + Data.VerrekenbareVoorheffing.VVTAndereEigen.GetValueOrDefault()) + Data.VerrekenbareVoorheffing.VVTAndereRoerendeVoorheffing.GetValueOrDefault()) + Data.VerrekenbareVoorheffing.VVTEigenBuitenlands.GetValueOrDefault();

            Data.BerekeningOverzicht.NonRepayableAdvanceLevies = Data.VerrekenbareVoorheffing.VVNTTotaal.GetValueOrDefault();
            Data.BerekeningOverzicht.RepayableAdvanceLevies = Data.VerrekenbareVoorheffing.VVTTotaal.GetValueOrDefault();
            Data.BerekeningOverzicht.RepayableResearchDevelopment = Data.VerrekenbareVoorheffing.VBelastingkredietHuiidTijdperk.GetValueOrDefault();

            

            #region Calc IPT

            Data.Aanslag.IPT2013 = 0;
            Data.Aanslag.IPT2014 = 0;
            Data.Aanslag.IPT2015 = 0;
            Data.BerekeningOverzicht.IPTBase = 0;
            Data.BerekeningOverzicht.IPTTotal = 0;
            Data.BerekeningOverzicht.IPTPerc = (decimal)0.6;
            if (Data.Aanslag.TotaalBedragVoorzieningen.GetValueOrDefault() > 0)
            {
                
                    Data.BerekeningOverzicht.IPTBase = Data.Aanslag.TotaalBedragVoorzieningen.GetValueOrDefault();
                    Data.Aanslag.IPT2013 = Math.Round(Data.Aanslag.TotaalBedragVoorzieningen.GetValueOrDefault() * (decimal)0.006, 2, MidpointRounding.AwayFromZero);
                    Data.Aanslag.IPT2014 = Math.Round(Data.Aanslag.TotaalBedragVoorzieningen.GetValueOrDefault() * (decimal)0.006, 2, MidpointRounding.AwayFromZero);
                    Data.Aanslag.IPT2015 = Math.Round(Data.Aanslag.TotaalBedragVoorzieningen.GetValueOrDefault() * (decimal)0.006, 2, MidpointRounding.AwayFromZero);

                    if (!Data.Aanslag.Taxable2015.GetValueOrDefault())
                    {
                        Data.Aanslag.IPT2014 += Data.Aanslag.IPT2015.GetValueOrDefault();
                        Data.Aanslag.IPT2015 = 0;
                        Data.BerekeningOverzicht.IPTPerc = (decimal)1.2;
                    }
                    Data.BerekeningOverzicht.IPTTotal = Data.Aanslag.IPT2014.GetValueOrDefault();
               
            }
            #endregion

       



            Data.BerekeningOverzicht.AssessmentYear =assessment;


            Data.BerekeningOverzicht.TotalTaxableReserves = Data.BelasteReserves.TDTotal.GetValueOrDefault();
            Data.BerekeningOverzicht.TotalTaxableReservesPlus = Data.BelasteReserves.OACapitalGainsOnShares.GetValueOrDefault()
                + Data.BelasteReserves.OACapitalGainsSharesReversal.GetValueOrDefault() + Data.BelasteReserves.OADefinitiveExemptionTaxShelter.GetValueOrDefault()
                + Data.BelasteReserves.OAExemptionRegionalPrem.GetValueOrDefault() + Data.BelasteReserves.OAFinalExemptionProfit.GetValueOrDefault()
                + Data.BelasteReserves.OAOthers.GetValueOrDefault();
            Data.BerekeningOverzicht.TotalTaxableReservesMinus = Data.BelasteReserves.OAInTheMin.GetValueOrDefault();

            Data.BerekeningOverzicht.TotalNonDeductibleTaxes = Data.VerworpenUitgaven.PDDisallowedCurrentIncomeTaxExpenses.GetValueOrDefault();
            Data.BerekeningOverzicht.TotalDissalowedExpensesWithoutNonDeductibleTax = Data.VerworpenUitgaven.DETotal.GetValueOrDefault();

            Data.BerekeningOverzicht.TaxableResult = Data.BerekeningOverzicht.StatAccountingResult.GetValueOrDefault()
                + Data.BerekeningOverzicht.TotalTaxableReserves.GetValueOrDefault()
                - Data.BerekeningOverzicht.TotalTaxableReservesPlus.GetValueOrDefault()
                + Data.BerekeningOverzicht.TotalTaxableReservesMinus.GetValueOrDefault()
                + Data.BerekeningOverzicht.TotalNonDeductibleTaxes.GetValueOrDefault()
                + Data.BerekeningOverzicht.TotalDissalowedExpensesWithoutNonDeductibleTax.GetValueOrDefault();

            if (Data.BerekeningOverzicht.TaxableResult.GetValueOrDefault() < 0)
                Data.BerekeningOverzicht.TaxableResult = 0;

            Data.BerekeningOverzicht.TotalDeductionLimit = Data.BelastbareBestZonderAftrek.TETotal.GetValueOrDefault();

            Data.BerekeningOverzicht.RemainingTaxableResult = Data.BerekeningOverzicht.TaxableResult.GetValueOrDefault() - Data.BerekeningOverzicht.TotalDeductionLimit.GetValueOrDefault();
            if (Data.BerekeningOverzicht.RemainingTaxableResult.GetValueOrDefault() < 0)
                Data.BerekeningOverzicht.RemainingTaxableResult = 0;



            Data.BerekeningOverzicht.TotalTaxTreaty = Data.OpdelingNaarOorsprong.IRTreatyForeignIncome.GetValueOrDefault();

            #region Fiscale aftrek herrekening
         
            decimal resterend = Data.BerekeningOverzicht.RemainingTaxableResult.GetValueOrDefault() - Data.BerekeningOverzicht.TotalTaxTreaty.GetValueOrDefault();

            Data.FiscaleAftrek.TotalNonTaxableElements = Data.FiscaleAftrek.TotalNonTaxableElements.HasValue ? Data.FiscaleAftrek.TotalNonTaxableElements.Value : 0;
            if (resterend < 0) resterend = 0;
            if (Data.FiscaleAftrek.TotalNonTaxableElements.Value > resterend)
            {
                   }
            else
            {
                resterend -= Data.FiscaleAftrek.TotalNonTaxableElements.Value;
                Data.FiscaleAftrek.PEBelgium = Data.FiscaleAftrek.PEBelgium.HasValue ? Data.FiscaleAftrek.PEBelgium.Value : 0;

                if ((resterend - Data.FiscaleAftrek.PEBelgium.Value) < 0)
                {
                  }
                else
                {

                    resterend -= Data.FiscaleAftrek.PEBelgium.Value;
                    Data.FiscaleAftrek.PIDBelgium = Data.FiscaleAftrek.PIDBelgium.HasValue ? Data.FiscaleAftrek.PIDBelgium.Value : 0;
                    if ((resterend - Data.FiscaleAftrek.PIDBelgium.Value) < 0)
                    {
                     }
                    else
                    {
                        resterend -= Data.FiscaleAftrek.PIDBelgium.Value;
                        Data.FiscaleAftrek.NIDBelgium = Data.FiscaleAftrek.NIDBelgium.HasValue ? Data.FiscaleAftrek.NIDBelgium.Value : 0;
                        if ((resterend - Data.FiscaleAftrek.NIDBelgium.Value) < 0)
                        {
                        }
                        else
                        {
                            resterend -= Data.FiscaleAftrek.NIDBelgium.Value;
                            Data.FiscaleAftrek.DTLBelgium = Data.FiscaleAftrek.DTLBelgium.HasValue ? Data.FiscaleAftrek.DTLBelgium.Value : 0;
                            if ((resterend - Data.FiscaleAftrek.DTLBelgium.Value) < 0)
                            {
                             }
                            else
                            {
                                resterend -= Data.FiscaleAftrek.DTLBelgium.Value;
                                Data.FiscaleAftrek.IDBelgium = Data.FiscaleAftrek.IDBelgium.HasValue ? Data.FiscaleAftrek.IDBelgium.Value : 0;
                                if ((resterend - Data.FiscaleAftrek.IDBelgium.Value) < 0)
                                {
                                }
                                else
                                {
                                    resterend -= Data.FiscaleAftrek.IDBelgium.Value;
                                }
                            }
                        }
                    }
                }

            }
            Data.FiscaleAftrek.TotalFiscaleAftrek = ((((Data.FiscaleAftrek.TotalNonTaxableElements + Data.FiscaleAftrek.PEBelgium) + Data.FiscaleAftrek.PIDBelgium) + Data.FiscaleAftrek.NIDBelgium) + Data.FiscaleAftrek.DTLBelgium) + Data.FiscaleAftrek.IDBelgium + Data.FiscaleAftrek.NIDHistoryBelgium.GetValueOrDefault();



            #endregion


            Data.BerekeningOverzicht.TotalFiscalDeductions = Data.FiscaleAftrek.TotalFiscaleAftrek.GetValueOrDefault();
            /* #166
            Data.BerekeningOverzicht.RemainingFiscalProfit = Data.BerekeningOverzicht.RemainingTaxableResult.GetValueOrDefault()
                    - Data.BerekeningOverzicht.TotalTaxTreaty.GetValueOrDefault()
                    - Data.BerekeningOverzicht.TotalFiscalDeductions.GetValueOrDefault();
             */ 
            if (Data.BerekeningOverzicht.TotalTaxTreaty.GetValueOrDefault() > 0)
            {
                Data.BerekeningOverzicht.RemainingFiscalProfit = Data.BerekeningOverzicht.RemainingTaxableResult.GetValueOrDefault()
                    - Data.BerekeningOverzicht.TotalTaxTreaty.GetValueOrDefault()
                    - Data.BerekeningOverzicht.TotalFiscalDeductions.GetValueOrDefault();
            }
            else {
                Data.BerekeningOverzicht.RemainingFiscalProfit = Data.BerekeningOverzicht.RemainingTaxableResult.GetValueOrDefault()
                    - Data.BerekeningOverzicht.TotalFiscalDeductions.GetValueOrDefault();
            }

            if (Data.BerekeningOverzicht.RemainingFiscalProfit.GetValueOrDefault() < 0)
                Data.BerekeningOverzicht.RemainingFiscalProfit = 0;

            Data.BerekeningOverzicht.CommonRateRemainingFiscalProfit = Data.BerekeningOverzicht.RemainingFiscalProfit;
            Data.BerekeningOverzicht.CommonRateDeductionLimit = Data.BerekeningOverzicht.TotalDeductionLimit.GetValueOrDefault()
                - Data.BelastbareBestZonderAftrek.TECapitalGains0412.GetValueOrDefault()
                - Data.BelastbareBestZonderAftrek.TECapitalAgriCulture.GetValueOrDefault();


           
            decimal? vaaDeelAuto = Data.BelastbareBestZonderAftrek.TEVaaCar.GetValueOrDefault();


            Data.BerekeningOverzicht.CapitalGainsShares25 = Data.BerekeningOverzicht.CommonRateCapitalGains25.GetValueOrDefault();

            Data.BerekeningOverzicht.CapitalGainsShares25Result = Math.Round((Data.BerekeningOverzicht.CapitalGainsShares25.GetValueOrDefault() / 100) * (Data.BerekeningOverzicht.CapitalGainsShares25Perc.GetValueOrDefault()), 2, MidpointRounding.AwayFromZero);



            Data.BerekeningOverzicht.CommonRateResult = Data.BerekeningOverzicht.CommonRateRemainingFiscalProfit.GetValueOrDefault()
                + Data.BerekeningOverzicht.CommonRateDeductionLimit.GetValueOrDefault()
                - Data.BerekeningOverzicht.CommonRateCapitalGains25.GetValueOrDefault();

            if (Data.BerekeningOverzicht.CommonRateResult.GetValueOrDefault() < 0)
                Data.BerekeningOverzicht.CommonRateResult = 0;

            #region Fairness Tax calc


        


            Data.FairnessTax.FairnessTaxResult = Math.Round(Data.FairnessTax.FairnessTaxBase.GetValueOrDefault() * (Data.FairnessTax.FairnessTaxPerc.GetValueOrDefault() / 100), 2, MidpointRounding.AwayFromZero);
           

            Data.BerekeningOverzicht.FairnessTaxBase = Data.FairnessTax.FairnessTaxBase.GetValueOrDefault();
            Data.BerekeningOverzicht.FairnessTaxPerc = Data.FairnessTax.FairnessTaxPerc.GetValueOrDefault();
            Data.BerekeningOverzicht.FairnessTaxTotal = Data.FairnessTax.FairnessTaxResult.GetValueOrDefault();
            Data.BerekeningOverzicht.BaseForIncreaseFairnessTax = Data.FairnessTax.FairnessTaxResult.GetValueOrDefault();

            #endregion


            Data.BerekeningOverzicht.ExitTarifResult = Math.Round(Data.BerekeningOverzicht.ExitTarif.GetValueOrDefault() * (Data.BerekeningOverzicht.ExitTarifPerc.GetValueOrDefault() / 100), 2, MidpointRounding.AwayFromZero);
            Data.BerekeningOverzicht.CapitalGainsShares0412 = Data.BelastbareBestZonderAftrek.TECapitalGains0412.GetValueOrDefault();
            Data.BerekeningOverzicht.CapitalGainsShares0412Result = Math.Round((Data.BerekeningOverzicht.CapitalGainsShares0412.GetValueOrDefault() / 100) * Data.BerekeningOverzicht.CapitalGainsShares0412Perc.GetValueOrDefault(), 2, MidpointRounding.AwayFromZero);

            decimal verminderdBovenGrens = Data.BerekeningOverzicht.VerminderdtariefStap3Base.GetValueOrDefault();


            Data.BerekeningOverzicht.VerminderdtariefStap1 = 0;

            Data.BerekeningOverzicht.VerminderdtariefStap2 = 0;

            Data.BerekeningOverzicht.VerminderdtariefStap3 = 0;
            Data.BerekeningOverzicht.EenduidigTariefBedrag = 0;

            // Added fbo 25082014
            Data.BerekeningOverzicht.EenduidigTariefTot = 0;

            if (Data.BerekeningOverzicht.CommonRateResult.GetValueOrDefault() > 0)
            {


                if (
                    Data.BerekeningOverzicht.CreditCorporationTradeEquipmentHousingCorporationTaxRate.GetValueOrDefault() ||
                    (!Data.BerekeningOverzicht.Verminderd.GetValueOrDefault() || (((Data.Tarief.BezitAandelenMeerDan50Percent == true || Data.Tarief.BezitAandelenDoorVennootschappen == true) || Data.Tarief.UitgekeerdDividendHogerDan13 == true) || Data.Tarief.ToegekendeBezoldiging == true) || (Data.BerekeningOverzicht.CommonRateResult.GetValueOrDefault() > verminderdBovenGrens)))
                {
                    Data.BerekeningOverzicht.VerminderdtariefStap1Base = 0;
                    Data.BerekeningOverzicht.VerminderdtariefStap1 = 0;
                    Data.BerekeningOverzicht.VerminderdtariefStap2Base = 0;
                    Data.BerekeningOverzicht.VerminderdtariefStap2 = 0;
                    Data.BerekeningOverzicht.VerminderdtariefStap3Base = 0;
                    Data.BerekeningOverzicht.VerminderdtariefStap3 = 0;

                    Data.BerekeningOverzicht.EenduidigTariefBedrag = Data.BerekeningOverzicht.CommonRateResult.GetValueOrDefault();

                    Data.BerekeningOverzicht.EenduidigTariefTot = Math.Round(Data.BerekeningOverzicht.EenduidigTariefBedrag.Value * (Data.BerekeningOverzicht.EenduidigTariefPerc.Value / 100), 2, MidpointRounding.AwayFromZero);
                    Data.BerekeningOverzicht.Verminderd = false;
                }
                else
                {
                    Data.BerekeningOverzicht.Verminderd = true;
                    Data.BerekeningOverzicht.EenduidigTariefBedrag = 0;
                    Data.BerekeningOverzicht.EenduidigTariefTot = 0;
                    decimal remainingTaxNormalTarif = Data.BerekeningOverzicht.CommonRateResult.GetValueOrDefault();

                    if (remainingTaxNormalTarif < Data.BerekeningOverzicht.VerminderdtariefStap1Base)
                    {
                        Data.BerekeningOverzicht.VerminderdtariefStap1Base = remainingTaxNormalTarif;
                        Data.BerekeningOverzicht.VerminderdtariefStap1 = Math.Round(Data.BerekeningOverzicht.VerminderdtariefStap1Base.Value * (Data.BerekeningOverzicht.VerminderdtariefStap1Perc.Value / 100), 2, MidpointRounding.AwayFromZero);
                        Data.BerekeningOverzicht.VerminderdtariefStap2 = 0;
                        Data.BerekeningOverzicht.VerminderdtariefStap3 = 0;
                        Data.BerekeningOverzicht.VerminderdtariefStap2Base = 0;
                        Data.BerekeningOverzicht.VerminderdtariefStap3Base = 0;
                    }
                    else
                    {
                        Data.BerekeningOverzicht.VerminderdtariefStap1 = Math.Round(Data.BerekeningOverzicht.VerminderdtariefStap1Base.Value * (Data.BerekeningOverzicht.VerminderdtariefStap1Perc.Value / 100), 2, MidpointRounding.AwayFromZero);
                        if (remainingTaxNormalTarif < Data.BerekeningOverzicht.VerminderdtariefStap2Base)
                        {
                            Data.BerekeningOverzicht.VerminderdtariefStap2Base = remainingTaxNormalTarif - Data.BerekeningOverzicht.VerminderdtariefStap1Base;
                            Data.BerekeningOverzicht.VerminderdtariefStap2 = Math.Round(Data.BerekeningOverzicht.VerminderdtariefStap2Base.Value * (Data.BerekeningOverzicht.VerminderdtariefStap2Perc.Value / 100), 2, MidpointRounding.AwayFromZero);

                            Data.BerekeningOverzicht.VerminderdtariefStap3 = 0;
                            Data.BerekeningOverzicht.VerminderdtariefStap3Base = 0;
                        }
                        else
                        {
                            Data.BerekeningOverzicht.VerminderdtariefStap2Base = Data.BerekeningOverzicht.VerminderdtariefStap2Base - Data.BerekeningOverzicht.VerminderdtariefStap1Base;
                            Data.BerekeningOverzicht.VerminderdtariefStap2 = Math.Round(Data.BerekeningOverzicht.VerminderdtariefStap2Base.Value * (Data.BerekeningOverzicht.VerminderdtariefStap2Perc.Value / 100), 2, MidpointRounding.AwayFromZero);
                            if (remainingTaxNormalTarif < Data.BerekeningOverzicht.VerminderdtariefStap3Base)
                            {
                                Data.BerekeningOverzicht.VerminderdtariefStap3Base = (remainingTaxNormalTarif - Data.BerekeningOverzicht.VerminderdtariefStap2Base) - Data.BerekeningOverzicht.VerminderdtariefStap1Base;
                                Data.BerekeningOverzicht.VerminderdtariefStap3 = Math.Round(Data.BerekeningOverzicht.VerminderdtariefStap3Base.Value * (Data.BerekeningOverzicht.VerminderdtariefStap3Perc.Value / 100), 2, MidpointRounding.AwayFromZero);

                            }
                            else
                            {
                                Data.BerekeningOverzicht.EenduidigTariefBedrag = remainingTaxNormalTarif;
                                Data.BerekeningOverzicht.EenduidigTariefTot = Math.Round(Data.BerekeningOverzicht.EenduidigTariefBedrag.Value * (Data.BerekeningOverzicht.EenduidigTariefPerc.Value / 100), 2, MidpointRounding.AwayFromZero);

                            }
                        }
                    }
                }
            }

            Data.BerekeningOverzicht.CorporateTaxBeforeRepayable = Data.BerekeningOverzicht.EenduidigTariefTot.GetValueOrDefault()
                + Data.BerekeningOverzicht.VerminderdtariefStap1.GetValueOrDefault() + Data.BerekeningOverzicht.VerminderdtariefStap2.GetValueOrDefault()
                + Data.BerekeningOverzicht.VerminderdtariefStap3.GetValueOrDefault() + Data.BerekeningOverzicht.ExitTarifResult.GetValueOrDefault()
                + Data.BerekeningOverzicht.CapitalGainsShares25Result.GetValueOrDefault() + Data.BerekeningOverzicht.CapitalGainsShares0412Result.GetValueOrDefault();

            Data.BerekeningOverzicht.RepayableTotal = 0;
            //If the corporate tax is higher than the positive nonrepayableAdvanceLevies 
            if (Data.BerekeningOverzicht.CorporateTaxBeforeRepayable.GetValueOrDefault() >= Math.Abs(Data.BerekeningOverzicht.NonRepayableAdvanceLevies.GetValueOrDefault()))
            {
                //add the non repayable advance levies as being the lower value as you cannot ever return more from the goverment than the set tax. The rest is lost.
                Data.BerekeningOverzicht.RepayableTotal += Math.Abs(Data.BerekeningOverzicht.NonRepayableAdvanceLevies.GetValueOrDefault());
            }
            else{
                //add the corporate tax as being the lower value as you cannot ever return more from the goverment than the set tax. The rest is lost.
                Data.BerekeningOverzicht.RepayableTotal += Data.BerekeningOverzicht.CorporateTaxBeforeRepayable.GetValueOrDefault();
            }

            //The following are always repayable and therefore are always added
            Data.BerekeningOverzicht.RepayableTotal += Data.BerekeningOverzicht.RepayableAdvanceLevies.GetValueOrDefault();
            Data.BerekeningOverzicht.RepayableTotal += Data.BerekeningOverzicht.RepayableResearchDevelopment.GetValueOrDefault();

            /*
                Data.BerekeningOverzicht.RepayableTotal = Data.BerekeningOverzicht.NonRepayableAdvanceLevies.GetValueOrDefault() + Data.BerekeningOverzicht.RepayableAdvanceLevies.GetValueOrDefault()
                + Data.BerekeningOverzicht.RepayableResearchDevelopment.GetValueOrDefault();
             
            if (Data.BerekeningOverzicht.CorporateTaxBeforeRepayable.GetValueOrDefault() - Data.BerekeningOverzicht.NonRepayableAdvanceLevies.GetValueOrDefault() > 0)
            {
                Data.BerekeningOverzicht.RepayableTotal = Data.BerekeningOverzicht.NonRepayableAdvanceLevies.GetValueOrDefault() + Data.BerekeningOverzicht.RepayableAdvanceLevies.GetValueOrDefault()
                + Data.BerekeningOverzicht.RepayableResearchDevelopment.GetValueOrDefault();
            }
            else
            {
                Data.BerekeningOverzicht.RepayableTotal = Data.BerekeningOverzicht.RepayableAdvanceLevies.GetValueOrDefault() + Data.BerekeningOverzicht.RepayableResearchDevelopment.GetValueOrDefault();
            }*/

            Data.BerekeningOverzicht.CorporateTaxAfterRepayable = Data.BerekeningOverzicht.CorporateTaxBeforeRepayable.GetValueOrDefault()
                - Data.BerekeningOverzicht.RepayableTotal.GetValueOrDefault();

            Data.BerekeningOverzicht.RetributionResearchDevelopmentBase = 0;
            Data.BerekeningOverzicht.RetributionResearchDevelopmentTotal = 0;
            if (Data.Aanslag.TerugbetalingBelastingskrediet.GetValueOrDefault() > 0)
            {
                Data.BerekeningOverzicht.RetributionResearchDevelopmentBase = Data.Aanslag.TerugbetalingBelastingskrediet.GetValueOrDefault();
                Data.BerekeningOverzicht.RetributionResearchDevelopmentTotal = Data.BerekeningOverzicht.RetributionResearchDevelopmentBase.GetValueOrDefault()
                    * (Data.BerekeningOverzicht.RetributionResearchDevelopmentPerc.GetValueOrDefault() / 100);
            }

            Data.BerekeningOverzicht.BaseForIncrease = Data.BerekeningOverzicht.CorporateTaxAfterRepayable.GetValueOrDefault()
                + Data.BerekeningOverzicht.RetributionResearchDevelopmentTotal.GetValueOrDefault()
                + Data.BerekeningOverzicht.BaseForIncreaseFairnessTax.GetValueOrDefault();

            if (!Data.BerekeningOverzicht.BaseForIncrease.HasValue) Data.BerekeningOverzicht.BaseForIncrease = 0;

            if (Data.BerekeningOverzicht.BaseForIncrease < 0)
            {
                Data.BerekeningOverzicht.BaseForIncrease = 0;
            }


            Data.BerekeningOverzicht.BaseForIncreaseTotal = Math.Round(Data.BerekeningOverzicht.BaseForIncrease.GetValueOrDefault() * (Data.BerekeningOverzicht.BaseForIncreasePerc.GetValueOrDefault() / 100), 2, MidpointRounding.AwayFromZero);


            Data.BerekeningOverzicht.AP1Total = Math.Round(Data.BerekeningOverzicht.AP1Base.Value * (Data.BerekeningOverzicht.AP1Percentage.Value / 100), 2, MidpointRounding.AwayFromZero);
            Data.BerekeningOverzicht.AP2Total = Math.Round(Data.BerekeningOverzicht.AP2Base.Value * (Data.BerekeningOverzicht.AP2Percentage.Value / 100), 2, MidpointRounding.AwayFromZero);
            Data.BerekeningOverzicht.AP3Total = Math.Round(Data.BerekeningOverzicht.AP3Base.Value * (Data.BerekeningOverzicht.AP3Percentage.Value / 100), 2, MidpointRounding.AwayFromZero);
            Data.BerekeningOverzicht.AP4Total = Math.Round(Data.BerekeningOverzicht.AP4Base.Value * (Data.BerekeningOverzicht.AP4Percentage.Value / 100), 2, MidpointRounding.AwayFromZero);

            Data.BerekeningOverzicht.APBaseTotal = ((Data.BerekeningOverzicht.AP1Base.GetValueOrDefault() + Data.BerekeningOverzicht.AP2Base.GetValueOrDefault()) + Data.BerekeningOverzicht.AP3Base.GetValueOrDefault()) + Data.BerekeningOverzicht.AP4Base.GetValueOrDefault();

            Data.BerekeningOverzicht.APTotal = ((Data.BerekeningOverzicht.AP1Total.GetValueOrDefault() + Data.BerekeningOverzicht.AP2Total.GetValueOrDefault()) + Data.BerekeningOverzicht.AP3Total.GetValueOrDefault()) + Data.BerekeningOverzicht.AP4Total.GetValueOrDefault();

            Data.BerekeningOverzicht.AdvancePayments = Data.BerekeningOverzicht.APBaseTotal;

            Data.BerekeningOverzicht.Test1Percent = Data.BerekeningOverzicht.BaseForIncrease.GetValueOrDefault() * (decimal)0.01;

            if (((Data.BerekeningOverzicht.BaseForIncreaseTotal.GetValueOrDefault() - Data.BerekeningOverzicht.APTotal.GetValueOrDefault()) < Data.BerekeningOverzicht.Test1Percent.GetValueOrDefault())
                || ((Data.BerekeningOverzicht.BaseForIncreaseTotal.GetValueOrDefault() - Data.BerekeningOverzicht.APTotal.GetValueOrDefault()) <= 40))
            {
                Data.BerekeningOverzicht.BaseForIncreaseApplied = 0;
            }
            else
            {
                Data.BerekeningOverzicht.BaseForIncreaseApplied = Data.BerekeningOverzicht.BaseForIncreaseTotal.GetValueOrDefault() - Data.BerekeningOverzicht.APTotal.GetValueOrDefault();
            }

            if (jongVen)
                Data.BerekeningOverzicht.BaseForIncreaseApplied = 0;

            Data.BerekeningOverzicht.TaxResult = Data.BerekeningOverzicht.CorporateTaxBeforeRepayable.GetValueOrDefault()
                + Data.BerekeningOverzicht.BaseForIncreaseApplied.GetValueOrDefault()
                - Data.BerekeningOverzicht.AdvancePayments.GetValueOrDefault();

            Data.BerekeningOverzicht.RepayableTotal = 0;
            //If the corporate tax is higher than the positive nonrepayableAdvanceLevies 
            if (Data.BerekeningOverzicht.CorporateTaxBeforeRepayable.GetValueOrDefault() >= Math.Abs(Data.BerekeningOverzicht.NonRepayableAdvanceLevies.GetValueOrDefault()))
            {
                //add the non repayable advance levies as being the lower value as you cannot ever return more from the goverment than the set tax. The rest is lost.
                Data.BerekeningOverzicht.RepayableTotal += Math.Abs(Data.BerekeningOverzicht.NonRepayableAdvanceLevies.GetValueOrDefault());
            }
            else
            {
                //add the corporate tax as being the lower value as you cannot ever return more from the goverment than the set tax. The rest is lost.
                Data.BerekeningOverzicht.RepayableTotal += Data.BerekeningOverzicht.CorporateTaxBeforeRepayable.GetValueOrDefault();
            }

            //The following are always repayable and therefore are always added
            Data.BerekeningOverzicht.RepayableTotal += Data.BerekeningOverzicht.RepayableAdvanceLevies.GetValueOrDefault();
            Data.BerekeningOverzicht.RepayableTotal += Data.BerekeningOverzicht.RepayableResearchDevelopment.GetValueOrDefault();


            Data.BerekeningOverzicht.TaxResult = Data.BerekeningOverzicht.TaxResult.GetValueOrDefault() - Data.BerekeningOverzicht.RepayableTotal;



            Data.BerekeningOverzicht.KapitaalEnInterestsubsidiesResult = 0;
            Data.BerekeningOverzicht.KapitaalEnInterestsubsidies = 0;
            if (Data.Aanslag.KapitaalEnInterestsubsidies.GetValueOrDefault() > 0)
            {
                Data.BerekeningOverzicht.KapitaalEnInterestsubsidies = Data.Aanslag.KapitaalEnInterestsubsidies.GetValueOrDefault();
                Data.BerekeningOverzicht.KapitaalEnInterestsubsidiesResult = Data.BerekeningOverzicht.KapitaalEnInterestsubsidies.GetValueOrDefault() * (Data.BerekeningOverzicht.KapitaalEnInterestsubsidiesPerc.GetValueOrDefault() / 100);

            }

            Data.BerekeningOverzicht.TaxResult += Data.BerekeningOverzicht.KapitaalEnInterestsubsidiesResult.GetValueOrDefault();


            // afzonderlijke aanslagen
            //Niet-verantwoorde kosten of voordelen van alle aard, verdoken meerwinsten en financiële voordelen of voordelen van alle aard
            Data.BerekeningOverzicht.UnjustifiedExpensesHiddenSurplusBase = 0;
            Data.BerekeningOverzicht.UnjustifiedExpensesHiddenSurplusTotal = 0;
            if (Data.Aanslag.NietVerantwoordeKosten.GetValueOrDefault() > 0)
            {
                Data.BerekeningOverzicht.UnjustifiedExpensesHiddenSurplusBase = Data.Aanslag.NietVerantwoordeKosten.GetValueOrDefault();
                Data.BerekeningOverzicht.UnjustifiedExpensesHiddenSurplusTotal = Data.BerekeningOverzicht.UnjustifiedExpensesHiddenSurplusBase.GetValueOrDefault()
                    * (Data.BerekeningOverzicht.UnjustifiedExpensesHiddenSurplusPerc.GetValueOrDefault() / 100);

            }
            Data.BerekeningOverzicht.TaxResult += Data.BerekeningOverzicht.UnjustifiedExpensesHiddenSurplusTotal.GetValueOrDefault();

            //Afzonderlijke aanslag van de belaste reserves ten name van erkende kredietinstellingen, tegen het tarief van 34 %
            Data.BerekeningOverzicht.SeparateAssessmentTaxedReserves3400Base = 0;
            Data.BerekeningOverzicht.SeparateAssessmentTaxedReserves3400Total = 0;
            if (Data.Aanslag.AfzonderlijkeAanslag34.GetValueOrDefault() > 0)
            {
                Data.BerekeningOverzicht.SeparateAssessmentTaxedReserves3400Base = Data.Aanslag.AfzonderlijkeAanslag34.GetValueOrDefault();
                Data.BerekeningOverzicht.SeparateAssessmentTaxedReserves3400Total = Data.BerekeningOverzicht.SeparateAssessmentTaxedReserves3400Base.GetValueOrDefault()
                    * (Data.BerekeningOverzicht.SeparateAssessmentTaxedReserves3400Perc.GetValueOrDefault() / 100);

            }
            Data.BerekeningOverzicht.TaxResult += Data.BerekeningOverzicht.SeparateAssessmentTaxedReserves3400Total.GetValueOrDefault();


            //Afzonderlijke aanslag van de belaste reserves ten name van erkende kredietinstellingen, tegen het tarief van 28 %
            Data.BerekeningOverzicht.SeparateAssessmentTaxedReserves2800Base = 0;
            Data.BerekeningOverzicht.SeparateAssessmentTaxedReserves2800Total = 0;
            if (Data.Aanslag.AfzonderlijkeAanslag28.GetValueOrDefault() > 0)
            {
                Data.BerekeningOverzicht.SeparateAssessmentTaxedReserves2800Base = Data.Aanslag.AfzonderlijkeAanslag28.GetValueOrDefault();
                Data.BerekeningOverzicht.SeparateAssessmentTaxedReserves2800Total = Data.BerekeningOverzicht.SeparateAssessmentTaxedReserves2800Base.GetValueOrDefault()
                    * (Data.BerekeningOverzicht.SeparateAssessmentTaxedReserves2800Perc.GetValueOrDefault() / 100);

            }
            Data.BerekeningOverzicht.TaxResult += Data.BerekeningOverzicht.SeparateAssessmentTaxedReserves2800Total.GetValueOrDefault();

            //Afzonderlijke aanslag van de uitgekeerde dividenden ten name van vennootschappen die krediet voor ambachtsoutillage mogen verstrekken en vennootschappen voor huisvesting
            Data.BerekeningOverzicht.SeparateAssessmentDividendsPaidBase = 0;
            Data.BerekeningOverzicht.SeparateAssessmentDividendsPaidTotal = 0;
            if (Data.Aanslag.AfzonderlijkeAanslagDividenden.GetValueOrDefault() > 0)
            {
                Data.BerekeningOverzicht.SeparateAssessmentDividendsPaidBase = Data.Aanslag.AfzonderlijkeAanslagDividenden.GetValueOrDefault();
                Data.BerekeningOverzicht.SeparateAssessmentDividendsPaidTotal = Data.BerekeningOverzicht.SeparateAssessmentDividendsPaidBase.GetValueOrDefault()
                    * (Data.BerekeningOverzicht.SeparateAssessmentDividendsPaidPerc.GetValueOrDefault() / 100);
            }
            Data.BerekeningOverzicht.TaxResult += Data.BerekeningOverzicht.SeparateAssessmentDividendsPaidTotal.GetValueOrDefault();

            // AFZONDERLIJKE AANSLAG FAIRNESS TAX (already calculated, not yet included)
            Data.BerekeningOverzicht.TaxResult += Data.BerekeningOverzicht.FairnessTaxTotal.GetValueOrDefault();

            //Afzonderlijke aanslag in het kader van de overgangsregeling van verlaagde roerende voorheffing op de uitkering van belaste reserves
            Data.BerekeningOverzicht.SeparateAssessmentTaxPaidTaxedReservesBase = 0;
            Data.BerekeningOverzicht.SeparateAssessmentTaxPaidTaxedReservesTotal = 0;
            if (Data.Aanslag.AfzonderlijkeAanslagRV.GetValueOrDefault() > 0)
            {
                Data.BerekeningOverzicht.SeparateAssessmentTaxPaidTaxedReservesBase = Data.Aanslag.AfzonderlijkeAanslagRV.GetValueOrDefault();
                Data.BerekeningOverzicht.SeparateAssessmentTaxPaidTaxedReservesTotal = Data.BerekeningOverzicht.SeparateAssessmentTaxPaidTaxedReservesBase.GetValueOrDefault()
                    * (Data.BerekeningOverzicht.SeparateAssessmentTaxPaidTaxedReservesPerc.GetValueOrDefault() / 100);
            }
            Data.BerekeningOverzicht.TaxResult += Data.BerekeningOverzicht.SeparateAssessmentTaxPaidTaxedReservesTotal.GetValueOrDefault();

            // Bijzondere Aanslagen

            Data.BerekeningOverzicht.DistributionCompanyAssets3300Base = 0;
            Data.BerekeningOverzicht.DistributionCompanyAssets3300Total = 0;
            if (Data.Aanslag.VerdelingVermogen33.GetValueOrDefault() > 0)
            {
                Data.BerekeningOverzicht.DistributionCompanyAssets3300Base = Data.Aanslag.VerdelingVermogen33.GetValueOrDefault();
                Data.BerekeningOverzicht.DistributionCompanyAssets3300Total = Data.BerekeningOverzicht.DistributionCompanyAssets3300Base.GetValueOrDefault()
                    * (Data.BerekeningOverzicht.DistributionCompanyAssets3300Perc.GetValueOrDefault() / 100);

            }
            Data.BerekeningOverzicht.TaxResult += Data.BerekeningOverzicht.DistributionCompanyAssets3300Total.GetValueOrDefault();

            Data.BerekeningOverzicht.DistributionCompanyAssets1650Base = 0;
            Data.BerekeningOverzicht.DistributionCompanyAssets1650Total = 0;
            if (Data.Aanslag.VerdelingVermogen16.GetValueOrDefault() > 0)
            {
                Data.BerekeningOverzicht.DistributionCompanyAssets1650Base = Data.Aanslag.VerdelingVermogen16.GetValueOrDefault();
                Data.BerekeningOverzicht.DistributionCompanyAssets1650Total = Data.BerekeningOverzicht.DistributionCompanyAssets1650Base.GetValueOrDefault()
                    * (Data.BerekeningOverzicht.DistributionCompanyAssets1650Perc.GetValueOrDefault() / 100);
            }
            Data.BerekeningOverzicht.TaxResult += Data.BerekeningOverzicht.DistributionCompanyAssets1650Total.GetValueOrDefault();

            Data.BerekeningOverzicht.BenefitsAllKindCompanyLiquidationBase = 0;
            Data.BerekeningOverzicht.BenefitsAllKindCompanyLiquidationTotal = 0;
            if (Data.Aanslag.VAAVennootschappen.GetValueOrDefault() > 0)
            {
                Data.BerekeningOverzicht.BenefitsAllKindCompanyLiquidationBase = Data.Aanslag.VAAVennootschappen.GetValueOrDefault();
                Data.BerekeningOverzicht.BenefitsAllKindCompanyLiquidationTotal = Data.BerekeningOverzicht.BenefitsAllKindCompanyLiquidationBase.GetValueOrDefault()
                    * (Data.BerekeningOverzicht.BenefitsAllKindCompanyLiquidationPerc.GetValueOrDefault() / 100);

            }
            Data.BerekeningOverzicht.TaxResult += Data.BerekeningOverzicht.BenefitsAllKindCompanyLiquidationTotal.GetValueOrDefault();

            // aanvullend diamant
            Data.BerekeningOverzicht.AdditionalDutiesDiamondTradersBase = 0;
            Data.BerekeningOverzicht.AdditionalDutiesDiamondTradersTotal = 0;
            if (Data.Aanslag.AanvullendeHeffingDiamant.GetValueOrDefault() > 0)
            {
                Data.BerekeningOverzicht.AdditionalDutiesDiamondTradersBase = Data.Aanslag.AanvullendeHeffingDiamant.GetValueOrDefault();
                Data.BerekeningOverzicht.AdditionalDutiesDiamondTradersTotal = Data.BerekeningOverzicht.AdditionalDutiesDiamondTradersBase.GetValueOrDefault()
                    * (Data.BerekeningOverzicht.AdditionalDutiesDiamondTradersPerc.GetValueOrDefault() / 100);
            }
            Data.BerekeningOverzicht.TaxResult += Data.BerekeningOverzicht.AdditionalDutiesDiamondTradersTotal.GetValueOrDefault();


            //Terugbetaling van een gedeelte van het voorheen verleende belastingkrediet voor onderzoek en ontwikkeling
          
            Data.BerekeningOverzicht.TaxResult += Data.BerekeningOverzicht.RetributionResearchDevelopmentTotal.GetValueOrDefault();


            // IPT ALREADY CALCULATED, JUST ADD TO TAXRESULT
            Data.BerekeningOverzicht.TaxResult += Data.BerekeningOverzicht.IPTTotal.GetValueOrDefault();


        }

        internal decimal? TaxCalculation()
        {
            
            if (Xbrl.Errors.Count > 0)
            {
                this.Xbrl.TaxCalcDetail = null;
                this.Xbrl.TaxCalc = null;
                return null;
            }

            TaxCalc.TaxCalc txc = new TaxCalc.TaxCalc();
            _ApplyFieldsToCalc(ref txc);
            _CalculateTax(ref txc);

            this.Xbrl.TaxCalcDetail = txc;
            this.Xbrl.TaxCalc = txc.BerekeningOverzicht.TaxResult;
            return txc.BerekeningOverzicht.TaxResult;
           
           
        }

                internal void ApplyExemptions()
        {
            string scenarioId = GetScenarioId("");
            string period = "D";
            var RemainingFiscalResultBelgium = Number(GetElementsByScenDefs(new string[] { "d-origin:OriginDimension::d-origin:BelgiumMember" }, new string[] { "D" }, new string[] { "tax-inc:RemainingFiscalResult" }));
            var RemainingFiscalResultNoTaxTreaty = Number(GetElementsByScenDefs(new string[] { "d-origin:OriginDimension::d-origin:NoTaxTreatyMember" }, new string[] { "D" }, new string[] { "tax-inc:RemainingFiscalResult" }));

            // SOURCES
            // Niet belastbare bestanddelen
            var MiscExemptionsSource = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Period == "D" && x.Name == "DeductibleMiscellaneousExemptions"));
            // DBI
            var DBISource = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Period == "D" && x.Name == "AccumulatedPEExemptIncomeMovableAssets"));
            // aftrek octrooi inkomsten
            var PatentsIncomeSource = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Period == "D" && x.Name == "DeductibleDeductionPatentsIncome"));
            // NID (Aftrek risicokapitaal)
            var AllowanceCorporateEquitySource = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Period == "D" && x.Name == "DeductionsBranchImmovablePropertyInEEAOutsideEEATreatyAfterDeductions"));
            // Gecompenseerde verliezen / vorige verliezen
            var CompensatedLossesSource = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Period == "D" && x.Name == "CompensableTaxLosses"));
            // InvesteringsAftrek
            var InvestDeductSource1 = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Period == "D" && x.Name == "BasicTaxableInvestmentDeductionCompatibleTaxCreditResearchDevelopment"));
            var InvestDeductSource2 = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Period == "D" && x.Name == "BasicTaxableInvestmentDeduction"));
            // NID History : Aftrek Risicokapitaal historiek
            var AllowanceCorporateEquityHistorySource = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Period == "D" && x.Name == "ExemptionsAllowanceCorporateEquityAccumulatedUpToAndIncludingAssessmentYear2012"));
            CompensatedLossesSource = Math.Abs(CompensatedLossesSource);
            // TARGETS
            
            var MiscExemptionsBelgium = CreateNewCalculationElement(null, new string[] { "D" }, "tax-inc:MiscellaneousExemptions", new string[] { "d-origin:OriginDimension::d-origin:BelgiumMember" }, "EUR", "INF").First(); 
            var MiscExemptionNoTaxTreaty = CreateNewCalculationElement(null, new string[] { "D" }, "tax-inc:MiscellaneousExemptions", new string[] { "d-origin:OriginDimension::d-origin:NoTaxTreatyMember" }, "EUR", "INF").First();

            var DBIBelgium= CreateNewCalculationElement(null, new string[] { "D" }, "tax-inc:PEExemptIncomeMovableAssets", new string[] { "d-origin:OriginDimension::d-origin:BelgiumMember" }, "EUR", "INF").First();
            var DBINoTaxTreaty = CreateNewCalculationElement(null, new string[] { "D" }, "tax-inc:PEExemptIncomeMovableAssets", new string[] { "d-origin:OriginDimension::d-origin:NoTaxTreatyMember" }, "EUR", "INF").First();

            var PatentsIncomeBelgium = CreateNewCalculationElement(null, new string[] { "D" }, "tax-inc:DeductionPatentsIncome", new string[] { "d-origin:OriginDimension::d-origin:BelgiumMember" }, "EUR", "INF").First();
            var PatentsIncomeNoTaxTreaty = CreateNewCalculationElement(null, new string[] { "D" }, "tax-inc:DeductionPatentsIncome", new string[] { "d-origin:OriginDimension::d-origin:NoTaxTreatyMember" }, "EUR", "INF").First();

            var AllowanceCorporateEquityBelgium = CreateNewCalculationElement(null, new string[] { "D" }, "tax-inc:AllowanceCorporateEquity", new string[] { "d-origin:OriginDimension::d-origin:BelgiumMember" }, "EUR", "INF").First();
            var AllowanceCorporateEquityNoTaxTreaty = CreateNewCalculationElement(null, new string[] { "D" }, "tax-inc:AllowanceCorporateEquity", new string[] { "d-origin:OriginDimension::d-origin:NoTaxTreatyMember" }, "EUR", "INF").First();

            var CompensatedLossesBelgium = CreateNewCalculationElement(null, new string[] { "D" }, "tax-inc:CompensatedTaxLosses", new string[] { "d-origin:OriginDimension::d-origin:BelgiumMember" }, "EUR", "INF").First();
            var CompensatedLossesNoTaxTreaty = CreateNewCalculationElement(null, new string[] { "D" }, "tax-inc:CompensatedTaxLosses", new string[] { "d-origin:OriginDimension::d-origin:NoTaxTreatyMember" }, "EUR", "INF").First();

            var InvestDeductBelgium = CreateNewCalculationElement(null, new string[] { "D" }, "tax-inc:AllowanceInvestmentDeduction", new string[] { "d-origin:OriginDimension::d-origin:BelgiumMember" }, "EUR", "INF").First();

            var AllowanceCorporateEquityHistoryBelgium = CreateNewCalculationElement(null, new string[] { "D" }, "tax-inc:DeductionExemptionsAllowanceCorporateEquityAccumulatedUpToAndIncludingAssessmentYear2012", new string[] { "d-origin:OriginDimension::d-origin:BelgiumMember" }, "EUR", "INF").First();
            var AllowanceCorporateEquityHistoryNoTaxTreaty = CreateNewCalculationElement(null, new string[] { "D" }, "tax-inc:DeductionExemptionsAllowanceCorporateEquityAccumulatedUpToAndIncludingAssessmentYear2012", new string[] { "d-origin:OriginDimension::d-origin:NoTaxTreatyMember" }, "EUR", "INF").First();



            // TOTALs
            // NID (Aftrek risicokapitaal)
            var AllowanceCorporateEquityResult = CreateNewCalculationElement(null, new string[] { "D" }, "tax-inc:AllowanceCorporateEquity", new string[] { "" }, "EUR", "INF").First();
            // Gecompenseerde verliezen / vorige verliezen
            var CompensatedLossesResult = CreateNewCalculationElement(null, new string[] { "D" }, "tax-inc:CompensatedTaxLossesIncludingTaxTreaty", new string[] { "" }, "EUR", "INF").First();

            var InvestDeductResult1 = CreateNewCalculationElement(null, new string[] { "D" }, "tax-inc:DeductibleAllowanceInvestmentDeductionCompatibleTaxCreditResearchDevelopment", new string[] { "" }, "EUR", "INF").First();
            var InvestDeductResult2 = CreateNewCalculationElement(null, new string[] { "D" }, "tax-inc:DeductibleAllowanceInvestmentDeductionIncompatibleTaxCreditResearchDevelopment", new string[] { "" }, "EUR", "INF").First();

            var AllowanceCorporateEquityHistoryResult = CreateNewCalculationElement(null, new string[] { "D" }, "tax-inc:DeductionExemptionsAllowanceCorporateEquityAccumulatedUpToAndIncludingAssessmentYear2012", new string[] { "" }, "EUR", "INF").First();


            //if (MiscExemptionsBelgium == null)
            //{
            //    MiscExemptionsBelgium = 
            //}
            //if (MiscExemptionNoTaxTreaty == null)
            //{
            //    MiscExemptionNoTaxTreaty = 
            //}

            // RESET
            
            MiscExemptionsBelgium.SetNumber(0);
            MiscExemptionNoTaxTreaty.SetNumber(0);
            DBIBelgium.SetNumber(0);
            DBINoTaxTreaty.SetNumber(0);
            PatentsIncomeBelgium.SetNumber(0);
            PatentsIncomeNoTaxTreaty.SetNumber(0);
            AllowanceCorporateEquityBelgium.SetNumber(0);
            AllowanceCorporateEquityNoTaxTreaty.SetNumber(0);
            CompensatedLossesBelgium.SetNumber(0);
            CompensatedLossesNoTaxTreaty.SetNumber(0);
            InvestDeductBelgium.SetNumber(0);
            AllowanceCorporateEquityHistoryBelgium.SetNumber(0);
            AllowanceCorporateEquityHistoryNoTaxTreaty.SetNumber(0);

            MiscExemptionsBelgium.Locked = true;
            MiscExemptionNoTaxTreaty.Locked = true;
            DBIBelgium.Locked = true;
            DBINoTaxTreaty.Locked = true;
            PatentsIncomeBelgium.Locked = true;
            PatentsIncomeNoTaxTreaty.Locked = true;
            AllowanceCorporateEquityBelgium.Locked = true;
            AllowanceCorporateEquityNoTaxTreaty.Locked = true;
            CompensatedLossesBelgium.Locked = true;
            CompensatedLossesNoTaxTreaty.Locked = true;
            InvestDeductBelgium.Locked = true;
            AllowanceCorporateEquityHistoryBelgium.Locked = true;
            AllowanceCorporateEquityHistoryNoTaxTreaty.Locked = true;
            
          


            // NIET BELASTBARE BESTANDDELEN
            if (RemainingFiscalResultBelgium > 0 && MiscExemptionsSource>0)
            {
                var exempted = RemainingFiscalResultBelgium > MiscExemptionsSource ? MiscExemptionsSource : RemainingFiscalResultBelgium;
                MiscExemptionsBelgium.SetNumber(exempted);
                MiscExemptionsSource -= exempted;
                RemainingFiscalResultBelgium -= exempted;

            }
            if (RemainingFiscalResultNoTaxTreaty > 0 && MiscExemptionsSource > 0)
            {
                var exempted = RemainingFiscalResultNoTaxTreaty > MiscExemptionsSource ? MiscExemptionsSource : RemainingFiscalResultNoTaxTreaty;
                MiscExemptionNoTaxTreaty.SetNumber(exempted);
                MiscExemptionsSource -= exempted;
                RemainingFiscalResultNoTaxTreaty -= exempted;

            }

            // DBI
            if (RemainingFiscalResultBelgium > 0 && DBISource > 0)
            {
                var exempted = RemainingFiscalResultBelgium > DBISource ? DBISource : RemainingFiscalResultBelgium;
                DBIBelgium.SetNumber(exempted);
                DBISource -= exempted;
                RemainingFiscalResultBelgium -= exempted;

            }
            if (RemainingFiscalResultNoTaxTreaty > 0 && DBISource > 0)
            {
                var exempted = RemainingFiscalResultNoTaxTreaty > DBISource ? DBISource : RemainingFiscalResultNoTaxTreaty;
                DBINoTaxTreaty.SetNumber(exempted);
                DBISource -= exempted;
                RemainingFiscalResultNoTaxTreaty -= exempted;

            }

            // AFTREK OCTROOIINKOMSTEN
            if (RemainingFiscalResultBelgium > 0 && PatentsIncomeSource > 0)
            {
                var exempted = RemainingFiscalResultBelgium > PatentsIncomeSource ? PatentsIncomeSource : RemainingFiscalResultBelgium;
                PatentsIncomeBelgium.SetNumber(exempted);
                PatentsIncomeSource -= exempted;
                RemainingFiscalResultBelgium -= exempted;

            }
            if (RemainingFiscalResultNoTaxTreaty > 0 && PatentsIncomeSource > 0)
            {
                var exempted = RemainingFiscalResultNoTaxTreaty > PatentsIncomeSource ? PatentsIncomeSource : RemainingFiscalResultNoTaxTreaty;
                PatentsIncomeNoTaxTreaty.SetNumber(exempted);
                PatentsIncomeSource -= exempted;
                RemainingFiscalResultNoTaxTreaty -= exempted;

            }

            // NID (aftrek risicokapitaal)
            if (RemainingFiscalResultBelgium > 0 && AllowanceCorporateEquitySource > 0)
            {
                var exempted = RemainingFiscalResultBelgium > AllowanceCorporateEquitySource ? AllowanceCorporateEquitySource : RemainingFiscalResultBelgium;
                AllowanceCorporateEquityBelgium.SetNumber(exempted);
                AllowanceCorporateEquitySource -= exempted;
                RemainingFiscalResultBelgium -= exempted;

            }
            if (RemainingFiscalResultNoTaxTreaty > 0 && AllowanceCorporateEquitySource > 0)
            {
                var exempted = RemainingFiscalResultNoTaxTreaty > AllowanceCorporateEquitySource ? AllowanceCorporateEquitySource : RemainingFiscalResultNoTaxTreaty;
                AllowanceCorporateEquityNoTaxTreaty.SetNumber(exempted);
                AllowanceCorporateEquitySource -= exempted;
                RemainingFiscalResultNoTaxTreaty -= exempted;

            }
            // SET RESULT USED
            AllowanceCorporateEquityResult.SetNumber(AllowanceCorporateEquityBelgium.GetNumberOrDefault(0) + AllowanceCorporateEquityNoTaxTreaty.GetNumberOrDefault(0));

            // Gecompenseerde verliezen
            if (RemainingFiscalResultBelgium > 0 && CompensatedLossesSource > 0)
            {
                var exempted = RemainingFiscalResultBelgium > CompensatedLossesSource ? CompensatedLossesSource : RemainingFiscalResultBelgium;
                CompensatedLossesBelgium.SetNumber(exempted);
                CompensatedLossesSource -= exempted;
                RemainingFiscalResultBelgium -= exempted;

            }
            if (RemainingFiscalResultNoTaxTreaty > 0 && CompensatedLossesSource > 0)
            {
                var exempted = RemainingFiscalResultNoTaxTreaty > CompensatedLossesSource ? CompensatedLossesSource : RemainingFiscalResultNoTaxTreaty;
                CompensatedLossesNoTaxTreaty.SetNumber(exempted);
                CompensatedLossesSource -= exempted;
                RemainingFiscalResultNoTaxTreaty -= exempted;

            }
            // SET RESULT USED
            CompensatedLossesResult.SetNumber(CompensatedLossesBelgium.GetNumberOrDefault(0) + CompensatedLossesNoTaxTreaty.GetNumberOrDefault(0));

            InvestDeductResult1.SetNumber(0);
            InvestDeductResult2.SetNumber(0);
            if (InvestDeductSource1 > 0)
            {
                if (RemainingFiscalResultBelgium > 0 && InvestDeductSource1 > 0)
                {
                    var exempted = RemainingFiscalResultBelgium > InvestDeductSource1 ? InvestDeductSource1 : RemainingFiscalResultBelgium;
                    InvestDeductBelgium.SetNumber(exempted);
                    InvestDeductSource1 -= exempted;
                    RemainingFiscalResultBelgium -= exempted;
                    InvestDeductResult1.SetNumber(exempted);
                }

            }
            else if (InvestDeductSource2 > 0)
            {
                if (RemainingFiscalResultBelgium > 0 && InvestDeductSource2 > 0)
                {
                    var exempted = RemainingFiscalResultBelgium > InvestDeductSource2 ? InvestDeductSource2 : RemainingFiscalResultBelgium;
                    InvestDeductBelgium.SetNumber(exempted);
                    InvestDeductSource2 -= exempted;
                    RemainingFiscalResultBelgium -= exempted;
                    InvestDeductResult2.SetNumber(exempted);

                }

            }


            // NID (aftrek risicokapitaal) HISTORIEK
            if (RemainingFiscalResultBelgium > 0 && AllowanceCorporateEquityHistorySource > 0)
            {
                var exempted = RemainingFiscalResultBelgium > AllowanceCorporateEquityHistorySource ? AllowanceCorporateEquityHistorySource : RemainingFiscalResultBelgium;
                AllowanceCorporateEquityHistoryBelgium.SetNumber(exempted);
                AllowanceCorporateEquityHistorySource -= exempted;
                RemainingFiscalResultBelgium -= exempted;

            }
            if (RemainingFiscalResultNoTaxTreaty > 0 && AllowanceCorporateEquityHistorySource > 0)
            {
                var exempted = RemainingFiscalResultNoTaxTreaty > AllowanceCorporateEquityHistorySource ? AllowanceCorporateEquityHistorySource : RemainingFiscalResultNoTaxTreaty;
                AllowanceCorporateEquityHistoryNoTaxTreaty.SetNumber(exempted);
                AllowanceCorporateEquityHistorySource -= exempted;
                RemainingFiscalResultNoTaxTreaty -= exempted;

            }
            // SET RESULT USED
            AllowanceCorporateEquityHistoryResult.SetNumber(AllowanceCorporateEquityHistoryBelgium.GetNumberOrDefault(0) + AllowanceCorporateEquityHistoryNoTaxTreaty.GetNumberOrDefault(0));
        }
    }
}
