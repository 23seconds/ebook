﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System;
using System.Xml;
using System.Xml.Linq;
using EY.com.eBook.BizTax.Contracts;
using EY.com.eBook.API.Contracts.Data.BizTax;

namespace EY.com.eBook.BizTax.AY2013.rcorp
{
    public class BizTaxRenderer : BizTaxRendererGenerated
    {
        public List<XmlQualifiedName> NameSpaces
        {
            get
            {
                return new List<XmlQualifiedName> {
          
            new XmlQualifiedName("xlink","http://www.w3.org/1999/xlink")
              ,
            new XmlQualifiedName("","http://www.xbrl.org/2003/instance")
          ,
            new XmlQualifiedName("iso4217","http://www.xbrl.org/2003/iso4217")
          ,
            new XmlQualifiedName("xsi","http://www.w3.org/2001/XMLSchema-instance")
          ,
            new XmlQualifiedName("link","http://www.xbrl.org/2003/linkbase")
   
          ,
            new XmlQualifiedName("xbrldi","http://xbrl.org/2006/xbrldi")
          ,
            new XmlQualifiedName("xbrldt","http://xbrl.org/2005/xbrldt")
          ,
            new XmlQualifiedName("tax-inc-rcorp","http://www.minfin.fgov.be/be/tax/inc/rcorp/2013-04-30")
          ,
            new XmlQualifiedName("xl","http://www.xbrl.org/2003/XLink")
          ,
            new XmlQualifiedName("t-reso","http://www.minfin.fgov.be/be/tax/t/reso/2013-04-30")
          ,
            new XmlQualifiedName("d-ty","http://www.minfin.fgov.be/be/tax/d/ty/2013-04-30")
          ,
            new XmlQualifiedName("tax-inc","http://www.minfin.fgov.be/be/tax/inc/2013-04-30")
          ,
            new XmlQualifiedName("pfs-dt","http://www.nbb.be/be/fr/pfs/ci/dt/2013-04-01")
          ,
            new XmlQualifiedName("ref","http://www.xbrl.org/2006/ref")
          ,
            new XmlQualifiedName("t-drfa","http://www.minfin.fgov.be/be/tax/t/drfa/2013-04-30")
          ,
            new XmlQualifiedName("t-wddc","http://www.minfin.fgov.be/be/tax/t/wddc/2013-04-30")
          ,
            new XmlQualifiedName("d-hh","http://www.minfin.fgov.be/be/tax/d/hh/2013-04-30")
          ,
            new XmlQualifiedName("d-ec","http://www.minfin.fgov.be/be/tax/d/ec/2013-04-30")
          ,
            new XmlQualifiedName("t-prre","http://www.minfin.fgov.be/be/tax/t/prre/2013-04-30")
          ,
            new XmlQualifiedName("d-expt","http://www.minfin.fgov.be/be/tax/d/expt/2013-04-30")
          ,
            new XmlQualifiedName("t-cg","http://www.minfin.fgov.be/be/tax/t/cg/2013-04-30")
          ,
            new XmlQualifiedName("d-asst","http://www.minfin.fgov.be/be/tax/d/asst/2013-04-30")
          ,
            new XmlQualifiedName("t-ricg","http://www.minfin.fgov.be/be/tax/t/ricg/2013-04-30")
          ,
            new XmlQualifiedName("t-stcg","http://www.minfin.fgov.be/be/tax/t/stcg/2013-04-30")
          ,
            new XmlQualifiedName("t-ace","http://www.minfin.fgov.be/be/tax/t/ace/2013-04-30")
          ,
            new XmlQualifiedName("t-pcs","http://www.minfin.fgov.be/be/tax/t/pcs/2013-04-30")
          ,
            new XmlQualifiedName("t-dpi","http://www.minfin.fgov.be/be/tax/t/dpi/2013-04-30")
          ,
            new XmlQualifiedName("t-itifa","http://www.minfin.fgov.be/be/tax/t/itifa/2013-04-30")
          ,
            new XmlQualifiedName("d-per","http://www.minfin.fgov.be/be/tax/d/per/2013-04-30")
          ,
            new XmlQualifiedName("t-idtc","http://www.minfin.fgov.be/be/tax/t/idtc/2013-04-30")
          ,
            new XmlQualifiedName("d-incc","http://www.minfin.fgov.be/be/tax/d/incc/2013-04-30")
          ,
            new XmlQualifiedName("d-invc","http://www.minfin.fgov.be/be/tax/d/invc/2013-04-30")
          ,
            new XmlQualifiedName("t-eap","http://www.minfin.fgov.be/be/tax/t/eap/2013-04-30")
          ,
            new XmlQualifiedName("d-empstat","http://www.minfin.fgov.be/be/tax/d/empstat/2013-04-30")
          ,
            new XmlQualifiedName("d-empreg","http://www.minfin.fgov.be/be/tax/d/empreg/2013-04-30")
          ,
            new XmlQualifiedName("d-tu","http://www.minfin.fgov.be/be/tax/d/tu/2013-04-30")
          ,
            new XmlQualifiedName("t-eapsf","http://www.minfin.fgov.be/be/tax/t/eapsf/2013-04-30")
          ,
            new XmlQualifiedName("d-empf","http://www.minfin.fgov.be/be/tax/d/empf/2013-04-30")
          ,
            new XmlQualifiedName("d-empper","http://www.minfin.fgov.be/be/tax/d/empper/2013-04-30")
          ,
            new XmlQualifiedName("t-dfa","http://www.minfin.fgov.be/be/tax/t/dfa/2013-04-30")
          ,
            new XmlQualifiedName("d-depm","http://www.minfin.fgov.be/be/tax/d/depm/2013-04-30")
          ,
            new XmlQualifiedName("d-br","http://www.minfin.fgov.be/be/tax/d/br/2013-04-30")
          ,
            new XmlQualifiedName("d-origin","http://www.minfin.fgov.be/be/tax/d/origin/2013-04-30")
          ,
            new XmlQualifiedName("pfs-gcd","http://www.nbb.be/be/fr/pfs/ci/gcd/2013-04-01")
          ,
            new XmlQualifiedName("pfs-vl","http://www.nbb.be/be/fr/pfs/ci/vl/2013-04-01")
          
        };
            }

        }

        public XDocument RenderBizTaxFile(BizTaxDataContract btdc, Guid fileId)
        {
            return RenderBizTaxFile(btdc, fileId, NameSpaces,TaxonomySchemaRef_rcorp);
        }


        public override BizTaxDataContract CalculateAndValidate()
        {
            base.CalculateAndValidate();
            Extra_275C_AllowanceCorporateCheck();
            Extra_275C_AllowanceCorporateHistoryCheck();
            Extra_275P_PatentsIncomeCheck();
            Extra_275U_TaxCreditResearchDevelopmentCheck();
            Extra_276Ws();
            // EXTRA'S

            return this.Xbrl;
        }

        internal override void Calc_f_rcorp_2211_assertion_set()
        {
            // PERIOD: "D"
            var RemainingFiscalResultBelgium = GetElementsByScenDefs(new string[] { "d-origin:OriginDimension::d-origin:BelgiumMember" }, new string[] { "D" }, new string[] { "tax-inc:RemainingFiscalResult" });
            var RemainingFiscalResultTaxTreaty = GetElementsByScenDefs(new string[] { "d-origin:OriginDimension::d-origin:TaxTreatyMember" }, new string[] { "D" }, new string[] { "tax-inc:RemainingFiscalResult" });
            var RemainingFiscalResultNoTaxTreaty = GetElementsByScenDefs(new string[] { "d-origin:OriginDimension::d-origin:NoTaxTreatyMember" }, new string[] { "D" }, new string[] { "tax-inc:RemainingFiscalResult" });
            var RemainingFiscalResultBeforeOriginDistribution = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:RemainingFiscalResultBeforeOriginDistribution" });
            var LossCurrentTaxPeriod = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:LossCurrentTaxPeriod" });
            if (RemainingFiscalResultBeforeOriginDistribution.Count > 0 || LossCurrentTaxPeriod.Count > 0 || RemainingFiscalResultBelgium.Count > 0 || RemainingFiscalResultTaxTreaty.Count > 0 || RemainingFiscalResultNoTaxTreaty.Count > 0)
            {
                var CalculatedAmountRemainingFiscalResultOriginDistributed = Sum(Number(RemainingFiscalResultBelgium, "0"), Number(RemainingFiscalResultTaxTreaty, "0"), Number(RemainingFiscalResultNoTaxTreaty, "0"));
                //CALCULATION HERE
                if ((Number(RemainingFiscalResultBeforeOriginDistribution, "0") < (decimal)0) || (Number(LossCurrentTaxPeriod, "0") < (decimal)0))
                {
                    //FORMULA HERE
                    bool test = ((Sum(Number(RemainingFiscalResultBelgium, "0"), Number(RemainingFiscalResultTaxTreaty, "0"), Number(RemainingFiscalResultNoTaxTreaty, "0")) < (decimal)0) && (Number(RemainingFiscalResultBeforeOriginDistribution, "0") < Sum(Number(RemainingFiscalResultBelgium, "0"), Number(RemainingFiscalResultTaxTreaty, "0"), Number(RemainingFiscalResultNoTaxTreaty, "0")))) ? (Sum(Number(RemainingFiscalResultBelgium, "0"), Number(RemainingFiscalResultTaxTreaty, "0"), Number(RemainingFiscalResultNoTaxTreaty, "0")) == Number(LossCurrentTaxPeriod, "0")) : (((Sum(Number(RemainingFiscalResultBelgium, "0"), Number(RemainingFiscalResultTaxTreaty, "0"), Number(RemainingFiscalResultNoTaxTreaty, "0")) >= (decimal)0) && (Number(RemainingFiscalResultBeforeOriginDistribution, "0") < Sum(Number(RemainingFiscalResultBelgium, "0"), Number(RemainingFiscalResultTaxTreaty, "0"), Number(RemainingFiscalResultNoTaxTreaty, "0")))) ? (Number(LossCurrentTaxPeriod, "0") == (decimal)0) : ((Number(RemainingFiscalResultBeforeOriginDistribution, "0") == Number(LossCurrentTaxPeriod, "0")) && (Sum(Number(RemainingFiscalResultBelgium, "0"), Number(RemainingFiscalResultTaxTreaty, "0"), Number(RemainingFiscalResultNoTaxTreaty, "0")) == Number(RemainingFiscalResultBeforeOriginDistribution, "0"))));
                    if (!test)
                    {
                        // MESSAGES
                        AddMessage(new BizTaxErrorDataContract
                        {
                            FileId = _fileId,
                            Id = "f-rcorp-2211",
                            Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Het bedrag vermeld in de rubriek '{Verlies van het belastbare tijdperk}' ({$LossCurrentTaxPeriod}) moet gelijk zijn aan het negatieve bedrag vermeld in de rubriek '{Resterend resultaat}' ({$RemainingFiscalResultBeforeOriginDistribution}) tenzij het zogenaamde \"recapture-mechanisme\" van toepassing is, d. i. wanneer het tijdens het belastbaar tijdperk geleden verlies in een buitenlandse inrichting niet in aanmerking kan worden genomen voor het vaststellen van de belastbare basis (art. 185, Â§ 3, WIB 92) of wanneer vorige verliezen geleden in een buitenlandse inrichting moeten worden toegevoegd aan het belastbaar inkomen van het belastbaar tijdperk (art. 206, Â§ 1, tweede lid, tweede zin, WIB 92). In voormelde gevallen moet het bedrag vermeld in de rubriek '{Verlies van het belastbare tijdperk}' ({$LossCurrentTaxPeriod}) gelijk zijn aan de som van de bedragen vermeld in de rubriek '{Resterend resultaat volgens oorsprong}' (= {$CalculatedAmountRemainingFiscalResultOriginDistributed})."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Le montant repris dans la rubrique '{Perte de la pÃ©riode imposable}' ({$LossCurrentTaxPeriod}) doit Ãªtre Ã©gal au montant nÃ©gatif mentionnÃ© dans la rubrique '{RÃ©sultat subsistant}' ({$RemainingFiscalResultBeforeOriginDistribution}) sauf si le mÃ©canisme dit de 'recapture' est d'application, c.-Ã -d. quand le montant des pertes de la pÃ©riode imposable Ã©prouvÃ©es dans un Ã©tablissement Ã©tranger n'est pas pris en considÃ©ration pour dÃ©terminer la base imposable (art. 185, Â§ 3, CIR 92) ou quand les pertes antÃ©rieures Ã©prouvÃ©es dans un Ã©tablissement Ã©tranger doivent Ãªtre ajoutÃ©es au bÃ©nÃ©fice imposable de la pÃ©riode imposable (art. 206, Â§ 1, 2Ã¨me alinÃ©a, 2Â°, CIR 92). Dans les cas mentionnÃ©s ci-dessus le montant mentionnÃ© dans la rubrique '{Perte de la pÃ©riode imposable}' ({$LossCurrentTaxPeriod}) doit Ãªtre Ã©gal Ã  la somme des montants repris dans la rubrique '{RÃ©sultat subsistant suivant sa provenance}' (= {$CalculatedAmountRemainingFiscalResultOriginDistributed})."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Der in der Rubrik '{Verlust des Besteuerungszeitraums}' erwÃ¤hnte Betrag ({$LossCurrentTaxPeriod}) muss dem negativen Betrag in der Rubrik '{Verbleibendes Ergebnis}' ({$RemainingFiscalResultBeforeOriginDistribution}) entsprechen, es sei denn, dass der so genannte \"recapture-Mechanismus\" Anwendung findet, d.h. wenn der wÃ¤hrend des Besteuerungszeitraums erlittene Verlust in einer auslÃ¤ndischer Niederlassung  fÃ¼r die Festlegung der steuerpflichtigen Grundlage nicht berÃ¼cksichtigt werden kann (Art. 185, Â§ 3, EStGB 92) oder wenn vorige in einer auslÃ¤ndischen Niederlassung erlittene Verluste zum steuerpflichtigen Einkommen des Besteuerungszeitraums hinzugefÃ¼gt werden mÃ¼ssen (Art. 206, Â§ 1, 2. Absatz, 2. Satz EStGB 92). In den vorerwÃ¤hnten FÃ¤llen muss der in der Rubrik '{Verlust des Besteuerungszeitraums}' angegebene Betrag ({$LossCurrentTaxPeriod}) der Summe der in der Rubrik '{Verbleibendes Ergebnis nach Herkunft}' erwÃ¤hnten BetrÃ¤ge (= {$CalculatedAmountRemainingFiscalResultOriginDistributed}) entsprechen."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Het bedrag vermeld in de rubriek '{Verlies van het belastbare tijdperk}' ({$LossCurrentTaxPeriod}) moet gelijk zijn aan het negatieve bedrag vermeld in de rubriek '{Resterend resultaat}' ({$RemainingFiscalResultBeforeOriginDistribution}) tenzij het zogenaamde \"recapture-mechanisme\" van toepassing is, d. i. wanneer het tijdens het belastbaar tijdperk geleden verlies in een buitenlandse inrichting niet in aanmerking kan worden genomen voor het vaststellen van de belastbare basis (art. 185, Â§ 3, WIB 92) of wanneer vorige verliezen geleden in een buitenlandse inrichting moeten worden toegevoegd aan het belastbaar inkomen van het belastbaar tijdperk (art. 206, Â§ 1, tweede lid, tweede zin, WIB 92). In voormelde gevallen moet het bedrag vermeld in de rubriek '{Verlies van het belastbare tijdperk}' ({$LossCurrentTaxPeriod}) gelijk zijn aan de som van de bedragen vermeld in de rubriek '{Resterend resultaat volgens oorsprong}' (= {$CalculatedAmountRemainingFiscalResultOriginDistributed})."}  
 }
                            ,
                            Fields = new List<string>()
                        }
                        );
                    }
                }
            }

        }


        internal override void Calc_f_idtc_5353_assertion_set()
        {
          
            string scenarioId = GetScenarioId("d-incc:IncentiveCategoryDimension::d-incc:InvestmentDeductionIncompatibleTaxCreditResearchDevelopmentMember/d-invc:InvestmentCategoryDimension::d-invc:ResearchDevelopmentMember");
            string period = "D";
            // SCENARIO: "d-incc:IncentiveCategoryDimension::d-incc:InvestmentDeductionIncompatibleTaxCreditResearchDevelopmentMember/d-invc:InvestmentCategoryDimension::d-invc:ResearchDevelopmentMember"
            // PERIOD: "D"
            var EligibleDepreciations_ResearchDevelopment = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:EligibleDepreciations" });
            var AcquisitionInvestmentValueSpread_ResearchDevelopment = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:AcquisitionInvestmentValueSpread" });
            if (EligibleDepreciations_ResearchDevelopment.Count > 0 || AcquisitionInvestmentValueSpread_ResearchDevelopment.Count > 0)
            {
                if (Number(EligibleDepreciations_ResearchDevelopment, "0") != 0 && Number(AcquisitionInvestmentValueSpread_ResearchDevelopment, "0") != 0)
                {
                    //CALCULATION HERE
                    //FORMULA HERE
                    bool test = Number(EligibleDepreciations_ResearchDevelopment, "0") < Number(AcquisitionInvestmentValueSpread_ResearchDevelopment, "0");
                    if (!test)
                    {
                        // MESSAGES
                        AddMessage(new BizTaxErrorDataContract
                        {
                            FileId = _fileId,
                            Id = "f-idtc-5353",
                            Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Het bedrag vermeld in de rubriek '{Aanneembare afschrijvingen}' moet kleiner zijn dan het bedrag vermeld in de rubriek '{Aanschaffings- of beleggingswaarde, afschrijfbaar}'."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Le montant repris dans la rubrique '{Amortissements admissibles}' doit Ãªtre infÃ©rieure au montant repris dans la rubrique '{Valeur d'acquisition ou d'investissement, amortissable}'."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Der in der Rubrik '{ZulÃ¤ssige Abschreibungen}' angegebene Betrag muss niedriger sein als der in der Rubrik '{Abschreibbarer Investitions- oder Anschaffungswert}' angegebene Betrag."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Het bedrag vermeld in de rubriek '{Aanneembare afschrijvingen}' moet kleiner zijn dan het bedrag vermeld in de rubriek '{Aanschaffings- of beleggingswaarde, afschrijfbaar}'."}  
 }
                            ,
                            Fields = new List<string>()
                        }
                        );
                    }
                }
            }

        
        }

        internal override void Calc_f_rcorp_2231_assertion_set()
        {
            // PERIOD: "D"
            var DeductibleAllowanceInvestmentDeductionIncompatibleTaxCreditResearchDevelopment = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:DeductibleAllowanceInvestmentDeductionIncompatibleTaxCreditResearchDevelopment" });
            var AllowanceInvestmentDeductionBelgium = GetElementsByScenDefs(new string[] { "d-origin:OriginDimension::d-origin:BelgiumMember" }, new string[] { "D" }, new string[] { "tax-inc:AllowanceInvestmentDeduction" });
            var DeductibleAllowanceInvestmentDeductionCompatibleTaxCreditResearchDevelopment = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:DeductibleAllowanceInvestmentDeductionCompatibleTaxCreditResearchDevelopment" });
            if (AllowanceInvestmentDeductionBelgium.Count > 0 || DeductibleAllowanceInvestmentDeductionIncompatibleTaxCreditResearchDevelopment.Count > 0 || DeductibleAllowanceInvestmentDeductionCompatibleTaxCreditResearchDevelopment.Count > 0)
            {
                //CALCULATION HERE
                //FORMULA HERE
                bool test = (Number(DeductibleAllowanceInvestmentDeductionIncompatibleTaxCreditResearchDevelopment, "0") > (decimal)0) ? ((Number(AllowanceInvestmentDeductionBelgium, "0") == Number(DeductibleAllowanceInvestmentDeductionIncompatibleTaxCreditResearchDevelopment, "0")) && (Number(DeductibleAllowanceInvestmentDeductionCompatibleTaxCreditResearchDevelopment, "0") == (decimal)0)) : ((Number(DeductibleAllowanceInvestmentDeductionCompatibleTaxCreditResearchDevelopment, "0") > (decimal)0) ? ((Number(AllowanceInvestmentDeductionBelgium, "0") == Number(DeductibleAllowanceInvestmentDeductionCompatibleTaxCreditResearchDevelopment, "0")) && (Number(DeductibleAllowanceInvestmentDeductionIncompatibleTaxCreditResearchDevelopment, "0") == (decimal)0)) : (Number(AllowanceInvestmentDeductionBelgium, "0") == (decimal)0));
                if (!test)
                {
                    // MESSAGES
                    AddMessage(new BizTaxErrorDataContract
                    {
                        FileId = _fileId,
                        Id = "f-rcorp-2231",
                        Messages = new List<BizTaxErrorMessageDataContract> { 
new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = string.Format("Het bedrag vermeld in de rubriek 'Investeringsaftrek' moet gelijk zijn aan het bedrag ingevuld in de bijlage in de rubriek 'Investeringsaftrek aftrekbaar voor het belastbare tijdperk' ({1} of {0}).",Number(DeductibleAllowanceInvestmentDeductionCompatibleTaxCreditResearchDevelopment).ToString("N",DefaultCulture),Number(DeductibleAllowanceInvestmentDeductionIncompatibleTaxCreditResearchDevelopment).ToString("N",DefaultCulture))}  
,new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = string.Format("Le montant mentionné dans la rubrique 'Déduction pour investissement' doit être égal à celui rempli dans l'annexe dans la rubrique 'Déduction pour investissement déductible pour la période imposable' ({1} of {0}).",Number(DeductibleAllowanceInvestmentDeductionCompatibleTaxCreditResearchDevelopment).ToString("N",DefaultCulture),Number(DeductibleAllowanceInvestmentDeductionIncompatibleTaxCreditResearchDevelopment).ToString("N",DefaultCulture))}  
,new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = string.Format("Der in der Rubrik 'Investitionsabzug' angegebene Betrag muss dem in der Rubrik  'Für den Besteuerungszeitraum abziehbarer Investitionsabzug' ({1} oder {0}) der Anlage ausgefüllten Betrag entsprechen.",Number(DeductibleAllowanceInvestmentDeductionCompatibleTaxCreditResearchDevelopment).ToString("N",DefaultCulture),Number(DeductibleAllowanceInvestmentDeductionIncompatibleTaxCreditResearchDevelopment).ToString("N",DefaultCulture))}  
,new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "NO TRANSLATION AVAILABLE IN LANGUAGE en-US."}  
 }
                        ,
                        Fields = new List<string>()
                    }
                    );
                }
            }

        }


        internal override void Calc_f_itifa_5327_assertion_set()
        {
            List<string> scenarioIds = GetScenarioIds(new string[] {"d-asst:AssetTypeDimension::d-asst:TangibleIntangibleFixedAssetsMember/d-per:PeriodDimension::d-per:TaxPeriod-0Member", "d-asst:AssetTypeDimension::d-asst:TangibleIntangibleFixedAssetsMember/d-per:PeriodDimension::d-per:TaxPeriod-1Member", "d-asst:AssetTypeDimension::d-asst:TangibleIntangibleFixedAssetsMember/d-per:PeriodDimension::d-per:TaxPeriod-2Member" });
            foreach (string scenarioId in scenarioIds)
            {
                string period = "D";
                // SCENARIO: "d-asst:AssetTypeDimension::d-asst:TangibleIntangibleFixedAssetsMember/d-per:PeriodDimension::d-per:TaxPeriod-1Member"
                // SCENARIO: "d-asst:AssetTypeDimension::d-asst:TangibleIntangibleFixedAssetsMember/d-per:PeriodDimension::d-per:TaxPeriod-2Member"
                // PERIOD: "D"
                var ExemptInvestmentReserveOverview = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:ExemptInvestmentReserveOverview" });
                var InvestmentsPreviousInvestmentReserveTaxPeriod = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:InvestmentsPreviousInvestmentReserveTaxPeriod" });
              //  var InvestmentCurrentTaxPeriod = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:InvestmentCurrentTaxPeriod" });

                var InvestmentCurrentTaxPeriod = GetElementsByScenDefsFilter(scenarioId,new string[] { "d-asst:AssetTypeDimension::d-asst:TangibleIntangibleFixedAssetsMember/d-per:PeriodDimension::d-per:TaxPeriod-0Member/d-ty:DateTypedDimension/d-ty:DescriptionTypedDimension", "d-asst:AssetTypeDimension::d-asst:TangibleIntangibleFixedAssetsMember/d-per:PeriodDimension::d-per:TaxPeriod-1Member/d-ty:DateTypedDimension/d-ty:DescriptionTypedDimension", "d-asst:AssetTypeDimension::d-asst:TangibleIntangibleFixedAssetsMember/d-per:PeriodDimension::d-per:TaxPeriod-2Member/d-ty:DateTypedDimension/d-ty:DescriptionTypedDimension" }, new string[] { period }, new string[] { "tax-inc:InvestmentCurrentTaxPeriod" });

                //var InvestmentCurrentTaxPeriod = GetElementsByScenDefs(new string[] { "d-asst:AssetTypeDimension::d-asst:TangibleIntangibleFixedAssetsMember/d-per:PeriodDimension::d-per:TaxPeriod-0Member/d-ty:DateTypedDimension/d-ty:DescriptionTypedDimension", "d-asst:AssetTypeDimension::d-asst:TangibleIntangibleFixedAssetsMember/d-per:PeriodDimension::d-per:TaxPeriod-1Member/d-ty:DateTypedDimension/d-ty:DescriptionTypedDimension", "d-asst:AssetTypeDimension::d-asst:TangibleIntangibleFixedAssetsMember/d-per:PeriodDimension::d-per:TaxPeriod-2Member/d-ty:DateTypedDimension/d-ty:DescriptionTypedDimension" }, new string[] { period }, new string[] { "tax-inc:InvestmentCurrentTaxPeriod" });
                var InvestBalance = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:InvestBalance" });
                var FormulaTarget = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:InvestBalance" });
                if (InvestBalance.Count > 0 || ExemptInvestmentReserveOverview.Count > 0 || InvestmentsPreviousInvestmentReserveTaxPeriod.Count > 0 || InvestmentCurrentTaxPeriod.Count > 0 || FormulaTarget.Count > 0)
                {
                    var dimensionValueQName = FactExplicitScenarioDimensionValue(ExemptInvestmentReserveOverview, QName("http://www . minfin . fgov . be/be/tax/d/per/2013-04-30", "d-per:PeriodDimension"));
                    var dimensionValueString = Data(LocalNameFromQname(dimensionValueQName));
                    var dimensionValue = SubstringBefore(dimensionValueString, "Member");
                    //CALCULATION HERE
                    if (FormulaTarget.Count == 0)
                    {
                        FormulaTarget = CreateNewCalculationElement(scenarioId, period, "tax-inc:InvestBalance", new string[] { "d-asst:AssetTypeDimension::d-asst:TangibleIntangibleFixedAssetsMember/d-per:PeriodDimension::d-per:TaxPeriod-1Member", "d-asst:AssetTypeDimension::d-asst:TangibleIntangibleFixedAssetsMember/d-per:PeriodDimension::d-per:TaxPeriod-2Member" }, "EUR", "INF");
                    }
                    if (FormulaTarget.Count > 1)
                    {
                        throw new Exception("More then one target for FormulaTarget in assertion f-itifa-5327");
                    }
                    FormulaTarget.First().Calculated = true;
                    FormulaTarget.First().SetNumber(Max(-Sum(-Number(ExemptInvestmentReserveOverview, "0"), Number(InvestmentsPreviousInvestmentReserveTaxPeriod, "0"), Number(InvestmentCurrentTaxPeriod, "0")), (decimal)0));
                    //FORMULA HERE
                    bool test = Max(-Sum(-Number(ExemptInvestmentReserveOverview, "0"), Number(InvestmentsPreviousInvestmentReserveTaxPeriod, "0"), Number(InvestmentCurrentTaxPeriod, "0")), (decimal)0) == Number(InvestBalance, "0");
                    if (!test)
                    {
                        // MESSAGES
                        AddMessage(new BizTaxErrorDataContract
                        {
                            FileId = _fileId,
                            Id = "f-itifa-5327",
                            Messages = new List<BizTaxErrorMessageDataContract> { 
new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = string.Format("Het totaal vermeld in de rubriek 'Nog te investeren saldo' ({0}) is niet juist (= ).",Number(InvestBalance).ToString("N",DefaultCulture))}  
,new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = string.Format("Le total repris dans la rubrique 'Solde des investissements à effectuer' ({0}) n'est pas correct (= ).",Number(InvestBalance).ToString("N",DefaultCulture))}  
,new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = string.Format("Der in der Rubrik 'Saldo der noch vorzunehmenden Investitionen' angegebene Gesamtbetrag ({0}) ist unzutreffend (= ).",Number(InvestBalance).ToString("N",DefaultCulture))}  
,new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "NO TRANSLATION AVAILABLE IN LANGUAGE en-US."}  
 }
                            ,
                            Fields = new List<string>()
                        }
                        );
                    }
                }
            } // end scenario loop

        }

        internal override void Calc_f_corp_1404_assertion_set()
        {
            string scenarioId = GetScenarioId("");
            string period = "D";
            // SCENARIO: ""
            // PERIOD: "D"
            var ExemptGifts = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:ExemptGifts" });
            var ShippingResultNotTonnageBased = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:ShippingResultNotTonnageBased" });
            if (ExemptGifts.Count > 0 || ShippingResultNotTonnageBased.Count > 0)
            {
                var CalculatedAmountLimitExemptGifts = Math.Round(Number(ShippingResultNotTonnageBased, "0") * Number(RateShippingResultNotTonnageBased, "0.05") * (decimal)100, MidpointRounding.AwayFromZero) / (decimal)100;
                //CALCULATION HERE
                if ((Number(ShippingResultNotTonnageBased, "0") >= (decimal)0) && (Number(ExemptGifts, "0") > (decimal)0))
                {
                    //FORMULA HERE
                    bool test = ((Math.Round(Number(ExemptGifts, "0") * (decimal)100, MidpointRounding.AwayFromZero) / (decimal)100) <= (Math.Round(Number(ShippingResultNotTonnageBased, "0") * Number(RateShippingResultNotTonnageBased, "0.05") * (decimal)100, MidpointRounding.AwayFromZero) / (decimal)100)) && (Number(ExemptGifts, "0") <= (decimal)500000);
                    if (!test)
                    {
                        // MESSAGES
                        AddMessage(new BizTaxErrorDataContract
                        {
                            FileId = _fileId,
                            Id = "f-corp-1404",
                            Messages = new List<BizTaxErrorMessageDataContract> { 
new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Het bedrag vermeld in de rubriek 'Vrijgestelde giften' 	moet kleiner zijn dan of gelijk zijn aan 5% van het bedrag vermeld in de rubriek 	'Werkelijk resultaat uit activiteiten waarvoor de winst niet wordt vastgesteld op basis van de tonnage' , met een maximum van 500 000 EUR. "}  
,new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Le montant repris dans la rubrique 'Libéralités exonérées' 	doit être inférieur ou égal à 5% du montant repris dans la rubrique 	'Résultat effectif des activités pour lesquelles le bénéfice n’est pas déterminé sur base du tonnage' , au maximum 500 000 EUR. "}  
,new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Der in der Rubrik 'Steuerfreie unentgeltliche Zuwendungen' 	angegebene Betrag darf sich, mit einem Maximalwert in Höhe von 500 000 EUR, maximal auf 5% des in der Rubrik 	'Effektives Ergebnis aus Aktivitäten, für die der Gewinn nicht anhand der Tonnage bestimmt wird'  angegebenen Betrags belaufen. "}  
,new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "NO TRANSLATION AVAILABLE IN LANGUAGE en-US."}  
 }
                            ,
                            Fields = new List<string>()
                        }
                        );
                    }
                }
            }

        }

        #region Extra checks
        internal void Extra_275C_AllowanceCorporateCheck()
        {
            var AllowanceCorporateEquityBelgium = GetElementsByScenDefs(new string[] { "d-origin:OriginDimension::d-origin:BelgiumMember" }, new string[] { "D" }, new string[] { "tax-inc:AllowanceCorporateEquity" });
            var AllowanceCorporateEquityNoTaxTreaty = GetElementsByScenDefs(new string[] { "d-origin:OriginDimension::d-origin:NoTaxTreatyMember" }, new string[] { "D" }, new string[] { "tax-inc:AllowanceCorporateEquity" });
            var AllowanceCorporateEquity275C = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:AllowanceCorporateEquity" });

            var calced = Sum(Number(AllowanceCorporateEquityBelgium, "0"), Number(AllowanceCorporateEquityNoTaxTreaty, "0"));
            bool test = calced == Number(AllowanceCorporateEquity275C, "0");

            if (!test)
            {
                // MESSAGES
                AddMessage(new BizTaxErrorDataContract
                {
                    FileId = _fileId,
                    Id = "f-xtra-rcorp-275c-allowcorp",
                    Messages = new List<BizTaxErrorMessageDataContract> { 
                        new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = string.Format("Het totaal vermeld in formulier 275C, 'Aftrek voor risicokapitaal van het huidig aanslagjaar die werkelijk wordt afgetrokken rubriek' ({0}€) is niet gelijk aan de som ({3}€) van de bedragen vermeld onder 'Belgisch' ({1}€) en 'Niet bij verdrag vrijgesteld' ({2}€) in het vak 'Uiteenzetting van de winst'.",Number(AllowanceCorporateEquity275C,"0").ToString("N",DefaultCulture),Number(AllowanceCorporateEquityBelgium,"0").ToString("N",DefaultCulture),Number(AllowanceCorporateEquityNoTaxTreaty,"0").ToString("N",DefaultCulture),Number(calced).ToString("N",DefaultCulture))} , 

                        new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = string.Format("Le total repris en formulaire 275C, 'Déduction pour capital à risque de l'exercice d'imposition actuel, effectivement déduite' ({0}€) n'est pas égal à la somme ({3}€) des quantités énumérées 'Belge' ({1}€) et 'Non exonéré par convention' ({2}€) dans la rubrique 'Détail des bénéfices'.",Number(AllowanceCorporateEquity275C,"0").ToString("N",DefaultCulture),Number(AllowanceCorporateEquityBelgium,"0").ToString("N",DefaultCulture),Number(AllowanceCorporateEquityNoTaxTreaty,"0").ToString("N",DefaultCulture),Number(calced).ToString("N",DefaultCulture))}  ,
                        new BizTaxErrorMessageDataContract { Culture = "en-US", Message = string.Format("Het totaal vermeld in formulier 275C, 'Aftrek voor risicokapitaal van het huidig aanslagjaar die werkelijk wordt afgetrokken rubriek' ({0}€) is niet gelijk aan de som ({3}€) van de bedragen vermeld onder 'Belgisch' ({1}€) en 'Niet bij verdrag vrijgesteld' ({2}€) in het vak 'Uiteenzetting van de winst'.",Number(AllowanceCorporateEquity275C,"0").ToString("N",DefaultCulture),Number(AllowanceCorporateEquityBelgium,"0").ToString("N",DefaultCulture),Number(AllowanceCorporateEquityNoTaxTreaty,"0").ToString("N",DefaultCulture),Number(calced).ToString("N",DefaultCulture))} , 
                        new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = string.Format("Het totaal vermeld in formulier 275C, 'Aftrek voor risicokapitaal van het huidig aanslagjaar die werkelijk wordt afgetrokken rubriek' ({0}€) is niet gelijk aan de som ({3}€) van de bedragen vermeld onder 'Belgisch' ({1}€) en 'Niet bij verdrag vrijgesteld' ({2}€) in het vak 'Uiteenzetting van de winst'.",Number(AllowanceCorporateEquity275C,"0").ToString("N",DefaultCulture),Number(AllowanceCorporateEquityBelgium,"0").ToString("N",DefaultCulture),Number(AllowanceCorporateEquityNoTaxTreaty,"0").ToString("N",DefaultCulture),Number(calced).ToString("N",DefaultCulture))}  
                    }
                    ,
                    Fields = new List<string>()
                }
                );
            }
        }

        internal void Extra_275C_AllowanceCorporateHistoryCheck() 
        {
            var AllowanceCorporateEquity2012Belgium = GetElementsByScenDefs(new string[] { "d-origin:OriginDimension::d-origin:BelgiumMember" }, new string[] { "D" }, new string[] { "tax-inc:DeductionExemptionsAllowanceCorporateEquityAccumulatedUpToAndIncludingAssessmentYear2012" });
            var AllowanceCorporateEquity2012NoTaxTreaty = GetElementsByScenDefs(new string[] { "d-origin:OriginDimension::d-origin:NoTaxTreatyMember" }, new string[] { "D" }, new string[] { "tax-inc:DeductionExemptionsAllowanceCorporateEquityAccumulatedUpToAndIncludingAssessmentYear2012" });
            var AllowanceCorporateEquity2012275C = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:DeductionExemptionsAllowanceCorporateEquityAccumulatedUpToAndIncludingAssessmentYear2012" });

            var calced = Sum(Number(AllowanceCorporateEquity2012Belgium, "0"), Number(AllowanceCorporateEquity2012NoTaxTreaty, "0"));
            bool test = calced == Number(AllowanceCorporateEquity2012275C, "0");

            if (!test)
            {
                // MESSAGES
                AddMessage(new BizTaxErrorDataContract
                {
                    FileId = _fileId,
                    Id = "f-xtra-rcorp-275c-allowcorphist",
                    Messages = new List<BizTaxErrorMessageDataContract> { 
                        new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = string.Format("Het totaal vermeld in formulier 275C, 'Voorheen en ten laatste tijdens aanslagjaar 2012 gevormde vrijstellingen voor risicokapitaal die werkelijk worden afgetrokken tijdens het huidige aanslagjaar' ({0}€) is niet gelijk aan de som ({3}€) van de bedragen vermeld onder 'Belgisch' ({1}€) en 'Niet bij verdrag vrijgesteld' ({2}€) in het vak 'Uiteenzetting van de winst'.",Number(AllowanceCorporateEquity2012275C,"0").ToString("N",DefaultCulture),Number(AllowanceCorporateEquity2012Belgium,"0").ToString("N",DefaultCulture),Number(AllowanceCorporateEquity2012NoTaxTreaty,"0").ToString("N",DefaultCulture),Number(calced).ToString("N",DefaultCulture))} , 

                        new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = string.Format("Le total repris en formulaire 275C, 'Exonérations pour capital à risque constituées antérieurement et au plus tard au cours de l'exercice d'imposition 2012, effectivement déduites au cours de l'exercice d'imposition actuel' ({0}€) n'est pas égal à la somme ({3}€) des quantités énumérées 'Belge' ({1}€) et 'Non exonéré par convention' ({2}€) dans la rubrique 'Détail des bénéfices'.",Number(AllowanceCorporateEquity2012275C,"0").ToString("N",DefaultCulture),Number(AllowanceCorporateEquity2012Belgium,"0").ToString("N",DefaultCulture),Number(AllowanceCorporateEquity2012NoTaxTreaty,"0").ToString("N",DefaultCulture),Number(calced).ToString("N",DefaultCulture))}  ,
                        new BizTaxErrorMessageDataContract { Culture = "en-US", Message = string.Format("Het totaal vermeld in formulier 275C, 'Voorheen en ten laatste tijdens aanslagjaar 2012 gevormde vrijstellingen voor risicokapitaal die werkelijk worden afgetrokken tijdens het huidige aanslagjaar' ({0}€) is niet gelijk aan de som ({3}€) van de bedragen vermeld onder 'Belgisch' ({1}€) en 'Niet bij verdrag vrijgesteld' ({2}€) in het vak 'Uiteenzetting van de winst'.",Number(AllowanceCorporateEquity2012275C,"0").ToString("N",DefaultCulture),Number(AllowanceCorporateEquity2012Belgium,"0").ToString("N",DefaultCulture),Number(AllowanceCorporateEquity2012NoTaxTreaty,"0").ToString("N",DefaultCulture),Number(calced).ToString("N",DefaultCulture))} , 
                        new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = string.Format("Het totaal vermeld in formulier 275C, 'Voorheen en ten laatste tijdens aanslagjaar 2012 gevormde vrijstellingen voor risicokapitaal die werkelijk worden afgetrokken tijdens het huidige aanslagjaar' ({0}€) is niet gelijk aan de som ({3}€) van de bedragen vermeld onder 'Belgisch' ({1}€) en 'Niet bij verdrag vrijgesteld' ({2}€) in het vak 'Uiteenzetting van de winst'.",Number(AllowanceCorporateEquity2012275C,"0").ToString("N",DefaultCulture),Number(AllowanceCorporateEquity2012Belgium,"0").ToString("N",DefaultCulture),Number(AllowanceCorporateEquity2012NoTaxTreaty,"0").ToString("N",DefaultCulture),Number(calced).ToString("N",DefaultCulture))} 
                    }
                    ,
                    Fields = new List<string>()
                }
                );
            }
        }


        internal void Extra_275P_PatentsIncomeCheck()
        {
            var DeductibleDeductionPatentsIncomeBelgium = GetElementsByScenDefs(new string[] { "d-origin:OriginDimension::d-origin:BelgiumMember" }, new string[] { "D" }, new string[] { "tax-inc:DeductionPatentsIncome" });
            var DeductibleDeductionPatentsIncomeNoTaxTreaty = GetElementsByScenDefs(new string[] { "d-origin:OriginDimension::d-origin:NoTaxTreatyMember" }, new string[] { "D" }, new string[] { "tax-inc:DeductionPatentsIncome" });
            var DeductibleDeductionPatentsIncome275P = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:DeductibleDeductionPatentsIncome" });

            var calced = Sum(Number(DeductibleDeductionPatentsIncomeBelgium, "0"), Number(DeductibleDeductionPatentsIncomeNoTaxTreaty, "0"));
            bool test = calced == Number(DeductibleDeductionPatentsIncome275P, "0");

            if (!test)
            {
                // MESSAGES
                AddMessage(new BizTaxErrorDataContract
                {
                    FileId = _fileId,
                    Id = "f-xtra-rcorp-275p-patincome",
                    Messages = new List<BizTaxErrorMessageDataContract> { 
                        new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = string.Format("Het totaal vermeld in formulier 275P, 'Aftrek voor octrooi-inkomsten' ({0}€) is niet gelijk aan de som ({3}€) van de bedragen vermeld onder 'Belgisch' ({1}€) en 'Niet bij verdrag vrijgesteld' ({2}€) in het vak 'Uiteenzetting van de winst'.",Number(DeductibleDeductionPatentsIncome275P,"0").ToString("N",DefaultCulture),Number(DeductibleDeductionPatentsIncomeBelgium,"0").ToString("N",DefaultCulture),Number(DeductibleDeductionPatentsIncomeNoTaxTreaty,"0").ToString("N",DefaultCulture),Number(calced).ToString("N",DefaultCulture))} , 

                        new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = string.Format("Le total repris en formulaire 275P, 'Déduction pour revenus de brevets' ({0}€) n'est pas égal à la somme ({3}€) des quantités énumérées 'Belge' ({1}€) et 'Non exonéré par convention' ({2}€) dans la rubrique 'Détail des bénéfices'.",Number(DeductibleDeductionPatentsIncome275P,"0").ToString("N",DefaultCulture),Number(DeductibleDeductionPatentsIncomeBelgium,"0").ToString("N",DefaultCulture),Number(DeductibleDeductionPatentsIncomeNoTaxTreaty,"0").ToString("N",DefaultCulture),Number(calced).ToString("N",DefaultCulture))}  ,
                        new BizTaxErrorMessageDataContract { Culture = "en-US", Message = string.Format("Het totaal vermeld in formulier 275P, 'Aftrek voor octrooi-inkomsten' ({0}€) is niet gelijk aan de som ({3}€) van de bedragen vermeld onder 'Belgisch' ({1}€) en 'Niet bij verdrag vrijgesteld' ({2}€) in het vak 'Uiteenzetting van de winst'.",Number(DeductibleDeductionPatentsIncome275P,"0").ToString("N",DefaultCulture),Number(DeductibleDeductionPatentsIncomeBelgium,"0").ToString("N",DefaultCulture),Number(DeductibleDeductionPatentsIncomeNoTaxTreaty,"0").ToString("N",DefaultCulture),Number(calced).ToString("N",DefaultCulture))} , 
                        new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = string.Format("Het totaal vermeld in formulier 275P, 'Aftrek voor octrooi-inkomsten' ({0}€) is niet gelijk aan de som ({3}€) van de bedragen vermeld onder 'Belgisch' ({1}€) en 'Niet bij verdrag vrijgesteld' ({2}€) in het vak 'Uiteenzetting van de winst'.",Number(DeductibleDeductionPatentsIncome275P,"0").ToString("N",DefaultCulture),Number(DeductibleDeductionPatentsIncomeBelgium,"0").ToString("N",DefaultCulture),Number(DeductibleDeductionPatentsIncomeNoTaxTreaty,"0").ToString("N",DefaultCulture),Number(calced).ToString("N",DefaultCulture))}  
                    }
                    ,
                    Fields = new List<string>()
                }
                );
            }
        }

        internal void Extra_275U_TaxCreditResearchDevelopmentCheck()
        {

            //DeductibleAllowanceInvestmentDeductionIncompatibleTaxCreditResearchDevelopment
            var AllowanceInvestmentDeductionBelgium = GetElementsByScenDefs(new string[] { "d-origin:OriginDimension::d-origin:BelgiumMember" }, new string[] { "D" }, new string[] { "tax-inc:AllowanceInvestmentDeduction" });
            var TaxCreditResearchDevelopment275U_G = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:DeductibleAllowanceInvestmentDeductionIncompatibleTaxCreditResearchDevelopment" });
            var TaxCreditResearchDevelopment275U_E = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:DeductibleAllowanceInvestmentDeductionCompatibleTaxCreditResearchDevelopment" });

            bool test = Number(AllowanceInvestmentDeductionBelgium, "0") == Number(TaxCreditResearchDevelopment275U_G, "0")
                           || Number(AllowanceInvestmentDeductionBelgium, "0") == Number(TaxCreditResearchDevelopment275U_E, "0");

            if (!test)
            {
                // MESSAGES
                AddMessage(new BizTaxErrorDataContract
                {
                    FileId = _fileId,
                    Id = "f-xtra-rcorp-275u-taxcredit",
                    Messages = new List<BizTaxErrorMessageDataContract> { 
                        new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = string.Format("Het bedrag ({0}€) aangewend in het vak 'Uiteenzetting van de winst' onder 'Investeringsaftrek aftrekbaar voor het belastbare tijdperk' is niet gelijk aan het bedrag in formulier 275U, vak 'Investeringsaftrek aftrekbaar voor het belastbare tijdperk' onder 'Investeringsaftrek voor vennootschappen die niet opteren voor het belastingkrediet voor onderzoek en ontwikkeling' [code 1437.1] ({1}€) noch onder 'Investeringsaftrek voor vennootschappen die opteren voor het belastingkrediet voor onderzoek en ontwikkeling' [code 1437.2] ({2}€).",Number(AllowanceInvestmentDeductionBelgium,"0").ToString("N",DefaultCulture),Number(TaxCreditResearchDevelopment275U_G,"0").ToString("N",DefaultCulture),Number(TaxCreditResearchDevelopment275U_E,"0").ToString("N",DefaultCulture))} , 
                        new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = string.Format("Le montant ({0}€) employée dans la boîte 'Détail des bénéfices',  'Déduction pour investissement' n'est pas égal du montant sous forme 275U 'Déduction pour investissement déductible pour la période imposable', 'Déduction pour investissement pour les sociétés qui n'optent pas pour le crédit d’impôt pour recherche et développement' [code 1437.1] ({1}€) ni 'Déduction pour investissement pour les sociétés qui optent pour le crédit d’impôt pour recherche et développement' [code 1437.2] ({2}€).",Number(AllowanceInvestmentDeductionBelgium,"0").ToString("N",DefaultCulture),Number(TaxCreditResearchDevelopment275U_G,"0").ToString("N",DefaultCulture),Number(TaxCreditResearchDevelopment275U_E,"0").ToString("N",DefaultCulture))} , 
                         new BizTaxErrorMessageDataContract { Culture = "en-US", Message = string.Format("Het bedrag ({0}€) aangewend in het vak 'Uiteenzetting van de winst' onder 'Investeringsaftrek aftrekbaar voor het belastbare tijdperk' is niet gelijk aan het bedrag in formulier 275U, vak 'Investeringsaftrek aftrekbaar voor het belastbare tijdperk' onder 'Investeringsaftrek voor vennootschappen die niet opteren voor het belastingkrediet voor onderzoek en ontwikkeling' [code 1437.1] ({1}€) noch onder 'Investeringsaftrek voor vennootschappen die opteren voor het belastingkrediet voor onderzoek en ontwikkeling' [code 1437.2] ({2}€).",Number(AllowanceInvestmentDeductionBelgium,"0").ToString("N",DefaultCulture),Number(TaxCreditResearchDevelopment275U_G,"0").ToString("N",DefaultCulture),Number(TaxCreditResearchDevelopment275U_E,"0").ToString("N",DefaultCulture))} , 
                         new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = string.Format("Het bedrag ({0}€) aangewend in het vak 'Uiteenzetting van de winst' onder 'Investeringsaftrek aftrekbaar voor het belastbare tijdperk' is niet gelijk aan het bedrag in formulier 275U, vak 'Investeringsaftrek aftrekbaar voor het belastbare tijdperk' onder 'Investeringsaftrek voor vennootschappen die niet opteren voor het belastingkrediet voor onderzoek en ontwikkeling' [code 1437.1] ({1}€) noch onder 'Investeringsaftrek voor vennootschappen die opteren voor het belastingkrediet voor onderzoek en ontwikkeling' [code 1437.2] ({2}€).",Number(AllowanceInvestmentDeductionBelgium,"0").ToString("N",DefaultCulture),Number(TaxCreditResearchDevelopment275U_G,"0").ToString("N",DefaultCulture),Number(TaxCreditResearchDevelopment275U_E,"0").ToString("N",DefaultCulture))} , 
                        
                    }
                    ,
                    Fields = new List<string>()
                }
                );
            }
        }

        internal void Extra_276Ws()
        {
            var elsW1 = GetElementsByScenDefs(new string[] { 
                "d-empf:EmploymentFunctionDimension::d-empf:ResearcherMember/d-empper:EmploymentPeriodDimension::d-empper:EmployedPartTimeMember/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension" ,
                "d-empf:EmploymentFunctionDimension::d-empf:HighlyQualifiedResearcherMember/d-empper:EmploymentPeriodDimension::d-empper:EmployedPartTimeMember/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension"
            }, new string[] { "D" }, new string[] { "tax-inc:ActualExemptionAdditionalPersonnel" });

            var elsW2 = GetElementsByScenDefs(new string[] { 
                "d-empf:EmploymentFunctionDimension::d-empf:IncreaseTechnologicalPotentialPersonnelMember/d-empper:EmploymentPeriodDimension::d-empper:EmployedPartTimeMember/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension"
                }, new string[] { "D" }, new string[] { "tax-inc:ActualExemptionAdditionalPersonnel" });

            var elsW3 = GetElementsByScenDefs(new string[] { 
                "d-empf:EmploymentFunctionDimension::d-empf:DepartmentHeadExportMember/d-empper:EmploymentPeriodDimension::d-empper:EmployedPartTimeMember"
            }, new string[] { "D" }, new string[] { "tax-inc:ActualExemptionAdditionalPersonnel" });

            var elsW4 = GetElementsByScenDefs(new string[] { 
                "d-empf:EmploymentFunctionDimension::d-empf:DepartmentHeadTQMMember/d-empper:EmploymentPeriodDimension::d-empper:EmployedPartTimeMember"
            }, new string[] { "D" }, new string[] { "tax-inc:ActualExemptionAdditionalPersonnel" });


            var ttl = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:ReversalPreviousExemptions" });
            var sumEls = Sum(Number(elsW1,"0"),Number(elsW2,"0"),Number(elsW3,"0"),Number(elsW4,"0"));
            bool test = Number(ttl, "0") == sumEls;

            if (!test)
            {
                AddMessage(new BizTaxErrorDataContract
                {
                    FileId = _fileId,
                    Id = "f-xtra-rcorp-276W1-4",
                    Messages = new List<BizTaxErrorMessageDataContract> { 
                        new BizTaxErrorMessageDataContract { Culture = "nl-BE", 
                            Message = string.Format(
                                "Het bedrag ({0}€) vemeld in het vak 'Verworpen uitgaven' onder 'Terugnemingen van vroegere vrijstellingen' moet gelijk zijn aan de som ({1}€) bekomen uit <ul>"
                                +"<li> 276W1 'In een vorig belastbaar tijdperk aangeworven personeel waarvan de voltijdse tewerkstelling tijdens het belastbare tijdperk is opgehouden', som van veld 'Effectief vrijgesteld' ({2}€)</li>"
                                +"<li> 276W2 'In een vorig belastbaar tijdperk aangeworven personeel waarvan de voltijdse tewerkstelling tijdens het belastbare tijdperk is opgehouden', som van veld 'Effectief vrijgesteld' ({3}€)</li>"
                                +"<li> 276W3 'Vrijstelling voor bijkomend personeel tewerkgesteld als diensthoofd voor de uitvoer', veld 'Effectief vrijgesteld' onder kolom 'Einde voltijds' ({4}€)</li>"
                                +"<li> 276W4 'Vrijstelling voor bijkomend personeel als diensthoofd van de afdeling Integrale kwaliteitszorg', veld 'Effectief vrijgesteld' onder kolom 'Einde voltijds' ({5}€)</li></ul>"
                                    , Number(ttl,"0").ToString("N",DefaultCulture)
                                    , sumEls.ToString("N",DefaultCulture)
                                    , Number(elsW1,"0").ToString("N",DefaultCulture)
                                    , Number(elsW2,"0").ToString("N",DefaultCulture)
                                    , Number(elsW3,"0").ToString("N",DefaultCulture)
                                    , Number(elsW4,"0").ToString("N",DefaultCulture)
                            )
                        },
                        new BizTaxErrorMessageDataContract { Culture = "fr-FR", 
                            Message = string.Format(
                                "Het bedrag ({0}€) vemeld in het vak 'Verworpen uitgaven' onder 'Terugnemingen van vroegere vrijstellingen' moet gelijk zijn aan de som ({1}€) bekomen uit <ul>"
                                +"<li> 276W1 'In een vorig belastbaar tijdperk aangeworven personeel waarvan de voltijdse tewerkstelling tijdens het belastbare tijdperk is opgehouden', som van veld 'Effectief vrijgesteld' ({2}€)</li>"
                                +"<li> 276W2 'In een vorig belastbaar tijdperk aangeworven personeel waarvan de voltijdse tewerkstelling tijdens het belastbare tijdperk is opgehouden', som van veld 'Effectief vrijgesteld' ({3}€)</li>"
                                +"<li> 276W3 'Vrijstelling voor bijkomend personeel tewerkgesteld als diensthoofd voor de uitvoer', veld 'Effectief vrijgesteld' onder kolom 'Einde voltijds' ({4}€)</li>"
                                +"<li> 276W4 'Vrijstelling voor bijkomend personeel als diensthoofd van de afdeling Integrale kwaliteitszorg', veld 'Effectief vrijgesteld' onder kolom 'Einde voltijds' ({5}€)</li></ul>"
                                    , Number(ttl,"0").ToString("N",DefaultCulture)
                                    , sumEls.ToString("N",DefaultCulture)
                                    , Number(elsW1,"0").ToString("N",DefaultCulture)
                                    , Number(elsW2,"0").ToString("N",DefaultCulture)
                                    , Number(elsW3,"0").ToString("N",DefaultCulture)
                                    , Number(elsW4,"0").ToString("N",DefaultCulture)
                            )
                        },
                        new BizTaxErrorMessageDataContract { Culture = "en-US", 
                            Message = string.Format(
                                "Het bedrag ({0}€) vemeld in het vak 'Verworpen uitgaven' onder 'Terugnemingen van vroegere vrijstellingen' moet gelijk zijn aan de som ({1}€) bekomen uit <ul>"
                                +"<li> 276W1 'In een vorig belastbaar tijdperk aangeworven personeel waarvan de voltijdse tewerkstelling tijdens het belastbare tijdperk is opgehouden', som van veld 'Effectief vrijgesteld' ({2}€)</li>"
                                +"<li> 276W2 'In een vorig belastbaar tijdperk aangeworven personeel waarvan de voltijdse tewerkstelling tijdens het belastbare tijdperk is opgehouden', som van veld 'Effectief vrijgesteld' ({3}€)</li>"
                                +"<li> 276W3 'Vrijstelling voor bijkomend personeel tewerkgesteld als diensthoofd voor de uitvoer', veld 'Effectief vrijgesteld' onder kolom 'Einde voltijds' ({4}€)</li>"
                                +"<li> 276W4 'Vrijstelling voor bijkomend personeel als diensthoofd van de afdeling Integrale kwaliteitszorg', veld 'Effectief vrijgesteld' onder kolom 'Einde voltijds' ({5}€)</li></ul>"
                                    , Number(ttl,"0").ToString("N",DefaultCulture)
                                    , sumEls.ToString("N",DefaultCulture)
                                    , Number(elsW1,"0").ToString("N",DefaultCulture)
                                    , Number(elsW2,"0").ToString("N",DefaultCulture)
                                    , Number(elsW3,"0").ToString("N",DefaultCulture)
                                    , Number(elsW4,"0").ToString("N",DefaultCulture)
                            )
                        },
                       new BizTaxErrorMessageDataContract { Culture = "de-DE", 
                            Message = string.Format(
                                "Het bedrag ({0}€) vemeld in het vak 'Verworpen uitgaven' onder 'Terugnemingen van vroegere vrijstellingen' moet gelijk zijn aan de som ({1}€) bekomen uit <ul>"
                                +"<li> 276W1 'In een vorig belastbaar tijdperk aangeworven personeel waarvan de voltijdse tewerkstelling tijdens het belastbare tijdperk is opgehouden', som van veld 'Effectief vrijgesteld' ({2}€)</li>"
                                +"<li> 276W2 'In een vorig belastbaar tijdperk aangeworven personeel waarvan de voltijdse tewerkstelling tijdens het belastbare tijdperk is opgehouden', som van veld 'Effectief vrijgesteld' ({3}€)</li>"
                                +"<li> 276W3 'Vrijstelling voor bijkomend personeel tewerkgesteld als diensthoofd voor de uitvoer', veld 'Effectief vrijgesteld' onder kolom 'Einde voltijds' ({4}€)</li>"
                                +"<li> 276W4 'Vrijstelling voor bijkomend personeel als diensthoofd van de afdeling Integrale kwaliteitszorg', veld 'Effectief vrijgesteld' onder kolom 'Einde voltijds' ({5}€)</li></ul>"
                                    , Number(ttl,"0").ToString("N",DefaultCulture)
                                    , sumEls.ToString("N",DefaultCulture)
                                    , Number(elsW1,"0").ToString("N",DefaultCulture)
                                    , Number(elsW2,"0").ToString("N",DefaultCulture)
                                    , Number(elsW3,"0").ToString("N",DefaultCulture)
                                    , Number(elsW4,"0").ToString("N",DefaultCulture)
                            )
                        }
                    }
                    ,
                    Fields = new List<string>()
                }
                );
            }
        }

        // ActualExemptionAdditionalPersonnel
        // ActualExemptionAdditionalPersonnel
        // ActualExemptionAdditionalPersonnel


        //      d-empf:EmploymentFunctionDimension/d-empper:EmploymentPeriodDimension/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension
        // NOT: d-empf:EmploymentFunctionDimension/d-empper:EmploymentPeriodDimension/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension
        #endregion

        internal void IndexFiches()
        {
            List<BizTaxFicheInfoDataContract> fiches = new List<BizTaxFicheInfoDataContract>
            {
                new BizTaxFicheInfoDataContract { FicheId="Id" },
                new BizTaxFicheInfoDataContract { FicheId="275.1.A" },
                new BizTaxFicheInfoDataContract { FicheId="275.1.B", Annexes =  Xbrl.Elements.Where(x=>x.BinaryValue!=null && x.BinaryValue.Count>0).ToList()}
            };

            if (Xbrl.Elements.Where(x => x.Name == "ExemptWriteDownDebtClaim" && x.GetNumberOrDefault(0) != 0).Count() > 0
                || Xbrl.Elements.Where(x => x.Name == "ExemptProvisionRisksExpenses" && x.GetNumberOrDefault(0) != 0).Count() > 0)
            {
                fiches.Add(new BizTaxFicheInfoDataContract { FicheId = "204.3" });
            }

            List<string> scenarioIds = GetScenarioIdsFuzzy(new string[] { "d-asst:AssetTypeDimension::d-asst:SeaVesselMember" });
            if (Xbrl.Elements.Count(x => scenarioIds.Contains(x.Context) && !string.IsNullOrEmpty(x.Value)) > 0)
            {
                fiches.Add(new BizTaxFicheInfoDataContract { FicheId = "275B" });
            }

            //D__id__SpecificSecurityMember__id__2010-01
            //D__id__SpecificSecurityMember__id__2013-02__id__2010-01
            scenarioIds = GetScenarioIdsFuzzy(new string[] { "d-asst:AssetTypeDimension::d-asst:SpecificSecurityMember" });
            if (Xbrl.Elements.Count(x => scenarioIds.Contains(x.Context) && !string.IsNullOrEmpty(x.Value)) > 0)
            {
                fiches.Add(new BizTaxFicheInfoDataContract { FicheId = "275K" });
            }

            scenarioIds = GetScenarioIdsFuzzy(new string[] { "d-asst:AssetTypeDimension::d-asst:TangibleIntangibleFixedAssetsMember" });
            if (Xbrl.Elements.Count(x => scenarioIds.Contains(x.Context) && !string.IsNullOrEmpty(x.Value)) > 0)
            {
                fiches.Add(new BizTaxFicheInfoDataContract { FicheId = "276K" });
            }

            scenarioIds = GetScenarioIdsFuzzy(new string[] { "d-asst:AssetTypeDimension::d-asst:CorporateVehicleMember" });
            if (Xbrl.Elements.Count(x => scenarioIds.Contains(x.Context) && !string.IsNullOrEmpty(x.Value)) > 0)
            {
                fiches.Add(new BizTaxFicheInfoDataContract { FicheId = "276N" });
            }

            scenarioIds = GetScenarioIdsFuzzy(new string[] { "d-asst:AssetTypeDimension::d-asst:RiverVesselMember" });
            if (Xbrl.Elements.Count(x => scenarioIds.Contains(x.Context) && !string.IsNullOrEmpty(x.Value)) > 0)
            {
                fiches.Add(new BizTaxFicheInfoDataContract { FicheId = "276P" });
            }

            if (Xbrl.Elements.Where(x => x.Name == "AllowanceCorporateEquityCurrentTaxPeriod" && x.GetNumberOrDefault(0) != 0).Count() > 0
                || Xbrl.Elements.Where(x => x.Name.StartsWith("ExemptionsAllowanceCorporateEquityAccumulatedUpToAndIncludingAssessmentYear") && x.GetNumberOrDefault(0) != 0).Count() > 0)
            {
                fiches.Add(new BizTaxFicheInfoDataContract { FicheId = "275C" });
            }

            if (Xbrl.Elements.Where(x => x.Name == "PaymentCertainState" && x.GetNumberOrDefault(0) != 0).Count() > 0)
            {
                fiches.Add(new BizTaxFicheInfoDataContract { FicheId = "275F" });
            }

            if (Xbrl.Elements.Where(x => x.Name == "CalculationBasisDeductionPatentsIncome" && x.GetNumberOrDefault(0) != 0).Count() > 0)
            {
                fiches.Add(new BizTaxFicheInfoDataContract { FicheId = "275P" });
            }


            if (Xbrl.Elements.Where(x => x.Name == "CorrectedIncreaseTaxableReservesInvestmentReserveExcluded" && x.GetNumberOrDefault(0) != 0).Count() > 0
               || Xbrl.Elements.Where(x => x.Name =="ExemptInvestmentReserveOverview" && x.GetNumberOrDefault(0) != 0).Count() > 0
                || Xbrl.Elements.Where(x => x.Name == "InvestBalance" && x.GetNumberOrDefault(0) != 0).Count() > 0)
            {
                fiches.Add(new BizTaxFicheInfoDataContract { FicheId = "275R" });
            }

            // d-incc:IncentiveCategoryDimension/d-invc:InvestmentCategoryDimension (?)
            if (Xbrl.Elements.Where(x => x.Name == "DeductibleAllowanceInvestmentDeductionIncompatibleTaxCreditResearchDevelopment" && x.GetNumberOrDefault(0) != 0).Count() > 0
               || Xbrl.Elements.Where(x => x.Name == "CarryOverInvestmentDeduction" && x.GetNumberOrDefault(0) != 0).Count() > 0
               || Xbrl.Elements.Where(x => x.Name == "DeductibleAllowanceInvestmentDeductionCompatibleTaxCreditResearchDevelopment" && x.GetNumberOrDefault(0) != 0).Count() > 0
               || Xbrl.Elements.Where(x => x.Name == "CarryOverInvestmentDeductionCompatibleTaxCreditResearchDevelopment" && x.GetNumberOrDefault(0) != 0).Count() > 0)
            {
                fiches.Add(new BizTaxFicheInfoDataContract { FicheId = "275U" });
            }


            if (Xbrl.Elements.Where(x => x.Name == "TaxCreditResearchDevelopment" && x.GetNumberOrDefault(0) != 0).Count() > 0
              || Xbrl.Elements.Where(x => x.Name == "ClearableTaxCreditResearchDevelopmentCurrentTaxPeriod" && x.GetNumberOrDefault(0) != 0).Count() > 0
              )
            {
                fiches.Add(new BizTaxFicheInfoDataContract { FicheId = "275W" });
            }
            //
            //
            //d-empreg:EmploymentRegimeDimension/d-empstat:EmploymentStatusDimension/d-per:PeriodDimension/d-tu:TimeUnitDimension/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension


            List<string> crefs = Xbrl.Contexts.Where(c => c.ScenarioDef=="d-empreg:EmploymentRegimeDimension/d-empstat:EmploymentStatusDimension/d-per:PeriodDimension/d-tu:TimeUnitDimension/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension").Select(c => c.Id).ToList();
            if (Xbrl.Elements.Count(x => crefs.Contains(x.ContextRef) && x.GetNumberOrDefault(0) != 0)>0)
            {
                fiches.Add(new BizTaxFicheInfoDataContract { FicheId = "276T" });
            }

            scenarioIds = GetScenarioIds(new string[] { 
                "d-empf:EmploymentFunctionDimension::d-empf:HighlyQualifiedResearcherMember/d-empper:EmploymentPeriodDimension::d-empper:EmployedFullTimeMember/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension",
                "d-empf:EmploymentFunctionDimension::d-empf:HighlyQualifiedResearcherMember/d-empper:EmploymentPeriodDimension::d-empper:EmployedPartTimeMember/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension",
                "d-empf:EmploymentFunctionDimension::d-empf:ResearcherMember/d-empper:EmploymentPeriodDimension::d-empper:EmployedPartTimeMember/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension",
                "d-empf:EmploymentFunctionDimension::d-empf:ResearcherMember/d-empper:EmploymentPeriodDimension::d-empper:EmployedFullTimeMember/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension"
            });
            if (Xbrl.Elements.Count(x => scenarioIds.Contains(x.Context) && !string.IsNullOrEmpty(x.Value)) > 0)
            {
                fiches.Add(new BizTaxFicheInfoDataContract { FicheId = "276W1" });
            }

            scenarioIds = GetScenarioIds(new string[] { 
                "d-empf:EmploymentFunctionDimension::d-empf:IncreaseTechnologicalPotentialPersonnelMember/d-empper:EmploymentPeriodDimension::d-empper:EmployedFullTimeMember/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension",
                "d-empf:EmploymentFunctionDimension::d-empf:IncreaseTechnologicalPotentialPersonnelMember/d-empper:EmploymentPeriodDimension::d-empper:EmployedPartTimeMember/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension"
            });
            if (Xbrl.Elements.Count(x => scenarioIds.Contains(x.Context) && !string.IsNullOrEmpty(x.Value)) > 0)
            {
                fiches.Add(new BizTaxFicheInfoDataContract { FicheId = "276W2" });
            }

            scenarioIds = GetScenarioIds(new string[] { 
                "d-empf:EmploymentFunctionDimension::d-empf:DepartmentHeadExportMember/d-empper:EmploymentPeriodDimension::d-empper:RecruitmentFullTimeMember",
                "d-empf:EmploymentFunctionDimension::d-empf:DepartmentHeadExportMember/d-empper:EmploymentPeriodDimension::d-empper:EmployedFullTimeTransferTaxPeriodMember",
                "d-empf:EmploymentFunctionDimension::d-empf:DepartmentHeadExportMember/d-empper:EmploymentPeriodDimension::d-empper:RecruitmentFullTimeReplacementMember",
                "d-empf:EmploymentFunctionDimension::d-empf:DepartmentHeadExportMember/d-empper:EmploymentPeriodDimension::d-empper:EmployedFullTimeMember",
                "d-empf:EmploymentFunctionDimension::d-empf:DepartmentHeadExportMember/d-empper:EmploymentPeriodDimension::d-empper:EmployedPartTimeMember"
            });
            if (Xbrl.Elements.Count(x => scenarioIds.Contains(x.Context) && !string.IsNullOrEmpty(x.Value)) > 0)
            {
                fiches.Add(new BizTaxFicheInfoDataContract { FicheId = "276W3" });
            }

            scenarioIds = GetScenarioIds(new string[] { 
                "d-empf:EmploymentFunctionDimension::d-empf:DepartmentHeadTQMMember/d-empper:EmploymentPeriodDimension::d-empper:RecruitmentFullTimeMember",
                "d-empf:EmploymentFunctionDimension::d-empf:DepartmentHeadTQMMember/d-empper:EmploymentPeriodDimension::d-empper:EmployedFullTimeTransferTaxPeriodMember",
                "d-empf:EmploymentFunctionDimension::d-empf:DepartmentHeadTQMMember/d-empper:EmploymentPeriodDimension::d-empper:RecruitmentFullTimeReplacementMember",
                "d-empf:EmploymentFunctionDimension::d-empf:DepartmentHeadTQMMember/d-empper:EmploymentPeriodDimension::d-empper:EmployedFullTimeMember",
                "d-empf:EmploymentFunctionDimension::d-empf:DepartmentHeadTQMMember/d-empper:EmploymentPeriodDimension::d-empper:EmployedPartTimeMember"
            });
            if (Xbrl.Elements.Count(x => scenarioIds.Contains(x.Context) && !string.IsNullOrEmpty(x.Value)) > 0)
            {
                fiches.Add(new BizTaxFicheInfoDataContract { FicheId = "276W3" });
            }
         
            scenarioIds = GetScenarioIds(new string[] { 
                "d-depm:DepreciationMethodDimension::d-depm:DegressiveDepreciationMethodMember/d-ty:DescriptionTypedDimension/d-ty:NatureTypedDimension"
            });
            if (Xbrl.Elements.Count(x => scenarioIds.Contains(x.Context) && !string.IsNullOrEmpty(x.Value)) > 0)
            {
                fiches.Add(new BizTaxFicheInfoDataContract { FicheId = "328K" });
            }

            scenarioIds = GetScenarioIds(new string[] { 
                "d-depm:DepreciationMethodDimension::d-depm:RenunciationDegressiveDepreciationMethodMember/d-ty:DescriptionTypedDimension/d-ty:NatureTypedDimension"
            });
            if (Xbrl.Elements.Count(x => scenarioIds.Contains(x.Context) && !string.IsNullOrEmpty(x.Value)) > 0)
            {
                fiches.Add(new BizTaxFicheInfoDataContract { FicheId = "328L" });
            }


            this.Xbrl.Fiches = fiches;
        }

        internal decimal? TaxCalculation()
        {
            decimal taxcalc = 0;

            if (Xbrl.Errors.Count > 0)
            {
                return null;
            }

            bool ExclusionReducedRate = Bool(Xbrl.Elements.FirstOrDefault(x => x.Name == "ExclusionReducedRate"));
            bool CreditCorporationTradeEquipmentHousingCorporationTaxRate = Bool(Xbrl.Elements.FirstOrDefault(x => x.Name == "CreditCorporationTradeEquipmentHousingCorporationTaxRate"));

            // UITEENZETTING VAN DE WINST
            decimal BasicTaxableAmountCommonRate = Number(Xbrl.Elements.Where(x => x.Name == "BasicTaxableAmountCommonRate"), "0");

            if (BasicTaxableAmountCommonRate > 0)
            {

                if ((!ExclusionReducedRate && !CreditCorporationTradeEquipmentHousingCorporationTaxRate)
                    || (ExclusionReducedRate && BasicTaxableAmountCommonRate > 322500)
                    )
                {
                    taxcalc += BasicTaxableAmountCommonRate * MaximumRateCorporateTax;
                }
                else if (ExclusionReducedRate)
                {
                    if (BasicTaxableAmountCommonRate <= (decimal)25000)
                    {
                        taxcalc += BasicTaxableAmountCommonRate * (decimal)0.24975;
                    }
                    else
                    {
                        taxcalc += 25000 * (decimal)0.249775;
                        BasicTaxableAmountCommonRate -= 25000;
                        if (BasicTaxableAmountCommonRate <= (decimal)65000)
                        {
                            taxcalc += BasicTaxableAmountCommonRate * (decimal)0.3193;
                        }
                        else
                        {
                            taxcalc += 65000 * (decimal)0.3193;
                            BasicTaxableAmountCommonRate -= 65000;

                            if (BasicTaxableAmountCommonRate <= (decimal)232500)
                            {
                                taxcalc += BasicTaxableAmountCommonRate * (decimal)0.35535;
                            }
                            else
                            {
                                taxcalc += 232500 * (decimal)0.35535;
                                BasicTaxableAmountCommonRate -= 232500;

                            }
                        }
                    }

                }
                else if (CreditCorporationTradeEquipmentHousingCorporationTaxRate)
                {
                    taxcalc += BasicTaxableAmountCommonRate * (decimal)0.0515;
                }
            }


            decimal CapitalGainsSharesRate2500 = Number(Xbrl.Elements.Where(x => x.Name == "CapitalGainsSharesRate2500"), "0");
            if (CapitalGainsSharesRate2500 > 0)
            {
                taxcalc += CapitalGainsSharesRate2500 * (decimal)0.2575;
            }

            decimal BasicTaxableAmountExitTaxRate = Number(Xbrl.Elements.Where(x => x.Name == "BasicTaxableAmountExitTaxRate"), "0");
            if (BasicTaxableAmountExitTaxRate > 0)
            {
                taxcalc += BasicTaxableAmountExitTaxRate * (decimal)0.16995;
            }

            decimal baseForIncrease = 0 + taxcalc;

            decimal CapitalSubsidiesInterestSubsidiesAgriculturalSupportRate500 = Number(Xbrl.Elements.Where(x => x.Name == "CapitalSubsidiesInterestSubsidiesAgriculturalSupportRate500"), "0");
            if (CapitalSubsidiesInterestSubsidiesAgriculturalSupportRate500 > 0)
            {
                taxcalc += CapitalSubsidiesInterestSubsidiesAgriculturalSupportRate500 * (decimal)0.0515;
            }

            //AFZONDERLIJKE AANSLAGEN
            decimal UnjustifiedExpensesHiddenSurplusProfitsFinancialAdvantagesBenefitsAllKind = Number(Xbrl.Elements.Where(x => x.Name == "UnjustifiedExpensesHiddenSurplusProfitsFinancialAdvantagesBenefitsAllKind"), "0");
            if (UnjustifiedExpensesHiddenSurplusProfitsFinancialAdvantagesBenefitsAllKind > 0)
            {
                taxcalc += UnjustifiedExpensesHiddenSurplusProfitsFinancialAdvantagesBenefitsAllKind * (decimal)3.09;
            }


            decimal SeparateAssessmentTaxedReservesCreditAssociationsInstitutionsOtherAuthorisedInstitutionsRate3400 = Number(Xbrl.Elements.Where(x => x.Name == "SeparateAssessmentTaxedReservesCreditAssociationsInstitutionsOtherAuthorisedInstitutionsRate3400"), "0");
            if (SeparateAssessmentTaxedReservesCreditAssociationsInstitutionsOtherAuthorisedInstitutionsRate3400 > 0)
            {
                taxcalc += SeparateAssessmentTaxedReservesCreditAssociationsInstitutionsOtherAuthorisedInstitutionsRate3400 * (decimal)0.3502;
            }

            decimal SeparateAssessmentTaxedReservesCreditAssociationsInstitutionsOtherAuthorisedInstitutionsRate2800 = Number(Xbrl.Elements.Where(x => x.Name == "SeparateAssessmentTaxedReservesCreditAssociationsInstitutionsOtherAuthorisedInstitutionsRate2800"), "0");
            if (SeparateAssessmentTaxedReservesCreditAssociationsInstitutionsOtherAuthorisedInstitutionsRate2800 > 0)
            {
                taxcalc += SeparateAssessmentTaxedReservesCreditAssociationsInstitutionsOtherAuthorisedInstitutionsRate2800 * (decimal)0.2884;
            }

            decimal SeparateAssessmentDividendsPaidCreditCorporationTradeEquipmentHousingCorporation = Number(Xbrl.Elements.Where(x => x.Name == "SeparateAssessmentDividendsPaidCreditCorporationTradeEquipmentHousingCorporation"), "0");
            if (SeparateAssessmentDividendsPaidCreditCorporationTradeEquipmentHousingCorporation > 0)
            {
                taxcalc += SeparateAssessmentDividendsPaidCreditCorporationTradeEquipmentHousingCorporation * (decimal)0.3502;
            }


            // BIJZONDERE AANSLAGEN
            decimal SpecialAssessmentPre1990FullPartialDistributionCompanyAssetsRate3300 = Number(Xbrl.Elements.Where(x => x.Name == "SpecialAssessmentPre1990FullPartialDistributionCompanyAssetsRate3300"), "0");
            if (SpecialAssessmentPre1990FullPartialDistributionCompanyAssetsRate3300 > 0)
            {
                taxcalc += SpecialAssessmentPre1990FullPartialDistributionCompanyAssetsRate3300 * (decimal)0.33;
            }

            decimal SpecialAssessmentPre1990FullPartialDistributionCompanyAssetsRate1650 = Number(Xbrl.Elements.Where(x => x.Name == "SpecialAssessmentPre1990FullPartialDistributionCompanyAssetsRate1650"), "0");
            if (SpecialAssessmentPre1990FullPartialDistributionCompanyAssetsRate1650 > 0)
            {
                taxcalc += SpecialAssessmentPre1990FullPartialDistributionCompanyAssetsRate1650 * (decimal)0.165;
            }

            decimal SpecialAssessmentPre1990BenefitsAllKindCompanyLiquidation = Number(Xbrl.Elements.Where(x => x.Name == "SpecialAssessmentPre1990BenefitsAllKindCompanyLiquidation"), "0");
            if (SpecialAssessmentPre1990BenefitsAllKindCompanyLiquidation > 0)
            {
                taxcalc += SpecialAssessmentPre1990BenefitsAllKindCompanyLiquidation * (decimal)0.45;
            }


            // AANVULLENDE HEFFING
            decimal AdditionalDutiesDiamondTraders = Number(Xbrl.Elements.Where(x => x.Name == "AdditionalDutiesDiamondTraders"), "0");
            if (AdditionalDutiesDiamondTraders > 0)
            {
                taxcalc += AdditionalDutiesDiamondTraders * (decimal)0.15;
            }

            // TOCHECK !!!
            decimal RetributionTaxCreditResearchDevelopment = Number(Xbrl.Elements.Where(x => x.Name == "RetributionTaxCreditResearchDevelopment"), "0");
            if (RetributionTaxCreditResearchDevelopment > 0)
            {
                taxcalc += RetributionTaxCreditResearchDevelopment;
            }

            bool SeparateAssessmentAdditionalIndividualPensionProvisionsSpreadThreeAssessmentYears2013Until2015 = Bool(Xbrl.Elements.FirstOrDefault(x => x.Name == "SeparateAssessmentAdditionalIndividualPensionProvisionsSpreadThreeAssessmentYears2013Until2015"));
            decimal SeparateAssessmentAdditionalIndividualPensionProvisionsOnEndTaxPeriodClosingDateBefore20120101 = Number(Xbrl.Elements.Where(x => x.Name == "SeparateAssessmentAdditionalIndividualPensionProvisionsOnEndTaxPeriodClosingDateBefore2012-01-01"), "0");

            if (SeparateAssessmentAdditionalIndividualPensionProvisionsOnEndTaxPeriodClosingDateBefore20120101 > 0)
            {
                if (SeparateAssessmentAdditionalIndividualPensionProvisionsSpreadThreeAssessmentYears2013Until2015)
                {
                    taxcalc += SeparateAssessmentAdditionalIndividualPensionProvisionsOnEndTaxPeriodClosingDateBefore20120101 * (decimal)0.006;
                }
                else
                {
                    taxcalc += SeparateAssessmentAdditionalIndividualPensionProvisionsOnEndTaxPeriodClosingDateBefore20120101 * (decimal)0.0175;
                }
            }



            taxcalc = Math.Round(taxcalc, 2, MidpointRounding.AwayFromZero);

            // VERREKENBARE VOORHEFFINGEN

            decimal RepayableAdvanceLevies = Number(Xbrl.Elements.Where(x => x.Name == "RepayableAdvanceLevies"), "0");
            decimal TaxCreditResearchDevelopmentClearablePreviousTaxPeriodsRepayableCurrentAssessmentYear = Number(Xbrl.Elements.Where(x => x.Name == "TaxCreditResearchDevelopmentClearablePreviousTaxPeriodsRepayableCurrentAssessmentYear"), "0");
            decimal NonRepayableAdvanceLevies = Number(Xbrl.Elements.Where(x => x.Name == "NonRepayableAdvanceLevies"), "0");

            taxcalc -= RepayableAdvanceLevies;
            taxcalc -= TaxCreditResearchDevelopmentClearablePreviousTaxPeriodsRepayableCurrentAssessmentYear;
            taxcalc -= NonRepayableAdvanceLevies;

            baseForIncrease -= RepayableAdvanceLevies;
            baseForIncrease -= TaxCreditResearchDevelopmentClearablePreviousTaxPeriodsRepayableCurrentAssessmentYear;
            baseForIncrease -= NonRepayableAdvanceLevies;


            bool FirstThreeAccountingYearsSmallCompanyCorporationCode = Bool(Xbrl.Elements.FirstOrDefault(x => x.Name == "FirstThreeAccountingYearsSmallCompanyCorporationCode"));

            if (!FirstThreeAccountingYearsSmallCompanyCorporationCode)
            {
                decimal baseIncrease = baseForIncrease * (decimal)0.0225;

                decimal vooraf = baseIncrease;
                decimal PrepaymentFirstQuarter = Number(Xbrl.Elements.Where(x => x.Name == "PrepaymentFirstQuarter"), "0");
                decimal PrepaymentSecondQuarter = Number(Xbrl.Elements.Where(x => x.Name == "PrepaymentSecondQuarter"), "0");
                decimal PrepaymentThirdQuarter = Number(Xbrl.Elements.Where(x => x.Name == "PrepaymentThirdQuarter"), "0");
                decimal PrepaymentFourthQuarter = Number(Xbrl.Elements.Where(x => x.Name == "PrepaymentFourthQuarter"), "0");

                vooraf -= PrepaymentFirstQuarter * (decimal)0.03;
                vooraf -= PrepaymentSecondQuarter * (decimal)0.025;
                vooraf -= PrepaymentThirdQuarter * (decimal)0.02;
                vooraf -= PrepaymentFourthQuarter * (decimal)0.015;
                vooraf = Math.Round(vooraf, 2, MidpointRounding.AwayFromZero);
                if (vooraf < (baseForIncrease * (decimal)0.01))
                {
                    vooraf = 0;
                }

                decimal increase = 0;
                if (vooraf > 40)
                {
                    increase = vooraf;
                }
                taxcalc += increase;

            }

            decimal Prepayments = Number(Xbrl.Elements.Where(x => x.Name == "Prepayments"), "0");

            taxcalc -= Prepayments;

            return taxcalc;
           
        }


     internal void ApplyExemptions()
        {
            string scenarioId = GetScenarioId("");
            string period = "D";
            var RemainingFiscalResultBelgium = Number(GetElementsByScenDefs(new string[] { "d-origin:OriginDimension::d-origin:BelgiumMember" }, new string[] { "D" }, new string[] { "tax-inc:RemainingFiscalResult" }));
            var RemainingFiscalResultNoTaxTreaty = Number(GetElementsByScenDefs(new string[] { "d-origin:OriginDimension::d-origin:NoTaxTreatyMember" }, new string[] { "D" }, new string[] { "tax-inc:RemainingFiscalResult" }));

            // SOURCES
            // Niet belastbare bestanddelen
            var MiscExemptionsSource = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Period == "D" && x.Name == "DeductibleMiscellaneousExemptions"));
            // DBI
            var DBISource = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Period == "D" && x.Name == "AccumulatedPEExemptIncomeMovableAssets"));
            // aftrek octrooi inkomsten
            var PatentsIncomeSource = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Period == "D" && x.Name == "DeductibleDeductionPatentsIncome"));
            // NID (Aftrek risicokapitaal)
            var AllowanceCorporateEquitySource = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Period == "D" && x.Name == "DeductionsBranchImmovablePropertyInEEAOutsideEEATreatyAfterDeductions"));
            // Gecompenseerde verliezen / vorige verliezen
            var CompensatedLossesSource = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Period == "D" && x.Name == "CompensableTaxLosses"));
            // InvesteringsAftrek
            var InvestDeductSource1 = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Period == "D" && x.Name == "BasicTaxableInvestmentDeductionCompatibleTaxCreditResearchDevelopment"));
            var InvestDeductSource2 = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Period == "D" && x.Name == "BasicTaxableInvestmentDeduction"));
            // NID History : Aftrek Risicokapitaal historiek
            var AllowanceCorporateEquityHistorySource = this.Number(this.Xbrl.Elements.FirstOrDefault(x => x.Period == "D" && x.Name == "ExemptionsAllowanceCorporateEquityAccumulatedUpToAndIncludingAssessmentYear2012"));
            CompensatedLossesSource = Math.Abs(CompensatedLossesSource);
            // TARGETS
            
            var MiscExemptionsBelgium = CreateNewCalculationElement(null, new string[] { "D" }, "tax-inc:MiscellaneousExemptions", new string[] { "d-origin:OriginDimension::d-origin:BelgiumMember" }, "EUR", "INF").First(); 
            var MiscExemptionNoTaxTreaty = CreateNewCalculationElement(null, new string[] { "D" }, "tax-inc:MiscellaneousExemptions", new string[] { "d-origin:OriginDimension::d-origin:NoTaxTreatyMember" }, "EUR", "INF").First();

            var DBIBelgium= CreateNewCalculationElement(null, new string[] { "D" }, "tax-inc:PEExemptIncomeMovableAssets", new string[] { "d-origin:OriginDimension::d-origin:BelgiumMember" }, "EUR", "INF").First();
            var DBINoTaxTreaty = CreateNewCalculationElement(null, new string[] { "D" }, "tax-inc:PEExemptIncomeMovableAssets", new string[] { "d-origin:OriginDimension::d-origin:NoTaxTreatyMember" }, "EUR", "INF").First();

            var PatentsIncomeBelgium = CreateNewCalculationElement(null, new string[] { "D" }, "tax-inc:DeductionPatentsIncome", new string[] { "d-origin:OriginDimension::d-origin:BelgiumMember" }, "EUR", "INF").First();
            var PatentsIncomeNoTaxTreaty = CreateNewCalculationElement(null, new string[] { "D" }, "tax-inc:DeductionPatentsIncome", new string[] { "d-origin:OriginDimension::d-origin:NoTaxTreatyMember" }, "EUR", "INF").First();

            var AllowanceCorporateEquityBelgium = CreateNewCalculationElement(null, new string[] { "D" }, "tax-inc:AllowanceCorporateEquity", new string[] { "d-origin:OriginDimension::d-origin:BelgiumMember" }, "EUR", "INF").First();
            var AllowanceCorporateEquityNoTaxTreaty = CreateNewCalculationElement(null, new string[] { "D" }, "tax-inc:AllowanceCorporateEquity", new string[] { "d-origin:OriginDimension::d-origin:NoTaxTreatyMember" }, "EUR", "INF").First();

            var CompensatedLossesBelgium = CreateNewCalculationElement(null, new string[] { "D" }, "tax-inc:CompensatedTaxLosses", new string[] { "d-origin:OriginDimension::d-origin:BelgiumMember" }, "EUR", "INF").First();
            var CompensatedLossesNoTaxTreaty = CreateNewCalculationElement(null, new string[] { "D" }, "tax-inc:CompensatedTaxLosses", new string[] { "d-origin:OriginDimension::d-origin:NoTaxTreatyMember" }, "EUR", "INF").First();

            var InvestDeductBelgium = CreateNewCalculationElement(null, new string[] { "D" }, "tax-inc:AllowanceInvestmentDeduction", new string[] { "d-origin:OriginDimension::d-origin:BelgiumMember" }, "EUR", "INF").First();

            var AllowanceCorporateEquityHistoryBelgium = CreateNewCalculationElement(null, new string[] { "D" }, "tax-inc:DeductionExemptionsAllowanceCorporateEquityAccumulatedUpToAndIncludingAssessmentYear2012", new string[] { "d-origin:OriginDimension::d-origin:BelgiumMember" }, "EUR", "INF").First();
            var AllowanceCorporateEquityHistoryNoTaxTreaty = CreateNewCalculationElement(null, new string[] { "D" }, "tax-inc:DeductionExemptionsAllowanceCorporateEquityAccumulatedUpToAndIncludingAssessmentYear2012", new string[] { "d-origin:OriginDimension::d-origin:NoTaxTreatyMember" }, "EUR", "INF").First();



            // TOTALs
            // NID (Aftrek risicokapitaal)
            var AllowanceCorporateEquityResult = CreateNewCalculationElement(null, new string[] { "D" }, "tax-inc:AllowanceCorporateEquity", new string[] { "" }, "EUR", "INF").First();
            // Gecompenseerde verliezen / vorige verliezen
            var CompensatedLossesResult = CreateNewCalculationElement(null, new string[] { "D" }, "tax-inc:CompensatedTaxLossesIncludingTaxTreaty", new string[] { "" }, "EUR", "INF").First();

            var InvestDeductResult1 = CreateNewCalculationElement(null, new string[] { "D" }, "tax-inc:DeductibleAllowanceInvestmentDeductionCompatibleTaxCreditResearchDevelopment", new string[] { "" }, "EUR", "INF").First();
            var InvestDeductResult2 = CreateNewCalculationElement(null, new string[] { "D" }, "tax-inc:DeductibleAllowanceInvestmentDeductionIncompatibleTaxCreditResearchDevelopment", new string[] { "" }, "EUR", "INF").First();

            var AllowanceCorporateEquityHistoryResult = CreateNewCalculationElement(null, new string[] { "D" }, "tax-inc:DeductionExemptionsAllowanceCorporateEquityAccumulatedUpToAndIncludingAssessmentYear2012", new string[] { "" }, "EUR", "INF").First();


            //if (MiscExemptionsBelgium == null)
            //{
            //    MiscExemptionsBelgium = 
            //}
            //if (MiscExemptionNoTaxTreaty == null)
            //{
            //    MiscExemptionNoTaxTreaty = 
            //}

            // RESET
            
            MiscExemptionsBelgium.SetNumber(0);
            MiscExemptionNoTaxTreaty.SetNumber(0);
            DBIBelgium.SetNumber(0);
            DBINoTaxTreaty.SetNumber(0);
            PatentsIncomeBelgium.SetNumber(0);
            PatentsIncomeNoTaxTreaty.SetNumber(0);
            AllowanceCorporateEquityBelgium.SetNumber(0);
            AllowanceCorporateEquityNoTaxTreaty.SetNumber(0);
            CompensatedLossesBelgium.SetNumber(0);
            CompensatedLossesNoTaxTreaty.SetNumber(0);
            InvestDeductBelgium.SetNumber(0);
            AllowanceCorporateEquityHistoryBelgium.SetNumber(0);
            AllowanceCorporateEquityHistoryNoTaxTreaty.SetNumber(0);

            MiscExemptionsBelgium.Locked = true;
            MiscExemptionNoTaxTreaty.Locked = true;
            DBIBelgium.Locked = true;
            DBINoTaxTreaty.Locked = true;
            PatentsIncomeBelgium.Locked = true;
            PatentsIncomeNoTaxTreaty.Locked = true;
            AllowanceCorporateEquityBelgium.Locked = true;
            AllowanceCorporateEquityNoTaxTreaty.Locked = true;
            CompensatedLossesBelgium.Locked = true;
            CompensatedLossesNoTaxTreaty.Locked = true;
            InvestDeductBelgium.Locked = true;
            AllowanceCorporateEquityHistoryBelgium.Locked = true;
            AllowanceCorporateEquityHistoryNoTaxTreaty.Locked = true;
            
          


            // NIET BELASTBARE BESTANDDELEN
            if (RemainingFiscalResultBelgium > 0 && MiscExemptionsSource>0)
            {
                var exempted = RemainingFiscalResultBelgium > MiscExemptionsSource ? MiscExemptionsSource : RemainingFiscalResultBelgium;
                MiscExemptionsBelgium.SetNumber(exempted);
                MiscExemptionsSource -= exempted;
                RemainingFiscalResultBelgium -= exempted;

            }
            if (RemainingFiscalResultNoTaxTreaty > 0 && MiscExemptionsSource > 0)
            {
                var exempted = RemainingFiscalResultNoTaxTreaty > MiscExemptionsSource ? MiscExemptionsSource : RemainingFiscalResultNoTaxTreaty;
                MiscExemptionNoTaxTreaty.SetNumber(exempted);
                MiscExemptionsSource -= exempted;
                RemainingFiscalResultNoTaxTreaty -= exempted;

            }

            // DBI
            if (RemainingFiscalResultBelgium > 0 && DBISource > 0)
            {
                var exempted = RemainingFiscalResultBelgium > DBISource ? DBISource : RemainingFiscalResultBelgium;
                DBIBelgium.SetNumber(exempted);
                DBISource -= exempted;
                RemainingFiscalResultBelgium -= exempted;

            }
            if (RemainingFiscalResultNoTaxTreaty > 0 && DBISource > 0)
            {
                var exempted = RemainingFiscalResultNoTaxTreaty > DBISource ? DBISource : RemainingFiscalResultNoTaxTreaty;
                DBINoTaxTreaty.SetNumber(exempted);
                DBISource -= exempted;
                RemainingFiscalResultNoTaxTreaty -= exempted;

            }

            // AFTREK OCTROOIINKOMSTEN
            if (RemainingFiscalResultBelgium > 0 && PatentsIncomeSource > 0)
            {
                var exempted = RemainingFiscalResultBelgium > PatentsIncomeSource ? PatentsIncomeSource : RemainingFiscalResultBelgium;
                PatentsIncomeBelgium.SetNumber(exempted);
                PatentsIncomeSource -= exempted;
                RemainingFiscalResultBelgium -= exempted;

            }
            if (RemainingFiscalResultNoTaxTreaty > 0 && PatentsIncomeSource > 0)
            {
                var exempted = RemainingFiscalResultNoTaxTreaty > PatentsIncomeSource ? PatentsIncomeSource : RemainingFiscalResultNoTaxTreaty;
                PatentsIncomeNoTaxTreaty.SetNumber(exempted);
                PatentsIncomeSource -= exempted;
                RemainingFiscalResultNoTaxTreaty -= exempted;

            }

            // NID (aftrek risicokapitaal)
            if (RemainingFiscalResultBelgium > 0 && AllowanceCorporateEquitySource > 0)
            {
                var exempted = RemainingFiscalResultBelgium > AllowanceCorporateEquitySource ? AllowanceCorporateEquitySource : RemainingFiscalResultBelgium;
                AllowanceCorporateEquityBelgium.SetNumber(exempted);
                AllowanceCorporateEquitySource -= exempted;
                RemainingFiscalResultBelgium -= exempted;

            }
            if (RemainingFiscalResultNoTaxTreaty > 0 && AllowanceCorporateEquitySource > 0)
            {
                var exempted = RemainingFiscalResultNoTaxTreaty > AllowanceCorporateEquitySource ? AllowanceCorporateEquitySource : RemainingFiscalResultNoTaxTreaty;
                AllowanceCorporateEquityNoTaxTreaty.SetNumber(exempted);
                AllowanceCorporateEquitySource -= exempted;
                RemainingFiscalResultNoTaxTreaty -= exempted;

            }
            // SET RESULT USED
            AllowanceCorporateEquityResult.SetNumber(AllowanceCorporateEquityBelgium.GetNumberOrDefault(0) + AllowanceCorporateEquityNoTaxTreaty.GetNumberOrDefault(0));

            // Gecompenseerde verliezen
            if (RemainingFiscalResultBelgium > 0 && CompensatedLossesSource > 0)
            {
                var exempted = RemainingFiscalResultBelgium > CompensatedLossesSource ? CompensatedLossesSource : RemainingFiscalResultBelgium;
                CompensatedLossesBelgium.SetNumber(exempted);
                CompensatedLossesSource -= exempted;
                RemainingFiscalResultBelgium -= exempted;

            }
            if (RemainingFiscalResultNoTaxTreaty > 0 && CompensatedLossesSource > 0)
            {
                var exempted = RemainingFiscalResultNoTaxTreaty > CompensatedLossesSource ? CompensatedLossesSource : RemainingFiscalResultNoTaxTreaty;
                CompensatedLossesNoTaxTreaty.SetNumber(exempted);
                CompensatedLossesSource -= exempted;
                RemainingFiscalResultNoTaxTreaty -= exempted;

            }
            // SET RESULT USED
            CompensatedLossesResult.SetNumber(CompensatedLossesBelgium.GetNumberOrDefault(0) + CompensatedLossesNoTaxTreaty.GetNumberOrDefault(0));

            InvestDeductResult1.SetNumber(0);
            InvestDeductResult2.SetNumber(0);
            if (InvestDeductSource1 > 0)
            {
                if (RemainingFiscalResultBelgium > 0 && InvestDeductSource1 > 0)
                {
                    var exempted = RemainingFiscalResultBelgium > InvestDeductSource1 ? InvestDeductSource1 : RemainingFiscalResultBelgium;
                    InvestDeductBelgium.SetNumber(exempted);
                    InvestDeductSource1 -= exempted;
                    RemainingFiscalResultBelgium -= exempted;
                    InvestDeductResult1.SetNumber(exempted);
                }

            }
            else if (InvestDeductSource2 > 0)
            {
                if (RemainingFiscalResultBelgium > 0 && InvestDeductSource2 > 0)
                {
                    var exempted = RemainingFiscalResultBelgium > InvestDeductSource2 ? InvestDeductSource2 : RemainingFiscalResultBelgium;
                    InvestDeductBelgium.SetNumber(exempted);
                    InvestDeductSource2 -= exempted;
                    RemainingFiscalResultBelgium -= exempted;
                    InvestDeductResult2.SetNumber(exempted);

                }

            }


            // NID (aftrek risicokapitaal) HISTORIEK
            if (RemainingFiscalResultBelgium > 0 && AllowanceCorporateEquityHistorySource > 0)
            {
                var exempted = RemainingFiscalResultBelgium > AllowanceCorporateEquityHistorySource ? AllowanceCorporateEquityHistorySource : RemainingFiscalResultBelgium;
                AllowanceCorporateEquityHistoryBelgium.SetNumber(exempted);
                AllowanceCorporateEquityHistorySource -= exempted;
                RemainingFiscalResultBelgium -= exempted;

            }
            if (RemainingFiscalResultNoTaxTreaty > 0 && AllowanceCorporateEquityHistorySource > 0)
            {
                var exempted = RemainingFiscalResultNoTaxTreaty > AllowanceCorporateEquityHistorySource ? AllowanceCorporateEquityHistorySource : RemainingFiscalResultNoTaxTreaty;
                AllowanceCorporateEquityHistoryNoTaxTreaty.SetNumber(exempted);
                AllowanceCorporateEquityHistorySource -= exempted;
                RemainingFiscalResultNoTaxTreaty -= exempted;

            }
            // SET RESULT USED
            AllowanceCorporateEquityHistoryResult.SetNumber(AllowanceCorporateEquityHistoryBelgium.GetNumberOrDefault(0) + AllowanceCorporateEquityHistoryNoTaxTreaty.GetNumberOrDefault(0));
        }
    }
}
