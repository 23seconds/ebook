﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System;
using System.Xml;
using System.Xml.Linq;
using EY.com.eBook.BizTax.Contracts;
using EY.com.eBook.API.Contracts.Data.BizTax;

namespace EY.com.eBook.BizTax.AY2013.nrcorp
{
    public class BizTaxRenderer : BizTaxRendererGenerated
    {
        public List<XmlQualifiedName> NameSpaces
        {
            get
            {
                return new List<XmlQualifiedName> {
          
            new XmlQualifiedName("xlink","http://www.w3.org/1999/xlink")
              ,
            new XmlQualifiedName("","http://www.xbrl.org/2003/instance")
          ,
            new XmlQualifiedName("iso4217","http://www.xbrl.org/2003/iso4217")
          ,
            new XmlQualifiedName("xsi","http://www.w3.org/2001/XMLSchema-instance")
          ,
            new XmlQualifiedName("link","http://www.xbrl.org/2003/linkbase")
   
          ,
            new XmlQualifiedName("xbrldi","http://xbrl.org/2006/xbrldi")
          ,
            new XmlQualifiedName("xbrldt","http://xbrl.org/2005/xbrldt")
          ,
            new XmlQualifiedName("tax-inc-nrcorp","http://www.minfin.fgov.be/be/tax/inc/nrcorp/2013-04-30")
          ,
            new XmlQualifiedName("xl","http://www.xbrl.org/2003/XLink")
          ,
            new XmlQualifiedName("t-reso","http://www.minfin.fgov.be/be/tax/t/reso/2013-04-30")
          ,
            new XmlQualifiedName("d-ty","http://www.minfin.fgov.be/be/tax/d/ty/2013-04-30")
          ,
            new XmlQualifiedName("tax-inc","http://www.minfin.fgov.be/be/tax/inc/2013-04-30")
          ,
            new XmlQualifiedName("pfs-dt","http://www.nbb.be/be/fr/pfs/ci/dt/2013-04-01")
          ,
            new XmlQualifiedName("ref","http://www.xbrl.org/2006/ref")
          ,
            new XmlQualifiedName("t-drfa","http://www.minfin.fgov.be/be/tax/t/drfa/2013-04-30")
          ,
            new XmlQualifiedName("t-wddc","http://www.minfin.fgov.be/be/tax/t/wddc/2013-04-30")
          ,
            new XmlQualifiedName("d-hh","http://www.minfin.fgov.be/be/tax/d/hh/2013-04-30")
          ,
            new XmlQualifiedName("d-ec","http://www.minfin.fgov.be/be/tax/d/ec/2013-04-30")
          ,
            new XmlQualifiedName("t-prre","http://www.minfin.fgov.be/be/tax/t/prre/2013-04-30")
          ,
            new XmlQualifiedName("d-expt","http://www.minfin.fgov.be/be/tax/d/expt/2013-04-30")
          ,
            new XmlQualifiedName("t-cg","http://www.minfin.fgov.be/be/tax/t/cg/2013-04-30")
          ,
            new XmlQualifiedName("d-asst","http://www.minfin.fgov.be/be/tax/d/asst/2013-04-30")
          ,
            new XmlQualifiedName("t-ricg","http://www.minfin.fgov.be/be/tax/t/ricg/2013-04-30")
          ,
            new XmlQualifiedName("t-stcg","http://www.minfin.fgov.be/be/tax/t/stcg/2013-04-30")
          ,
            new XmlQualifiedName("t-ace","http://www.minfin.fgov.be/be/tax/t/ace/2013-04-30")
          ,
            new XmlQualifiedName("t-pcs","http://www.minfin.fgov.be/be/tax/t/pcs/2013-04-30")
          ,
            new XmlQualifiedName("t-dpi","http://www.minfin.fgov.be/be/tax/t/dpi/2013-04-30")
          ,
            new XmlQualifiedName("t-itifa","http://www.minfin.fgov.be/be/tax/t/itifa/2013-04-30")
          ,
            new XmlQualifiedName("d-per","http://www.minfin.fgov.be/be/tax/d/per/2013-04-30")
          ,
            new XmlQualifiedName("t-idtc","http://www.minfin.fgov.be/be/tax/t/idtc/2013-04-30")
          ,
            new XmlQualifiedName("d-incc","http://www.minfin.fgov.be/be/tax/d/incc/2013-04-30")
          ,
            new XmlQualifiedName("d-invc","http://www.minfin.fgov.be/be/tax/d/invc/2013-04-30")
          ,
            new XmlQualifiedName("t-eap","http://www.minfin.fgov.be/be/tax/t/eap/2013-04-30")
          ,
            new XmlQualifiedName("d-empstat","http://www.minfin.fgov.be/be/tax/d/empstat/2013-04-30")
          ,
            new XmlQualifiedName("d-empreg","http://www.minfin.fgov.be/be/tax/d/empreg/2013-04-30")
          ,
            new XmlQualifiedName("d-tu","http://www.minfin.fgov.be/be/tax/d/tu/2013-04-30")
          ,
            new XmlQualifiedName("t-eapsf","http://www.minfin.fgov.be/be/tax/t/eapsf/2013-04-30")
          ,
            new XmlQualifiedName("d-empf","http://www.minfin.fgov.be/be/tax/d/empf/2013-04-30")
          ,
            new XmlQualifiedName("d-empper","http://www.minfin.fgov.be/be/tax/d/empper/2013-04-30")
          ,
            new XmlQualifiedName("t-dfa","http://www.minfin.fgov.be/be/tax/t/dfa/2013-04-30")
          ,
            new XmlQualifiedName("d-depm","http://www.minfin.fgov.be/be/tax/d/depm/2013-04-30")
          ,
            new XmlQualifiedName("d-br","http://www.minfin.fgov.be/be/tax/d/br/2013-04-30")
          ,
            new XmlQualifiedName("d-origin","http://www.minfin.fgov.be/be/tax/d/origin/2013-04-30")
          ,
            new XmlQualifiedName("pfs-gcd","http://www.nbb.be/be/fr/pfs/ci/gcd/2013-04-01")
          ,
            new XmlQualifiedName("pfs-vl","http://www.nbb.be/be/fr/pfs/ci/vl/2013-04-01")
          
        };
            }

        }
        

        public XDocument RenderBizTaxFile(BizTaxDataContract btdc, Guid fileId)
        {
            return RenderBizTaxFile(btdc, fileId, NameSpaces,TaxonomySchemaRef_nrcorp);
        }

        /*
        internal override void Calc_f_prre_5050_assertion_set()
        {
            List<ContextElementDataContract> contexts = GetContexts(new List<string> { "I-End" },
                new List<string> { "d-ec:ExemptionCategoryDimension::d-ec:RemainingBalanceAfterDecreaseProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ExpensesCompensationCoveredCurrentTaxPeriodMember/d-ty:ProvisionRiskExpenseTypedDimension",
                    "d-ec:ExemptionCategoryDimension::d-ec:RemainingBalanceAfterDecreaseProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ExpensesOverhaulImportantRepairsMaximum10YearsMember/d-ty:ProvisionRiskExpenseTypedDimension",
                    "d-ec:ExemptionCategoryDimension::d-ec:RemainingBalanceAfterDecreaseProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ProfessionalExpensesCurrentTaxPeriodMember/d-ty:ProvisionRiskExpenseTypedDimension",
                   });
            foreach (ContextElementDataContract context in contexts)
            {
                var ExemptProvisionRisksExpensesEnd = FilterElementsByContext(context, new List<string> { "tax-inc:ExemptProvisionRisksExpenses" }, new List<string> { "I-End" }, null, null);
                var ExemptProvisionRisksExpensesStart = FilterElementsByContext(context, new List<string> { "tax-inc:ExemptProvisionRisksExpenses" }, new List<string> { "I-Start" }, null, null);
                var IncreaseExemptProvisionRisksExpenses = FilterElementsByContext(context, new List<string> { "tax-inc:IncreaseExemptProvisionRisksExpenses" }, new List<string> { "D" }, new List<DimensionFilter> { new DimensionFilter { Invert = false, Dimension = "d-ec:ExemptionCategoryDimension" }, new DimensionFilter { Invert = true, Dimension = "d-ec:ExemptionCategoryDimension", Values = new List<string> { "d-ec:NewProvisionRisksExpensesMember" } } }, new List<string> { "d-ec:ExemptionCategoryDimension::d-ec:UnalteredBalanceProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ExpensesCompensationCoveredCurrentTaxPeriodMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:UnalteredBalanceProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ExpensesOverhaulImportantRepairsMaximum10YearsMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:UnalteredBalanceProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ProfessionalExpensesCurrentTaxPeriodMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:RemainingBalanceAfterDecreaseProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ExpensesCompensationCoveredCurrentTaxPeriodMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:RemainingBalanceAfterDecreaseProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ExpensesOverhaulImportantRepairsMaximum10YearsMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:RemainingBalanceAfterDecreaseProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ProfessionalExpensesCurrentTaxPeriodMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:RemainingBalanceAfterIncreaseProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ExpensesCompensationCoveredCurrentTaxPeriodMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:RemainingBalanceAfterIncreaseProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ExpensesOverhaulImportantRepairsMaximum10YearsMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:RemainingBalanceAfterIncreaseProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ProfessionalExpensesCurrentTaxPeriodMember/d-ty:ProvisionRiskExpenseTypedDimension" });
                var DecreaseExemptProvisionRisksExpensesBorneExpensesCurrentTaxPeriod = FilterElementsByContext(context, new List<string> { "tax-inc:DecreaseExemptProvisionRisksExpensesBorneExpensesCurrentTaxPeriod" }, new List<string> { "D" }, new List<DimensionFilter> { new DimensionFilter { Invert = false, Dimension = "d-ec:ExemptionCategoryDimension" }, new DimensionFilter { Invert = true, Dimension = "d-ec:ExemptionCategoryDimension", Values = new List<string> { "d-ec:NewProvisionRisksExpensesMember" } } }, new List<string> { "d-ec:ExemptionCategoryDimension::d-ec:UnalteredBalanceProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ExpensesCompensationCoveredCurrentTaxPeriodMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:UnalteredBalanceProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ExpensesOverhaulImportantRepairsMaximum10YearsMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:UnalteredBalanceProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ProfessionalExpensesCurrentTaxPeriodMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:RemainingBalanceAfterDecreaseProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ExpensesCompensationCoveredCurrentTaxPeriodMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:RemainingBalanceAfterDecreaseProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ExpensesOverhaulImportantRepairsMaximum10YearsMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:RemainingBalanceAfterDecreaseProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ProfessionalExpensesCurrentTaxPeriodMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:RemainingBalanceAfterIncreaseProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ExpensesCompensationCoveredCurrentTaxPeriodMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:RemainingBalanceAfterIncreaseProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ExpensesOverhaulImportantRepairsMaximum10YearsMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:RemainingBalanceAfterIncreaseProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ProfessionalExpensesCurrentTaxPeriodMember/d-ty:ProvisionRiskExpenseTypedDimension" });
                var DecreaseExemptProvisionRisksExpensesNewEstimationProbableExpenses = FilterElementsByContext(context, new List<string> { "tax-inc:DecreaseExemptProvisionRisksExpensesNewEstimationProbableExpenses" }, new List<string> { "D" }, new List<DimensionFilter> { new DimensionFilter { Invert = false, Dimension = "d-ec:ExemptionCategoryDimension" }, new DimensionFilter { Invert = true, Dimension = "d-ec:ExemptionCategoryDimension", Values = new List<string> { "d-ec:NewProvisionRisksExpensesMember" } } }, new List<string> { "d-ec:ExemptionCategoryDimension::d-ec:UnalteredBalanceProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ExpensesCompensationCoveredCurrentTaxPeriodMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:UnalteredBalanceProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ExpensesOverhaulImportantRepairsMaximum10YearsMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:UnalteredBalanceProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ProfessionalExpensesCurrentTaxPeriodMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:RemainingBalanceAfterDecreaseProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ExpensesCompensationCoveredCurrentTaxPeriodMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:RemainingBalanceAfterDecreaseProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ExpensesOverhaulImportantRepairsMaximum10YearsMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:RemainingBalanceAfterDecreaseProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ProfessionalExpensesCurrentTaxPeriodMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:RemainingBalanceAfterIncreaseProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ExpensesCompensationCoveredCurrentTaxPeriodMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:RemainingBalanceAfterIncreaseProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ExpensesOverhaulImportantRepairsMaximum10YearsMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:RemainingBalanceAfterIncreaseProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ProfessionalExpensesCurrentTaxPeriodMember/d-ty:ProvisionRiskExpenseTypedDimension" });
                if (ExemptProvisionRisksExpensesStart.Count > 0 || ExemptProvisionRisksExpensesEnd.Count > 0 || IncreaseExemptProvisionRisksExpenses.Count > 0 || DecreaseExemptProvisionRisksExpensesBorneExpensesCurrentTaxPeriod.Count > 0 || DecreaseExemptProvisionRisksExpensesNewEstimationProbableExpenses.Count > 0)
                {
                    var PeriodStartDate = GetNodeByName("tax-inc", "PeriodStartDate");
                    var PeriodEndDate = GetNodeByName("tax-inc", "PeriodEndDate");
                    var CalculatedAmountExemptProvisionRisksExpenses = Sum(Number(ExemptProvisionRisksExpensesEnd, "0"), -Number(ExemptProvisionRisksExpensesStart, "0"));
                    var CalculatedAmountDecreaseExemptProvisionRisksExpenses = Sum(Number(DecreaseExemptProvisionRisksExpensesBorneExpensesCurrentTaxPeriod, "0"), Number(DecreaseExemptProvisionRisksExpensesNewEstimationProbableExpenses, "0"));
                    //CALCULATION HERE
                    //FORMULA HERE
                    bool test = (Sum(Number(ExemptProvisionRisksExpensesEnd, "0"), -Number(ExemptProvisionRisksExpensesStart, "0")) == Number(IncreaseExemptProvisionRisksExpenses, "0")) || (Sum(Number(ExemptProvisionRisksExpensesEnd, "0"), -Number(ExemptProvisionRisksExpensesStart, "0")) == Sum(-Number(DecreaseExemptProvisionRisksExpensesBorneExpensesCurrentTaxPeriod, "0"), -Number(DecreaseExemptProvisionRisksExpensesNewEstimationProbableExpenses, "0")));
                    if (!test)
                    {
                        // MESSAGES
                        AddMessage(new BizTaxErrorDataContract
                        {
                            FileId = _fileId,
                            Id = "f-prre-5050",
                            Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Het verschil tussen de vrijgestelde voorziening bij het begin van het belastbare tijdperk ({$ExemptProvisionRisksExpensesStart}) en op het einde van het belastbare tijdperk ({$ExemptProvisionRisksExpensesEnd}) 	moet bij een vermindering (= {$CalculatedAmountDecreaseExemptProvisionRisksExpenses}) overeenstemmen met de 	'{Vermindering van de vrijgestelde voorziening ingevolge kosten die tijdens het boekjaar effectief gedragen zijn}' 	en/of 	'{Vermindering van de vrijgestelde voorziening ingevolge een nieuwe schatting van de waarschijnlijke kosten tijdens het boekjaar}', 	OF 	bij een verhoging met het bedrag ingevuld in de kolom 	'{Verhoging van de vrijgestelde voorziening}' ({$IncreaseExemptProvisionRisksExpenses})  (= {$CalculatedAmountExemptProvisionRisksExpenses})."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "La différence entre la provision exonérée au début de l'exercice imposable ({$ExemptProvisionRisksExpensesStart}) et à la fin de l'exercice imposable ({$ExemptProvisionRisksExpensesEnd}) 	doit correspondre en cas de diminution (= {$CalculatedAmountDecreaseExemptProvisionRisksExpenses}) à la 	'{Diminution de la provision exonérée résultant de la prise en charge effective au cours de l'exercice comptable}' 	et/ou la  	'{Diminution de la provision exonérée résultant d'une nouvelle estimation de la charge probable au cours de l'exercice comptable}', 	OU en cas d'augmentation au montant complété dans la colonne 		'{Augmentation de la provision exonérée}' ({$IncreaseExemptProvisionRisksExpenses})  (= {$CalculatedAmountExemptProvisionRisksExpenses})."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Der Unterschied zwischen den steuerfreien Rückstellungen am Anfang ({$ExemptProvisionRisksExpensesStart}) und am Ende ({$ExemptProvisionRisksExpensesEnd}) 	des Besteuerungszeitraums muss im Falle einer Verringerung (= {$CalculatedAmountDecreaseExemptProvisionRisksExpenses}) der Rubrik 	'{Die Verringerung, die sich aus den während des Rechnungsjahres, worauf sich das Verzeichnis bezieht, effektiv getragenen Kosten ergeben}' 	und/oder 	'{Die Verringerung, die sich aus einer Neueinschätzung der wahrscheinlichen Kosten ergeben}', 	entsprechen ODER im Falle einer Erhöhung dem in der Spalte	'{Erhöhung der steuerbefreiten Rückstellung}' ({$IncreaseExemptProvisionRisksExpenses})  (= {$CalculatedAmountExemptProvisionRisksExpenses}) angegebenen Betrag entsprechen."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Het verschil tussen de vrijgestelde voorziening bij het begin van het belastbare tijdperk ({$ExemptProvisionRisksExpensesStart}) en op het einde van het belastbare tijdperk ({$ExemptProvisionRisksExpensesEnd}) 	moet bij een vermindering (= {$CalculatedAmountDecreaseExemptProvisionRisksExpenses}) overeenstemmen met de 	'{Vermindering van de vrijgestelde voorziening ingevolge kosten die tijdens het boekjaar effectief gedragen zijn}' 	en/of 	'{Vermindering van de vrijgestelde voorziening ingevolge een nieuwe schatting van de waarschijnlijke kosten tijdens het boekjaar}', 	OF 	bij een verhoging met het bedrag ingevuld in de kolom 	'{Verhoging van de vrijgestelde voorziening}' ({$IncreaseExemptProvisionRisksExpenses})  (= {$CalculatedAmountExemptProvisionRisksExpenses})."}  
 }
                            ,
                            Fields = new List<string>()
                        }
                        );
                    }
                }
            }

        }
         */

        public override BizTaxDataContract CalculateAndValidate()
        {
            base.CalculateAndValidate();
           
            Extra_275P_PatentsIncomeCheck();
            Extra_275U_TaxCreditResearchDevelopmentCheck();
            Extra_276Ws();
            // EXTRA'S

            return this.Xbrl;
        }

        #region Extra checks
       

        internal void Extra_275P_PatentsIncomeCheck()
        {
            var DeductibleDeductionPatentsIncomeBelgium = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:DeductionPatentsIncome" });
           var DeductibleDeductionPatentsIncome275P = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:DeductibleDeductionPatentsIncome" });

            var calced = Sum(Number(DeductibleDeductionPatentsIncomeBelgium, "0"));
            bool test = calced == Number(DeductibleDeductionPatentsIncome275P, "0");

            if (!test)
            {
                // MESSAGES
                AddMessage(new BizTaxErrorDataContract
                {
                    FileId = _fileId,
                    Id = "f-xtra-nrcorp-275p-patincome",
                    Messages = new List<BizTaxErrorMessageDataContract> { 
                        new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = string.Format("Het totaal vermeld in formulier 275P, 'Aftrek voor octrooi-inkomsten' ({0}€) is niet gelijk aan het bedrag ({1}€) in het vak 'Uiteenzetting van de winst'.",Number(DeductibleDeductionPatentsIncome275P,"0").ToString("N",DefaultCulture),Number(DeductibleDeductionPatentsIncomeBelgium,"0").ToString("N",DefaultCulture),Number(calced).ToString("N",DefaultCulture))} , 

                        new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = string.Format("Le total repris en formulaire 275P, 'Déduction pour revenus de brevets' ({0}€) n'est pas égal au montant ({1}€) dans la rubrique 'Détail des bénéfices'.",Number(DeductibleDeductionPatentsIncome275P,"0").ToString("N",DefaultCulture),Number(DeductibleDeductionPatentsIncomeBelgium,"0").ToString("N",DefaultCulture),Number(calced).ToString("N",DefaultCulture))}  ,
                        new BizTaxErrorMessageDataContract { Culture = "en-US", Message = string.Format("Het totaal vermeld in formulier 275P, 'Aftrek voor octrooi-inkomsten' ({0}€) is niet gelijk aan het bedrag ({1}€) in het vak 'Uiteenzetting van de winst'.",Number(DeductibleDeductionPatentsIncome275P,"0").ToString("N",DefaultCulture),Number(DeductibleDeductionPatentsIncomeBelgium,"0").ToString("N",DefaultCulture),Number(calced).ToString("N",DefaultCulture))} , 
                        new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = string.Format("Het totaal vermeld in formulier 275P, 'Aftrek voor octrooi-inkomsten' ({0}€) is niet gelijk aan het bedrag ({1}€) in het vak 'Uiteenzetting van de winst'.",Number(DeductibleDeductionPatentsIncome275P,"0").ToString("N",DefaultCulture),Number(DeductibleDeductionPatentsIncomeBelgium,"0").ToString("N",DefaultCulture),Number(calced).ToString("N",DefaultCulture))} 
                    }
                    ,
                    Fields = new List<string>()
                }
                );
            }
        }

        internal void Extra_275U_TaxCreditResearchDevelopmentCheck()
        {

            //DeductibleAllowanceInvestmentDeductionIncompatibleTaxCreditResearchDevelopment
            var AllowanceInvestmentDeductionBelgium = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:AllowanceInvestmentDeduction" });
            var TaxCreditResearchDevelopment275U_G = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:DeductibleAllowanceInvestmentDeductionIncompatibleTaxCreditResearchDevelopment" });
            var TaxCreditResearchDevelopment275U_E = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:DeductibleAllowanceInvestmentDeductionCompatibleTaxCreditResearchDevelopment" });

            bool test = Number(AllowanceInvestmentDeductionBelgium, "0") == Number(TaxCreditResearchDevelopment275U_G, "0")
                           || Number(AllowanceInvestmentDeductionBelgium, "0") == Number(TaxCreditResearchDevelopment275U_E, "0");

            if (!test)
            {
                // MESSAGES
                AddMessage(new BizTaxErrorDataContract
                {
                    FileId = _fileId,
                    Id = "f-xtra-nrcorp-275u-taxcredit",
                    Messages = new List<BizTaxErrorMessageDataContract> { 
                        new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = string.Format("Het bedrag ({0}€) aangewend in het vak 'Uiteenzetting van de winst' onder 'Investeringsaftrek aftrekbaar voor het belastbare tijdperk' is niet gelijk aan het bedrag in formulier 275U, vak 'Investeringsaftrek aftrekbaar voor het belastbare tijdperk' onder 'Investeringsaftrek voor vennootschappen die niet opteren voor het belastingkrediet voor onderzoek en ontwikkeling' [code 1437.1] ({1}€) noch onder 'Investeringsaftrek voor vennootschappen die opteren voor het belastingkrediet voor onderzoek en ontwikkeling' [code 1437.2] ({2}€).",Number(AllowanceInvestmentDeductionBelgium,"0").ToString("N",DefaultCulture),Number(TaxCreditResearchDevelopment275U_G,"0").ToString("N",DefaultCulture),Number(TaxCreditResearchDevelopment275U_E,"0").ToString("N",DefaultCulture))} , 
                        new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = string.Format("Le montant ({0}€) employée dans la boîte 'Détail des bénéfices',  'Déduction pour investissement' n'est pas égal du montant sous forme 275U 'Déduction pour investissement déductible pour la période imposable', 'Déduction pour investissement pour les sociétés qui n'optent pas pour le crédit d’impôt pour recherche et développement' [code 1437.1] ({1}€) ni 'Déduction pour investissement pour les sociétés qui optent pour le crédit d’impôt pour recherche et développement' [code 1437.2] ({2}€).",Number(AllowanceInvestmentDeductionBelgium,"0").ToString("N",DefaultCulture),Number(TaxCreditResearchDevelopment275U_G,"0").ToString("N",DefaultCulture),Number(TaxCreditResearchDevelopment275U_E,"0").ToString("N",DefaultCulture))} , 
                         new BizTaxErrorMessageDataContract { Culture = "en-US", Message = string.Format("Het bedrag ({0}€) aangewend in het vak 'Uiteenzetting van de winst' onder 'Investeringsaftrek aftrekbaar voor het belastbare tijdperk' is niet gelijk aan het bedrag in formulier 275U, vak 'Investeringsaftrek aftrekbaar voor het belastbare tijdperk' onder 'Investeringsaftrek voor vennootschappen die niet opteren voor het belastingkrediet voor onderzoek en ontwikkeling' [code 1437.1] ({1}€) noch onder 'Investeringsaftrek voor vennootschappen die opteren voor het belastingkrediet voor onderzoek en ontwikkeling' [code 1437.2] ({2}€).",Number(AllowanceInvestmentDeductionBelgium,"0").ToString("N",DefaultCulture),Number(TaxCreditResearchDevelopment275U_G,"0").ToString("N",DefaultCulture),Number(TaxCreditResearchDevelopment275U_E,"0").ToString("N",DefaultCulture))} , 
                         new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = string.Format("Het bedrag ({0}€) aangewend in het vak 'Uiteenzetting van de winst' onder 'Investeringsaftrek aftrekbaar voor het belastbare tijdperk' is niet gelijk aan het bedrag in formulier 275U, vak 'Investeringsaftrek aftrekbaar voor het belastbare tijdperk' onder 'Investeringsaftrek voor vennootschappen die niet opteren voor het belastingkrediet voor onderzoek en ontwikkeling' [code 1437.1] ({1}€) noch onder 'Investeringsaftrek voor vennootschappen die opteren voor het belastingkrediet voor onderzoek en ontwikkeling' [code 1437.2] ({2}€).",Number(AllowanceInvestmentDeductionBelgium,"0").ToString("N",DefaultCulture),Number(TaxCreditResearchDevelopment275U_G,"0").ToString("N",DefaultCulture),Number(TaxCreditResearchDevelopment275U_E,"0").ToString("N",DefaultCulture))} , 
                        
                    }
                    ,
                    Fields = new List<string>()
                }
                );
            }
        }

        internal void Extra_276Ws()
        {
            var elsW1 = GetElementsByScenDefs(new string[] { 
                "d-empf:EmploymentFunctionDimension::d-empf:ResearcherMember/d-empper:EmploymentPeriodDimension::d-empper:EmployedPartTimeMember/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension" ,
                "d-empf:EmploymentFunctionDimension::d-empf:HighlyQualifiedResearcherMember/d-empper:EmploymentPeriodDimension::d-empper:EmployedPartTimeMember/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension"
            }, new string[] { "D" }, new string[] { "tax-inc:ActualExemptionAdditionalPersonnel" });

            var elsW2 = GetElementsByScenDefs(new string[] { 
                "d-empf:EmploymentFunctionDimension::d-empf:IncreaseTechnologicalPotentialPersonnelMember/d-empper:EmploymentPeriodDimension::d-empper:EmployedPartTimeMember/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension"
                }, new string[] { "D" }, new string[] { "tax-inc:ActualExemptionAdditionalPersonnel" });

            var elsW3 = GetElementsByScenDefs(new string[] { 
                "d-empf:EmploymentFunctionDimension::d-empf:DepartmentHeadExportMember/d-empper:EmploymentPeriodDimension::d-empper:EmployedPartTimeMember"
            }, new string[] { "D" }, new string[] { "tax-inc:ActualExemptionAdditionalPersonnel" });

            var elsW4 = GetElementsByScenDefs(new string[] { 
                "d-empf:EmploymentFunctionDimension::d-empf:DepartmentHeadTQMMember/d-empper:EmploymentPeriodDimension::d-empper:EmployedPartTimeMember"
            }, new string[] { "D" }, new string[] { "tax-inc:ActualExemptionAdditionalPersonnel" });


            var ttl = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:ReversalPreviousExemptions" });
            var sumEls = Sum(Number(elsW1, "0"), Number(elsW2, "0"), Number(elsW3, "0"), Number(elsW4, "0"));
            bool test = Number(ttl, "0") == sumEls;

            if (!test)
            {
                AddMessage(new BizTaxErrorDataContract
                {
                    FileId = _fileId,
                    Id = "f-xtra-nrcorp-276W1-4",
                    Messages = new List<BizTaxErrorMessageDataContract> { 
                        new BizTaxErrorMessageDataContract { Culture = "nl-BE", 
                            Message = string.Format(
                                "Het bedrag ({0}€) vemeld in het vak 'Verworpen uitgaven' onder 'Terugnemingen van vroegere vrijstellingen' moet gelijk zijn aan de som ({1}€) bekomen uit <ul>"
                                +"<li> 276W1 'In een vorig belastbaar tijdperk aangeworven personeel waarvan de voltijdse tewerkstelling tijdens het belastbare tijdperk is opgehouden', som van veld 'Effectief vrijgesteld' ({2}€)</li>"
                                +"<li> 276W2 'In een vorig belastbaar tijdperk aangeworven personeel waarvan de voltijdse tewerkstelling tijdens het belastbare tijdperk is opgehouden', som van veld 'Effectief vrijgesteld' ({3}€)</li>"
                                +"<li> 276W3 'Vrijstelling voor bijkomend personeel tewerkgesteld als diensthoofd voor de uitvoer', veld 'Effectief vrijgesteld' onder kolom 'Einde voltijds' ({4}€)</li>"
                                +"<li> 276W4 'Vrijstelling voor bijkomend personeel als diensthoofd van de afdeling Integrale kwaliteitszorg', veld 'Effectief vrijgesteld' onder kolom 'Einde voltijds' ({5}€)</li></ul>"
                                    , Number(ttl,"0").ToString("N",DefaultCulture)
                                    , sumEls.ToString("N",DefaultCulture)
                                    , Number(elsW1,"0").ToString("N",DefaultCulture)
                                    , Number(elsW2,"0").ToString("N",DefaultCulture)
                                    , Number(elsW3,"0").ToString("N",DefaultCulture)
                                    , Number(elsW4,"0").ToString("N",DefaultCulture)
                            )
                        },
                        new BizTaxErrorMessageDataContract { Culture = "fr-FR", 
                            Message = string.Format(
                                "Het bedrag ({0}€) vemeld in het vak 'Verworpen uitgaven' onder 'Terugnemingen van vroegere vrijstellingen' moet gelijk zijn aan de som ({1}€) bekomen uit <ul>"
                                +"<li> 276W1 'In een vorig belastbaar tijdperk aangeworven personeel waarvan de voltijdse tewerkstelling tijdens het belastbare tijdperk is opgehouden', som van veld 'Effectief vrijgesteld' ({2}€)</li>"
                                +"<li> 276W2 'In een vorig belastbaar tijdperk aangeworven personeel waarvan de voltijdse tewerkstelling tijdens het belastbare tijdperk is opgehouden', som van veld 'Effectief vrijgesteld' ({3}€)</li>"
                                +"<li> 276W3 'Vrijstelling voor bijkomend personeel tewerkgesteld als diensthoofd voor de uitvoer', veld 'Effectief vrijgesteld' onder kolom 'Einde voltijds' ({4}€)</li>"
                                +"<li> 276W4 'Vrijstelling voor bijkomend personeel als diensthoofd van de afdeling Integrale kwaliteitszorg', veld 'Effectief vrijgesteld' onder kolom 'Einde voltijds' ({5}€)</li></ul>"
                                    , Number(ttl,"0").ToString("N",DefaultCulture)
                                    , sumEls.ToString("N",DefaultCulture)
                                    , Number(elsW1,"0").ToString("N",DefaultCulture)
                                    , Number(elsW2,"0").ToString("N",DefaultCulture)
                                    , Number(elsW3,"0").ToString("N",DefaultCulture)
                                    , Number(elsW4,"0").ToString("N",DefaultCulture)
                            )
                        },
                        new BizTaxErrorMessageDataContract { Culture = "en-US", 
                            Message = string.Format(
                                "Het bedrag ({0}€) vemeld in het vak 'Verworpen uitgaven' onder 'Terugnemingen van vroegere vrijstellingen' moet gelijk zijn aan de som ({1}€) bekomen uit <ul>"
                                +"<li> 276W1 'In een vorig belastbaar tijdperk aangeworven personeel waarvan de voltijdse tewerkstelling tijdens het belastbare tijdperk is opgehouden', som van veld 'Effectief vrijgesteld' ({2}€)</li>"
                                +"<li> 276W2 'In een vorig belastbaar tijdperk aangeworven personeel waarvan de voltijdse tewerkstelling tijdens het belastbare tijdperk is opgehouden', som van veld 'Effectief vrijgesteld' ({3}€)</li>"
                                +"<li> 276W3 'Vrijstelling voor bijkomend personeel tewerkgesteld als diensthoofd voor de uitvoer', veld 'Effectief vrijgesteld' onder kolom 'Einde voltijds' ({4}€)</li>"
                                +"<li> 276W4 'Vrijstelling voor bijkomend personeel als diensthoofd van de afdeling Integrale kwaliteitszorg', veld 'Effectief vrijgesteld' onder kolom 'Einde voltijds' ({5}€)</li></ul>"
                                    , Number(ttl,"0").ToString("N",DefaultCulture)
                                    , sumEls.ToString("N",DefaultCulture)
                                    , Number(elsW1,"0").ToString("N",DefaultCulture)
                                    , Number(elsW2,"0").ToString("N",DefaultCulture)
                                    , Number(elsW3,"0").ToString("N",DefaultCulture)
                                    , Number(elsW4,"0").ToString("N",DefaultCulture)
                            )
                        },
                       new BizTaxErrorMessageDataContract { Culture = "de-DE", 
                            Message = string.Format(
                                "Het bedrag ({0}€) vemeld in het vak 'Verworpen uitgaven' onder 'Terugnemingen van vroegere vrijstellingen' moet gelijk zijn aan de som ({1}€) bekomen uit <ul>"
                                +"<li> 276W1 'In een vorig belastbaar tijdperk aangeworven personeel waarvan de voltijdse tewerkstelling tijdens het belastbare tijdperk is opgehouden', som van veld 'Effectief vrijgesteld' ({2}€)</li>"
                                +"<li> 276W2 'In een vorig belastbaar tijdperk aangeworven personeel waarvan de voltijdse tewerkstelling tijdens het belastbare tijdperk is opgehouden', som van veld 'Effectief vrijgesteld' ({3}€)</li>"
                                +"<li> 276W3 'Vrijstelling voor bijkomend personeel tewerkgesteld als diensthoofd voor de uitvoer', veld 'Effectief vrijgesteld' onder kolom 'Einde voltijds' ({4}€)</li>"
                                +"<li> 276W4 'Vrijstelling voor bijkomend personeel als diensthoofd van de afdeling Integrale kwaliteitszorg', veld 'Effectief vrijgesteld' onder kolom 'Einde voltijds' ({5}€)</li></ul>"
                                    , Number(ttl,"0").ToString("N",DefaultCulture)
                                    , sumEls.ToString("N",DefaultCulture)
                                    , Number(elsW1,"0").ToString("N",DefaultCulture)
                                    , Number(elsW2,"0").ToString("N",DefaultCulture)
                                    , Number(elsW3,"0").ToString("N",DefaultCulture)
                                    , Number(elsW4,"0").ToString("N",DefaultCulture)
                            )
                        }
                    }
                    ,
                    Fields = new List<string>()
                }
                );
            }
        }

        // ActualExemptionAdditionalPersonnel
        // ActualExemptionAdditionalPersonnel
        // ActualExemptionAdditionalPersonnel


        //      d-empf:EmploymentFunctionDimension/d-empper:EmploymentPeriodDimension/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension
        // NOT: d-empf:EmploymentFunctionDimension/d-empper:EmploymentPeriodDimension/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension
        #endregion

        internal void IndexFiches()
        {
            List<BizTaxFicheInfoDataContract> fiches = new List<BizTaxFicheInfoDataContract>
            {
                new BizTaxFicheInfoDataContract { FicheId="Id" }
            };

            if (Xbrl.Elements.Where(x => x.Name == "BasicTaxableAmountCommonRate" && x.GetNumberOrDefault(0) != 0).Count() > 0
                || Xbrl.Elements.Where(x => x.Name == "RemainingFiscalResultBeforeOriginDistribution" && x.GetNumberOrDefault(0) != 0).Count() > 0
                || Xbrl.Elements.Where(x => x.Name == "FiscalResult" && x.GetNumberOrDefault(0) != 0).Count() > 0
                || Xbrl.Elements.Where(x => x.Name == "AnnualWorkForceAverageCorporationCodeCurrentTaxPeriod" && x.GetNumberOrDefault(0) != 0).Count() > 0)
            {
                fiches.Add(new BizTaxFicheInfoDataContract { FicheId = "275.2.A.1" });
                fiches.Add(new BizTaxFicheInfoDataContract { FicheId = "275.2.B.1", Annexes = Xbrl.Elements.Where(x => 
                    x.BinaryValue != null && x.BinaryValue.Count > 0
                    && (x.Name == "BalanceSheetStatutorySeatStatutoryAccountsBelgianBranch" ||
            x.Name == "ExemptionProfitHomologationReorganizationPlanAmicableSettlementDocuments" ||
            x.Name == "InternalStatutoryAccounts" ||
            x.Name == "DepreciationTableNonStructured" ||
            x.Name == "OtherDocuments")
                    ).ToList() });
            }


            if (Xbrl.Elements.Where(x => x.Name == "LetOutImmovablePropertyIncomeBelgium" && x.GetNumberOrDefault(0) != 0).Count() > 0
                || Xbrl.Elements.Where(x => x.Name == "TaxableImmovablePropertyIncomeBelgium" && x.GetNumberOrDefault(0) != 0).Count() > 0)
            {
                fiches.Add(new BizTaxFicheInfoDataContract { FicheId = "275.2.A.2" });
                fiches.Add(new BizTaxFicheInfoDataContract
                {
                    FicheId = "275.2.B.2",
                    Annexes = Xbrl.Elements.Where(x =>
                        x.BinaryValue != null && x.BinaryValue.Count > 0
                        && (x.Name == "BalanceSheetStatutorySeatStatutoryAccountsBelgianBranchLegalEntity" ||
                        x.Name == "AccountsAssessmentLiabilityTaxRegimeLegalEntityIncomeTaxTaxableIncome" ||
                        x.Name == "OtherDocumentsLegalEntity" )
                        ).ToList()
                });
            }
            

            // 
            //
            //
            if (Xbrl.Elements.Where(x => x.Name == "ExemptWriteDownDebtClaim" && x.GetNumberOrDefault(0) != 0).Count() > 0
                || Xbrl.Elements.Where(x => x.Name == "ExemptProvisionRisksExpenses" && x.GetNumberOrDefault(0) != 0).Count() > 0)
            {
                fiches.Add(new BizTaxFicheInfoDataContract { FicheId = "204.3" });
            }

            List<string> scenarioIds = GetScenarioIds(new string[] { "d-asst:AssetTypeDimension::d-asst:SeaVesselMember" });
            if (Xbrl.Elements.Count(x => scenarioIds.Contains(x.Context) && !string.IsNullOrEmpty(x.Value)) > 0)
            {
                fiches.Add(new BizTaxFicheInfoDataContract { FicheId = "275B" });
            }

            //D__id__SpecificSecurityMember__id__2010-01
            //D__id__SpecificSecurityMember__id__2013-02__id__2010-01
            scenarioIds = GetScenarioIdsFuzzy(new string[] { "d-asst:AssetTypeDimension::d-asst:SpecificSecurityMember" });
            if (Xbrl.Elements.Count(x => scenarioIds.Contains(x.Context) && !string.IsNullOrEmpty(x.Value)) > 0)
            {
                fiches.Add(new BizTaxFicheInfoDataContract { FicheId = "275K" });
            }

            scenarioIds = GetScenarioIdsFuzzy(new string[] { "d-asst:AssetTypeDimension::d-asst:TangibleIntangibleFixedAssetsMember" });
            if (Xbrl.Elements.Count(x => scenarioIds.Contains(x.Context) && !string.IsNullOrEmpty(x.Value)) > 0)
            {
                fiches.Add(new BizTaxFicheInfoDataContract { FicheId = "276K" });
            }

            scenarioIds = GetScenarioIdsFuzzy(new string[] { "d-asst:AssetTypeDimension::d-asst:CorporateVehicleMember" });
            if (Xbrl.Elements.Count(x => scenarioIds.Contains(x.Context) && !string.IsNullOrEmpty(x.Value)) > 0)
            {
                fiches.Add(new BizTaxFicheInfoDataContract { FicheId = "276N" });
            }

            scenarioIds = GetScenarioIdsFuzzy(new string[] { "d-asst:AssetTypeDimension::d-asst:RiverVesselMember" });
            if (Xbrl.Elements.Count(x => scenarioIds.Contains(x.Context) && !string.IsNullOrEmpty(x.Value)) > 0)
            {
                fiches.Add(new BizTaxFicheInfoDataContract { FicheId = "276P" });
            }

            if (Xbrl.Elements.Where(x => x.Name == "AllowanceCorporateEquityCurrentTaxPeriod" && x.GetNumberOrDefault(0) != 0).Count() > 0
                || Xbrl.Elements.Where(x => x.Name.StartsWith("ExemptionsAllowanceCorporateEquityAccumulatedUpToAndIncludingAssessmentYear") && x.GetNumberOrDefault(0) != 0).Count() > 0)
            {
                fiches.Add(new BizTaxFicheInfoDataContract { FicheId = "275C" });
            }

            if (Xbrl.Elements.Where(x => x.Name == "PaymentCertainState" && x.GetNumberOrDefault(0) != 0).Count() > 0)
            {
                fiches.Add(new BizTaxFicheInfoDataContract { FicheId = "275F" });
            }

            if (Xbrl.Elements.Where(x => x.Name == "CalculationBasisDeductionPatentsIncome" && x.GetNumberOrDefault(0) != 0).Count() > 0)
            {
                fiches.Add(new BizTaxFicheInfoDataContract { FicheId = "275P" });
            }


            if (Xbrl.Elements.Where(x => x.Name == "CorrectedIncreaseTaxableReservesInvestmentReserveExcluded" && x.GetNumberOrDefault(0) != 0).Count() > 0
               || Xbrl.Elements.Where(x => x.Name == "ExemptInvestmentReserveOverview" && x.GetNumberOrDefault(0) != 0).Count() > 0
                || Xbrl.Elements.Where(x => x.Name == "InvestBalance" && x.GetNumberOrDefault(0) != 0).Count() > 0)
            {
                fiches.Add(new BizTaxFicheInfoDataContract { FicheId = "275R" });
            }

            // d-incc:IncentiveCategoryDimension/d-invc:InvestmentCategoryDimension (?)
            if (Xbrl.Elements.Where(x => x.Name == "DeductibleAllowanceInvestmentDeductionIncompatibleTaxCreditResearchDevelopment" && x.GetNumberOrDefault(0) != 0).Count() > 0
               || Xbrl.Elements.Where(x => x.Name == "CarryOverInvestmentDeduction" && x.GetNumberOrDefault(0) != 0).Count() > 0
               || Xbrl.Elements.Where(x => x.Name == "DeductibleAllowanceInvestmentDeductionCompatibleTaxCreditResearchDevelopment" && x.GetNumberOrDefault(0) != 0).Count() > 0
               || Xbrl.Elements.Where(x => x.Name == "CarryOverInvestmentDeductionCompatibleTaxCreditResearchDevelopment" && x.GetNumberOrDefault(0) != 0).Count() > 0)
            {
                fiches.Add(new BizTaxFicheInfoDataContract { FicheId = "275U" });
            }


            if (Xbrl.Elements.Where(x => x.Name == "TaxCreditResearchDevelopment" && x.GetNumberOrDefault(0) != 0).Count() > 0
              || Xbrl.Elements.Where(x => x.Name == "ClearableTaxCreditResearchDevelopmentCurrentTaxPeriod" && x.GetNumberOrDefault(0) != 0).Count() > 0
              )
            {
                fiches.Add(new BizTaxFicheInfoDataContract { FicheId = "275W" });
            }
            //
            //
            //d-empreg:EmploymentRegimeDimension/d-empstat:EmploymentStatusDimension/d-per:PeriodDimension/d-tu:TimeUnitDimension/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension


            List<string> crefs = Xbrl.Contexts.Where(c => c.ScenarioDef == "d-empreg:EmploymentRegimeDimension/d-empstat:EmploymentStatusDimension/d-per:PeriodDimension/d-tu:TimeUnitDimension/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension").Select(c => c.Id).ToList();
            if (Xbrl.Elements.Count(x => crefs.Contains(x.ContextRef) && x.GetNumberOrDefault(0) != 0) > 0)
            {
                fiches.Add(new BizTaxFicheInfoDataContract { FicheId = "276T" });
            }

            scenarioIds = GetScenarioIds(new string[] { 
                "d-empf:EmploymentFunctionDimension::d-empf:HighlyQualifiedResearcherMember/d-empper:EmploymentPeriodDimension::d-empper:EmployedFullTimeMember/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension",
                "d-empf:EmploymentFunctionDimension::d-empf:HighlyQualifiedResearcherMember/d-empper:EmploymentPeriodDimension::d-empper:EmployedPartTimeMember/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension",
                "d-empf:EmploymentFunctionDimension::d-empf:ResearcherMember/d-empper:EmploymentPeriodDimension::d-empper:EmployedPartTimeMember/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension",
                "d-empf:EmploymentFunctionDimension::d-empf:ResearcherMember/d-empper:EmploymentPeriodDimension::d-empper:EmployedFullTimeMember/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension"
            });
            if (Xbrl.Elements.Count(x => scenarioIds.Contains(x.Context) && !string.IsNullOrEmpty(x.Value)) > 0)
            {
                fiches.Add(new BizTaxFicheInfoDataContract { FicheId = "276W1" });
            }

            scenarioIds = GetScenarioIds(new string[] { 
                "d-empf:EmploymentFunctionDimension::d-empf:IncreaseTechnologicalPotentialPersonnelMember/d-empper:EmploymentPeriodDimension::d-empper:EmployedFullTimeMember/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension",
                "d-empf:EmploymentFunctionDimension::d-empf:IncreaseTechnologicalPotentialPersonnelMember/d-empper:EmploymentPeriodDimension::d-empper:EmployedPartTimeMember/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension"
            });
            if (Xbrl.Elements.Count(x => scenarioIds.Contains(x.Context) && !string.IsNullOrEmpty(x.Value)) > 0)
            {
                fiches.Add(new BizTaxFicheInfoDataContract { FicheId = "276W2" });
            }

            scenarioIds = GetScenarioIds(new string[] { 
                "d-empf:EmploymentFunctionDimension::d-empf:DepartmentHeadExportMember/d-empper:EmploymentPeriodDimension::d-empper:RecruitmentFullTimeMember",
                "d-empf:EmploymentFunctionDimension::d-empf:DepartmentHeadExportMember/d-empper:EmploymentPeriodDimension::d-empper:EmployedFullTimeTransferTaxPeriodMember",
                "d-empf:EmploymentFunctionDimension::d-empf:DepartmentHeadExportMember/d-empper:EmploymentPeriodDimension::d-empper:RecruitmentFullTimeReplacementMember",
                "d-empf:EmploymentFunctionDimension::d-empf:DepartmentHeadExportMember/d-empper:EmploymentPeriodDimension::d-empper:EmployedFullTimeMember",
                "d-empf:EmploymentFunctionDimension::d-empf:DepartmentHeadExportMember/d-empper:EmploymentPeriodDimension::d-empper:EmployedPartTimeMember"
            });
            if (Xbrl.Elements.Count(x => scenarioIds.Contains(x.Context) && !string.IsNullOrEmpty(x.Value)) > 0)
            {
                fiches.Add(new BizTaxFicheInfoDataContract { FicheId = "276W3" });
            }

            scenarioIds = GetScenarioIds(new string[] { 
                "d-empf:EmploymentFunctionDimension::d-empf:DepartmentHeadTQMMember/d-empper:EmploymentPeriodDimension::d-empper:RecruitmentFullTimeMember",
                "d-empf:EmploymentFunctionDimension::d-empf:DepartmentHeadTQMMember/d-empper:EmploymentPeriodDimension::d-empper:EmployedFullTimeTransferTaxPeriodMember",
                "d-empf:EmploymentFunctionDimension::d-empf:DepartmentHeadTQMMember/d-empper:EmploymentPeriodDimension::d-empper:RecruitmentFullTimeReplacementMember",
                "d-empf:EmploymentFunctionDimension::d-empf:DepartmentHeadTQMMember/d-empper:EmploymentPeriodDimension::d-empper:EmployedFullTimeMember",
                "d-empf:EmploymentFunctionDimension::d-empf:DepartmentHeadTQMMember/d-empper:EmploymentPeriodDimension::d-empper:EmployedPartTimeMember"
            });
            if (Xbrl.Elements.Count(x => scenarioIds.Contains(x.Context) && !string.IsNullOrEmpty(x.Value)) > 0)
            {
                fiches.Add(new BizTaxFicheInfoDataContract { FicheId = "276W3" });
            }

            scenarioIds = GetScenarioIds(new string[] { 
                "d-depm:DepreciationMethodDimension::d-depm:DegressiveDepreciationMethodMember/d-ty:DescriptionTypedDimension/d-ty:NatureTypedDimension"
            });
            if (Xbrl.Elements.Count(x => scenarioIds.Contains(x.Context) && !string.IsNullOrEmpty(x.Value)) > 0)
            {
                fiches.Add(new BizTaxFicheInfoDataContract { FicheId = "328K" });
            }

            scenarioIds = GetScenarioIds(new string[] { 
                "d-depm:DepreciationMethodDimension::d-depm:RenunciationDegressiveDepreciationMethodMember/d-ty:DescriptionTypedDimension/d-ty:NatureTypedDimension"
            });
            if (Xbrl.Elements.Count(x => scenarioIds.Contains(x.Context) && !string.IsNullOrEmpty(x.Value)) > 0)
            {
                fiches.Add(new BizTaxFicheInfoDataContract { FicheId = "328L" });
            }


            this.Xbrl.Fiches = fiches;
        }

        internal decimal? TaxCalculation()
        {
            decimal taxcalc = 0;

            if (Xbrl.Errors.Count > 0)
            {
                return null;
            }

            bool ExclusionReducedRate = Bool(Xbrl.Elements.FirstOrDefault(x => x.Name == "ExclusionReducedRate"));
            bool CreditCorporationTradeEquipmentHousingCorporationTaxRate = Bool(Xbrl.Elements.FirstOrDefault(x => x.Name == "CreditCorporationTradeEquipmentHousingCorporationTaxRate"));

            // UITEENZETTING VAN DE WINST
            decimal BasicTaxableAmountCommonRate = Number(Xbrl.Elements.Where(x => x.Name == "BasicTaxableAmountCommonRate"), "0");

            if (BasicTaxableAmountCommonRate > 0)
            {

                if ((!ExclusionReducedRate && !CreditCorporationTradeEquipmentHousingCorporationTaxRate)
                    || (ExclusionReducedRate && BasicTaxableAmountCommonRate > 322500)
                    )
                {
                    taxcalc += BasicTaxableAmountCommonRate * MaximumRateCorporateTax;
                }
                else if (ExclusionReducedRate)
                {
                    if (BasicTaxableAmountCommonRate <= (decimal)25000)
                    {
                        taxcalc += BasicTaxableAmountCommonRate * (decimal)0.24975;
                    }
                    else
                    {
                        taxcalc += 25000 * (decimal)0.249775;
                        BasicTaxableAmountCommonRate -= 25000;
                        if (BasicTaxableAmountCommonRate <= (decimal)65000)
                        {
                            taxcalc += BasicTaxableAmountCommonRate * (decimal)0.3193;
                        }
                        else
                        {
                            taxcalc += 65000 * (decimal)0.3193;
                            BasicTaxableAmountCommonRate -= 65000;

                            if (BasicTaxableAmountCommonRate <= (decimal)232500)
                            {
                                taxcalc += BasicTaxableAmountCommonRate * (decimal)0.35535;
                            }
                            else
                            {
                                taxcalc += 232500 * (decimal)0.35535;
                                BasicTaxableAmountCommonRate -= 232500;

                            }
                        }
                    }

                }
                else if (CreditCorporationTradeEquipmentHousingCorporationTaxRate)
                {
                    taxcalc += BasicTaxableAmountCommonRate * (decimal)0.0515;
                }
            }


            decimal CapitalGainsSharesRate2500 = Number(Xbrl.Elements.Where(x => x.Name == "CapitalGainsSharesRate2500"), "0");
            if (CapitalGainsSharesRate2500 > 0)
            {
                taxcalc += CapitalGainsSharesRate2500 * (decimal)0.2575;
            }

            decimal BasicTaxableAmountExitTaxRate = Number(Xbrl.Elements.Where(x => x.Name == "BasicTaxableAmountExitTaxRate"), "0");
            if (BasicTaxableAmountExitTaxRate > 0)
            {
                taxcalc += BasicTaxableAmountExitTaxRate * (decimal)0.16995;
            }

            decimal baseForIncrease = 0 + taxcalc;

            decimal CapitalSubsidiesInterestSubsidiesAgriculturalSupportRate500 = Number(Xbrl.Elements.Where(x => x.Name == "CapitalSubsidiesInterestSubsidiesAgriculturalSupportRate500"), "0");
            if (CapitalSubsidiesInterestSubsidiesAgriculturalSupportRate500 > 0)
            {
                taxcalc += CapitalSubsidiesInterestSubsidiesAgriculturalSupportRate500 * (decimal)0.0515;
            }

            //AFZONDERLIJKE AANSLAGEN
            decimal UnjustifiedExpensesHiddenSurplusProfitsFinancialAdvantagesBenefitsAllKind = Number(Xbrl.Elements.Where(x => x.Name == "UnjustifiedExpensesHiddenSurplusProfitsFinancialAdvantagesBenefitsAllKind"), "0");
            if (UnjustifiedExpensesHiddenSurplusProfitsFinancialAdvantagesBenefitsAllKind > 0)
            {
                taxcalc += UnjustifiedExpensesHiddenSurplusProfitsFinancialAdvantagesBenefitsAllKind * (decimal)3.09;
            }


            decimal SeparateAssessmentTaxedReservesCreditAssociationsInstitutionsOtherAuthorisedInstitutionsRate3400 = Number(Xbrl.Elements.Where(x => x.Name == "SeparateAssessmentTaxedReservesCreditAssociationsInstitutionsOtherAuthorisedInstitutionsRate3400"), "0");
            if (SeparateAssessmentTaxedReservesCreditAssociationsInstitutionsOtherAuthorisedInstitutionsRate3400 > 0)
            {
                taxcalc += SeparateAssessmentTaxedReservesCreditAssociationsInstitutionsOtherAuthorisedInstitutionsRate3400 * (decimal)0.3502;
            }

            decimal SeparateAssessmentTaxedReservesCreditAssociationsInstitutionsOtherAuthorisedInstitutionsRate2800 = Number(Xbrl.Elements.Where(x => x.Name == "SeparateAssessmentTaxedReservesCreditAssociationsInstitutionsOtherAuthorisedInstitutionsRate2800"), "0");
            if (SeparateAssessmentTaxedReservesCreditAssociationsInstitutionsOtherAuthorisedInstitutionsRate2800 > 0)
            {
                taxcalc += SeparateAssessmentTaxedReservesCreditAssociationsInstitutionsOtherAuthorisedInstitutionsRate2800 * (decimal)0.2884;
            }

            decimal SeparateAssessmentDividendsPaidCreditCorporationTradeEquipmentHousingCorporation = Number(Xbrl.Elements.Where(x => x.Name == "SeparateAssessmentDividendsPaidCreditCorporationTradeEquipmentHousingCorporation"), "0");
            if (SeparateAssessmentDividendsPaidCreditCorporationTradeEquipmentHousingCorporation > 0)
            {
                taxcalc += SeparateAssessmentDividendsPaidCreditCorporationTradeEquipmentHousingCorporation * (decimal)0.3502;
            }


            // BIJZONDERE AANSLAGEN
            decimal SpecialAssessmentPre1990FullPartialDistributionCompanyAssetsRate3300 = Number(Xbrl.Elements.Where(x => x.Name == "SpecialAssessmentPre1990FullPartialDistributionCompanyAssetsRate3300"), "0");
            if (SpecialAssessmentPre1990FullPartialDistributionCompanyAssetsRate3300 > 0)
            {
                taxcalc += SpecialAssessmentPre1990FullPartialDistributionCompanyAssetsRate3300 * (decimal)0.33;
            }

            decimal SpecialAssessmentPre1990FullPartialDistributionCompanyAssetsRate1650 = Number(Xbrl.Elements.Where(x => x.Name == "SpecialAssessmentPre1990FullPartialDistributionCompanyAssetsRate1650"), "0");
            if (SpecialAssessmentPre1990FullPartialDistributionCompanyAssetsRate1650 > 0)
            {
                taxcalc += SpecialAssessmentPre1990FullPartialDistributionCompanyAssetsRate1650 * (decimal)0.165;
            }

            decimal SpecialAssessmentPre1990BenefitsAllKindCompanyLiquidation = Number(Xbrl.Elements.Where(x => x.Name == "SpecialAssessmentPre1990BenefitsAllKindCompanyLiquidation"), "0");
            if (SpecialAssessmentPre1990BenefitsAllKindCompanyLiquidation > 0)
            {
                taxcalc += SpecialAssessmentPre1990BenefitsAllKindCompanyLiquidation * (decimal)0.45;
            }


            // AANVULLENDE HEFFING
            decimal AdditionalDutiesDiamondTraders = Number(Xbrl.Elements.Where(x => x.Name == "AdditionalDutiesDiamondTraders"), "0");
            if (AdditionalDutiesDiamondTraders > 0)
            {
                taxcalc += AdditionalDutiesDiamondTraders * (decimal)0.15;
            }

            // TOCHECK !!!
            decimal RetributionTaxCreditResearchDevelopment = Number(Xbrl.Elements.Where(x => x.Name == "RetributionTaxCreditResearchDevelopment"), "0");
            if (RetributionTaxCreditResearchDevelopment > 0)
            {
                taxcalc += RetributionTaxCreditResearchDevelopment;
            }

            bool SeparateAssessmentAdditionalIndividualPensionProvisionsSpreadThreeAssessmentYears2013Until2015 = Bool(Xbrl.Elements.FirstOrDefault(x => x.Name == "SeparateAssessmentAdditionalIndividualPensionProvisionsSpreadThreeAssessmentYears2013Until2015"));
            decimal SeparateAssessmentAdditionalIndividualPensionProvisionsOnEndTaxPeriodClosingDateBefore20120101 = Number(Xbrl.Elements.Where(x => x.Name == "SeparateAssessmentAdditionalIndividualPensionProvisionsOnEndTaxPeriodClosingDateBefore2012-01-01"), "0");

            if (SeparateAssessmentAdditionalIndividualPensionProvisionsOnEndTaxPeriodClosingDateBefore20120101 > 0)
            {
                if (SeparateAssessmentAdditionalIndividualPensionProvisionsSpreadThreeAssessmentYears2013Until2015)
                {
                    taxcalc += SeparateAssessmentAdditionalIndividualPensionProvisionsOnEndTaxPeriodClosingDateBefore20120101 * (decimal)0.006;
                }
                else
                {
                    taxcalc += SeparateAssessmentAdditionalIndividualPensionProvisionsOnEndTaxPeriodClosingDateBefore20120101 * (decimal)0.0175;
                }
            }



            taxcalc = Math.Round(taxcalc, 2, MidpointRounding.AwayFromZero);

            // VERREKENBARE VOORHEFFINGEN

            decimal RepayableAdvanceLevies = Number(Xbrl.Elements.Where(x => x.Name == "RepayableAdvanceLevies"), "0");
            decimal TaxCreditResearchDevelopmentClearablePreviousTaxPeriodsRepayableCurrentAssessmentYear = Number(Xbrl.Elements.Where(x => x.Name == "TaxCreditResearchDevelopmentClearablePreviousTaxPeriodsRepayableCurrentAssessmentYear"), "0");
            decimal NonRepayableAdvanceLevies = Number(Xbrl.Elements.Where(x => x.Name == "NonRepayableAdvanceLevies"), "0");

            taxcalc -= RepayableAdvanceLevies;
            taxcalc -= TaxCreditResearchDevelopmentClearablePreviousTaxPeriodsRepayableCurrentAssessmentYear;
            taxcalc -= NonRepayableAdvanceLevies;

            baseForIncrease -= RepayableAdvanceLevies;
            baseForIncrease -= TaxCreditResearchDevelopmentClearablePreviousTaxPeriodsRepayableCurrentAssessmentYear;
            baseForIncrease -= NonRepayableAdvanceLevies;

           

            bool FirstThreeAccountingYearsSmallCompanyCorporationCode = Bool(Xbrl.Elements.FirstOrDefault(x => x.Name == "FirstThreeAccountingYearsSmallCompanyCorporationCode"));

            if (!FirstThreeAccountingYearsSmallCompanyCorporationCode)
            {
                decimal baseIncrease = baseForIncrease * (decimal)0.0225;

                decimal vooraf = baseIncrease;
                decimal PrepaymentFirstQuarter = Number(Xbrl.Elements.Where(x => x.Name == "PrepaymentFirstQuarter"), "0");
                decimal PrepaymentSecondQuarter = Number(Xbrl.Elements.Where(x => x.Name == "PrepaymentSecondQuarter"), "0");
                decimal PrepaymentThirdQuarter = Number(Xbrl.Elements.Where(x => x.Name == "PrepaymentThirdQuarter"), "0");
                decimal PrepaymentFourthQuarter = Number(Xbrl.Elements.Where(x => x.Name == "PrepaymentFourthQuarter"), "0");

                vooraf -= PrepaymentFirstQuarter * (decimal)0.03;
                vooraf -= PrepaymentSecondQuarter * (decimal)0.025;
                vooraf -= PrepaymentThirdQuarter * (decimal)0.02;
                vooraf -= PrepaymentFourthQuarter * (decimal)0.015;
                vooraf = Math.Round(vooraf, 2, MidpointRounding.AwayFromZero);
                if (vooraf < (baseForIncrease * (decimal)0.01))
                {
                    vooraf = 0;
                }

                decimal increase = 0;
                if (vooraf > 40)
                {
                    increase = vooraf;
                }
                taxcalc += increase;

            }

            decimal Prepayments = Number(Xbrl.Elements.Where(x => x.Name == "Prepayments"), "0");

            taxcalc -= Prepayments;

            return taxcalc;

        }
    }
}
