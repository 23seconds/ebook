﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using EY.com.eBook.Core.Data;
using EY.com.eBook.BizTax.Contracts;
using EY.com.eBook.BizTax.Contracts.Criteria;

namespace EY.com.eBook.BizTax.Interfaces
{
    [ServiceContract]
    interface IBizTaxService
    {
        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        FileXbrlDataContract GetBizTax(CriteriaBiztaxDataContract cbdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        Contracts.CalcResultDataContract SaveBizTax(FileXbrlDataContract fxdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        void UpdateBizTaxMetaData(FileXbrlInfoDataContract fxidc);

        /*[OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        BizTaxDataContract SaveBizTax(CriteriaFileDataContract cfdc);*/

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        decimal? GetTaxCalculation(CriteriaBiztaxDataContract cbdc);

        [OperationContract]
        [FaultContract(typeof(DefaultFaultContract))]
        Contracts.FileXbrlDataContract Publish(Contracts.Criteria.CriteriaBiztaxDataContract cbdc);
    }
}
