﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.ServiceModel.Activation;
using EY.com.eBook.BizTax.Interfaces;
using EY.com.eBook.Core.EF;

namespace EY.com.eBook.BizTax.AY2015.Service
{
    // CONVENIENCE IN ORDER TO BRIDGE BETWEEN TWO VERSIONS, NOT CALLED AFTER THEY HAVE LATEST UI VERSION
    [EntityFrameworkServiceBehavior]
    [ServiceBehavior(IncludeExceptionDetailInFaults = true, ConcurrencyMode = ConcurrencyMode.Multiple, InstanceContextMode = InstanceContextMode.PerCall)]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class BizTaxService : IBizTaxService
    {

        private bool IsBni(Guid fileId)
        {
            Guid cid = eBookWriteManager.Context.Files.First(f => f.Id == fileId).ClientId;
            return eBookWriteManager.Context.ClientSet.First(c => c.Id == cid).BNI;
        }

        #region IBizTaxService Members
        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
         RequestFormat = WebMessageFormat.Json,
         ResponseFormat = WebMessageFormat.Json,
         UriTemplate = "/GetBizTax")]
        public Contracts.FileXbrlDataContract GetBizTax(Contracts.Criteria.CriteriaBiztaxDataContract cbdc)
        {
            if (IsBni(cbdc.FileId))
            {
               // AY2015.nrcorp.FileBizTaxRepository fbr = new AY2015.nrcorp.FileBizTaxRepository();
               // return fbr.GetData(cbdc, true);
                return null;
            }
            else
            {

                AY2015.rcorp.FileBizTaxRepository fbr = new AY2015.rcorp.FileBizTaxRepository();
                return fbr.GetData(cbdc, true);
            }
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
         RequestFormat = WebMessageFormat.Json,
         ResponseFormat = WebMessageFormat.Json,
         UriTemplate = "/SaveBizTax")]
        public Contracts.CalcResultDataContract SaveBizTax(Contracts.FileXbrlDataContract fxdc)
        {
            if (IsBni(fxdc.FileId))
            {
              //  AY2015.nrcorp.FileBizTaxRepository fbr = new AY2015.nrcorp.FileBizTaxRepository();
               // fbr.Save(fxdc, true);

               // return fbr.GetCalcResults();
                return null;
            }
            else {
                AY2015.rcorp.FileBizTaxRepository fbr = new AY2015.rcorp.FileBizTaxRepository();

				fbr.Save(fxdc, true);

                return fbr.GetCalcResults();
            }  
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
         RequestFormat = WebMessageFormat.Json,
         ResponseFormat = WebMessageFormat.Json,
         UriTemplate = "/UpdateBizTaxMetaData")]
        public void UpdateBizTaxMetaData(Contracts.FileXbrlInfoDataContract fxidc)
        {
            if (IsBni(fxidc.FileId))
            {
               // AY2015.nrcorp.FileBizTaxRepository fbr = new AY2015.nrcorp.FileBizTaxRepository();
                //fbr.UpdateMeta(fxidc);
            }
            else
            {
                AY2015.rcorp.FileBizTaxRepository fbr = new AY2015.rcorp.FileBizTaxRepository();
                fbr.UpdateMeta(fxidc);
            }
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
         RequestFormat = WebMessageFormat.Json,
         ResponseFormat = WebMessageFormat.Json,
         UriTemplate = "/GetTaxCalculation")]
        public decimal? GetTaxCalculation(Contracts.Criteria.CriteriaBiztaxDataContract cbdc)
        {
            return null;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
         RequestFormat = WebMessageFormat.Json,
         ResponseFormat = WebMessageFormat.Json,
         UriTemplate = "/Publish")]
        public Contracts.FileXbrlDataContract Publish(Contracts.Criteria.CriteriaBiztaxDataContract cbdc)
        {
            if (IsBni(cbdc.FileId))
            {
               // AY2015.nrcorp.FileBizTaxRepository fbr = new AY2015.nrcorp.FileBizTaxRepository();
                //fbr.Publish(cbdc);
                //return fbr._fileXbrl;
                return null;
            }
            else
            {
                AY2015.rcorp.FileBizTaxRepository fbr = new AY2015.rcorp.FileBizTaxRepository();
                fbr.Publish(cbdc);
                return fbr._fileXbrl;
            }
        }

        #endregion
    }
    
}