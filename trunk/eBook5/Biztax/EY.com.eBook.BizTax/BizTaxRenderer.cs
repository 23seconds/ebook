﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Globalization;
using System.Xml.Linq;
using EY.com.eBook.BizTax.Contracts;
using EY.com.eBook.Core;
using System.Text.RegularExpressions;
using EY.com.eBook.API.BL;
using EY.com.eBook.BizTax;

namespace EY.com.eBook.BizTax
{
    public static class Extensions
    {
        public static string CleanXbrlId(this string val)
        {
            Regex rgx = new Regex("[^a-zA-Z0-9 \\-]");
            val = rgx.Replace(val, "").Replace(" ","");
            return val;
        }

        public static string ToXbrlValue(this DateTime dte)
        {
            return dte.ToIso8601DateOnly();
        }

        public static string ToXbrlValue(this DateTime? dte)
        {
            if (!dte.HasValue) return null;
            return dte.Value.ToIso8601DateOnly();
        }

        public static string ToXbrlValue(this decimal nr)
        {
            //  if (nr == 0) return null;
            return nr.ToString(System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
        }

        public static string ToXbrlValue(this int nr)
        {
            // if (nr == 0) return null;
            return nr.ToString(System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
        }

        public static string ToXbrlValue(this bool val)
        {
            return val.ToString().ToLower();
        }

        public static string ToXbrlValue(this string val)
        {
            return val.GetValueOrDefault();
        }

        public static string ToXbrlValue(this decimal? nr)
        {
            return nr.HasValue ? nr.Value.ToXbrlValue() : null;
        }

        public static string ToXbrlValue(this int? nr)
        {
            return nr.HasValue ? nr.Value.ToXbrlValue() : null;
        }

        public static string ToXbrlValue(this bool? val)
        {
            return val.HasValue ? val.Value.ToXbrlValue() : null;
        }

        public static IEnumerable<T> FlattenHierarchy<T>(this IEnumerable<T> list, Func<T, IEnumerable<T>> getChildEnumerator)
        {
            IEnumerable<T> result = list;
            foreach (T element in list)
            {
               // yield return element;
                IEnumerable<T> childList = getChildEnumerator(element);
                if (childList != null)
                {
                    
                    IEnumerable<T> cresult = childList.FlattenHierarchy(getChildEnumerator);
                    result = result.Concat(cresult);
                    
                    //list.FlattenListHierarchy(childList, getChildEnumerator);
                 
                   // list.FlattenHierarchy(getChildEnumerator(element), getChildEnumerator);
                }
            }
            return result;
            
        }

        public static IEnumerable<T> FromIndexList<T>(this List<T> list, IEnumerable<int> indexes)
        {
            foreach (int idx in indexes)
            {
                yield return list[idx];
            }
        }

        public static IEnumerable<XbrlElementDataContract> FromIndexList(this IEnumerable<XbrlElementDataContract> list, IEnumerable<List<int>> indexes) 
        {
            foreach (List<int> subindexes in indexes)
            {
                if (subindexes.Count == 1)
                {
                    yield return list.ElementAt(subindexes.First());
                }
                else if (subindexes.Count > 1)
                {
                    IEnumerable<XbrlElementDataContract> chlds = list;
                    XbrlElementDataContract x = null;
                    foreach (int idx in subindexes)
                    {
                        if (chlds == null)
                        {
                            x = null;
                            break;
                        }
                        x = chlds.ElementAt(idx);
                        if (x == null) break;
                        chlds = x.Children;
                        
                        
                    }
                    if (x != null)
                        yield return x;
                }
                
            }
        }

    }
    
    public class BizTaxRenderer
    {

       


        public bool? HasErrors { get; set; }

        internal Guid _fileId { get; set; }

        internal int _errId { get; set; }


        public BizTaxDataContract Xbrl { get; set; }

        public XbrlIndexDataContract Index { get; set; }

        
        public virtual void ApplyDefaultContexts(string identifier, DateTime periodStart, DateTime periodEnd)
        {
            
        }
       
        #region Creation of contexts
        public string AddContext(List<ContextScenarioDataContract> scenarios, DateTime periodStart, DateTime periodEnd, Guid defid)
        {
            return AddContext(scenarios, periodStart, periodEnd, defid,null);
        }

        public string AddContext(List<ContextScenarioDataContract> scenarios,DateTime periodStart, DateTime periodEnd,Guid defid, string prescontextId)
        {
            string identifier = Xbrl.EntityIdentifier.IdentifierValue;
            ContextEntityDataContract entity = Xbrl.EntityIdentifier;

            string startDate = ToIso8601DateOnly(periodStart);
            string endDate = ToIso8601DateOnly(periodEnd);
            string previousEndDate = ToIso8601DateOnly(periodStart.AddDays(-1));

            ContextPeriodDataContract periodD = new ContextPeriodDataContract { StartDate = startDate, EndDate = endDate };
            ContextPeriodDataContract periodIstart = new ContextPeriodDataContract { Instant = previousEndDate };
            ContextPeriodDataContract periodIend = new ContextPeriodDataContract { Instant = endDate };

            string ctxId = "";
            foreach (ContextScenarioDataContract csdc in scenarios)
            {
                
                if (string.IsNullOrEmpty(ctxId))
                {
                    ctxId = "id__";
                }
                else
                {

                    ctxId += "__id__";
                }
                if (csdc is ContextScenarioTypeDataContract)
                {
                    ctxId += csdc.Value.CleanXbrlId();
                }
                else
                {
                    ctxId += csdc.Value.Split(new char[] { ':' })[1];
                }

            }

            if (Xbrl.Contexts.Count(c => c.Id == "D__" + ctxId) == 0)
            {
                Xbrl.Contexts.Add(
                    new ContextElementDataContract
                    {
                        Entity = Xbrl.EntityIdentifier
                        ,
                        Id = "D__" + ctxId
                        ,
                        Period = periodD
                        ,
                        Scenario = scenarios
                        ,
                        DefId = defid
                        ,PresContextId = prescontextId
                    });
            }
            if (Xbrl.Contexts.Count(c => c.Id == "I-Start__" + ctxId) == 0)
            {
                Xbrl.Contexts.Add(
                    new ContextElementDataContract
                    {
                        Entity = Xbrl.EntityIdentifier
                        ,
                        Id = "I-Start__" + ctxId
                        ,
                        Period = periodIstart
                        ,
                        Scenario = scenarios
                         ,
                        DefId = defid
                        ,
                        PresContextId = prescontextId
                    });
            }

            if (Xbrl.Contexts.Count(c => c.Id == "I-End__" + ctxId) == 0)
            {
                Xbrl.Contexts.Add(
                    new ContextElementDataContract
                    {
                        Entity = Xbrl.EntityIdentifier
                        ,
                        Id = "I-End__" + ctxId
                        ,
                        Period = periodIend
                        ,
                        Scenario = scenarios
                         ,
                        DefId = defid
                        ,
                        PresContextId = prescontextId
                    });
            }
            return ctxId;
            
        }

        public string AddContextsFor(List<ContextScenarioExplicitDataContract> scenarios, string identifier, DateTime periodStart, DateTime periodEnd)
        {
           return AddContextsFor(scenarios, identifier, periodStart, periodEnd,null);
        }

        public string AddContextsFor(List<ContextScenarioExplicitDataContract> scenarios, string identifier, DateTime periodStart, DateTime periodEnd, Guid? contextId)
        {
            ContextEntityDataContract entity = Xbrl.EntityIdentifier;

            string startDate = ToIso8601DateOnly(periodStart);
            string endDate = ToIso8601DateOnly(periodEnd);
            string previousEndDate = ToIso8601DateOnly(periodStart.AddDays(-1));

            ContextPeriodDataContract periodD = new ContextPeriodDataContract { StartDate = startDate, EndDate = endDate };
            ContextPeriodDataContract periodIstart = new ContextPeriodDataContract { Instant = previousEndDate };
            ContextPeriodDataContract periodIend = new ContextPeriodDataContract { Instant = endDate };

            List<ContextScenarioDataContract> scens = new List<ContextScenarioDataContract>();
            string ctxId = "";
            foreach (ContextScenarioExplicitDataContract csdc in scenarios)
            {
                scens.Add(csdc);
                if (string.IsNullOrEmpty(ctxId))
                {
                    ctxId = "id__";
                }
                else
                {

                    ctxId += "__id__";
                }
                ctxId += csdc.Value.Split(new char[] { ':' })[1];

            }

            if (Xbrl.Contexts.Count(c => c.Id == "D__" + ctxId) == 0)
            {
                Xbrl.Contexts.Add(
                    new ContextElementDataContract
                    {
                        Entity = Xbrl.EntityIdentifier
                        ,
                        Id = "D__" + ctxId
                        ,
                        Period = periodD
                        ,
                        Scenario = scens
                        , DefId = contextId
                    });
            }
            if (Xbrl.Contexts.Count(c => c.Id == "I-Start__" + ctxId) == 0)
            {
                Xbrl.Contexts.Add(
                    new ContextElementDataContract
                    {
                        Entity = Xbrl.EntityIdentifier
                        ,
                        Id = "I-Start__" + ctxId
                        ,
                        Period = periodIstart
                        ,
                        Scenario = scens
                         ,
                        DefId = contextId
                    });
            }

            if (Xbrl.Contexts.Count(c => c.Id == "I-End__" + ctxId) == 0)
            {
                Xbrl.Contexts.Add(
                    new ContextElementDataContract
                    {
                        Entity = Xbrl.EntityIdentifier
                        ,
                        Id = "I-End__" + ctxId
                        ,
                        Period = periodIend
                        ,
                        Scenario = scens
                         ,
                        DefId = contextId
                    });
            }
            return ctxId;
        }

#endregion

        public BizTaxDataContract CreateNew(string identifier, DateTime periodStart, DateTime periodEnd, string schemaRef)
        {
            if (string.IsNullOrEmpty(identifier))
                throw new Exception("Entity identifier is missing");

            if (!identifier.StartsWith("BE")) identifier = identifier.ToBelgianEnterprise();
            
            Xbrl = new BizTaxDataContract
            {
                Contexts = new List<ContextElementDataContract>()
                ,
                Elements = new List<XbrlElementDataContract>()
                ,
                EntityIdentifier = new ContextEntityDataContract
                {
                    IdentifierScheme = "http://www.fgov.be"
                    ,
                    IdentifierValue = ToBelgianEnterprise(identifier)  // CLEANUP OF ENTERPRISE NUMBER
                }
                ,
                Units = new List<UnitElementDataContract>
                {
                    new UnitElementDataContract { Measure="pure",Id="U-Pure"}
                    ,new UnitElementDataContract { Measure="iso4217:EUR",Id="EUR"}
                }
                ,
                AssessmentYear = periodEnd.AddDays(1).Year
                , LastCalculated = null
                , LastSaved = null
                , NameSpaces=new List<XbrlNamespace>()
                , Partner =null
                , ProxyId = null
                , ProxyTitle = null
                ,
                SchemaRef = schemaRef
                
            };

            string startDate = ToIso8601DateOnly(periodStart);
            string endDate = ToIso8601DateOnly(periodEnd);
            string previousEndDate = ToIso8601DateOnly(periodStart.AddDays(-1));

            ContextPeriodDataContract periodD = new ContextPeriodDataContract { StartDate = startDate, EndDate = endDate };
            ContextPeriodDataContract periodIstart = new ContextPeriodDataContract { Instant = previousEndDate };
            ContextPeriodDataContract periodIend = new ContextPeriodDataContract { Instant = endDate };

            // CREATE STANDARD CONTEXTS
            Xbrl.Contexts.Add(new ContextElementDataContract
            {
                Entity = Xbrl.EntityIdentifier
                ,
                Id = "D"
                ,
                Period = periodD
                ,
                Scenario = new List<ContextScenarioDataContract>()
            });
            Xbrl.Contexts.Add(new ContextElementDataContract
            {
                Entity = Xbrl.EntityIdentifier
                ,
                Id = "I-Start"
                ,
                Period = periodIstart
                ,
                Scenario = new List<ContextScenarioDataContract>()
            });
            Xbrl.Contexts.Add(new ContextElementDataContract
            {
                Entity = Xbrl.EntityIdentifier
                ,
                Id = "I-End"
                ,
                Period = periodIend
                ,
                Scenario = new List<ContextScenarioDataContract>()
            });

            return Xbrl;
        }

        #region Loading of data
        public void Load(BizTaxDataContract btdc)
        {
            this.Xbrl = btdc;
            // CreateIndexes();
        }

        public BizTaxDataContract LoadFromXml(string path, Guid? fileId)
        {
            try
            {
                return LoadFromXml(XDocument.Load(path), fileId);
            }
            catch (Exception e)
            {
                Console.WriteLine(path);
                Console.WriteLine(e.Message);
                return null;
            }
        }

        public BizTaxDataContract LoadFromXml(XDocument doc, Guid? fileId)
        {
            Dictionary<string, XNamespace> namespaces = doc.Root.Attributes()
                        .Where(a => a.IsNamespaceDeclaration)
                        .GroupBy(a => a.Name.Namespace == XNamespace.None ? string.Empty : a.Name.LocalName,
                                a => XNamespace.Get(a.Value))
                        .ToDictionary(g => g.Key,
                                     g => g.First());

            Xbrl = new BizTaxDataContract
            {
                NameSpaces = new List<XbrlNamespace>(),

                Contexts = new List<ContextElementDataContract>()
                ,
                Elements = new List<XbrlElementDataContract>()
                ,
                EntityIdentifier = null
                ,
                Units = new List<UnitElementDataContract>()
            };


            foreach (string key in namespaces.Keys)
            {
                //                Console.WriteLine(key + " :: " + namespaces[key].NamespaceName);
                Xbrl.NameSpaces.Add(new XbrlNamespace { Prefix = key, Uri = namespaces[key].NamespaceName });


            }


            string defaultNs = namespaces[string.Empty].NamespaceName;

            // SEPERATE ELEMENTS FROM CONTEXT, UNITS AND SUCH.
            List<XElement> elements = new List<XElement>();

            foreach (XElement element in doc.Root.Elements())
            {
                if (element.Name.LocalName == "unit")
                {
                    Xbrl.Units.Add(new UnitElementDataContract
                    {
                        Measure = element.Elements().First().Value,
                        Id = element.Attribute("id").Value
                    });
                }
                else if (element.Name.LocalName == "context")
                {

                    ContextElementDataContract cte = new ContextElementDataContract
                    {
                        AutoRendered = false
                        ,
                        Id = element.Attribute("id").Value
                        ,
                        Period = new ContextPeriodDataContract()
                        ,
                        Entity = new ContextEntityDataContract()
                        ,
                        Scenario = new List<ContextScenarioDataContract>()
                    };
                    XElement entityEl = element.Element(XName.Get("entity", defaultNs));
                    XElement idEl = entityEl.Element(XName.Get("identifier", defaultNs));
                    cte.Entity.IdentifierScheme = idEl.Attribute("scheme").Value;
                    cte.Entity.IdentifierValue = idEl.Value;
                    Xbrl.EntityIdentifier = cte.Entity;

                    XElement periodEl = element.Element(XName.Get("period", defaultNs));
                    XElement periodInstEl = periodEl.Element(XName.Get("instant", defaultNs));
                    XElement periodStartEl = periodEl.Element(XName.Get("startDate", defaultNs));
                    XElement periodEndEl = periodEl.Element(XName.Get("endDate", defaultNs));

                    cte.Period.Instant = periodInstEl != null ? periodInstEl.Value : null;
                    cte.Period.StartDate = periodStartEl != null ? periodStartEl.Value : null;
                    cte.Period.EndDate = periodEndEl != null ? periodEndEl.Value : null;


                    XElement scenario = element.Element(XName.Get("scenario", defaultNs));
                    if (scenario != null)
                    {
                        foreach (XElement scenEl in scenario.Elements())
                        {
                            if (scenEl.Name.LocalName == "explicitMember")
                            {
                                ContextScenarioExplicitDataContract csedc = new ContextScenarioExplicitDataContract
                                {
                                    Dimension = scenEl.Attribute("dimension").Value
                                    ,
                                    Value = scenEl.Value
                                };
                                cte.Scenario.Add(csedc);
                            }
                            else
                            {
                                XElement typeEl = scenEl.Elements().First();
                                ContextScenarioTypeDataContract cstdc = new ContextScenarioTypeDataContract
                                {
                                    Dimension = scenEl.Attribute("dimension").Value
                                    ,
                                    Type = string.Format("{0}:{1}", namespaces.FirstOrDefault(t => t.Value == typeEl.Name.Namespace.NamespaceName).Key, typeEl.Name.LocalName)
                                        //typeEl.Name.LocalName,
                                        ,
                                    Value = typeEl.Value
                                };
                                cte.Scenario.Add(cstdc);
                            }


                        }
                    }
                    Xbrl.Contexts.Add(cte);
                }
                else if (element.Name.LocalName == "schemaRef")
                {
                    Xbrl.SchemaRef = element.Attribute(XName.Get("href", "http://www.w3.org/1999/xlink")).Value;
                }
                else
                {
                    elements.Add(element);
                }
            }

            foreach (XElement element in elements)
            {
                Xbrl.Elements.Add(GetElementFromXElement(element, Xbrl));
            }

            CreateIndexes();

            return Xbrl;
        }

        #endregion

        #region apply data (general data)
        public void SetGeneralData(string identifier, DateTime periodStart, DateTime periodEnd, Core.EF.Write.Client client, string culture)
        {
            XbrlElementDataContract xedc = null;
            XbrlElementDataContract pxedc = null;

            this.ApplyDefaultContexts(identifier, periodStart, periodEnd);


            #region SET GLOBAL INFO


            xedc = new XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-Start"
              ,
                Decimals = ""
              ,
                Name = "PeriodStartDate"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = ""
              ,
                Value = periodStart.ToXbrlValue()
                ,
                Locked=true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (Xbrl.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = Xbrl.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc.AutoRendered || string.IsNullOrEmpty(pxedc.Value))
                    {
                        pxedc.Value = xedc.Value;
                        pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    Xbrl.Elements.Add(pxedc);
                }
            }


            xedc = new XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "I-End"
              ,
                Decimals = ""
              ,
                Name = "PeriodEndDate"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = ""
              ,
                Value = periodEnd.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (Xbrl.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = Xbrl.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc.AutoRendered || string.IsNullOrEmpty(pxedc.Value))
                    {
                        pxedc.Value = xedc.Value;
                        pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    Xbrl.Elements.Add(pxedc);
                }
            }


            xedc = new XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = ""
              ,
                Name = "AssessmentYear"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = ""
              ,
                Value = periodEnd.AddDays(1).Year.ToXbrlValue()
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (Xbrl.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = Xbrl.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc.AutoRendered || string.IsNullOrEmpty(pxedc.Value))
                    {
                        pxedc.Value = xedc.Value;
                        pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    Xbrl.Elements.Add(pxedc);
                }
            }


            xedc = new XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = ""
              ,
                Name = "TaxReturnType"
              ,
                NameSpace = ""
              ,
                Prefix = "tax-inc"
              ,
                UnitRef = ""
              ,
                Value =  client.BNI ? "BNI/ven" : "VenB"
                ,
                Locked = true
            };

            if (xedc != null && (!string.IsNullOrEmpty(xedc.Value) || xedc.Children != null))
            {
                if (Xbrl.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = Xbrl.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    if (pxedc.AutoRendered || string.IsNullOrEmpty(pxedc.Value))
                    {
                        pxedc.Value = xedc.Value;
                        pxedc.Locked = xedc.Locked;
                    }
                }
                else
                {
                    pxedc = xedc;
                    Xbrl.Elements.Add(pxedc);
                }
            }


            xedc = new XbrlElementDataContract
            {
                AutoRendered = false
              ,
                Children =
              new List<XbrlElementDataContract> {
        new XbrlElementDataContract
      {
        AutoRendered = false
        ,
        Children = 
      new List<XbrlElementDataContract> {
        new XbrlElementDataContract
      {
        AutoRendered = true
        ,
        Children = null
        ,
        Context = ""
        ,
        Period = "D"
        ,
        Decimals = ""
        ,
        Name = "EntityCurrentLegalName"
        ,
        NameSpace = ""
        ,
        Prefix = "pfs-gcd"
        ,
        UnitRef = ""
        ,
        Value = client.Name
      }
      } 
    
        ,
        Context = ""
        ,
        Period = ""
        ,
        Decimals = ""
        ,
        Name = "EntityName"
        ,
        NameSpace = ""
        ,
        Prefix = "pfs-gcd"
        ,
        UnitRef = ""
        ,
        Value = null
      },new XbrlElementDataContract
      {
        AutoRendered = false
        ,
        Children = 
      new List<XbrlElementDataContract> {
        new XbrlElementDataContract
      {
        AutoRendered = true
        ,
        Children = null
        ,
        Context = ""
        ,
        Period = "D"
        ,
        Decimals = ""
        ,
        Name = "IdentifierName"
        ,
        NameSpace = ""
        ,
        Prefix = "pfs-gcd"
        ,
        UnitRef = ""
        ,
        Value = culture == "fr-FR" ? "Numéro d'entreprise" : "Ondernemingsnummer"
        ,
        Locked=true
      },new XbrlElementDataContract
      {
        AutoRendered = true
        ,
        Children = null
        ,
        Context = ""
        ,
        Period = "D"
        ,
        Decimals = ""
        ,
        Name = "IdentifierValue"
        ,
        NameSpace = ""
        ,
        Prefix = "pfs-gcd"
        ,
        UnitRef = ""
        ,
        Value = identifier
        ,
                Locked=true
      }
      } 
    
        ,
        Context = ""
        ,
        Period = ""
        ,
        Decimals = ""
        ,
        Name = "EntityIdentifier"
        ,
        NameSpace = ""
        ,
        Prefix = "pfs-gcd"
        ,
        UnitRef = ""
        ,
        Value = null
      },new XbrlElementDataContract
      {
        AutoRendered = true
        ,
        Children = GetEntityFormClient(client)
        ,
        Context = ""
        ,
        Period = ""
        ,
        Decimals = ""
        ,
        Name = "EntityForm"
        ,
        NameSpace = ""
        ,
        Prefix = "pfs-gcd"
        ,
        UnitRef = ""
        ,
        Value = null
      },GetClientAddress(client)
      }

              ,
                Context = ""
              ,
                Period = ""
              ,
                Decimals = ""
              ,
                Name = "EntityInformation"
              ,
                NameSpace = ""
              ,
                Prefix = "pfs-gcd"
              ,
                UnitRef = ""
              ,
                Value = null
            };



            if (xedc != null)
            {
                if (Xbrl.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = Xbrl.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    CheckChildren(ref pxedc, xedc.Children);
                }
                else
                {
                    Xbrl.Elements.Add(xedc);
                }
            }


            xedc = new XbrlElementDataContract
            {
                AutoRendered = false
              ,
                Children =
              new List<XbrlElementDataContract> {
        new XbrlElementDataContract
      {
        AutoRendered = true
        ,
        Children = null
        ,
        Context = ""
        ,
        Period = "D"
        ,
        Decimals = ""
        ,
        Name = "DocumentIdentifier"
        ,
        NameSpace = ""
        ,
        Prefix = "pfs-gcd"
        ,
        UnitRef = ""
        ,
        Value = "Aangifte"
        ,
                Locked=true
      }
      , new XbrlElementDataContract
      {
        AutoRendered = true
        ,
         Children =
              new List<XbrlElementDataContract> {
                   new XbrlElementDataContract
                  {
                    AutoRendered = true
                    ,
                    Children = null
                    ,
                    Context = ""
                    ,
                    Period = "D"
                    ,
                    Decimals = ""
                    ,
                    Name = "XCode_LanguageCode_"+ culture.Substring(0,2).ToUpper()
                    ,
                    NameSpace = ""
                    ,
                    Prefix = "pfs-vl"
                    ,
                    UnitRef = ""
                    ,
                    Value =  culture.Substring(0,2).ToUpper()
                    ,
                Locked=true
                  }
              }
        ,
        Context = ""
        ,
        Period = ""
        ,
        Decimals = ""
        ,
        Name = "DocumentLanguage"
        ,
        NameSpace = ""
        ,
        Prefix = "pfs-gcd"
        ,
        UnitRef = ""
        ,
        Value = null
      }
      }

              ,
                Context = ""
              ,
                Period = ""
              ,
                Decimals = ""
              ,
                Name = "DocumentInformation"
              ,
                NameSpace = ""
              ,
                Prefix = "pfs-gcd"
              ,
                UnitRef = ""
              ,
                Value = null
            };



            if (xedc != null)
            {
                if (Xbrl.Elements.Count(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context) > 0)
                {
                    pxedc = Xbrl.Elements.First(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix && e.Context == xedc.Context);
                    CheckChildren(ref pxedc, xedc.Children);
                }
                else
                {
                    Xbrl.Elements.Add(xedc);
                }
            }

            #endregion
        }

        public void CheckChildren(ref XbrlElementDataContract existing, List<XbrlElementDataContract> children)
        {
            if (existing.Children == null) existing.Children = new List<XbrlElementDataContract>();
            foreach (XbrlElementDataContract xedc in children)
            {
                XbrlElementDataContract exist = existing.Children.FirstOrDefault(e => e.Name == xedc.Name && e.Period == xedc.Period && e.Prefix == xedc.Prefix);
                if (xedc.Name.StartsWith("XCode")) // IS VALUE LIST VALUE
                {
                    if (exist == null)
                    {
                       // if(
                      //  XbrlElementDataContract
                        existing.Children.Clear();
                        existing.Children.Add(xedc);
                    }
                    // else nothing todo, value (list value) is equal.
                }
                else
                {

                    if (exist != null)
                    {
                        if (xedc.Children != null && xedc.Children.Count > 0 && xedc.Name!="EntityForm")
                        {
                            CheckChildren(ref exist, xedc.Children);
                        }
                        else
                        {
                            if (exist.AutoRendered || !string.IsNullOrEmpty(xedc.Value))
                            {

                                exist.Value = xedc.Value;
                                exist.Locked = xedc.Locked;
                            }
                        }
                    }
                    else
                    {
                        existing.Children.Add(xedc);
                    }
                }
            }
        }
        #endregion

        #region HelperMethods
        public List<XbrlElementDataContract> GetEntityFormClient(Core.EF.Write.Client client)
        {
            if (string.IsNullOrEmpty(client.CLIENT_LEGAL_TYPE_ID)) return null;
            if (client.CLIENT_LEGAL_TYPE_ID == "393") return null;
            return new List<XbrlElementDataContract> {
                new XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = ""
              ,
                Name = string.Format("XCode_LegalFormCode_{0}",client.CLIENT_LEGAL_TYPE_ID)
              ,
                NameSpace = ""
              ,
                Prefix = "pfs-vl"
              ,
                UnitRef = ""
              ,
                Value = client.CLIENT_LEGAL_TYPE_ID
            }
            };

        }

        public List<XbrlElementDataContract> GetPostalCodeCityClient(Core.EF.Write.Client client)
        {
            if (string.IsNullOrEmpty(client.ZipCode) || client.ZipCode.Trim().Length != 4) return null;
            return new List<XbrlElementDataContract> {
                new XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = ""
              ,
                Name = string.Format("XCode_PostalCode_{0}",client.ZipCode.Trim())
              ,
                NameSpace = ""
              ,
                Prefix = "pfs-vl"
              ,
                UnitRef = ""
              ,
                Value = client.ZipCode.Trim()
            }
            };
        }

        public List<XbrlElementDataContract> GetCountryCodeClient(Core.EF.Write.Client client)
        {
            //if (string.IsNullOrEmpty(set.DataContainer.Client.Country)) set.DataContainer.Client.Country="Belgium";
            return new List<XbrlElementDataContract> {
                new XbrlElementDataContract
            {
                AutoRendered = true
              ,
                Children = null
              ,
                Context = ""
              ,
                Period = "D"
              ,
                Decimals = ""
              ,
                Name = string.Format("XCode_CountryCode_BE")
              ,
                NameSpace = ""
              ,
                Prefix = "pfs-vl"
              ,
                UnitRef = ""
              ,
                Value = "BE"
            }
            };
        }

        private XbrlElementDataContract GetClientAddress(Core.EF.Write.Client client)
        {
            XbrlElementDataContract EntityInformation = Xbrl.Elements.FirstOrDefault(x => x.Name == "EntityInformation");
            if (EntityInformation != null && EntityInformation.Children != null)
            {
                XbrlElementDataContract EntityAddress = EntityInformation.Children.FirstOrDefault(x => x.Name == "EntityAddress");
                if (EntityAddress != null)
                {
                    return EntityAddress; // input once, from there on after, only manual overrides.
                }
            }

            string street = "";
            string number = "";
            if (!string.IsNullOrEmpty(client.Address))
            {
                street = client.Address;
                Regex re = new Regex("(?<nr>[0-9]{1,})");
                MatchCollection mc = re.Matches(client.Address);
                if (mc.Count > 0)
                {
                    street = re.Replace(street, "");
                    // street = street.Substring(0, mc[0].Index - 1).Trim();

                }

                re = new Regex("(?<nr>[0-9]{1,})");
                mc = re.Matches(client.Address);
                if (mc.Count > 0)
                {
                    number= mc[0].Value.Trim();

                }
            }

           

            return new XbrlElementDataContract
            {
                AutoRendered = false
              ,
                Children =
              new List<XbrlElementDataContract> {
        new XbrlElementDataContract
      {
        AutoRendered = false
        ,
        Children = 
      new List<XbrlElementDataContract> {
        new XbrlElementDataContract
      {
        AutoRendered = true
        ,
        Children = null
        ,
        Context = ""
        ,
        Period = "D"
        ,
        Decimals = ""
        ,
        Name = "XCode_AddressTypeCode_001"
        ,
        NameSpace = ""
        ,
        Prefix = "pfs-vl"
        ,
        UnitRef = ""
        ,
        Value = "001"
      }
      } 
    
        ,
        Context = ""
        ,
        Period = ""
        ,
        Decimals = ""
        ,
        Name = "AddressType"
        ,
        NameSpace = ""
        ,
        Prefix = "pfs-gcd"
        ,
        UnitRef = ""
        ,
        Value = null
      },new XbrlElementDataContract
      {
        AutoRendered = true
        ,
        Children = null
        ,
        Context = ""
        ,
        Period = "D"
        ,
        Decimals = ""
        ,
        Name = "Street"
        ,
        NameSpace = ""
        ,
        Prefix = "pfs-gcd"
        ,
        UnitRef = ""
        ,
        Value = street
      },new XbrlElementDataContract
      {
        AutoRendered = true
        ,
        Children = null
        ,
        Context = ""
        ,
        Period = "D"
        ,
        Decimals = ""
        ,
        Name = "Number"
        ,
        NameSpace = ""
        ,
        Prefix = "pfs-gcd"
        ,
        UnitRef = "",
        
        Value = number
      },new XbrlElementDataContract
      {
        AutoRendered = true
        ,
        Children = GetPostalCodeCityClient(client)
        ,
        Context = ""
        ,
        Period = ""
        ,
        Decimals = ""
        ,
        Name = "PostalCodeCity"
        ,
        NameSpace = ""
        ,
        Prefix = "pfs-gcd"
        ,
        UnitRef = ""
        ,
        Value = null
      },new XbrlElementDataContract
      {
        AutoRendered = true
        ,
        Children = GetCountryCodeClient(client)
        ,
        Context = ""
        ,
        Period = ""
        ,
        Decimals = ""
        ,
        Name = "CountryCode"
        ,
        NameSpace = ""
        ,
        Prefix = "pfs-gcd"
        ,
        UnitRef = ""
        ,
        Value = null
      }
      }

              ,
                Context = ""
              ,
                Period = ""
              ,
                Decimals = ""
              ,
                Name = "EntityAddress"
              ,
                NameSpace = ""
              ,
                Prefix = "pfs-gcd"
              ,
                UnitRef = ""
              ,
                Value = null
            };
        }

        #endregion

   

        // TO REVIEW
        internal ContextElementDataContract GetContext(string id)
        {
           
            return Xbrl.Contexts.FirstOrDefault(c => c.Id == id);
        }

        #region Indexing

        public void CreateIndexes()
        {
            CreateIndexes(false);
        }

        public void CreateIndexes(bool retry)
        {
            //this.Xbrl.Elements.RemoveAll(r => r.Calculated);
           // try
           // {
                Index = new XbrlIndexDataContract
                {
                    ContextsById = new Dictionary<string, int>()
                    ,
                    ContextsByPeriod = new Dictionary<string, List<int>>()
                    ,
                    ContextsByDimension = new Dictionary<string, List<int>>()
                    ,
                    ContextsByDimensionValue = new Dictionary<string, List<int>>()
                    ,
                    ElementsByContext = new Dictionary<string, List<List<int>>>()
                    ,
                    ElementsByName = new Dictionary<string, List<List<int>>>()
                    ,
                    ElementsByNamePrefix = new Dictionary<string, List<List<int>>>()
                    ,
                    ElementsByPeriodId = new Dictionary<string, List<List<int>>>()
                };

                Index.ContextsByDimension.Add(string.Empty, new List<int>());

                for (int i = 0; i < Xbrl.Contexts.Count; i++)
                {
                    ContextElementDataContract cedc = Xbrl.Contexts[i];
                    Index.ContextsById.Add(cedc.Id, i);

                    if (cedc.Scenario == null || cedc.Scenario.Count == 0)
                    {
                        Index.ContextsByDimension[string.Empty].Add(i);
                    }
                    else
                    {
                        foreach (ContextScenarioDataContract cscdc in cedc.Scenario)
                        {
                            if (!Index.ContextsByDimension.ContainsKey(cscdc.Dimension))
                                Index.ContextsByDimension.Add(cscdc.Dimension, new List<int>());
                            Index.ContextsByDimension[cscdc.Dimension].Add(i);

                            string dimValue = string.Format("{0}!{1}", cscdc.Dimension, cscdc.Value);
                            if (!Index.ContextsByDimensionValue.ContainsKey(dimValue))
                                Index.ContextsByDimensionValue.Add(dimValue, new List<int>());
                            Index.ContextsByDimensionValue[dimValue].Add(i);
                        }
                    }


                    Index.ElementsByContext.Add(cedc.Id, new List<List<int>>());
                }

                IndexElements(Xbrl.Elements, null);

           /* }
            catch (Exception e)
            {
                if (retry)
                {
                    throw e;
                }
                else
                {
                    Xbrl.Elements = CleanupMissingPrefix(Xbrl.Elements, null);
                    CreateIndexes(true);
                }
            }*/



        }


        private List<XbrlElementDataContract> CleanupMissingPrefix(List<XbrlElementDataContract> els, XbrlElementDataContract parent)
        {
            foreach (XbrlElementDataContract el in els)
            {
                if (string.IsNullOrEmpty(el.Prefix))
                {
                    if (parent != null)
                    {
                        el.Prefix = parent.Prefix;
                    }
                    else
                    {
                        if (el.Children.Count(c => !string.IsNullOrEmpty(c.Prefix)) > 0)
                        {
                            el.Prefix = el.Children.First(c => !string.IsNullOrEmpty(c.Prefix)).Prefix;
                        }
                        else {
                            el.Prefix = "tax-inc";
                        }
                    }
                }
                if (el.Children != null && el.Children.Count > 0)
                {
                    el.Children = CleanupMissingPrefix(el.Children, el);
                }
            }
            return els;
        }

        private void IndexElements(List<XbrlElementDataContract> lst, List<int> previous)
        {
            
            for (int i = 0; i < lst.Count; i++)
            {
                List<int> index = new List<int>();
                if (previous != null) index.AddRange(previous);
                index.Add(i);
                XbrlElementDataContract xedc = lst[i];
                if(!Index.ElementsByName.ContainsKey(xedc.Name)) 
                    Index.ElementsByName.Add(xedc.Name,new List<List<int>>());
                Index.ElementsByName[xedc.Name].Add(index);

                string fname = string.Format("{0}:{1}", xedc.Prefix, xedc.Name);

                if (!Index.ElementsByNamePrefix.ContainsKey(fname))
                    Index.ElementsByNamePrefix.Add(fname, new List<List<int>>());
                Index.ElementsByNamePrefix[fname].Add(index);

                if (!string.IsNullOrEmpty(xedc.Period))
                {
                    if (!Index.ElementsByPeriodId.ContainsKey(xedc.Period))
                        Index.ElementsByPeriodId.Add(xedc.Period, new List<List<int>>());
                    Index.ElementsByPeriodId[xedc.Period].Add(index);
                }

                if(!string.IsNullOrEmpty(xedc.ContextRef))
                    {
                        if (Index.ElementsByContext.ContainsKey(xedc.ContextRef))
                        {
                            Index.ElementsByContext[xedc.ContextRef].Add(index);
                        }
                        else
                        {
                            Xbrl.Elements.Remove(xedc);
                        }
                    
                    }

                if (xedc.Children != null && xedc.Children.Count > 0)
                {
                    IndexElements(xedc.Children, index);
                }
            }
        }

        #endregion



        private XbrlElementDataContract GetElementFromXElement(XElement element,BizTaxDataContract xbrl )
        {
            XbrlElementDataContract xed = new XbrlElementDataContract
            {
                AutoRendered = false
                ,
                BinaryValue = null
                ,
                Children = new List<XbrlElementDataContract>()
                ,
                Context = null
                ,
                ContextRef = element.Attribute("contextRef") !=null ? element.Attribute("contextRef").Value : null
                ,
                Decimals = element.Attribute("decimals") != null ? element.Attribute("decimals").Value : null
                ,
                Name = element.Name.LocalName
                ,
                NameSpace = element.Name.Namespace.NamespaceName
                ,
                Period = null
                ,
                Prefix = xbrl.NameSpaces.First(e => e.Uri == element.Name.Namespace.NamespaceName).Prefix
                ,
                UnitRef = element.Attribute("unitRef") != null ? element.Attribute("unitRef").Value : null
                ,
                Value = element.HasElements ? null : element.Value
            };
            if (element.Attribute("contextRef") != null)
            {
                string cref = element.Attribute("contextRef").Value;
                ContextElementDataContract cedc = xbrl.Contexts.FirstOrDefault(c => c.Id == cref);
                if (cedc != null)
                {
                    // PERIOD
                    if (!string.IsNullOrEmpty(cedc.Period.StartDate) && !string.IsNullOrEmpty(cedc.Period.EndDate) && cref.StartsWith("D"))
                    {
                        xed.Period = "D";
                    }
                    // INSTANCES
                    else if (!string.IsNullOrEmpty(cedc.Period.Instant) && cref.StartsWith("I-Start"))
                    {
                        xed.Period = "I-Start";
                    }
                    else if (!string.IsNullOrEmpty(cedc.Period.Instant) && cref.StartsWith("I-End"))
                    {
                        xed.Period = "I-End";
                    }
                    else
                    {
                        throw new Exception("Context " + cref + " UNKNOWN PERIOD");
                    }
                    if (cref.Length > xed.Period.Length)
                    {
                        xed.Context = cref.Substring(xed.Period.Length + 2);
                    }
                }
                else
                {
                    throw new Exception("Context " + cref + " NOT FOUND");
                }
            }

            if (element.HasElements)
            {
                foreach (XElement child in element.Elements())
                {
                    xed.Children.Add(GetElementFromXElement(child, xbrl));
                }
            }

            return xed;
        }

        public virtual BizTaxDataContract Calculate()
        {

            return this.Xbrl;
        }

        public virtual BizTaxDataContract Validate()
        {

            return this.Xbrl;
        }

     
        public virtual BizTaxDataContract CalculateAndValidate()
        {

            return this.Xbrl;
        }

        /* HELPER FUNCTIONS */

        internal void AddMessage(BizTaxErrorDataContract bte)
        {
            if (this.Xbrl.Errors == null) this.Xbrl.Errors = new List<BizTaxErrorDataContract>();
            this.Xbrl.Errors.Add(bte);
            //Console.WriteLine("ERROR:: " + bte.Id + " :: " + bte.Messages.First(f => f.Culture == "nl-BE").Message);
        }

        // NEW SELECTION FUNCTIONS




        #region Create Calc elements

        internal List<XbrlElementDataContract> CreateNewCalculationElement(string scenarioId, string[] periods, string qname, string[] scendefs, string unit, string dec)
        {
            if (periods.Length == 0) throw new Exception("Calculation element can only have 1 period");
            return CreateNewCalculationElement(scenarioId, periods[0], qname, scendefs, unit, dec);
        }

        internal List<XbrlElementDataContract> CreateNewCalculationElement(string scenarioId, string period, string qname, string[] scendefs, string unit, string dec)
        {
            IEnumerable<ContextElementDataContract> cRefs = Xbrl.Contexts.AsQueryable();
            if (scenarioId != null && !string.IsNullOrEmpty(period))
            {
                cRefs = Xbrl.Contexts.Where(c => c.GetMyDefId() == scenarioId && c.PeriodId == period);
            }
            else
            {
                
                if (scenarioId != null)
                {
                    cRefs = cRefs.Where(c => c.GetMyDefId() == scenarioId);
                }
                if (!string.IsNullOrEmpty(period))
                {
                    cRefs = cRefs.Where(c => c.PeriodId == period);
                }
                if (scendefs != null && scendefs.Length > 0)
                {
                    cRefs = cRefs.Where(c => scendefs.Contains(c.ScenarioId));
                }

            }
            ContextElementDataContract cedc = cRefs.FirstOrDefault();
            if (cedc == null)
            {
                throw new Exception("No context found for calculation eleement");
            }

            string[] qn = qname.Split(new char[] { ':' });
            XbrlElementDataContract xel = new XbrlElementDataContract
            {
                Calculated = true
                ,
                Prefix = qn[0]
                ,
                BinaryValue = null
                ,
                Children = null
                ,
                Name = qn[1]
                ,
                Context = cedc.GetMyDefId()
                ,
                Decimals = dec
                ,
                UnitRef = unit
                ,
                Period = period
            };

            XbrlElementDataContract exXel = Xbrl.Elements.FirstOrDefault(e => e.Id == xel.Id);
            if (exXel != null) return Xbrl.Elements.Where(e => e.Id == xel.Id).ToList();
            this.Xbrl.Elements.Add(xel);
            return new List<XbrlElementDataContract> { xel };
        }

        // OLD
        internal List<XbrlElementDataContract> CreateNewCalculationElement(ContextElementDataContract context, string qname, List<string> period, List<DimensionFilter> dimFilters, List<string> scenDefs, string unit, string dec)
        {
            if (period.Count > 1 || period.Count == 0)
            {
                return CreateNewCalculationElement(context, qname, context.PeriodId, dimFilters, scenDefs, unit, dec);
            }
            else
            {
                return CreateNewCalculationElement(context, qname, period.First(), dimFilters, scenDefs, unit, dec);
            }
        }

        internal List<XbrlElementDataContract> CreateNewCalculationElement(ContextElementDataContract context, string qname, string period, List<DimensionFilter> dimFilters, List<string> scenDefs, string unit, string dec)
        {
            //TODO
            string[] qn = qname.Split(new char[] { ':' });

            if (string.IsNullOrEmpty(period))
                period = context.PeriodId;

            ContextElementDataContract fcontext = FindContext(context, period, dimFilters, scenDefs);




            string[] cid = fcontext.Id.Split(new string[] { "__" }, StringSplitOptions.None);
            string ct = string.Join("__", cid, 1, cid.Length - 1);
            XbrlElementDataContract xel = new XbrlElementDataContract
            {
                Calculated = true
                ,
                Prefix = qn[0]
                ,
                BinaryValue = null
                ,
                Children = null
                ,
                Name = qn[1]
                ,
                Context = ct
                ,
                Decimals = dec
                ,
                UnitRef = unit
                ,
                Period = period
            };

            XbrlElementDataContract exXel = Xbrl.Elements.FirstOrDefault(e => e.Id == xel.Id);
            if (exXel != null) return Xbrl.Elements.Where(e => e.Id == xel.Id).ToList();
            this.Xbrl.Elements.Add(xel);
            return new List<XbrlElementDataContract> { xel };
            //throw new NotImplementedException();
        }

        #endregion

        #region Find elements & contexts
        //NEW 

        internal string GetScenarioId(string scenDefId)
        {
            return Xbrl.Contexts.Where(c => c.ScenarioId == scenDefId).Select(c => c.GetMyDefId()).FirstOrDefault();
        }

        internal List<string> GetScenarioIds(string[] scenDefIds)
        {
            return Xbrl.Contexts.Where(c => scenDefIds.Contains(c.ScenarioId)).Select(c => c.GetMyDefId()).Distinct().ToList();
        }

        internal List<string> GetScenarioIdsFuzzy(string[] scenDefIds)
        {
            return Xbrl.Contexts.Where(c => scenDefIds.Count(sd=>c.ScenarioId.Contains(sd))>0).Select(c => c.GetMyDefId()).Distinct().ToList();
        }

        internal bool HasElementsInContext(List<string> scenIds)
        {

            return Xbrl.Elements.FirstOrDefault(x => scenIds.Contains(x.Context) && !x.Calculated && !string.IsNullOrEmpty(x.Value)) == null ? false : true;
        }

        internal bool HasElementsInContext(string scenarioId)
        {
            return Xbrl.Elements.FirstOrDefault(x => x.Context == scenarioId && !x.Calculated && !string.IsNullOrEmpty(x.Value)) == null ? false : true;
        }

        internal List<XbrlElementDataContract> GetElementsByRef(string scenarioId, string[] periods, string[] qnames)
        {
            if (periods.Length == 0) return Xbrl.Elements.Where(x => x.Context == scenarioId && qnames.Contains(x.FullName)).ToList();
            return Xbrl.Elements.Where(x => x.Context == scenarioId && periods.Contains(x.Period) && qnames.Contains(x.FullName)).ToList();
        }

        internal List<XbrlElementDataContract> GetElementsByScenDefsFilter(string scenarioId, string[] scenDefIds, string[] periods, string[] qnames)
        {
            List<ContextElementDataContract> crefs = Xbrl.Contexts.Where(c => scenDefIds.Contains(c.ScenarioId) && periods.Contains(c.PeriodId)).ToList();
            ContextElementDataContract cedc = Xbrl.Contexts.FirstOrDefault(c => c.GetMyDefId() == scenarioId);

            foreach (ContextScenarioDataContract csdc in cedc.Scenario)
            {
                crefs = crefs.Where(c => c.Scenario.Count(s => s.Dimension == csdc.Dimension && s.Value == csdc.Value) > 0).ToList();
            }
            List<string> refs = crefs.Select(c => c.Id).ToList();

            return Xbrl.Elements.Where(x => refs.Contains(x.ContextRef) && qnames.Contains(x.FullName)).ToList();
        }

        internal List<XbrlElementDataContract> GetElementsByScenDefs(string[] scenDefIds, string[] periods, string[] qnames)
        {
            List<string> crefs = Xbrl.Contexts.Where(c => scenDefIds.Contains(c.ScenarioId) && periods.Contains(c.PeriodId)).Select(c => c.Id).ToList();
            return Xbrl.Elements.Where(x => crefs.Contains(x.ContextRef) && qnames.Contains(x.FullName)).ToList();
        }

        // OLDS
        internal ContextElementDataContract FindContext(ContextElementDataContract context, string period, List<DimensionFilter> dimFilters, List<string> scenDefs)
        {


            if (context!=null && scenDefs != null && scenDefs.Count > 0 && (dimFilters==null || dimFilters.Count==0))
            {
                if (scenDefs.Contains(context.ScenarioId) && context.PeriodId == period)
                {
                    return context;
                }
            }

            List<ContextElementDataContract> ctxs = Xbrl.Contexts.Where(p => p.PeriodId == period).ToList();
            if(scenDefs!=null && scenDefs.Count>0) {
                ctxs = ctxs.Where(c => scenDefs.Contains(c.ScenarioId)).ToList();

            }
            if (ctxs.Count > 1 && dimFilters != null && dimFilters.Count > 0)
            {
                foreach (DimensionFilter df in dimFilters)
                {
                    if (!df.Invert)
                    {
                        if (df.Values != null && df.Values.Count > 0)
                        {
                            ctxs = ctxs.Where(c => c.Scenario.Count(s => s.Dimension == df.Dimension && df.Values.Contains(s.Value)) > 0).ToList();
                        }
                        else
                        {
                            ctxs = ctxs.Where(c => c.Scenario.Count(s => s.Dimension == df.Dimension) > 0).ToList();
                        }
                    }
                    else
                    {
                        if (df.Values != null && df.Values.Count > 0)
                        {
                            ctxs = ctxs.Where(c => c.Scenario.Count(s => s.Dimension == df.Dimension && df.Values.Contains(s.Value)) == 0).ToList();
                        }
                        else
                        {
                            ctxs = ctxs.Where(c => c.Scenario.Count(s => s.Dimension == df.Dimension) == 0).ToList();
                        }
                    }
                }
            }
            return ctxs.FirstOrDefault();

        }

        
        internal List<XbrlElementDataContract> FilterElementsByContext(ContextElementDataContract context, List<string> qnames, List<string> periods, List<DimensionFilter> dimensions, List<string> scenDefs)
        {
            if (context == null) return GetElements(qnames, periods, dimensions, scenDefs);
           // List<ContextElementDataContract> ctxs = GetContexts(periods, new List<string> { context.ScenarioDef });
            List<XbrlElementDataContract> elements = Xbrl.Elements.Where(e => qnames.Contains(e.FullName)).ToList();
            if (dimensions == null) dimensions = new List<DimensionFilter>();
            if (scenDefs == null) scenDefs = new List<string>();

            if (periods == null || periods.Count == 0)
            {
                periods = new List<string> { context.PeriodId };
            }

            if ((dimensions == null || dimensions.Count == 0) &&( scenDefs==null || scenDefs.Count==0 || scenDefs.Contains(context.ScenarioId)) )
            {
                foreach (ContextScenarioDataContract scdc in context.Scenario.Where(s => dimensions.Count(d => d.Dimension == s.Dimension) == 0))
                {
                    dimensions.Add(new DimensionFilter { Dimension = scdc.Dimension, Values = new List<string> { scdc.Value } });
                }
            }
            List<ContextElementDataContract> contexts = elements.Select(e => GetContext(e.ContextRef)).Distinct().ToList();
            if (scenDefs != null && scenDefs.Count > 0)
            {
                contexts = contexts.Where(c => scenDefs.Contains(c.ScenarioId)).ToList();
            }

            

            foreach (DimensionFilter df in dimensions)
            {
                if (!df.Invert)
                {
                    if (df.Values != null && df.Values.Count > 0)
                    {
                        contexts = contexts.Where(c => c.Scenario.Count(s => s.Dimension == df.Dimension && df.Values.Contains(s.Value)) > 0).ToList();
                    }
                    else
                    {
                        contexts = contexts.Where(c => c.Scenario.Count(s => s.Dimension == df.Dimension) > 0).ToList();
                    }
                }
                else
                {
                    if (df.Values != null && df.Values.Count > 0)
                    {
                        contexts = contexts.Where(c => c.Scenario.Count(s => s.Dimension == df.Dimension && df.Values.Contains(s.Value)) == 0).ToList();
                    }
                    else
                    {
                        contexts = contexts.Where(c => c.Scenario.Count(s => s.Dimension == df.Dimension) == 0).ToList();
                    }
                }
            }
            List<string> scendefs = contexts.Select(s => s.ScenarioId).Distinct().ToList();


            if (periods != null && periods.Count > 0)
            {
                contexts = contexts.Where(c => periods.Contains(c.PeriodId)).ToList();
            }
            else
            {
                if (context != null)
                {
                    contexts = contexts.Where(c => c.PeriodId == context.PeriodId).ToList();
                }
            }

            /*
            if (contexts.Count(c=>c.Id==context.Id) == 1)
            {
                return elements.Where(e => e.ContextRef == context.Id).ToList();
            }
             * 
             **/
            if (scendefs != null && scendefs.Count() > 1 && contexts.Count(c => c.GetMyDefId() == context.GetMyDefId()) == 1)
            {
                contexts = new List<ContextElementDataContract> { context };
            }
            List<string> refs = contexts.Select(c=>c.Id).ToList();
            elements = elements.Where(e => refs.Contains(e.ContextRef)).ToList();

            /*
            foreach (ContextElementDataContract ctx in contexts)
            {
                foreach (DimensionFilter df in dimensions)
                {
                    int cnt =ctx.Scenario.Count(s => s.Dimension == df.Dimension);

                    if (df.Invert)
                    {
                        // NOT()
                        if (cnt > 0)
                        {
                            if (df.Values!=null && df.Values.Count>0)
                            {
                                ContextScenarioDataContract csdc = ctx.Scenario.FirstOrDefault(s => s.Dimension == df.Dimension);
                                if(csdc!=null && df.Values.Contains(csdc.Value))
                                {
                                    elements = elements.Where(e => e.ContextRef != ctx.Id).ToList();
                                }
                            }
                            else
                            {
                                elements = elements.Where(e => e.ContextRef != ctx.Id).ToList();
                            }
                        }
                    }
                    else
                    {
                        if (cnt == 0)
                        {
                            elements = elements.Where(e => e.ContextRef != ctx.Id).ToList();
                        }
                        else
                        {
                            if (df.Values!=null && df.Values.Count>0)
                            {
                                ContextScenarioDataContract csdc = ctx.Scenario.First(s => s.Dimension == df.Dimension);
                                if (!df.Values.Contains(csdc.Value))
                                {
                                    elements = elements.Where(e => e.ContextRef != ctx.Id).ToList();
                                }
                            }

                        }
                    }

                    
                }
            }
             * */
            return elements;
           // ctxs.Where(c=> c.Scenario.Where(
           // throw new NotImplementedException();
        }


        internal List<XbrlElementDataContract> GetElements(List<string> qnames, List<string> periods, List<DimensionFilter> dimensions, List<string> scenDefs)
        {
            List<ContextElementDataContract> contexts = Xbrl.Contexts;
            if(periods !=null && periods.Count>0) {
                contexts = contexts.Where(c => periods.Contains(c.PeriodId)).ToList();
            }
            if (scenDefs.Count > 0)
            {
                contexts = contexts.Where(c => scenDefs.Contains(c.ScenarioId)).ToList();

            }
            foreach (DimensionFilter df in dimensions)
            {
                if (!df.Invert)
                {
                    if (df.Values != null && df.Values.Count > 0)
                    {
                        contexts = contexts.Where(c => c.Scenario.Count(s => s.Dimension == df.Dimension && df.Values.Contains(s.Value)) > 0).ToList();
                    }
                    else
                    {
                        contexts = contexts.Where(c => c.Scenario.Count(s => s.Dimension == df.Dimension) > 0).ToList();
                    }
                }
                else
                {
                    if (df.Values != null && df.Values.Count > 0)
                    {
                        contexts = contexts.Where(c => c.Scenario.Count(s => s.Dimension == df.Dimension && df.Values.Contains(s.Value)) == 0).ToList();
                    }
                    else
                    {
                        contexts = contexts.Where(c => c.Scenario.Count(s => s.Dimension == df.Dimension) == 0).ToList();
                    }
                }
            }

            List<string> refs = contexts.Select(c => c.Id).ToList();
            return Xbrl.Elements.Where(e => refs.Contains(e.ContextRef) && qnames.Contains(e.FullName)).ToList();
        }

        internal List<ContextElementDataContract> GetContexts(List<string> periods, List<string> scenDefs)
        {
            if (periods.Count > 0)
            {
                return Xbrl.Contexts.Where(c => periods.Contains(c.PeriodId) && scenDefs.Contains(c.ScenarioId)).ToList();
            }

            return Xbrl.Contexts.Where(c => scenDefs.Contains(c.ScenarioId)).ToList();
        }


       

        #endregion

        #region helper function
        public static string ToBelgianEnterprise(string entNr)
        {
            if (string.IsNullOrEmpty(entNr)) return "";
            entNr = entNr.Replace(".", "").Replace("BE", "");
            while (entNr.Length < 10)
            {
                entNr = "0" + entNr;
            }
            return "BE" + entNr;
        }

        public static string ToIso8601DateOnly(DateTime dte)
        {
            
            return dte.ToString("yyyy-MM-dd");
        }

    #endregion

        /* CALCULATION FUNCTIONS */

       

        #region Calculation Functions



        public XbrlElementDataContract GetNodeByName(string prefix, string name)
        {
            // INDEXED
         //   return GetElements(name, prefix).FirstOrDefault();
            return Xbrl.Elements.FirstOrDefault(e => e.Prefix == prefix && e.Name == name);
        }

        public List<object> GetFromPath(string path)
        {
            // xbrli is default xbrl ns, todo: utilise variable for this and true namespace handling
            path = path.Replace("xbrli:", "");
            // remove root node all if present
            path = path.Replace("/xbrl","");

            string[] paths = path.Split(new char[] { '/' });

            // get base list
            List<object> list = new List<object>();
            if(paths.Length==0) return list;
            string rootPath = paths[0];
            
            if (rootPath.Contains(":"))
            {
                return GetPathFromElements(paths.ToList());
            } 
            else {
                switch (paths[0].ToLower())
                {
                    case "context":
                        return GetPathFromContext(paths.Skip(1).ToList());
                        break;
                    case "unit":
                        return GetPathFromUnits(paths.Skip(1).ToList());
                        break;
                }
            }
            return new List<object>();


        }

        public List<object> GetPathFromElements(List<string> paths)
        {
            IEnumerable<XbrlElementDataContract> els = Xbrl.Elements;
            foreach (string path in paths)
            {
                string[] pathSplit = path.Split(new char[] { ':' });
                string prefix = pathSplit.Length > 1 ? pathSplit[0] : null;
                string name = pathSplit.Length > 1 ? pathSplit[1] : pathSplit[0];
                els = els.Where(e => e.Name.ToLower() == name.ToLower() && (!string.IsNullOrEmpty(e.Prefix) && !string.IsNullOrEmpty(prefix) && prefix.ToLower() == e.Prefix.ToLower()));

            }
            return els.Cast<object>().ToList();
        }

        public List<object> GetPathFromContext(List<string> paths)
        {
            IEnumerable<ContextElementDataContract> els = Xbrl.Contexts;

            switch(paths[0]) {
                case "period":
                        IEnumerable<ContextPeriodDataContract> periods = els.Select(e=>e.Period);
                        if(paths.Count>1) {

                            switch(paths[1].ToLower()) {
                                case "instant":
                                    return periods.Where(p=>!string.IsNullOrEmpty(p.Instant)).Select(p=>p.Instant).Cast<object>().ToList();
                                    break;
                                case "startdate":
                                    return periods.Where(p=>!string.IsNullOrEmpty(p.StartDate)).Select(p=>p.StartDate).Cast<object>().ToList();
                                    break;
                                case "enddate":
                                    return periods.Where(p=>!string.IsNullOrEmpty(p.EndDate)).Select(p=>p.EndDate).Cast<object>().ToList();
                                    break;
                            }
                        }
                        return periods.Cast<object>().ToList();
                        break;
                case "entity":
                        IEnumerable<ContextEntityDataContract> ents = els.Select(e=>e.Entity);
                         if(paths.Count>1) {

                            switch(paths[1].ToLower()) {
                                case "identifier":
                                    return ents.Where(p=>!string.IsNullOrEmpty(p.IdentifierValue)).Select(p=>p.IdentifierValue).Cast<object>().ToList();
                                    break;
                            }
                        }
                        return ents.Cast<object>().ToList();
                        break;
            }

            return new List<object>();
        }

        public List<object> GetPathFromUnits(List<string> paths)
        {
            IEnumerable<UnitElementDataContract> els = Xbrl.Units;

            if (paths.Count > 0)
            {
                if (paths[0] == "measure")
                {
                    return els.Select(e => e.Measure).Cast<object>().ToList();
                }
            }

            return els.Cast<object>().ToList();
        }

        public decimal Sum(XbrlElementDataContract[] elements)
        {
            return elements.Sum(e => e.GetNumberOrDefault(0));
        }

        public decimal Sum(params decimal[] elements)
        {
            return elements.Sum(e => e);
        }

        private bool IsNumeric(string value)
        {
            return Regex.IsMatch(value, @"^-?\d*[0-9]?(|.\d*[0-9]|,\d*[0-9])?$");
        }
        
        public string String(string el)
        {
            if (IsNumeric(el))
            {
                el = Number(el).ToString();
            }
            return el;
        }

        public string String(bool el)
        {
            return el.ToString().ToLower();
        }
    
        public string String(IEnumerable<XbrlElementDataContract> el) 
        {
            if (el == null) return string.Empty;
            XbrlElementDataContract x = el.FirstOrDefault();
            return String(x);
        }

        public string String(decimal d)
        {
            d = Math.Round((d/100)*100, 4);
            return d.ToString(CultureInfo.InvariantCulture.NumberFormat);
        }

        public string String(object el)
        {
            return String(el, string.Empty);
        }

        public string String(object el,string def)
        {
            if (el == null) return def;
            if (el is string) return !string.IsNullOrEmpty((string)el) ?
                IsNumeric((string)el) ? String(Number((string)el)) : (string)el
                : def;
            if (el is List<string>) return ((List<string>)el).Count>0 ? ((List<string>)el).First() : def;
            if (el is IEnumerable<XbrlElementDataContract>) return String(((IEnumerable<XbrlElementDataContract>)el).FirstOrDefault(),def);
            if (el == null) return !string.IsNullOrEmpty(def) ? def : string.Empty;
            if(el.GetType().Name=="XbrlElementDataContract") return !string.IsNullOrEmpty(((XbrlElementDataContract)el).Value) ? String(((XbrlElementDataContract)el).Value,def) : def;
            return !string.IsNullOrEmpty(el.ToString()) ? el.ToString() : def;
        }

        public bool Bool(List<XbrlElementDataContract> els, string def)
        {
            bool bdef = def.ToLower().Replace("()","")=="true";
            if (els.Count == 0) return bdef;
            return Bool(els.Select(e => e.GetBool()).ToList(),bdef);
        }

        public bool Bool(List<bool?> list, bool bdef)
        {
            if (list.Count(b => !b.HasValue)>0) return bdef;
            return Bool(list.Select(b => b.GetValueOrDefault()).ToList());
        }

        public bool Bool(XbrlElementDataContract el)
        {
            if (el == null) return false;
            if(string.IsNullOrEmpty(el.Value)) return false;
            bool res = false;
            bool.TryParse(el.Value, out res);
            return res;
        }

        public bool Bool(bool el)
        {
            return el;
        }


        public bool Bool(List<bool> lst)
        {

            return lst.Count>0 && lst.Count(c=>!c)==0;
        }

        public int Integer(object el)
        {
            if (el.GetType().Name == "XbrlElementDataContract") 
                return ((XbrlElementDataContract)el).GetIntegerOrDefault(0);

            int t = 0;
            int.TryParse(el.ToString(), out t);
            return t;
        }

        public int Integer(int el)
        {
            return el;
        }

        public bool EndsWith(string input, string comparison)
        {
            return input.EndsWith(comparison);
        }

        public decimal Number(int el)
        {
            return (decimal)el;
        }

        public decimal Number(decimal el)
        {
            return el;
        }

        public decimal Number(decimal el,string fb)
        {
            return el;
        }

        public decimal Number(int el, string fb)
        {
            return el;
        }

        public decimal DateNumber(string val)
        {
            if (string.IsNullOrEmpty(val)) return 0;
            decimal res = 0;
            decimal.TryParse(val.Replace("-", ""), NumberStyles.Any, CultureInfo.InvariantCulture.NumberFormat, out res);
            return res;
        }

        public decimal DateNumber(string val, string fallback)
        {
            decimal fb = 0;
            decimal.TryParse(fallback, NumberStyles.Any, CultureInfo.InvariantCulture.NumberFormat, out fb);
            if (string.IsNullOrEmpty(val)) return fb;
            decimal res = 0;
            decimal.TryParse(val.Replace("-", ""), NumberStyles.Any, CultureInfo.InvariantCulture.NumberFormat, out res);
            return res==0 ? fb : res;
        }

        public decimal DateNumber(IEnumerable<XbrlElementDataContract> els, string fallback)
        {
            decimal fb = 0;
            decimal.TryParse(fallback, NumberStyles.Any, CultureInfo.InvariantCulture.NumberFormat, out fb);
            if (els == null) return fb;
            return DateNumber(els);
        }

        public decimal DateNumber(IEnumerable<XbrlElementDataContract> els)
        {
            return els.Sum(x => x.GetDateNumberOrDefault(0));
        }

        public decimal Number(IEnumerable<XbrlElementDataContract> els)
        {
            return els.Sum(x => x.GetNumberOrDefault(0));
        }

        public decimal Number(IEnumerable<XbrlElementDataContract> els,string fallback)
        {
            decimal fb = 0;
            decimal.TryParse(fallback, NumberStyles.Any, CultureInfo.InvariantCulture.NumberFormat, out fb);
            if (els == null) return fb;
            return els.Sum(x => x.GetNumberOrDefault(fb));
        }

        public decimal Number(string el)
        {
            decimal fb = 0;
            decimal.TryParse(el, NumberStyles.Any, CultureInfo.InvariantCulture.NumberFormat, out fb);
            return (fb / 100)*100 ;
        }

        public decimal Number(object el)
        {
            if (el == null) return 0;
            if (el.GetType().Name == "XbrlElementDataContract")
                return ((XbrlElementDataContract)el).GetNumberOrDefault(0);
            if (el.GetType().Name == typeof(IQueryable<XbrlElementDataContract>).Name)
            {
                if (((IQueryable<XbrlElementDataContract>)el).Count() == 0) return 0;
                return ((IQueryable<XbrlElementDataContract>)el).Sum(e => e.GetNumberOrDefault(0));
            }
            if (el.GetType().Name == "WhereListIterator`1")
            {
                if (((IEnumerable<XbrlElementDataContract>)el).Count() == 0) return 0;
                return ((IEnumerable<XbrlElementDataContract>)el).Sum(e => e.GetNumberOrDefault(0));
            }
            
            decimal t = 0;
            decimal.TryParse(el.ToString(), out t);
            return t;
        }

        public DateTime? Date(DateTime el)
        {
            return el;
        }

        public DateTime? Date(object el)
        {
            // TODO: HANDLE NULLS
            if (el == null) return null;
            if (el.GetType().Name == "DateTime")
                return (DateTime)el;

            if (el.GetType().Name == "XbrlElementDataContract")
                return ((XbrlElementDataContract)el).GetDate();

            DateTime result = DateTime.MinValue;
            DateTime.TryParse(el.ToString(), null, DateTimeStyles.RoundtripKind, out result);
            if (result == DateTime.MinValue) return null;
            return result;
        }

        public Duration YearMonthDuration(string type)
        {
            //DateTime dte = DateTime.MinValue;
            return new Duration(type);
        }

        public TimeSpan DayTimeDuration(string type)
        {
            if (type == "P1D") return new TimeSpan(1, 0, 0, 0);
            return new TimeSpan();
            // ONE DAY P1D
        }

        public int DaysFromDuration(TimeSpan ts)
        {
            return ts.Days;
        }

        public int DaysFromDuration(DateTime dte)
        {
            TimeSpan ts = dte - new DateTime(0, 0, 0);
            return DaysFromDuration(ts);
        }

        public DateTime Date(IEnumerable<XbrlElementDataContract> el,string def)
        {
            return Date(el.First());
        }

        public DateTime Date(IEnumerable<XbrlElementDataContract> el)
        {
            return Date(el.First());
        }

        public DateTime Date(XbrlElementDataContract el)
        {
            if (el == null) 
                return DateTime.Now;
            return DateTime.Parse(el.Value);
        }

        /*
         * HANDLE IN GENERATION: Add or substract one year from datetime
        public int YearMonthDuration(string type)
        {
            
            throw new Exception();
            // ONE YEAR P1Y
        }*/

        // public int DaysFromDuration ==> TimeSpan.Days

        public string QName(string value)
        {
            if (value == "xbrli:pure") return "pure";
            return value;
        }

        public string QName(string ns, string nodename)
        {
            if (ns.Contains(" ")) ns = ns.Replace(" ", ""); // CLEANUP SPACES, just in case.
            XbrlNamespace xn = Xbrl.NameSpaces.FirstOrDefault(n => n.Uri == ns);
            
            
            string[] nms = nodename.Split(new char[] { ':' });
            string localname = nms.Length > 1 ? nms[1] : nms[0];
            if (xn == null) return localname;

            return string.Format("{0}:{1}", xn.Prefix, localname);
        }

        public string LocalNameFromQname(string qname)
        {
            if (string.IsNullOrEmpty(qname)) return null;
            string[] nms = qname.Split(new char[] { ':' });
            return nms.Length > 1 ? nms[1] : nms[0];
        }

        public string Data(string name)
        {
            //??
            return name;
        }

        public string FactExplicitScenarioDimensionValue(IEnumerable<XbrlElementDataContract> qel,string dimensionName)
        {
            XbrlElementDataContract el = qel.FirstOrDefault();
            if (el == null) return null;
            ContextElementDataContract cedc = Xbrl.Contexts.FirstOrDefault(c => c.Id == el.ContextRef);
            if (cedc == null) return null;
            ContextScenarioDataContract csdc = cedc.Scenario.FirstOrDefault(s => s.Name == dimensionName);
            if (csdc == null) return null;
            return csdc.Dimension;

        }

        public int StringLength(object el)
        {
            if (el.GetType().Name != "String") el = String(el);
            return el.ToString().Length;
        }

        public string Substring(object el, int from)
        {
            return Substring(el, from, null);
        }

        public string Substring(object el, decimal from, decimal? length)
        {
            return Substring(el, Convert.ToInt32(from), length.HasValue ? Convert.ToInt32(length.Value) : (int?)null);
        }

        public string Substring(object el, int from, int? length)
        {
            from = from - 1;
            if (from < 0) return string.Empty;
            if (el.GetType().Name != "String") el = String(el);
            if (length.HasValue)
            {
                if (length.Value > el.ToString().Length) return el.ToString().Substring(from);
                return el.ToString().Substring(from,length.Value);
            }
            return el.ToString().Substring(from);
        }

        public string SubstringBefore(string source, string before)
        {
            if (string.IsNullOrEmpty(source) || string.IsNullOrEmpty(before)) return source;
            return source.Substring(0, source.IndexOf(before));
        }

        public string SubstringAfter(string source, string after)
        {
            return source.Substring(source.IndexOf(after) + after.Length);
        }

        public bool Exists(decimal el)
        {
            return true;
        }


        public bool Exists(XbrlElementDataContract el)
        {
            return el != null && !string.IsNullOrEmpty(el.Value);
        }


        public bool Exists(IEnumerable<XbrlElementDataContract> els)
        {
            return els.FirstOrDefault() != null && !string.IsNullOrEmpty(els.FirstOrDefault().Value);
        }

        public List<string> GetFieldIdsOf(List<object> items)
        {
            List<XbrlElementDataContract> xels = new List<XbrlElementDataContract>();
            items.Where(i => i.GetType().Name == typeof(XbrlElementDataContract).Name).ToList().ForEach(i =>
            {
                xels.Add((XbrlElementDataContract)i);
            });

            items.Where(i => i.GetType().Name == typeof(List<XbrlElementDataContract>).Name).ToList().ForEach(i =>
            {
                xels.AddRange((List<XbrlElementDataContract>)i);
            });
            return xels.Select(x => x.Id.Substring(x.Prefix.Length+1)).ToList();
        }
       

        public int Count(XbrlElementDataContract[] elements)
        {

            return elements.Count(e => e != null && !string.IsNullOrEmpty(e.Value));
        }

        public int Count(List<XbrlElementDataContract> elements)
        {

            return elements.Count(e => e != null && !string.IsNullOrEmpty(e.Value));
        }

        public int Count(string[] elements)
        {
            return elements.Count(e => !string.IsNullOrEmpty(e));
        }

        public int Count(List<string> elements)
        {
            return elements.Count(e => !string.IsNullOrEmpty(e));
        }

        // TODO: REFINE IF NEEDED
        public int Count(params object[] elements)
        {
            if (elements is IEnumerable<XbrlElementDataContract>[] || typeof(IEnumerable<XbrlElementDataContract>[]).IsAssignableFrom(elements.GetType()))
            {
                return Count((IEnumerable<XbrlElementDataContract>[])elements);
            }
            return elements.Count();
            //return Count(elements.Cast<XbrlElementDataContract>().ToList());
        }

        public int Count(params IEnumerable<XbrlElementDataContract>[] elements)
        {
            int cnt = 0;
            foreach (IEnumerable<XbrlElementDataContract> el in elements)
            {
                cnt += el.Count();
            }
            return cnt;
        }

     


        // DISTINCT ARE USUALY ONLY USED IN COUNTS ?
        public List<string> DistinctValues(List<string> list)
        {
            return new HashSet<string>(list).ToList();
        }


        public List<string> DistinctValues(List<XbrlElementDataContract> list) 
        {
            return list.Select(l => l.Value).Distinct().ToList();
        }

        public List<string> DistinctValues(string path)
        {
            List<object> list = GetFromPath(path);
            if (list.Count == 0) return new List<string>();
            if (list.First().GetType().Name == "XbrlElementDataContract")
            {
                return DistinctValues(list.Cast<XbrlElementDataContract>().ToList());
            }
            return list.Select(o=>o.ToString()).Distinct().ToList();

        }

        private decimal GetDecimalOfObject(object o)
        {
            decimal d = 0;
            if (o.GetType().Name == typeof(XbrlElementDataContract).Name)
            {
                d = ((XbrlElementDataContract)o).GetNumberOrDefault(0);
            } else {
                decimal.TryParse(o.ToString(), out d);
            }

            return d;
        }

        public decimal Max(object m1, object m2)
        {
            decimal d1 = GetDecimalOfObject(m1);
            decimal d2 = GetDecimalOfObject(m2);

            return Math.Max(d1, d2);

        }

        public decimal Max(params decimal[] decs)
        {
            if (decs.Length == 0) return 0;
            if (decs.Length == 1) return decs[0];
            if (decs.Length == 2) return Math.Max(decs[0], decs[1]);

            decimal mx = Math.Max(decs[0],decs[1]);
            for (int i = 2; i < decs.Length; i++)
            {
                mx = Math.Max(mx, decs[i]);
            }
            return mx;

        }

        public decimal Min(params decimal[] decs)
        {
            if (decs.Length == 0) return 0;
            if (decs.Length == 1) return decs[0];
            if (decs.Length == 2) return Math.Min(decs[0], decs[1]);

            decimal mn = Math.Min(decs[0], decs[1]);
            for (int i = 2; i < decs.Length; i++)
            {
                mn = Math.Min(mn, decs[i]);
            }
            return mn;

        }

        public decimal Min(object m1, object m2)
        {
            decimal d1 = GetDecimalOfObject(m1);
            decimal d2 = GetDecimalOfObject(m2);

            return Math.Min(d1, d2);
        }

        public bool exists(object el)
        {
            return false;
        }

        public List<string> distinctvalues(IEnumerable<string> lst)
        {
            return lst.Where(s=>s!=null).Distinct().ToList();
        }

        public List<XbrlElementDataContract> distinctvalues(IEnumerable<XbrlElementDataContract> lst)
        {
            return lst.Distinct().ToList();
        }

        public XbrlElementDataContract CreateNewElement(string name, string namspc, string periodtype, string decimals, string unitRef, ContextElementDataContract cedc, IEnumerable<XbrlElementDataContract> source)
        {
            if (cedc != null)
            {
                // create context if not exists !
                XbrlNamespace xns = Xbrl.NameSpaces.FirstOrDefault(n => n.Uri.ToLower() == namspc.ToLower());
                string prefix = xns != null ? xns.Prefix : null;
                string period = cedc.Id.Substring(0,cedc.Id.IndexOf("__"));
                XbrlElementDataContract xedc = new XbrlElementDataContract
                {
                    AutoRendered = true,
                    Children = new List<XbrlElementDataContract>(),
                    Period = periodtype == "duration" ? "D" : period,
                    Context = cedc.Id.Substring(period.Length + 2),
                    Decimals = decimals,
                    UnitRef = unitRef,
                    Value = null,
                    Prefix = prefix,
                    NameSpace= namspc
                };
                Xbrl.Elements.Add(xedc);
                
                
                return xedc;

                     
            }
            return null;
        }

        public decimal NumericOr(params decimal[] decs)
        {
            decimal? dec = decs.FirstOrDefault(d => d != 0);
            return dec.GetValueOrDefault();
            //throw new NotImplementedException();
        }

        public bool NumericOr(params bool[] bls)
        {
           
            for (int i = 0; i < bls.Length; i++)
            {
                if (bls[i]) return true;
            }
            return false;
        }

        public List<string> ToList(string p, string p_2)
        {
            return new List<string> { p, p_2 };
        }


        #endregion
        // Max, Min, Round via System.Math.
        // true() and false() to true & false
        // not => !(...)
        // YearMonthDuration Add or substract one year from date
        // days-from-duration: (date1 - date2).days (=timespan)
        //

        public void Clean(XbrlElementDataContract xel)
        {
            if (xel.Children != null && xel.Children.Count > 0)
            {
                xel.Children.ForEach(x => { Clean(x); });
                //xel.Children.RemoveAll(x => string.IsNullOrEmpty(x.Value) && (x.Children == null || x.Children.Count == 0));
                xel.Children.RemoveAll(x => x.Name != null && !x.Name.Contains("Xcode") && (x.BinaryValue == null || x.BinaryValue.Count == 0) && string.IsNullOrEmpty(x.Value) && (x.Children == null || x.Children.Count == 0));
            }
             
        }

        public XDocument RenderBizTaxFile(BizTaxDataContract btdc, Guid fileId, List<XmlQualifiedName> NameSpaces, string SchemaRef)
        {


            Dictionary<string, XNamespace> ns = NameSpaces.ToDictionary(k => k.Name, v => XNamespace.Get(v.Namespace));

            XNamespace defaultns = ns[""];

            XElement biztax = new XElement("biztax");

            XElement xbrl = new XElement(defaultns + "xbrl");
            foreach (XmlQualifiedName qname in NameSpaces)
            {
                if (!string.IsNullOrEmpty(qname.Name))
                {
                    xbrl.Add(new XAttribute(XNamespace.Xmlns + qname.Name, qname.Namespace));
                }
            }
            //xbrl.Add(new XAttribute("xmlns", "http://www.xbrl.org/2003/instance"));
            biztax.Add(xbrl);
            XDocument doc = XDocument.Parse(biztax.ToString());
            xbrl = doc.Root.Element(defaultns + "xbrl");


            xbrl.Add(new XElement(ns["link"] + "schemaRef"
                        , new XAttribute(ns["xlink"] + "href", SchemaRef)
                        , new XAttribute(ns["xlink"] + "type", "simple")
                        )
            );

            // cleanup
            btdc.Elements.ForEach(x => { Clean(x); });
            btdc.Elements.RemoveAll(x => string.IsNullOrEmpty(x.Value) && (x.Children == null || x.Children.Count == 0));
          //  btdc.Elements.RemoveAll(x => x.Calculated);
            List<string> ctxRefs =btdc.Elements.Select(e => e.ContextRef).Distinct().ToList();
            btdc.Contexts.RemoveAll(c => c.Scenario != null && c.Scenario.Count > 0 && !ctxRefs.Contains(c.Id));

            foreach (ContextElementDataContract ctedc in btdc.Contexts)
            {
                XElement ctx = new XElement(defaultns + "context", new XAttribute("id", ctedc.Id));
                XElement ent = new XElement(defaultns + "entity", new XElement(defaultns + "identifier", new XAttribute("scheme", ctedc.Entity.IdentifierScheme), ctedc.Entity.IdentifierValue));
                XElement period = new XElement(defaultns + "period");
                if (!string.IsNullOrEmpty(ctedc.Period.Instant))
                {
                    period.Add(new XElement(defaultns + "instant", ctedc.Period.Instant));
                }
                else
                {
                    period.Add(new XElement(defaultns + "startDate", ctedc.Period.StartDate), new XElement(defaultns + "endDate", ctedc.Period.EndDate));
                }
                ctx.Add(ent, period);
                if (ctedc.Scenario != null && ctedc.Scenario.Count > 0)
                {
                    XElement scen = new XElement(defaultns + "scenario");
                    foreach (ContextScenarioDataContract csdc in ctedc.Scenario)
                    {
                        XElement cts = null;
                        if (csdc.GetType() == typeof(ContextScenarioExplicitDataContract))
                        {
                            ContextScenarioExplicitDataContract csedc = (ContextScenarioExplicitDataContract)csdc;
                            cts = new XElement(ns["xbrldi"] + "explicitMember", new XAttribute("dimension", csedc.Dimension), csedc.Value);
                        }

                        else if (csdc.GetType() == typeof(ContextScenarioTypeDataContract))
                        {
                            /*
                              <xbrldi:typedMember dimension="d-ty:DescriptionTypedDimension">
                                    <d-ty:DescriptionTypedID>anderereserves3</d-ty:DescriptionTypedID>
                               </xbrldi:typedMember>
                             */
                            ContextScenarioTypeDataContract cstdc = (ContextScenarioTypeDataContract)csdc;
                            cts = new XElement(ns["xbrldi"] + "typedMember", new XAttribute("dimension", cstdc.Dimension));
                            string[] csName = cstdc.Type.Split(new char[] { ':' });
                            cts.Add(new XElement(ns[csName[0]] + csName[1], cstdc.Value));
                        }
                        else
                        {
                            if (csdc.Name == "explicitMember")
                            {
                                cts = new XElement(ns["xbrldi"] + "explicitMember", new XAttribute("dimension", csdc.Dimension), csdc.Value);
                            }
                            else if (csdc.Name == "typedMember")
                            {
                                cts = new XElement(ns["xbrldi"] + "typedMember", new XAttribute("dimension", csdc.Dimension));
                                string[] csName = csdc.Type.Split(new char[] { ':' });
                                cts.Add(new XElement(ns[csName[0]] + csName[1], csdc.Value));
                            }
                        }
                        if (cts != null)
                        {
                            scen.Add(cts);
                        }
                    }
                    ctx.Add(scen);
                }
                xbrl.Add(ctx);
            }

            foreach (UnitElementDataContract unit in btdc.Units)
            {
                xbrl.Add(new XElement(defaultns + "unit", new XAttribute("id", unit.Id), new XElement(defaultns + "measure", unit.Measure)));
            }



            foreach (XbrlElementDataContract xedc in btdc.Elements)
            {
                xbrl.Add(XbrlElementToXml(xedc, ns, fileId));
            }


            //doc.Save(@"C:\Projects_TFS\EY\Ebook\Main-v5.0\DATA\BIZTAX_" + Guid.NewGuid().ToString() + ".xml");

            return doc;
        }

        public XElement XbrlElementToXml(XbrlElementDataContract xedc, Dictionary<string, XNamespace> ns, Guid fileid)
        {
            return XbrlElementToXml(xedc, ns, fileid, null);
        }

        public XElement XbrlElementToXml(XbrlElementDataContract xedc, Dictionary<string, XNamespace> ns, Guid fileid, string parentPrefix)
        {
            if (string.IsNullOrEmpty(xedc.Prefix) && !string.IsNullOrEmpty(parentPrefix)) xedc.Prefix = parentPrefix;
            if (xedc.Name == "PostalCodeCity" && xedc.Children.Count(c => c.Name == "PostalCodeOther") > 0)
            {
                List<XbrlElementDataContract> cs = CoreHelper.DeepConeDC(xedc.Children);
                xedc.Children = new List<XbrlElementDataContract> {
                    new XbrlElementDataContract {
                    AutoRendered = true
                    ,
                    Children = cs
                    ,
                    Context = ""
                    ,
                    Period = ""
                    ,
                    Decimals = ""
                    ,
                    Name = "PostalCodeCityOther"
                    ,
                    NameSpace = ""
                    ,
                    Prefix = "pfs-gcd"
                    ,
                    UnitRef = ""
                    ,
                    Value = null
                  
                }
                };
            }
            XElement xed = new XElement(ns[xedc.Prefix] + xedc.Name);
            if (!string.IsNullOrEmpty(xedc.ContextRef)) xed.Add(new XAttribute("contextRef", xedc.ContextRef));
            if (!string.IsNullOrEmpty(xedc.Decimals) && xedc.Value!="false" && xedc.Value!="true") xed.Add(new XAttribute("decimals", xedc.Decimals));
            if (!string.IsNullOrEmpty(xedc.UnitRef) && xedc.Value != "false" && xedc.Value != "true") xed.Add(new XAttribute("unitRef", xedc.UnitRef));

            if (xedc.BinaryValue != null && xedc.BinaryValue.Count > 0)
            {
                ITextRepository it = new ITextRepository();
                string base64 = it.GenerateBundleItemsBase64(CoreHelper.DeepConeDC(xedc.BinaryValue), fileid);
                xed.Add(base64);
                // GET BASE 64 ENCODED BUNDLE
            }
            else
            {
                if (!string.IsNullOrEmpty(xedc.Decimals) && xedc.Value != null && xedc.Value != "" && xedc.Value != "false" && xedc.Value != "true")
                {
                     xedc.Value = Math.Round(xedc.GetNumberOrDefault(0), 2, MidpointRounding.AwayFromZero).ToXbrlValue();
                 }

                if (xedc.Value != "[object Object]")
                {
                    xed.Add(xedc.Value);
                }

                if (xedc.Children != null && xedc.Children.Count > 0)
                {
                    foreach (XbrlElementDataContract xedcChild in xedc.Children)
                    {
                        XElement xedChild = XbrlElementToXml(xedcChild, ns, fileid, xedc.Prefix);
                        if (xedChild != null && (!string.IsNullOrEmpty(xedChild.Value) || xedChild.Elements().Count() > 0))
                        {
                            xed.Add(xedChild);
                        }
                    }
                }
            }

            return xed;
        }

    }
}
