﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EY.com.eBook.BizTax
{
    public class XbrlElementFilter
    {
        public string Type { get; set; }

        public string Value1 { get; set; }

        public string Value2 { get; set; }
    }
}
