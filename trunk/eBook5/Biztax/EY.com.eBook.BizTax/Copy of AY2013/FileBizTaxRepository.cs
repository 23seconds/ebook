﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EY.com.eBook.Core;
using EY.com.eBook.BizTax.Contracts;

namespace EY.com.eBook.BizTax.AY2013
{
    public class FileBizTaxRepository
    {

        internal AY2013.BizTaxRenderer Renderer = new BizTaxRenderer();

        internal Contracts.FileXbrlDataContract _fileXbrl { get; set; }

        public Contracts.FileXbrlDataContract GetData(Contracts.Criteria.CriteriaBiztaxDataContract cbdc, bool recalc)
        {

            Core.EF.Write.FileXbrl fx = Core.EF.eBookWriteManager.Context.FileXbrlSet.FirstOrDefault(x => x.FileId == cbdc.FileId);
              Core.EF.Write.File fle = Core.EF.eBookWriteManager.Context.Files.First(f => f.Id == cbdc.FileId);
                Core.EF.Write.Client cle = Core.EF.eBookWriteManager.Context.ClientSet.First(c => c.Id == fle.ClientId);
            if (fx != null)
            {
                _fileXbrl = new Contracts.FileXbrlDataContract
                {
                    Calculation = fx.Calculation
                    ,
                    Data = CoreHelper.DeserializeFromString<BizTaxDataContract>(fx.XbrlData)
                    ,
                    DeclaringDepartment = fx.DeclaringDepartment
                    ,
                    DeclaringPartnerId = fx.DeclaringPartnerId
                    ,
                    DeclaringPartnerName = fx.DeclaringPartnerName
                    ,
                    FileId = fx.FileId
                    ,
                    History = new List<FileXbrlHistoryDataContract>()
                    ,
                    Locked = fx.Locked
                    ,
                    Status = fx.StatusId
                    ,
                    Type = fx.XbrlType
                    ,
                    Validated = fx.Validated
                };
                if (recalc)
                {
                    Renderer.Load(_fileXbrl.Data);
                }
            }
            else
            {
              
                string sref = cbdc.Type == "rcorp" ? Renderer.TaxonomySchemaRef_rcorp : cbdc.Type == "nrcorp" ? Renderer.TaxonomySchemaRef_nrcorp : Renderer.TaxonomySchemaRef_rle;
                Renderer.CreateNew(cle.EnterpriseNumber, fle.StartDate, fle.EndDate, sref);
                _fileXbrl = new FileXbrlDataContract
                {
                    Calculation = null
                    ,
                    Data = Renderer.Xbrl
                    ,
                    DeclaringDepartment = null
                    ,
                    DeclaringPartnerId = null
                    ,
                    DeclaringPartnerName = null
                    ,
                    FileId = fle.Id
                    ,
                    History = new List<FileXbrlHistoryDataContract>()
                    ,
                    Locked = false
                    ,
                    Status = 0
                    ,
                    Type = cbdc.Type
                    ,
                    Validated = false
                };
                if (!recalc) Save();
                //recalc = false;
            }
            Renderer.SetGeneralData(cle.EnterpriseNumber.ToBelgianEnterprise(), fle.StartDate, fle.EndDate, cle, fle.Culture);
            Renderer.CreateIndexes();
            _fileXbrl.Data = Renderer.Xbrl;
            if (recalc)
            {
                CalculateAndValidate();
                Save();
            }


            return _fileXbrl;
        }

        public void CalculateAndValidate()
        {
            if (_fileXbrl == null || Renderer == null || Renderer.Xbrl == null) return;
            if (_fileXbrl.Locked) return;
            Renderer.CalculateAndValidate();
            _fileXbrl.Data = Renderer.Xbrl;
        }

        public void Save(Contracts.FileXbrlDataContract fxdc,bool recalc)
        {
            _fileXbrl = fxdc;
            if (recalc)
            {
                Renderer.Load(fxdc.Data);
                Renderer.CreateIndexes();
                CalculateAndValidate();
            }
            Save();
        }

        public void Save()
        {
            Save(true);
        }

        public void Save(bool includeData)
        {
            if (_fileXbrl != null)
            {
                Core.EF.Write.FileXbrl fx = Core.EF.eBookWriteManager.Context.FileXbrlSet.FirstOrDefault(x => x.FileId == _fileXbrl.FileId);
                if (fx == null)
                {
                    fx = new Core.EF.Write.FileXbrl { FileId = _fileXbrl.FileId,XbrlType = _fileXbrl.Type };
                }
                fx.Calculation = _fileXbrl.Calculation;
                fx.DeclaringDepartment = _fileXbrl.DeclaringDepartment;
                fx.DeclaringPartnerId = _fileXbrl.DeclaringPartnerId;
                fx.DeclaringPartnerName = _fileXbrl.DeclaringPartnerName;
                fx.Locked = _fileXbrl.Locked;
                fx.StatusId = _fileXbrl.Status;
                fx.Validated = _fileXbrl.Validated;
                if(includeData) fx.XbrlData = CoreHelper.SerializeToString(_fileXbrl.Data);
                

                if (fx.EntityState == System.Data.EntityState.Detached)
                    Core.EF.eBookWriteManager.Context.AddToFileXbrlSet(fx);
                
                Core.EF.eBookWriteManager.Context.SaveChanges();
            }

        }


        internal void UpdateMeta(FileXbrlInfoDataContract fxidc)
        {
            if (_fileXbrl == null)
            {
                _fileXbrl = new FileXbrlDataContract
                {
                    FileId = fxidc.FileId,
                    Type= fxidc.Type,
                    Locked = false
                };
            }
            
            _fileXbrl.Calculation = fxidc.Calculation;
            _fileXbrl.DeclaringDepartment = fxidc.DeclaringDepartment;
            _fileXbrl.DeclaringPartnerId = fxidc.DeclaringPartnerId;
            _fileXbrl.DeclaringPartnerName = fxidc.DeclaringPartnerName;
            _fileXbrl.Locked = fxidc.Locked;
            _fileXbrl.Status = fxidc.Status;
            _fileXbrl.Validated = fxidc.Validated;
            _fileXbrl.Type = fxidc.Type;
            Save(false);
        }

        internal void Publish(Contracts.Criteria.CriteriaBiztaxDataContract cbdc)
        {
            GetData(cbdc, false);
            if (!_fileXbrl.Locked) throw new Exception("Please lock the contents prior to publish");
            // Generate XBRL
            // Change state
        }

        internal CalcResultDataContract GetCalcResults()
        {
            return new CalcResultDataContract {
                Elements = _fileXbrl.Data.Elements.Where(e=>e.Calculated).ToList(),
                Errors = _fileXbrl.Data.Errors
            };
        }
    }
}
