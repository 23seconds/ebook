﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.ServiceModel.Activation;
using EY.com.eBook.BizTax.Interfaces;
using EY.com.eBook.Core.EF;

namespace EY.com.eBook.BizTax.AY2013.Service
{
    [EntityFrameworkServiceBehavior]
    [ServiceBehavior(IncludeExceptionDetailInFaults = true, ConcurrencyMode = ConcurrencyMode.Multiple, InstanceContextMode = InstanceContextMode.PerCall)]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class BizTaxService : IBizTaxService
    {

        #region IBizTaxService Members
        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
         RequestFormat = WebMessageFormat.Json,
         ResponseFormat = WebMessageFormat.Json,
         UriTemplate = "/GetBizTax")]
        public Contracts.FileXbrlDataContract GetBizTax(Contracts.Criteria.CriteriaBiztaxDataContract cbdc)
        {
            AY2013.FileBizTaxRepository fbr = new AY2013.FileBizTaxRepository();
            return fbr.GetData(cbdc,true);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
         RequestFormat = WebMessageFormat.Json,
         ResponseFormat = WebMessageFormat.Json,
         UriTemplate = "/SaveBizTax")]
        public Contracts.CalcResultDataContract SaveBizTax(Contracts.FileXbrlDataContract fxdc)
        {
            AY2013.FileBizTaxRepository fbr = new AY2013.FileBizTaxRepository();
            fbr.Save(fxdc, true);

            return fbr.GetCalcResults();
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
         RequestFormat = WebMessageFormat.Json,
         ResponseFormat = WebMessageFormat.Json,
         UriTemplate = "/UpdateBizTaxMetaData")]
        public void UpdateBizTaxMetaData(Contracts.FileXbrlInfoDataContract fxidc)
        {
            AY2013.FileBizTaxRepository fbr = new AY2013.FileBizTaxRepository();
            fbr.UpdateMeta(fxidc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
         RequestFormat = WebMessageFormat.Json,
         ResponseFormat = WebMessageFormat.Json,
         UriTemplate = "/GetTaxCalculation")]
        public decimal? GetTaxCalculation(Contracts.Criteria.CriteriaBiztaxDataContract cbdc)
        {
            return null;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
         RequestFormat = WebMessageFormat.Json,
         ResponseFormat = WebMessageFormat.Json,
         UriTemplate = "/Publish")]
        public Contracts.FileXbrlDataContract Publish(Contracts.Criteria.CriteriaBiztaxDataContract cbdc)
        {
            AY2013.FileBizTaxRepository fbr = new AY2013.FileBizTaxRepository();
            fbr.Publish(cbdc);
            return fbr._fileXbrl;
        }

        #endregion
    }
    
}