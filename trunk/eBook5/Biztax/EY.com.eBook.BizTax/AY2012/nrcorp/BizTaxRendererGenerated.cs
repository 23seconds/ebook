﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EY.com.eBook.BizTax.Contracts;

namespace EY.com.eBook.BizTax.AY2012.nrcorp
{
    public class BizTaxRendererGenerated : EY.com.eBook.BizTax.BizTaxRenderer
    {
        #region DefaultContexts
        public override void ApplyDefaultContexts(string identifier, DateTime periodStart, DateTime periodEnd)
        {
            base.ApplyDefaultContexts(identifier, periodStart, periodEnd);

            AddContextsFor(new List<ContextScenarioExplicitDataContract> { new ContextScenarioExplicitDataContract { Dimension = "d-br:BranchDimension", Value = "d-br:BelgianBranchMember" } }, identifier, periodStart, periodEnd);
            AddContextsFor(new List<ContextScenarioExplicitDataContract> { new ContextScenarioExplicitDataContract { Dimension = "d-br:BranchDimension", Value = "d-br:ForeignBranchMember" } }, identifier, periodStart, periodEnd);
            AddContextsFor(new List<ContextScenarioExplicitDataContract> { new ContextScenarioExplicitDataContract { Dimension = "d-origin:OriginDimension", Value = "d-origin:TaxTreatyMember" } }, identifier, periodStart, periodEnd);
            AddContextsFor(new List<ContextScenarioExplicitDataContract> { new ContextScenarioExplicitDataContract { Dimension = "d-origin:OriginDimension", Value = "d-origin:NoTaxTreatyMember" } }, identifier, periodStart, periodEnd);
            AddContextsFor(new List<ContextScenarioExplicitDataContract> { new ContextScenarioExplicitDataContract { Dimension = "d-origin:OriginDimension", Value = "d-origin:BelgiumMember" } }, identifier, periodStart, periodEnd);
            AddContextsFor(new List<ContextScenarioExplicitDataContract> { new ContextScenarioExplicitDataContract { Dimension = "d-incc:IncentiveCategoryDimension", Value = "d-incc:InvestmentDeductionIncompatibleTaxCreditResearchDevelopmentMember" }, new ContextScenarioExplicitDataContract { Dimension = "d-invc:InvestmentCategoryDimension", Value = "d-invc:PatentsMember" } }, identifier, periodStart, periodEnd);
            AddContextsFor(new List<ContextScenarioExplicitDataContract> { new ContextScenarioExplicitDataContract { Dimension = "d-incc:IncentiveCategoryDimension", Value = "d-incc:InvestmentDeductionIncompatibleTaxCreditResearchDevelopmentMember" }, new ContextScenarioExplicitDataContract { Dimension = "d-invc:InvestmentCategoryDimension", Value = "d-invc:ResearchDevelopmentMember" } }, identifier, periodStart, periodEnd);
            AddContextsFor(new List<ContextScenarioExplicitDataContract> { new ContextScenarioExplicitDataContract { Dimension = "d-incc:IncentiveCategoryDimension", Value = "d-incc:InvestmentDeductionIncompatibleTaxCreditResearchDevelopmentMember" }, new ContextScenarioExplicitDataContract { Dimension = "d-invc:InvestmentCategoryDimension", Value = "d-invc:EnergySavingsMember" } }, identifier, periodStart, periodEnd);
            AddContextsFor(new List<ContextScenarioExplicitDataContract> { new ContextScenarioExplicitDataContract { Dimension = "d-incc:IncentiveCategoryDimension", Value = "d-incc:InvestmentDeductionIncompatibleTaxCreditResearchDevelopmentMember" }, new ContextScenarioExplicitDataContract { Dimension = "d-invc:InvestmentCategoryDimension", Value = "d-invc:SmokeExtractionAirTreatmentSystemsHorecaOutletsMember" } }, identifier, periodStart, periodEnd);
            AddContextsFor(new List<ContextScenarioExplicitDataContract> { new ContextScenarioExplicitDataContract { Dimension = "d-incc:IncentiveCategoryDimension", Value = "d-incc:InvestmentDeductionIncompatibleTaxCreditResearchDevelopmentMember" }, new ContextScenarioExplicitDataContract { Dimension = "d-invc:InvestmentCategoryDimension", Value = "d-invc:ElectricVehicleChargingStationsMember" } }, identifier, periodStart, periodEnd);
            AddContextsFor(new List<ContextScenarioExplicitDataContract> { new ContextScenarioExplicitDataContract { Dimension = "d-incc:IncentiveCategoryDimension", Value = "d-incc:InvestmentDeductionIncompatibleTaxCreditResearchDevelopmentMember" }, new ContextScenarioExplicitDataContract { Dimension = "d-invc:InvestmentCategoryDimension", Value = "d-invc:PromoteReutilisationRefillableBeveragePackagesReusableIndustrialProductsMember" } }, identifier, periodStart, periodEnd);
            AddContextsFor(new List<ContextScenarioExplicitDataContract> { new ContextScenarioExplicitDataContract { Dimension = "d-incc:IncentiveCategoryDimension", Value = "d-incc:InvestmentDeductionIncompatibleTaxCreditResearchDevelopmentMember" }, new ContextScenarioExplicitDataContract { Dimension = "d-invc:InvestmentCategoryDimension", Value = "d-invc:SecurityDevicesMember" } }, identifier, periodStart, periodEnd);
            AddContextsFor(new List<ContextScenarioExplicitDataContract> { new ContextScenarioExplicitDataContract { Dimension = "d-incc:IncentiveCategoryDimension", Value = "d-incc:InvestmentDeductionIncompatibleTaxCreditResearchDevelopmentMember" }, new ContextScenarioExplicitDataContract { Dimension = "d-invc:InvestmentCategoryDimension", Value = "d-invc:SeaVesselsNonBelgianOriginMember" } }, identifier, periodStart, periodEnd);
            AddContextsFor(new List<ContextScenarioExplicitDataContract> { new ContextScenarioExplicitDataContract { Dimension = "d-incc:IncentiveCategoryDimension", Value = "d-incc:InvestmentDeductionCompatibleTaxCreditResearchDevelopmentMember" }, new ContextScenarioExplicitDataContract { Dimension = "d-invc:InvestmentCategoryDimension", Value = "d-invc:EnergySavingsMember" } }, identifier, periodStart, periodEnd);
            AddContextsFor(new List<ContextScenarioExplicitDataContract> { new ContextScenarioExplicitDataContract { Dimension = "d-incc:IncentiveCategoryDimension", Value = "d-incc:InvestmentDeductionCompatibleTaxCreditResearchDevelopmentMember" }, new ContextScenarioExplicitDataContract { Dimension = "d-invc:InvestmentCategoryDimension", Value = "d-invc:SmokeExtractionAirTreatmentSystemsHorecaOutletsMember" } }, identifier, periodStart, periodEnd);
            AddContextsFor(new List<ContextScenarioExplicitDataContract> { new ContextScenarioExplicitDataContract { Dimension = "d-incc:IncentiveCategoryDimension", Value = "d-incc:InvestmentDeductionCompatibleTaxCreditResearchDevelopmentMember" }, new ContextScenarioExplicitDataContract { Dimension = "d-invc:InvestmentCategoryDimension", Value = "d-invc:ElectricVehicleChargingStationsMember" } }, identifier, periodStart, periodEnd);
            AddContextsFor(new List<ContextScenarioExplicitDataContract> { new ContextScenarioExplicitDataContract { Dimension = "d-incc:IncentiveCategoryDimension", Value = "d-incc:InvestmentDeductionCompatibleTaxCreditResearchDevelopmentMember" }, new ContextScenarioExplicitDataContract { Dimension = "d-invc:InvestmentCategoryDimension", Value = "d-invc:PromoteReutilisationRefillableBeveragePackagesReusableIndustrialProductsMember" } }, identifier, periodStart, periodEnd);
            AddContextsFor(new List<ContextScenarioExplicitDataContract> { new ContextScenarioExplicitDataContract { Dimension = "d-incc:IncentiveCategoryDimension", Value = "d-incc:InvestmentDeductionCompatibleTaxCreditResearchDevelopmentMember" }, new ContextScenarioExplicitDataContract { Dimension = "d-invc:InvestmentCategoryDimension", Value = "d-invc:SecurityDevicesMember" } }, identifier, periodStart, periodEnd);
            AddContextsFor(new List<ContextScenarioExplicitDataContract> { new ContextScenarioExplicitDataContract { Dimension = "d-incc:IncentiveCategoryDimension", Value = "d-incc:InvestmentDeductionCompatibleTaxCreditResearchDevelopmentMember" }, new ContextScenarioExplicitDataContract { Dimension = "d-invc:InvestmentCategoryDimension", Value = "d-invc:SeaVesselsNonBelgianOriginMember" } }, identifier, periodStart, periodEnd);
            AddContextsFor(new List<ContextScenarioExplicitDataContract> { new ContextScenarioExplicitDataContract { Dimension = "d-incc:IncentiveCategoryDimension", Value = "d-incc:TaxCreditResearchDevelopmentMember" }, new ContextScenarioExplicitDataContract { Dimension = "d-invc:InvestmentCategoryDimension", Value = "d-invc:PatentsMember" } }, identifier, periodStart, periodEnd);
            AddContextsFor(new List<ContextScenarioExplicitDataContract> { new ContextScenarioExplicitDataContract { Dimension = "d-incc:IncentiveCategoryDimension", Value = "d-incc:TaxCreditResearchDevelopmentMember" }, new ContextScenarioExplicitDataContract { Dimension = "d-invc:InvestmentCategoryDimension", Value = "d-invc:ResearchDevelopmentMember" } }, identifier, periodStart, periodEnd);
            AddContextsFor(new List<ContextScenarioExplicitDataContract> { new ContextScenarioExplicitDataContract { Dimension = "d-empf:EmploymentFunctionDimension", Value = "d-empf:DepartmentHeadExportMember" }, new ContextScenarioExplicitDataContract { Dimension = "d-empper:EmploymentPeriodDimension", Value = "d-empper:RecruitmentFullTimeMember" } }, identifier, periodStart, periodEnd);
            AddContextsFor(new List<ContextScenarioExplicitDataContract> { new ContextScenarioExplicitDataContract { Dimension = "d-empf:EmploymentFunctionDimension", Value = "d-empf:DepartmentHeadExportMember" }, new ContextScenarioExplicitDataContract { Dimension = "d-empper:EmploymentPeriodDimension", Value = "d-empper:EmployedFullTimeTransferTaxPeriodMember" } }, identifier, periodStart, periodEnd);
            AddContextsFor(new List<ContextScenarioExplicitDataContract> { new ContextScenarioExplicitDataContract { Dimension = "d-empf:EmploymentFunctionDimension", Value = "d-empf:DepartmentHeadExportMember" }, new ContextScenarioExplicitDataContract { Dimension = "d-empper:EmploymentPeriodDimension", Value = "d-empper:EmployedFullTimeMember" } }, identifier, periodStart, periodEnd);
            AddContextsFor(new List<ContextScenarioExplicitDataContract> { new ContextScenarioExplicitDataContract { Dimension = "d-empf:EmploymentFunctionDimension", Value = "d-empf:DepartmentHeadExportMember" }, new ContextScenarioExplicitDataContract { Dimension = "d-empper:EmploymentPeriodDimension", Value = "d-empper:EmployedPartTimeMember" } }, identifier, periodStart, periodEnd);
            AddContextsFor(new List<ContextScenarioExplicitDataContract> { new ContextScenarioExplicitDataContract { Dimension = "d-empf:EmploymentFunctionDimension", Value = "d-empf:DepartmentHeadTQMMember" }, new ContextScenarioExplicitDataContract { Dimension = "d-empper:EmploymentPeriodDimension", Value = "d-empper:RecruitmentFullTimeMember" } }, identifier, periodStart, periodEnd);
            AddContextsFor(new List<ContextScenarioExplicitDataContract> { new ContextScenarioExplicitDataContract { Dimension = "d-empf:EmploymentFunctionDimension", Value = "d-empf:DepartmentHeadTQMMember" }, new ContextScenarioExplicitDataContract { Dimension = "d-empper:EmploymentPeriodDimension", Value = "d-empper:EmployedFullTimeTransferTaxPeriodMember" } }, identifier, periodStart, periodEnd);
            AddContextsFor(new List<ContextScenarioExplicitDataContract> { new ContextScenarioExplicitDataContract { Dimension = "d-empf:EmploymentFunctionDimension", Value = "d-empf:DepartmentHeadTQMMember" }, new ContextScenarioExplicitDataContract { Dimension = "d-empper:EmploymentPeriodDimension", Value = "d-empper:EmployedFullTimeMember" } }, identifier, periodStart, periodEnd);
            AddContextsFor(new List<ContextScenarioExplicitDataContract> { new ContextScenarioExplicitDataContract { Dimension = "d-empf:EmploymentFunctionDimension", Value = "d-empf:DepartmentHeadTQMMember" }, new ContextScenarioExplicitDataContract { Dimension = "d-empper:EmploymentPeriodDimension", Value = "d-empper:EmployedPartTimeMember" } }, identifier, periodStart, periodEnd);
            AddContextsFor(new List<ContextScenarioExplicitDataContract> { new ContextScenarioExplicitDataContract { Dimension = "d-empf:EmploymentFunctionDimension", Value = "d-empf:DepartmentHeadExportMember" }, new ContextScenarioExplicitDataContract { Dimension = "d-empper:EmploymentPeriodDimension", Value = "d-empper:RecruitmentFullTimeReplacementMember" } }, identifier, periodStart, periodEnd);
            AddContextsFor(new List<ContextScenarioExplicitDataContract> { new ContextScenarioExplicitDataContract { Dimension = "d-empf:EmploymentFunctionDimension", Value = "d-empf:DepartmentHeadTQMMember" }, new ContextScenarioExplicitDataContract { Dimension = "d-empper:EmploymentPeriodDimension", Value = "d-empper:RecruitmentFullTimeReplacementMember" } }, identifier, periodStart, periodEnd);
            AddContextsFor(new List<ContextScenarioExplicitDataContract> { new ContextScenarioExplicitDataContract { Dimension = "d-per:PeriodDimension", Value = "d-per:TaxPeriod-0Member" }, new ContextScenarioExplicitDataContract { Dimension = "d-asst:AssetTypeDimension", Value = "d-asst:TangibleIntangibleFixedAssetsMember" } }, identifier, periodStart, periodEnd);
            AddContextsFor(new List<ContextScenarioExplicitDataContract> { new ContextScenarioExplicitDataContract { Dimension = "d-per:PeriodDimension", Value = "d-per:TaxPeriod-1Member" }, new ContextScenarioExplicitDataContract { Dimension = "d-asst:AssetTypeDimension", Value = "d-asst:TangibleIntangibleFixedAssetsMember" } }, identifier, periodStart, periodEnd);
            AddContextsFor(new List<ContextScenarioExplicitDataContract> { new ContextScenarioExplicitDataContract { Dimension = "d-per:PeriodDimension", Value = "d-per:TaxPeriod-2Member" }, new ContextScenarioExplicitDataContract { Dimension = "d-asst:AssetTypeDimension", Value = "d-asst:TangibleIntangibleFixedAssetsMember" } }, identifier, periodStart, periodEnd);
            AddContextsFor(new List<ContextScenarioExplicitDataContract> { new ContextScenarioExplicitDataContract { Dimension = "d-incc:IncentiveCategoryDimension", Value = "d-incc:InvestmentDeductionCompatibleTaxCreditResearchDevelopmentMember" }, new ContextScenarioExplicitDataContract { Dimension = "d-invc:InvestmentCategoryDimension", Value = "d-invc:PatentsMember" } }, identifier, periodStart, periodEnd);
            AddContextsFor(new List<ContextScenarioExplicitDataContract> { new ContextScenarioExplicitDataContract { Dimension = "d-incc:IncentiveCategoryDimension", Value = "d-incc:InvestmentDeductionCompatibleTaxCreditResearchDevelopmentMember" }, new ContextScenarioExplicitDataContract { Dimension = "d-invc:InvestmentCategoryDimension", Value = "d-invc:ResearchDevelopmentMember" } }, identifier, periodStart, periodEnd);
            AddContextsFor(new List<ContextScenarioExplicitDataContract> { new ContextScenarioExplicitDataContract { Dimension = "d-incc:IncentiveCategoryDimension", Value = "d-incc:TaxCreditResearchDevelopmentMember" }, new ContextScenarioExplicitDataContract { Dimension = "d-invc:InvestmentCategoryDimension", Value = "d-invc:EnergySavingsMember" } }, identifier, periodStart, periodEnd);
            AddContextsFor(new List<ContextScenarioExplicitDataContract> { new ContextScenarioExplicitDataContract { Dimension = "d-incc:IncentiveCategoryDimension", Value = "d-incc:TaxCreditResearchDevelopmentMember" }, new ContextScenarioExplicitDataContract { Dimension = "d-invc:InvestmentCategoryDimension", Value = "d-invc:SmokeExtractionAirTreatmentSystemsHorecaOutletsMember" } }, identifier, periodStart, periodEnd);
            AddContextsFor(new List<ContextScenarioExplicitDataContract> { new ContextScenarioExplicitDataContract { Dimension = "d-incc:IncentiveCategoryDimension", Value = "d-incc:TaxCreditResearchDevelopmentMember" }, new ContextScenarioExplicitDataContract { Dimension = "d-invc:InvestmentCategoryDimension", Value = "d-invc:ElectricVehicleChargingStationsMember" } }, identifier, periodStart, periodEnd);
            AddContextsFor(new List<ContextScenarioExplicitDataContract> { new ContextScenarioExplicitDataContract { Dimension = "d-incc:IncentiveCategoryDimension", Value = "d-incc:TaxCreditResearchDevelopmentMember" }, new ContextScenarioExplicitDataContract { Dimension = "d-invc:InvestmentCategoryDimension", Value = "d-invc:PromoteReutilisationRefillableBeveragePackagesReusableIndustrialProductsMember" } }, identifier, periodStart, periodEnd);
            AddContextsFor(new List<ContextScenarioExplicitDataContract> { new ContextScenarioExplicitDataContract { Dimension = "d-incc:IncentiveCategoryDimension", Value = "d-incc:TaxCreditResearchDevelopmentMember" }, new ContextScenarioExplicitDataContract { Dimension = "d-invc:InvestmentCategoryDimension", Value = "d-invc:SecurityDevicesMember" } }, identifier, periodStart, periodEnd);
            AddContextsFor(new List<ContextScenarioExplicitDataContract> { new ContextScenarioExplicitDataContract { Dimension = "d-incc:IncentiveCategoryDimension", Value = "d-incc:TaxCreditResearchDevelopmentMember" }, new ContextScenarioExplicitDataContract { Dimension = "d-invc:InvestmentCategoryDimension", Value = "d-invc:SeaVesselsNonBelgianOriginMember" } }, identifier, periodStart, periodEnd);
           // AddContextsFor(new List<ContextScenarioExplicitDataContract> { }, identifier, periodStart, periodEnd);

        }
        #endregion

        #region Global Parameters

        internal string TaxonomySchemaRef_rcorp = "be-tax-inc-rcorp-2012-04-30.xsd";
        internal string TaxonomySchemaRef_nrcorp = "be-tax-inc-nrcorp-2012-04-30.xsd";
        internal string TaxonomySchemaRef_rle = "be-tax-inc-rle-2012-04-30.xsd";
        internal DateTime AssessmentYearEndFirst = new DateTime(2011, 12, 31);
        internal DateTime AssessmentYearEndLast = new DateTime(2012, 12, 30);
        internal string CurrentAssessmentYear = "2012";
        internal string Decimals = "INF";
        internal decimal MaximumLimitReducedRateBasicTaxableAmount = (decimal)322500;
        internal decimal RateShippingResultNotTonnageBased = (decimal)0.05;
        internal DateTime FirstDateRealisationRiverVessel = new DateTime(2007, 1, 1);
        internal DateTime FirstDateRealisationSpecifSecurity = new DateTime(1990, 1, 1);
        internal decimal RateAllowanceCorporateEquityCurrentTaxPeriod = (decimal)0.03425;
        internal decimal IncreasedRateSMEsAllowanceCorporateEquityCurrentTaxPeriod = (decimal)0.03925;
        internal decimal RateCalculationBasisDeductionPatentsIncome = (decimal)0.80;
        internal decimal LimitExemptInvestmentReserve = (decimal)18750;
        internal decimal InvestmentDeduction_Patents = (decimal)0.1350;
        internal decimal TaxCredit_Patents = (decimal)0.1350;
        internal decimal MaximumRateCorporateTax = (decimal)0.3399;
        internal decimal InvestmentDeduction_ResearchDevelopment = (decimal)0.1350;
        internal decimal TaxCredit_ResearchDevelopment = (decimal)0.1350;
        internal decimal InvestmentDeduction_EnergySavings = (decimal)0.1350;
        internal decimal InvestmentDeduction_SmokeExtractionAirTreatmentSystemsHorecaOutlets = (decimal)0.1350;
        internal decimal InvestmentDeduction_ElectricVehicleChargingStations = (decimal)0.1350;
        internal decimal InvestmentDeduction_PromoteReutilisationRefillableBeveragePackagesReusableIndustrialProducts = (decimal)0.0300;
        internal decimal InvestmentDeduction_SecurityDevices = (decimal)0.2050;
        internal decimal InvestmentDeduction_SeaVesselsNonBelgianOrigin = (decimal)0.3000;
        internal decimal InvestmentDeduction_ResearchDevelopment_Spread = (decimal)0.2050;
        internal decimal TaxCredit_ResearchDevelopment_Spread = (decimal)0.2050;
        internal decimal LowerLimitOtherPreviousCarryOverInvestmentDeduction = (decimal)876620;
        internal decimal UpperLimitOtherPreviousCarryOverInvestmentDeduction = (decimal)3506470;
        internal decimal RateOtherPreviousCarryOverInvestmentDeduction = (decimal)0.25;
        internal decimal LowerLimitOtherPreviousCarryOverInvestmentDeductionCompatibleTaxCredit = (decimal)438310;
        internal decimal UpperLimitOtherPreviousCarryOverInvestmentDeductionCompatibleTaxCredit = (decimal)1753240;
        internal decimal RateOtherPreviousCarryOverInvestmentDeductionCompatibleTaxCreditReverse = (decimal)0.75;
        internal decimal ExemptionAllPersonnel = (decimal)5260;
        internal decimal MaximumDayPayLowWage = (decimal)90.32;
        internal decimal MaximumHourPayLowWage = (decimal)11.88;
        internal decimal ExemptionAdditionalPersonnelSpecificFunction = (decimal)14140;


        #endregion

        public override BizTaxDataContract CalculateAndValidate()
        {
            this.Xbrl.Errors = new List<BizTaxErrorDataContract>();
            this.HasErrors = false;
            Calc_f_1000_assertion_set();
            Calc_f_1001_assertion_set();
            Calc_f_1002_assertion_set();
            Calc_f_1003_assertion_set();
            Calc_f_1006_assertion_set();
            Calc_f_1007_assertion_set();
            Calc_f_1008_assertion_set();
            Calc_f_1009_assertion_set();
            Calc_f_1010_assertion_set();
            Calc_f_1011_assertion_set();
            Calc_f_corp_1101_assertion_set();
            Calc_f_corp_1102_assertion_set();
            Calc_f_corp_1105_assertion_set();
            Calc_f_corp_1106_assertion_set();
            Calc_f_corp_1107_assertion_set();
            Calc_f_corp_1202_assertion_set();
            Calc_f_nrcorp_2201_assertion_set();
            Calc_f_corp_1203_assertion_set();
            Calc_f_corp_1204_assertion_set();
            Calc_f_corp_1206_assertion_set();
            Calc_f_corp_1207_assertion_set();
            Calc_f_corp_1235_assertion_set();
            Calc_f_corp_1236_assertion_set();
            Calc_f_corp_1237_assertion_set();
            Calc_f_corp_1400_assertion_set();
            Calc_f_corp_1401_assertion_set();
            Calc_f_corp_1402_assertion_set();
            Calc_f_corp_1403_assertion_set();
            Calc_f_corp_1404_assertion_set();
            Calc_f_corp_1405_assertion_set();
            Calc_f_corp_1406_assertion_set();
            Calc_f_corp_1407_assertion_set();
            Calc_f_corp_1408_assertion_set();
            Calc_f_corp_1409_assertion_set();
            Calc_f_corp_1410_assertion_set();
            Calc_f_corp_1411_assertion_set();
            Calc_f_corp_1412_assertion_set();
            Calc_f_corp_1414_assertion_set();
            Calc_f_corp_1415a_assertion_set();
            Calc_f_corp_1416_assertion_set();
            Calc_f_corp_1417_assertion_set();
            Calc_f_corp_1422_assertion_set();
            Calc_f_corp_1435_assertion_set();
            Calc_f_corp_1436_assertion_set();
            Calc_f_corp_1437_assertion_set();
            Calc_f_corp_1438_assertion_set();
            Calc_f_corp_1439_assertion_set();
            Calc_f_corp_1440_assertion_set();
            Calc_f_corp_1441_assertion_set();
            Calc_f_corp_1442_assertion_set();
            Calc_f_corp_1443_assertion_set();
            Calc_f_nrcorp_2001_assertion_set();
            Calc_f_nrcorp_2011_assertion_set();
            Calc_f_nrcorp_2101_assertion_set();
            Calc_f_nrcorp_2103_assertion_set();
            Calc_f_nrcorp_2120_assertion_set();
            
            Calc_f_nrcorp_1211_assertion_set();
            Calc_f_nrcorp_2212_assertion_set();
            Calc_f_nrcorp_2216_assertion_set();
            Calc_f_nrcorp_2218_assertion_set();
            Calc_f_nrcorp_2224_assertion_set();
            Calc_f_nrcorp_2225_assertion_set();
            Calc_f_nrcorp_2226_assertion_set();
            Calc_f_nrcorp_2229_assertion_set();
            Calc_f_nrcorp_2230_assertion_set();
            Calc_f_nrcorp_2231_assertion_set();
            Calc_f_nrcorp_2232_assertion_set();
            Calc_f_nrcorp_2234_assertion_set();
            Calc_f_nrcorp_2238_assertion_set();
            Calc_f_nrcorp_2418_assertion_set();
            Calc_f_nrcorp_2419_assertion_set();
            Calc_f_nrcorp_2420_assertion_set();
            Calc_f_nrcorp_2444_assertion_set();
            Calc_f_nrcorp_2445_assertion_set();
            Calc_f_nrcorp_2446_assertion_set();
            Calc_f_nrcorp_5306_assertion_set();
            Calc_f_nrcorp_5308_assertion_set();
            Calc_f_le_3001_assertion_set();
            Calc_f_le_3002_assertion_set();
            Calc_f_le_3003_assertion_set();
            Calc_f_le_3010_assertion_set();
            Calc_f_le_3011_assertion_set();
            Calc_f_le_3012_assertion_set();
            Calc_f_le_3013_assertion_set();
            Calc_f_wddc_5001_assertion_set();
            Calc_f_wddc_5002_assertion_set();
            Calc_f_wddc_5003_assertion_set();
            Calc_f_wddc_5004_assertion_set();
            Calc_f_wddc_5005_assertion_set();
            Calc_f_wddc_5006_assertion_set();
            Calc_f_wddc_5007_assertion_set();
            Calc_f_wddc_5008_assertion_set();
            Calc_f_prre_5050_assertion_set();
            Calc_f_prre_5051_assertion_set();
            Calc_f_prre_5052_assertion_set();
            Calc_f_prre_5053_assertion_set();
            Calc_f_prre_5054_assertion_set();
            Calc_f_prre_5055_assertion_set();
            Calc_f_cg_5101_assertion_set();
            Calc_f_cg_5102_assertion_set();
            Calc_f_cg_5103a_assertion_set();
            Calc_f_cg_5103b_assertion_set();
            Calc_f_cg_5104a_assertion_set();
            Calc_f_cg_5104b_assertion_set();
            Calc_f_cg_5105_assertion_set();
            Calc_f_cg_5121_assertion_set();
            Calc_f_cg_5131_assertion_set();
            Calc_f_cg_5141_assertion_set();
            Calc_f_cg_5142_assertion_set();
            Calc_f_cg_5143_assertion_set();
            Calc_f_cg_5144_assertion_set();
            Calc_f_cg_5145_assertion_set();
            Calc_f_cg_5151_assertion_set();
            Calc_f_cg_5152_assertion_set();
            Calc_f_cg_5153_assertion_set();
            Calc_f_cg_5154_assertion_set();
            Calc_f_cg_5155_assertion_set();
            Calc_f_cg_5161_assertion_set();
            Calc_f_cg_5162_assertion_set();
            Calc_f_ricg_5121_assertion_set();
            Calc_f_ricg_5122_assertion_set();
            Calc_f_ricg_5123_assertion_set();
            Calc_f_ricg_5124_assertion_set();
            Calc_f_ricg_5125_assertion_set();
            Calc_f_stcg_5132_assertion_set();
            Calc_f_stcg_5131_assertion_set();
            Calc_f_ace_5301_assertion_set();
            Calc_f_ace_5302_assertion_set();
            Calc_f_ace_5304_assertion_set();
            Calc_f_ace_5305_assertion_set();
            Calc_f_ace_5307_assertion_set();
            Calc_f_pcs_5320_assertion_set();
            Calc_f_dpi_5375_assertion_set();
            Calc_f_dpi_5376_assertion_set();
            Calc_f_dpi_5377_assertion_set();
            Calc_f_itifa_5321_assertion_set();
            Calc_f_itifa_5322_assertion_set();
            Calc_f_itifa_5323_assertion_set();
            Calc_f_itifa_5324_assertion_set();
            Calc_f_itifa_5325_assertion_set();
            Calc_f_itifa_5327_assertion_set();
            Calc_f_itifa_5328_assertion_set();
            Calc_f_itifa_5329_assertion_set();
            Calc_f_idtc_5341_assertion_set();
            Calc_f_idtc_5343_assertion_set();
            Calc_f_idtc_5344_assertion_set();
            Calc_f_idtc_5345_assertion_set();
            Calc_f_idtc_5346_assertion_set();
            Calc_f_idtc_5347_assertion_set();
            Calc_f_idtc_5348_assertion_set();
            Calc_f_idtc_5349_assertion_set();
            Calc_f_idtc_5350_assertion_set();
            Calc_f_idtc_5351_assertion_set();
            Calc_f_idtc_5352_assertion_set();
            Calc_f_idtc_5353_assertion_set();
            Calc_f_idtc_5354_assertion_set();
            Calc_f_idtc_5355_assertion_set();
            Calc_f_idtc_5356_assertion_set();
            Calc_f_idtc_5357_assertion_set();
            Calc_f_idtc_5358_assertion_set();
            Calc_f_idtc_5359_assertion_set();
            Calc_f_idtc_5360_assertion_set();
            Calc_f_idtc_5361_assertion_set();
            Calc_f_idtc_5362_assertion_set();
            Calc_f_idtc_5363_assertion_set();
            Calc_f_idtc_5364_assertion_set();
            Calc_f_idtc_5365_assertion_set();
            Calc_f_idtc_5366_assertion_set();
            Calc_f_idtc_5367_assertion_set();
            Calc_f_idtc_5368_assertion_set();
            Calc_f_idtc_5369_assertion_set();
            Calc_f_idtc_5370_assertion_set();
            Calc_f_idtc_5371_assertion_set();
            Calc_f_idtc_5372_assertion_set();
            Calc_f_idtc_5374_assertion_set();
            Calc_f_idtc_5375_assertion_set();
            Calc_f_idtc_5376_assertion_set();
            Calc_f_eap_5402_assertion_set();
            Calc_f_eap_5403_assertion_set();
            Calc_f_eap_5405_assertion_set();
            Calc_f_eap_5407_assertion_set();
            Calc_f_eap_5409a_assertion_set();
            Calc_f_eap_5409b_assertion_set();
            Calc_f_eap_5412_assertion_set();
            Calc_f_eap_5413_assertion_set();
            Calc_f_eap_5414_assertion_set();
            Calc_f_eapsf_5441_assertion_set();
            Calc_f_eapsf_5442_assertion_set();
            Calc_f_eapsf_5443_assertion_set();
            Calc_f_eapsf_5444_assertion_set();
            Calc_f_eapsf_5445_assertion_set();
            Calc_f_eapsf_5446_assertion_set();
            Calc_f_eapsf_5447_assertion_set();
            Calc_f_eapsf_5448_assertion_set();
            Calc_f_eapsf_5450_assertion_set();
            Calc_f_eapsf_5451_assertion_set();
            Calc_f_eapsf_5452_assertion_set();
            Calc_f_eapsf_5453_assertion_set();
            Calc_f_eapsf_5455_assertion_set();
            Calc_f_eapsf_5456_assertion_set();
            Calc_f_eapsf_5457_assertion_set();
            Calc_f_eapsf_5458_assertion_set();
            Calc_f_eapsf_5460_assertion_set();
            Calc_f_eapsf_5461_assertion_set();
            Calc_f_dfa_5501_assertion_set();
            Calc_f_dfa_5502_assertion_set();

            return base.CalculateAndValidate();
        }
        #region Validations and calculations

        internal void Calc_f_1000_assertion_set()
        {

        }

        internal void Calc_f_1001_assertion_set()
        {

        }

        internal void Calc_f_1002_assertion_set()
        {

        }

        internal void Calc_f_1003_assertion_set()
        {

        }

        internal void Calc_f_1006_assertion_set()
        {

        }

        internal void Calc_f_1007_assertion_set()
        {

        }

        internal void Calc_f_1008_assertion_set()
        {

        }

        internal void Calc_f_1009_assertion_set()
        {
            // HAS NO UNBOUND SEQUENCES
            var ProbableCostStart = GetElementsByScenDefs(new string[] { "d-ec:ExemptionCategoryDimension::d-ec:UnalteredBalanceProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ExpensesCompensationCoveredCurrentTaxPeriodMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:UnalteredBalanceProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ExpensesOverhaulImportantRepairsMaximum10YearsMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:UnalteredBalanceProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ProfessionalExpensesCurrentTaxPeriodMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:RemainingBalanceAfterDecreaseProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ExpensesCompensationCoveredCurrentTaxPeriodMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:RemainingBalanceAfterDecreaseProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ExpensesOverhaulImportantRepairsMaximum10YearsMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:RemainingBalanceAfterDecreaseProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ProfessionalExpensesCurrentTaxPeriodMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:RemainingBalanceAfterIncreaseProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ExpensesCompensationCoveredCurrentTaxPeriodMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:RemainingBalanceAfterIncreaseProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ExpensesOverhaulImportantRepairsMaximum10YearsMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:RemainingBalanceAfterIncreaseProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ProfessionalExpensesCurrentTaxPeriodMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:NewProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ExpensesCompensationCoveredCurrentTaxPeriodMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:NewProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ExpensesOverhaulImportantRepairsMaximum10YearsMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:NewProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ProfessionalExpensesCurrentTaxPeriodMember/d-ty:ProvisionRiskExpenseTypedDimension" }, new string[] { }, new string[] { "tax-inc:ProbableCost" });
            if (ProbableCostStart.Count > 0)
            {
                var PeriodStartDate = GetNodeByName("tax-inc", "PeriodStartDate");
                //CALCULATION HERE
                //FORMULA HERE
                bool test = Number(ProbableCostStart, "0") == (decimal)0;
                if (!test)
                {
                    // MESSAGES
                    AddMessage(new BizTaxErrorDataContract
                    {
                        FileId = _fileId,
                        Id = "f-1009",
                        Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "De aangifte bevat elementen die niet toegelaten zijn 'bij het begin van het boekjaar':  'Waarschijnlijke kost' ({$ProbableCostStart})."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "La déclaration contient des éléments qui ne sont pas autorisés 'au début de l'exercice': 'Charge probable' ({$ProbableCostStart})."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Die Erklärung enthält Bestandteile die nicht zulässig sind 'am Anfang des Rechnungsjahres': 'Wahrscheinliche Kosten' ({$ProbableCostStart})."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "De aangifte bevat elementen die niet toegelaten zijn 'bij het begin van het boekjaar':  'Waarschijnlijke kost' ({$ProbableCostStart})."}  
 }
                        ,
                        Fields = new List<string>()
                    }
                    );
                }
            }

        }

        internal void Calc_f_1010_assertion_set()
        {
            string scenarioId = GetScenarioId("");
            // SCENARIO: ""
            var Equity = GetElementsByRef(scenarioId, new string[] { "I-End" }, new string[] { "tax-inc:Equity" });
            var OwnSharesFiscalValue = GetElementsByRef(scenarioId, new string[] { "I-End" }, new string[] { "tax-inc:OwnSharesFiscalValue" });
            var FinancialFixedAssetsParticipationsOtherShares = GetElementsByRef(scenarioId, new string[] { "I-End" }, new string[] { "tax-inc:FinancialFixedAssetsParticipationsOtherShares" });
            var SharesInvestmentCorporations = GetElementsByRef(scenarioId, new string[] { "I-End" }, new string[] { "tax-inc:SharesInvestmentCorporations" });
            var BranchesCountryTaxTreaty = GetElementsByRef(scenarioId, new string[] { "I-End" }, new string[] { "tax-inc:BranchesCountryTaxTreaty" });
            var ImmovablePropertyCountryTaxTreaty = GetElementsByRef(scenarioId, new string[] { "I-End" }, new string[] { "tax-inc:ImmovablePropertyCountryTaxTreaty" });
            var TangibleFixedAssetsUnreasonableRelatedCosts = GetElementsByRef(scenarioId, new string[] { "I-End" }, new string[] { "tax-inc:TangibleFixedAssetsUnreasonableRelatedCosts" });
            var InvestmentsNoPeriodicalIncome = GetElementsByRef(scenarioId, new string[] { "I-End" }, new string[] { "tax-inc:InvestmentsNoPeriodicalIncome" });
            var ImmovablePropertyUseManager = GetElementsByRef(scenarioId, new string[] { "I-End" }, new string[] { "tax-inc:ImmovablePropertyUseManager" });
            var UnrealisedExpressedCapitalGains = GetElementsByRef(scenarioId, new string[] { "I-End" }, new string[] { "tax-inc:UnrealisedExpressedCapitalGains" });
            var TaxCreditResearchDevelopmentDeductionEquityAllowanceCorporateEquity = GetElementsByRef(scenarioId, new string[] { "I-End" }, new string[] { "tax-inc:TaxCreditResearchDevelopmentDeductionEquityAllowanceCorporateEquity" });
            var InvestmentGrants = GetElementsByRef(scenarioId, new string[] { "I-End" }, new string[] { "tax-inc:InvestmentGrants" });
            var ActualisationStockRecognisedDiamondTraders = GetElementsByRef(scenarioId, new string[] { "I-End" }, new string[] { "tax-inc:ActualisationStockRecognisedDiamondTraders" });
            var BorrowedFundsMainEstablishmentInterestChargedTaxableResultBelgianBranch = GetElementsByRef(scenarioId, new string[] { "I-End" }, new string[] { "tax-inc:BorrowedFundsMainEstablishmentInterestChargedTaxableResultBelgianBranch" });
            var DeductionsEquityAllowanceCorporateEquity = GetElementsByRef(scenarioId, new string[] { "I-End" }, new string[] { "tax-inc:DeductionsEquityAllowanceCorporateEquity" });
            if (Equity.Count > 0 || DeductionsEquityAllowanceCorporateEquity.Count > 0 || OwnSharesFiscalValue.Count > 0 || FinancialFixedAssetsParticipationsOtherShares.Count > 0 || SharesInvestmentCorporations.Count > 0 || BranchesCountryTaxTreaty.Count > 0 || ImmovablePropertyCountryTaxTreaty.Count > 0 || TangibleFixedAssetsUnreasonableRelatedCosts.Count > 0 || InvestmentsNoPeriodicalIncome.Count > 0 || ImmovablePropertyUseManager.Count > 0 || UnrealisedExpressedCapitalGains.Count > 0 || TaxCreditResearchDevelopmentDeductionEquityAllowanceCorporateEquity.Count > 0 || InvestmentGrants.Count > 0 || ActualisationStockRecognisedDiamondTraders.Count > 0 || BorrowedFundsMainEstablishmentInterestChargedTaxableResultBelgianBranch.Count > 0)
            {
                var PeriodEndDate = GetNodeByName("tax-inc", "PeriodEndDate");
                //CALCULATION HERE
                //FORMULA HERE
                bool test = (decimal)0 == Sum(Number(Equity, "0"), Number(OwnSharesFiscalValue, "0"), Number(FinancialFixedAssetsParticipationsOtherShares, "0"), Number(SharesInvestmentCorporations, "0"), Number(BranchesCountryTaxTreaty, "0"), Number(ImmovablePropertyCountryTaxTreaty, "0"), Number(TangibleFixedAssetsUnreasonableRelatedCosts, "0"), Number(InvestmentsNoPeriodicalIncome, "0"), Number(ImmovablePropertyUseManager, "0"), Number(UnrealisedExpressedCapitalGains, "0"), Number(TaxCreditResearchDevelopmentDeductionEquityAllowanceCorporateEquity, "0"), Number(InvestmentGrants, "0"), Number(ActualisationStockRecognisedDiamondTraders, "0"), Number(BorrowedFundsMainEstablishmentInterestChargedTaxableResultBelgianBranch, "0"));
                if (!test)
                {
                    // MESSAGES
                    AddMessage(new BizTaxErrorDataContract
                    {
                        FileId = _fileId,
                        Id = "f-1010",
                        Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "De aangifte bevat elementen die niet toegelaten zijn ' op het einde van het boekjaar':  	'{Eigen vermogen}', 	'{Eigen aandelen}', 	'{Financiële vaste activa die uit deelnemingen en andere aandelen bestaan}', 	'{Aandelen van beleggingsvennootschappen}', 	'{Inrichtingen gelegen in een land met verdrag}', 	'{Onroerende goederen gelegen in een land met verdrag}', 	'{Materiële vaste activa in zover de erop betrekking hebbende kosten onredelijk zijn}', 	'{Bestanddelen die als belegging worden gehouden en geen belastbaar periodiek inkomen voortbrengen}', 	'{Onroerende goederen waarvan bedrijfsleiders het gebruik hebben}', 	'{Uitgedrukte maar niet-verwezenlijkte meerwaarden}', 	'{Belastingkrediet voor onderzoek en ontwikkeling}', 	'{Kapitaalsubsidies}', 	'{Voorraadactualisering erkende diamanthandelaars}', 	'{Ten name van de hoofdzetel ontleende middelen met betrekking tot dewelke de interesten ten laste van het belastbaar resultaat van de Belgische inrichting wordt gelegd}'."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "La déclaration rapporte des éléments qui ne sont pas autorisées 'à la fin de l'exercice':  	'{Capitaux propres}', 	'{Actions ou parts propres}', 	'{Immobilisations financières consistant en participations et autres actions ou parts}', 	'{Actions ou parts émises par des sociétés d'investissement}', 	'{Etablissements situés dans un pays avec convention}', 	'{Immeubles situés dans un pays avec convention}', 	'{Actifs corporels dans la mesure où les frais y afférents sont déraisonnables}', 	'{Eléments détenus à titre de placement non productifs de revenus périodiques imposables}', 	'{Biens immobiliers dont les dirigeants ont l'usage}', 	'{Plus-values exprimées mais non réalisées}', 	'{Crédit d’impôt pour recherche et développement}', 	'{Subsides en capital}', 	'{Actualisation des stocks des diamantaires agréés}', 	'{Moyens empruntés dans le chef du siège principal dont les intérêts sont à charge du résultat imposable de l'établissement belge}'."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Die Erklärung enthält Bestandteile die nicht zulässig sind 'am Ende des Rechnungsjahres':  	'{Eigenkapital}', 	'{Eigene Aktien oder Anteile}', 	'{Finanzanlagen, die aus Beteiligungen und anderen Aktien oder Anteilen bestehen}', 	'{Von Investmentgesellschaften ausgegebene Aktien oder Anteile}', 	'{Niederlassungen, die in Ländern mit Abkommen liegen}', 	'{Immobilien, die in Ländern mit Abkommen liegen}', 	'{Sachanlagen in dem Maße, wie die diesbezüglichen Kosten unverhältnismäßig sind}', 	'{Als Anlage dienende Bestandteile, die kein steuerbares regelmäßiges Einkommen erzeugen}', 	'{Immobilien, die von den Betriebsleitern genutzt werden}', 	'{Aufgezeichnete, aber nicht verwirklichte Mehrwerte}', 	'{Steuergutschrift für Forschung und Entwicklung}', 	'{Kapitalsubventionen}', 	'{Bestandsaktualisierung anerkannter Diamantenhändler}', 	'{Auf den Namen des Gesellschaftssitzes geliehene Mittel, für die die Zinsen zu Lasten des steuerpflichtigen Ergebnisses der belgischen Niederlassung fallen}'."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "De aangifte bevat elementen die niet toegelaten zijn ' op het einde van het boekjaar':  	'{Eigen vermogen}', 	'{Eigen aandelen}', 	'{Financiële vaste activa die uit deelnemingen en andere aandelen bestaan}', 	'{Aandelen van beleggingsvennootschappen}', 	'{Inrichtingen gelegen in een land met verdrag}', 	'{Onroerende goederen gelegen in een land met verdrag}', 	'{Materiële vaste activa in zover de erop betrekking hebbende kosten onredelijk zijn}', 	'{Bestanddelen die als belegging worden gehouden en geen belastbaar periodiek inkomen voortbrengen}', 	'{Onroerende goederen waarvan bedrijfsleiders het gebruik hebben}', 	'{Uitgedrukte maar niet-verwezenlijkte meerwaarden}', 	'{Belastingkrediet voor onderzoek en ontwikkeling}', 	'{Kapitaalsubsidies}', 	'{Voorraadactualisering erkende diamanthandelaars}', 	'{Ten name van de hoofdzetel ontleende middelen met betrekking tot dewelke de interesten ten laste van het belastbaar resultaat van de Belgische inrichting wordt gelegd}'."}  
 }
                        ,
                        Fields = new List<string>()
                    }
                    );
                }
            }

        }

        internal void Calc_f_1011_assertion_set()
        {
            string scenarioId = GetScenarioId("");
            string period = "D";
            // SCENARIO: ""
            // PERIOD: "D"
            var ExclusionReducedRate = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:ExclusionReducedRate" });
            var FirstThreeAccountingYearsSmallCompanyCorporationCode = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:FirstThreeAccountingYearsSmallCompanyCorporationCode" });
            var BelgianCorporationAudiovisualWorksTaxShelterAgreement = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:BelgianCorporationAudiovisualWorksTaxShelterAgreement" });
            var AssociatedCompanyCorporationCodeCurrentTaxPeriod = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:AssociatedCompanyCorporationCodeCurrentTaxPeriod" });
            var AssociatedForeignCompanyCorporationCodeCurrentTaxPeriod = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:AssociatedForeignCompanyCorporationCodeCurrentTaxPeriod" });
            var ComplianceSmallForeignCompanyCorporationCodeLastButOneTaxPeriod = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:ComplianceSmallForeignCompanyCorporationCodeLastButOneTaxPeriod" });
            var ComplianceSmallForeignCompanyCorporationCodePreviousTaxPeriod = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:ComplianceSmallForeignCompanyCorporationCodePreviousTaxPeriod" });
            var ExclusionReducedRateNonResidentCorporations = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:ExclusionReducedRateNonResidentCorporations" });
            var ForeignAssociatedCompanyCorporationCodeCurrentTaxPeriod = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:ForeignAssociatedCompanyCorporationCodeCurrentTaxPeriod" });
            var ForeignCorporationAudiovisualWorksTaxShelterAgreement = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:ForeignCorporationAudiovisualWorksTaxShelterAgreement" });
            var CreditCorporationTradeEquipmentHousingCorporationTaxRate = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:CreditCorporationTradeEquipmentHousingCorporationTaxRate" });
            var FirstThreeAccountingYearsSmallForeignCompanyCorporationCode = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:FirstThreeAccountingYearsSmallForeignCompanyCorporationCode" });
            if (AssociatedForeignCompanyCorporationCodeCurrentTaxPeriod.Count > 0 || ComplianceSmallForeignCompanyCorporationCodeLastButOneTaxPeriod.Count > 0 || ComplianceSmallForeignCompanyCorporationCodePreviousTaxPeriod.Count > 0 || ExclusionReducedRateNonResidentCorporations.Count > 0 || ForeignAssociatedCompanyCorporationCodeCurrentTaxPeriod.Count > 0 || ForeignCorporationAudiovisualWorksTaxShelterAgreement.Count > 0 || CreditCorporationTradeEquipmentHousingCorporationTaxRate.Count > 0 || FirstThreeAccountingYearsSmallForeignCompanyCorporationCode.Count > 0 || ExclusionReducedRate.Count > 0 || FirstThreeAccountingYearsSmallCompanyCorporationCode.Count > 0 || BelgianCorporationAudiovisualWorksTaxShelterAgreement.Count > 0 || AssociatedCompanyCorporationCodeCurrentTaxPeriod.Count > 0)
            {
                //CALCULATION HERE
                //FORMULA HERE
                bool test = ((String(String(ExclusionReducedRate, "false")) == "true") || (String(String(ExclusionReducedRate, "false")) == "false")) && ((String(String(FirstThreeAccountingYearsSmallCompanyCorporationCode, "false")) == "true") || (String(String(FirstThreeAccountingYearsSmallCompanyCorporationCode, "false")) == "false")) && ((String(String(BelgianCorporationAudiovisualWorksTaxShelterAgreement, "false")) == "true") || (String(String(BelgianCorporationAudiovisualWorksTaxShelterAgreement, "false")) == "false")) && ((String(String(AssociatedCompanyCorporationCodeCurrentTaxPeriod, "false")) == "true") || (String(String(AssociatedCompanyCorporationCodeCurrentTaxPeriod, "false")) == "false")) && ((String(String(AssociatedForeignCompanyCorporationCodeCurrentTaxPeriod, "false")) == "true") || (String(String(AssociatedForeignCompanyCorporationCodeCurrentTaxPeriod, "false")) == "false")) && ((String(String(ComplianceSmallForeignCompanyCorporationCodeLastButOneTaxPeriod, "false")) == "true") || (String(String(ComplianceSmallForeignCompanyCorporationCodeLastButOneTaxPeriod, "false")) == "false")) && ((String(String(ComplianceSmallForeignCompanyCorporationCodePreviousTaxPeriod, "false")) == "true") || (String(String(ComplianceSmallForeignCompanyCorporationCodePreviousTaxPeriod, "false")) == "false")) && ((String(String(ExclusionReducedRateNonResidentCorporations, "false")) == "true") || (String(String(ExclusionReducedRateNonResidentCorporations, "false")) == "false")) && ((String(String(ForeignAssociatedCompanyCorporationCodeCurrentTaxPeriod, "false")) == "true") || (String(String(ForeignAssociatedCompanyCorporationCodeCurrentTaxPeriod, "false")) == "false")) && ((String(String(ForeignCorporationAudiovisualWorksTaxShelterAgreement, "false")) == "true") || (String(String(ForeignCorporationAudiovisualWorksTaxShelterAgreement, "false")) == "false")) && ((String(String(CreditCorporationTradeEquipmentHousingCorporationTaxRate, "false")) == "true") || (String(String(CreditCorporationTradeEquipmentHousingCorporationTaxRate, "false")) == "false")) && ((String(String(FirstThreeAccountingYearsSmallForeignCompanyCorporationCode, "false")) == "true") || (String(String(FirstThreeAccountingYearsSmallForeignCompanyCorporationCode, "false")) == "false"));
                if (!test)
                {
                    // MESSAGES
                    AddMessage(new BizTaxErrorDataContract
                    {
                        FileId = _fileId,
                        Id = "f-1011",
                        Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Er mogen geen cijfers worden gebruikt voor de antwoorden op de ja/neen-vragen."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Aucun chiffre ne peut être utilisé aux réponses aux questions oui ou non."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Die Ja/Nein-Fragen dürfen nicht mit einer Zahl beantwortet werden."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Er mogen geen cijfers worden gebruikt voor de antwoorden op de ja/neen-vragen."}  
 }
                        ,
                        Fields = new List<string>()
                    }
                    );
                }
            }

        }

        internal void Calc_f_corp_1101_assertion_set()
        {

        }

        internal void Calc_f_corp_1102_assertion_set()
        {

        }

        internal void Calc_f_corp_1105_assertion_set()
        {
            string scenarioId = GetScenarioId("");
            string period = "D";
            // SCENARIO: ""
            // PERIOD: "D"
            var TaxableReservedProfit = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:TaxableReservedProfit" });
            var TaxableReservesEnd = GetElementsByRef(scenarioId, new string[] { "I-End" }, new string[] { "tax-inc:TaxableReserves" });
            var TaxableReservesAfterAdjustmentsStart = GetElementsByRef(scenarioId, new string[] { "I-Start" }, new string[] { "tax-inc:TaxableReservesAfterAdjustments" });
            var FormulaTarget = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:TaxableReservedProfit" });
            if (TaxableReservedProfit.Count > 0 || TaxableReservesEnd.Count > 0 || TaxableReservesAfterAdjustmentsStart.Count > 0 || FormulaTarget.Count > 0)
            {
                var PeriodStartDate = GetNodeByName("tax-inc", "PeriodStartDate");
                var PeriodEndDate = GetNodeByName("tax-inc", "PeriodEndDate");
                //CALCULATION HERE
                if (FormulaTarget.Count == 0)
                {
                    FormulaTarget = CreateNewCalculationElement(scenarioId, period, "tax-inc:TaxableReservedProfit", new string[] { "" }, "EUR", "INF");
                }
                if (FormulaTarget.Count > 1)
                {
                    throw new Exception("More then one target for FormulaTarget in assertion f-corp-1105");
                }
                FormulaTarget.First().Calculated = true;
                FormulaTarget.First().SetNumber(Sum(Number(TaxableReservesEnd, "0"), -Number(TaxableReservesAfterAdjustmentsStart, "0")));
                //FORMULA HERE
                bool test = Number(TaxableReservedProfit, "0") == Sum(Number(TaxableReservesEnd, "0"), -Number(TaxableReservesAfterAdjustmentsStart, "0"));
                if (!test)
                {
                    // MESSAGES
                    AddMessage(new BizTaxErrorDataContract
                    {
                        FileId = _fileId,
                        Id = "f-corp-1105",
                        Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Het totaal vermeld in de rubriek 	'{Belastbare gereserveerde winst}' ({$TaxableReservedProfit}) moet gelijk zijn aan de aangroei of de afname van de 	'{Belastbare reserves na aanpassing van de begintoestand der reserves}' ({$TaxableReservesAfterAdjustmentsStart}) en 	'{Belastbare reserves}' ({$TaxableReservesEnd})."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Le total repris dans la rubrique 	'{Bénéfices réservés imposables}' ({$TaxableReservedProfit}) doit être égal à l'augmentation ou la diminution des 	'{Réserves imposables après adaptation de la situation de début des réserves}' ({$TaxableReservesAfterAdjustmentsStart}) et des 	'{Réserves imposables}' ({$TaxableReservesEnd})."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Der in der Rubrik 	'{Steuerpflichtige Gewinnrücklagen}' ({$TaxableReservedProfit}) angegebene Gesamtbetrag soll der Zunahme oder der Abnahme der 	'{Steuerpflichtige Rücklagen nach Anpassung des Anfangsstandes der Rücklagen}' ({$TaxableReservesAfterAdjustmentsStart}) und der 	'{Steuerpflichtige Rücklagen}' ({$TaxableReservesEnd}) entsprechen."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Het totaal vermeld in de rubriek 	'{Belastbare gereserveerde winst}' ({$TaxableReservedProfit}) moet gelijk zijn aan de aangroei of de afname van de 	'{Belastbare reserves na aanpassing van de begintoestand der reserves}' ({$TaxableReservesAfterAdjustmentsStart}) en 	'{Belastbare reserves}' ({$TaxableReservesEnd})."}  
 }
                        ,
                        Fields = new List<string>()
                    }
                    );
                }
            }

        }

        internal void Calc_f_corp_1106_assertion_set()
        {
            string scenarioId = GetScenarioId("");
            foreach (string period in new string[] { "I-Start", "I-End" })
            {
                // SCENARIO: ""
                // PERIOD: "I-Start"
                // PERIOD: "I-End"
                var ExemptReservedProfit = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:ExemptReservedProfit" });
                var ExemptWriteDownDebtClaim = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:ExemptWriteDownDebtClaim" });
                var ExemptProvisionRisksExpenses = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:ExemptProvisionRisksExpenses" });
                var UnrealisedExpressedCapitalGainsExemptReserve = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:UnrealisedExpressedCapitalGainsExemptReserve" });
                var CapitalGainsSpecificSecuritiesExemptReserve = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:CapitalGainsSpecificSecuritiesExemptReserve" });
                var CapitalGainsTangibleIntangibleFixedAssetsExemptReserve = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:CapitalGainsTangibleIntangibleFixedAssetsExemptReserve" });
                var CapitalGainsCorporateVehiclesExemptReserve = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:CapitalGainsCorporateVehiclesExemptReserve" });
                var CapitalGainsRiverVesselExemptReserve = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:CapitalGainsRiverVesselExemptReserve" });
                var CapitalGainsSeaVesselExemptReserve = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:CapitalGainsSeaVesselExemptReserve" });
                var OtherRealisedCapitalGainsExemptReserve = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:OtherRealisedCapitalGainsExemptReserve" });
                var ExemptInvestmentReserve = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:ExemptInvestmentReserve" });
                var TaxShelterAuthorisedAudiovisualWorkExemptReserve = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:TaxShelterAuthorisedAudiovisualWorkExemptReserve" });
                var ProfitHomologationReorganizationPlanAmicableSettlementExemptReserve = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:ProfitHomologationReorganizationPlanAmicableSettlementExemptReserve" });
                var OtherExemptReserves = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:OtherExemptReserves" });
                var FormulaTarget = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:ExemptReservedProfit" });
                if (ExemptReservedProfit.Count > 0 || ExemptWriteDownDebtClaim.Count > 0 || ExemptProvisionRisksExpenses.Count > 0 || UnrealisedExpressedCapitalGainsExemptReserve.Count > 0 || CapitalGainsSpecificSecuritiesExemptReserve.Count > 0 || CapitalGainsTangibleIntangibleFixedAssetsExemptReserve.Count > 0 || CapitalGainsCorporateVehiclesExemptReserve.Count > 0 || CapitalGainsRiverVesselExemptReserve.Count > 0 || CapitalGainsSeaVesselExemptReserve.Count > 0 || OtherRealisedCapitalGainsExemptReserve.Count > 0 || ExemptInvestmentReserve.Count > 0 || TaxShelterAuthorisedAudiovisualWorkExemptReserve.Count > 0 || ProfitHomologationReorganizationPlanAmicableSettlementExemptReserve.Count > 0 || OtherExemptReserves.Count > 0 || FormulaTarget.Count > 0)
                {
                    var CalculatedAmountExemptReservedProfit = Sum(Number(ExemptWriteDownDebtClaim, "0"), Number(ExemptProvisionRisksExpenses, "0"), Number(UnrealisedExpressedCapitalGainsExemptReserve, "0"), Number(CapitalGainsSpecificSecuritiesExemptReserve, "0"), Number(CapitalGainsTangibleIntangibleFixedAssetsExemptReserve, "0"), Number(CapitalGainsCorporateVehiclesExemptReserve, "0"), Number(CapitalGainsRiverVesselExemptReserve, "0"), Number(CapitalGainsSeaVesselExemptReserve, "0"), Number(OtherRealisedCapitalGainsExemptReserve, "0"), Number(ExemptInvestmentReserve, "0"), Number(TaxShelterAuthorisedAudiovisualWorkExemptReserve, "0"), Number(ProfitHomologationReorganizationPlanAmicableSettlementExemptReserve, "0"), Number(OtherExemptReserves, "0"));
                    //CALCULATION HERE
                    if (FormulaTarget.Count == 0)
                    {
                        FormulaTarget = CreateNewCalculationElement(scenarioId, period, "tax-inc:ExemptReservedProfit", new string[] { "" }, "EUR", "INF");
                    }
                    if (FormulaTarget.Count > 1)
                    {
                        throw new Exception("More then one target for FormulaTarget in assertion f-corp-1106");
                    }
                    FormulaTarget.First().Calculated = true;
                    FormulaTarget.First().SetNumber(Sum(Number(ExemptWriteDownDebtClaim, "0"), Number(ExemptProvisionRisksExpenses, "0"), Number(UnrealisedExpressedCapitalGainsExemptReserve, "0"), Number(CapitalGainsSpecificSecuritiesExemptReserve, "0"), Number(CapitalGainsTangibleIntangibleFixedAssetsExemptReserve, "0"), Number(CapitalGainsCorporateVehiclesExemptReserve, "0"), Number(CapitalGainsRiverVesselExemptReserve, "0"), Number(CapitalGainsSeaVesselExemptReserve, "0"), Number(OtherRealisedCapitalGainsExemptReserve, "0"), Number(ExemptInvestmentReserve, "0"), Number(TaxShelterAuthorisedAudiovisualWorkExemptReserve, "0"), Number(ProfitHomologationReorganizationPlanAmicableSettlementExemptReserve, "0"), Number(OtherExemptReserves, "0")));
                    //FORMULA HERE
                    bool test = Number(ExemptReservedProfit, "0") == Sum(Number(ExemptWriteDownDebtClaim, "0"), Number(ExemptProvisionRisksExpenses, "0"), Number(UnrealisedExpressedCapitalGainsExemptReserve, "0"), Number(CapitalGainsSpecificSecuritiesExemptReserve, "0"), Number(CapitalGainsTangibleIntangibleFixedAssetsExemptReserve, "0"), Number(CapitalGainsCorporateVehiclesExemptReserve, "0"), Number(CapitalGainsRiverVesselExemptReserve, "0"), Number(CapitalGainsSeaVesselExemptReserve, "0"), Number(OtherRealisedCapitalGainsExemptReserve, "0"), Number(ExemptInvestmentReserve, "0"), Number(TaxShelterAuthorisedAudiovisualWorkExemptReserve, "0"), Number(ProfitHomologationReorganizationPlanAmicableSettlementExemptReserve, "0"), Number(OtherExemptReserves, "0"));
                    if (!test)
                    {
                        // MESSAGES
                        AddMessage(new BizTaxErrorDataContract
                        {
                            FileId = _fileId,
                            Id = "f-corp-1106",
                            Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Het totaal vermeld in de rubriek '{Vrijgestelde gereserveerde winst}' ({$ExemptReservedProfit}) is niet juist (= {$CalculatedAmountExemptReservedProfit})."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Le total repris dans la rubrique '{Bénéfices réservés exonérés}' ({$ExemptReservedProfit}) n'est pas correct (= {$CalculatedAmountExemptReservedProfit})."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Der in der Rubrik '{Steuerfreie Gewinnrücklagen}' angegebene Gesamtbetrag ({$ExemptReservedProfit}) ist unzutreffend (= {$CalculatedAmountExemptReservedProfit})."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Het totaal vermeld in de rubriek '{Vrijgestelde gereserveerde winst}' ({$ExemptReservedProfit}) is niet juist (= {$CalculatedAmountExemptReservedProfit})."}  
 }
                            ,
                            Fields = new List<string>()
                        }
                        );
                    }
                }
            } // end period loop

        }

        internal void Calc_f_corp_1107_assertion_set()
        {
            string scenarioId = GetScenarioId("");
            string period = "I-End";
            // SCENARIO: ""
            // PERIOD: "I-End"
            var ExemptInvestmentReserveEnd = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:ExemptInvestmentReserve" });
            var ExemptInvestmentReserveStart = GetElementsByRef(scenarioId, new string[] { "I-Start" }, new string[] { "tax-inc:ExemptInvestmentReserve" });
            if (ExemptInvestmentReserveStart.Count > 0 || ExemptInvestmentReserveEnd.Count > 0)
            {
                var PeriodStartDate = GetNodeByName("tax-inc", "PeriodStartDate");
                var PeriodEndDate = GetNodeByName("tax-inc", "PeriodEndDate");
                var IncreaseExemptInvestmentReserve = Sum(Number(ExemptInvestmentReserveEnd, "0"), -Number(ExemptInvestmentReserveStart, "0"));
                //CALCULATION HERE
                if (Sum(Number(ExemptInvestmentReserveEnd, "0"), Number(ExemptInvestmentReserveStart, "0"))!=0)
                {
                    //FORMULA HERE
                    bool test = Sum(Number(ExemptInvestmentReserveEnd, "0"), -Number(ExemptInvestmentReserveStart, "0")) <= Number(LimitExemptInvestmentReserve, "18750");
                    if (!test)
                    {
                        // MESSAGES
                        AddMessage(new BizTaxErrorDataContract
                        {
                            FileId = _fileId,
                            Id = "f-corp-1107",
                            Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "De aangroei van de investeringsreserve ({$IncreaseExemptInvestmentReserve}) mag maximaal {$LimitExemptInvestmentReserve} EUR bedragen."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "L’augmentation du réserve d'investissement ({$IncreaseExemptInvestmentReserve}) doit être inférieure ou égale à {$LimitExemptInvestmentReserve} EUR."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Die Zunahme der Investitionsrücklagen ({$IncreaseExemptInvestmentReserve}) darf höchstens {$LimitExemptInvestmentReserve} EUR betragen."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "De aangroei van de investeringsreserve ({$IncreaseExemptInvestmentReserve}) mag maximaal {$LimitExemptInvestmentReserve} EUR bedragen."}  
 }
                            ,
                            Fields = new List<string>()
                        }
                        );
                    }
                }
            }

        }

        internal void Calc_f_corp_1202_assertion_set()
        {
            // HAS NO UNBOUND SEQUENCES
            var ShippingProfitTonnageBased = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:ShippingProfitTonnageBased" });
            var ShippingResultTonnageBased = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:ShippingResultTonnageBased" });
            if (ShippingResultTonnageBased.Count > 0 || ShippingProfitTonnageBased.Count > 0)
            {
                //CALCULATION HERE
                if (Exists(ShippingResultTonnageBased))
                {
                    //FORMULA HERE
                    bool test = Exists(ShippingProfitTonnageBased);
                    if (!test)
                    {
                        // MESSAGES
                        AddMessage(new BizTaxErrorDataContract
                        {
                            FileId = _fileId,
                            Id = "f-corp-1202",
                            Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Indien de rubriek '{Werkelijk resultaat uit de zeescheepvaart waarvoor de winst wordt vastgesteld op basis van de tonnage}' 	is ingevuld, dan is rubriek '{Winst uit zeescheepvaart, vastgesteld op basis van de tonnage}' verplicht in te vullen."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Si la rubrique '{Résultat effectif des activités de la navigation maritime, pour lesquelles le bénéfice est déterminé sur base du tonnage}' 	est remplie, la rubrique '{Bénéfice provenant de la navigation maritime, déterminé sur base du tonnage}' doit également être remplie."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Wenn die Rubrik '{Wirkliches Ergebnis aus Hochseeschifffahrtsaktivitäten, für die der Gewinn anhand der Tonnage bestimmt wird}' ausgefüllt ist, 	ist die Rubrik '{Gewinn aus der Hochseeschifffahrt, bestimmt anhand der Tonnage}' ebenfalls auszufüllen."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Indien de rubriek '{Werkelijk resultaat uit de zeescheepvaart waarvoor de winst wordt vastgesteld op basis van de tonnage}' 	is ingevuld, dan is rubriek '{Winst uit zeescheepvaart, vastgesteld op basis van de tonnage}' verplicht in te vullen."}  
 }
                            ,
                            Fields = new List<string>()
                        }
                        );
                    }
                }
            }

        }

        internal void Calc_f_corp_1203_assertion_set()
        {
            string scenarioId = GetScenarioId("");
            string period = "D";
            // SCENARIO: ""
            // PERIOD: "D"
            var ShippingResultNotTonnageBased = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:ShippingResultNotTonnageBased" });
            var FiscalResult = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:FiscalResult" });
            var ShippingResultTonnageBased = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:ShippingResultTonnageBased" });
            var FormulaTarget = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:ShippingResultNotTonnageBased" });
            if (ShippingResultNotTonnageBased.Count > 0 || FiscalResult.Count > 0 || ShippingResultTonnageBased.Count > 0 || FormulaTarget.Count > 0)
            {
                var CalculatedAmountShippingResultNotTonnageBased = Sum(Number(FiscalResult, "0"), -Number(ShippingResultTonnageBased, "0"));
                //CALCULATION HERE
                if (FormulaTarget.Count == 0)
                {
                    FormulaTarget = CreateNewCalculationElement(scenarioId, period, "tax-inc:ShippingResultNotTonnageBased", new string[] { "" }, "EUR", "INF");
                }
                if (FormulaTarget.Count > 1)
                {
                    throw new Exception("More then one target for FormulaTarget in assertion f-corp-1203");
                }
                FormulaTarget.First().Calculated = true;
                FormulaTarget.First().SetNumber(Sum(Number(FiscalResult, "0"), -Number(ShippingResultTonnageBased, "0")));
                //FORMULA HERE
                bool test = Number(ShippingResultNotTonnageBased, "0") == Sum(Number(FiscalResult, "0"), -Number(ShippingResultTonnageBased, "0"));
                if (!test)
                {
                    // MESSAGES
                    AddMessage(new BizTaxErrorDataContract
                    {
                        FileId = _fileId,
                        Id = "f-corp-1203",
                        Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Het totaal vermeld in de rubriek '{Werkelijk resultaat uit activiteiten waarvoor de winst niet wordt vastgesteld op basis van de tonnage}'	({$ShippingResultNotTonnageBased})	is niet juist (= {$CalculatedAmountShippingResultNotTonnageBased})."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Le total repris dans la rubrique '{Résultat effectif des activités pour lesquelles le bénéfice n’est pas déterminé sur base du tonnage}'	({$ShippingResultNotTonnageBased})	n'est pas correct (= {$CalculatedAmountShippingResultNotTonnageBased})."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Der Betrag, der angegeben ist in der Rubrik '{Effektives Ergebnis aus Aktivitäten, für die der Gewinn nicht anhand der Tonnage bestimmt wird}'	({$ShippingResultNotTonnageBased})	ist unzutreffend (= {$CalculatedAmountShippingResultNotTonnageBased})."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Het totaal vermeld in de rubriek '{Werkelijk resultaat uit activiteiten waarvoor de winst niet wordt vastgesteld op basis van de tonnage}'	({$ShippingResultNotTonnageBased})	is niet juist (= {$CalculatedAmountShippingResultNotTonnageBased})."}  
 }
                        ,
                        Fields = new List<string>()
                    }
                    );
                }
            }

        }

        internal void Calc_f_corp_1204_assertion_set()
        {
            string scenarioId = GetScenarioId("");
            string period = "D";
            // SCENARIO: ""
            // PERIOD: "D"
            var DeductionLimitElementsFiscalResult = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:DeductionLimitElementsFiscalResult" });
            var BenevolentAbnormalFinancialAdvantagesBenefitsAllKind = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:BenevolentAbnormalFinancialAdvantagesBenefitsAllKind" });
            var ViolationInvestmentCommitmentIntangibilityConditionInvestmentReserve = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:ViolationInvestmentCommitmentIntangibilityConditionInvestmentReserve" });
            var NonDeductibleCarExpensesPartBenefitsAllKind = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:NonDeductibleCarExpensesPartBenefitsAllKind" });
            var EmployeeParticipation = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:EmployeeParticipation" });
            var CapitalSubsidiesInterestSubsidiesAgriculturalSupport = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:CapitalSubsidiesInterestSubsidiesAgriculturalSupport" });
            var FormulaTarget = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:DeductionLimitElementsFiscalResult" });
            if (DeductionLimitElementsFiscalResult.Count > 0 || BenevolentAbnormalFinancialAdvantagesBenefitsAllKind.Count > 0 || ViolationInvestmentCommitmentIntangibilityConditionInvestmentReserve.Count > 0 || NonDeductibleCarExpensesPartBenefitsAllKind.Count > 0 || EmployeeParticipation.Count > 0 || CapitalSubsidiesInterestSubsidiesAgriculturalSupport.Count > 0 || FormulaTarget.Count > 0)
            {
                var CalculatedAmountDeductionLimitElementsFiscalResult = Sum(Number(BenevolentAbnormalFinancialAdvantagesBenefitsAllKind, "0"), Number(ViolationInvestmentCommitmentIntangibilityConditionInvestmentReserve, "0"), Number(NonDeductibleCarExpensesPartBenefitsAllKind, "0"), Number(EmployeeParticipation, "0"), Number(CapitalSubsidiesInterestSubsidiesAgriculturalSupport, "0"));
                //CALCULATION HERE
                if (FormulaTarget.Count == 0)
                {
                    FormulaTarget = CreateNewCalculationElement(scenarioId, period, "tax-inc:DeductionLimitElementsFiscalResult", new string[] { "" }, "EUR", "INF");
                }
                if (FormulaTarget.Count > 1)
                {
                    throw new Exception("More then one target for FormulaTarget in assertion f-corp-1204");
                }
                FormulaTarget.First().Calculated = true;
                FormulaTarget.First().SetNumber(Sum(Number(BenevolentAbnormalFinancialAdvantagesBenefitsAllKind, "0"), Number(ViolationInvestmentCommitmentIntangibilityConditionInvestmentReserve, "0"), Number(NonDeductibleCarExpensesPartBenefitsAllKind, "0"), Number(EmployeeParticipation, "0"), Number(CapitalSubsidiesInterestSubsidiesAgriculturalSupport, "0")));
                //FORMULA HERE
                bool test = Number(DeductionLimitElementsFiscalResult, "0") == Sum(Number(BenevolentAbnormalFinancialAdvantagesBenefitsAllKind, "0"), Number(ViolationInvestmentCommitmentIntangibilityConditionInvestmentReserve, "0"), Number(NonDeductibleCarExpensesPartBenefitsAllKind, "0"), Number(EmployeeParticipation, "0"), Number(CapitalSubsidiesInterestSubsidiesAgriculturalSupport, "0"));
                if (!test)
                {
                    // MESSAGES
                    AddMessage(new BizTaxErrorDataContract
                    {
                        FileId = _fileId,
                        Id = "f-corp-1204",
                        Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Het totaal vermeld in de rubriek '{Bestanddelen van het resultaat waarop de aftrekbeperking van toepassing is}' ({$DeductionLimitElementsFiscalResult}) 	is niet juist (= {$CalculatedAmountDeductionLimitElementsFiscalResult})."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Le total repris dans la rubrique '{Eléments du résultat sur lesquels s’applique la limitation de déduction}' ({$DeductionLimitElementsFiscalResult}) 	n'est pas correct (= {$CalculatedAmountDeductionLimitElementsFiscalResult})."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Der Betrag, der angegeben ist in der Rubrik '{Bestandteile des Ergebnisses, worauf die Abzugsbegrenzung anwendbar ist}' ({$DeductionLimitElementsFiscalResult}) 	ist unzutreffend (= {$CalculatedAmountDeductionLimitElementsFiscalResult})."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Het totaal vermeld in de rubriek '{Bestanddelen van het resultaat waarop de aftrekbeperking van toepassing is}' ({$DeductionLimitElementsFiscalResult}) 	is niet juist (= {$CalculatedAmountDeductionLimitElementsFiscalResult})."}  
 }
                        ,
                        Fields = new List<string>()
                    }
                    );
                }
            }

        }

        internal void Calc_f_corp_1206_assertion_set()
        {
            string scenarioId = GetScenarioId("");
            string period = "D";
            // SCENARIO: ""
            // PERIOD: "D"
            var RemainingFiscalResultBeforeOriginDistribution = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:RemainingFiscalResultBeforeOriginDistribution" });
            var ShippingResultNotTonnageBased = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:ShippingResultNotTonnageBased" });
            var DeductionLimitElementsFiscalResult = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:DeductionLimitElementsFiscalResult" });
            var FormulaTarget = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:RemainingFiscalResultBeforeOriginDistribution" });
            if (RemainingFiscalResultBeforeOriginDistribution.Count > 0 || ShippingResultNotTonnageBased.Count > 0 || DeductionLimitElementsFiscalResult.Count > 0 || FormulaTarget.Count > 0)
            {
                var CalculatedAmountRemainingFiscalResultBeforeOriginDistribution = Sum(Number(ShippingResultNotTonnageBased, "0"), -Number(DeductionLimitElementsFiscalResult, "0"));
                //CALCULATION HERE
                if (FormulaTarget.Count == 0)
                {
                    FormulaTarget = CreateNewCalculationElement(scenarioId, period, "tax-inc:RemainingFiscalResultBeforeOriginDistribution", new string[] { "" }, "EUR", "INF");
                }
                if (FormulaTarget.Count > 1)
                {
                    throw new Exception("More then one target for FormulaTarget in assertion f-corp-1206");
                }
                FormulaTarget.First().Calculated = true;
                FormulaTarget.First().SetNumber(Sum(Number(ShippingResultNotTonnageBased, "0"), -Number(DeductionLimitElementsFiscalResult, "0")));
                if (Number(ShippingResultNotTonnageBased, "0") >= (decimal)0)
                {
                    //FORMULA HERE
                    bool test = Number(RemainingFiscalResultBeforeOriginDistribution, "0") == Sum(Number(ShippingResultNotTonnageBased, "0"), -Number(DeductionLimitElementsFiscalResult, "0"));
                    if (!test)
                    {
                        // MESSAGES
                        AddMessage(new BizTaxErrorDataContract
                        {
                            FileId = _fileId,
                            Id = "f-corp-1206",
                            Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Het totaal vermeld in de rubriek '{Resterend resultaat}' ({$RemainingFiscalResultBeforeOriginDistribution}) is niet juist (= {$CalculatedAmountRemainingFiscalResultBeforeOriginDistribution})."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Le total repris dans la rubrique '{Résultat subsistant}' ({$RemainingFiscalResultBeforeOriginDistribution}) n'est pas correct (= {$CalculatedAmountRemainingFiscalResultBeforeOriginDistribution})."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Der in der Rubrik '{Verbleibendes Ergebnis}' angegebene Gesamtbetrag ({$RemainingFiscalResultBeforeOriginDistribution}) ist unzutreffend (= {$CalculatedAmountRemainingFiscalResultBeforeOriginDistribution})."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Het totaal vermeld in de rubriek '{Resterend resultaat}' ({$RemainingFiscalResultBeforeOriginDistribution}) is niet juist (= {$CalculatedAmountRemainingFiscalResultBeforeOriginDistribution})."}  
 }
                            ,
                            Fields = new List<string>()
                        }
                        );
                    }
                }
            }

        }

        internal void Calc_f_corp_1207_assertion_set()
        {
            string scenarioId = GetScenarioId("");
            string period = "D";
            // SCENARIO: ""
            // PERIOD: "D"
            var RemainingFiscalResultBeforeOriginDistribution = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:RemainingFiscalResultBeforeOriginDistribution" });
            var ShippingResultNotTonnageBased = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:ShippingResultNotTonnageBased" });
            var DeductionLimitElementsFiscalResult = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:DeductionLimitElementsFiscalResult" });
            var FormulaTarget = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:RemainingFiscalResultBeforeOriginDistribution" });
            if (RemainingFiscalResultBeforeOriginDistribution.Count > 0 || ShippingResultNotTonnageBased.Count > 0 || DeductionLimitElementsFiscalResult.Count > 0 || FormulaTarget.Count > 0)
            {
                var CalculatedAmountRemainingFiscalResultBeforeOriginDistribution = Sum(Number(ShippingResultNotTonnageBased, "0"), -Number(DeductionLimitElementsFiscalResult, "0"));
                //CALCULATION HERE
                if (FormulaTarget.Count == 0)
                {
                    FormulaTarget = CreateNewCalculationElement(scenarioId, period, "tax-inc:RemainingFiscalResultBeforeOriginDistribution", new string[] { "" }, "EUR", "INF");
                }
                if (FormulaTarget.Count > 1)
                {
                    throw new Exception("More then one target for FormulaTarget in assertion f-corp-1207");
                }
                FormulaTarget.First().Calculated = true;
                FormulaTarget.First().SetNumber(Sum(Number(ShippingResultNotTonnageBased, "0"), -Number(DeductionLimitElementsFiscalResult, "0")));
                if (Number(ShippingResultNotTonnageBased, "0") < (decimal)0)
                {
                    //FORMULA HERE
                    bool test = Number(RemainingFiscalResultBeforeOriginDistribution, "0") == Sum(Number(ShippingResultNotTonnageBased, "0"), -Number(DeductionLimitElementsFiscalResult, "0"));
                    if (!test)
                    {
                        // MESSAGES
                        AddMessage(new BizTaxErrorDataContract
                        {
                            FileId = _fileId,
                            Id = "f-corp-1207",
                            Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Het totaal vermeld in de rubriek '{Resterend resultaat}' ({$RemainingFiscalResultBeforeOriginDistribution}) is niet juist (= {$CalculatedAmountRemainingFiscalResultBeforeOriginDistribution})."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Le total repris dans la rubrique '{Résultat subsistant}' ({$RemainingFiscalResultBeforeOriginDistribution}) n'est pas correct (= {$CalculatedAmountRemainingFiscalResultBeforeOriginDistribution})."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Der in der Rubrik '{Verbleibendes Ergebnis}' angegebene Gesamtbetrag ({$RemainingFiscalResultBeforeOriginDistribution}) ist unzutreffend (= {$CalculatedAmountRemainingFiscalResultBeforeOriginDistribution})."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Het totaal vermeld in de rubriek '{Resterend resultaat}' ({$RemainingFiscalResultBeforeOriginDistribution}) is niet juist (= {$CalculatedAmountRemainingFiscalResultBeforeOriginDistribution})."}  
 }
                            ,
                            Fields = new List<string>()
                        }
                        );
                    }
                }
            }

        }

        internal void Calc_f_corp_1235_assertion_set()
        {
            // HAS NO UNBOUND SEQUENCES
            var CapitalSubsidiesInterestSubsidiesAgriculturalSupport = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:CapitalSubsidiesInterestSubsidiesAgriculturalSupport" });
            var CapitalSubsidiesInterestSubsidiesAgriculturalSupportRate500 = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:CapitalSubsidiesInterestSubsidiesAgriculturalSupportRate500" });
            if (CapitalSubsidiesInterestSubsidiesAgriculturalSupport.Count > 0 || CapitalSubsidiesInterestSubsidiesAgriculturalSupportRate500.Count > 0)
            {
                //CALCULATION HERE
                //FORMULA HERE
                bool test = String(CapitalSubsidiesInterestSubsidiesAgriculturalSupport, "0") == String(CapitalSubsidiesInterestSubsidiesAgriculturalSupportRate500, "0");
                if (!test)
                {
                    // MESSAGES
                    AddMessage(new BizTaxErrorDataContract
                    {
                        FileId = _fileId,
                        Id = "f-corp-1235",
                        Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Het totaal vermeld in de rubriek 	'{Kapitaal- en interestsubsidies in het kader van de steun aan de landbouw, belastbaar tegen 5 %}' 	({$CapitalSubsidiesInterestSubsidiesAgriculturalSupportRate500}) is niet juist (= {$CapitalSubsidiesInterestSubsidiesAgriculturalSupport})."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Le total repris dans la rubrique 	'{Subsides en capital et en intérêts dans le cadre de l'aide à l'agriculture, imposables à 5 %}' 	({$CapitalSubsidiesInterestSubsidiesAgriculturalSupportRate500}) n'est pas correct (= {$CapitalSubsidiesInterestSubsidiesAgriculturalSupport}). "}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Der Betrag, der angegeben ist in der Rubrik 	'{Kapital- und Zinszuschüsse im Rahmen von Agrarbeihilfen, steuerbar zu 5 %}' 	({$CapitalSubsidiesInterestSubsidiesAgriculturalSupportRate500}) ist unzutreffend (= {$CapitalSubsidiesInterestSubsidiesAgriculturalSupport})."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Het totaal vermeld in de rubriek 	'{Kapitaal- en interestsubsidies in het kader van de steun aan de landbouw, belastbaar tegen 5 %}' 	({$CapitalSubsidiesInterestSubsidiesAgriculturalSupportRate500}) is niet juist (= {$CapitalSubsidiesInterestSubsidiesAgriculturalSupport})."}  
 }
                        ,
                        Fields = new List<string>()
                    }
                    );
                }
            }

        }

        internal void Calc_f_corp_1236_assertion_set()
        {
            string scenarioId = GetScenarioId("");
            string period = "D";
            // SCENARIO: ""
            // PERIOD: "D"
            var CarryOverNextTaxPeriodPEExemptIncomeMovableAssets = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:CarryOverNextTaxPeriodPEExemptIncomeMovableAssets" });
            var AccumulatedPEExemptIncomeMovableAssets = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:AccumulatedPEExemptIncomeMovableAssets" }); ;
            var CarryOverNextTaxPeriodInsufficientProfitsPEExemptIncomeMovableAssetsCurrentTaxPeriod = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:CarryOverNextTaxPeriodInsufficientProfitsPEExemptIncomeMovableAssetsCurrentTaxPeriod" });
            var DeductionPEExemptIncomeMovableAssetsPreviousTaxPeriod = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:DeductionPEExemptIncomeMovableAssetsPreviousTaxPeriod" });
            var FormulaTarget = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:CarryOverNextTaxPeriodPEExemptIncomeMovableAssets" });
            if (AccumulatedPEExemptIncomeMovableAssets.Count > 0 || CarryOverNextTaxPeriodInsufficientProfitsPEExemptIncomeMovableAssetsCurrentTaxPeriod.Count > 0 || DeductionPEExemptIncomeMovableAssetsPreviousTaxPeriod.Count > 0 || CarryOverNextTaxPeriodPEExemptIncomeMovableAssets.Count > 0 || FormulaTarget.Count > 0)
            {
                var CalculatedAmountCarryOverNextTaxPeriodPEExemptIncomeMovableAssets = Max(Sum(Number(AccumulatedPEExemptIncomeMovableAssets, "0"), Number(CarryOverNextTaxPeriodInsufficientProfitsPEExemptIncomeMovableAssetsCurrentTaxPeriod, "0"), -Number(DeductionPEExemptIncomeMovableAssetsPreviousTaxPeriod, "0")), (decimal)0);
                //CALCULATION HERE
                if (FormulaTarget.Count == 0)
                {
                    FormulaTarget = CreateNewCalculationElement(scenarioId, period, "tax-inc:CarryOverNextTaxPeriodPEExemptIncomeMovableAssets", new string[] { "" }, "EUR", "INF");
                }
                if (FormulaTarget.Count > 1)
                {
                    throw new Exception("More then one target for FormulaTarget in assertion f-corp-1236");
                }
                FormulaTarget.First().Calculated = true;
                FormulaTarget.First().SetNumber(Sum(Number(AccumulatedPEExemptIncomeMovableAssets, "0"), Number(CarryOverNextTaxPeriodInsufficientProfitsPEExemptIncomeMovableAssetsCurrentTaxPeriod, "0"), -Number(DeductionPEExemptIncomeMovableAssetsPreviousTaxPeriod, "0")));
                //FORMULA HERE
                bool test = Number(CarryOverNextTaxPeriodPEExemptIncomeMovableAssets, "0") == Max(Sum(Number(AccumulatedPEExemptIncomeMovableAssets, "0"), Number(CarryOverNextTaxPeriodInsufficientProfitsPEExemptIncomeMovableAssetsCurrentTaxPeriod, "0"), -Number(DeductionPEExemptIncomeMovableAssetsPreviousTaxPeriod, "0")), (decimal)0);
                if (!test)
                {
                    // MESSAGES
                    AddMessage(new BizTaxErrorDataContract
                    {
                        FileId = _fileId,
                        Id = "f-corp-1236",
                        Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Het totaal vermeld in de rubriek '{Saldo van de aftrek definitief belaste inkomsten dat overdraagbaar is naar het volgende belastbare tijdperk}' ({$CarryOverNextTaxPeriodPEExemptIncomeMovableAssets}) is niet juist (= {$CalculatedAmountCarryOverNextTaxPeriodPEExemptIncomeMovableAssets})."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Le total repris dans la rubrique '{Solde de la déduction revenus définitivement taxés reportable sur la période imposable suivante}' ({$CarryOverNextTaxPeriodPEExemptIncomeMovableAssets}) n'est pas correct (= {$CalculatedAmountCarryOverNextTaxPeriodPEExemptIncomeMovableAssets})."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Der in der Rubrik '{Restbetrag des Abzugs von endgültig besteuerten Einkünften, der auf den folgenden Besteuerungszeitraum vorgetragen werden kann}' ({$CarryOverNextTaxPeriodPEExemptIncomeMovableAssets}) angegebene Gesamtbetrag ist unzutreffend (= {$CalculatedAmountCarryOverNextTaxPeriodPEExemptIncomeMovableAssets})."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Het totaal vermeld in de rubriek '{Saldo van de aftrek definitief belaste inkomsten dat overdraagbaar is naar het volgende belastbare tijdperk}' ({$CarryOverNextTaxPeriodPEExemptIncomeMovableAssets}) is niet juist (= {$CalculatedAmountCarryOverNextTaxPeriodPEExemptIncomeMovableAssets})."}  
 }
                        ,
                        Fields = new List<string>()
                    }
                    );
                }
            }

        }

        internal void Calc_f_corp_1237_assertion_set()
        {
            // HAS NO UNBOUND SEQUENCES
            var AccumulatedPEExemptIncomeMovableAssets = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:AccumulatedPEExemptIncomeMovableAssets" });
            var DeductionPEExemptIncomeMovableAssetsPreviousTaxPeriod = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:DeductionPEExemptIncomeMovableAssetsPreviousTaxPeriod" });
            if (AccumulatedPEExemptIncomeMovableAssets.Count > 0 || DeductionPEExemptIncomeMovableAssetsPreviousTaxPeriod.Count > 0)
            {
                //CALCULATION HERE
                //FORMULA HERE
                bool test = Number(AccumulatedPEExemptIncomeMovableAssets, "0") >= Number(DeductionPEExemptIncomeMovableAssetsPreviousTaxPeriod, "0");
                if (!test)
                {
                    // MESSAGES
                    AddMessage(new BizTaxErrorDataContract
                    {
                        FileId = _fileId,
                        Id = "f-corp-1237",
                        Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Het bedrag vermeld in de rubriek 	'{Saldo van de overgedragen aftrek definitief belaste inkomsten}' ({$AccumulatedPEExemptIncomeMovableAssets}) 	moet groter zijn dan of gelijk zijn aan het bedrag vermeld in de rubriek 	'{Overgedragen aftrek definitief belaste inkomsten die werkelijk wordt afgetrokken van het belastbare tijdperk}' ({$DeductionPEExemptIncomeMovableAssetsPreviousTaxPeriod})."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Le montant repris dans la rubrique '{Solde reporté de la déduction des revenus définitivement taxés}' ({$AccumulatedPEExemptIncomeMovableAssets}) 	doit être supérieur ou égal au montant repris dans la rubrique 	'{Déduction revenus définitivement taxés reportée qui a été effectivement déduite durant la période imposable}' ({$DeductionPEExemptIncomeMovableAssetsPreviousTaxPeriod})."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Der Betrag, der angegeben ist in der Rubrik 	'{Restbetrag des vorgetragenen Abzugs von endgültig besteuerten Einkünften}' ({$AccumulatedPEExemptIncomeMovableAssets}) 	muss höher sein als der Betrag, der angegeben ist in der Rubrik 	'{Vorgetragener Abzug von endgültig besteuerten Einkünften, der tatsächlich im Besteuerungszeitraum abgezogen wurde}' oder diesem entsprechen ({$DeductionPEExemptIncomeMovableAssetsPreviousTaxPeriod})."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Het bedrag vermeld in de rubriek 	'{Saldo van de overgedragen aftrek definitief belaste inkomsten}' ({$AccumulatedPEExemptIncomeMovableAssets}) 	moet groter zijn dan of gelijk zijn aan het bedrag vermeld in de rubriek 	'{Overgedragen aftrek definitief belaste inkomsten die werkelijk wordt afgetrokken van het belastbare tijdperk}' ({$DeductionPEExemptIncomeMovableAssetsPreviousTaxPeriod})."}  
 }
                        ,
                        Fields = new List<string>()
                    }
                    );
                }
            }

        }

        internal void Calc_f_corp_1400_assertion_set()
        {
            string scenarioId = GetScenarioId("");
            string period = "D";
            // SCENARIO: ""
            // PERIOD: "D"
            var DeductibleMiscellaneousExemptions = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:DeductibleMiscellaneousExemptions" });
            var ExemptGifts = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:ExemptGifts" });
            var ExemptionAdditionalPersonnelMiscellaneousExemptions = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:ExemptionAdditionalPersonnelMiscellaneousExemptions" });
            var ExemptionAdditionalPersonnelSMEs = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:ExemptionAdditionalPersonnelSMEs" });
            var ExemptionTrainingPeriodBonus = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:ExemptionTrainingPeriodBonus" });
            var OtherMiscellaneousExemptions = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:OtherMiscellaneousExemptions" });
            var FormulaTarget = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:DeductibleMiscellaneousExemptions" });
            if (DeductibleMiscellaneousExemptions.Count > 0 || ExemptGifts.Count > 0 || ExemptionAdditionalPersonnelMiscellaneousExemptions.Count > 0 || ExemptionAdditionalPersonnelSMEs.Count > 0 || ExemptionTrainingPeriodBonus.Count > 0 || OtherMiscellaneousExemptions.Count > 0 || FormulaTarget.Count > 0)
            {
                var CalculatedAmountDeductibleMiscellaneousExemptions = Sum(Number(ExemptGifts, "0"), Number(ExemptionAdditionalPersonnelMiscellaneousExemptions, "0"), Number(ExemptionAdditionalPersonnelSMEs, "0"), Number(ExemptionTrainingPeriodBonus, "0"), Number(OtherMiscellaneousExemptions, "0"));
                //CALCULATION HERE
                if (FormulaTarget.Count == 0)
                {
                    FormulaTarget = CreateNewCalculationElement(scenarioId, period, "tax-inc:DeductibleMiscellaneousExemptions", new string[] { "" }, "EUR", "INF");
                }
                if (FormulaTarget.Count > 1)
                {
                    throw new Exception("More then one target for FormulaTarget in assertion f-corp-1400");
                }
                FormulaTarget.First().Calculated = true;
                FormulaTarget.First().SetNumber(Sum(Number(ExemptGifts, "0"), Number(ExemptionAdditionalPersonnelMiscellaneousExemptions, "0"), Number(ExemptionAdditionalPersonnelSMEs, "0"), Number(ExemptionTrainingPeriodBonus, "0"), Number(OtherMiscellaneousExemptions, "0")));
                //FORMULA HERE
                bool test = Number(DeductibleMiscellaneousExemptions, "0") == Sum(Number(ExemptGifts, "0"), Number(ExemptionAdditionalPersonnelMiscellaneousExemptions, "0"), Number(ExemptionAdditionalPersonnelSMEs, "0"), Number(ExemptionTrainingPeriodBonus, "0"), Number(OtherMiscellaneousExemptions, "0"));
                if (!test)
                {
                    // MESSAGES
                    AddMessage(new BizTaxErrorDataContract
                    {
                        FileId = _fileId,
                        Id = "f-corp-1400",
                        Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Het totaal vermeld in de rubriek '{Niet-belastbare bestanddelen}' ({$DeductibleMiscellaneousExemptions}) is niet juist (= {$CalculatedAmountDeductibleMiscellaneousExemptions})."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Le total repris dans la rubrique '{Eléments non imposables}' ({$DeductibleMiscellaneousExemptions}) n'est pas correct (= {$CalculatedAmountDeductibleMiscellaneousExemptions})."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Der in der Rubrik '{Nicht steuerpflichtige Bestandteile}' angegebene Gesamtbetrag ({$DeductibleMiscellaneousExemptions}) ist unzutreffend (= {$CalculatedAmountDeductibleMiscellaneousExemptions})."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Het totaal vermeld in de rubriek '{Niet-belastbare bestanddelen}' ({$DeductibleMiscellaneousExemptions}) is niet juist (= {$CalculatedAmountDeductibleMiscellaneousExemptions})."}  
 }
                        ,
                        Fields = new List<string>()
                    }
                    );
                }
            }

        }

        internal void Calc_f_corp_1401_assertion_set()
        {

        }

        internal void Calc_f_corp_1402_assertion_set()
        {
            string scenarioId = GetScenarioId("");
            string period = "D";
            // SCENARIO: ""
            // PERIOD: "D"
            var ExemptionAdditionalPersonnelSMEs = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:ExemptionAdditionalPersonnelSMEs" });
            var ExemptionAdditionalPersonnel = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:ExemptionAdditionalPersonnel" });
            if (ExemptionAdditionalPersonnel.Count > 0 || ExemptionAdditionalPersonnelSMEs.Count > 0)
            {
                //CALCULATION HERE
                //FORMULA HERE
                bool test = String(ExemptionAdditionalPersonnelSMEs, "0") == String(ExemptionAdditionalPersonnel, "0");
                if (!test)
                {
                    // MESSAGES
                    AddMessage(new BizTaxErrorDataContract
                    {
                        FileId = _fileId,
                        Id = "f-corp-1402",
                        Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Indien de rubriek '{Vrijstelling bijkomend personeel KMO}' ({$ExemptionAdditionalPersonnelSMEs}) is ingevuld, dan is dit bedrag gelijk aan het bedrag ingevuld in de bijlage '{276T}' ({$ExemptionAdditionalPersonnel})."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Si la rubrique '{Exonération pour personnel supplémentaire PME}' ({$ExemptionAdditionalPersonnelSMEs}) est remplie, ce montant est égal à celui rempli dans l'annexe '{276T}' ({$ExemptionAdditionalPersonnel})."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Wenn die Rubrik '{Steuerbefreiung für Zusatzpersonal KMB}' ausgefüllt ist ({$ExemptionAdditionalPersonnelSMEs}), entspricht dieser Betrag dem in der Anlage '{276T}' ausgefüllten Betrag ({$ExemptionAdditionalPersonnel})."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Indien de rubriek '{Vrijstelling bijkomend personeel KMO}' ({$ExemptionAdditionalPersonnelSMEs}) is ingevuld, dan is dit bedrag gelijk aan het bedrag ingevuld in de bijlage '{276T}' ({$ExemptionAdditionalPersonnel})."}  
 }
                        ,
                        Fields = new List<string>()
                    }
                    );
                }
            }

        }

        internal void Calc_f_corp_1403_assertion_set()
        {
            string scenarioId = GetScenarioId("");
            string period = "D";
            // SCENARIO: ""
            // PERIOD: "D"
            var ExemptGifts = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:ExemptGifts" });
            var Liberalities = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:Liberalities" });
            if (ExemptGifts.Count > 0 || Liberalities.Count > 0)
            {
                //CALCULATION HERE
                //FORMULA HERE
                bool test = Number(ExemptGifts, "0") <= Number(Liberalities, "0");
                if (!test)
                {
                    // MESSAGES
                    AddMessage(new BizTaxErrorDataContract
                    {
                        FileId = _fileId,
                        Id = "f-corp-1403",
                        Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Het bedrag vermeld in de rubriek '{Vrijgestelde giften}' ({$ExemptGifts}) 	moet kleiner zijn dan of gelijk zijn aan het bedrag vermeld in de rubriek 	'{Liberaliteiten}' ({$Liberalities})."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Le montant repris dans la rubrique '{Libéralités exonérées}' ({$ExemptGifts}) 	doit être inférieur ou égal au montant repris dans la rubrique 	'{Libéralités}' ({$Liberalities})."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Der in der Rubrik '{Steuerfreie unentgeltliche Zuwendungen}' ({$ExemptGifts}) 	angegebene Betrag muss niedriger sein als der in der Rubrik 	'{Unentgeltliche Zuwendungen}' 	angegebene Betrag oder diesem entsprechen ({$Liberalities})."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Het bedrag vermeld in de rubriek '{Vrijgestelde giften}' ({$ExemptGifts}) 	moet kleiner zijn dan of gelijk zijn aan het bedrag vermeld in de rubriek 	'{Liberaliteiten}' ({$Liberalities})."}  
 }
                        ,
                        Fields = new List<string>()
                    }
                    );
                }
            }

        }

        internal void Calc_f_corp_1404_assertion_set()
        {
            string scenarioId = GetScenarioId("");
            string period = "D";
            // SCENARIO: ""
            // PERIOD: "D"
            var ExemptGifts = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:ExemptGifts" });
            var ShippingResultNotTonnageBased = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:ShippingResultNotTonnageBased" });
            if (ExemptGifts.Count > 0 || ShippingResultNotTonnageBased.Count > 0)
            {
                var CalculatedAmountLimitExemptGifts = Math.Round(Number(ShippingResultNotTonnageBased, "0") * Number(RateShippingResultNotTonnageBased, "0.05") * (decimal)100, MidpointRounding.AwayFromZero) / (decimal)100;
                //CALCULATION HERE
                if ((Number(ShippingResultNotTonnageBased, "0") >= (decimal)0) && (Number(ExemptGifts, "0") > (decimal)0))
                {
                    //FORMULA HERE
                    bool test = (Number(ExemptGifts, "0") <= (Math.Round(Number(ShippingResultNotTonnageBased, "0") * Number(RateShippingResultNotTonnageBased, "0.05") * (decimal)100, MidpointRounding.AwayFromZero) / (decimal)100)) && (Number(ExemptGifts, "0") <= (decimal)500000);
                    if (!test)
                    {
                        // MESSAGES
                        AddMessage(new BizTaxErrorDataContract
                        {
                            FileId = _fileId,
                            Id = "f-corp-1404",
                            Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Het bedrag vermeld in de rubriek '{Vrijgestelde giften}' ({$ExemptGifts}) 	moet kleiner zijn dan of gelijk zijn aan 5% van het bedrag vermeld in de rubriek 	'{Werkelijk resultaat uit activiteiten waarvoor de winst niet wordt vastgesteld op basis van de tonnage}' (= {$CalculatedAmountLimitExemptGifts}), met een maximum van 500 000 EUR. "}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Le montant repris dans la rubrique '{Libéralités exonérées}' ({$ExemptGifts}) 	doit être inférieur ou égal à 5% du montant repris dans la rubrique 	'{Résultat effectif des activités pour lesquelles le bénéfice n’est pas déterminé sur base du tonnage}' (= {$CalculatedAmountLimitExemptGifts}), au maximum 500 000 EUR. "}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Der in der Rubrik '{Steuerfreie unentgeltliche Zuwendungen}' ({$ExemptGifts}) 	angegebene Betrag darf sich, mit einem Maximalwert in Höhe von 500 000 EUR, maximal auf 5% des in der Rubrik 	'{Effektives Ergebnis aus Aktivitäten, für die der Gewinn nicht anhand der Tonnage bestimmt wird}' (= {$CalculatedAmountLimitExemptGifts}) angegebenen Betrags belaufen. "}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Het bedrag vermeld in de rubriek '{Vrijgestelde giften}' ({$ExemptGifts}) 	moet kleiner zijn dan of gelijk zijn aan 5% van het bedrag vermeld in de rubriek 	'{Werkelijk resultaat uit activiteiten waarvoor de winst niet wordt vastgesteld op basis van de tonnage}' (= {$CalculatedAmountLimitExemptGifts}), met een maximum van 500 000 EUR. "}  
 }
                            ,
                            Fields = new List<string>()
                        }
                        );
                    }
                }
            }

        }

        internal void Calc_f_corp_1405_assertion_set()
        {
            // HAS NO UNBOUND SEQUENCES
            var Liberalities = GetElementsByScenDefs(new string[] { "d-ty:DescriptionTypedDimension/d-ty:NatureTypedDimension", "" }, new string[] { "D" }, new string[] { "tax-inc:Liberalities" });
            var ExemptGifts = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:ExemptGifts" });
            if (ExemptGifts.Count > 0 || Liberalities.Count > 0)
            {
                //CALCULATION HERE
                if (Number(ExemptGifts) > (decimal)0)
                {
                    //FORMULA HERE
                    bool test = Number(Liberalities, "0") >= (decimal)40.00;
                    if (!test)
                    {
                        // MESSAGES
                        AddMessage(new BizTaxErrorDataContract
                        {
                            FileId = _fileId,
                            Id = "f-corp-1405",
                            Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Het bedrag vermeld in de rubriek '{Liberaliteiten}' moet groter zijn dan of gelijk zijn aan 40,00 EUR, 	indien de rubriek '{Vrijgestelde giften}' is ingevuld."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Le montant repris dans la rubrique '{Libéralités}' doit être supérieur ou égal à 40,00 EUR, 	si la rubrique '{Libéralités exonérées}' est complétée."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Wenn die Rubrik '{Steuerfreie unentgeltliche Zuwendungen}' ausgefüllt wurde, 	muss der in der Rubrik '{Unentgeltliche Zuwendungen}' angegebene Betrag sich mindestens auf 40,00 EUR belaufen."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Het bedrag vermeld in de rubriek '{Liberaliteiten}' moet groter zijn dan of gelijk zijn aan 40,00 EUR, 	indien de rubriek '{Vrijgestelde giften}' is ingevuld."}  
 }
                            ,
                            Fields = new List<string>()
                        }
                        );
                    }
                }
            }

        }

        internal void Calc_f_corp_1406_assertion_set()
        {
            // HAS NO UNBOUND SEQUENCES
            var ExemptGifts = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:ExemptGifts" });
            var ShippingResultNotTonnageBased = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:ShippingResultNotTonnageBased" });
            if (ExemptGifts.Count > 0 || ShippingResultNotTonnageBased.Count > 0)
            {
                //CALCULATION HERE
                if (Number(ShippingResultNotTonnageBased, "0") < (decimal)0)
                {
                    //FORMULA HERE
                    bool test = Number(ExemptGifts, "0") == (decimal)0;
                    if (!test)
                    {
                        // MESSAGES
                        AddMessage(new BizTaxErrorDataContract
                        {
                            FileId = _fileId,
                            Id = "f-corp-1406",
                            Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Indien het bedrag vermeld in de rubriek '{Werkelijk resultaat uit activiteiten waarvoor de winst niet wordt vastgesteld op basis van de tonnage}' ({$ShippingResultNotTonnageBased}) 	negatief is, dan mag de rubriek 	'{Vrijgestelde giften}' ({$ExemptGifts}) niet ingevuld worden."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Si le montant repris dans la rubrique '{Résultat effectif des activités pour lesquelles le bénéfice n’est pas déterminé sur base du tonnage}' ({$ShippingResultNotTonnageBased}) 	est négatif, la rubrique 	'{Libéralités exonérées}' ({$ExemptGifts}) ne peut pas être remplie."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Wenn der Betrag, der angegeben ist in der Rubrik '{Effektives Ergebnis aus Aktivitäten, für die der Gewinn nicht anhand der Tonnage bestimmt wird}' ({$ShippingResultNotTonnageBased}) 	negativ ist, darf die Rubrik 	'{Steuerfreie unentgeltliche Zuwendungen}' ({$ExemptGifts}) nicht ausgefüllt werden."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Indien het bedrag vermeld in de rubriek '{Werkelijk resultaat uit activiteiten waarvoor de winst niet wordt vastgesteld op basis van de tonnage}' ({$ShippingResultNotTonnageBased}) 	negatief is, dan mag de rubriek 	'{Vrijgestelde giften}' ({$ExemptGifts}) niet ingevuld worden."}  
 }
                            ,
                            Fields = new List<string>()
                        }
                        );
                    }
                }
            }

        }

        internal void Calc_f_corp_1407_assertion_set()
        {
            string scenarioId = GetScenarioId("");
            string period = "D";
            // SCENARIO: ""
            // PERIOD: "D"
            var GrossPEExemptIncomeMovableAssets = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:GrossPEExemptIncomeMovableAssets" });
            var NetBelgianIncomeSharesPEExemptIncomeMovableAssets = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:NetBelgianIncomeSharesPEExemptIncomeMovableAssets" });
            var WithholdingTaxBelgianIncomeSharesPEExemptIncomeMovableAssets = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:WithholdingTaxBelgianIncomeSharesPEExemptIncomeMovableAssets" });
            var NetForeignIncomeSharesPEExemptIncomeMovableAssets = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:NetForeignIncomeSharesPEExemptIncomeMovableAssets" });
            var WithholdingTaxForeignIncomeSharesPEExemptIncomeMovableAssets = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:WithholdingTaxForeignIncomeSharesPEExemptIncomeMovableAssets" });
            var NetOtherBelgianIncomeSharesPEExemptIncomeMovableAssets = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:NetOtherBelgianIncomeSharesPEExemptIncomeMovableAssets" });
            var WithholdingTaxOtherBelgianIncomeSharesPEExemptIncomeMovableAssets = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:WithholdingTaxOtherBelgianIncomeSharesPEExemptIncomeMovableAssets" });
            var NetOtherForeignIncomeSharesPEExemptIncomeMovableAssets = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:NetOtherForeignIncomeSharesPEExemptIncomeMovableAssets" });
            var WithholdingTaxOtherForeignIncomeSharesPEExemptIncomeMovableAssets = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:WithholdingTaxOtherForeignIncomeSharesPEExemptIncomeMovableAssets" });
            var OtherExemptIncomeMovableAssets = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:OtherExemptIncomeMovableAssets" });
            var FormulaTarget = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:GrossPEExemptIncomeMovableAssets" });
            if (GrossPEExemptIncomeMovableAssets.Count > 0 || NetBelgianIncomeSharesPEExemptIncomeMovableAssets.Count > 0 || WithholdingTaxBelgianIncomeSharesPEExemptIncomeMovableAssets.Count > 0 || NetForeignIncomeSharesPEExemptIncomeMovableAssets.Count > 0 || WithholdingTaxForeignIncomeSharesPEExemptIncomeMovableAssets.Count > 0 || NetOtherBelgianIncomeSharesPEExemptIncomeMovableAssets.Count > 0 || WithholdingTaxOtherBelgianIncomeSharesPEExemptIncomeMovableAssets.Count > 0 || NetOtherForeignIncomeSharesPEExemptIncomeMovableAssets.Count > 0 || WithholdingTaxOtherForeignIncomeSharesPEExemptIncomeMovableAssets.Count > 0 || OtherExemptIncomeMovableAssets.Count > 0 || FormulaTarget.Count > 0)
            {
                var CalculatedAmountGrossPEExemptIncomeMovableAssets = Sum(Number(NetBelgianIncomeSharesPEExemptIncomeMovableAssets, "0"), Number(WithholdingTaxBelgianIncomeSharesPEExemptIncomeMovableAssets, "0"), Number(NetForeignIncomeSharesPEExemptIncomeMovableAssets, "0"), Number(WithholdingTaxForeignIncomeSharesPEExemptIncomeMovableAssets, "0"), Number(NetOtherBelgianIncomeSharesPEExemptIncomeMovableAssets, "0"), Number(WithholdingTaxOtherBelgianIncomeSharesPEExemptIncomeMovableAssets, "0"), Number(NetOtherForeignIncomeSharesPEExemptIncomeMovableAssets, "0"), Number(WithholdingTaxOtherForeignIncomeSharesPEExemptIncomeMovableAssets, "0"), Number(OtherExemptIncomeMovableAssets, "0"));
                //CALCULATION HERE
                if (FormulaTarget.Count == 0)
                {
                    FormulaTarget = CreateNewCalculationElement(scenarioId, period, "tax-inc:GrossPEExemptIncomeMovableAssets", new string[] { "" }, "EUR", "INF");
                }
                if (FormulaTarget.Count > 1)
                {
                    throw new Exception("More then one target for FormulaTarget in assertion f-corp-1407");
                }
                FormulaTarget.First().Calculated = true;
                FormulaTarget.First().SetNumber(Sum(Number(NetBelgianIncomeSharesPEExemptIncomeMovableAssets, "0"), Number(WithholdingTaxBelgianIncomeSharesPEExemptIncomeMovableAssets, "0"), Number(NetForeignIncomeSharesPEExemptIncomeMovableAssets, "0"), Number(WithholdingTaxForeignIncomeSharesPEExemptIncomeMovableAssets, "0"), Number(NetOtherBelgianIncomeSharesPEExemptIncomeMovableAssets, "0"), Number(WithholdingTaxOtherBelgianIncomeSharesPEExemptIncomeMovableAssets, "0"), Number(NetOtherForeignIncomeSharesPEExemptIncomeMovableAssets, "0"), Number(WithholdingTaxOtherForeignIncomeSharesPEExemptIncomeMovableAssets, "0"), Number(OtherExemptIncomeMovableAssets, "0")));
                //FORMULA HERE
                bool test = Number(GrossPEExemptIncomeMovableAssets, "0") == Sum(Number(NetBelgianIncomeSharesPEExemptIncomeMovableAssets, "0"), Number(WithholdingTaxBelgianIncomeSharesPEExemptIncomeMovableAssets, "0"), Number(NetForeignIncomeSharesPEExemptIncomeMovableAssets, "0"), Number(WithholdingTaxForeignIncomeSharesPEExemptIncomeMovableAssets, "0"), Number(NetOtherBelgianIncomeSharesPEExemptIncomeMovableAssets, "0"), Number(WithholdingTaxOtherBelgianIncomeSharesPEExemptIncomeMovableAssets, "0"), Number(NetOtherForeignIncomeSharesPEExemptIncomeMovableAssets, "0"), Number(WithholdingTaxOtherForeignIncomeSharesPEExemptIncomeMovableAssets, "0"), Number(OtherExemptIncomeMovableAssets, "0"));
                if (!test)
                {
                    // MESSAGES
                    AddMessage(new BizTaxErrorDataContract
                    {
                        FileId = _fileId,
                        Id = "f-corp-1407",
                        Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Het totaal vermeld in de rubriek '{Definitief belaste inkomsten en vrijgestelde roerende inkomsten voor aftrek kosten}' ({$GrossPEExemptIncomeMovableAssets}) is niet juist (= {$CalculatedAmountGrossPEExemptIncomeMovableAssets})."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Le total repris dans la rubrique '{Revenus définitivement taxés et revenus mobiliers exonérés avant déduction des frais}' (= {$GrossPEExemptIncomeMovableAssets}) n'est pas correct ({$CalculatedAmountGrossPEExemptIncomeMovableAssets})."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Der in der Rubrik '{Endgültig besteuerte Einkünfte und steuerfreie Mobilieneinkünfte vor Abzug der Kosten}' ({$GrossPEExemptIncomeMovableAssets}) angegebene Gesamtbetrag ist unzutreffend (= {$CalculatedAmountGrossPEExemptIncomeMovableAssets})."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Het totaal vermeld in de rubriek '{Definitief belaste inkomsten en vrijgestelde roerende inkomsten voor aftrek kosten}' ({$GrossPEExemptIncomeMovableAssets}) is niet juist (= {$CalculatedAmountGrossPEExemptIncomeMovableAssets})."}  
 }
                        ,
                        Fields = new List<string>()
                    }
                    );
                }
            }

        }

        internal void Calc_f_corp_1408_assertion_set()
        {
            string scenarioId = GetScenarioId("");
            string period = "D";
            // SCENARIO: ""
            // PERIOD: "D"
            var ExpensesSharesPEExemptIncomeMovableAssets = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:ExpensesSharesPEExemptIncomeMovableAssets" });
            var GrossPEExemptIncomeMovableAssets = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:GrossPEExemptIncomeMovableAssets" });
            var FormulaTarget = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:ExpensesSharesPEExemptIncomeMovableAssets" });
            if (GrossPEExemptIncomeMovableAssets.Count > 0 || ExpensesSharesPEExemptIncomeMovableAssets.Count > 0 || FormulaTarget.Count > 0)
            {
                var CalculatedAmountExpensesSharesPEExemptIncomeMovableAssets = (Math.Round(Number(GrossPEExemptIncomeMovableAssets, "0") * (decimal)0.05 * (decimal)100, MidpointRounding.AwayFromZero) / (decimal)100);
                //CALCULATION HERE
                if (FormulaTarget.Count == 0)
                {
                    FormulaTarget = CreateNewCalculationElement(scenarioId, period, "tax-inc:ExpensesSharesPEExemptIncomeMovableAssets", new string[] { "" }, "EUR", "INF");
                }
                if (FormulaTarget.Count > 1)
                {
                    throw new Exception("More then one target for FormulaTarget in assertion f-corp-1408");
                }
                FormulaTarget.First().Calculated = true;
                FormulaTarget.First().SetNumber((Math.Round(Number(GrossPEExemptIncomeMovableAssets, "0") * (decimal)0.05 * (decimal)100, MidpointRounding.AwayFromZero) / (decimal)100));
                //FORMULA HERE
                bool test = Number(ExpensesSharesPEExemptIncomeMovableAssets, "0") == (Math.Round(Number(GrossPEExemptIncomeMovableAssets, "0") * (decimal)0.05 * (decimal)100, MidpointRounding.AwayFromZero) / (decimal)100);
                if (!test)
                {
                    // MESSAGES
                    AddMessage(new BizTaxErrorDataContract
                    {
                        FileId = _fileId,
                        Id = "f-corp-1408",
                        Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Het bedrag vermeld in de rubriek 	'{Kosten}' ({$ExpensesSharesPEExemptIncomeMovableAssets}) 	moet gelijk zijn aan 5% van de '{Definitief belaste inkomsten en vrijgestelde roerende inkomsten voor aftrek kosten}' (= {$CalculatedAmountExpensesSharesPEExemptIncomeMovableAssets})."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Le montant repris dans la rubrique 	'{Frais}' ({$ExpensesSharesPEExemptIncomeMovableAssets}) 	doit être égal ou inférieur à 5% de la rubrique '{Revenus définitivement taxés et revenus mobiliers exonérés avant déduction des frais}' (= {$CalculatedAmountExpensesSharesPEExemptIncomeMovableAssets})."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Der in der Rubrik 	'{Kosten}' ({$ExpensesSharesPEExemptIncomeMovableAssets}) 	angegebene Betrag muss gleich oder niedriger sein als 5% der Rubrik '{Endgültig besteuerte Einkünfte und steuerfreie Mobilieneinkünfte vor Abzug der Kosten}' (= {$CalculatedAmountExpensesSharesPEExemptIncomeMovableAssets})."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Het bedrag vermeld in de rubriek 	'{Kosten}' ({$ExpensesSharesPEExemptIncomeMovableAssets}) 	moet gelijk zijn aan 5% van de '{Definitief belaste inkomsten en vrijgestelde roerende inkomsten voor aftrek kosten}' (= {$CalculatedAmountExpensesSharesPEExemptIncomeMovableAssets})."}  
 }
                        ,
                        Fields = new List<string>()
                    }
                    );
                }
            }

        }

        internal void Calc_f_corp_1409_assertion_set()
        {
            string scenarioId = GetScenarioId("");
            string period = "D";
            // SCENARIO: ""
            // PERIOD: "D"
            var NetPEExemptIncomeMovableAssets = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:NetPEExemptIncomeMovableAssets" });
            var GrossPEExemptIncomeMovableAssets = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:GrossPEExemptIncomeMovableAssets" });
            var ExpensesSharesPEExemptIncomeMovableAssets = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:ExpensesSharesPEExemptIncomeMovableAssets" });
            var FormulaTarget = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:NetPEExemptIncomeMovableAssets" });
            if (NetPEExemptIncomeMovableAssets.Count > 0 || GrossPEExemptIncomeMovableAssets.Count > 0 || ExpensesSharesPEExemptIncomeMovableAssets.Count > 0 || FormulaTarget.Count > 0)
            {
                var CalculatedAmountNetPEExemptIncomeMovableAssets = Max(Sum(Number(GrossPEExemptIncomeMovableAssets, "0"), -Number(ExpensesSharesPEExemptIncomeMovableAssets, "0")), (decimal)0);
                //CALCULATION HERE
                if (FormulaTarget.Count == 0)
                {
                    FormulaTarget = CreateNewCalculationElement(scenarioId, period, "tax-inc:NetPEExemptIncomeMovableAssets", new string[] { "" }, "EUR", "INF");
                }
                if (FormulaTarget.Count > 1)
                {
                    throw new Exception("More then one target for FormulaTarget in assertion f-corp-1409");
                }
                FormulaTarget.First().Calculated = true;
                FormulaTarget.First().SetNumber(Sum(Number(GrossPEExemptIncomeMovableAssets, "0"), -Number(ExpensesSharesPEExemptIncomeMovableAssets, "0")));
                //FORMULA HERE
                bool test = Number(NetPEExemptIncomeMovableAssets, "0") == Max(Sum(Number(GrossPEExemptIncomeMovableAssets, "0"), -Number(ExpensesSharesPEExemptIncomeMovableAssets, "0")), (decimal)0);
                if (!test)
                {
                    // MESSAGES
                    AddMessage(new BizTaxErrorDataContract
                    {
                        FileId = _fileId,
                        Id = "f-corp-1409",
                        Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Het totaal vermeld in de rubriek '{Definitief belaste inkomsten en vrijgestelde roerende inkomsten na aftrek kosten}' ({$NetPEExemptIncomeMovableAssets}) is niet juist (= {$CalculatedAmountNetPEExemptIncomeMovableAssets})."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Le total repris dans la rubrique '{Revenus définitivement taxés et revenus mobiliers exonérés après déduction des frais}' ({$NetPEExemptIncomeMovableAssets}) n'est pas correct (= {$CalculatedAmountNetPEExemptIncomeMovableAssets})."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Der in der Rubrik '{Endgültig besteuerte Einkünfte und steuerfreie Mobilieneinkünfte nach Abzug der Kosten}' angegebene Gesamtbetrag ({$NetPEExemptIncomeMovableAssets}) ist unzutreffend (= {$CalculatedAmountNetPEExemptIncomeMovableAssets})."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Het totaal vermeld in de rubriek '{Definitief belaste inkomsten en vrijgestelde roerende inkomsten na aftrek kosten}' ({$NetPEExemptIncomeMovableAssets}) is niet juist (= {$CalculatedAmountNetPEExemptIncomeMovableAssets})."}  
 }
                        ,
                        Fields = new List<string>()
                    }
                    );
                }
            }

        }

        internal void Calc_f_corp_1410_assertion_set()
        {
            string scenarioId = GetScenarioId("");
            string period = "D";
            // SCENARIO: ""
            // PERIOD: "D"
            var DeductiblePEExemptIncomeMovableAssets = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:DeductiblePEExemptIncomeMovableAssets" });
            var NetPEExemptIncomeMovableAssets = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:NetPEExemptIncomeMovableAssets" });
            var IncomeMergerAcquisitionRetainedSharesSimilarRegulationOtherEUMemberState = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:IncomeMergerAcquisitionRetainedSharesSimilarRegulationOtherEUMemberState" });
            var ExemptIncomeMovableAssetsRefinancingLoans = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:ExemptIncomeMovableAssetsRefinancingLoans" });
            var FormulaTarget = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:DeductiblePEExemptIncomeMovableAssets" });
            if (DeductiblePEExemptIncomeMovableAssets.Count > 0 || NetPEExemptIncomeMovableAssets.Count > 0 || IncomeMergerAcquisitionRetainedSharesSimilarRegulationOtherEUMemberState.Count > 0 || ExemptIncomeMovableAssetsRefinancingLoans.Count > 0 || FormulaTarget.Count > 0)
            {
                var CalculatedAmountDeductiblePEExemptIncomeMovableAssets = Sum(Number(NetPEExemptIncomeMovableAssets, "0"), Number(IncomeMergerAcquisitionRetainedSharesSimilarRegulationOtherEUMemberState, "0"), Number(ExemptIncomeMovableAssetsRefinancingLoans, "0"));
                //CALCULATION HERE
                if (FormulaTarget.Count == 0)
                {
                    FormulaTarget = CreateNewCalculationElement(scenarioId, period, "tax-inc:DeductiblePEExemptIncomeMovableAssets", new string[] { "" }, "EUR", "INF");
                }
                if (FormulaTarget.Count > 1)
                {
                    throw new Exception("More then one target for FormulaTarget in assertion f-corp-1410");
                }
                FormulaTarget.First().Calculated = true;
                FormulaTarget.First().SetNumber(Sum(Number(NetPEExemptIncomeMovableAssets, "0"), Number(IncomeMergerAcquisitionRetainedSharesSimilarRegulationOtherEUMemberState, "0"), Number(ExemptIncomeMovableAssetsRefinancingLoans, "0")));
                //FORMULA HERE
                bool test = Number(DeductiblePEExemptIncomeMovableAssets, "0") == Sum(Number(NetPEExemptIncomeMovableAssets, "0"), Number(IncomeMergerAcquisitionRetainedSharesSimilarRegulationOtherEUMemberState, "0"), Number(ExemptIncomeMovableAssetsRefinancingLoans, "0"));
                if (!test)
                {
                    // MESSAGES
                    AddMessage(new BizTaxErrorDataContract
                    {
                        FileId = _fileId,
                        Id = "f-corp-1410",
                        Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Het totaal vermeld in de rubriek '{Definitief belaste inkomsten en vrijgestelde roerende inkomsten}' ({$DeductiblePEExemptIncomeMovableAssets}) is niet juist (= {$CalculatedAmountDeductiblePEExemptIncomeMovableAssets})."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Le total repris dans la rubrique '{Revenus définitivement taxés et revenus mobiliers exonérés}' ({$DeductiblePEExemptIncomeMovableAssets}) n'est pas correct (= {$CalculatedAmountDeductiblePEExemptIncomeMovableAssets})."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Der in der Rubrik '{Endgültig besteuerte Einkünfte und steuerfreie Mobilieneinkünfte}' ({$DeductiblePEExemptIncomeMovableAssets}) angegebene Gesamtbetrag ist unzutreffend (= {$CalculatedAmountDeductiblePEExemptIncomeMovableAssets})."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Het totaal vermeld in de rubriek '{Definitief belaste inkomsten en vrijgestelde roerende inkomsten}' ({$DeductiblePEExemptIncomeMovableAssets}) is niet juist (= {$CalculatedAmountDeductiblePEExemptIncomeMovableAssets})."}  
 }
                        ,
                        Fields = new List<string>()
                    }
                    );
                }
            }

        }

        internal void Calc_f_corp_1411_assertion_set()
        {
            string scenarioId = GetScenarioId("");
            string period = "D";
            // SCENARIO: ""
            // PERIOD: "D"
            var CarryOverTaxLosses = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:CarryOverTaxLosses" });
            var CompensableTaxLosses = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:CompensableTaxLosses" });
            var CompensatedTaxLosses = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:CompensatedTaxLosses" });
            var LossCurrentTaxPeriod = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:LossCurrentTaxPeriod" });
            var FormulaTarget = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:CarryOverTaxLosses" });
            if (CarryOverTaxLosses.Count > 0 || CompensableTaxLosses.Count > 0 || CompensatedTaxLosses.Count > 0 || LossCurrentTaxPeriod.Count > 0 || FormulaTarget.Count > 0)
            {
                var CalculatedAmountCarryOverTaxLosses = (Number(CompensatedTaxLosses, "0") < (decimal)0) ? Sum(Number(CompensableTaxLosses, "0"), Number(CompensatedTaxLosses, "0")) : Sum(Number(CompensableTaxLosses, "0"), Number(LossCurrentTaxPeriod, "0"));
                //CALCULATION HERE
                if (FormulaTarget.Count == 0)
                {
                    FormulaTarget = CreateNewCalculationElement(scenarioId, period, "tax-inc:CarryOverTaxLosses", new string[] { "" }, "EUR", "INF");
                }
                if (FormulaTarget.Count > 1)
                {
                    throw new Exception("More then one target for FormulaTarget in assertion f-corp-1411");
                }
                FormulaTarget.First().Calculated = true;
               // FormulaTarget.First().SetNumber(Sum(Number(CompensableTaxLosses, "0"), Number(CompensatedTaxLosses, "0")) );//|| Sum(Number(CompensableTaxLosses, "0"), Number(LossCurrentTaxPeriod, "0")));
                FormulaTarget.First().SetNumber((Number(CompensatedTaxLosses, "0") > (decimal)0) ? Sum(Number(CompensableTaxLosses, "0"), Number(CompensatedTaxLosses, "0")) : Sum(Number(CompensableTaxLosses, "0"), Number(LossCurrentTaxPeriod, "0")));
                
                if (((Number(CarryOverTaxLosses, "0") != (decimal)0) || (Number(CompensableTaxLosses, "0") != (decimal)0) || (Number(CompensatedTaxLosses, "0") != (decimal)0) || (Number(LossCurrentTaxPeriod, "0") != (decimal)0)))
                {
                    //FORMULA HERE
                    bool test = (Number(CarryOverTaxLosses, "0") == Sum(Number(CompensableTaxLosses, "0"), Number(CompensatedTaxLosses, "0"))) || (Number(CarryOverTaxLosses, "0") == Sum(Number(CompensableTaxLosses, "0"), Number(LossCurrentTaxPeriod, "0")));
                    if (!test)
                    {
                        // MESSAGES
                        AddMessage(new BizTaxErrorDataContract
                        {
                            FileId = _fileId,
                            Id = "f-corp-1411",
                            Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Het totaal vermeld in de rubriek '{Verlies over te brengen naar het volgende belastbare tijdperk}' ({$CarryOverTaxLosses}) is niet juist (= {$CalculatedAmountCarryOverTaxLosses})."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Le total repris dans la rubrique '{Perte à reporter sur la période imposable suivante}' ({$CarryOverTaxLosses}) n'est pas correct (= {$CalculatedAmountCarryOverTaxLosses})."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Der in der Rubrik '{Auf den folgenden Besteuerungszeitraum vorzutragender Verlust}' angegebene Gesamtbetrag ({$CarryOverTaxLosses}) ist unzutreffend (= {$CalculatedAmountCarryOverTaxLosses})."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Het totaal vermeld in de rubriek '{Verlies over te brengen naar het volgende belastbare tijdperk}' ({$CarryOverTaxLosses}) is niet juist (= {$CalculatedAmountCarryOverTaxLosses})."}  
 }
                            ,
                            Fields = new List<string>()
                        }
                        );
                    }
                }
            }

        }

        internal void Calc_f_corp_1412_assertion_set()
        {
            string scenarioId = GetScenarioId("");
            string period = "D";
            // SCENARIO: ""
            // PERIOD: "D"
            var CompensableTaxLosses = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:CompensableTaxLosses" });
            var CompensatedTaxLosses = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:CompensatedTaxLosses" });
            if (CompensableTaxLosses.Count > 0 || CompensatedTaxLosses.Count > 0)
            {
                //CALCULATION HERE
                if (((Number(CompensableTaxLosses, "0") != (decimal)0) || (Number(CompensatedTaxLosses, "0") != (decimal)0)))
                {
                    //FORMULA HERE
                    bool test = Sum(Number(CompensableTaxLosses, "0"), Number(CompensatedTaxLosses, "0")) <= (decimal)0;
                    if (!test)
                    {
                        // MESSAGES
                        AddMessage(new BizTaxErrorDataContract
                        {
                            FileId = _fileId,
                            Id = "f-corp-1412",
                            Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Het bedrag vermeld in de rubriek '{Gecompenseerde verliezen}' ({$CompensatedTaxLosses}) 	is kleiner dan of gelijk aan het bedrag vermeld in de rubriek 	'{Saldo van de compenseerbare vorige verliezen}' ({$CompensableTaxLosses})."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Le montant repris dans la rubrique '{Pertes récupérées}' ({$CompensatedTaxLosses}) 	doit être égal ou inférieur au montant repris dans la rubrique 	'{Solde des pertes antérieures récupérables}' ({$CompensableTaxLosses})."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Der in der Rubrik '{Ausgeglichene Verluste}' angegebene Betrag ({$CompensatedTaxLosses}) 	muss niedriger sein als der in der Rubrik 	'{Restbetrag der ausgleichbaren vorherigen Verluste}' angegebene Betrag oder diesem entsprechen ({$CompensableTaxLosses})."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Het bedrag vermeld in de rubriek '{Gecompenseerde verliezen}' ({$CompensatedTaxLosses}) 	is kleiner dan of gelijk aan het bedrag vermeld in de rubriek 	'{Saldo van de compenseerbare vorige verliezen}' ({$CompensableTaxLosses})."}  
 }
                            ,
                            Fields = new List<string>()
                        }
                        );
                    }
                }
            }

        }

        internal void Calc_f_corp_1414_assertion_set()
        {
            // HAS NO UNBOUND SEQUENCES
            var LossCurrentTaxPeriod = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:LossCurrentTaxPeriod" });
            var CompensatedTaxLosses = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:CompensatedTaxLosses" });
            if (CompensatedTaxLosses.Count > 0 || LossCurrentTaxPeriod.Count > 0)
            {
                //CALCULATION HERE
                if (Number(CompensatedTaxLosses, "0") > (decimal)0)
                {
                    //FORMULA HERE
                    bool test = Number(LossCurrentTaxPeriod, "0") == (decimal)0;
                    if (!test)
                    {
                        // MESSAGES
                        AddMessage(new BizTaxErrorDataContract
                        {
                            FileId = _fileId,
                            Id = "f-corp-1414",
                            Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Indien de rubriek '{Gecompenseerde verliezen}' ({$CompensatedTaxLosses}) 	is ingevuld, dan mag rubriek 	'{Verlies van het belastbare tijdperk}' ({$LossCurrentTaxPeriod}) niet ingevuld worden."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Si la rubrique '{Pertes récupérées}' ({$CompensatedTaxLosses}) 	est remplie, la rubrique '{Perte de la période imposable}' ({$LossCurrentTaxPeriod}) ne peut pas être remplie."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Wenn die Rubrik '{Ausgeglichene Verluste}' ({$CompensatedTaxLosses}) 	ausgefüllt ist, darf die Rubrik 	'{Verlust des Besteuerungszeitraums}' ({$LossCurrentTaxPeriod}) nicht ausgefüllt werden."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Indien de rubriek '{Gecompenseerde verliezen}' ({$CompensatedTaxLosses}) 	is ingevuld, dan mag rubriek 	'{Verlies van het belastbare tijdperk}' ({$LossCurrentTaxPeriod}) niet ingevuld worden."}  
 }
                            ,
                            Fields = new List<string>()
                        }
                        );
                    }
                }
            }

        }

        internal void Calc_f_corp_1415a_assertion_set()
        {
            // HAS NO UNBOUND SEQUENCES
            var Prepayments = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:Prepayments" });
            var PrepaymentFirstQuarter = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:PrepaymentFirstQuarter" });
            var PrepaymentSecondQuarter = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:PrepaymentSecondQuarter" });
            var PrepaymentThirdQuarter = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:PrepaymentThirdQuarter" });
            var PrepaymentFourthQuarter = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:PrepaymentFourthQuarter" });
            if (Prepayments.Count > 0 || PrepaymentFirstQuarter.Count > 0 || PrepaymentSecondQuarter.Count > 0 || PrepaymentThirdQuarter.Count > 0 || PrepaymentFourthQuarter.Count > 0)
            {
                var CalculatedAmountPrepayments = Sum(Number(PrepaymentFirstQuarter, "0"), Number(PrepaymentSecondQuarter, "0"), Number(PrepaymentThirdQuarter, "0"), Number(PrepaymentFourthQuarter, "0"));
                //CALCULATION HERE
                if (Sum(Number(PrepaymentFirstQuarter, "0"), Number(PrepaymentSecondQuarter, "0"), Number(PrepaymentThirdQuarter, "0"), Number(PrepaymentFourthQuarter, "0")) > (decimal)0)
                {
                    //FORMULA HERE
                    bool test = Number(Prepayments, "0") > (decimal)0;
                    if (!test)
                    {
                        // MESSAGES
                        AddMessage(new BizTaxErrorDataContract
                        {
                            FileId = _fileId,
                            Id = "f-corp-1415a",
                            Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Het totaal vermeld in de rubriek '{In aanmerking te nemen voorafbetalingen}' ({$Prepayments}) is niet ingevuld (= {$CalculatedAmountPrepayments})."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Le total repris dans la rubrique '{Versements anticipés à prendre en considération}' ({$Prepayments}) n'est pas rempli (= {$CalculatedAmountPrepayments})."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Der in der Rubrik '{Zu berücksichtigende Vorauszahlungen}' angegebene Gesamtbetrag ({$Prepayments}) ist nicht ausgefüllt (= {$CalculatedAmountPrepayments})."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Het totaal vermeld in de rubriek '{In aanmerking te nemen voorafbetalingen}' ({$Prepayments}) is niet ingevuld (= {$CalculatedAmountPrepayments})."}  
 }
                            ,
                            Fields = new List<string>()
                        }
                        );
                    }
                }
            }

        }

        internal void Calc_f_corp_1416_assertion_set()
        {

        }

        internal void Calc_f_corp_1417_assertion_set()
        {

        }

        internal void Calc_f_corp_1422_assertion_set()
        {
            // HAS NO UNBOUND SEQUENCES
            var BasicTaxableAmountCommonRate = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:BasicTaxableAmountCommonRate" });
            var ExclusionReducedRate = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:ExclusionReducedRate" });
            if (BasicTaxableAmountCommonRate.Count > 0 || ExclusionReducedRate.Count > 0)
            {
                //CALCULATION HERE
                if (Bool(ExclusionReducedRate, "false") == true)
                {
                    //FORMULA HERE
                    bool test = Number(BasicTaxableAmountCommonRate, "0") <= Number(MaximumLimitReducedRateBasicTaxableAmount, "322500");
                    if (!test)
                    {
                        // MESSAGES
                        AddMessage(new BizTaxErrorDataContract
                        {
                            FileId = _fileId,
                            Id = "f-corp-1422",
                            Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Wanneer het belastbaar bedrag groter is dan {$MaximumLimitReducedRateBasicTaxableAmount} EUR, kan de vennootschap geen aanspraak maken op het verminderd tarief."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Si la base imposable est plus élevée que {$MaximumLimitReducedRateBasicTaxableAmount} EUR, la société n'a pas droit au taux réduit."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Wenn die Besteuerungsgrundlage {$MaximumLimitReducedRateBasicTaxableAmount} EUR übersteigt, kann das Unternehmen keinen Anspruch auf den ermäßigten Satz machen."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Wanneer het belastbaar bedrag groter is dan {$MaximumLimitReducedRateBasicTaxableAmount} EUR, kan de vennootschap geen aanspraak maken op het verminderd tarief."}  
 }
                            ,
                            Fields = new List<string>()
                        }
                        );
                    }
                }
            }

        }

        internal void Calc_f_corp_1435_assertion_set()
        {

        }

        internal void Calc_f_corp_1436_assertion_set()
        {

        }

        internal void Calc_f_corp_1437_assertion_set()
        {

        }

        internal void Calc_f_corp_1438_assertion_set()
        {

        }

        internal void Calc_f_corp_1439_assertion_set()
        {

        }

        internal void Calc_f_corp_1440_assertion_set()
        {

        }

        internal void Calc_f_corp_1441_assertion_set()
        {

        }

        internal void Calc_f_corp_1442_assertion_set()
        {

        }

        internal void Calc_f_corp_1443_assertion_set()
        {

        }

        internal void Calc_f_nrcorp_2001_assertion_set()
        {

        }

        internal void Calc_f_nrcorp_2011_assertion_set()
        {

        }

        internal void Calc_f_nrcorp_2101_assertion_set()
        {
            string scenarioId = GetScenarioId("");
            foreach (string period in new string[] { "I-Start", "I-End" })
            {
                // SCENARIO: ""
                // PERIOD: "I-Start"
                // PERIOD: "I-End"
                var TaxableReservesCapital = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:TaxableReservesCapital" });
                var BalanceProvisionProfitOnAccount = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:BalanceProvisionProfitOnAccount" });
                var TaxablePortionRevaluationSurpluses = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:TaxablePortionRevaluationSurpluses" });
                var UnavailableReserves = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:UnavailableReserves" });
                var AvailableReserves = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:AvailableReserves" });
                var AccumulatedProfitsLosses = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:AccumulatedProfitsLosses" });
                var TaxableProvisions = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:TaxableProvisions" });
                var TaxableWriteDownsUndisclosedReserve = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:TaxableWriteDownsUndisclosedReserve" });
                var ExaggeratedDepreciationsUndisclosedReserve = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:ExaggeratedDepreciationsUndisclosedReserve" });
                var OtherUnderestimationsAssetsUndisclosedReserve = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:OtherUnderestimationsAssetsUndisclosedReserve" });
                var OtherOverestimationsLiabilitiesUndisclosedReserve = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:OtherOverestimationsLiabilitiesUndisclosedReserve" });
                var DimOtherReserves = GetElementsByScenDefs(new string[] { "d-ty:DescriptionTypedDimension" }, new string[] { period }, new string[] { "tax-inc:OtherReserves", "tax-inc:OtherTaxableReserves" });
                var TaxableReserves = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:TaxableReserves" });
                var FormulaTarget = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:TaxableReserves" });
                var OtherReserves = GetElementsByScenDefs(new string[] { "d-ty:DescriptionTypedDimension" }, new string[] { period }, new string[] { "tax-inc:OtherReserves" });
                var OtherTaxableReserves = GetElementsByScenDefs(new string[] { "d-ty:DescriptionTypedDimension" }, new string[] { period }, new string[] { "tax-inc:OtherTaxableReserves" });
                if (TaxableReserves.Count > 0 || TaxableReservesCapital.Count > 0 || TaxablePortionRevaluationSurpluses.Count > 0 || BalanceProvisionProfitOnAccount.Count > 0 || UnavailableReserves.Count > 0 || AvailableReserves.Count > 0 || AccumulatedProfitsLosses.Count > 0 || TaxableProvisions.Count > 0 || TaxableWriteDownsUndisclosedReserve.Count > 0 || ExaggeratedDepreciationsUndisclosedReserve.Count > 0 || OtherUnderestimationsAssetsUndisclosedReserve.Count > 0 || OtherOverestimationsLiabilitiesUndisclosedReserve.Count > 0 || DimOtherReserves.Count > 0 || FormulaTarget.Count > 0 || OtherReserves.Count > 0 || OtherTaxableReserves.Count > 0)
                {
                    //CALCULATION HERE
                    if (FormulaTarget.Count == 0)
                    {
                        FormulaTarget = CreateNewCalculationElement(scenarioId, period, "tax-inc:TaxableReserves", new string[] { "" }, "EUR", "INF");
                    }
                    if (FormulaTarget.Count > 1)
                    {
                        throw new Exception("More then one target for FormulaTarget in assertion f-nrcorp-2101");
                    }
                    FormulaTarget.First().Calculated = true;
                    FormulaTarget.First().SetNumber(Sum(Number(TaxableReservesCapital, "0"), Number(BalanceProvisionProfitOnAccount, "0"), Number(TaxablePortionRevaluationSurpluses, "0"), Number(UnavailableReserves, "0"), Number(AvailableReserves, "0"), Number(AccumulatedProfitsLosses, "0"), Number(TaxableProvisions, "0"), Number(TaxableWriteDownsUndisclosedReserve, "0"), Number(ExaggeratedDepreciationsUndisclosedReserve, "0"), Number(OtherUnderestimationsAssetsUndisclosedReserve, "0"), Number(OtherOverestimationsLiabilitiesUndisclosedReserve, "0"), Number(OtherReserves, "0"), Number(OtherTaxableReserves, "0")));
                    //FORMULA HERE
                    bool test = Sum(Number(TaxableReservesCapital, "0"), Number(BalanceProvisionProfitOnAccount, "0"), Number(TaxablePortionRevaluationSurpluses, "0"), Number(UnavailableReserves, "0"), Number(AvailableReserves, "0"), Number(AccumulatedProfitsLosses, "0"), Number(TaxableProvisions, "0"), Number(TaxableWriteDownsUndisclosedReserve, "0"), Number(ExaggeratedDepreciationsUndisclosedReserve, "0"), Number(OtherUnderestimationsAssetsUndisclosedReserve, "0"), Number(OtherOverestimationsLiabilitiesUndisclosedReserve, "0"), Number(DimOtherReserves, "0"), -Number(TaxableReserves, "0")) == (decimal)0;
                    if (!test)
                    {
                        // MESSAGES
                        AddMessage(new BizTaxErrorDataContract
                        {
                            FileId = _fileId,
                            Id = "f-nrcorp-2101",
                            Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Het totaal vermeld in de rubriek '{Belastbare reserves}' ({$TaxableReserves}) is niet juist (= {})."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Le total repris dans la rubrique '{Réserves imposables}' ({$TaxableReserves}) n'est pas correct (= {})."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Der in der Rubrik '{Steuerpflichtige Rücklagen}' angegebene Gesamtbetrag ({$TaxableReserves}) ist unzutreffend (= {})."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Het totaal vermeld in de rubriek '{Belastbare reserves}' ({$TaxableReserves}) is niet juist (= {})."}  
 }
                            ,
                            Fields = new List<string>()
                        }
                        );
                    }
                }
            } // end period loop

        }

        internal void Calc_f_nrcorp_2103_assertion_set()
        {
            string scenarioId = GetScenarioId("");
            string period = "I-Start";
            // SCENARIO: ""
            // PERIOD: "I-Start"
            var TaxableReservesAfterAdjustmentsStart = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:TaxableReservesAfterAdjustments" });
            var TaxableReservesStart = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:TaxableReserves" });
            var AdjustmentsReservesPlus = GetElementsByRef(scenarioId, new string[] { "D" }, new string[] { "tax-inc:CapitalGainsShares", "tax-inc:CapitalGainsSharesReversalPreviousExemptionsAdjustmentReservesPlus", "tax-inc:DefinitiveExemptionTaxShelterAuthorisedAudiovisualWorksAdjustmentReservesPlus", "tax-inc:ExemptionRegionalGrantsCapitalSubsidiesInterestSubsidiesAdjustmentReservesPlus", "tax-inc:FinalExemptionProfitHomologationReorganizationPlanAmicableSettlement", "tax-inc:OtherAdjustmentsReservesPlus" }); ;
            var AdjustmentsReservesMinus = GetElementsByRef(scenarioId, new string[] { "D" }, new string[] { "tax-inc:AdjustmentsReservesMinus" });
            var FormulaTarget = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:TaxableReservesAfterAdjustments" });
            if (TaxableReservesStart.Count > 0 || TaxableReservesAfterAdjustmentsStart.Count > 0 || AdjustmentsReservesPlus.Count > 0 || AdjustmentsReservesMinus.Count > 0 || FormulaTarget.Count > 0)
            {
                var PeriodStartDate = GetNodeByName("tax-inc", "PeriodStartDate");
                var CalculatedAmountTaxableReservesAfterAdjustmentsStart = Sum(Number(TaxableReservesStart, "0"), Number(AdjustmentsReservesPlus, "0"));
                //CALCULATION HERE
                if (FormulaTarget.Count == 0)
                {
                    FormulaTarget = CreateNewCalculationElement(scenarioId, period, "tax-inc:TaxableReservesAfterAdjustments", new string[] { "" }, "EUR", "INF");
                }
                if (FormulaTarget.Count > 1)
                {
                    throw new Exception("More then one target for FormulaTarget in assertion f-nrcorp-2103");
                }
                FormulaTarget.First().Calculated = true;
                FormulaTarget.First().SetNumber(Sum(Number(TaxableReservesStart, "0"), Number(AdjustmentsReservesPlus, "0")));
                //FORMULA HERE
                bool test = Number(TaxableReservesAfterAdjustmentsStart, "0") == Sum(Number(TaxableReservesStart, "0"), Number(AdjustmentsReservesPlus, "0"));
                if (!test)
                {
                    // MESSAGES
                    AddMessage(new BizTaxErrorDataContract
                    {
                        FileId = _fileId,
                        Id = "f-nrcorp-2103",
                        Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Het totaal vermeld in de rubriek '{Belastbare reserves na aanpassing van de begintoestand der reserves}' ({$TaxableReservesAfterAdjustmentsStart}) is niet juist (= {$CalculatedAmountTaxableReservesAfterAdjustmentsStart})."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Le total repris dans la rubrique '{Réserves imposables après adaptation de la situation de début des réserves}' ({$TaxableReservesAfterAdjustmentsStart}) n'est pas correct (= {$CalculatedAmountTaxableReservesAfterAdjustmentsStart})."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Der in der Rubrik '{Steuerpflichtige Rücklagen nach Anpassung des Anfangsstandes der Rücklagen}' angegebene Gesamtbetrag ({$TaxableReservesAfterAdjustmentsStart}) ist unzutreffend (= {$CalculatedAmountTaxableReservesAfterAdjustmentsStart})."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Het totaal vermeld in de rubriek '{Belastbare reserves na aanpassing van de begintoestand der reserves}' ({$TaxableReservesAfterAdjustmentsStart}) is niet juist (= {$CalculatedAmountTaxableReservesAfterAdjustmentsStart})."}  
 }
                        ,
                        Fields = new List<string>()
                    }
                    );
                }
            }

        }

        internal void Calc_f_nrcorp_2120_assertion_set()
        {
            string scenarioId = GetScenarioId("");
            string period = "D";
            // SCENARIO: ""
            // PERIOD: "D"
            var DisallowedExpenses = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:DisallowedExpenses" });
            var NonDeductibleTaxes = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:NonDeductibleTaxes" });
            var NonDeductibleRegionalTaxesDutiesRetributions = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:NonDeductibleRegionalTaxesDutiesRetributions" });
            var NonDeductibleFinesConfiscationsPenaltiesAllKind = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:NonDeductibleFinesConfiscationsPenaltiesAllKind" });
            var NonDeductiblePensionsCapitalEmployerContributionsEmployerPremiums = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:NonDeductiblePensionsCapitalEmployerContributionsEmployerPremiums" });
            var NonDeductibleCarExpensesLossValuesCars = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:NonDeductibleCarExpensesLossValuesCars" });
            var NonDeductibleCarExpensesPartBenefitsAllKind = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:NonDeductibleCarExpensesPartBenefitsAllKind" });
            var NonDeductibleReceptionBusinessGiftsExpenses = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:NonDeductibleReceptionBusinessGiftsExpenses" });
            var NonDeductibleRestaurantExpenses = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:NonDeductibleRestaurantExpenses" });
            var NonDeductibleNonSpecificProfessionalClothsExpenses = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:NonDeductibleNonSpecificProfessionalClothsExpenses" });
            var ExaggeratedInterests = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:ExaggeratedInterests" });
            var NonDeductibleParticularPortionInterestsLoans = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:NonDeductibleParticularPortionInterestsLoans" });
            var AbnormalBenevolentAdvantages = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:AbnormalBenevolentAdvantages" });
            var ProfitTransferBelgiumAbroad = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:ProfitTransferBelgiumAbroad" });
            var NonDeductibleSocialAdvantages = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:NonDeductibleSocialAdvantages" });
            var NonDeductibleAdvantagesLuncheonSportsCultureEcoVouchers = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:NonDeductibleAdvantagesLuncheonSportsCultureEcoVouchers" });
            var Liberalities = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:Liberalities" });
            var WriteDownsLossValuesShares = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:WriteDownsLossValuesShares" });
            var ReversalPreviousExemptions = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:ReversalPreviousExemptions" });
            var EmployeeParticipation = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:EmployeeParticipation" });
            var IndemnityMissingCoupon = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:IndemnityMissingCoupon" });
            var ExpensesTaxShelterAuthorisedAudiovisualWork = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:ExpensesTaxShelterAuthorisedAudiovisualWork" });
            var RegionalPremiumCapitalSubsidiesInterestSubsidies = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:RegionalPremiumCapitalSubsidiesInterestSubsidies" });
            var NonDeductiblePaymentsCertainStates = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:NonDeductiblePaymentsCertainStates" });
            var OtherDisallowedExpenses = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:OtherDisallowedExpenses" });
            var FormulaTarget = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:DisallowedExpenses" });
            if (DisallowedExpenses.Count > 0 || NonDeductibleTaxes.Count > 0 || NonDeductibleRegionalTaxesDutiesRetributions.Count > 0 || NonDeductibleFinesConfiscationsPenaltiesAllKind.Count > 0 || NonDeductiblePensionsCapitalEmployerContributionsEmployerPremiums.Count > 0 || NonDeductibleCarExpensesLossValuesCars.Count > 0 || NonDeductibleCarExpensesPartBenefitsAllKind.Count > 0 || NonDeductibleReceptionBusinessGiftsExpenses.Count > 0 || NonDeductibleRestaurantExpenses.Count > 0 || NonDeductibleNonSpecificProfessionalClothsExpenses.Count > 0 || ExaggeratedInterests.Count > 0 || NonDeductibleParticularPortionInterestsLoans.Count > 0 || AbnormalBenevolentAdvantages.Count > 0 || ProfitTransferBelgiumAbroad.Count > 0 || NonDeductibleSocialAdvantages.Count > 0 || NonDeductibleAdvantagesLuncheonSportsCultureEcoVouchers.Count > 0 || Liberalities.Count > 0 || WriteDownsLossValuesShares.Count > 0 || ReversalPreviousExemptions.Count > 0 || EmployeeParticipation.Count > 0 || IndemnityMissingCoupon.Count > 0 || ExpensesTaxShelterAuthorisedAudiovisualWork.Count > 0 || RegionalPremiumCapitalSubsidiesInterestSubsidies.Count > 0 || NonDeductiblePaymentsCertainStates.Count > 0 || OtherDisallowedExpenses.Count > 0 || FormulaTarget.Count > 0)
            {
                var CalculatedAmountDisallowedExpenses = Sum(Number(NonDeductibleTaxes, "0"), Number(NonDeductibleRegionalTaxesDutiesRetributions, "0"), Number(NonDeductibleFinesConfiscationsPenaltiesAllKind, "0"), Number(NonDeductiblePensionsCapitalEmployerContributionsEmployerPremiums, "0"), Number(NonDeductibleCarExpensesLossValuesCars, "0"), Number(NonDeductibleCarExpensesPartBenefitsAllKind, "0"), Number(NonDeductibleReceptionBusinessGiftsExpenses, "0"), Number(NonDeductibleRestaurantExpenses, "0"), Number(NonDeductibleNonSpecificProfessionalClothsExpenses, "0"), Number(ExaggeratedInterests, "0"), Number(NonDeductibleParticularPortionInterestsLoans, "0"), Number(AbnormalBenevolentAdvantages, "0"), Number(ProfitTransferBelgiumAbroad, "0"), Number(NonDeductibleSocialAdvantages, "0"), Number(NonDeductibleAdvantagesLuncheonSportsCultureEcoVouchers, "0"), Number(Liberalities, "0"), Number(WriteDownsLossValuesShares, "0"), Number(ReversalPreviousExemptions, "0"), Number(EmployeeParticipation, "0"), Number(IndemnityMissingCoupon, "0"), Number(ExpensesTaxShelterAuthorisedAudiovisualWork, "0"), Number(RegionalPremiumCapitalSubsidiesInterestSubsidies, "0"), Number(NonDeductiblePaymentsCertainStates, "0"), Number(OtherDisallowedExpenses, "0"));
                //CALCULATION HERE
                if (FormulaTarget.Count == 0)
                {
                    FormulaTarget = CreateNewCalculationElement(scenarioId, period, "tax-inc:DisallowedExpenses", new string[] { "" }, "EUR", "INF");
                }
                if (FormulaTarget.Count > 1)
                {
                    throw new Exception("More then one target for FormulaTarget in assertion f-nrcorp-2120");
                }
                FormulaTarget.First().Calculated = true;
                FormulaTarget.First().SetNumber(Sum(Number(NonDeductibleTaxes, "0"), Number(NonDeductibleRegionalTaxesDutiesRetributions, "0"), Number(NonDeductibleFinesConfiscationsPenaltiesAllKind, "0"), Number(NonDeductiblePensionsCapitalEmployerContributionsEmployerPremiums, "0"), Number(NonDeductibleCarExpensesLossValuesCars, "0"), Number(NonDeductibleCarExpensesPartBenefitsAllKind, "0"), Number(NonDeductibleReceptionBusinessGiftsExpenses, "0"), Number(NonDeductibleRestaurantExpenses, "0"), Number(NonDeductibleNonSpecificProfessionalClothsExpenses, "0"), Number(ExaggeratedInterests, "0"), Number(NonDeductibleParticularPortionInterestsLoans, "0"), Number(AbnormalBenevolentAdvantages, "0"), Number(ProfitTransferBelgiumAbroad, "0"), Number(NonDeductibleSocialAdvantages, "0"), Number(NonDeductibleAdvantagesLuncheonSportsCultureEcoVouchers, "0"), Number(Liberalities, "0"), Number(WriteDownsLossValuesShares, "0"), Number(ReversalPreviousExemptions, "0"), Number(EmployeeParticipation, "0"), Number(IndemnityMissingCoupon, "0"), Number(ExpensesTaxShelterAuthorisedAudiovisualWork, "0"), Number(RegionalPremiumCapitalSubsidiesInterestSubsidies, "0"), Number(NonDeductiblePaymentsCertainStates, "0"), Number(OtherDisallowedExpenses, "0")));
                //FORMULA HERE
                bool test = Number(DisallowedExpenses, "0") == Sum(Number(NonDeductibleTaxes, "0"), Number(NonDeductibleRegionalTaxesDutiesRetributions, "0"), Number(NonDeductibleFinesConfiscationsPenaltiesAllKind, "0"), Number(NonDeductiblePensionsCapitalEmployerContributionsEmployerPremiums, "0"), Number(NonDeductibleCarExpensesLossValuesCars, "0"), Number(NonDeductibleCarExpensesPartBenefitsAllKind, "0"), Number(NonDeductibleReceptionBusinessGiftsExpenses, "0"), Number(NonDeductibleRestaurantExpenses, "0"), Number(NonDeductibleNonSpecificProfessionalClothsExpenses, "0"), Number(ExaggeratedInterests, "0"), Number(NonDeductibleParticularPortionInterestsLoans, "0"), Number(AbnormalBenevolentAdvantages, "0"), Number(ProfitTransferBelgiumAbroad, "0"), Number(NonDeductibleSocialAdvantages, "0"), Number(NonDeductibleAdvantagesLuncheonSportsCultureEcoVouchers, "0"), Number(Liberalities, "0"), Number(WriteDownsLossValuesShares, "0"), Number(ReversalPreviousExemptions, "0"), Number(EmployeeParticipation, "0"), Number(IndemnityMissingCoupon, "0"), Number(ExpensesTaxShelterAuthorisedAudiovisualWork, "0"), Number(RegionalPremiumCapitalSubsidiesInterestSubsidies, "0"), Number(NonDeductiblePaymentsCertainStates, "0"), Number(OtherDisallowedExpenses, "0"));
                if (!test)
                {
                    // MESSAGES
                    AddMessage(new BizTaxErrorDataContract
                    {
                        FileId = _fileId,
                        Id = "f-nrcorp-2120",
                        Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Het totaal vermeld in de rubriek '{Verworpen uitgaven}' ({$DisallowedExpenses}) is niet juist (= {$CalculatedAmountDisallowedExpenses})."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Le total repris dans la rubrique '{Dépenses non admises}' ({$DisallowedExpenses}) n'est pas correct (= {$CalculatedAmountDisallowedExpenses})."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Der in der Rubrik '{Nicht zugelassene Ausgaben}' angegebene Gesamtbetrag ({$DisallowedExpenses}) ist unzutreffend (= {$CalculatedAmountDisallowedExpenses})."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Het totaal vermeld in de rubriek '{Verworpen uitgaven}' ({$DisallowedExpenses}) is niet juist (= {$CalculatedAmountDisallowedExpenses})."}  
 }
                        ,
                        Fields = new List<string>()
                    }
                    );
                }
            }

        }

        internal void Calc_f_nrcorp_2201_assertion_set()
        {
            string scenarioId = GetScenarioId("");
            string period = "D";
            // SCENARIO: ""
            // PERIOD: "D"
            var FiscalResult = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:FiscalResult" });
            var TaxableReservedProfit = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:TaxableReservedProfit" });
            var DisallowedExpenses = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:DisallowedExpenses" });
            var ImmovablePropertyIncome = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:ImmovablePropertyIncome" });
            var IncomePartnerCompaniesJointVenturesAssociationsNoLegalPersonality = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:IncomePartnerCompaniesJointVenturesAssociationsNoLegalPersonality" });
            var FormulaTarget = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:FiscalResult" });
            if (FiscalResult.Count > 0 || TaxableReservedProfit.Count > 0 || DisallowedExpenses.Count > 0 || ImmovablePropertyIncome.Count > 0 || IncomePartnerCompaniesJointVenturesAssociationsNoLegalPersonality.Count > 0 || FormulaTarget.Count > 0)
            {
                var CalculatedAmountFiscalResult = Sum(Number(TaxableReservedProfit, "0"), Number(DisallowedExpenses, "0"), Number(ImmovablePropertyIncome, "0"), Number(IncomePartnerCompaniesJointVenturesAssociationsNoLegalPersonality, "0"));
                //CALCULATION HERE
                if (FormulaTarget.Count == 0)
                {
                    FormulaTarget = CreateNewCalculationElement(scenarioId, period, "tax-inc:FiscalResult", new string[] { "" }, "EUR", "INF");
                }
                if (FormulaTarget.Count > 1)
                {
                    throw new Exception("More then one target for FormulaTarget in assertion f-nrcorp-2201");
                }
                FormulaTarget.First().Calculated = true;
                FormulaTarget.First().SetNumber(Sum(Number(TaxableReservedProfit, "0"), Number(DisallowedExpenses, "0"), Number(ImmovablePropertyIncome, "0"), Number(IncomePartnerCompaniesJointVenturesAssociationsNoLegalPersonality, "0")));
                //FORMULA HERE
                bool test = Number(FiscalResult, "0") == Sum(Number(TaxableReservedProfit, "0"), Number(DisallowedExpenses, "0"), Number(ImmovablePropertyIncome, "0"), Number(IncomePartnerCompaniesJointVenturesAssociationsNoLegalPersonality, "0"));
                if (!test)
                {
                    // MESSAGES
                    AddMessage(new BizTaxErrorDataContract
                    {
                        FileId = _fileId,
                        Id = "f-nrcorp-2201",
                        Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Het totaal vermeld in de rubriek '{Resultaat van het belastbare tijdperk}' ({$FiscalResult}) is niet juist (= {$CalculatedAmountFiscalResult})."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Le total repris dans la rubrique '{Résultat de la période imposable}' ({$FiscalResult}) n'est pas correct (= {$CalculatedAmountFiscalResult})."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Der in der Rubrik '{Ergebnis des Besteuerungszeitraums}' angegebene Gesamtbetrag ({$FiscalResult}) ist unzutreffend (= {$CalculatedAmountFiscalResult})."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Het totaal vermeld in de rubriek '{Resultaat van het belastbare tijdperk}' ({$FiscalResult}) is niet juist (= {$CalculatedAmountFiscalResult})."}  
 }
                        ,
                        Fields = new List<string>()
                    }
                    );
                }
            }

        }

        internal virtual void Calc_f_nrcorp_1211_assertion_set()
        {
            string scenarioId = GetScenarioId("");
            string period = "D";
            // SCENARIO: ""
            // PERIOD: "D"
            var RemainingFiscalResultBeforeOriginDistribution = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:RemainingFiscalResultBeforeOriginDistribution" });
            var LossCurrentTaxPeriod = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:LossCurrentTaxPeriod" });
            if (RemainingFiscalResultBeforeOriginDistribution.Count > 0 || LossCurrentTaxPeriod.Count > 0)
            {
                //CALCULATION HERE
                if ((Number(RemainingFiscalResultBeforeOriginDistribution, "0") < (decimal)0) || (Number(LossCurrentTaxPeriod, "0") < (decimal)0))
                {
                    //FORMULA HERE
                    bool test = String(RemainingFiscalResultBeforeOriginDistribution, "0") == String(LossCurrentTaxPeriod, "0");
                    if (!test)
                    {
                        // MESSAGES
                        AddMessage(new BizTaxErrorDataContract
                        {
                            FileId = _fileId,
                            Id = "f-nrcorp-1211",
                            Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Is het bedrag vermeld in de rubriek '{Resterend resultaat}' ({$RemainingFiscalResultBeforeOriginDistribution}) negatief, dan is dit bedrag gelijk aan het bedrag vermeld in de rubriek 	'{Verlies van het belastbare tijdperk}' ({$LossCurrentTaxPeriod})."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Si le montant repris dans la rubrique '{Résultat subsistant}' ({$RemainingFiscalResultBeforeOriginDistribution}) est négatif, ce montant doit être égal au montant repris dans la rubrique 	'{Perte de la période imposable}' ({$LossCurrentTaxPeriod})."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Wenn der in der Rubrik '{Verbleibendes Ergebnis}' angegebene Betrag ({$RemainingFiscalResultBeforeOriginDistribution}) negativ ist, entspricht dieser Betrag dem in der Rubrik 	'{Verlust des Besteuerungszeitraums}'angegebenen Betrag ({$LossCurrentTaxPeriod})."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Is het bedrag vermeld in de rubriek '{Resterend resultaat}' ({$RemainingFiscalResultBeforeOriginDistribution}) negatief, dan is dit bedrag gelijk aan het bedrag vermeld in de rubriek 	'{Verlies van het belastbare tijdperk}' ({$LossCurrentTaxPeriod})."}  
 }
                            ,
                            Fields = new List<string>()
                        }
                        );
                    }
                }
            }

        }

        internal void Calc_f_nrcorp_2212_assertion_set()
        {
            string scenarioId = GetScenarioId("");
            string period = "D";
            // SCENARIO: ""
            // PERIOD: "D"
            var MiscellaneousExemptions = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:MiscellaneousExemptions" });
            var PEExemptIncomeMovableAssets = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:PEExemptIncomeMovableAssets" });
            var DeductionPatentsIncome = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:DeductionPatentsIncome" });
            var AllowanceCorporateEquity = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:AllowanceCorporateEquity" });
            var CompensatedTaxLosses = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:CompensatedTaxLosses" });
            var AllowanceInvestmentDeduction = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:AllowanceInvestmentDeduction" });
            var RemainingFiscalResultBeforeOriginDistribution = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:RemainingFiscalResultBeforeOriginDistribution" });
            if (MiscellaneousExemptions.Count > 0 || PEExemptIncomeMovableAssets.Count > 0 || DeductionPatentsIncome.Count > 0 || AllowanceCorporateEquity.Count > 0 || CompensatedTaxLosses.Count > 0 || AllowanceInvestmentDeduction.Count > 0 || RemainingFiscalResultBeforeOriginDistribution.Count > 0)
            {
                //CALCULATION HERE
                if (Number(RemainingFiscalResultBeforeOriginDistribution, "0") < (decimal)0)
                {
                    //FORMULA HERE
                    bool test = Sum(Number(MiscellaneousExemptions, "0"), Number(PEExemptIncomeMovableAssets, "0"), Number(DeductionPatentsIncome, "0"), Number(AllowanceCorporateEquity, "0"), Number(CompensatedTaxLosses, "0"), Number(AllowanceInvestmentDeduction, "0")) == (decimal)0;
                    if (!test)
                    {
                        // MESSAGES
                        AddMessage(new BizTaxErrorDataContract
                        {
                            FileId = _fileId,
                            Id = "f-nrcorp-2212",
                            Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Indien het bedrag vermeld in de rubriek 	'{Resterend resultaat}' ({$RemainingFiscalResultBeforeOriginDistribution}) 		negatief of nul is, dan zijn geen aftrekken mogelijk."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Si le montant repris dans la rubrique 	'{Résultat subsistant}' ({$RemainingFiscalResultBeforeOriginDistribution}) 		est négatif ou égal à zéro, il n'y pas de déductions possibles."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Wenn der in der Rubrik 	'{Verbleibendes Ergebnis} ({$RemainingFiscalResultBeforeOriginDistribution}) 	angegebene Betrag negativ oder Null ist, sind keine Abzüge erlaubt."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Indien het bedrag vermeld in de rubriek 	'{Resterend resultaat}' ({$RemainingFiscalResultBeforeOriginDistribution}) 		negatief of nul is, dan zijn geen aftrekken mogelijk."}  
 }
                            ,
                            Fields = new List<string>()
                        }
                        );
                    }
                }
            }

        }

        internal void Calc_f_nrcorp_2216_assertion_set()
        {
            string scenarioId = GetScenarioId("");
            string period = "D";
            // SCENARIO: ""
            // PERIOD: "D"
            var MiscellaneousExemptions = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:MiscellaneousExemptions" });
            var DeductibleMiscellaneousExemptions = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:DeductibleMiscellaneousExemptions" });
            if (DeductibleMiscellaneousExemptions.Count > 0 || MiscellaneousExemptions.Count > 0)
            {
                //CALCULATION HERE
                //FORMULA HERE
                bool test = Number(MiscellaneousExemptions, "0") <= Number(DeductibleMiscellaneousExemptions, "0");
                if (!test)
                {
                    // MESSAGES
                    AddMessage(new BizTaxErrorDataContract
                    {
                        FileId = _fileId,
                        Id = "f-nrcorp-2216",
                        Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Het totaal van de bedragen vermeld in de rubriek 	'{Niet-belastbare bestanddelen} (= {$MiscellaneousExemptions}) 	is groter dan het totaal vermeld in het vak	'{Niet-belastbare bestanddelen}' ({$DeductibleMiscellaneousExemptions})."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Le total des montants complétés dans la rubrique 	'{Eléments non imposables} (= {$MiscellaneousExemptions}) 	est plus élevé que le total repris dans le cadre 	'{Eléments non imposables}' ({$DeductibleMiscellaneousExemptions})."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Die Summe der in der Rubrik 	'{Nicht steuerpflichtige Bestandteile} (= {}) 	angegebenen Beträge übersteigt den im Feld 	'{Nicht steuerpflichtige Bestandteile}' ({$DeductibleMiscellaneousExemptions})	angegebenen Gesamtbetrag."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Het totaal van de bedragen vermeld in de rubriek 	'{Niet-belastbare bestanddelen} (= {$MiscellaneousExemptions}) 	is groter dan het totaal vermeld in het vak	'{Niet-belastbare bestanddelen}' ({$DeductibleMiscellaneousExemptions})."}  
 }
                        ,
                        Fields = new List<string>()
                    }
                    );
                }
            }

        }

        internal void Calc_f_nrcorp_2218_assertion_set()
        {
            // HAS NO UNBOUND SEQUENCES
            var PEExemptIncomeMovableAssets = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:PEExemptIncomeMovableAssets" });
            var RemainingFiscalResultBeforeOriginDistribution = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:RemainingFiscalResultBeforeOriginDistribution" });
            var MiscellaneousExemptions = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:MiscellaneousExemptions" });
            if (PEExemptIncomeMovableAssets.Count > 0 || RemainingFiscalResultBeforeOriginDistribution.Count > 0 || MiscellaneousExemptions.Count > 0)
            {
                var BalanceRemainingResult_AfterMiscellaneousExemptions = Sum(Number(RemainingFiscalResultBeforeOriginDistribution, "0"), -Number(MiscellaneousExemptions, "0"));
                //CALCULATION HERE
                if ((Number(PEExemptIncomeMovableAssets, "0") > (decimal)0) && (Number(BalanceRemainingResult_AfterMiscellaneousExemptions) > (decimal)0))
                {
                    //FORMULA HERE
                    bool test = Number(PEExemptIncomeMovableAssets, "0") <= Number(BalanceRemainingResult_AfterMiscellaneousExemptions);
                    if (!test)
                    {
                        // MESSAGES
                        AddMessage(new BizTaxErrorDataContract
                        {
                            FileId = _fileId,
                            Id = "f-nrcorp-2218",
                            Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Het bedrag vermeld in de rubriek 	'{Definitief belaste inkomsten en vrijgestelde roerende inkomsten}' 	mag slechts in mindering gebracht worden van de resterende winst na de vorige bewerking ({$BalanceRemainingResult_AfterMiscellaneousExemptions}) 	en zonder deze resterende winst te overtreffen."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Le montant repris dans la rubrique 	'{Revenus définitivement taxés et revenus mobiliers exonérés}' 	peut être déduit du bénéfice subsistant belge après la déduction précédente ({$BalanceRemainingResult_AfterMiscellaneousExemptions}) 	en ne dépassant pas ce bénéfice subsistant."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Der in der Rubrik 	'{Endgültig besteuerte Einkünfte und steuerfreie Mobilieneinkünfte}' 	angegebene Betrag darf nur nach dem letzten Vorgang in Abzug gebracht werden und darf diese verbleibenden Gewinne ({$BalanceRemainingResult_AfterMiscellaneousExemptions}) 	nicht überschreiten."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Het bedrag vermeld in de rubriek 	'{Definitief belaste inkomsten en vrijgestelde roerende inkomsten}' 	mag slechts in mindering gebracht worden van de resterende winst na de vorige bewerking ({$BalanceRemainingResult_AfterMiscellaneousExemptions}) 	en zonder deze resterende winst te overtreffen."}  
 }
                            ,
                            Fields = new List<string>()
                        }
                        );
                    }
                }
            }

        }

        internal void Calc_f_nrcorp_2224_assertion_set()
        {
            // HAS NO UNBOUND SEQUENCES
            var DeductionPatentsIncome = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:DeductionPatentsIncome" });
            var RemainingFiscalResultBeforeOriginDistribution = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:RemainingFiscalResultBeforeOriginDistribution" });
            var MiscellaneousExemptions = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:MiscellaneousExemptions" });
            var PEExemptIncomeMovableAssets = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:PEExemptIncomeMovableAssets" });
            if (DeductionPatentsIncome.Count > 0 || RemainingFiscalResultBeforeOriginDistribution.Count > 0 || MiscellaneousExemptions.Count > 0 || PEExemptIncomeMovableAssets.Count > 0)
            {
                var BalanceRemainingResult_AfterPEExemptIncomeMovableAssets = Sum(Number(RemainingFiscalResultBeforeOriginDistribution, "0"), -Number(MiscellaneousExemptions, "0"), -Number(PEExemptIncomeMovableAssets, "0"));
                //CALCULATION HERE
                if ((Number(DeductionPatentsIncome, "0") > (decimal)0) && (Number(BalanceRemainingResult_AfterPEExemptIncomeMovableAssets) > (decimal)0))
                {
                    //FORMULA HERE
                    bool test = Number(DeductionPatentsIncome, "0") <= Number(BalanceRemainingResult_AfterPEExemptIncomeMovableAssets);
                    if (!test)
                    {
                        // MESSAGES
                        AddMessage(new BizTaxErrorDataContract
                        {
                            FileId = _fileId,
                            Id = "f-nrcorp-2224",
                            Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Het bedrag vermeld in de rubriek 	'{Aftrek voor octrooi-inkomsten}' 	mag slechts in mindering gebracht worden van de resterende winst na de vorige bewerking ({$BalanceRemainingResult_AfterPEExemptIncomeMovableAssets}) 	en zonder deze resterende winst te overtreffen."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Le montant repris dans la rubrique 	'{Déduction pour revenus de brevets}' 	peut être déduit du bénéfice subsistant belge après la déduction précédente ({$BalanceRemainingResult_AfterPEExemptIncomeMovableAssets}) 	en ne dépassant pas ce bénéfice subsistant."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Der in der Rubrik 	'{Abzug für Einkünfte aus Patenten}' 	angegebene Betrag darf nur nach dem letzten Vorgang in Abzug gebracht werden und darf diese verbleibenden Gewinne ({$BalanceRemainingResult_AfterPEExemptIncomeMovableAssets}) 	nicht überschreiten."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Het bedrag vermeld in de rubriek 	'{Aftrek voor octrooi-inkomsten}' 	mag slechts in mindering gebracht worden van de resterende winst na de vorige bewerking ({$BalanceRemainingResult_AfterPEExemptIncomeMovableAssets}) 	en zonder deze resterende winst te overtreffen."}  
 }
                            ,
                            Fields = new List<string>()
                        }
                        );
                    }
                }
            }

        }

        internal void Calc_f_nrcorp_2225_assertion_set()
        {
            string scenarioId = GetScenarioId("");
            string period = "D";
            // SCENARIO: ""
            // PERIOD: "D"
            var DeductionPatentsIncome = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:DeductionPatentsIncome" });
            var DeductibleDeductionPatentsIncome = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:DeductibleDeductionPatentsIncome" });
            if (DeductibleDeductionPatentsIncome.Count > 0 || DeductionPatentsIncome.Count > 0)
            {
                //CALCULATION HERE
                //FORMULA HERE
                bool test = Number(DeductionPatentsIncome, "0") <= Number(DeductibleDeductionPatentsIncome, "0");
                if (!test)
                {
                    // MESSAGES
                    AddMessage(new BizTaxErrorDataContract
                    {
                        FileId = _fileId,
                        Id = "f-nrcorp-2225",
                        Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Indien de rubriek '{Aftrek voor octrooi-inkomsten}' ({$DeductibleDeductionPatentsIncome}) is ingevuld, dan is het bedrag vermeld in de rubriek 	'{Aftrek voor octrooi-inkomsten} ({$DeductionPatentsIncome}) kleiner of hieraan gelijk."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Si la rubrique '{Déduction pour revenus de brevets}' ({$DeductibleDeductionPatentsIncome}) est remplie, le montant repris dans la rubrique 	'{Déduction pour revenus de brevets}' ({$DeductionPatentsIncome}) y est inférieur ou égal."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Wenn die Rubrik '{Abzug für Einkünfte aus Patenten}' ({$DeductibleDeductionPatentsIncome}) ausgefüllt ist, entspricht dieser Betrag die Rubrik 	'{Abzug für Einkünfte aus Patenten} ({$DeductionPatentsIncome})."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Indien de rubriek '{Aftrek voor octrooi-inkomsten}' ({$DeductibleDeductionPatentsIncome}) is ingevuld, dan is het bedrag vermeld in de rubriek 	'{Aftrek voor octrooi-inkomsten} ({$DeductionPatentsIncome}) kleiner of hieraan gelijk."}  
 }
                        ,
                        Fields = new List<string>()
                    }
                    );
                }
            }

        }

        internal void Calc_f_nrcorp_2226_assertion_set()
        {
            // HAS NO UNBOUND SEQUENCES
            var AllowanceCorporateEquity = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:AllowanceCorporateEquity" });
            var RemainingFiscalResultBeforeOriginDistribution = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:RemainingFiscalResultBeforeOriginDistribution" });
            var MiscellaneousExemptions = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:MiscellaneousExemptions" });
            var PEExemptIncomeMovableAssets = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:PEExemptIncomeMovableAssets" });
            var DeductionPatentsIncome = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:DeductionPatentsIncome" });
            if (AllowanceCorporateEquity.Count > 0 || RemainingFiscalResultBeforeOriginDistribution.Count > 0 || MiscellaneousExemptions.Count > 0 || PEExemptIncomeMovableAssets.Count > 0 || DeductionPatentsIncome.Count > 0)
            {
                var BalanceRemainingResult_AfterDeductionPatentsIncome = Sum(Number(RemainingFiscalResultBeforeOriginDistribution, "0"), -Number(MiscellaneousExemptions, "0"), -Number(PEExemptIncomeMovableAssets, "0"), -Number(DeductionPatentsIncome, "0"));
                //CALCULATION HERE
                if ((Number(AllowanceCorporateEquity, "0") > (decimal)0) && (Number(BalanceRemainingResult_AfterDeductionPatentsIncome) > (decimal)0))
                {
                    //FORMULA HERE
                    bool test = Number(AllowanceCorporateEquity, "0") <= Number(BalanceRemainingResult_AfterDeductionPatentsIncome);
                    if (!test)
                    {
                        // MESSAGES
                        AddMessage(new BizTaxErrorDataContract
                        {
                            FileId = _fileId,
                            Id = "f-nrcorp-2226",
                            Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Het bedrag vermeld in de rubriek 	'{Aftrek voor risicokapitaal}' 	mag slechts in mindering gebracht worden van de resterende winst na de vorige bewerking ({$BalanceRemainingResult_AfterDeductionPatentsIncome}) 	en zonder deze resterende winst te overtreffen."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Le montant repris dans la rubrique 	'{Déduction pour capital à risque}' 	peut être déduit du bénéfice subsistant belge après la déduction précédente ({$BalanceRemainingResult_AfterDeductionPatentsIncome}) 	en ne dépassant pas ce bénéfice subsistant."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Der in der Rubrik 	'{Abzug für Risikokapital}' 	angegebene Betrag darf nur nach dem letzten Vorgang in Abzug gebracht werden und darf diese verbleibenden Gewinne ({$BalanceRemainingResult_AfterDeductionPatentsIncome}) 	nicht überschreiten."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Het bedrag vermeld in de rubriek 	'{Aftrek voor risicokapitaal}' 	mag slechts in mindering gebracht worden van de resterende winst na de vorige bewerking ({$BalanceRemainingResult_AfterDeductionPatentsIncome}) 	en zonder deze resterende winst te overtreffen."}  
 }
                            ,
                            Fields = new List<string>()
                        }
                        );
                    }
                }
            }

        }

        internal void Calc_f_nrcorp_2229_assertion_set()
        {
            // HAS NO UNBOUND SEQUENCES
            var CompensatedTaxLosses = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:CompensatedTaxLosses" });
            var RemainingFiscalResultBeforeOriginDistribution = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:RemainingFiscalResultBeforeOriginDistribution" });
            var MiscellaneousExemptions = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:MiscellaneousExemptions" });
            var PEExemptIncomeMovableAssets = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:PEExemptIncomeMovableAssets" });
            var DeductionPatentsIncome = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:DeductionPatentsIncome" });
            var AllowanceCorporateEquity = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:AllowanceCorporateEquity" });
            if (RemainingFiscalResultBeforeOriginDistribution.Count > 0 || MiscellaneousExemptions.Count > 0 || PEExemptIncomeMovableAssets.Count > 0 || DeductionPatentsIncome.Count > 0 || AllowanceCorporateEquity.Count > 0 || CompensatedTaxLosses.Count > 0)
            {
                var generalVariable_BalanceRemainingResult_AfterAllowanceCorporateEquity = Sum(Number(RemainingFiscalResultBeforeOriginDistribution, "0"), -Number(MiscellaneousExemptions, "0"), -Number(PEExemptIncomeMovableAssets, "0"), -Number(DeductionPatentsIncome, "0"), -Number(AllowanceCorporateEquity, "0"));
                //CALCULATION HERE
                if ((Number(CompensatedTaxLosses, "0") > (decimal)0) && (Number(generalVariable_BalanceRemainingResult_AfterAllowanceCorporateEquity) > (decimal)0))
                {
                    //FORMULA HERE
                    bool test = Number(CompensatedTaxLosses, "0") <= Number(generalVariable_BalanceRemainingResult_AfterAllowanceCorporateEquity);
                    if (!test)
                    {
                        // MESSAGES
                        AddMessage(new BizTaxErrorDataContract
                        {
                            FileId = _fileId,
                            Id = "f-nrcorp-2229",
                            Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Het bedrag vermeld in de rubriek 	'{Vorige verliezen}' 	mag slechts in mindering gebracht worden van de resterende winst na de vorige bewerking ({$generalVariable_BalanceRemainingResult_AfterAllowanceCorporateEquity}) 	en zonder deze resterende winst te overtreffen. "}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Le montant repris dans la rubrique 	'{Pertes antérieures}' 	peut être déduit du bénéfice subsistant belge après la déduction précédente ({$generalVariable_BalanceRemainingResult_AfterAllowanceCorporateEquity}) 	en ne dépassant pas ce bénéfice subsistant."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Der in der Rubrik 	'{Vorherige Verluste}' 	angegebene Betrag darf nur nach dem letzten Vorgang in Abzug gebracht werden und darf diese verbleibenden Gewinne ({$generalVariable_BalanceRemainingResult_AfterAllowanceCorporateEquity}) 	nicht überschreiten."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Het bedrag vermeld in de rubriek 	'{Vorige verliezen}' 	mag slechts in mindering gebracht worden van de resterende winst na de vorige bewerking ({$generalVariable_BalanceRemainingResult_AfterAllowanceCorporateEquity}) 	en zonder deze resterende winst te overtreffen. "}  
 }
                            ,
                            Fields = new List<string>()
                        }
                        );
                    }
                }
            }

        }

        internal void Calc_f_nrcorp_2230_assertion_set()
        {
            // HAS NO UNBOUND SEQUENCES
            var AllowanceInvestmentDeduction = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:AllowanceInvestmentDeduction" });
            var RemainingFiscalResultBeforeOriginDistribution = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:RemainingFiscalResultBeforeOriginDistribution" });
            var MiscellaneousExemptions = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:MiscellaneousExemptions" });
            var PEExemptIncomeMovableAssets = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:PEExemptIncomeMovableAssets" });
            var DeductionPatentsIncome = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:DeductionPatentsIncome" });
            var AllowanceCorporateEquity = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:AllowanceCorporateEquity" });
            var CompensatedTaxLosses = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:CompensatedTaxLosses" });
            if (RemainingFiscalResultBeforeOriginDistribution.Count > 0 || MiscellaneousExemptions.Count > 0 || PEExemptIncomeMovableAssets.Count > 0 || DeductionPatentsIncome.Count > 0 || AllowanceCorporateEquity.Count > 0 || CompensatedTaxLosses.Count > 0 || AllowanceInvestmentDeduction.Count > 0)
            {
                var generalVariable_BalanceRemainingResult_AfterCompensatedTaxLosses = Sum(Number(RemainingFiscalResultBeforeOriginDistribution, "0"), -Number(MiscellaneousExemptions, "0"), -Number(PEExemptIncomeMovableAssets, "0"), -Number(DeductionPatentsIncome, "0"), -Number(AllowanceCorporateEquity, "0"), -Number(CompensatedTaxLosses, "0"));
                //CALCULATION HERE
                if ((Number(AllowanceInvestmentDeduction, "0") > (decimal)0) && (Number(generalVariable_BalanceRemainingResult_AfterCompensatedTaxLosses) > (decimal)0))
                {
                    //FORMULA HERE
                    bool test = (Number(AllowanceInvestmentDeduction, "0") <= Number(generalVariable_BalanceRemainingResult_AfterCompensatedTaxLosses));
                    if (!test)
                    {
                        // MESSAGES
                        AddMessage(new BizTaxErrorDataContract
                        {
                            FileId = _fileId,
                            Id = "f-nrcorp-2230",
                            Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Het bedrag vermeld in de rubriek 	'{Aftrek voor octrooi-inkomsten}' 	mag slechts in mindering gebracht worden van de resterende winst na de vorige bewerking ({$generalVariable_BalanceRemainingResult_AfterCompensatedTaxLosses}) 	en zonder deze resterende winst te overtreffen."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Le montant repris dans la rubrique 	'{Déduction pour revenus de brevets}' 	peut être déduit du bénéfice subsistant après la déduction précédente ({$generalVariable_BalanceRemainingResult_AfterCompensatedTaxLosses}) 	en ne dépassant pas ce bénéfice subsistant."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Der in der Rubrik 	'{Abzug für Einkünfte aus Patenten}' 	angegebene Betrag darf nur nach dem letzten Vorgang in Abzug gebracht werden und darf diese verbleibenden Gewinne ({$generalVariable_BalanceRemainingResult_AfterCompensatedTaxLosses}) 	nicht überschreiten."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Het bedrag vermeld in de rubriek 	'{Aftrek voor octrooi-inkomsten}' 	mag slechts in mindering gebracht worden van de resterende winst na de vorige bewerking ({$generalVariable_BalanceRemainingResult_AfterCompensatedTaxLosses}) 	en zonder deze resterende winst te overtreffen."}  
 }
                            ,
                            Fields = new List<string>()
                        }
                        );
                    }
                }
            }

        }

        internal void Calc_f_nrcorp_2231_assertion_set()
        {
            string scenarioId = GetScenarioId("");
            string period = "D";
            // SCENARIO: ""
            // PERIOD: "D"
            var AllowanceInvestmentDeduction = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:AllowanceInvestmentDeduction" });
            var DeductibleAllowanceInvestmentDeductionIncompatibleTaxCreditResearchDevelopment = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:DeductibleAllowanceInvestmentDeductionIncompatibleTaxCreditResearchDevelopment" });
            var DeductibleAllowanceInvestmentDeductionCompatibleTaxCreditResearchDevelopment = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:DeductibleAllowanceInvestmentDeductionCompatibleTaxCreditResearchDevelopment" });
            if (AllowanceInvestmentDeduction.Count > 0 || DeductibleAllowanceInvestmentDeductionIncompatibleTaxCreditResearchDevelopment.Count > 0 || DeductibleAllowanceInvestmentDeductionCompatibleTaxCreditResearchDevelopment.Count > 0)
            {
                //CALCULATION HERE
                //FORMULA HERE
                bool test = (String(AllowanceInvestmentDeduction, "0") == String(DeductibleAllowanceInvestmentDeductionIncompatibleTaxCreditResearchDevelopment, "0")) || (String(AllowanceInvestmentDeduction, "0") == String(DeductibleAllowanceInvestmentDeductionCompatibleTaxCreditResearchDevelopment, "0"));
                if (!test)
                {
                    // MESSAGES
                    AddMessage(new BizTaxErrorDataContract
                    {
                        FileId = _fileId,
                        Id = "f-nrcorp-2231",
                        Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Het bedrag vermeld in de rubriek '{Investeringsaftrek}' moet gelijk zijn aan dit ingevuld in de bijlage in de rubriek '{Investeringsaftrek aftrekbaar voor het belastbare tijdperk}' ({$DeductibleAllowanceInvestmentDeductionIncompatibleTaxCreditResearchDevelopment} of {$DeductibleAllowanceInvestmentDeductionCompatibleTaxCreditResearchDevelopment})."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Le montant repris dans la rubrique '{Déduction pour investissement}' doit être égal à celui rempli dans l'annexe dans la rubrique '{Déduction pour investissement déductible pour la période imposable}' ({$DeductibleAllowanceInvestmentDeductionIncompatibleTaxCreditResearchDevelopment} ou {$DeductibleAllowanceInvestmentDeductionCompatibleTaxCreditResearchDevelopment})."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Wenn die Rubrik '{Investitionsabzug}' ausgefüllt wurde, darf der in der Rubrik '{Für den Besteuerungszeitraum abziehbarer Investitionsabzug}' ({$DeductibleAllowanceInvestmentDeductionIncompatibleTaxCreditResearchDevelopment} oder {$DeductibleAllowanceInvestmentDeductionCompatibleTaxCreditResearchDevelopment}) angegebene Betrag sich maximal auf diesen Betrag belaufen."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Het bedrag vermeld in de rubriek '{Investeringsaftrek}' moet gelijk zijn aan dit ingevuld in de bijlage in de rubriek '{Investeringsaftrek aftrekbaar voor het belastbare tijdperk}' ({$DeductibleAllowanceInvestmentDeductionIncompatibleTaxCreditResearchDevelopment} of {$DeductibleAllowanceInvestmentDeductionCompatibleTaxCreditResearchDevelopment})."}  
 }
                        ,
                        Fields = new List<string>()
                    }
                    );
                }
            }

        }

        internal void Calc_f_nrcorp_2232_assertion_set()
        {
            // HAS NO UNBOUND SEQUENCES
            var RemainingFiscalProfitCommonRate = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:RemainingFiscalProfitCommonRate" });
            var RemainingFiscalResultBeforeOriginDistribution = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:RemainingFiscalResultBeforeOriginDistribution" });
            var MiscellaneousExemptions = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:MiscellaneousExemptions" });
            var PEExemptIncomeMovableAssets = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:PEExemptIncomeMovableAssets" });
            var DeductionPatentsIncome = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:DeductionPatentsIncome" });
            var AllowanceCorporateEquity = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:AllowanceCorporateEquity" });
            var CompensatedTaxLosses = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:CompensatedTaxLosses" });
            var AllowanceInvestmentDeduction = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:AllowanceInvestmentDeduction" });
            var FormulaTarget = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:RemainingFiscalProfitCommonRate" });
            if (RemainingFiscalProfitCommonRate.Count > 0 || RemainingFiscalResultBeforeOriginDistribution.Count > 0 || MiscellaneousExemptions.Count > 0 || PEExemptIncomeMovableAssets.Count > 0 || DeductionPatentsIncome.Count > 0 || AllowanceCorporateEquity.Count > 0 || CompensatedTaxLosses.Count > 0 || AllowanceInvestmentDeduction.Count > 0 || FormulaTarget.Count > 0)
            {
                var generalVariable_BalanceRemainingResult_AfterAllowanceInvestmentDeduction = Max(Sum(Number(RemainingFiscalResultBeforeOriginDistribution, "0"), -Number(MiscellaneousExemptions, "0"), -Number(PEExemptIncomeMovableAssets, "0"), -Number(DeductionPatentsIncome, "0"), -Number(AllowanceCorporateEquity, "0"), -Number(CompensatedTaxLosses, "0"), -Number(AllowanceInvestmentDeduction, "0")), (decimal)0);
                //CALCULATION HERE
                if (FormulaTarget.Count == 0)
                {
                    FormulaTarget = CreateNewCalculationElement(null, new string[] { "D" }, "tax-inc:RemainingFiscalProfitCommonRate", new string[] { "" }, "EUR", "INF");
                }
                if (FormulaTarget.Count > 1)
                {
                    throw new Exception("More then one target for FormulaTarget in assertion f-nrcorp-2232");
                }
                FormulaTarget.First().Calculated = true;
                FormulaTarget.First().SetNumber(Number(generalVariable_BalanceRemainingResult_AfterAllowanceInvestmentDeduction));
                if ((Number(RemainingFiscalResultBeforeOriginDistribution, "0") > (decimal)0) && (Number(generalVariable_BalanceRemainingResult_AfterAllowanceInvestmentDeduction) >= (decimal)0))
                {
                    //FORMULA HERE
                    bool test = String(RemainingFiscalProfitCommonRate, "0") == String(generalVariable_BalanceRemainingResult_AfterAllowanceInvestmentDeduction);
                    if (!test)
                    {
                        // MESSAGES
                        AddMessage(new BizTaxErrorDataContract
                        {
                            FileId = _fileId,
                            Id = "f-nrcorp-2232",
                            Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Het totaal vermeld in de rubriek '{Resterende winst}' ({$RemainingFiscalProfitCommonRate}) na aftrekken is niet juist (= {$generalVariable_BalanceRemainingResult_AfterAllowanceInvestmentDeduction}). "}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Le total repris dans la rubrique '{Bénéfice subsistant}' ({$RemainingFiscalProfitCommonRate}) après les déductions n'est pas correct (= {$generalVariable_BalanceRemainingResult_AfterAllowanceInvestmentDeduction}). "}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Der in der Rubrik '{Verbleibender Gewinn}' angegebene Gesamtbetrag ({$RemainingFiscalProfitCommonRate}) nach Abzüge ist unzutreffend (= {$generalVariable_BalanceRemainingResult_AfterAllowanceInvestmentDeduction}). "}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Het totaal vermeld in de rubriek '{Resterende winst}' ({$RemainingFiscalProfitCommonRate}) na aftrekken is niet juist (= {$generalVariable_BalanceRemainingResult_AfterAllowanceInvestmentDeduction}). "}  
 }
                            ,
                            Fields = new List<string>()
                        }
                        );
                    }
                }
            }

        }

        internal void Calc_f_nrcorp_2234_assertion_set()
        {
            string scenarioId = GetScenarioId("");
            string period = "D";
            // SCENARIO: ""
            // PERIOD: "D"
            var BasicTaxableAmountExitTaxRate = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:BasicTaxableAmountExitTaxRate" });
            var CapitalGainsSharesRate2500 = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:CapitalGainsSharesRate2500" });
            var BasicTaxableAmountCommonRate = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:BasicTaxableAmountCommonRate" });
            var RemainingFiscalProfitCommonRate = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:RemainingFiscalProfitCommonRate" });
            var ShippingProfitTonnageBased = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:ShippingProfitTonnageBased" });
            var BenevolentAbnormalFinancialAdvantagesBenefitsAllKind = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:BenevolentAbnormalFinancialAdvantagesBenefitsAllKind" });
            var ViolationInvestmentCommitmentIntangibilityConditionInvestmentReserve = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:ViolationInvestmentCommitmentIntangibilityConditionInvestmentReserve" });
            var NonDeductibleCarExpensesPartBenefitsAllKind = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:NonDeductibleCarExpensesPartBenefitsAllKind" });
            var EmployeeParticipation = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:EmployeeParticipation" });
            var FormulaTarget = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:BasicTaxableAmountCommonRate" });
            if (CapitalGainsSharesRate2500.Count > 0 || BasicTaxableAmountExitTaxRate.Count > 0 || BasicTaxableAmountCommonRate.Count > 0 || BenevolentAbnormalFinancialAdvantagesBenefitsAllKind.Count > 0 || ViolationInvestmentCommitmentIntangibilityConditionInvestmentReserve.Count > 0 || NonDeductibleCarExpensesPartBenefitsAllKind.Count > 0 || EmployeeParticipation.Count > 0 || RemainingFiscalProfitCommonRate.Count > 0 || ShippingProfitTonnageBased.Count > 0 || FormulaTarget.Count > 0)
            {
                var CalculatedAmountBasicTaxableAmountCommonRate = ((Number(BasicTaxableAmountExitTaxRate, "0") > (decimal)0) || (Number(CapitalGainsSharesRate2500, "0") > (decimal)0)) ? Sum(Number(RemainingFiscalProfitCommonRate, "0"), Number(ShippingProfitTonnageBased, "0"), Number(BenevolentAbnormalFinancialAdvantagesBenefitsAllKind, "0"), Number(ViolationInvestmentCommitmentIntangibilityConditionInvestmentReserve, "0"), Number(NonDeductibleCarExpensesPartBenefitsAllKind, "0"), Number(EmployeeParticipation, "0"), -Number(BasicTaxableAmountExitTaxRate, "0"), -Number(CapitalGainsSharesRate2500, "0")) : Sum(Number(RemainingFiscalProfitCommonRate, "0"), Number(ShippingProfitTonnageBased, "0"), Number(BenevolentAbnormalFinancialAdvantagesBenefitsAllKind, "0"), Number(ViolationInvestmentCommitmentIntangibilityConditionInvestmentReserve, "0"), Number(NonDeductibleCarExpensesPartBenefitsAllKind, "0"), Number(EmployeeParticipation, "0"));
                //CALCULATION HERE
                if (FormulaTarget.Count == 0)
                {
                    FormulaTarget = CreateNewCalculationElement(scenarioId, period, "tax-inc:BasicTaxableAmountCommonRate", new string[] { "" }, "EUR", "INF");
                }
                if (FormulaTarget.Count > 1)
                {
                    throw new Exception("More then one target for FormulaTarget in assertion f-nrcorp-2234");
                }
                FormulaTarget.First().Calculated = true;
                FormulaTarget.First().SetNumber((Number(BasicTaxableAmountExitTaxRate, "0") > (decimal)0) ? Sum(Number(RemainingFiscalProfitCommonRate, "0"), Number(ShippingProfitTonnageBased, "0"), Number(BenevolentAbnormalFinancialAdvantagesBenefitsAllKind, "0"), Number(ViolationInvestmentCommitmentIntangibilityConditionInvestmentReserve, "0"), Number(NonDeductibleCarExpensesPartBenefitsAllKind, "0"), Number(EmployeeParticipation, "0"), -Number(BasicTaxableAmountExitTaxRate, "0")) : Sum(Number(RemainingFiscalProfitCommonRate, "0"), Number(ShippingProfitTonnageBased, "0"), Number(BenevolentAbnormalFinancialAdvantagesBenefitsAllKind, "0"), Number(ViolationInvestmentCommitmentIntangibilityConditionInvestmentReserve, "0"), Number(NonDeductibleCarExpensesPartBenefitsAllKind, "0"), Number(EmployeeParticipation, "0")));
                //FORMULA HERE
                bool test = ((Number(BasicTaxableAmountExitTaxRate, "0") > (decimal)0) || (Number(CapitalGainsSharesRate2500, "0") > (decimal)0)) ? (Number(BasicTaxableAmountCommonRate, "0") == Sum(Number(RemainingFiscalProfitCommonRate, "0"), Number(ShippingProfitTonnageBased, "0"), Number(BenevolentAbnormalFinancialAdvantagesBenefitsAllKind, "0"), Number(ViolationInvestmentCommitmentIntangibilityConditionInvestmentReserve, "0"), Number(NonDeductibleCarExpensesPartBenefitsAllKind, "0"), Number(EmployeeParticipation, "0"), -Number(BasicTaxableAmountExitTaxRate, "0"), -Number(CapitalGainsSharesRate2500, "0"))) : (Number(BasicTaxableAmountCommonRate, "0") == Sum(Number(RemainingFiscalProfitCommonRate, "0"), Number(ShippingProfitTonnageBased, "0"), Number(BenevolentAbnormalFinancialAdvantagesBenefitsAllKind, "0"), Number(ViolationInvestmentCommitmentIntangibilityConditionInvestmentReserve, "0"), Number(NonDeductibleCarExpensesPartBenefitsAllKind, "0"), Number(EmployeeParticipation, "0")));
                if (!test)
                {
                    // MESSAGES
                    AddMessage(new BizTaxErrorDataContract
                    {
                        FileId = _fileId,
                        Id = "f-nrcorp-2234",
                        Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Het totaal vermeld in de rubriek '{Belastbaar tegen gewoon tarief}' ({$BasicTaxableAmountCommonRate}) is niet juist (= {$CalculatedAmountBasicTaxableAmountCommonRate}). Indien de rubriek '{Belastbaar tegen exit tax tarief (16,5%)}' ({$BasicTaxableAmountExitTaxRate}) of de rubriek '{Meerwaarden op aandelen belastbaar tegen 25%}' ({$CapitalGainsSharesRate2500})  is ingevuld, dient het bedrag ingevuld in de rubriek '{Belastbaar tegen gewoon tarief}' gelijk te zijn aan de som van alle elementen en na aftrek van het bedrag ingevuld in de rubriek '{Belastbaar tegen exit tax tarief (16,5%)}' of de rubriek '{Meerwaarden op aandelen belastbaar tegen 25%}'.	"}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Le total repris dans la rubrique '{Imposable au taux normal}' ({$BasicTaxableAmountCommonRate}) n'est pas correct (= {$CalculatedAmountBasicTaxableAmountCommonRate}). Si la rubrique '{Imposable au taux de l'exit tax (16,5%)}' ({$BasicTaxableAmountExitTaxRate}) ou la rubrique '{Plus-values sur actions ou parts imposables à 25%}' ({$CapitalGainsSharesRate2500}) est complétée, le montant complété dans la rubrique '{Imposable au taux normal}' doit être égal à la somme des élements après soustraction du montant complété dans la rubrique '{Imposable au taux de l'exit tax (16,5%)}' ou la rubrique '{Plus-values sur actions ou parts imposables à 25%}'.	"}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Der in der Rubrik 	'{Zum normalen Satz steuerbar}' ({$BasicTaxableAmountCommonRate}) 	angegebene Gesamtbetrag ist nicht korrekt 	(= {$CalculatedAmountBasicTaxableAmountCommonRate}). 	Wenn die Rubrik 	'{Steuerbar zum Exit-Tax-Steuersatz (16,5 %)}' ({$BasicTaxableAmountExitTaxRate}) oder '{Mehrwerte auf Aktien oder Anteile, steuerbar zum Satz von 25 %}' ({$CapitalGainsSharesRate2500}) 	ausgefüllt wurde, muss der in der Rubrik 	'{Zum normalen Satz steuerbar}' 	angegebene Betrag, nach Abzug des in der Rubrik 	'{Steuerbar zum Exit-Tax-Steuersatz (16,5 %)}' oder '{Mehrwerte auf Aktien oder Anteile, steuerbar zum Satz von 25 %}'	angegebenen Betrags, gleich der Summe aller Elemente sein."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Het totaal vermeld in de rubriek '{Belastbaar tegen gewoon tarief}' ({$BasicTaxableAmountCommonRate}) is niet juist (= {$CalculatedAmountBasicTaxableAmountCommonRate}). Indien de rubriek '{Belastbaar tegen exit tax tarief (16,5%)}' ({$BasicTaxableAmountExitTaxRate}) of de rubriek '{Meerwaarden op aandelen belastbaar tegen 25%}' ({$CapitalGainsSharesRate2500})  is ingevuld, dient het bedrag ingevuld in de rubriek '{Belastbaar tegen gewoon tarief}' gelijk te zijn aan de som van alle elementen en na aftrek van het bedrag ingevuld in de rubriek '{Belastbaar tegen exit tax tarief (16,5%)}' of de rubriek '{Meerwaarden op aandelen belastbaar tegen 25%}'.	"}  
 }
                        ,
                        Fields = new List<string>()
                    }
                    );
                }
            }

        }

        internal void Calc_f_nrcorp_2238_assertion_set()
        {

        }

        internal void Calc_f_nrcorp_2418_assertion_set()
        {
            string scenarioId = GetScenarioId("");
            string period = "D";
            // SCENARIO: ""
            // PERIOD: "D"
            var NonRepayableAdvanceLevies = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:NonRepayableAdvanceLevies" });
            var NonRepayableFictiousWitholdingTax = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:NonRepayableFictiousWitholdingTax" });
            var NonRepayableLumpSumForeignTaxes = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:NonRepayableLumpSumForeignTaxes" });
            var TaxCreditResearchDevelopment = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:TaxCreditResearchDevelopment" });
            var FormulaTarget = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:NonRepayableAdvanceLevies" });
            if (NonRepayableAdvanceLevies.Count > 0 || NonRepayableFictiousWitholdingTax.Count > 0 || NonRepayableLumpSumForeignTaxes.Count > 0 || TaxCreditResearchDevelopment.Count > 0 || FormulaTarget.Count > 0)
            {
                var CalculatedAmountNonRepayableAdvanceLevies = Sum(Number(NonRepayableFictiousWitholdingTax, "0"), Number(NonRepayableLumpSumForeignTaxes, "0"), Number(TaxCreditResearchDevelopment, "0"));
                //CALCULATION HERE
                if (FormulaTarget.Count == 0)
                {
                    FormulaTarget = CreateNewCalculationElement(scenarioId, period, "tax-inc:NonRepayableAdvanceLevies", new string[] { "" }, "EUR", "INF");
                }
                if (FormulaTarget.Count > 1)
                {
                    throw new Exception("More then one target for FormulaTarget in assertion f-nrcorp-2418");
                }
                FormulaTarget.First().Calculated = true;
                FormulaTarget.First().SetNumber(Sum(Number(NonRepayableFictiousWitholdingTax, "0"), Number(NonRepayableLumpSumForeignTaxes, "0"), Number(TaxCreditResearchDevelopment, "0")));
                //FORMULA HERE
                bool test = Number(NonRepayableAdvanceLevies, "0") == Sum(Number(NonRepayableFictiousWitholdingTax, "0"), Number(NonRepayableLumpSumForeignTaxes, "0"), Number(TaxCreditResearchDevelopment, "0"));
                if (!test)
                {
                    // MESSAGES
                    AddMessage(new BizTaxErrorDataContract
                    {
                        FileId = _fileId,
                        Id = "f-nrcorp-2418",
                        Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Het totaal vermeld in de rubriek '{Niet-terugbetaalbare voorheffingen}' ({$NonRepayableAdvanceLevies}) is niet juist (= {$CalculatedAmountNonRepayableAdvanceLevies})."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Le total repris dans la rubrique '{Précomptes non remboursables}' ({$NonRepayableAdvanceLevies}) n'est pas correct (= {$CalculatedAmountNonRepayableAdvanceLevies})."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Der in der Rubrik '{Nicht rückzahlbare Vorabzüge}' angegebene Gesamtbetrag ({$NonRepayableAdvanceLevies}) ist unzutreffend (= {$CalculatedAmountNonRepayableAdvanceLevies})."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Het totaal vermeld in de rubriek '{Niet-terugbetaalbare voorheffingen}' ({$NonRepayableAdvanceLevies}) is niet juist (= {$CalculatedAmountNonRepayableAdvanceLevies})."}  
 }
                        ,
                        Fields = new List<string>()
                    }
                    );
                }
            }

        }

        internal void Calc_f_nrcorp_2419_assertion_set()
        {
            string scenarioId = GetScenarioId("");
            string period = "D";
            // SCENARIO: ""
            // PERIOD: "D"
            var RepayableAdvanceLevies = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:RepayableAdvanceLevies" });
            var RepayableRealFictitiousWithholdingTaxOtherSharesPEExemptIncomeMovableAssetsBelgium = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:RepayableRealFictitiousWithholdingTaxOtherSharesPEExemptIncomeMovableAssetsBelgium" });
            var RepayableWithholdingTaxPEExemptionLiquidationBonusesBonusesAcquisitionOwnSharesBelgiumForeign = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:RepayableWithholdingTaxPEExemptionLiquidationBonusesBonusesAcquisitionOwnSharesBelgiumForeign" });
            var RepayableWithholdingTaxOtherPEForeign = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:RepayableWithholdingTaxOtherPEForeign" });
            var RepayableWithholdingTaxOtherLiquidationBonusesBonusesAcquisitionOwnShares = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:RepayableWithholdingTaxOtherLiquidationBonusesBonusesAcquisitionOwnShares" });
            var RepayableWithholdingTaxOtherDividends = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:RepayableWithholdingTaxOtherDividends" });
            var OtherRepayableWithholdingTaxes = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:OtherRepayableWithholdingTaxes" });
            var WithholdingTaxEarnedIncomeCapitalGainsImmovableProperty = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:WithholdingTaxEarnedIncomeCapitalGainsImmovableProperty" });
            var WithholdingTaxEarnedIncomeIncomePartner = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:WithholdingTaxEarnedIncomeIncomePartner" });
            var FormulaTarget = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:RepayableAdvanceLevies" });
            if (RepayableAdvanceLevies.Count > 0 || RepayableRealFictitiousWithholdingTaxOtherSharesPEExemptIncomeMovableAssetsBelgium.Count > 0 || RepayableWithholdingTaxPEExemptionLiquidationBonusesBonusesAcquisitionOwnSharesBelgiumForeign.Count > 0 || RepayableWithholdingTaxOtherPEForeign.Count > 0 || RepayableWithholdingTaxOtherLiquidationBonusesBonusesAcquisitionOwnShares.Count > 0 || RepayableWithholdingTaxOtherDividends.Count > 0 || OtherRepayableWithholdingTaxes.Count > 0 || WithholdingTaxEarnedIncomeCapitalGainsImmovableProperty.Count > 0 || WithholdingTaxEarnedIncomeIncomePartner.Count > 0 || FormulaTarget.Count > 0)
            {
                var CalculatedAmountRepayableAdvanceLevies = Sum(Number(RepayableRealFictitiousWithholdingTaxOtherSharesPEExemptIncomeMovableAssetsBelgium, "0"), Number(RepayableWithholdingTaxPEExemptionLiquidationBonusesBonusesAcquisitionOwnSharesBelgiumForeign, "0"), Number(RepayableWithholdingTaxOtherPEForeign, "0"), Number(RepayableWithholdingTaxOtherLiquidationBonusesBonusesAcquisitionOwnShares, "0"), Number(RepayableWithholdingTaxOtherDividends, "0"), Number(OtherRepayableWithholdingTaxes, "0"), Number(WithholdingTaxEarnedIncomeCapitalGainsImmovableProperty, "0"), Number(WithholdingTaxEarnedIncomeIncomePartner, "0"));
                //CALCULATION HERE
                if (FormulaTarget.Count == 0)
                {
                    FormulaTarget = CreateNewCalculationElement(scenarioId, period, "tax-inc:RepayableAdvanceLevies", new string[] { "" }, "EUR", "INF");
                }
                if (FormulaTarget.Count > 1)
                {
                    throw new Exception("More then one target for FormulaTarget in assertion f-nrcorp-2419");
                }
                FormulaTarget.First().Calculated = true;
                FormulaTarget.First().SetNumber(Sum(Number(RepayableRealFictitiousWithholdingTaxOtherSharesPEExemptIncomeMovableAssetsBelgium, "0"), Number(RepayableWithholdingTaxPEExemptionLiquidationBonusesBonusesAcquisitionOwnSharesBelgiumForeign, "0"), Number(RepayableWithholdingTaxOtherPEForeign, "0"), Number(RepayableWithholdingTaxOtherLiquidationBonusesBonusesAcquisitionOwnShares, "0"), Number(RepayableWithholdingTaxOtherDividends, "0"), Number(OtherRepayableWithholdingTaxes, "0"), Number(WithholdingTaxEarnedIncomeCapitalGainsImmovableProperty, "0"), Number(WithholdingTaxEarnedIncomeIncomePartner, "0")));
                //FORMULA HERE
                bool test = Number(RepayableAdvanceLevies, "0") == Sum(Number(RepayableRealFictitiousWithholdingTaxOtherSharesPEExemptIncomeMovableAssetsBelgium, "0"), Number(RepayableWithholdingTaxPEExemptionLiquidationBonusesBonusesAcquisitionOwnSharesBelgiumForeign, "0"), Number(RepayableWithholdingTaxOtherPEForeign, "0"), Number(RepayableWithholdingTaxOtherLiquidationBonusesBonusesAcquisitionOwnShares, "0"), Number(RepayableWithholdingTaxOtherDividends, "0"), Number(OtherRepayableWithholdingTaxes, "0"), Number(WithholdingTaxEarnedIncomeCapitalGainsImmovableProperty, "0"), Number(WithholdingTaxEarnedIncomeIncomePartner, "0"));
                if (!test)
                {
                    // MESSAGES
                    AddMessage(new BizTaxErrorDataContract
                    {
                        FileId = _fileId,
                        Id = "f-nrcorp-2419",
                        Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Het totaal vermeld in de rubriek '{Terugbetaalbare voorheffingen}' ({$RepayableAdvanceLevies}) is niet juist (= {$CalculatedAmountRepayableAdvanceLevies})."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Le total repris dans la rubrique '{Précomptes remboursables}' ({$RepayableAdvanceLevies}) n'est pas correct (= {$CalculatedAmountRepayableAdvanceLevies})."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Der in der Rubrik '{Rückzahlbare Vorabzüge}' angegebene Gesamtbetrag ({$RepayableAdvanceLevies}) ist unzutreffend (= {$CalculatedAmountRepayableAdvanceLevies})."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Het totaal vermeld in de rubriek '{Terugbetaalbare voorheffingen}' ({$RepayableAdvanceLevies}) is niet juist (= {$CalculatedAmountRepayableAdvanceLevies})."}  
 }
                        ,
                        Fields = new List<string>()
                    }
                    );
                }
            }

        }

        internal void Calc_f_nrcorp_2420_assertion_set()
        {

        }

        internal void Calc_f_nrcorp_2444_assertion_set()
        {
            // HAS NO UNBOUND SEQUENCES
            var nonNegativeMonetaryElementsNonResidentLegalEntity = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:LetOutImmovablePropertyIncomeBelgium", "tax-inc:IncomeConstitutionTransferLongLeaseRightsBuildingRightsPlantingRightsSimilarLandRightsBelgium", "tax-inc:TaxableImmovablePropertyIncomeBelgium", "tax-inc:TaxableFinancialAdvantagesBenefitsAllKind", "tax-inc:TaxablePensionsCapitalsEmployerContributionsEmployerPremiums", "tax-inc:TaxableUnjustifiedExpensesFinancialAdvantagesBenefitsAllKind", "tax-inc:CompensableLossesUnbuiltImmovablePropertyRightsInRemImmovablePropertyFivePreviousTaxPeriods", "tax-inc:TaxableRealisedCapitalGainsUnbuiltImmovablePropertyRightsInRemImmovableProperty", "tax-inc:RegistrationDutiesUnbuiltImmovablePropertyRightsInRemImmovableProperty", "tax-inc:CompensableLossesBuiltImmovablePropertyRightsInRemImmovablePropertyFivePreviousTaxPeriods", "tax-inc:TaxableRealisedCapitalGainsBuiltImmovablePropertyRightsInRemImmovableProperty", "tax-inc:RegistrationDutiesBuiltImmovablePropertyRightsInRemImmovableProperty" });
            var monetaryElementsNonResidentLegalEntity = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:NetRealisedCapitalGainsLossesUnbuiltImmovablePropertyRightsInRemImmovableProperty", "tax-inc:NetRealisedCapitalGainsLossesBuiltImmovablePropertyRightsInRemImmovableProperty" });
            var nonNegativeMonetaryElementsNonResidentCorporate = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:TaxableReservedProfit", "tax-inc:DisallowedExpenses", "tax-inc:ImmovablePropertyIncome", "tax-inc:IncomePartnerCompaniesJointVenturesAssociationsNoLegalPersonality", "tax-inc:DeductionLimitElementsFiscalResult", "tax-inc:BasicTaxableAmountCommonRate", "tax-inc:BasicTaxableAmountExitTaxRate", "tax-inc:UnjustifiedExpensesHiddenSurplusProfitsFinancialAdvantagesBenefitsAllKind", "tax-inc:AdditionalDutiesDiamondTraders", "tax-inc:RetributionTaxCreditResearchDevelopment", "tax-inc:DeductibleMiscellaneousExemptions", "tax-inc:PEExemptIncomeMovableAssets", "tax-inc:CarryOverNextTaxPeriodPEExemptIncomeMovableAssets", "tax-inc:AccumulatedAllowanceCorporateEquity", "tax-inc:CarryOverNextTaxPeriodInsufficientProfitsAllowanceCorporateEquity", "tax-inc:Prepayments", "tax-inc:NonRepayableAdvanceLevies", "tax-inc:RepayableAdvanceLevies" });
            var monetaryElementsNonResidentCorporate = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:ShippingResultTonnageBased", "tax-inc:RemainingFiscalResultBeforeOriginDistribution" });
            var nonPositiveMonetaryElementsNonResidentCorporate = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:CarryOverTaxLosses" });
            if (nonNegativeMonetaryElementsNonResidentLegalEntity.Count > 0 || monetaryElementsNonResidentLegalEntity.Count > 0 || nonNegativeMonetaryElementsNonResidentCorporate.Count > 0 || monetaryElementsNonResidentCorporate.Count > 0 || nonPositiveMonetaryElementsNonResidentCorporate.Count > 0)
            {
                //CALCULATION HERE
                if ((Sum(Number(nonNegativeMonetaryElementsNonResidentCorporate, "0")) > (decimal)0) || (Sum(Number(monetaryElementsNonResidentCorporate, "0")) != (decimal)0) || (Sum(Number(nonPositiveMonetaryElementsNonResidentCorporate, "0")) < (decimal)0))
                {
                    //FORMULA HERE
                    bool test = (Sum(Number(nonNegativeMonetaryElementsNonResidentLegalEntity, "0")) == (decimal)0) && (Sum(Number(monetaryElementsNonResidentLegalEntity, "0")) == (decimal)0);
                    if (!test)
                    {
                        // MESSAGES
                        AddMessage(new BizTaxErrorDataContract
                        {
                            FileId = _fileId,
                            Id = "f-nrcorp-2444",
                            Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "De delen '{Aangifte in de belasting van niet-inwoners - Buitenlandse vennootschappen, verenigingen, instellingen of lichamen die een onderneming exploiteren of zich met verrichtingen van winstgevende aard bezighouden}' en '{Aangifte in de belasting van niet-inwoners - Rechtspersonen die geen onderneming exploiteren of zich niet met verrichtingen van winstgevende aard bezighouden}' mogen niet gelijktijdig worden ingevuld."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Les parties '{Déclaration à l’impôt des non-résidents - Sociétés, associations, établissements ou organismes quelconques étrangers qui se livrent à une exploitation ou à des opérations de caractère lucratif}' et '{Déclaration à l’impôt des non-résidents - Personnes morales qui ne se livrent pas à une exploitation ou à des opérations de caractère lucratif}' ne peuvent pas être complétées simultanément."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Die Teile '{Erklärung zur Steuer der Gebietsfremden - Ausländische Gesellschaften, Vereinigungen, Einrichtungen oder Körperschaften, die sich gewerblich betätigen oder gewinnbringende Tätigkeiten betreiben}' und '{Erklärung zur Steuer der Gebietsfremden - Juristische Personen, die sich nicht gewerblich betätigen oder keine gewinnbringenden Tätigkeiten betreiben}' dürfen nicht gleichzeitig ausgefüllt werden."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "De delen '{Aangifte in de belasting van niet-inwoners - Buitenlandse vennootschappen, verenigingen, instellingen of lichamen die een onderneming exploiteren of zich met verrichtingen van winstgevende aard bezighouden}' en '{Aangifte in de belasting van niet-inwoners - Rechtspersonen die geen onderneming exploiteren of zich niet met verrichtingen van winstgevende aard bezighouden}' mogen niet gelijktijdig worden ingevuld."}  
 }
                            ,
                            Fields = new List<string>()
                        }
                        );
                    }
                }
            }

        }

        internal void Calc_f_nrcorp_2445_assertion_set()
        {
            // HAS NO UNBOUND SEQUENCES
            var nonNegativeMonetaryElementsNonResidentCorporate = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:TaxableReservedProfit", "tax-inc:DisallowedExpenses", "tax-inc:ImmovablePropertyIncome", "tax-inc:IncomePartnerCompaniesJointVenturesAssociationsNoLegalPersonality", "tax-inc:DeductionLimitElementsFiscalResult", "tax-inc:BasicTaxableAmountCommonRate", "tax-inc:BasicTaxableAmountExitTaxRate", "tax-inc:UnjustifiedExpensesHiddenSurplusProfitsFinancialAdvantagesBenefitsAllKind", "tax-inc:AdditionalDutiesDiamondTraders", "tax-inc:RetributionTaxCreditResearchDevelopment", "tax-inc:DeductibleMiscellaneousExemptions", "tax-inc:PEExemptIncomeMovableAssets", "tax-inc:CarryOverNextTaxPeriodPEExemptIncomeMovableAssets", "tax-inc:AccumulatedAllowanceCorporateEquity", "tax-inc:CarryOverNextTaxPeriodInsufficientProfitsAllowanceCorporateEquity", "tax-inc:Prepayments", "tax-inc:NonRepayableAdvanceLevies", "tax-inc:RepayableAdvanceLevies" });
            var monetaryElementsNonResidentCorporate = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:ShippingResultTonnageBased", "tax-inc:RemainingFiscalResultBeforeOriginDistribution" });
            var nonPositiveMonetaryElementsNonResidentCorporate = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:CarryOverTaxLosses" });
            var nonNegativeMonetaryElementsNonResidentLegalEntity = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:LetOutImmovablePropertyIncomeBelgium", "tax-inc:IncomeConstitutionTransferLongLeaseRightsBuildingRightsPlantingRightsSimilarLandRightsBelgium", "tax-inc:TaxableImmovablePropertyIncomeBelgium", "tax-inc:TaxableFinancialAdvantagesBenefitsAllKind", "tax-inc:TaxablePensionsCapitalsEmployerContributionsEmployerPremiums", "tax-inc:TaxableUnjustifiedExpensesFinancialAdvantagesBenefitsAllKind", "tax-inc:CompensableLossesUnbuiltImmovablePropertyRightsInRemImmovablePropertyFivePreviousTaxPeriods", "tax-inc:TaxableRealisedCapitalGainsUnbuiltImmovablePropertyRightsInRemImmovableProperty", "tax-inc:RegistrationDutiesUnbuiltImmovablePropertyRightsInRemImmovableProperty", "tax-inc:CompensableLossesBuiltImmovablePropertyRightsInRemImmovablePropertyFivePreviousTaxPeriods", "tax-inc:TaxableRealisedCapitalGainsBuiltImmovablePropertyRightsInRemImmovableProperty", "tax-inc:RegistrationDutiesBuiltImmovablePropertyRightsInRemImmovableProperty" });
            var monetaryElementsNonResidentLegalEntity = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:NetRealisedCapitalGainsLossesUnbuiltImmovablePropertyRightsInRemImmovableProperty", "tax-inc:NetRealisedCapitalGainsLossesBuiltImmovablePropertyRightsInRemImmovableProperty" });
            if (nonNegativeMonetaryElementsNonResidentLegalEntity.Count > 0 || monetaryElementsNonResidentLegalEntity.Count > 0 || nonNegativeMonetaryElementsNonResidentCorporate.Count > 0 || monetaryElementsNonResidentCorporate.Count > 0 || nonPositiveMonetaryElementsNonResidentCorporate.Count > 0)
            {
                //CALCULATION HERE
                if ((Sum(Number(nonNegativeMonetaryElementsNonResidentLegalEntity, "0")) > (decimal)0) || (Sum(Number(monetaryElementsNonResidentLegalEntity, "0")) != (decimal)0))
                {
                    //FORMULA HERE
                    bool test = (Sum(Number(nonNegativeMonetaryElementsNonResidentCorporate, "0")) == (decimal)0) && (Sum(Number(monetaryElementsNonResidentCorporate, "0")) == (decimal)0) && (Sum(Number(nonPositiveMonetaryElementsNonResidentCorporate, "0")) == (decimal)0);
                    if (!test)
                    {
                        // MESSAGES
                        AddMessage(new BizTaxErrorDataContract
                        {
                            FileId = _fileId,
                            Id = "f-nrcorp-2445",
                            Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "De delen '{Aangifte in de belasting van niet-inwoners - Buitenlandse vennootschappen, verenigingen, instellingen of lichamen die een onderneming exploiteren of zich met verrichtingen van winstgevende aard bezighouden}' en '{Aangifte in de belasting van niet-inwoners - Rechtspersonen die geen onderneming exploiteren of zich niet met verrichtingen van winstgevende aard bezighouden}' mogen niet gelijktijdig worden ingevuld."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Les parties '{Déclaration à l’impôt des non-résidents - Sociétés, associations, établissements ou organismes quelconques étrangers qui se livrent à une exploitation ou à des opérations de caractère lucratif}' en '{Déclaration à l’impôt des non-résidents - Personnes morales qui ne se livrent pas à une exploitation ou à des opérations de caractère lucratif}' ne peuvent pas être complétées simultanément."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Die Teile '{Erklärung zur Steuer der Gebietsfremden - Ausländische Gesellschaften, Vereinigungen, Einrichtungen oder Körperschaften, die sich gewerblich betätigen oder gewinnbringende Tätigkeiten betreiben}' und '{Erklärung zur Steuer der Gebietsfremden - Juristische Personen, die sich nicht gewerblich betätigen oder keine gewinnbringenden Tätigkeiten betreiben}' dürfen nicht gleichzeitig ausgefüllt werden."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "De delen '{Aangifte in de belasting van niet-inwoners - Buitenlandse vennootschappen, verenigingen, instellingen of lichamen die een onderneming exploiteren of zich met verrichtingen van winstgevende aard bezighouden}' en '{Aangifte in de belasting van niet-inwoners - Rechtspersonen die geen onderneming exploiteren of zich niet met verrichtingen van winstgevende aard bezighouden}' mogen niet gelijktijdig worden ingevuld."}  
 }
                            ,
                            Fields = new List<string>()
                        }
                        );
                    }
                }
            }

        }

        internal virtual void Calc_f_nrcorp_2446_assertion_set()
        {
            // HAS NO UNBOUND SEQUENCES
            var nonNegativeMonetaryElementsNonResidentCorporate = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:TaxableReservedProfit", "tax-inc:DisallowedExpenses", "tax-inc:ImmovablePropertyIncome", "tax-inc:IncomePartnerCompaniesJointVenturesAssociationsNoLegalPersonality", "tax-inc:DeductionLimitElementsFiscalResult", "tax-inc:BasicTaxableAmountCommonRate", "tax-inc:BasicTaxableAmountExitTaxRate", "tax-inc:UnjustifiedExpensesHiddenSurplusProfitsFinancialAdvantagesBenefitsAllKind", "tax-inc:AdditionalDutiesDiamondTraders", "tax-inc:RetributionTaxCreditResearchDevelopment", "tax-inc:DeductibleMiscellaneousExemptions", "tax-inc:PEExemptIncomeMovableAssets", "tax-inc:CarryOverNextTaxPeriodPEExemptIncomeMovableAssets", "tax-inc:AccumulatedAllowanceCorporateEquity", "tax-inc:CarryOverNextTaxPeriodInsufficientProfitsAllowanceCorporateEquity", "tax-inc:Prepayments", "tax-inc:NonRepayableAdvanceLevies", "tax-inc:RepayableAdvanceLevies" });
            var monetaryElementsNonResidentCorporate = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:ShippingResultTonnageBased", "tax-inc:RemainingFiscalResultBeforeOriginDistribution" });
            var nonPositiveMonetaryElementsNonResidentCorporate = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:CarryOverTaxLosses" });
            if (nonNegativeMonetaryElementsNonResidentCorporate.Count > 0 || monetaryElementsNonResidentCorporate.Count > 0 || nonPositiveMonetaryElementsNonResidentCorporate.Count > 0)
            {
                //CALCULATION HERE
                if ((Sum(Number(nonNegativeMonetaryElementsNonResidentCorporate, "0")) > (decimal)0) || (Sum(Number(monetaryElementsNonResidentCorporate, "0")) != (decimal)0) || (Sum(Number(nonPositiveMonetaryElementsNonResidentCorporate, "0")) < (decimal)0))
                {
                    //FORMULA HERE
                    bool test = Count(Xbrl.Elements.Where(e => e.Name == "BalanceSheetStatutorySeatStatutoryAccountsBelgianBranch").ToList()) != (decimal)0;
                    if (!test)
                    {
                        // MESSAGES
                        AddMessage(new BizTaxErrorDataContract
                        {
                            FileId = _fileId,
                            Id = "f-nrcorp-2446",
                            Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "De bijlage '{Balans opgesteld door de maatschappelijke zetel en jaarrekening van de inrichting (balans, resultatenrekening en eventuele toelichting)}' is verplicht bij te voegen in het tabblad 	'{Diverse bescheiden en opgaven}'."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "L'annexe '{Bilan établi par le siège social et les comptes annuels de l'établissement (bilan, compte de résultats et annexe éventuelle)}' doit obligatoirement être ajoutée dans l'onglet 	'{Documents et relevés divers}'."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Der Anhang '{Vom Gesellschaftssitz aufgestellte Bilanz und Jahresabschluss der Einrichtung (Bilanz, Ergebnisrechnung und eventuelle Anlage)}' muss obligatorisch in die Registerkarte '{Verschiedene Unterlagen und Verzeichnisse}' eingegeben werden."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "De bijlage '{Balans opgesteld door de maatschappelijke zetel en jaarrekening van de inrichting (balans, resultatenrekening en eventuele toelichting)}' is verplicht bij te voegen in het tabblad 	'{Diverse bescheiden en opgaven}'."}  
 }
                            ,
                            Fields = new List<string>()
                        }
                        );
                    }
                }
            }

        }

        internal void Calc_f_nrcorp_5306_assertion_set()
        {
            string scenarioId = GetScenarioId("");
            string period = "D";
            // SCENARIO: ""
            // PERIOD: "D"
            var AllowanceCorporateEquity = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:AllowanceCorporateEquity" });
            var DeductionAllowanceCorporateEquityCurrentAssessmentYear = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:DeductionAllowanceCorporateEquityCurrentAssessmentYear" });
            var DeductionAllowanceCorporateEquityPreviousAssessmentYears = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:DeductionAllowanceCorporateEquityPreviousAssessmentYears" });
            var FormulaTarget = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:AllowanceCorporateEquity" });
            if (AllowanceCorporateEquity.Count > 0 || DeductionAllowanceCorporateEquityCurrentAssessmentYear.Count > 0 || DeductionAllowanceCorporateEquityPreviousAssessmentYears.Count > 0 || FormulaTarget.Count > 0)
            {
                var CalculatedAmountAllowanceCorporateEquity = Sum(Number(DeductionAllowanceCorporateEquityCurrentAssessmentYear, "0"), Number(DeductionAllowanceCorporateEquityPreviousAssessmentYears, "0"));
                //CALCULATION HERE
                if (FormulaTarget.Count == 0)
                {
                    FormulaTarget = CreateNewCalculationElement(scenarioId, period, "tax-inc:AllowanceCorporateEquity", new string[] { "" }, "EUR", "INF");
                }
                if (FormulaTarget.Count > 1)
                {
                    throw new Exception("More then one target for FormulaTarget in assertion f-nrcorp-5306");
                }
                FormulaTarget.First().Calculated = true;
                FormulaTarget.First().SetNumber(Sum(Number(DeductionAllowanceCorporateEquityCurrentAssessmentYear, "0"), Number(DeductionAllowanceCorporateEquityPreviousAssessmentYears, "0")));
                //FORMULA HERE
                bool test = Number(AllowanceCorporateEquity, "0") == Sum(Number(DeductionAllowanceCorporateEquityCurrentAssessmentYear, "0"), Number(DeductionAllowanceCorporateEquityPreviousAssessmentYears, "0"));
                if (!test)
                {
                    // MESSAGES
                    AddMessage(new BizTaxErrorDataContract
                    {
                        FileId = _fileId,
                        Id = "f-nrcorp-5306",
                        Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Het bedrag vermeld in de rubriek '{Aftrek voor risicokapitaal dat voor het huidig aanslagjaar wordt afgetrokken}' ({$AllowanceCorporateEquity}) moet gelijk zijn aan de som van de bedragen vermeld in de rubrieken 	'{Aftrek voor risicokapitaal van het huidig aanslagjaar die werkelijk wordt afgetrokken}' 	en '{Aftrek voor risicokapitaal van vorige aanslagjaren dat werkelijk wordt afgetrokken}' (= {$CalculatedAmountAllowanceCorporateEquity})."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Le montant repris dans la rubrique '{Déduction pour capital à risque déduite au cours de l'exercice d'imposition actuel}' ({$AllowanceCorporateEquity}) 	doit être égal à la somme des montants repris dans des rubriques 	'{Déduction pour capital à risque de l'exercice d'imposition actuel, effectivement déduite}' 	et '{Déduction pour capital à risque antérieure effectivement déduite}' 	(= {$CalculatedAmountAllowanceCorporateEquity})."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Der Betrag, der angegeben ist in der Rubrik '{Abzug für Risikokapital, der im Laufe des jetzigen Steuerjahres abgezogen wird}' ({$AllowanceCorporateEquity}) muss der Gesamtsumme der Beträge, 	die angegeben sind in den Rubriken '{Abzug für Risikokapital, der tatsächlich für das aktuelle Steuerjahr abgezogen wird}' 	und '{Tatsächlich abgezogener Abzug für Risikokapital, der im Laufe vorangehender Steuerjahre gebildet wurde}' entsprechen (= {$CalculatedAmountAllowanceCorporateEquity})."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Het bedrag vermeld in de rubriek '{Aftrek voor risicokapitaal dat voor het huidig aanslagjaar wordt afgetrokken}' ({$AllowanceCorporateEquity}) moet gelijk zijn aan de som van de bedragen vermeld in de rubrieken 	'{Aftrek voor risicokapitaal van het huidig aanslagjaar die werkelijk wordt afgetrokken}' 	en '{Aftrek voor risicokapitaal van vorige aanslagjaren dat werkelijk wordt afgetrokken}' (= {$CalculatedAmountAllowanceCorporateEquity})."}  
 }
                        ,
                        Fields = new List<string>()
                    }
                    );
                }
            }

        }

        internal void Calc_f_nrcorp_5308_assertion_set()
        {

        }

        internal void Calc_f_le_3001_assertion_set()
        {
            string scenarioId = GetScenarioId("");
            string period = "D";
            // SCENARIO: ""
            // PERIOD: "D"
            var NetRentRentalBenefitsBelgium = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:NetRentRentalBenefitsBelgium" });
            var GrossRentRentalBenefitsBelgium = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:GrossRentRentalBenefitsBelgium" });
            var DeductibleCostsRentRentalBenefitsBelgium = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:DeductibleCostsRentRentalBenefitsBelgium" });
            var FormulaTarget = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:NetRentRentalBenefitsBelgium" });
            if (NetRentRentalBenefitsBelgium.Count > 0 || GrossRentRentalBenefitsBelgium.Count > 0 || DeductibleCostsRentRentalBenefitsBelgium.Count > 0 || FormulaTarget.Count > 0)
            {
                var CalculatedAmountNetRentRentalBenefitsBelgium = Sum(Number(GrossRentRentalBenefitsBelgium, "0"), -Number(DeductibleCostsRentRentalBenefitsBelgium, "0"));
                //CALCULATION HERE
                if (FormulaTarget.Count == 0)
                {
                    FormulaTarget = CreateNewCalculationElement(scenarioId, period, "tax-inc:NetRentRentalBenefitsBelgium", new string[] { "" }, "EUR", "INF");
                }
                if (FormulaTarget.Count > 1)
                {
                    throw new Exception("More then one target for FormulaTarget in assertion f-le-3001");
                }
                FormulaTarget.First().Calculated = true;
                FormulaTarget.First().SetNumber(Max(Sum(Number(GrossRentRentalBenefitsBelgium, "0"), -Number(DeductibleCostsRentRentalBenefitsBelgium, "0")), (decimal)0));
                //FORMULA HERE
                bool test = Number(NetRentRentalBenefitsBelgium, "0") == Max(Sum(Number(GrossRentRentalBenefitsBelgium, "0"), -Number(DeductibleCostsRentRentalBenefitsBelgium, "0")), (decimal)0);
                if (!test)
                {
                    // MESSAGES
                    AddMessage(new BizTaxErrorDataContract
                    {
                        FileId = _fileId,
                        Id = "f-le-3001",
                        Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Het totaal vermeld in de rubriek '{Netto-inkomsten uit verhuurde onroerende goederen}'	({$NetRentRentalBenefitsBelgium})	is niet juist (= {$CalculatedAmountNetRentRentalBenefitsBelgium})."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Le total repris dans la rubrique '{Revenus nets d'immeubles donnés en location}'	({$NetRentRentalBenefitsBelgium})	n'est pas correct (= {$CalculatedAmountNetRentRentalBenefitsBelgium})."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Der Betrag, der angegeben ist in der Rubrik '{Nettoeinkünfte aus vermieteten Immobilien}'	({$NetRentRentalBenefitsBelgium})	ist unzutreffend (= {$CalculatedAmountNetRentRentalBenefitsBelgium})."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Het totaal vermeld in de rubriek '{Netto-inkomsten uit verhuurde onroerende goederen}'	({$NetRentRentalBenefitsBelgium})	is niet juist (= {$CalculatedAmountNetRentRentalBenefitsBelgium})."}  
 }
                        ,
                        Fields = new List<string>()
                    }
                    );
                }
            }

        }

        internal void Calc_f_le_3002_assertion_set()
        {
            string scenarioId = GetScenarioId("");
            string period = "D";
            // SCENARIO: ""
            // PERIOD: "D"
            var LetOutImmovablePropertyIncomeBelgium = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:LetOutImmovablePropertyIncomeBelgium" });
            var NetRentRentalBenefitsBelgium = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:NetRentRentalBenefitsBelgium" });
            var IndexedCadastralIncomeBelgium = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:IndexedCadastralIncomeBelgium" });
            var FormulaTarget = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:LetOutImmovablePropertyIncomeBelgium" });
            if (LetOutImmovablePropertyIncomeBelgium.Count > 0 || NetRentRentalBenefitsBelgium.Count > 0 || IndexedCadastralIncomeBelgium.Count > 0 || FormulaTarget.Count > 0)
            {
                var CalculatedAmountLetOutImmovablePropertyIncomeBelgium = Sum(Number(NetRentRentalBenefitsBelgium, "0"), -Number(IndexedCadastralIncomeBelgium, "0"));
                //CALCULATION HERE
                if (FormulaTarget.Count == 0)
                {
                    FormulaTarget = CreateNewCalculationElement(scenarioId, period, "tax-inc:LetOutImmovablePropertyIncomeBelgium", new string[] { "" }, "EUR", "INF");
                }
                if (FormulaTarget.Count > 1)
                {
                    throw new Exception("More then one target for FormulaTarget in assertion f-le-3002");
                }
                FormulaTarget.First().Calculated = true;
                FormulaTarget.First().SetNumber(Max(Sum(Number(NetRentRentalBenefitsBelgium, "0"), -Number(IndexedCadastralIncomeBelgium, "0")), (decimal)0));
                //FORMULA HERE
                bool test = Number(LetOutImmovablePropertyIncomeBelgium, "0") == Max(Sum(Number(NetRentRentalBenefitsBelgium, "0"), -Number(IndexedCadastralIncomeBelgium, "0")), (decimal)0);
                if (!test)
                {
                    // MESSAGES
                    AddMessage(new BizTaxErrorDataContract
                    {
                        FileId = _fileId,
                        Id = "f-le-3002",
                        Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Het totaal vermeld in de rubriek '{Inkomsten uit verhuurde onroerende goederen}'	({$LetOutImmovablePropertyIncomeBelgium})	is niet juist (= {$CalculatedAmountLetOutImmovablePropertyIncomeBelgium})."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Le total repris dans la rubrique '{Revenus d'immeubles donnés en location}'	({$LetOutImmovablePropertyIncomeBelgium})	n'est pas correct (= {$CalculatedAmountLetOutImmovablePropertyIncomeBelgium})."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Der Betrag, der angegeben ist in der Rubrik '{Einkünfte aus vermieteten Immobilien}'	({$LetOutImmovablePropertyIncomeBelgium})	ist unzutreffend (= {$CalculatedAmountLetOutImmovablePropertyIncomeBelgium})."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Het totaal vermeld in de rubriek '{Inkomsten uit verhuurde onroerende goederen}'	({$LetOutImmovablePropertyIncomeBelgium})	is niet juist (= {$CalculatedAmountLetOutImmovablePropertyIncomeBelgium})."}  
 }
                        ,
                        Fields = new List<string>()
                    }
                    );
                }
            }

        }

        internal void Calc_f_le_3003_assertion_set()
        {
            string scenarioId = GetScenarioId("");
            string period = "D";
            // SCENARIO: ""
            // PERIOD: "D"
            var TaxableImmovablePropertyIncomeBelgium = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:TaxableImmovablePropertyIncomeBelgium" });
            var LetOutImmovablePropertyIncomeBelgium = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:LetOutImmovablePropertyIncomeBelgium" });
            var IncomeConstitutionTransferLongLeaseRightsBuildingRightsPlantingRightsSimilarLandRightsBelgium = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:IncomeConstitutionTransferLongLeaseRightsBuildingRightsPlantingRightsSimilarLandRightsBelgium" });
            var FormulaTarget = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:TaxableImmovablePropertyIncomeBelgium" });
            if (TaxableImmovablePropertyIncomeBelgium.Count > 0 || LetOutImmovablePropertyIncomeBelgium.Count > 0 || IncomeConstitutionTransferLongLeaseRightsBuildingRightsPlantingRightsSimilarLandRightsBelgium.Count > 0 || FormulaTarget.Count > 0)
            {
                var CalculatedAmountTaxableImmovablePropertyIncomeBelgium = Sum(Number(LetOutImmovablePropertyIncomeBelgium, "0"), Number(IncomeConstitutionTransferLongLeaseRightsBuildingRightsPlantingRightsSimilarLandRightsBelgium, "0"));
                //CALCULATION HERE
                if (FormulaTarget.Count == 0)
                {
                    FormulaTarget = CreateNewCalculationElement(scenarioId, period, "tax-inc:TaxableImmovablePropertyIncomeBelgium", new string[] { "" }, "EUR", "INF");
                }
                if (FormulaTarget.Count > 1)
                {
                    throw new Exception("More then one target for FormulaTarget in assertion f-le-3003");
                }
                FormulaTarget.First().Calculated = true;
                FormulaTarget.First().SetNumber(Sum(Number(LetOutImmovablePropertyIncomeBelgium, "0"), Number(IncomeConstitutionTransferLongLeaseRightsBuildingRightsPlantingRightsSimilarLandRightsBelgium, "0")));
                //FORMULA HERE
                bool test = Number(TaxableImmovablePropertyIncomeBelgium, "0") == Sum(Number(LetOutImmovablePropertyIncomeBelgium, "0"), Number(IncomeConstitutionTransferLongLeaseRightsBuildingRightsPlantingRightsSimilarLandRightsBelgium, "0"));
                if (!test)
                {
                    // MESSAGES
                    AddMessage(new BizTaxErrorDataContract
                    {
                        FileId = _fileId,
                        Id = "f-le-3003",
                        Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Het totaal vermeld in de rubriek '{Belastbare Belgische inkomsten}'	({$TaxableImmovablePropertyIncomeBelgium})	is niet juist (= {$CalculatedAmountTaxableImmovablePropertyIncomeBelgium})."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Le total repris dans la rubrique '{Revenus belges imposables}'	({$TaxableImmovablePropertyIncomeBelgium})	n'est pas correct (= {$CalculatedAmountTaxableImmovablePropertyIncomeBelgium})."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Der Betrag, der angegeben ist in der Rubrik '{Steuerpflichtige belgische Einkünfte}'	({$TaxableImmovablePropertyIncomeBelgium})	ist unzutreffend (= {$CalculatedAmountTaxableImmovablePropertyIncomeBelgium})."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Het totaal vermeld in de rubriek '{Belastbare Belgische inkomsten}'	({$TaxableImmovablePropertyIncomeBelgium})	is niet juist (= {$CalculatedAmountTaxableImmovablePropertyIncomeBelgium})."}  
 }
                        ,
                        Fields = new List<string>()
                    }
                    );
                }
            }

        }

        internal void Calc_f_le_3010_assertion_set()
        {
            string scenarioId = GetScenarioId("");
            string period = "D";
            // SCENARIO: ""
            // PERIOD: "D"
            var CompensableLossesUnbuiltImmovablePropertyRightsInRemImmovablePropertyFivePreviousTaxPeriods = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:CompensableLossesUnbuiltImmovablePropertyRightsInRemImmovablePropertyFivePreviousTaxPeriods" });
            var NetRealisedCapitalGainsLossesUnbuiltImmovablePropertyRightsInRemImmovableProperty = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:NetRealisedCapitalGainsLossesUnbuiltImmovablePropertyRightsInRemImmovableProperty" });
            if (CompensableLossesUnbuiltImmovablePropertyRightsInRemImmovablePropertyFivePreviousTaxPeriods.Count > 0 || NetRealisedCapitalGainsLossesUnbuiltImmovablePropertyRightsInRemImmovableProperty.Count > 0)
            {
                //CALCULATION HERE
                //FORMULA HERE
                bool test = Number(CompensableLossesUnbuiltImmovablePropertyRightsInRemImmovablePropertyFivePreviousTaxPeriods) <= Number(NetRealisedCapitalGainsLossesUnbuiltImmovablePropertyRightsInRemImmovableProperty, "()");
                if (!test)
                {
                    // MESSAGES
                    AddMessage(new BizTaxErrorDataContract
                    {
                        FileId = _fileId,
                        Id = "f-le-3010",
                        Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Het bedrag ingevuld in de rubriek '{Saldo van de tijdens de vijf vorige belastbare tijdperken geleden en nog te compenseren verliezen op de vervreemding van dergelijke goederen of rechten, beperkt tot het positief nettoresultaat}' 	moet kleiner zijn dan of gelijk zijn aan het bedrag ingevuld in de rubriek '{Nettoresultaat van de meerwaarden en verliezen met betrekking tot dergelijke tijdens het belastbare tijdperk vervreemde goederen of rechten}'."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Le montant complété dans la rubrique '{Solde des pertes restant à compenser éprouvées au cours des cinq périodes imposables antérieures lors de l'aliénation de biens ou droits de l'espèce, à limiter au résultat positif net}' 	doit être égal ou inférieur au montant complété dans la rubrique '{Résultat net des plus-values et des pertes relatives à des biens ou droits de l'espèce qui ont été aliénés au cours de la période imposable}'."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Der in der Rubrik 	'{Restbetrag der im Laufe der fünf vorangehenden Besteuerungszeiträume erlittenen und noch auszugleichenden Verluste bei der Veräußerung von Gütern und Rechten dieser Art, begrenzt auf das positive Nettoergebnis}' 	angegebene Betrag darf den in der Rubrik 	'{Nettoergebnis der Mehrwerte und der Verluste in Bezug auf während des Besteuerungszeitraums veräußerte Güter oder Rechte dieser Art}' 	angegebenen Betrag nicht übersteigen."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Het bedrag ingevuld in de rubriek '{Saldo van de tijdens de vijf vorige belastbare tijdperken geleden en nog te compenseren verliezen op de vervreemding van dergelijke goederen of rechten, beperkt tot het positief nettoresultaat}' 	moet kleiner zijn dan of gelijk zijn aan het bedrag ingevuld in de rubriek '{Nettoresultaat van de meerwaarden en verliezen met betrekking tot dergelijke tijdens het belastbare tijdperk vervreemde goederen of rechten}'."}  
 }
                        ,
                        Fields = new List<string>()
                    }
                    );
                }
            }

        }

        internal void Calc_f_le_3011_assertion_set()
        {
            string scenarioId = GetScenarioId("");
            string period = "D";
            // SCENARIO: ""
            // PERIOD: "D"
            var TaxableRealisedCapitalGainsUnbuiltImmovablePropertyRightsInRemImmovableProperty = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:TaxableRealisedCapitalGainsUnbuiltImmovablePropertyRightsInRemImmovableProperty" });
            var NetRealisedCapitalGainsLossesUnbuiltImmovablePropertyRightsInRemImmovableProperty = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:NetRealisedCapitalGainsLossesUnbuiltImmovablePropertyRightsInRemImmovableProperty" });
            var CompensableLossesUnbuiltImmovablePropertyRightsInRemImmovablePropertyFivePreviousTaxPeriods = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:CompensableLossesUnbuiltImmovablePropertyRightsInRemImmovablePropertyFivePreviousTaxPeriods" });
            var FormulaTarget = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:TaxableRealisedCapitalGainsUnbuiltImmovablePropertyRightsInRemImmovableProperty" });
            if (TaxableRealisedCapitalGainsUnbuiltImmovablePropertyRightsInRemImmovableProperty.Count > 0 || NetRealisedCapitalGainsLossesUnbuiltImmovablePropertyRightsInRemImmovableProperty.Count > 0 || CompensableLossesUnbuiltImmovablePropertyRightsInRemImmovablePropertyFivePreviousTaxPeriods.Count > 0 || FormulaTarget.Count > 0)
            {
                var CalculatedAmountTaxableRealisedCapitalGainsUnbuiltImmovablePropertyRightsInRemImmovableProperty = Sum(Number(NetRealisedCapitalGainsLossesUnbuiltImmovablePropertyRightsInRemImmovableProperty, "0"), -Number(CompensableLossesUnbuiltImmovablePropertyRightsInRemImmovablePropertyFivePreviousTaxPeriods, "0"));
                //CALCULATION HERE
                if (FormulaTarget.Count == 0)
                {
                    FormulaTarget = CreateNewCalculationElement(scenarioId, period, "tax-inc:TaxableRealisedCapitalGainsUnbuiltImmovablePropertyRightsInRemImmovableProperty", new string[] { "" }, "EUR", "INF");
                }
                if (FormulaTarget.Count > 1)
                {
                    throw new Exception("More then one target for FormulaTarget in assertion f-le-3011");
                }
                FormulaTarget.First().Calculated = true;
                FormulaTarget.First().SetNumber(Max(Sum(Number(NetRealisedCapitalGainsLossesUnbuiltImmovablePropertyRightsInRemImmovableProperty, "0"), -Number(CompensableLossesUnbuiltImmovablePropertyRightsInRemImmovablePropertyFivePreviousTaxPeriods, "0")), (decimal)0));
                //FORMULA HERE
                bool test = Number(TaxableRealisedCapitalGainsUnbuiltImmovablePropertyRightsInRemImmovableProperty, "0") == Max(Sum(Number(NetRealisedCapitalGainsLossesUnbuiltImmovablePropertyRightsInRemImmovableProperty, "0"), -Number(CompensableLossesUnbuiltImmovablePropertyRightsInRemImmovablePropertyFivePreviousTaxPeriods, "0")), (decimal)0);
                if (!test)
                {
                    // MESSAGES
                    AddMessage(new BizTaxErrorDataContract
                    {
                        FileId = _fileId,
                        Id = "f-le-3011",
                        Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Het totaal vermeld in de rubriek '{Belastbare meerwaarden}'	({$TaxableRealisedCapitalGainsUnbuiltImmovablePropertyRightsInRemImmovableProperty})	is niet juist (= {$CalculatedAmountTaxableRealisedCapitalGainsUnbuiltImmovablePropertyRightsInRemImmovableProperty})."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Le total repris dans la rubrique '{Plus-values imposables}'	({$TaxableRealisedCapitalGainsUnbuiltImmovablePropertyRightsInRemImmovableProperty})	n'est pas correct (= {$CalculatedAmountTaxableRealisedCapitalGainsUnbuiltImmovablePropertyRightsInRemImmovableProperty})."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Der Betrag, der angegeben ist in der Rubrik '{Steuerpflichtige Mehrwerte}'	({$TaxableRealisedCapitalGainsUnbuiltImmovablePropertyRightsInRemImmovableProperty})	ist unzutreffend (= {$CalculatedAmountTaxableRealisedCapitalGainsUnbuiltImmovablePropertyRightsInRemImmovableProperty})."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Het totaal vermeld in de rubriek '{Belastbare meerwaarden}'	({$TaxableRealisedCapitalGainsUnbuiltImmovablePropertyRightsInRemImmovableProperty})	is niet juist (= {$CalculatedAmountTaxableRealisedCapitalGainsUnbuiltImmovablePropertyRightsInRemImmovableProperty})."}  
 }
                        ,
                        Fields = new List<string>()
                    }
                    );
                }
            }

        }

        internal void Calc_f_le_3012_assertion_set()
        {
            string scenarioId = GetScenarioId("");
            string period = "D";
            // SCENARIO: ""
            // PERIOD: "D"
            var CompensableLossesBuiltImmovablePropertyRightsInRemImmovablePropertyFivePreviousTaxPeriods = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:CompensableLossesBuiltImmovablePropertyRightsInRemImmovablePropertyFivePreviousTaxPeriods" });
            var NetRealisedCapitalGainsLossesBuiltImmovablePropertyRightsInRemImmovableProperty = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:NetRealisedCapitalGainsLossesBuiltImmovablePropertyRightsInRemImmovableProperty" });
            if (CompensableLossesBuiltImmovablePropertyRightsInRemImmovablePropertyFivePreviousTaxPeriods.Count > 0 || NetRealisedCapitalGainsLossesBuiltImmovablePropertyRightsInRemImmovableProperty.Count > 0)
            {
                //CALCULATION HERE
                //FORMULA HERE
                bool test = Number(CompensableLossesBuiltImmovablePropertyRightsInRemImmovablePropertyFivePreviousTaxPeriods) <= Number(NetRealisedCapitalGainsLossesBuiltImmovablePropertyRightsInRemImmovableProperty, "()");
                if (!test)
                {
                    // MESSAGES
                    AddMessage(new BizTaxErrorDataContract
                    {
                        FileId = _fileId,
                        Id = "f-le-3012",
                        Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Het bedrag ingevuld in de rubriek '{Saldo van de tijdens de vijf vorige belastbare tijdperken geleden en nog te compenseren verliezen op de vervreemding van dergelijke goederen of rechten, beperkt tot het positief nettoresultaat}' 	moet kleiner zijn dan of gelijk zijn aan het bedrag ingevuld in de rubriek '{Nettoresultaat van de meerwaarden en verliezen met betrekking tot dergelijke tijdens het belastbare tijdperk vervreemde goederen of rechten}'."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Le montant complété dans la rubrique '{Solde des pertes restant à compenser éprouvées au cours des cinq périodes imposables antérieures lors de l'aliénation de biens ou droits de l'espèce, à limiter au résultat positif net}' 	doit être égal ou inférieur au montant complété dans la rubrique '{Résultat net des plus-values et des pertes relatives à des biens ou droits de l'espèce qui ont été aliénés au cours de la période imposable}'."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Der in der Rubrik 	'{Restbetrag der im Laufe der fünf vorangehenden Besteuerungszeiträume erlittenen und noch auszugleichenden Verluste bei der Veräußerung von Gütern und Rechten dieser Art, begrenzt auf das positive Nettoergebnis}'  	angegebene Betrag darf den in der Rubrik 	'{Nettoergebnis der Mehrwerte und der Verluste in Bezug auf während des Besteuerungszeitraums veräußerte Güter oder Rechte dieser Art}'  	angegebenen Betrag nicht übersteigen."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Het bedrag ingevuld in de rubriek '{Saldo van de tijdens de vijf vorige belastbare tijdperken geleden en nog te compenseren verliezen op de vervreemding van dergelijke goederen of rechten, beperkt tot het positief nettoresultaat}' 	moet kleiner zijn dan of gelijk zijn aan het bedrag ingevuld in de rubriek '{Nettoresultaat van de meerwaarden en verliezen met betrekking tot dergelijke tijdens het belastbare tijdperk vervreemde goederen of rechten}'."}  
 }
                        ,
                        Fields = new List<string>()
                    }
                    );
                }
            }

        }

        internal void Calc_f_le_3013_assertion_set()
        {
            string scenarioId = GetScenarioId("");
            string period = "D";
            // SCENARIO: ""
            // PERIOD: "D"
            var TaxableRealisedCapitalGainsBuiltImmovablePropertyRightsInRemImmovableProperty = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:TaxableRealisedCapitalGainsBuiltImmovablePropertyRightsInRemImmovableProperty" });
            var NetRealisedCapitalGainsLossesBuiltImmovablePropertyRightsInRemImmovableProperty = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:NetRealisedCapitalGainsLossesBuiltImmovablePropertyRightsInRemImmovableProperty" });
            var CompensableLossesBuiltImmovablePropertyRightsInRemImmovablePropertyFivePreviousTaxPeriods = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:CompensableLossesBuiltImmovablePropertyRightsInRemImmovablePropertyFivePreviousTaxPeriods" });
            var FormulaTarget = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:TaxableRealisedCapitalGainsBuiltImmovablePropertyRightsInRemImmovableProperty" });
            if (TaxableRealisedCapitalGainsBuiltImmovablePropertyRightsInRemImmovableProperty.Count > 0 || NetRealisedCapitalGainsLossesBuiltImmovablePropertyRightsInRemImmovableProperty.Count > 0 || CompensableLossesBuiltImmovablePropertyRightsInRemImmovablePropertyFivePreviousTaxPeriods.Count > 0 || FormulaTarget.Count > 0)
            {
                var CalculatedAmountTaxableRealisedCapitalGainsBuiltImmovablePropertyRightsInRemImmovableProperty = Sum(Number(NetRealisedCapitalGainsLossesBuiltImmovablePropertyRightsInRemImmovableProperty, "0"), -Number(CompensableLossesBuiltImmovablePropertyRightsInRemImmovablePropertyFivePreviousTaxPeriods, "0"));
                //CALCULATION HERE
                if (FormulaTarget.Count == 0)
                {
                    FormulaTarget = CreateNewCalculationElement(scenarioId, period, "tax-inc:TaxableRealisedCapitalGainsBuiltImmovablePropertyRightsInRemImmovableProperty", new string[] { "" }, "EUR", "INF");
                }
                if (FormulaTarget.Count > 1)
                {
                    throw new Exception("More then one target for FormulaTarget in assertion f-le-3013");
                }
                FormulaTarget.First().Calculated = true;
                FormulaTarget.First().SetNumber(Max(Sum(Number(NetRealisedCapitalGainsLossesBuiltImmovablePropertyRightsInRemImmovableProperty, "0"), -Number(CompensableLossesBuiltImmovablePropertyRightsInRemImmovablePropertyFivePreviousTaxPeriods, "0")), (decimal)0));
                //FORMULA HERE
                bool test = Number(TaxableRealisedCapitalGainsBuiltImmovablePropertyRightsInRemImmovableProperty, "0") == Max(Sum(Number(NetRealisedCapitalGainsLossesBuiltImmovablePropertyRightsInRemImmovableProperty, "0"), -Number(CompensableLossesBuiltImmovablePropertyRightsInRemImmovablePropertyFivePreviousTaxPeriods, "0")), (decimal)0);
                if (!test)
                {
                    // MESSAGES
                    AddMessage(new BizTaxErrorDataContract
                    {
                        FileId = _fileId,
                        Id = "f-le-3013",
                        Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Het totaal vermeld in de rubriek '{Belastbare meerwaarden}'	({$TaxableRealisedCapitalGainsBuiltImmovablePropertyRightsInRemImmovableProperty})	is niet juist (= {$CalculatedAmountTaxableRealisedCapitalGainsBuiltImmovablePropertyRightsInRemImmovableProperty})."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Le total repris dans la rubrique '{Plus-values imposables}'	({$TaxableRealisedCapitalGainsBuiltImmovablePropertyRightsInRemImmovableProperty})	n'est pas correct (= {$CalculatedAmountTaxableRealisedCapitalGainsBuiltImmovablePropertyRightsInRemImmovableProperty})."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Der Betrag, der angegeben ist in der Rubrik '{Steuerpflichtige Mehrwerte}'	({$TaxableRealisedCapitalGainsBuiltImmovablePropertyRightsInRemImmovableProperty})	ist unzutreffend (= {$CalculatedAmountTaxableRealisedCapitalGainsBuiltImmovablePropertyRightsInRemImmovableProperty})."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Het totaal vermeld in de rubriek '{Belastbare meerwaarden}'	({$TaxableRealisedCapitalGainsBuiltImmovablePropertyRightsInRemImmovableProperty})	is niet juist (= {$CalculatedAmountTaxableRealisedCapitalGainsBuiltImmovablePropertyRightsInRemImmovableProperty})."}  
 }
                        ,
                        Fields = new List<string>()
                    }
                    );
                }
            }

        }

        internal void Calc_f_wddc_5001_assertion_set()
        {
            List<string> scenarioIds = GetScenarioIds(new string[] { "d-ec:ExemptionCategoryDimension::d-ec:UnalteredBalanceWriteDownDebtClaimMember/d-ty:WriteDownDebtClaimTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:RemainingBalanceAfterIncreaseWriteDownDebtClaimMember/d-ty:WriteDownDebtClaimTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:RemainingBalanceAfterDecreaseWriteDownDebtClaimMember/d-ty:WriteDownDebtClaimTypedDimension" });
            foreach (string scenarioId in scenarioIds)
            {
                string period = "D";
                // SCENARIO: "d-ec:ExemptionCategoryDimension::d-ec:UnalteredBalanceWriteDownDebtClaimMember/d-ty:WriteDownDebtClaimTypedDimension"
                // SCENARIO: "d-ec:ExemptionCategoryDimension::d-ec:RemainingBalanceAfterIncreaseWriteDownDebtClaimMember/d-ty:WriteDownDebtClaimTypedDimension"
                // SCENARIO: "d-ec:ExemptionCategoryDimension::d-ec:RemainingBalanceAfterDecreaseWriteDownDebtClaimMember/d-ty:WriteDownDebtClaimTypedDimension"
                // PERIOD: "D"
                var IdentityTradeDebtor = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:IdentityTradeDebtor" });
                var AddressTradeDebtor = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:AddressTradeDebtor" });
                var DebtClaimStart = GetElementsByRef(scenarioId, new string[] { "I-Start" }, new string[] { "tax-inc:DebtClaim" });
                var ExemptWriteDownDebtClaimStart = GetElementsByRef(scenarioId, new string[] { "I-Start" }, new string[] { "tax-inc:ExemptWriteDownDebtClaim" });
                if (ExemptWriteDownDebtClaimStart.Count > 0 || IdentityTradeDebtor.Count > 0 || AddressTradeDebtor.Count > 0 || DebtClaimStart.Count > 0)
                {
                    var PeriodStartDate = GetNodeByName("tax-inc", "PeriodStartDate");
                    //CALCULATION HERE
                    if (Number(ExemptWriteDownDebtClaimStart) > (decimal)0)
                    {
                        //FORMULA HERE
                        bool test = Exists(IdentityTradeDebtor) && Exists(AddressTradeDebtor) && (Number(DebtClaimStart, "()") > (decimal)0);
                        if (!test)
                        {
                            // MESSAGES
                            AddMessage(new BizTaxErrorDataContract
                            {
                                FileId = _fileId,
                                Id = "f-wddc-5001",
                                Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Voor een '{Vrijgestelde waardevermindering}' ({$ExemptWriteDownDebtClaimStart}) moeten de rubrieken 	'{Naam van de schuldenaar}', 	'{Adres van de schuldenaar}' 	en 	'{Schuldvordering}'	worden ingevuld."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Pour une '{Réduction de valeur exonérée}' ({$ExemptWriteDownDebtClaimStart}) les rubriques suivantes doivent obligatoirement être remplies: 	'{Nom du débiteur}', 	'{Adresse du débiteur}' 	et 	'{Créance}'."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Für eine '{Steuerbefreite Wertminderung}' ({$ExemptWriteDownDebtClaimStart}) sind die nachstehenden Rubriken auszufüllen: 	'{Name des Schuldners}', 	'{Adresse des Schuldners}' 	und 	'{Schuldforderung}'."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Voor een '{Vrijgestelde waardevermindering}' ({$ExemptWriteDownDebtClaimStart}) moeten de rubrieken 	'{Naam van de schuldenaar}', 	'{Adres van de schuldenaar}' 	en 	'{Schuldvordering}'	worden ingevuld."}  
 }
                                ,
                                Fields = new List<string>()
                            }
                            );
                        }
                    }
                }
            } // end scenario loop

        }

        internal void Calc_f_wddc_5002_assertion_set()
        {
            List<string> scenarioIds = GetScenarioIds(new string[] { "d-ec:ExemptionCategoryDimension::d-ec:NewWriteDownDebtClaimMember/d-ty:WriteDownDebtClaimTypedDimension" });
            foreach (string scenarioId in scenarioIds)
            {
                string period = "D";
                // SCENARIO: "d-ec:ExemptionCategoryDimension::d-ec:NewWriteDownDebtClaimMember/d-ty:WriteDownDebtClaimTypedDimension"
                // PERIOD: "D"
                var IdentityTradeDebtor = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:IdentityTradeDebtor" });
                var AddressTradeDebtor = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:AddressTradeDebtor" });
                var DebtClaimEnd = GetElementsByRef(scenarioId, new string[] { "I-End" }, new string[] { "tax-inc:DebtClaim" });
                var ExemptWriteDownDebtClaimEnd = GetElementsByRef(scenarioId, new string[] { "I-End" }, new string[] { "tax-inc:ExemptWriteDownDebtClaim" });
                if (ExemptWriteDownDebtClaimEnd.Count > 0 || IdentityTradeDebtor.Count > 0 || AddressTradeDebtor.Count > 0 || DebtClaimEnd.Count > 0)
                {
                    var PeriodEndDate = GetNodeByName("tax-inc", "PeriodEndDate");
                    //CALCULATION HERE
                    if (Exists(ExemptWriteDownDebtClaimEnd))
                    {
                        //FORMULA HERE
                        bool test = Exists(IdentityTradeDebtor) && Exists(AddressTradeDebtor) && (Number(DebtClaimEnd, "()") > (decimal)0);
                        if (!test)
                        {
                            // MESSAGES
                            AddMessage(new BizTaxErrorDataContract
                            {
                                FileId = _fileId,
                                Id = "f-wddc-5002",
                                Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Voor een '{Vrijgestelde waardevermindering}' ({$ExemptWriteDownDebtClaimEnd}) moeten de rubrieken 	'{Naam van de schuldenaar}', 	'{Adres van de schuldenaar}' 	en 	'{Schuldvordering}'	worden ingevuld."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Pour une '{Réduction de valeur exonérée}' ({$ExemptWriteDownDebtClaimEnd}) les rubriques suivantes doivent obligatoirement être remplies: 	'{Nom du débiteur}', 	'{Adresse du débiteur}' 	et 	'{Créance}'."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Für eine '{Steuerbefreite Wertminderung}' ({$ExemptWriteDownDebtClaimEnd}) sind die nachstehenden Rubriken auszufüllen: 	'{Name des Schuldners}', 	'{Adresse des Schuldners}' 	und 	'{Schuldforderung}'."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Voor een '{Vrijgestelde waardevermindering}' ({$ExemptWriteDownDebtClaimEnd}) moeten de rubrieken 	'{Naam van de schuldenaar}', 	'{Adres van de schuldenaar}' 	en 	'{Schuldvordering}'	worden ingevuld."}  
 }
                                ,
                                Fields = new List<string>()
                            }
                            );
                        }
                    }
                }
            } // end scenario loop

        }

        internal void Calc_f_wddc_5003_assertion_set()
        {
            List<string> scenarioIds = GetScenarioIds(new string[] { "d-ec:ExemptionCategoryDimension::d-ec:UnalteredBalanceWriteDownDebtClaimMember/d-ty:WriteDownDebtClaimTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:RemainingBalanceAfterIncreaseWriteDownDebtClaimMember/d-ty:WriteDownDebtClaimTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:RemainingBalanceAfterDecreaseWriteDownDebtClaimMember/d-ty:WriteDownDebtClaimTypedDimension" });
            foreach (string scenarioId in scenarioIds)
            {
                string period = "I-End";
                // SCENARIO: "d-ec:ExemptionCategoryDimension::d-ec:UnalteredBalanceWriteDownDebtClaimMember/d-ty:WriteDownDebtClaimTypedDimension"
                // SCENARIO: "d-ec:ExemptionCategoryDimension::d-ec:RemainingBalanceAfterIncreaseWriteDownDebtClaimMember/d-ty:WriteDownDebtClaimTypedDimension"
                // SCENARIO: "d-ec:ExemptionCategoryDimension::d-ec:RemainingBalanceAfterDecreaseWriteDownDebtClaimMember/d-ty:WriteDownDebtClaimTypedDimension"
                // PERIOD: "I-End"
                var ExemptWriteDownDebtClaimEnd = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:ExemptWriteDownDebtClaim" });
                var ExemptWriteDownDebtClaimStart = GetElementsByRef(scenarioId, new string[] { "I-Start" }, new string[] { "tax-inc:ExemptWriteDownDebtClaim" });
                var IncreaseExemptWriteDownDebtClaim = GetElementsByRef(scenarioId, new string[] { "D" }, new string[] { "tax-inc:IncreaseExemptWriteDownDebtClaim" });
                var DecreaseExemptWriteDownDebtClaimFinalWriteDownCurrentTaxPeriod = GetElementsByRef(scenarioId, new string[] { "D" }, new string[] { "tax-inc:DecreaseExemptWriteDownDebtClaimFinalWriteDownCurrentTaxPeriod" });
                var DecreaseExemptWriteDownDebtClaimFullPartialCollectionCurrentTaxPeriod = GetElementsByRef(scenarioId, new string[] { "D" }, new string[] { "tax-inc:DecreaseExemptWriteDownDebtClaimFullPartialCollectionCurrentTaxPeriod" });
                var DecreaseExemptWriteDownDebtClaimNewEstimationProbableLoss = GetElementsByRef(scenarioId, new string[] { "D" }, new string[] { "tax-inc:DecreaseExemptWriteDownDebtClaimNewEstimationProbableLoss" });
                if (ExemptWriteDownDebtClaimStart.Count > 0 || ExemptWriteDownDebtClaimEnd.Count > 0 || IncreaseExemptWriteDownDebtClaim.Count > 0 || DecreaseExemptWriteDownDebtClaimFinalWriteDownCurrentTaxPeriod.Count > 0 || DecreaseExemptWriteDownDebtClaimFullPartialCollectionCurrentTaxPeriod.Count > 0 || DecreaseExemptWriteDownDebtClaimNewEstimationProbableLoss.Count > 0)
                {
                    var PeriodStartDate = GetNodeByName("tax-inc", "PeriodStartDate");
                    var PeriodEndDate = GetNodeByName("tax-inc", "PeriodEndDate");
                    var CalculatedAmountMovementExemptWriteDownDebtClaim = Sum(Number(ExemptWriteDownDebtClaimEnd, "0"), -Number(ExemptWriteDownDebtClaimStart, "0"));
                    var CalculatedAmountMovementsExemptWriteDownDebtClaim = Sum(Number(IncreaseExemptWriteDownDebtClaim, "0"), -Number(DecreaseExemptWriteDownDebtClaimFinalWriteDownCurrentTaxPeriod, "0"), -Number(DecreaseExemptWriteDownDebtClaimFullPartialCollectionCurrentTaxPeriod, "0"), -Number(DecreaseExemptWriteDownDebtClaimNewEstimationProbableLoss, "0"));
                    //CALCULATION HERE
                    //FORMULA HERE
                    bool test = Sum(Number(ExemptWriteDownDebtClaimEnd, "0"), -Number(ExemptWriteDownDebtClaimStart, "0")) == Sum(Number(IncreaseExemptWriteDownDebtClaim, "0"), -Number(DecreaseExemptWriteDownDebtClaimFinalWriteDownCurrentTaxPeriod, "0"), -Number(DecreaseExemptWriteDownDebtClaimFullPartialCollectionCurrentTaxPeriod, "0"), -Number(DecreaseExemptWriteDownDebtClaimNewEstimationProbableLoss, "0"));
                    if (!test)
                    {
                        // MESSAGES
                        AddMessage(new BizTaxErrorDataContract
                        {
                            FileId = _fileId,
                            Id = "f-wddc-5003",
                            Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Het verschil tussen de vrijgestelde waardevermindering, bij het begin van het boekjaar en op het einde van het boekjaar (= {$CalculatedAmountMovementExemptWriteDownDebtClaim}) moet overeenstemmen met de bedragen ingevuld als vermindering of verhoging (= {$CalculatedAmountMovementsExemptWriteDownDebtClaim})."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "La différence entre la réduction de valeur exonérée au début et à la fin de l'exercice (= {$CalculatedAmountMovementExemptWriteDownDebtClaim}) doit correspondre aux montants complétés comme augmentation ou diminution (= {$CalculatedAmountMovementsExemptWriteDownDebtClaim})."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Der Unterschied zwischen den steuerfreien Wertminderungen am Anfang und am Ende des Geschäftsjahres (= {$CalculatedAmountMovementExemptWriteDownDebtClaim}) muss den als Minderung oder Erhöhung (= {$CalculatedAmountMovementsExemptWriteDownDebtClaim}) angegebenen Beträgen entsprechen."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Het verschil tussen de vrijgestelde waardevermindering, bij het begin van het boekjaar en op het einde van het boekjaar (= {$CalculatedAmountMovementExemptWriteDownDebtClaim}) moet overeenstemmen met de bedragen ingevuld als vermindering of verhoging (= {$CalculatedAmountMovementsExemptWriteDownDebtClaim})."}  
 }
                            ,
                            Fields = new List<string>()
                        }
                        );
                    }
                }
            } // end scenario loop

        }

        internal void Calc_f_wddc_5004_assertion_set()
        {
            List<string> scenarioIds = GetScenarioIds(new string[] { "d-ec:ExemptionCategoryDimension::d-ec:UnalteredBalanceWriteDownDebtClaimMember/d-ty:WriteDownDebtClaimTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:RemainingBalanceAfterIncreaseWriteDownDebtClaimMember/d-ty:WriteDownDebtClaimTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:RemainingBalanceAfterDecreaseWriteDownDebtClaimMember/d-ty:WriteDownDebtClaimTypedDimension" });
            foreach (string scenarioId in scenarioIds)
            {
                string period = "I-Start";
                // SCENARIO: "d-ec:ExemptionCategoryDimension::d-ec:UnalteredBalanceWriteDownDebtClaimMember/d-ty:WriteDownDebtClaimTypedDimension"
                // SCENARIO: "d-ec:ExemptionCategoryDimension::d-ec:RemainingBalanceAfterIncreaseWriteDownDebtClaimMember/d-ty:WriteDownDebtClaimTypedDimension"
                // SCENARIO: "d-ec:ExemptionCategoryDimension::d-ec:RemainingBalanceAfterDecreaseWriteDownDebtClaimMember/d-ty:WriteDownDebtClaimTypedDimension"
                // PERIOD: "I-Start"
                var ExemptWriteDownDebtClaimStart = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:ExemptWriteDownDebtClaim" });
                var DebtClaimStart = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:DebtClaim" });
                if (ExemptWriteDownDebtClaimStart.Count > 0 || DebtClaimStart.Count > 0)
                {
                    var PeriodStartDate = GetNodeByName("tax-inc", "PeriodStartDate");
                    //CALCULATION HERE
                    //FORMULA HERE
                    bool test = Number(ExemptWriteDownDebtClaimStart, "0") <= Number(DebtClaimStart, "0");
                    if (!test)
                    {
                        // MESSAGES
                        AddMessage(new BizTaxErrorDataContract
                        {
                            FileId = _fileId,
                            Id = "f-wddc-5004",
                            Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "De rubriek '{Vrijgestelde waardevermindering}, bij het begin van het boekjaar' ({$ExemptWriteDownDebtClaimStart}) moet kleiner zijn dan of gelijk zijn aan de rubriek '{Schuldvordering}, bij het begin van het boekjaar' ({$DebtClaimStart})."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "La rubrique '{Réduction de valeur exonérée}, au début de l'exercice' ({$ExemptWriteDownDebtClaimStart}) doit être égale ou inférieure à la rubrique '{Créance}, au début de l'exercice' ({$DebtClaimStart})."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Die Rubrik '{Steuerbefreite Wertminderung}, am Anfang des Rechnungsjahres' ({$ExemptWriteDownDebtClaimStart}) muss niedriger sein als die Rubrik '{Schuldforderung}, am Anfang des Rechnungsjahres' ({$DebtClaimStart}) oder dieser entsprechen."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "De rubriek '{Vrijgestelde waardevermindering}, bij het begin van het boekjaar' ({$ExemptWriteDownDebtClaimStart}) moet kleiner zijn dan of gelijk zijn aan de rubriek '{Schuldvordering}, bij het begin van het boekjaar' ({$DebtClaimStart})."}  
 }
                            ,
                            Fields = new List<string>()
                        }
                        );
                    }
                }
            } // end scenario loop

        }

        internal void Calc_f_wddc_5005_assertion_set()
        {
            List<string> scenarioIds = GetScenarioIds(new string[] { "d-ec:ExemptionCategoryDimension::d-ec:NewWriteDownDebtClaimMember/d-ty:WriteDownDebtClaimTypedDimension" });
            foreach (string scenarioId in scenarioIds)
            {
                string period = "I-Start";
                // SCENARIO: "d-ec:ExemptionCategoryDimension::d-ec:NewWriteDownDebtClaimMember/d-ty:WriteDownDebtClaimTypedDimension"
                // PERIOD: "I-Start"
                var ExemptWriteDownDebtClaimStart = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:ExemptWriteDownDebtClaim" });
                var DebtClaimStart = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:DebtClaim" });
                if (ExemptWriteDownDebtClaimStart.Count > 0 || DebtClaimStart.Count > 0)
                {
                    var PeriodStartDate = GetNodeByName("tax-inc", "PeriodStartDate");
                    //CALCULATION HERE
                    //FORMULA HERE
                    bool test = (Number(ExemptWriteDownDebtClaimStart, "0") == (decimal)0) && (Number(DebtClaimStart, "0") == (decimal)0);
                    if (!test)
                    {
                        // MESSAGES
                        AddMessage(new BizTaxErrorDataContract
                        {
                            FileId = _fileId,
                            Id = "f-wddc-5005",
                            Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Voor een nieuwe '{Vrijgestelde waardevermindering}' mogen de rubrieken '{Vrijgestelde waardevermindering}, bij het begin van het boekjaar' ({$ExemptWriteDownDebtClaimStart}) en '{Schuldvordering}, bij het begin van het boekjaar' ({$DebtClaimStart}) niet worden ingevuld."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Pour une nouvelle '{Réduction de valeur exonérée}' les rubriques suivantes ne peuvent pas être remplies: '{Réduction de valeur exonérée}, au début de l'exercice' ({$ExemptWriteDownDebtClaimStart}) et '{Créance}, au début de l'exercice' ({$DebtClaimStart})."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Für eine neue '{Steuerbefreite Wertminderung}' dürfen die nachstehenden Rubriken nicht ausgefüllt werden: '{Steuerbefreite Wertminderung}, am Anfang des Rechnungsjahres' ({$ExemptWriteDownDebtClaimStart}) und '{Schuldforderung}, am Anfang des Rechnungsjahres' ({$DebtClaimStart})."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Voor een nieuwe '{Vrijgestelde waardevermindering}' mogen de rubrieken '{Vrijgestelde waardevermindering}, bij het begin van het boekjaar' ({$ExemptWriteDownDebtClaimStart}) en '{Schuldvordering}, bij het begin van het boekjaar' ({$DebtClaimStart}) niet worden ingevuld."}  
 }
                            ,
                            Fields = new List<string>()
                        }
                        );
                    }
                }
            } // end scenario loop

        }

        internal void Calc_f_wddc_5006_assertion_set()
        {
            List<string> scenarioIds = GetScenarioIds(new string[] { "d-ec:ExemptionCategoryDimension::d-ec:UnalteredBalanceWriteDownDebtClaimMember/d-ty:WriteDownDebtClaimTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:RemainingBalanceAfterIncreaseWriteDownDebtClaimMember/d-ty:WriteDownDebtClaimTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:RemainingBalanceAfterDecreaseWriteDownDebtClaimMember/d-ty:WriteDownDebtClaimTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:NewWriteDownDebtClaimMember/d-ty:WriteDownDebtClaimTypedDimension" });
            foreach (string scenarioId in scenarioIds)
            {
                string period = "I-End";
                // SCENARIO: "d-ec:ExemptionCategoryDimension::d-ec:UnalteredBalanceWriteDownDebtClaimMember/d-ty:WriteDownDebtClaimTypedDimension"
                // SCENARIO: "d-ec:ExemptionCategoryDimension::d-ec:RemainingBalanceAfterIncreaseWriteDownDebtClaimMember/d-ty:WriteDownDebtClaimTypedDimension"
                // SCENARIO: "d-ec:ExemptionCategoryDimension::d-ec:RemainingBalanceAfterDecreaseWriteDownDebtClaimMember/d-ty:WriteDownDebtClaimTypedDimension"
                // SCENARIO: "d-ec:ExemptionCategoryDimension::d-ec:NewWriteDownDebtClaimMember/d-ty:WriteDownDebtClaimTypedDimension"
                // PERIOD: "I-End"
                var ExemptWriteDownDebtClaimEnd = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:ExemptWriteDownDebtClaim" });
                var DebtClaimEnd = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:DebtClaim" });
                if (ExemptWriteDownDebtClaimEnd.Count > 0 || DebtClaimEnd.Count > 0)
                {
                    var PeriodEndDate = GetNodeByName("tax-inc", "PeriodEndDate");
                    //CALCULATION HERE
                    //FORMULA HERE
                    bool test = Number(ExemptWriteDownDebtClaimEnd, "0") <= Number(DebtClaimEnd, "0");
                    if (!test)
                    {
                        // MESSAGES
                        AddMessage(new BizTaxErrorDataContract
                        {
                            FileId = _fileId,
                            Id = "f-wddc-5006",
                            Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "De rubriek '{Vrijgestelde waardevermindering}' ({$ExemptWriteDownDebtClaimEnd}) moet kleiner zijn dan of gelijk zijn aan de rubriek '{Schuldvordering}, op het einde van het boekjaar' ({$DebtClaimEnd})."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "La rubrique '{Réduction de valeur exonérée}' ({$ExemptWriteDownDebtClaimEnd}) doit être égale ou inférieure à la rubrique '{Créance}, à la fin de l'exercice' ({$DebtClaimEnd})."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Die Rubrik '{Steuerbefreite Wertminderung}' ({$ExemptWriteDownDebtClaimEnd}) muss niedriger sein als die Rubrik'{Schuldforderung}, am Ende des Rechnungsjahres' ({$DebtClaimEnd}) oder dieser entsprechen."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "De rubriek '{Vrijgestelde waardevermindering}' ({$ExemptWriteDownDebtClaimEnd}) moet kleiner zijn dan of gelijk zijn aan de rubriek '{Schuldvordering}, op het einde van het boekjaar' ({$DebtClaimEnd})."}  
 }
                            ,
                            Fields = new List<string>()
                        }
                        );
                    }
                }
            } // end scenario loop

        }

        internal void Calc_f_wddc_5007_assertion_set()
        {
            // HAS NO UNBOUND SEQUENCES
            var ExemptWriteDownDebtClaimStart = GetElementsByScenDefs(new string[] { "d-ec:ExemptionCategoryDimension::d-ec:UnalteredBalanceWriteDownDebtClaimMember/d-ty:WriteDownDebtClaimTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:RemainingBalanceAfterIncreaseWriteDownDebtClaimMember/d-ty:WriteDownDebtClaimTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:RemainingBalanceAfterDecreaseWriteDownDebtClaimMember/d-ty:WriteDownDebtClaimTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:NewWriteDownDebtClaimMember/d-ty:WriteDownDebtClaimTypedDimension" }, new string[] { "I-Start" }, new string[] { "tax-inc:ExemptWriteDownDebtClaim" });
            var ExemptWriteDownDebtClaimStartTotal = GetElementsByScenDefs(new string[] { "" }, new string[] { "I-Start" }, new string[] { "tax-inc:ExemptWriteDownDebtClaim" });
            var FormulaTarget = GetElementsByScenDefs(new string[] { "" }, new string[] { "I-Start" }, new string[] { "tax-inc:ExemptWriteDownDebtClaim" });
            if (ExemptWriteDownDebtClaimStart.Count > 0 || ExemptWriteDownDebtClaimStartTotal.Count > 0 || FormulaTarget.Count > 0)
            {
                var PeriodStartDate = GetNodeByName("tax-inc", "PeriodStartDate");
                var CalculatedAmountExemptWriteDownDebtClaimStartTotal = Sum(Number(ExemptWriteDownDebtClaimStart, "0"));
                //CALCULATION HERE
                if (FormulaTarget.Count == 0)
                {
                    FormulaTarget = CreateNewCalculationElement(null, new string[] { "I-Start" }, "tax-inc:ExemptWriteDownDebtClaim", new string[] { "" }, "EUR", "INF");
                }
                if (FormulaTarget.Count > 1)
                {
                    throw new Exception("More then one target for FormulaTarget in assertion f-wddc-5007");
                }
                FormulaTarget.First().Calculated = true;
                FormulaTarget.First().SetNumber(Sum(Number(ExemptWriteDownDebtClaimStart, "0")));
                //FORMULA HERE
                bool test = Sum(Number(ExemptWriteDownDebtClaimStart, "0")) == Number(ExemptWriteDownDebtClaimStartTotal, "0");
                if (!test)
                {
                    // MESSAGES
                    AddMessage(new BizTaxErrorDataContract
                    {
                        FileId = _fileId,
                        Id = "f-wddc-5007",
                        Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Het totaal vermeld in de kolom '{Vrijgestelde waardevermindering}' ({$ExemptWriteDownDebtClaimStartTotal}) is niet juist (= {$CalculatedAmountExemptWriteDownDebtClaimStartTotal})."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Le total repris dans la colonne '{Réduction de valeur exonérée}' ({$ExemptWriteDownDebtClaimStartTotal}) n'est pas correct (= {$CalculatedAmountExemptWriteDownDebtClaimStartTotal})."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Der in der Spalte '{Steuerbefreite Wertminderung}' ({$ExemptWriteDownDebtClaimStartTotal}) angegebene Gesamtbetrag ist nicht korrekt (= {$CalculatedAmountExemptWriteDownDebtClaimStartTotal})."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Het totaal vermeld in de kolom '{Vrijgestelde waardevermindering}' ({$ExemptWriteDownDebtClaimStartTotal}) is niet juist (= {$CalculatedAmountExemptWriteDownDebtClaimStartTotal})."}  
 }
                        ,
                        Fields = new List<string>()
                    }
                    );
                }
            }

        }

        internal void Calc_f_wddc_5008_assertion_set()
        {
            // HAS NO UNBOUND SEQUENCES
            var ExemptWriteDownDebtClaimEnd = GetElementsByScenDefs(new string[] { "d-ec:ExemptionCategoryDimension::d-ec:UnalteredBalanceWriteDownDebtClaimMember/d-ty:WriteDownDebtClaimTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:RemainingBalanceAfterIncreaseWriteDownDebtClaimMember/d-ty:WriteDownDebtClaimTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:RemainingBalanceAfterDecreaseWriteDownDebtClaimMember/d-ty:WriteDownDebtClaimTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:NewWriteDownDebtClaimMember/d-ty:WriteDownDebtClaimTypedDimension" }, new string[] { "I-End" }, new string[] { "tax-inc:ExemptWriteDownDebtClaim" });
            var ExemptWriteDownDebtClaimEndTotal = GetElementsByScenDefs(new string[] { "" }, new string[] { "I-End" }, new string[] { "tax-inc:ExemptWriteDownDebtClaim" });
            var FormulaTarget = GetElementsByScenDefs(new string[] { "" }, new string[] { "I-End" }, new string[] { "tax-inc:ExemptWriteDownDebtClaim" });
            if (ExemptWriteDownDebtClaimEnd.Count > 0 || ExemptWriteDownDebtClaimEndTotal.Count > 0 || FormulaTarget.Count > 0)
            {
                var PeriodEndDate = GetNodeByName("tax-inc", "PeriodEndDate");
                var CalculatedAmountExemptWriteDownDebtClaimEndTotal = Sum(Number(ExemptWriteDownDebtClaimEnd, "0"));
                //CALCULATION HERE
                if (FormulaTarget.Count == 0)
                {
                    FormulaTarget = CreateNewCalculationElement(null, new string[] { "I-End" }, "tax-inc:ExemptWriteDownDebtClaim", new string[] { "" }, "EUR", "INF");
                }
                if (FormulaTarget.Count > 1)
                {
                    throw new Exception("More then one target for FormulaTarget in assertion f-wddc-5008");
                }
                FormulaTarget.First().Calculated = true;
                FormulaTarget.First().SetNumber(Sum(Number(ExemptWriteDownDebtClaimEnd, "0")));
                //FORMULA HERE
                bool test = Sum(Number(ExemptWriteDownDebtClaimEnd, "0")) == Number(ExemptWriteDownDebtClaimEndTotal, "0");
                if (!test)
                {
                    // MESSAGES
                    AddMessage(new BizTaxErrorDataContract
                    {
                        FileId = _fileId,
                        Id = "f-wddc-5008",
                        Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Het totaal vermeld in de kolom '{Vrijgestelde waardevermindering}' ({$ExemptWriteDownDebtClaimEndTotal}) is niet juist (= {$CalculatedAmountExemptWriteDownDebtClaimEndTotal})."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Le total repris dans la colonne '{Réduction de valeur exonérée}' ({$ExemptWriteDownDebtClaimEndTotal}) n'est pas correct (= {$CalculatedAmountExemptWriteDownDebtClaimEndTotal})."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Der in der Spalte '{Steuerbefreite Wertminderung}' ({$ExemptWriteDownDebtClaimEndTotal}) angegebene Gesamtbetrag ist nicht korrekt (= {$CalculatedAmountExemptWriteDownDebtClaimEndTotal})."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Het totaal vermeld in de kolom '{Vrijgestelde waardevermindering}' ({$ExemptWriteDownDebtClaimEndTotal}) is niet juist (= {$CalculatedAmountExemptWriteDownDebtClaimEndTotal})."}  
 }
                        ,
                        Fields = new List<string>()
                    }
                    );
                }
            }

        }

        internal void Calc_f_prre_5050_assertion_set()
        {
            List<string> scenarioIds = GetScenarioIds(new string[] { "d-ec:ExemptionCategoryDimension::d-ec:UnalteredBalanceProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ExpensesCompensationCoveredCurrentTaxPeriodMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:UnalteredBalanceProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ExpensesOverhaulImportantRepairsMaximum10YearsMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:UnalteredBalanceProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ProfessionalExpensesCurrentTaxPeriodMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:RemainingBalanceAfterDecreaseProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ExpensesCompensationCoveredCurrentTaxPeriodMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:RemainingBalanceAfterDecreaseProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ExpensesOverhaulImportantRepairsMaximum10YearsMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:RemainingBalanceAfterDecreaseProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ProfessionalExpensesCurrentTaxPeriodMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:RemainingBalanceAfterIncreaseProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ExpensesCompensationCoveredCurrentTaxPeriodMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:RemainingBalanceAfterIncreaseProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ExpensesOverhaulImportantRepairsMaximum10YearsMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:RemainingBalanceAfterIncreaseProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ProfessionalExpensesCurrentTaxPeriodMember/d-ty:ProvisionRiskExpenseTypedDimension" });
            foreach (string scenarioId in scenarioIds)
            {
                string period = "I-End";
                // SCENARIO: "d-ec:ExemptionCategoryDimension::d-ec:UnalteredBalanceProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ExpensesCompensationCoveredCurrentTaxPeriodMember/d-ty:ProvisionRiskExpenseTypedDimension"
                // SCENARIO: "d-ec:ExemptionCategoryDimension::d-ec:UnalteredBalanceProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ExpensesOverhaulImportantRepairsMaximum10YearsMember/d-ty:ProvisionRiskExpenseTypedDimension"
                // SCENARIO: "d-ec:ExemptionCategoryDimension::d-ec:UnalteredBalanceProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ProfessionalExpensesCurrentTaxPeriodMember/d-ty:ProvisionRiskExpenseTypedDimension"
                // SCENARIO: "d-ec:ExemptionCategoryDimension::d-ec:RemainingBalanceAfterDecreaseProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ExpensesCompensationCoveredCurrentTaxPeriodMember/d-ty:ProvisionRiskExpenseTypedDimension"
                // SCENARIO: "d-ec:ExemptionCategoryDimension::d-ec:RemainingBalanceAfterDecreaseProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ExpensesOverhaulImportantRepairsMaximum10YearsMember/d-ty:ProvisionRiskExpenseTypedDimension"
                // SCENARIO: "d-ec:ExemptionCategoryDimension::d-ec:RemainingBalanceAfterDecreaseProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ProfessionalExpensesCurrentTaxPeriodMember/d-ty:ProvisionRiskExpenseTypedDimension"
                // SCENARIO: "d-ec:ExemptionCategoryDimension::d-ec:RemainingBalanceAfterIncreaseProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ExpensesCompensationCoveredCurrentTaxPeriodMember/d-ty:ProvisionRiskExpenseTypedDimension"
                // SCENARIO: "d-ec:ExemptionCategoryDimension::d-ec:RemainingBalanceAfterIncreaseProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ExpensesOverhaulImportantRepairsMaximum10YearsMember/d-ty:ProvisionRiskExpenseTypedDimension"
                // SCENARIO: "d-ec:ExemptionCategoryDimension::d-ec:RemainingBalanceAfterIncreaseProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ProfessionalExpensesCurrentTaxPeriodMember/d-ty:ProvisionRiskExpenseTypedDimension"
                // PERIOD: "I-End"
                var ExemptProvisionRisksExpensesEnd = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:ExemptProvisionRisksExpenses" });
                var ExemptProvisionRisksExpensesStart = GetElementsByRef(scenarioId, new string[] { "I-Start" }, new string[] { "tax-inc:ExemptProvisionRisksExpenses" });
                var IncreaseExemptProvisionRisksExpenses = GetElementsByRef(scenarioId, new string[] { "D" }, new string[] { "tax-inc:IncreaseExemptProvisionRisksExpenses" });
                var DecreaseExemptProvisionRisksExpensesBorneExpensesCurrentTaxPeriod = GetElementsByRef(scenarioId, new string[] { "D" }, new string[] { "tax-inc:DecreaseExemptProvisionRisksExpensesBorneExpensesCurrentTaxPeriod" });
                var DecreaseExemptProvisionRisksExpensesNewEstimationProbableExpenses = GetElementsByRef(scenarioId, new string[] { "D" }, new string[] { "tax-inc:DecreaseExemptProvisionRisksExpensesNewEstimationProbableExpenses" });
                if (ExemptProvisionRisksExpensesStart.Count > 0 || ExemptProvisionRisksExpensesEnd.Count > 0 || IncreaseExemptProvisionRisksExpenses.Count > 0 || DecreaseExemptProvisionRisksExpensesBorneExpensesCurrentTaxPeriod.Count > 0 || DecreaseExemptProvisionRisksExpensesNewEstimationProbableExpenses.Count > 0)
                {
                    var PeriodStartDate = GetNodeByName("tax-inc", "PeriodStartDate");
                    var PeriodEndDate = GetNodeByName("tax-inc", "PeriodEndDate");
                    var CalculatedAmountExemptProvisionRisksExpenses = Sum(Number(ExemptProvisionRisksExpensesEnd, "()"), -Number(ExemptProvisionRisksExpensesStart, "()"));
                    var CalculatedAmountDecreaseExemptProvisionRisksExpenses = Sum(Number(DecreaseExemptProvisionRisksExpensesBorneExpensesCurrentTaxPeriod, "()"), Number(DecreaseExemptProvisionRisksExpensesNewEstimationProbableExpenses, "()"));
                    //CALCULATION HERE
                    //FORMULA HERE
                    bool test = (Sum(Number(ExemptProvisionRisksExpensesEnd, "()"), -Number(ExemptProvisionRisksExpensesStart, "()")) == Number(IncreaseExemptProvisionRisksExpenses, "()")) || (Sum(Number(ExemptProvisionRisksExpensesEnd, "()"), -Number(ExemptProvisionRisksExpensesStart, "()")) == Sum(-Number(DecreaseExemptProvisionRisksExpensesBorneExpensesCurrentTaxPeriod, "()"), -Number(DecreaseExemptProvisionRisksExpensesNewEstimationProbableExpenses, "()")));
                    if (!test)
                    {
                        // MESSAGES
                        AddMessage(new BizTaxErrorDataContract
                        {
                            FileId = _fileId,
                            Id = "f-prre-5050",
                            Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Het verschil tussen de vrijgestelde voorziening bij het begin van het belastbare tijdperk ({$ExemptProvisionRisksExpensesStart}) en op het einde van het belastbare tijdperk ({$ExemptProvisionRisksExpensesEnd}) 	moet bij een vermindering (= {$CalculatedAmountDecreaseExemptProvisionRisksExpenses}) overeenstemmen met de 	'{Vermindering van de vrijgestelde voorziening ingevolge kosten die tijdens het boekjaar effectief gedragen zijn}' 	en/of 	'{Vermindering van de vrijgestelde voorziening ingevolge een nieuwe schatting van de waarschijnlijke kosten tijdens het boekjaar}', 	OF 	bij een verhoging met het bedrag ingevuld in de kolom 	'{Verhoging van de vrijgestelde voorziening}' ({$IncreaseExemptProvisionRisksExpenses})  (= {$CalculatedAmountExemptProvisionRisksExpenses})."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "La différence entre la provision exonérée au début de l'exercice imposable ({$ExemptProvisionRisksExpensesStart}) et à la fin de l'exercice imposable ({$ExemptProvisionRisksExpensesEnd}) 	doit correspondre en cas de diminution (= {$CalculatedAmountDecreaseExemptProvisionRisksExpenses}) à la 	'{Diminution de la provision exonérée résultant de la prise en charge effective au cours de l'exercice comptable}' 	et/ou la  	'{Diminution de la provision exonérée résultant d'une nouvelle estimation de la charge probable au cours de l'exercice comptable}', 	OU en cas d'augmentation au montant complété dans la colonne 		'{Augmentation de la provision exonérée}' ({$IncreaseExemptProvisionRisksExpenses})  (= {$CalculatedAmountExemptProvisionRisksExpenses})."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Der Unterschied zwischen den steuerfreien Rückstellungen am Anfang ({$ExemptProvisionRisksExpensesStart}) und am Ende ({$ExemptProvisionRisksExpensesEnd}) 	des Besteuerungszeitraums muss im Falle einer Verringerung (= {$CalculatedAmountDecreaseExemptProvisionRisksExpenses}) der Rubrik 	'{Die Verringerung, die sich aus den während des Rechnungsjahres, worauf sich das Verzeichnis bezieht, effektiv getragenen Kosten ergeben}' 	und/oder 	'{Die Verringerung, die sich aus einer Neueinschätzung der wahrscheinlichen Kosten ergeben}', 	entsprechen ODER im Falle einer Erhöhung dem in der Spalte	'{Erhöhung der steuerbefreiten Rückstellung}' ({$IncreaseExemptProvisionRisksExpenses})  (= {$CalculatedAmountExemptProvisionRisksExpenses}) angegebenen Betrag entsprechen."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Het verschil tussen de vrijgestelde voorziening bij het begin van het belastbare tijdperk ({$ExemptProvisionRisksExpensesStart}) en op het einde van het belastbare tijdperk ({$ExemptProvisionRisksExpensesEnd}) 	moet bij een vermindering (= {$CalculatedAmountDecreaseExemptProvisionRisksExpenses}) overeenstemmen met de 	'{Vermindering van de vrijgestelde voorziening ingevolge kosten die tijdens het boekjaar effectief gedragen zijn}' 	en/of 	'{Vermindering van de vrijgestelde voorziening ingevolge een nieuwe schatting van de waarschijnlijke kosten tijdens het boekjaar}', 	OF 	bij een verhoging met het bedrag ingevuld in de kolom 	'{Verhoging van de vrijgestelde voorziening}' ({$IncreaseExemptProvisionRisksExpenses})  (= {$CalculatedAmountExemptProvisionRisksExpenses})."}  
 }
                            ,
                            Fields = new List<string>()
                        }
                        );
                    }
                }
            } // end scenario loop

        }

        internal void Calc_f_prre_5051_assertion_set()
        {
            // HAS NO UNBOUND SEQUENCES
            var ExemptProvisionRisksExpensesStart = GetElementsByScenDefs(new string[] { "d-ec:ExemptionCategoryDimension::d-ec:NewProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ExpensesCompensationCoveredCurrentTaxPeriodMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:NewProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ExpensesOverhaulImportantRepairsMaximum10YearsMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:NewProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ProfessionalExpensesCurrentTaxPeriodMember/d-ty:ProvisionRiskExpenseTypedDimension" }, new string[] { "I-Start" }, new string[] { "tax-inc:ExemptProvisionRisksExpenses" });
            if (ExemptProvisionRisksExpensesStart.Count > 0)
            {
                var PeriodStartDate = GetNodeByName("tax-inc", "PeriodStartDate");
                //CALCULATION HERE
                //FORMULA HERE
                bool test = Number(ExemptProvisionRisksExpensesStart, "()") == (decimal)0;
                if (!test)
                {
                    // MESSAGES
                    AddMessage(new BizTaxErrorDataContract
                    {
                        FileId = _fileId,
                        Id = "f-prre-5051",
                        Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Voor een nieuwe vrijgestelde voorziening mag de rubriek '{Vrijgestelde voorziening}' ({$ExemptProvisionRisksExpensesStart}) bij het begin van het boekjaar niet worden ingevuld."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Pour une nouvelle provision exonérée; la rubrique suivante ne peut pas être remplie: '{Provision exonérée}' ({$ExemptProvisionRisksExpensesStart}) au début de l'exercice."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Für eine neue steuerbefreite Rückstellung darf die nachstehende Rubrik nicht ausgefüllt werden: '{Steuerbefreite Rückstellung}' ({$ExemptProvisionRisksExpensesStart}) am Anfang des Rechnungsjahres."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Voor een nieuwe vrijgestelde voorziening mag de rubriek '{Vrijgestelde voorziening}' ({$ExemptProvisionRisksExpensesStart}) bij het begin van het boekjaar niet worden ingevuld."}  
 }
                        ,
                        Fields = new List<string>()
                    }
                    );
                }
            }

        }

        internal void Calc_f_prre_5052_assertion_set()
        {
            List<string> scenarioIds = GetScenarioIds(new string[] { "d-ec:ExemptionCategoryDimension::d-ec:UnalteredBalanceProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ExpensesCompensationCoveredCurrentTaxPeriodMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:UnalteredBalanceProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ExpensesOverhaulImportantRepairsMaximum10YearsMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:UnalteredBalanceProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ProfessionalExpensesCurrentTaxPeriodMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:RemainingBalanceAfterDecreaseProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ExpensesCompensationCoveredCurrentTaxPeriodMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:RemainingBalanceAfterDecreaseProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ExpensesOverhaulImportantRepairsMaximum10YearsMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:RemainingBalanceAfterDecreaseProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ProfessionalExpensesCurrentTaxPeriodMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:RemainingBalanceAfterIncreaseProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ExpensesCompensationCoveredCurrentTaxPeriodMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:RemainingBalanceAfterIncreaseProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ExpensesOverhaulImportantRepairsMaximum10YearsMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:RemainingBalanceAfterIncreaseProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ProfessionalExpensesCurrentTaxPeriodMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:NewProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ExpensesCompensationCoveredCurrentTaxPeriodMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:NewProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ExpensesOverhaulImportantRepairsMaximum10YearsMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:NewProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ProfessionalExpensesCurrentTaxPeriodMember/d-ty:ProvisionRiskExpenseTypedDimension" });
            foreach (string scenarioId in scenarioIds)
            {
                string period = "I-End";
                // SCENARIO: "d-ec:ExemptionCategoryDimension::d-ec:UnalteredBalanceProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ExpensesCompensationCoveredCurrentTaxPeriodMember/d-ty:ProvisionRiskExpenseTypedDimension"
                // SCENARIO: "d-ec:ExemptionCategoryDimension::d-ec:UnalteredBalanceProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ExpensesOverhaulImportantRepairsMaximum10YearsMember/d-ty:ProvisionRiskExpenseTypedDimension"
                // SCENARIO: "d-ec:ExemptionCategoryDimension::d-ec:UnalteredBalanceProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ProfessionalExpensesCurrentTaxPeriodMember/d-ty:ProvisionRiskExpenseTypedDimension"
                // SCENARIO: "d-ec:ExemptionCategoryDimension::d-ec:RemainingBalanceAfterDecreaseProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ExpensesCompensationCoveredCurrentTaxPeriodMember/d-ty:ProvisionRiskExpenseTypedDimension"
                // SCENARIO: "d-ec:ExemptionCategoryDimension::d-ec:RemainingBalanceAfterDecreaseProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ExpensesOverhaulImportantRepairsMaximum10YearsMember/d-ty:ProvisionRiskExpenseTypedDimension"
                // SCENARIO: "d-ec:ExemptionCategoryDimension::d-ec:RemainingBalanceAfterDecreaseProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ProfessionalExpensesCurrentTaxPeriodMember/d-ty:ProvisionRiskExpenseTypedDimension"
                // SCENARIO: "d-ec:ExemptionCategoryDimension::d-ec:RemainingBalanceAfterIncreaseProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ExpensesCompensationCoveredCurrentTaxPeriodMember/d-ty:ProvisionRiskExpenseTypedDimension"
                // SCENARIO: "d-ec:ExemptionCategoryDimension::d-ec:RemainingBalanceAfterIncreaseProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ExpensesOverhaulImportantRepairsMaximum10YearsMember/d-ty:ProvisionRiskExpenseTypedDimension"
                // SCENARIO: "d-ec:ExemptionCategoryDimension::d-ec:RemainingBalanceAfterIncreaseProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ProfessionalExpensesCurrentTaxPeriodMember/d-ty:ProvisionRiskExpenseTypedDimension"
                // SCENARIO: "d-ec:ExemptionCategoryDimension::d-ec:NewProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ExpensesCompensationCoveredCurrentTaxPeriodMember/d-ty:ProvisionRiskExpenseTypedDimension"
                // SCENARIO: "d-ec:ExemptionCategoryDimension::d-ec:NewProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ExpensesOverhaulImportantRepairsMaximum10YearsMember/d-ty:ProvisionRiskExpenseTypedDimension"
                // SCENARIO: "d-ec:ExemptionCategoryDimension::d-ec:NewProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ProfessionalExpensesCurrentTaxPeriodMember/d-ty:ProvisionRiskExpenseTypedDimension"
                // PERIOD: "I-End"
                var ExemptProvisionRisksExpensesEnd = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:ExemptProvisionRisksExpenses" });
                var ProbableCostEnd = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:ProbableCost" });
                if (ExemptProvisionRisksExpensesEnd.Count > 0 || ProbableCostEnd.Count > 0)
                {
                    var PeriodEndDate = GetNodeByName("tax-inc", "PeriodEndDate");
                    //CALCULATION HERE
                    //FORMULA HERE
                    bool test = Number(ExemptProvisionRisksExpensesEnd, "0") <= Number(ProbableCostEnd, "()");
                    if (!test)
                    {
                        // MESSAGES
                        AddMessage(new BizTaxErrorDataContract
                        {
                            FileId = _fileId,
                            Id = "f-prre-5052",
                            Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Het bedrag ingevuld in de rubriek '{Vrijgestelde voorziening}' ({$ExemptProvisionRisksExpensesEnd}) moet kleiner zijn dan of gelijk zijn aan het bedrag ingevuld in de rubriek '{Waarschijnlijke kost}' ({$ProbableCostEnd})."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Le montant repris dans la rubrique '{Provision exonérée}' ({$ExemptProvisionRisksExpensesEnd}) doit être égal ou inférieur au montant repris dans la rubrique '{Charge probable}' ({$ProbableCostEnd})."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Der in der Rubrik '{Steuerbefreite Rückstellung}' angegebene Betrag ({$ExemptProvisionRisksExpensesEnd}) muss gleich oder niedriger sein als der in der Rubrik '{Voraussichtliche Kosten}' ausgefüllte Betrag ({$ProbableCostEnd})."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Het bedrag ingevuld in de rubriek '{Vrijgestelde voorziening}' ({$ExemptProvisionRisksExpensesEnd}) moet kleiner zijn dan of gelijk zijn aan het bedrag ingevuld in de rubriek '{Waarschijnlijke kost}' ({$ProbableCostEnd})."}  
 }
                            ,
                            Fields = new List<string>()
                        }
                        );
                    }
                }
            } // end scenario loop

        }

        internal void Calc_f_prre_5053_assertion_set()
        {
            // HAS NO UNBOUND SEQUENCES
            var ExemptProvisionRisksExpensesStart = GetElementsByScenDefs(new string[] { "d-ec:ExemptionCategoryDimension::d-ec:UnalteredBalanceProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ExpensesCompensationCoveredCurrentTaxPeriodMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:UnalteredBalanceProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ExpensesOverhaulImportantRepairsMaximum10YearsMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:UnalteredBalanceProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ProfessionalExpensesCurrentTaxPeriodMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:RemainingBalanceAfterDecreaseProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ExpensesCompensationCoveredCurrentTaxPeriodMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:RemainingBalanceAfterDecreaseProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ExpensesOverhaulImportantRepairsMaximum10YearsMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:RemainingBalanceAfterDecreaseProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ProfessionalExpensesCurrentTaxPeriodMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:RemainingBalanceAfterIncreaseProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ExpensesCompensationCoveredCurrentTaxPeriodMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:RemainingBalanceAfterIncreaseProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ExpensesOverhaulImportantRepairsMaximum10YearsMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:RemainingBalanceAfterIncreaseProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ProfessionalExpensesCurrentTaxPeriodMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:NewProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ExpensesCompensationCoveredCurrentTaxPeriodMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:NewProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ExpensesOverhaulImportantRepairsMaximum10YearsMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:NewProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ProfessionalExpensesCurrentTaxPeriodMember/d-ty:ProvisionRiskExpenseTypedDimension" }, new string[] { "I-Start" }, new string[] { "tax-inc:ExemptProvisionRisksExpenses" });
            var ExemptProvisionRisksExpensesStartTotal = GetElementsByScenDefs(new string[] { "" }, new string[] { "I-Start" }, new string[] { "tax-inc:ExemptProvisionRisksExpenses" });
            var FormulaTarget = GetElementsByScenDefs(new string[] { "" }, new string[] { "I-Start" }, new string[] { "tax-inc:ExemptProvisionRisksExpenses" });
            if (ExemptProvisionRisksExpensesStart.Count > 0 || ExemptProvisionRisksExpensesStartTotal.Count > 0 || FormulaTarget.Count > 0)
            {
                var PeriodStartDate = GetNodeByName("tax-inc", "PeriodStartDate");
                var CalculatedAmountExemptProvisionRisksExpensesStartTotal = Sum(Number(ExemptProvisionRisksExpensesStart, "()"));
                //CALCULATION HERE
                if (FormulaTarget.Count == 0)
                {
                    FormulaTarget = CreateNewCalculationElement(null, new string[] { "I-Start" }, "tax-inc:ExemptProvisionRisksExpenses", new string[] { "" }, "EUR", "INF");
                }
                if (FormulaTarget.Count > 1)
                {
                    throw new Exception("More then one target for FormulaTarget in assertion f-prre-5053");
                }
                FormulaTarget.First().Calculated = true;
                FormulaTarget.First().SetNumber(Sum(Number(ExemptProvisionRisksExpensesStart, "()")));
                //FORMULA HERE
                bool test = Sum(Number(ExemptProvisionRisksExpensesStart, "()")) == Number(ExemptProvisionRisksExpensesStartTotal, "()");
                if (!test)
                {
                    // MESSAGES
                    AddMessage(new BizTaxErrorDataContract
                    {
                        FileId = _fileId,
                        Id = "f-prre-5053",
                        Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Het totaal vermeld in de rubriek '{Vrijgestelde voorziening}' ({$ExemptProvisionRisksExpensesStartTotal}) is niet juist (= {$CalculatedAmountExemptProvisionRisksExpensesStartTotal})."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Le total repris dans la rubrique '{Provision exonérée}' ({$ExemptProvisionRisksExpensesStartTotal}) n'est pas correct (= {$CalculatedAmountExemptProvisionRisksExpensesStartTotal})."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Der in der Rubrik '{Steuerbefreite Rückstellung}' angegebene Gesamtbetrag ({$ExemptProvisionRisksExpensesStartTotal}) ist unzutreffend (= {$CalculatedAmountExemptProvisionRisksExpensesStartTotal})."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Het totaal vermeld in de rubriek '{Vrijgestelde voorziening}' ({$ExemptProvisionRisksExpensesStartTotal}) is niet juist (= {$CalculatedAmountExemptProvisionRisksExpensesStartTotal})."}  
 }
                        ,
                        Fields = new List<string>()
                    }
                    );
                }
            }

        }

        internal void Calc_f_prre_5054_assertion_set()
        {
            // HAS NO UNBOUND SEQUENCES
            var ExemptProvisionRisksExpensesEnd = GetElementsByScenDefs(new string[] { "d-ec:ExemptionCategoryDimension::d-ec:UnalteredBalanceProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ExpensesCompensationCoveredCurrentTaxPeriodMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:UnalteredBalanceProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ExpensesOverhaulImportantRepairsMaximum10YearsMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:UnalteredBalanceProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ProfessionalExpensesCurrentTaxPeriodMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:RemainingBalanceAfterDecreaseProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ExpensesCompensationCoveredCurrentTaxPeriodMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:RemainingBalanceAfterDecreaseProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ExpensesOverhaulImportantRepairsMaximum10YearsMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:RemainingBalanceAfterDecreaseProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ProfessionalExpensesCurrentTaxPeriodMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:RemainingBalanceAfterIncreaseProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ExpensesCompensationCoveredCurrentTaxPeriodMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:RemainingBalanceAfterIncreaseProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ExpensesOverhaulImportantRepairsMaximum10YearsMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:RemainingBalanceAfterIncreaseProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ProfessionalExpensesCurrentTaxPeriodMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:NewProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ExpensesCompensationCoveredCurrentTaxPeriodMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:NewProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ExpensesOverhaulImportantRepairsMaximum10YearsMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:NewProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ProfessionalExpensesCurrentTaxPeriodMember/d-ty:ProvisionRiskExpenseTypedDimension" }, new string[] { "I-End" }, new string[] { "tax-inc:ExemptProvisionRisksExpenses" });
            var ExemptProvisionRisksExpensesEndTotal = GetElementsByScenDefs(new string[] { "" }, new string[] { "I-End" }, new string[] { "tax-inc:ExemptProvisionRisksExpenses" });
            var FormulaTarget = GetElementsByScenDefs(new string[] { "" }, new string[] { "I-End" }, new string[] { "tax-inc:ExemptProvisionRisksExpenses" });
            if (ExemptProvisionRisksExpensesEnd.Count > 0 || ExemptProvisionRisksExpensesEndTotal.Count > 0 || FormulaTarget.Count > 0)
            {
                var PeriodEndDate = GetNodeByName("tax-inc", "PeriodEndDate");
                var CalculatedAmountExemptProvisionRisksExpensesEndTotal = Sum(Number(ExemptProvisionRisksExpensesEnd, "()"));
                //CALCULATION HERE
                if (FormulaTarget.Count == 0)
                {
                    FormulaTarget = CreateNewCalculationElement(null, new string[] { "I-End" }, "tax-inc:ExemptProvisionRisksExpenses", new string[] { "" }, "EUR", "INF");
                }
                if (FormulaTarget.Count > 1)
                {
                    throw new Exception("More then one target for FormulaTarget in assertion f-prre-5054");
                }
                FormulaTarget.First().Calculated = true;
                FormulaTarget.First().SetNumber(Sum(Number(ExemptProvisionRisksExpensesEnd, "()")));
                //FORMULA HERE
                bool test = Sum(Number(ExemptProvisionRisksExpensesEnd, "()")) == Number(ExemptProvisionRisksExpensesEndTotal, "()");
                if (!test)
                {
                    // MESSAGES
                    AddMessage(new BizTaxErrorDataContract
                    {
                        FileId = _fileId,
                        Id = "f-prre-5054",
                        Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Het totaal vermeld in de rubriek '{Vrijgestelde voorziening}' ({$ExemptProvisionRisksExpensesEndTotal}) is niet juist (= {$CalculatedAmountExemptProvisionRisksExpensesEndTotal})."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Le total repris dans la rubrique '{Provision exonérée}' ({$ExemptProvisionRisksExpensesEndTotal}) n'est pas correct (= {$CalculatedAmountExemptProvisionRisksExpensesEndTotal})."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Der in der Rubrik '{Steuerbefreite Rückstellung}' angegebene Gesamtbetrag ({$ExemptProvisionRisksExpensesEndTotal}) ist unzutreffend (= {$CalculatedAmountExemptProvisionRisksExpensesEndTotal})."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Het totaal vermeld in de rubriek '{Vrijgestelde voorziening}' ({$ExemptProvisionRisksExpensesEndTotal}) is niet juist (= {$CalculatedAmountExemptProvisionRisksExpensesEndTotal})."}  
 }
                        ,
                        Fields = new List<string>()
                    }
                    );
                }
            }

        }

        internal void Calc_f_prre_5055_assertion_set()
        {
            // HAS NO UNBOUND SEQUENCES
            var ProbableCostEnd = GetElementsByScenDefs(new string[] { "d-ec:ExemptionCategoryDimension::d-ec:UnalteredBalanceProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ExpensesCompensationCoveredCurrentTaxPeriodMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:UnalteredBalanceProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ExpensesOverhaulImportantRepairsMaximum10YearsMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:UnalteredBalanceProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ProfessionalExpensesCurrentTaxPeriodMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:RemainingBalanceAfterDecreaseProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ExpensesCompensationCoveredCurrentTaxPeriodMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:RemainingBalanceAfterDecreaseProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ExpensesOverhaulImportantRepairsMaximum10YearsMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:RemainingBalanceAfterDecreaseProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ProfessionalExpensesCurrentTaxPeriodMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:RemainingBalanceAfterIncreaseProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ExpensesCompensationCoveredCurrentTaxPeriodMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:RemainingBalanceAfterIncreaseProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ExpensesOverhaulImportantRepairsMaximum10YearsMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:RemainingBalanceAfterIncreaseProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ProfessionalExpensesCurrentTaxPeriodMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:NewProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ExpensesCompensationCoveredCurrentTaxPeriodMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:NewProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ExpensesOverhaulImportantRepairsMaximum10YearsMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:NewProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ProfessionalExpensesCurrentTaxPeriodMember/d-ty:ProvisionRiskExpenseTypedDimension" }, new string[] { "I-End" }, new string[] { "tax-inc:ProbableCost" });
            var ExemptProvisionRisksExpensesEnd = GetElementsByScenDefs(new string[] { "d-ec:ExemptionCategoryDimension::d-ec:UnalteredBalanceProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ExpensesCompensationCoveredCurrentTaxPeriodMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:UnalteredBalanceProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ExpensesOverhaulImportantRepairsMaximum10YearsMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:UnalteredBalanceProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ProfessionalExpensesCurrentTaxPeriodMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:RemainingBalanceAfterDecreaseProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ExpensesCompensationCoveredCurrentTaxPeriodMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:RemainingBalanceAfterDecreaseProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ExpensesOverhaulImportantRepairsMaximum10YearsMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:RemainingBalanceAfterDecreaseProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ProfessionalExpensesCurrentTaxPeriodMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:RemainingBalanceAfterIncreaseProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ExpensesCompensationCoveredCurrentTaxPeriodMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:RemainingBalanceAfterIncreaseProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ExpensesOverhaulImportantRepairsMaximum10YearsMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:RemainingBalanceAfterIncreaseProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ProfessionalExpensesCurrentTaxPeriodMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:NewProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ExpensesCompensationCoveredCurrentTaxPeriodMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:NewProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ExpensesOverhaulImportantRepairsMaximum10YearsMember/d-ty:ProvisionRiskExpenseTypedDimension", "d-ec:ExemptionCategoryDimension::d-ec:NewProvisionRisksExpensesMember/d-expt:ExpenseTypeDimension::d-expt:ProfessionalExpensesCurrentTaxPeriodMember/d-ty:ProvisionRiskExpenseTypedDimension" }, new string[] { "I-End" }, new string[] { "tax-inc:ExemptProvisionRisksExpenses" });
            if (ExemptProvisionRisksExpensesEnd.Count > 0 || ProbableCostEnd.Count > 0)
            {
                var PeriodEndDate = GetNodeByName("tax-inc", "PeriodEndDate");
                //CALCULATION HERE
                if (Number(ExemptProvisionRisksExpensesEnd, "()") > (decimal)0)
                {
                    //FORMULA HERE
                    bool test = Number(ProbableCostEnd, "0") > (decimal)0;
                    if (!test)
                    {
                        // MESSAGES
                        AddMessage(new BizTaxErrorDataContract
                        {
                            FileId = _fileId,
                            Id = "f-prre-5055",
                            Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Voor elke '{Vrijgestelde voorziening}' op het einde van het boekjaar ({$ExemptProvisionRisksExpensesEnd}) moet de rubriek '{Waarschijnlijke kost}' op het einde van het boekjaar ({$ProbableCostEnd}) worden ingevuld."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Pour chaque '{Provision exonérée}' à la fin de l'exercice ({$ExemptProvisionRisksExpensesEnd}) la rubrique suivante est obligatoire à remplir: '{Charge probable}' à la fin de l'exercice ({$ProbableCostEnd})."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Für eine '{Steuerbefreite Rückstellung}' am Ende des Rechnungsjahres ({$ExemptProvisionRisksExpensesEnd}) ist die nachstehende Rubrik auszufüllen: '{Voraussichtliche Kosten}' am Ende des Rechnungsjahres ({$ProbableCostEnd})."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Voor elke '{Vrijgestelde voorziening}' op het einde van het boekjaar ({$ExemptProvisionRisksExpensesEnd}) moet de rubriek '{Waarschijnlijke kost}' op het einde van het boekjaar ({$ProbableCostEnd}) worden ingevuld."}  
 }
                            ,
                            Fields = new List<string>()
                        }
                        );
                    }
                }
            }

        }

        internal void Calc_f_cg_5101_assertion_set()
        {

        }

        internal void Calc_f_cg_5102_assertion_set()
        {

        }

        internal void Calc_f_cg_5103a_assertion_set()
        {

        }

        internal void Calc_f_cg_5103b_assertion_set()
        {

        }

        internal void Calc_f_cg_5104a_assertion_set()
        {

        }

        internal void Calc_f_cg_5104b_assertion_set()
        {

        }

        internal void Calc_f_cg_5105_assertion_set()
        {

        }

        internal void Calc_f_cg_5121_assertion_set()
        {

        }

        internal void Calc_f_cg_5131_assertion_set()
        {

        }

        internal void Calc_f_cg_5141_assertion_set()
        {
            List<string> scenarioIds = GetScenarioIds(new string[] { "d-asst:AssetTypeDimension::d-asst:SpecificSecurityMember/d-ty:CapitalGainTypedDimension" });
            foreach (string scenarioId in scenarioIds)
            {
                string period = "D";
                // SCENARIO: "d-asst:AssetTypeDimension::d-asst:SpecificSecurityMember/d-ty:CapitalGainTypedDimension"
                // PERIOD: "D"
                var FiscalNetWorth = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:FiscalNetWorth" });
                var AcquisitionValue = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:AcquisitionValue" });
                var TaxAcceptedWriteDownsInvestment = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:TaxAcceptedWriteDownsInvestment" });
                var FormulaTarget = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:FiscalNetWorth" });
                if (FiscalNetWorth.Count > 0 || AcquisitionValue.Count > 0 || TaxAcceptedWriteDownsInvestment.Count > 0 || FormulaTarget.Count > 0)
                {
                    var CalculatedAmountFiscalNetWorth = Max(Sum(Number(AcquisitionValue, "0"), -Number(TaxAcceptedWriteDownsInvestment, "0")), (decimal)0);
                    //CALCULATION HERE
                    if (FormulaTarget.Count == 0)
                    {
                        FormulaTarget = CreateNewCalculationElement(scenarioId, period, "tax-inc:FiscalNetWorth", new string[] { "d-asst:AssetTypeDimension::d-asst:SpecificSecurityMember/d-ty:CapitalGainTypedDimension" }, "EUR", "INF");
                    }
                    if (FormulaTarget.Count > 1)
                    {
                        throw new Exception("More then one target for FormulaTarget in assertion f-cg-5141");
                    }
                    FormulaTarget.First().Calculated = true;
                    FormulaTarget.First().SetNumber(Max(Sum(Number(AcquisitionValue, "0"), -Number(TaxAcceptedWriteDownsInvestment, "0")), (decimal)0));
                    //FORMULA HERE
                    bool test = Number(FiscalNetWorth, "0") == Max(Sum(Number(AcquisitionValue, "0"), -Number(TaxAcceptedWriteDownsInvestment, "0")), (decimal)0);
                    if (!test)
                    {
                        // MESSAGES
                        AddMessage(new BizTaxErrorDataContract
                        {
                            FileId = _fileId,
                            Id = "f-cg-5141",
                            Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Het totaal vermeld in de rubriek '{Fiscale nettowaarde}' ({$FiscalNetWorth}) van een gespreid te belasten meerwaarde op bepaalde effecten is niet juist (= {$CalculatedAmountFiscalNetWorth})."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Le total repris dans la rubrique '{Valeur fiscale nette}' ({$FiscalNetWorth}) d'une plus-value sur certains titres n'est pas correct (= {$CalculatedAmountFiscalNetWorth})."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Der in der Rubrik '{Steuerlicher Nettowert}' angegebene Gesamtbetrag ({$FiscalNetWorth}), gestaffelt zu versteuernder Mehrwert auf bestimmte Wertpapiere, ist unzutreffend (= {$CalculatedAmountFiscalNetWorth})."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Het totaal vermeld in de rubriek '{Fiscale nettowaarde}' ({$FiscalNetWorth}) van een gespreid te belasten meerwaarde op bepaalde effecten is niet juist (= {$CalculatedAmountFiscalNetWorth})."}  
 }
                            ,
                            Fields = new List<string>()
                        }
                        );
                    }
                }
            } // end scenario loop

        }

        internal void Calc_f_cg_5142_assertion_set()
        {
            List<string> scenarioIds = GetScenarioIds(new string[] { "d-asst:AssetTypeDimension::d-asst:TangibleIntangibleFixedAssetsMember/d-ty:CapitalGainTypedDimension" });
            foreach (string scenarioId in scenarioIds)
            {
                string period = "D";
                // SCENARIO: "d-asst:AssetTypeDimension::d-asst:TangibleIntangibleFixedAssetsMember/d-ty:CapitalGainTypedDimension"
                // PERIOD: "D"
                var FiscalNetWorth = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:FiscalNetWorth" });
                var AcquisitionInvestmentValue = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:AcquisitionInvestmentValue" });
                var TaxAcceptedDepreciationsWriteDownsInvestment = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:TaxAcceptedDepreciationsWriteDownsInvestment" });
                var FormulaTarget = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:FiscalNetWorth" });
                if (FiscalNetWorth.Count > 0 || AcquisitionInvestmentValue.Count > 0 || TaxAcceptedDepreciationsWriteDownsInvestment.Count > 0 || FormulaTarget.Count > 0)
                {
                    var CalculatedAmountFiscalNetWorth = Max(Sum(Number(AcquisitionInvestmentValue, "0"), -Number(TaxAcceptedDepreciationsWriteDownsInvestment, "0")), (decimal)0);
                    //CALCULATION HERE
                    if (FormulaTarget.Count == 0)
                    {
                        FormulaTarget = CreateNewCalculationElement(scenarioId, period, "tax-inc:FiscalNetWorth", new string[] { "d-asst:AssetTypeDimension::d-asst:TangibleIntangibleFixedAssetsMember/d-ty:CapitalGainTypedDimension" }, "EUR", "INF");
                    }
                    if (FormulaTarget.Count > 1)
                    {
                        throw new Exception("More then one target for FormulaTarget in assertion f-cg-5142");
                    }
                    FormulaTarget.First().Calculated = true;
                    FormulaTarget.First().SetNumber(Max(Sum(Number(AcquisitionInvestmentValue, "0"), -Number(TaxAcceptedDepreciationsWriteDownsInvestment, "0")), (decimal)0));
                    //FORMULA HERE
                    bool test = Number(FiscalNetWorth, "0") == Max(Sum(Number(AcquisitionInvestmentValue, "0"), -Number(TaxAcceptedDepreciationsWriteDownsInvestment, "0")), (decimal)0);
                    if (!test)
                    {
                        // MESSAGES
                        AddMessage(new BizTaxErrorDataContract
                        {
                            FileId = _fileId,
                            Id = "f-cg-5142",
                            Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Het totaal vermeld in de rubriek '{Fiscale nettowaarde}' ({$FiscalNetWorth}) van een meerwaarde op een materiële en immateriële vaste activa is niet juist (= {$CalculatedAmountFiscalNetWorth})."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Le total repris dans la rubrique '{Valeur fiscale nette}' ({$FiscalNetWorth}) d'une plus-value sur des immobilisations corporelles et incorporelles n'est pas correct (= {$CalculatedAmountFiscalNetWorth})."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Der in der Rubrik '{Steuerlicher Nettowert}' angegebene Gesamtbetrag ({$FiscalNetWorth}), Mehrwert auf immaterielle Anlagen und Sachanlagen, ist unzutreffend (= {$CalculatedAmountFiscalNetWorth})."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Het totaal vermeld in de rubriek '{Fiscale nettowaarde}' ({$FiscalNetWorth}) van een meerwaarde op een materiële en immateriële vaste activa is niet juist (= {$CalculatedAmountFiscalNetWorth})."}  
 }
                            ,
                            Fields = new List<string>()
                        }
                        );
                    }
                }
            } // end scenario loop

        }

        internal void Calc_f_cg_5143_assertion_set()
        {
            List<string> scenarioIds = GetScenarioIds(new string[] { "d-asst:AssetTypeDimension::d-asst:CorporateVehicleMember/d-ty:CapitalGainTypedDimension" });
            foreach (string scenarioId in scenarioIds)
            {
                string period = "D";
                // SCENARIO: "d-asst:AssetTypeDimension::d-asst:CorporateVehicleMember/d-ty:CapitalGainTypedDimension"
                // PERIOD: "D"
                var FiscalNetWorth = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:FiscalNetWorth" });
                var AcquisitionInvestmentValue = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:AcquisitionInvestmentValue" });
                var TaxAcceptedDepreciationsWriteDownsInvestment = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:TaxAcceptedDepreciationsWriteDownsInvestment" });
                var FormulaTarget = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:FiscalNetWorth" });
                if (FiscalNetWorth.Count > 0 || AcquisitionInvestmentValue.Count > 0 || TaxAcceptedDepreciationsWriteDownsInvestment.Count > 0 || FormulaTarget.Count > 0)
                {
                    var CalculatedAmountFiscalNetWorth = Max(Sum(Number(AcquisitionInvestmentValue, "0"), -Number(TaxAcceptedDepreciationsWriteDownsInvestment, "0")), (decimal)0);
                    //CALCULATION HERE
                    if (FormulaTarget.Count == 0)
                    {
                        FormulaTarget = CreateNewCalculationElement(scenarioId, period, "tax-inc:FiscalNetWorth", new string[] { "d-asst:AssetTypeDimension::d-asst:CorporateVehicleMember/d-ty:CapitalGainTypedDimension" }, "EUR", "INF");
                    }
                    if (FormulaTarget.Count > 1)
                    {
                        throw new Exception("More then one target for FormulaTarget in assertion f-cg-5143");
                    }
                    FormulaTarget.First().Calculated = true;
                    FormulaTarget.First().SetNumber(Max(Sum(Number(AcquisitionInvestmentValue, "0"), -Number(TaxAcceptedDepreciationsWriteDownsInvestment, "0")), (decimal)0));
                    //FORMULA HERE
                    bool test = Number(FiscalNetWorth, "0") == Max(Sum(Number(AcquisitionInvestmentValue, "0"), -Number(TaxAcceptedDepreciationsWriteDownsInvestment, "0")), (decimal)0);
                    if (!test)
                    {
                        // MESSAGES
                        AddMessage(new BizTaxErrorDataContract
                        {
                            FileId = _fileId,
                            Id = "f-cg-5143",
                            Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Het totaal vermeld in de rubriek '{Fiscale nettowaarde}' ({$FiscalNetWorth}) van een meerwaarde op bedrijfsvoertuigen is niet juist (= {$CalculatedAmountFiscalNetWorth})."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Le total repris dans la rubrique '{Valeur fiscale nette}' ({$FiscalNetWorth}) d'une plus-value sur véhicules d'entreprises n'est pas correct (= {$CalculatedAmountFiscalNetWorth})."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Der in der Rubrik '{Steuerlicher Nettowert}' angegebene Gesamtbetrag ({$FiscalNetWorth}), Mehrwert auf Betriebsfahrzeuge, ist unzutreffend (= {$CalculatedAmountFiscalNetWorth})."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Het totaal vermeld in de rubriek '{Fiscale nettowaarde}' ({$FiscalNetWorth}) van een meerwaarde op bedrijfsvoertuigen is niet juist (= {$CalculatedAmountFiscalNetWorth})."}  
 }
                            ,
                            Fields = new List<string>()
                        }
                        );
                    }
                }
            } // end scenario loop

        }

        internal void Calc_f_cg_5144_assertion_set()
        {
            List<string> scenarioIds = GetScenarioIds(new string[] { "d-asst:AssetTypeDimension::d-asst:RiverVesselMember/d-ty:CapitalGainTypedDimension" });
            foreach (string scenarioId in scenarioIds)
            {
                string period = "D";
                // SCENARIO: "d-asst:AssetTypeDimension::d-asst:RiverVesselMember/d-ty:CapitalGainTypedDimension"
                // PERIOD: "D"
                var FiscalNetWorth = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:FiscalNetWorth" });
                var AcquisitionInvestmentValue = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:AcquisitionInvestmentValue" });
                var TaxAcceptedDepreciationsWriteDownsInvestment = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:TaxAcceptedDepreciationsWriteDownsInvestment" });
                var FormulaTarget = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:FiscalNetWorth" });
                if (FiscalNetWorth.Count > 0 || AcquisitionInvestmentValue.Count > 0 || TaxAcceptedDepreciationsWriteDownsInvestment.Count > 0 || FormulaTarget.Count > 0)
                {
                    var CalculatedAmountFiscalNetWorth = Max(Sum(Number(AcquisitionInvestmentValue, "0"), -Number(TaxAcceptedDepreciationsWriteDownsInvestment, "0")), (decimal)0);
                    //CALCULATION HERE
                    if (FormulaTarget.Count == 0)
                    {
                        FormulaTarget = CreateNewCalculationElement(scenarioId, period, "tax-inc:FiscalNetWorth", new string[] { "d-asst:AssetTypeDimension::d-asst:RiverVesselMember/d-ty:CapitalGainTypedDimension" }, "EUR", "INF");
                    }
                    if (FormulaTarget.Count > 1)
                    {
                        throw new Exception("More then one target for FormulaTarget in assertion f-cg-5144");
                    }
                    FormulaTarget.First().Calculated = true;
                    FormulaTarget.First().SetNumber(Max(Sum(Number(AcquisitionInvestmentValue, "0"), -Number(TaxAcceptedDepreciationsWriteDownsInvestment, "0")), (decimal)0));
                    //FORMULA HERE
                    bool test = Number(FiscalNetWorth, "0") == Max(Sum(Number(AcquisitionInvestmentValue, "0"), -Number(TaxAcceptedDepreciationsWriteDownsInvestment, "0")), (decimal)0);
                    if (!test)
                    {
                        // MESSAGES
                        AddMessage(new BizTaxErrorDataContract
                        {
                            FileId = _fileId,
                            Id = "f-cg-5144",
                            Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Het totaal vermeld in de rubriek '{Fiscale nettowaarde}' ({$FiscalNetWorth}) van een meerwaarde op binnenschepen voor de commerciële vaart is niet juist (= {$CalculatedAmountFiscalNetWorth})."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Le total repris dans la rubrique '{Valeur fiscale nette}' ({$FiscalNetWorth}) d'une plus-value sur bateaux de navigation intérieure n'est pas correct (= {$CalculatedAmountFiscalNetWorth})."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Der in der Rubrik '{Steuerlicher Nettowert}' angegebene Gesamtbetrag ({$FiscalNetWorth}), Mehrwert auf Binnenschiffe für die gewerbliche Binnenschifffahrt, ist unzutreffend (= {$CalculatedAmountFiscalNetWorth})."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Het totaal vermeld in de rubriek '{Fiscale nettowaarde}' ({$FiscalNetWorth}) van een meerwaarde op binnenschepen voor de commerciële vaart is niet juist (= {$CalculatedAmountFiscalNetWorth})."}  
 }
                            ,
                            Fields = new List<string>()
                        }
                        );
                    }
                }
            } // end scenario loop

        }

        internal void Calc_f_cg_5145_assertion_set()
        {
            List<string> scenarioIds = GetScenarioIds(new string[] { "d-asst:AssetTypeDimension::d-asst:SeaVesselMember/d-ty:CapitalGainTypedDimension" });
            foreach (string scenarioId in scenarioIds)
            {
                string period = "D";
                // SCENARIO: "d-asst:AssetTypeDimension::d-asst:SeaVesselMember/d-ty:CapitalGainTypedDimension"
                // PERIOD: "D"
                var FiscalNetWorth = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:FiscalNetWorth" });
                var AcquisitionInvestmentValue = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:AcquisitionInvestmentValue" });
                var TaxAcceptedDepreciationsWriteDownsInvestment = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:TaxAcceptedDepreciationsWriteDownsInvestment" });
                var FormulaTarget = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:FiscalNetWorth" });
                if (FiscalNetWorth.Count > 0 || AcquisitionInvestmentValue.Count > 0 || TaxAcceptedDepreciationsWriteDownsInvestment.Count > 0 || FormulaTarget.Count > 0)
                {
                    var CalculatedAmountFiscalNetWorth = Max(Sum(Number(AcquisitionInvestmentValue, "0"), -Number(TaxAcceptedDepreciationsWriteDownsInvestment, "0")), (decimal)0);
                    //CALCULATION HERE
                    if (FormulaTarget.Count == 0)
                    {
                        FormulaTarget = CreateNewCalculationElement(scenarioId, period, "tax-inc:FiscalNetWorth", new string[] { "d-asst:AssetTypeDimension::d-asst:SeaVesselMember/d-ty:CapitalGainTypedDimension" }, "EUR", "INF");
                    }
                    if (FormulaTarget.Count > 1)
                    {
                        throw new Exception("More then one target for FormulaTarget in assertion f-cg-5145");
                    }
                    FormulaTarget.First().Calculated = true;
                    FormulaTarget.First().SetNumber(Max(Sum(Number(AcquisitionInvestmentValue, "0"), -Number(TaxAcceptedDepreciationsWriteDownsInvestment, "0")), (decimal)0));
                    //FORMULA HERE
                    bool test = Number(FiscalNetWorth, "0") == Max(Sum(Number(AcquisitionInvestmentValue, "0"), -Number(TaxAcceptedDepreciationsWriteDownsInvestment, "0")), (decimal)0);
                    if (!test)
                    {
                        // MESSAGES
                        AddMessage(new BizTaxErrorDataContract
                        {
                            FileId = _fileId,
                            Id = "f-cg-5145",
                            Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Het totaal vermeld in de rubriek '{Fiscale nettowaarde}' ({$FiscalNetWorth})' van een meerwaarde op een zeeschip is niet juist (= {$CalculatedAmountFiscalNetWorth})."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Le total repris dans la rubrique '{Valeur fiscale nette}' ({$FiscalNetWorth})' d'une plus-value sur une navire n'est pas correct (= {$CalculatedAmountFiscalNetWorth})."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Der in der Rubrik '{Steuerlicher Nettowert}' ({$FiscalNetWorth})' angegebene Gesamtbetrag, Mehrwert auf ein Seeschiff, ist unzutreffend (= {$CalculatedAmountFiscalNetWorth})."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Het totaal vermeld in de rubriek '{Fiscale nettowaarde}' ({$FiscalNetWorth})' van een meerwaarde op een zeeschip is niet juist (= {$CalculatedAmountFiscalNetWorth})."}  
 }
                            ,
                            Fields = new List<string>()
                        }
                        );
                    }
                }
            } // end scenario loop

        }

        internal void Calc_f_cg_5151_assertion_set()
        {
            List<string> scenarioIds = GetScenarioIds(new string[] { "d-asst:AssetTypeDimension::d-asst:SpecificSecurityMember/d-ty:CapitalGainTypedDimension" });
            foreach (string scenarioId in scenarioIds)
            {
                string period = "D";
                // SCENARIO: "d-asst:AssetTypeDimension::d-asst:SpecificSecurityMember/d-ty:CapitalGainTypedDimension"
                // PERIOD: "D"
                var RealisedCapitalGain = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:RealisedCapitalGain" });
                var SellingPrice = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:SellingPrice" });
                var FiscalNetWorth = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:FiscalNetWorth" });
                var FormulaTarget = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:RealisedCapitalGain" });
                if (RealisedCapitalGain.Count > 0 || SellingPrice.Count > 0 || FiscalNetWorth.Count > 0 || FormulaTarget.Count > 0)
                {
                    var CalculatedAmountRealisedCapitalGain = Max(Sum(Number(SellingPrice, "0"), -Number(FiscalNetWorth, "0")), (decimal)0);
                    //CALCULATION HERE
                    if (FormulaTarget.Count == 0)
                    {
                        FormulaTarget = CreateNewCalculationElement(scenarioId, period, "tax-inc:RealisedCapitalGain", new string[] { "d-asst:AssetTypeDimension::d-asst:SpecificSecurityMember/d-ty:CapitalGainTypedDimension" }, "EUR", "INF");
                    }
                    if (FormulaTarget.Count > 1)
                    {
                        throw new Exception("More then one target for FormulaTarget in assertion f-cg-5151");
                    }
                    FormulaTarget.First().Calculated = true;
                    FormulaTarget.First().SetNumber(Max(Sum(Number(SellingPrice, "0"), -Number(FiscalNetWorth, "0")), (decimal)0));
                    //FORMULA HERE
                    bool test = Number(RealisedCapitalGain, "0") == Max(Sum(Number(SellingPrice, "0"), -Number(FiscalNetWorth, "0")), (decimal)0);
                    if (!test)
                    {
                        // MESSAGES
                        AddMessage(new BizTaxErrorDataContract
                        {
                            FileId = _fileId,
                            Id = "f-cg-5151",
                            Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Het totaal vermeld in de rubriek '{Verwezenlijkte meerwaarde}' ({$RealisedCapitalGain}) van een gespreid te belasten meerwaarde op bepaalde effecten is niet juist (= {$CalculatedAmountRealisedCapitalGain})."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Le total repris dans la rubrique '{Plus-value réalisée}' ({$RealisedCapitalGain}) d'une plus-value sur certains titres n'est pas correct (= {$CalculatedAmountRealisedCapitalGain})."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Der in der Rubrik '{Verwirklichter Mehrwert}'angegebene Gesamtbetrag ({$RealisedCapitalGain}), gestaffelt zu versteuernder Mehrwert auf bestimmte Wertpapiere, ist unzutreffend (= {$CalculatedAmountRealisedCapitalGain})."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Het totaal vermeld in de rubriek '{Verwezenlijkte meerwaarde}' ({$RealisedCapitalGain}) van een gespreid te belasten meerwaarde op bepaalde effecten is niet juist (= {$CalculatedAmountRealisedCapitalGain})."}  
 }
                            ,
                            Fields = new List<string>()
                        }
                        );
                    }
                }
            } // end scenario loop

        }

        internal void Calc_f_cg_5152_assertion_set()
        {
            List<string> scenarioIds = GetScenarioIds(new string[] { "d-asst:AssetTypeDimension::d-asst:TangibleIntangibleFixedAssetsMember/d-ty:CapitalGainTypedDimension" });
            foreach (string scenarioId in scenarioIds)
            {
                string period = "D";
                // SCENARIO: "d-asst:AssetTypeDimension::d-asst:TangibleIntangibleFixedAssetsMember/d-ty:CapitalGainTypedDimension"
                // PERIOD: "D"
                var RealisedCapitalGain = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:RealisedCapitalGain" });
                var RealisedSellingValue = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:RealisedSellingValue" });
                var CostRealisation = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:CostRealisation" });
                var FiscalNetWorth = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:FiscalNetWorth" });
                var ReceivedCompensation = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:ReceivedCompensation" });
                var FormulaTarget = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:RealisedCapitalGain" });
                if (RealisedCapitalGain.Count > 0 || RealisedSellingValue.Count > 0 || ReceivedCompensation.Count > 0 || CostRealisation.Count > 0 || FiscalNetWorth.Count > 0 || FormulaTarget.Count > 0)
                {
                    var CalculatedAmountRealisedCapitalGain = (Number(RealisedSellingValue, "0") > (decimal)0) ? Max(Sum(Number(RealisedSellingValue, "0"), -Number(CostRealisation, "0"), -Number(FiscalNetWorth, "0")), (decimal)0) : Max(Sum(Number(ReceivedCompensation, "0"), -Number(CostRealisation, "0"), -Number(FiscalNetWorth, "0")), (decimal)0);
                    //CALCULATION HERE
                    if (FormulaTarget.Count == 0)
                    {
                        FormulaTarget = CreateNewCalculationElement(scenarioId, period, "tax-inc:RealisedCapitalGain", new string[] { "d-asst:AssetTypeDimension::d-asst:TangibleIntangibleFixedAssetsMember/d-ty:CapitalGainTypedDimension" }, "EUR", "INF");
                    }
                    if (FormulaTarget.Count > 1)
                    {
                        throw new Exception("More then one target for FormulaTarget in assertion f-cg-5152");
                    }
                    FormulaTarget.First().Calculated = true;
                    FormulaTarget.First().SetNumber(Max(Sum(Number(RealisedSellingValue, "0"), -Number(CostRealisation, "0"), -Number(FiscalNetWorth, "0")), Sum(Number(ReceivedCompensation, "0"), -Number(CostRealisation, "0"), -Number(FiscalNetWorth, "0")), (decimal)0));
                    //FORMULA HERE
                    bool test = (Number(RealisedCapitalGain, "0") == Max(Sum(Number(RealisedSellingValue, "0"), -Number(CostRealisation, "0"), -Number(FiscalNetWorth, "0")), (decimal)0)) || (Number(RealisedCapitalGain, "0") == Max(Sum(Number(ReceivedCompensation, "0"), -Number(CostRealisation, "0"), -Number(FiscalNetWorth, "0")), (decimal)0));
                    if (!test)
                    {
                        // MESSAGES
                        AddMessage(new BizTaxErrorDataContract
                        {
                            FileId = _fileId,
                            Id = "f-cg-5152",
                            Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Het totaal vermeld in de rubriek '{Verwezenlijkte meerwaarde}' ({$RealisedCapitalGain}) van een meerwaarde op een materiële en immateriële vaste activa is niet juist (= {$CalculatedAmountRealisedCapitalGain})."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Le total repris dans la rubrique '{Plus-value réalisée}' ({$RealisedCapitalGain}) d'une plus-value sur des immobilisations corporelles et incorporelles n'est pas correct (= {$CalculatedAmountRealisedCapitalGain})."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Der in der Rubrik '{Verwirklichter Mehrwert}' angegebene Gesamtbetrag ({$RealisedCapitalGain}), Mehrwert auf immaterielle Anlagen und Sachanlagen, ist unzutreffend (= {$CalculatedAmountRealisedCapitalGain})."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Het totaal vermeld in de rubriek '{Verwezenlijkte meerwaarde}' ({$RealisedCapitalGain}) van een meerwaarde op een materiële en immateriële vaste activa is niet juist (= {$CalculatedAmountRealisedCapitalGain})."}  
 }
                            ,
                            Fields = new List<string>()
                        }
                        );
                    }
                }
            } // end scenario loop

        }

        internal void Calc_f_cg_5153_assertion_set()
        {
            List<string> scenarioIds = GetScenarioIds(new string[] { "d-asst:AssetTypeDimension::d-asst:CorporateVehicleMember/d-ty:CapitalGainTypedDimension" });
            foreach (string scenarioId in scenarioIds)
            {
                string period = "D";
                // SCENARIO: "d-asst:AssetTypeDimension::d-asst:CorporateVehicleMember/d-ty:CapitalGainTypedDimension"
                // PERIOD: "D"
                var RealisedCapitalGain = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:RealisedCapitalGain" });
                var RealisedSellingValue = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:RealisedSellingValue" });
                var CostRealisation = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:CostRealisation" });
                var FiscalNetWorth = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:FiscalNetWorth" });
                var ReceivedCompensation = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:ReceivedCompensation" });
                var FormulaTarget = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:RealisedCapitalGain" });
                if (RealisedCapitalGain.Count > 0 || RealisedSellingValue.Count > 0 || ReceivedCompensation.Count > 0 || CostRealisation.Count > 0 || FiscalNetWorth.Count > 0 || FormulaTarget.Count > 0)
                {
                    var CalculatedAmountRealisedCapitalGain = (Number(RealisedSellingValue, "0") > (decimal)0) ? Max(Sum(Number(RealisedSellingValue, "0"), -Number(CostRealisation, "0"), -Number(FiscalNetWorth, "0")), (decimal)0) : Max(Sum(Number(ReceivedCompensation, "0"), -Number(CostRealisation, "0"), -Number(FiscalNetWorth, "0")), (decimal)0);
                    //CALCULATION HERE
                    if (FormulaTarget.Count == 0)
                    {
                        FormulaTarget = CreateNewCalculationElement(scenarioId, period, "tax-inc:RealisedCapitalGain", new string[] { "d-asst:AssetTypeDimension::d-asst:CorporateVehicleMember/d-ty:CapitalGainTypedDimension" }, "EUR", "INF");
                    }
                    if (FormulaTarget.Count > 1)
                    {
                        throw new Exception("More then one target for FormulaTarget in assertion f-cg-5153");
                    }
                    FormulaTarget.First().Calculated = true;
                    FormulaTarget.First().SetNumber(Max(Sum(Number(RealisedSellingValue, "0"), -Number(CostRealisation, "0"), -Number(FiscalNetWorth, "0")), Sum(Number(ReceivedCompensation, "0"), -Number(CostRealisation, "0"), -Number(FiscalNetWorth, "0")), (decimal)0));
                    //FORMULA HERE
                    bool test = (Number(RealisedCapitalGain, "0") == Max(Sum(Number(RealisedSellingValue, "0"), -Number(CostRealisation, "0"), -Number(FiscalNetWorth, "0")), (decimal)0)) || (Number(RealisedCapitalGain, "0") == Max(Sum(Number(ReceivedCompensation, "0"), -Number(CostRealisation, "0"), -Number(FiscalNetWorth, "0")), (decimal)0));
                    if (!test)
                    {
                        // MESSAGES
                        AddMessage(new BizTaxErrorDataContract
                        {
                            FileId = _fileId,
                            Id = "f-cg-5153",
                            Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Het totaal vermeld in de rubriek '{Verwezenlijkte meerwaarde}' ({$RealisedCapitalGain}) van een meerwaarde op bedrijfsvoertuigen is niet juist (= {$CalculatedAmountRealisedCapitalGain})."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Le total repris dans la rubrique '{Plus-value réalisée}' ({$RealisedCapitalGain}) d'une plus-value sur véhicules d'entreprises n'est pas correct (= {$CalculatedAmountRealisedCapitalGain})."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Der in der Rubrik '{Verwirklichter Mehrwert}' angegebene Gesamtbetrag ({$RealisedCapitalGain}), Mehrwert auf Betriebsfahrzeuge, ist unzutreffend (= {$CalculatedAmountRealisedCapitalGain})."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Het totaal vermeld in de rubriek '{Verwezenlijkte meerwaarde}' ({$RealisedCapitalGain}) van een meerwaarde op bedrijfsvoertuigen is niet juist (= {$CalculatedAmountRealisedCapitalGain})."}  
 }
                            ,
                            Fields = new List<string>()
                        }
                        );
                    }
                }
            } // end scenario loop

        }

        internal void Calc_f_cg_5154_assertion_set()
        {
            List<string> scenarioIds = GetScenarioIds(new string[] { "d-asst:AssetTypeDimension::d-asst:RiverVesselMember/d-ty:CapitalGainTypedDimension" });
            foreach (string scenarioId in scenarioIds)
            {
                string period = "D";
                // SCENARIO: "d-asst:AssetTypeDimension::d-asst:RiverVesselMember/d-ty:CapitalGainTypedDimension"
                // PERIOD: "D"
                var RealisedCapitalGain = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:RealisedCapitalGain" });
                var RealisedSellingValue = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:RealisedSellingValue" });
                var CostRealisation = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:CostRealisation" });
                var FiscalNetWorth = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:FiscalNetWorth" });
                var ReceivedCompensation = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:ReceivedCompensation" });
                var FormulaTarget = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:RealisedCapitalGain" });
                if (RealisedCapitalGain.Count > 0 || RealisedSellingValue.Count > 0 || ReceivedCompensation.Count > 0 || CostRealisation.Count > 0 || FiscalNetWorth.Count > 0 || FormulaTarget.Count > 0)
                {
                    var CalculatedAmountRealisedCapitalGain = (Number(RealisedSellingValue, "0") > (decimal)0) ? Max(Sum(Number(RealisedSellingValue, "0"), -Number(CostRealisation, "0"), -Number(FiscalNetWorth, "0")), (decimal)0) : Max(Sum(Number(ReceivedCompensation, "0"), -Number(CostRealisation, "0"), -Number(FiscalNetWorth, "0")), (decimal)0);
                    //CALCULATION HERE
                    if (FormulaTarget.Count == 0)
                    {
                        FormulaTarget = CreateNewCalculationElement(scenarioId, period, "tax-inc:RealisedCapitalGain", new string[] { "d-asst:AssetTypeDimension::d-asst:RiverVesselMember/d-ty:CapitalGainTypedDimension" }, "EUR", "INF");
                    }
                    if (FormulaTarget.Count > 1)
                    {
                        throw new Exception("More then one target for FormulaTarget in assertion f-cg-5154");
                    }
                    FormulaTarget.First().Calculated = true;
                    FormulaTarget.First().SetNumber(Max(Sum(Number(RealisedSellingValue, "0"), -Number(CostRealisation, "0"), -Number(FiscalNetWorth, "0")), Sum(Number(ReceivedCompensation, "0"), -Number(CostRealisation, "0"), -Number(FiscalNetWorth, "0")), (decimal)0));
                    //FORMULA HERE
                    bool test = (Number(RealisedCapitalGain, "0") == Max(Sum(Number(RealisedSellingValue, "0"), -Number(CostRealisation, "0"), -Number(FiscalNetWorth, "0")), (decimal)0)) || (Number(RealisedCapitalGain, "0") == Max(Sum(Number(ReceivedCompensation, "0"), -Number(CostRealisation, "0"), -Number(FiscalNetWorth, "0")), (decimal)0));
                    if (!test)
                    {
                        // MESSAGES
                        AddMessage(new BizTaxErrorDataContract
                        {
                            FileId = _fileId,
                            Id = "f-cg-5154",
                            Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Het totaal vermeld in de rubriek '{Verwezenlijkte meerwaarde}' ({$RealisedCapitalGain}) van een meerwaarde op binnenschepen voor de commerciële vaart is niet juist (= {$CalculatedAmountRealisedCapitalGain})."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Le total repris dans la rubrique '{Plus-value réalisée}' ({$RealisedCapitalGain}) d'une plus-value sur bateaux de navigation intérieure n'est pas correct (= {$CalculatedAmountRealisedCapitalGain})."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Der in der Rubrik '{Verwirklichter Mehrwert}' angegebene Gesamtbetrag ({$RealisedCapitalGain}), Mehrwert auf Binnenschiffe für die gewerbliche Binnenschifffahrt, ist unzutreffend (= {$CalculatedAmountRealisedCapitalGain}). "}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Het totaal vermeld in de rubriek '{Verwezenlijkte meerwaarde}' ({$RealisedCapitalGain}) van een meerwaarde op binnenschepen voor de commerciële vaart is niet juist (= {$CalculatedAmountRealisedCapitalGain})."}  
 }
                            ,
                            Fields = new List<string>()
                        }
                        );
                    }
                }
            } // end scenario loop

        }

        internal void Calc_f_cg_5155_assertion_set()
        {
            List<string> scenarioIds = GetScenarioIds(new string[] { "d-asst:AssetTypeDimension::d-asst:SeaVesselMember/d-ty:CapitalGainTypedDimension" });
            foreach (string scenarioId in scenarioIds)
            {
                string period = "D";
                // SCENARIO: "d-asst:AssetTypeDimension::d-asst:SeaVesselMember/d-ty:CapitalGainTypedDimension"
                // PERIOD: "D"
                var RealisedCapitalGain = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:RealisedCapitalGain" });
                var RealisedSellingValue = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:RealisedSellingValue" });
                var FiscalNetWorth = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:FiscalNetWorth" });
                var FormulaTarget = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:RealisedCapitalGain" });
                if (RealisedCapitalGain.Count > 0 || RealisedSellingValue.Count > 0 || FiscalNetWorth.Count > 0 || FormulaTarget.Count > 0)
                {
                    var CalculatedAmountRealisedCapitalGain = Max(Sum(Number(RealisedSellingValue, "0"), -Number(FiscalNetWorth, "0")), (decimal)0);
                    //CALCULATION HERE
                    if (FormulaTarget.Count == 0)
                    {
                        FormulaTarget = CreateNewCalculationElement(scenarioId, period, "tax-inc:RealisedCapitalGain", new string[] { "d-asst:AssetTypeDimension::d-asst:SeaVesselMember/d-ty:CapitalGainTypedDimension" }, "EUR", "INF");
                    }
                    if (FormulaTarget.Count > 1)
                    {
                        throw new Exception("More then one target for FormulaTarget in assertion f-cg-5155");
                    }
                    FormulaTarget.First().Calculated = true;
                    FormulaTarget.First().SetNumber(Max(Sum(Number(RealisedSellingValue, "0"), -Number(FiscalNetWorth, "0")), (decimal)0));
                    //FORMULA HERE
                    bool test = Number(RealisedCapitalGain, "0") == Max(Sum(Number(RealisedSellingValue, "0"), -Number(FiscalNetWorth, "0")), (decimal)0);
                    if (!test)
                    {
                        // MESSAGES
                        AddMessage(new BizTaxErrorDataContract
                        {
                            FileId = _fileId,
                            Id = "f-cg-5155",
                            Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Het totaal vermeld in de rubriek '{Verwezenlijkte meerwaarde}' ({$RealisedCapitalGain}) van een meerwaarde op een zeeschip is niet juist (= {$CalculatedAmountRealisedCapitalGain})."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Le total repris dans la rubrique'{Plus-value réalisée}' ({$RealisedCapitalGain}) d'une plus-value sur une navire n'est pas correct (= {$CalculatedAmountRealisedCapitalGain})."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Der in der Rubrik '{Verwirklichter Mehrwert}' angegebene Gesamtbetrag ({$RealisedCapitalGain}), Mehrwert auf ein Seeschiff, ist unzutreffend (= {$CalculatedAmountRealisedCapitalGain})."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Het totaal vermeld in de rubriek '{Verwezenlijkte meerwaarde}' ({$RealisedCapitalGain}) van een meerwaarde op een zeeschip is niet juist (= {$CalculatedAmountRealisedCapitalGain})."}  
 }
                            ,
                            Fields = new List<string>()
                        }
                        );
                    }
                }
            } // end scenario loop

        }

        internal void Calc_f_cg_5161_assertion_set()
        {
            List<string> scenarioIds = GetScenarioIds(new string[] { "d-asst:AssetTypeDimension::d-asst:SpecificSecurityMember/d-ty:CapitalGainTypedDimension" });
            foreach (string scenarioId in scenarioIds)
            {
                string period = "D";
                // SCENARIO: "d-asst:AssetTypeDimension::d-asst:SpecificSecurityMember/d-ty:CapitalGainTypedDimension"
                // PERIOD: "D"
                var NonMonetaryFractionRealisedCapitalGain = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:NonMonetaryFractionRealisedCapitalGain" });
                var RealisedCapitalGain = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:RealisedCapitalGain" });
                var MonetaryFractionRealisedCapitalGain = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:MonetaryFractionRealisedCapitalGain" });
                var FormulaTarget = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:NonMonetaryFractionRealisedCapitalGain" });
                if (NonMonetaryFractionRealisedCapitalGain.Count > 0 || RealisedCapitalGain.Count > 0 || MonetaryFractionRealisedCapitalGain.Count > 0 || FormulaTarget.Count > 0)
                {
                    var CalculatedAmountNonMonetaryFractionRealisedCapitalGain = Max(Sum(Number(RealisedCapitalGain, "0"), -Number(MonetaryFractionRealisedCapitalGain, "0")), (decimal)0);
                    //CALCULATION HERE
                    if (FormulaTarget.Count == 0)
                    {
                        FormulaTarget = CreateNewCalculationElement(scenarioId, period, "tax-inc:NonMonetaryFractionRealisedCapitalGain", new string[] { "d-asst:AssetTypeDimension::d-asst:SpecificSecurityMember/d-ty:CapitalGainTypedDimension" }, "EUR", "INF");
                    }
                    if (FormulaTarget.Count > 1)
                    {
                        throw new Exception("More then one target for FormulaTarget in assertion f-cg-5161");
                    }
                    FormulaTarget.First().Calculated = true;
                    FormulaTarget.First().SetNumber(Max(Sum(Number(RealisedCapitalGain, "0"), -Number(MonetaryFractionRealisedCapitalGain, "0")), (decimal)0));
                    //FORMULA HERE
                    bool test = Number(NonMonetaryFractionRealisedCapitalGain, "0") == Max(Sum(Number(RealisedCapitalGain, "0"), -Number(MonetaryFractionRealisedCapitalGain, "0")), (decimal)0);
                    if (!test)
                    {
                        // MESSAGES
                        AddMessage(new BizTaxErrorDataContract
                        {
                            FileId = _fileId,
                            Id = "f-cg-5161",
                            Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Het totaal vermeld in de rubriek '{Niet-monetair gedeelte verwezenlijkte meerwaarde}' ({$NonMonetaryFractionRealisedCapitalGain}) van een gespreid te belasten meerwaarde op bepaalde effecten is niet juist (= {$CalculatedAmountNonMonetaryFractionRealisedCapitalGain})."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Le total repris dans la rubrique '{Quotité non monétaire de la plus-value réalisée}' ({$NonMonetaryFractionRealisedCapitalGain}) d'une	plus-value sur certains titres n'est pas correct (= {$CalculatedAmountNonMonetaryFractionRealisedCapitalGain})."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Der in der Rubrik '{Nicht monetärer Anteil des verwirklichten Mehrwerts}' angegebene Gesamtbetrag ({$NonMonetaryFractionRealisedCapitalGain}), gestaffelt zu versteuernder Mehrwert auf bestimmte Wertpapiere, ist unzutreffend (= {$CalculatedAmountNonMonetaryFractionRealisedCapitalGain})."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Het totaal vermeld in de rubriek '{Niet-monetair gedeelte verwezenlijkte meerwaarde}' ({$NonMonetaryFractionRealisedCapitalGain}) van een gespreid te belasten meerwaarde op bepaalde effecten is niet juist (= {$CalculatedAmountNonMonetaryFractionRealisedCapitalGain})."}  
 }
                            ,
                            Fields = new List<string>()
                        }
                        );
                    }
                }
            } // end scenario loop

        }

        internal void Calc_f_cg_5162_assertion_set()
        {
            List<string> scenarioIds = GetScenarioIds(new string[] { "d-asst:AssetTypeDimension::d-asst:TangibleIntangibleFixedAssetsMember/d-ty:CapitalGainTypedDimension" });
            foreach (string scenarioId in scenarioIds)
            {
                string period = "D";
                // SCENARIO: "d-asst:AssetTypeDimension::d-asst:TangibleIntangibleFixedAssetsMember/d-ty:CapitalGainTypedDimension"
                // PERIOD: "D"
                var NonMonetaryFractionRealisedCapitalGain = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:NonMonetaryFractionRealisedCapitalGain" });
                var RealisedCapitalGain = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:RealisedCapitalGain" });
                var MonetaryFractionRealisedCapitalGain = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:MonetaryFractionRealisedCapitalGain" });
                var FormulaTarget = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:NonMonetaryFractionRealisedCapitalGain" });
                if (NonMonetaryFractionRealisedCapitalGain.Count > 0 || RealisedCapitalGain.Count > 0 || MonetaryFractionRealisedCapitalGain.Count > 0 || FormulaTarget.Count > 0)
                {
                    var CalculatedAmountNonMonetaryFractionRealisedCapitalGain = Max(Sum(Number(RealisedCapitalGain, "0"), -Number(MonetaryFractionRealisedCapitalGain, "0")), (decimal)0);
                    //CALCULATION HERE
                    if (FormulaTarget.Count == 0)
                    {
                        FormulaTarget = CreateNewCalculationElement(scenarioId, period, "tax-inc:NonMonetaryFractionRealisedCapitalGain", new string[] { "d-asst:AssetTypeDimension::d-asst:TangibleIntangibleFixedAssetsMember/d-ty:CapitalGainTypedDimension" }, "EUR", "INF");
                    }
                    if (FormulaTarget.Count > 1)
                    {
                        throw new Exception("More then one target for FormulaTarget in assertion f-cg-5162");
                    }
                    FormulaTarget.First().Calculated = true;
                    FormulaTarget.First().SetNumber(Max(Sum(Number(RealisedCapitalGain, "0"), -Number(MonetaryFractionRealisedCapitalGain, "0")), (decimal)0));
                    //FORMULA HERE
                    bool test = Number(NonMonetaryFractionRealisedCapitalGain, "0") == Max(Sum(Number(RealisedCapitalGain, "0"), -Number(MonetaryFractionRealisedCapitalGain, "0")), (decimal)0);
                    if (!test)
                    {
                        // MESSAGES
                        AddMessage(new BizTaxErrorDataContract
                        {
                            FileId = _fileId,
                            Id = "f-cg-5162",
                            Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Het totaal vermeld in de rubriek '{Niet-monetair gedeelte verwezenlijkte meerwaarde}' ({$NonMonetaryFractionRealisedCapitalGain}) van een meerwaarde op een materiële en immateriële vaste activa is niet juist (= {$CalculatedAmountNonMonetaryFractionRealisedCapitalGain})."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Le total repris dans la rubrique '{Quotité non monétaire de la plus-value réalisée}' ({$NonMonetaryFractionRealisedCapitalGain}) d'une plus-value sur des immobilisations corporelles et incorporelles n'est pas correct (= {$CalculatedAmountNonMonetaryFractionRealisedCapitalGain})."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Der in der Rubrik '{Nicht monetärer Anteil des verwirklichten Mehrwerts}' angegebene Gesamtbetrag ({$NonMonetaryFractionRealisedCapitalGain}), Mehrwert auf immaterielle Anlagen und Sachanlagen, ist unzutreffend (= {$CalculatedAmountNonMonetaryFractionRealisedCapitalGain})."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Het totaal vermeld in de rubriek '{Niet-monetair gedeelte verwezenlijkte meerwaarde}' ({$NonMonetaryFractionRealisedCapitalGain}) van een meerwaarde op een materiële en immateriële vaste activa is niet juist (= {$CalculatedAmountNonMonetaryFractionRealisedCapitalGain})."}  
 }
                            ,
                            Fields = new List<string>()
                        }
                        );
                    }
                }
            } // end scenario loop

        }

        internal void Calc_f_ricg_5121_assertion_set()
        {

        }

        internal void Calc_f_ricg_5122_assertion_set()
        {

        }

        internal void Calc_f_ricg_5123_assertion_set()
        {

        }

        internal void Calc_f_ricg_5124_assertion_set()
        {

        }

        internal void Calc_f_ricg_5125_assertion_set()
        {

        }

        internal void Calc_f_stcg_5132_assertion_set()
        {

        }

        internal void Calc_f_stcg_5131_assertion_set()
        {

        }

        internal void Calc_f_ace_5301_assertion_set()
        {
            string scenarioId = GetScenarioId("");
            string period = "I-Start";
            // SCENARIO: ""
            // PERIOD: "I-Start"
            var DeductionsEquityAllowanceCorporateEquity = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:DeductionsEquityAllowanceCorporateEquity" });
            var OwnSharesFiscalValue = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:OwnSharesFiscalValue" });
            var FinancialFixedAssetsParticipationsOtherShares = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:FinancialFixedAssetsParticipationsOtherShares" });
            var SharesInvestmentCorporations = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:SharesInvestmentCorporations" });
            var BranchesCountryTaxTreaty = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:BranchesCountryTaxTreaty" });
            var ImmovablePropertyCountryTaxTreaty = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:ImmovablePropertyCountryTaxTreaty" });
            var TangibleFixedAssetsUnreasonableRelatedCosts = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:TangibleFixedAssetsUnreasonableRelatedCosts" });
            var InvestmentsNoPeriodicalIncome = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:InvestmentsNoPeriodicalIncome" });
            var ImmovablePropertyUseManager = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:ImmovablePropertyUseManager" });
            var UnrealisedExpressedCapitalGains = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:UnrealisedExpressedCapitalGains" });
            var TaxCreditResearchDevelopmentDeductionEquityAllowanceCorporateEquity = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:TaxCreditResearchDevelopmentDeductionEquityAllowanceCorporateEquity" });
            var InvestmentGrants = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:InvestmentGrants" });
            var ActualisationStockRecognisedDiamondTraders = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:ActualisationStockRecognisedDiamondTraders" });
            var BorrowedFundsMainEstablishmentInterestChargedTaxableResultBelgianBranch = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:BorrowedFundsMainEstablishmentInterestChargedTaxableResultBelgianBranch" });
            var FormulaTarget = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:DeductionsEquityAllowanceCorporateEquity" });
            if (DeductionsEquityAllowanceCorporateEquity.Count > 0 || OwnSharesFiscalValue.Count > 0 || FinancialFixedAssetsParticipationsOtherShares.Count > 0 || SharesInvestmentCorporations.Count > 0 || BranchesCountryTaxTreaty.Count > 0 || ImmovablePropertyCountryTaxTreaty.Count > 0 || TangibleFixedAssetsUnreasonableRelatedCosts.Count > 0 || InvestmentsNoPeriodicalIncome.Count > 0 || ImmovablePropertyUseManager.Count > 0 || UnrealisedExpressedCapitalGains.Count > 0 || TaxCreditResearchDevelopmentDeductionEquityAllowanceCorporateEquity.Count > 0 || InvestmentGrants.Count > 0 || ActualisationStockRecognisedDiamondTraders.Count > 0 || BorrowedFundsMainEstablishmentInterestChargedTaxableResultBelgianBranch.Count > 0 || FormulaTarget.Count > 0)
            {
                var PeriodStartDate = GetNodeByName("tax-inc", "PeriodStartDate");
                var CalculatedAmountDeductionsEquityAllowanceCorporateEquity = Sum(Number(OwnSharesFiscalValue, "0"), Number(FinancialFixedAssetsParticipationsOtherShares, "0"), Number(SharesInvestmentCorporations, "0"), Number(BranchesCountryTaxTreaty, "0"), Number(ImmovablePropertyCountryTaxTreaty, "0"), Number(TangibleFixedAssetsUnreasonableRelatedCosts, "0"), Number(InvestmentsNoPeriodicalIncome, "0"), Number(ImmovablePropertyUseManager, "0"), Number(UnrealisedExpressedCapitalGains, "0"), Number(TaxCreditResearchDevelopmentDeductionEquityAllowanceCorporateEquity, "0"), Number(InvestmentGrants, "0"), Number(ActualisationStockRecognisedDiamondTraders, "0"), Number(BorrowedFundsMainEstablishmentInterestChargedTaxableResultBelgianBranch, "0"));
                //CALCULATION HERE
                if (FormulaTarget.Count == 0)
                {
                    FormulaTarget = CreateNewCalculationElement(scenarioId, period, "tax-inc:DeductionsEquityAllowanceCorporateEquity", new string[] { "" }, "EUR", "INF");
                }
                if (FormulaTarget.Count > 1)
                {
                    throw new Exception("More then one target for FormulaTarget in assertion f-ace-5301");
                }
                FormulaTarget.First().Calculated = true;
                FormulaTarget.First().SetNumber(Sum(Number(OwnSharesFiscalValue, "0"), Number(FinancialFixedAssetsParticipationsOtherShares, "0"), Number(SharesInvestmentCorporations, "0"), Number(BranchesCountryTaxTreaty, "0"), Number(ImmovablePropertyCountryTaxTreaty, "0"), Number(TangibleFixedAssetsUnreasonableRelatedCosts, "0"), Number(InvestmentsNoPeriodicalIncome, "0"), Number(ImmovablePropertyUseManager, "0"), Number(UnrealisedExpressedCapitalGains, "0"), Number(TaxCreditResearchDevelopmentDeductionEquityAllowanceCorporateEquity, "0"), Number(InvestmentGrants, "0"), Number(ActualisationStockRecognisedDiamondTraders, "0"), Number(BorrowedFundsMainEstablishmentInterestChargedTaxableResultBelgianBranch, "0")));
                //FORMULA HERE
                bool test = Number(DeductionsEquityAllowanceCorporateEquity, "0") == Sum(Number(OwnSharesFiscalValue, "0"), Number(FinancialFixedAssetsParticipationsOtherShares, "0"), Number(SharesInvestmentCorporations, "0"), Number(BranchesCountryTaxTreaty, "0"), Number(ImmovablePropertyCountryTaxTreaty, "0"), Number(TangibleFixedAssetsUnreasonableRelatedCosts, "0"), Number(InvestmentsNoPeriodicalIncome, "0"), Number(ImmovablePropertyUseManager, "0"), Number(UnrealisedExpressedCapitalGains, "0"), Number(TaxCreditResearchDevelopmentDeductionEquityAllowanceCorporateEquity, "0"), Number(InvestmentGrants, "0"), Number(ActualisationStockRecognisedDiamondTraders, "0"), Number(BorrowedFundsMainEstablishmentInterestChargedTaxableResultBelgianBranch, "0"));
                if (!test)
                {
                    // MESSAGES
                    AddMessage(new BizTaxErrorDataContract
                    {
                        FileId = _fileId,
                        Id = "f-ace-5301",
                        Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Het totaal vermeld in de rubriek '{Bestanddelen af te trekken van het eigen vermogen}' 	({$DeductionsEquityAllowanceCorporateEquity}) is niet juist (= {$CalculatedAmountDeductionsEquityAllowanceCorporateEquity})."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Le total repris dans la rubrique '{Eléments à déduire des capitaux propres}' 	({$DeductionsEquityAllowanceCorporateEquity}) n'est pas correct (= {$CalculatedAmountDeductionsEquityAllowanceCorporateEquity})."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Der in der Rubrik '{Vom Eigenkapital abzuziehende Bestandteile}' 	({$DeductionsEquityAllowanceCorporateEquity}) angegebene Gesamtbetrag ist unzutreffend (= {$CalculatedAmountDeductionsEquityAllowanceCorporateEquity})."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Het totaal vermeld in de rubriek '{Bestanddelen af te trekken van het eigen vermogen}' 	({$DeductionsEquityAllowanceCorporateEquity}) is niet juist (= {$CalculatedAmountDeductionsEquityAllowanceCorporateEquity})."}  
 }
                        ,
                        Fields = new List<string>()
                    }
                    );
                }
            }

        }

        internal void Calc_f_ace_5302_assertion_set()
        {
            string scenarioId = GetScenarioId("");
            string period = "D";
            // SCENARIO: ""
            // PERIOD: "D"
            var AllowanceCorporateEquityCurrentTaxPeriod = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:AllowanceCorporateEquityCurrentTaxPeriod" });
            var EquityStart = GetElementsByRef(scenarioId, new string[] { "I-Start" }, new string[] { "tax-inc:Equity" });
            var DeductionsEquityAllowanceCorporateEquityStart = GetElementsByRef(scenarioId, new string[] { "I-Start" }, new string[] { "tax-inc:DeductionsEquityAllowanceCorporateEquity" });
            var MovementEquityAfterDeductionsAllowanceCorporateEquity = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:MovementEquityAfterDeductionsAllowanceCorporateEquity" });
            var FormulaTarget = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:AllowanceCorporateEquityCurrentTaxPeriod" });
            if (AllowanceCorporateEquityCurrentTaxPeriod.Count > 0 || EquityStart.Count > 0 || DeductionsEquityAllowanceCorporateEquityStart.Count > 0 || MovementEquityAfterDeductionsAllowanceCorporateEquity.Count > 0 || FormulaTarget.Count > 0)
            {
                var PeriodStartDate = GetNodeByName("tax-inc", "PeriodStartDate");
                var CalculatedAmountAllowanceCorporateEquityCurrentTaxPeriod = Max(Sum(Number(EquityStart, "0"), -Number(DeductionsEquityAllowanceCorporateEquityStart, "0"), Number(MovementEquityAfterDeductionsAllowanceCorporateEquity, "0")), (decimal)0);
                //CALCULATION HERE
                if (FormulaTarget.Count == 0)
                {
                    FormulaTarget = CreateNewCalculationElement(scenarioId, period, "tax-inc:AllowanceCorporateEquityCurrentTaxPeriod", new string[] { "" }, "EUR", "INF");
                }
                if (FormulaTarget.Count > 1)
                {
                    throw new Exception("More then one target for FormulaTarget in assertion f-ace-5302");
                }
                FormulaTarget.First().Calculated = true;
                FormulaTarget.First().SetNumber(Sum(Number(EquityStart, "0"), -Number(DeductionsEquityAllowanceCorporateEquityStart, "0"), Number(MovementEquityAfterDeductionsAllowanceCorporateEquity, "0")));
                if ((Number(AllowanceCorporateEquityCurrentTaxPeriod, "0") > (decimal)0) || (Sum(Number(EquityStart, "0"), -Number(DeductionsEquityAllowanceCorporateEquityStart, "0"), Number(MovementEquityAfterDeductionsAllowanceCorporateEquity, "0")) > (decimal)0))
                {
                    //FORMULA HERE
                    bool test = Number(AllowanceCorporateEquityCurrentTaxPeriod, "0") == Max(Sum(Number(EquityStart, "0"), -Number(DeductionsEquityAllowanceCorporateEquityStart, "0"), Number(MovementEquityAfterDeductionsAllowanceCorporateEquity, "0")), (decimal)0);
                    if (!test)
                    {
                        // MESSAGES
                        AddMessage(new BizTaxErrorDataContract
                        {
                            FileId = _fileId,
                            Id = "f-ace-5302",
                            Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Het totaal vermeld in de rubriek '{Risicokapitaal van het belastbaar tijdperk}' 	({$AllowanceCorporateEquityCurrentTaxPeriod}) is niet juist (= {$CalculatedAmountAllowanceCorporateEquityCurrentTaxPeriod})."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Le total repris dans la rubrique '{Capital à risque de la période imposable}' 	({$AllowanceCorporateEquityCurrentTaxPeriod}) n'est pas correct (= {$CalculatedAmountAllowanceCorporateEquityCurrentTaxPeriod})."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Der in der Rubrik '{Risikokapital des Besteuerungszeitraums}' 	({$AllowanceCorporateEquityCurrentTaxPeriod}) angegebene Gesamtbetrag ist unzutreffend (= {$CalculatedAmountAllowanceCorporateEquityCurrentTaxPeriod})."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Het totaal vermeld in de rubriek '{Risicokapitaal van het belastbaar tijdperk}' 	({$AllowanceCorporateEquityCurrentTaxPeriod}) is niet juist (= {$CalculatedAmountAllowanceCorporateEquityCurrentTaxPeriod})."}  
 }
                            ,
                            Fields = new List<string>()
                        }
                        );
                    }
                }
            }

        }

        internal void Calc_f_ace_5304_assertion_set()
        {
            string scenarioId = GetScenarioId("");
            string period = "D";
            // SCENARIO: ""
            // PERIOD: "D"
            var DeductibleAllowanceCorporateEquityCurrentAssessmentYear = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:DeductibleAllowanceCorporateEquityCurrentAssessmentYear" });
            var AllowanceCorporateEquityCurrentTaxPeriod = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:AllowanceCorporateEquityCurrentTaxPeriod" });
            var ComplianceSmallCompanyCorporationCodeLastButOneTaxPeriod = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:ComplianceSmallCompanyCorporationCodeLastButOneTaxPeriod" });
            var ComplianceSmallCompanyCorporationCodePreviousTaxPeriod = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:ComplianceSmallCompanyCorporationCodePreviousTaxPeriod" });
            if (DeductibleAllowanceCorporateEquityCurrentAssessmentYear.Count > 0 || AllowanceCorporateEquityCurrentTaxPeriod.Count > 0 || ComplianceSmallCompanyCorporationCodeLastButOneTaxPeriod.Count > 0 || ComplianceSmallCompanyCorporationCodePreviousTaxPeriod.Count > 0)
            {
                var PeriodStartDate = GetNodeByName("tax-inc", "PeriodStartDate");
                var PeriodEndDate = GetNodeByName("tax-inc", "PeriodEndDate");
                var NumberOfDays = DaysFromDuration(Date(PeriodEndDate) - Date(PeriodStartDate) + DayTimeDuration("P1D"));
                var NumberOfDaysAssessmentYear = DaysFromDuration(Date(PeriodEndDate) - (Date(PeriodEndDate) - YearMonthDuration("P1Y")));
                var CalculatedAmountIncreasedRate = Math.Round(Number(AllowanceCorporateEquityCurrentTaxPeriod, "0") * Number(IncreasedRateSMEsAllowanceCorporateEquityCurrentTaxPeriod, "0.03925") * (decimal)100 * (Number(NumberOfDays) / Number(NumberOfDaysAssessmentYear)), MidpointRounding.AwayFromZero) / (decimal)100;
                var CalculatedAmountNormalRate = Math.Round(Number(AllowanceCorporateEquityCurrentTaxPeriod, "0") * Number(RateAllowanceCorporateEquityCurrentTaxPeriod, "0.03425") * (decimal)100 * (Number(NumberOfDays) / Number(NumberOfDaysAssessmentYear)), MidpointRounding.AwayFromZero) / (decimal)100;
                //CALCULATION HERE
                if (Number(DeductibleAllowanceCorporateEquityCurrentAssessmentYear, "0") > (decimal)0)
                {
                    //FORMULA HERE
                    bool test = (Number(DeductibleAllowanceCorporateEquityCurrentAssessmentYear, "0") == Math.Round(Number(AllowanceCorporateEquityCurrentTaxPeriod, "0") * Number(IncreasedRateSMEsAllowanceCorporateEquityCurrentTaxPeriod, "0.03925") * (decimal)100 * (Number(NumberOfDays) / Number(NumberOfDaysAssessmentYear)), MidpointRounding.AwayFromZero) / (decimal)100) || (Number(DeductibleAllowanceCorporateEquityCurrentAssessmentYear, "0") == Math.Round(Number(AllowanceCorporateEquityCurrentTaxPeriod, "0") * Number(RateAllowanceCorporateEquityCurrentTaxPeriod, "0.03425") * (decimal)100 * (Number(NumberOfDays) / Number(NumberOfDaysAssessmentYear)), MidpointRounding.AwayFromZero) / (decimal)100);
                    if (!test)
                    {
                        // MESSAGES
                        AddMessage(new BizTaxErrorDataContract
                        {
                            FileId = _fileId,
                            Id = "f-ace-5304",
                            Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Het bedrag vermeld in de rubriek '{In principe aftrekbaar}' ({$DeductibleAllowanceCorporateEquityCurrentAssessmentYear}) 	is {$RateAllowanceCorporateEquityCurrentTaxPeriod*100}% (= {$CalculatedAmountNormalRate}) of {$IncreasedRateSMEsAllowanceCorporateEquityCurrentTaxPeriod*100}% voor een KMO  (= {$CalculatedAmountIncreasedRate}) van het bedrag 	vermeld in de rubriek '{Risicokapitaal van het belastbaar tijdperk}' ({$AllowanceCorporateEquityCurrentTaxPeriod}) 	rekening houdend met de duur van het belastbaar tijdperk ({$NumberOfDays} gedeeld door {$NumberOfDaysAssessmentYear})."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Le montant repris dans la rubrique '{Déductible en principe}' 	({$DeductibleAllowanceCorporateEquityCurrentAssessmentYear}) 	est {$RateAllowanceCorporateEquityCurrentTaxPeriod*100}% (= {$CalculatedAmountNormalRate}) ou {$IncreasedRateSMEsAllowanceCorporateEquityCurrentTaxPeriod*100}% pour une PME (= {$CalculatedAmountIncreasedRate}) du montant repris 	dans la rubrique '{Capital à risque de la période imposable}' ({$AllowanceCorporateEquityCurrentTaxPeriod}) 	tenant compte de la durée de la période imposable ({$NumberOfDays} divisé par {$NumberOfDaysAssessmentYear}). "}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Der Betrag, der angegeben ist in der Rubrik '{Grundsätzlich abziehbar}' 	({$DeductibleAllowanceCorporateEquityCurrentAssessmentYear}) ist {$RateAllowanceCorporateEquityCurrentTaxPeriod*100}% (= {$CalculatedAmountNormalRate}) oder {$IncreasedRateSMEsAllowanceCorporateEquityCurrentTaxPeriod*100}% für KMB (= {$CalculatedAmountIncreasedRate}) 	des in der Rubrik '{Risikokapital des Besteuerungszeitraums}' ({$AllowanceCorporateEquityCurrentTaxPeriod})	angegebenen Betrags, unter Berücksichtigung des Besteuerungszeitraums ({$NumberOfDays} geteilt durch {$NumberOfDaysAssessmentYear}). "}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Het bedrag vermeld in de rubriek '{In principe aftrekbaar}' ({$DeductibleAllowanceCorporateEquityCurrentAssessmentYear}) 	is {$RateAllowanceCorporateEquityCurrentTaxPeriod*100}% (= {$CalculatedAmountNormalRate}) of {$IncreasedRateSMEsAllowanceCorporateEquityCurrentTaxPeriod*100}% voor een KMO  (= {$CalculatedAmountIncreasedRate}) van het bedrag 	vermeld in de rubriek '{Risicokapitaal van het belastbaar tijdperk}' ({$AllowanceCorporateEquityCurrentTaxPeriod}) 	rekening houdend met de duur van het belastbaar tijdperk ({$NumberOfDays} gedeeld door {$NumberOfDaysAssessmentYear})."}  
 }
                            ,
                            Fields = new List<string>()
                        }
                        );
                    }
                }
            }

        }

        internal void Calc_f_ace_5305_assertion_set()
        {
            string scenarioId = GetScenarioId("");
            string period = "D";
            // SCENARIO: ""
            // PERIOD: "D"
            var CarryOverCurrentAssessmentYearInsufficientProfitsAllowanceCorporateEquity = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:CarryOverCurrentAssessmentYearInsufficientProfitsAllowanceCorporateEquity" });
            var DeductibleAllowanceCorporateEquityCurrentAssessmentYear = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:DeductibleAllowanceCorporateEquityCurrentAssessmentYear" });
            var DeductionAllowanceCorporateEquityCurrentAssessmentYear = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:DeductionAllowanceCorporateEquityCurrentAssessmentYear" });
            var FormulaTarget = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:CarryOverCurrentAssessmentYearInsufficientProfitsAllowanceCorporateEquity" });
            if (CarryOverCurrentAssessmentYearInsufficientProfitsAllowanceCorporateEquity.Count > 0 || DeductibleAllowanceCorporateEquityCurrentAssessmentYear.Count > 0 || DeductionAllowanceCorporateEquityCurrentAssessmentYear.Count > 0 || FormulaTarget.Count > 0)
            {
                //CALCULATION HERE
                if (FormulaTarget.Count == 0)
                {
                    FormulaTarget = CreateNewCalculationElement(scenarioId, period, "tax-inc:CarryOverCurrentAssessmentYearInsufficientProfitsAllowanceCorporateEquity", new string[] { "" }, "EUR", "INF");
                }
                if (FormulaTarget.Count > 1)
                {
                    throw new Exception("More then one target for FormulaTarget in assertion f-ace-5305");
                }
                FormulaTarget.First().Calculated = true;
                FormulaTarget.First().SetNumber(Max(Sum(Number(DeductibleAllowanceCorporateEquityCurrentAssessmentYear, "0"), -Number(DeductionAllowanceCorporateEquityCurrentAssessmentYear, "0")), "0"));
                //FORMULA HERE
                bool test = (Number(CarryOverCurrentAssessmentYearInsufficientProfitsAllowanceCorporateEquity, "0") == Max(Sum(Number(DeductibleAllowanceCorporateEquityCurrentAssessmentYear, "0"), -Number(DeductionAllowanceCorporateEquityCurrentAssessmentYear, "0")), (decimal)0)) && (Number(DeductibleAllowanceCorporateEquityCurrentAssessmentYear, "0") >= Number(DeductionAllowanceCorporateEquityCurrentAssessmentYear, "0"));
                if (!test)
                {
                    // MESSAGES
                    AddMessage(new BizTaxErrorDataContract
                    {
                        FileId = _fileId,
                        Id = "f-ace-5305",
                        Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Het bedrag vermeld in de rubriek 	'{Saldo van de aftrek voor risicokapitaal van het huidig aanslagjaar dat overdraagbaar is naar latere aanslagjaren}' ({$CarryOverCurrentAssessmentYearInsufficientProfitsAllowanceCorporateEquity})	moet gelijk zijn aan het verschil van de bedragen vermeld in de rubrieken 	'{In principe aftrekbaar}' ({$DeductibleAllowanceCorporateEquityCurrentAssessmentYear})	en 	'{Aftrek voor risicokapitaal van het huidig aanslagjaar die werkelijk wordt afgetrokken}' ({$DeductionAllowanceCorporateEquityCurrentAssessmentYear}).  	Het bedrag vermeld in de rubriek '{In principe aftrekbaar}' ({$DeductibleAllowanceCorporateEquityCurrentAssessmentYear}) 	moet groter zijn dan of gelijk zijn aan het bedrag vermeld in de rubriek 	'{Aftrek voor risicokapitaal van het huidig aanslagjaar die werkelijk wordt afgetrokken}' ({$DeductionAllowanceCorporateEquityCurrentAssessmentYear})."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Le montant repris dans la rubrique 	'{Solde de la déduction pour capital à risque de l'exercice d'imposition actuel à reporter sur les exercices d'imposition ultérieurs}' ({$CarryOverCurrentAssessmentYearInsufficientProfitsAllowanceCorporateEquity}) 	doit être égal à la différence des montants repris dans des rubriques 	'{Déductible en principe}' ({$DeductibleAllowanceCorporateEquityCurrentAssessmentYear}) et 	'{Déduction pour capital à risque de l'exercice d'imposition actuel, effectivement déduite}' ({$DeductionAllowanceCorporateEquityCurrentAssessmentYear}).  	Le montant repris dans la rubrique '{Déductible en principe}' ({$DeductibleAllowanceCorporateEquityCurrentAssessmentYear}) 	doit être supérieur ou égal au montant repris dans la rubrique 	'{Déduction pour capital à risque de l'exercice d'imposition actuel, effectivement déduite}' ({$DeductionAllowanceCorporateEquityCurrentAssessmentYear})."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Der Betrag, der angegeben ist in der Rubrik 	'{Restbetrag des auf spätere Steuerjahre übertragbaren Restbetrags des Abzugs für Risikokapital des aktuellen Steuerjahres}' ({$CarryOverCurrentAssessmentYearInsufficientProfitsAllowanceCorporateEquity})  	muss der Differenz der Beträge, die angegeben sind in den Rubriken 	'{Grundsätzlich abziehbar}' ({$DeductibleAllowanceCorporateEquityCurrentAssessmentYear}) 	und 	'{Abzug für Risikokapital, der tatsächlich für das aktuelle Steuerjahr abgezogen wird}' ({$DeductionAllowanceCorporateEquityCurrentAssessmentYear}) entsprechen.  	Der Betrag, der angegeben ist in der Rubrik '{Grundsätzlich abziehbar}' ({$DeductibleAllowanceCorporateEquityCurrentAssessmentYear})  	muss höher sein als der Betrag, der angegeben ist in der Rubrik 	'{Abzug für Risikokapital, der tatsächlich für das aktuelle Steuerjahr abgezogen wird}' ({$DeductionAllowanceCorporateEquityCurrentAssessmentYear}) 	oder diesem entsprechen."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Het bedrag vermeld in de rubriek 	'{Saldo van de aftrek voor risicokapitaal van het huidig aanslagjaar dat overdraagbaar is naar latere aanslagjaren}' ({$CarryOverCurrentAssessmentYearInsufficientProfitsAllowanceCorporateEquity})	moet gelijk zijn aan het verschil van de bedragen vermeld in de rubrieken 	'{In principe aftrekbaar}' ({$DeductibleAllowanceCorporateEquityCurrentAssessmentYear})	en 	'{Aftrek voor risicokapitaal van het huidig aanslagjaar die werkelijk wordt afgetrokken}' ({$DeductionAllowanceCorporateEquityCurrentAssessmentYear}).  	Het bedrag vermeld in de rubriek '{In principe aftrekbaar}' ({$DeductibleAllowanceCorporateEquityCurrentAssessmentYear}) 	moet groter zijn dan of gelijk zijn aan het bedrag vermeld in de rubriek 	'{Aftrek voor risicokapitaal van het huidig aanslagjaar die werkelijk wordt afgetrokken}' ({$DeductionAllowanceCorporateEquityCurrentAssessmentYear})."}  
 }
                        ,
                        Fields = new List<string>()
                    }
                    );
                }
            }

        }

        internal void Calc_f_ace_5307_assertion_set()
        {
            // HAS NO UNBOUND SEQUENCES
            var MovementEquityAfterDeductionsAllowanceCorporateEquity = GetElementsByScenDefs(new string[] { "d-ty:DateTypedDimension/d-ty:DescriptionTypedDimension" }, new string[] { "D" }, new string[] { "tax-inc:MovementEquityAfterDeductionsAllowanceCorporateEquity" });
            var MovementEquityAfterDeductionsAllowanceCorporateEquityTotal = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:MovementEquityAfterDeductionsAllowanceCorporateEquity" });
            var FormulaTarget = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:MovementEquityAfterDeductionsAllowanceCorporateEquity" });
            if (MovementEquityAfterDeductionsAllowanceCorporateEquity.Count > 0 || MovementEquityAfterDeductionsAllowanceCorporateEquityTotal.Count > 0 || FormulaTarget.Count > 0)
            {
                var CalculatedAmountMovementEquityAfterDeductionsAllowanceCorporateEquityTotal = Sum(Number(MovementEquityAfterDeductionsAllowanceCorporateEquity, "0"));
                //CALCULATION HERE
                if (FormulaTarget.Count == 0)
                {
                    FormulaTarget = CreateNewCalculationElement(null, new string[] { "D" }, "tax-inc:MovementEquityAfterDeductionsAllowanceCorporateEquity", new string[] { "" }, "EUR", "INF");
                }
                if (FormulaTarget.Count > 1)
                {
                    throw new Exception("More then one target for FormulaTarget in assertion f-ace-5307");
                }
                FormulaTarget.First().Calculated = true;
                FormulaTarget.First().SetNumber(Sum(Number(MovementEquityAfterDeductionsAllowanceCorporateEquity, "0")));
                //FORMULA HERE
                bool test = Sum(Number(MovementEquityAfterDeductionsAllowanceCorporateEquity, "0")) == Number(MovementEquityAfterDeductionsAllowanceCorporateEquityTotal, "0");
                if (!test)
                {
                    // MESSAGES
                    AddMessage(new BizTaxErrorDataContract
                    {
                        FileId = _fileId,
                        Id = "f-ace-5307",
                        Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Het totaal vermeld in de rubriek 	'{Wijzigingen tijdens het belastbare tijdperk van het eigen vermogen en van de bestanddelen die hiervan afgetrokken mogen worden}' 	({$MovementEquityAfterDeductionsAllowanceCorporateEquityTotal}) 	is niet juist (= {$CalculatedAmountMovementEquityAfterDeductionsAllowanceCorporateEquityTotal}). "}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Le total repris dans la rubrique 	'{Variations en cours de période imposable des capitaux propres et des éléments à déduire}' 	({$MovementEquityAfterDeductionsAllowanceCorporateEquityTotal}) 	n'est pas correct (= {$CalculatedAmountMovementEquityAfterDeductionsAllowanceCorporateEquityTotal})."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Der in der Rubrik 	'{Änderungen des Eigenkapitals im Laufe des Besteuerungszeitraums und der davon abzuziehenden Bestandteile}' 	({$MovementEquityAfterDeductionsAllowanceCorporateEquityTotal}) angegebene Gesamtbetrag 	ist unzutreffend (= {$CalculatedAmountMovementEquityAfterDeductionsAllowanceCorporateEquityTotal})."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Het totaal vermeld in de rubriek 	'{Wijzigingen tijdens het belastbare tijdperk van het eigen vermogen en van de bestanddelen die hiervan afgetrokken mogen worden}' 	({$MovementEquityAfterDeductionsAllowanceCorporateEquityTotal}) 	is niet juist (= {$CalculatedAmountMovementEquityAfterDeductionsAllowanceCorporateEquityTotal}). "}  
 }
                        ,
                        Fields = new List<string>()
                    }
                    );
                }
            }

        }

        internal void Calc_f_pcs_5320_assertion_set()
        {
            // HAS NO UNBOUND SEQUENCES
            var PaymentCertainState = GetElementsByScenDefs(new string[] { "d-ty:DateTypedDimension/d-ty:NatureTypedDimension" }, new string[] { "D" }, new string[] { "tax-inc:PaymentCertainState" });
            var PaymentCertainStateTotal = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:PaymentCertainState" });
            var FormulaTarget = GetElementsByScenDefs(new string[] { "d-ty:DateTypedDimension/d-ty:NatureTypedDimension" }, new string[] { "D" }, new string[] { "tax-inc:PaymentCertainState" });
            if (PaymentCertainState.Count > 0 || PaymentCertainStateTotal.Count > 0 || FormulaTarget.Count > 0)
            {
                var CalculatedAmountPaymentCertainStateTotal = Sum(Number(PaymentCertainState, "0"));
                //CALCULATION HERE
                if (FormulaTarget.Count == 0)
                {
                    FormulaTarget = CreateNewCalculationElement(null, new string[] { "D" }, "tax-inc:PaymentCertainState", new string[] { "" }, "EUR", "INF");
                }
                if (FormulaTarget.Count > 1)
                {
                    throw new Exception("More then one target for FormulaTarget in assertion f-pcs-5320");
                }
                FormulaTarget.First().Calculated = true;
                FormulaTarget.First().SetNumber(Sum(Number(PaymentCertainState, "0")));
                //FORMULA HERE
                bool test = Sum(Number(PaymentCertainState, "0")) == Number(PaymentCertainStateTotal, "0");
                if (!test)
                {
                    // MESSAGES
                    AddMessage(new BizTaxErrorDataContract
                    {
                        FileId = _fileId,
                        Id = "f-pcs-5320",
                        Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Het totaal van de bedragen ingevuld in de kolom '{Betaling uitgedrukt in EUR}' ({$PaymentCertainState}) stemt niet overeen met het bedrag ingevuld in de totaalrij (= {$CalculatedAmountPaymentCertainStateTotal})."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Le total des valeurs complétées dans la colonne '{Paiement exprimé en EUR}' ({$PaymentCertainState}) ne correspond pas à la valeur complétée dans la ligne qui contient le total (= {$CalculatedAmountPaymentCertainStateTotal})."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Die Summe der in der Rubrik '{Zahlung ausgedrückt in EUR}' ({$PaymentCertainState}) angegebenen Beträge entspricht dem in der Reihe der Summen (= {$CalculatedAmountPaymentCertainStateTotal}) angegebenen Gesamtbetrag nicht."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Het totaal van de bedragen ingevuld in de kolom '{Betaling uitgedrukt in EUR}' ({$PaymentCertainState}) stemt niet overeen met het bedrag ingevuld in de totaalrij (= {$CalculatedAmountPaymentCertainStateTotal})."}  
 }
                        ,
                        Fields = new List<string>()
                    }
                    );
                }
            }

        }

        internal void Calc_f_dpi_5375_assertion_set()
        {
            string scenarioId = GetScenarioId("");
            string period = "D";
            // SCENARIO: ""
            // PERIOD: "D"
            var CorrectedIncomePatentsWhollyPartiallyObtainedThirdParty = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:CorrectedIncomePatentsWhollyPartiallyObtainedThirdParty" });
            var IncomePatentsWhollyPartiallyObtainedThirdParty = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:IncomePatentsWhollyPartiallyObtainedThirdParty" });
            var CompensationOwedThirdPartiesPertainingPatents = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:CompensationOwedThirdPartiesPertainingPatents" });
            var AmortisationAcquisitionInvestmentValuePatents = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:AmortisationAcquisitionInvestmentValuePatents" });
            var FormulaTarget = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:CorrectedIncomePatentsWhollyPartiallyObtainedThirdParty" });
            if (CorrectedIncomePatentsWhollyPartiallyObtainedThirdParty.Count > 0 || IncomePatentsWhollyPartiallyObtainedThirdParty.Count > 0 || CompensationOwedThirdPartiesPertainingPatents.Count > 0 || AmortisationAcquisitionInvestmentValuePatents.Count > 0 || FormulaTarget.Count > 0)
            {
                var CalculatedAmountCorrectedIncomePatentsWhollyPartiallyObtainedThirdParty = Max(Sum(Number(IncomePatentsWhollyPartiallyObtainedThirdParty, "0"), -Number(CompensationOwedThirdPartiesPertainingPatents, "0"), -Number(AmortisationAcquisitionInvestmentValuePatents, "0")), (decimal)0);
                //CALCULATION HERE
                if (FormulaTarget.Count == 0)
                {
                    FormulaTarget = CreateNewCalculationElement(scenarioId, period, "tax-inc:CorrectedIncomePatentsWhollyPartiallyObtainedThirdParty", new string[] { "" }, "EUR", "INF");
                }
                if (FormulaTarget.Count > 1)
                {
                    throw new Exception("More then one target for FormulaTarget in assertion f-dpi-5375");
                }
                FormulaTarget.First().Calculated = true;
                FormulaTarget.First().SetNumber(Max(Sum(Number(IncomePatentsWhollyPartiallyObtainedThirdParty, "0"), -Number(CompensationOwedThirdPartiesPertainingPatents, "0"), -Number(AmortisationAcquisitionInvestmentValuePatents, "0")), (decimal)0));
                //FORMULA HERE
                bool test = Number(CorrectedIncomePatentsWhollyPartiallyObtainedThirdParty, "0") == Max(Sum(Number(IncomePatentsWhollyPartiallyObtainedThirdParty, "0"), -Number(CompensationOwedThirdPartiesPertainingPatents, "0"), -Number(AmortisationAcquisitionInvestmentValuePatents, "0")), (decimal)0);
                if (!test)
                {
                    // MESSAGES
                    AddMessage(new BizTaxErrorDataContract
                    {
                        FileId = _fileId,
                        Id = "f-dpi-5375",
                        Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Het totaal vermeld in de rubriek '{Gecorrigeerde aftrekbare inkomsten of het gedeelte van inkomsten uit octrooien die de vennootschap heeft verworven van derden}' ({$CorrectedIncomePatentsWhollyPartiallyObtainedThirdParty}) is niet juist (= {$CalculatedAmountCorrectedIncomePatentsWhollyPartiallyObtainedThirdParty}). "}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Le total repris dans la rubrique '{Revenus ou quotité de revenus corrigés déductibles provenant de brevets acquis de tiers par la société}' ({$CorrectedIncomePatentsWhollyPartiallyObtainedThirdParty}) n'est pas correct (= {$CalculatedAmountCorrectedIncomePatentsWhollyPartiallyObtainedThirdParty}). "}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Der Betrag, der angegeben ist in der Rubrik '{Abzugsfähige, berichtigte Einkommen oder Einkommensanteile aus Patenten, die die Gesellschaft von Dritten erworben hat}' ({$CorrectedIncomePatentsWhollyPartiallyObtainedThirdParty}) ist unzutreffend (= {$CalculatedAmountCorrectedIncomePatentsWhollyPartiallyObtainedThirdParty}). "}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Het totaal vermeld in de rubriek '{Gecorrigeerde aftrekbare inkomsten of het gedeelte van inkomsten uit octrooien die de vennootschap heeft verworven van derden}' ({$CorrectedIncomePatentsWhollyPartiallyObtainedThirdParty}) is niet juist (= {$CalculatedAmountCorrectedIncomePatentsWhollyPartiallyObtainedThirdParty}). "}  
 }
                        ,
                        Fields = new List<string>()
                    }
                    );
                }
            }

        }

        internal void Calc_f_dpi_5376_assertion_set()
        {
            string scenarioId = GetScenarioId("");
            string period = "D";
            // SCENARIO: ""
            // PERIOD: "D"
            var CalculationBasisDeductionPatentsIncome = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:CalculationBasisDeductionPatentsIncome" });
            var IncomeRegisteredCorporationPatentsWhollyPartiallyDeveloped = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:IncomeRegisteredCorporationPatentsWhollyPartiallyDeveloped" });
            var CorrectedIncomePatentsWhollyPartiallyObtainedThirdParty = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:CorrectedIncomePatentsWhollyPartiallyObtainedThirdParty" });
            var FormulaTarget = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:CalculationBasisDeductionPatentsIncome" });
            if (CalculationBasisDeductionPatentsIncome.Count > 0 || IncomeRegisteredCorporationPatentsWhollyPartiallyDeveloped.Count > 0 || CorrectedIncomePatentsWhollyPartiallyObtainedThirdParty.Count > 0 || FormulaTarget.Count > 0)
            {
                var CalculatedAmountCalculationBasisDeductionPatentsIncome = Sum(Number(IncomeRegisteredCorporationPatentsWhollyPartiallyDeveloped, "0"), Number(CorrectedIncomePatentsWhollyPartiallyObtainedThirdParty, "0"));
                //CALCULATION HERE
                if (FormulaTarget.Count == 0)
                {
                    FormulaTarget = CreateNewCalculationElement(scenarioId, period, "tax-inc:CalculationBasisDeductionPatentsIncome", new string[] { "" }, "EUR", "INF");
                }
                if (FormulaTarget.Count > 1)
                {
                    throw new Exception("More then one target for FormulaTarget in assertion f-dpi-5376");
                }
                FormulaTarget.First().Calculated = true;
                FormulaTarget.First().SetNumber(Sum(Number(IncomeRegisteredCorporationPatentsWhollyPartiallyDeveloped, "0"), Number(CorrectedIncomePatentsWhollyPartiallyObtainedThirdParty, "0")));
                //FORMULA HERE
                bool test = Number(CalculationBasisDeductionPatentsIncome, "0") == Sum(Number(IncomeRegisteredCorporationPatentsWhollyPartiallyDeveloped, "0"), Number(CorrectedIncomePatentsWhollyPartiallyObtainedThirdParty, "0"));
                if (!test)
                {
                    // MESSAGES
                    AddMessage(new BizTaxErrorDataContract
                    {
                        FileId = _fileId,
                        Id = "f-dpi-5376",
                        Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Het totaal vermeld in de rubriek '{Berekeningsgrondslag van de aftrek voor octrooi-inkomsten}' ({$CalculationBasisDeductionPatentsIncome}) is niet juist (= {$CalculatedAmountCalculationBasisDeductionPatentsIncome})."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Le total repris dans la rubrique '{Base de calcul de la déduction pour revenus de brevet}' ({$CalculationBasisDeductionPatentsIncome}) n'est pas correct (= {$CalculatedAmountCalculationBasisDeductionPatentsIncome})."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Der in der Rubrik '{Bemessungsgrundlage für den Abzug für Einkünfte aus Patenten}' ({$CalculationBasisDeductionPatentsIncome}) angegebene Gesamtbetrag ist unzutreffend (= {$CalculatedAmountCalculationBasisDeductionPatentsIncome})."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Het totaal vermeld in de rubriek '{Berekeningsgrondslag van de aftrek voor octrooi-inkomsten}' ({$CalculationBasisDeductionPatentsIncome}) is niet juist (= {$CalculatedAmountCalculationBasisDeductionPatentsIncome})."}  
 }
                        ,
                        Fields = new List<string>()
                    }
                    );
                }
            }

        }

        internal void Calc_f_dpi_5377_assertion_set()
        {
            string scenarioId = GetScenarioId("");
            string period = "D";
            // SCENARIO: ""
            // PERIOD: "D"
            var DeductibleDeductionPatentsIncome = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:DeductibleDeductionPatentsIncome" });
            var CalculationBasisDeductionPatentsIncome = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:CalculationBasisDeductionPatentsIncome" });
            var FormulaTarget = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:DeductibleDeductionPatentsIncome" });
            if (DeductibleDeductionPatentsIncome.Count > 0 || CalculationBasisDeductionPatentsIncome.Count > 0 || FormulaTarget.Count > 0)
            {
                var CalculatedAmountDeductibleDeductionPatentsIncome = Math.Round(Number(CalculationBasisDeductionPatentsIncome, "0") * Number(RateCalculationBasisDeductionPatentsIncome, "0.80") * (decimal)100, MidpointRounding.AwayFromZero) / (decimal)100;
                //CALCULATION HERE
                if (FormulaTarget.Count == 0)
                {
                    FormulaTarget = CreateNewCalculationElement(scenarioId, period, "tax-inc:DeductibleDeductionPatentsIncome", new string[] { "" }, "EUR", "INF");
                }
                if (FormulaTarget.Count > 1)
                {
                    throw new Exception("More then one target for FormulaTarget in assertion f-dpi-5377");
                }
                FormulaTarget.First().Calculated = true;
                FormulaTarget.First().SetNumber((Math.Round(Number(CalculationBasisDeductionPatentsIncome, "0") * Number(RateCalculationBasisDeductionPatentsIncome, "0.80") * (decimal)100, MidpointRounding.AwayFromZero) / (decimal)100));
                //FORMULA HERE
                bool test = Number(DeductibleDeductionPatentsIncome, "0") == (Math.Round(Number(CalculationBasisDeductionPatentsIncome, "0") * Number(RateCalculationBasisDeductionPatentsIncome, "0.80") * (decimal)100, MidpointRounding.AwayFromZero) / (decimal)100);
                if (!test)
                {
                    // MESSAGES
                    AddMessage(new BizTaxErrorDataContract
                    {
                        FileId = _fileId,
                        Id = "f-dpi-5377",
                        Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Het bedrag vermeld in de rubriek '{Aftrek voor octrooi-inkomsten}' ({$DeductibleDeductionPatentsIncome}) 	moet gelijk zijn aan 80% van het bedrag vermeld in de rubriek 	'{Berekeningsgrondslag van de aftrek voor octrooi-inkomsten}' (= {$CalculatedAmountDeductibleDeductionPatentsIncome})."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Le montant repris dans la rubrique '{Déduction pour revenus de brevets}' ({$DeductibleDeductionPatentsIncome}) 	doit être égal à 80% du montant repris dans la rubrique 	'{Base de calcul de la déduction pour revenus de brevet}' (= {$CalculatedAmountDeductibleDeductionPatentsIncome})."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Der in der Rubrik '{Abzug für Einkünfte aus Patenten}' ({$DeductibleDeductionPatentsIncome}) 	angegebene Betrag muss  80% des in der Rubrik 	'{Bemessungsgrundlage für den Abzug für Einkünfte aus Patenten}' angegebenen Betrags entsprechen (= {$CalculatedAmountDeductibleDeductionPatentsIncome})."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Het bedrag vermeld in de rubriek '{Aftrek voor octrooi-inkomsten}' ({$DeductibleDeductionPatentsIncome}) 	moet gelijk zijn aan 80% van het bedrag vermeld in de rubriek 	'{Berekeningsgrondslag van de aftrek voor octrooi-inkomsten}' (= {$CalculatedAmountDeductibleDeductionPatentsIncome})."}  
 }
                        ,
                        Fields = new List<string>()
                    }
                    );
                }
            }

        }

        internal void Calc_f_itifa_5321_assertion_set()
        {
            string scenarioId = GetScenarioId("");
            string period = "D";
            // SCENARIO: ""
            // PERIOD: "D"
            var ReductionsIncreaseTaxableReservesInvestmentReserveExcluded = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:ReductionsIncreaseTaxableReservesInvestmentReserveExcluded" });
            var CapitalGainsSharesInvestmentReserve = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:CapitalGainsSharesInvestmentReserve" });
            var CapitalGainsVehiclesNoProfit = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:CapitalGainsVehiclesNoProfit" });
            var DecreasePaidUpCapital = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:DecreasePaidUpCapital" });
            var IncreaseParticularDebtClaims = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:IncreaseParticularDebtClaims" });
            var FormulaTarget = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:ReductionsIncreaseTaxableReservesInvestmentReserveExcluded" });
            if (ReductionsIncreaseTaxableReservesInvestmentReserveExcluded.Count > 0 || CapitalGainsSharesInvestmentReserve.Count > 0 || CapitalGainsVehiclesNoProfit.Count > 0 || DecreasePaidUpCapital.Count > 0 || IncreaseParticularDebtClaims.Count > 0 || FormulaTarget.Count > 0)
            {
                var CalculatedAmountReductionsIncreaseTaxableReservesInvestmentReserveExcluded = Sum(Number(CapitalGainsSharesInvestmentReserve, "0"), Number(CapitalGainsVehiclesNoProfit, "0"), Number(DecreasePaidUpCapital, "0"), Number(IncreaseParticularDebtClaims, "0"));
                //CALCULATION HERE
                if (FormulaTarget.Count == 0)
                {
                    FormulaTarget = CreateNewCalculationElement(scenarioId, period, "tax-inc:ReductionsIncreaseTaxableReservesInvestmentReserveExcluded", new string[] { "" }, "EUR", "INF");
                }
                if (FormulaTarget.Count > 1)
                {
                    throw new Exception("More then one target for FormulaTarget in assertion f-itifa-5321");
                }
                FormulaTarget.First().Calculated = true;
                FormulaTarget.First().SetNumber(Sum(Number(CapitalGainsSharesInvestmentReserve, "0"), Number(CapitalGainsVehiclesNoProfit, "0"), Number(DecreasePaidUpCapital, "0"), Number(IncreaseParticularDebtClaims, "0")));
                //FORMULA HERE
                bool test = Number(ReductionsIncreaseTaxableReservesInvestmentReserveExcluded, "0") == Sum(Number(CapitalGainsSharesInvestmentReserve, "0"), Number(CapitalGainsVehiclesNoProfit, "0"), Number(DecreasePaidUpCapital, "0"), Number(IncreaseParticularDebtClaims, "0"));
                if (!test)
                {
                    // MESSAGES
                    AddMessage(new BizTaxErrorDataContract
                    {
                        FileId = _fileId,
                        Id = "f-itifa-5321",
                        Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Het totaal vermeld in de rubriek '{Verminderingen op deze aangroei}' ({$ReductionsIncreaseTaxableReservesInvestmentReserveExcluded}) is niet juist (= {$CalculatedAmountReductionsIncreaseTaxableReservesInvestmentReserveExcluded})."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Le total repris dans la rubrique '{Diminutions de cet accroissement}' ({$ReductionsIncreaseTaxableReservesInvestmentReserveExcluded}) n'est pas correct (= {$CalculatedAmountReductionsIncreaseTaxableReservesInvestmentReserveExcluded})."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Der in der Rubrik '{Verringerung dieses Zuwachses}' angegebene Gesamtbetrag ({$ReductionsIncreaseTaxableReservesInvestmentReserveExcluded}) ist unzutreffend (= {$CalculatedAmountReductionsIncreaseTaxableReservesInvestmentReserveExcluded})."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Het totaal vermeld in de rubriek '{Verminderingen op deze aangroei}' ({$ReductionsIncreaseTaxableReservesInvestmentReserveExcluded}) is niet juist (= {$CalculatedAmountReductionsIncreaseTaxableReservesInvestmentReserveExcluded})."}  
 }
                        ,
                        Fields = new List<string>()
                    }
                    );
                }
            }

        }

        internal void Calc_f_itifa_5322_assertion_set()
        {
            string scenarioId = GetScenarioId("");
            string period = "D";
            // SCENARIO: ""
            // PERIOD: "D"
            var CorrectedIncreaseTaxableReservesInvestmentReserveExcluded = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:CorrectedIncreaseTaxableReservesInvestmentReserveExcluded" });
            var IncreaseTaxableReservesInvestmentReserveExcluded = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:IncreaseTaxableReservesInvestmentReserveExcluded" });
            var ReductionsIncreaseTaxableReservesInvestmentReserveExcluded = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:ReductionsIncreaseTaxableReservesInvestmentReserveExcluded" });
            var FormulaTarget = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:CorrectedIncreaseTaxableReservesInvestmentReserveExcluded" });
            if (CorrectedIncreaseTaxableReservesInvestmentReserveExcluded.Count > 0 || IncreaseTaxableReservesInvestmentReserveExcluded.Count > 0 || ReductionsIncreaseTaxableReservesInvestmentReserveExcluded.Count > 0 || FormulaTarget.Count > 0)
            {
                var CalculatedAmountCorrectedIncreaseTaxableReservesInvestmentReserveExcluded = Min(Sum(Number(IncreaseTaxableReservesInvestmentReserveExcluded, "0"), -Number(ReductionsIncreaseTaxableReservesInvestmentReserveExcluded, "0")), (decimal)37500.00);
                //CALCULATION HERE
                if (FormulaTarget.Count == 0)
                {
                    FormulaTarget = CreateNewCalculationElement(scenarioId, period, "tax-inc:CorrectedIncreaseTaxableReservesInvestmentReserveExcluded", new string[] { "" }, "EUR", "INF");
                }
                if (FormulaTarget.Count > 1)
                {
                    throw new Exception("More then one target for FormulaTarget in assertion f-itifa-5322");
                }
                FormulaTarget.First().Calculated = true;
                FormulaTarget.First().SetNumber(Max(Sum(Number(IncreaseTaxableReservesInvestmentReserveExcluded, "0"), -Number(ReductionsIncreaseTaxableReservesInvestmentReserveExcluded, "0")), (decimal)0));
                //FORMULA HERE
                bool test = Number(CorrectedIncreaseTaxableReservesInvestmentReserveExcluded, "0") == Min(Sum(Number(IncreaseTaxableReservesInvestmentReserveExcluded, "0"), -Number(ReductionsIncreaseTaxableReservesInvestmentReserveExcluded, "0")), (decimal)37500.00);
                if (!test)
                {
                    // MESSAGES
                    AddMessage(new BizTaxErrorDataContract
                    {
                        FileId = _fileId,
                        Id = "f-itifa-5322",
                        Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Het totaal vermeld in de rubriek '{Aangroei van de belaste reserves van het belastbare tijdperk voor de aanleg van de investeringsreserve na de verminderingen, beperkt tot het maximum}' ({$CorrectedIncreaseTaxableReservesInvestmentReserveExcluded}) is niet juist (= {$CalculatedAmountCorrectedIncreaseTaxableReservesInvestmentReserveExcluded})."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Le total repris dans la rubrique '{Accroissement des réserves taxées de la période imposable, avant constitution de la réserve d'investissement, après les diminutions, limité au maximum}' ({$CorrectedIncreaseTaxableReservesInvestmentReserveExcluded}) n'est pas correct (= {$CalculatedAmountCorrectedIncreaseTaxableReservesInvestmentReserveExcluded})."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Der Betrag, der angegeben ist in der Rubrik '{Zuwachs der besteuerten Rücklagen des Besteuerungszeitraums vor Bildung der Investitionsrücklagen, nach Abzügen, begrenzt auf den Höchstbetrag}' ({$CorrectedIncreaseTaxableReservesInvestmentReserveExcluded}) ist unzutreffend (= {$CalculatedAmountCorrectedIncreaseTaxableReservesInvestmentReserveExcluded})."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Het totaal vermeld in de rubriek '{Aangroei van de belaste reserves van het belastbare tijdperk voor de aanleg van de investeringsreserve na de verminderingen, beperkt tot het maximum}' ({$CorrectedIncreaseTaxableReservesInvestmentReserveExcluded}) is niet juist (= {$CalculatedAmountCorrectedIncreaseTaxableReservesInvestmentReserveExcluded})."}  
 }
                        ,
                        Fields = new List<string>()
                    }
                    );
                }
            }

        }

        internal void Calc_f_itifa_5323_assertion_set()
        {
            // HAS NO UNBOUND SEQUENCES
            var CorrectedIncreaseTaxableReservesInvestmentReserveExcluded = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:CorrectedIncreaseTaxableReservesInvestmentReserveExcluded" });
            if (CorrectedIncreaseTaxableReservesInvestmentReserveExcluded.Count > 0)
            {
                //CALCULATION HERE
                //FORMULA HERE
                bool test = Number(CorrectedIncreaseTaxableReservesInvestmentReserveExcluded, "0") <= (decimal)37500.00;
                if (!test)
                {
                    // MESSAGES
                    AddMessage(new BizTaxErrorDataContract
                    {
                        FileId = _fileId,
                        Id = "f-itifa-5323",
                        Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Het bedrag vermeld in de rubriek '{Aangroei van de belaste reserves van het belastbare tijdperk voor de aanleg van de investeringsreserve na de verminderingen, beperkt tot het maximum}' ({$CorrectedIncreaseTaxableReservesInvestmentReserveExcluded}) moet kleiner zijn dan of gelijk zijn aan 37500 EUR."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Le montant repris dans la rubrique '{Accroissement des réserves taxées de la période imposable, avant constitution de la réserve d'investissement, après les diminutions, limité au maximum}' ({$CorrectedIncreaseTaxableReservesInvestmentReserveExcluded}) doit être inférieur ou égal à 37500 EUR."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Der Betrag, der angegeben ist in der Rubrik '{Zuwachs der besteuerten Rücklagen des Besteuerungszeitraums vor Bildung der Investitionsrücklagen, nach Abzügen, begrenzt auf den Höchstbetrag}' ({$CorrectedIncreaseTaxableReservesInvestmentReserveExcluded}) muss niedriger sein als 37500 EUR oder diesem Betrag entsprechen."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Het bedrag vermeld in de rubriek '{Aangroei van de belaste reserves van het belastbare tijdperk voor de aanleg van de investeringsreserve na de verminderingen, beperkt tot het maximum}' ({$CorrectedIncreaseTaxableReservesInvestmentReserveExcluded}) moet kleiner zijn dan of gelijk zijn aan 37500 EUR."}  
 }
                        ,
                        Fields = new List<string>()
                    }
                    );
                }
            }

        }

        internal void Calc_f_itifa_5324_assertion_set()
        {
            string scenarioId = GetScenarioId("");
            string period = "D";
            // SCENARIO: ""
            // PERIOD: "D"
            var ExemptibleInvestmentReserve = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:ExemptibleInvestmentReserve" });
            var CorrectedIncreaseTaxableReservesInvestmentReserveExcluded = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:CorrectedIncreaseTaxableReservesInvestmentReserveExcluded" });
            var IncreaseTaxableReservesInvestmentReserveIncluded = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:IncreaseTaxableReservesInvestmentReserveIncluded" });
            if (ExemptibleInvestmentReserve.Count > 0 || CorrectedIncreaseTaxableReservesInvestmentReserveExcluded.Count > 0 || IncreaseTaxableReservesInvestmentReserveIncluded.Count > 0)
            {
                var CalculatedAmountExemptibleInvestmentReserve = ((Math.Round(Min(Number(CorrectedIncreaseTaxableReservesInvestmentReserveExcluded, "0"), Number(IncreaseTaxableReservesInvestmentReserveIncluded, "0")) * (decimal)0.50 * (decimal)100, MidpointRounding.AwayFromZero) / (decimal)100) > (decimal)18750) ? (decimal)18750 : Math.Round(Min(Number(CorrectedIncreaseTaxableReservesInvestmentReserveExcluded, "0"), Number(IncreaseTaxableReservesInvestmentReserveIncluded, "0")) * (decimal)0.50 * (decimal)100, MidpointRounding.AwayFromZero) / (decimal)100;
                //CALCULATION HERE
                if ((Number(CorrectedIncreaseTaxableReservesInvestmentReserveExcluded, "0") > (decimal)0) && (Number(IncreaseTaxableReservesInvestmentReserveIncluded, "0") > (decimal)0))
                {
                    //FORMULA HERE
                    bool test = (Number(ExemptibleInvestmentReserve, "0") == (Math.Round(Min(Number(CorrectedIncreaseTaxableReservesInvestmentReserveExcluded, "0"), Number(IncreaseTaxableReservesInvestmentReserveIncluded, "0")) * (decimal)0.50 * (decimal)100, MidpointRounding.AwayFromZero) / (decimal)100)) && (Number(ExemptibleInvestmentReserve, "0") <= (decimal)18750);
                    if (!test)
                    {
                        // MESSAGES
                        AddMessage(new BizTaxErrorDataContract
                        {
                            FileId = _fileId,
                            Id = "f-itifa-5324",
                            Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Het bedrag vermeld in de rubriek 	'{Maximaal vrijgestelde reserve van het belastbare tijdperk}' 	moet beperkt worden tot 50 % van het kleinste bedrag uit de rubrieken 	'{Aangroei van de belaste reserves van het belastbare tijdperk voor de aanleg van de investeringsreserve na de verminderingen, beperkt tot het maximum}' 	of 	'{Aangroei van de belaste reserves ten opzichte van de belaste reserves op het einde van het vorig belastbaar tijdperk waarvoor een investeringsreserve werd genoten}'	en dit maximum moet kleiner zijn dan of gelijk zijn aan 18750 EUR (= {$CalculatedAmountExemptibleInvestmentReserve})."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Le montant repris dans la rubrique 	'{Réserve d’investissement exonérée maximale de la période imposable}' 	est limité à la moitié du montant le moins élevé repris dans les rubriques 	'{Accroissement des réserves taxées de la période imposable, avant constitution de la réserve d'investissement, après les diminutions, limité au maximum}'	ou 	'{Accroissement des réserves taxées par rapport aux réserves taxées à la fin de la période imposable antérieure pour laquelle une réserve d'investissement a été obtenue}'	et ce maximum doit être inférieur ou égal à 18750 EUR (= {$CalculatedAmountExemptibleInvestmentReserve})."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Der in der Rubrik 	'{Maximal befreite Investitionsrücklage des Besteuerungszeitraums}' 	angegebene Betrag ist auf die Hälfte des niedrigsten Betrags, der angegeben ist in den Rubriken 	'{Zuwachs der besteuerten Rücklagen des Besteuerungszeitraums vor Bildung der Investitionsrücklagen, nach Abzügen, begrenzt auf den Höchstbetrag}'	oder 	'{Zuwachs der besteuerten Rücklagen im Vergleich zu den am Ende des vorhergehenden Besteuerungszeitraums, für den eine Investitionsrücklage erhalten wurde, besteuerten Rücklagen}'	herabzusetzen und dieser Höchstbetrag muss niedriger sein als 18750 EUR oder diesem Betrag entsprechen (= {$CalculatedAmountExemptibleInvestmentReserve})."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Het bedrag vermeld in de rubriek 	'{Maximaal vrijgestelde reserve van het belastbare tijdperk}' 	moet beperkt worden tot 50 % van het kleinste bedrag uit de rubrieken 	'{Aangroei van de belaste reserves van het belastbare tijdperk voor de aanleg van de investeringsreserve na de verminderingen, beperkt tot het maximum}' 	of 	'{Aangroei van de belaste reserves ten opzichte van de belaste reserves op het einde van het vorig belastbaar tijdperk waarvoor een investeringsreserve werd genoten}'	en dit maximum moet kleiner zijn dan of gelijk zijn aan 18750 EUR (= {$CalculatedAmountExemptibleInvestmentReserve})."}  
 }
                            ,
                            Fields = new List<string>()
                        }
                        );
                    }
                }
            }

        }

        internal void Calc_f_itifa_5325_assertion_set()
        {
            string scenarioId = GetScenarioId("");
            string period = "D";
            // SCENARIO: ""
            // PERIOD: "D"
            var ExemptibleInvestmentReserve = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:ExemptibleInvestmentReserve" });
            var CorrectedIncreaseTaxableReservesInvestmentReserveExcluded = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:CorrectedIncreaseTaxableReservesInvestmentReserveExcluded" });
            var IncreaseTaxableReservesInvestmentReserveIncluded = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:IncreaseTaxableReservesInvestmentReserveIncluded" });
            if (ExemptibleInvestmentReserve.Count > 0 || CorrectedIncreaseTaxableReservesInvestmentReserveExcluded.Count > 0 || IncreaseTaxableReservesInvestmentReserveIncluded.Count > 0)
            {
                var CalculatedAmountExemptibleInvestmentReserve = (Number(CorrectedIncreaseTaxableReservesInvestmentReserveExcluded, "0") == (decimal)0) ? (Math.Round(Number(IncreaseTaxableReservesInvestmentReserveIncluded, "0") * (decimal)0.50 * (decimal)100, MidpointRounding.AwayFromZero) / (decimal)100) : (Math.Round(Number(CorrectedIncreaseTaxableReservesInvestmentReserveExcluded, "0") * (decimal)0.50 * (decimal)100, MidpointRounding.AwayFromZero) / (decimal)100);
                var CalculatedAmountExemptibleInvestmentReserveMax = (Number(CalculatedAmountExemptibleInvestmentReserve) > (decimal)18750) ? (decimal)18750 : Number(CalculatedAmountExemptibleInvestmentReserve);
                //CALCULATION HERE
                if ((Number(CorrectedIncreaseTaxableReservesInvestmentReserveExcluded, "0") == (decimal)0) || (Number(IncreaseTaxableReservesInvestmentReserveIncluded, "0") == (decimal)0))
                {
                    //FORMULA HERE
                    bool test = (Number(ExemptibleInvestmentReserve, "0") <= (decimal)18750) && ((Number(CorrectedIncreaseTaxableReservesInvestmentReserveExcluded, "0") == (decimal)0) ? (Number(ExemptibleInvestmentReserve, "0") == (Math.Round(Number(IncreaseTaxableReservesInvestmentReserveIncluded, "0") * (decimal)0.50 * (decimal)100, MidpointRounding.AwayFromZero) / (decimal)100)) : (Number(ExemptibleInvestmentReserve, "0") == (Math.Round(Number(CorrectedIncreaseTaxableReservesInvestmentReserveExcluded, "0") * (decimal)0.50 * (decimal)100, MidpointRounding.AwayFromZero) / (decimal)100)));
                    if (!test)
                    {
                        // MESSAGES
                        AddMessage(new BizTaxErrorDataContract
                        {
                            FileId = _fileId,
                            Id = "f-itifa-5325",
                            Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Het bedrag vermeld in de rubriek 	'{Maximaal vrijgestelde reserve van het belastbare tijdperk}' 	moet beperkt worden tot 50 % van het kleinste bedrag uit de rubrieken 	'{Aangroei van de belaste reserves ten opzichte van de belaste reserves op het einde van het vorig belastbaar tijdperk waarvoor een investeringsreserve werd genoten}' 	of 	'{Aangroei van de belaste reserves van het belastbare tijdperk voor de aanleg van de investeringsreserve na de verminderingen, beperkt tot het maximum}' 	en dit maximum moet kleiner zijn dan of gelijk zijn aan 18750 EUR (= {$CalculatedAmountExemptibleInvestmentReserveMax})."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Le montant repris dans la rubrique 	'{Réserve d’investissement exonérée maximale de la période imposable}' 	est limité à la moitié du montant repris dans les rubriques 	'{Accroissement des réserves taxées par rapport aux réserves taxées à la fin de la période imposable antérieure pour laquelle une réserve d'investissement a été obtenue}' 	ou 	'{Accroissement des réserves taxées de la période imposable, avant constitution de la réserve d'investissement, après les diminutions, limité au maximum}' 	et ce maximum doit être inférieure ou égale à 18750 EUR (= {$CalculatedAmountExemptibleInvestmentReserveMax})."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Der in der Rubrik 	'{Maximal befreite Investitionsrücklage des Besteuerungszeitraums}' 	angegebene Betrag ist auf die Hälfte des niedrigsten Betrags, der angegeben ist in den Rubriken 	'{Zuwachs der besteuerten Rücklagen im Vergleich zu den am Ende des vorhergehenden Besteuerungszeitraums, für den eine Investitionsrücklage erhalten wurde, besteuerten Rücklagen}' 	oder 	'{Zuwachs der besteuerten Rücklagen des Besteuerungszeitraums vor Bildung der Investitionsrücklagen, nach Abzügen, begrenzt auf den Höchstbetrag}' 	herabzusetzen und dieser Höchstbetrag muss niedriger sein als 18750 EUR oder diesem Betrag entsprechen (= {$CalculatedAmountExemptibleInvestmentReserveMax})."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Het bedrag vermeld in de rubriek 	'{Maximaal vrijgestelde reserve van het belastbare tijdperk}' 	moet beperkt worden tot 50 % van het kleinste bedrag uit de rubrieken 	'{Aangroei van de belaste reserves ten opzichte van de belaste reserves op het einde van het vorig belastbaar tijdperk waarvoor een investeringsreserve werd genoten}' 	of 	'{Aangroei van de belaste reserves van het belastbare tijdperk voor de aanleg van de investeringsreserve na de verminderingen, beperkt tot het maximum}' 	en dit maximum moet kleiner zijn dan of gelijk zijn aan 18750 EUR (= {$CalculatedAmountExemptibleInvestmentReserveMax})."}  
 }
                            ,
                            Fields = new List<string>()
                        }
                        );
                    }
                }
            }

        }

        internal void Calc_f_itifa_5327_assertion_set()
        {
            List<string> scenarioIds = GetScenarioIds(new string[] { "d-asst:AssetTypeDimension::d-asst:TangibleIntangibleFixedAssetsMember/d-per:PeriodDimension::d-per:TaxPeriod-0Member", "d-asst:AssetTypeDimension::d-asst:TangibleIntangibleFixedAssetsMember/d-per:PeriodDimension::d-per:TaxPeriod-1Member", "d-asst:AssetTypeDimension::d-asst:TangibleIntangibleFixedAssetsMember/d-per:PeriodDimension::d-per:TaxPeriod-2Member" });
            foreach (string scenarioId in scenarioIds)
            {
                string period = "D";
                // SCENARIO: "d-asst:AssetTypeDimension::d-asst:TangibleIntangibleFixedAssetsMember/d-per:PeriodDimension::d-per:TaxPeriod-0Member"
                // SCENARIO: "d-asst:AssetTypeDimension::d-asst:TangibleIntangibleFixedAssetsMember/d-per:PeriodDimension::d-per:TaxPeriod-1Member"
                // SCENARIO: "d-asst:AssetTypeDimension::d-asst:TangibleIntangibleFixedAssetsMember/d-per:PeriodDimension::d-per:TaxPeriod-2Member"
                // PERIOD: "D"
                var ExemptInvestmentReserveOverview = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:ExemptInvestmentReserveOverview" });
                var InvestmentsPreviousInvestmentReserveTaxPeriod = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:InvestmentsPreviousInvestmentReserveTaxPeriod" });
                var InvestmentCurrentTaxPeriod = GetElementsByScenDefs(new string[] { "d-asst:AssetTypeDimension::d-asst:TangibleIntangibleFixedAssetsMember/d-per:PeriodDimension::d-per:TaxPeriod-0Member/d-ty:DateTypedDimension/d-ty:DescriptionTypedDimension", "d-asst:AssetTypeDimension::d-asst:TangibleIntangibleFixedAssetsMember/d-per:PeriodDimension::d-per:TaxPeriod-1Member/d-ty:DateTypedDimension/d-ty:DescriptionTypedDimension", "d-asst:AssetTypeDimension::d-asst:TangibleIntangibleFixedAssetsMember/d-per:PeriodDimension::d-per:TaxPeriod-2Member/d-ty:DateTypedDimension/d-ty:DescriptionTypedDimension" }, new string[] { period }, new string[] { "tax-inc:InvestmentCurrentTaxPeriod" });
                var InvestBalance = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:InvestBalance" });
                var FormulaTarget = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:InvestBalance" });
                if (InvestBalance.Count > 0 || ExemptInvestmentReserveOverview.Count > 0 || InvestmentsPreviousInvestmentReserveTaxPeriod.Count > 0 || InvestmentCurrentTaxPeriod.Count > 0 || FormulaTarget.Count > 0)
                {
                    var dimensionValueQName = FactExplicitScenarioDimensionValue(ExemptInvestmentReserveOverview, QName("http://www . minfin . fgov . be/be/tax/d/per/2012-04-30", "d-per:PeriodDimension"));
                    var dimensionValueString = Data(LocalNameFromQname(dimensionValueQName));
                    var dimensionValue = SubstringBefore(dimensionValueString, "Member");
                    //CALCULATION HERE
                    if (FormulaTarget.Count == 0)
                    {
                        FormulaTarget = CreateNewCalculationElement(scenarioId, period, "tax-inc:InvestBalance", new string[] { "d-asst:AssetTypeDimension::d-asst:TangibleIntangibleFixedAssetsMember/d-per:PeriodDimension::d-per:TaxPeriod-0Member", "d-asst:AssetTypeDimension::d-asst:TangibleIntangibleFixedAssetsMember/d-per:PeriodDimension::d-per:TaxPeriod-1Member", "d-asst:AssetTypeDimension::d-asst:TangibleIntangibleFixedAssetsMember/d-per:PeriodDimension::d-per:TaxPeriod-2Member" }, "EUR", "INF");
                    }
                    if (FormulaTarget.Count > 1)
                    {
                        throw new Exception("More then one target for FormulaTarget in assertion f-itifa-5327");
                    }
                    FormulaTarget.First().Calculated = true;
                    FormulaTarget.First().SetNumber(Max(-Sum(-Number(ExemptInvestmentReserveOverview, "0"), Number(InvestmentsPreviousInvestmentReserveTaxPeriod, "0"), Number(InvestmentCurrentTaxPeriod, "0")), (decimal)0));
                    //FORMULA HERE
                    bool test = Max(-Sum(-Number(ExemptInvestmentReserveOverview, "0"), Number(InvestmentsPreviousInvestmentReserveTaxPeriod, "0"), Number(InvestmentCurrentTaxPeriod, "0")), (decimal)0) == Number(InvestBalance, "0");
                    if (!test)
                    {
                        // MESSAGES
                        AddMessage(new BizTaxErrorDataContract
                        {
                            FileId = _fileId,
                            Id = "f-itifa-5327",
                            Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Het totaal vermeld in de rubriek '{Nog te investeren saldo}' ({$InvestBalance}) is niet juist (= {})."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Le total repris dans la rubrique '{Solde des investissements à effectuer}' ({$InvestBalance}) n'est pas correct (= {})."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Der in der Rubrik '{Saldo der noch vorzunehmenden Investitionen}' angegebene Gesamtbetrag ({$InvestBalance}) ist unzutreffend (= {})."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Het totaal vermeld in de rubriek '{Nog te investeren saldo}' ({$InvestBalance}) is niet juist (= {})."}  
 }
                            ,
                            Fields = new List<string>()
                        }
                        );
                    }
                }
            } // end scenario loop

        }

        internal void Calc_f_itifa_5328_assertion_set()
        {

        }

        internal void Calc_f_itifa_5329_assertion_set()
        {
            string scenarioId = GetScenarioId("");
            string period = "D";
            // SCENARIO: ""
            // PERIOD: "D"
            var CapitalGainsSharesInvestmentReserve = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:CapitalGainsSharesInvestmentReserve" });
            var CapitalGainsShares = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:CapitalGainsShares" });
            if (CapitalGainsSharesInvestmentReserve.Count > 0 || CapitalGainsShares.Count > 0)
            {
                //CALCULATION HERE
                if (Number(CapitalGainsSharesInvestmentReserve, "0") > (decimal)0)
                {
                    //FORMULA HERE
                    bool test = String(CapitalGainsSharesInvestmentReserve, "0") == String(CapitalGainsShares, "0");
                    if (!test)
                    {
                        // MESSAGES
                        AddMessage(new BizTaxErrorDataContract
                        {
                            FileId = _fileId,
                            Id = "f-itifa-5329",
                            Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Het bedrag vermeld in de rubriek 	'{Meerwaarden op aandelen}' ({$CapitalGainsShares}) 	in het vak '{Reserves}' 	van de aangifte moet gelijk zijn aan het bedrag ingevuld in de rubriek 	'{Meerwaarden op aandelen}' ({$CapitalGainsSharesInvestmentReserve}) 	in de bijlage {275R}."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Le total repris dans la rubrique 	'{Plus-values sur actions ou parts}' ({$CapitalGainsShares}) 	dans le cadre '{Réserves}' de la déclaration 	doit être égal au montant complété dans la rubrique 	'{Plus-values sur actions ou parts}' ({$CapitalGainsSharesInvestmentReserve}) dans l'annexe 	{275R}."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Der in der Rubrik 	'{Mehrwerte auf Aktien oder Anteile}' ({$CapitalGainsShares}) 	des Feldes '{Rücklagen}' 	der Erklärung angegebene Betrag muss gleich dem in der Rubrik 	'{Mehrwerte auf Aktien oder Anteile}' ({$CapitalGainsSharesInvestmentReserve}) des Anhangs 	{275R} angegebenen Betrag sein."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Het bedrag vermeld in de rubriek 	'{Meerwaarden op aandelen}' ({$CapitalGainsShares}) 	in het vak '{Reserves}' 	van de aangifte moet gelijk zijn aan het bedrag ingevuld in de rubriek 	'{Meerwaarden op aandelen}' ({$CapitalGainsSharesInvestmentReserve}) 	in de bijlage {275R}."}  
 }
                            ,
                            Fields = new List<string>()
                        }
                        );
                    }
                }
            }

        }

        internal void Calc_f_idtc_5341_assertion_set()
        {
            // HAS NO UNBOUND SEQUENCES
            var NonDimensionalElementsIncompatibleTaxCreditResearchDevelopment = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:InvestmentDeductionOneGoSpreadCurrentTaxPeriod", "tax-inc:InvestmentDeductionSpreadPreviousTaxPeriods", "tax-inc:PreviousCarryOverInvestmentDeduction", "tax-inc:PreviousCarryOverInvestmentDeductionSeaVessels", "tax-inc:OtherPreviousCarryOverInvestmentDeduction", "tax-inc:BasicTaxableInvestmentDeduction", "tax-inc:DeductibleAllowanceInvestmentDeductionIncompatibleTaxCreditResearchDevelopment", "tax-inc:CarryOverInvestmentDeduction", "tax-inc:CarryOverInvestmentDeductionSeaVessels", "tax-inc:OtherCarryOverInvestmentDeduction" });
            var NonDimensionalElementsCompatibleTaxCreditResearchDevelopment = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:PreviousCarryOverSpreadInvestmentDeductionNoResearchDevelopmentCompatibleTaxCreditResearchDevelopment", "tax-inc:PreviousCarryOverInvestmentDeductionCompatibleTaxCreditResearchDevelopment", "tax-inc:PreviousCarryOverInvestmentDeductionSeaVesselsCompatibleTaxCreditResearchDevelopment", "tax-inc:OtherPreviousCarryOverInvestmentDeductionCompatibleTaxCreditResearchDevelopment", "tax-inc:PreviousThreeTaxPeriodsCarryOverInvestmentDeductionParticularDeductionsCompatibleTaxCreditResearchDevelopment", "tax-inc:BasicTaxableInvestmentDeductionCompatibleTaxCreditResearchDevelopment", "tax-inc:DeductibleAllowanceInvestmentDeductionCompatibleTaxCreditResearchDevelopment", "tax-inc:CarryOverInvestmentDeductionCompatibleTaxCreditResearchDevelopment", "tax-inc:CarryOverInvestmentDeductionSeaVesselsCompatibleTaxCreditResearchDevelopment", "tax-inc:CarryOverInvestmentDeductionParticularDeductionsCompatibleTaxCreditResearchDevelopment" });
            var NonDimensionalElementsTaxCreditResearchDevelopment = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:SpreadTaxCreditResearchDevelopmentPreviousTaxPeriods", "tax-inc:AdditionalTaxCreditResearchDevelopment", "tax-inc:ClearableTaxCreditResearchDevelopmentCurrentTaxPeriod", "tax-inc:ClearableTaxCreditResearchDevelopmentPreviousTaxPeriods", "tax-inc:TaxCreditResearchDevelopmentClearablePreviousTaxPeriodsRepayableCurrentAssessmentYear", "tax-inc:ClearableNonRepayableTaxCreditResearchDevelopmentPreviousTaxPeriods", "tax-inc:TaxCreditResearchDevelopment" });
            if (NonDimensionalElementsIncompatibleTaxCreditResearchDevelopment.Count > 0 || NonDimensionalElementsCompatibleTaxCreditResearchDevelopment.Count > 0 || NonDimensionalElementsTaxCreditResearchDevelopment.Count > 0)
            {
                //CALCULATION HERE
                //FORMULA HERE
                bool test = (((Sum(Number(NonDimensionalElementsIncompatibleTaxCreditResearchDevelopment, "0")) >= (decimal)0)) && ((Sum(Number(NonDimensionalElementsCompatibleTaxCreditResearchDevelopment, "0")) == (decimal)0)) && ((Sum(Number(NonDimensionalElementsTaxCreditResearchDevelopment, "0")) == (decimal)0))) || (((Sum(Number(NonDimensionalElementsCompatibleTaxCreditResearchDevelopment, "0")) >= (decimal)0)) && ((Sum(Number(NonDimensionalElementsIncompatibleTaxCreditResearchDevelopment, "0")) == (decimal)0))) || (((Sum(Number(NonDimensionalElementsTaxCreditResearchDevelopment, "0")) >= (decimal)0)) && ((Sum(Number(NonDimensionalElementsIncompatibleTaxCreditResearchDevelopment, "0")) == (decimal)0)));
                if (!test)
                {
                    // MESSAGES
                    AddMessage(new BizTaxErrorDataContract
                    {
                        FileId = _fileId,
                        Id = "f-idtc-5341",
                        Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Indien geopteerd wordt voor het belastingkrediet voor onderzoek en ontwikkeling, dan mag de sectie 	'{Investeringsaftrek voor vennootschappen die niet opteren voor het belastingkrediet voor onderzoek en ontwikkeling}' 	niet ingevuld worden.	Indien niet wordt geopteerd voor het belastingkrediet voor onderzoek en ontwikkeling, dan mogen de sectie 	'{Investeringsaftrek voor vennootschappen die opteren voor het belastingkrediet voor onderzoek en ontwikkeling}' 	en het formulier 	'{Belastingkrediet voor onderzoek en ontwikkeling}' 	niet worden ingevuld."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Si la société opte pour le crédit d’impôt pour recherche et développement, la section 	'{Déduction pour investissement pour les sociétés qui n'optent pas pour le crédit d’impôt pour recherche et développement}' 	ne peut pas être remplie.	Si la société n'opte pas pour le crédit d’impôt pour recherche et développement, la section 	'{Déduction pour investissement pour les sociétés qui optent pour le crédit d’impôt pour recherche et développement}' 	et le formulaire 	'{Crédit d'impôt pour recherche et développement}' 	ne peuvent pas être remplis."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Wenn die Gesellschaft die Steuergutschrift für Forschung und Entwicklung wählt, kann die Sektion 	'{Investitionsabzug für Gesellschaften, die die Steuergutschrift für Forschung und Entwicklung nicht wählen}' 	nicht ausgefüllt werden.	Wenn die Gesellschaft die Steuergutschrift für Forschung und Entwicklung nicht wählt, kann die Sektion 	'{Investitionsabzug für Gesellschaften, die die Steuergutschrift für Forschung und Entwicklung wählen}' 	und das Formular 	'{Steuergutschrift für Forschung und Entwicklung}' 	nicht ausgefüllt werden."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Indien geopteerd wordt voor het belastingkrediet voor onderzoek en ontwikkeling, dan mag de sectie 	'{Investeringsaftrek voor vennootschappen die niet opteren voor het belastingkrediet voor onderzoek en ontwikkeling}' 	niet ingevuld worden.	Indien niet wordt geopteerd voor het belastingkrediet voor onderzoek en ontwikkeling, dan mogen de sectie 	'{Investeringsaftrek voor vennootschappen die opteren voor het belastingkrediet voor onderzoek en ontwikkeling}' 	en het formulier 	'{Belastingkrediet voor onderzoek en ontwikkeling}' 	niet worden ingevuld."}  
 }
                        ,
                        Fields = new List<string>()
                    }
                    );
                }
            }

        }

        internal void Calc_f_idtc_5343_assertion_set()
        {
            string scenarioId = GetScenarioId("d-incc:IncentiveCategoryDimension::d-incc:InvestmentDeductionIncompatibleTaxCreditResearchDevelopmentMember/d-invc:InvestmentCategoryDimension::d-invc:PatentsMember");
            string period = "D";
            // SCENARIO: "d-incc:IncentiveCategoryDimension::d-incc:InvestmentDeductionIncompatibleTaxCreditResearchDevelopmentMember/d-invc:InvestmentCategoryDimension::d-invc:PatentsMember"
            // PERIOD: "D"
            var InvestmentIncentiveOneGo_Patents = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:InvestmentIncentiveOneGo" });
            var AcquisitionInvestmentValue_Patents = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:AcquisitionInvestmentValue" });
            var FormulaTarget = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:InvestmentIncentiveOneGo" });
            if (AcquisitionInvestmentValue_Patents.Count > 0 || InvestmentIncentiveOneGo_Patents.Count > 0 || FormulaTarget.Count > 0)
            {
                var CalculatedAmountInvestmentIncentiveOneGo_Patents = Math.Round(Number(AcquisitionInvestmentValue_Patents, "0") * Number(InvestmentDeduction_Patents, "0.1350") * (decimal)100, MidpointRounding.AwayFromZero) / (decimal)100;
                //CALCULATION HERE
                if (FormulaTarget.Count == 0)
                {
                    FormulaTarget = CreateNewCalculationElement(scenarioId, period, "tax-inc:InvestmentIncentiveOneGo", new string[] { "d-incc:IncentiveCategoryDimension::d-incc:InvestmentDeductionIncompatibleTaxCreditResearchDevelopmentMember/d-invc:InvestmentCategoryDimension::d-invc:PatentsMember" }, "EUR", "INF");
                }
                if (FormulaTarget.Count > 1)
                {
                    throw new Exception("More then one target for FormulaTarget in assertion f-idtc-5343");
                }
                FormulaTarget.First().Calculated = true;
                FormulaTarget.First().SetNumber((Math.Round(Number(AcquisitionInvestmentValue_Patents, "0") * Number(InvestmentDeduction_Patents, "0.1350") * (decimal)100, MidpointRounding.AwayFromZero) / (decimal)100));
                //FORMULA HERE
                bool test = Number(InvestmentIncentiveOneGo_Patents, "0") == (Math.Round(Number(AcquisitionInvestmentValue_Patents, "0") * Number(InvestmentDeduction_Patents, "0.1350") * (decimal)100, MidpointRounding.AwayFromZero) / (decimal)100);
                if (!test)
                {
                    // MESSAGES
                    AddMessage(new BizTaxErrorDataContract
                    {
                        FileId = _fileId,
                        Id = "f-idtc-5343",
                        Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "De investeringen in '{Octrooien}' die aan de wettelijke voorwaarden voldoen geven recht op een investeringsaftrek die gelijk is aan {$InvestmentDeduction_Patents*100}% van de aanschaffings- of beleggingswaarde van deze investeringen 	(= {$CalculatedAmountInvestmentIncentiveOneGo_Patents})."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Les investissements en '{Brevets}' qui répondent aux conditions légales donnent droit à une déduction pour investissement égale à {$InvestmentDeduction_Patents*100}% de la valeur d'investissement ou de revient de ces investissements 	(= {$CalculatedAmountInvestmentIncentiveOneGo_Patents}).	"}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Die Investitionen in '{Patente}', die die gesetzlichen Bedingungen erfüllen, berechtigen zu einem Investitionsabzug, {$InvestmentDeduction_Patents*100}% des Investitions- oder Anschaffungswertes dieser Investitionen entspricht 	(= {$CalculatedAmountInvestmentIncentiveOneGo_Patents}).	"}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "De investeringen in '{Octrooien}' die aan de wettelijke voorwaarden voldoen geven recht op een investeringsaftrek die gelijk is aan {$InvestmentDeduction_Patents*100}% van de aanschaffings- of beleggingswaarde van deze investeringen 	(= {$CalculatedAmountInvestmentIncentiveOneGo_Patents})."}  
 }
                        ,
                        Fields = new List<string>()
                    }
                    );
                }
            }

        }

        internal void Calc_f_idtc_5344_assertion_set()
        {
            string scenarioId = GetScenarioId("d-incc:IncentiveCategoryDimension::d-incc:TaxCreditResearchDevelopmentMember/d-invc:InvestmentCategoryDimension::d-invc:PatentsMember");
            string period = "D";
            // SCENARIO: "d-incc:IncentiveCategoryDimension::d-incc:TaxCreditResearchDevelopmentMember/d-invc:InvestmentCategoryDimension::d-invc:PatentsMember"
            // PERIOD: "D"
            var InvestmentIncentiveOneGo_PatentsTaxCredit = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:InvestmentIncentiveOneGo" });
            var AcquisitionInvestmentValue_PatentsTaxCredit = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:AcquisitionInvestmentValue" });
            var FormulaTarget = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:InvestmentIncentiveOneGo" });
            if (AcquisitionInvestmentValue_PatentsTaxCredit.Count > 0 || InvestmentIncentiveOneGo_PatentsTaxCredit.Count > 0 || FormulaTarget.Count > 0)
            {
                var CalculatedAmountInvestmentIncentiveOneGo_PatentsTaxCredit = Math.Round(Number(AcquisitionInvestmentValue_PatentsTaxCredit, "0") * Number(TaxCredit_Patents, "0.1350") * Number(MaximumRateCorporateTax, "0.3399") * (decimal)100, MidpointRounding.AwayFromZero) / (decimal)100;
                //CALCULATION HERE
                if (FormulaTarget.Count == 0)
                {
                    FormulaTarget = CreateNewCalculationElement(scenarioId, period, "tax-inc:InvestmentIncentiveOneGo", new string[] { "d-incc:IncentiveCategoryDimension::d-incc:TaxCreditResearchDevelopmentMember/d-invc:InvestmentCategoryDimension::d-invc:PatentsMember" }, "EUR", "INF");
                }
                if (FormulaTarget.Count > 1)
                {
                    throw new Exception("More then one target for FormulaTarget in assertion f-idtc-5344");
                }
                FormulaTarget.First().Calculated = true;
                FormulaTarget.First().SetNumber((Math.Round(Number(AcquisitionInvestmentValue_PatentsTaxCredit, "0") * Number(TaxCredit_Patents, "0.1350") * Number(MaximumRateCorporateTax, "0.3399") * (decimal)100, MidpointRounding.AwayFromZero) / (decimal)100));
                //FORMULA HERE
                bool test = Number(InvestmentIncentiveOneGo_PatentsTaxCredit, "0") == (Math.Round(Number(AcquisitionInvestmentValue_PatentsTaxCredit, "0") * Number(TaxCredit_Patents, "0.1350") * Number(MaximumRateCorporateTax, "0.3399") * (decimal)100, MidpointRounding.AwayFromZero) / (decimal)100);
                if (!test)
                {
                    // MESSAGES
                    AddMessage(new BizTaxErrorDataContract
                    {
                        FileId = _fileId,
                        Id = "f-idtc-5344",
                        Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "De investeringen in '{Octrooien}' die aan de wettelijke voorwaarden voldoen geven recht op een investeringsaftrek die gelijk is aan {$TaxCredit_Patents*100}% van de aanschaffings- of beleggingswaarde van die investeringen vermenigvuldigd met het tarief 	(= {$CalculatedAmountInvestmentIncentiveOneGo_PatentsTaxCredit})."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Les investissements en '{Brevets}' qui répondent aux conditions légales donnent droit à une déduction pour investissement égale à {$TaxCredit_Patents*100}% de la valeur d'investissement ou de revient de ces investissements multiplié avec le tarif 	(= {$CalculatedAmountInvestmentIncentiveOneGo_PatentsTaxCredit})."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Die Investitionen in '{Patente}', die die gesetzlichen Bedingungen erfüllen, berechtigen zu einem Investitionsabzug, {$TaxCredit_Patents*100}% des Investitions- oder Anschaffungswertes dieser Investitionen, multipliziert mit dem Satz, entspricht 	(= {$CalculatedAmountInvestmentIncentiveOneGo_PatentsTaxCredit})."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "De investeringen in '{Octrooien}' die aan de wettelijke voorwaarden voldoen geven recht op een investeringsaftrek die gelijk is aan {$TaxCredit_Patents*100}% van de aanschaffings- of beleggingswaarde van die investeringen vermenigvuldigd met het tarief 	(= {$CalculatedAmountInvestmentIncentiveOneGo_PatentsTaxCredit})."}  
 }
                        ,
                        Fields = new List<string>()
                    }
                    );
                }
            }

        }

        internal void Calc_f_idtc_5345_assertion_set()
        {
            string scenarioId = GetScenarioId("d-incc:IncentiveCategoryDimension::d-incc:InvestmentDeductionIncompatibleTaxCreditResearchDevelopmentMember/d-invc:InvestmentCategoryDimension::d-invc:ResearchDevelopmentMember");
            string period = "D";
            // SCENARIO: "d-incc:IncentiveCategoryDimension::d-incc:InvestmentDeductionIncompatibleTaxCreditResearchDevelopmentMember/d-invc:InvestmentCategoryDimension::d-invc:ResearchDevelopmentMember"
            // PERIOD: "D"
            var InvestmentIncentiveOneGo_ResearchDevelopment = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:InvestmentIncentiveOneGo" });
            var AcquisitionInvestmentValue_ResearchDevelopment = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:AcquisitionInvestmentValue" });
            var FormulaTarget = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:InvestmentIncentiveOneGo" });
            if (AcquisitionInvestmentValue_ResearchDevelopment.Count > 0 || InvestmentIncentiveOneGo_ResearchDevelopment.Count > 0 || FormulaTarget.Count > 0)
            {
                var CalculatedAmountInvestmentIncentiveOneGo_ResearchDevelopment = Math.Round(Number(AcquisitionInvestmentValue_ResearchDevelopment, "0") * Number(InvestmentDeduction_ResearchDevelopment, "0.1350") * (decimal)100, MidpointRounding.AwayFromZero) / (decimal)100;
                //CALCULATION HERE
                if (FormulaTarget.Count == 0)
                {
                    FormulaTarget = CreateNewCalculationElement(scenarioId, period, "tax-inc:InvestmentIncentiveOneGo", new string[] { "d-incc:IncentiveCategoryDimension::d-incc:InvestmentDeductionIncompatibleTaxCreditResearchDevelopmentMember/d-invc:InvestmentCategoryDimension::d-invc:ResearchDevelopmentMember" }, "EUR", "INF");
                }
                if (FormulaTarget.Count > 1)
                {
                    throw new Exception("More then one target for FormulaTarget in assertion f-idtc-5345");
                }
                FormulaTarget.First().Calculated = true;
                FormulaTarget.First().SetNumber((Math.Round(Number(AcquisitionInvestmentValue_ResearchDevelopment, "0") * Number(InvestmentDeduction_ResearchDevelopment, "0.1350") * (decimal)100, MidpointRounding.AwayFromZero) / (decimal)100));
                //FORMULA HERE
                bool test = Number(InvestmentIncentiveOneGo_ResearchDevelopment, "0") == (Math.Round(Number(AcquisitionInvestmentValue_ResearchDevelopment, "0") * Number(InvestmentDeduction_ResearchDevelopment, "0.1350") * (decimal)100, MidpointRounding.AwayFromZero) / (decimal)100);
                if (!test)
                {
                    // MESSAGES
                    AddMessage(new BizTaxErrorDataContract
                    {
                        FileId = _fileId,
                        Id = "f-idtc-5345",
                        Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "De investeringen in '{Onderzoek en ontwikkeling}' die aan de wettelijke voorwaarden voldoen geven recht op een investeringsaftrek die gelijk is aan {$InvestmentDeduction_ResearchDevelopment*100}% van de aanschaffings- of beleggingswaarde van die investeringen (= {$CalculatedAmountInvestmentIncentiveOneGo_ResearchDevelopment})."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Les investissements en '{Recherche et développement}' qui répondent aux conditions légales donnent droit à une déduction pour investissement égale à {$InvestmentDeduction_ResearchDevelopment*100}% de la valeur d'investissement ou de revient de ces investissements (= {$CalculatedAmountInvestmentIncentiveOneGo_ResearchDevelopment})."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Die Investitionen in '{Forschung und Entwicklung}', die die gesetzlichen Bedingungen erfüllen, berechtigen zu einem Investitionsabzug, {$InvestmentDeduction_ResearchDevelopment*100}% des Investitions- oder Anschaffungswertes dieser Investitionen entspricht (= {$CalculatedAmountInvestmentIncentiveOneGo_ResearchDevelopment})."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "De investeringen in '{Onderzoek en ontwikkeling}' die aan de wettelijke voorwaarden voldoen geven recht op een investeringsaftrek die gelijk is aan {$InvestmentDeduction_ResearchDevelopment*100}% van de aanschaffings- of beleggingswaarde van die investeringen (= {$CalculatedAmountInvestmentIncentiveOneGo_ResearchDevelopment})."}  
 }
                        ,
                        Fields = new List<string>()
                    }
                    );
                }
            }

        }

        internal void Calc_f_idtc_5346_assertion_set()
        {
            string scenarioId = GetScenarioId("d-incc:IncentiveCategoryDimension::d-incc:TaxCreditResearchDevelopmentMember/d-invc:InvestmentCategoryDimension::d-invc:ResearchDevelopmentMember");
            string period = "D";
            // SCENARIO: "d-incc:IncentiveCategoryDimension::d-incc:TaxCreditResearchDevelopmentMember/d-invc:InvestmentCategoryDimension::d-invc:ResearchDevelopmentMember"
            // PERIOD: "D"
            var InvestmentIncentiveOneGo_ResearchDevelopmentTaxCredit = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:InvestmentIncentiveOneGo" });
            var AcquisitionInvestmentValue_ResearchDevelopmentTaxCredit = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:AcquisitionInvestmentValue" });
            var FormulaTarget = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:InvestmentIncentiveOneGo" });
            if (AcquisitionInvestmentValue_ResearchDevelopmentTaxCredit.Count > 0 || InvestmentIncentiveOneGo_ResearchDevelopmentTaxCredit.Count > 0 || FormulaTarget.Count > 0)
            {
                var CalculatedAmountInvestmentIncentiveOneGo_ResearchDevelopmentTaxCredit = Math.Round(Number(AcquisitionInvestmentValue_ResearchDevelopmentTaxCredit, "0") * Number(TaxCredit_ResearchDevelopment, "0.1350") * Number(MaximumRateCorporateTax, "0.3399") * (decimal)100, MidpointRounding.AwayFromZero) / (decimal)100;
                //CALCULATION HERE
                if (FormulaTarget.Count == 0)
                {
                    FormulaTarget = CreateNewCalculationElement(scenarioId, period, "tax-inc:InvestmentIncentiveOneGo", new string[] { "d-incc:IncentiveCategoryDimension::d-incc:TaxCreditResearchDevelopmentMember/d-invc:InvestmentCategoryDimension::d-invc:ResearchDevelopmentMember" }, "EUR", "INF");
                }
                if (FormulaTarget.Count > 1)
                {
                    throw new Exception("More then one target for FormulaTarget in assertion f-idtc-5346");
                }
                FormulaTarget.First().Calculated = true;
                FormulaTarget.First().SetNumber((Math.Round(Number(AcquisitionInvestmentValue_ResearchDevelopmentTaxCredit, "0") * Number(TaxCredit_ResearchDevelopment, "0.1350") * Number(MaximumRateCorporateTax, "0.3399") * (decimal)100, MidpointRounding.AwayFromZero) / (decimal)100));
                //FORMULA HERE
                bool test = Number(InvestmentIncentiveOneGo_ResearchDevelopmentTaxCredit, "0") == (Math.Round(Number(AcquisitionInvestmentValue_ResearchDevelopmentTaxCredit, "0") * Number(TaxCredit_ResearchDevelopment, "0.1350") * Number(MaximumRateCorporateTax, "0.3399") * (decimal)100, MidpointRounding.AwayFromZero) / (decimal)100);
                if (!test)
                {
                    // MESSAGES
                    AddMessage(new BizTaxErrorDataContract
                    {
                        FileId = _fileId,
                        Id = "f-idtc-5346",
                        Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "De investeringen in '{Onderzoek en ontwikkeling}'die aan de wettelijke voorwaarden voldoen geven recht op een investeringsaftrek die gelijk is aan {$TaxCredit_ResearchDevelopment*100}% van de aanschaffings- of beleggingswaarde van die investeringen vermenigvuldigd met het tarief (= {$CalculatedAmountInvestmentIncentiveOneGo_ResearchDevelopmentTaxCredit})."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Les investissements en '{Recherche et développement}'qui répondent aux conditions légales donnent droit à une déduction pour investissement égale à {$TaxCredit_ResearchDevelopment*100}% de la valeur d'investissement ou de revient de ces investissements multiplié avec le tarif (= {$CalculatedAmountInvestmentIncentiveOneGo_ResearchDevelopmentTaxCredit})."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Die Investitionen in '{Forschung und Entwicklung}', die die gesetzlichen Bedingungen erfüllen, berechtigen zu einem Investitionsabzug, {$TaxCredit_ResearchDevelopment*100}% des Investitions- oder Anschaffungswertes dieser Investitionen, multipliziert mit dem Satz, entspricht (= {$CalculatedAmountInvestmentIncentiveOneGo_ResearchDevelopmentTaxCredit})."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "De investeringen in '{Onderzoek en ontwikkeling}'die aan de wettelijke voorwaarden voldoen geven recht op een investeringsaftrek die gelijk is aan {$TaxCredit_ResearchDevelopment*100}% van de aanschaffings- of beleggingswaarde van die investeringen vermenigvuldigd met het tarief (= {$CalculatedAmountInvestmentIncentiveOneGo_ResearchDevelopmentTaxCredit})."}  
 }
                        ,
                        Fields = new List<string>()
                    }
                    );
                }
            }

        }

        internal void Calc_f_idtc_5347_assertion_set()
        {
            List<string> scenarioIds = GetScenarioIds(new string[] { "d-incc:IncentiveCategoryDimension::d-incc:InvestmentDeductionIncompatibleTaxCreditResearchDevelopmentMember/d-invc:InvestmentCategoryDimension::d-invc:EnergySavingsMember", "d-incc:IncentiveCategoryDimension::d-incc:InvestmentDeductionCompatibleTaxCreditResearchDevelopmentMember/d-invc:InvestmentCategoryDimension::d-invc:EnergySavingsMember" });
            foreach (string scenarioId in scenarioIds)
            {
                string period = "D";
                // SCENARIO: "d-incc:IncentiveCategoryDimension::d-incc:InvestmentDeductionIncompatibleTaxCreditResearchDevelopmentMember/d-invc:InvestmentCategoryDimension::d-invc:EnergySavingsMember"
                // SCENARIO: "d-incc:IncentiveCategoryDimension::d-incc:InvestmentDeductionCompatibleTaxCreditResearchDevelopmentMember/d-invc:InvestmentCategoryDimension::d-invc:EnergySavingsMember"
                // PERIOD: "D"
                var InvestmentIncentiveOneGo_EnergySavings = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:InvestmentIncentiveOneGo" });
                var AcquisitionInvestmentValue_EnergySavings = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:AcquisitionInvestmentValue" });
                var FormulaTarget = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:InvestmentIncentiveOneGo" });
                if (AcquisitionInvestmentValue_EnergySavings.Count > 0 || InvestmentIncentiveOneGo_EnergySavings.Count > 0 || FormulaTarget.Count > 0)
                {
                    var CalculatedAmountInvestmentIncentiveOneGo_EnergySavings = Math.Round(Number(AcquisitionInvestmentValue_EnergySavings, "0") * Number(InvestmentDeduction_EnergySavings, "0.1350") * (decimal)100, MidpointRounding.AwayFromZero) / (decimal)100;
                    //CALCULATION HERE
                    if (FormulaTarget.Count == 0)
                    {
                        FormulaTarget = CreateNewCalculationElement(scenarioId, period, "tax-inc:InvestmentIncentiveOneGo", new string[] { "d-incc:IncentiveCategoryDimension::d-incc:InvestmentDeductionIncompatibleTaxCreditResearchDevelopmentMember/d-invc:InvestmentCategoryDimension::d-invc:EnergySavingsMember", "d-incc:IncentiveCategoryDimension::d-incc:InvestmentDeductionCompatibleTaxCreditResearchDevelopmentMember/d-invc:InvestmentCategoryDimension::d-invc:EnergySavingsMember" }, "EUR", "INF");
                    }
                    if (FormulaTarget.Count > 1)
                    {
                        throw new Exception("More then one target for FormulaTarget in assertion f-idtc-5347");
                    }
                    FormulaTarget.First().Calculated = true;
                    FormulaTarget.First().SetNumber((Math.Round(Number(AcquisitionInvestmentValue_EnergySavings, "0") * Number(InvestmentDeduction_EnergySavings, "0.1350") * (decimal)100, MidpointRounding.AwayFromZero) / (decimal)100));
                    //FORMULA HERE
                    bool test = Number(InvestmentIncentiveOneGo_EnergySavings, "0") == (Math.Round(Number(AcquisitionInvestmentValue_EnergySavings, "0") * Number(InvestmentDeduction_EnergySavings, "0.1350") * (decimal)100, MidpointRounding.AwayFromZero) / (decimal)100);
                    if (!test)
                    {
                        // MESSAGES
                        AddMessage(new BizTaxErrorDataContract
                        {
                            FileId = _fileId,
                            Id = "f-idtc-5347",
                            Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "De investeringen in '{Energiebesparende investeringen}' die aan de wettelijke voorwaarden voldoen geven recht op een investeringsaftrek die gelijk is aan {$InvestmentDeduction_EnergySavings*100}% van de aanschaffings- of beleggingswaarde van die investeringen (= {$CalculatedAmountInvestmentIncentiveOneGo_EnergySavings})."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Les investissements en '{Investissements économiseurs d’énergie}' qui répondent aux conditions légales donnent droit à une déduction pour investissement égale à {$InvestmentDeduction_EnergySavings*100}% de la valeur d'investissement ou de revient de ces investissements (= {$CalculatedAmountInvestmentIncentiveOneGo_EnergySavings})."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Die Investitionen in '{Energie sparende Investitionen}', die die gesetzlichen Bedingungen erfüllen, berechtigen zu einem Investitionsabzug, {$InvestmentDeduction_EnergySavings*100}% des Investitions- oder Anschaffungswertes dieser Investitionen entspricht (= {$CalculatedAmountInvestmentIncentiveOneGo_EnergySavings})."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "De investeringen in '{Energiebesparende investeringen}' die aan de wettelijke voorwaarden voldoen geven recht op een investeringsaftrek die gelijk is aan {$InvestmentDeduction_EnergySavings*100}% van de aanschaffings- of beleggingswaarde van die investeringen (= {$CalculatedAmountInvestmentIncentiveOneGo_EnergySavings})."}  
 }
                            ,
                            Fields = new List<string>()
                        }
                        );
                    }
                }
            } // end scenario loop

        }

        internal void Calc_f_idtc_5348_assertion_set()
        {
            List<string> scenarioIds = GetScenarioIds(new string[] { "d-incc:IncentiveCategoryDimension::d-incc:InvestmentDeductionIncompatibleTaxCreditResearchDevelopmentMember/d-invc:InvestmentCategoryDimension::d-invc:SmokeExtractionAirTreatmentSystemsHorecaOutletsMember", "d-incc:IncentiveCategoryDimension::d-incc:InvestmentDeductionCompatibleTaxCreditResearchDevelopmentMember/d-invc:InvestmentCategoryDimension::d-invc:SmokeExtractionAirTreatmentSystemsHorecaOutletsMember" });
            foreach (string scenarioId in scenarioIds)
            {
                string period = "D";
                // SCENARIO: "d-incc:IncentiveCategoryDimension::d-incc:InvestmentDeductionIncompatibleTaxCreditResearchDevelopmentMember/d-invc:InvestmentCategoryDimension::d-invc:SmokeExtractionAirTreatmentSystemsHorecaOutletsMember"
                // SCENARIO: "d-incc:IncentiveCategoryDimension::d-incc:InvestmentDeductionCompatibleTaxCreditResearchDevelopmentMember/d-invc:InvestmentCategoryDimension::d-invc:SmokeExtractionAirTreatmentSystemsHorecaOutletsMember"
                // PERIOD: "D"
                var InvestmentIncentiveOneGo_SmokeExtractionAirTreatmentSystemsHorecaOutlets = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:InvestmentIncentiveOneGo" });
                var AcquisitionInvestmentValue_SmokeExtractionAirTreatmentSystemsHorecaOutlets = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:AcquisitionInvestmentValue" });
                var FormulaTarget = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:InvestmentIncentiveOneGo" });
                if (AcquisitionInvestmentValue_SmokeExtractionAirTreatmentSystemsHorecaOutlets.Count > 0 || InvestmentIncentiveOneGo_SmokeExtractionAirTreatmentSystemsHorecaOutlets.Count > 0 || FormulaTarget.Count > 0)
                {
                    var CalculatedAmountInvestmentIncentiveOneGo_SmokeExtractionAirTreatmentSystemsHorecaOutlets = Math.Round(Number(AcquisitionInvestmentValue_SmokeExtractionAirTreatmentSystemsHorecaOutlets, "0") * Number(InvestmentDeduction_SmokeExtractionAirTreatmentSystemsHorecaOutlets, "0.1350") * (decimal)100, MidpointRounding.AwayFromZero) / (decimal)100;
                    //CALCULATION HERE
                    if (FormulaTarget.Count == 0)
                    {
                        FormulaTarget = CreateNewCalculationElement(scenarioId, period, "tax-inc:InvestmentIncentiveOneGo", new string[] { "d-incc:IncentiveCategoryDimension::d-incc:InvestmentDeductionIncompatibleTaxCreditResearchDevelopmentMember/d-invc:InvestmentCategoryDimension::d-invc:SmokeExtractionAirTreatmentSystemsHorecaOutletsMember", "d-incc:IncentiveCategoryDimension::d-incc:InvestmentDeductionCompatibleTaxCreditResearchDevelopmentMember/d-invc:InvestmentCategoryDimension::d-invc:SmokeExtractionAirTreatmentSystemsHorecaOutletsMember" }, "EUR", "INF");
                    }
                    if (FormulaTarget.Count > 1)
                    {
                        throw new Exception("More then one target for FormulaTarget in assertion f-idtc-5348");
                    }
                    FormulaTarget.First().Calculated = true;
                    FormulaTarget.First().SetNumber((Math.Round(Number(AcquisitionInvestmentValue_SmokeExtractionAirTreatmentSystemsHorecaOutlets, "0") * Number(InvestmentDeduction_SmokeExtractionAirTreatmentSystemsHorecaOutlets, "0.1350") * (decimal)100, MidpointRounding.AwayFromZero) / (decimal)100));
                    //FORMULA HERE
                    bool test = Number(InvestmentIncentiveOneGo_SmokeExtractionAirTreatmentSystemsHorecaOutlets, "0") == (Math.Round(Number(AcquisitionInvestmentValue_SmokeExtractionAirTreatmentSystemsHorecaOutlets, "0") * Number(InvestmentDeduction_SmokeExtractionAirTreatmentSystemsHorecaOutlets, "0.1350") * (decimal)100, MidpointRounding.AwayFromZero) / (decimal)100);
                    if (!test)
                    {
                        // MESSAGES
                        AddMessage(new BizTaxErrorDataContract
                        {
                            FileId = _fileId,
                            Id = "f-idtc-5348",
                            Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "De investeringen in '{Rookafzuig- of verluchtingssystemen in horeca-inrichtingen}' die aan de wettelijke voorwaarden voldoen geven recht op een investeringsaftrek die gelijk is aan {$InvestmentDeduction_SmokeExtractionAirTreatmentSystemsHorecaOutlets*100}% van de aanschaffings- of beleggingswaarde van die investeringen (= {$CalculatedAmountInvestmentIncentiveOneGo_SmokeExtractionAirTreatmentSystemsHorecaOutlets})."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Les investissements en '{Systèmes d’extraction ou d’épuration d’air dans des établissements horeca}' qui répondent aux conditions légales donnent droit à une déduction pour investissement égale à {$InvestmentDeduction_SmokeExtractionAirTreatmentSystemsHorecaOutlets*100}% de la valeur d'investissement ou de revient de ces investissements (= {$CalculatedAmountInvestmentIncentiveOneGo_SmokeExtractionAirTreatmentSystemsHorecaOutlets})."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Die Investitionen in '{Luftabzugs- oder Luftreinigungsanlagen in Horeca-Betrieben}', die die gesetzlichen Bedingungen erfüllen, berechtigen zu einem Investitionsabzug, {$InvestmentDeduction_SmokeExtractionAirTreatmentSystemsHorecaOutlets*100}% des Investitions- oder Anschaffungswertes dieser Investitionen entspricht (= {$CalculatedAmountInvestmentIncentiveOneGo_SmokeExtractionAirTreatmentSystemsHorecaOutlets})."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "De investeringen in '{Rookafzuig- of verluchtingssystemen in horeca-inrichtingen}' die aan de wettelijke voorwaarden voldoen geven recht op een investeringsaftrek die gelijk is aan {$InvestmentDeduction_SmokeExtractionAirTreatmentSystemsHorecaOutlets*100}% van de aanschaffings- of beleggingswaarde van die investeringen (= {$CalculatedAmountInvestmentIncentiveOneGo_SmokeExtractionAirTreatmentSystemsHorecaOutlets})."}  
 }
                            ,
                            Fields = new List<string>()
                        }
                        );
                    }
                }
            } // end scenario loop

        }

        internal void Calc_f_idtc_5349_assertion_set()
        {
            List<string> scenarioIds = GetScenarioIds(new string[] { "d-incc:IncentiveCategoryDimension::d-incc:InvestmentDeductionIncompatibleTaxCreditResearchDevelopmentMember/d-invc:InvestmentCategoryDimension::d-invc:PromoteReutilisationRefillableBeveragePackagesReusableIndustrialProductsMember", "d-incc:IncentiveCategoryDimension::d-incc:InvestmentDeductionCompatibleTaxCreditResearchDevelopmentMember/d-invc:InvestmentCategoryDimension::d-invc:PromoteReutilisationRefillableBeveragePackagesReusableIndustrialProductsMember" });
            foreach (string scenarioId in scenarioIds)
            {
                string period = "D";
                // SCENARIO: "d-incc:IncentiveCategoryDimension::d-incc:InvestmentDeductionIncompatibleTaxCreditResearchDevelopmentMember/d-invc:InvestmentCategoryDimension::d-invc:PromoteReutilisationRefillableBeveragePackagesReusableIndustrialProductsMember"
                // SCENARIO: "d-incc:IncentiveCategoryDimension::d-incc:InvestmentDeductionCompatibleTaxCreditResearchDevelopmentMember/d-invc:InvestmentCategoryDimension::d-invc:PromoteReutilisationRefillableBeveragePackagesReusableIndustrialProductsMember"
                // PERIOD: "D"
                var InvestmentIncentiveOneGo_PromoteReutilisationRefillableBeveragePackagesReusableIndustrialProducts = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:InvestmentIncentiveOneGo" });
                var AcquisitionInvestmentValue_PromoteReutilisationRefillableBeveragePackagesReusableIndustrialProducts = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:AcquisitionInvestmentValue" });
                var FormulaTarget = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:InvestmentIncentiveOneGo" });
                if (AcquisitionInvestmentValue_PromoteReutilisationRefillableBeveragePackagesReusableIndustrialProducts.Count > 0 || InvestmentIncentiveOneGo_PromoteReutilisationRefillableBeveragePackagesReusableIndustrialProducts.Count > 0 || FormulaTarget.Count > 0)
                {
                    var CalculatedAmountInvestmentIncentiveOneGo_PromoteReutilisationRefillableBeveragePackagesReusableIndustrialProducts = Math.Round(Number(AcquisitionInvestmentValue_PromoteReutilisationRefillableBeveragePackagesReusableIndustrialProducts, "0") * Number(InvestmentDeduction_PromoteReutilisationRefillableBeveragePackagesReusableIndustrialProducts, "0.0300") * (decimal)100, MidpointRounding.AwayFromZero) / (decimal)100;
                    //CALCULATION HERE
                    if (FormulaTarget.Count == 0)
                    {
                        FormulaTarget = CreateNewCalculationElement(scenarioId, period, "tax-inc:InvestmentIncentiveOneGo", new string[] { "d-incc:IncentiveCategoryDimension::d-incc:InvestmentDeductionIncompatibleTaxCreditResearchDevelopmentMember/d-invc:InvestmentCategoryDimension::d-invc:PromoteReutilisationRefillableBeveragePackagesReusableIndustrialProductsMember", "d-incc:IncentiveCategoryDimension::d-incc:InvestmentDeductionCompatibleTaxCreditResearchDevelopmentMember/d-invc:InvestmentCategoryDimension::d-invc:PromoteReutilisationRefillableBeveragePackagesReusableIndustrialProductsMember" }, "EUR", "INF");
                    }
                    if (FormulaTarget.Count > 1)
                    {
                        throw new Exception("More then one target for FormulaTarget in assertion f-idtc-5349");
                    }
                    FormulaTarget.First().Calculated = true;
                    FormulaTarget.First().SetNumber((Math.Round(Number(AcquisitionInvestmentValue_PromoteReutilisationRefillableBeveragePackagesReusableIndustrialProducts, "0") * Number(InvestmentDeduction_PromoteReutilisationRefillableBeveragePackagesReusableIndustrialProducts, "0.0300") * (decimal)100, MidpointRounding.AwayFromZero) / (decimal)100));
                    //FORMULA HERE
                    bool test = Number(InvestmentIncentiveOneGo_PromoteReutilisationRefillableBeveragePackagesReusableIndustrialProducts, "0") == (Math.Round(Number(AcquisitionInvestmentValue_PromoteReutilisationRefillableBeveragePackagesReusableIndustrialProducts, "0") * Number(InvestmentDeduction_PromoteReutilisationRefillableBeveragePackagesReusableIndustrialProducts, "0.0300") * (decimal)100, MidpointRounding.AwayFromZero) / (decimal)100);
                    if (!test)
                    {
                        // MESSAGES
                        AddMessage(new BizTaxErrorDataContract
                        {
                            FileId = _fileId,
                            Id = "f-idtc-5349",
                            Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "De investeringen in '{Aanmoediging van het hergebruik van verpakkingen}' die aan de wettelijke voorwaarden voldoen geven recht op een investeringsaftrek die gelijk is aan {$InvestmentDeduction_PromoteReutilisationRefillableBeveragePackagesReusableIndustrialProducts*100}% van de aanschaffings- of beleggingswaarde van die investeringen (= {$CalculatedAmountInvestmentIncentiveOneGo_PromoteReutilisationRefillableBeveragePackagesReusableIndustrialProducts})."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Les investissements en '{Incitation à la réutilisation de récipients}' qui répondent aux conditions légales donnent droit à une déduction pour investissement égale à {$InvestmentDeduction_PromoteReutilisationRefillableBeveragePackagesReusableIndustrialProducts*100}% de la valeur d'investissement ou de revient de ces investissements (= {$CalculatedAmountInvestmentIncentiveOneGo_PromoteReutilisationRefillableBeveragePackagesReusableIndustrialProducts})."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Die Investitionen in '{Förderung der Wiederverwertung von Behältern}', die die gesetzlichen Bedingungen erfüllen, berechtigen zu einem Investitionsabzug, {$InvestmentDeduction_PromoteReutilisationRefillableBeveragePackagesReusableIndustrialProducts*100}% des Investitions- oder Anschaffungswertes dieser Investitionen entspricht (= {$CalculatedAmountInvestmentIncentiveOneGo_PromoteReutilisationRefillableBeveragePackagesReusableIndustrialProducts})."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "De investeringen in '{Aanmoediging van het hergebruik van verpakkingen}' die aan de wettelijke voorwaarden voldoen geven recht op een investeringsaftrek die gelijk is aan {$InvestmentDeduction_PromoteReutilisationRefillableBeveragePackagesReusableIndustrialProducts*100}% van de aanschaffings- of beleggingswaarde van die investeringen (= {$CalculatedAmountInvestmentIncentiveOneGo_PromoteReutilisationRefillableBeveragePackagesReusableIndustrialProducts})."}  
 }
                            ,
                            Fields = new List<string>()
                        }
                        );
                    }
                }
            } // end scenario loop

        }

        internal void Calc_f_idtc_5350_assertion_set()
        {
            List<string> scenarioIds = GetScenarioIds(new string[] { "d-incc:IncentiveCategoryDimension::d-incc:InvestmentDeductionIncompatibleTaxCreditResearchDevelopmentMember/d-invc:InvestmentCategoryDimension::d-invc:SecurityDevicesMember", "d-incc:IncentiveCategoryDimension::d-incc:InvestmentDeductionCompatibleTaxCreditResearchDevelopmentMember/d-invc:InvestmentCategoryDimension::d-invc:SecurityDevicesMember" });
            foreach (string scenarioId in scenarioIds)
            {
                string period = "D";
                // SCENARIO: "d-incc:IncentiveCategoryDimension::d-incc:InvestmentDeductionIncompatibleTaxCreditResearchDevelopmentMember/d-invc:InvestmentCategoryDimension::d-invc:SecurityDevicesMember"
                // SCENARIO: "d-incc:IncentiveCategoryDimension::d-incc:InvestmentDeductionCompatibleTaxCreditResearchDevelopmentMember/d-invc:InvestmentCategoryDimension::d-invc:SecurityDevicesMember"
                // PERIOD: "D"
                var InvestmentIncentiveOneGo_SecurityDevices = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:InvestmentIncentiveOneGo" });
                var AcquisitionInvestmentValue_SecurityDevices = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:AcquisitionInvestmentValue" });
                var FormulaTarget = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:InvestmentIncentiveOneGo" });
                if (AcquisitionInvestmentValue_SecurityDevices.Count > 0 || InvestmentIncentiveOneGo_SecurityDevices.Count > 0 || FormulaTarget.Count > 0)
                {
                    var CalculatedAmountInvestmentIncentiveOneGo_SecurityDevices = Math.Round(Number(AcquisitionInvestmentValue_SecurityDevices, "0") * Number(InvestmentDeduction_SecurityDevices, "0.2050") * (decimal)100, MidpointRounding.AwayFromZero) / (decimal)100;
                    //CALCULATION HERE
                    if (FormulaTarget.Count == 0)
                    {
                        FormulaTarget = CreateNewCalculationElement(scenarioId, period, "tax-inc:InvestmentIncentiveOneGo", new string[] { "d-incc:IncentiveCategoryDimension::d-incc:InvestmentDeductionIncompatibleTaxCreditResearchDevelopmentMember/d-invc:InvestmentCategoryDimension::d-invc:SecurityDevicesMember", "d-incc:IncentiveCategoryDimension::d-incc:InvestmentDeductionCompatibleTaxCreditResearchDevelopmentMember/d-invc:InvestmentCategoryDimension::d-invc:SecurityDevicesMember" }, "EUR", "INF");
                    }
                    if (FormulaTarget.Count > 1)
                    {
                        throw new Exception("More then one target for FormulaTarget in assertion f-idtc-5350");
                    }
                    FormulaTarget.First().Calculated = true;
                    FormulaTarget.First().SetNumber((Math.Round(Number(AcquisitionInvestmentValue_SecurityDevices, "0") * Number(InvestmentDeduction_SecurityDevices, "0.2050") * (decimal)100, MidpointRounding.AwayFromZero) / (decimal)100));
                    //FORMULA HERE
                    bool test = Number(InvestmentIncentiveOneGo_SecurityDevices, "0") == (Math.Round(Number(AcquisitionInvestmentValue_SecurityDevices, "0") * Number(InvestmentDeduction_SecurityDevices, "0.2050") * (decimal)100, MidpointRounding.AwayFromZero) / (decimal)100);
                    if (!test)
                    {
                        // MESSAGES
                        AddMessage(new BizTaxErrorDataContract
                        {
                            FileId = _fileId,
                            Id = "f-idtc-5350",
                            Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "De investeringen in '{Beveiliging}' die aan de wettelijke voorwaarden voldoen geven recht op een investeringsaftrek die gelijk is aan {$InvestmentDeduction_SecurityDevices*100}% van de aanschaffings- of beleggingswaarde van die investeringen ({$CalculatedAmountInvestmentIncentiveOneGo_SecurityDevices})."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Les investissements en '{Sécurisation}' qui répondent aux conditions légales donnent droit à une déduction pour investissement égale à {$InvestmentDeduction_SecurityDevices*100}% de la valeur d'investissement ou de revient de ces investissements ({$CalculatedAmountInvestmentIncentiveOneGo_SecurityDevices})."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Die Investitionen in '{Absicherung}', die die gesetzlichen Bedingungen erfüllen, berechtigen zu einem Investitionsabzug, {$InvestmentDeduction_SecurityDevices*100}% des Investitions- oder Anschaffungswertes dieser Investitionen entspricht ({$CalculatedAmountInvestmentIncentiveOneGo_SecurityDevices})."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "De investeringen in '{Beveiliging}' die aan de wettelijke voorwaarden voldoen geven recht op een investeringsaftrek die gelijk is aan {$InvestmentDeduction_SecurityDevices*100}% van de aanschaffings- of beleggingswaarde van die investeringen ({$CalculatedAmountInvestmentIncentiveOneGo_SecurityDevices})."}  
 }
                            ,
                            Fields = new List<string>()
                        }
                        );
                    }
                }
            } // end scenario loop

        }

        internal void Calc_f_idtc_5351_assertion_set()
        {
            List<string> scenarioIds = GetScenarioIds(new string[] { "d-incc:IncentiveCategoryDimension::d-incc:InvestmentDeductionIncompatibleTaxCreditResearchDevelopmentMember/d-invc:InvestmentCategoryDimension::d-invc:SeaVesselsNonBelgianOriginMember", "d-incc:IncentiveCategoryDimension::d-incc:InvestmentDeductionCompatibleTaxCreditResearchDevelopmentMember/d-invc:InvestmentCategoryDimension::d-invc:SeaVesselsNonBelgianOriginMember" });
            foreach (string scenarioId in scenarioIds)
            {
                string period = "D";
                // SCENARIO: "d-incc:IncentiveCategoryDimension::d-incc:InvestmentDeductionIncompatibleTaxCreditResearchDevelopmentMember/d-invc:InvestmentCategoryDimension::d-invc:SeaVesselsNonBelgianOriginMember"
                // SCENARIO: "d-incc:IncentiveCategoryDimension::d-incc:InvestmentDeductionCompatibleTaxCreditResearchDevelopmentMember/d-invc:InvestmentCategoryDimension::d-invc:SeaVesselsNonBelgianOriginMember"
                // PERIOD: "D"
                var InvestmentIncentiveOneGo_SeaVesselsNonBelgianOrigin = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:InvestmentIncentiveOneGo" });
                var AcquisitionInvestmentValue_SeaVesselsNonBelgianOrigin = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:AcquisitionInvestmentValue" });
                var FormulaTarget = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:InvestmentIncentiveOneGo" });
                if (AcquisitionInvestmentValue_SeaVesselsNonBelgianOrigin.Count > 0 || InvestmentIncentiveOneGo_SeaVesselsNonBelgianOrigin.Count > 0 || FormulaTarget.Count > 0)
                {
                    var CalculatedAmountInvestmentIncentiveOneGo_SeaVesselsNonBelgianOrigin = Math.Round(Number(AcquisitionInvestmentValue_SeaVesselsNonBelgianOrigin, "0") * Number(InvestmentDeduction_SeaVesselsNonBelgianOrigin, "0.3000") * (decimal)100, MidpointRounding.AwayFromZero) / (decimal)100;
                    //CALCULATION HERE
                    if (FormulaTarget.Count == 0)
                    {
                        FormulaTarget = CreateNewCalculationElement(scenarioId, period, "tax-inc:InvestmentIncentiveOneGo", new string[] { "d-incc:IncentiveCategoryDimension::d-incc:InvestmentDeductionIncompatibleTaxCreditResearchDevelopmentMember/d-invc:InvestmentCategoryDimension::d-invc:SeaVesselsNonBelgianOriginMember", "d-incc:IncentiveCategoryDimension::d-incc:InvestmentDeductionCompatibleTaxCreditResearchDevelopmentMember/d-invc:InvestmentCategoryDimension::d-invc:SeaVesselsNonBelgianOriginMember" }, "EUR", "INF");
                    }
                    if (FormulaTarget.Count > 1)
                    {
                        throw new Exception("More then one target for FormulaTarget in assertion f-idtc-5351");
                    }
                    FormulaTarget.First().Calculated = true;
                    FormulaTarget.First().SetNumber((Math.Round(Number(AcquisitionInvestmentValue_SeaVesselsNonBelgianOrigin, "0") * Number(InvestmentDeduction_SeaVesselsNonBelgianOrigin, "0.3000") * (decimal)100, MidpointRounding.AwayFromZero) / (decimal)100));
                    //FORMULA HERE
                    bool test = Number(InvestmentIncentiveOneGo_SeaVesselsNonBelgianOrigin, "0") == (Math.Round(Number(AcquisitionInvestmentValue_SeaVesselsNonBelgianOrigin, "0") * Number(InvestmentDeduction_SeaVesselsNonBelgianOrigin, "0.3000") * (decimal)100, MidpointRounding.AwayFromZero) / (decimal)100);
                    if (!test)
                    {
                        // MESSAGES
                        AddMessage(new BizTaxErrorDataContract
                        {
                            FileId = _fileId,
                            Id = "f-idtc-5351",
                            Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "De investeringen in '{Nieuwe zeeschepen of tweedehandse zeeschepen die voor het eerst in het bezit van een Belgische belastingplichtige komen}' die aan de wettelijke voorwaarden voldoen geven recht op een investeringsaftrek die gelijk is aan {$InvestmentDeduction_SeaVesselsNonBelgianOrigin*100}% van de aanschaffings- of beleggingswaarde van die investeringen ({$CalculatedAmountInvestmentIncentiveOneGo_SeaVesselsNonBelgianOrigin})."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Les investissements en '{Navires neufs ou navires de seconde main qui entrent pour la première fois en la possession d’un contribuable belge}' qui répondent aux conditions légales donnent droit à une déduction pour investissement égale à {$InvestmentDeduction_SeaVesselsNonBelgianOrigin*100}% de la valeur d'investissement ou de revient de ces investissements ({$CalculatedAmountInvestmentIncentiveOneGo_SeaVesselsNonBelgianOrigin})."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Die Investitionen in '{Neue Schiffe oder gebrauchte Schiffe, die erstmals durch einen belgischen Steuerpflichtigen in Besitz genommen werden}', die die gesetzlichen Bedingungen erfüllen, berechtigen zu einem Investitionsabzug, {$InvestmentDeduction_SeaVesselsNonBelgianOrigin*100}% des Investitions- oder Anschaffungswertes dieser Investitionen entspricht ({$CalculatedAmountInvestmentIncentiveOneGo_SeaVesselsNonBelgianOrigin})."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "De investeringen in '{Nieuwe zeeschepen of tweedehandse zeeschepen die voor het eerst in het bezit van een Belgische belastingplichtige komen}' die aan de wettelijke voorwaarden voldoen geven recht op een investeringsaftrek die gelijk is aan {$InvestmentDeduction_SeaVesselsNonBelgianOrigin*100}% van de aanschaffings- of beleggingswaarde van die investeringen ({$CalculatedAmountInvestmentIncentiveOneGo_SeaVesselsNonBelgianOrigin})."}  
 }
                            ,
                            Fields = new List<string>()
                        }
                        );
                    }
                }
            } // end scenario loop

        }

        internal void Calc_f_idtc_5352_assertion_set()
        {
            string scenarioId = GetScenarioId("d-incc:IncentiveCategoryDimension::d-incc:InvestmentDeductionIncompatibleTaxCreditResearchDevelopmentMember/d-invc:InvestmentCategoryDimension::d-invc:ResearchDevelopmentMember");
            string period = "D";
            // SCENARIO: "d-incc:IncentiveCategoryDimension::d-incc:InvestmentDeductionIncompatibleTaxCreditResearchDevelopmentMember/d-invc:InvestmentCategoryDimension::d-invc:ResearchDevelopmentMember"
            // PERIOD: "D"
            var AcquisitionInvestmentValueSpread = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:AcquisitionInvestmentValueSpread" });
            var EligibleDepreciations = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:EligibleDepreciations" });
            if (AcquisitionInvestmentValueSpread.Count > 0 || EligibleDepreciations.Count > 0)
            {
                //CALCULATION HERE
                if ((Number(AcquisitionInvestmentValueSpread, "0") > (decimal)0) || (Number(EligibleDepreciations, "0") > (decimal)0))
                {
                    //FORMULA HERE
                    bool test = (Number(AcquisitionInvestmentValueSpread, "0") > (decimal)0) && (Number(EligibleDepreciations, "0") > (decimal)0);
                    if (!test)
                    {
                        // MESSAGES
                        AddMessage(new BizTaxErrorDataContract
                        {
                            FileId = _fileId,
                            Id = "f-idtc-5352",
                            Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "De rubrieken '{Aanschaffings- of beleggingswaarde, afschrijfbaar}' en '{Aanneembare afschrijvingen}' moeten beide worden ingevuld."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Les rubriques '{Valeur d'acquisition ou d'investissement, amortissable}' et '{Amortissements admissibles}' sont obligatoires à remplir."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Die Rubriken '{Abschreibbarer Investitions- oder Anschaffungswert}' und '{Zulässige Abschreibungen}' sind auszufüllen."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "De rubrieken '{Aanschaffings- of beleggingswaarde, afschrijfbaar}' en '{Aanneembare afschrijvingen}' moeten beide worden ingevuld."}  
 }
                            ,
                            Fields = new List<string>()
                        }
                        );
                    }
                }
            }

        }

        internal void Calc_f_idtc_5353_assertion_set()
        {
            string scenarioId = GetScenarioId("d-incc:IncentiveCategoryDimension::d-incc:InvestmentDeductionIncompatibleTaxCreditResearchDevelopmentMember/d-invc:InvestmentCategoryDimension::d-invc:ResearchDevelopmentMember");
            string period = "D";
            // SCENARIO: "d-incc:IncentiveCategoryDimension::d-incc:InvestmentDeductionIncompatibleTaxCreditResearchDevelopmentMember/d-invc:InvestmentCategoryDimension::d-invc:ResearchDevelopmentMember"
            // PERIOD: "D"
            var EligibleDepreciations_ResearchDevelopment = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:EligibleDepreciations" });
            var AcquisitionInvestmentValueSpread_ResearchDevelopment = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:AcquisitionInvestmentValueSpread" });
            if (EligibleDepreciations_ResearchDevelopment.Count > 0 || AcquisitionInvestmentValueSpread_ResearchDevelopment.Count > 0)
            {
                //CALCULATION HERE
                //FORMULA HERE
                bool test = Number(EligibleDepreciations_ResearchDevelopment, "0") < Number(AcquisitionInvestmentValueSpread_ResearchDevelopment, "0");
                if (!test)
                {
                    // MESSAGES
                    AddMessage(new BizTaxErrorDataContract
                    {
                        FileId = _fileId,
                        Id = "f-idtc-5353",
                        Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Het bedrag vermeld in de rubriek '{Aanneembare afschrijvingen}' moet kleiner zijn dan het bedrag vermeld in de rubriek '{Aanschaffings- of beleggingswaarde, afschrijfbaar}'."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Le montant repris dans la rubrique '{Amortissements admissibles}' doit être inférieure au montant repris dans la rubrique '{Valeur d'acquisition ou d'investissement, amortissable}'."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Der in der Rubrik '{Zulässige Abschreibungen}' angegebene Betrag muss niedriger sein als der in der Rubrik '{Abschreibbarer Investitions- oder Anschaffungswert}' angegebene Betrag."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Het bedrag vermeld in de rubriek '{Aanneembare afschrijvingen}' moet kleiner zijn dan het bedrag vermeld in de rubriek '{Aanschaffings- of beleggingswaarde, afschrijfbaar}'."}  
 }
                        ,
                        Fields = new List<string>()
                    }
                    );
                }
            }

        }

        internal void Calc_f_idtc_5354_assertion_set()
        {
            string scenarioId = GetScenarioId("d-incc:IncentiveCategoryDimension::d-incc:InvestmentDeductionIncompatibleTaxCreditResearchDevelopmentMember/d-invc:InvestmentCategoryDimension::d-invc:ResearchDevelopmentMember");
            string period = "D";
            // SCENARIO: "d-incc:IncentiveCategoryDimension::d-incc:InvestmentDeductionIncompatibleTaxCreditResearchDevelopmentMember/d-invc:InvestmentCategoryDimension::d-invc:ResearchDevelopmentMember"
            // PERIOD: "D"
            var InvestmentIncentiveSpread_ResearchDevelopment = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:InvestmentIncentiveSpread" });
            var EligibleDepreciations_ResearchDevelopment = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:EligibleDepreciations" });
            var FormulaTarget = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:InvestmentIncentiveSpread" });
            if (EligibleDepreciations_ResearchDevelopment.Count > 0 || InvestmentIncentiveSpread_ResearchDevelopment.Count > 0 || FormulaTarget.Count > 0)
            {
                var CalculatedAmountInvestmentIncentiveSpread_ResearchDevelopment = Math.Round(Number(EligibleDepreciations_ResearchDevelopment, "0") * Number(InvestmentDeduction_ResearchDevelopment_Spread, "0.2050") * (decimal)100, MidpointRounding.AwayFromZero) / (decimal)100;
                //CALCULATION HERE
                if (FormulaTarget.Count == 0)
                {
                    FormulaTarget = CreateNewCalculationElement(scenarioId, period, "tax-inc:InvestmentIncentiveSpread", new string[] { "d-incc:IncentiveCategoryDimension::d-incc:InvestmentDeductionIncompatibleTaxCreditResearchDevelopmentMember/d-invc:InvestmentCategoryDimension::d-invc:ResearchDevelopmentMember" }, "EUR", "INF");
                }
                if (FormulaTarget.Count > 1)
                {
                    throw new Exception("More then one target for FormulaTarget in assertion f-idtc-5354");
                }
                FormulaTarget.First().Calculated = true;
                FormulaTarget.First().SetNumber((Math.Round(Number(EligibleDepreciations_ResearchDevelopment, "0") * Number(InvestmentDeduction_ResearchDevelopment_Spread, "0.2050") * (decimal)100, MidpointRounding.AwayFromZero) / (decimal)100));
                //FORMULA HERE
                bool test = Number(InvestmentIncentiveSpread_ResearchDevelopment, "0") == (Math.Round(Number(EligibleDepreciations_ResearchDevelopment, "0") * Number(InvestmentDeduction_ResearchDevelopment_Spread, "0.2050") * (decimal)100, MidpointRounding.AwayFromZero) / (decimal)100);
                if (!test)
                {
                    // MESSAGES
                    AddMessage(new BizTaxErrorDataContract
                    {
                        FileId = _fileId,
                        Id = "f-idtc-5354",
                        Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "De investeringen in '{Onderzoek en ontwikkeling}' die aan de wettelijke voorwaarden voldoen geven recht op een gespreide investeringsaftrek die gelijk is aan {$InvestmentDeduction_ResearchDevelopment_Spread*100}% van de aanneembare afschrijvingen van die investeringen ({$CalculatedAmountInvestmentIncentiveSpread_ResearchDevelopment})."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Les investissements en '{Recherche et développement}' qui répondent aux conditions légales donnent droit à une déduction étalée pour investissement égale à {$InvestmentDeduction_ResearchDevelopment_Spread*100}% de la valeur des amortissements admissibles de ces investissements ({$CalculatedAmountInvestmentIncentiveSpread_ResearchDevelopment})."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Die Investitionen in '{Forschung und Entwicklung}', die die gesetzlichen Bedingungen erfüllen, berechtigen zu einem gestaffelten Investitionsabzug, {$InvestmentDeduction_ResearchDevelopment_Spread*100}% der zulässigen Abschreibungen dieser Investitionen entspricht ({$CalculatedAmountInvestmentIncentiveSpread_ResearchDevelopment})."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "De investeringen in '{Onderzoek en ontwikkeling}' die aan de wettelijke voorwaarden voldoen geven recht op een gespreide investeringsaftrek die gelijk is aan {$InvestmentDeduction_ResearchDevelopment_Spread*100}% van de aanneembare afschrijvingen van die investeringen ({$CalculatedAmountInvestmentIncentiveSpread_ResearchDevelopment})."}  
 }
                        ,
                        Fields = new List<string>()
                    }
                    );
                }
            }

        }

        internal void Calc_f_idtc_5355_assertion_set()
        {
            string scenarioId = GetScenarioId("d-incc:IncentiveCategoryDimension::d-incc:TaxCreditResearchDevelopmentMember/d-invc:InvestmentCategoryDimension::d-invc:ResearchDevelopmentMember");
            string period = "D";
            // SCENARIO: "d-incc:IncentiveCategoryDimension::d-incc:TaxCreditResearchDevelopmentMember/d-invc:InvestmentCategoryDimension::d-invc:ResearchDevelopmentMember"
            // PERIOD: "D"
            var InvestmentIncentiveSpread_ResearchDevelopmentTaxCredit = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:InvestmentIncentiveSpread" });
            var EligibleDepreciations_ResearchDevelopmentTaxCredit = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:EligibleDepreciations" });
            var FormulaTarget = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:InvestmentIncentiveSpread" });
            if (EligibleDepreciations_ResearchDevelopmentTaxCredit.Count > 0 || InvestmentIncentiveSpread_ResearchDevelopmentTaxCredit.Count > 0 || FormulaTarget.Count > 0)
            {
                var CalculatedAmountInvestmentIncentiveSpread_ResearchDevelopmentTaxCredit = Math.Round(Number(EligibleDepreciations_ResearchDevelopmentTaxCredit, "0") * Number(TaxCredit_ResearchDevelopment_Spread, "0.2050") * Number(MaximumRateCorporateTax, "0.3399") * (decimal)100, MidpointRounding.AwayFromZero) / (decimal)100;
                //CALCULATION HERE
                if (FormulaTarget.Count == 0)
                {
                    FormulaTarget = CreateNewCalculationElement(scenarioId, period, "tax-inc:InvestmentIncentiveSpread", new string[] { "d-incc:IncentiveCategoryDimension::d-incc:TaxCreditResearchDevelopmentMember/d-invc:InvestmentCategoryDimension::d-invc:ResearchDevelopmentMember" }, "EUR", "INF");
                }
                if (FormulaTarget.Count > 1)
                {
                    throw new Exception("More then one target for FormulaTarget in assertion f-idtc-5355");
                }
                FormulaTarget.First().Calculated = true;
                FormulaTarget.First().SetNumber((Math.Round(Number(EligibleDepreciations_ResearchDevelopmentTaxCredit, "0") * Number(TaxCredit_ResearchDevelopment_Spread, "0.2050") * Number(MaximumRateCorporateTax, "0.3399") * (decimal)100, MidpointRounding.AwayFromZero) / (decimal)100));
                //FORMULA HERE
                bool test = Number(InvestmentIncentiveSpread_ResearchDevelopmentTaxCredit, "0") == (Math.Round(Number(EligibleDepreciations_ResearchDevelopmentTaxCredit, "0") * Number(TaxCredit_ResearchDevelopment_Spread, "0.2050") * Number(MaximumRateCorporateTax, "0.3399") * (decimal)100, MidpointRounding.AwayFromZero) / (decimal)100);
                if (!test)
                {
                    // MESSAGES
                    AddMessage(new BizTaxErrorDataContract
                    {
                        FileId = _fileId,
                        Id = "f-idtc-5355",
                        Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "De investeringen in '{Onderzoek en ontwikkeling}' die aan de wettelijke voorwaarden voldoen geven recht op een gespreid belastingkrediet dat gelijk is aan {$TaxCredit_ResearchDevelopment_Spread*100}% van de aanneembare afschrijvingen van die investeringen vermenigvuldigd met het tarief, {$MaximumRateCorporateTax*100}%, ({$CalculatedAmountInvestmentIncentiveSpread_ResearchDevelopmentTaxCredit})."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Les investissements en '{Recherche et développement}' qui répondent aux conditions légales donnent droit à une déduction étalée pour investissement égale à {$TaxCredit_ResearchDevelopment_Spread*100}% de la valeur des amortissements admissibles de ces investissements multiplié avec le tarif, {$MaximumRateCorporateTax*100}%, ({$CalculatedAmountInvestmentIncentiveSpread_ResearchDevelopmentTaxCredit})."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Die Investitionen in '{Forschung und Entwicklung}', die die gesetzlichen Bedingungen erfüllen, berechtigen zu einer gestaffelten Steuergutschrift, {$TaxCredit_ResearchDevelopment_Spread*100}% der zulässigen Abschreibungen dieser Investitionen, multipliziert mit dem Satz, {$MaximumRateCorporateTax*100}%, entspricht ({$CalculatedAmountInvestmentIncentiveSpread_ResearchDevelopmentTaxCredit})."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "De investeringen in '{Onderzoek en ontwikkeling}' die aan de wettelijke voorwaarden voldoen geven recht op een gespreid belastingkrediet dat gelijk is aan {$TaxCredit_ResearchDevelopment_Spread*100}% van de aanneembare afschrijvingen van die investeringen vermenigvuldigd met het tarief, {$MaximumRateCorporateTax*100}%, ({$CalculatedAmountInvestmentIncentiveSpread_ResearchDevelopmentTaxCredit})."}  
 }
                        ,
                        Fields = new List<string>()
                    }
                    );
                }
            }

        }

        internal void Calc_f_idtc_5356_assertion_set()
        {
            // HAS NO UNBOUND SEQUENCES
            var InvestmentDeductionOneGoSpreadCurrentTaxPeriod = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:InvestmentDeductionOneGoSpreadCurrentTaxPeriod" });
            var DimensionalElements = GetElementsByScenDefs(new string[] { "d-incc:IncentiveCategoryDimension::d-incc:InvestmentDeductionIncompatibleTaxCreditResearchDevelopmentMember/d-invc:InvestmentCategoryDimension::d-invc:ResearchDevelopmentMember" }, new string[] { "D" }, new string[] { "tax-inc:InvestmentIncentiveOneGo", "tax-inc:InvestmentIncentiveSpread" });
            var FormulaTarget = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:InvestmentDeductionOneGoSpreadCurrentTaxPeriod" });
            if (InvestmentDeductionOneGoSpreadCurrentTaxPeriod.Count > 0 || DimensionalElements.Count > 0 || FormulaTarget.Count > 0)
            {
                var CalculatedAmountDimensionalElements = Sum(Number(DimensionalElements, "0"));
                //CALCULATION HERE
                if (FormulaTarget.Count == 0)
                {
                    FormulaTarget = CreateNewCalculationElement(null, new string[] { "D" }, "tax-inc:InvestmentDeductionOneGoSpreadCurrentTaxPeriod", new string[] { "" }, "EUR", "INF");
                }
                if (FormulaTarget.Count > 1)
                {
                    throw new Exception("More then one target for FormulaTarget in assertion f-idtc-5356");
                }
                FormulaTarget.First().Calculated = true;
                FormulaTarget.First().SetNumber(Sum(Number(DimensionalElements, "0")));
                //FORMULA HERE
                bool test = Number(InvestmentDeductionOneGoSpreadCurrentTaxPeriod, "0") == Sum(Number(DimensionalElements, "0"));
                if (!test)
                {
                    // MESSAGES
                    AddMessage(new BizTaxErrorDataContract
                    {
                        FileId = _fileId,
                        Id = "f-idtc-5356",
                        Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Het totaal vermeld in de rubriek '{Eenmalige en gespreide investeringsaftrek}' is niet juist (= {$CalculatedAmountDimensionalElements})."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Le total repris dans la rubrique '{Déduction pour investissement en une fois et déduction pour investissement étalée}' n'est pas correct (= {$CalculatedAmountDimensionalElements})."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Der in der Rubrik '{Einmaliger Abzug und gestaffelter Abzug}' angegebene Gesamtbetrag ist unzutreffend (= {$CalculatedAmountDimensionalElements})."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Het totaal vermeld in de rubriek '{Eenmalige en gespreide investeringsaftrek}' is niet juist (= {$CalculatedAmountDimensionalElements})."}  
 }
                        ,
                        Fields = new List<string>()
                    }
                    );
                }
            }

        }

        internal void Calc_f_idtc_5357_assertion_set()
        {
            string scenarioId = GetScenarioId("");
            string period = "D";
            // SCENARIO: ""
            // PERIOD: "D"
            var PreviousCarryOverInvestmentDeduction = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:PreviousCarryOverInvestmentDeduction" });
            var PreviousCarryOverInvestmentDeductionSeaVessels = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:PreviousCarryOverInvestmentDeductionSeaVessels" });
            var OtherPreviousCarryOverInvestmentDeduction = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:OtherPreviousCarryOverInvestmentDeduction" });
            var FormulaTarget = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:PreviousCarryOverInvestmentDeduction" });
            if (PreviousCarryOverInvestmentDeduction.Count > 0 || PreviousCarryOverInvestmentDeductionSeaVessels.Count > 0 || OtherPreviousCarryOverInvestmentDeduction.Count > 0 || FormulaTarget.Count > 0)
            {
                var CalculatedAmountPreviousCarryOverInvestmentDeduction = Sum(Number(PreviousCarryOverInvestmentDeductionSeaVessels, "0"), Number(OtherPreviousCarryOverInvestmentDeduction, "0"));
                //CALCULATION HERE
                if (FormulaTarget.Count == 0)
                {
                    FormulaTarget = CreateNewCalculationElement(scenarioId, period, "tax-inc:PreviousCarryOverInvestmentDeduction", new string[] { "" }, "EUR", "INF");
                }
                if (FormulaTarget.Count > 1)
                {
                    throw new Exception("More then one target for FormulaTarget in assertion f-idtc-5357");
                }
                FormulaTarget.First().Calculated = true;
                FormulaTarget.First().SetNumber(Sum(Number(PreviousCarryOverInvestmentDeductionSeaVessels, "0"), Number(OtherPreviousCarryOverInvestmentDeduction, "0")));
                //FORMULA HERE
                bool test = Number(PreviousCarryOverInvestmentDeduction, "0") == Sum(Number(PreviousCarryOverInvestmentDeductionSeaVessels, "0"), Number(OtherPreviousCarryOverInvestmentDeduction, "0"));
                if (!test)
                {
                    // MESSAGES
                    AddMessage(new BizTaxErrorDataContract
                    {
                        FileId = _fileId,
                        Id = "f-idtc-5357",
                        Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Het totaal vermeld in de rubriek '{Gecumuleerde vroegere nog niet afgetrokken investeringsaftrekken}' is niet juist (= {$CalculatedAmountPreviousCarryOverInvestmentDeduction})."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Le total repris dans la rubrique '{Cumul des déductions pour investissement antérieures qui n’ont pas encore été déduites}' n'est pas correct (= {$CalculatedAmountPreviousCarryOverInvestmentDeduction})."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Der Betrag, der angegeben ist in der Rubrik '{Gesamtbetrag der früheren Investitionsabzüge, die noch nicht abgezogen wurden}' ist unzutreffend (= {$CalculatedAmountPreviousCarryOverInvestmentDeduction})."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Het totaal vermeld in de rubriek '{Gecumuleerde vroegere nog niet afgetrokken investeringsaftrekken}' is niet juist (= {$CalculatedAmountPreviousCarryOverInvestmentDeduction})."}  
 }
                        ,
                        Fields = new List<string>()
                    }
                    );
                }
            }

        }

        internal void Calc_f_idtc_5358_assertion_set()
        {
            string scenarioId = GetScenarioId("");
            string period = "D";
            // SCENARIO: ""
            // PERIOD: "D"
            var BasicTaxableInvestmentDeduction = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:BasicTaxableInvestmentDeduction" });
            var InvestmentDeductionOneGoSpreadCurrentTaxPeriod = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:InvestmentDeductionOneGoSpreadCurrentTaxPeriod" });
            var InvestmentDeductionSpreadPreviousTaxPeriods = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:InvestmentDeductionSpreadPreviousTaxPeriods" });
            var PreviousCarryOverInvestmentDeduction = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:PreviousCarryOverInvestmentDeduction" });
            var FormulaTarget = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:BasicTaxableInvestmentDeduction" });
            if (BasicTaxableInvestmentDeduction.Count > 0 || InvestmentDeductionOneGoSpreadCurrentTaxPeriod.Count > 0 || InvestmentDeductionSpreadPreviousTaxPeriods.Count > 0 || PreviousCarryOverInvestmentDeduction.Count > 0 || FormulaTarget.Count > 0)
            {
                var CalculatedAmountBasicTaxableInvestmentDeduction = Sum(Number(InvestmentDeductionOneGoSpreadCurrentTaxPeriod, "0"), Number(InvestmentDeductionSpreadPreviousTaxPeriods, "0"), Number(PreviousCarryOverInvestmentDeduction, "0"));
                //CALCULATION HERE
                if (FormulaTarget.Count == 0)
                {
                    FormulaTarget = CreateNewCalculationElement(scenarioId, period, "tax-inc:BasicTaxableInvestmentDeduction", new string[] { "" }, "EUR", "INF");
                }
                if (FormulaTarget.Count > 1)
                {
                    throw new Exception("More then one target for FormulaTarget in assertion f-idtc-5358");
                }
                FormulaTarget.First().Calculated = true;
                FormulaTarget.First().SetNumber(Sum(Number(InvestmentDeductionOneGoSpreadCurrentTaxPeriod, "0"), Number(InvestmentDeductionSpreadPreviousTaxPeriods, "0"), Number(PreviousCarryOverInvestmentDeduction, "0")));
                //FORMULA HERE
                bool test = Number(BasicTaxableInvestmentDeduction, "0") == Sum(Number(InvestmentDeductionOneGoSpreadCurrentTaxPeriod, "0"), Number(InvestmentDeductionSpreadPreviousTaxPeriods, "0"), Number(PreviousCarryOverInvestmentDeduction, "0"));
                if (!test)
                {
                    // MESSAGES
                    AddMessage(new BizTaxErrorDataContract
                    {
                        FileId = _fileId,
                        Id = "f-idtc-5358",
                        Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Het totaal vermeld in de rubriek '{In beginsel aftrekbare investeringsaftrek}' is niet juist (= {$CalculatedAmountBasicTaxableInvestmentDeduction})."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Le total repris dans la rubrique '{Déduction pour investissement déductible en principe}' n'est pas correct (= {$CalculatedAmountBasicTaxableInvestmentDeduction})."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Der Betrag, der angegeben ist in der Rubrik '{Grundsätzlich abziehbare Investitionsabzüge}' ist unzutreffend (= {$CalculatedAmountBasicTaxableInvestmentDeduction})."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Het totaal vermeld in de rubriek '{In beginsel aftrekbare investeringsaftrek}' is niet juist (= {$CalculatedAmountBasicTaxableInvestmentDeduction})."}  
 }
                        ,
                        Fields = new List<string>()
                    }
                    );
                }
            }

        }

        internal void Calc_f_idtc_5359_assertion_set()
        {
            string scenarioId = GetScenarioId("");
            string period = "D";
            // SCENARIO: ""
            // PERIOD: "D"
            var DeductibleAllowanceInvestmentDeductionIncompatibleTaxCreditResearchDevelopment = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:DeductibleAllowanceInvestmentDeductionIncompatibleTaxCreditResearchDevelopment" });
            var InvestmentDeductionOneGoSpreadCurrentTaxPeriod = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:InvestmentDeductionOneGoSpreadCurrentTaxPeriod" });
            var InvestmentDeductionSpreadPreviousTaxPeriods = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:InvestmentDeductionSpreadPreviousTaxPeriods" });
            var PreviousCarryOverInvestmentDeductionSeaVessels = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:PreviousCarryOverInvestmentDeductionSeaVessels" });
            var OtherPreviousCarryOverInvestmentDeduction = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:OtherPreviousCarryOverInvestmentDeduction" });
            if (InvestmentDeductionOneGoSpreadCurrentTaxPeriod.Count > 0 || InvestmentDeductionSpreadPreviousTaxPeriods.Count > 0 || PreviousCarryOverInvestmentDeductionSeaVessels.Count > 0 || OtherPreviousCarryOverInvestmentDeduction.Count > 0 || DeductibleAllowanceInvestmentDeductionIncompatibleTaxCreditResearchDevelopment.Count > 0)
            {
                var CalculatedAmountDeductibleAllowanceInvestmentDeductionIncompatibleTaxCreditResearchDevelopment = Sum(Number(InvestmentDeductionOneGoSpreadCurrentTaxPeriod, "0"), Number(InvestmentDeductionSpreadPreviousTaxPeriods, "0"), Number(PreviousCarryOverInvestmentDeductionSeaVessels, "0"), Number(OtherPreviousCarryOverInvestmentDeduction, "0"));
                //CALCULATION HERE
                if ((Number(OtherPreviousCarryOverInvestmentDeduction, "0") <= Number(LowerLimitOtherPreviousCarryOverInvestmentDeduction, "876620")))
                {
                    //FORMULA HERE
                    bool test = Number(DeductibleAllowanceInvestmentDeductionIncompatibleTaxCreditResearchDevelopment, "0") <= Sum(Number(InvestmentDeductionOneGoSpreadCurrentTaxPeriod, "0"), Number(InvestmentDeductionSpreadPreviousTaxPeriods, "0"), Number(PreviousCarryOverInvestmentDeductionSeaVessels, "0"), Number(OtherPreviousCarryOverInvestmentDeduction, "0"));
                    if (!test)
                    {
                        // MESSAGES
                        AddMessage(new BizTaxErrorDataContract
                        {
                            FileId = _fileId,
                            Id = "f-idtc-5359",
                            Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Indien het bedrag vermeld in de rubriek '{Vroegere nog niet afgetrokken investeringsaftrekken met betrekking tot andere investeringen}' kleiner is dan of gelijk is aan {$LowerLimitOtherPreviousCarryOverInvestmentDeduction} EUR, dan dient het bedrag vermeld in de rubriek '{Investeringsaftrek aftrekbaar voor het belastbare tijdperk}' ({$OtherPreviousCarryOverInvestmentDeduction}) niet beperkt te worden."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Si le montant repris dans la rubrique '{Déductions pour investissement antérieures qui n'ont pas encore été déduites concernant d'autres investissements}' est égal ou inférieur à {$LowerLimitOtherPreviousCarryOverInvestmentDeduction} EUR, le montant repris dans la rubrique '{Déduction pour investissement déductible pour la période imposable}' ({$OtherPreviousCarryOverInvestmentDeduction}) ne doit pas être réduit."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Wenn der Betrag, der angegeben ist in der Rubrik '{Frühere noch nicht abgezogene Investitionsabzüge in Bezug auf andere Investitionen}' gleich oder niedriger ist als {$LowerLimitOtherPreviousCarryOverInvestmentDeduction} EUR, ist der in der Rubrik '{Abziehbarer Investitionsabzug für den Besteuerungszeitraum}' angegebene Betrag ({$OtherPreviousCarryOverInvestmentDeduction}) nicht herabzusetzen."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Indien het bedrag vermeld in de rubriek '{Vroegere nog niet afgetrokken investeringsaftrekken met betrekking tot andere investeringen}' kleiner is dan of gelijk is aan {$LowerLimitOtherPreviousCarryOverInvestmentDeduction} EUR, dan dient het bedrag vermeld in de rubriek '{Investeringsaftrek aftrekbaar voor het belastbare tijdperk}' ({$OtherPreviousCarryOverInvestmentDeduction}) niet beperkt te worden."}  
 }
                            ,
                            Fields = new List<string>()
                        }
                        );
                    }
                }
            }

        }

        internal void Calc_f_idtc_5360_assertion_set()
        {
            string scenarioId = GetScenarioId("");
            string period = "D";
            // SCENARIO: ""
            // PERIOD: "D"
            var DeductibleAllowanceInvestmentDeductionIncompatibleTaxCreditResearchDevelopment = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:DeductibleAllowanceInvestmentDeductionIncompatibleTaxCreditResearchDevelopment" });
            var InvestmentDeductionOneGoSpreadCurrentTaxPeriod = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:InvestmentDeductionOneGoSpreadCurrentTaxPeriod" });
            var InvestmentDeductionSpreadPreviousTaxPeriods = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:InvestmentDeductionSpreadPreviousTaxPeriods" });
            var PreviousCarryOverInvestmentDeductionSeaVessels = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:PreviousCarryOverInvestmentDeductionSeaVessels" });
            var OtherPreviousCarryOverInvestmentDeduction = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:OtherPreviousCarryOverInvestmentDeduction" });
            if (InvestmentDeductionOneGoSpreadCurrentTaxPeriod.Count > 0 || InvestmentDeductionSpreadPreviousTaxPeriods.Count > 0 || PreviousCarryOverInvestmentDeductionSeaVessels.Count > 0 || OtherPreviousCarryOverInvestmentDeduction.Count > 0 || DeductibleAllowanceInvestmentDeductionIncompatibleTaxCreditResearchDevelopment.Count > 0)
            {
                var CalculatedAmountMinMaxLimitDeductibleAllowanceInvestmentDeductionIncompatibleTaxCreditResearchDevelopment = Sum(Number(InvestmentDeductionOneGoSpreadCurrentTaxPeriod, "0"), Number(InvestmentDeductionSpreadPreviousTaxPeriods, "0"), Number(PreviousCarryOverInvestmentDeductionSeaVessels, "0"), Number(LowerLimitOtherPreviousCarryOverInvestmentDeduction, "876620"));
                //CALCULATION HERE
                if ((Number(OtherPreviousCarryOverInvestmentDeduction, "0") > Number(LowerLimitOtherPreviousCarryOverInvestmentDeduction, "876620")) && (Number(OtherPreviousCarryOverInvestmentDeduction, "0") <= Number(UpperLimitOtherPreviousCarryOverInvestmentDeduction, "3506470")))
                {
                    //FORMULA HERE
                    bool test = Number(DeductibleAllowanceInvestmentDeductionIncompatibleTaxCreditResearchDevelopment, "0") <= Sum(Number(InvestmentDeductionOneGoSpreadCurrentTaxPeriod, "0"), Number(InvestmentDeductionSpreadPreviousTaxPeriods, "0"), Number(PreviousCarryOverInvestmentDeductionSeaVessels, "0"), Number(LowerLimitOtherPreviousCarryOverInvestmentDeduction, "876620"));
                    if (!test)
                    {
                        // MESSAGES
                        AddMessage(new BizTaxErrorDataContract
                        {
                            FileId = _fileId,
                            Id = "f-idtc-5360",
                            Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Indien het bedrag vermeld in de rubriek 	'{Vroegere nog niet afgetrokken investeringsaftrekken met betrekking tot andere investeringen}' 	groter is dan {$LowerLimitOtherPreviousCarryOverInvestmentDeduction} EUR en kleiner dan of gelijk aan {$UpperLimitOtherPreviousCarryOverInvestmentDeduction} EUR, 	dan dient het bedrag vermeld in de rubriek 	'{Investeringsaftrek aftrekbaar voor het belastbare tijdperk}' 	beperkt te worden tot de som van de bedragen vermeld in de rubrieken 	'{Eenmalige en gespreide investeringsaftrek}', 	'{Gespreide aftrekken voor investeringen van vorige belastbare tijdperken}', 	'{Vroegere nog niet afgetrokken investeringsaftrekken met betrekking tot investeringen in nieuwe of tweedehandse zeeschepen die voor het eerst in het bezit van een Belgische belastingplichtige komen}' 	en 25% van {$UpperLimitOtherPreviousCarryOverInvestmentDeduction} EUR (= {$CalculatedAmountMinMaxLimitDeductibleAllowanceInvestmentDeductionIncompatibleTaxCreditResearchDevelopment})."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Si le montant de la rubrique 	'{Déductions pour investissement antérieures qui n'ont pas encore été déduites concernant d'autres investissements}' 	est supérieur à {$LowerLimitOtherPreviousCarryOverInvestmentDeduction} EUR et est égal ou inférieur à {$UpperLimitOtherPreviousCarryOverInvestmentDeduction} EUR, 	le montant repris dans la rubrique 	'{Déduction pour investissement déductible pour la période imposable}' 	doit être réduit à la somme des montants repris dans les rubriques 	'{Déduction pour investissement en une fois et déduction pour investissement étalée}', 	'{Déductions étalées en raison d’investissements des périodes imposables précédentes}',  	'{Déductions pour investissement antérieures qui n’ont pas encore été déduites concernant des investissements en navires neufs ou en navires de seconde main qui entrent pour la première fois en la possession d’un contribuable belge}' 	et 25% de {$UpperLimitOtherPreviousCarryOverInvestmentDeduction} EUR (= {$CalculatedAmountMinMaxLimitDeductibleAllowanceInvestmentDeductionIncompatibleTaxCreditResearchDevelopment})."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Wenn der Betrag, der angegeben ist in der Rubrik 	'{Frühere noch nicht abgezogene Investitionsabzüge in Bezug auf andere Investitionen}' 	höher ist als {$LowerLimitOtherPreviousCarryOverInvestmentDeduction} EUR und gleich oder niedriger ist als {$UpperLimitOtherPreviousCarryOverInvestmentDeduction} EUR, 	muss der in der Rubrik 	'{Abziehbarer Investitionsabzug für den Besteuerungszeitraum}' 	angegebene Betrag herabgesetzt werden auf die Beträge, die angegeben sind in den Rubriken 	'{Einmaliger Abzug und gestaffelter Abzug}', 	'{Gestaffelte Abzüge für Investitionen der vorhergehenden Besteuerungszeiträume}',  	'{Frühere noch nicht abgezogene Investitionsabzüge in Bezug auf Investitionen in neue oder gebrauchte Schiffe, die erstmals von einem belgischen Steuerpflichtigen in Besitz genommen werden}' 	und  25% von {$UpperLimitOtherPreviousCarryOverInvestmentDeduction} EUR (= {$CalculatedAmountMinMaxLimitDeductibleAllowanceInvestmentDeductionIncompatibleTaxCreditResearchDevelopment})."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Indien het bedrag vermeld in de rubriek 	'{Vroegere nog niet afgetrokken investeringsaftrekken met betrekking tot andere investeringen}' 	groter is dan {$LowerLimitOtherPreviousCarryOverInvestmentDeduction} EUR en kleiner dan of gelijk aan {$UpperLimitOtherPreviousCarryOverInvestmentDeduction} EUR, 	dan dient het bedrag vermeld in de rubriek 	'{Investeringsaftrek aftrekbaar voor het belastbare tijdperk}' 	beperkt te worden tot de som van de bedragen vermeld in de rubrieken 	'{Eenmalige en gespreide investeringsaftrek}', 	'{Gespreide aftrekken voor investeringen van vorige belastbare tijdperken}', 	'{Vroegere nog niet afgetrokken investeringsaftrekken met betrekking tot investeringen in nieuwe of tweedehandse zeeschepen die voor het eerst in het bezit van een Belgische belastingplichtige komen}' 	en 25% van {$UpperLimitOtherPreviousCarryOverInvestmentDeduction} EUR (= {$CalculatedAmountMinMaxLimitDeductibleAllowanceInvestmentDeductionIncompatibleTaxCreditResearchDevelopment})."}  
 }
                            ,
                            Fields = new List<string>()
                        }
                        );
                    }
                }
            }

        }

        internal void Calc_f_idtc_5361_assertion_set()
        {
            string scenarioId = GetScenarioId("");
            string period = "D";
            // SCENARIO: ""
            // PERIOD: "D"
            var DeductibleAllowanceInvestmentDeductionIncompatibleTaxCreditResearchDevelopment = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:DeductibleAllowanceInvestmentDeductionIncompatibleTaxCreditResearchDevelopment" });
            var InvestmentDeductionOneGoSpreadCurrentTaxPeriod = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:InvestmentDeductionOneGoSpreadCurrentTaxPeriod" });
            var InvestmentDeductionSpreadPreviousTaxPeriods = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:InvestmentDeductionSpreadPreviousTaxPeriods" });
            var PreviousCarryOverInvestmentDeductionSeaVessels = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:PreviousCarryOverInvestmentDeductionSeaVessels" });
            var OtherPreviousCarryOverInvestmentDeduction = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:OtherPreviousCarryOverInvestmentDeduction" });
            if (InvestmentDeductionOneGoSpreadCurrentTaxPeriod.Count > 0 || InvestmentDeductionSpreadPreviousTaxPeriods.Count > 0 || PreviousCarryOverInvestmentDeductionSeaVessels.Count > 0 || OtherPreviousCarryOverInvestmentDeduction.Count > 0 || DeductibleAllowanceInvestmentDeductionIncompatibleTaxCreditResearchDevelopment.Count > 0)
            {
                var CalculatedAmountMaxLimitDeductibleAllowanceInvestmentDeductionIncompatibleTaxCreditResearchDevelopment = Sum(Number(InvestmentDeductionOneGoSpreadCurrentTaxPeriod, "0"), Number(InvestmentDeductionSpreadPreviousTaxPeriods, "0"), Number(PreviousCarryOverInvestmentDeductionSeaVessels, "0"), (Math.Round(Number(OtherPreviousCarryOverInvestmentDeduction, "0") * Number(RateOtherPreviousCarryOverInvestmentDeduction, "0.25") * (decimal)100, MidpointRounding.AwayFromZero) / (decimal)100));
                //CALCULATION HERE
                if ((Number(OtherPreviousCarryOverInvestmentDeduction, "0") > Number(UpperLimitOtherPreviousCarryOverInvestmentDeduction, "3506470")))
                {
                    //FORMULA HERE
                    bool test = Number(DeductibleAllowanceInvestmentDeductionIncompatibleTaxCreditResearchDevelopment, "0") <= Sum(Number(InvestmentDeductionOneGoSpreadCurrentTaxPeriod, "0"), Number(InvestmentDeductionSpreadPreviousTaxPeriods, "0"), Number(PreviousCarryOverInvestmentDeductionSeaVessels, "0"), (Math.Round(Number(OtherPreviousCarryOverInvestmentDeduction, "0") * Number(RateOtherPreviousCarryOverInvestmentDeduction, "0.25") * (decimal)100, MidpointRounding.AwayFromZero) / (decimal)100));
                    if (!test)
                    {
                        // MESSAGES
                        AddMessage(new BizTaxErrorDataContract
                        {
                            FileId = _fileId,
                            Id = "f-idtc-5361",
                            Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Indien het bedrag vermeld in de rubriek 	'{Vroegere nog niet afgetrokken investeringsaftrekken met betrekking tot andere investeringen}' 	groter is dan {$UpperLimitOtherPreviousCarryOverInvestmentDeduction} EUR, dan dient het bedrag vermeld in de rubriek 	'{Investeringsaftrek aftrekbaar voor het belastbare tijdperk}' 	beperkt te worden tot de som van de bedragen vermeld in de rubrieken 	'{Eenmalige en gespreide investeringsaftrek}', 	'{Gespreide aftrekken voor investeringen van vorige belastbare tijdperken}', 	'{Vroegere nog niet afgetrokken investeringsaftrekken met betrekking tot investeringen in nieuwe of tweedehandse zeeschepen die voor het eerst in het bezit van een Belgische belastingplichtige komen}' 	en 25% van de rubriek 	'{Vroegere nog niet afgetrokken investeringsaftrekken met betrekking tot andere investeringen}' (= {$CalculatedAmountMaxLimitDeductibleAllowanceInvestmentDeductionIncompatibleTaxCreditResearchDevelopment})."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Si le montant repris dans la rubrique 	'{Déductions pour investissement antérieures qui n'ont pas encore été déduites concernant d'autres investissements}' 	est supérieur à  {$UpperLimitOtherPreviousCarryOverInvestmentDeduction} EUR, le montant de la rubrique 	'{Déduction pour investissement déductible pour la période imposable}' 	doit être réduit à la somme des montants repris dans les rubriques 	'{Déduction pour investissement en une fois et déduction pour investissement étalée}', 	'{Déductions étalées en raison d’investissements des périodes imposables précédentes}',  	'{Déductions pour investissement antérieures qui n’ont pas encore été déduites concernant des investissements en navires neufs ou en navires de seconde main qui entrent pour la première fois en la possession d’un contribuable belge}' 	et 25% de la rubrique 	'{Déductions pour investissement antérieures qui n'ont pas encore été déduites concernant d'autres investissements}' (= {$CalculatedAmountMaxLimitDeductibleAllowanceInvestmentDeductionIncompatibleTaxCreditResearchDevelopment})."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Wenn der Betrag, der angegeben ist in der Rubrik 	'{Frühere noch nicht abgezogene Investitionsabzüge in Bezug auf andere Investitionen}' 	höher ist als {$UpperLimitOtherPreviousCarryOverInvestmentDeduction} EUR, muss der in der Rubrik 	'{Abziehbarer Investitionsabzug für den Besteuerungszeitraum}' 	angegebene Betrag herabgesetzt werden auf die Beträge, die angegeben sind in den Rubriken 	'{Einmaliger Abzug und gestaffelter Abzug}', 	'{Gestaffelte Abzüge für Investitionen der vorhergehenden Besteuerungszeiträume}', 	'{Frühere noch nicht abgezogene Investitionsabzüge in Bezug auf Investitionen in neue oder gebrauchte Schiffe, die erstmals von einem belgischen Steuerpflichtigen in Besitz genommen werden}' 	und auf  25% der Rubrik 	'{Frühere noch nicht abgezogene Investitionsabzüge in Bezug auf andere Investitionen}' (= {$CalculatedAmountMaxLimitDeductibleAllowanceInvestmentDeductionIncompatibleTaxCreditResearchDevelopment})."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Indien het bedrag vermeld in de rubriek 	'{Vroegere nog niet afgetrokken investeringsaftrekken met betrekking tot andere investeringen}' 	groter is dan {$UpperLimitOtherPreviousCarryOverInvestmentDeduction} EUR, dan dient het bedrag vermeld in de rubriek 	'{Investeringsaftrek aftrekbaar voor het belastbare tijdperk}' 	beperkt te worden tot de som van de bedragen vermeld in de rubrieken 	'{Eenmalige en gespreide investeringsaftrek}', 	'{Gespreide aftrekken voor investeringen van vorige belastbare tijdperken}', 	'{Vroegere nog niet afgetrokken investeringsaftrekken met betrekking tot investeringen in nieuwe of tweedehandse zeeschepen die voor het eerst in het bezit van een Belgische belastingplichtige komen}' 	en 25% van de rubriek 	'{Vroegere nog niet afgetrokken investeringsaftrekken met betrekking tot andere investeringen}' (= {$CalculatedAmountMaxLimitDeductibleAllowanceInvestmentDeductionIncompatibleTaxCreditResearchDevelopment})."}  
 }
                            ,
                            Fields = new List<string>()
                        }
                        );
                    }
                }
            }

        }

        internal void Calc_f_idtc_5362_assertion_set()
        {
            string scenarioId = GetScenarioId("");
            string period = "D";
            // SCENARIO: ""
            // PERIOD: "D"
            var CarryOverInvestmentDeduction = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:CarryOverInvestmentDeduction" });
            var BasicTaxableInvestmentDeduction = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:BasicTaxableInvestmentDeduction" });
            var DeductibleAllowanceInvestmentDeductionIncompatibleTaxCreditResearchDevelopment = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:DeductibleAllowanceInvestmentDeductionIncompatibleTaxCreditResearchDevelopment" });
            var FormulaTarget = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:CarryOverInvestmentDeduction" });
            if (CarryOverInvestmentDeduction.Count > 0 || BasicTaxableInvestmentDeduction.Count > 0 || DeductibleAllowanceInvestmentDeductionIncompatibleTaxCreditResearchDevelopment.Count > 0 || FormulaTarget.Count > 0)
            {
                var CalculatedAmountCarryOverInvestmentDeduction = Max(Sum(Number(BasicTaxableInvestmentDeduction, "0"), -Number(DeductibleAllowanceInvestmentDeductionIncompatibleTaxCreditResearchDevelopment, "0")), (decimal)0);
                //CALCULATION HERE
                if (FormulaTarget.Count == 0)
                {
                    FormulaTarget = CreateNewCalculationElement(scenarioId, period, "tax-inc:CarryOverInvestmentDeduction", new string[] { "" }, "EUR", "INF");
                }
                if (FormulaTarget.Count > 1)
                {
                    throw new Exception("More then one target for FormulaTarget in assertion f-idtc-5362");
                }
                FormulaTarget.First().Calculated = true;
                FormulaTarget.First().SetNumber(Sum(Number(BasicTaxableInvestmentDeduction, "0"), -Number(DeductibleAllowanceInvestmentDeductionIncompatibleTaxCreditResearchDevelopment, "0")));
                //FORMULA HERE
                bool test = Number(CarryOverInvestmentDeduction, "0") == Max(Sum(Number(BasicTaxableInvestmentDeduction, "0"), -Number(DeductibleAllowanceInvestmentDeductionIncompatibleTaxCreditResearchDevelopment, "0")), (decimal)0);
                if (!test)
                {
                    // MESSAGES
                    AddMessage(new BizTaxErrorDataContract
                    {
                        FileId = _fileId,
                        Id = "f-idtc-5362",
                        Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Het totaal vermeld in de rubriek '{Over te dragen naar volgende belastbare tijdperken}' is niet juist (= {$CalculatedAmountCarryOverInvestmentDeduction})."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Le total repris dans la rubrique '{A reporter sur les périodes imposables ultérieures}' n'est pas correct (= {$CalculatedAmountCarryOverInvestmentDeduction})."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Der Betrag, der angegeben ist in der Rubrik '{Auf spätere Besteuerungszeiträume zu übertragen}' ist unzutreffend (= {$CalculatedAmountCarryOverInvestmentDeduction})."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Het totaal vermeld in de rubriek '{Over te dragen naar volgende belastbare tijdperken}' is niet juist (= {$CalculatedAmountCarryOverInvestmentDeduction})."}  
 }
                        ,
                        Fields = new List<string>()
                    }
                    );
                }
            }

        }

        internal void Calc_f_idtc_5363_assertion_set()
        {
            string scenarioId = GetScenarioId("");
            string period = "D";
            // SCENARIO: ""
            // PERIOD: "D"
            var CarryOverInvestmentDeduction = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:CarryOverInvestmentDeduction" });
            var CarryOverInvestmentDeductionSeaVessels = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:CarryOverInvestmentDeductionSeaVessels" });
            var OtherCarryOverInvestmentDeduction = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:OtherCarryOverInvestmentDeduction" });
            if (CarryOverInvestmentDeduction.Count > 0 || CarryOverInvestmentDeductionSeaVessels.Count > 0 || OtherCarryOverInvestmentDeduction.Count > 0)
            {
                var CalculatedAmountCarryOverInvestmentDeduction = Sum(Number(CarryOverInvestmentDeductionSeaVessels, "0"), Number(OtherCarryOverInvestmentDeduction, "0"));
                //CALCULATION HERE
                //FORMULA HERE
                bool test = Number(CarryOverInvestmentDeduction, "0") == Sum(Number(CarryOverInvestmentDeductionSeaVessels, "0"), Number(OtherCarryOverInvestmentDeduction, "0"));
                if (!test)
                {
                    // MESSAGES
                    AddMessage(new BizTaxErrorDataContract
                    {
                        FileId = _fileId,
                        Id = "f-idtc-5363",
                        Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Het totaal vermeld in de rubriek '{Over te dragen naar volgende belastbare tijdperken}' is niet juist (= {$CalculatedAmountCarryOverInvestmentDeduction})."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Le total repris dans la rubrique '{A reporter sur les périodes imposables ultérieures}' n'est pas correct (= {$CalculatedAmountCarryOverInvestmentDeduction})."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Der Betrag, der angegeben ist in der Rubrik '{Auf spätere Besteuerungszeiträume zu übertragen}' ist unzutreffend (= {$CalculatedAmountCarryOverInvestmentDeduction})."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Het totaal vermeld in de rubriek '{Over te dragen naar volgende belastbare tijdperken}' is niet juist (= {$CalculatedAmountCarryOverInvestmentDeduction})."}  
 }
                        ,
                        Fields = new List<string>()
                    }
                    );
                }
            }

        }

        internal void Calc_f_idtc_5364_assertion_set()
        {
            string scenarioId = GetScenarioId("");
            string period = "D";
            // SCENARIO: ""
            // PERIOD: "D"
            var PreviousCarryOverInvestmentDeductionCompatibleTaxCreditResearchDevelopment = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:PreviousCarryOverInvestmentDeductionCompatibleTaxCreditResearchDevelopment" });
            var PreviousCarryOverInvestmentDeductionSeaVesselsCompatibleTaxCreditResearchDevelopment = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:PreviousCarryOverInvestmentDeductionSeaVesselsCompatibleTaxCreditResearchDevelopment" });
            var OtherPreviousCarryOverInvestmentDeductionCompatibleTaxCreditResearchDevelopment = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:OtherPreviousCarryOverInvestmentDeductionCompatibleTaxCreditResearchDevelopment" });
            var PreviousThreeTaxPeriodsCarryOverInvestmentDeductionParticularDeductionsCompatibleTaxCreditResearchDevelopment = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:PreviousThreeTaxPeriodsCarryOverInvestmentDeductionParticularDeductionsCompatibleTaxCreditResearchDevelopment" });
            var FormulaTarget = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:PreviousCarryOverInvestmentDeductionCompatibleTaxCreditResearchDevelopment" });
            if (PreviousCarryOverInvestmentDeductionCompatibleTaxCreditResearchDevelopment.Count > 0 || PreviousCarryOverInvestmentDeductionSeaVesselsCompatibleTaxCreditResearchDevelopment.Count > 0 || OtherPreviousCarryOverInvestmentDeductionCompatibleTaxCreditResearchDevelopment.Count > 0 || PreviousThreeTaxPeriodsCarryOverInvestmentDeductionParticularDeductionsCompatibleTaxCreditResearchDevelopment.Count > 0 || FormulaTarget.Count > 0)
            {
                var CalculatedAmountPreviousCarryOverInvestmentDeductionCompatibleTaxCreditResearchDevelopment = Max(Sum(Number(PreviousCarryOverInvestmentDeductionSeaVesselsCompatibleTaxCreditResearchDevelopment, "0"), Number(OtherPreviousCarryOverInvestmentDeductionCompatibleTaxCreditResearchDevelopment, "0"), -Number(PreviousThreeTaxPeriodsCarryOverInvestmentDeductionParticularDeductionsCompatibleTaxCreditResearchDevelopment, "0")), (decimal)0);
                //CALCULATION HERE
                if (FormulaTarget.Count == 0)
                {
                    FormulaTarget = CreateNewCalculationElement(scenarioId, period, "tax-inc:PreviousCarryOverInvestmentDeductionCompatibleTaxCreditResearchDevelopment", new string[] { "" }, "EUR", "INF");
                }
                if (FormulaTarget.Count > 1)
                {
                    throw new Exception("More then one target for FormulaTarget in assertion f-idtc-5364");
                }
                FormulaTarget.First().Calculated = true;
                FormulaTarget.First().SetNumber(Sum(Number(PreviousCarryOverInvestmentDeductionSeaVesselsCompatibleTaxCreditResearchDevelopment, "0"), Number(OtherPreviousCarryOverInvestmentDeductionCompatibleTaxCreditResearchDevelopment, "0"), -Number(PreviousThreeTaxPeriodsCarryOverInvestmentDeductionParticularDeductionsCompatibleTaxCreditResearchDevelopment, "0")));
                //FORMULA HERE
                bool test = Number(PreviousCarryOverInvestmentDeductionCompatibleTaxCreditResearchDevelopment, "0") == Max(Sum(Number(PreviousCarryOverInvestmentDeductionSeaVesselsCompatibleTaxCreditResearchDevelopment, "0"), Number(OtherPreviousCarryOverInvestmentDeductionCompatibleTaxCreditResearchDevelopment, "0"), -Number(PreviousThreeTaxPeriodsCarryOverInvestmentDeductionParticularDeductionsCompatibleTaxCreditResearchDevelopment, "0")), (decimal)0);
                if (!test)
                {
                    // MESSAGES
                    AddMessage(new BizTaxErrorDataContract
                    {
                        FileId = _fileId,
                        Id = "f-idtc-5364",
                        Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Het totaal vermeld in de rubriek '{Gecumuleerde vroegere nog niet afgetrokken investeringsaftrekken}' is niet juist (= {$CalculatedAmountPreviousCarryOverInvestmentDeductionCompatibleTaxCreditResearchDevelopment})."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Le total repris dans la rubrique '{Cumul des déductions pour investissement antérieures qui n’ont pas encore été déduites}' n'est pas correct (= {$CalculatedAmountPreviousCarryOverInvestmentDeductionCompatibleTaxCreditResearchDevelopment})."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Der Betrag, der angegeben ist in der Rubrik '{Gesamtbetrag der früheren Investitionsabzüge, die noch nicht abgezogen wurden}' ist unzutreffend (= {$CalculatedAmountPreviousCarryOverInvestmentDeductionCompatibleTaxCreditResearchDevelopment})."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Het totaal vermeld in de rubriek '{Gecumuleerde vroegere nog niet afgetrokken investeringsaftrekken}' is niet juist (= {$CalculatedAmountPreviousCarryOverInvestmentDeductionCompatibleTaxCreditResearchDevelopment})."}  
 }
                        ,
                        Fields = new List<string>()
                    }
                    );
                }
            }

        }

        internal void Calc_f_idtc_5365_assertion_set()
        {
            string scenarioId = GetScenarioId("");
            string period = "D";
            // SCENARIO: ""
            // PERIOD: "D"
            var OtherPreviousCarryOverInvestmentDeductionCompatibleTaxCreditResearchDevelopment = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:OtherPreviousCarryOverInvestmentDeductionCompatibleTaxCreditResearchDevelopment" });
            var PreviousThreeTaxPeriodsCarryOverInvestmentDeductionParticularDeductionsCompatibleTaxCreditResearchDevelopment = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:PreviousThreeTaxPeriodsCarryOverInvestmentDeductionParticularDeductionsCompatibleTaxCreditResearchDevelopment" });
            if (OtherPreviousCarryOverInvestmentDeductionCompatibleTaxCreditResearchDevelopment.Count > 0 || PreviousThreeTaxPeriodsCarryOverInvestmentDeductionParticularDeductionsCompatibleTaxCreditResearchDevelopment.Count > 0)
            {
                //CALCULATION HERE
                //FORMULA HERE
                bool test = Number(OtherPreviousCarryOverInvestmentDeductionCompatibleTaxCreditResearchDevelopment, "0") >= Number(PreviousThreeTaxPeriodsCarryOverInvestmentDeductionParticularDeductionsCompatibleTaxCreditResearchDevelopment, "0");
                if (!test)
                {
                    // MESSAGES
                    AddMessage(new BizTaxErrorDataContract
                    {
                        FileId = _fileId,
                        Id = "f-idtc-5365",
                        Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Het bedrag vermeld in de rubriek 	'{Vroegere nog niet afgetrokken investeringsaftrekken met betrekking tot andere investeringen}' 	moet groter zijn dan of gelijk zijn aan het bedrag vermeld in de rubriek 	'{Investeringsaftrekken die niet konden worden afgetrokken voor de drie voorafgaande aanslagjaren met betrekking tot de eenmalige investeringsaftrek voor octrooien en de eenmalige en de gespreide investeringsaftrekken voor investeringen in onderzoek en ontwikkeling}'."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Le montant repris dans la rubrique 	'{Déductions pour investissement antérieures qui n'ont pas encore été déduites concernant d'autres investissements}' 	doit être supérieur ou égal au montant repris dans la rubrique 	'{Déductions pour investissement antérieures qui n'ont pas encore été déduites pour les trois exercices d'imposition précédents concernant la déduction pour investissement en une fois en matière de brevets et les déductions pour investissement en une fois et étalée en matière d'investissements pour la recherche et le développement}'."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Der Betrag, der angegeben ist in der Rubrik 	'{Frühere Investitionsabzüge bezüglich anderer Investitionen, die noch nicht abgezogen wurden}' 	muss höher sein als der Betrag, der angegeben ist in der Rubrik 	'{Frühere Abzüge für Investitionen, die nicht für die drei vorangehenden Steuerjahre abgezogen werden konnten in Bezug auf den einmaligen Investitionsabzug in Sachen Patente und für den einmaligen und den gestaffelten Investitionsabzug in Sachen Investitionen für Forschung und Entwicklung}'."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Het bedrag vermeld in de rubriek 	'{Vroegere nog niet afgetrokken investeringsaftrekken met betrekking tot andere investeringen}' 	moet groter zijn dan of gelijk zijn aan het bedrag vermeld in de rubriek 	'{Investeringsaftrekken die niet konden worden afgetrokken voor de drie voorafgaande aanslagjaren met betrekking tot de eenmalige investeringsaftrek voor octrooien en de eenmalige en de gespreide investeringsaftrekken voor investeringen in onderzoek en ontwikkeling}'."}  
 }
                        ,
                        Fields = new List<string>()
                    }
                    );
                }
            }

        }

        internal void Calc_f_idtc_5366_assertion_set()
        {
            // HAS NO UNBOUND SEQUENCES
            var BasicTaxableInvestmentDeductionCompatibleTaxCreditResearchDevelopment = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:BasicTaxableInvestmentDeductionCompatibleTaxCreditResearchDevelopment" });
            var DimensionalElements = GetElementsByScenDefs(new string[] { "d-incc:IncentiveCategoryDimension::d-incc:InvestmentDeductionCompatibleTaxCreditResearchDevelopmentMember/d-invc:InvestmentCategoryDimension::d-invc:EnergySavingsMember", "d-incc:IncentiveCategoryDimension::d-incc:InvestmentDeductionCompatibleTaxCreditResearchDevelopmentMember/d-invc:InvestmentCategoryDimension::d-invc:SmokeExtractionAirTreatmentSystemsHorecaOutletsMember", "d-incc:IncentiveCategoryDimension::d-incc:InvestmentDeductionCompatibleTaxCreditResearchDevelopmentMember/d-invc:InvestmentCategoryDimension::d-invc:PromoteReutilisationRefillableBeveragePackagesReusableIndustrialProductsMember", "d-incc:IncentiveCategoryDimension::d-incc:InvestmentDeductionCompatibleTaxCreditResearchDevelopmentMember/d-invc:InvestmentCategoryDimension::d-invc:SecurityDevicesMember", "d-incc:IncentiveCategoryDimension::d-incc:InvestmentDeductionCompatibleTaxCreditResearchDevelopmentMember/d-invc:InvestmentCategoryDimension::d-invc:SeaVesselsNonBelgianOriginMember", "d-incc:IncentiveCategoryDimension::d-incc:InvestmentDeductionCompatibleTaxCreditResearchDevelopmentMember/d-invc:InvestmentCategoryDimension::d-invc:ElectricVehicleChargingStationsMember" }, new string[] { "D" }, new string[] { "tax-inc:InvestmentIncentiveOneGo" });
            var NonDimensionalElements = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:PreviousCarryOverSpreadInvestmentDeductionNoResearchDevelopmentCompatibleTaxCreditResearchDevelopment", "tax-inc:PreviousCarryOverInvestmentDeductionCompatibleTaxCreditResearchDevelopment" });
            var FormulaTarget = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:BasicTaxableInvestmentDeductionCompatibleTaxCreditResearchDevelopment" });
            if (BasicTaxableInvestmentDeductionCompatibleTaxCreditResearchDevelopment.Count > 0 || NonDimensionalElements.Count > 0 || DimensionalElements.Count > 0 || FormulaTarget.Count > 0)
            {
                var CalculatedAmountBasicTaxableInvestmentDeductionCompatibleTaxCreditResearchDevelopment = Sum(Number(DimensionalElements, "0"), Number(NonDimensionalElements, "0"));
                //CALCULATION HERE
                if (FormulaTarget.Count == 0)
                {
                    FormulaTarget = CreateNewCalculationElement(null, new string[] { "D" }, "tax-inc:BasicTaxableInvestmentDeductionCompatibleTaxCreditResearchDevelopment", new string[] { "" }, "EUR", "INF");
                }
                if (FormulaTarget.Count > 1)
                {
                    throw new Exception("More then one target for FormulaTarget in assertion f-idtc-5366");
                }
                FormulaTarget.First().Calculated = true;
                FormulaTarget.First().SetNumber(Sum(Number(DimensionalElements, "0"), Number(NonDimensionalElements, "0")));
                //FORMULA HERE
                bool test = Number(BasicTaxableInvestmentDeductionCompatibleTaxCreditResearchDevelopment, "0") == Sum(Number(DimensionalElements, "0"), Number(NonDimensionalElements, "0"));
                if (!test)
                {
                    // MESSAGES
                    AddMessage(new BizTaxErrorDataContract
                    {
                        FileId = _fileId,
                        Id = "f-idtc-5366",
                        Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Het totaal vermeld in de rubriek '{In beginsel aftrekbare investeringsaftrek}' is niet juist (= {$CalculatedAmountBasicTaxableInvestmentDeductionCompatibleTaxCreditResearchDevelopment})."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Le total repris dans la rubrique '{Déduction pour investissement déductible en principe}' n'est pas correct (= {$CalculatedAmountBasicTaxableInvestmentDeductionCompatibleTaxCreditResearchDevelopment})."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Der Betrag, der angegeben ist in der Rubrik '{Grundsätzlich abziehbare Investitionsabzüge}' ist unzutreffend (= {$CalculatedAmountBasicTaxableInvestmentDeductionCompatibleTaxCreditResearchDevelopment})."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Het totaal vermeld in de rubriek '{In beginsel aftrekbare investeringsaftrek}' is niet juist (= {$CalculatedAmountBasicTaxableInvestmentDeductionCompatibleTaxCreditResearchDevelopment})."}  
 }
                        ,
                        Fields = new List<string>()
                    }
                    );
                }
            }

        }

        internal void Calc_f_idtc_5367_assertion_set()
        {
            string scenarioId = GetScenarioId("");
            string period = "D";
            // SCENARIO: ""
            // PERIOD: "D"
            var DeductibleAllowanceInvestmentDeductionCompatibleTaxCreditResearchDevelopment = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:DeductibleAllowanceInvestmentDeductionCompatibleTaxCreditResearchDevelopment" });
            var BasicTaxableInvestmentDeductionCompatibleTaxCreditResearchDevelopment = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:BasicTaxableInvestmentDeductionCompatibleTaxCreditResearchDevelopment" });
            var OtherPreviousCarryOverInvestmentDeductionCompatibleTaxCreditResearchDevelopment = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:OtherPreviousCarryOverInvestmentDeductionCompatibleTaxCreditResearchDevelopment" });
            var PreviousCarryOverInvestmentDeductionSeaVesselsCompatibleTaxCreditResearchDevelopment = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:PreviousCarryOverInvestmentDeductionSeaVesselsCompatibleTaxCreditResearchDevelopment" });
            if (OtherPreviousCarryOverInvestmentDeductionCompatibleTaxCreditResearchDevelopment.Count > 0 || PreviousCarryOverInvestmentDeductionSeaVesselsCompatibleTaxCreditResearchDevelopment.Count > 0 || DeductibleAllowanceInvestmentDeductionCompatibleTaxCreditResearchDevelopment.Count > 0 || BasicTaxableInvestmentDeductionCompatibleTaxCreditResearchDevelopment.Count > 0)
            {
                //CALCULATION HERE
                if (Sum(Number(OtherPreviousCarryOverInvestmentDeductionCompatibleTaxCreditResearchDevelopment, "0"), -Number(PreviousCarryOverInvestmentDeductionSeaVesselsCompatibleTaxCreditResearchDevelopment, "0")) <= Number(LowerLimitOtherPreviousCarryOverInvestmentDeductionCompatibleTaxCredit, "438310"))
                {
                    //FORMULA HERE
                    bool test = Number(DeductibleAllowanceInvestmentDeductionCompatibleTaxCreditResearchDevelopment, "0") <= Number(BasicTaxableInvestmentDeductionCompatibleTaxCreditResearchDevelopment, "0");
                    if (!test)
                    {
                        // MESSAGES
                        AddMessage(new BizTaxErrorDataContract
                        {
                            FileId = _fileId,
                            Id = "f-idtc-5367",
                            Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "De aftrek van de overgedragen vrijstelling op de winst mag niet meer bedragen dan {$LowerLimitOtherPreviousCarryOverInvestmentDeductionCompatibleTaxCredit} EUR."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "La déduction de l’exonération reportée sur les bénéfices ne peut pas excéder  {$LowerLimitOtherPreviousCarryOverInvestmentDeductionCompatibleTaxCredit} EUR."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Der Abzug der übertragenen Befreiung auf die Gewinne darf nicht mehr betragen als {$LowerLimitOtherPreviousCarryOverInvestmentDeductionCompatibleTaxCredit} EUR."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "De aftrek van de overgedragen vrijstelling op de winst mag niet meer bedragen dan {$LowerLimitOtherPreviousCarryOverInvestmentDeductionCompatibleTaxCredit} EUR."}  
 }
                            ,
                            Fields = new List<string>()
                        }
                        );
                    }
                }
            }

        }

        internal void Calc_f_idtc_5368_assertion_set()
        {
            string scenarioId = GetScenarioId("");
            string period = "D";
            // SCENARIO: ""
            // PERIOD: "D"
            var DeductibleAllowanceInvestmentDeductionCompatibleTaxCreditResearchDevelopment = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:DeductibleAllowanceInvestmentDeductionCompatibleTaxCreditResearchDevelopment" });
            var BasicTaxableInvestmentDeductionCompatibleTaxCreditResearchDevelopment = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:BasicTaxableInvestmentDeductionCompatibleTaxCreditResearchDevelopment" });
            var OtherPreviousCarryOverInvestmentDeductionCompatibleTaxCreditResearchDevelopment = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:OtherPreviousCarryOverInvestmentDeductionCompatibleTaxCreditResearchDevelopment" });
            var PreviousThreeTaxPeriodsCarryOverInvestmentDeductionParticularDeductionsCompatibleTaxCreditResearchDevelopment = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:PreviousThreeTaxPeriodsCarryOverInvestmentDeductionParticularDeductionsCompatibleTaxCreditResearchDevelopment" });
            var PreviousCarryOverInvestmentDeductionSeaVesselsCompatibleTaxCreditResearchDevelopment = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:PreviousCarryOverInvestmentDeductionSeaVesselsCompatibleTaxCreditResearchDevelopment" });
            if (OtherPreviousCarryOverInvestmentDeductionCompatibleTaxCreditResearchDevelopment.Count > 0 || PreviousCarryOverInvestmentDeductionSeaVesselsCompatibleTaxCreditResearchDevelopment.Count > 0 || DeductibleAllowanceInvestmentDeductionCompatibleTaxCreditResearchDevelopment.Count > 0 || BasicTaxableInvestmentDeductionCompatibleTaxCreditResearchDevelopment.Count > 0 || PreviousThreeTaxPeriodsCarryOverInvestmentDeductionParticularDeductionsCompatibleTaxCreditResearchDevelopment.Count > 0)
            {
                var CalculatedAmountMinMaxLimitDeductibleAllowanceInvestmentDeductionCompatibleTaxCreditResearchDevelopment = Sum(Number(BasicTaxableInvestmentDeductionCompatibleTaxCreditResearchDevelopment, "0"), -Number(OtherPreviousCarryOverInvestmentDeductionCompatibleTaxCreditResearchDevelopment, "0"), Number(PreviousThreeTaxPeriodsCarryOverInvestmentDeductionParticularDeductionsCompatibleTaxCreditResearchDevelopment, "0"), Number(LowerLimitOtherPreviousCarryOverInvestmentDeductionCompatibleTaxCredit, "438310"));
                //CALCULATION HERE
                if ((Sum(Number(OtherPreviousCarryOverInvestmentDeductionCompatibleTaxCreditResearchDevelopment, "0"), -Number(PreviousCarryOverInvestmentDeductionSeaVesselsCompatibleTaxCreditResearchDevelopment, "0")) > Number(LowerLimitOtherPreviousCarryOverInvestmentDeductionCompatibleTaxCredit, "438310")) && (Sum(Number(OtherPreviousCarryOverInvestmentDeductionCompatibleTaxCreditResearchDevelopment, "0"), -Number(PreviousCarryOverInvestmentDeductionSeaVesselsCompatibleTaxCreditResearchDevelopment, "0")) <= Number(UpperLimitOtherPreviousCarryOverInvestmentDeductionCompatibleTaxCredit, "1753240")))
                {
                    //FORMULA HERE
                    bool test = Number(DeductibleAllowanceInvestmentDeductionCompatibleTaxCreditResearchDevelopment, "0") <= Sum(Number(BasicTaxableInvestmentDeductionCompatibleTaxCreditResearchDevelopment, "0"), -Number(OtherPreviousCarryOverInvestmentDeductionCompatibleTaxCreditResearchDevelopment, "0"), Number(PreviousThreeTaxPeriodsCarryOverInvestmentDeductionParticularDeductionsCompatibleTaxCreditResearchDevelopment, "0"), Number(LowerLimitOtherPreviousCarryOverInvestmentDeductionCompatibleTaxCredit, "438310"));
                    if (!test)
                    {
                        // MESSAGES
                        AddMessage(new BizTaxErrorDataContract
                        {
                            FileId = _fileId,
                            Id = "f-idtc-5368",
                            Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Wanneer de aftrek van de overgedragen vrijstelling op de winst van elk van de volgende belastbare tijdperken {$LowerLimitOtherPreviousCarryOverInvestmentDeductionCompatibleTaxCredit} EUR per belastbaar tijdperk overtreft maar het totale bedrag van de overgedragen vrijstelling op het einde van het vorige belastbare tijdperk kleiner is dan of gelijk is aan {$UpperLimitOtherPreviousCarryOverInvestmentDeductionCompatibleTaxCredit} EUR, dan moet de investeringsaftrek kleiner zijn dan de som van alle bestanddelen en 25% van {$UpperLimitOtherPreviousCarryOverInvestmentDeductionCompatibleTaxCredit} EUR (= {$CalculatedAmountMinMaxLimitDeductibleAllowanceInvestmentDeductionCompatibleTaxCreditResearchDevelopment})."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Si la déduction de l’exonération reportée sur les bénéfices de chacune des périodes imposables suivantes excède, par période imposable, {$LowerLimitOtherPreviousCarryOverInvestmentDeductionCompatibleTaxCredit} EUR, le montant total de l’exonération reportée à la fin de la période imposable précédente doit être égal ou inférieur à {$UpperLimitOtherPreviousCarryOverInvestmentDeductionCompatibleTaxCredit} EUR, la déduction pour investissement doit être égale ou inférieure à la somme de ses composants et {$UpperLimitOtherPreviousCarryOverInvestmentDeductionCompatibleTaxCredit} EUR (= {$CalculatedAmountMinMaxLimitDeductibleAllowanceInvestmentDeductionCompatibleTaxCreditResearchDevelopment})."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Wenn der Abzug der übertragenen Befreiung auf die Gewinne jeder der nachstehenden Besteuerungszeiträume  {$LowerLimitOtherPreviousCarryOverInvestmentDeductionCompatibleTaxCredit} EUR per Besteuerungszeitraum überschreitet, der Gesamtbetrag der übertragenen Befreiung am Ende des vorigen Besteuerungszeitraums muss jedoch gleich oder niedriger sein als {$UpperLimitOtherPreviousCarryOverInvestmentDeductionCompatibleTaxCredit} EUR, muss der Investitionsabzug niedriger sein als die Summe sämtlicher Bestandteile und {$UpperLimitOtherPreviousCarryOverInvestmentDeductionCompatibleTaxCredit} EUR (= {$CalculatedAmountMinMaxLimitDeductibleAllowanceInvestmentDeductionCompatibleTaxCreditResearchDevelopment})."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Wanneer de aftrek van de overgedragen vrijstelling op de winst van elk van de volgende belastbare tijdperken {$LowerLimitOtherPreviousCarryOverInvestmentDeductionCompatibleTaxCredit} EUR per belastbaar tijdperk overtreft maar het totale bedrag van de overgedragen vrijstelling op het einde van het vorige belastbare tijdperk kleiner is dan of gelijk is aan {$UpperLimitOtherPreviousCarryOverInvestmentDeductionCompatibleTaxCredit} EUR, dan moet de investeringsaftrek kleiner zijn dan de som van alle bestanddelen en 25% van {$UpperLimitOtherPreviousCarryOverInvestmentDeductionCompatibleTaxCredit} EUR (= {$CalculatedAmountMinMaxLimitDeductibleAllowanceInvestmentDeductionCompatibleTaxCreditResearchDevelopment})."}  
 }
                            ,
                            Fields = new List<string>()
                        }
                        );
                    }
                }
            }

        }

        internal void Calc_f_idtc_5369_assertion_set()
        {
            string scenarioId = GetScenarioId("");
            string period = "D";
            // SCENARIO: ""
            // PERIOD: "D"
            var DeductibleAllowanceInvestmentDeductionCompatibleTaxCreditResearchDevelopment = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:DeductibleAllowanceInvestmentDeductionCompatibleTaxCreditResearchDevelopment" });
            var BasicTaxableInvestmentDeductionCompatibleTaxCreditResearchDevelopment = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:BasicTaxableInvestmentDeductionCompatibleTaxCreditResearchDevelopment" });
            var OtherPreviousCarryOverInvestmentDeductionCompatibleTaxCreditResearchDevelopment = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:OtherPreviousCarryOverInvestmentDeductionCompatibleTaxCreditResearchDevelopment" });
            var PreviousThreeTaxPeriodsCarryOverInvestmentDeductionParticularDeductionsCompatibleTaxCreditResearchDevelopment = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:PreviousThreeTaxPeriodsCarryOverInvestmentDeductionParticularDeductionsCompatibleTaxCreditResearchDevelopment" });
            var PreviousCarryOverInvestmentDeductionSeaVesselsCompatibleTaxCreditResearchDevelopment = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:PreviousCarryOverInvestmentDeductionSeaVesselsCompatibleTaxCreditResearchDevelopment" });
            if (OtherPreviousCarryOverInvestmentDeductionCompatibleTaxCreditResearchDevelopment.Count > 0 || PreviousCarryOverInvestmentDeductionSeaVesselsCompatibleTaxCreditResearchDevelopment.Count > 0 || DeductibleAllowanceInvestmentDeductionCompatibleTaxCreditResearchDevelopment.Count > 0 || BasicTaxableInvestmentDeductionCompatibleTaxCreditResearchDevelopment.Count > 0 || PreviousThreeTaxPeriodsCarryOverInvestmentDeductionParticularDeductionsCompatibleTaxCreditResearchDevelopment.Count > 0)
            {
                var CalculatedAmountMaxLimitDeductibleAllowanceInvestmentDeductionCompatibleTaxCreditResearchDevelopment = Sum(Number(BasicTaxableInvestmentDeductionCompatibleTaxCreditResearchDevelopment, "0"), -(Math.Round(Number(OtherPreviousCarryOverInvestmentDeductionCompatibleTaxCreditResearchDevelopment, "0") * Number(RateOtherPreviousCarryOverInvestmentDeductionCompatibleTaxCreditReverse, "0.75") * (decimal)100, MidpointRounding.AwayFromZero) / (decimal)100), (Math.Round(Number(PreviousThreeTaxPeriodsCarryOverInvestmentDeductionParticularDeductionsCompatibleTaxCreditResearchDevelopment, "0") * Number(RateOtherPreviousCarryOverInvestmentDeductionCompatibleTaxCreditReverse, "0.75") * (decimal)100, MidpointRounding.AwayFromZero) / (decimal)100));
                //CALCULATION HERE
                if (Sum(Number(OtherPreviousCarryOverInvestmentDeductionCompatibleTaxCreditResearchDevelopment, "0"), -Number(PreviousCarryOverInvestmentDeductionSeaVesselsCompatibleTaxCreditResearchDevelopment, "0")) > Number(UpperLimitOtherPreviousCarryOverInvestmentDeductionCompatibleTaxCredit, "1753240"))
                {
                    //FORMULA HERE
                    bool test = Number(DeductibleAllowanceInvestmentDeductionCompatibleTaxCreditResearchDevelopment, "0") <= Sum(Number(BasicTaxableInvestmentDeductionCompatibleTaxCreditResearchDevelopment, "0"), -(Math.Round(Number(OtherPreviousCarryOverInvestmentDeductionCompatibleTaxCreditResearchDevelopment, "0") * Number(RateOtherPreviousCarryOverInvestmentDeductionCompatibleTaxCreditReverse, "0.75") * (decimal)100, MidpointRounding.AwayFromZero) / (decimal)100), (Math.Round(Number(PreviousThreeTaxPeriodsCarryOverInvestmentDeductionParticularDeductionsCompatibleTaxCreditResearchDevelopment, "0") * Number(RateOtherPreviousCarryOverInvestmentDeductionCompatibleTaxCreditReverse, "0.75") * (decimal)100, MidpointRounding.AwayFromZero) / (decimal)100));
                    if (!test)
                    {
                        // MESSAGES
                        AddMessage(new BizTaxErrorDataContract
                        {
                            FileId = _fileId,
                            Id = "f-idtc-5369",
                            Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "De aftrek van de overgedragen vrijstelling op de winst van elk van de volgende belastbare tijdperken wordt beperkt tot 25% van dat totale bedrag wanneer het totale bedrag van de overgedragen vrijstelling op het einde van het vorige belastbare tijdperk {$UpperLimitOtherPreviousCarryOverInvestmentDeductionCompatibleTaxCredit} EUR overtreft (= {$CalculatedAmountMaxLimitDeductibleAllowanceInvestmentDeductionCompatibleTaxCreditResearchDevelopment})."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "La déduction de l'exonération reportée sur les bénéfices ou les profits de chacune des périodes imposables suivantes est limité à  25% de ce montant total si le montant total de l'exonération reportée à la fin de la période imposable précédente excède {$UpperLimitOtherPreviousCarryOverInvestmentDeductionCompatibleTaxCredit} EUR (= {$CalculatedAmountMaxLimitDeductibleAllowanceInvestmentDeductionCompatibleTaxCreditResearchDevelopment})."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Der Abzug der übertragenen Befreiung auf die Gewinne jeder der nachstehenden Besteuerungszeiträume wird auf 25% dieses Gesamtbetrages beschränkt, wenn der Gesamtbetrag der übertragenen Befreiung am Ende des vorigen Besteuerungszeitraums {$UpperLimitOtherPreviousCarryOverInvestmentDeductionCompatibleTaxCredit} EUR überschreitet (= {$CalculatedAmountMaxLimitDeductibleAllowanceInvestmentDeductionCompatibleTaxCreditResearchDevelopment})."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "De aftrek van de overgedragen vrijstelling op de winst van elk van de volgende belastbare tijdperken wordt beperkt tot 25% van dat totale bedrag wanneer het totale bedrag van de overgedragen vrijstelling op het einde van het vorige belastbare tijdperk {$UpperLimitOtherPreviousCarryOverInvestmentDeductionCompatibleTaxCredit} EUR overtreft (= {$CalculatedAmountMaxLimitDeductibleAllowanceInvestmentDeductionCompatibleTaxCreditResearchDevelopment})."}  
 }
                            ,
                            Fields = new List<string>()
                        }
                        );
                    }
                }
            }

        }

        internal void Calc_f_idtc_5370_assertion_set()
        {
            string scenarioId = GetScenarioId("");
            string period = "D";
            // SCENARIO: ""
            // PERIOD: "D"
            var CarryOverInvestmentDeductionCompatibleTaxCreditResearchDevelopment = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:CarryOverInvestmentDeductionCompatibleTaxCreditResearchDevelopment" });
            var BasicTaxableInvestmentDeductionCompatibleTaxCreditResearchDevelopment = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:BasicTaxableInvestmentDeductionCompatibleTaxCreditResearchDevelopment" });
            var DeductibleAllowanceInvestmentDeductionCompatibleTaxCreditResearchDevelopment = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:DeductibleAllowanceInvestmentDeductionCompatibleTaxCreditResearchDevelopment" });
            var FormulaTarget = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:CarryOverInvestmentDeductionCompatibleTaxCreditResearchDevelopment" });
            if (CarryOverInvestmentDeductionCompatibleTaxCreditResearchDevelopment.Count > 0 || BasicTaxableInvestmentDeductionCompatibleTaxCreditResearchDevelopment.Count > 0 || DeductibleAllowanceInvestmentDeductionCompatibleTaxCreditResearchDevelopment.Count > 0 || FormulaTarget.Count > 0)
            {
                var CalculatedAmountCarryOverInvestmentDeductionCompatibleTaxCreditResearchDevelopment = Max(Sum(Number(BasicTaxableInvestmentDeductionCompatibleTaxCreditResearchDevelopment, "0"), -Number(DeductibleAllowanceInvestmentDeductionCompatibleTaxCreditResearchDevelopment, "0")), (decimal)0);
                //CALCULATION HERE
                if (FormulaTarget.Count == 0)
                {
                    FormulaTarget = CreateNewCalculationElement(scenarioId, period, "tax-inc:CarryOverInvestmentDeductionCompatibleTaxCreditResearchDevelopment", new string[] { "" }, "EUR", "INF");
                }
                if (FormulaTarget.Count > 1)
                {
                    throw new Exception("More then one target for FormulaTarget in assertion f-idtc-5370");
                }
                FormulaTarget.First().Calculated = true;
                FormulaTarget.First().SetNumber(Sum(Number(BasicTaxableInvestmentDeductionCompatibleTaxCreditResearchDevelopment, "0"), -Number(DeductibleAllowanceInvestmentDeductionCompatibleTaxCreditResearchDevelopment, "0")));
                //FORMULA HERE
                bool test = Number(CarryOverInvestmentDeductionCompatibleTaxCreditResearchDevelopment, "0") == Max(Sum(Number(BasicTaxableInvestmentDeductionCompatibleTaxCreditResearchDevelopment, "0"), -Number(DeductibleAllowanceInvestmentDeductionCompatibleTaxCreditResearchDevelopment, "0")), (decimal)0);
                if (!test)
                {
                    // MESSAGES
                    AddMessage(new BizTaxErrorDataContract
                    {
                        FileId = _fileId,
                        Id = "f-idtc-5370",
                        Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Het totaal vermeld in de rubriek '{Over te dragen naar volgende belastbare tijdperken}' is niet juist (= {$CalculatedAmountCarryOverInvestmentDeductionCompatibleTaxCreditResearchDevelopment})."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Le total repris dans la rubrique '{A reporter sur les périodes imposables ultérieures}' n'est pas correct (= {$CalculatedAmountCarryOverInvestmentDeductionCompatibleTaxCreditResearchDevelopment})."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Der in der Rubrik '{Auf spätere Besteuerungszeiträume zu übertragen}' angegebene Gesamtbetrag ist unzutreffend (= {$CalculatedAmountCarryOverInvestmentDeductionCompatibleTaxCreditResearchDevelopment})."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Het totaal vermeld in de rubriek '{Over te dragen naar volgende belastbare tijdperken}' is niet juist (= {$CalculatedAmountCarryOverInvestmentDeductionCompatibleTaxCreditResearchDevelopment})."}  
 }
                        ,
                        Fields = new List<string>()
                    }
                    );
                }
            }

        }

        internal void Calc_f_idtc_5371_assertion_set()
        {
            string scenarioId = GetScenarioId("");
            string period = "D";
            // SCENARIO: ""
            // PERIOD: "D"
            var CarryOverInvestmentDeductionCompatibleTaxCreditResearchDevelopment = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:CarryOverInvestmentDeductionCompatibleTaxCreditResearchDevelopment" });
            var CarryOverInvestmentDeductionSeaVesselsCompatibleTaxCreditResearchDevelopment = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:CarryOverInvestmentDeductionSeaVesselsCompatibleTaxCreditResearchDevelopment" });
            var CarryOverInvestmentDeductionParticularDeductionsCompatibleTaxCreditResearchDevelopment = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:CarryOverInvestmentDeductionParticularDeductionsCompatibleTaxCreditResearchDevelopment" });
            if (CarryOverInvestmentDeductionCompatibleTaxCreditResearchDevelopment.Count > 0 || CarryOverInvestmentDeductionSeaVesselsCompatibleTaxCreditResearchDevelopment.Count > 0 || CarryOverInvestmentDeductionParticularDeductionsCompatibleTaxCreditResearchDevelopment.Count > 0)
            {
                var CalculatedAmountCarryOverInvestmentDeductionCompatibleTaxCreditResearchDevelopment = Sum(Number(CarryOverInvestmentDeductionSeaVesselsCompatibleTaxCreditResearchDevelopment, "0"), Number(CarryOverInvestmentDeductionParticularDeductionsCompatibleTaxCreditResearchDevelopment, "0"));
                //CALCULATION HERE
                //FORMULA HERE
                bool test = Number(CarryOverInvestmentDeductionCompatibleTaxCreditResearchDevelopment, "0") == Sum(Number(CarryOverInvestmentDeductionSeaVesselsCompatibleTaxCreditResearchDevelopment, "0"), Number(CarryOverInvestmentDeductionParticularDeductionsCompatibleTaxCreditResearchDevelopment, "0"));
                if (!test)
                {
                    // MESSAGES
                    AddMessage(new BizTaxErrorDataContract
                    {
                        FileId = _fileId,
                        Id = "f-idtc-5371",
                        Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Het totaal vermeld in de rubriek '{Over te dragen naar volgende belastbare tijdperken}' is niet juist (= {$CalculatedAmountCarryOverInvestmentDeductionCompatibleTaxCreditResearchDevelopment})."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Le total repris dans la rubrique '{A reporter sur les périodes imposables ultérieures}' n'est pas correct (= {$CalculatedAmountCarryOverInvestmentDeductionCompatibleTaxCreditResearchDevelopment})."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Der in der Rubrik '{Auf spätere Besteuerungszeiträume zu übertragen}' angegebene Gesamtbetrag ist unzutreffend (= {$CalculatedAmountCarryOverInvestmentDeductionCompatibleTaxCreditResearchDevelopment})."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Het totaal vermeld in de rubriek '{Over te dragen naar volgende belastbare tijdperken}' is niet juist (= {$CalculatedAmountCarryOverInvestmentDeductionCompatibleTaxCreditResearchDevelopment})."}  
 }
                        ,
                        Fields = new List<string>()
                    }
                    );
                }
            }

        }

        internal void Calc_f_idtc_5372_assertion_set()
        {
            // HAS NO UNBOUND SEQUENCES
            var ClearableTaxCreditResearchDevelopmentCurrentTaxPeriod = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:ClearableTaxCreditResearchDevelopmentCurrentTaxPeriod" });
            var DimensionalElements = GetElementsByScenDefs(new string[] { "d-incc:IncentiveCategoryDimension::d-incc:TaxCreditResearchDevelopmentMember/d-invc:InvestmentCategoryDimension::d-invc:ResearchDevelopmentMember" }, new string[] { "D" }, new string[] { "tax-inc:InvestmentIncentiveOneGo", "tax-inc:InvestmentIncentiveSpread" });
            var NonDimensionalElements = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:AdditionalTaxCreditResearchDevelopment", "tax-inc:SpreadTaxCreditResearchDevelopmentPreviousTaxPeriods" });
            var FormulaTarget = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:ClearableTaxCreditResearchDevelopmentCurrentTaxPeriod" });
            if (ClearableTaxCreditResearchDevelopmentCurrentTaxPeriod.Count > 0 || NonDimensionalElements.Count > 0 || DimensionalElements.Count > 0 || FormulaTarget.Count > 0)
            {
                var CalculatedAmountClearableTaxCreditResearchDevelopmentCurrentTaxPeriod = Sum(Number(DimensionalElements, "0"), Number(NonDimensionalElements, "0"));
                //CALCULATION HERE
                if (FormulaTarget.Count == 0)
                {
                    FormulaTarget = CreateNewCalculationElement(null, new string[] { "D" }, "tax-inc:ClearableTaxCreditResearchDevelopmentCurrentTaxPeriod", new string[] { "" }, "EUR", "INF");
                }
                if (FormulaTarget.Count > 1)
                {
                    throw new Exception("More then one target for FormulaTarget in assertion f-idtc-5372");
                }
                FormulaTarget.First().Calculated = true;
                FormulaTarget.First().SetNumber(Sum(Number(DimensionalElements, "0"), Number(NonDimensionalElements, "0")));
                //FORMULA HERE
                bool test = Number(ClearableTaxCreditResearchDevelopmentCurrentTaxPeriod, "0") == Sum(Number(DimensionalElements, "0"), Number(NonDimensionalElements, "0"));
                if (!test)
                {
                    // MESSAGES
                    AddMessage(new BizTaxErrorDataContract
                    {
                        FileId = _fileId,
                        Id = "f-idtc-5372",
                        Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Het totaal vermeld in de rubriek '{In beginsel verrekenbaar belastingkrediet voor onderzoek en ontwikkeling van het belastbaar tijdperk}' is niet juist (= {$CalculatedAmountClearableTaxCreditResearchDevelopmentCurrentTaxPeriod})."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "La rubrique '{Crédit d'impôt pour recherche et développement de la période imposable imputable en principe}' n'est pas correct (= {$CalculatedAmountClearableTaxCreditResearchDevelopmentCurrentTaxPeriod})."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Der in der Rubrik '{Grundsätzlich anrechenbare Steuergutschrift für Forschung und Entwicklung des Besteuerungszeitraums}' angegebene Gesamtbetrag ist unzutreffend (= {$CalculatedAmountClearableTaxCreditResearchDevelopmentCurrentTaxPeriod})."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Het totaal vermeld in de rubriek '{In beginsel verrekenbaar belastingkrediet voor onderzoek en ontwikkeling van het belastbaar tijdperk}' is niet juist (= {$CalculatedAmountClearableTaxCreditResearchDevelopmentCurrentTaxPeriod})."}  
 }
                        ,
                        Fields = new List<string>()
                    }
                    );
                }
            }

        }

        internal void Calc_f_idtc_5374_assertion_set()
        {
            string scenarioId = GetScenarioId("");
            string period = "D";
            // SCENARIO: ""
            // PERIOD: "D"
            var TaxCreditResearchDevelopment = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:TaxCreditResearchDevelopment" });
            var ClearableTaxCreditResearchDevelopmentCurrentTaxPeriod = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:ClearableTaxCreditResearchDevelopmentCurrentTaxPeriod" });
            var ClearableTaxCreditResearchDevelopmentPreviousTaxPeriods = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:ClearableTaxCreditResearchDevelopmentPreviousTaxPeriods" });
            var ClearableNonRepayableTaxCreditResearchDevelopmentPreviousTaxPeriods = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:ClearableNonRepayableTaxCreditResearchDevelopmentPreviousTaxPeriods" });
            var FormulaTarget = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:TaxCreditResearchDevelopment" });
            if (TaxCreditResearchDevelopment.Count > 0 || ClearableTaxCreditResearchDevelopmentCurrentTaxPeriod.Count > 0 || ClearableTaxCreditResearchDevelopmentPreviousTaxPeriods.Count > 0 || ClearableNonRepayableTaxCreditResearchDevelopmentPreviousTaxPeriods.Count > 0 || FormulaTarget.Count > 0)
            {
                var CalculatedAmountTaxCreditResearchDevelopment = Sum(Number(ClearableTaxCreditResearchDevelopmentCurrentTaxPeriod, "0"), Number(ClearableTaxCreditResearchDevelopmentPreviousTaxPeriods, "0"), Number(ClearableNonRepayableTaxCreditResearchDevelopmentPreviousTaxPeriods, "0"));
                //CALCULATION HERE
                if (FormulaTarget.Count == 0)
                {
                    FormulaTarget = CreateNewCalculationElement(scenarioId, period, "tax-inc:TaxCreditResearchDevelopment", new string[] { "" }, "EUR", "INF");
                }
                if (FormulaTarget.Count > 1)
                {
                    throw new Exception("More then one target for FormulaTarget in assertion f-idtc-5374");
                }
                FormulaTarget.First().Calculated = true;
                FormulaTarget.First().SetNumber(Sum(Number(ClearableTaxCreditResearchDevelopmentCurrentTaxPeriod, "0"), Number(ClearableTaxCreditResearchDevelopmentPreviousTaxPeriods, "0"), Number(ClearableNonRepayableTaxCreditResearchDevelopmentPreviousTaxPeriods, "0")));
                //FORMULA HERE
                bool test = Number(TaxCreditResearchDevelopment, "0") == Sum(Number(ClearableTaxCreditResearchDevelopmentCurrentTaxPeriod, "0"), Number(ClearableTaxCreditResearchDevelopmentPreviousTaxPeriods, "0"), Number(ClearableNonRepayableTaxCreditResearchDevelopmentPreviousTaxPeriods, "0"));
                if (!test)
                {
                    // MESSAGES
                    AddMessage(new BizTaxErrorDataContract
                    {
                        FileId = _fileId,
                        Id = "f-idtc-5374",
                        Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Het totaal vermeld in de rubriek '{In beginsel verrekenbaar belastingkrediet voor onderzoek en ontwikkeling}' is niet juist (= {$CalculatedAmountTaxCreditResearchDevelopment})."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Le total repris dans la rubrique '{Crédit d’impôt pour recherche et développement imputable en principe}' n'est pas correct (= {$CalculatedAmountTaxCreditResearchDevelopment})."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Der in der Rubrik '{Grundsätzlich anrechenbare Steuergutschrift für Forschung und Entwicklung}' angegebene Gesamtbetrag ist unzutreffend (= {$CalculatedAmountTaxCreditResearchDevelopment})."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Het totaal vermeld in de rubriek '{In beginsel verrekenbaar belastingkrediet voor onderzoek en ontwikkeling}' is niet juist (= {$CalculatedAmountTaxCreditResearchDevelopment})."}  
 }
                        ,
                        Fields = new List<string>()
                    }
                    );
                }
            }

        }

        internal void Calc_f_idtc_5375_assertion_set()
        {
            List<string> scenarioIds = GetScenarioIds(new string[] { "d-incc:IncentiveCategoryDimension::d-incc:InvestmentDeductionIncompatibleTaxCreditResearchDevelopmentMember/d-invc:InvestmentCategoryDimension::d-invc:ElectricVehicleChargingStationsMember", "d-incc:IncentiveCategoryDimension::d-incc:InvestmentDeductionCompatibleTaxCreditResearchDevelopmentMember/d-invc:InvestmentCategoryDimension::d-invc:ElectricVehicleChargingStationsMember" });
            foreach (string scenarioId in scenarioIds)
            {
                string period = "D";
                // SCENARIO: "d-incc:IncentiveCategoryDimension::d-incc:InvestmentDeductionIncompatibleTaxCreditResearchDevelopmentMember/d-invc:InvestmentCategoryDimension::d-invc:ElectricVehicleChargingStationsMember"
                // SCENARIO: "d-incc:IncentiveCategoryDimension::d-incc:InvestmentDeductionCompatibleTaxCreditResearchDevelopmentMember/d-invc:InvestmentCategoryDimension::d-invc:ElectricVehicleChargingStationsMember"
                // PERIOD: "D"
                var InvestmentIncentiveOneGo_ElectricVehicleChargingStations = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:InvestmentIncentiveOneGo" });
                var AcquisitionInvestmentValue_ElectricVehicleChargingStations = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:AcquisitionInvestmentValue" });
                var FormulaTarget = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:InvestmentIncentiveOneGo" });
                if (AcquisitionInvestmentValue_ElectricVehicleChargingStations.Count > 0 || InvestmentIncentiveOneGo_ElectricVehicleChargingStations.Count > 0 || FormulaTarget.Count > 0)
                {
                    var CalculatedAmountInvestmentIncentiveOneGo_ElectricVehicleChargingStations = Math.Round(Number(AcquisitionInvestmentValue_ElectricVehicleChargingStations, "0") * Number(InvestmentDeduction_ElectricVehicleChargingStations, "0.1350") * (decimal)100, MidpointRounding.AwayFromZero) / (decimal)100;
                    //CALCULATION HERE
                    if (FormulaTarget.Count == 0)
                    {
                        FormulaTarget = CreateNewCalculationElement(scenarioId, period, "tax-inc:InvestmentIncentiveOneGo", new string[] { "d-incc:IncentiveCategoryDimension::d-incc:InvestmentDeductionIncompatibleTaxCreditResearchDevelopmentMember/d-invc:InvestmentCategoryDimension::d-invc:ElectricVehicleChargingStationsMember", "d-incc:IncentiveCategoryDimension::d-incc:InvestmentDeductionCompatibleTaxCreditResearchDevelopmentMember/d-invc:InvestmentCategoryDimension::d-invc:ElectricVehicleChargingStationsMember" }, "EUR", "INF");
                    }
                    if (FormulaTarget.Count > 1)
                    {
                        throw new Exception("More then one target for FormulaTarget in assertion f-idtc-5375");
                    }
                    FormulaTarget.First().Calculated = true;
                    FormulaTarget.First().SetNumber((Math.Round(Number(AcquisitionInvestmentValue_ElectricVehicleChargingStations, "0") * Number(InvestmentDeduction_ElectricVehicleChargingStations, "0.1350") * (decimal)100, MidpointRounding.AwayFromZero) / (decimal)100));
                    //FORMULA HERE
                    bool test = Number(InvestmentIncentiveOneGo_ElectricVehicleChargingStations, "0") == (Math.Round(Number(AcquisitionInvestmentValue_ElectricVehicleChargingStations, "0") * Number(InvestmentDeduction_ElectricVehicleChargingStations, "0.1350") * (decimal)100, MidpointRounding.AwayFromZero) / (decimal)100);
                    if (!test)
                    {
                        // MESSAGES
                        AddMessage(new BizTaxErrorDataContract
                        {
                            FileId = _fileId,
                            Id = "f-idtc-5375",
                            Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "De investeringen in '{Oplaadstations voor elektrische voertuigen}' die aan de wettelijke voorwaarden voldoen geven recht op een investeringsaftrek die gelijk is aan {$InvestmentDeduction_ElectricVehicleChargingStations*100}% van de aanschaffings- of beleggingswaarde van die investeringen (= {$CalculatedAmountInvestmentIncentiveOneGo_ElectricVehicleChargingStations})."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Les investissements en '{Stations de rechargement des véhicules électriques}' qui répondent aux conditions légales donnent droit à une déduction pour investissement égale à {$InvestmentDeduction_ElectricVehicleChargingStations*100}% de la valeur d'investissement ou de revient de ces investissements (= {$CalculatedAmountInvestmentIncentiveOneGo_ElectricVehicleChargingStations})."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Die Investitionen in '{Aufladestationen für Elektrofahrzeuge}', die die gesetzlichen Bedingungen erfüllen, berechtigen zu einem Investitionsabzug, {$InvestmentDeduction_ElectricVehicleChargingStations*100}%  des Investitions- oder Anschaffungswertes dieser Investitionen entspricht (= {$CalculatedAmountInvestmentIncentiveOneGo_ElectricVehicleChargingStations})."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "De investeringen in '{Oplaadstations voor elektrische voertuigen}' die aan de wettelijke voorwaarden voldoen geven recht op een investeringsaftrek die gelijk is aan {$InvestmentDeduction_ElectricVehicleChargingStations*100}% van de aanschaffings- of beleggingswaarde van die investeringen (= {$CalculatedAmountInvestmentIncentiveOneGo_ElectricVehicleChargingStations})."}  
 }
                            ,
                            Fields = new List<string>()
                        }
                        );
                    }
                }
            } // end scenario loop

        }

        internal void Calc_f_idtc_5376_assertion_set()
        {
            // HAS NO UNBOUND SEQUENCES
            var InvestmentDeductionOneGoCurrentTaxPeriodCompatibleTaxCreditResearchDevelopment = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:InvestmentDeductionOneGoCurrentTaxPeriodCompatibleTaxCreditResearchDevelopment" });
            var DimensionalElements = GetElementsByScenDefs(new string[] { "d-incc:IncentiveCategoryDimension::d-incc:InvestmentDeductionCompatibleTaxCreditResearchDevelopmentMember/d-invc:InvestmentCategoryDimension::d-invc:EnergySavingsMember", "d-incc:IncentiveCategoryDimension::d-incc:InvestmentDeductionCompatibleTaxCreditResearchDevelopmentMember/d-invc:InvestmentCategoryDimension::d-invc:SmokeExtractionAirTreatmentSystemsHorecaOutletsMember", "d-incc:IncentiveCategoryDimension::d-incc:InvestmentDeductionCompatibleTaxCreditResearchDevelopmentMember/d-invc:InvestmentCategoryDimension::d-invc:PromoteReutilisationRefillableBeveragePackagesReusableIndustrialProductsMember", "d-incc:IncentiveCategoryDimension::d-incc:InvestmentDeductionCompatibleTaxCreditResearchDevelopmentMember/d-invc:InvestmentCategoryDimension::d-invc:SecurityDevicesMember", "d-incc:IncentiveCategoryDimension::d-incc:InvestmentDeductionCompatibleTaxCreditResearchDevelopmentMember/d-invc:InvestmentCategoryDimension::d-invc:SeaVesselsNonBelgianOriginMember", "d-incc:IncentiveCategoryDimension::d-incc:InvestmentDeductionCompatibleTaxCreditResearchDevelopmentMember/d-invc:InvestmentCategoryDimension::d-invc:ElectricVehicleChargingStationsMember" }, new string[] { "D" }, new string[] { "tax-inc:InvestmentIncentiveOneGo" });
            var FormulaTarget = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:InvestmentDeductionOneGoCurrentTaxPeriodCompatibleTaxCreditResearchDevelopment" });
            if (InvestmentDeductionOneGoCurrentTaxPeriodCompatibleTaxCreditResearchDevelopment.Count > 0 || DimensionalElements.Count > 0 || FormulaTarget.Count > 0)
            {
                var CalculatedAmountInvestmentDeductionOneGoCurrentTaxPeriodCompatibleTaxCreditResearchDevelopment = Sum(Number(DimensionalElements, "0"));
                //CALCULATION HERE
                if (FormulaTarget.Count == 0)
                {
                    FormulaTarget = CreateNewCalculationElement(null, new string[] { "D" }, "tax-inc:InvestmentDeductionOneGoCurrentTaxPeriodCompatibleTaxCreditResearchDevelopment", new string[] { "" }, "EUR", "INF");
                }
                if (FormulaTarget.Count > 1)
                {
                    throw new Exception("More then one target for FormulaTarget in assertion f-idtc-5376");
                }
                FormulaTarget.First().Calculated = true;
                FormulaTarget.First().SetNumber(Sum(Number(DimensionalElements, "0")));
                //FORMULA HERE
                bool test = Number(InvestmentDeductionOneGoCurrentTaxPeriodCompatibleTaxCreditResearchDevelopment, "0") == Sum(Number(DimensionalElements, "0"));
                if (!test)
                {
                    // MESSAGES
                    AddMessage(new BizTaxErrorDataContract
                    {
                        FileId = _fileId,
                        Id = "f-idtc-5376",
                        Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Het totaal vermeld in de rubriek '{Eenmalige investeringsaftrek}' is niet juist (= {$CalculatedAmountInvestmentDeductionOneGoCurrentTaxPeriodCompatibleTaxCreditResearchDevelopment})."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Le total repris dans la rubrique '{Déduction pour investissement en une fois}' n'est pas correct (= {$CalculatedAmountInvestmentDeductionOneGoCurrentTaxPeriodCompatibleTaxCreditResearchDevelopment})."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Der in der Rubrik '{Einmaliger Investitionsabzug}' ist unzutreffend (= {$CalculatedAmountInvestmentDeductionOneGoCurrentTaxPeriodCompatibleTaxCreditResearchDevelopment})."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Het totaal vermeld in de rubriek '{Eenmalige investeringsaftrek}' is niet juist (= {$CalculatedAmountInvestmentDeductionOneGoCurrentTaxPeriodCompatibleTaxCreditResearchDevelopment})."}  
 }
                        ,
                        Fields = new List<string>()
                    }
                    );
                }
            }

        }

        internal void Calc_f_eap_5402_assertion_set()
        {
            List<string> scenarioIds = GetScenarioIds(new string[] { "d-empreg:EmploymentRegimeDimension::d-empreg:FullTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:WorkerMember/d-per:PeriodDimension::d-per:CalendarYear-1Member/d-tu:TimeUnitDimension::d-tu:Quarter1Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension", "d-empreg:EmploymentRegimeDimension::d-empreg:FullTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:WorkerMember/d-per:PeriodDimension::d-per:CalendarYear-2Member/d-tu:TimeUnitDimension::d-tu:Quarter1Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension", "d-empreg:EmploymentRegimeDimension::d-empreg:FullTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:WorkerMember/d-per:PeriodDimension::d-per:CalendarYear-1Member/d-tu:TimeUnitDimension::d-tu:Quarter2Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension", "d-empreg:EmploymentRegimeDimension::d-empreg:FullTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:WorkerMember/d-per:PeriodDimension::d-per:CalendarYear-2Member/d-tu:TimeUnitDimension::d-tu:Quarter2Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension", "d-empreg:EmploymentRegimeDimension::d-empreg:FullTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:WorkerMember/d-per:PeriodDimension::d-per:CalendarYear-1Member/d-tu:TimeUnitDimension::d-tu:Quarter3Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension", "d-empreg:EmploymentRegimeDimension::d-empreg:FullTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:WorkerMember/d-per:PeriodDimension::d-per:CalendarYear-2Member/d-tu:TimeUnitDimension::d-tu:Quarter3Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension", "d-empreg:EmploymentRegimeDimension::d-empreg:FullTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:WorkerMember/d-per:PeriodDimension::d-per:CalendarYear-1Member/d-tu:TimeUnitDimension::d-tu:Quarter4Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension", "d-empreg:EmploymentRegimeDimension::d-empreg:FullTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:WorkerMember/d-per:PeriodDimension::d-per:CalendarYear-2Member/d-tu:TimeUnitDimension::d-tu:Quarter4Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension", "d-empreg:EmploymentRegimeDimension::d-empreg:PartTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:WorkerMember/d-per:PeriodDimension::d-per:CalendarYear-1Member/d-tu:TimeUnitDimension::d-tu:Quarter1Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension", "d-empreg:EmploymentRegimeDimension::d-empreg:PartTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:WorkerMember/d-per:PeriodDimension::d-per:CalendarYear-2Member/d-tu:TimeUnitDimension::d-tu:Quarter1Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension", "d-empreg:EmploymentRegimeDimension::d-empreg:PartTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:WorkerMember/d-per:PeriodDimension::d-per:CalendarYear-1Member/d-tu:TimeUnitDimension::d-tu:Quarter2Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension", "d-empreg:EmploymentRegimeDimension::d-empreg:PartTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:WorkerMember/d-per:PeriodDimension::d-per:CalendarYear-2Member/d-tu:TimeUnitDimension::d-tu:Quarter2Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension", "d-empreg:EmploymentRegimeDimension::d-empreg:PartTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:WorkerMember/d-per:PeriodDimension::d-per:CalendarYear-1Member/d-tu:TimeUnitDimension::d-tu:Quarter3Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension", "d-empreg:EmploymentRegimeDimension::d-empreg:PartTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:WorkerMember/d-per:PeriodDimension::d-per:CalendarYear-2Member/d-tu:TimeUnitDimension::d-tu:Quarter3Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension", "d-empreg:EmploymentRegimeDimension::d-empreg:PartTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:WorkerMember/d-per:PeriodDimension::d-per:CalendarYear-1Member/d-tu:TimeUnitDimension::d-tu:Quarter4Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension", "d-empreg:EmploymentRegimeDimension::d-empreg:PartTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:WorkerMember/d-per:PeriodDimension::d-per:CalendarYear-2Member/d-tu:TimeUnitDimension::d-tu:Quarter4Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension" });
            foreach (string scenarioId in scenarioIds)
            {
                string period = "D";
                // SCENARIO: "d-empreg:EmploymentRegimeDimension::d-empreg:FullTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:WorkerMember/d-per:PeriodDimension::d-per:CalendarYear-1Member/d-tu:TimeUnitDimension::d-tu:Quarter1Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension"
                // SCENARIO: "d-empreg:EmploymentRegimeDimension::d-empreg:FullTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:WorkerMember/d-per:PeriodDimension::d-per:CalendarYear-2Member/d-tu:TimeUnitDimension::d-tu:Quarter1Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension"
                // SCENARIO: "d-empreg:EmploymentRegimeDimension::d-empreg:FullTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:WorkerMember/d-per:PeriodDimension::d-per:CalendarYear-1Member/d-tu:TimeUnitDimension::d-tu:Quarter2Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension"
                // SCENARIO: "d-empreg:EmploymentRegimeDimension::d-empreg:FullTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:WorkerMember/d-per:PeriodDimension::d-per:CalendarYear-2Member/d-tu:TimeUnitDimension::d-tu:Quarter2Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension"
                // SCENARIO: "d-empreg:EmploymentRegimeDimension::d-empreg:FullTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:WorkerMember/d-per:PeriodDimension::d-per:CalendarYear-1Member/d-tu:TimeUnitDimension::d-tu:Quarter3Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension"
                // SCENARIO: "d-empreg:EmploymentRegimeDimension::d-empreg:FullTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:WorkerMember/d-per:PeriodDimension::d-per:CalendarYear-2Member/d-tu:TimeUnitDimension::d-tu:Quarter3Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension"
                // SCENARIO: "d-empreg:EmploymentRegimeDimension::d-empreg:FullTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:WorkerMember/d-per:PeriodDimension::d-per:CalendarYear-1Member/d-tu:TimeUnitDimension::d-tu:Quarter4Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension"
                // SCENARIO: "d-empreg:EmploymentRegimeDimension::d-empreg:FullTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:WorkerMember/d-per:PeriodDimension::d-per:CalendarYear-2Member/d-tu:TimeUnitDimension::d-tu:Quarter4Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension"
                // SCENARIO: "d-empreg:EmploymentRegimeDimension::d-empreg:PartTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:WorkerMember/d-per:PeriodDimension::d-per:CalendarYear-1Member/d-tu:TimeUnitDimension::d-tu:Quarter1Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension"
                // SCENARIO: "d-empreg:EmploymentRegimeDimension::d-empreg:PartTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:WorkerMember/d-per:PeriodDimension::d-per:CalendarYear-2Member/d-tu:TimeUnitDimension::d-tu:Quarter1Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension"
                // SCENARIO: "d-empreg:EmploymentRegimeDimension::d-empreg:PartTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:WorkerMember/d-per:PeriodDimension::d-per:CalendarYear-1Member/d-tu:TimeUnitDimension::d-tu:Quarter2Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension"
                // SCENARIO: "d-empreg:EmploymentRegimeDimension::d-empreg:PartTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:WorkerMember/d-per:PeriodDimension::d-per:CalendarYear-2Member/d-tu:TimeUnitDimension::d-tu:Quarter2Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension"
                // SCENARIO: "d-empreg:EmploymentRegimeDimension::d-empreg:PartTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:WorkerMember/d-per:PeriodDimension::d-per:CalendarYear-1Member/d-tu:TimeUnitDimension::d-tu:Quarter3Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension"
                // SCENARIO: "d-empreg:EmploymentRegimeDimension::d-empreg:PartTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:WorkerMember/d-per:PeriodDimension::d-per:CalendarYear-2Member/d-tu:TimeUnitDimension::d-tu:Quarter3Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension"
                // SCENARIO: "d-empreg:EmploymentRegimeDimension::d-empreg:PartTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:WorkerMember/d-per:PeriodDimension::d-per:CalendarYear-1Member/d-tu:TimeUnitDimension::d-tu:Quarter4Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension"
                // SCENARIO: "d-empreg:EmploymentRegimeDimension::d-empreg:PartTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:WorkerMember/d-per:PeriodDimension::d-per:CalendarYear-2Member/d-tu:TimeUnitDimension::d-tu:Quarter4Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension"
                // PERIOD: "D"
                var WorkingDaysAllPersonnel = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:WorkingDaysAllPersonnel" });
                var WorkingDaysPaid = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:WorkingDaysPaid" });
                var HolidaysWorkers = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:HolidaysWorkers" });
                var CorrectionsWorkingDays = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:CorrectionsWorkingDays" });
                var FormulaTarget = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:WorkingDaysAllPersonnel" });
                if (HolidaysWorkers.Count > 0 || WorkingDaysAllPersonnel.Count > 0 || WorkingDaysPaid.Count > 0 || CorrectionsWorkingDays.Count > 0 || FormulaTarget.Count > 0)
                {
                    var CalculatedAmountWorkingDaysAllPersonnel = Max(Sum(Number(WorkingDaysPaid, "0"), Number(HolidaysWorkers, "0"), -Number(CorrectionsWorkingDays, "0")), (decimal)0);
                    //CALCULATION HERE
                    if (FormulaTarget.Count == 0)
                    {
                        FormulaTarget = CreateNewCalculationElement(scenarioId, period, "tax-inc:WorkingDaysAllPersonnel", new string[] { "d-empreg:EmploymentRegimeDimension::d-empreg:FullTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:WorkerMember/d-per:PeriodDimension::d-per:CalendarYear-1Member/d-tu:TimeUnitDimension::d-tu:Quarter1Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension", "d-empreg:EmploymentRegimeDimension::d-empreg:FullTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:WorkerMember/d-per:PeriodDimension::d-per:CalendarYear-2Member/d-tu:TimeUnitDimension::d-tu:Quarter1Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension", "d-empreg:EmploymentRegimeDimension::d-empreg:FullTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:WorkerMember/d-per:PeriodDimension::d-per:CalendarYear-1Member/d-tu:TimeUnitDimension::d-tu:Quarter2Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension", "d-empreg:EmploymentRegimeDimension::d-empreg:FullTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:WorkerMember/d-per:PeriodDimension::d-per:CalendarYear-2Member/d-tu:TimeUnitDimension::d-tu:Quarter2Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension", "d-empreg:EmploymentRegimeDimension::d-empreg:FullTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:WorkerMember/d-per:PeriodDimension::d-per:CalendarYear-1Member/d-tu:TimeUnitDimension::d-tu:Quarter3Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension", "d-empreg:EmploymentRegimeDimension::d-empreg:FullTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:WorkerMember/d-per:PeriodDimension::d-per:CalendarYear-2Member/d-tu:TimeUnitDimension::d-tu:Quarter3Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension", "d-empreg:EmploymentRegimeDimension::d-empreg:FullTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:WorkerMember/d-per:PeriodDimension::d-per:CalendarYear-1Member/d-tu:TimeUnitDimension::d-tu:Quarter4Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension", "d-empreg:EmploymentRegimeDimension::d-empreg:FullTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:WorkerMember/d-per:PeriodDimension::d-per:CalendarYear-2Member/d-tu:TimeUnitDimension::d-tu:Quarter4Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension", "d-empreg:EmploymentRegimeDimension::d-empreg:PartTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:WorkerMember/d-per:PeriodDimension::d-per:CalendarYear-1Member/d-tu:TimeUnitDimension::d-tu:Quarter1Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension", "d-empreg:EmploymentRegimeDimension::d-empreg:PartTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:WorkerMember/d-per:PeriodDimension::d-per:CalendarYear-2Member/d-tu:TimeUnitDimension::d-tu:Quarter1Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension", "d-empreg:EmploymentRegimeDimension::d-empreg:PartTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:WorkerMember/d-per:PeriodDimension::d-per:CalendarYear-1Member/d-tu:TimeUnitDimension::d-tu:Quarter2Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension", "d-empreg:EmploymentRegimeDimension::d-empreg:PartTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:WorkerMember/d-per:PeriodDimension::d-per:CalendarYear-2Member/d-tu:TimeUnitDimension::d-tu:Quarter2Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension", "d-empreg:EmploymentRegimeDimension::d-empreg:PartTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:WorkerMember/d-per:PeriodDimension::d-per:CalendarYear-1Member/d-tu:TimeUnitDimension::d-tu:Quarter3Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension", "d-empreg:EmploymentRegimeDimension::d-empreg:PartTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:WorkerMember/d-per:PeriodDimension::d-per:CalendarYear-2Member/d-tu:TimeUnitDimension::d-tu:Quarter3Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension", "d-empreg:EmploymentRegimeDimension::d-empreg:PartTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:WorkerMember/d-per:PeriodDimension::d-per:CalendarYear-1Member/d-tu:TimeUnitDimension::d-tu:Quarter4Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension", "d-empreg:EmploymentRegimeDimension::d-empreg:PartTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:WorkerMember/d-per:PeriodDimension::d-per:CalendarYear-2Member/d-tu:TimeUnitDimension::d-tu:Quarter4Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension" }, "U-Pure", "INF");
                    }
                    if (FormulaTarget.Count > 1)
                    {
                        throw new Exception("More then one target for FormulaTarget in assertion f-eap-5402");
                    }
                    FormulaTarget.First().Calculated = true;
                    FormulaTarget.First().SetNumber(Max(Sum(Number(WorkingDaysPaid, "0"), Number(HolidaysWorkers, "0"), -Number(CorrectionsWorkingDays, "0")), (decimal)0));
                    //FORMULA HERE
                    bool test = Number(WorkingDaysAllPersonnel, "0") == Max(Sum(Number(WorkingDaysPaid, "0"), Number(HolidaysWorkers, "0"), -Number(CorrectionsWorkingDays, "0")), (decimal)0);
                    if (!test)
                    {
                        // MESSAGES
                        AddMessage(new BizTaxErrorDataContract
                        {
                            FileId = _fileId,
                            Id = "f-eap-5402",
                            Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Het totaal vermeld in de rubriek '{Arbeidsdagen, alle personeelsleden}' {$WorkingDaysAllPersonnel} is niet juist (= {$CalculatedAmountWorkingDaysAllPersonnel})."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Le total repris dans la rubrique '{Jours de travail, tous les travailleurs}' {$WorkingDaysAllPersonnel} n'est pas correct (= {$CalculatedAmountWorkingDaysAllPersonnel})."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Der in der Rubrik '{Arbeitstage, alle Personalmitglieder}' {$WorkingDaysAllPersonnel} angegebene Gesamtbetrag ist unzutreffend (= {$CalculatedAmountWorkingDaysAllPersonnel})."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Het totaal vermeld in de rubriek '{Arbeidsdagen, alle personeelsleden}' {$WorkingDaysAllPersonnel} is niet juist (= {$CalculatedAmountWorkingDaysAllPersonnel})."}  
 }
                            ,
                            Fields = new List<string>()
                        }
                        );
                    }
                }
            } // end scenario loop

        }

        internal void Calc_f_eap_5403_assertion_set()
        {
            List<string> scenarioIds = GetScenarioIds(new string[] { "d-empreg:EmploymentRegimeDimension::d-empreg:FullTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:EmployeeMember/d-per:PeriodDimension::d-per:CalendarYear-1Member/d-tu:TimeUnitDimension::d-tu:Quarter1Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension", "d-empreg:EmploymentRegimeDimension::d-empreg:FullTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:EmployeeMember/d-per:PeriodDimension::d-per:CalendarYear-2Member/d-tu:TimeUnitDimension::d-tu:Quarter1Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension", "d-empreg:EmploymentRegimeDimension::d-empreg:FullTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:EmployeeMember/d-per:PeriodDimension::d-per:CalendarYear-1Member/d-tu:TimeUnitDimension::d-tu:Quarter2Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension", "d-empreg:EmploymentRegimeDimension::d-empreg:FullTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:EmployeeMember/d-per:PeriodDimension::d-per:CalendarYear-2Member/d-tu:TimeUnitDimension::d-tu:Quarter2Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension", "d-empreg:EmploymentRegimeDimension::d-empreg:FullTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:EmployeeMember/d-per:PeriodDimension::d-per:CalendarYear-1Member/d-tu:TimeUnitDimension::d-tu:Quarter3Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension", "d-empreg:EmploymentRegimeDimension::d-empreg:FullTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:EmployeeMember/d-per:PeriodDimension::d-per:CalendarYear-2Member/d-tu:TimeUnitDimension::d-tu:Quarter3Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension", "d-empreg:EmploymentRegimeDimension::d-empreg:FullTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:EmployeeMember/d-per:PeriodDimension::d-per:CalendarYear-1Member/d-tu:TimeUnitDimension::d-tu:Quarter4Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension", "d-empreg:EmploymentRegimeDimension::d-empreg:FullTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:EmployeeMember/d-per:PeriodDimension::d-per:CalendarYear-2Member/d-tu:TimeUnitDimension::d-tu:Quarter4Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension", "d-empreg:EmploymentRegimeDimension::d-empreg:PartTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:EmployeeMember/d-per:PeriodDimension::d-per:CalendarYear-1Member/d-tu:TimeUnitDimension::d-tu:Quarter1Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension", "d-empreg:EmploymentRegimeDimension::d-empreg:PartTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:EmployeeMember/d-per:PeriodDimension::d-per:CalendarYear-2Member/d-tu:TimeUnitDimension::d-tu:Quarter1Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension", "d-empreg:EmploymentRegimeDimension::d-empreg:PartTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:EmployeeMember/d-per:PeriodDimension::d-per:CalendarYear-1Member/d-tu:TimeUnitDimension::d-tu:Quarter2Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension", "d-empreg:EmploymentRegimeDimension::d-empreg:PartTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:EmployeeMember/d-per:PeriodDimension::d-per:CalendarYear-2Member/d-tu:TimeUnitDimension::d-tu:Quarter2Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension", "d-empreg:EmploymentRegimeDimension::d-empreg:PartTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:EmployeeMember/d-per:PeriodDimension::d-per:CalendarYear-1Member/d-tu:TimeUnitDimension::d-tu:Quarter3Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension", "d-empreg:EmploymentRegimeDimension::d-empreg:PartTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:EmployeeMember/d-per:PeriodDimension::d-per:CalendarYear-2Member/d-tu:TimeUnitDimension::d-tu:Quarter3Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension", "d-empreg:EmploymentRegimeDimension::d-empreg:PartTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:EmployeeMember/d-per:PeriodDimension::d-per:CalendarYear-1Member/d-tu:TimeUnitDimension::d-tu:Quarter4Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension", "d-empreg:EmploymentRegimeDimension::d-empreg:PartTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:EmployeeMember/d-per:PeriodDimension::d-per:CalendarYear-2Member/d-tu:TimeUnitDimension::d-tu:Quarter4Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension" });
            foreach (string scenarioId in scenarioIds)
            {
                string period = "D";
                // SCENARIO: "d-empreg:EmploymentRegimeDimension::d-empreg:FullTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:EmployeeMember/d-per:PeriodDimension::d-per:CalendarYear-1Member/d-tu:TimeUnitDimension::d-tu:Quarter1Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension"
                // SCENARIO: "d-empreg:EmploymentRegimeDimension::d-empreg:FullTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:EmployeeMember/d-per:PeriodDimension::d-per:CalendarYear-2Member/d-tu:TimeUnitDimension::d-tu:Quarter1Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension"
                // SCENARIO: "d-empreg:EmploymentRegimeDimension::d-empreg:FullTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:EmployeeMember/d-per:PeriodDimension::d-per:CalendarYear-1Member/d-tu:TimeUnitDimension::d-tu:Quarter2Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension"
                // SCENARIO: "d-empreg:EmploymentRegimeDimension::d-empreg:FullTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:EmployeeMember/d-per:PeriodDimension::d-per:CalendarYear-2Member/d-tu:TimeUnitDimension::d-tu:Quarter2Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension"
                // SCENARIO: "d-empreg:EmploymentRegimeDimension::d-empreg:FullTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:EmployeeMember/d-per:PeriodDimension::d-per:CalendarYear-1Member/d-tu:TimeUnitDimension::d-tu:Quarter3Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension"
                // SCENARIO: "d-empreg:EmploymentRegimeDimension::d-empreg:FullTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:EmployeeMember/d-per:PeriodDimension::d-per:CalendarYear-2Member/d-tu:TimeUnitDimension::d-tu:Quarter3Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension"
                // SCENARIO: "d-empreg:EmploymentRegimeDimension::d-empreg:FullTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:EmployeeMember/d-per:PeriodDimension::d-per:CalendarYear-1Member/d-tu:TimeUnitDimension::d-tu:Quarter4Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension"
                // SCENARIO: "d-empreg:EmploymentRegimeDimension::d-empreg:FullTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:EmployeeMember/d-per:PeriodDimension::d-per:CalendarYear-2Member/d-tu:TimeUnitDimension::d-tu:Quarter4Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension"
                // SCENARIO: "d-empreg:EmploymentRegimeDimension::d-empreg:PartTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:EmployeeMember/d-per:PeriodDimension::d-per:CalendarYear-1Member/d-tu:TimeUnitDimension::d-tu:Quarter1Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension"
                // SCENARIO: "d-empreg:EmploymentRegimeDimension::d-empreg:PartTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:EmployeeMember/d-per:PeriodDimension::d-per:CalendarYear-2Member/d-tu:TimeUnitDimension::d-tu:Quarter1Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension"
                // SCENARIO: "d-empreg:EmploymentRegimeDimension::d-empreg:PartTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:EmployeeMember/d-per:PeriodDimension::d-per:CalendarYear-1Member/d-tu:TimeUnitDimension::d-tu:Quarter2Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension"
                // SCENARIO: "d-empreg:EmploymentRegimeDimension::d-empreg:PartTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:EmployeeMember/d-per:PeriodDimension::d-per:CalendarYear-2Member/d-tu:TimeUnitDimension::d-tu:Quarter2Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension"
                // SCENARIO: "d-empreg:EmploymentRegimeDimension::d-empreg:PartTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:EmployeeMember/d-per:PeriodDimension::d-per:CalendarYear-1Member/d-tu:TimeUnitDimension::d-tu:Quarter3Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension"
                // SCENARIO: "d-empreg:EmploymentRegimeDimension::d-empreg:PartTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:EmployeeMember/d-per:PeriodDimension::d-per:CalendarYear-2Member/d-tu:TimeUnitDimension::d-tu:Quarter3Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension"
                // SCENARIO: "d-empreg:EmploymentRegimeDimension::d-empreg:PartTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:EmployeeMember/d-per:PeriodDimension::d-per:CalendarYear-1Member/d-tu:TimeUnitDimension::d-tu:Quarter4Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension"
                // SCENARIO: "d-empreg:EmploymentRegimeDimension::d-empreg:PartTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:EmployeeMember/d-per:PeriodDimension::d-per:CalendarYear-2Member/d-tu:TimeUnitDimension::d-tu:Quarter4Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension"
                // PERIOD: "D"
                var WorkingDaysAllPersonnel = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:WorkingDaysAllPersonnel" });
                var WorkingDaysPaid = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:WorkingDaysPaid" });
                var CorrectionsWorkingDays = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:CorrectionsWorkingDays" });
                var FormulaTarget = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:WorkingDaysAllPersonnel" });
                if (WorkingDaysAllPersonnel.Count > 0 || WorkingDaysPaid.Count > 0 || CorrectionsWorkingDays.Count > 0 || FormulaTarget.Count > 0)
                {
                    var CalculatedAmountWorkingDaysAllPersonnel = Max(Sum(Number(WorkingDaysPaid, "0"), -Number(CorrectionsWorkingDays, "0")), (decimal)0);
                    //CALCULATION HERE
                    if (FormulaTarget.Count == 0)
                    {
                        FormulaTarget = CreateNewCalculationElement(scenarioId, period, "tax-inc:WorkingDaysAllPersonnel", new string[] { "d-empreg:EmploymentRegimeDimension::d-empreg:FullTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:EmployeeMember/d-per:PeriodDimension::d-per:CalendarYear-1Member/d-tu:TimeUnitDimension::d-tu:Quarter1Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension", "d-empreg:EmploymentRegimeDimension::d-empreg:FullTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:EmployeeMember/d-per:PeriodDimension::d-per:CalendarYear-2Member/d-tu:TimeUnitDimension::d-tu:Quarter1Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension", "d-empreg:EmploymentRegimeDimension::d-empreg:FullTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:EmployeeMember/d-per:PeriodDimension::d-per:CalendarYear-1Member/d-tu:TimeUnitDimension::d-tu:Quarter2Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension", "d-empreg:EmploymentRegimeDimension::d-empreg:FullTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:EmployeeMember/d-per:PeriodDimension::d-per:CalendarYear-2Member/d-tu:TimeUnitDimension::d-tu:Quarter2Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension", "d-empreg:EmploymentRegimeDimension::d-empreg:FullTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:EmployeeMember/d-per:PeriodDimension::d-per:CalendarYear-1Member/d-tu:TimeUnitDimension::d-tu:Quarter3Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension", "d-empreg:EmploymentRegimeDimension::d-empreg:FullTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:EmployeeMember/d-per:PeriodDimension::d-per:CalendarYear-2Member/d-tu:TimeUnitDimension::d-tu:Quarter3Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension", "d-empreg:EmploymentRegimeDimension::d-empreg:FullTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:EmployeeMember/d-per:PeriodDimension::d-per:CalendarYear-1Member/d-tu:TimeUnitDimension::d-tu:Quarter4Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension", "d-empreg:EmploymentRegimeDimension::d-empreg:FullTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:EmployeeMember/d-per:PeriodDimension::d-per:CalendarYear-2Member/d-tu:TimeUnitDimension::d-tu:Quarter4Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension", "d-empreg:EmploymentRegimeDimension::d-empreg:PartTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:EmployeeMember/d-per:PeriodDimension::d-per:CalendarYear-1Member/d-tu:TimeUnitDimension::d-tu:Quarter1Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension", "d-empreg:EmploymentRegimeDimension::d-empreg:PartTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:EmployeeMember/d-per:PeriodDimension::d-per:CalendarYear-2Member/d-tu:TimeUnitDimension::d-tu:Quarter1Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension", "d-empreg:EmploymentRegimeDimension::d-empreg:PartTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:EmployeeMember/d-per:PeriodDimension::d-per:CalendarYear-1Member/d-tu:TimeUnitDimension::d-tu:Quarter2Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension", "d-empreg:EmploymentRegimeDimension::d-empreg:PartTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:EmployeeMember/d-per:PeriodDimension::d-per:CalendarYear-2Member/d-tu:TimeUnitDimension::d-tu:Quarter2Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension", "d-empreg:EmploymentRegimeDimension::d-empreg:PartTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:EmployeeMember/d-per:PeriodDimension::d-per:CalendarYear-1Member/d-tu:TimeUnitDimension::d-tu:Quarter3Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension", "d-empreg:EmploymentRegimeDimension::d-empreg:PartTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:EmployeeMember/d-per:PeriodDimension::d-per:CalendarYear-2Member/d-tu:TimeUnitDimension::d-tu:Quarter3Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension", "d-empreg:EmploymentRegimeDimension::d-empreg:PartTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:EmployeeMember/d-per:PeriodDimension::d-per:CalendarYear-1Member/d-tu:TimeUnitDimension::d-tu:Quarter4Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension", "d-empreg:EmploymentRegimeDimension::d-empreg:PartTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:EmployeeMember/d-per:PeriodDimension::d-per:CalendarYear-2Member/d-tu:TimeUnitDimension::d-tu:Quarter4Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension" }, "U-Pure", "INF");
                    }
                    if (FormulaTarget.Count > 1)
                    {
                        throw new Exception("More then one target for FormulaTarget in assertion f-eap-5403");
                    }
                    FormulaTarget.First().Calculated = true;
                    FormulaTarget.First().SetNumber(Max(Sum(Number(WorkingDaysPaid, "0"), -Number(CorrectionsWorkingDays, "0")), (decimal)0));
                    //FORMULA HERE
                    bool test = Number(WorkingDaysAllPersonnel, "0") == Max(Sum(Number(WorkingDaysPaid, "0"), -Number(CorrectionsWorkingDays, "0")), (decimal)0);
                    if (!test)
                    {
                        // MESSAGES
                        AddMessage(new BizTaxErrorDataContract
                        {
                            FileId = _fileId,
                            Id = "f-eap-5403",
                            Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Het totaal vermeld in de rubriek '{Arbeidsdagen, alle personeelsleden}' {$WorkingDaysAllPersonnel} is niet juist (= {$CalculatedAmountWorkingDaysAllPersonnel})."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Le total repris dans la rubrique '{Jours de travail, tous les travailleurs}' {$WorkingDaysAllPersonnel} n'est pas correct (= {$CalculatedAmountWorkingDaysAllPersonnel})."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Der in der Rubrik '{Arbeitstage, alle Personalmitglieder}' {$WorkingDaysAllPersonnel} angegebene Gesamtbetrag ist unzutreffend (= {$CalculatedAmountWorkingDaysAllPersonnel})."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Het totaal vermeld in de rubriek '{Arbeidsdagen, alle personeelsleden}' {$WorkingDaysAllPersonnel} is niet juist (= {$CalculatedAmountWorkingDaysAllPersonnel})."}  
 }
                            ,
                            Fields = new List<string>()
                        }
                        );
                    }
                }
            } // end scenario loop

        }

        internal void Calc_f_eap_5405_assertion_set()
        {
            List<string> scenarioIds = GetScenarioIds(new string[] { "d-empreg:EmploymentRegimeDimension::d-empreg:FullTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:EmployeeMember/d-per:PeriodDimension::d-per:CalendarYear-1Member/d-tu:TimeUnitDimension::d-tu:Quarter1Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension", "d-empreg:EmploymentRegimeDimension::d-empreg:FullTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:EmployeeMember/d-per:PeriodDimension::d-per:CalendarYear-2Member/d-tu:TimeUnitDimension::d-tu:Quarter1Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension", "d-empreg:EmploymentRegimeDimension::d-empreg:FullTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:EmployeeMember/d-per:PeriodDimension::d-per:CalendarYear-1Member/d-tu:TimeUnitDimension::d-tu:Quarter2Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension", "d-empreg:EmploymentRegimeDimension::d-empreg:FullTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:EmployeeMember/d-per:PeriodDimension::d-per:CalendarYear-2Member/d-tu:TimeUnitDimension::d-tu:Quarter2Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension", "d-empreg:EmploymentRegimeDimension::d-empreg:FullTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:EmployeeMember/d-per:PeriodDimension::d-per:CalendarYear-1Member/d-tu:TimeUnitDimension::d-tu:Quarter3Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension", "d-empreg:EmploymentRegimeDimension::d-empreg:FullTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:EmployeeMember/d-per:PeriodDimension::d-per:CalendarYear-2Member/d-tu:TimeUnitDimension::d-tu:Quarter3Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension", "d-empreg:EmploymentRegimeDimension::d-empreg:FullTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:EmployeeMember/d-per:PeriodDimension::d-per:CalendarYear-1Member/d-tu:TimeUnitDimension::d-tu:Quarter4Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension", "d-empreg:EmploymentRegimeDimension::d-empreg:FullTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:EmployeeMember/d-per:PeriodDimension::d-per:CalendarYear-2Member/d-tu:TimeUnitDimension::d-tu:Quarter4Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension" });
            foreach (string scenarioId in scenarioIds)
            {
                string period = "D";
                // SCENARIO: "d-empreg:EmploymentRegimeDimension::d-empreg:FullTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:EmployeeMember/d-per:PeriodDimension::d-per:CalendarYear-1Member/d-tu:TimeUnitDimension::d-tu:Quarter1Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension"
                // SCENARIO: "d-empreg:EmploymentRegimeDimension::d-empreg:FullTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:EmployeeMember/d-per:PeriodDimension::d-per:CalendarYear-2Member/d-tu:TimeUnitDimension::d-tu:Quarter1Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension"
                // SCENARIO: "d-empreg:EmploymentRegimeDimension::d-empreg:FullTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:EmployeeMember/d-per:PeriodDimension::d-per:CalendarYear-1Member/d-tu:TimeUnitDimension::d-tu:Quarter2Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension"
                // SCENARIO: "d-empreg:EmploymentRegimeDimension::d-empreg:FullTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:EmployeeMember/d-per:PeriodDimension::d-per:CalendarYear-2Member/d-tu:TimeUnitDimension::d-tu:Quarter2Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension"
                // SCENARIO: "d-empreg:EmploymentRegimeDimension::d-empreg:FullTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:EmployeeMember/d-per:PeriodDimension::d-per:CalendarYear-1Member/d-tu:TimeUnitDimension::d-tu:Quarter3Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension"
                // SCENARIO: "d-empreg:EmploymentRegimeDimension::d-empreg:FullTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:EmployeeMember/d-per:PeriodDimension::d-per:CalendarYear-2Member/d-tu:TimeUnitDimension::d-tu:Quarter3Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension"
                // SCENARIO: "d-empreg:EmploymentRegimeDimension::d-empreg:FullTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:EmployeeMember/d-per:PeriodDimension::d-per:CalendarYear-1Member/d-tu:TimeUnitDimension::d-tu:Quarter4Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension"
                // SCENARIO: "d-empreg:EmploymentRegimeDimension::d-empreg:FullTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:EmployeeMember/d-per:PeriodDimension::d-per:CalendarYear-2Member/d-tu:TimeUnitDimension::d-tu:Quarter4Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension"
                // PERIOD: "D"
                var AverageDayPay = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:AverageDayPay" });
                var GrossIncome = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:GrossIncome" });
                var WorkingDaysAllPersonnel = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:WorkingDaysAllPersonnel" });
                if (WorkingDaysAllPersonnel.Count > 0 || AverageDayPay.Count > 0 || GrossIncome.Count > 0)
                {
                    var CalculatedAmountAverageDayPay = Math.Round(Number(GrossIncome, "0") / Number(WorkingDaysAllPersonnel, "1") * (decimal)100, MidpointRounding.AwayFromZero) / (decimal)100;
                    //CALCULATION HERE
                    if ((Number(WorkingDaysAllPersonnel, "1") > (decimal)1) || (Number(AverageDayPay, "0") > (decimal)0) || (Number(GrossIncome, "0") > (decimal)0))
                    {
                        //FORMULA HERE
                        bool test = Number(AverageDayPay, "0") == (Math.Round(Number(GrossIncome, "0") / Number(WorkingDaysAllPersonnel, "1") * (decimal)100, MidpointRounding.AwayFromZero) / (decimal)100);
                        if (!test)
                        {
                            // MESSAGES
                            AddMessage(new BizTaxErrorDataContract
                            {
                                FileId = _fileId,
                                Id = "f-eap-5405",
                                Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Het totaal vermeld in de rubriek '{Gemiddeld dagloon}' {$AverageDayPay} 	is niet gelijk aan de breuk van 	'{Brutoloon}' en 	'{Arbeidsdagen, alle personeelsleden}' (= {$CalculatedAmountAverageDayPay})."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Le total repris dans la rubrique '{Rémunération journalière moyenne}' {$AverageDayPay} 	n'est pas égal à la fraction de 	'{Rémunération brute}' et 	'{Jours de travail, tous les travailleurs}' (= {$CalculatedAmountAverageDayPay})."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Der in der Rubrik '{Durchschnittlicher Tageslohn}' {$AverageDayPay} angegebene Betrag 	entspricht nicht der Bruchzahl der Rubrik 	'{Bruttolohn}' und 	'{Arbeitstage, alle Personalmitglieder}' (= {$CalculatedAmountAverageDayPay})."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Het totaal vermeld in de rubriek '{Gemiddeld dagloon}' {$AverageDayPay} 	is niet gelijk aan de breuk van 	'{Brutoloon}' en 	'{Arbeidsdagen, alle personeelsleden}' (= {$CalculatedAmountAverageDayPay})."}  
 }
                                ,
                                Fields = new List<string>()
                            }
                            );
                        }
                    }
                }
            } // end scenario loop

        }

        internal void Calc_f_eap_5407_assertion_set()
        {
            List<string> scenarioIds = GetScenarioIds(new string[] { "d-empreg:EmploymentRegimeDimension::d-empreg:PartTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:EmployeeMember/d-per:PeriodDimension::d-per:CalendarYear-1Member/d-tu:TimeUnitDimension::d-tu:Quarter1Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension", "d-empreg:EmploymentRegimeDimension::d-empreg:PartTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:EmployeeMember/d-per:PeriodDimension::d-per:CalendarYear-2Member/d-tu:TimeUnitDimension::d-tu:Quarter1Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension", "d-empreg:EmploymentRegimeDimension::d-empreg:PartTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:EmployeeMember/d-per:PeriodDimension::d-per:CalendarYear-1Member/d-tu:TimeUnitDimension::d-tu:Quarter2Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension", "d-empreg:EmploymentRegimeDimension::d-empreg:PartTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:EmployeeMember/d-per:PeriodDimension::d-per:CalendarYear-2Member/d-tu:TimeUnitDimension::d-tu:Quarter2Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension", "d-empreg:EmploymentRegimeDimension::d-empreg:PartTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:EmployeeMember/d-per:PeriodDimension::d-per:CalendarYear-1Member/d-tu:TimeUnitDimension::d-tu:Quarter3Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension", "d-empreg:EmploymentRegimeDimension::d-empreg:PartTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:EmployeeMember/d-per:PeriodDimension::d-per:CalendarYear-2Member/d-tu:TimeUnitDimension::d-tu:Quarter3Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension", "d-empreg:EmploymentRegimeDimension::d-empreg:PartTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:EmployeeMember/d-per:PeriodDimension::d-per:CalendarYear-1Member/d-tu:TimeUnitDimension::d-tu:Quarter4Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension", "d-empreg:EmploymentRegimeDimension::d-empreg:PartTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:EmployeeMember/d-per:PeriodDimension::d-per:CalendarYear-2Member/d-tu:TimeUnitDimension::d-tu:Quarter4Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension", "d-empreg:EmploymentRegimeDimension::d-empreg:PartTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:WorkerMember/d-per:PeriodDimension::d-per:CalendarYear-1Member/d-tu:TimeUnitDimension::d-tu:Quarter1Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension", "d-empreg:EmploymentRegimeDimension::d-empreg:PartTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:WorkerMember/d-per:PeriodDimension::d-per:CalendarYear-2Member/d-tu:TimeUnitDimension::d-tu:Quarter1Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension", "d-empreg:EmploymentRegimeDimension::d-empreg:PartTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:WorkerMember/d-per:PeriodDimension::d-per:CalendarYear-1Member/d-tu:TimeUnitDimension::d-tu:Quarter2Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension", "d-empreg:EmploymentRegimeDimension::d-empreg:PartTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:WorkerMember/d-per:PeriodDimension::d-per:CalendarYear-2Member/d-tu:TimeUnitDimension::d-tu:Quarter2Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension", "d-empreg:EmploymentRegimeDimension::d-empreg:PartTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:WorkerMember/d-per:PeriodDimension::d-per:CalendarYear-1Member/d-tu:TimeUnitDimension::d-tu:Quarter3Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension", "d-empreg:EmploymentRegimeDimension::d-empreg:PartTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:WorkerMember/d-per:PeriodDimension::d-per:CalendarYear-2Member/d-tu:TimeUnitDimension::d-tu:Quarter3Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension", "d-empreg:EmploymentRegimeDimension::d-empreg:PartTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:WorkerMember/d-per:PeriodDimension::d-per:CalendarYear-1Member/d-tu:TimeUnitDimension::d-tu:Quarter4Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension", "d-empreg:EmploymentRegimeDimension::d-empreg:PartTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:WorkerMember/d-per:PeriodDimension::d-per:CalendarYear-2Member/d-tu:TimeUnitDimension::d-tu:Quarter4Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension" });
            foreach (string scenarioId in scenarioIds)
            {
                string period = "D";
                // SCENARIO: "d-empreg:EmploymentRegimeDimension::d-empreg:PartTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:EmployeeMember/d-per:PeriodDimension::d-per:CalendarYear-1Member/d-tu:TimeUnitDimension::d-tu:Quarter1Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension"
                // SCENARIO: "d-empreg:EmploymentRegimeDimension::d-empreg:PartTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:EmployeeMember/d-per:PeriodDimension::d-per:CalendarYear-2Member/d-tu:TimeUnitDimension::d-tu:Quarter1Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension"
                // SCENARIO: "d-empreg:EmploymentRegimeDimension::d-empreg:PartTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:EmployeeMember/d-per:PeriodDimension::d-per:CalendarYear-1Member/d-tu:TimeUnitDimension::d-tu:Quarter2Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension"
                // SCENARIO: "d-empreg:EmploymentRegimeDimension::d-empreg:PartTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:EmployeeMember/d-per:PeriodDimension::d-per:CalendarYear-2Member/d-tu:TimeUnitDimension::d-tu:Quarter2Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension"
                // SCENARIO: "d-empreg:EmploymentRegimeDimension::d-empreg:PartTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:EmployeeMember/d-per:PeriodDimension::d-per:CalendarYear-1Member/d-tu:TimeUnitDimension::d-tu:Quarter3Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension"
                // SCENARIO: "d-empreg:EmploymentRegimeDimension::d-empreg:PartTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:EmployeeMember/d-per:PeriodDimension::d-per:CalendarYear-2Member/d-tu:TimeUnitDimension::d-tu:Quarter3Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension"
                // SCENARIO: "d-empreg:EmploymentRegimeDimension::d-empreg:PartTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:EmployeeMember/d-per:PeriodDimension::d-per:CalendarYear-1Member/d-tu:TimeUnitDimension::d-tu:Quarter4Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension"
                // SCENARIO: "d-empreg:EmploymentRegimeDimension::d-empreg:PartTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:EmployeeMember/d-per:PeriodDimension::d-per:CalendarYear-2Member/d-tu:TimeUnitDimension::d-tu:Quarter4Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension"
                // SCENARIO: "d-empreg:EmploymentRegimeDimension::d-empreg:PartTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:WorkerMember/d-per:PeriodDimension::d-per:CalendarYear-1Member/d-tu:TimeUnitDimension::d-tu:Quarter1Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension"
                // SCENARIO: "d-empreg:EmploymentRegimeDimension::d-empreg:PartTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:WorkerMember/d-per:PeriodDimension::d-per:CalendarYear-2Member/d-tu:TimeUnitDimension::d-tu:Quarter1Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension"
                // SCENARIO: "d-empreg:EmploymentRegimeDimension::d-empreg:PartTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:WorkerMember/d-per:PeriodDimension::d-per:CalendarYear-1Member/d-tu:TimeUnitDimension::d-tu:Quarter2Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension"
                // SCENARIO: "d-empreg:EmploymentRegimeDimension::d-empreg:PartTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:WorkerMember/d-per:PeriodDimension::d-per:CalendarYear-2Member/d-tu:TimeUnitDimension::d-tu:Quarter2Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension"
                // SCENARIO: "d-empreg:EmploymentRegimeDimension::d-empreg:PartTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:WorkerMember/d-per:PeriodDimension::d-per:CalendarYear-1Member/d-tu:TimeUnitDimension::d-tu:Quarter3Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension"
                // SCENARIO: "d-empreg:EmploymentRegimeDimension::d-empreg:PartTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:WorkerMember/d-per:PeriodDimension::d-per:CalendarYear-2Member/d-tu:TimeUnitDimension::d-tu:Quarter3Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension"
                // SCENARIO: "d-empreg:EmploymentRegimeDimension::d-empreg:PartTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:WorkerMember/d-per:PeriodDimension::d-per:CalendarYear-1Member/d-tu:TimeUnitDimension::d-tu:Quarter4Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension"
                // SCENARIO: "d-empreg:EmploymentRegimeDimension::d-empreg:PartTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:WorkerMember/d-per:PeriodDimension::d-per:CalendarYear-2Member/d-tu:TimeUnitDimension::d-tu:Quarter4Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension"
                // PERIOD: "D"
                var AverageHourPay = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:AverageHourPay" });
                var GrossIncome = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:GrossIncome" });
                var PaidHoursQuarter = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:PaidHoursQuarter" });
                if (PaidHoursQuarter.Count > 0 || AverageHourPay.Count > 0 || GrossIncome.Count > 0)
                {
                    var CalculatedAmountAverageHourPay = Math.Round(Number(GrossIncome, "0") / Number(PaidHoursQuarter, "1") * (decimal)100, MidpointRounding.AwayFromZero) / (decimal)100;
                    //CALCULATION HERE
                    if ((Number(PaidHoursQuarter, "1") > (decimal)1) || (Number(AverageHourPay, "0") > (decimal)0) || (Number(GrossIncome, "0") > (decimal)0))
                    {
                        //FORMULA HERE
                        bool test = Number(AverageHourPay, "0") == (Math.Round(Number(GrossIncome, "0") / Number(PaidHoursQuarter, "1") * (decimal)100, MidpointRounding.AwayFromZero) / (decimal)100);
                        if (!test)
                        {
                            // MESSAGES
                            AddMessage(new BizTaxErrorDataContract
                            {
                                FileId = _fileId,
                                Id = "f-eap-5407",
                                Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Het totaal vermeld in de rubriek '{Gemiddeld uurloon}' {$AverageHourPay} is niet juist (= {$CalculatedAmountAverageHourPay})."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Le total repris dans la rubrique '{Rémunération horaire moyenne}' {$AverageHourPay} n'est pas correct (= {$CalculatedAmountAverageHourPay})."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Der in der Rubrik '{Durchschnittlicher Stundenlohn}' {$AverageHourPay} angegebene Gesamtbetrag ist unzutreffend (= {$CalculatedAmountAverageHourPay})."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Het totaal vermeld in de rubriek '{Gemiddeld uurloon}' {$AverageHourPay} is niet juist (= {$CalculatedAmountAverageHourPay})."}  
 }
                                ,
                                Fields = new List<string>()
                            }
                            );
                        }
                    }
                }
            } // end scenario loop

        }

        internal void Calc_f_eap_5409a_assertion_set()
        {
            List<string> scenarioIds = GetScenarioIds(new string[] { "d-empreg:EmploymentRegimeDimension::d-empreg:FullTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:EmployeeMember/d-per:PeriodDimension::d-per:CalendarYear-1Member/d-tu:TimeUnitDimension::d-tu:Quarter1Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension", "d-empreg:EmploymentRegimeDimension::d-empreg:FullTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:EmployeeMember/d-per:PeriodDimension::d-per:CalendarYear-2Member/d-tu:TimeUnitDimension::d-tu:Quarter1Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension", "d-empreg:EmploymentRegimeDimension::d-empreg:FullTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:EmployeeMember/d-per:PeriodDimension::d-per:CalendarYear-1Member/d-tu:TimeUnitDimension::d-tu:Quarter2Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension", "d-empreg:EmploymentRegimeDimension::d-empreg:FullTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:EmployeeMember/d-per:PeriodDimension::d-per:CalendarYear-2Member/d-tu:TimeUnitDimension::d-tu:Quarter2Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension", "d-empreg:EmploymentRegimeDimension::d-empreg:FullTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:EmployeeMember/d-per:PeriodDimension::d-per:CalendarYear-1Member/d-tu:TimeUnitDimension::d-tu:Quarter3Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension", "d-empreg:EmploymentRegimeDimension::d-empreg:FullTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:EmployeeMember/d-per:PeriodDimension::d-per:CalendarYear-2Member/d-tu:TimeUnitDimension::d-tu:Quarter3Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension", "d-empreg:EmploymentRegimeDimension::d-empreg:FullTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:EmployeeMember/d-per:PeriodDimension::d-per:CalendarYear-1Member/d-tu:TimeUnitDimension::d-tu:Quarter4Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension", "d-empreg:EmploymentRegimeDimension::d-empreg:FullTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:EmployeeMember/d-per:PeriodDimension::d-per:CalendarYear-2Member/d-tu:TimeUnitDimension::d-tu:Quarter4Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension", "d-empreg:EmploymentRegimeDimension::d-empreg:FullTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:WorkerMember/d-per:PeriodDimension::d-per:CalendarYear-1Member/d-tu:TimeUnitDimension::d-tu:Quarter1Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension", "d-empreg:EmploymentRegimeDimension::d-empreg:FullTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:WorkerMember/d-per:PeriodDimension::d-per:CalendarYear-2Member/d-tu:TimeUnitDimension::d-tu:Quarter1Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension", "d-empreg:EmploymentRegimeDimension::d-empreg:FullTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:WorkerMember/d-per:PeriodDimension::d-per:CalendarYear-1Member/d-tu:TimeUnitDimension::d-tu:Quarter2Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension", "d-empreg:EmploymentRegimeDimension::d-empreg:FullTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:WorkerMember/d-per:PeriodDimension::d-per:CalendarYear-2Member/d-tu:TimeUnitDimension::d-tu:Quarter2Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension", "d-empreg:EmploymentRegimeDimension::d-empreg:FullTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:WorkerMember/d-per:PeriodDimension::d-per:CalendarYear-1Member/d-tu:TimeUnitDimension::d-tu:Quarter3Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension", "d-empreg:EmploymentRegimeDimension::d-empreg:FullTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:WorkerMember/d-per:PeriodDimension::d-per:CalendarYear-2Member/d-tu:TimeUnitDimension::d-tu:Quarter3Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension", "d-empreg:EmploymentRegimeDimension::d-empreg:FullTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:WorkerMember/d-per:PeriodDimension::d-per:CalendarYear-1Member/d-tu:TimeUnitDimension::d-tu:Quarter4Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension", "d-empreg:EmploymentRegimeDimension::d-empreg:FullTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:WorkerMember/d-per:PeriodDimension::d-per:CalendarYear-2Member/d-tu:TimeUnitDimension::d-tu:Quarter4Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension" });
            foreach (string scenarioId in scenarioIds)
            {
                string period = "D";
                // SCENARIO: "d-empreg:EmploymentRegimeDimension::d-empreg:FullTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:EmployeeMember/d-per:PeriodDimension::d-per:CalendarYear-1Member/d-tu:TimeUnitDimension::d-tu:Quarter1Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension"
                // SCENARIO: "d-empreg:EmploymentRegimeDimension::d-empreg:FullTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:EmployeeMember/d-per:PeriodDimension::d-per:CalendarYear-2Member/d-tu:TimeUnitDimension::d-tu:Quarter1Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension"
                // SCENARIO: "d-empreg:EmploymentRegimeDimension::d-empreg:FullTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:EmployeeMember/d-per:PeriodDimension::d-per:CalendarYear-1Member/d-tu:TimeUnitDimension::d-tu:Quarter2Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension"
                // SCENARIO: "d-empreg:EmploymentRegimeDimension::d-empreg:FullTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:EmployeeMember/d-per:PeriodDimension::d-per:CalendarYear-2Member/d-tu:TimeUnitDimension::d-tu:Quarter2Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension"
                // SCENARIO: "d-empreg:EmploymentRegimeDimension::d-empreg:FullTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:EmployeeMember/d-per:PeriodDimension::d-per:CalendarYear-1Member/d-tu:TimeUnitDimension::d-tu:Quarter3Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension"
                // SCENARIO: "d-empreg:EmploymentRegimeDimension::d-empreg:FullTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:EmployeeMember/d-per:PeriodDimension::d-per:CalendarYear-2Member/d-tu:TimeUnitDimension::d-tu:Quarter3Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension"
                // SCENARIO: "d-empreg:EmploymentRegimeDimension::d-empreg:FullTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:EmployeeMember/d-per:PeriodDimension::d-per:CalendarYear-1Member/d-tu:TimeUnitDimension::d-tu:Quarter4Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension"
                // SCENARIO: "d-empreg:EmploymentRegimeDimension::d-empreg:FullTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:EmployeeMember/d-per:PeriodDimension::d-per:CalendarYear-2Member/d-tu:TimeUnitDimension::d-tu:Quarter4Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension"
                // SCENARIO: "d-empreg:EmploymentRegimeDimension::d-empreg:FullTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:WorkerMember/d-per:PeriodDimension::d-per:CalendarYear-1Member/d-tu:TimeUnitDimension::d-tu:Quarter1Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension"
                // SCENARIO: "d-empreg:EmploymentRegimeDimension::d-empreg:FullTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:WorkerMember/d-per:PeriodDimension::d-per:CalendarYear-2Member/d-tu:TimeUnitDimension::d-tu:Quarter1Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension"
                // SCENARIO: "d-empreg:EmploymentRegimeDimension::d-empreg:FullTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:WorkerMember/d-per:PeriodDimension::d-per:CalendarYear-1Member/d-tu:TimeUnitDimension::d-tu:Quarter2Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension"
                // SCENARIO: "d-empreg:EmploymentRegimeDimension::d-empreg:FullTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:WorkerMember/d-per:PeriodDimension::d-per:CalendarYear-2Member/d-tu:TimeUnitDimension::d-tu:Quarter2Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension"
                // SCENARIO: "d-empreg:EmploymentRegimeDimension::d-empreg:FullTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:WorkerMember/d-per:PeriodDimension::d-per:CalendarYear-1Member/d-tu:TimeUnitDimension::d-tu:Quarter3Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension"
                // SCENARIO: "d-empreg:EmploymentRegimeDimension::d-empreg:FullTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:WorkerMember/d-per:PeriodDimension::d-per:CalendarYear-2Member/d-tu:TimeUnitDimension::d-tu:Quarter3Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension"
                // SCENARIO: "d-empreg:EmploymentRegimeDimension::d-empreg:FullTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:WorkerMember/d-per:PeriodDimension::d-per:CalendarYear-1Member/d-tu:TimeUnitDimension::d-tu:Quarter4Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension"
                // SCENARIO: "d-empreg:EmploymentRegimeDimension::d-empreg:FullTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:WorkerMember/d-per:PeriodDimension::d-per:CalendarYear-2Member/d-tu:TimeUnitDimension::d-tu:Quarter4Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension"
                // PERIOD: "D"
                var AverageDayPay = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:AverageDayPay" });
                var WorkingDaysLowWages = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:WorkingDaysLowWages" });
                var WorkingDaysAllPersonnel = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:WorkingDaysAllPersonnel" });
                var AverageHourPay = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:AverageHourPay" });
                if (AverageHourPay.Count > 0 || AverageDayPay.Count > 0 || WorkingDaysLowWages.Count > 0 || WorkingDaysAllPersonnel.Count > 0)
                {
                    //CALCULATION HERE
                    //FORMULA HERE
                    bool test = (Number(AverageDayPay) <= Number(MaximumDayPayLowWage, "90.32")) ? (String(WorkingDaysLowWages, "0") == String(WorkingDaysAllPersonnel, "0")) : (Number(WorkingDaysLowWages, "0") < Number(WorkingDaysAllPersonnel, "0"));
                    if (!test)
                    {
                        // MESSAGES
                        AddMessage(new BizTaxErrorDataContract
                        {
                            FileId = _fileId,
                            Id = "f-eap-5409a",
                            Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "De rubrieken '{Arbeidsdagen, alle personeelsleden}' ({$WorkingDaysAllPersonnel}) 	en 	'{Arbeidsdagen, personeelsleden met een laag loon}' ({$WorkingDaysLowWages}) 	zijn gelijk indien er enkel personeelsleden met een laag loon zijn."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Les rubriques '{Jours de travail, tous les travailleurs}' ({$WorkingDaysAllPersonnel}) 	et 	'{Jours de travail des travailleurs avec bas salaire}' ({$WorkingDaysLowWages}) 	sont égales quand il n'y a que des travailleurs avec bas salaire."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Die Rubriken '{Arbeitstage, alle Personalmitglieder}' ({$WorkingDaysAllPersonnel}) 	und 	'{Arbeitstage der Personalmitglieder mit niedrigem Lohn}' ({$WorkingDaysLowWages}) 	sind identisch, wenn es nur Personalmitglieder mit niedrigem Lohn gibt."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "De rubrieken '{Arbeidsdagen, alle personeelsleden}' ({$WorkingDaysAllPersonnel}) 	en 	'{Arbeidsdagen, personeelsleden met een laag loon}' ({$WorkingDaysLowWages}) 	zijn gelijk indien er enkel personeelsleden met een laag loon zijn."}  
 }
                            ,
                            Fields = new List<string>()
                        }
                        );
                    }
                }
            } // end scenario loop

        }

        internal void Calc_f_eap_5409b_assertion_set()
        {
            List<string> scenarioIds = GetScenarioIds(new string[] { "d-empreg:EmploymentRegimeDimension::d-empreg:PartTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:EmployeeMember/d-per:PeriodDimension::d-per:CalendarYear-1Member/d-tu:TimeUnitDimension::d-tu:Quarter1Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension", "d-empreg:EmploymentRegimeDimension::d-empreg:PartTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:EmployeeMember/d-per:PeriodDimension::d-per:CalendarYear-2Member/d-tu:TimeUnitDimension::d-tu:Quarter1Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension", "d-empreg:EmploymentRegimeDimension::d-empreg:PartTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:EmployeeMember/d-per:PeriodDimension::d-per:CalendarYear-1Member/d-tu:TimeUnitDimension::d-tu:Quarter2Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension", "d-empreg:EmploymentRegimeDimension::d-empreg:PartTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:EmployeeMember/d-per:PeriodDimension::d-per:CalendarYear-2Member/d-tu:TimeUnitDimension::d-tu:Quarter2Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension", "d-empreg:EmploymentRegimeDimension::d-empreg:PartTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:EmployeeMember/d-per:PeriodDimension::d-per:CalendarYear-1Member/d-tu:TimeUnitDimension::d-tu:Quarter3Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension", "d-empreg:EmploymentRegimeDimension::d-empreg:PartTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:EmployeeMember/d-per:PeriodDimension::d-per:CalendarYear-2Member/d-tu:TimeUnitDimension::d-tu:Quarter3Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension", "d-empreg:EmploymentRegimeDimension::d-empreg:PartTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:EmployeeMember/d-per:PeriodDimension::d-per:CalendarYear-1Member/d-tu:TimeUnitDimension::d-tu:Quarter4Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension", "d-empreg:EmploymentRegimeDimension::d-empreg:PartTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:EmployeeMember/d-per:PeriodDimension::d-per:CalendarYear-2Member/d-tu:TimeUnitDimension::d-tu:Quarter4Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension", "d-empreg:EmploymentRegimeDimension::d-empreg:PartTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:WorkerMember/d-per:PeriodDimension::d-per:CalendarYear-1Member/d-tu:TimeUnitDimension::d-tu:Quarter1Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension", "d-empreg:EmploymentRegimeDimension::d-empreg:PartTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:WorkerMember/d-per:PeriodDimension::d-per:CalendarYear-2Member/d-tu:TimeUnitDimension::d-tu:Quarter1Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension", "d-empreg:EmploymentRegimeDimension::d-empreg:PartTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:WorkerMember/d-per:PeriodDimension::d-per:CalendarYear-1Member/d-tu:TimeUnitDimension::d-tu:Quarter2Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension", "d-empreg:EmploymentRegimeDimension::d-empreg:PartTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:WorkerMember/d-per:PeriodDimension::d-per:CalendarYear-2Member/d-tu:TimeUnitDimension::d-tu:Quarter2Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension", "d-empreg:EmploymentRegimeDimension::d-empreg:PartTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:WorkerMember/d-per:PeriodDimension::d-per:CalendarYear-1Member/d-tu:TimeUnitDimension::d-tu:Quarter3Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension", "d-empreg:EmploymentRegimeDimension::d-empreg:PartTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:WorkerMember/d-per:PeriodDimension::d-per:CalendarYear-2Member/d-tu:TimeUnitDimension::d-tu:Quarter3Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension", "d-empreg:EmploymentRegimeDimension::d-empreg:PartTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:WorkerMember/d-per:PeriodDimension::d-per:CalendarYear-1Member/d-tu:TimeUnitDimension::d-tu:Quarter4Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension", "d-empreg:EmploymentRegimeDimension::d-empreg:PartTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:WorkerMember/d-per:PeriodDimension::d-per:CalendarYear-2Member/d-tu:TimeUnitDimension::d-tu:Quarter4Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension" });
            foreach (string scenarioId in scenarioIds)
            {
                string period = "D";
                // SCENARIO: "d-empreg:EmploymentRegimeDimension::d-empreg:PartTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:EmployeeMember/d-per:PeriodDimension::d-per:CalendarYear-1Member/d-tu:TimeUnitDimension::d-tu:Quarter1Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension"
                // SCENARIO: "d-empreg:EmploymentRegimeDimension::d-empreg:PartTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:EmployeeMember/d-per:PeriodDimension::d-per:CalendarYear-2Member/d-tu:TimeUnitDimension::d-tu:Quarter1Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension"
                // SCENARIO: "d-empreg:EmploymentRegimeDimension::d-empreg:PartTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:EmployeeMember/d-per:PeriodDimension::d-per:CalendarYear-1Member/d-tu:TimeUnitDimension::d-tu:Quarter2Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension"
                // SCENARIO: "d-empreg:EmploymentRegimeDimension::d-empreg:PartTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:EmployeeMember/d-per:PeriodDimension::d-per:CalendarYear-2Member/d-tu:TimeUnitDimension::d-tu:Quarter2Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension"
                // SCENARIO: "d-empreg:EmploymentRegimeDimension::d-empreg:PartTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:EmployeeMember/d-per:PeriodDimension::d-per:CalendarYear-1Member/d-tu:TimeUnitDimension::d-tu:Quarter3Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension"
                // SCENARIO: "d-empreg:EmploymentRegimeDimension::d-empreg:PartTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:EmployeeMember/d-per:PeriodDimension::d-per:CalendarYear-2Member/d-tu:TimeUnitDimension::d-tu:Quarter3Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension"
                // SCENARIO: "d-empreg:EmploymentRegimeDimension::d-empreg:PartTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:EmployeeMember/d-per:PeriodDimension::d-per:CalendarYear-1Member/d-tu:TimeUnitDimension::d-tu:Quarter4Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension"
                // SCENARIO: "d-empreg:EmploymentRegimeDimension::d-empreg:PartTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:EmployeeMember/d-per:PeriodDimension::d-per:CalendarYear-2Member/d-tu:TimeUnitDimension::d-tu:Quarter4Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension"
                // SCENARIO: "d-empreg:EmploymentRegimeDimension::d-empreg:PartTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:WorkerMember/d-per:PeriodDimension::d-per:CalendarYear-1Member/d-tu:TimeUnitDimension::d-tu:Quarter1Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension"
                // SCENARIO: "d-empreg:EmploymentRegimeDimension::d-empreg:PartTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:WorkerMember/d-per:PeriodDimension::d-per:CalendarYear-2Member/d-tu:TimeUnitDimension::d-tu:Quarter1Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension"
                // SCENARIO: "d-empreg:EmploymentRegimeDimension::d-empreg:PartTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:WorkerMember/d-per:PeriodDimension::d-per:CalendarYear-1Member/d-tu:TimeUnitDimension::d-tu:Quarter2Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension"
                // SCENARIO: "d-empreg:EmploymentRegimeDimension::d-empreg:PartTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:WorkerMember/d-per:PeriodDimension::d-per:CalendarYear-2Member/d-tu:TimeUnitDimension::d-tu:Quarter2Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension"
                // SCENARIO: "d-empreg:EmploymentRegimeDimension::d-empreg:PartTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:WorkerMember/d-per:PeriodDimension::d-per:CalendarYear-1Member/d-tu:TimeUnitDimension::d-tu:Quarter3Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension"
                // SCENARIO: "d-empreg:EmploymentRegimeDimension::d-empreg:PartTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:WorkerMember/d-per:PeriodDimension::d-per:CalendarYear-2Member/d-tu:TimeUnitDimension::d-tu:Quarter3Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension"
                // SCENARIO: "d-empreg:EmploymentRegimeDimension::d-empreg:PartTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:WorkerMember/d-per:PeriodDimension::d-per:CalendarYear-1Member/d-tu:TimeUnitDimension::d-tu:Quarter4Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension"
                // SCENARIO: "d-empreg:EmploymentRegimeDimension::d-empreg:PartTimeMember/d-empstat:EmploymentStatusDimension::d-empstat:WorkerMember/d-per:PeriodDimension::d-per:CalendarYear-2Member/d-tu:TimeUnitDimension::d-tu:Quarter4Member/d-ty:NameTypedDimension/d-ty:NationalNumberTypedDimension"
                // PERIOD: "D"
                var AverageHourPay = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:AverageHourPay" });
                var WorkingDaysLowWages = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:WorkingDaysLowWages" });
                var WorkingDaysAllPersonnel = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:WorkingDaysAllPersonnel" });
                var AverageDayPay = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:AverageDayPay" });
                if (AverageHourPay.Count > 0 || AverageDayPay.Count > 0 || WorkingDaysLowWages.Count > 0 || WorkingDaysAllPersonnel.Count > 0)
                {
                    //CALCULATION HERE
                    //FORMULA HERE
                    bool test = (Number(AverageHourPay) <= Number(MaximumHourPayLowWage, "11.88")) ? (String(WorkingDaysLowWages, "0") == String(WorkingDaysAllPersonnel, "0")) : (Number(WorkingDaysLowWages, "0") < Number(WorkingDaysAllPersonnel, "0"));
                    if (!test)
                    {
                        // MESSAGES
                        AddMessage(new BizTaxErrorDataContract
                        {
                            FileId = _fileId,
                            Id = "f-eap-5409b",
                            Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "De rubrieken '{Arbeidsdagen, alle personeelsleden}' ({$WorkingDaysAllPersonnel}) 	en 	'{Arbeidsdagen, personeelsleden met een laag loon}' ({$WorkingDaysLowWages}) 	zijn gelijk indien er enkel personeelsleden met een laag loon zijn."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Les rubriques '{Jours de travail, tous les travailleurs}' ({$WorkingDaysAllPersonnel}) 	et 	'{Jours de travail des travailleurs avec bas salaire}' ({$WorkingDaysLowWages}) 	sont égales quand il n'y a que des travailleurs avec bas salaire."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Die Rubriken '{Arbeitstage, alle Personalmitglieder}' ({$WorkingDaysAllPersonnel}) 	und 	'{Arbeitstage der Personalmitglieder mit niedrigem Lohn}' ({$WorkingDaysLowWages}) 	sind identisch, wenn es nur Personalmitglieder mit niedrigem Lohn gibt."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "De rubrieken '{Arbeidsdagen, alle personeelsleden}' ({$WorkingDaysAllPersonnel}) 	en 	'{Arbeidsdagen, personeelsleden met een laag loon}' ({$WorkingDaysLowWages}) 	zijn gelijk indien er enkel personeelsleden met een laag loon zijn."}  
 }
                            ,
                            Fields = new List<string>()
                        }
                        );
                    }
                }
            } // end scenario loop

        }

        internal void Calc_f_eap_5412_assertion_set()
        {
            string scenarioId = GetScenarioId("");
            string period = "D";
            // SCENARIO: ""
            // PERIOD: "D"
            var EmployeesEligibleExemptionAdditionalPersonnel = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:EmployeesEligibleExemptionAdditionalPersonnel" });
            var MovementAllPersonnelCalendarYear_1 = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:MovementAllPersonnelCalendarYear-1" });
            var IncreasePersonnelLowWageCalendarYear_1 = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:IncreasePersonnelLowWageCalendarYear-1" });
            if (EmployeesEligibleExemptionAdditionalPersonnel.Count > 0 || MovementAllPersonnelCalendarYear_1.Count > 0 || IncreasePersonnelLowWageCalendarYear_1.Count > 0)
            {
                var CalculatedAmountEmployeesEligibleExemptionAdditionalPersonnel = Min(Number(MovementAllPersonnelCalendarYear_1, "0"), Number(IncreasePersonnelLowWageCalendarYear_1, "0"));
                //CALCULATION HERE
                if (Number(MovementAllPersonnelCalendarYear_1, "0") > (decimal)0)
                {
                    //FORMULA HERE
                    bool test = Number(EmployeesEligibleExemptionAdditionalPersonnel, "0") == Min(Number(MovementAllPersonnelCalendarYear_1, "0"), Number(IncreasePersonnelLowWageCalendarYear_1, "0"));
                    if (!test)
                    {
                        // MESSAGES
                        AddMessage(new BizTaxErrorDataContract
                        {
                            FileId = _fileId,
                            Id = "f-eap-5412",
                            Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Het totaal vermeld in de rubriek '{Aantal personeelsleden waarvoor de vrijstelling kan bekomen worden}' ({$EmployeesEligibleExemptionAdditionalPersonnel}) 	is niet juist (= {$CalculatedAmountEmployeesEligibleExemptionAdditionalPersonnel})."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Le total repris dans la rubrique '{Nombre de travailleurs pour lesquels l'exonération peut être obtenue}' ({$EmployeesEligibleExemptionAdditionalPersonnel}) 	n'est pas correct (= {$CalculatedAmountEmployeesEligibleExemptionAdditionalPersonnel})."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Der Betrag, der angegeben ist in der Rubrik '{Anzahl Personalmitglieder, für die die Befreiung erhalten werden kann}' ({$EmployeesEligibleExemptionAdditionalPersonnel}) 	ist unzutreffend (= {$CalculatedAmountEmployeesEligibleExemptionAdditionalPersonnel})."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Het totaal vermeld in de rubriek '{Aantal personeelsleden waarvoor de vrijstelling kan bekomen worden}' ({$EmployeesEligibleExemptionAdditionalPersonnel}) 	is niet juist (= {$CalculatedAmountEmployeesEligibleExemptionAdditionalPersonnel})."}  
 }
                            ,
                            Fields = new List<string>()
                        }
                        );
                    }
                }
            }

        }

        internal void Calc_f_eap_5413_assertion_set()
        {
            string scenarioId = GetScenarioId("");
            string period = "D";
            // SCENARIO: ""
            // PERIOD: "D"
            var ExemptionAdditionalPersonnel = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:ExemptionAdditionalPersonnel" });
            var EmployeesEligibleExemptionAdditionalPersonnel = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:EmployeesEligibleExemptionAdditionalPersonnel" });
            var FormulaTarget = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:ExemptionAdditionalPersonnel" });
            if (ExemptionAdditionalPersonnel.Count > 0 || EmployeesEligibleExemptionAdditionalPersonnel.Count > 0 || FormulaTarget.Count > 0)
            {
                var CalculatedAmountExemptionAdditionalPersonnel = Number(EmployeesEligibleExemptionAdditionalPersonnel, "0") * Number(ExemptionAllPersonnel, "5260");
                //CALCULATION HERE
                if (FormulaTarget.Count == 0)
                {
                    FormulaTarget = CreateNewCalculationElement(scenarioId, period, "tax-inc:ExemptionAdditionalPersonnel", new string[] { "" }, "EUR", "INF");
                }
                if (FormulaTarget.Count > 1)
                {
                    throw new Exception("More then one target for FormulaTarget in assertion f-eap-5413");
                }
                FormulaTarget.First().Calculated = true;
                FormulaTarget.First().SetNumber((Number(EmployeesEligibleExemptionAdditionalPersonnel, "0") * Number(ExemptionAllPersonnel, "5260")));
                //FORMULA HERE
                bool test = Number(ExemptionAdditionalPersonnel, "0") == (Number(EmployeesEligibleExemptionAdditionalPersonnel, "0") * Number(ExemptionAllPersonnel, "5260"));
                if (!test)
                {
                    // MESSAGES
                    AddMessage(new BizTaxErrorDataContract
                    {
                        FileId = _fileId,
                        Id = "f-eap-5413",
                        Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Het totaal vermeld in de rubriek '{Vrijstelling}' ({$ExemptionAdditionalPersonnel}) is niet juist (= {$CalculatedAmountExemptionAdditionalPersonnel})."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Le total repris dans la rubrique '{Exonération}' ({$ExemptionAdditionalPersonnel}) n'est pas correct (= {$CalculatedAmountExemptionAdditionalPersonnel})."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Der in der Rubrik '{Befreiung}' angegebene Gesamtbetrag ({$ExemptionAdditionalPersonnel}) ist unzutreffend (= {$CalculatedAmountExemptionAdditionalPersonnel})."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Het totaal vermeld in de rubriek '{Vrijstelling}' ({$ExemptionAdditionalPersonnel}) is niet juist (= {$CalculatedAmountExemptionAdditionalPersonnel})."}  
 }
                        ,
                        Fields = new List<string>()
                    }
                    );
                }
            }

        }

        internal void Calc_f_eap_5414_assertion_set()
        {
            string scenarioId = GetScenarioId("");
            string period = "D";
            // SCENARIO: ""
            // PERIOD: "D"
            var ReversalPreviousExemptionAdditionalPersonnel = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:ReversalPreviousExemptionAdditionalPersonnel" });
            var ExemptionAdditionalPersonnelPreviousAssessmentYear = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:ExemptionAdditionalPersonnelPreviousAssessmentYear" });
            var BasicTaxableReversalPreviousExemptionAdditionalPersonnel = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:BasicTaxableReversalPreviousExemptionAdditionalPersonnel" });
            if (ReversalPreviousExemptionAdditionalPersonnel.Count > 0 || ExemptionAdditionalPersonnelPreviousAssessmentYear.Count > 0 || BasicTaxableReversalPreviousExemptionAdditionalPersonnel.Count > 0)
            {
                var CalculatedAmountReversalPreviousExemptionAdditionalPersonnel = Min(Number(ExemptionAdditionalPersonnelPreviousAssessmentYear, "0"), Number(BasicTaxableReversalPreviousExemptionAdditionalPersonnel, "0"));
                //CALCULATION HERE
                //FORMULA HERE
                bool test = Number(ReversalPreviousExemptionAdditionalPersonnel, "0") == Min(Number(ExemptionAdditionalPersonnelPreviousAssessmentYear, "0"), Number(BasicTaxableReversalPreviousExemptionAdditionalPersonnel, "0"));
                if (!test)
                {
                    // MESSAGES
                    AddMessage(new BizTaxErrorDataContract
                    {
                        FileId = _fileId,
                        Id = "f-eap-5414",
                        Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Het totaal vermeld in de rubriek '{Terug te nemen vrijstelling}' ({$ReversalPreviousExemptionAdditionalPersonnel}) is niet juist (= {$CalculatedAmountReversalPreviousExemptionAdditionalPersonnel})."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Le total repris dans la rubrique '{Exonération à reprendre}' ({$ReversalPreviousExemptionAdditionalPersonnel}) n'est pas correct (= {$CalculatedAmountReversalPreviousExemptionAdditionalPersonnel})."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Der in der Rubrik '{Zu übernehmende Befreiung}' ({$ReversalPreviousExemptionAdditionalPersonnel}) angegebene Gesamtbetrag ist unzutreffend (= {$CalculatedAmountReversalPreviousExemptionAdditionalPersonnel})."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Het totaal vermeld in de rubriek '{Terug te nemen vrijstelling}' ({$ReversalPreviousExemptionAdditionalPersonnel}) is niet juist (= {$CalculatedAmountReversalPreviousExemptionAdditionalPersonnel})."}  
 }
                        ,
                        Fields = new List<string>()
                    }
                    );
                }
            }

        }

        internal void Calc_f_eapsf_5441_assertion_set()
        {

        }

        internal void Calc_f_eapsf_5442_assertion_set()
        {

        }

        internal void Calc_f_eapsf_5443_assertion_set()
        {

        }

        internal void Calc_f_eapsf_5444_assertion_set()
        {

        }

        internal void Calc_f_eapsf_5445_assertion_set()
        {

        }

        internal void Calc_f_eapsf_5446_assertion_set()
        {

        }

        internal void Calc_f_eapsf_5447_assertion_set()
        {

        }

        internal void Calc_f_eapsf_5448_assertion_set()
        {

        }

        internal void Calc_f_eapsf_5450_assertion_set()
        {

        }

        internal void Calc_f_eapsf_5451_assertion_set()
        {

        }

        internal void Calc_f_eapsf_5452_assertion_set()
        {

        }

        internal void Calc_f_eapsf_5453_assertion_set()
        {

        }

        internal void Calc_f_eapsf_5455_assertion_set()
        {

        }

        internal void Calc_f_eapsf_5456_assertion_set()
        {

        }

        internal void Calc_f_eapsf_5457_assertion_set()
        {

        }

        internal void Calc_f_eapsf_5458_assertion_set()
        {

        }

        internal void Calc_f_eapsf_5460_assertion_set()
        {

        }

        internal void Calc_f_eapsf_5461_assertion_set()
        {

        }

        internal void Calc_f_dfa_5501_assertion_set()
        {

        }

        internal void Calc_f_dfa_5502_assertion_set()
        {

        }


        #endregion

      
    
    }
}