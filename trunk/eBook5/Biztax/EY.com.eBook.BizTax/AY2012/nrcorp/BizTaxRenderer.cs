﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System;
using System.Xml;
using System.Xml.Linq;
using EY.com.eBook.BizTax.Contracts;

namespace EY.com.eBook.BizTax.AY2012.nrcorp
{
    public class BizTaxRenderer : BizTaxRendererGenerated
    {
        public List<XmlQualifiedName> NameSpaces
        {
            get
            {
                return new List<XmlQualifiedName> {
          
            new XmlQualifiedName("xlink","http://www.w3.org/1999/xlink")
              ,
            new XmlQualifiedName("","http://www.xbrl.org/2003/instance")
          ,
            new XmlQualifiedName("iso4217","http://www.xbrl.org/2003/iso4217")
          ,
            new XmlQualifiedName("xsi","http://www.w3.org/2001/XMLSchema-instance")
          ,
            new XmlQualifiedName("link","http://www.xbrl.org/2003/linkbase")
   
          ,
            new XmlQualifiedName("xbrldi","http://xbrl.org/2006/xbrldi")
          ,
            new XmlQualifiedName("xbrldt","http://xbrl.org/2005/xbrldt")
          ,
            new XmlQualifiedName("tax-inc-nrcorp","http://www.minfin.fgov.be/be/tax/inc/nrcorp/2012-04-30")
          ,
            new XmlQualifiedName("xl","http://www.xbrl.org/2003/XLink")
          ,
            new XmlQualifiedName("t-reso","http://www.minfin.fgov.be/be/tax/t/reso/2012-04-30")
          ,
            new XmlQualifiedName("d-ty","http://www.minfin.fgov.be/be/tax/d/ty/2012-04-30")
          ,
            new XmlQualifiedName("tax-inc","http://www.minfin.fgov.be/be/tax/inc/2012-04-30")
          ,
            new XmlQualifiedName("pfs-dt","http://www.nbb.be/be/fr/pfs/ci/dt/2013-04-01")
          ,
            new XmlQualifiedName("ref","http://www.xbrl.org/2006/ref")
          ,
            new XmlQualifiedName("t-drfa","http://www.minfin.fgov.be/be/tax/t/drfa/2012-04-30")
          ,
            new XmlQualifiedName("t-wddc","http://www.minfin.fgov.be/be/tax/t/wddc/2012-04-30")
          ,
            new XmlQualifiedName("d-hh","http://www.minfin.fgov.be/be/tax/d/hh/2012-04-30")
          ,
            new XmlQualifiedName("d-ec","http://www.minfin.fgov.be/be/tax/d/ec/2012-04-30")
          ,
            new XmlQualifiedName("t-prre","http://www.minfin.fgov.be/be/tax/t/prre/2012-04-30")
          ,
            new XmlQualifiedName("d-expt","http://www.minfin.fgov.be/be/tax/d/expt/2012-04-30")
          ,
            new XmlQualifiedName("t-cg","http://www.minfin.fgov.be/be/tax/t/cg/2012-04-30")
          ,
            new XmlQualifiedName("d-asst","http://www.minfin.fgov.be/be/tax/d/asst/2012-04-30")
          ,
            new XmlQualifiedName("t-ricg","http://www.minfin.fgov.be/be/tax/t/ricg/2012-04-30")
          ,
            new XmlQualifiedName("t-stcg","http://www.minfin.fgov.be/be/tax/t/stcg/2012-04-30")
          ,
            new XmlQualifiedName("t-ace","http://www.minfin.fgov.be/be/tax/t/ace/2012-04-30")
          ,
            new XmlQualifiedName("t-pcs","http://www.minfin.fgov.be/be/tax/t/pcs/2012-04-30")
          ,
            new XmlQualifiedName("t-dpi","http://www.minfin.fgov.be/be/tax/t/dpi/2012-04-30")
          ,
            new XmlQualifiedName("t-itifa","http://www.minfin.fgov.be/be/tax/t/itifa/2012-04-30")
          ,
            new XmlQualifiedName("d-per","http://www.minfin.fgov.be/be/tax/d/per/2012-04-30")
          ,
            new XmlQualifiedName("t-idtc","http://www.minfin.fgov.be/be/tax/t/idtc/2012-04-30")
          ,
            new XmlQualifiedName("d-incc","http://www.minfin.fgov.be/be/tax/d/incc/2012-04-30")
          ,
            new XmlQualifiedName("d-invc","http://www.minfin.fgov.be/be/tax/d/invc/2012-04-30")
          ,
            new XmlQualifiedName("t-eap","http://www.minfin.fgov.be/be/tax/t/eap/2012-04-30")
          ,
            new XmlQualifiedName("d-empstat","http://www.minfin.fgov.be/be/tax/d/empstat/2012-04-30")
          ,
            new XmlQualifiedName("d-empreg","http://www.minfin.fgov.be/be/tax/d/empreg/2012-04-30")
          ,
            new XmlQualifiedName("d-tu","http://www.minfin.fgov.be/be/tax/d/tu/2012-04-30")
          ,
            new XmlQualifiedName("t-eapsf","http://www.minfin.fgov.be/be/tax/t/eapsf/2012-04-30")
          ,
            new XmlQualifiedName("d-empf","http://www.minfin.fgov.be/be/tax/d/empf/2012-04-30")
          ,
            new XmlQualifiedName("d-empper","http://www.minfin.fgov.be/be/tax/d/empper/2012-04-30")
          ,
            new XmlQualifiedName("t-dfa","http://www.minfin.fgov.be/be/tax/t/dfa/2012-04-30")
          ,
            new XmlQualifiedName("d-depm","http://www.minfin.fgov.be/be/tax/d/depm/2012-04-30")
          ,
            new XmlQualifiedName("d-br","http://www.minfin.fgov.be/be/tax/d/br/2012-04-30")
          ,
            new XmlQualifiedName("d-origin","http://www.minfin.fgov.be/be/tax/d/origin/2012-04-30")
          ,
            new XmlQualifiedName("pfs-gcd","http://www.nbb.be/be/fr/pfs/ci/gcd/2013-04-01")
          ,
            new XmlQualifiedName("pfs-vl","http://www.nbb.be/be/fr/pfs/ci/vl/2013-04-01")
          
        };
            }

        }

        public XDocument RenderBizTaxFile(BizTaxDataContract btdc, Guid fileId)
        {
            return RenderBizTaxFile(btdc, fileId, NameSpaces,TaxonomySchemaRef_rcorp);
        }

        internal override void Calc_f_nrcorp_2446_assertion_set()
        {
            // HAS NO UNBOUND SEQUENCES
            var nonNegativeMonetaryElementsNonResidentCorporate = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:TaxableReservedProfit", "tax-inc:DisallowedExpenses", "tax-inc:ImmovablePropertyIncome", "tax-inc:IncomePartnerCompaniesJointVenturesAssociationsNoLegalPersonality", "tax-inc:DeductionLimitElementsFiscalResult", "tax-inc:BasicTaxableAmountCommonRate", "tax-inc:BasicTaxableAmountExitTaxRate", "tax-inc:UnjustifiedExpensesHiddenSurplusProfitsFinancialAdvantagesBenefitsAllKind", "tax-inc:AdditionalDutiesDiamondTraders", "tax-inc:RetributionTaxCreditResearchDevelopment", "tax-inc:DeductibleMiscellaneousExemptions", "tax-inc:PEExemptIncomeMovableAssets", "tax-inc:CarryOverNextTaxPeriodPEExemptIncomeMovableAssets", "tax-inc:AccumulatedAllowanceCorporateEquity", "tax-inc:CarryOverNextTaxPeriodInsufficientProfitsAllowanceCorporateEquity", "tax-inc:Prepayments", "tax-inc:NonRepayableAdvanceLevies", "tax-inc:RepayableAdvanceLevies" });
            var monetaryElementsNonResidentCorporate = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:ShippingResultTonnageBased", "tax-inc:RemainingFiscalResultBeforeOriginDistribution" });
            var nonPositiveMonetaryElementsNonResidentCorporate = GetElementsByScenDefs(new string[] { "" }, new string[] { "D" }, new string[] { "tax-inc:CarryOverTaxLosses" });
            if (nonNegativeMonetaryElementsNonResidentCorporate.Count > 0 || monetaryElementsNonResidentCorporate.Count > 0 || nonPositiveMonetaryElementsNonResidentCorporate.Count > 0)
            {
                //CALCULATION HERE
                if ((Sum(Number(nonNegativeMonetaryElementsNonResidentCorporate, "0")) > (decimal)0) || (Sum(Number(monetaryElementsNonResidentCorporate, "0")) != (decimal)0) || (Sum(Number(nonPositiveMonetaryElementsNonResidentCorporate, "0")) < (decimal)0))
                {
                    //FORMULA HERE
                    bool test = Count(Xbrl.Elements.Where(e => e.Name == "BalanceSheetStatutorySeatStatutoryAccountsBelgianBranch").ToList()) != (decimal)0;
                    if (!test)
                    {
                        // MESSAGES
                        AddMessage(new BizTaxErrorDataContract
                        {
                            FileId = _fileId,
                            Id = "f-nrcorp-2446",
                            Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "De bijlage '{Balans opgesteld door de maatschappelijke zetel en jaarrekening van de inrichting (balans, resultatenrekening en eventuele toelichting)}' is verplicht bij te voegen in het tabblad 	'{Diverse bescheiden en opgaven}'."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "L'annexe '{Bilan établi par le siège social et les comptes annuels de l'établissement (bilan, compte de résultats et annexe éventuelle)}' doit obligatoirement être ajoutée dans l'onglet 	'{Documents et relevés divers}'."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Der Anhang '{Vom Gesellschaftssitz aufgestellte Bilanz und Jahresabschluss der Einrichtung (Bilanz, Ergebnisrechnung und eventuelle Anlage)}' muss obligatorisch in die Registerkarte '{Verschiedene Unterlagen und Verzeichnisse}' eingegeben werden."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "De bijlage '{Balans opgesteld door de maatschappelijke zetel en jaarrekening van de inrichting (balans, resultatenrekening en eventuele toelichting)}' is verplicht bij te voegen in het tabblad 	'{Diverse bescheiden en opgaven}'."}  
 }
                            ,
                            Fields = new List<string>()
                        }
                        );
                    }
                }
            }

        }

        internal override void Calc_f_nrcorp_1211_assertion_set()
        {
            string scenarioId = GetScenarioId("");
            string period = "D";
            // SCENARIO: ""
            // PERIOD: "D"
            var RemainingFiscalResultBeforeOriginDistribution = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:RemainingFiscalResultBeforeOriginDistribution" });
            var LossCurrentTaxPeriod = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:LossCurrentTaxPeriod" });
            if (RemainingFiscalResultBeforeOriginDistribution.Count > 0 || LossCurrentTaxPeriod.Count > 0)
            {
                //CALCULATION HERE
                if ((Number(RemainingFiscalResultBeforeOriginDistribution, "0") < (decimal)0) || (Number(LossCurrentTaxPeriod, "0") < (decimal)0))
                {
                    //FORMULA HERE
                    bool test = Number(RemainingFiscalResultBeforeOriginDistribution, "0") == Number(LossCurrentTaxPeriod, "0");
                    if (!test)
                    {
                        // MESSAGES
                        AddMessage(new BizTaxErrorDataContract
                        {
                            FileId = _fileId,
                            Id = "f-nrcorp-1211",
                            Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Is het bedrag vermeld in de rubriek '{Resterend resultaat}' ({$RemainingFiscalResultBeforeOriginDistribution}) negatief, dan is dit bedrag gelijk aan het bedrag vermeld in de rubriek 	'{Verlies van het belastbare tijdperk}' ({$LossCurrentTaxPeriod})."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Si le montant repris dans la rubrique '{Résultat subsistant}' ({$RemainingFiscalResultBeforeOriginDistribution}) est négatif, ce montant doit être égal au montant repris dans la rubrique 	'{Perte de la période imposable}' ({$LossCurrentTaxPeriod})."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Wenn der in der Rubrik '{Verbleibendes Ergebnis}' angegebene Betrag ({$RemainingFiscalResultBeforeOriginDistribution}) negativ ist, entspricht dieser Betrag dem in der Rubrik 	'{Verlust des Besteuerungszeitraums}'angegebenen Betrag ({$LossCurrentTaxPeriod})."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Is het bedrag vermeld in de rubriek '{Resterend resultaat}' ({$RemainingFiscalResultBeforeOriginDistribution}) negatief, dan is dit bedrag gelijk aan het bedrag vermeld in de rubriek 	'{Verlies van het belastbare tijdperk}' ({$LossCurrentTaxPeriod})."}  
 }
                            ,
                            Fields = new List<string>()
                        }
                        );
                    }
                }
            }

        }
    }
}
