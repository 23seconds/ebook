﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EY.com.eBook.Core;
using EY.com.eBook.BizTax.Contracts;
using System.Xml.Linq;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.API.BL;
using EY.com.eBook.Core.EF;

namespace EY.com.eBook.BizTax.AY2012.nrcorp
{
    public class FileBizTaxRepository : EY.com.eBook.BizTax.FileBizTaxRepository
    {

        internal AY2012.nrcorp.BizTaxRenderer Renderer = new AY2012.nrcorp.BizTaxRenderer();

        internal Contracts.FileXbrlDataContract _fileXbrl { get; set; }

        public Contracts.FileXbrlDataContract GetData(Contracts.Criteria.CriteriaBiztaxDataContract cbdc, bool recalc)
        {
            DateTime? wsUpdate = null;
            bool updateWS = false;
            if (Core.EF.eBookWriteManager.Context.FileServices.Count(f => f.FileId == cbdc.FileId && f.ServiceId == ServiceIdBiztaxAuto) > 0)
            {
                wsUpdate = Core.EF.eBookWriteManager.Context.Worksheets.Where(w => w.FileId == cbdc.FileId).Max(w => w.DataLU);
            }
            Core.EF.Write.FileXbrl fx = Core.EF.eBookWriteManager.Context.FileXbrlSet.FirstOrDefault(x => x.FileId == cbdc.FileId);
              Core.EF.Write.File fle = Core.EF.eBookWriteManager.Context.Files.First(f => f.Id == cbdc.FileId);
                Core.EF.Write.Client cle = Core.EF.eBookWriteManager.Context.ClientSet.First(c => c.Id == fle.ClientId);
            if (fx != null)
            {
                _fileXbrl = new Contracts.FileXbrlDataContract
                {
                    Calculation = fx.Calculation
                    ,
                    Data = CoreHelper.DeserializeFromString<EY.com.eBook.BizTax.Contracts.BizTaxDataContract>(fx.XbrlData)
                    ,
                    DeclaringDepartment = fx.DeclaringDepartment
                    ,
                    DeclaringPartnerId = fx.DeclaringPartnerId
                    ,
                    DeclaringPartnerName = fx.DeclaringPartnerName
                    ,
                    FileId = fx.FileId
                    ,
                    History = new List<EY.com.eBook.BizTax.Contracts.FileXbrlHistoryDataContract>()
                    ,
                    Locked = fx.Locked
                    ,
                    Status = fx.StatusId
                    ,
                    Type = fx.XbrlType
                    ,
                    Validated = fx.Validated
                };
                if (!_fileXbrl.Data.LastSaved.HasValue) _fileXbrl.Data.LastSaved = DateTime.Now.AddYears(-1);
                if (wsUpdate.HasValue && wsUpdate.Value > _fileXbrl.Data.LastSaved.Value)
                {
                    _fileXbrl.WorksheetsNewer = true;
                }
                XbrlElementDataContract xedc = _fileXbrl.Data.Elements.FirstOrDefault(e => e.Name == "TaxReturnType");
                if (xedc != null && xedc.Value != "BNI/ven")
                {
                    xedc.Value = "BNI/ven";
                    Save(true);

                }
                    Renderer.Load(_fileXbrl.Data);
                
            }
            else
            {
              
                string sref = cbdc.Type == "rcorp" ? Renderer.TaxonomySchemaRef_rcorp : cbdc.Type == "nrcorp" ? Renderer.TaxonomySchemaRef_nrcorp : Renderer.TaxonomySchemaRef_rle;
                Renderer.CreateNew(cle.EnterpriseNumber, fle.StartDate, fle.EndDate, sref);
                _fileXbrl = new EY.com.eBook.BizTax.Contracts.FileXbrlDataContract
                {
                    Calculation = null
                    ,
                    Data = Renderer.Xbrl
                    ,
                    DeclaringDepartment = null
                    ,
                    DeclaringPartnerId = null
                    ,
                    DeclaringPartnerName = null
                    ,
                    FileId = fle.Id
                    ,
                    History = new List<EY.com.eBook.BizTax.Contracts.FileXbrlHistoryDataContract>()
                    ,
                    Locked = false
                    ,
                    Status = 0
                    ,
                    Type = cbdc.Type
                    ,
                    Validated = false
                    ,
                    WorksheetsNewer = wsUpdate.HasValue
                };
                if (!recalc) Save();
                //recalc = false;
            }

            

            if (!Renderer.Xbrl.ProxyId.HasValue || _fileXbrl.Status < 1)
            {
                Renderer.Xbrl = FindProxy(Renderer.Xbrl, fle);
            }

            Renderer.SetGeneralData(cle.EnterpriseNumber.ToBelgianEnterprise(), fle.StartDate, fle.EndDate, cle, fle.Culture);
            Renderer.CreateIndexes();

            _fileXbrl.Data = Renderer.Xbrl;


            if (recalc)
            {
                CalculateAndValidate();
                Save();
            }
          //  _fileXbrl.Locked = true;

            return _fileXbrl;
        }

        public void CalculateAndValidate()
        {
            if (_fileXbrl == null || Renderer == null || Renderer.Xbrl == null) return;
            if (_fileXbrl.Locked) return;
            Renderer.Xbrl.Elements.Where(r => r.Calculated).ToList().ForEach(e => { e.Value = null; });
            Renderer.CalculateAndValidate();
            Renderer.CalculateAndValidate();
            _fileXbrl.Data = Renderer.Xbrl;
        }

        public void Save(Contracts.FileXbrlDataContract fxdc,bool recalc)
        {
            _fileXbrl = fxdc;
            if (recalc)
            {
                Renderer.Load(fxdc.Data);
                Renderer.CreateIndexes();
                CalculateAndValidate();

            }
            Save();
        }

        public void Save()
        {
            Save(true);
        }

        public void Save(bool includeData)
        {
            if (_fileXbrl != null)
            {
                Core.EF.Write.FileXbrl fx = Core.EF.eBookWriteManager.Context.FileXbrlSet.FirstOrDefault(x => x.FileId == _fileXbrl.FileId);
                if (fx == null)
                {
                    fx = new Core.EF.Write.FileXbrl { FileId = _fileXbrl.FileId,XbrlType = _fileXbrl.Type };
                }
                fx.Calculation = _fileXbrl.Calculation;
                fx.DeclaringDepartment = _fileXbrl.DeclaringDepartment;
                fx.DeclaringPartnerId = _fileXbrl.DeclaringPartnerId;
                fx.DeclaringPartnerName = _fileXbrl.DeclaringPartnerName;
                fx.Locked = _fileXbrl.Locked;
                fx.StatusId = _fileXbrl.Status;
                fx.Validated = _fileXbrl.Validated;
                if (includeData)
                {
                    _fileXbrl.Data.LastSaved = DateTime.Now;
                    fx.XbrlData = CoreHelper.SerializeToString(_fileXbrl.Data);
                }
                

                if (fx.EntityState == System.Data.EntityState.Detached)
                    Core.EF.eBookWriteManager.Context.AddToFileXbrlSet(fx);
                
                Core.EF.eBookWriteManager.Context.SaveChanges();
            }

        }


        internal void UpdateMeta(FileXbrlInfoDataContract fxidc)
        {
            if (_fileXbrl == null)
            {
                _fileXbrl = new EY.com.eBook.BizTax.Contracts.FileXbrlDataContract
                {
                    FileId = fxidc.FileId,
                    Type= fxidc.Type,
                    Locked = false
                };
            }
            
            _fileXbrl.Calculation = fxidc.Calculation;
            _fileXbrl.DeclaringDepartment = fxidc.DeclaringDepartment;
            _fileXbrl.DeclaringPartnerId = fxidc.DeclaringPartnerId;
            _fileXbrl.DeclaringPartnerName = fxidc.DeclaringPartnerName;
            _fileXbrl.Locked = fxidc.Status>0;
            _fileXbrl.Status = fxidc.Status;
            _fileXbrl.Validated = fxidc.Validated;
            _fileXbrl.Type = fxidc.Type;
            Save(false);
        }

        internal void Publish(Contracts.Criteria.CriteriaBiztaxDataContract cbdc)
        {
            GetData(cbdc, false);
            if (!_fileXbrl.Locked) throw new Exception("Please lock the contents prior to publish");

            // Generate XBRL
            XDocument xbrl = Renderer.RenderBizTaxFile(Renderer.Xbrl, _fileXbrl.FileId);
            ActivePersonDataContract apdc = null;
            if (!string.IsNullOrEmpty(_fileXbrl.EncodedActivePerson))
            {
                apdc = new BusinessHelper().DecodePerson(_fileXbrl.EncodedActivePerson);
            }
            using (new Impersonator(Config.AppuserUsername, Config.AppuserDomain, Config.AppuserPwd))
            {
                EY.com.eBook.Core.EF.Write.BizTaxDeclaration btdExists = eBookWriteManager.Context.BizTaxDeclarationSet.FirstOrDefault(d => d.Id == _fileXbrl.FileId);
                if (btdExists != null)
                {

                    if (System.IO.Directory.Exists(btdExists.CurrentPath))
                    {
                        System.IO.Directory.Delete(btdExists.CurrentPath, true);
                    }

                    eBookWriteManager.Context.DeleteObject(btdExists);
                    eBookWriteManager.Context.SaveChanges();
                }

                string basefolder = Config.GetBiztaxPublishFolder(_fileXbrl.DeclaringDepartment, _fileXbrl.Data.AssessmentYear.ToString());
                basefolder = System.IO.Path.Combine(basefolder, _fileXbrl.DeclaringPartnerName.Replace(" ", "."));

                if (!System.IO.Directory.Exists(basefolder))
                {
                    System.IO.Directory.CreateDirectory(basefolder);
                }

                EY.com.eBook.Core.EF.Read.File file = eBookReadManager.Context.Files.Include("Client").First(f => f.Id == _fileXbrl.FileId);
                basefolder = System.IO.Path.Combine(basefolder, file.Client.Name.CleanXbrlId());
                string filebasefolder = System.IO.Path.Combine(basefolder, file.EndDate.ToString("dd-MM-yyyy"));
                if (System.IO.Directory.Exists(filebasefolder))
                {
                    filebasefolder = System.IO.Path.Combine(basefolder, string.Format("{0}_{1}", file.EndDate.ToString("dd-MM-yyyy"), file.Id.ToString().CleanXbrlId()));
                }
                string eBookFile = System.IO.Path.Combine(filebasefolder, string.Format("{0}.eBook", _fileXbrl.FileId.ToString()));
                if (!System.IO.Directory.Exists(filebasefolder))
                {
                    System.IO.Directory.CreateDirectory(filebasefolder);

                }

                if (!System.IO.File.Exists(eBookFile))
                {
                    System.IO.StreamWriter sw = System.IO.File.CreateText(eBookFile);
                    sw.WriteLine(string.Format("Created: {0} - ClientId:{1}", DateTime.Now.ToString(), file.Client.Id.ToString()));
                    sw.Close();
                }


                
                xbrl.Save(System.IO.Path.Combine(filebasefolder, @"biztax.biztax"));
                
                if (_fileXbrl.Data.ProxyId.HasValue)
                {
                    string path = new EY.com.eBook.API.BL.Read.Repository().GetFilePath(_fileXbrl.Data.ProxyId.Value);
                    if (System.IO.File.Exists(System.IO.Path.Combine(filebasefolder, "proxy.pdf")))
                    {
                        System.IO.File.Delete(System.IO.Path.Combine(filebasefolder, "proxy.pdf"));
                    }
                    if (System.IO.File.Exists(path))
                    {
                        System.IO.File.Copy(path, System.IO.Path.Combine(filebasefolder, "proxy.pdf"));
                    }
                }

                
                EY.com.eBook.Core.EF.Write.BizTaxDeclaration btd = new EY.com.eBook.Core.EF.Write.BizTaxDeclaration
                {
                    Id = file.Id
                    ,
                    AssessmentYear = file.EndDate.AddDays(1).Year
                    ,
                    BiztaxCalc = null
                    ,
                    ClientEnterprise = file.Client.EnterpriseNumber
                    ,
                    ClientId = file.Client.Id
                    ,
                    ClientName = file.Client.Name
                    ,
                    CurrentPath = filebasefolder
                    ,
                    Department = _fileXbrl.DeclaringDepartment
                    ,
                    eBookCalc = null
                    ,
                    Errors = null
                    ,
                    FileType = "F"
                    ,
                    InProgressBy = null
                    ,
                    LastChanged = DateTime.Now
                    ,
                    PartnerId = _fileXbrl.DeclaringPartnerId.Value
                    ,
                    PartnerName = _fileXbrl.DeclaringPartnerName
                    ,
                    Proxy = _fileXbrl.Data.ProxyId.HasValue
                    ,
                    SenderDate = DateTime.Now
                    ,
                    SenderName = apdc != null ? apdc.Name : "TEST"
                    ,
                    Status = 10//xdc.Status
                };


                eBookWriteManager.Context.AddToBizTaxDeclarationSet(btd);
                eBookWriteManager.Context.SaveChanges();
            }
            // Change state
            _fileXbrl.Status = 10;
            Save(true);
        }

        internal CalcResultDataContract GetCalcResults()
        {
            return new CalcResultDataContract {
                Elements = _fileXbrl.Data.Elements.Where(e=>e.Calculated).ToList(),
                Errors = _fileXbrl.Data.Errors
            };
        }
    }
}
