﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System;
using System.Xml;
using System.Xml.Linq;
using EY.com.eBook.BizTax.Contracts;

namespace EY.com.eBook.BizTax.AY2012.rcorp
{
    public class BizTaxRenderer : BizTaxRendererGenerated
    {
        public List<XmlQualifiedName> NameSpaces
        {
            get
            {
                return new List<XmlQualifiedName> {
          
            new XmlQualifiedName("xlink","http://www.w3.org/1999/xlink")
              ,
            new XmlQualifiedName("","http://www.xbrl.org/2003/instance")
          ,
            new XmlQualifiedName("iso4217","http://www.xbrl.org/2003/iso4217")
          ,
            new XmlQualifiedName("xsi","http://www.w3.org/2001/XMLSchema-instance")
          ,
            new XmlQualifiedName("link","http://www.xbrl.org/2003/linkbase")
   
          ,
            new XmlQualifiedName("xbrldi","http://xbrl.org/2006/xbrldi")
          ,
            new XmlQualifiedName("xbrldt","http://xbrl.org/2005/xbrldt")
          ,
            new XmlQualifiedName("tax-inc-rcorp","http://www.minfin.fgov.be/be/tax/inc/rcorp/2012-04-30")
          ,
            new XmlQualifiedName("xl","http://www.xbrl.org/2003/XLink")
          ,
            new XmlQualifiedName("t-reso","http://www.minfin.fgov.be/be/tax/t/reso/2012-04-30")
          ,
            new XmlQualifiedName("d-ty","http://www.minfin.fgov.be/be/tax/d/ty/2012-04-30")
          ,
            new XmlQualifiedName("tax-inc","http://www.minfin.fgov.be/be/tax/inc/2012-04-30")
          ,
            new XmlQualifiedName("pfs-dt","http://www.nbb.be/be/fr/pfs/ci/dt/2013-04-01")
          ,
            new XmlQualifiedName("ref","http://www.xbrl.org/2006/ref")
          ,
            new XmlQualifiedName("t-drfa","http://www.minfin.fgov.be/be/tax/t/drfa/2012-04-30")
          ,
            new XmlQualifiedName("t-wddc","http://www.minfin.fgov.be/be/tax/t/wddc/2012-04-30")
          ,
            new XmlQualifiedName("d-hh","http://www.minfin.fgov.be/be/tax/d/hh/2012-04-30")
          ,
            new XmlQualifiedName("d-ec","http://www.minfin.fgov.be/be/tax/d/ec/2012-04-30")
          ,
            new XmlQualifiedName("t-prre","http://www.minfin.fgov.be/be/tax/t/prre/2012-04-30")
          ,
            new XmlQualifiedName("d-expt","http://www.minfin.fgov.be/be/tax/d/expt/2012-04-30")
          ,
            new XmlQualifiedName("t-cg","http://www.minfin.fgov.be/be/tax/t/cg/2012-04-30")
          ,
            new XmlQualifiedName("d-asst","http://www.minfin.fgov.be/be/tax/d/asst/2012-04-30")
          ,
            new XmlQualifiedName("t-ricg","http://www.minfin.fgov.be/be/tax/t/ricg/2012-04-30")
          ,
            new XmlQualifiedName("t-stcg","http://www.minfin.fgov.be/be/tax/t/stcg/2012-04-30")
          ,
            new XmlQualifiedName("t-ace","http://www.minfin.fgov.be/be/tax/t/ace/2012-04-30")
          ,
            new XmlQualifiedName("t-pcs","http://www.minfin.fgov.be/be/tax/t/pcs/2012-04-30")
          ,
            new XmlQualifiedName("t-dpi","http://www.minfin.fgov.be/be/tax/t/dpi/2012-04-30")
          ,
            new XmlQualifiedName("t-itifa","http://www.minfin.fgov.be/be/tax/t/itifa/2012-04-30")
          ,
            new XmlQualifiedName("d-per","http://www.minfin.fgov.be/be/tax/d/per/2012-04-30")
          ,
            new XmlQualifiedName("t-idtc","http://www.minfin.fgov.be/be/tax/t/idtc/2012-04-30")
          ,
            new XmlQualifiedName("d-incc","http://www.minfin.fgov.be/be/tax/d/incc/2012-04-30")
          ,
            new XmlQualifiedName("d-invc","http://www.minfin.fgov.be/be/tax/d/invc/2012-04-30")
          ,
            new XmlQualifiedName("t-eap","http://www.minfin.fgov.be/be/tax/t/eap/2012-04-30")
          ,
            new XmlQualifiedName("d-empstat","http://www.minfin.fgov.be/be/tax/d/empstat/2012-04-30")
          ,
            new XmlQualifiedName("d-empreg","http://www.minfin.fgov.be/be/tax/d/empreg/2012-04-30")
          ,
            new XmlQualifiedName("d-tu","http://www.minfin.fgov.be/be/tax/d/tu/2012-04-30")
          ,
            new XmlQualifiedName("t-eapsf","http://www.minfin.fgov.be/be/tax/t/eapsf/2012-04-30")
          ,
            new XmlQualifiedName("d-empf","http://www.minfin.fgov.be/be/tax/d/empf/2012-04-30")
          ,
            new XmlQualifiedName("d-empper","http://www.minfin.fgov.be/be/tax/d/empper/2012-04-30")
          ,
            new XmlQualifiedName("t-dfa","http://www.minfin.fgov.be/be/tax/t/dfa/2012-04-30")
          ,
            new XmlQualifiedName("d-depm","http://www.minfin.fgov.be/be/tax/d/depm/2012-04-30")
          ,
            new XmlQualifiedName("d-br","http://www.minfin.fgov.be/be/tax/d/br/2012-04-30")
          ,
            new XmlQualifiedName("d-origin","http://www.minfin.fgov.be/be/tax/d/origin/2012-04-30")
          ,
            new XmlQualifiedName("pfs-gcd","http://www.nbb.be/be/fr/pfs/ci/gcd/2013-04-01")
          ,
            new XmlQualifiedName("pfs-vl","http://www.nbb.be/be/fr/pfs/ci/vl/2013-04-01")
          
        };
            }

        }

        public XDocument RenderBizTaxFile(BizTaxDataContract btdc, Guid fileId)
        {
            return RenderBizTaxFile(btdc, fileId, NameSpaces,TaxonomySchemaRef_rcorp);
        }

        internal override void  Calc_f_corp_1411_assertion_set()
        {
            string scenarioId = GetScenarioId("");
            string period = "D";
            // SCENARIO: ""
            // PERIOD: "D"
            var CarryOverTaxLosses = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:CarryOverTaxLosses" });
            var CompensableTaxLosses = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:CompensableTaxLosses" });
            var CompensatedTaxLosses = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:CompensatedTaxLosses" });
            var LossCurrentTaxPeriod = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:LossCurrentTaxPeriod" });
            var FormulaTarget = GetElementsByRef(scenarioId, new string[] { period }, new string[] { "tax-inc:CarryOverTaxLosses" });
            if (CarryOverTaxLosses.Count > 0 || CompensableTaxLosses.Count > 0 || CompensatedTaxLosses.Count > 0 || LossCurrentTaxPeriod.Count > 0 || FormulaTarget.Count > 0)
            {
                var CalculatedAmountCarryOverTaxLosses = (Number(CompensatedTaxLosses, "0") < (decimal)0) ? Sum(Number(CompensableTaxLosses, "0"), Number(CompensatedTaxLosses, "0")) : Sum(Number(CompensableTaxLosses, "0"), Number(LossCurrentTaxPeriod, "0"));
                //CALCULATION HERE
                if (FormulaTarget.Count == 0)
                {
                    FormulaTarget = CreateNewCalculationElement(scenarioId, period, "tax-inc:CarryOverTaxLosses", new string[] { "" }, "EUR", "INF");
                }
                if (FormulaTarget.Count > 1)
                {
                    throw new Exception("More then one target for FormulaTarget in assertion f-corp-1411");
                }
                FormulaTarget.First().Calculated = true;

                FormulaTarget.First().SetNumber((Number(CompensatedTaxLosses, "0") < (decimal)0) ? Sum(Number(CompensableTaxLosses, "0"), Number(CompensatedTaxLosses, "0")) : Sum(Number(CompensableTaxLosses, "0"), Number(LossCurrentTaxPeriod, "0")));

                if (Number(FormulaTarget) == 0) FormulaTarget.First().SetNumber(Sum(Number(CompensableTaxLosses, "0"), Number(LossCurrentTaxPeriod, "0")));

                if (((Number(CarryOverTaxLosses, "0") != (decimal)0) || (Number(CompensableTaxLosses, "0") != (decimal)0) || (Number(CompensatedTaxLosses, "0") != (decimal)0) || (Number(LossCurrentTaxPeriod, "0") != (decimal)0)))
                {
                    //FORMULA HERE
                    bool test = (Number(CarryOverTaxLosses, "0") == Sum(Number(CompensableTaxLosses, "0"), Number(CompensatedTaxLosses, "0"))) || (Number(CarryOverTaxLosses, "0") == Sum(Number(CompensableTaxLosses, "0"), Number(LossCurrentTaxPeriod, "0")));
                    if (!test)
                    {
                        // MESSAGES
                        AddMessage(new BizTaxErrorDataContract
                        {
                            FileId = _fileId,
                            Id = "f-corp-1411",
                            Messages = new List<BizTaxErrorMessageDataContract> { 
 new BizTaxErrorMessageDataContract { Culture = "nl-BE", Message = "Het totaal vermeld in de rubriek '{Verlies over te brengen naar het volgende belastbare tijdperk}' ({$CarryOverTaxLosses}) is niet juist (= {$CalculatedAmountCarryOverTaxLosses})."}  
, new BizTaxErrorMessageDataContract { Culture = "fr-FR", Message = "Le total repris dans la rubrique '{Perte à reporter sur la période imposable suivante}' ({$CarryOverTaxLosses}) n'est pas correct (= {$CalculatedAmountCarryOverTaxLosses})."}  
, new BizTaxErrorMessageDataContract { Culture = "de-DE", Message = "Der in der Rubrik '{Auf den folgenden Besteuerungszeitraum vorzutragender Verlust}' angegebene Gesamtbetrag ({$CarryOverTaxLosses}) ist unzutreffend (= {$CalculatedAmountCarryOverTaxLosses})."}  
, new BizTaxErrorMessageDataContract { Culture = "en-US", Message = "Het totaal vermeld in de rubriek '{Verlies over te brengen naar het volgende belastbare tijdperk}' ({$CarryOverTaxLosses}) is niet juist (= {$CalculatedAmountCarryOverTaxLosses})."}  
 }
                            ,
                            Fields = new List<string>()
                        }
                        );
                    }
                }
            }

        }
    }
}
