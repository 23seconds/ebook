﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.BizTax.Contracts
{
    [DataContract]
    public class CalcResultDataContract
    {
        
        [DataMember]
        public List<XbrlElementDataContract> Elements { get; set; }

        [DataMember]
        public List<BizTaxErrorDataContract> Errors { get; set; }

        /*[DataMember(Order = 9)]
        public Guid? Partner { get; set; }*/
    }
}
