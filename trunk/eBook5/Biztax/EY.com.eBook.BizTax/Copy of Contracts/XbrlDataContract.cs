﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Xml;

namespace EY.com.eBook.BizTax
{
    [DataContract]
    public class XbrlDataContract
    {
        [DataMember]
        public string SchemaRef { get; set; }

        [DataMember]
        public List<XbrlNamespace> NameSpaces { get; set; }
        
        [DataMember]
        public ContextEntityDataContract EntityIdentifier { get; set; } // used in ALL contexts

        [DataMember]
        public List<ContextElementDataContract> Contexts { get; set; }

        [DataMember]
        public List<UnitElementDataContract> Units { get; set; }

        [DataMember]
        public List<XbrlElementDataContract> Elements { get; set; }

    }

    [DataContract]
    public class XbrlIndexDataContract
    {
        [DataMember]
        public Dictionary<string, List<List<int>>> ElementsByName { get; set; }

        [DataMember]
        public Dictionary<string, List<List<int>>> ElementsByNamePrefix { get; set; }

        [DataMember]
        public Dictionary<string, List<List<int>>> ElementsByContext { get; set; }

        [DataMember]
        public Dictionary<string, List<List<int>>> ElementsByPeriodId { get; set; }


        [DataMember]
        public Dictionary<string, int> ContextsById { get; set; }

        [DataMember]
        public Dictionary<string, List<int>> ContextsByPeriod { get; set; }

        [DataMember]
        public Dictionary<string, List<int>> ContextsByDimension { get; set; }

        [DataMember]
        public Dictionary<string, List<int>> ContextsByDimensionValue { get; set; }
    }

    [DataContract]
    public class XbrlNamespace
    {
        [DataMember]
        public string Prefix { get; set; }
        [DataMember]
        public string Uri { get; set; }
    }

}