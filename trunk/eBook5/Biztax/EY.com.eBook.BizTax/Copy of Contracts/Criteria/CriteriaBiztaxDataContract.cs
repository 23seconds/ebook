﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.BizTax.Contracts.Criteria
{
    [DataContract]
    public class CriteriaBiztaxDataContract
    {
        [DataMember]
        public Guid FileId { get; set; }

        [DataMember]
        public string Culture { get; set; }

        [DataMember]
        public string Type { get; set; }
    }
}
