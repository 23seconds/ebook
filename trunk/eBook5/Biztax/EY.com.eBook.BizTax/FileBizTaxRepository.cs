﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EY.com.eBook.Core;
using EY.com.eBook.BizTax.Contracts;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.Core.EF;

namespace EY.com.eBook.BizTax
{
    public class FileBizTaxRepository
    {

        public void CleanPrefix(XbrlElementDataContract current, XbrlElementDataContract parent)
        {
            if(current==null) return;
            if (string.IsNullOrEmpty(current.Prefix) && parent !=null && !string.IsNullOrEmpty(parent.Prefix)) current.Prefix = parent.Prefix;
            if (current.Children != null) current.Children.ForEach(x => CleanPrefix(x, current));
        }

        public static Guid ServiceIdBiztaxAuto = new Guid("BD7C2EAE-8500-4103-8984-0131E73D07FA");

        public EY.com.eBook.BizTax.Contracts.BizTaxDataContract FindProxy(EY.com.eBook.BizTax.Contracts.BizTaxDataContract btdc, Core.EF.Write.File file)
        {
            
            Guid structureId = new Guid("D33CE2B5-FBEB-4D58-9C47-2A4731804173");
            Guid signedStatus = new Guid("FCF7A560-3D74-4341-A32B-310C8FA336CF");

            EY.com.eBook.Core.EF.Read.RepositoryItem ri = eBookReadManager.Context.RepositoryItems
                .FirstOrDefault(r => r.ClientId == file.ClientId && r.Deleted == false
                    && r.StructureId == structureId 
                    && r.StartDate <= file.EndDate && r.EndDate >= file.StartDate 
                    && r.Status==signedStatus);
            if (ri!=null)
            {
              btdc.ProxyId = ri.ItemId;
              btdc.ProxyTitle = ri.Name;
              return btdc;
            }
            btdc.ProxyId = null;
            btdc.ProxyTitle = null;
            return btdc;
        }

    }
}
