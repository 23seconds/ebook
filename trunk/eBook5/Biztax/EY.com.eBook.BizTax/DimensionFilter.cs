﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.BizTax
{
    public class DimensionFilter
    {
        public bool Invert { get; set; }

        public string Dimension { get; set; }

        public List<string> Values { get; set; }
    }
}
