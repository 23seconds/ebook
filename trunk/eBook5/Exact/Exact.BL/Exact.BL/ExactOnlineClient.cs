﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Net;
using System.Web;
using System.Xml;
using System.Xml.Linq;
using System.IO;

namespace Exact.BL
{
    public class ExactOnlineClient
    {
        public static string DictionaryToPostString(Dictionary<string, string> postVariables)
        {
            string postString = "";
            try
            {
                postString = postVariables.Aggregate(postString, (current, pair) => current + (HttpUtility.UrlEncode(pair.Key) + "=" + HttpUtility.UrlEncode(pair.Value) + "&"));
            }
            catch (Exception)
            {

                throw;
            }

            return postString;
        }

        private IWebProxy GetProxy()
        {
            
            IWebProxy proxy = new System.Net.WebProxy("defrceprx01.eurw.ey.net", 8080);
                //"defrceprx01.eurw.ey.net", 8080);

                proxy.Credentials = new System.Net.NetworkCredential("appebook", "EYext123", "eurw");

                //"appebook", "EYext123", "eurw");
                //}
          

            return proxy;
        }

        public void GetDivisions()
        {

            Dictionary<string, string> postParams = new Dictionary<string, string>();
            postParams.Add("_UserName_", "EYAppUser");
            postParams.Add("_Password_", "45-jhFldk*(1i)");
            string postString = DictionaryToPostString(postParams);
            byte[] postBytes = Encoding.UTF8.GetBytes(postString);

            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(new Uri("https://start.exactonline.be/docs/XMLDivisions.aspx"));
            webRequest.Proxy = GetProxy();
            webRequest.Method = "POST";
            webRequest.ContentType = "application/x-www-form-urlencoded";
            webRequest.ContentLength = postBytes.Length;
            webRequest.ReadWriteTimeout = 1000 * 1000;
            webRequest.Timeout = 1000 * 1000;

            Stream postStream = webRequest.GetRequestStream();

            postStream.Write(postBytes, 0, postBytes.Length);
            postStream.Close();
            HttpWebResponse webResponse = (HttpWebResponse)webRequest.GetResponse();

            Stream responseStream = webResponse.GetResponseStream();
            string _cookieHeader = webResponse.Headers["Set-cookie"];
            StreamReader responseStreamReader = new StreamReader(responseStream);
            string content = responseStreamReader.ReadToEnd();
            
            

        }
    }
}
