﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using ExactOnline;
using ExactOnline.Contracts;
using System.Xml.Linq;
using System.Runtime.Serialization;
using System.Xml;
using System.Xml.XPath;
using System.Xml.Serialization;
using System.IO;

namespace ExactOnlineTester
{
    class Program
    {

        static void Main(string[] args)
        {
            try
            {
                Console.WriteLine("UpdateAdministrations");
                UpdateAdministrations();
                Console.WriteLine("DownloadFinancialPeriods");
                //DownloadFinancialPeriods();
                Console.WriteLine("DownloadRelations");
              //  DownloadRelations();
                Console.WriteLine("DownloadBankLinks");
              //  DownloadBankLinks();
            }
            catch (Exception e) {
                Console.WriteLine(e.Message);
                Console.ReadLine();
            }
        }

        static void Main_AdminAccountcheck()
        {
            XElement resultingList = new XElement("ExactAccountDescsByTermId");

            XDocument termsDoc = XDocument.Load(@"C:\Projects_TFS2010\exact\DownloadSchemasExact\bin\Debug\CrossDossierByTermID.xml");

            List<string> termids = termsDoc.Root.Elements("termid").Select(e => e.Attribute("id").Value).Distinct().ToList();

            eBookExactDataContext eedc = new eBookExactDataContext();
            Console.WriteLine("Initializing Exact connection");
            HttpData htd = new HttpData();
            htd.Initialize();
            Console.WriteLine("Traversing administrations based upon previous download (eBook DB)");
            foreach (ExactAdmin ea in eedc.ExactAdmins.ToList())
            {
                XElement xeClient = new XElement("client", new XAttribute("exactCode", ea.ExactAdminCode), new XAttribute("name", ea.ExactAdminName));
                Console.WriteLine("Handling {0} ({1}):", ea.ExactAdminName, ea.ExactAdminCode);
                Console.Write("...downloading account data");
                // Download accountlist
              
                XDocument res = htd.GetGlobalAccounts(ea.ExactAdminCode);
                // save local ?
                if (res != null)
                {
                    Console.Write("...download done...saving locally");
                  //  res.Save(ea.ExactAdminCode + "_GL.xml");

                    Console.Write("...filtering termid's");
                    // List<XElement> els = new List<XElement>();
                    foreach (string termid in termids)
                    {
                        var resfilt = res.Root.XPathSelectElements("//Description[@termid='" + termid + "']");
                        if (resfilt != null && resfilt.Count() > 0)
                        {
                            foreach (XElement fel in resfilt)
                            {
                                xeClient.Add(new XElement("Account", new XAttribute("code", fel.Ancestors("GLAccount").First().Attribute("code").Value)
                                , new XAttribute("termid", fel.Attribute("termid").Value), new XAttribute("description", fel.Value)));
                            }
                            //  els.AddRange(resfilt);
                        }
                    }
                    xeClient.Add(new XAttribute("success", true));

                }
                else
                {
                    xeClient.Add(new XAttribute("success", false));
                }
                resultingList.Add(xeClient);


               
                // filter on known termid issues

                //Description[@termid='']

                Console.Write("...DONE ADMIN");

                Console.WriteLine("-------------------------------");
            }

           // resultingList.Save("resultingTermCheck.xml");

            Console.WriteLine("");
            Console.WriteLine("ALL DONE");
            Console.ReadLine();

        }

        const string AdminstrationDownloadId = "EYAUTOMATION_EBOOK_ADMINISTRATIONS";
        static void UpdateAdministrations()
        {
            Stopwatch sw = new Stopwatch();
            Console.WriteLine("fetching adminstritions detailed info");
            sw.Start();
            HttpData htd = new HttpData();
            htd.Initialize();

           // string[] docs = Directory.GetFiles(@"C:\Users\betgov\Documents\Exact\Administrations_13102014\", "*.xml");

            XDocument docd = htd.GetAdministrations(Guid.NewGuid().ToString());
            Console.WriteLine("Download Done");
          //  Console.ReadLine();
          //  return;
            Console.WriteLine("Processing");
            List<Administrations> lAdmins = new List<Administrations>();
            Administrations admins = DeserializeXmlFromString<Administrations>(docd.Root.Element("Administrations"));
            lAdmins.Add(admins);
          /*  foreach (string d in docs) {
                XDocument doc = XDocument.Load(d);
                Administrations admins = DeserializeXmlFromString<Administrations>(doc.Root.Element("Administrations"));
                lAdmins.Add(admins);
            }
            */

          //  XElement xe = new XElement("ArrayOfAdministration", doc.Root.Element("Administrations").Elements());
            eBookExactDataContext eedc = new eBookExactDataContext();

            
            foreach(Administrations las in lAdmins){
                foreach (Administration admin in las.Administration)
                {
                    Console.WriteLine("administration:" + admin.Name);
                    bool nw = false;
                    ExactAdmin ea = eedc.ExactAdmins.FirstOrDefault(e => e.ExactAdminCode == admin.code);
                    if (ea == null)
                    {
                        nw = true;
                        ea = new ExactAdmin { ExactAdminCode = admin.code };
                    }
                    ea.ExactAdminAddresLine1 = admin.AddressLine1;
                    ea.ExactAdminAddresLine2 = admin.AddressLine2;
                    ea.ExactAdminAddresLine3 = admin.AddressLine3;
                    ea.ExactAdminNr = admin.number.ToString();
                    ea.ExactAdminName = admin.Name;
                    ea.ExactAdminCity = admin.City;
                    ea.ExactAdminZipCode = admin.Postcode;
                    ea.ExactAdminState = admin.State != null ? admin.State.code : null;
                    ea.ExactAdminCountry = admin.Country != null ? admin.Country.code : null;
                    ea.ExactAdminCurrency = admin.Currency != null ? admin.Currency.code : null;
                    ea.ExactAdminVat = admin.VATNumber;
                    if (admin.DivisionClassNames != null && admin.DivisionClassNames.Count(d => d.number == "1") == 1)
                    {
                        DivisionClassName dcn = admin.DivisionClassNames.First(d => d.number == "1");
                        if (dcn.DivisionClasses != null && dcn.DivisionClasses.Length>0)
                        {
                            DivisionClass dc = dcn.DivisionClasses.First();
                            ea.ExactAdminClusterCode = dc.code;
                            ea.ExactAdminClusterTermId = dc.Description !=null ? (int?)dc.Description.termid : null;
                        }
                    }
                    else
                    {
                        ea.ExactAdminClusterCode = null;
                        ea.ExactAdminClusterTermId = null;
                    }

                    if (admin.DigitalPostBoxes != null)
                    {
                        ea.ExactAdminMainPostbox = admin.DigitalPostBoxes.First().mailbox;
                    }

                
                
                    if (nw) eedc.ExactAdmins.InsertOnSubmit(ea);
                }
            }
            eedc.SubmitChanges();

           // htd.GetGlobalAccounts("69255").Save("69255.xml");
            sw.Stop();
            Console.WriteLine("fetch duration: " + sw.Elapsed.ToString());
            /*
            string res = htd.GetDivisions();
            XDocument doc = XDocument.Parse(res);
            
             */
            //doc.Save("administrations.xml");
            Console.WriteLine("done");
            Console.ReadLine();
        }


        static void DownloadFinancialPeriods()
        {
            HttpData htd = new HttpData();
            htd.Initialize();

            eBookExactDataContext eedc = new eBookExactDataContext();
            foreach (ExactAdmin ea in eedc.ExactAdmins.ToList())
            {
                 Console.WriteLine("financial periods of " + ea.ExactAdminName);
                Console.WriteLine("REMOVING ALL FINANCIAL PERIODS");
                eedc.ExactAdminFinYears.DeleteAllOnSubmit(eedc.ExactAdminFinYears.Where(f => f.ExactFinYearAdminCode == ea.ExactAdminCode).ToList());
                ea.ExactAdminHasFinancialPeriods = false;
                eedc.SubmitChanges();

                Console.WriteLine("Downloading info of " + ea.ExactAdminName);
                XDocument doc = htd.GetFinancialYears(ea.ExactAdminCode);
                if (doc != null)
                {
                    Console.WriteLine("Saving local copy of xml");
                   // doc.Save(System.IO.Path.Combine(@"C:\temp\exact\finyears", ea.ExactAdminCode + ".xml"));
                    Console.WriteLine("Update finyears");
                    FinYears fyears = DeserializeXmlFromString<FinYears>(doc.Root.Element("FinYears"));



                    var res = fyears.FinYear.Select(f => new { Number = f.number, StartDate = f.FinPeriod.First().DateStart, EndDate = f.FinPeriod.Last().DateEnd });
                    ea.ExactAdminHasFinancialPeriods = res.Count() > 0;

                    Console.WriteLine("ADDING FINANCIAL PERIODS");
                    res.ToList().ForEach(f =>
                    {
                        eedc.ExactAdminFinYears.InsertOnSubmit(new ExactAdminFinYear
                        {
                            ExactFinYearAdminCode = ea.ExactAdminCode,
                            ExactFinYearNumber = f.Number,
                            ExactFinYearStartDate = f.StartDate,
                            ExactFinYearEndDate = f.EndDate
                        });
                    });

                    eedc.SubmitChanges();
                }
                
                
            }
            Console.WriteLine("DONE FIN YEARS");
        }

        static void DownloadBankLinks()
        {
            // download relations, only for adminstrations WITH digital postbox
            HttpData htd = new HttpData();
            htd.Initialize();

            XElement xeErr = new XElement("Errors");
            DateTime dt = DateTime.Now;
            int thrCnt = 0;

            eBookExactDataContext eedc = new eBookExactDataContext();
            foreach (ExactAdmin ea in eedc.ExactAdmins.ToList())
            {
               Console.WriteLine("Bank accounts of " + ea.ExactAdminName);
                // Throttle max 60/min. (55 to be safe)
               if (thrCnt >= 55 || (DateTime.Now - dt).TotalSeconds > 60)
               {
                   thrCnt = 0;
                   int sleep = 60000; // -(int)(DateTime.Now - dt).TotalMilliseconds;
                   if(sleep <0) sleep  = 0;
                  // sleep += 100;
                   Console.WriteLine("Sleeping " + sleep + " :: throttle");
                   System.Threading.Thread.Sleep(sleep);
                   
                   dt = DateTime.Now;

               }
               try
               {
                   XDocument doc = htd.GetBankLinks(ea.ExactAdminCode);

                   if (doc == null)
                   {
                       Console.WriteLine("NULL RESULT EXACT ?");
                       xeErr.Add(new XElement("error", new XAttribute("admincode", ea.ExactAdminCode), "RESPONSE Exact  NULL"));
                   }
                   else
                   {
                       thrCnt++;

                       Console.WriteLine("REMOVING ALL KNOWN BANK ACCOUNTS");
                       eedc.ExactAdminBanks.DeleteAllOnSubmit(eedc.ExactAdminBanks.Where(f => f.ExactBankAdminCode == ea.ExactAdminCode).ToList());

                       eedc.SubmitChanges();
                       List<XElement> accs = doc.Root.XPathSelectElements("//OwnBankAccount[./IBAN[text()]]").ToList();
                       if (accs.Count > 0)
                       {
                           foreach (XElement acc in accs)
                           {
                               OwnBankAccount oba = DeserializeXmlFromString<OwnBankAccount>(acc);

                               Console.Write("-- BA: " + oba.IBAN);
                               var bea = eedc.ExactAdminBanks.FirstOrDefault(e => e.ExactBankAdminCode == ea.ExactAdminCode && e.ExactBankIBAN == oba.IBAN);
                               if (bea == null)
                               {
                                   eedc.ExactAdminBanks.InsertOnSubmit(new ExactAdminBank
                                   {
                                       ExactBankAdminCode = ea.ExactAdminCode,
                                       ExactBankBIC = oba.BICCode,
                                       ExactBankIBAN = oba.IBAN
                                   });
                                   eedc.SubmitChanges();
                                   Console.Write("  ADDED");
                               }
                               Console.WriteLine("");
                           }
                       }
                       else
                       {
                           Console.WriteLine("-- NO BANK ACCOUNTS");
                       }
                       /* Journals journals = DeserializeXmlFromString<Journals>(doc.Root.Element("Journals"));

                        if (journals != null && journals.Journal != null)
                        {

                            Console.WriteLine("Saving {0} Journals", journals.Journal.Length);

                            foreach (Journal jn in journals.Journal)
                            {
                                if (jn.OwnBankAccount != null)
                                {
                                    var bea = eedc.ExactAdminBanks.FirstOrDefault(e => e.ExactBankAdminCode == ea.ExactAdminCode && e.ExactBankIBAN == jn.OwnBankAccount.IBAN);
                                    if (bea == null)
                                    {
                                        eedc.ExactAdminBanks.InsertOnSubmit(new ExactAdminBank
                                        {
                                            ExactBankAdminCode = ea.ExactAdminCode,
                                            ExactBankBIC = jn.OwnBankAccount.BICCode,
                                            ExactBankIBAN = jn.OwnBankAccount.IBAN
                                        });
                                        eedc.SubmitChanges();
                                    }
                                }

                            }

                        }
                        else
                        {
                            Console.WriteLine("NO journals");
                        }
                        * */
                   }
               }

               catch (Exception e)
               {
                   Console.WriteLine(e.Message);
               }
            }
            xeErr.Save("ExactBankErrrors.xml");

     }

     static void DownloadRelations()
     {
         // download relations, only for adminstrations WITH digital postbox
         HttpData htd = new HttpData();
         htd.Initialize();

         List<string> relationTypes = new List<string>();

         eBookExactDataContext eedc = new eBookExactDataContext();
         foreach (ExactAdmin ea in eedc.ExactAdmins.Where(a => a.ExactAdminMainPostbox != null && a.ExactAdminMainPostbox != "").ToList())
         {
             Console.WriteLine("Downloading info of " + ea.ExactAdminName);
             XDocument doc = htd.GetRelations(ea.ExactAdminCode);
             Console.WriteLine("Saving local copy of xml");
            // doc.Save(System.IO.Path.Combine(@"C:\temp\exact\relations", ea.ExactAdminCode + ".xml"));
             Console.WriteLine("Update relations");
             Accounts relations = DeserializeXmlFromString<Accounts>(doc.Root.Element("Accounts"));

             if (relations != null && relations.Account != null)
             {

                 Console.WriteLine("Saving {0} relations", relations.Account.Length);

                 foreach (typeAccount ta in relations.Account)
                 {
                     Guid id = new Guid(ta.ID);
                     ExactAdminRelation ear = eedc.ExactAdminRelations.FirstOrDefault(r => r.ExactRelationAdminCode == ea.ExactAdminCode && r.ExactRelationId == id);
                     if (ear == null)
                     {
                         ear = new ExactAdminRelation
                         {
                             ExactRelationAdminCode = ea.ExactAdminCode
                             ,
                             ExactRelationId = id
                         };
                         eedc.ExactAdminRelations.InsertOnSubmit(ear);
                     }
                     ear.ExactRelationName = ta.Name;
                     ear.ExactRelationVAT = ta.VATNumber;
                     ear.ExactRelationType = ta.IsSupplier ? 'S' : 'C';
                        
                     if (ta.Address != null && ta.Address.Length > 0)
                     {
                         Address ad = ta.Address.FirstOrDefault(a => a.@default);
                         if (ad == null)
                         {
                             ad = ta.Address.FirstOrDefault();
                         }
                         if (ad != null)
                         {
                             if (ad.Items != null && ad.Items.Length > 0)
                             {
                                 ear.ExactRelationStreetName = string.Join(" ", ad.Items);
                             }
                             ear.ExactRelationCityName = ad.City;
                             ear.ExactRelationPostalCode = ad.PostalCode;
                             if (ad.Country != null)
                             {
                                 ear.ExactRelationCountryID = ad.Country.code;
                             }
                             else
                             {
                                 ear.ExactRelationCountryID = "BE";
                             }
                                
                         }
                     }
                     eedc.SubmitChanges();

                 }
                    
             }
             else
             {
                 Console.WriteLine("NO relations");
             }
         }
         

        }
     

        public static T DeserializeXmlFromString<T>(XElement input)
        {
            T result = default(T);
            XmlSerializer dcs = new XmlSerializer(typeof(T));
            var sr = new System.IO.StringReader(input.ToString());
            var xr = new XmlTextReader(sr);
            result = (T)dcs.Deserialize(xr);
            xr.Close();
            sr.Close();
            return result;
        }

        public static T DeserializeFromString<T>(XElement input)
        {
            T result = default(T);
            DataContractSerializer dcs = new DataContractSerializer(typeof(T));
            var sr = new System.IO.StringReader(input.ToString());
            var xr = new XmlTextReader(sr);
            result = (T)dcs.ReadObject(xr);
            xr.Close();
            sr.Close();
            return result;
        }
    }
}

