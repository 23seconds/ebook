﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using System.Xml.Schema;

namespace ExactOnline.Contracts
{
    /*
      <Name>ORTHEVAL Sprl</Name>
      <AddressLine1>avenue John Kennedy 10a</AddressLine1>
      <Postcode>1330</Postcode>
      <City>Rixensart</City>
      <State code="B-W" />
      <Country code="BE" />
      <Currency code="EUR" />
      <Phone>02/652 32 83</Phone>
      <StartDate>2013-08-23</StartDate>
      <BlockingStatus>0</BlockingStatus>
     */
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public class AdminDetailDataContract
    {
       /* [DataMember,XmlAttribute(AttributeName="code")]
        public int Code { get; set; }

        [DataMember, XmlAttribute(AttributeName = "number")]
        public string Number { get; set; }
        */


       [System.Xml.Serialization.XmlElementAttribute(Form = XmlSchemaForm.Unqualified)]
        public string Name { get; set; }

       [System.Xml.Serialization.XmlElementAttribute(Form = XmlSchemaForm.Unqualified)]
        public string AddressLine1 { get; set; }

       [System.Xml.Serialization.XmlElementAttribute(Form = XmlSchemaForm.Unqualified)]
        public string AddressLine2 { get; set; }

       [System.Xml.Serialization.XmlElementAttribute(Form = XmlSchemaForm.Unqualified)]
        public string AddressLine3 { get; set; }

        [System.Xml.Serialization.XmlElementAttribute(Form = XmlSchemaForm.Unqualified)]
        public string PostCode { get; set; }

        [System.Xml.Serialization.XmlElementAttribute(Form = XmlSchemaForm.Unqualified)]
        public string City { get; set; }

        [System.Xml.Serialization.XmlElementAttribute(Form = XmlSchemaForm.Unqualified)]
        public CodeDataContract State { get; set; }

        [System.Xml.Serialization.XmlElementAttribute(Form = XmlSchemaForm.Unqualified)]
        public CodeDataContract Country { get; set; }

        [System.Xml.Serialization.XmlElementAttribute(Form = XmlSchemaForm.Unqualified)]
        public CodeDataContract Currency { get; set; }

        [System.Xml.Serialization.XmlElementAttribute(Form = XmlSchemaForm.Unqualified)]
        public string Phone { get; set; }

        [System.Xml.Serialization.XmlElementAttribute(Form = XmlSchemaForm.Unqualified)]
        public string Fax { get; set; }

        [System.Xml.Serialization.XmlElementAttribute(Form = XmlSchemaForm.Unqualified)]
        public string Email { get; set; }

        [System.Xml.Serialization.XmlElementAttribute(Form = XmlSchemaForm.Unqualified)]
        public string VATNumber { get; set; }

        [System.Xml.Serialization.XmlElementAttribute(Form = XmlSchemaForm.Unqualified)]
        public string ChamberOfCommerce { get; set; }

        [System.Xml.Serialization.XmlElementAttribute(Form = XmlSchemaForm.Unqualified)]
        public string StartDate { get; set; }

        [DataMember]
        public int BlockingStatus { get; set; }
        
    }
}
