﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace ExactOnline.Contracts
{
    [DataContract]
    public class CodeDataContract
    {
        [DataMember, XmlAttribute(AttributeName="code")]
        public string Code { get; set; }
    }
}
