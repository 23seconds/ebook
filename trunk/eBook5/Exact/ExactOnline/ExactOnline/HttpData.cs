﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Xml;
using System.Xml.Linq;
using ExactOnline.Enum;

namespace ExactOnline
{
    public class HttpData
    {
        private string _host = ConfigurationManager.AppSettings["Exact.Host"];
        //private string user = "nick.wuyts@datamotive.be";
        //private string password = "Enternet10";

        private CookieContainer _CookieContainer = new CookieContainer();
        private string _cookieHeader = string.Empty;

        private bool _initialized = false;

        #region Invoice

        public XDocument PrepareInvoiceMailXml(XDocument mail, XDocument invoice, string fileName, string attachementToInclude, InvoiceType invoiceType, string mailTo)
        {
            string type = ((int)invoiceType).ToString(),  // "1000", // verkoopfactur : 1100..... aankoopfactuur : 1000,  herrinering : 1010
                baseType = "20",
                attachementType = "10";

            var mailMessage = mail.Descendants("MailMessage").First();
            mail.Descendants("Type").First().SetValue(type);

            mail.Descendants("Recipient").First().SetValue(mailTo); 
            mail.Descendants("Sender").First().SetValue("scanstraat@be.ey.com");

            //mailMessage.SetAttributeValue("Type", type);
            var attachments = mail.Descendants("Attachments").First();

            var firstAttachement = GetXElementOfXDocument(fileName, invoice, baseType);
            var secondAttachement = GetXElementOfAttachement(attachementToInclude, attachementType);
            attachments.Add(firstAttachement);
            attachments.Add(secondAttachement);

            return mail;
        }

        public string SendInvoice(XDocument file, string fileName, string attachementToInclude, InvoiceType invoiceType,string mailTo)
        {
            var mailMessagePath = ConfigurationManager.AppSettings["Exact.Mail.MessageTemplate"];
            var mailMessage = XDocument.Load(mailMessagePath);
            mailMessage = PrepareInvoiceMailXml(mailMessage, file, fileName, attachementToInclude, invoiceType,mailTo);
            return Send(mailMessage);
        }

        #endregion

        #region Bank

        public XDocument PrepareBankXml(XDocument mail, string codaFile)
        {
            string type = "2000",  // Bank
                baseType = "70";

            var mailMessage = mail.Descendants("MailMessage").First();
            mail.Descendants("Type").First().SetValue(type);
            //mailMessage.SetAttributeValue("Type", type);
            var attachments = mail.Descendants("Attachments").First();

            var bankAttachement = GetXElementOfAttachement(codaFile, baseType);
            attachments.Add(bankAttachement);

            return mail;
        }

        public string SendCoda(string codaFile)
        {
            var mailMessagePath = ConfigurationManager.AppSettings["Exact.Mail.MessageTemplate"];
            var mailMessage = XDocument.Load(mailMessagePath);
            mailMessage = PrepareBankXml(mailMessage, codaFile);

            return Send(mailMessage);
        }

        #endregion

        public void Initialize()
        {
            GetDivisions();
        }

        public XDocument XDocumentPrepareMailXml(string mailXml, string fileToInclude, string attachementToInclude, UploadType uploadType)
        {
            var mail = XDocument.Load(mailXml);
            return PrepareMailXml(mail, fileToInclude, attachementToInclude, uploadType);
        }

        public XDocument PrepareMailXml(XDocument mail, string fileToInclude, string attachementToInclude, UploadType uploadType)
        {
            string type, baseType;

            if (uploadType == UploadType.Bank)
            {
                type = "2000";
                baseType = "70";
            }
            else
            {
                type = "1000";
                baseType = "20";
            }
            const string attachementType = "10";

            var mailMessage = mail.Descendants("MailMessage").First();
            mailMessage.SetAttributeValue("Type", type);
            var attachments = mail.Descendants("Attachments").First();


            var firstAttachement = GetXElementOfAttachement(fileToInclude, baseType);
            var secondAttachement = GetXElementOfAttachement(attachementToInclude, attachementType);
            attachments.Add(firstAttachement);
            attachments.Add(secondAttachement);

            return mail;
        }

        public string SendByFilePath(XDocument mailXmlToPrepare, string fileToInclude, string secondFileToInclude, UploadType uploadType)
        {
            var mailXml = PrepareMailXml(mailXmlToPrepare, fileToInclude, secondFileToInclude, uploadType);
            return Send(mailXml);
        }

        private IWebProxy GetProxy()
        {
            //return null;
            IWebProxy proxy = WebRequest.GetSystemWebProxy();
            if (ConfigurationManager.AppSettings["Proxy"] == "true")
            {
                // fallback on config settings
                proxy = new System.Net.WebProxy(ConfigurationManager.AppSettings["Proxy.Address"], int.Parse(ConfigurationManager.AppSettings["Proxy.Port"]));
                //"defrceprx01.eurw.ey.net", 8080);

                proxy.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["Proxy.User.Name"], ConfigurationManager.AppSettings["Proxy.User.Pwd"], ConfigurationManager.AppSettings["Proxy.User.Domain"]);

                //"appebook", "EYext123", "eurw");
                //}
            }
           
            return proxy;
        }

        #region Webservices call

        public XDocument GetAdministrations(string downloadid)
        {
            if (!_initialized)
            {
                throw new Exception("Not initialized");
            }
            int pagecounter = 0;
            Stream objStream = null;
            StreamReader objReader = null;
            XDocument result = null;
            string tspaging = null;
            string prevtspaging = null;
            bool ready = false;
            int count = 0;
            int pageSize = 0;
            int errCount = 0;


            while (!ready)
            {
            //    System.Threading.Thread.Sleep(10000);
                pagecounter++;
                Console.WriteLine("page:" + pagecounter);
                Dictionary<string, string> parameters = new Dictionary<string, string>();
                parameters.Add("Topic", "Administrations");
                parameters.Add("PartnerKey", ConfigurationManager.AppSettings["Exact.PartnerKey"]); // "{8e0c49ec-fab0-4401-b25d-2cb83d71e6ab}");
                parameters.Add("_Division_", ConfigurationManager.AppSettings["Exact.MainDivision"]); //"9001"); // EY MAIN DIVISION
                parameters.Add("Params_DownloadID", downloadid);

                if (!string.IsNullOrEmpty(tspaging))
                {
                    parameters.Add("TSPaging", tspaging);
                }

                Uri uri = GetUri(ConfigurationManager.AppSettings["Exact.Pages.XMLDownload"], parameters);

                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
                //request.Headers.Add("Cookie", _cookieHeader);
                request.CookieContainer = _CookieContainer;
                request.Proxy = GetProxy();
                request.Method = "GET";
                request.Timeout = 200 * 60 * 1000;

                HttpWebResponse response = request.GetResponse() as HttpWebResponse;

                StreamReader sr = new StreamReader(response.GetResponseStream(), true);
                string xtext = sr.ReadToEnd();
              //  XmlTextReader reader = new XmlTextReader(response.GetResponseStream());

                XDocument partial = XDocument.Parse(xtext);
            
                /*
                objStream = request.GetResponse().GetResponseStream();
                
                //objReader = new StreamReader(objStream, Encoding.UTF8);
                StringBuilder content = new StringBuilder();
                using (BufferedStream buffer = new BufferedStream(objStream))
                {
                    //buffer.ReadTimeout = 20 * 60 * 1000;
                    using (StreamReader reader = new StreamReader(buffer, Encoding.UTF8))
                    {
                       
                        string res = "";
                        int linecnt = 1;
                        while ((res = reader.ReadLine()) != null)
                        {
                            Console.Write(linecnt + ",");
                            content.Append(res);
                            linecnt++;
                        }
                      // content = reader.ReadToEnd();
                    }
                }

               // string content = objReader.ReadToEnd();
                XDocument partial = XDocument.Parse(content.ToString());
                */
                if (result == null)
                {
                    Console.WriteLine("Administrations count:" + partial.Root.Element("Administrations").Elements().Count());
                    //result = XDocument.Parse(
                    result =  XDocument.Parse(partial.Root.ToString());
                   // var writer = result.CreateWriter();
                    //partial.WriteTo(writer);
                    //result = partial;
                   // partial.Save("adminsp1.xml");
                   // partial.Root.Element("Administrations").Remove();
                   // partial.Save("stripped.xml");
                   
                }
                else
                {
                    if(partial.Root.Element("Administrations").Elements().Count()>0) {
                    result.Root.Element("Administrations").Add(partial.Root.Element("Administrations").Elements());
                    }
                }

                XElement topic = partial.Root.Element("Topics").Element("Topic");
                tspaging = topic.Attribute("ts_d").Value;
                count = int.Parse(topic.Attribute("count").Value);
                pageSize = int.Parse(topic.Attribute("pagesize").Value);
                if (count < pageSize || tspaging == prevtspaging)
                {
                    ready = true;
                }
                else
                {
                    prevtspaging = tspaging;
                    Console.WriteLine("Sleeping for a minute");
                    System.Threading.Thread.Sleep(1000 * 60);
                }
            }
            return result;

        }

        public XDocument GetGlobalAccounts(string division)
        {
            try
            {
                Stream objStream = null;
                StreamReader objReader = null;
                XDocument result = null;
                string tspaging = null;
                bool ready = false;
                int count = 0;
                int pageSize = 0;


                while (!ready)
                {
                    Dictionary<string, string> parameters = new Dictionary<string, string>();
                    parameters.Add("Topic", "GLAccounts");
                    parameters.Add("PartnerKey", ConfigurationManager.AppSettings["Exact.PartnerKey"]); // "{8e0c49ec-fab0-4401-b25d-2cb83d71e6ab}");

                    parameters.Add("_Division_", division);
                    parameters.Add("OldStyle", "1");

                    if (!string.IsNullOrEmpty(tspaging))
                    {
                        parameters.Add("TSPaging", tspaging);
                    }

                    Uri uri = GetUri(ConfigurationManager.AppSettings["Exact.Pages.XMLDownload"], parameters);

                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
                    //request.Headers.Add("Cookie", _cookieHeader);
                    request.CookieContainer = _CookieContainer;
                    //request.KeepAlive = true;
                    //request.ProtocolVersion = HttpVersion.Version10;
                    // request.ServicePoint.ConnectionLimit = 1;
                    request.Proxy = GetProxy();
                    request.Method = "GET";

                    objStream = request.GetResponse().GetResponseStream();
                    objReader = new StreamReader(objStream, Encoding.UTF8);
                    string content = objReader.ReadToEnd();
                    XDocument partial = XDocument.Parse(content);
                    if (result == null)
                    {
                        result = partial;
                    }
                    else
                    {
                        if (partial.Root.Element("GLAccounts").Elements().Count() > 0)
                        {
                            result.Root.Element("GLAccounts").Add(partial.Root.Element("GLAccounts").Elements());
                        }
                    }

                    XElement topic = partial.Root.Element("Topics").Element("Topic");
                    tspaging = topic.Attribute("ts_d").Value;
                    count = int.Parse(topic.Attribute("count").Value);
                    pageSize = int.Parse(topic.Attribute("pagesize").Value);
                    if (count < pageSize)
                    {
                        ready = true;
                    }
                    else
                    {
                        Console.Write("...paged...fetching next page");
                    }
                }
                return result;
            }
            catch (Exception e)
            {
                return null;
            }
        }


        public XDocument GetAdministrationTopic(string topicKey, string rootElement, string division)
        {
            try
            {
                Stream objStream = null;
                StreamReader objReader = null;
                XDocument result = null;
                string tspaging = null;
                bool ready = false;
                int count = 0;
                int pageSize = 0;


                while (!ready)
                {
                    Dictionary<string, string> parameters = new Dictionary<string, string>();
                    parameters.Add("Topic", topicKey);
                    parameters.Add("PartnerKey", ConfigurationManager.AppSettings["Exact.PartnerKey"]); // "{8e0c49ec-fab0-4401-b25d-2cb83d71e6ab}");

                    parameters.Add("_Division_", division);
                    parameters.Add("OldStyle", "1");

                    if (!string.IsNullOrEmpty(tspaging))
                    {
                        parameters.Add("TSPaging", tspaging);
                    }

                    Uri uri = GetUri(ConfigurationManager.AppSettings["Exact.Pages.XMLDownload"], parameters);

                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
                    //request.Headers.Add("Cookie", _cookieHeader);
                    request.CookieContainer = _CookieContainer;
                    //request.KeepAlive = true;
                    //request.ProtocolVersion = HttpVersion.Version10;
                    // request.ServicePoint.ConnectionLimit = 1;
                    request.Proxy = GetProxy();
                    request.Method = "GET";
                    request.Timeout = 1000 * 60 * 60;

                    objStream = request.GetResponse().GetResponseStream();
                    objReader = new StreamReader(objStream, Encoding.UTF8);
                    string content = objReader.ReadToEnd();
                    XDocument partial = XDocument.Parse(content);
                    if (result == null)
                    {
                        result = partial;
                    }
                    else
                    {
                        if (partial.Root.Element(rootElement).Elements().Count() > 0)
                        {
                            result.Root.Element(rootElement).Add(partial.Root.Element(rootElement).Elements());
                        }
                    }

                    XElement topic = partial.Root.Element("Topics").Element("Topic");
                    tspaging = topic.Attribute("ts_d").Value;
                    count = int.Parse(topic.Attribute("count").Value);
                    pageSize = int.Parse(topic.Attribute("pagesize").Value);
                    if (count < pageSize)
                    {
                        ready = true;
                    }
                    else
                    {
                        Console.Write("...paged...fetching next page");
                    }
                }
                return result;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public XDocument GetRelations(string division)
        {
            return GetAdministrationTopic("Accounts", "Accounts", division);
        }
        
        public XDocument GetBankLinks(string division)
        {
          //  return GetAdministrationTopic("BankLink", "BankLink", division);
            return GetAdministrationTopic("Journals", "Journals", division);
        }

        public XDocument GetFinancialYears(string division)
        {
            return GetAdministrationTopic("FinYears", "FinYears", division);
        }


        public XDocument _GetRelations_OLD(string division)
        {
            try
            {
                Stream objStream = null;
                StreamReader objReader = null;
                XDocument result = null;
                string tspaging = null;
                bool ready = false;
                int count = 0;
                int pageSize = 0;


                while (!ready)
                {
                    Dictionary<string, string> parameters = new Dictionary<string, string>();
                    parameters.Add("Topic", "Accounts");
                    parameters.Add("PartnerKey", ConfigurationManager.AppSettings["Exact.PartnerKey"]); // "{8e0c49ec-fab0-4401-b25d-2cb83d71e6ab}");

                    parameters.Add("_Division_", division);
                    parameters.Add("OldStyle", "1");

                    if (!string.IsNullOrEmpty(tspaging))
                    {
                        parameters.Add("TSPaging", tspaging);
                    }

                    Uri uri = GetUri(ConfigurationManager.AppSettings["Exact.Pages.XMLDownload"], parameters);

                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
                    //request.Headers.Add("Cookie", _cookieHeader);
                    request.CookieContainer = _CookieContainer;
                    //request.KeepAlive = true;
                    //request.ProtocolVersion = HttpVersion.Version10;
                    // request.ServicePoint.ConnectionLimit = 1;
                    request.Proxy = GetProxy();
                    request.Method = "GET";

                    objStream = request.GetResponse().GetResponseStream();
                    objReader = new StreamReader(objStream, Encoding.UTF8);
                    string content = objReader.ReadToEnd();
                    XDocument partial = XDocument.Parse(content);
                    if (result == null)
                    {
                        result = partial;
                    }
                    else
                    {
                        if (partial.Root.Element("Accounts").Elements().Count() > 0)
                        {
                            result.Root.Element("Accounts").Add(partial.Root.Element("Accounts").Elements());
                        }
                    }

                    XElement topic = partial.Root.Element("Topics").Element("Topic");
                    tspaging = topic.Attribute("ts_d").Value;
                    count = int.Parse(topic.Attribute("count").Value);
                    pageSize = int.Parse(topic.Attribute("pagesize").Value);
                    if (count < pageSize)
                    {
                        ready = true;
                    }
                    else
                    {
                        Console.Write("...paged...fetching next page");
                    }
                }
                return result;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public string Send(XDocument mailXml)
        {

            Stream objStream = null;
            StreamReader objReader = null;


            try
            {
                //clearSession();
                GetDivisions();

                Dictionary<string, string> parameters = new Dictionary<string, string>();
                //parameters.Add("_Division_", ConfigurationManager.AppSettings["ExactDivision"]);
                //parameters.Add("Params_Year", ConfigurationManager.AppSettings["ExactParamYear"]);
                //parameters.Add("PartnerKey", ConfigurationManager.AppSettings["ExactPartnerKey"]);
                //parameters.Add("output", ConfigurationManager.AppSettings["ExactOutput"]);
                //parameters.Add("Params_HID", documentId);
                Uri uri = GetUri(ConfigurationManager.AppSettings["Exact.Pages.DigitalPostbox"], parameters);



                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
                request.Headers.Add("Cookie", _cookieHeader);
                request.KeepAlive = true;
                request.ProtocolVersion = HttpVersion.Version10;
                request.ServicePoint.ConnectionLimit = 1;
                request.Proxy = GetProxy();
                request.Method = "POST";
                request.AllowWriteStreamBuffering = true;

                var requestStream = request.GetRequestStream();

                ASCIIEncoding encoding = new ASCIIEncoding();
                byte[] mailXmlBytes = encoding.GetBytes(mailXml.ToString());

                requestStream.Write(mailXmlBytes, 0, mailXmlBytes.Length);

                requestStream.Close();

                objStream = request.GetResponse().GetResponseStream();
                objReader = new StreamReader(objStream, Encoding.UTF8);
                string content = objReader.ReadToEnd();
                XDocument serializedContent = XDocument.Parse(content);

                return "Document imported successfully";
            }
            catch (XmlException)
            {
                return "Authentification to ExactOnline failed";

            }
            catch (Exception ex)
            {
                return "Import failed, please contact your local It administrator " + ex.Message + " " + ex.StackTrace + " " + ((ex.InnerException != null) ? ex.InnerException.Message + " " + ex.InnerException.StackTrace : "");
            }
            finally
            {
                if (objStream != null) objStream.Close();
                if (objReader != null) objReader.Close();
                if (HttpContext.Current != null)
                {
                    HttpContext.Current.Session.Clear();
                    HttpContext.Current.Session.Abandon();
                }
            }
        }

        public string GetDivisions()
        {
            string content = "";
            string user = ConfigurationManager.AppSettings["Exact.Login"];
            string password = ConfigurationManager.AppSettings["Exact.Password"];
            try
            {

                Uri uri = GetUri(ConfigurationManager.AppSettings["Exact.Pages.XMLDivisions"], null);
                Dictionary<string, string> postParams = new Dictionary<string, string>();
                postParams.Add("_UserName_", user);
                postParams.Add("_Password_", password);
                string postString = DictionaryToPostString(postParams);
                byte[] postBytes = Encoding.UTF8.GetBytes(postString);

                HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(uri);
                //webRequest.Proxy = WebRequest.GetSystemWebProxy();
                webRequest.Proxy = GetProxy();
                webRequest.Method = "POST";
                webRequest.ContentType = "application/x-www-form-urlencoded";
                webRequest.CookieContainer = _CookieContainer;
                webRequest.ContentLength = postBytes.Length;

                Stream postStream = webRequest.GetRequestStream();

                postStream.Write(postBytes, 0, postBytes.Length);
                postStream.Close();
                HttpWebResponse webResponse = (HttpWebResponse)webRequest.GetResponse();

                Stream responseStream = webResponse.GetResponseStream();
                _cookieHeader = webResponse.Headers["Set-cookie"];
                StreamReader responseStreamReader = new StreamReader(responseStream);
                content = responseStreamReader.ReadToEnd();
                responseStreamReader.Close();
                responseStream.Close();
                _initialized = true;

            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                
            }
            return content;
        }

        #endregion


        #region Help Function

        public XElement GetXElementOfXDocument(string fileName, XDocument xDocument, string fileType)
        {
            byte[] xDocumentBytes = Encoding.UTF8.GetBytes(xDocument.ToString());
            string encodedData =
                Convert.ToBase64String(xDocumentBytes,
                                       Base64FormattingOptions.InsertLineBreaks);

            var fileAttachment = new XElement("Attachment",
                                          new XElement("Name", fileName),
                                          new XElement("Type", fileType),
                                          new XElement("BinaryData", encodedData));
            return fileAttachment;
        }


        public XElement GetXElementOfAttachement(string fileToInclude, string fileType)
        {
            var fileName = new FileInfo(fileToInclude).Name;
            FileStream fs = new FileStream(fileToInclude,
                                 FileMode.Open,
                                 FileAccess.Read);
            byte[] filebytes = new byte[fs.Length];
            fs.Read(filebytes, 0, Convert.ToInt32(fs.Length));
            string encodedData =
                Convert.ToBase64String(filebytes,
                                       Base64FormattingOptions.InsertLineBreaks);

            var fileAttachment = new XElement("Attachment",
                                          new XElement("Name", fileName),
                                          new XElement("Type", fileType),
                                          new XElement("BinaryData", encodedData));
            return fileAttachment;
        }

        private Uri GetUri(string page, Dictionary<string, string> parameters)
        {
            try
            {
                StringBuilder urlString = new StringBuilder(_host).Append("/").Append(page)
               .Append("?");

                return GetUri(urlString, parameters);
            }
            catch (Exception)
            {

                throw;
            }


        }

        private Uri GetUri(string page, string topic, Dictionary<string, string> parameters)
        {
            try
            {
                StringBuilder urlString = new StringBuilder(_host).Append(page)
               .Append("?Topic=").Append(topic);

                return GetUri(urlString, parameters);
            }
            catch (Exception)
            {

                throw;
            }


        }

        private Uri GetUri(StringBuilder urlString, Dictionary<string, string> parameters)
        {
            Uri uri = null;

            try
            {
                if (parameters != null)
                {


                    urlString.Append(string.Join("&", parameters.Select(p => p.Key + "=" + HttpUtility.UrlEncode(p.Value)).ToArray()));
                }

               // string url = HttpUtility.UrlEncode(urlString.ToString());
                uri = new Uri(urlString.ToString());
            }
            catch (Exception)
            {

                throw;
            }

            return uri;
        }

        public static string DictionaryToPostString(Dictionary<string, string> postVariables)
        {
            string postString = "";
            try
            {
                postString = postVariables.Aggregate(postString, (current, pair) => current + (HttpUtility.UrlEncode(pair.Key) + "=" + HttpUtility.UrlEncode(pair.Value) + "&"));
            }
            catch (Exception)
            {

                throw;
            }

            return postString;
        }

        #endregion

    }
}
