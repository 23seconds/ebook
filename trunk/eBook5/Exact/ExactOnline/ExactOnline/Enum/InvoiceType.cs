﻿namespace ExactOnline.Enum
{
    public enum InvoiceType
    {
        Verkoopfactuur = 1100,
        Aankoopfactuur = 1000,
        Herrinering = 1010
    }
}