﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EY.com.eBook.Core;
using EY.com.eBook.API.Contracts.Data;
using System.Data;
using System.Transactions;
using EY.com.eBook.Core.EF;
using System.Collections;
using LinqToExcel;
using System.IO;
using EY.com.eBook.Excel.BL.Translators;
using LinqToExcel.Query;
using EY.com.eBook.API.Excel.Data;
using System.Text.RegularExpressions;
using System.Configuration;
using EY.com.eBook.API.Contracts.Data.Logic;

namespace EY.com.eBook.Excel.BL
{
    /// <summary>
    /// ExcelRepository
    /// </summary>
    public class ExcelRepository : RepositoryBase
    {
        // public string Path { get; set; }

        #region Read Excel

        public ExcelInfoDataContract ExcelGetBasicInfo(CriteriaExcelDataContract cedc)
        {
            string path = Path.Combine(Config.UploadFolder, cedc.FileName);
            EY.com.eBook.Excel.BL.ExcelRepository erep = new EY.com.eBook.Excel.BL.ExcelRepository();
            ExcelInfoDataContract esdc = erep.GetExcelBasicInfo(path);
            return esdc;
        }

        public ResponseDataContract<List<ExcelGridRowDataContract>> ExcelGetSheetData(CriteriaExcelSheetDataContract cesdc)
        {
            string path = Path.Combine(Config.UploadFolder, cesdc.FileName);
            EY.com.eBook.Excel.BL.ExcelRepository erep = new EY.com.eBook.Excel.BL.ExcelRepository();
            ExcelSheetDataContract esdc = erep.GetSheet(path, cesdc.SheetName);

            if (esdc == null || esdc.Rows == null || cesdc == null || cesdc.Start == null || cesdc.Limit == null)
            {
                return new ResponseDataContract<List<ExcelGridRowDataContract>>
                {
                    Data = null,
                    Success = false,
                    Total = 0,
                    ServiceError = new ServiceErrorDataContract
                    {
                        Message = "NO DATA FOUND, REVIEW YOUR SETTINGS/EXCEL"
                    }
                };
            }
            return new ResponseDataContract<List<ExcelGridRowDataContract>>
            {
                Data = esdc.Rows.Skip(cesdc.Start.Value).Take(cesdc.Limit.Value).ToList()
                ,
                Success = true
                ,
                Total = esdc.Rows.Count
            };

        }

        public ResponseDataContract<List<ExcelAccountImportDataContract>> ExcelGetAccountSchema(CriteriaExcelSheetMappedDataContract cesmdc)
        {
            string path = Path.Combine(Config.UploadFolder, cesmdc.FileName);
            EY.com.eBook.Excel.BL.ExcelRepository erep = new EY.com.eBook.Excel.BL.ExcelRepository();
            List<ExcelAccountImportDataContract> list = erep.GetAccountSchema(path, cesmdc.SheetName, cesmdc.Mapping, false);

            return new ResponseDataContract<List<ExcelAccountImportDataContract>>
            {
                Data = list.Skip(cesmdc.Start.Value).Take(cesmdc.Limit.Value).ToList()
                ,
                Success = true
                ,
                Total = list.Count
            };

        }

        public ResponseDataContract<List<ExcelBalansImportDataContract>> ExcelGetBalans(CriteriaExcelSheetMappedDataContract cesmdc)
        {
            string path = Path.Combine(Config.UploadFolder, cesmdc.FileName);
            EY.com.eBook.Excel.BL.ExcelRepository erep = new EY.com.eBook.Excel.BL.ExcelRepository();
            List<ExcelBalansImportDataContract> list = erep.GetBalans(path, cesmdc.SheetName, cesmdc.Mapping, false);

            return new ResponseDataContract<List<ExcelBalansImportDataContract>>
            {
                Data = list.Skip(cesmdc.Start.Value).Take(cesmdc.Limit.Value).ToList()
                ,
                Success = true
                ,
                Total = list.Count
            };

        }


        private string[] GetWorksheetNames(ExcelQueryFactory eqf)
        {
            List<string> names = eqf.GetWorksheetNames().ToList();
            List<string> resultingNames = new List<string>();
            foreach (string name in names)
            {
                if (!name.EndsWith("Print_Titles")) resultingNames.Add(name);
            }
            return resultingNames.ToArray();
        }

        private bool TestSheetAccess(string sheet, ExcelQueryFactory eqf)
        {
            bool success = false;
            try
            {
                eqf.WorksheetNoHeader(sheet).FirstOrDefault();
                success = true;
            }
            catch { }
            return success;
        }

        
        public ExcelInfoDataContract GetExcelBasicInfo(string path)
        {
            ExcelQueryFactory eqf = new ExcelQueryFactory(path);
            ExcelInfoDataContract ret = new ExcelInfoDataContract
            {
                FileName = Path.GetFileName(path)
                ,
                SheetNames = eqf.GetWorksheetNames().ToList()//GetWorksheetNames(eqf)
            };
            string[] sheetNames = new string[ret.SheetNames.Count];
            ret.SheetNames.CopyTo(sheetNames);

            foreach (string sheet in sheetNames)
            {
                if (!TestSheetAccess(sheet, eqf))
                {
                    ret.SheetNames.Remove(sheet);
                }
            }
            return ret;
        }

        public ExcelInfoDataContract GetExcelInfo(string path)
        {
            ExcelQueryFactory eqf = new ExcelQueryFactory(path);
            ExcelInfoDataContract ret = new ExcelInfoDataContract
            {
                FileName = Path.GetFileName(path)
                ,
                SheetNames = eqf.GetWorksheetNames().ToList()//GetWorksheetNames(eqf)
                ,
                Sheets = new List<ExcelSheetDataContract>()
            };
            string[] sheetNames = new string[ret.SheetNames.Count];
            ret.SheetNames.CopyTo(sheetNames);

            foreach (string sheet in sheetNames)
            {
                if (TestSheetAccess(sheet, eqf))
                {
                    ret.Sheets.Add(ReadExcelSheet(eqf, sheet, 20, false));
                }
                else
                {
                    ret.SheetNames.Remove(sheet);
                }
            }
            return ret;
        }

        public ExcelSheetDataContract GetSheet(string path,string sheetName)
        {
            ExcelQueryFactory eqf = new ExcelQueryFactory(path);
            if (TestSheetAccess(sheetName, eqf))
            {
                return ReadExcelSheet(eqf, sheetName);
            }
            return null;
        }

        private ExcelSheetDataContract ReadExcelSheet(ExcelQueryFactory eqf, string sheet)
        {
            ExcelSheetDataContract esdc = new ExcelSheetDataContract
            {
                Name = sheet
                ,
                Rows = new List<EY.com.eBook.API.Excel.Data.ExcelGridRowDataContract>()
                ,
                ColumnNames = new string[] {}
            };
            ExcelQueryable<RowNoHeader> rowsNhQry = eqf.WorksheetNoHeader(sheet);
             esdc.Rows = Translator.TranslateList<ExcelGridRowDataContract, RowNoHeader>(rowsNhQry.ToList());
            
            return esdc;
        }

        private List<RowNoHeader> GetRowsNoHeader(ExcelQueryFactory eqf, string sheet)
        {
            return eqf.WorksheetNoHeader(sheet).ToList();
        }

        private List<Row> GetRowsFirstHeader(ExcelQueryFactory eqf, string sheet)
        {
            return eqf.Worksheet(sheet).ToList();
        }

        public List<ExcelAccountImportDataContract> GetAccountSchema(string path, string sheetName, List<ExcelMappingDataContract> mappings, bool firstIsHeader)
        {
            ExcelQueryFactory eqf = new ExcelQueryFactory(path);
            if (TestSheetAccess(sheetName, eqf))
            {
                return ReadAccountSchema(eqf,mappings, sheetName,firstIsHeader);
            }
            return new List<ExcelAccountImportDataContract>();
        }

        public List<ExcelBalansImportDataContract> GetBalans(string path, string sheetName, List<ExcelMappingDataContract> mappings, bool firstIsHeader)
        {
            ExcelQueryFactory eqf = new ExcelQueryFactory(path);
            if (TestSheetAccess(sheetName, eqf))
            {
                return ReadBalans(eqf, mappings, sheetName, firstIsHeader);
            }
            return new List<ExcelBalansImportDataContract>();
        }

        private List<ExcelAccountImportDataContract> ReadAccountSchema(ExcelQueryFactory eqf,List<ExcelMappingDataContract> mappings, string sheet, bool firstIsHeader)
        {
            List<char> cols = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".ToCharArray().ToList();
            // temporary:
            firstIsHeader=true;

            List<ExcelAccountImportDataContract> list = new List<ExcelAccountImportDataContract>();


            ExcelMappingDataContract anmdc = mappings.Where(m => m.Name.ToLower() == "accountnr").First();
            ExcelMappingDataContract admdc = mappings.Where(m => m.Name.ToLower() == "accountdescription").First();
            
            int anIdx = cols.IndexOf(anmdc.Column);
            int adIdx = cols.IndexOf(admdc.Column);

            if (firstIsHeader)
            {

                foreach (RowNoHeader row in eqf.WorksheetNoHeader(sheet).ToList())
                {
                    if (!string.IsNullOrEmpty(row[anIdx].Value.ToString()) && !string.IsNullOrEmpty(row[adIdx].Value.ToString()))
                    {
                        if (Regex.IsMatch(row[anIdx].Value.ToString(), @"^-?\d*[0-9]?(|.\d*[0-9]|,\d*[0-9])?$"))
                        {
                            if (anIdx == adIdx)
                            {
                                string val = row[anIdx].ToString();
                                string[] vals = val.Split(new char[] { ' ' });
                                list.Add(new ExcelAccountImportDataContract
                                {
                                    AccountNr = vals[0]
                                    ,
                                    AccountDescription = string.Join(" ", vals.Skip(1).ToArray())
                                });
                            }
                            else
                            {
                                list.Add(new ExcelAccountImportDataContract
                                {
                                    AccountNr = row[anIdx].ToString()
                                    ,
                                    AccountDescription = row[adIdx].ToString()
                                });
                            }
                        }

                    }
                }
            }
            else
            {


                foreach (Row row in eqf.Worksheet(sheet).ToList())
                {
                    if (!string.IsNullOrEmpty(row[anIdx].Value.ToString()) && !string.IsNullOrEmpty(row[adIdx].Value.ToString()))
                    {
                        if (Regex.IsMatch(row[anIdx].Value.ToString(), @"^-?\d*[0-9]?(|.\d*[0-9]|,\d*[0-9])?$"))
                        {
                            if (anIdx == adIdx)
                            {
                                string val = row[anIdx].ToString();
                                string[] vals = val.Split(new char[] { ' ' });
                                list.Add(new ExcelAccountImportDataContract
                                {
                                    AccountNr = vals[0]
                                    ,
                                    AccountDescription = string.Join(" ", vals.Skip(1).ToArray())
                                });
                            }
                            else
                            {
                                list.Add(new ExcelAccountImportDataContract
                                {
                                    AccountNr = row[anIdx].ToString()
                                    ,
                                    AccountDescription = row[adIdx].ToString()
                                });
                            }
                        }

                    }
                }
            }
            return list.OrderBy(a=>a.AccountNr).ToList();
        }

        private bool ValidRow<T>(T row,int[] indexes) where T : List<Cell>
        {
            foreach (int idx in indexes)
            {
                if (idx > -1)
                {
                    if (string.IsNullOrEmpty(row[idx].Value.ToString())) return false;
                }
            }
            return true;
        }

        private bool IsNumeric(string value)
        {
            return Regex.IsMatch(value, @"^-?\d*[0-9]?(|.\d*[0-9]|,\d*[0-9])?$");
        }

        private List<ExcelBalansImportDataContract> ReadBalans(ExcelQueryFactory eqf, List<ExcelMappingDataContract> mappings, string sheet, bool firstIsHeader)
        {
            List<char> cols = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".ToCharArray().ToList();
            // temporary:
            firstIsHeader = true;

            List<ExcelBalansImportDataContract> list = new List<ExcelBalansImportDataContract>();


            ExcelMappingDataContract anmdc = mappings.Where(m => m.Name.ToLower() == "accountnr").First();
            ExcelMappingDataContract admdc = mappings.Where(m => m.Name.ToLower() == "accountdescription").First();

            ExcelMappingDataContract saldoMdc = mappings.Where(m => m.Name.ToLower() == "saldo").FirstOrDefault();
            ExcelMappingDataContract debetMdc = mappings.Where(m => m.Name.ToLower() == "debet").FirstOrDefault();
            ExcelMappingDataContract creditMdc = mappings.Where(m => m.Name.ToLower() == "credit").FirstOrDefault();

            if (saldoMdc == null && debetMdc == null && creditMdc == null)
            {
                return new List<ExcelBalansImportDataContract>();
            }
            

            int anIdx = cols.IndexOf(anmdc.Column);
            int adIdx = cols.IndexOf(admdc.Column);
            int salIdx = saldoMdc!=null ? cols.IndexOf(saldoMdc.Column):-1;
            int debIdx = debetMdc != null ? cols.IndexOf(debetMdc.Column) : -1;
            int creIdx = creditMdc != null ? cols.IndexOf(creditMdc.Column) : -1;
            int[] indexes = new int[] { anIdx, adIdx, salIdx, debIdx, creIdx };

            if (firstIsHeader)
            {

                foreach (RowNoHeader row in eqf.WorksheetNoHeader(sheet).ToList())
                {
                    if (ValidRow(row,indexes) && IsNumeric(row[anIdx].Value.ToString()))
                    {
                        ExcelBalansImportDataContract ebidc = new ExcelBalansImportDataContract();
                        ebidc.AccountNr = row[anIdx].Value.ToString();
                        ebidc.AccountDescription = row[adIdx].Value.ToString();

                        if(anIdx == adIdx) {
                            ebidc.AccountNr = ebidc.AccountNr.Split(new char[] {' '})[0];
                            ebidc.AccountDescription = string.Join(" ",ebidc.AccountDescription.Split(new char[] {' '}).Skip(1).ToArray());
                        }

                        decimal saldi = 0;
                        if (salIdx > -1)
                        {
                            try
                            {
                                saldi = row[salIdx].Cast<decimal>();
                            }
                            catch { }
                        }
                        else
                        {
                            decimal dDebet = 0;
                            decimal dCredit = 0;
                            try
                            {
                                dDebet = row[debIdx].Cast<decimal>();
                            }
                            catch { }
                            try
                            {
                                dCredit = row[creIdx].Cast<decimal>();
                            }
                            catch { }
                            saldi = Math.Abs(dDebet) - Math.Abs(dCredit);
                        }
                        //if (saldi != 0)
                        //{
                            ebidc.Saldo = saldi;
                            list.Add(ebidc);
                        //}

                    }
                }
            }
            else
            {
                foreach (Row row in eqf.Worksheet(sheet).ToList())
                {
                    if (ValidRow(row, indexes) && IsNumeric(row[anIdx].Value.ToString()))
                    {
                        ExcelBalansImportDataContract ebidc = new ExcelBalansImportDataContract();
                        ebidc.AccountNr = row[anIdx].Value.ToString();
                        ebidc.AccountDescription = row[adIdx].Value.ToString();

                        if (anIdx == adIdx)
                        {
                            ebidc.AccountNr = ebidc.AccountNr.Split(new char[] { ' ' })[0];
                            ebidc.AccountDescription = string.Join(" ", ebidc.AccountDescription.Split(new char[] { ' ' }).Skip(1).ToArray());
                        }

                        decimal saldi = 0;
                        if (salIdx > -1)
                        {
                            try
                            {
                                saldi = row[salIdx].Cast<decimal>();
                            }
                            catch { }
                        }
                        else
                        {
                            decimal dDebet = 0;
                            decimal dCredit = 0;
                            try
                            {
                                dDebet = row[debIdx].Cast<decimal>();
                            }
                            catch { }
                            try
                            {
                                dCredit = row[creIdx].Cast<decimal>();
                            }
                            catch { }
                            saldi = Math.Abs(dDebet) - Math.Abs(dCredit);
                        }
                        //if (saldi != 0)
                        //{
                            ebidc.Saldo = saldi;
                            list.Add(ebidc);
                        //}

                    }
                }
            }
            return list.OrderBy(a => a.AccountNr).ToList();
        }

        private ExcelSheetDataContract ReadExcelSheet(ExcelQueryFactory eqf, string sheet, int rows, bool firstRowIsHeader)
        {
            ExcelSheetDataContract esdc = new ExcelSheetDataContract
            {
                Name = sheet
                ,
                Rows = new List<ExcelGridRowDataContract>()
                ,
                ColumnNames = eqf.GetColumnNames(sheet).ToArray()
            };
            if (firstRowIsHeader)
            {
                ExcelQueryable<Row> rowsQry = eqf.Worksheet(sheet);
                esdc.Rows = Translator.TranslateList<ExcelGridRowDataContract, Row>(rows > 0 ? rowsQry.Take(rows).ToList() : rowsQry.ToList());
            }
            else
            {
                ExcelQueryable<RowNoHeader> rowsNhQry = eqf.WorksheetNoHeader(sheet);
                esdc.Rows = Translator.TranslateList<ExcelGridRowDataContract, RowNoHeader>(rows > 0 ? rowsNhQry.Take(rows).ToList() : rowsNhQry.ToList());
            }
            return esdc;
        }

        #endregion

        #region Write Excel
        /* WRITE EXCEL FILES
         * What about all different layouts? 
         * Some sort of layout config class?
         * Together with translation classes for item to "write excel datacontract"
        */

        #endregion

        protected override void RegisterTranslators()
        {
            Translator.RegisterEntityTranslator(new ExcelRowTranslator());
            Translator.RegisterEntityTranslator(new ExcelRowNoHeaderTranslator());
            Translator.RegisterEntityTranslator(new ExcelGridRowTranslator());
            Translator.RegisterEntityTranslator(new ExcelGridRowNoHeaderTranslator());
        }
    }
}