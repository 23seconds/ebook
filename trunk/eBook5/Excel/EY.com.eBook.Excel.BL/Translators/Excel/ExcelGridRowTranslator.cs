﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EY.com.eBook.Core.EntityTranslation;
using EY.com.eBook.Core.EF;
using EY.com.eBook.API.Contracts.Data;
using LinqToExcel;
using EY.com.eBook.API.Excel.Data;
using System.Reflection;

namespace EY.com.eBook.Excel.BL.Translators
{
    /// <summary>
    /// ExcelGridRowTranslator
    /// </summary>
    public class ExcelGridRowTranslator : EntityMapperTranslator<Row, ExcelGridRowDataContract>
    {
        /// <summary>
        /// Translates the business object to the service object.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <param name="value">The value.</param>
        /// <param name="levelsToTranslate">The levels to translate.</param>
        /// <returns></returns>
        protected override ExcelGridRowDataContract BusinessToService(IEntityTranslatorService service, Row value, TranslateLevel levelsToTranslate)
        {
            using (new InternalTranslators(service, new List<IEntityTranslator> { }))
            {
                var erdc = new ExcelGridRowDataContract();

                char[] alfabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".ToCharArray();

                int mx = value.Count > 24 ? 24 : value.Count;
                for (int i = 0; i < mx; i++)
                {
                    PropertyInfo propInfo = erdc.GetType().GetProperty(alfabet[i].ToString());
                    if (propInfo != null)
                    {
                        propInfo.SetValue(erdc, value[i].Value, null);
                    }
                }
               /* foreach (Cell cell in value)
                {
                    erdc.Cells.Add(cell.Value);
                }*/
                return erdc;
            }
            

        }

        /// <summary>
        /// Translates the service object to the business object.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <param name="value">The value.</param>
        /// <param name="levelsToTranslate">The levels to translate.</param>
        /// <returns></returns>
        protected override Row ServiceToBusiness(IEntityTranslatorService service, ExcelGridRowDataContract value, TranslateLevel levelsToTranslate)
        {
            throw new NotImplementedException();
        }

    }
}
