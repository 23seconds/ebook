﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EY.com.eBook.Core.EntityTranslation;
using EY.com.eBook.Core.EF;
using EY.com.eBook.API.Contracts.Data;
using LinqToExcel;

namespace EY.com.eBook.Excel.BL.Translators
{
    /// <summary>
    /// ExcelRowTranslator
    /// </summary>
    public class ExcelRowTranslator : EntityMapperTranslator<Row, ExcelRowDataContract>
    {
        /// <summary>
        /// Translates the business object to the service object.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <param name="value">The value.</param>
        /// <param name="levelsToTranslate">The levels to translate.</param>
        /// <returns></returns>
        protected override ExcelRowDataContract BusinessToService(IEntityTranslatorService service, Row value, TranslateLevel levelsToTranslate)
        {
            using (new InternalTranslators(service, new List<IEntityTranslator> { }))
            {
                var erdc = new ExcelRowDataContract
                {
                    Cells = new List<object>()
                };

                foreach (Cell cell in value)
                {
                    erdc.Cells.Add(cell.Value);
                }
                return erdc;
            }
            

        }

        /// <summary>
        /// Translates the service object to the business object.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <param name="value">The value.</param>
        /// <param name="levelsToTranslate">The levels to translate.</param>
        /// <returns></returns>
        protected override Row ServiceToBusiness(IEntityTranslatorService service, ExcelRowDataContract value, TranslateLevel levelsToTranslate)
        {
            throw new NotImplementedException();
        }

    }
}
