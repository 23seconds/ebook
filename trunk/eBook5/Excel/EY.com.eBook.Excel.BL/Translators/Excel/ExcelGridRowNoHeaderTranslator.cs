﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EY.com.eBook.Core.EntityTranslation;
using EY.com.eBook.Core.EF;
using EY.com.eBook.API.Contracts.Data;
using LinqToExcel;
using EY.com.eBook.API.Excel.Data;
using System.Reflection;

namespace EY.com.eBook.Excel.BL.Translators
{
    /// <summary>
    /// ExcelGridRowNoHeaderTranslator
    /// </summary>
    public class ExcelGridRowNoHeaderTranslator : EntityMapperTranslator<RowNoHeader, ExcelGridRowDataContract>
    {
        /// <summary>
        /// Translates the business object to the service object.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <param name="value">The value.</param>
        /// <param name="levelsToTranslate">The levels to translate.</param>
        /// <returns></returns>
        protected override ExcelGridRowDataContract BusinessToService(IEntityTranslatorService service, RowNoHeader value, TranslateLevel levelsToTranslate)
        {
            using (new InternalTranslators(service, new List<IEntityTranslator> { }))
            {
                var erdc = new ExcelGridRowDataContract();

                //char[] alfabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".ToCharArray();




                /*
                for (int i = 0; i < alfabet.Length; i++)
                {
                    PropertyInfo propInfo = erdc.GetType().GetProperty(alfabet[i].ToString());
                    if (propInfo != null)
                    {
                        propInfo.SetValue(erdc, "", null);
                    }
                }*/
                int mx = value.Count > 24 ? 24 : value.Count;
                for (int i = 0; i < mx; i++)
                {
                    PropertyInfo propInfo = erdc.GetType().GetProperty(GetColumnName(i));
                    if (propInfo != null && value[i].Value.GetType()!=typeof(System.DBNull))
                    {
                        propInfo.SetValue(erdc, value[i].Value, null);
                    }
                    
                }
               /* foreach (Cell cell in value)
                {
                    erdc.Cells.Add(cell.Value);
                }*/
                return erdc;
            }
            

        }

        private string GetColumnName(int index)
        {
            char[] alfabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".ToCharArray();

            if (index > 24)
            {
                decimal d = (index / 24);
                int firstLetter = Convert.ToInt32(Math.Floor(d));
                int secondLetter = index - (24 * firstLetter);
                return string.Format("{0}{1}", alfabet[firstLetter].ToString(), alfabet[secondLetter].ToString());
            }

            return alfabet[index].ToString();
        }

        /// <summary>
        /// Translates the service object to the business object.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <param name="value">The value.</param>
        /// <param name="levelsToTranslate">The levels to translate.</param>
        /// <returns></returns>
        protected override RowNoHeader ServiceToBusiness(IEntityTranslatorService service, ExcelGridRowDataContract value, TranslateLevel levelsToTranslate)
        {
            throw new NotImplementedException();
        }

    }
}

