﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    /// <summary>
    /// ExcelBalansImportDataContract
    /// </summary>
    [DataContract]
    public class ExcelBalansImportDataContract:ExcelAccountImportDataContract
    {
        [DataMember]
        public decimal Saldo { get; set; }
    }
}
