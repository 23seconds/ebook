﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using EY.com.eBook.API.Excel.Data;

namespace EY.com.eBook.API.Contracts.Data
{
    /// <summary>
    /// ExcelSheetDataContract
    /// </summary>
    [DataContract]
    public class ExcelSheetDataContract
    {
        /// <summary>
        /// Name
        /// </summary>
        /// <value>Name</value>
        [DataMember(Name = "n")]
        public string Name { get; set; }

        /// <summary>
        /// Rows
        /// </summary>
        /// <value>Rows</value>
        [DataMember(Name = "rs")]
        public List<ExcelGridRowDataContract> Rows { get; set; }

        /// <summary>
        /// Column Names
        /// </summary>
        /// <value>Column Names</value>
        [DataMember(Name = "cns")]
        public string[] ColumnNames { get; set; }
    }
}
