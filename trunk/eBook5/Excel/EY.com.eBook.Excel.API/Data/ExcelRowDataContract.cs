﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    /// <summary>
    /// ExcelRowDataContract
    /// </summary>
    [DataContract]
    public class ExcelRowDataContract
    {
        /// <summary>
        /// Cells
        /// </summary>
        /// <value>Cells</value>
        [DataMember(Name = "cs")]
        public List<object> Cells { get; set; }
    }
}
