﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    /// <summary>
    /// ExcelMappingDataContract
    /// </summary>
    [DataContract]
    public class ExcelMappingDataContract
    {
        [DataMember(Name="name")]
        public string Name { get; set; }

        [DataMember(Name="column")]
        public char Column { get; set; }
    }
}
