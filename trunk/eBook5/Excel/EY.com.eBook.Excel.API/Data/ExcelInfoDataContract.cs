﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    /// <summary>
    /// ExcelInfoDataContract
    /// </summary>
    [DataContract]
    public class ExcelInfoDataContract
    {
        /// <summary>
        /// FileName
        /// </summary>
        /// <value>FileName</value>
        [DataMember(Name = "fn")]
        public string FileName { get; set; }

        /// <summary>
        /// Sheet Names
        /// </summary>
        /// <value>Sheet Names</value>
        [DataMember(Name = "sns")]
        public List<string> SheetNames { get; set; }

        /// <summary>
        /// Sheets
        /// </summary>
        /// <value>Sheets</value>
        [DataMember(Name = "s")]
        public List<ExcelSheetDataContract> Sheets { get; set; }
        
    }
}
