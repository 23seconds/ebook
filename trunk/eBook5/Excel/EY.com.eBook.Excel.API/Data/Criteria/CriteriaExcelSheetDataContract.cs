﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    /// <summary>
    /// CriteriaExcelSheetDataContract
    /// </summary>
    [DataContract]
    public class CriteriaExcelSheetDataContract
    {
        [DataMember]
        public string FileName { get; set; }

        [DataMember]
        public string SheetName { get; set; }

        /// <summary>
        /// Limit for paging
        /// </summary>
        /// <value>Limit.</value>
        [DataMember]
        public int? Limit { get; set; }

        /// <summary>
        /// Start from... (for paging)
        /// </summary>
        /// <value>Start.</value>
        [DataMember]
        public int? Start { get; set; }  
    }
}

