﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    /// <summary>
    /// CriteriaExcelDataContract
    /// </summary>
    [DataContract]
    public class CriteriaExcelDataContract
    {
        [DataMember]
        public string FileName { get; set; }
    }
}
