﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Contracts.Data
{
    /// <summary>
    /// CriteriaExcelSheetMappedDataContract
    /// </summary>
    [DataContract]
    public class CriteriaExcelSheetMappedDataContract
    {
        [DataMember]
        public Guid FileId { get; set; }


        [DataMember]
        public string FileName { get; set; }

        [DataMember]
        public string SheetName { get; set; }

        /// <summary>
        /// Limit for paging
        /// </summary>
        /// <value>Limit.</value>
        [DataMember]
        public int? Limit { get; set; }

        /// <summary>
        /// Start from... (for paging)
        /// </summary>
        /// <value>Start.</value>
        [DataMember]
        public int? Start { get; set; }

        [DataMember]
        public string ImportType { get; set; }

        [DataMember]
        public string Culture { get; set; }

        [DataMember]
        public List<ExcelMappingDataContract> Mapping { get; set; }

        [DataMember]
        public bool active { get; set; }

        [DataMember]
        public string txt { get; set; }

    }
}

