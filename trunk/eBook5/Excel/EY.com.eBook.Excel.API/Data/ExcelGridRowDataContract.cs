﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace EY.com.eBook.API.Excel.Data
{
    public class ExcelGridRowDataContract
    {
        [DataMember]
        public object A { get; set; }

        [DataMember]
        public object B { get; set; }

        [DataMember]
        public object C { get; set; }

        [DataMember]
        public object D { get; set; }

        [DataMember]
        public object E { get; set; }

        [DataMember]
        public object F { get; set; }

        [DataMember]
        public object G { get; set; }

        [DataMember]
        public object H { get; set; }

        [DataMember]
        public object I { get; set; }

        [DataMember]
        public object J { get; set; }

        [DataMember]
        public object K { get; set; }

        [DataMember]
        public object L { get; set; }

        [DataMember]
        public object M { get; set; }

        [DataMember]
        public object N { get; set; }

        [DataMember]
        public object O { get; set; }

        [DataMember]
        public object P { get; set; }

        [DataMember]
        public object Q { get; set; }

        [DataMember]
        public object R { get; set; }

        [DataMember]
        public object S { get; set; }

        [DataMember]
        public object T { get; set; }

        [DataMember]
        public object U { get; set; }

        [DataMember]
        public object V { get; set; }

        [DataMember]
        public object W { get; set; }

        [DataMember]
        public object X { get; set; }

        [DataMember]
        public object Y { get; set; }

        [DataMember]
        public object Z { get; set; }
    }
}
