﻿#region Using directives
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel.Web;
using System.Security.Principal;
using System.Runtime.InteropServices;
using System.ComponentModel;
using EY.com.eBook.API.Contracts.Services;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.ProAcc.API.Contracts.Data;
using EY.com.eBook.ProAcc.API.Contracts.Services;
using EY.com.eBook.API.BL;
using EY.com.eBook.Core.EF;
using EY.com.eBook.API.Contracts.Data.Logic;
using System.ServiceModel.Activation;
using System.ServiceModel;
using System.Xml.Linq;
using EY.com.eBook.Core.Data;
using System.IO;
using System.Runtime.Serialization.Json;
using EY.com.eBook.API.Contracts.Proxies;
using EY.com.eBook.API.BL.Translators;
using System.Web;
using System.Configuration;
using EY.com.eBook.API.Excel.Data;
using System.Transactions;
using EY.com.eBook.API.Contracts;
using EY.com.eBook.API.Contracts.Data.Meta;
using EY.com.eBook.Core;
using EY.com.eBook.API.BL.Read;
#endregion

namespace EY.com.eBook.API.Implementation
{
    /// <summary>
    /// BizTaxService
    /// </summary>
    [EntityFrameworkServiceBehavior]
    [ServiceBehavior(IncludeExceptionDetailInFaults = true, ConcurrencyMode = ConcurrencyMode.Multiple, InstanceContextMode = InstanceContextMode.PerCall)]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class RepositoryService : IRepositoryService
    {

        #region IRepositoryService Members

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/UploadFile")]
        public bool UploadFile()
        {
            throw new NotImplementedException();
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
          RequestFormat = WebMessageFormat.Json,
          ResponseFormat = WebMessageFormat.Json,
          UriTemplate = "/GetStructureTree")]
        public RepositoryStructuresDataContract GetStructureTree(CriteriaCultureDataContract ccdc)
        {
            return new BL.Read.Repository().GetStructureTree(ccdc.Culture);
        }
        

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
          RequestFormat = WebMessageFormat.Json,
          ResponseFormat = WebMessageFormat.Json,
          UriTemplate = "/GetChildren")]
        public List<RepositoryNodeDataContract> GetChildren(CriteriaRepositoryChildrenDataContract crcdc)
        {
            return new BL.Read.Repository().GetChildren(crcdc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
         RequestFormat = WebMessageFormat.Json,
         ResponseFormat = WebMessageFormat.Json,
         UriTemplate = "/DeleteFile")]
        public bool DeleteFile(CriteriaIdDataContract cidc)
        {
            return new BL.Read.Repository().DeleteFile(cidc.Id);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
          RequestFormat = WebMessageFormat.Json,
          ResponseFormat = WebMessageFormat.Json,
          UriTemplate = "/GetStatusList")]
        public List<GuidListItemDataContract> GetStatusList(CriteriaStatusDataContract csdc)
        {
            return new BL.Read.Repository().GetStatusList(csdc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
         RequestFormat = WebMessageFormat.Json,
         ResponseFormat = WebMessageFormat.Json,
         UriTemplate = "/ReplaceItem")]
        public bool ReplaceItem(CriteriaReplaceRepositoryItemDataContract crridc)
        {
            return new BL.Read.Repository().ReplaceRepositoryItem(crridc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
          RequestFormat = WebMessageFormat.Json,
          ResponseFormat = WebMessageFormat.Json,
          UriTemplate = "/SaveItem")]
        public Guid SaveItem(CriteriaSaveRepositoryItemDataContract csridc)
        {
            return new BL.Read.Repository().SaveRepositoryItem(csridc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
          RequestFormat = WebMessageFormat.Json,
          ResponseFormat = WebMessageFormat.Json,
          UriTemplate = "/AddLink")]
        public bool AddLink(RepositoryLinkDataContract rldc)
        {
            return new Repository().AddLink(rldc);
        }


        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
          RequestFormat = WebMessageFormat.Json,
          ResponseFormat = WebMessageFormat.Json,
          UriTemplate = "/DeleteLink")]
        public bool DeleteLink(RepositoryListItemDataContract rlidc)
        {
            return new Repository().DeleteLink(rlidc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
          RequestFormat = WebMessageFormat.Json,
          ResponseFormat = WebMessageFormat.Json,
          UriTemplate = "/GetItemLinks")]
        public List<RepositoryListItemDataContract> GetItemLinks(CriteriaItemLinksDataContract cildc)
        {
            return new Repository().GetItemLinks(cildc);
        }


        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
         RequestFormat = WebMessageFormat.Json,
         ResponseFormat = WebMessageFormat.Json,
          UriTemplate = "/GetFileLinks")]
        public List<RepositoryListItemDataContract> GetFileLinks(CriteriaIdAndCultureDataContract cicdc)
        {
            return new Repository().GetFileLinks(cicdc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
         RequestFormat = WebMessageFormat.Json,
         ResponseFormat = WebMessageFormat.Json,
          UriTemplate = "/GetFileNode")]
        public RepositoryNodeDataContract GetFileNode(CriteriaIdAndCultureDataContract cicdc)
        {
            return new Repository().GetFileNode(cicdc);
        }

        /*public System.IO.Stream GetFileLinks(CriteriaIdDataContract cidc)
        {
            string json = new Repository().GetFileLinks(cidc);
            WebOperationContext.Current.OutgoingResponse.ContentType ="application/json; charset=utf-8";
            return new MemoryStream(Encoding.UTF8.GetBytes(json));
        }*/

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
          RequestFormat = WebMessageFormat.Json,
          ResponseFormat = WebMessageFormat.Json,
          UriTemplate = "/GetYearsInt")]
        public List<YearItemDataContract> GetYearsInt(CriteriaPeriodYearsDataContract cpydc)
        {
            List<YearItemDataContract> list = new List<YearItemDataContract>();
            if (!cpydc.PeriodStart.HasValue || !cpydc.PeriodEnd.HasValue)
            {
                cpydc.PeriodStart = DateTime.Now.AddYears(-5);
                cpydc.PeriodEnd = DateTime.Now.AddYears(+2);
            }

            int year = cpydc.PeriodStart.Value.Year;
            while (year <= cpydc.PeriodEnd.Value.Year)
            {
                list.Add(new YearItemDataContract { Id = year });
                year++;
            }
            return list;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
         RequestFormat = WebMessageFormat.Json,
         ResponseFormat = WebMessageFormat.Json,
         UriTemplate = "/GetQuarterOrMonthList")]
        public List<MonthDataContract> GetQuarterOrMonthList(CriteriaQuarterMonthDataContract cqmdc)
        {
            List<MonthDataContract> list = new List<MonthDataContract>();
            if (!cqmdc.PeriodStart.HasValue || !cqmdc.PeriodEnd.HasValue)
            {
                cqmdc.PeriodStart = new DateTime(DateTime.Now.Year - 1, 1, 1);
                cqmdc.PeriodEnd = new DateTime(DateTime.Now.Year + 1, 12, 31);
            }
            else
            {
                cqmdc.PeriodStart = new DateTime(cqmdc.PeriodStart.Value.Year, cqmdc.PeriodStart.Value.Month, 1);
                cqmdc.PeriodEnd = new DateTime(cqmdc.PeriodEnd.Value.Year, cqmdc.PeriodEnd.Value.Month, 1).AddMonths(1).AddDays(-1);
            }
            DateTime start = cqmdc.PeriodStart.Value;
            DateTime end = cqmdc.PeriodStart.Value.AddDays(-1) ;
            
            switch (cqmdc.QuarterOrMonth.ToUpper())
            {
                case "Q":
                    
                    int quarter=(int)Math.Floor(start.Month/3d);
                    while (end < cqmdc.PeriodEnd)
                    {
                        start = end.AddDays(1);
                        if(start.Year != end.Year) quarter=0;
                        end = start.AddMonths(3).AddDays(-1);
                        quarter++;
                        if (start < cqmdc.PeriodEnd.Value)
                        {
                            list.Add(new MonthDataContract { Id = string.Format("Q0{0}/{1}", quarter, end.Year) });
                        }
                    }
                    break;
                case "M":
                    while (start < cqmdc.PeriodEnd)
                    {
                        start = end.AddDays(1);
                        end = start.AddMonths(1).AddDays(-1);
                        if (start < cqmdc.PeriodEnd.Value)
                        {
                            list.Add(new MonthDataContract { Id = string.Format("{0}/{1}", start.Month.ToString().PadLeft(2, '0'), start.Year) });
                        }
                    }
                    break;
            }
            return list;
        }


        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
          RequestFormat = WebMessageFormat.Json,
          ResponseFormat = WebMessageFormat.Json,
          UriTemplate = "/QueryFiles")]
        public List<RepositoryNodeDataContract> QueryFiles(CriteriaQueryFilesDataContract cqfdc)
        {
            return new Repository().QueryFiles(cqfdc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
          RequestFormat = WebMessageFormat.Json,
          ResponseFormat = WebMessageFormat.Json,
          UriTemplate = "/QueryRepositoryItems")]
        public List<RepositoryItemDataContract> QueryRepositoryItems(CriteriaQueryFilesDataContract cqfdc)
        {
            return new Repository().QueryRepositoryItems(cqfdc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
          RequestFormat = WebMessageFormat.Json,
          ResponseFormat = WebMessageFormat.Json,
          UriTemplate = "/GthCheckEngagementLetters")]
        public Boolean GthCheckEngagementLetters(CriteriaRepositoryChildrenDataContract crcdc)
        {
            return new Repository().GthCheckEngagementLetters(crcdc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
          RequestFormat = WebMessageFormat.Json,
          ResponseFormat = WebMessageFormat.Json,
          UriTemplate = "/PersonHasGTHrole")]
        public Boolean PersonHasGTHrole(CriteriaIdDataContract cidc)
        {
            return new Repository().PersonHasGTHrole(cidc);
        }

        #endregion
    }	
}
