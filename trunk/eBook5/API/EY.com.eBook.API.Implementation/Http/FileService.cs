﻿#region Using directives
using System;
using System.Xml.Linq;
using System.Web;
using System.Transactions;
using System.Text;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.ServiceModel.Activation;
using System.Security.Principal;
using System.Runtime.Serialization.Json;
using System.Runtime.InteropServices;
using System.Linq;
using System.IO;
using System.Configuration;
using System.ComponentModel;
using System.Collections.Generic;
using EY.com.eBook.ProAcc.API.Contracts.Services;
using EY.com.eBook.ProAcc.API.Contracts.Data;
using EY.com.eBook.Core;
using EY.com.eBook.Core.EF;
using EY.com.eBook.Core.Data;
using EY.com.eBook.API.Excel.Data;
using EY.com.eBook.API.Contracts;
using EY.com.eBook.API.Contracts.Services;
using EY.com.eBook.API.Contracts.Proxies;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.API.Contracts.Data.Meta;
using EY.com.eBook.API.Contracts.Data.Logic;
using EY.com.eBook.API.Contracts.Data.Cache;
using EY.com.eBook.API.BL;
using EY.com.eBook.API.BL.Translators;
using EY.com.eBook.API.BL.Read;

#endregion

namespace EY.com.eBook.API.Implementation
{
    /// <summary>
    /// FileService
    /// </summary>
    [EntityFrameworkServiceBehavior]
    [ServiceBehavior(IncludeExceptionDetailInFaults = true, ConcurrencyMode = ConcurrencyMode.Multiple, InstanceContextMode = InstanceContextMode.PerCall)]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class FileService : IFileService
    {

        #region IFileService Members
        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/DeclareBizTax")]
        public bool DeclareBizTax(CriteriaIdDataContract cidc)
        {
            // return new BizTaxStandardRenderer().Declare(cidc.Id);
            throw new Exception("METHOD NOT IN USE");
            return false; // NOT IN USE
        }


        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/GetFileInfos")]
        public List<FileInfoDataContract> GetFileInfos(CriteriaFilesDataContract cfdc)
        {
            return new BL.Read.FileReadRepository().GetFileInfos(cfdc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/GetFiles")]
        public List<FileDataContract> GetFiles(CriteriaFilesDataContract cfdc)
        {
            return new BL.Read.FileReadRepository().GetFiles(cfdc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/GetFile")]
        public FileDataContract GetFile(CriteriaIdAndCultureDataContract cidc)
        {
            return new BL.Read.FileReadRepository().GetFile(cidc);
        }


        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/CreateFile")]
        public Guid CreateFile(CriteriaCreateFileDataContract ccfdc)
        {
            return new BL.Write.FileRepository().CreateFile(ccfdc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/ImportAccountseBook")]
        public bool ImportAccountseBook(ImportAccountseBookDataContract iaedc)
        {
            return new BL.Write.FileRepository().ImportAccountseBook(iaedc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/ImportMappingseBook")]
        public bool ImportMappingseBook(CriteriaFileDataContract cfdc)
        {
            return new BL.Write.FileRepository().ImportMappingseBook(cfdc);
        }


        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/ImportAccountsProAcc")]
        public DateTime? ImportAccountsProAcc(ImportAccountseBookDataContract iaedc)
        {
            return new BL.Write.FileRepository().ImportAccountsProAcc(iaedc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/ImportAccountsExcel")]
        public DateTime? ImportAccountsExcel(CriteriaExcelSheetMappedDataContract cesmdc)
        {
            return new BL.Write.FileRepository().ImportAccountsExcel(cesmdc);
        }

        /*
        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/ImportAccountsTxt")]
        public DateTime? ImportAccountsTxt(CriteriaExcelSheetMappedDataContract cesmdc)
        {
            return new BL.Write.FileRepository().ImportAccountsTxt(cesmdc);
        }*/

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/ImportCurrentStateExcel")]
        public DateTime? ImportCurrentStateExcel(CriteriaExcelSheetMappedDataContract cesmdc)
        {
            return new BL.Write.FileRepository().ImportAccountsBalansExcel(cesmdc, false);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/ImportAccountsTxt")]
        public DateTime? ImportAccountsTxt(CriteriaExcelSheetMappedDataContract cesmdc)
        {
            return new BL.Write.FileRepository().ImportAccountsTxt(cesmdc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/ImportCurrentStateTxt")]
        public DateTime? ImportCurrentStateTxt(CriteriaExcelSheetMappedDataContract cesmdc)
        {
            return new BL.Write.FileRepository().ImportAccountsBalansTxt(cesmdc, false);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/ImportPreviousStateExcel")]
        public DateTime? ImportPreviousStateExcel(CriteriaExcelSheetMappedDataContract cesmdc)
        {
            return new BL.Write.FileRepository().ImportAccountsBalansExcel(cesmdc, true);
        }


        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/SetFileInCache")]
        public bool SetFileInCache(CriteriaFileDataContract cfdc)
        {
            CacheProxy cp = new CacheProxy();
            cp.Open();
            cp.ReloadOrAddFileInCache(cfdc.FileId);
            cp.Close();
            return true;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/ForceFileCacheReload")]
        public bool ForceFileCacheReload(CriteriaFileDataContract cfdc)
        {
            CacheProxy cp = new CacheProxy();
            cp.Open();
            cp.ReloadOrAddFileInCacheForce(cfdc.FileId, true);
            cp.Close();
            return true;
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/GetMessages")]
        public List<FileMessageDataContract> GetMessages(CriteriaFileDataContract cfdc)
        {
            return new BL.Read.FileReadRepository().GetMessages(cfdc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/GetMessagesWithoutNotes")]
        public List<FileMessageDataContract> GetMessagesWithoutNotes(CriteriaFileDataContract cfdc)
        {
            return new BL.Read.FileReadRepository().GetMessagesWithoutNotes(cfdc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/GetNotes")]
        public List<FileMessageDataContract> GetNotes(CriteriaFileDataContract cfdc)
        {
            return new BL.Read.FileReadRepository().GetNotes(cfdc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/GetFileMeta")]
        public FileMetaDataContract GetFileMeta(CriteriaIdAndPersonDataContract cipdc)
        {
            return new BL.Read.FileReadRepository().GetMeta(cipdc.Id);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/GetMetaByClientId")]
        public List<FileMetaDataContract> GetMetaByClientId(CriteriaIdDataContract cidc)
        {
            return new BL.Read.FileReadRepository().GetMetaByClientId(cidc.Id);
        }


        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/SaveFileMeta")]
        public bool SaveFileMeta(CriteriaFileMetaDataContract cfmdc)
        {

            new BL.Write.FileRepository().SaveMeta(cfmdc);
            return true;
        }


        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/GetFileSettings")]
        public FileSettingsInfoDataContract GetFileSettings(CriteriaIdDataContract cidc)
        {
            return new BL.Read.FileReadRepository().GetFileSettings(cidc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/SaveFileSettings")]
        public bool SaveFileSettings(CriteriaSaveFileSettingsDataContract csfsdc)
        {
            bool res = new BL.Write.FileRepository().SaveFileSettings(csfsdc);
            CacheProxy cp = new CacheProxy();
            cp.Open();
            cp.RemoveFileFromCache(csfsdc.Settings.FileId);
            cp.Close();
            cp = null;
            return res;

        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/SetFileMarkDeleted")]
        public bool SetFileMarkDeleted(CriteriaIdDataContract cidc)
        {

            new BL.Write.FileRepository().MarkFileForDeletion(cidc.Id);
            return true;

        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/SetFileUnMarkDeleted")]
        public bool SetFileUnMarkDeleted(CriteriaIdDataContract cidc)
        {

            new BL.Write.FileRepository().UnMarkFileForDeletion(cidc.Id);
            return true;

        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/DeleteFile")]
        public bool DeleteFile(CriteriaIdDataContract cidc)
        {
            new BL.Write.FileRepository().DeleteFile(cidc.Id);
            return true;

        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/ReopenFile")]
        public void ReopenFile(List<CriteriaIdAndBoolDataContract> lcidc)
        {
            new BL.Write.FileRepository().ReopenFile(lcidc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
          RequestFormat = WebMessageFormat.Json,
          ResponseFormat = WebMessageFormat.Json,
          UriTemplate = "/CloseFile")]
        public void CloseFile(List<CriteriaIdDataContract> lcibdc)
        {
            new BL.Write.FileRepository().CloseFile(lcibdc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
          RequestFormat = WebMessageFormat.Json,
          ResponseFormat = WebMessageFormat.Json,
          UriTemplate = "/GetExactAdmins")]
        public List<ExactAdminDataContract> GetExactAdmins(CriteriaIdDataContract cidc)
        {
            return new BL.Read.FileReadRepository().GetExactAdmins(cidc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
          RequestFormat = WebMessageFormat.Json,
          ResponseFormat = WebMessageFormat.Json,
          UriTemplate = "/GetExactFinYears")]
        public List<ExactFinYearsDataContract> GetExactFinYears(CriteriaStringDataContract csdc)
        {
            return new BL.Read.FileReadRepository().GetExactFinYears(csdc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
          RequestFormat = WebMessageFormat.Json,
          ResponseFormat = WebMessageFormat.Json,
          UriTemplate = "/GetDistinctFilesWithCounter")]
        public List<DistinctCounterFileDataContract> GetDistinctFilesWithCounter(CriteriaStringDataContract csdc) 
        {
            return new BL.Read.FileReadRepository().GetDistinctFilesWithCounter(csdc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/CloseService")]
        public void CloseService(CriteriaFileServicePersonDataContract cfspdc)
        {
            new BL.Write.FileServiceRepository().CloseService(cfspdc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/ReOpenService")]
        public void ReOpenService(CriteriaFileServicePersonDataContract cfspdc)
        {
            new BL.Write.FileServiceRepository().ReOpenService(cfspdc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
          RequestFormat = WebMessageFormat.Json,
          ResponseFormat = WebMessageFormat.Json,
          UriTemplate = "/GetFileServiceStatus")]
        public FileServiceStatusDataContract GetFileServiceStatus(CriteriaFileServiceDataContract cfsdc)
        {
            return new BL.Read.FileServiceRepository().GetFileServiceStatus(cfsdc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
          RequestFormat = WebMessageFormat.Json,
          ResponseFormat = WebMessageFormat.Json,
          UriTemplate = "/GetFileServiceLog")]
        public List<FileServiceLogDataContract> GetFileServiceLog(CriteriaFileServiceDataContract cfsdc)
        {
            return new BL.Read.FileServiceRepository().GetFileServiceLog(cfsdc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
              RequestFormat = WebMessageFormat.Json,
              ResponseFormat = WebMessageFormat.Json,
              UriTemplate = "/LogFileServiceActions")]
        public void LogFileServiceActions(UpdateFileServiceDataContract ufsdc)
        {
            new BL.Write.FileServiceRepository().LogFileServiceActions(ufsdc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
          RequestFormat = WebMessageFormat.Json,
          ResponseFormat = WebMessageFormat.Json,
          UriTemplate = "/UpdateFileService")]
        public FileServiceStatusDataContract UpdateFileService(UpdateFileServiceDataContract ufsdc)
        {
            return new BL.Write.FileServiceRepository().UpdateFileService(ufsdc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
          RequestFormat = WebMessageFormat.Json,
          ResponseFormat = WebMessageFormat.Json,
          UriTemplate = "/ResetServiceFlow")]
        public void ResetServiceFlow(UpdateFileServiceDataContract ufsdc)
        {
            new BL.Write.FileServiceRepository().ResetServiceFlow(ufsdc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "/GetFileServiceReport")]
        public ResponseDataContract<List<ClientPersonFileServiceDataContract>> GetFileServiceReport(CriteriaClientPersonFileServiceDataContract ccpfsdc)
        {
            return new BL.Read.FileServiceRepository().GetFileServiceReport(ccpfsdc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "/GetServiceStatusesList")]
        public List<StatusDataContract> GetServiceStatusesList(CriteriaServiceCultureDataContract cscdc)
        {
            return new BL.Read.FileServiceRepository().GetServiceStatusesList(cscdc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "/GetUserSetStatusesList")]
        public List<StatusDataContract> GetUserSetStatusesList(CriteriaFileServicePersonDataContract cfspdc)
        {
            return new BL.Read.FileServiceRepository().GetUserSetStatusesList(cfspdc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "/AddService")]
        public FileServiceDataContract AddService(CriteriaFileServicePersonDataContract cfspdc)
        {
            return new BL.Write.FileServiceRepository().AddService(cfspdc);
        }

        #endregion
    }
}
