﻿#region Using directives
using System;
using System.Xml.Linq;
using System.Web;
using System.Transactions;
using System.Text;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.ServiceModel.Activation;
using System.Security.Principal;
using System.Runtime.Serialization.Json;
using System.Runtime.InteropServices;
using System.Linq;
using System.IO;
using System.Configuration;
using System.ComponentModel;
using System.Collections.Generic;
using EY.com.eBook.ProAcc.API.Contracts.Services;
using EY.com.eBook.ProAcc.API.Contracts.Data;
using EY.com.eBook.Core;
using EY.com.eBook.Core.EF;
using EY.com.eBook.Core.Data;
using EY.com.eBook.API.Excel.Data;
using EY.com.eBook.API.Contracts;
using EY.com.eBook.API.Contracts.Services;
using EY.com.eBook.API.Contracts.Proxies;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.API.Contracts.Data.Meta;
using EY.com.eBook.API.Contracts.Data.Logic;
using EY.com.eBook.API.Contracts.Data.Cache;
using EY.com.eBook.API.BL;
using EY.com.eBook.API.BL.Translators;
using EY.com.eBook.Excel.BL;
using ProtoBuf.Meta;

#endregion

namespace EY.com.eBook.API.Implementation
{
    /// <summary>
    /// ExcelService
    /// </summary>
    [EntityFrameworkServiceBehavior]
    [ServiceBehavior(IncludeExceptionDetailInFaults = true, ConcurrencyMode = ConcurrencyMode.Multiple, InstanceContextMode = InstanceContextMode.PerCall)]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class AccountSchemaService : IAccountSchemaService
    {
        //public AccountSchemaService()
        //{
        //    //RuntimeTypeModel.Default.AutoAddMissingTypes = true;
        //    //RuntimeTypeModel.Default.AutoCompile = true;
        //    //RuntimeTypeModel.Default.Add(typeof(List<AccountDataContract>), true);
        //}


        #region IAccountSchemaService Members
        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/GetAccountsMappingView")]
        public List<AccountDataContract> GetAccountsMappingView(CriteriaAccountMappingViewDataContract cicdc)
        {
            return new BL.Read.AccountSchemaRepository().GetAccountsMappingView(cicdc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/GetAccountsList")]
        public ResponseDataContract<List<StringListItemDataContract>> GetAccountsList(CriteriaAccountsDataContract cadc)
        {
            return new BL.Read.AccountSchemaRepository().GetAccountsList(cadc);
        }


        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/GetQueriedAccountsList")]
        public List<StringListItemDataContract> GetQueriedAccountsList(CriteriaAccountsDataContract cadc)
        {
            return new BL.Read.AccountSchemaRepository().GetQueriedAccountsList(cadc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/GetAccountsListByRanges")]
        public ResponseDataContract<List<StringListItemDataContract>> GetAccountsListByRanges(CriteriaAccountsByRangesDataContract cardc)
        {
            return new BL.Read.AccountSchemaRepository().GetAccountsListByRanges(cardc);
        }


        //[WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
        //   RequestFormat = WebMessageFormat.Json,
        //   ResponseFormat = WebMessageFormat.Json,
        //   UriTemplate = "/GetAccountsListByRanges")]
        //public ResponseDataContract<List<StringListItemDataContract>> GetAccountsListByRanges(CriteriaAccountsByRangesDataContract cardc)
        //{
        //    return new BL.Read.AccountSchemaRepository().GetAccountsListByRanges(cardc);
        //}

        public List<AccountDescriptionDataContract> GetAccountTranslations(CriteriaAccountDataContract cadc)
        {
            throw new NotImplementedException();
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/GetAccount")]
        public AccountDataContract GetAccount(CriteriaAccountDataContract cadc)
        {
            return new BL.Read.AccountSchemaRepository().GetAccount(cadc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/GetAccountListItem")]
        public StringListItemDataContract GetAccountListItem(CriteriaAccountDataContract cadc)
        {
            cadc.IncludeAllDescriptions = true;
            AccountDataContract adc = new BL.Read.AccountSchemaRepository().GetAccount(cadc);
            if (adc == null) return new StringListItemDataContract { };
            return adc.GetListItem();
        }


        public List<AccountDataContract> GetAllAccounts(CriteriaIdAndCultureDataContract cicdc)
        {
            throw new NotImplementedException();
        }

        public ResponseDataContract<List<AccountDataContract>> GetAccountsDeep(CriteriaAccountsDataContract cardc)
        {
            throw new NotImplementedException();
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
          RequestFormat = WebMessageFormat.Json,
          ResponseFormat = WebMessageFormat.Json,
          UriTemplate = "/GetAccountsListFilteredByMapping")]
        public ResponseDataContract<List<StringListItemDataContract>> GetAccountsListFilteredByMapping(CriteriaAccountFilteredByMappingDataContract cadc)
        {
            return new BL.Read.AccountSchemaRepository().GetAccountsListByMapping(cadc);
        }

        public List<AccountMappingDataContract> GetAccountMappings(CriteriaSingleAccountDataContract csadc)
        {
            throw new NotImplementedException();
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/SaveNewAccount")]
        public bool SaveNewAccount(AccountDataContract adc)
        {
            return new BL.Write.AccountSchemaRepository().SaveNewAccount(adc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/SaveAccount")]
        public bool SaveAccount(AccountDataContract adc)
        {
            return new BL.Write.AccountSchemaRepository().SaveAccount(adc);
        }

        

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/SaveAccountMapping")]
        public AccountMappingDataContract SaveAccountMapping(AccountMappingDataContract amdc)
        {
            return new BL.Write.AccountSchemaRepository().SaveAccountMapping(amdc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/DeleteAccountMapping")]
        public void DeleteAccountMapping(CriteriaAccountMappingDataContract camdc)
        {
            new BL.Write.AccountSchemaRepository().DeleteAccountMapping(camdc);
        }

        public bool ImportAccountsProAcc(CriteriaIdDataContract cidc)
        {
            throw new NotImplementedException();
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/GetAllBookings")]
        public List<BookingSingleDataContract> GetAllBookings(CriteriaFileDataContract cfdc)
        {
            return new BL.Read.AccountSchemaRepository().GetAllbookings(cfdc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
                   RequestFormat = WebMessageFormat.Json,
                   ResponseFormat = WebMessageFormat.Json,
                   UriTemplate = "/GetAutomaticBookings")]
        public List<BookingSingleDataContract> GetAutomaticBookings(CriteriaFileDataContract cfdc)
        {
            return new BL.Read.AccountSchemaRepository().GetAutomaticBookings(cfdc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/GetManualBookings")]
        public List<BookingSingleDataContract> GetManualBookings(CriteriaFileDataContract cfdc)
        {
            return new BL.Read.AccountSchemaRepository().GetManualBookings(cfdc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/GetWorksheetBookings")]
        public List<BookingSingleDataContract> GetWorksheetBookings(CriteriaWorksheetDataContract cwdc)
        {
            return new BL.Read.AccountSchemaRepository().GetWorksheetBookings(cwdc);
        }


        public List<AccountStateDataContract> GetState(CriteriaShemaStateDataContract cssdc)
        {
            throw new NotImplementedException();
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/GetFinalTrialBalance")]
        public List<FinalTrialBalanceLineDataContract> GetFinalTrialBalance(CriteriaIdAndCultureDataContract cicdc)
        {
            return new BL.Read.AccountSchemaRepository().GetFinalTrialBalance(cicdc);
        }

        public BookingDataContract GetBooking(CriteriaIdDataContract cidc)
        {
            throw new NotImplementedException();
        }

        public List<BookingDataContract> GetProAccExportBookings(CriteriaIdDataContract cidc)
        {
            throw new NotImplementedException();
        }

        
        public List<BookingDataContract> GetBookings(CriteriaIdAndCultureDataContract cicdc)
        {
            throw new NotImplementedException();
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/SaveBooking")]
        public BookingDataContract SaveBooking(CriteriaSaveBookingDataContract csbdc)
        {
            return new BL.Write.AccountSchemaRepository().SaveBooking(csbdc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/DeleteBooking")]
        public void DeleteBooking(Guid id)
        {
            new BL.Write.AccountSchemaRepository().DeleteBooking(id);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/GetBookingLinesByAccountNr")]
        public List<BookingSingleDataContract> GetBookingLinesByAccountNr(CriteriaBookingLineDataContract cbldc)
        {
            return new BL.Read.AccountSchemaRepository().GetBookingLinesByAccountNr(cbldc);
        } 


        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/GetSchemaBooking")]
        public List<BookingSingleDataContract> GetSchemaBooking(CriteriaIdDataContract cidc)
        {
            return new BL.Read.AccountSchemaRepository().GetSchemaBooking(cidc);
        } 
        


        #endregion
    }	
}
