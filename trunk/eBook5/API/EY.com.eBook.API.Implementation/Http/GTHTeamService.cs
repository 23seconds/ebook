﻿#region Using directives
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel.Web;
using System.Security.Principal;
using System.Runtime.InteropServices;
using System.ComponentModel;
using EY.com.eBook.API.Contracts.Services;
using EY.com.eBook.API.Contracts.Data;
using EY.com.eBook.ProAcc.API.Contracts.Data;
using EY.com.eBook.ProAcc.API.Contracts.Services;
using EY.com.eBook.API.BL;
using EY.com.eBook.Core.EF;
using EY.com.eBook.API.Contracts.Data.Logic;
using System.ServiceModel.Activation;
using System.ServiceModel;
using System.Xml.Linq;
using EY.com.eBook.Core.Data;
using System.IO;
using System.Runtime.Serialization.Json;
using EY.com.eBook.API.Contracts.Proxies;
using EY.com.eBook.API.BL.Translators;
using System.Web;
using System.Configuration;
using EY.com.eBook.API.Excel.Data;
using System.Transactions;
using EY.com.eBook.API.Contracts;
using EY.com.eBook.API.Contracts.Data.Meta;
using EY.com.eBook.Core;
using EY.com.eBook.API.BL.Read;
using EY.com.eBook.BL.Export;
#endregion

namespace EY.com.eBook.API.Implementation
{
    /// <summary>
    /// BizTaxService
    /// </summary>
    [EntityFrameworkServiceBehavior]
    [ServiceBehavior(IncludeExceptionDetailInFaults = true, ConcurrencyMode = ConcurrencyMode.Multiple, InstanceContextMode = InstanceContextMode.PerCall)]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class GTHTeamService : IGTHTeamService
    {
        

        #region IGTHTeamService Members

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "/GetClients")]
        public ResponseDataContract<List<GTHClientStatusDataContract>> GetClients(CriteriaGTHSearchClientTeamDataContract cstdc)
        {
            return new GTHRepository().GetClients(cstdc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "/GetAllClients")]
        public ResponseDataContract<List<GTHClientStatusDataContract>> GetAllClients(CriteriaGTHSearchClientTeamDataContract cstdc)
        {
            return new GTHRepository().GetAllClients(cstdc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "/SelectClient")]
        public GTHClientStatusDataContract SelectClient(CriteriaClientTeamDataContract cctdc)
        {
            Guid id = new BL.Write.GTHRepository().SelectClient(cctdc);
            return new GTHRepository().GetGTHClient(id);
        }


        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "/GetClientStartList")]
        public GTHClientStartDataContract GetClientStartList(CriteriaGTHTeamDataContract ctdc)
        {
            return new GTHRepository().GetClientStartList(ctdc);
        }

        #endregion

        #region IGTHTeamService Members


        public GTHClientStatusDataContract StartClient(CriteriaStartGTHClientStatusDataContract csdc)
        {
            throw new NotImplementedException();
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "/ProcessFilesTeam1")]
        public bool ProcessFilesTeam1(CriteriaProcessFilesDataContract cpfdc)
        {
            return new BL.Write.GTHRepository().ProcessNBBFiles(cpfdc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "/ProcessFilesTeam2")]
        public bool ProcessFilesTeam2(CriteriaProcessFilesDataContract cpfdc)
        {
            return new BL.Write.GTHRepository().ProcessTeam2Files(cpfdc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "/ProcessTeam6")]
        public bool ProcessTeam6(CriteriaProcessFilesDataContract cpfdc)
        {
            return new BL.Write.GTHRepository().ProcessTeam6(cpfdc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "/getPersons")]
        public List<PersonBaseDataContract> getPersons()
        {
            return new BL.Read.GTHRepository().getPersons();
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "/GetPersonClients")]
        public List<GTHPersonClientDataContract> GetPersonClients(CriteriaStringDataContract csdc)
        {
            return new BL.Read.GTHRepository().GetPersonClients(csdc);
        }


        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "/SavePerson")]
        public void SavePerson(CriteriaPersonDataContract cpdc)
        {
            new BL.Write.GTHRepository().SavePerson(cpdc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "/GetPersonRoles")]
        public List<PersonRolesDataContract> GetPersonRoles(CriteriaBaseIdDataContract cbidc)
        {
            return new BL.Read.GTHRepository().GetPersonRoles(cbidc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "/SavePersonRoles")]
        public void SavePersonRoles(List<CriteriaPersonRolesDataContract> lcprdc)
        {
            new BL.Write.GTHRepository().SavePersonRoles(lcprdc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "/GetRoles")]
        public List<GTHRolesDataContract> GetRoles()
        {
            return new BL.Read.GTHRepository().GetRoles();
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/ReplacePersons")]
        public void ReplacePersons(CriteriaReplacePersonsDataContract crpdc)
        {
            new BL.Write.GTHRepository().ReplacePersons(crpdc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/Team5NotifyTeam")]
        public void Team5NotifyTeam(CriteriaIdAndCultureDataContract ciacdc)
        {
            new BL.Read.GTHRepository().Team5NotifyTeam(ciacdc);
        }

        [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped,
           RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "/Team5MissingFiles")]
        public void Team5MissingFiles(CriteriaIdAndCultureDataContract ciacdc)
        {
            new BL.Read.GTHRepository().Team5MissingFiles(ciacdc);
        }


        #endregion
    }	
}
